GO
/****** Object:  StoredProcedure [dbo].[IsClientExist]    Script Date: 30-May-18 10:02:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
EXEC IsClientExist @ClientName='abc'
*/

CREATE PROCEDURE [dbo].[IsClientExist]
(  
	@ClientName VARCHAR(10)
)  

AS  
BEGIN  
DECLARE @Result VARCHAR(8000)    
		SELECT @Result=COALESCE(MAX('Y' ),'N')
		FROM vwClientNew 
		WHERE ClientName LIKE @ClientName+'%'
		
	SELECT @Result  
END
