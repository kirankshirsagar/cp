﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web;
using CommonBLL;

namespace UserManagementBLL
{

   public class RolePermissions : IRolePermissions
    {
        RolePermissionsDAL obj;
        public int RoleId { get; set; }
        public int UsersId { get; set; }
        public int ParentId { get; set; }
        public int ChildId { get; set; }
        public int AccessId { get; set; }

        public string ParentName { get; set; }
        public string ChildName { get; set; }
        public string Add { get; set; }
        public string Update { get; set; }
        public string Delete { get; set; }
        public string View { get; set; }

        public string AddReadOnly { get; set; }
        public string UpdateReadOnly { get; set; }
        public string DeleteReadOnly { get; set; }
        public string ViewReadOnly { get; set; }
        public string AllReadOnly { get; set; }

        public string ControlName { get; set; }
        public string isApplicable { get; set; }
        
        public List<RolePermissions> ReadParent { get; set; }
        public List<RolePermissions> ReadChild { get; set; }
        public List<RolePermissions> ReadGrandChild { get; set; }


        public  DataTable RoleAccess { get; set; }
        public DataTable RoleAccessDynamic { get; set; }

        int x = 0; 


        public void ReadData()
        {
            try
            {
                obj = new RolePermissionsDAL();
                obj.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       
        public string InsertRecord()
        {
            try
            {
                obj = new RolePermissionsDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


       /// <summary>
       /// This table will returns page access permissions for the role..
       /// </summary>
        /// <param name="rpt"> Parent Repeater Name </param>
       /// <returns></returns>
        public DataTable GetAccessTable(Repeater rpt)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ChildId", typeof(int));
            dt.Columns.Add("Add", typeof(string));
            dt.Columns.Add("Update", typeof(string));
            dt.Columns.Add("Delete", typeof(string));
            dt.Columns.Add("View", typeof(string));

            foreach (RepeaterItem itemParent in rpt.Items)
            {
                Repeater rptChild = (Repeater)itemParent.FindControl("rptChild");

                foreach (RepeaterItem itemChild in rptChild.Items)
                {
                    Label lblChildId = (Label)itemChild.FindControl("lblChildId");
                    HtmlInputCheckBox chkAdd = (HtmlInputCheckBox)itemChild.FindControl("chkAdd");
                    HtmlInputCheckBox chkUpdate = (HtmlInputCheckBox)itemChild.FindControl("chkUpdate");
                    HtmlInputCheckBox chkDelete = (HtmlInputCheckBox)itemChild.FindControl("chkDelete");
                    HtmlInputCheckBox chkView = (HtmlInputCheckBox)itemChild.FindControl("chkView");

                    dt.Rows.Add
                     (  
                        int.Parse(lblChildId.Text),
                        chkAdd.Checked ? "Y" : "N",
                        chkUpdate.Checked ? "Y" : "N",
                        chkDelete.Checked ? "Y" : "N",
                        chkView.Checked ? "Y" : "N"
                      );

                }
            }
            return dt;
        }



        /// <summary>
        /// This table will returns featur specific permissions for the role..
        /// </summary>
        /// <param name="rpt"> Parent Repeater Name </param>
        /// <returns></returns>


        public DataTable GetAccessTableFeature(Repeater rpt)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("AccessId", typeof(int));

            foreach (RepeaterItem itemParent in rpt.Items)
            {
                Repeater rptChild = (Repeater)itemParent.FindControl("rptChild");

                foreach (RepeaterItem itemChild in rptChild.Items)
                {
                    Repeater rptGrandChild = (Repeater)itemChild.FindControl("rptGrandChild");
                    foreach (RepeaterItem itemGrangChild in rptGrandChild.Items)
                    {
                        Label lblAccessId = (Label)itemGrangChild.FindControl("lblAccessId");
                        HtmlInputCheckBox chkisApplicable = (HtmlInputCheckBox)itemGrangChild.FindControl("chkisApplicable");

                        if (chkisApplicable.Checked == true) { 
                        dt.Rows.Add
                         (
                            int.Parse(lblAccessId.Text)
                          );
                        }

                    }

                }
            }
            return dt;
        }
    }
}

