﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;

namespace UserManagementBLL
{
    public interface IUsers : ICrud, INavigation
    {
        string Flag { get; set; }
        int UserId { get; set; }
        int CityId { get; set; }
        string CityName { get; set; }
        int CountryId { get; set; }
        string CountryName { get; set; }
        int StateId { get; set; }
        string StateName { get; set; }
        int RoleId { get; set; }
        string RoleName { get; set; }
        int DepartmentId { get; set; }
        string DepartmentName { get; set; }
        int TitleId { get; set; }
        string TitleName { get; set; }
        string ImageUrl { get; set; }
        string UserIds { get; set; }
        string FirstName { get; set; }
        string MiddleName { get; set; }
        string LastName { get; set; }
        string Address { get; set; }
        string MobileNo { get; set; }
        string Phone { get; set; }
        string Email { get; set; }
        string UserName { get; set; }
        string Pincode { get; set; }
        string MailTo { get; set; }
        string MailFrom { get; set; }
        string Search { get; set; }
        string IsUsed { get; set; }
        string FullName { get; set; }
        string isActive { get; set; }
        string AccessIds { get; set; }
        string AccessMainSectionIds { get; set; }
        string IsValidLink();
        
        string ChangeIsActive();
        string UpdateUserPasswordExpireStatus();
        string UserPasswordActivityLog();
        string UserPasswordActivityLog(int update);

        string MailType { get; set; }
        char IsOutlookSynchEnable { get; set; }
        string OutlookFileName { get; set; }

        List<SelectControlFields> SelectData(int withOutSelect = 0);

        List<Users> ReadData();

        void CreateDALObjForMembershipWork();
        void GetUsersDetails();
        void GetUsersDetailsByEmailId();
        string GetEmailId();
        string GetUserFirstName();
        int GetUserEmailExists();
        string CheckUserIsActive();
        string getTotalLoginfailed();
        string GetUserNameDB();
        string LockUser();
        void PasswordChangedSuccessfully();
        void UpdateUserPasswordExpireStatusConfirm();
        string GetUserFullName();
        string UnlockUsers();
        string ProfileImgUrl();
        List<SelectControlFields> SelectSubordinateUsers(int withOutSelect = 0);
        List<SelectControlFields> SelectAllUsers();
        string RemainingCount();
        int ContractTypeId { get; set; }

    }

}
