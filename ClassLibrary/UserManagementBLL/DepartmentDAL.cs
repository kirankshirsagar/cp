﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;


namespace UserManagementBLL
{
    public  class DepartmentDAL : DAL
    {
        public string InsertRecord(IDepartment I)
        {

            Parameters.AddWithValue("@DepartmentId",I.DepartmentId);
            Parameters.AddWithValue("@DepartmentName",I.DepartmentName);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@Status",0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterDepartmentAddUpdate", "@Status");
        }

        public string UpdateRecord(IDepartment I)
        {
            Parameters.AddWithValue("@DepartmentId", I.DepartmentId);
            Parameters.AddWithValue("@DepartmentName", I.DepartmentName);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterDepartmentAddUpdate", "@Status");
        }

        public string DeleteRecord(IDepartment I)
        {
            Parameters.AddWithValue("@DepartmentIds", I.DepartmentIds);
            return getExcuteQuery("MasterDepartmentDelete");
        }

        public string ChangeIsActive(IDepartment I)
        {
            Parameters.AddWithValue("@DepartmentIds", I.DepartmentIds);
            Parameters.AddWithValue("@isActive", I.isActive);
            return getExcuteQuery("MasterDepartmentChangeIsActive");
        }

        public List<SelectControlFields>SelectData(IDepartment I)
        {
            SqlDataReader dr = getExecuteReader("MasterDepartmentSelect");
            return SelectControlFields.SelectData(dr);
        }


        public List<Department> ReadData(IDepartment I)
        {
            int i = 0;
            List<Department> myList = new List<Department>();
            Parameters.AddWithValue("@DepartmentId", I.DepartmentId);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            SqlDataReader dr = getExecuteReader("MasterDepartmentRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new Department());
                    myList[i].DepartmentId = int.Parse(dr["DepartmentId"].ToString());
                    myList[i].DepartmentName = dr["DepartmentName"].ToString();
                    myList[i].IsUsed = dr["isUsed"].ToString();
                    myList[i].isActive = dr["isActive"].ToString();
                    myList[i].Description = dr["Description"].ToString();
          
                    //myList[i].AddedBy = dr["UserName"].ToString();
                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally{
                if (dr.IsClosed != true && dr != null)
                dr.Close();
            }
            return myList;
        }




    }
}
