﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonBLL;
using System.Data.SqlClient;

namespace UserManagementBLL
{
   public class AccessHomeDAL : DAL
   {
       public List<AccessHome> ReadData(IAccessHome I)
       {
           int i = 0;
           List<AccessHome> myList = new List<AccessHome>();
           Parameters.AddWithValue("@UserId", I.UserId);
           SqlDataReader dr = getExecuteReader("AccessHomePage");
           try
           {
               while (dr.Read())
               {
                   myList.Add(new AccessHome());
                   myList[i].MyDashBoard = dr["MyDashBoard"].ToString();
                   myList[i].MyContracts = dr["MyContracts"].ToString();
                   myList[i].MyReports = dr["MyReports"].ToString();
                   myList[i].CreateNewContract = dr["CreateNewContract"].ToString();
                   myList[i].NewReviewRequest = dr["NewReviewRequest"].ToString();
                   myList[i].MyClients = dr["MyClients"].ToString();
                   myList[i].RenewContract = dr["RenewContract"].ToString();
                   myList[i].ReviseContract = dr["ReviseContract"].ToString();
                   myList[i].MyVault = dr["MyVault"].ToString();
                   myList[i].TerminateContract = dr["TerminateContract"].ToString();
                   myList[i].AutoRenewal = dr["AutoRenewal"].ToString();
                   myList[i].Calender = dr["Calender"].ToString();
                   myList[i].CustomReport = dr["CustomReport"].ToString();
                   myList[i].AdvanceSearch = dr["AdvanceSearch"].ToString();
                   myList[i].AuditTrail = dr["AuditTrail"].ToString();
                   i = i + 1;
               }

               dr.NextResult();
               while (dr.Read())
               {
                   myList.Add(new AccessHome());
                   myList[i].PageLink = dr["PageName"].ToString();
                   myList[i].ParentId = Convert.ToInt16(dr["ParentId"]);
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
               if (dr.IsClosed != true && dr != null)
                   dr.Close();
           }
           return myList;
       }

   }
}
