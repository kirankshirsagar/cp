﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;

namespace UserManagementBLL
{
    public class UsersDAL : DAL
    {
        public string InsertRecord(IUsers I)
        {
            Parameters.Clear();
            //Parameters.AddWithValue("@CityId", I.CityId);
            //Parameters.AddWithValue("@StateId", I.StateId);
            Parameters.AddWithValue("@CityName", I.CityName);
            Parameters.AddWithValue("@StateName", I.StateName);

            Parameters.AddWithValue("@CountryId", I.CountryId);
            Parameters.AddWithValue("@RoleId", I.RoleId);
            Parameters.AddWithValue("@DepartmentId", I.DepartmentId);
            Parameters.AddWithValue("@TitleId", I.TitleId);
            Parameters.AddWithValue("@FirstName", I.FirstName);
            Parameters.AddWithValue("@MiddleName", I.MiddleName);
            Parameters.AddWithValue("@LastName", I.LastName);
            Parameters.AddWithValue("@UserName", I.UserName);
            Parameters.AddWithValue("@Address", I.Address);
            Parameters.AddWithValue("@Pincode", I.Pincode);
            Parameters.AddWithValue("@Phone", I.Phone);
            Parameters.AddWithValue("@MobileNo", I.MobileNo);
            Parameters.AddWithValue("@ImageUrl", I.ImageUrl);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@status", "0");

            Parameters["@status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("[MasterUserAddUpdate]", "@status");
        }

        public string UpdateRecord(IUsers I)
        {
            //Parameters.AddWithValue("@CityId", I.CityId);
            //Parameters.AddWithValue("@StateId", I.StateId);
            Parameters.AddWithValue("@CityName", I.CityName);
            Parameters.AddWithValue("@StateName", I.StateName);

            Parameters.AddWithValue("@CountryId", I.CountryId);
            Parameters.AddWithValue("@RoleId", I.RoleId);
            Parameters.AddWithValue("@DepartmentId", I.DepartmentId);
            Parameters.AddWithValue("@TitleId", I.TitleId);
            Parameters.AddWithValue("@FirstName", I.FirstName);
            Parameters.AddWithValue("@MiddleName", I.MiddleName);
            Parameters.AddWithValue("@LastName", I.LastName);
            Parameters.AddWithValue("@UserName", I.UserName);
            Parameters.AddWithValue("@Address", I.Address);
            Parameters.AddWithValue("@Email", I.Email);
            Parameters.AddWithValue("@Pincode", I.Pincode);
            Parameters.AddWithValue("@Phone", I.Phone);
            Parameters.AddWithValue("@MobileNo", I.MobileNo);
            Parameters.AddWithValue("@ImageUrl", I.ImageUrl);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@userkeyid", I.UserId);
            
            Parameters.AddWithValue("@Status", "0");

            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterUserAddUpdate", "@Status");
        }
       

        public string DeleteRecord(IUsers I)
        {
            Parameters.AddWithValue("@UserIds", I.UserIds);
            return getExcuteQuery("MasterUserDelete");
        }

        public string UnlockUsers(IUsers I)
        {
            Parameters.AddWithValue("@UserIds", I.UserIds);
            return getExcuteQuery("MasterUserUnlock");
        }

        public string ChangeIsActive(IUsers I)
        {
            Parameters.AddWithValue("@UserIds", I.UserIds);
            Parameters.AddWithValue("@isActive", I.isActive);
            return getExcuteQuery("MasterUsers_RegisterChangeIsActive");
        }


        public List<SelectControlFields> SelectData(IUsers I, int withOutSelect = 0)
        {
            if (withOutSelect == 0)
            {
                Parameters.AddWithValue("@DepartmentId", I.DepartmentId);
                Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
                SqlDataReader dr = getExecuteReader("MasterUserSelectWithDepartment");
                return SelectControlFields.SelectData(dr);
            }
            else
            {
                Parameters.AddWithValue("@DepartmentId", I.DepartmentId);
                Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
                SqlDataReader dr = getExecuteReader("MasterUserSelectWithDepartment");
                return SelectControlFields.SelectDataNoSelect(dr);
            }
        }


        public List<SelectControlFields>SelectSubordinateUsers(IUsers I, int withOutSelect = 0)
        {
            Parameters.AddWithValue("@DepartmentId", I.DepartmentId);
            Parameters.AddWithValue("@UsersId", I.UserId);
            if (withOutSelect == 0)
            {
                SqlDataReader dr = getExecuteReader("SubordinateUserSelect");
                return SelectControlFields.SelectData(dr);
            }
            else
            {
                SqlDataReader dr = getExecuteReader("SubordinateUserSelect");
                return SelectControlFields.SelectDataNoSelect(dr);
            }
        }


        public List<SelectControlFields> SelectAllUsers(IUsers I)
        {

            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
                SqlDataReader dr = getExecuteReader("SelectAllUsers");
                return SelectControlFields.SelectData(dr);
           
        }


        

        public List<Users> ReadData(IUsers I)
        {
            int i = 0;
            List<Users> myList = new List<Users>();
            Parameters.AddWithValue("@UsersId", I.UserId);
            Parameters.AddWithValue("@PageNo", I.PageNo);  
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@DepartmentId", I.DepartmentId);
            Parameters.AddWithValue("@RoleId", I.RoleId);
            SqlDataReader dr = getExecuteReader("MasterUserRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new Users());
                  //  myList[i].CityId = int.Parse(dr["CityId"].ToString());
                    myList[i].CityName = dr["CityName"].ToString();

                 //   myList[i].StateId = int.Parse(dr["StateId"].ToString());
                    myList[i].StateName = dr["StateName"].ToString();

                    myList[i].TitleId = int.Parse(dr["TitleId"].ToString());
                    myList[i].TitleName = dr["TitleName"].ToString();

                    myList[i].CountryId = int.Parse(dr["CountryId"].ToString());
                    myList[i].CountryName = dr["CountryName"].ToString();

                    myList[i].DepartmentId = int.Parse(dr["DepartmentId"].ToString());
                    myList[i].DepartmentName = dr["DepartmentName"].ToString();

                    myList[i].RoleId = int.Parse(dr["RoleId"].ToString());
                    myList[i].RoleName = dr["RoleName"].ToString();
                    myList[i].ImageUrl = dr["ImageUrl"].ToString();
                    myList[i].FirstName = dr["FirstName"].ToString();
                    myList[i].MiddleName = dr["MiddleName"].ToString();
                    myList[i].LastName = dr["LastName"].ToString();
                    myList[i].MobileNo = dr["MobileNo"].ToString();
                    myList[i].Phone = dr["Phone"].ToString();
                    myList[i].Address = dr["Address"].ToString();
                    myList[i].Email = dr["Email"].ToString();
                    myList[i].Pincode = dr["Pincode"].ToString();
                    myList[i].UserName = dr["UserName"].ToString();
                    myList[i].UserId= int.Parse(dr["UsersId"].ToString());
                    myList[i].IsUsed = dr["isUsed"].ToString();
                    myList[i].isActive = dr["isActive"].ToString();
                    myList[i].Description = dr["Description"].ToString();
                    myList[i].FullName = dr["FullName"].ToString();
                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return myList;
        }


        public void GetUsersDetails(IUsers I)
        {
            Parameters.AddWithValue("@UserName", I.UserName);
            SqlDataReader dr = getExecuteReader("MasterUserGetUsersId");
            try
            {
                while (dr.Read())
                {
                    I.UserId = int.Parse(dr["UserId"].ToString());
                    I.FullName = dr["FullName"].ToString();
                    I.RoleName = dr["RoleName"].ToString();
                    I.OutlookFileName = dr["OutlookFileName"].ToString();
                    I.IsOutlookSynchEnable = Convert.ToChar(dr["IsOutlookSynchEnable"]);
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.AccessIds = dr["AccessIds"].ToString();
                    }
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.DepartmentId= Convert.ToInt32(dr["DepartmentID"].ToString());
                    }
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.AccessMainSectionIds = dr["AccessMainSectionIds"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }


        }
        public string CheckUserIsActive(IUsers I)
        {
            Parameters.AddWithValue("@UserName", I.UserName);
            Parameters.AddWithValue("@Status", 0);

            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteScalar("MasterUserCheckISActive");
        }

        public string UserPasswordActivityLog(IUsers I)
        {
            Parameters.AddWithValue("@UserName", I.UserName);
            Parameters.AddWithValue("@EmailId", I.Email);
            Parameters.AddWithValue("@MailType", I.MailType);
            return getExcuteScalar("UserPasswordActivityLog");
        }

        public string UserPasswordActivityLog(IUsers I, int Update)
        {
            Parameters.AddWithValue("@UserName", I.UserName);
            Parameters.AddWithValue("@Update", Update);
            return getExcuteScalar("UserPasswordActivityLog");
        }

         public string UserGetIsLinkValid(IUsers I)
        {
            Parameters.AddWithValue("@UserName", I.UserName);
            return getExcuteScalar("UserGetIsLinkValid");
        }

        public string GetEmailId(IUsers I)
        {
            Parameters.AddWithValue("@UserName", I.UserName);
            Parameters.AddWithValue("@Email", I.Email);
            return getExcuteScalar("UsersGetUserInfoByMailId");
        }
        public int GetUserEmailExists(IUsers I)
        {
            Parameters.AddWithValue("@Email", I.Email);
            Parameters.AddWithValue("@Status", 0);

            Parameters["@Status"].Direction = ParameterDirection.Output;
            return int.Parse(getExcuteQuery("MasterUserEmailExists", "@Status"));
        }
        public string getTotalLoginfailed(IUsers I)
        {
            Parameters.AddWithValue("@UserName", I.UserName);
            return getExcuteScalar("MasterUserGetLoginFailedCount");
        }
       
        public string GetUserFirstName(IUsers I)
        {
            Parameters.AddWithValue("@UserName", I.UserName);
            return getExcuteScalar("MasterUserGetUserFirstName");
        }
        public string GetUserFullName(IUsers I)
        {
            Parameters.AddWithValue("@UserName", I.UserName);
            return getExcuteScalar("MasterGetUserFullName");
        }
        public string UserPasswordChanged(IUsers I)
        {
            Parameters.AddWithValue("@UserName", I.UserName);
            return getExcuteQuery("UsersUserPasswordChanged");
        }
        public string GetUserNameDB(IUsers I)
        {
            Parameters.AddWithValue("@UserId", I.UserIds);
            return getExcuteQuery("GetUserName");
        }
        public string LockUser(IUsers I)
        {
            Parameters.AddWithValue("@Flag", I.Flag);
            Parameters.AddWithValue("@UsersId", I.UserIds);
            Parameters.AddWithValue("@Status", 0);

            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteScalar("LockUser");
        }

        public void PasswordChangedSuccessfully(IUsers I)
        {
            Parameters.AddWithValue("@UserName", I.UserName);
            getExcuteScalar("UserPasswordChanged");
        }
        public void UpdateUserPasswordExpireStatusConfirm(IUsers I)
        {
            Parameters.AddWithValue("@UserName", I.UserName);
            getExcuteQuery("UserPasswordChangedConfirm");

        }
        //For Profile image
        public string ProfileImgUrl(IUsers I)
        {
            Parameters.AddWithValue("@UsersId", I.UserId);
            return getExcuteScalar("MasterUserProfile");
        }

        public string RemainingCount(IUsers I)
        {
           return getExcuteScalar("MasterUserStillAdd"); 
        }

        public void GetUsersDetailsByEmailId(IUsers I)
        {
            Parameters.AddWithValue("@EmailId", I.Email);
            SqlDataReader dr = getExecuteReader("MasterUserGetUsersIdByEmailId");
            try
            {
                while (dr.Read())
                {
                    I.UserId = int.Parse(dr["UserId"].ToString());
                    I.FullName = dr["FullName"].ToString();
                    I.RoleName = dr["RoleName"].ToString();
                    I.OutlookFileName = dr["OutlookFileName"].ToString();
                    I.IsOutlookSynchEnable = Convert.ToChar(dr["IsOutlookSynchEnable"]);
                    I.UserName = dr["UserName"].ToString();
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.AccessIds = dr["AccessIds"].ToString();
                    }
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.DepartmentId = Convert.ToInt32(dr["DepartmentID"].ToString());
                    }
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.AccessMainSectionIds = dr["AccessMainSectionIds"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }


        }
    }
}
