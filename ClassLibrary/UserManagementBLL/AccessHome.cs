﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UserManagementBLL
{
   public  class AccessHome : IAccessHome
   {
       AccessHomeDAL obj;
       public int UserId { get; set; }
       public string MyDashBoard { get; set; }
       public string MyContracts { get; set; }
       public string MyReports { get; set; }
       public string CreateNewContract { get; set; }
       public string NewReviewRequest { get; set; }
       public string MyClients { get; set; }
       public string RenewContract { get; set; }
       public string ReviseContract { get; set; }
       public string MyVault { get; set; }
       public string TerminateContract { get; set; }
       public string AutoRenewal { get; set; }
       public string Calender { get; set; }
       public string PageLink { get; set; }
       public int ParentId { get; set; }
       public string CustomReport { get; set; }
       public string AdvanceSearch { get; set; }
       public string AuditTrail { get; set; }

       public List<AccessHome> ReadData()
       {
           try
           {
               obj = new AccessHomeDAL();
               return obj.ReadData(this);
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }


   }
}
