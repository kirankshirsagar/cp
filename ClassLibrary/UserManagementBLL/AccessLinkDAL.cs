﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;


namespace UserManagementBLL
{
    public class AccessLinkDAL : DAL
    {

        public List<AccessLink>ReadData(IAccessLink I)
        {
            int i = 0;
            List<AccessLink> myList = new List<AccessLink>();
            Parameters.AddWithValue("@UserId", I.UserId);
            Parameters.AddWithValue("@Flag", I.Flag);
            SqlDataReader dr = getExecuteReader("AccessGetLinks");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new AccessLink());
                    myList[i].PageLink = dr["PageLink"].ToString();
                    myList[i].PageName = dr["PageName"].ToString();
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally{
                if (dr.IsClosed != true && dr != null)
                dr.Close();
            }
            return myList;
        }

        public List<SelectControlFields> SelectData(IAccessLink I)
        {
            Parameters.AddWithValue("@UserId", I.UserId);
            Parameters.AddWithValue("@Flag", I.Flag);
            SqlDataReader dr = getExecuteReader("AccessGetLinks");
            return SelectControlFields.SelectData(dr,1);
        }

        //Added By Prashant for fetching Parent access links
        public List<AccessLink> ReadParentData(IAccessLink I)
        {
            int i = 0;
            List<AccessLink> myList = new List<AccessLink>();
            Parameters.AddWithValue("@UserId", I.UserId);
            SqlDataReader dr = getExecuteReader("AccessGetParentLinks");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new AccessLink());
                    myList[i].PageLink = dr["value"].ToString();
                    myList[i].PageName = dr["text"].ToString();
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
        }
    }
}
