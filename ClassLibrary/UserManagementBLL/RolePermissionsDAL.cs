﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;


namespace UserManagementBLL
{
    public  class RolePermissionsDAL : DAL
    {
        public string InsertRecord(IRolePermissions I)
        {
            Parameters.AddWithValue("@RoleId", I.RoleId);
            Parameters.AddWithValue("@RoleAccess", I.RoleAccess);
            Parameters.AddWithValue("@RoleAccessDynamic", I.RoleAccessDynamic);
            Parameters.AddWithValue("@Status",0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("AccessAdd", "@Status");
        }  

        public void ReadData(IRolePermissions I)
        {
            int i = 0;
            List<RolePermissions> myList1 = new List<RolePermissions>();
            List<RolePermissions> myList2 = new List<RolePermissions>();
            List<RolePermissions> myList3 = new List<RolePermissions>();

            Parameters.AddWithValue("@RoleId", I.RoleId);
            Parameters.AddWithValue("@UsersId", I.UsersId);
            SqlDataReader dr = getExecuteReader("AccessRead");
            try
            {
                while (dr.Read())
                {
                    myList1.Add(new RolePermissions());
                    myList1[i].ParentId = int.Parse(dr["ParentId"].ToString());
                    myList1[i].ParentName = dr["ParentName"].ToString();
                    i = i + 1;
                }
                I.ReadParent = myList1;
               
                i = 0;

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        myList2.Add(new RolePermissions());
                        myList2[i].ParentId = int.Parse(dr["ParentId"].ToString());
                        myList2[i].ChildId = int.Parse(dr["ChildId"].ToString());
                        myList2[i].ChildName = dr["ChildName"].ToString();
                        myList2[i].Add = dr["Add"].ToString();
                        myList2[i].Update = dr["Update"].ToString();
                        myList2[i].Delete = dr["Delete"].ToString();
                        myList2[i].View = dr["View"].ToString();
                        myList2[i].AddReadOnly = dr["AddReadOnly"].ToString();
                        myList2[i].UpdateReadOnly = dr["UpdateReadOnly"].ToString();
                        myList2[i].DeleteReadOnly = dr["DeleteReadOnly"].ToString();
                        myList2[i].ViewReadOnly = dr["ViewReadOnly"].ToString();
                        myList2[i].AllReadOnly = dr["AllReadOnly"].ToString();

                        i = i + 1;
                    }
                }
                I.ReadChild = myList2;

                i = 0;

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        myList3.Add(new RolePermissions());
                        myList3[i].AccessId = int.Parse(dr["AccessId"].ToString());
                        myList3[i].ChildId = int.Parse(dr["ChildId"].ToString());
                        myList3[i].ControlName = dr["ControlName"].ToString();
                        myList3[i].isApplicable = dr["isApplicable"].ToString();
                        i = i + 1;
                    }
                }
                I.ReadGrandChild = myList3;               
            }
           
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }            
        }
    }
}
