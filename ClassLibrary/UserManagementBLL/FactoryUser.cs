﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UserManagementBLL
{
    public class FactoryUser
    {
        public static IRolePermissions GetRolePermissionDetail()
        {
            return new RolePermissions();
        }

        public static IRole GetRoleDetail()
        {
            return new Role();
        }

        public static IUsers GetUsersDetail()
        {
            return new Users();
        }

        public static IDepartment GetDepartmentDetail()
        {
            return new Department();
        }

        public static IAccessLink GetAccessDetail()
        {
            return new AccessLink();
        }

        public static IAccessHome GetAccessHomeDetail()
        {
            return new AccessHome();
        }


    }
}
