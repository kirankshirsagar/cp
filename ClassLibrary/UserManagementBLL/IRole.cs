﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;

namespace UserManagementBLL
{
    public interface IRole : ICrud
    {
        int RoleId { get; set; }
        string RoleIds { get; set; }
        string RoleName { get; set; }
        int ReportingId { get; set; }
        string ReportingName { get; set; }
        string IsUsed { get; set; }
        string Search{ get; set; }
        string isActive { get; set; }

        string IsRoleUsed();
        string ChangeIsActive();
        List<SelectControlFields> SelectData(int withOutSelect=0);
        List<SelectControlFields> SelectReportingTo();
        List<Role> ReadData();

    }

}
