﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CrudBLL;
using CommonBLL;

namespace UserManagementBLL
{
    public interface IRolePermissions 
    {
        int RoleId { get; set; }
        int UsersId { get; set; }
        int ParentId { get; set; }
        int ChildId { get; set; }
        int AccessId { get; set; }

        string ParentName { get; set; }
        string ChildName { get; set; }
        string Add { get; set; }
        string Update { get; set; }
        string Delete { get; set; }
        string View { get; set; }

        string AddReadOnly { get; set; }
        string UpdateReadOnly { get; set; }
        string DeleteReadOnly { get; set; }
        string ViewReadOnly { get; set; }
        string AllReadOnly { get; set; }

        string ControlName { get; set; }
        string isApplicable { get; set; }

        void ReadData();
        List<RolePermissions> ReadParent { get;  set; }
        List<RolePermissions> ReadChild { get;  set; }
        List<RolePermissions> ReadGrandChild { get; set; }

        DataTable RoleAccess { get; set; }
        DataTable RoleAccessDynamic { get; set; }

        DataTable GetAccessTable(Repeater rpt);
        DataTable GetAccessTableFeature(Repeater rpt);

        

        string InsertRecord();
        

    }

}
