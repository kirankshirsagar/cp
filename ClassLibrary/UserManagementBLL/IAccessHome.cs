﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UserManagementBLL
{
    public interface IAccessHome
    {
        int UserId { get; set; }
        string MyDashBoard   { get; set; }
		string MyContracts { get; set; } 
		string MyReports { get; set; }
		string CreateNewContract { get; set; }  
		string NewReviewRequest { get; set; } 
		string MyClients { get; set; } 
		string RenewContract { get; set; } 
		string ReviseContract { get; set; } 
		string MyVault { get; set; } 
		string TerminateContract { get; set; } 
		string AutoRenewal { get; set; }
        string Calender { get; set; }
        string PageLink { get; set; }
        int ParentId { get; set; }
        List<AccessHome> ReadData();
 


    }
}
