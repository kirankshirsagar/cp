﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;

namespace UserManagementBLL
{
    public interface IDepartment : ICrud, INavigation
    {
        int DepartmentId { get; set; }
        string DepartmentIds { get; set; }
        string DepartmentName { get; set; }
        string IsUsed { get; set; }
        string Search{ get; set; }
        string isActive { get; set; }

        string ChangeIsActive();
        List<SelectControlFields> SelectData(); 
        List<Department> ReadData();
    }

}
