﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using CommonBLL;

namespace UserManagementBLL
{
    public class AccessLink : IAccessLink
    {

        AccessLinkDAL obj;
        public int UserId { get; set; }
        public string Flag { get; set; }
        public string PageLink { get; set; }
        public string PageName { get; set; }

        public List<AccessLink> ReadData()
        {
            try
            {
                obj = new AccessLinkDAL();
                return obj.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields> SelectData()
        {
            try
            {
                obj = new AccessLinkDAL();
                return obj.SelectData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Added By Prashant for fetching Parent access links
        public List<AccessLink> ReadParentData()
        {
            try
            {
                obj = new AccessLinkDAL();
                return obj.ReadParentData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

