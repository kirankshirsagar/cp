﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;

namespace UserManagementBLL
{
    public interface IAccessLink
    {
        int UserId { get; set;}
        string Flag { get; set; }
        string PageLink { get; set; }
        string PageName { get; set; }

        List<AccessLink> ReadData();
        List<SelectControlFields> SelectData();
        List<AccessLink> ReadParentData();
    }

}
