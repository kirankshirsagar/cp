﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using CommonBLL;

namespace UserManagementBLL
{
   public class Role : IRole
    {

        RoleDAL obj;
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleIds { get; set; }
        public int ReportingId { get; set; }
        public string ReportingName { get; set; }
        public string Search { get; set; }
        public string IsUsed { get; set; }
       
      
        
        public string isActive { get; set; }

        private int _AddedBy;

        public int AddedBy
        {
            get { return _AddedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _AddedBy = value;
            }
        }

        private int _ModifiedBy;

        public int ModifiedBy
        {
            get { return _ModifiedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _ModifiedBy = value;
            }
        }


        public string IpAddress { get; set; }
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Description { get; set; }




        public List<SelectControlFields> SelectReportingTo()
        {
            try
            {
                obj = new RoleDAL();
                return obj.SelectReportingTo(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DeleteRecord()
        {
            try
            {
                obj = new RoleDAL();
                return obj.DeleteRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string InsertRecord()
        {
            try
            {
                obj = new RoleDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateRecord()
        {
            try
            {
                obj = new RoleDAL();
                return obj.UpdateRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ChangeIsActive()
        {
            try
            {
                obj = new RoleDAL();
                return obj.ChangeIsActive(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<Role>ReadData()
        {
            try
            {
                obj = new RoleDAL();
                return obj.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string IsRoleUsed()
        {
            try
            {
                obj = new RoleDAL();
                return obj.IsRoleUsed(this);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        

        public List<SelectControlFields> SelectData(int withOutSelect = 0)
        {
            try
            {
                obj = new RoleDAL();
                return obj.SelectData(this,withOutSelect);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
     
    }
}

