﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;
using System.ComponentModel;

namespace UserManagementBLL
{
    public  class RoleDAL : DAL
    {
        public string InsertRecord(IRole I)
        {

            Parameters.AddWithValue("@RoleId",I.RoleId);
            Parameters.AddWithValue("@RoleName",I.RoleName);
            Parameters.AddWithValue("@ReportingId", I.ReportingId);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@Status",0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterRoleAddUpdate", "@Status");
        }

        public string UpdateRecord(IRole I)
        {
            Parameters.AddWithValue("@RoleId", I.RoleId);
            Parameters.AddWithValue("@RoleName", I.RoleName);
            Parameters.AddWithValue("@ReportingId", I.ReportingId);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterRoleAddUpdate", "@Status");
        }

        public string DeleteRecord(IRole I)
        {
            Parameters.AddWithValue("@RoleIds", I.RoleIds);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterRoleDelete");
        }

        public string ChangeIsActive(IRole I)
        {
            Parameters.AddWithValue("@RoleIds", I.RoleIds);
            Parameters.AddWithValue("@isActive", I.isActive);
            return getExcuteQuery("MasterRoleChangeIsActive");
        }

        public List<SelectControlFields> SelectData(IRole I, int withOutSelect = 0)
        {
            if (withOutSelect == 0)
            {
                //SqlDataReader dr = getExecuteReader("MasterRoleSelect");
                SqlDataReader dr = getExecuteReader("MasterRoleCreateUserSelect");
                return SelectControlFields.SelectData(dr);
            }
            else
            {
                SqlDataReader dr = getExecuteReader("MasterRoleSelect");
                return SelectControlFields.SelectDataNoSelect(dr);
            }

        }


        public List<SelectControlFields> SelectReportingTo(IRole I)
        {
            Parameters.AddWithValue("@RoleId", I.RoleId);
            SqlDataReader dr = getExecuteReader("MasterRoleGetReporting");
            return SelectControlFields.SelectDataNoSelect(dr);
        }


        public List<Role> ReadData(IRole I)
        {
            int i = 0;
            List<Role> myList = new List<Role>();
            Parameters.AddWithValue("@RoleId", I.RoleId);
            SqlDataReader dr = getExecuteReader("MasterRoleRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new Role());
                    myList[i].RoleId = int.Parse(dr["RoleId"].ToString());
                    myList[i].RoleName = dr["RoleName"].ToString();
                    myList[i].IsUsed = dr["isUsed"].ToString();
                    myList[i].isActive = dr["isActive"].ToString();
                    myList[i].Description = dr["Description"].ToString();
                    myList[i].ReportingId = int.Parse(dr["ReportingId"].ToString());
                    myList[i].ReportingName = dr["ReportingName"].ToString();
                    i = i + 1;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally{
                if (dr.IsClosed != true && dr != null)
                dr.Close();
            }
            return myList;
        }


        public string IsRoleUsed(IRole I)
        {
            Parameters.AddWithValue("@RoleId", I.RoleId);
            return getExcuteScalar("MasterisRoleUsed");
        }



    }
}
