﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using CommonBLL;

namespace UserManagementBLL
{
    public class Users : IUsers
    {

        UsersDAL obj;
        public string Flag { get; set; }
        public int UserId { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public string ImageUrl { get; set; }
        public string UserIds { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string MobileNo { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Pincode { get; set; }
        public string MailTo { get; set; }
        public string MailFrom { get; set; }
        public string Search { get; set; }
        public string IsUsed { get; set; }
        public int Direction { get; set; }
        public string SortColumn { get; set; }
        public string AccessMainSectionIds { get; set; }
        public int ContractTypeId { get; set; }
       
       
        public string MailType { get; set; }

        private int _AddedBy;

        public int AddedBy
        {
            get { return _AddedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _AddedBy = value;
            }
        }

        private int _ModifiedBy;

        public int ModifiedBy
        {
            get { return _ModifiedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _ModifiedBy = value;
            }
        }


        public string IpAddress { get; set; }
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Description { get; set; }
        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public string isActive { get; set; }
        public string FullName { get; set; }
        public string AccessIds { get; set; }

        public char IsOutlookSynchEnable { get; set; }
        public string OutlookFileName { get; set; }

        public string RemainingCount()
        {
            try
            {
                obj = new UsersDAL();
                return obj.RemainingCount(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GetUsersDetails()
        {
            try
            {
                obj = new UsersDAL();
                obj.GetUsersDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void GetUsersDetailsByEmailId()
        {
            try
            {
                obj = new UsersDAL();
                obj.GetUsersDetailsByEmailId(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UserPasswordActivityLog()
        {
            try
            {
                obj = new UsersDAL();
                return obj.UserPasswordActivityLog(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        }

        public string UserPasswordActivityLog(int update)
        {
            try
            {
                obj = new UsersDAL();
                return obj.UserPasswordActivityLog(this,update);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string IsValidLink()
        {
            try
            {
                obj = new UsersDAL();
                return obj.UserGetIsLinkValid(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string GetUserFullName()
        {
            try
            {
                obj = new UsersDAL();
                return obj.GetUserFullName(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetUserFirstName()
        {
            try
            {
                obj = new UsersDAL();
                return obj.GetUserFirstName(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string getTotalLoginfailed()
        {
            try
            {
                obj = new UsersDAL();
                return obj.getTotalLoginfailed(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public string GetEmailId()
        {
            try
            {
                obj = new UsersDAL();
                return obj.GetEmailId(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetUserNameDB()
        {
            try
            {
                obj = new UsersDAL();
                return obj.GetUserNameDB(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string LockUser()
        {
            try
            {
                obj = new UsersDAL();
                return obj.LockUser(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public int GetUserEmailExists()
        {
            try
            {
                obj = new UsersDAL();
                return obj.GetUserEmailExists(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string CheckUserIsActive()
        {
            try
            {
                obj = new UsersDAL();
                return obj.CheckUserIsActive(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DeleteRecord()
        {
            try
            {
                obj = new UsersDAL();
                return obj.DeleteRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UnlockUsers()
        {
            try
            {
                obj = new UsersDAL();
                return obj.UnlockUsers(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string InsertRecord()
        {
            try
            {
                obj = new UsersDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateRecord()
        {
            try
            {
                obj = new UsersDAL();
                return obj.UpdateRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ChangeIsActive()
        {
            try
            {
                obj = new UsersDAL();
                return obj.ChangeIsActive(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Users> ReadData()
        {
            try
            {
                obj = new UsersDAL();
                return obj.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields> SelectData(int withOutSelect = 0)
        {
            try
            {
                obj = new UsersDAL();
                return obj.SelectData(this,withOutSelect);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string UpdateUserPasswordExpireStatus()
        {
            try
            {
                obj = new UsersDAL();
                return obj.UserPasswordChanged(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        public void PasswordChangedSuccessfully()
        {
            try
            {
                obj = new UsersDAL();
                obj.PasswordChangedSuccessfully(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void UpdateUserPasswordExpireStatusConfirm()
        {
            try
            {
                obj = new UsersDAL();
                obj.UpdateUserPasswordExpireStatusConfirm(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields> SelectSubordinateUsers(int withOutSelect = 0)
        {
           try
            {
                obj = new UsersDAL();
                return obj.SelectSubordinateUsers(this, withOutSelect);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<SelectControlFields> SelectAllUsers()
        {
           try
            {
                obj = new UsersDAL();
                return obj.SelectAllUsers(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }    

        public void CreateDALObjForMembershipWork()
        {
            try
            {
                obj = new UsersDAL();
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ProfileImgUrl()
        {
            try
            {
                obj = new UsersDAL();
                return obj.ProfileImgUrl(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

