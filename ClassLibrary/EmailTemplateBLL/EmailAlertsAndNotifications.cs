﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using CommonBLL;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;



namespace EmailTemplateBLL

{


   public class EmailAlertsAndNotifications : IEmailAlertsAndNotifications
    {

        EmailAlertsAndNotificationsDAL obj;
        public int EmailTriggerId { get; set; }
        public string EmailTemplateIds { get; set; }
        public string EmailTriggerName { get; set; }
        public DataTable EmailAlertsAndNotification { get; set; }
        private int _ModifiedBy;
        public int ModifiedBy
        {
            get { return _ModifiedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _ModifiedBy = value;
            }
        }

        private string _IpAddress;
        public string IpAddress
        {
            get { return _IpAddress; }
            set
            {
                if (value.ToString() == "")
                {
                    throw new CustomException("IP not captured");
                }
                _IpAddress = value;
            }
        }
        public DateTime ModifiedOn { get; set; }
      

        public string UpdateRecord()
        {
            try
            {
                obj = new EmailAlertsAndNotificationsDAL();
                return obj.UpdateRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
        public List<EmailAlertsAndNotifications>ReadData()
        {
            try
            {
                obj = new EmailAlertsAndNotificationsDAL();
                return obj.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataTable getEmailAlertsAndNotification(Repeater rpt)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("EmailTriggerId");
            dt.Columns.Add("EmailTemplateIds");
           
            DataRow row;

            foreach (RepeaterItem item in rpt.Items)
            {
                Label lblEmailTriggerId = (Label)item.FindControl("lblEmailTriggerId");
                HtmlSelect ddl = (HtmlSelect)item.FindControl("ddlEmailTemplate");
                ddl.Multiple = true;
                row = dt.NewRow();
                row["EmailTriggerId"] = int.Parse(lblEmailTriggerId.Text);
                row["EmailTemplateIds"] = ddl.extGetSelectedValues();
                dt.Rows.Add(row);
            }

            return dt;
        }


        public string InsertRecord()
        {
            throw new NotImplementedException();
        }

        public string DeleteRecord()
        {
            throw new NotImplementedException();
        }

        public int AddedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public DateTime AddedOn
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string Description
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


      
    }
}

