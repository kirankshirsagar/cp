﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;


namespace EmailTemplateBLL
{
    public  class EmailTemplateDAL : DAL
    {
        public string InsertRecord(IEmailTemplate I)
        {
            Parameters.AddWithValue("@EmailTemplateId",I.EmailTemplateId);
            Parameters.AddWithValue("@EmailTemplateName",I.EmailTemplateName);
            Parameters.AddWithValue("@Subject", I.Subject);
            Parameters.AddWithValue("@RoleIds", I.RoleIds);
            Parameters.AddWithValue("@UsersIds", I.UsersIds);
            Parameters.AddWithValue("@ReplyTo", I.ReplyTo);
            Parameters.AddWithValue("@TemplateBody", I.TemplateBody);
            Parameters.AddWithValue("@EmailTemplateFilesType", I.Attachments);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IP", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@EventExecutionType", I.EventExecutionType);
            Parameters.AddWithValue("@EventExecutionValue", I.EventExecutionValue);
            Parameters.AddWithValue("@Status",0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("EmailTemplateAddUpdate", "@Status");
        }

        public string UpdateRecord(IEmailTemplate I)
        {
            Parameters.AddWithValue("@EmailTemplateId", I.EmailTemplateId);
            Parameters.AddWithValue("@EmailTemplateName", I.EmailTemplateName);
            Parameters.AddWithValue("@Subject", I.Subject);
            Parameters.AddWithValue("@RoleIds", I.RoleIds);
            Parameters.AddWithValue("@UsersIds", I.UsersIds);
            Parameters.AddWithValue("@ReplyTo", I.ReplyTo);
            Parameters.AddWithValue("@TemplateBody", I.TemplateBody);
            Parameters.AddWithValue("@EmailTemplateFilesType", I.Attachments);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IP", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@EventExecutionType", I.EventExecutionType);
            Parameters.AddWithValue("@EventExecutionValue", I.EventExecutionValue);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("EmailTemplateAddUpdate", "@Status");
        }

        public string DeleteRecord(IEmailTemplate I)
        {
            Parameters.AddWithValue("@EmailTemplateIds", I.EmailTemplateIds);
            return getExcuteQuery("EmailTemplateDelete");
        }

        public string ChangeIsActive(IEmailTemplate I)
        {
            Parameters.AddWithValue("@EmailTemplateIds", I.EmailTemplateIds);
            Parameters.AddWithValue("@isActive", I.isActive);
            return getExcuteQuery("MasterEmailTemplateChangeIsActive");
        }

        public List<SelectControlFields> SelectData(IEmailTemplate I, int withOutSelect = 0)
        {
            if (withOutSelect == 0)
            {
                SqlDataReader dr = getExecuteReader("EmailTemplateSelect");
                return SelectControlFields.SelectData(dr);
            }
            else
            {
                SqlDataReader dr = getExecuteReader("EmailTemplateSelect");
                return SelectControlFields.SelectDataNoSelect(dr);
            }
        }


        public List<SelectControlFields>SelectDataWithOutTemplate(IEmailTemplate I, int withOutSelect = 0)
        {
            if (withOutSelect == 0)
            {
                Parameters.AddWithValue("@TemplateForRequest", 1);
                SqlDataReader dr = getExecuteReader("EmailTemplateSelect");
                return SelectControlFields.SelectData(dr);
            }
            else
            {
                Parameters.AddWithValue("@TemplateForRequest", 1);
                SqlDataReader dr = getExecuteReader("EmailTemplateSelect");
                return SelectControlFields.SelectDataNoSelect(dr);
            }
        }

        public List<EmailTemplate> ReadData(IEmailTemplate I)
        {
            int i = 0;
            List<EmailTemplate> myList = new List<EmailTemplate>();
            List<EmailTemplate> myAttachments = new List<EmailTemplate>();

            Parameters.AddWithValue("@EmailTemplateId", I.EmailTemplateId);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            SqlDataReader dr = getExecuteReader("EmailTemplateRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new EmailTemplate());
                    myList[i].EmailTemplateId = int.Parse(dr["EmailTemplateId"].ToString());
                    myList[i].EmailTemplateName = dr["EmailTemplateName"].ToString();
                    myList[i].RoleIds = dr["RoleIds"].ToString();
                    myList[i].UsersIds = dr["UsersIds"].ToString();
                    myList[i].ReplyTo = dr["ReplyTo"].ToString();
                    myList[i].TemplateBody = dr["TemplateBody"].ToString();
                    myList[i].IsUsed = dr["isUsed"].ToString();
                    myList[i].isActive = dr["isActive"].ToString();
                    myList[i].Subject = dr["Subject"].ToString();
                    myList[i].Description = dr["Description"].ToString();
                    myList[i].EventExecutionValue = int.Parse(dr["EventExecutionValue"].ToString());
                    myList[i].EventExecutionType = dr["EventExecutionType"].ToString();
                    i = i + 1;
                }

                if (I.EmailTemplateId > 0)
                {
                    i = 0;
                    if (dr.NextResult())
                    {
                        while (dr.Read())
                        {
                            myAttachments.Add(new EmailTemplate());
                            myAttachments[i].FileName = dr["FileName"].ToString();
                            myAttachments[i].FileNameOriginal = dr["FileNameOriginal"].ToString();
                            myAttachments[i].AddedByName = dr["AddedByName"].ToString();
                            myAttachments[i].SizeBytes = Convert.ToDecimal(dr["SizeBytes"].ToString());
                            myAttachments[i].SizeModified = dr["SizeModified"].ToString();
                            i = i + 1;
                        }
                        I.AttachmentLists = myAttachments;

                    }
                }
                else
                {
                    if (dr.NextResult())
                    {
                        while (dr.Read())
                        {
                            I.TotalRecords = int.Parse(dr[0].ToString());
                          
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally{
                if (dr.IsClosed != true && dr != null)
                dr.Close();
            }
            return myList;
        }




    }
}
