﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmailTemplateBLL
{
    public interface IEmailTrigger
    {
         int EmailTriggerId { get; set; } 
         int EmailTemplateId { get; set; }
         int ContractTypeId { get; set; }
         int ContractTemplateId { get; set; }
         int RequestId { get; set; }
         string Subject { get; set; }
         string RecipientEmailIds { get; set; }
         string ReplyTo { get; set; }
         string Description { get; set; }
         string Attachments { get; set; }
         string EmailBody { get; set; }
         void BulkEmail();
         void BulkEmailScheduler();
         int userId { get; set; }
         int ActivityId { get; set; }
         int AssignedToUserId { get; set; }
         int StageId { get; set; }
         int ContractId { get; set; }
         string URL { get; set; }
         Nullable<DateTime> AlertDate { get; set; }
    }
}
