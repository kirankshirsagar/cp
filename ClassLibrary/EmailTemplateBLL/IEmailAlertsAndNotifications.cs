﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;
using System.Web.UI.WebControls;

namespace EmailTemplateBLL
{
    public interface IEmailAlertsAndNotifications : ICrud
    {
        int EmailTriggerId { get; set; }
        string EmailTemplateIds { get; set; }
        string EmailTriggerName { get; set; }
        DataTable EmailAlertsAndNotification { get; set; }
        DataTable getEmailAlertsAndNotification(Repeater rpt);
        List<EmailAlertsAndNotifications> ReadData();


    }

}
