﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;

namespace EmailTemplateBLL
{
    public interface IEmailTemplate : ICrud, INavigation
    {
        int EmailTemplateId { get; set; }
        string EmailTemplateIds { get; set; }
        string EmailTemplateName { get; set; }
        string IsUsed { get; set; }
        string Search{ get; set; }
        string isActive { get; set; }
        int ContractTemplateId { get; set; }
        int ContractTypeId { get; set; }
        string Subject { get; set; }
        string RoleIds { get; set; }
        string UsersIds { get; set; }
        string ReplyTo { get; set; }
        string FileName { get; set; }
        string FileNameOriginal { get; set; }
        string SizeModified { get; set; }
        decimal SizeBytes { get; set; }
        string AddedByName { get; set; }

        string EventExecutionType { get; set; }
        int EventExecutionValue { get; set; }

        string ContractTypeName { get; set; }
        string ContractTemplateName { get; set; }

        DataTable Attachments { get; set; }
        DataTable getAttachments();
        string TemplateBody { get; set; }

        string ChangeIsActive();
        List<SelectControlFields> SelectData(int withOutSelect = 0);
        List<SelectControlFields> SelectDataWithOutTemplate(int withOutSelect = 0); 
        List<EmailTemplate> ReadData();
        List<EmailTemplate> AttachmentLists { get; set; }

    }

}
