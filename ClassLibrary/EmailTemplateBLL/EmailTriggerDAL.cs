﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;
using EmailBLL;
using System.Configuration;
using System.Web;


namespace EmailTemplateBLL
{
    public class EmailTriggerDAL : DAL
    {
        public string EmailActivityFlagUpdate(string ActivityIds)
        {
            Parameters.AddWithValue("@ActivityIds",ActivityIds);
            return getExcuteScalar("EmailActivityFlagUpdate");
        }
            
        public bool SendEmail(IEmailTrigger I)
        {
            bool rval = false;
            SendMail sm = new SendMail();
            try
            {
                sm.MailFrom = ConfigurationManager.AppSettings["MailFrom"];
                sm.Subject = I.Subject;
                sm.ReplyTo = I.ReplyTo;
                sm.Body = I.EmailBody;

                sm.RequestId = I.RequestId;
                sm.Mode = "Communication";
                sm.EmailTriggerId = I.EmailTriggerId;
                sm.EmailTemplateId = I.EmailTemplateId;
                sm.UserId = I.userId.ToString();
                sm.ActivityId = I.ActivityId;
                I.Attachments = I.Attachments.Trim(',');

                if (I.Attachments.Trim() != "")
                {
                    string[] att = I.Attachments.Split(',');
                    foreach (var item in att)
                    {
                        try
                        {
                            sm.Attachment.Add(HttpContext.Current.Server.MapPath(item));
                        }
                        catch (Exception ex)
                        {                            
                        }
                    }
                }
                sm.MailTo = I.RecipientEmailIds;
                rval = sm.SendSimpleMail();
            }
            catch (Exception)
            {
                rval = false;
                throw;
            }
            return rval;
        }

        
        public List<EmailTrigger> ReadDataScheduler(IEmailTrigger I)
        {
            int i = 0;
            I.URL = HttpContext.Current.Session[Declarations.URL].ToString();
            List<EmailTrigger> myList = new List<EmailTrigger>();
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@ActivityId", I.ActivityId);
            Parameters.AddWithValue("@URL", I.URL);
            SqlDataReader dr = getExecuteReader("EmailSendDataScheduler");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new EmailTrigger());
                    myList[i].EmailTemplateId = int.Parse(dr["EmailTemplateId"].ToString());
                    myList[i].Subject = dr["Subject"].ToString();
                    myList[i].ReplyTo = dr["ReplyTo"].ToString();
                    myList[i].EmailBody = dr["EmailBody"].ToString();
                    myList[i].RecipientEmailIds = dr["RecipientEmailIds"].ToString();
                    myList[i].Attachments = dr["Attachments"].ToString();
                    myList[i].EmailTriggerId = int.Parse(dr["EmailTriggerId"].ToString());
                    myList[i].RequestId = int.Parse(dr["RequestId"].ToString());
                    myList[i].ActivityId = int.Parse(dr["ActivityId"].ToString());

                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
        }

        public List<EmailTrigger> ReadData(IEmailTrigger I)
        {
            int i = 0;
            I.URL = HttpContext.Current.Session[Declarations.URL].ToString();
            List<EmailTrigger> myList = new List<EmailTrigger>();
            Parameters.AddWithValue("@EmailTriggerId", I.EmailTriggerId);
            Parameters.AddWithValue("@RequestId", I.RequestId);
            //Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
            //Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
            Parameters.AddWithValue("@AssignedToUserId", I.AssignedToUserId);
            Parameters.AddWithValue("@StageId", I.StageId);
            Parameters.AddWithValue("@ActivityId", I.ActivityId);
            Parameters.AddWithValue("@URL", I.URL);
            SqlDataReader dr = getExecuteReader("EmailSendData");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new EmailTrigger());
                    myList[i].EmailTemplateId = int.Parse(dr["EmailTemplateId"].ToString());
                    myList[i].Subject = dr["Subject"].ToString();
                    myList[i].ReplyTo = dr["ReplyTo"].ToString();
                    myList[i].EmailBody = dr["EmailBody"].ToString();
                    myList[i].RecipientEmailIds = dr["RecipientEmailIds"].ToString();
                    myList[i].Attachments = dr["Attachments"].ToString();
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
        }






    }
}
