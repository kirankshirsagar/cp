﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmailTemplateBLL
{
    public class FactoryEmailTemplate
    {
         public static IEmailTemplate GetEmailTemplateDetail()
         {
             return new EmailTemplate();
         }

         public static IEmailAlertsAndNotifications GetEmailAlertsAndNotificationDetail()
         {
             return new EmailAlertsAndNotifications();
         }

         public static IEmailTrigger GetEmailTriggerDetail()
         {
             return new EmailTrigger();
         }

    }
}
