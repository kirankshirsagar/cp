﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;


namespace EmailTemplateBLL
{
    public  class EmailAlertsAndNotificationsDAL : DAL
    {
        

        public string UpdateRecord(IEmailAlertsAndNotifications I)
        {
            Parameters.AddWithValue("@EmailAlertsAndNotifications", I.EmailAlertsAndNotification);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IP", I.IpAddress);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("EmailAlertsAndNotificationsUpdate", "@Status");
        }

       
        public List<EmailAlertsAndNotifications> ReadData(IEmailAlertsAndNotifications I)
        {
            int i = 0;
            List<EmailAlertsAndNotifications> myList = new List<EmailAlertsAndNotifications>();
         
            SqlDataReader dr = getExecuteReader("EmailAlertsAndNotificationsRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new EmailAlertsAndNotifications());
                    myList[i].EmailTriggerId = int.Parse(dr["EmailTriggerId"].ToString());
                    myList[i].EmailTriggerName = dr["EmailTriggerName"].ToString();
                    myList[i].EmailTemplateIds = dr["EmailTemplateIds"].ToString();
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally{
                if (dr.IsClosed != true && dr != null)
                dr.Close();
            }
            return myList;
        }




    }
}
