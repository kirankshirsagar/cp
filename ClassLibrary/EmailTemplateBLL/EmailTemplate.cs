﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using CommonBLL;

namespace EmailTemplateBLL

{


   public class EmailTemplate : IEmailTemplate
    {

        EmailTemplateDAL obj;
        public int EmailTemplateId { get; set; }
       
        private string _EmailTemplateName;

        public string EmailTemplateName
        {
            get { return _EmailTemplateName; }
            set {
                 if (value.Length == 0 && value.ToString() == "")
                {
                    throw new CustomException("EmailTemplate Name cannot be blank");
                }
                 _EmailTemplateName = value;
            }
        }
        public string EmailTemplateIds { get; set; }
        public string Search { get; set; }
        public string IsUsed { get; set; }
        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }

        public int ContractTemplateId { get; set; }
        public int ContractTypeId { get; set; }

        public int Direction { get; set; }
        public string SortColumn { get; set; }

        public string isActive { get; set; }
        public string FileName { get; set; }
        public string FileNameOriginal { get; set; }
        public string SizeModified { get; set; }
        public decimal SizeBytes { get; set; }
        public string AddedByName { get; set; }

        public string ContractTypeName { get; set; }
        public string ContractTemplateName { get; set; }
        
        public string Subject { get; set; }
        public string RoleIds { get; set; }
        public string UsersIds { get; set; }
        public string ReplyTo { get; set; }
        public DataTable Attachments { get; set; }
        public string TemplateBody { get; set; }
        public List<EmailTemplate> AttachmentLists { get; set; }

        private int _AddedBy;

        public int AddedBy
        {
            get { return _AddedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _AddedBy = value;
            }
        }

        private int _ModifiedBy;

        public int ModifiedBy
        {
            get { return _ModifiedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _ModifiedBy = value;
            }
        }

        private string _IpAddress;

        public string IpAddress
        {
            get { return _IpAddress; }
            set
            {
                if (value.ToString() == "")
                {
                    throw new CustomException("IP not captured");
                }
                _IpAddress = value;
            }
        }
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Description { get; set; }
        public string EventExecutionType { get; set; }
        public int EventExecutionValue { get; set; }



        public string DeleteRecord()
        {
            try
            {
                obj = new EmailTemplateDAL();
                return obj.DeleteRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string InsertRecord()
        {
            try
            {
                obj = new EmailTemplateDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateRecord()
        {
            try
            {
                obj = new EmailTemplateDAL();
                return obj.UpdateRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ChangeIsActive()
        {
            try
            {
                obj = new EmailTemplateDAL();
                return obj.ChangeIsActive(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<EmailTemplate>ReadData()
        {
            try
            {
                obj = new EmailTemplateDAL();
                return obj.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields> SelectData(int withOutSelect = 0)
        {
            try
            {
                obj = new EmailTemplateDAL();
                return obj.SelectData(this, withOutSelect);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<SelectControlFields> SelectDataWithOutTemplate(int withOutSelect = 0)
        {
            try
            {
                obj = new EmailTemplateDAL();
                return obj.SelectDataWithOutTemplate(this, withOutSelect);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable getAttachments()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("FileNameOriginal");
            dt.Columns.Add("FileName");
            dt.Columns.Add("FileSizeKb");

            DataRow row;

            if (FileName.Trim() != string.Empty)
            {
                try
                {

                string[] stringSeparators1 = new string[] { "$##$" };
                string[] attachments1 = FileName.Split(stringSeparators1, StringSplitOptions.None);

                foreach (var item1 in attachments1)
                {
                    string[] stringSeparators2 = new string[] { "#@@#" };
                    string[] attachments2 = item1.Split(stringSeparators2, StringSplitOptions.None);
                    row = dt.NewRow();
                    row["FileNameOriginal"] = attachments2[0];
                    row["FileName"] = attachments2[1];
                    row["FileSizeKb"] = attachments2[2];
                    dt.Rows.Add(row);
                }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            return dt;
        }
     
    }
}

