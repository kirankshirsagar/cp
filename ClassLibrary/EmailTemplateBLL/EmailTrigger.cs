﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using EmailBLL;


namespace EmailTemplateBLL
{
    public class EmailTrigger : IEmailTrigger
    {
        EmailTriggerDAL obj;
        public int EmailTriggerId { get; set; } 
        public int EmailTemplateId { get; set; }
        public int RequestId { get; set; }
        public string Subject { get; set; }
        public string RecipientEmailIds { get; set; }
        public string ReplyTo { get; set; }
        public string Description { get; set; }
        public string Attachments { get; set; }
        public string EmailBody { get; set; }
        public int ContractTypeId { get; set; }
        public int ContractTemplateId { get; set; }
        public int userId { get; set; }
        public int ActivityId { get; set; }
        public int AssignedToUserId { get; set; }
        public int StageId { get; set; }
        public int ContractId { get; set; }
        public string URL { get; set; }
        public Nullable<DateTime> AlertDate { get; set; }

        public bool SendEmail()
        {
            try
            {
                obj = new EmailTriggerDAL();
                return obj.SendEmail(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public void BulkEmail()
        {
            List<EmailTrigger> lst = new List<EmailTrigger>();
            obj = new EmailTriggerDAL();
            lst = obj.ReadData(this);

            foreach (var item in lst)
            {
                Subject = item.Subject.Replace("\n","").Replace("\r","");
                ReplyTo = item.ReplyTo;
                EmailBody = item.EmailBody;
                Attachments = item.Attachments;
                RecipientEmailIds = item.RecipientEmailIds;
                EmailTemplateId = item.EmailTemplateId;
                SendEmail();
            }
        }

        public void BulkEmailScheduler()
        {
            List<EmailTrigger> lst = new List<EmailTrigger>();
            obj = new EmailTriggerDAL();
            lst = obj.ReadDataScheduler(this);
            string activityIds = "";

            int i = 0;

            foreach (var item in lst)
            {
                Subject = item.Subject.Replace("\n", "").Replace("\r", "");
                ReplyTo = item.ReplyTo;
                EmailBody = item.EmailBody;
                Attachments = item.Attachments;
                RecipientEmailIds = item.RecipientEmailIds;
                RequestId = item.RequestId;
                ContractTypeId = item.ContractTypeId;
                ContractTemplateId = item.ContractTemplateId;
                EmailTemplateId = item.EmailTemplateId;
                EmailTriggerId = item.EmailTriggerId;
                ActivityId = item.ActivityId;

                SendEmail();
                if (i == 0)
                {
                    activityIds = Convert.ToString(item.ActivityId);
                }
                else
                {
                    activityIds = activityIds + "," + Convert.ToString(item.ActivityId);
                }
                i += 1;
            }
            obj.EmailActivityFlagUpdate(activityIds);

        }


    }
}
