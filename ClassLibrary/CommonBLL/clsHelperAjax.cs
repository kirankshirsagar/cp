﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using System.Web;




namespace CommonBLL
{
    public static class clsHelperAjax
    {
        public static void extSerialize(this HttpContext p, object obj)
        {
            JavaScriptSerializer jsonSerializaer = new JavaScriptSerializer();
            var result = (string)jsonSerializaer.Serialize(obj);
        }

    }
}
