﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Web;

namespace CommonBLL
{
    public static class clsHelperFind
    {


        public static void ClassChange(this Repeater rpt, LinkButton btn)
        {
            try
            {

            Control HeaderTemplate = rpt.Controls[0].Controls[0];
            LinkButton btn1 = HeaderTemplate.FindControl(btn.ID) as LinkButton;
            btn1.CssClass = btn.CssClass.ToLower() == "sort desc" ? "sort asc" : "sort desc";             
            }
            catch (Exception)
            {

                throw;
            }

        }



        private static string ValidKey(string str)
        {
            try
            {
                List<string> values = str.Trim().Split('$').ToList<string>();
                int i = values.Count();
                return values[i - 1].ToString().Trim();
            }
            catch (Exception ex)
            {
                return "";
            }
        }


        public static string extValue(this HttpRequest request, string compairString)
        {
            NameValueCollection preValues = request.Form;
            string value = "";

            try
            {
                if (preValues.Keys.Count > 0)
                {
                    foreach (var key in request.Form.AllKeys)
                    {
                        if (key.Contains(compairString))
                        {

                            if (compairString.Trim() == ValidKey(key))
                            {
                                value = request.Form[key];
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                value = "";
                
            }
            return value; 
        }
        
    }
}
