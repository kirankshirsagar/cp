﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace CommonBLL
{
    public class DomainActivity
    {

        public static string DBName { get; set; }
        public static int NumberOfUsers { get; set; }
        public static DateTime ActivationDate { get; set; }
        public static DateTime ExpiryDate { get; set; }
        public static string ErrorMessage { get; set; }
        public static string IsOutlookCalendarEnable { get; set; }


        public static void GetDBInformation(string DomainName)
        {
            
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["CMSCentral_connString"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            try
            {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetDBName";
                cmd.Connection = con;
                cmd.Parameters.AddWithValue("@DomainName", DomainName);
                SqlDataReader dr;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    DBName = dr["DBName"].ToString();
                    NumberOfUsers = int.Parse(dr["NumberOfUsers"].ToString());
                    ActivationDate = Convert.ToDateTime(dr["ActivationDate"].ToString());
                    ExpiryDate = Convert.ToDateTime(dr["ExpiryDate"].ToString());
                    ErrorMessage = dr["ErrorMessage"].ToString();
                    IsOutlookCalendarEnable = dr["IsOutlookCalendarEnable"].ToString();
                }
                cmd.Parameters.Clear();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Dispose();
                con.Close();
            }
        }


        /*
        public static string GetDBName(string DomainName)
        {
            string Rvalue = "";
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["CMSCentral_connString"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            try
            {               
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetDBName";
                cmd.Connection = con;
                cmd.Parameters.AddWithValue("@DomainName", DomainName);
                Rvalue = Convert.ToString(cmd.ExecuteScalar());
                cmd.Parameters.Clear();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Dispose();
                con.Close();
            }
            return Rvalue;
        }

        */

    }
}
