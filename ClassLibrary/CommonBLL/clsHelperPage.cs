﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CommonBLL
{
    public static class Messages
    {
        public enum Type
        {
            SaveSuccess,
            UpdateSuccess,
            SaveFailure,
            IsExists,
            UpdateFail,
            DocGenerate,
            InvalidTemplate,
            AllColumnsEmpty
        }

        public static void Message(this Page p, string status, string addUpdateFlg)
        {
            if (addUpdateFlg == "0")
            {
                if (status == "1")
                {
                    p.MessageAlert(Messages.Type.SaveSuccess);
                }
                else if (status == "2")
                {
                    p.MessageAlert(Messages.Type.IsExists);
                }
                else if (status == "3")
                {
                    p.MessageAlert(Messages.Type.InvalidTemplate);
                }
                else if (status == "4")
                {
                    p.MessageAlert(Messages.Type.AllColumnsEmpty);
                }
                else
                {
                    p.MessageAlert(Messages.Type.SaveFailure);
                }
            }
            else
            {
                if (status == "1")
                {
                    p.MessageAlert(Messages.Type.UpdateSuccess);
                }
                else if (status == "2")
                {
                    p.MessageAlert(Messages.Type.IsExists);
                }
                else if (status == "3")
                {
                    p.MessageAlert(Messages.Type.InvalidTemplate);
                }

                else
                {
                    p.MessageAlert(Messages.Type.UpdateFail);
                }
            }
        }


    }

    public static class clsHelperPage
    {

        public static void MessageAlert(this Page p, Messages.Type msgType)
        {
            switch (msgType)
            {
                case Messages.Type.SaveSuccess:
                    p.JavaScriptClientScriptBlock("msg", "msgSaveSuccess()");
                    break;
                case Messages.Type.UpdateSuccess:
                    p.JavaScriptClientScriptBlock("msg", "msgUpdateSuccess()");
                    break;
                case Messages.Type.IsExists:
                    p.JavaScriptClientScriptBlock("msg", "msgExistsRecord()");
                    break;
                case Messages.Type.SaveFailure:
                    p.JavaScriptClientScriptBlock("msg", "msgSaveFail()");
                    break;
                case Messages.Type.UpdateFail:
                    p.JavaScriptClientScriptBlock("msg", "msgUpdateFail()");
                    break;
                case Messages.Type.InvalidTemplate:
                    p.JavaScriptClientScriptBlock("msg", "msgInvalidTemplate()");
                    break;
                case Messages.Type.AllColumnsEmpty:
                    p.JavaScriptClientScriptBlock("msg", "msgAllColumnsEmpty()");
                    break;
                default:
                    break;
            }
        }


        public static void JavaScriptStartupScript(this Page p, string uniqueIdentifierParamKey, string javaScriptFunction)
        {
            // add system.web.extention ref
            ScriptManager.RegisterStartupScript(p, typeof(Page), uniqueIdentifierParamKey, javaScriptFunction, true);
        }

        public static void JavaScriptClientScriptBlock(this Page p, string uniqueIdentifierParamKey, string javaScriptFunction)
        {
            // add system.web.extention ref
            ScriptManager.RegisterClientScriptBlock(p, typeof(Page), uniqueIdentifierParamKey, javaScriptFunction, true);
        }

        public static void ClearAllHiddenFileds(this Page p)
        {
            ContentPlaceHolder cPH = (ContentPlaceHolder)p.Master.FindControl("ContentPlaceHolder1");
            foreach (Control ctrl in cPH.Controls)
            {
                if (ctrl is HiddenField)
                {
                    HiddenField hf = (HiddenField)ctrl;
                    hf.Value = string.Empty;
                }
            }
        }

        public static void ClearAllHiddenFileds(this Page p, short noMasterPage)
        {

            foreach (Control ctrl in p.Controls)
            {
                if (ctrl is HiddenField)
                {
                    HiddenField hf = (HiddenField)ctrl;
                    hf.Value = string.Empty;
                }
            }
        }


        public static void extDisableControls(this Page p)
        {

            ContentPlaceHolder myContent =
            (ContentPlaceHolder)p.Master.FindControl("MainContent");


            foreach (Control ctrl in myContent.Controls)
            {

                if (ctrl is TextBox)
                {
                    ((TextBox)ctrl).Enabled = false;
                }

                if (ctrl is Button)
                {
                    ((Button)ctrl).Enabled = false;
                }

                if (ctrl is CheckBox)
                {
                    ((CheckBox)ctrl).Enabled = false;
                }

                if (ctrl is DropDownList)
                {
                    ((DropDownList)ctrl).Enabled = false;
                }

                if (ctrl is CheckBox)
                {
                    ((CheckBox)ctrl).Enabled = false;
                }

                if (ctrl is RadioButton)
                {
                    ((RadioButton)ctrl).Enabled = false;
                }

                if (ctrl is LinkButton)
                {
                    ((LinkButton)ctrl).Enabled = false;
                }


                if (ctrl is HtmlInputText)
                {
                    ((HtmlInputText)ctrl).Disabled = true;
                }

                if (ctrl is HtmlInputButton)
                {
                    ((HtmlInputButton)ctrl).Disabled = true;
                }

                if (ctrl is HtmlSelect)
                {
                    ((HtmlSelect)ctrl).Disabled = true;
                }

                if (ctrl is HtmlInputCheckBox)
                {
                    ((HtmlInputCheckBox)ctrl).Disabled = true;
                }

                if (ctrl is HtmlInputRadioButton)
                {
                    ((HtmlInputRadioButton)ctrl).Disabled = true;
                }

            }
        }


    }
}
