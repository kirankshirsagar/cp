﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace CommonBLL
{
    public class Declarations
    {
        public static readonly string PreviousStr = "« Previous";
        public static readonly string NextStr = "Next »";
        public static readonly string FirstStr = "First";
        public static readonly string LastStr = "Last";
        public static readonly string TotalRecord = "TotalRecord";
        public static readonly string User = "User";
        public static readonly string Department = "Department";
        public static readonly string IP = "IP";
        public static readonly string Message = "Message";
        public static readonly string AccessCSV = "AccessCSV";
        public static readonly string AccessMainSectionCSV = "AccessMainSectionCSV";

        public static readonly string Add = "Add";
        public static readonly string View = "View";
        public static readonly string Delete = "Delete";
        public static readonly string Update = "Update";

        public static readonly string RequestId = "RequestId";
        public static readonly string ContractID = "ContractID";

        public static readonly string SortControl = "SortControl";

        public static readonly string UserFullName = "UserFullName";

        public static readonly string UserRole = "UserRole";
        public static readonly string DeleteMSG = "DeleteMSG";

        public static readonly string RemainingCount = "RemainingCount";

        public static readonly string OutlookFileName = "OutlookFileName";
        public static readonly string OutlookURL = "OutlookURL";
        public static readonly string IsOutlookSynchEnable = "IsOutlookSynchEnable";
        public static readonly string URL = "URL";

        public static readonly string SelectQuery = "SelectQuery";
        public static readonly string CustomReportId = "CustomReportId";      

        public static readonly int RequestForm = 6;
        public static readonly int ImportantDates = 3;
        public static readonly int KeyObligations = 10;
        public static readonly int RecordsPerPage = 50;
    }
}
