﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.IO;


/// <summary>
/// Summary description for myTrace
/// </summary>

namespace CommonBLL
{
    public static class clsTrace
    {
        /// <summary>
        /// Warn trace on page.
        /// </summary>
        /// <param name="msg">write your message.</param>
        public static void TraceWarn(this Page p, string msg)
        {
            if (p.Trace.IsEnabled == true)
            {
                StackFrame CallStack = new StackFrame(1, true);
                msg = (msg + ", File: " + CallStack.GetFileName() + ", Line: " + CallStack.GetFileLineNumber());
                p.Trace.Warn(msg);
            }
        }

        /// <summary>
        /// Write trace on page.
        /// </summary>
        /// <param name="msg">write your message.</param>
        public static void TraceWrite(this Page p, string msg)
        {
           
            if (p.Trace.IsEnabled == true)
            {
                StackFrame CallStack = new StackFrame(1, true);
                msg = (msg + ", File: " + CallStack.GetFileName() + ", Line: " + CallStack.GetFileLineNumber());
                p.Trace.Write(msg);
            }
        }

        /// <summary>
        /// Warn trace from class file.
        /// </summary>
        /// <param name="msg">write your message.</param>
        public static void TraceWarn(string msg)
        {
            if (HttpContext.Current.Trace.IsEnabled == true)
            {
                StackFrame CallStack = new StackFrame(1, true);
                msg = (msg + ", File: " + CallStack.GetFileName() + ", Line: " + CallStack.GetFileLineNumber());
                HttpContext.Current.Trace.Warn(msg);
            }
        }


        /// <summary>
        /// Write trace from class file.
        /// </summary>
        /// <param name="msg">write your message.</param>
        public static void TraceWrite(string msg)
        {
     
            if (HttpContext.Current.Trace.IsEnabled == true)
            {
                StackFrame CallStack = new StackFrame(1, true);
                msg = (msg + ", File: " + CallStack.GetFileName() + ", Line: " + CallStack.GetFileLineNumber());
                HttpContext.Current.Trace.Write(msg);
            }
        }



    }
}
