﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using System.Data;
using System.Web.Profile;

namespace CommonBLL
{
    public abstract class DAL 
    {
        public SqlConnection con = new SqlConnection();
        public SqlTransaction tran = null;
        SqlDataReader rdr;
        public SqlCommand cmd;
        public SqlDataAdapter adptr;
        DataTable dt;
        DataSet ds;

        string constr = "";
        public DAL()
        {
            constr = ConfigurationManager.ConnectionStrings["connString"].ConnectionString;
            cmd = new SqlCommand();
        }
        public DAL(string constring)
        {
            constr = constring;
        }

        #region SqlConnections

        void DBOpenconnection()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.ConnectionString = constr;
                con.Open();
            }
            else
            {
                con.Dispose();
                con.Close();
                DBOpenconnection();
            }

        }
         void DBCloseconnection()
        {
            if (con.State == ConnectionState.Open)
            {
                con.Dispose();
                con.Close();
            }

        }
          #endregion

        #region SqlParameterCollection
        public SqlParameterCollection Parameters
        {
             get{return cmd.Parameters;}
        }
        #endregion

        public DataSet getDataSet(string StoredProcName)
        {            
            ds = new DataSet();
            try
            {
                DBOpenconnection();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = StoredProcName.Trim();
                cmd.CommandTimeout = 0;
                adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                adptr.Fill(ds);
                cmd.Parameters.Clear();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
                
            }
            finally
            {
                cmd.Dispose();
                DBCloseconnection();
            }
            return ds;
        }

        public DataSet getDataSet(string StoredProcName, int isDynamicQry)
        {

            ds = new DataSet();
            try
            {
                DBOpenconnection();
                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = StoredProcName.Trim();
                cmd.CommandTimeout = 0;
                adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                adptr.Fill(ds);
                cmd.Parameters.Clear();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd.Dispose();
                DBCloseconnection();
            }
            return ds;
        }

        public DataTable getDataTable(string StoredProcName)
        {
            dt = new DataTable();
            try
            {
                DBOpenconnection();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = StoredProcName.Trim();
                cmd.CommandTimeout = 0;
                adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                adptr.Fill(dt);
                cmd.Parameters.Clear();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                DBCloseconnection();
            }
            return dt;
        }

        public string getExcuteScalar(string StoredProcName)
        {
            string Rvalue = "";
            try
            {
                DBOpenconnection();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = StoredProcName.Trim();
                cmd.CommandTimeout = 0;

                cmd.Connection = con;
                Rvalue = Convert.ToString(cmd.ExecuteScalar());
                cmd.Parameters.Clear();
            }
            catch (Exception ex)
            {
                throw ex;
            
            }
            finally
            {
                cmd.Dispose();
                DBCloseconnection();
            }
            return Rvalue;
        }

        public string getExcuteScalar(string StoredProcName, short DynamicQuery)
        {
            string Rvalue = "";
            try
            {
                DBOpenconnection();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = StoredProcName.Trim();
                cmd.CommandTimeout = 0;
                cmd.Connection = con;
                Rvalue = Convert.ToString(cmd.ExecuteScalar());
                cmd.Parameters.Clear();
            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd.Dispose();
                DBCloseconnection();

            }
            return Rvalue;
        }

        public bool CheckQuery(string StoredProcName)
        {
           bool status = true;
            try
            {
                DBOpenconnection();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = StoredProcName.Trim();
                cmd.CommandTimeout = 0;
                cmd.Connection = con;
                cmd.ExecuteScalar();
                cmd.Parameters.Clear();
                status = true;
            }
            catch (Exception ex)
            {
                status = false;
            }
            finally
            {
                cmd.Dispose();
                DBCloseconnection();

            }
            return status;
        }

        public String ReadRecordCount(string StoredProcName)
        {
            string  Rvalue = "";
            try
            {
                DBOpenconnection();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = StoredProcName.Trim();
                cmd.CommandTimeout = 0;
                cmd.Connection = con;
                rdr = cmd.ExecuteReader();

                int i = 0;
                while (rdr.Read())
                {
                    i++;
                }
                Rvalue = i.ToString();
                cmd.Parameters.Clear();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                DBCloseconnection();

            }
            return Rvalue;
        }

        public string getExcuteQuery(string StoredProcName)
        {
            string Rvalue = "";
            try
            {
                DBOpenconnection();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = StoredProcName.Trim();
                cmd.CommandTimeout = 2000;
                cmd.Connection = con;
                Rvalue = Convert.ToString(cmd.ExecuteNonQuery());
                cmd.Parameters.Clear();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                DBCloseconnection();

            }
            return Rvalue;
        }

        /// <summary>
        /// Takes 1 output parameter and return the value of that output parameter...
        /// </summary>
        /// <param name="StoredProcName"></param>
        /// <param name="outPutParameter"></param>
        /// <returns></returns>

        public string getExcuteQuery(string StoredProcName, string outPutParameter)
        {
            
            string Rvalue = "";
            try
            {
                DBOpenconnection();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = StoredProcName.Trim();
                cmd.CommandTimeout = 0;
                cmd.Connection = con;
                cmd.ExecuteNonQuery();
                Rvalue= cmd.Parameters[outPutParameter].Value.ToString();
                cmd.Parameters.Clear();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                DBCloseconnection();
            }
            return Rvalue;
        }
        public string getExcuteQuery(string StoredProcName, string outPutParameter1, string outPutParameter2)
        {

            string Rvalue = "";
            try
            {
                DBOpenconnection();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = StoredProcName.Trim();
                cmd.CommandTimeout = 0;
                cmd.Connection = con; 
                cmd.ExecuteNonQuery();
                Rvalue = cmd.Parameters[outPutParameter1].Value.ToString();
                if (Rvalue != string.Empty)
                {
                    Rvalue = Rvalue + "," + cmd.Parameters[outPutParameter2].Value.ToString();
                }

                cmd.Parameters.Clear();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                DBCloseconnection();

            }
            return Rvalue;
        }
        public string getExcuteQuery(string StoredProcName, CommandType StoredProcOrText, short allowDynamicSql)
        {
            string Rvalue = "";
            try
            {
                DBOpenconnection();
                cmd.CommandType = StoredProcOrText;
                cmd.CommandText = StoredProcName.Trim();
                cmd.CommandTimeout = 0;
                cmd.Connection = con;
                Rvalue = Convert.ToString(cmd.ExecuteNonQuery());
                cmd.Parameters.Clear();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                DBCloseconnection();

            }
            return Rvalue;
        }

        public SqlDataReader getExecuteReader(string qry)
        {
            try
            {
                DBOpenconnection();
                cmd.Connection = con;
                cmd.CommandTimeout = 0;
                cmd.CommandText = qry;
                cmd.CommandType = CommandType.StoredProcedure;
                rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                cmd.Parameters.Clear();
            }
            catch (SqlException ex)
            {
                throw ex;
            }

            catch (Exception ex)
            {
                throw ex;
               
            }
            finally
            {
                if (cmd != null) { cmd.Dispose(); }
                //DBCloseconnection();
            }
            return rdr;
        }

        public SqlDataReader getExecuteReader(string qry, CommandType StoredProcOrText)
        {
            try
            {
                DBOpenconnection();
                cmd.Connection = con;
                cmd.CommandTimeout = 0;
                cmd.CommandText = qry;
                cmd.CommandType = StoredProcOrText;
                rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                cmd.Parameters.Clear();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmd != null) { cmd.Dispose(); }
                //DBCloseconnection();
            }
            return rdr;
        }
    }
}
