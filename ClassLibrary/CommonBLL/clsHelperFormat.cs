﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace CommonBLL
{
    public static class clsHelperFormat
    {
        public static string extCurrencyFormat(this string inputValue, int noDecimal)
        {
            double localValue = 0;
            try
            {
                localValue = Convert.ToDouble(inputValue);
            }
            catch (Exception)
            {
                // if string is not able to convert into double then it will be converted to zero
                localValue = 0;
            }
            //localValue = CurrencyValue(localValue.ToString());
            //return String.Format("{0:0.00}", Convert.ToDouble(localValue));
            return setCurrencyFormat(localValue.ToString());
        }

        public static string extCurrencyFormatDecimal(this string inputValue, int noDecimal)
        {
            double localValue = 0;
            try
            {
                localValue = Convert.ToDouble(inputValue);
            }
            catch (Exception)
            {
                // if string is not able to convert into double then it will be converted to zero
                localValue = 0;
            }
            //localValue = CurrencyValue(localValue.ToString());
            //return String.Format("{0:0.00}", Convert.ToDouble(localValue));
            return setCurrencyFormat(String.Format("{0:0.00}", Convert.ToDouble(localValue)),0);
        }

        public static string extCurrencyFormat(this string inputValue)
        {
            double localValue = 0;
            try
            {
                localValue = Convert.ToDouble(inputValue);
            }
            catch (Exception)
            {
                // if string is not able to convert into double then it will be converted to zero
                localValue = 0;
            }
            //localValue = CurrencyValue(localValue.ToString());
            return String.Format("{0:0.00}", Convert.ToDouble(localValue));
            //return setCurrencyFormat(localValue.ToString());
        }

        public static string extCurrencyFormat(this int inputValue)
        {
            return String.Format("{0:0.00}", inputValue);
        }

        public static string extCurrencyFormat(this double inputValue)
        {
            return String.Format("{0:0.00}", inputValue);
        }


        private static string setCurrencyFormat( string val)
        {
           int valLength = val.Length;
           double intVal = Microsoft.VisualBasic.Conversion.Val(val);
           intVal = Math.Round(intVal);
           CultureInfo hindi = new CultureInfo("hi-IN");
           string rv =string.Format(hindi, "{0:c}", intVal);
           return rv.Substring(2, rv.Length -5);  // round decimal
           //return intVal.ToString("C");
        }

        private static string setCurrencyFormat(string val, short noround)
        {
            int valLength = val.Length;
            double intVal = Microsoft.VisualBasic.Conversion.Val(val);
           
            CultureInfo hindi = new CultureInfo("hi-IN");
            string rv = string.Format(hindi, "{0:c}", intVal);
            return rv.Substring(2, rv.Length - 2);  // round decimal
            //return intVal.ToString("C");
        }


        public static double extCurrencyValue(this string strValue)
        {
            strValue = strValue.Trim();
            double ReturnValue = 0;
            if (strValue != string.Empty)
            {
                ReturnValue = getActualValue(strValue);
            }
            else
            {
                ReturnValue = 0;
            }
            return ReturnValue;
        }

        private static double getActualValue(string strValue)
        {
            double retVal = 0;
            char s1 = (char)strValue[0];
            int checkValue = Convert.ToInt32(s1);
            string str = "";
            if (checkValue >= 48 && checkValue <= 57)
            {
                str = strValue;
            }
            else
            {
                str = strValue.Substring(1, strValue.Length - 1);
            }
            retVal = Microsoft.VisualBasic.Conversion.Val(str.Replace(",", string.Empty));
            retVal = Math.Round(retVal, 2);
            return retVal;
        }


    }
}
