﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace CommonBLL
{
    public static class clsHelperBind
    {
        /// <summary>
        /// This will fills dropdown control by splitting csv..
        /// </summary>
        /// <param name="stringValuesToBind">csv</param>
        public static void extDataBind(this DropDownList ddl, string stringValuesToBind)
        {
            try
            {
                string[] ddlDisplayText = stringValuesToBind.Split(',');
                int i = 0;
                foreach (string items in ddlDisplayText)
                {
                    ddl.Items.Insert(i, new ListItem(items, items));
                    i++;
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static void extDataBind(this DropDownList ddl, List<SelectControlFields> myList)
        {
            try
            {
                ddl.DataSource = myList;
                ddl.DataValueField = SelectControlFields.IdField;
                ddl.DataTextField = SelectControlFields.IdName;
                ddl.DataBind();
            }
            catch (Exception ex)
            {
            }
        }

        public static void extDataBind(this DropDownList ddl, List<SelectControlFields> myList, string isIdAsString)
        {
            try
            {
                ddl.DataSource = myList;
                ddl.DataValueField = SelectControlFields.IdFields;
                ddl.DataTextField = SelectControlFields.IdName;
                ddl.DataBind();
            }
            catch (Exception ex)
            {
            }
        }

        public static void extDataBind(this HtmlSelect ddl, List<SelectControlFields> myList)
        {
            try
            {
                ddl.DataSource = myList;
                ddl.DataValueField = SelectControlFields.IdField;
                ddl.DataTextField = SelectControlFields.IdName;
                ddl.DataBind();
            }
            catch (Exception ex)
            {
            }
        }




        public static void extDataBind(this DropDownList ddl, SqlDataReader dr)
        {
            try
            {
                ddl.DataSource = dr;
                ddl.DataValueField = dr.GetName(0);
                ddl.DataTextField = dr.GetName(1);
                ddl.DataBind();
                if (dr.IsClosed == false)
                {
                    dr.Close();
                }
            }
            catch (Exception ex)
            {
                dr.Close();
            }

        }


        public static void extDataBind(this HtmlSelect ddl, SqlDataReader dr)
        {
            try
            {

                ddl.DataSource = dr;
                ddl.DataValueField = dr.GetName(0);
                ddl.DataTextField = dr.GetName(1);
                ddl.DataBind();
                if (dr.IsClosed == true)
                {
                    dr.Close();
                }
            }
            catch (Exception ex)
            {
            }

        }



        public static void extDataBind(this DropDownList ddl, string stringValuesToBind, short isSelectOption)
        {
            try
            {
                string[] ddlDisplayText = stringValuesToBind.Split(',');
                int i = 0;
                ddl.Items.Insert(i, new ListItem("Select", "0"));
                foreach (string items in ddlDisplayText)
                {
                    i++;
                    ddl.Items.Insert(i, new ListItem(items, items));
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static void extDataBind(this DropDownList ddl, DataTable dt)
        {
            ddl.DataSource = "";
            ddl.DataSource = dt;
            ddl.DataValueField = dt.Columns[0].ToString();
            ddl.DataTextField = dt.Columns[1].ToString();
            ddl.DataBind();
        }

        public static void extDataBind(this HtmlSelect ddl, DataTable dt)
        {
            ddl.DataSource = "";
            ddl.DataSource = dt;
            ddl.DataValueField = dt.Columns[0].ToString();
            ddl.DataTextField = dt.Columns[1].ToString();
            ddl.DataBind();
        }

        public static void extDataBind(this DropDownList ddl, DataSet ds)
        {
            ddl.DataSource = "";
            ddl.DataSource = ds;
            ddl.DataValueField = ds.Tables[0].Columns[0].ToString();
            ddl.DataTextField = ds.Tables[0].Columns[1].ToString();
            ddl.DataBind();
        }

        public static void extDataBind(this GridView gridView, DataTable dt)
        {
            gridView.DataSource = dt;
            gridView.DataBind();
        }

        public static void extDataBind(this Repeater Rpt, DataTable dt)
        {
            Rpt.DataSource = dt;
            Rpt.DataBind();
        }

        /// <summary>
        ///set multiple html select value
        /// </summary>
        /// <param name="ddl">html select </param>
        /// <param name="strSelected"> csv string values</param>
        public static void extSelectedValues(this HtmlSelect ddl, string strSelected)
        {
            int Param = 0;
            //string[] strArray = strSelected.Split(',');
            //foreach (string items in strArray)
            //{
            //    if (items != "")
            //    {
            //        ddl.Items[ddl.Items.IndexOf(ddl.Items.FindByValue(items))].Selected = true;
            //    }
            //}
            string[] strArray = strSelected.Split(',');
            foreach (string items in strArray)
            {
                if (items != "")
                {
                    try
                    {
                        ddl.Items[ddl.Items.IndexOf(ddl.Items.FindByValue(items))].Selected = true;
                    }
                    catch
                    {
                        Param = 1;
                    }
                }
            }
            if (Param == 1)
            {
                try
                {
                    throw new Exception("Some recipients were removed as their email ids have changed.");
                }
                catch { throw; }
            }
        }

        /// <summary>
        ///set multiple html select value
        /// </summary>
        /// <param name="ddl">html select </param>
        /// <param name="strSelected"> csv string values</param>
        /// <param name="addValueIfNotFound">this will add item to the control with the checking value </param> 
        public static void extSelectedValues(this HtmlSelect ddl, string strSelected, string addItemIfNotFound)
        {
            string[] strArray = strSelected.Split(',');

            foreach (string items in strArray)
            {
                if (items != "")
                {
                    try
                    {
                        ddl.Items[ddl.Items.IndexOf(ddl.Items.FindByValue(items))].Selected = true;
                    }
                    catch (Exception ex)
                    {
                        if (ex is SystemException)
                        {
                            ddl.Items.Insert(ddl.Items.Count, new ListItem(addItemIfNotFound, strSelected));
                            ddl.Items[ddl.Items.IndexOf(ddl.Items.FindByValue(items))].Selected = true;

                            List<ListItem> tmpLst = new List<ListItem>();
                            foreach (ListItem item in ddl.Items)
                            {
                                // hold values in tmpLst for sorting...
                                tmpLst.Add(item);
                            }
                            ddl.Items.Clear();

                            // sorting list except select i.e. value 0"
                            var res = tmpLst.OrderBy(item => item.Value != "0").ThenBy(item => item.Text);

                            foreach (ListItem item in res)
                            {
                                ddl.Items.Add(item);
                            }
                            //foreach (ListItem item in tmpLst.OrderBy(item => item.Text))
                        }
                    }
                }
            }
        }





        public static void extSelectedValues(this HtmlSelect ddl, string strSelected, Char SplitChar)
        {
            string[] strArray = strSelected.Split(SplitChar);
            foreach (string items in strArray)
            {
                if (items != "")
                {
                    ddl.Items[ddl.Items.IndexOf(ddl.Items.FindByValue(items))].Selected = true;
                }
            }
        }

        public static void extSelectedValues(this HtmlSelect ddl, string strSelected, short byText)
        {
            string[] strArray = strSelected.Split(',');
            foreach (string items in strArray)
            {
                if (items != "")
                {
                    ddl.Items[ddl.Items.IndexOf(ddl.Items.FindByText(items))].Selected = true;
                }
            }
        }

        public static void extSelectedValues(this HtmlSelect ddl, string strSelected, Char SplitChar, short byText)
        {
            string[] strArray = strSelected.Split(SplitChar);
            foreach (string items in strArray)
            {
                if (items != "")
                {
                    ddl.Items[ddl.Items.IndexOf(ddl.Items.FindByText(items))].Selected = true;
                }
            }
        }


        /// <summary>
        /// returns selected values 
        /// </summary>
        /// <param name="ddl"> html select control </param>
        /// <param name="strSelected"></param>
        /// <param name="SplitChar"></param>
        /// <param name="byText"></param>
        public static string extGetSelectedValues(this HtmlSelect ddl)
        {
            int i = 0;
            string selectedValues = "";
            foreach (ListItem listItem in ddl.Items)
            {
                if (listItem.Selected == true)
                {
                    if (i == 0)
                    {
                        selectedValues = listItem.Value;
                        i += 1;
                    }
                    else
                    {
                        selectedValues = selectedValues + "," + listItem.Value;
                    }
                }
            }

            return selectedValues;
        }


        public static DataTable TrimCells(this DataTable dt)
        {
            try
            {

                foreach (DataRow dr in dt.Rows)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        dr[dc] = dr[dc].ToString().Trim();
                    }
                }
            }
            catch (Exception)
            {
            }

            return dt;
        }

        public static DataTable RemoveInvalidHTMLData(this DataTable dt, string removeChar = "'")
        {
            try
            {

                foreach (DataRow dr in dt.Rows)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        dr[dc] = dr[dc].ToString().Replace(removeChar, " ");
                    }
                }
            }
            catch (Exception)
            {
            }
            return dt;

        }

        public static DataTable extReplaceQuotes(this DataTable dt)
        {
            try
            {
                try
                {
                    if (dt.Columns.Contains("Maxcount"))
                    {
                        dt.Columns.Remove("Maxcount");
                        dt.Columns.Remove("N");
                        dt.Columns.Remove("RequestId");
                        dt.Columns.Remove("N1");
                        dt.Columns.Remove("RequestId_ID");
                        dt.Columns.Remove("N2");
                        dt.Columns.Remove("RequestId_KO");
                    }
                    if (dt.Columns.Contains("TotalValue"))
                        dt.Columns.Remove("TotalValue");
                }
                catch (Exception)
                {
                }

                foreach (DataRow dr in dt.Rows)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {

                        dr[dc] = dr[dc].ToString().Replace("'", "’").Replace("\"", "”").Replace("`", "’");
                    }
                }
            }
            catch (Exception)
            {
            }
            return dt;

        }



        /// <summary>
        /// warning don't use this to insert data to the table.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>

        public static DataTable extReplaceExcelQuotes(this DataTable dt)
        {
            try
            {
                try
                {
                    if (dt.Columns.Contains("Maxcount"))
                    {
                        dt.Columns.Remove("Maxcount");
                        dt.Columns.Remove("N");
                        dt.Columns.Remove("RequestId");
                        dt.Columns.Remove("N1");
                        dt.Columns.Remove("RequestId_ID");
                        dt.Columns.Remove("N2");
                        dt.Columns.Remove("RequestId_KO");
                    }
                    if (dt.Columns.Contains("TotalValue"))
                        dt.Columns.Remove("TotalValue");
                }
                catch (Exception)
                {
                }

                foreach (DataRow dr in dt.Rows)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        dr[dc] = dr[dc].ToString().Replace("’", "'").Replace("`", "'").Replace("‘", "'");
                    }
                }
            }
            catch (Exception)
            {
            }

            return dt;

        }


        public static string extReplaceQuotes(this string inputString)
        {
            return inputString.Replace("'", "’").Replace("\"", "”").Replace("`", "’");
        }



        public static DataTable RemoveInvalidHTMLData(this DataTable dt, string removeChar, string replaceChar)
        {
            try
            {

                foreach (DataRow dr in dt.Rows)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        dr[dc] = dr[dc].ToString().Replace(removeChar, replaceChar);
                    }
                }
            }
            catch (Exception)
            {
            }
            return dt;

        }

        public static DataTable ReplaceEmptyCellByNull(this DataTable dt)
        {
            try
            {
                foreach (DataRow dr in dt.Rows)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        if (dr[dc].ToString().Replace(" ", "").Trim() == string.Empty)
                        {
                            dr[dc] = DBNull.Value;
                        }
                    }
                }
            }
            catch (Exception)
            {
            }

            return dt;
        }

        public static DataTable ToDataTable<T>(this List<T> iList)
        {
            DataTable dataTable = new DataTable();
            System.ComponentModel.PropertyDescriptorCollection propertyDescriptorCollection =
                System.ComponentModel.TypeDescriptor.GetProperties(typeof(T));
            for (int i = 0; i < propertyDescriptorCollection.Count; i++)
            {
                System.ComponentModel.PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
                Type type = propertyDescriptor.PropertyType;

                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    type = Nullable.GetUnderlyingType(type);


                dataTable.Columns.Add(propertyDescriptor.Name, type);
            }
            object[] values = new object[propertyDescriptorCollection.Count];
            foreach (T iListItem in iList)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = propertyDescriptorCollection[i].GetValue(iListItem);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        public static string ext_ExecutableString(this SqlCommand cmd)
        {
            StringBuilder rvalue = new StringBuilder();
            string value = string.Empty;
            int i = 0;
            try
            {
                foreach (SqlParameter p in cmd.Parameters)
                {
                    if (p.Value == null)
                    {
                        value = "null";
                    }
                    else
                    {
                        value = "'" + p.Value.ToString() + "'";
                    }

                    if (i == 0)
                    {
                        rvalue.Append("EXEC " + cmd.CommandText + " " + p.ParameterName + "=" + value);
                    }
                    else
                    {
                        rvalue = rvalue.Append(", " + p.ParameterName + "=" + value);
                    }
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                rvalue.Append(ex.Message);
            }
            return rvalue.ToString();
        }

    }
}
