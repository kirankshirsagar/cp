﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonBLL
{
    public static class Strings
    {
        /// <summary>
        /// Attach time stamp string.
        /// </summary>
        /// <param name="str"></param>
        public static string extTimeStamp(this string inputString, string padding="")
        {
            string strReturn = inputString;
            try
            {
                int lastIndex = inputString.LastIndexOf('.');
                string dateTimeStr = String.Format("{0:MM/dd/yy HH:mm:ss}", DateTime.Now).Replace("/", "").Replace(":", "").Replace(" ", "");
                if (padding.Trim() != "")
                {
                    dateTimeStr = padding + "_" + dateTimeStr;
                }

                if (lastIndex > -1)
                {
                    strReturn = inputString.Substring(0, lastIndex) + "_" + dateTimeStr + inputString.Substring(lastIndex, inputString.Length - lastIndex);
                }
                else
                {
                    strReturn = inputString + dateTimeStr;
                }
            }
            catch (Exception)
            {
               
            }
            return strReturn;
        }

        /// <summary>
        /// returns url string like.... http://localhost:25401/CM/
        /// </summary>
        /// <returns></returns>
        public static string domainURL()
        {
            string folderPath = System.Web.HttpContext.Current.Request.FilePath;
            string lastPart = "/" + System.Web.HttpContext.Current.Request.Url.Segments[1].ToString();
            string replacePart = folderPath.Replace(lastPart,"");
            //return System.Web.HttpContext.Current.Request.Url.ToString().Replace(replacePart,"");
            string url = System.Web.HttpContext.Current.Request.Url.ToString();
            int resultIndex = url.IndexOf(replacePart);
            if (resultIndex != -1)
            {
                url = url.Substring(0, resultIndex);
            }
            return url;

        }

    }
}
