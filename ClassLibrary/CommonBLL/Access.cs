﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace CommonBLL
{

    /// <summary>
    /// because of sealed class you can not inherit this class..
    /// </summary>
    public sealed class Access : DAL
    {

        public static string Add { get; private set; }
        public static string Update { get; private set; }
        public static string Delete { get; private set; }
        public static string View { get; private set; }



        private Access()
        {
            /// can create object with in the class but not from outside... 
        }


        public static void PageAccess(string pageName, string userId)
        {
            Access temp = new Access();
            temp.SetAccess(pageName, userId);
        }

        public static void PageAccess(Page page, string userId, string parentFolder)
        {
            string pageName = page.ToString().ToLower().Substring(4, page.ToString().Substring(4).Length).Replace("_aspx", ".aspx").Replace(parentFolder.ToLower(), "");
            Access temp = new Access();
            temp.SetAccess(pageName, userId);
        }

        public static void PageAccess(UserControl page, string userId, string parentFolder)
        {

            string pageName = page.ToString().ToLower().Substring(4, page.ToString().Substring(4).Length).Replace("_ascx", ".ascx").Replace(parentFolder.ToLower(), "");
            Access temp = new Access();
            temp.SetAccess(pageName, userId);

            //string pageName = userControl.ToString().ToLower().Substring(4, userControl.ToString().Substring(4).Length - 5); //+ ".aspx";
            //pageName = pageName.Replace(parentFolder.ToLower(), "").Replace("data", "").ToLower();
            //Access temp = new Access();
            //temp.SetAccess(pageName, userId);
        }





        public void SetAccess(string _pageName, string _userId)
        {
            System.Data.SqlClient.SqlDataReader dr = null;

            try
            {
                Parameters.AddWithValue("@PageName", _pageName);
                Parameters.AddWithValue("@UserId", _userId);
                dr = getExecuteReader("AccessGet");

                Access.Add = "N";
                Access.Update = "N";
                Access.Delete = "N";
                Access.View = "N";

                while (dr.Read())
                {
                    Access.Add = dr["Add"].ToString();
                    Access.Update = dr["Update"].ToString();
                    Access.Delete = dr["Delete"].ToString();
                    Access.View = dr["View"].ToString();
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
        }


        public static bool isCustomised(int accessId)
        {
            bool val = false;

            try
            {
                if (System.Web.HttpContext.Current.Session[Declarations.AccessCSV] != null)
                {

                    string[] accessIds = System.Web.HttpContext.Current.Session[Declarations.AccessCSV].ToString().Split(',');

                    foreach (var id in accessIds)
                    {
                        if (int.Parse(id) == accessId)
                        {
                            val = true;
                        }
                        if (val == true)
                        {
                            break;
                        }
                    } 

                }
                else
                {
                    val = false;
                }
            }
            catch (Exception)
            {
                val = false;
            }
            return val;
        }



        public static bool isMainSectionAccess(int parentId)
        {
            bool val = false;

            try
            {
                if (System.Web.HttpContext.Current.Session[Declarations.AccessMainSectionCSV] != null)
                {

                    string[] accessIds = System.Web.HttpContext.Current.Session[Declarations.AccessMainSectionCSV].ToString().Split(',');

                    foreach (var id in accessIds)
                    {
                        if (int.Parse(id) == parentId)
                        {
                            val = true;
                        }
                        if (val == true)
                        {
                            break;
                        }
                    }

                }
                else
                {
                    val = false;
                }
            }
            catch (Exception)
            {
                val = false;
            }
            return val;
        }


    }
}
