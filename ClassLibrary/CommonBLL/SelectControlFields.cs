﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonBLL
{

    /// <summary>
    /// This class is used for filling dropdownlist control through the application.
    /// </summary>
    public class SelectControlFields
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Ids { get; set; }


        // IdField is property Id of the class
        // IdName is property Name of the class

        public static string IdField = "Id";
        public static string IdName = "Name";
        public static string IdFields = "Ids";


        public static List<SelectControlFields>SelectData(System.Data.SqlClient.SqlDataReader dr)
        {
            
            List<SelectControlFields> myList = new List<SelectControlFields>();

            myList.Add(new SelectControlFields());
            myList[0].Id = 0;
            myList[0].Name = "--------Select--------";

            int i = 1;
            try
            {
                while (dr.Read())
                {
                    myList.Add(new SelectControlFields());
                    myList[i].Id = int.Parse(dr[0].ToString());
                    myList[i].Name = dr[1].ToString();
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
          return myList;
        }
        public static List<SelectControlFields> SelectData(System.Data.DataTable dt)
        {
            List<SelectControlFields> myList = new List<SelectControlFields>();

            myList.Add(new SelectControlFields());
            myList[0].Id = 0;
            myList[0].Name = "--------Select--------";
           
            try
            {
                for(int i=0; i<dt.Rows.Count;i++)
                {
                    myList.Add(new SelectControlFields());
                    myList[i+1].Id = int.Parse(dt.Rows[i][0].ToString());
                    myList[i+1].Name = dt.Rows[i][1].ToString();                   
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dt = null;
            }
            return myList;
        }

        public static List<SelectControlFields> SelectDataNoSelect(System.Data.SqlClient.SqlDataReader dr)
        {
            List<SelectControlFields> myList = new List<SelectControlFields>();
            int i = 0;
            try
            {
                while (dr.Read())
                {
                    myList.Add(new SelectControlFields());
                    myList[i].Id = int.Parse(dr[0].ToString());
                    myList[i].Name = dr[1].ToString();
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
        }

        public static List<SelectControlFields>SelectData(System.Data.SqlClient.SqlDataReader dr, int isIdAsString)
        {

            List<SelectControlFields> myList = new List<SelectControlFields>();

            myList.Add(new SelectControlFields());
            myList[0].Ids = "0";
            myList[0].Name = "--------Select--------";

            int i = 1;
            try
            {
                while (dr.Read())
                {
                    myList.Add(new SelectControlFields());
                    myList[i].Ids = dr[0].ToString();
                    myList[i].Name = dr[1].ToString();
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
        }

        public static List<SelectControlFields> SelectDataNoSelect(System.Data.SqlClient.SqlDataReader dr, int isIdAsString)
        {

            List<SelectControlFields> myList = new List<SelectControlFields>();
            int i = 0;
            try
            {
                while (dr.Read())
                {
                    myList.Add(new SelectControlFields());
                    myList[i].Ids = dr[0].ToString();
                    myList[i].Name = dr[1].ToString();
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
        }

        /// <summary>
        /// pass prompt name which pop up 
        /// </summary>
        /// <param name="dr"> pass data reader </param>
        /// <param name="promptName"> prompt name </param>
        /// <returns></returns>
        public static List<SelectControlFields> SelectData(System.Data.SqlClient.SqlDataReader dr, string promptName)
        {

            List<SelectControlFields> myList = new List<SelectControlFields>();

            myList.Add(new SelectControlFields());
            myList[0].Id = 0;
            myList[0].Name = "--------" + promptName + "--------";

            int i = 1;
            try
            {
                while (dr.Read())
                {
                    myList.Add(new SelectControlFields());
                    myList[i].Id = int.Parse(dr[0].ToString());
                    myList[i].Name = dr[1].ToString();
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
        }


    }


      


}
