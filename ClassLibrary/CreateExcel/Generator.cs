﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;

namespace CreateExcel
{
    public class Generator
    {

        /// <summary>
        /// returns excel file stream
        /// </summary>
        /// <param name="filePath">upload file path</param>
        /// <param name="dt"> data table gets converted as excel file </param>
        /// <param name="isEmptyFile">defines file with header and data or with header only</param>
        /// <returns></returns>
        public static FileStream CreateExcelFile(string filePath, DataTable dt, bool isEmptyFile = true)
        {

            ExcelDocument document = new ExcelDocument();
            document.UserName = "test";
            int column = 0;

            foreach (DataColumn dc in dt.Columns)
            {
                document.ColumnWidth(column, 100);
                document[0, column].Value = dc.Caption;
                document[0, column].Font = new System.Drawing.Font("Tahoma", 10, System.Drawing.FontStyle.Bold);
                document[0, column].ForeColor = ExcelColor.Black;
                document[0, column].Alignment = Alignment.Centered;
                document[0, column].BackColor = ExcelColor.Automatic;

                column += 1;
            }
            if (isEmptyFile == false)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        document.WriteCell(i + 1, j, dt.Rows[i].ItemArray[j].ToString());
                    }
                }
            }

            FileStream stream = new FileStream(filePath, FileMode.Create);
            document.Save(stream);
            return stream;

        }


    }
}
