﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace CreateExcel
{
    internal class ColumnInfo
    {
        public int Width { get; set; }
        public int Index { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is ColumnInfo)
            {
                return (this.Index == ((ColumnInfo)obj).Index);
            }
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
