﻿

using System;
using System.Collections.Generic;
using System.Text;

namespace CreateExcel
{
    public enum Alignment
    {
        General,
        Left,
        Centered,
        Right,
        Filled
    }
}
