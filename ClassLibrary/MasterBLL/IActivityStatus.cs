﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CommonBLL;

namespace MasterBLL
{
    public interface IActivityStatus 
    {
        int ActivityTypeId { get; set; }
        string StatusName { get; set; }
        List<SelectControlFields> SelectData(); 
    }

}
