﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasterBLL
{
     public class FactoryMaster
    {
        public static ICity GetCityDetail()
        {
            return new City();
        }

        public static ICountry GetCountryDetail()
        {
            return new Country();
        }

        public static IState GetStateDetail()
        {
            return new State();
        }

        public static IRequest GetRequestDetail()
        {
            return new Request();
        }

        public static ICurrency GetCurrencyDetail()
        {
            return new Currency();
        }

        public static IContractType GetContractTypeDetail()
        {
            return new ContractType();
        }

        //public static ICustomReports GetContractTypeDetail()
        //{
        //    return new ContractType();
        //}

        public static ICommonMaster GetCommonMasterDetail()
        {
            return new CommonMaster();
        }

        public static ITitle GetTitleDetail()
        {
            return new Title();
        }
        public static IContractTemplate GetContractTemplateDetail()
        {
            return new ContractTemplate();
        }

        public static IDocumentType GetDocumentTypeDetail()
        {
            return new DocumentType();
        }

        public static IActivityType GetActivityTypeDetail()
        {
            return new ActivityType();
        }

        public static IActivityStatus GetActivityStatusDetail()
        {
            return new ActivityStatus();
        }

        public static IContractingParty GetContractingPartyDetails()
        {
            return new ContractingParty();
        }
    }
}
