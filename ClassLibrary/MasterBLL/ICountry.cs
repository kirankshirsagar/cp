﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;

namespace MasterBLL
{
    public interface ICountry : ICrud, INavigation
    {
        int CountryId { get; set; }
        string CountryIds { get; set; }
        string CountryName { get; set; }
        string IsUsed { get; set; }
        string Search{ get; set; }
        string isActive { get; set; }

        string ChangeIsActive();
        List<SelectControlFields> SelectData(); 
        List<Country> ReadData();
    }

}
