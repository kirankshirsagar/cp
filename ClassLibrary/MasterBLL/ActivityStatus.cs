﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonBLL;

namespace MasterBLL
{

    
    public class ActivityStatus : IActivityStatus
    {
        ActivityStatusDAL obj;
       public int ActivityTypeId { get; set; }
       public string StatusName { get; set; }

       public List<SelectControlFields> SelectData()
       {
           try
           {
               obj = new ActivityStatusDAL();
               return obj.SelectData(this);
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

    }
}
