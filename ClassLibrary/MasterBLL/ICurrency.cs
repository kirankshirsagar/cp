﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;

namespace MasterBLL
{
    public interface ICurrency : ICrud, INavigation
    {
        int CurrencyId { get; set; }
        string CurrencyName { get; set; }
        string CurrencyCode { get; set; }
        string CurrencySymbol { get; set; }
        decimal ConversionRate { get; set; }
        string IsBaseCurrency { get; set; }
        string IsActive { get; set; }
        string status { get; set; }
        string Search { get; set; }
        DataTable CurrencyRateTable { get; set; }

        List<SelectControlFields> SelectData();
        List<Currency> ReadData();
    }

}
