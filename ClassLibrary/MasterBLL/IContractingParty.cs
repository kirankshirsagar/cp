﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;

namespace MasterBLL
{
    public interface IContractingParty : ICrud, INavigation
    {
        int ContractingPartyId { get; set; }
        string ContractingPartyIds { get; set; }
        string ContractingPartyName { get; set; }
        string IsUsed { get; set; }
        string Search{ get; set; }
        string isActive { get; set; }

        string ChangeIsActive();
        List<SelectControlFields> SelectData();
        List<ContractingParty> ReadData();
    }

}
