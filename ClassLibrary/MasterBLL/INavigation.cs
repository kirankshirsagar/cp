﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasterBLL
{
    public interface INavigation
    {
        long TotalRecords { get; set; }
        int PageNo { get; set; }
        int RecordsPerPage { get; set; }

        string AddedBy { get; set; }
        string ModifiedBy { get; set; }
        DateTime AddedOn { get; set; }
        DateTime ModifiedOn { get; set; }
        string IpAddress { get; set; }
        string Description { get; set; }
    }
}
