﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;


namespace MasterBLL
{
    public class CurrencyDAL : DAL
    {
        public string UpdateRecord(ICurrency I)
        {
            Parameters.AddWithValue("@CurrencyId", I.CurrencyId);           
            Parameters.AddWithValue("@ConversionRate", I.ConversionRate);
            Parameters.AddWithValue("@IsBaseCurrency", I.IsBaseCurrency);
            Parameters.AddWithValue("@isActive", I.IsActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@CurrencyRateTable", I.CurrencyRateTable);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterCurrencyUpdate", "@Status");
        }

        public List<SelectControlFields>SelectData(ICurrency I)
        {
            SqlDataReader dr = getExecuteReader("MasterCurrencySelect");
            return SelectControlFields.SelectData(dr);
        }

        public List<Currency> ReadData(ICurrency I)
        {
            int i = 0;
            List<Currency> myList = new List<Currency>();
            Parameters.AddWithValue("@CurrencyId", I.CurrencyId);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            SqlDataReader dr = getExecuteReader("MasterCurrencyRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new Currency());
                    myList[i].CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    myList[i].CurrencyName = dr["CurrencyName"].ToString();
                    myList[i].CurrencyCode = dr["CurrencyCode"].ToString();
                    myList[i].ConversionRate = decimal.Parse(dr["ConversionRate"].ToString());
                    myList[i].IsBaseCurrency = dr["IsBaseCurrency"].ToString();
                    myList[i].IsUsed = dr["IsUsed"].ToString();
                    myList[i].IsActive = dr["IsActive"].ToString();          
                    myList[i].ModifiedBy = int.Parse(dr["ModifiedBy"].ToString());

                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally{
                if (dr.IsClosed != true && dr != null)
                dr.Close();
            }
            return myList;
        }
    }
}
