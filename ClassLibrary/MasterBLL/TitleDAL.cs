﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;


namespace MasterBLL
{
    public  class TitleDAL : DAL
    {
        public string InsertRecord(ITitle I)
        {

            Parameters.AddWithValue("@TitleId",I.TitleId);
            Parameters.AddWithValue("@TitleName",I.TitleName);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@Status",0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterTitleAddUpdate", "@Status");
        }

        public string UpdateRecord(ITitle I)
        {
            Parameters.AddWithValue("@TitleId", I.TitleId);
            Parameters.AddWithValue("@TitleName", I.TitleName);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterTitleAddUpdate", "@Status");
        }

        public string DeleteRecord(ITitle I)
        {
            Parameters.AddWithValue("@TitleIds", I.TitleIds);
            return getExcuteQuery("MasterTitleDelete");
        }

        public string ChangeIsActive(ITitle I)
        {
            Parameters.AddWithValue("@TitleIds", I.TitleIds);
            Parameters.AddWithValue("@isActive", I.isActive);
            return getExcuteQuery("MasterTitleChangeIsActive");
        }

        public List<SelectControlFields>SelectData(ITitle I)
        {
            SqlDataReader dr = getExecuteReader("MasterTitleSelect");
            return SelectControlFields.SelectData(dr);
        }


        public List<Title> ReadData(ITitle I)
        {
            int i = 0;
            List<Title> myList = new List<Title>();
            Parameters.AddWithValue("@TitleId", I.TitleId);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            SqlDataReader dr = getExecuteReader("MasterTitleRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new Title());
                    myList[i].TitleId = int.Parse(dr["TitleId"].ToString());
                    myList[i].TitleName = dr["TitleName"].ToString();
                    myList[i].IsUsed = dr["isUsed"].ToString();
                    myList[i].isActive = dr["isActive"].ToString();
                    myList[i].Description = dr["Description"].ToString();
          
                    //myList[i].AddedBy = dr["UserName"].ToString();
                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally{
                if (dr.IsClosed != true && dr != null)
                dr.Close();
            }
            return myList;
        }




    }
}
