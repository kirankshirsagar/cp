﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using CommonBLL;

namespace MasterBLL

{

    public class City : ICity
    {

        CityDAL obj;
        public int CityId { get; set; }

        private string _CityName;
        public string CityName
        {
            get { return _CityName; }
            set 
            {
                if (value.Length == 0 && value.ToString() == "")
                {
                    throw new CustomException("City Name cannot be blank");
                }
                _CityName = value;
            }
        }

        public string CityIds { get; set; }

        private int _StateId;
        public int StateId
        {
            get { return _StateId; }
            set
            {
                if (value.ToString() == "0" && value.ToString() == "")
                {
                    throw new CustomException("Select a StateID");
                }
                _StateId = value;
            }
        }

        private int _CountryId;
        public int CountryId
        {
            get { return _CountryId; }
            set
            {
                if (value.ToString() == "0" && value.ToString() == "")
                {
                    throw new CustomException("Select a CountryID");
                }
                _CountryId = value;
            }
        }

        public string StateName { get; set; }
        public string CountryName { get; set; }
        public string Search { get; set; }
        public string IsUsed { get; set; }
        public int Direction { get; set; }
        public string SortColumn { get; set; }

        private int _AddedBy;

        public int AddedBy
        {
            get { return _AddedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _AddedBy = value;
            }
        }

        private int _ModifiedBy;

        public int ModifiedBy
        {
            get { return _ModifiedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _ModifiedBy = value;
            }
        }

        private string _IpAddress;
        public string IpAddress
        {
            get { return _IpAddress; }
            set 
            {
                if (value.ToString() == "")
                {
                    throw new CustomException("IP not captured");
                }
                _IpAddress = value; 
            }
        }
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Description { get; set; }
        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }

        public string isActive { get; set; }

        public string DeleteRecord()
        {
            try
            {
                obj = new CityDAL();
                return obj.DeleteRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string InsertRecord()
        {
            try
            {
                obj = new CityDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateRecord()
        {
            try
            {
                obj = new CityDAL();
                return obj.UpdateRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ChangeIsActive()
        {
            try
            {
                obj = new CityDAL();
                return obj.ChangeIsActive(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<City>ReadData()
        {
            try
            {
                obj = new CityDAL();
                return obj.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields>SelectData()
        {
            try
            {
                obj = new CityDAL();
                return obj.SelectData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}

