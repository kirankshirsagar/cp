﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using CommonBLL;

namespace MasterBLL

{


   public class Country : ICountry
    {

        CountryDAL obj;
        public int CountryId { get; set; }
       
        private string _CountryName;

        public string CountryName
        {
            get { return _CountryName; }
            set {
                 if (value.Length == 0 && value.ToString() == "")
                {
                    throw new CustomException("Country Name cannot be blank");
                }
                 _CountryName = value;
            }
        }
        public string CountryIds { get; set; }
        public string Search { get; set; }
        public string IsUsed { get; set; }
        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }

        public int Direction { get; set; }
        public string SortColumn { get; set; }

        public string isActive { get; set; }

        private int _AddedBy;

        public int AddedBy
        {
            get { return _AddedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _AddedBy = value;
            }
        }

        private int _ModifiedBy;

        public int ModifiedBy
        {
            get { return _ModifiedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _ModifiedBy = value;
            }
        }


     
        private string _IpAddress;

        public string IpAddress
        {
            get { return _IpAddress; }
            set
            {
                if (value.ToString() == "")
                {
                    throw new CustomException("IP not captured");
                }
                _IpAddress = value;
            }
        }
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Description { get; set; }


        public string DeleteRecord()
        {
            try
            {
                obj = new CountryDAL();
                return obj.DeleteRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string InsertRecord()
        {
            try
            {
                obj = new CountryDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateRecord()
        {
            try
            {
                obj = new CountryDAL();
                return obj.UpdateRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ChangeIsActive()
        {
            try
            {
                obj = new CountryDAL();
                return obj.ChangeIsActive(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<Country>ReadData()
        {
            try
            {
                obj = new CountryDAL();
                return obj.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields>SelectData()
        {
            try
            {
                obj = new CountryDAL();
                return obj.SelectData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
     
    }
}

