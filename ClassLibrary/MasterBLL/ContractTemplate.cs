﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using CommonBLL;

namespace MasterBLL

{

    public class ContractTemplate : IContractTemplate
    {

        ContractTemplateDAL obj;
        public int ContractTemplateId { get; set; }
        public int ContractTempIdOutPut { get; set; }
        private string _ContractTemplateName;

        public int Direction { get; set; }
        public string SortColumn { get; set; }

        public string ContractTemplateName
        {
            get { return _ContractTemplateName; }
            set 
            {
                if (value.Length == 0 && value.ToString() == String.Empty)
                {
                    throw new CustomException("ContractTemplate Name cannot be blank");
                }
                _ContractTemplateName = value;
            }
        }

        public string ContractTemplateIds { get; set; }

        public string TemplateFileName { get; set; }
        
        private string _TemplateFileActualName;

        public string TemplateFileActualName
        {
            get { return _TemplateFileActualName; }
            set
            {
                if (value.Length == 0 && value.ToString() == String.Empty)
                {
                    throw new CustomException("Fleupload cannot be blank");
                }
                //else if (value.Length < 6)
                //{
                //    throw new CustomException("Fleupload should be equal to six numbers");
                //}
                _TemplateFileActualName = value;
            }
        }

        private int _ContractTypeId;
        public int ContractTypeId
        {
            get { return _ContractTypeId; }
            set
            {
                if (value.ToString() == "0" && value.ToString() == String.Empty)
                {
                    throw new CustomException("Select a ContractTypeId");
                }
                _ContractTypeId = value;
            }
        }

        private int _RequestTypeId;
        public int RequestTypeId
        {
            get { return _RequestTypeId; }
            set
            {
                if (value.ToString() == "0" && value.ToString() == String.Empty)
                {
                    throw new CustomException("Select a RequestTypeId");
                }
                _RequestTypeId = value;
            }
        }

        public string ContractTypeName { get; set; }
        public string RequestTypeName { get; set; }
        public string Search { get; set; }
        public string IsUsed { get; set; }


        private int _AddedBy;

        public int AddedBy
        {
            get { return _AddedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _AddedBy = value;
            }
        }

        private int _ModifiedBy;

        public int ModifiedBy
        {
            get { return _ModifiedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _ModifiedBy = value;
            }
        }

        private string _IpAddress;
        public string IpAddress
        {
            get { return _IpAddress; }
            set 
            {
                if (value.ToString() == "")
                {
                    throw new CustomException("IP not captured");
                }
                _IpAddress = value; 
            }
        }

        public int UsersId { get; set; }

        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Description { get; set; }
        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public string IsExist { get; set; }
        public string RequestTypeIds { get; set; }

        public string isActive { get; set; }

        public string DeleteRecord()
        {
            try
            {
                obj = new ContractTemplateDAL();
                return obj.DeleteRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string InsertRecord()
        {
            try
            {
                obj = new ContractTemplateDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateRecord()
        {
            try
            {
                obj = new ContractTemplateDAL();
                return obj.UpdateRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ChangeIsActive()
        {
            try
            {
                obj = new ContractTemplateDAL();
                return obj.ChangeIsActive(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ContractTemplate>ReadData()
        {
            try
            {
                obj = new ContractTemplateDAL();
                return obj.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string IsExistInContractQuestionsAnswersTbl()
        {
            try
            {
                obj = new ContractTemplateDAL();
                return obj.CheckIsExist(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields>SelectData()
        {
            try
            {
                obj = new ContractTemplateDAL();
                return obj.SelectData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<SelectControlFields> SelectBulkImportData()
        {
            try
            {
                obj = new ContractTemplateDAL();
                return obj.SelectBulkImportData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        

        public List<SelectControlFields> SelectDataRequestType()
        {
            try
            {
                obj = new ContractTemplateDAL();
                return obj.SelectDataRequestType(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}

