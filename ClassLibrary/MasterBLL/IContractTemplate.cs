﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;

namespace MasterBLL
{
    public interface IContractTemplate : ICrud, INavigation
    {
        int ContractTemplateId { get; set; }
        int ContractTempIdOutPut { get; set; }
        int ContractTypeId { get; set; }
        int RequestTypeId { get; set; }
        string TemplateFileActualName { get; set; }
        string TemplateFileName { get; set; }
        string ContractTemplateIds { get; set; }
        string ContractTemplateName { get; set; }
        string ContractTypeName { get; set; }
        string RequestTypeName { get; set; }
        string IsUsed { get; set; }
        string Search{ get; set; }
        string isActive { get; set; }
        string IsExist { get; set; }
        string RequestTypeIds { get; set; }
        int UsersId { get; set; }

        string ChangeIsActive();
        string IsExistInContractQuestionsAnswersTbl();  
        List<SelectControlFields> SelectData();
        List<SelectControlFields> SelectBulkImportData();
        List<SelectControlFields> SelectDataRequestType(); 
        List<ContractTemplate> ReadData();
    }

}
