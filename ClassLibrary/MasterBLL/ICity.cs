﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;

namespace MasterBLL
{
    public interface ICity : ICrud, INavigation
    {
        int CityId { get; set; }
        int StateId { get; set; }
        int CountryId { get; set; }
        string CityIds { get; set; }
        string CityName { get; set; }
        string StateName { get; set; }
        string CountryName { get; set; }
        string IsUsed { get; set; }
        string Search{ get; set; }
        string isActive { get; set; }

        string ChangeIsActive();
        List<SelectControlFields> SelectData(); 
        List<City> ReadData();
    }

}
