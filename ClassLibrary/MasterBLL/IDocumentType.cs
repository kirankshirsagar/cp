﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;

namespace MasterBLL
{
    public interface IDocumentType : ICrud, INavigation
    {
        int DocumentTypeId { get; set; }
        string DocumentTypeIds { get; set; }
        string DocumentTypeName { get; set; }
       
        string IsUsed { get; set; }
        string Search{ get; set; }
        string isActive { get; set; }
      

        string ChangeIsActive();
        List<SelectControlFields> SelectData(); 
        List<DocumentType> ReadData();
    }

}
