﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;


namespace MasterBLL
{
    public  class ActivityTypeDAL : DAL
    {
        public string InsertRecord(IActivityType I)
        {

            Parameters.AddWithValue("@ActivityTypeId",I.ActivityTypeId);
            Parameters.AddWithValue("@ActivityTypeName",I.ActivityTypeName);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@Status",0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterActivityTypeAddUpdate", "@Status");
        }

        public string UpdateRecord(IActivityType I)
        {
            Parameters.AddWithValue("@ActivityTypeId", I.ActivityTypeId);
            Parameters.AddWithValue("@ActivityTypeName", I.ActivityTypeName);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterActivityTypeAddUpdate", "@Status");
        }

        public string DeleteRecord(IActivityType I)
        {
            Parameters.AddWithValue("@ActivityTypeIds", I.ActivityTypeIds);
            return getExcuteQuery("MasterActivityTypeDelete");
        }

        public string ChangeIsActive(IActivityType I)
        {
            Parameters.AddWithValue("@ActivityTypeIds", I.ActivityTypeIds);
            Parameters.AddWithValue("@isActive", I.isActive);
            return getExcuteQuery("MasterActivityTypeChangeIsActive");
        }

        public List<SelectControlFields>SelectData(IActivityType I)
        {
            Parameters.AddWithValue("@ActivityID", I.ActivityTypeId);
            SqlDataReader dr = getExecuteReader("MasterActivityTypeSelect");
            return SelectControlFields.SelectData(dr);
        }


        public List<ActivityType> ReadData(IActivityType I)
        {
            int i = 0;
            List<ActivityType> myList = new List<ActivityType>();
            Parameters.AddWithValue("@ActivityTypeId", I.ActivityTypeId);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            SqlDataReader dr = getExecuteReader("MasterActivityTypeRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new ActivityType());
                    myList[i].ActivityTypeId = int.Parse(dr["ActivityTypeId"].ToString());
                    myList[i].ActivityTypeName = dr["ActivityTypeName"].ToString();
                    myList[i].IsUsed = dr["isUsed"].ToString();
                    myList[i].isActive = dr["isActive"].ToString();
                    myList[i].Description = dr["Description"].ToString();
          
                    //myList[i].AddedBy = dr["UserName"].ToString();
                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally{
                if (dr.IsClosed != true && dr != null)
                dr.Close();
            }
            return myList;
        }




    }
}
