﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;

namespace MasterBLL
{
    public interface IActivityType : ICrud, INavigation
    {
        int ActivityTypeId { get; set; }
        string ActivityTypeIds { get; set; }
        string ActivityTypeName { get; set; }
        string IsUsed { get; set; }
        string Search{ get; set; }
        string isActive { get; set; }

        string ChangeIsActive();
        List<SelectControlFields> SelectData(); 
        List<ActivityType> ReadData();
    }

}
