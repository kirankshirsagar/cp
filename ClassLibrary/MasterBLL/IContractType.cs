﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;

namespace MasterBLL
{
    public interface IContractType : ICrud, INavigation
    {
        int ContractTypeId { get; set; }
        string ContractTypeIds { get; set; }
        string ContractTypeName { get; set; }
        string ContractTemplateName { get; set; }
        int StageCount { get; set; }
        int ContractTemplateId { get; set; }
        string IsUsed { get; set; }
        string Search{ get; set; }
        string isActive { get; set; }
       


        string ChangeIsActive();
        List<SelectControlFields> SelectData();
        List<ContractType> ReadData();
        List<ContractType> ReadDataRequestType();

        int UserId { get; set; }
    }

}
