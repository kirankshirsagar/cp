﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;
namespace MasterBLL
{
    public class ContractTypeDAL : DAL
    {
        public string InsertRecord(IContractType I)
        {

            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
            Parameters.AddWithValue("@ContractTypeName", I.ContractTypeName);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@Addedby", I.AddedBy);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@IP", I.IpAddress);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterContractTypeAddUpdate", "@Status");
        }

        public string UpdateRecord(IContractType I)
        {
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
            Parameters.AddWithValue("@ContractTypeName", I.ContractTypeName);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@Modifiedby", I.ModifiedBy);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@IP", I.IpAddress);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterContractTypeAddUpdate", "@Status");
        }

        public string DeleteRecord(IContractType I)
        {
            Parameters.AddWithValue("@ContractTypeIds", I.ContractTypeIds);
            return getExcuteQuery("MasterContractTypeDelete");
        }

        public string ChangeIsActive(IContractType I)
        {
            Parameters.AddWithValue("@ContractTypeIds", I.ContractTypeIds);
            Parameters.AddWithValue("@isActive", I.isActive);
            return getExcuteQuery("MasterContractTypeChangeIsActive");
        }

        public List<SelectControlFields> SelectData(IContractType I)
        {
            Parameters.AddWithValue("@RequestId", I.ContractTypeId);
            SqlDataReader dr = getExecuteReader("MasterContractTypeSelect");
            return SelectControlFields.SelectData(dr);
        }


        public List<ContractType> ReadData(IContractType I)
        {
            int i = 0;
            List<ContractType> myList = new List<ContractType>();
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@UserID", I.UserId);
            SqlDataReader dr = getExecuteReader("StageContractTypeRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new ContractType()); 
                    myList[i].ContractTypeId = int.Parse(dr["ContractTypeId"].ToString());
                    myList[i].ContractTypeName = dr["ContractTypeName"].ToString();
                    myList[i].ContractTemplateName = dr["ContractTemplateName"].ToString();
                    myList[i].ContractTemplateId = int.Parse(dr["ContractTemplateId"].ToString());
                    myList[i].StageCount = int.Parse(dr["StageCount"].ToString());
                    myList[i].Description = dr["Description"].ToString();
                    myList[i].IsUsed = dr["isUsed"].ToString();
                    myList[i].isActive = dr["isActive"].ToString();
                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
        }

        public List<ContractType> ReadDataRequestType(IContractType I)
        {
            int i = 0;
            List<ContractType> myList = new List<ContractType>();
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            Parameters.AddWithValue("@UserID", I.UserId);

            SqlDataReader dr = getExecuteReader("MasterContractTypeRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new ContractType());
                    myList[i].ContractTypeId = int.Parse(dr["ContractTypeId"].ToString());
                    myList[i].ContractTypeName = dr["ContractTypeName"].ToString();
                    myList[i].Description = dr["Description"].ToString();
                    myList[i].IsUsed = dr["isUsed"].ToString();
                    myList[i].isActive = dr["isActive"].ToString();
                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
        }




    }
}
