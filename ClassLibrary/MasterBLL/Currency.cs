﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using CommonBLL;

namespace MasterBLL
{
    public class Currency : ICurrency
    {
        CurrencyDAL obj;

        public int CurrencyId { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencySymbol { get; set; }
        public decimal ConversionRate { get; set; }
        public string IsBaseCurrency { get; set; }
        public string IsActive { get; set; }
        public string status { get; set; }

        public string Search { get; set; }
        public string IsUsed { get; set; }
        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public int Direction { get; set; }
        public string SortColumn { get; set; }
        public string Description { get; set; }
        public DataTable CurrencyRateTable { get; set; }

        private int _AddedBy;
        public int AddedBy
        {
            get { return _AddedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _AddedBy = value;
            }
        }

        private int _ModifiedBy;
        public int ModifiedBy
        {
            get { return _ModifiedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _ModifiedBy = value;
            }
        }

        private string _IpAddress;

        public string IpAddress
        {
            get { return _IpAddress; }
            set
            {
                if (value.ToString() == "")
                {
                    throw new CustomException("IP not captured");
                }
                _IpAddress = value;
            }
        }
        public string DeleteRecord()
        {
            try
            {
                obj = new CurrencyDAL();
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string InsertRecord()
        {
            try
            {
                obj = new CurrencyDAL();
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public string UpdateRecord()
        {
            try
            {
                obj = new CurrencyDAL();
                return obj.UpdateRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Currency> ReadData()
        {
            try
            {
                obj = new CurrencyDAL();
                return obj.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields> SelectData()
        {
            try
            {
                obj = new CurrencyDAL();
                return obj.SelectData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

