﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;





namespace MasterBLL
{
    public  class CityDAL : DAL
    {
        public string InsertRecord(ICity I)
        {

            Parameters.AddWithValue("@CityId",I.CityId);
            Parameters.AddWithValue("@CityName",I.CityName);
            Parameters.AddWithValue("@StateId", I.StateId);
            Parameters.AddWithValue("@CountryId", I.CountryId);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@Status",0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterCityAddUpdate", "@Status");
        }

        public string UpdateRecord(ICity I)
        {
            Parameters.AddWithValue("@CityId", I.CityId);
            Parameters.AddWithValue("@CityName", I.CityName);
            Parameters.AddWithValue("@StateId", I.StateId);
            Parameters.AddWithValue("@CountryId", I.CountryId);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterCityAddUpdate", "@Status");
        }

        public string DeleteRecord(ICity I)
        {
            Parameters.AddWithValue("@CityIds", I.CityIds);
            return getExcuteQuery("MasterCityDelete");
        }

        public string ChangeIsActive(ICity I)
        {
            Parameters.AddWithValue("@CityIds", I.CityIds);
            Parameters.AddWithValue("@isActive", I.isActive);
            return getExcuteQuery("MasterCityChangeIsActive");
        }

        public List<SelectControlFields>SelectData(ICity I)
        {
            SqlDataReader dr = getExecuteReader("MasterCitySelect");
            return SelectControlFields.SelectData(dr);
        }


        public List<City> ReadData(ICity I)
        {
            int i = 0;
            List<City> myList = new List<City>();
            Parameters.AddWithValue("@CityId", I.CityId);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            SqlDataReader dr = getExecuteReader("MasterCityRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new City());
                    myList[i].CityId = int.Parse(dr["CityId"].ToString());
                    myList[i].CityName = dr["CityName"].ToString();
                    myList[i].IsUsed = dr["isUsed"].ToString();
                    myList[i].isActive = dr["isActive"].ToString();
                    myList[i].Description = dr["Description"].ToString();
                    myList[i].CountryName = dr["CountryName"].ToString();
                    myList[i].StateName = dr["StateName"].ToString();
                    myList[i].CountryId = int.Parse(dr["CountryId"].ToString());
                    myList[i].StateId = int.Parse(dr["StateId"].ToString());
                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally{
                if (dr.IsClosed != true && dr != null)
                dr.Close();
            }
            return myList;
        }




    }
}
