﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonBLL;

namespace MasterBLL
{
    public class CommonMaster : ICommonMaster
    {
        CommonMasterDAL obj;
        public int ParentId { get; set; }
        public string TableName { get; set; }
        public string Search { get; set; }
        public int StageId { get; set; }
        public int RequestId { get; set; }
        public int FieldId { get; set; }
        public int UserId { get; set; }

        public List<SelectControlFields> SelectData()
        {
            try
            {
                obj = new CommonMasterDAL();
                return obj.SelectData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields> SelectDataStr()
        {
            try
            {
                obj = new CommonMasterDAL();
                return obj.SelectDataStr(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields> SelectDataNoSelect()
        {
            try
            {
                obj = new CommonMasterDAL();
                return obj.SelectDataNoSelect(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields> SelectDataStrNoSelect()
        {
            try
            {
                obj = new CommonMasterDAL();
                return obj.SelectDataStrNoSelect(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields> SelectData(string promptName)
        {
            try
            {
                obj = new CommonMasterDAL();
                return obj.SelectData(this,promptName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
