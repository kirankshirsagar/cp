﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;





namespace MasterBLL
{
    public class ContractTemplateDAL : DAL
    {
        public string InsertRecord(IContractTemplate I)
        {

            Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
            Parameters.AddWithValue("@ContractTemplateName", I.ContractTemplateName);
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
            Parameters.AddWithValue("@RequestTypeId", I.RequestTypeId);
            Parameters.AddWithValue("@RequestTypeIds", I.RequestTypeIds);
            Parameters.AddWithValue("@TemplateFileName", I.TemplateFileName);
            Parameters.AddWithValue("@TemplateFileActualName", I.TemplateFileActualName);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@Status", 0);
            Parameters.AddWithValue("@ContractTempID", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            Parameters["@ContractTempID"].Direction = ParameterDirection.Output;
            return getExcuteQuery("ContractTemplateAddUpdate", "@Status", "@ContractTempID");
        }

        public string UpdateRecord(IContractTemplate I)
        {
            Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
            Parameters.AddWithValue("@ContractTemplateName", I.ContractTemplateName);
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
            Parameters.AddWithValue("@RequestTypeId", I.RequestTypeId);
            Parameters.AddWithValue("@RequestTypeIds", I.RequestTypeIds.TrimEnd(','));
            Parameters.AddWithValue("@TemplateFileName", I.TemplateFileName);
            Parameters.AddWithValue("@TemplateFileActualName", I.TemplateFileActualName);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@Status", 0);
            Parameters.AddWithValue("@ContractTempID", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
           Parameters["@ContractTempID"].Direction = ParameterDirection.Output;
           return getExcuteQuery("ContractTemplateAddUpdate", "@Status", "@ContractTempID");
        }

        public string DeleteRecord(IContractTemplate I)
        {
            Parameters.AddWithValue("@ContractTemplateIds", I.ContractTemplateIds);
            return getExcuteQuery("ContractTemplateDelete");
        }

        public string ChangeIsActive(IContractTemplate I)
        {
            Parameters.AddWithValue("@ContractTemplateIds", I.ContractTemplateIds);
            Parameters.AddWithValue("@isActive", I.isActive);
            return getExcuteQuery("ContractTemplateChangeIsActive");
        }

        public List<SelectControlFields>SelectData(IContractTemplate I)
        {
            SqlDataReader dr = getExecuteReader("ContractTemplateSelect");
            return SelectControlFields.SelectData(dr);
        }

        public List<SelectControlFields>SelectBulkImportData(IContractTemplate I)
        {
            Parameters.AddWithValue("@UsersId", I.UsersId);
            SqlDataReader dr = getExecuteReader("GetContractTemplate");
            return SelectControlFields.SelectData(dr);
        }

        
        public List<SelectControlFields> SelectDataRequestType(IContractTemplate I)
        {
            SqlDataReader dr = getExecuteReader("ContractRequestTypeSelect");
            return SelectControlFields.SelectData(dr);
        }

        public string CheckIsExist(IContractTemplate I)
        {
            int i = 0;
            string result = string.Empty;
           
            Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
            SqlDataReader dr = getExecuteReader("ContractTemplateIsExist");
             try
             {
                 while (dr.Read())
                 {
                     result = "RecordExist";
                 }
             }
             catch (Exception ex)
             {
                 throw ex;
             }
            return result;
        }

        public List<ContractTemplate> ReadData(IContractTemplate I)
        {
            int i = 0;
            List<ContractTemplate> myList = new List<ContractTemplate>();
            Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            SqlDataReader dr = getExecuteReader("ContractTemplateRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new ContractTemplate());
                    myList[i].ContractTemplateId = int.Parse(dr["ContractTemplateId"].ToString());
                    myList[i].ContractTemplateName = dr["ContractTemplateName"].ToString();
                    myList[i].TemplateFileActualName = dr["TemplateFileActualName"].ToString();
                    myList[i].TemplateFileName = dr["TemplateFileName"].ToString();
                    myList[i].IsUsed = dr["isUsed"].ToString();
                    myList[i].isActive = dr["isActive"].ToString();
                    myList[i].Description = dr["Description"].ToString();
                    myList[i].ContractTypeName = dr["ContractTypeName"].ToString();
                    //myList[i].RequestTypeName = dr["RequestTypeName"].ToString();
                    myList[i].RequestTypeName = dr["RequestTypeName"].ToString().TrimEnd(',');
                    myList[i].RequestTypeId = int.Parse(dr["RequestTypeId"].ToString());
                    myList[i].ContractTypeId = int.Parse(dr["ContractTypeId"].ToString());

                    myList[i].RequestTypeIds = dr["RequestTypeIDs"].ToString();
                  
                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally{
                if (dr.IsClosed != true && dr != null)
                dr.Close();
            }
            return myList;
        }




    }
}
