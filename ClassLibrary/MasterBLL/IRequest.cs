﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;

namespace MasterBLL
{
    public interface IRequest : ICrud, INavigation
    {
        int RequestId { get; set; }
        string RequestIds { get; set; }
        string RequestName { get; set; }
        string IsUsed { get; set; }
        string Search{ get; set; }
        string isActive { get; set; }
        

        string ChangeIsActive();
        List<SelectControlFields> SelectData(); 
        List<Request> ReadData();
    }

}
