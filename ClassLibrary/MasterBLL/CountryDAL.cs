﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;


namespace MasterBLL
{
    public  class CountryDAL : DAL
    {
        public string InsertRecord(ICountry I)
        {

            Parameters.AddWithValue("@CountryId",I.CountryId);
            Parameters.AddWithValue("@CountryName",I.CountryName);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@Status",0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterCountryAddUpdate", "@Status");
        }

        public string UpdateRecord(ICountry I)
        {
            Parameters.AddWithValue("@CountryId", I.CountryId);
            Parameters.AddWithValue("@CountryName", I.CountryName);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterCountryAddUpdate", "@Status");
        }

        public string DeleteRecord(ICountry I)
        {
            Parameters.AddWithValue("@CountryIds", I.CountryIds);
            return getExcuteQuery("MasterCountryDelete");
        }

        public string ChangeIsActive(ICountry I)
        {
            Parameters.AddWithValue("@CountryIds", I.CountryIds);
            Parameters.AddWithValue("@isActive", I.isActive);
            return getExcuteQuery("MasterCountryChangeIsActive");
        }

        public List<SelectControlFields>SelectData(ICountry I)
        {
            SqlDataReader dr = getExecuteReader("MasterCountrySelect");
            return SelectControlFields.SelectData(dr);
        }


        public List<Country> ReadData(ICountry I)
        {
            int i = 0;
            List<Country> myList = new List<Country>();
            Parameters.AddWithValue("@CountryId", I.CountryId);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            SqlDataReader dr = getExecuteReader("MasterCountryRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new Country());
                    myList[i].CountryId = int.Parse(dr["CountryId"].ToString());
                    myList[i].CountryName = dr["CountryName"].ToString();
                    myList[i].IsUsed = dr["isUsed"].ToString();
                    myList[i].isActive = dr["isActive"].ToString();
                    myList[i].Description = dr["Description"].ToString();
          
                    //myList[i].AddedBy = dr["UserName"].ToString();
                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally{
                if (dr.IsClosed != true && dr != null)
                dr.Close();
            }
            return myList;
        }




    }
}
