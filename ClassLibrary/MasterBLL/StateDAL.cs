﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;


namespace MasterBLL
{
    public  class StateDAL : DAL
    {
        public string InsertRecord(IState I)
        {

            Parameters.AddWithValue("@StateId",I.StateId);
            Parameters.AddWithValue("@StateName",I.StateName);
            Parameters.AddWithValue("@CountryId", I.CountryId);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@Status",0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterStateAddUpdate", "@Status");
        }

        public string UpdateRecord(IState I)
        {
            Parameters.AddWithValue("@StateId", I.StateId);
            Parameters.AddWithValue("@StateName", I.StateName);
            Parameters.AddWithValue("@CountryId", I.CountryId);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterStateAddUpdate", "@Status");
        }

        public string DeleteRecord(IState I)
        {
            Parameters.AddWithValue("@StateIds", I.StateIds);
            return getExcuteQuery("MasterStateDelete");
        }

        public string ChangeIsActive(IState I)
        {
            Parameters.AddWithValue("@StateIds", I.StateIds);
            Parameters.AddWithValue("@isActive", I.isActive);
            return getExcuteQuery("MasterStateChangeIsActive");
        }

        public List<SelectControlFields>SelectData(IState I)
        {
            //Parameters.AddWithValue("@CountryId", I.CountryId);
            SqlDataReader dr = getExecuteReader("MasterStateSelect");
            return SelectControlFields.SelectData(dr);
        }


        public List<State> ReadData(IState I)
        {
            int i = 0;
            List<State> myList = new List<State>();
            Parameters.AddWithValue("@StateId", I.StateId);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);

            SqlDataReader dr = getExecuteReader("MasterStateRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new State());
                    myList[i].StateId = int.Parse(dr["StateId"].ToString());
                    myList[i].StateName = dr["StateName"].ToString();
                    myList[i].IsUsed = dr["isUsed"].ToString();
                    myList[i].isActive = dr["isActive"].ToString();
                    myList[i].Description = dr["Description"].ToString();
                    myList[i].CountryId = int.Parse(dr["CountryId"].ToString());
                    myList[i].CountryName = dr["CountryName"].ToString();
                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally{
                if (dr.IsClosed != true && dr != null)
                dr.Close();
            }
            return myList;
        }




    }
}
