﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;

namespace MasterBLL
{
    public class CommonMasterDAL : DAL
    {
        public List<SelectControlFields>SelectData(ICommonMaster I)
        {
            Parameters.AddWithValue("@PartentId", I.ParentId);
            Parameters.AddWithValue("@TableName", I.TableName);
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@FieldID", I.FieldId);
            SqlDataReader dr = getExecuteReader("MasterCommonSelect");
            return SelectControlFields.SelectData(dr);
        }


        public List<SelectControlFields>SelectDataStr(ICommonMaster I)
        {
            Parameters.AddWithValue("@PartentId", I.ParentId);
            Parameters.AddWithValue("@TableName", I.TableName);
            Parameters.AddWithValue("@RequestId", I.RequestId);
            SqlDataReader dr = getExecuteReader("MasterCommonSelect");
            return SelectControlFields.SelectData(dr,0);
        }


        public List<SelectControlFields> SelectDataNoSelect(ICommonMaster I)
        {
            Parameters.AddWithValue("@PartentId", I.ParentId);
            Parameters.AddWithValue("@TableName", I.TableName);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@StageId", I.StageId);
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@FieldID", I.FieldId);
            Parameters.AddWithValue("@UserId", I.UserId);
            SqlDataReader dr = getExecuteReader("MasterCommonSelect");
            return SelectControlFields.SelectDataNoSelect(dr);
        }


        public List<SelectControlFields> SelectDataStrNoSelect(ICommonMaster I)
        {
            Parameters.AddWithValue("@PartentId", I.ParentId);
            Parameters.AddWithValue("@TableName", I.TableName);
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@UserId", I.UserId);
            SqlDataReader dr = getExecuteReader("MasterCommonSelect");
            return SelectControlFields.SelectDataNoSelect(dr, 0);
        }

        public List<SelectControlFields> SelectData(ICommonMaster I, string promptName)
        {
            Parameters.AddWithValue("@PartentId", I.ParentId);
            Parameters.AddWithValue("@TableName", I.TableName);
            Parameters.AddWithValue("@RequestId", I.RequestId);
            SqlDataReader dr = getExecuteReader("MasterCommonSelect");
            return SelectControlFields.SelectData(dr, promptName);
        }


    }
}
