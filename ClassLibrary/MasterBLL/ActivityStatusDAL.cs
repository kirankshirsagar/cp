﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;

namespace MasterBLL
{
    public  class ActivityStatusDAL : DAL
    {
        public List<SelectControlFields> SelectData(IActivityStatus I)
        {
            SqlDataReader dr = getExecuteReader("MasterActivityStatusRead");
            return SelectControlFields.SelectData(dr);
        }
    }
}
