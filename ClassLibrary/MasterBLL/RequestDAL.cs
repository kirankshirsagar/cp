﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;





namespace MasterBLL
{
    public  class RequestDAL : DAL
    {
        public string InsertRecord(IRequest I)
        {

            Parameters.AddWithValue("@RequestId",I.RequestId);
            Parameters.AddWithValue("@RequestName",I.RequestName);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@Status",0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterRequestAddUpdate", "@Status");
        }

        public string UpdateRecord(IRequest I)
        {
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@RequestName", I.RequestName);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterRequestAddUpdate", "@Status");
        }

        public string DeleteRecord(IRequest I)
        {
            Parameters.AddWithValue("@RequestIds", I.RequestIds);
            return getExcuteQuery("MasterRequestDelete");
        }

        public string ChangeIsActive(IRequest I)
        {
            Parameters.AddWithValue("@RequestIds", I.RequestIds);
            Parameters.AddWithValue("@isActive", I.isActive);
            return getExcuteQuery("MasterRequestChangeIsActive");
        }

        public List<SelectControlFields>SelectData(IRequest I)
        {
            SqlDataReader dr = getExecuteReader("MasterRequestSelect");
            return SelectControlFields.SelectData(dr);
        }


        public List<Request> ReadData(IRequest I)
        {
            int i = 0;
            List<Request> myList = new List<Request>();
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@Search", I.Search);
            SqlDataReader dr = getExecuteReader("MasterRequestRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new Request());
                    myList[i].RequestId = int.Parse(dr["RequestId"].ToString());
                    myList[i].RequestName = dr["RequestName"].ToString();
                    myList[i].IsUsed = dr["isUsed"].ToString();
                    myList[i].isActive = dr["isActive"].ToString();
                    myList[i].Description = dr["Description"].ToString();
                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally{
                if (dr.IsClosed != true && dr != null)
                dr.Close();
            }
            return myList;
        }




    }
}
