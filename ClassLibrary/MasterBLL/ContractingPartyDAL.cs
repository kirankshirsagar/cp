﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;


namespace MasterBLL
{
    public class ContractingPartyDAL : DAL
    {
        public string InsertRecord(IContractingParty I)
        {

            Parameters.AddWithValue("@ContractingPartyId", I.ContractingPartyId);
            Parameters.AddWithValue("@ContractingPartyName", I.ContractingPartyName);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@Status",0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterContractingPartyAddUpdate", "@Status");
        }

        public string UpdateRecord(IContractingParty I)
        {
            Parameters.AddWithValue("@ContractingPartyId", I.ContractingPartyId);
            Parameters.AddWithValue("@ContractingPartyName", I.ContractingPartyName);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterContractingPartyAddUpdate", "@Status");
        }

        public string DeleteRecord(IContractingParty I)
        {
            Parameters.AddWithValue("@ContractingPartyIds", I.ContractingPartyIds);
            return getExcuteQuery("MasterContractingPartyDelete");
        }

        public string ChangeIsActive(IContractingParty I)
        {
            Parameters.AddWithValue("@ContractingPartyIds", I.ContractingPartyIds);
            Parameters.AddWithValue("@isActive", I.isActive);
            return getExcuteQuery("MasterContractingPartyIsActive");
        }

        public List<SelectControlFields>SelectData(IContractingParty I)
        {
            SqlDataReader dr = getExecuteReader("MasterContractingPartySelect");
            return SelectControlFields.SelectData(dr);
        }


        public List<ContractingParty> ReadData(IContractingParty I)
        {
            int i = 0;
            List<ContractingParty> myList = new List<ContractingParty>();
            Parameters.AddWithValue("@ContractingPartyId", I.ContractingPartyId);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            SqlDataReader dr = getExecuteReader("MasterContractingPartyRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new ContractingParty());
                    myList[i].ContractingPartyId = int.Parse(dr["ContractingPartyId"].ToString());
                    myList[i].ContractingPartyName = dr["ContractingPartyName"].ToString();
                    myList[i].IsUsed = dr["isUsed"].ToString();
                    myList[i].isActive = dr["isActive"].ToString();
                    myList[i].Description = dr["Description"].ToString();      
                    //myList[i].AddedBy = dr["UserName"].ToString();
                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally{
                if (dr.IsClosed != true && dr != null)
                dr.Close();
            }
            return myList;
        }




    }
}
