﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;

namespace MasterBLL
{
    public interface ICommonMaster 
    {
        int ParentId { get; set; }
        string TableName { get; set; }
        string Search { get; set; }
        int StageId { get; set; }
        int RequestId { get; set; }
        int FieldId { get; set; }
        int UserId { get; set; }
        List<SelectControlFields> SelectData();
        List<SelectControlFields> SelectDataStr();
        List<SelectControlFields> SelectDataNoSelect();
        List<SelectControlFields> SelectDataStrNoSelect();
        List<SelectControlFields> SelectData(string promptName);

    }

}
