﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;
namespace MasterBLL
{
    public class DocumentTypeDAL : DAL
    {
        public string InsertRecord(IDocumentType I)
        {

            Parameters.AddWithValue("@DocumentTypeId", I.DocumentTypeId);
            Parameters.AddWithValue("@DocumentTypeName", I.DocumentTypeName);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@Addedby", I.AddedBy);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@IP", I.IpAddress);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterDocumentTypeAddUpdate", "@Status");
        }

        public string UpdateRecord(IDocumentType I)
        {
            Parameters.AddWithValue("@DocumentTypeId", I.DocumentTypeId);
            Parameters.AddWithValue("@DocumentTypeName", I.DocumentTypeName);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@Modifiedby", I.ModifiedBy);
            Parameters.AddWithValue("@Description", I.Description);
            Parameters.AddWithValue("@IP", I.IpAddress);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("MasterDocumentTypeAddUpdate", "@Status");
        }

        public string DeleteRecord(IDocumentType I)
        {
            Parameters.AddWithValue("@DocumentTypeIds", I.DocumentTypeIds);
            return getExcuteQuery("MasterDocumentTypeDelete");
        }

        public string ChangeIsActive(IDocumentType I)
        {
            Parameters.AddWithValue("@DocumentTypeIds", I.DocumentTypeIds);
            Parameters.AddWithValue("@isActive", I.isActive);
            return getExcuteQuery("MasterDocumentTypeChangeIsActive");
        }

        public List<SelectControlFields> SelectData(IDocumentType I)
        {
            SqlDataReader dr = getExecuteReader("MasterDocumentTypeSelect");
            return SelectControlFields.SelectData(dr);
        }


        public List<DocumentType> ReadData(IDocumentType I)
        {
            int i = 0;
            List<DocumentType> myList = new List<DocumentType>();
            Parameters.AddWithValue("@DocumentTypeId", I.DocumentTypeId);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);

            SqlDataReader dr = getExecuteReader("MasterDocumentTypeRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new DocumentType()); 
                    myList[i].DocumentTypeId = int.Parse(dr["DocumentTypeId"].ToString());
                    myList[i].DocumentTypeName = dr["DocumentTypeName"].ToString();
                    myList[i].Description = dr["Description"].ToString();
                    myList[i].IsUsed = dr["isUsed"].ToString();
                    myList[i].isActive = dr["isActive"].ToString();
                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
        }




    }
}
