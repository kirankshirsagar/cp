﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using System.Data;
using System.Web.Security;
using CommonBLL;
using UserManagementBLL;
using MailBee;
using MailBee.Mime;
using MailBee.SmtpMail;

namespace EmailBLL
{
    public class EmailPassword : DAL
    {
        IUsers obj;
        public string _Username { get; set; }
        public string FirstName { get; set; }
        public string _EmailID { get; set; }

        public string _MailTo { get; set; }
        public string _Subject { get; set; }

        public string _Password { get; set; }
        public string _EncryptedPassword { get; set; }
        public string _Path { get; set; }

        public string UserId { get; set; }
        public string LoginUserId { get; set; }

        public bool SendPasswordExpiresMail()
        {
            CreateObjects();
            bool status = false;
            try
            {
                if (_MailTo != "")
                {
                    obj.UserName = _Username;
                    obj.MailTo = _MailTo;
                    string strURLWithData = obj.GetEmailId();

                    string UserId = strURLWithData;

                    string recoveryLink = HttpContext.Current.Request.Url.AbsoluteUri.Remove(HttpContext.Current.Request.Url.AbsoluteUri.Length - HttpContext.Current.Request.Url.Segments.Last().Length);


                    recoveryLink = recoveryLink.TrimStart('{').TrimEnd('}');
                    //recoveryLink = recoveryLink.Replace("AddUserService.asmx/UsersResetPassword", "Account/confirmpassword.aspx");

                    recoveryLink = recoveryLink + "UserManagement/confirmpassword.aspx" + "?UserId=" + UserId;
                    if (recoveryLink.Contains("122.179.131.228") == true)
                    {
                        recoveryLink = recoveryLink.Replace("122.179.131.228", "122.179.131.228:8084");
                    }
                    else if (recoveryLink.Contains("121.247.65.205") == true)
                    {
                        recoveryLink = recoveryLink.Replace("121.247.65.205", "121.247.65.205:8084");
                    }
                    obj.UserName = _Username;
                    string firstName = obj.GetUserFirstName();
                    StringBuilder strBody = new StringBuilder();
                    strBody.Append("<font face='Arial' size='2' color='#006666'><p>Hi ");
                    strBody.Append(firstName);
                    strBody.Append(",");
                    strBody.Append("</p>");
                    strBody.Append("<p>&nbsp;</p><p>Your password has expired.Please click the link below to setup a new password using our secure server:</p><p>");

                    strBody.Append(" <p><a href='" + recoveryLink + "' moz-do-not-send='true'>" + recoveryLink + "</a></p>");
                    strBody.Append("<p>&nbsp;</p><p>Alternately, you can copy and paste the link into your browser&#39;s address window, or retype it there.&nbsp;</p>");

                    strBody.Append("<p>&nbsp;</p><p>	Thanks,</p><p>	Contract Management Team</p>");
                    strBody.Append("<br><br><font face='Arial' size='1' color='#006666'>Need help? Please do not hesitate to contact our technical support team on 0800 699 0045 or at help@contractpod.com.");
                    strBody.Append("<br>The information in this email is confidential and for use by the addressee(s) only. If you are not the intended recipient, please notify us immediately on 0800 699 0045 and delete the message from your computer. You may not copy or forward the e-mail, or use it or disclose its contents to any other person. We do not accept any liability or responsibility for changes made to this email after it was sent, or viruses transmitted through this e-mail or any attachment.</font>");
        

                    string Body = strBody.ToString();

                    SendMail objmail = new SendMail();
                    objmail.UserId = UserId;
                    objmail.MailTo = _MailTo;
                    objmail.Subject = "[Contract Managment] - Password Reset Request";
                    objmail.Body = Body;
                    bool issend = objmail.SendEMail();
                    if (issend)
                    {
                        ChangeIsPasswordStatus(UserId);
                    }
                    status = issend;


                }

            }
            catch (Exception ex)
            {
                status = false;
            }
            return status;


        }

        public bool SendMail(string UserName, string MailTo, string path)
        {
            CreateObjects();
            bool status = false;

            obj.UserName = UserName;
            obj.MailTo = MailTo;

            string strURLWithData = obj.GetEmailId();
            string UserId = strURLWithData;

            if (UserId != "")
            {
                try
                {
                    string recoveryLink = path;//HttpContext.Current.Request.Url.ToString();
                    recoveryLink = recoveryLink.TrimStart('{').TrimEnd('}');
                    recoveryLink = recoveryLink.Replace("RegisterData.aspx", "confirmpassword.aspx");
                    //recoveryLink = recoveryLink + "?userId=" + UserId;
                    recoveryLink = recoveryLink + "?UserId=" + UserId;
                    if (recoveryLink.Contains("122.179.131.228") == true)
                    {
                        recoveryLink = recoveryLink.Replace("122.179.131.228", "122.179.131.228:8084");
                    }
                    else if (recoveryLink.Contains("121.247.65.205") == true)
                    {
                        recoveryLink = recoveryLink.Replace("121.247.65.205", "121.247.65.205:8084");
                    }
                     obj.UserName = UserName;
                     string FullName = obj.GetUserFirstName();
                    StringBuilder strBody = new StringBuilder();
                    strBody.Append("<font face='Arial' size='2' color='#006666'><p>Hi ");
                    strBody.Append(FullName);
                   // strBody.Append(System.Text.RegularExpressions.Regex.Split(FullName, " ")[0]);
                    //strBody.Append(",");
                    strBody.Append("</p>");
                    strBody.Append("<p>We received a request to reset the password associated with your account.</p><p>");
                    strBody.Append("Please click on the link below to reset your password:</p>");
                    strBody.Append(" <p><a href='" + recoveryLink + "' moz-do-not-send='true'>" + recoveryLink + "</a></p>");
                    strBody.Append("<p>Alternately, you can copy and paste the link into your browser&#39;s address window, or retype it there.&nbsp;</p>");
                    strBody.Append("<p>If you did not request to have your password reset you can safely ignore this email. Your account settings will remain unchanged.</p>");
                    strBody.Append("<p>Kind regards</p><p>The ContractPod™ Team</p>");

                    strBody.Append("<br><br><font face='Arial' size='1' color='#006666'>Need help? Please do not hesitate to contact our technical support team on 0800 699 0045 or at help@contractpod.com.");
                    strBody.Append("<br>The information in this email is confidential and for use by the addressee(s) only. If you are not the intended recipient, please notify us immediately on 0800 699 0045 and delete the message from your computer. You may not copy or forward the e-mail, or use it or disclose its contents to any other person. We do not accept any liability or responsibility for changes made to this email after it was sent, or viruses transmitted through this e-mail or any attachment.</font>");
        
                    string Body = strBody.ToString();

                    SendMail objmail = new SendMail();
                    objmail.UserId = UserId;
                    objmail.MailTo = MailTo;
                    objmail.Subject = "Password Reset Request";
                    objmail.Body = Body;
                    bool issend = objmail.SendEMail();
                    if (issend)
                    {
                        ChangeIsPasswordStatus(UserId);
                    }
                    status = issend;
                }
                catch (Exception ex)
                {
                    status = false;
                }
            }
            else
            {
                status = false;
            }
            return status;
        }
        void CreateObjects()
        {
            obj = FactoryUser.GetUsersDetail();
        }

        public string SendMail_New(string UserName, string MailTo, string path)
        {
            bool status = false;

            string strURLWithData = GetUserInformation(UserName, MailTo);

            string UserId = strURLWithData;

            UserName = _Username;
            MailTo = _EmailID;
            string message = "F";
            if (UserId != "")
            {
                try
                {
                    string recoveryLink = HttpContext.Current.Request.Url.ToString();
                    recoveryLink = recoveryLink.TrimStart('{').TrimEnd('}');
                    recoveryLink = recoveryLink.Replace("passwordforgot.aspx", "confirmpassword.aspx");
                    //recoveryLink = recoveryLink + "?userId=" + UserId;
                    recoveryLink = recoveryLink + "?UserId=" + UserId;
                    if (recoveryLink.Contains("122.179.131.228") == true)
                    {
                        recoveryLink = recoveryLink.Replace("122.179.131.228", "122.179.131.228:8084");
                    }
                    else if (recoveryLink.Contains("121.247.65.205") == true)
                    {
                        recoveryLink = recoveryLink.Replace("121.247.65.205", "121.247.65.205:8084");
                    }
                    string firstName = getFirstNameByUserName(UserName);
                    StringBuilder strBody = new StringBuilder();
                    strBody.Append("<font face='Arial' size='2' color='#006666'><p>Hi ");
                    strBody.Append(firstName);
                    strBody.Append(",");
                    strBody.Append("</p>");
                    strBody.Append("<p>&nbsp;</p><p>We received a request to reset the password associated with this e-mail address.</p><p>");
                    strBody.Append("If you made this request, click the link below to reset your password using our secure server:</p><p>&nbsp;</p>");
                    strBody.Append(" <p><a href='" + recoveryLink + "' moz-do-not-send='true'>" + recoveryLink + "</a></p>");
                    strBody.Append("<p>&nbsp;</p><p>Alternately, you can copy and paste the link into your browser&#39;s address window, or retype it there.&nbsp;</p>");
                    strBody.Append("<p>If you did not request to have your password reset you can safely ignore this email. Your account settings will remain unchanged.</p>");
                    strBody.Append("<p>&nbsp;</p><p>	Thanks,</p><p>	Contract Managment Team</p>");

                    strBody.Append("<br><br><font face='Arial' size='1' color='#006666'>Need help? Please do not hesitate to contact our technical support team on 0800 699 0045 or at help@contractpod.com.");
                    strBody.Append("<br>The information in this email is confidential and for use by the addressee(s) only. If you are not the intended recipient, please notify us immediately on 0800 699 0045 and delete the message from your computer. You may not copy or forward the e-mail, or use it or disclose its contents to any other person. We do not accept any liability or responsibility for changes made to this email after it was sent, or viruses transmitted through this e-mail or any attachment.</font>");
        
                    string Body = strBody.ToString();



                    SendMail objmail = new SendMail();
                    objmail.UserId = LoginUserId;
                    objmail.MailTo = MailTo;
                    objmail.Subject = "[contract Managment] - Password Reset Request";
                    objmail.Body = Body;
                    bool issend = objmail.SendEMail();
                    if (issend)
                    {
                        ChangeIsPasswordStatus(UserId);
                        message = "T";
                    }
                }
                catch (Exception ex)
                {
                    status = false;
                    message = ex.Message;
                }
            }
            else
            {
                message = "T";
                status = false;
            }

            return message;
        }

        //public bool sendPasswordMail()
        //{
        //    bool status = false;
        //    try
        //    {
        //        if (_EmailID != "")
        //        {

        //            string strURLWithData = GetUserInformation(_Username, _EmailID);

        //            string UserId = strURLWithData;

        //            string recoveryLink = HttpContext.Current.Request.Url.ToString();
        //            recoveryLink = recoveryLink.TrimStart('{').TrimEnd('}');
        //            recoveryLink = recoveryLink.Replace("AddUserService.asmx/AddNewUser", "Account/confirmpassword.aspx").Replace("AddUserService.asmx/AddUser", "Account/confirmpassword.aspx");

        //            recoveryLink = recoveryLink + "?UserId=" + UserId;
        //            if (recoveryLink.Contains("122.179.131.228") == true)
        //            {
        //                recoveryLink = recoveryLink.Replace("122.179.131.228", "122.179.131.228:8084");
        //            }
        //            else if (recoveryLink.Contains("121.247.65.205") == true)
        //            {
        //                recoveryLink = recoveryLink.Replace("121.247.65.205", "121.247.65.205:8084");
        //            }

        //            obj.UserName = _Username;
        //            string FullName = obj.GetUserFullName();
        //            StringBuilder strBody = new StringBuilder();
        //            strBody.Append("<font face='verdana' size=2><p>Hi ");
        //            strBody.Append(FullName);
        //            strBody.Append(",");
        //            strBody.Append("</p>");
        //            strBody.Append("<p> Your login details are created successfully with following UserName.<p>");
        //            strBody.Append("<p> UserName = " + _Username + " &nbsp;</p>");

        //            strBody.Append("If you made this request, click the below url to reset your password using our server: </p>");
        //            strBody.Append(" <p><a href='" + recoveryLink + "' moz-do-not-send='true'>" + recoveryLink + "</a></p>");
        //            strBody.Append("<p>Alternately, you can copy and paste the above url into your browser's address bar, or retype url in the browser's address bar.&nbsp;</p>");


        //            strBody.Append("<p>	Thanks,</p><p>	NeoGrowth Team. </p>");

        //            string Body = strBody.ToString();

        //            SendMail objmail = new SendMail();
        //            objmail.UserId = LoginUserId;
        //            objmail.MailTo = _EmailID;
        //            objmail.Subject = "[Contract Managment] - New user password set assistance";
        //            objmail.Body = Body;
        //            bool issend = objmail.SendEMail();
        //            if (issend)
        //            {
        //                ChangeIsPasswordStatus(UserId);
        //            }
        //            status = issend;

        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        status = false;
        //    }
        //    return status;
        //}


        //public bool sendPasswordMailAll()
        //{
        //    bool status = false;
        //    try
        //    {
        //        if (_EmailID != "")
        //        {

        //            string strURLWithData = GetUserInformation(_Username, _EmailID);

        //            string UserId = strURLWithData;

        //            string recoveryLink = HttpContext.Current.Request.Url.ToString();
        //            recoveryLink = recoveryLink.TrimStart('{').TrimEnd('}');
        //            recoveryLink = recoveryLink.Replace("AddUserService.asmx/UsersResetPassword", "Account/confirmpassword.aspx");

        //            recoveryLink = recoveryLink + "?UserId=" + UserId;
        //            if (recoveryLink.Contains("122.179.131.228") == true)
        //            {
        //                recoveryLink = recoveryLink.Replace("122.179.131.228", "122.179.131.228:8084");
        //            }
        //            else if (recoveryLink.Contains("121.247.65.205") == true)
        //            {
        //                recoveryLink = recoveryLink.Replace("121.247.65.205", "121.247.65.205:8084");
        //            }

        //            obj.UserName = _Username;
        //            string FullName = obj.GetUserFullName();

        //            StringBuilder strBody = new StringBuilder();
        //            strBody.Append("<font face='verdana' size=2><p>Hi ");
        //            strBody.Append(FullName);
        //            strBody.Append(",");
        //            strBody.Append("</p>");
        //            strBody.Append("<p>&nbsp;</p><p>We received a request to reset the password associated with this e-mail address.</p><p>");
        //            strBody.Append("If you made this request, click the link below to reset your password using our secure server:</p><p>&nbsp;</p>");
        //            strBody.Append(" <p><a href='" + recoveryLink + "' moz-do-not-send='true'>" + recoveryLink + "</a></p>");
        //            strBody.Append("<p>&nbsp;</p><p>Alternately, you can copy and paste the link into your browser&#39;s address window, or retype it there.&nbsp;</p>");
        //            strBody.Append("<p>If you did not request to have your password reset you can safely ignore this email. Your account settings will remain unchanged.</p>");
        //            strBody.Append("<p>&nbsp;</p><p>	Thanks,</p><p>	Contract Managment Team</p>");
        //            strBody.Append("<p><a href='' moz-do-not-send='true'>www.ContractManagment.in</a></p></font>");

        //            string Body = strBody.ToString();

        //            SendMail objmail = new SendMail();
        //            objmail.UserId = LoginUserId;
        //            objmail.MailTo = _EmailID;
        //            objmail.Subject = "[Contract Management] - Password Reset Assistance";
        //            objmail.Body = Body;
        //            bool issend = objmail.SendEMail();
        //            if (issend)
        //            {
        //                ChangeIsPasswordStatus(UserId);
        //            }
        //            status = issend;


        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        status = false;
        //    }
        //    return status;
        //}

        //public string EncryptQueryString(string strQueryString)
        //{
        //    /// EncryptDecryptQueryString objEDQueryString = new EncryptDecryptQueryString();
        //    return EncryptDecryptQueryString.Encrypt(strQueryString, "r0b1nr0y");
        //}

        private string GetUserInformation(string userName, string mailId)
        {
            //***************************************
            IUsers obj = FactoryUser.GetUsersDetail();
            obj.CreateDALObjForMembershipWork();
            //****************************************

            if (userName.Trim() == "")
            {
                userName = Membership.GetUserNameByEmail(mailId);

            }
            else if (mailId.Trim() == "")
            {
                mailId = getEmailIDByUserName(userName);
            }

            _Username = userName;
            _EmailID = mailId;
            string userId = "";
            Parameters.AddWithValue("@EmailId", mailId);
            Parameters.AddWithValue("@UserName", userName.ToLower());
            userId = getExcuteScalar("UsersGetUserInfoByMailId", 0);
            return userId;

        }

        private string getEmailIDByUserName(string userName)
        {
            return getExcuteScalar("UsersGetUserInfoByMailId'" + userName + "'");

        }
        private string getFirstNameByUserName(string userName)
        {
            return getExcuteScalar("MasterUserGetUserFirstName'" + userName + "'");

        }
        private void ChangeIsPasswordStatus(string userId)
        {
            Parameters.AddWithValue("@UserId", userId);
            getExcuteScalar("UsersChangeUserPasswordStatus", 0);
        }

        public string PasswordRequestUserName(string userId)
        {
            string val = "";
            Parameters.AddWithValue("@UserId", userId);
            val = getExcuteScalar("UsersUserPasswordRequest", 0);
            return val;
        }

        public void PasswordChangedSuccessfully(string userId)
        {
            Parameters.AddWithValue("@UserId", userId);
            getExcuteScalar("UsersUserPasswordChanged", 0);
        }



    }

}

