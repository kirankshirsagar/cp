﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using System.Web;
using System.Data;
using System.Web.Security;
using MailBee;
using MailBee.Mime;
using MailBee.SmtpMail;
using System.Data.SqlClient;
using CommonBLL;


namespace EmailBLL
{
    public class SendMail : DAL
    {
        public string ReplyTo { get; set; }
        public string MailFrom { get; set; }
        public string MailTo { get; set; }
        public string MailBCC { get; set; }
        public string MailCc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string DisplayName { get; set; }
        public List<String> Attachment { get; set; }

        public string Mode { get; set; }
        public int RequestId { get; set; }
        public int EmailTriggerId { get; set; }	
        public int EmailTemplateId { get; set; }	
        public string UserId { get; set; }	
	    public string ErrorDescription { get; set; }
        public Nullable<DateTime> AlertDate { get; set; }

        private char isSent { get; set; }
        public int ActivityId { get; set; }

        public SendMail()
        {
            MailFrom = ConfigurationSettings.AppSettings["MailFrom"];
            MailBCC = ConfigurationSettings.AppSettings["MailBCC"]; 
            DisplayName = ConfigurationSettings.AppSettings["DisplayName"];
            Attachment = new List<String>();
          
        }

        public bool SendEMail()
        {
            string MailBeeUsed = "N";
            try
            {
                MailBeeUsed = ConfigurationSettings.AppSettings["MailBeeUsed"];
            }
            catch {
                MailBeeUsed = "N";
            }

            if (MailBeeUsed == "Y")
                return SendMailBeeEMail();
            else
                return SendSimpleMail();
        }

        public bool SendSimpleMail()
        {
            bool status = false;
            try
            {
                System.Net.Mail.MailMessage objEmail = new System.Net.Mail.MailMessage();
                string mailServer = System.Configuration.ConfigurationSettings.AppSettings["mailserver"];
                string usernm = System.Configuration.ConfigurationSettings.AppSettings["UserName"];
                string Password = System.Configuration.ConfigurationSettings.AppSettings["Password"];
                Int32 Port = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["Port"]);

                SmtpClient objSC = new SmtpClient(mailServer, Port);
                NetworkCredential cre = new NetworkCredential(usernm, Password);
                objSC.UseDefaultCredentials = false;
                objSC.Credentials = cre;
                objEmail.From = new System.Net.Mail.MailAddress(MailFrom, DisplayName);
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(Body, null, "text/html");
                try
                {
                    //if (!string.IsNullOrEmpty(MailTo))
                    {
                        objEmail.To.Clear();
                        if (!String.IsNullOrEmpty(MailTo))
                            objEmail.To.Add(MailTo);
                        if (MailBCC != "" && MailBCC != null)
                        {
                            objEmail.Bcc.Add(MailBCC);
                        }
                        if (MailCc != "" && MailCc != null)
                        {
                            objEmail.CC.Add(MailCc);
                        }
                        else
                            MailCc = MailFrom;
                        objEmail.Subject = Subject;
                        // objEmail.Body = Body;
                        objEmail.AlternateViews.Add(htmlView);
                       
                        if (Attachment != null)
                        {
                            for (int i = 0; i < Attachment.Count; i++)
                            {
                                System.Net.Mail.Attachment items = new System.Net.Mail.Attachment(Attachment[i]);
                                objEmail.Attachments.Add(items);
                            }
                        }
                        if (!string.IsNullOrEmpty(ReplyTo))
                        {
                            objEmail.ReplyTo = new MailAddress(ReplyTo);
                        }
                        objEmail.IsBodyHtml = true;
                        objSC.Send(objEmail);
                        status = true;
                        if (status)
                            isSent = 'Y';                        
                        EmailSentLog();
                    }
                }
                catch (FormatException ex)
                {
                    status = false;
                    isSent = 'N';
                    EmailSentLog();
                }
                catch (SmtpException ex)
                {
                    status = false;
                    isSent = 'N';
                    EmailSentLog();
                }
                finally
                {
                    // conn.Close();
                }
            }
            catch (Exception ex)
            {
                status = false;
                isSent = 'N';
            }


            return status;
        }

        public bool SendMailBeeEMail()
        {
            bool status = false;
            try
            {

                MailBee.Global.LicenseKey = System.Configuration.ConfigurationSettings.AppSettings["MailBeeLienceKey"];
                string mailserver = System.Configuration.ConfigurationSettings.AppSettings["mailserver"];
                string username = System.Configuration.ConfigurationSettings.AppSettings["UserName"];
                string passowrd = System.Configuration.ConfigurationSettings.AppSettings["Password"];

                //   MailBee.Global.LicenseKey = "MN700-EC242411248C24E32489203B36B7-D24B";
                Smtp mailer = new Smtp();

                mailer.SmtpServers.Add(mailserver, username, passowrd);

                mailer.Message.From.AsString = MailFrom;
                mailer.Message.From.DisplayName = DisplayName;
                mailer.Message.To.AsString = MailTo;
                if (MailCc != "" && MailCc != null)
                {
                    mailer.Message.Cc.AsString = MailCc;
                }
                else
                    MailCc = MailFrom;
                if (MailBCC != "" && MailBCC != null)
                {
                    mailer.Message.Bcc.AsString = MailBCC;
                }
               
                mailer.Message.Subject = Subject;
                mailer.Message.BodyHtmlText = Body;
                if (!string.IsNullOrEmpty(ReplyTo))
                {
                    mailer.Message.ReplyTo.AsString = ReplyTo;
                   
                }
                if (Attachment != null)
                {
                    for (int i = 0; i < Attachment.Count; i++)
                    {

                        mailer.AddAttachment(Attachment[i]);
                    }
                }
                mailer.SubmitToPickupFolder("C:\\MailBeeNetQueue Files\\Pickup", false);

                status = true;
                if (status)
                    isSent = 'Y';               
                EmailSentLog();
            }
            catch (Exception ex)
            {
                status = false;
                isSent = 'N';
                EmailSentLog();
            }


            return status;
        }

        protected void EmailSentLog()
        {
            string status = "0";
            string Attach = null;
            if (Attachment.Count > 0)
            {
                for (int i = 0; i < Attachment.Count; i++)
                {
                    string a = Attachment[i].Substring(Attachment[i].LastIndexOf(@"\") + 1);
                    if (Attach == null)
                        Attach = a;
                    else
                        Attach += "," + a;
                }
                Attach.TrimEnd(',');
            }
            
            Parameters.Clear();
            Parameters.AddWithValue("@RequestId", RequestId);
            Parameters.AddWithValue("@Mode", Mode);
            Parameters.AddWithValue("@EmailTriggerId", EmailTriggerId);
            Parameters.AddWithValue("@EmailTemplateId", EmailTemplateId);
            Parameters.AddWithValue("@SentBy", UserId);
            Parameters.AddWithValue("@EmailFrom", MailFrom);
            Parameters.AddWithValue("@EmailTo", MailTo);
            Parameters.AddWithValue("@EmailCC", MailCc);
            Parameters.AddWithValue("@EmailBCC", MailBCC);
            Parameters.AddWithValue("@EmailAttachments", Attach);
            Parameters.AddWithValue("@EmailSubject", Subject);
            Parameters.AddWithValue("@EmailBody", Body);
            Parameters.AddWithValue("@isSent", isSent);
            Parameters.AddWithValue("@ErrorDescription", ErrorDescription);
            Parameters.AddWithValue("@AlertDate", AlertDate);
            Parameters.AddWithValue("@ActivityId", ActivityId);
            try
            {
                status = getExcuteScalar("EmailSentLogAdd");               

            }
            catch (Exception ex)
            {
                status = "0";
            }
            finally
            {

            }
        }
    }

}

