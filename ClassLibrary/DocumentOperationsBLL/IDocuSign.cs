﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClauseLibraryBLL.DocuSignAPI;

namespace DocumentOperationsBLL
{
    public interface IDocuSign
    {

       int RequestId{get;set;}
       int ContractID{get;set;}
       int ContractFileId{get;set;}
       string Name{get;set;}
       string Email{get;set;}
       string EnvelopeID{get;set;}
       string SignedCopyFileName{get;set;}
       string Status{get;set;}
       string SendDate{get;set;}
       string ReceivedDate{get;set;}
       string Addedon{get;set;}
       int Addedby{get;set;}
       string Modifiedon{get;set;}
       int Modifiedby{get;set;}
       string IP { get; set; }
       string DocStatus { get; set; }

       string SaveDetails();
       string UpdateDetails();
       void ReadDocusignDetails();
       string ReadEnvelopeId();
       APIServiceSoapClient CreateAPIProxy();
       List<DocuSign> DocumentSignatureDetails{get;set;}



    }
}
