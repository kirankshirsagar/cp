﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonBLL;
using CrudBLL;
using System.Data;
using System.Data.SqlClient;

namespace DocumentOperationsBLL
{
    public interface IContractProcedures : ICrud
    {
        int RequestId { get; set; }
        int ContractId { get; set; }
        int ContractTemplateId { get; set; }
        string ModifiedByUserName { get; set; }
        string ModifiedOnFormattedDate { get; set; }
        string StageName { get; set; }

        char isPDFFile { get; set; }
        char isUploaded { get; set; }
        char IsCheckOutDone { get; set; }
        char IsMailSent { get; set; }
        int ContractFileId { get; set; }
        int ContractFileActivityTypeId { get; set; }
        string ContractFileActivityDetails { get; set; }
        string FileName { get; set; }
        string FileSizeKB { get; set; }
        string MailSubject { get; set; }
        string MailBody { get; set; }
        string EmailId { get; set; }
        string linkToApproval { get; set; }
        int AssignTo { get; set; }
        int StageId { get; set; }

        int ContractApprovalId { get; set; }
        char IsApproved { get; set; }
        char IsApprovalDone { get; set; }
        char IsActive { get; set; }
        string Remark { get; set; }
        string Condition { get; set; }
        string Answer { get; set; }
        char IsValidDocument { get; set; }
        string IsOCR { get; set; }
        string Approvaluri { get; set; }

        #region Request Document
         int ContractRequestDocumentId { get; set; }
         string DocumentTypeName { get; set; }
         string OrigionalFileName { get; set; }
         string ActuallFileName { get; set; }
         string FullName { get; set; }
         string AddedDate { get; set; }         
        #endregion

        int AddedBy { get; set; }
        int ModifiedBy { get; set; }
        string IpAddress { get; set; }
        DateTime AddedOn { get; set; }
        DateTime ModifiedOn { get; set; }
        string Description { get; set; }
        string ContractRequestDocumentIds { get; set; }
        string isFromDocuSign { get; set; }
        string GUID { get; set; }
        string AssignedToUserName { get; set; }
        char IsFromAI { get; set; }

        List<ContractProcedures> StageDetails { get; set; }
        List<ContractProcedures> StageApprovalActivity { get; set; }
        List<ContractProcedures> StagesPendingForApproval { get; set; }

        List<ContractProcedures> ContractFiles { get; set; }
        List<ContractProcedures> ContractFileActivity { get; set; }

        string ApprovalActivtiyDetails { get; set; }

        string DeleteContractFile();
        List<ContractProcedures> RequestDocumentDetails();
        string FileActivityInsertRecord();
       
    }
}
