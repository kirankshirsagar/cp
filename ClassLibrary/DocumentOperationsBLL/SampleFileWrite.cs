﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using System.Reflection;
using aspose = Aspose.Words;

public static class SampleFileWrite
{
    public static int CreateSampleDoc()
    {

        object template = Missing.Value; //No template.
        object newTemplate = Missing.Value; //Not creating a template.
        object documentType = Missing.Value; //Plain old text document.
        object visible = true;  //Show the doc while we work.

        aspose.Document doc = new aspose.Document();

        //doc.Variables.Add("name", "Id");
        //doc.Variables.Add("name_Type", "TEXT"); //options: Text, multiline, Date, 
        //                                        //Single Select With Values(SingleValues), Single Select With Masters(SingleValues), 
        //                                        //Multi Select With Values(MultiValues), Multi Select With Masters(MultiMasters)
        //                                        //Checkbox with Values(CBValues), Checkbox with masters(CBMasters), 
        //                                        //Radio button with values(RBValues), Checkbox with Masters(RBMasters), 
        //doc.Variables.Add("name_TextType", "All"); //options: All   Alphanumeric   Letters   Numbers   Decimal
        //doc.Variables.Add("name_DefaultValue", "");//depend upon Type(not TextType)
        //doc.Variables.Add("name_DefaultSize", "20");//number of characters depend upon Type(not TextType)
        //doc.Variables.Add("name_DefaultDate", "");// If Type is Date 
        //doc.Variables.Add("name_Required", "false");
        //doc.Variables.Add("name_Question", "Client's Name");
        //doc.Variables.Add("name_Options", "India\nPakistan\nUSA\nSouth Africa\nSolomon Islands");//if Type other than Text, Multiline text, Date
        //doc.Variables.Add("name_Masters", "TableName");//if Type isselection from masters
        //doc.Variables.Add("name_Bubble", "Help Text");

        doc.Variables.Add("name", "Id");
        doc.Variables.Add("name_DisplayName", "Name");
        doc.Variables.Add("name_Type", "TEXT");
        doc.Variables.Add("name_TextType", "All");
        doc.Variables.Add("name_DefaultValue", "All value textbox");
        doc.Variables.Add("name_Question", "What is your name?");

        doc.Variables.Add("dob", "Id");
        doc.Variables.Add("name_DisplayName", "Name");
        doc.Variables.Add("dob_Type", "Date");
        doc.Variables.Add("dob_Bubble", "Date");
        doc.Variables.Add("dob_Required", "false");
        doc.Variables.Add("dob_Question", "Date of Birth");
        doc.Variables.Add("dob_DefaultDate", "01/01/1930");

        doc.Variables.Add("address", "Id");
        doc.Variables.Add("address_Bubble", "Address");
        doc.Variables.Add("address_Type", "multiline");
        doc.Variables.Add("address_Required", "false");
        doc.Variables.Add("address_Question", "Address");

        doc.Variables.Add("contact", "Id");
        doc.Variables.Add("contact_Bubble", "Contact Number");
        doc.Variables.Add("contact_Type", "Text");
        doc.Variables.Add("contact_TextType", "Numbers");
        doc.Variables.Add("contact_Required", "false");
        doc.Variables.Add("contact_Question", "Phone Number");

        doc.Variables.Add("maritalStatus", "Id");
        doc.Variables.Add("maritalStatus_Type", "RBValues");
        doc.Variables.Add("maritalStatus_Required", "false");
        doc.Variables.Add("maritalStatus_Question", "Marital Status");
        doc.Variables.Add("maritalStatus_Options", "Single\nMarried");
        doc.Variables.Add("maritalStatus_DefaultValue", "Single");

        doc.Variables.Add("nationality", "Id");
        doc.Variables.Add("nationality_Type", "SingleValues");
        doc.Variables.Add("nationality_Required", "true");
        doc.Variables.Add("nationality_Question", "Nationality");
        doc.Variables.Add("nationality_Options", "India\nPakistan\nUSA\nSouth Africa\nSolomon Islands");

        aspose.DocumentBuilder docBuilder = new aspose.DocumentBuilder(doc);
        docBuilder.InsertParagraph();
        docBuilder.InsertHtml("My Name is <label id='name'>##Name## </label>" +
                              "<br clear=all style='mso-special-character:line-break;page-break-before:always'> " +
                              "My date of birth is <label id='dob'> ##Name## </lable>\n" +
                              "I stay at ##address## \n" +
                              "Married ##married## \n" +
                              "Contact me on ##contact## \n" +
                              "Nationality ##nationality## \n"
                         );

        //System.Web.UI.WebControls.Panel pnl = Credentials.GenerateQuetionnair(doc.Variables);
        //Save the file, use default values except for filename.
        //object fileName = Environment.CurrentDirectory + "\\example2_new";
        string fileName = @"C:\Users\usil-22\Desktop\example2_new.docx";

        doc.Save(fileName, aspose.SaveFormat.Docx);
        return 0;
    }
}