﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.IO;
using System.Text;

using System.Reflection;
using AsposeWord = Aspose.Words;

using System.Web.UI;
using System.Text.RegularExpressions;
using CommonBLL;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using MasterBLL;

namespace DocumentOperationsBLL
{
    public class Credentials : DAL
    {
        public string IP = "";
        public int UserId = 0;
        public int RequestId;
        public int ContractId;

        static string flid = "", flvid = "";
        static int INDEX = 0;
        public Credentials()
        {
            INDEX = 0;
            Aspose.Words.License license = new Aspose.Words.License();
            license.SetLicense("Aspose.Words.lic");
        }

        public XmlDocument GetXML(AsposeWord.VariableCollection WordVar)
        {
            XmlDocument Docvariables = new XmlDocument();

            XmlNode xmlNode = Docvariables.CreateNode(XmlNodeType.XmlDeclaration, "nm", "uri");
            XmlElement rootElement = Docvariables.CreateElement("Variables");
            Docvariables.AppendChild(rootElement);

            XmlElement rootElement1 = Docvariables.CreateElement("Tables");
            //Docvariables.AppendChild(rootElement1);

            XmlElement Element = null;
            foreach (DictionaryEntry variable in WordVar)
            {
                string KEY = "", VALUE = "";
                KEY = variable.Key.ToString();
                VALUE = variable.Value.ToString();
                try
                {
                    if ((KEY.IndexOf("_") == -1 && VALUE.ToUpper() == "ID"))// && !KEY.ToUpper().StartsWith("CLAUSEID_"))
                    {
                        XmlElement element = Docvariables.CreateElement("Variable");
                        XmlElement element1 = Docvariables.CreateElement("Table");
                        XmlAttribute VarAttribute = Docvariables.CreateAttribute(VariableCollection.Id);
                        VarAttribute.Value = KEY.Split('_')[0].Replace(" ", string.Empty).Replace(":", string.Empty);

                        XmlAttribute VarAttributeFieldName = Docvariables.CreateAttribute(VariableCollection.FieldName);
                        VarAttributeFieldName.Value = KEY.Split('_')[0];

                        Element = element;
                        element.Attributes.Append(VarAttribute);
                        element.Attributes.Append(VarAttributeFieldName);
                        Docvariables.DocumentElement.PrependChild(element);
                        Docvariables.ChildNodes.Item(0).AppendChild(element);
                    }
                    //else if (KEY.StartsWith("ClauseID_"))
                    //{
                    //    XmlElement element = Docvariables.CreateElement("Variable");
                    //    Element = Docvariables.CreateElement(KEY);
                    //}
                    else
                    {
                        XmlElement element;
                        if (KEY.StartsWith("ClauseID_"))
                        {
                            element = Docvariables.CreateElement(KEY);
                        }
                        else
                        {
                            element = Docvariables.CreateElement(KEY.Contains('_') ? KEY.Split('_')[1] : KEY);
                        }
                        //element = Docvariables.CreateElement(KEY.Contains('_') ? KEY.Split('_')[1] : KEY);
                        XmlText installationIdText = Docvariables.CreateTextNode(VALUE);
                        element.AppendChild(installationIdText);
                        if (Element == null)
                            Element = Docvariables.CreateElement("Variable");

                        if (KEY.Contains('_') && !KEY.StartsWith("ClauseID_"))
                        {
                            XmlNode xmlEle = element.OwnerDocument.DocumentElement.SelectSingleNode("Variable [@FieldName='" + KEY.Split('_')[0] + "']");
                            xmlEle.PrependChild(element);
                        }
                        else
                        {
                            Element.PrependChild(element);
                            //Docvariables.DocumentElement.PrependChild(Element);
                            //Docvariables.ChildNodes.Item(0).AppendChild(Element);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }            
            return Docvariables;
        }

        public HtmlTableRow DesignHtmlRow(XmlElement ele, string AttachedToFieldValue, int option = 0)
        {
            HtmlTableRow tr = null;
            string val = ele.Attributes.GetNamedItem(VariableCollection.Id).Value;
            string ID = "", FieldName = "", question = "", FieldTypeId = "", ToolTip = "", Answer = "", ContractQAId = "";
            int MaxLength = 0;

            string AttachedToField = ele.Attributes["AttachedToField"].Value;
            string AttachedToValue = AttachedToFieldValue;// ele.Attributes["AttachedToValue"].Value;


            flid = AttachedToField;
            flvid = AttachedToValue;
            tr = new HtmlTableRow();
            if (AttachedToField != "0" && AttachedToValue != "0")
                tr.Attributes.Add("class", "AttachedToField" + AttachedToField + " AttachedToValue" + AttachedToValue + " IsConditional");
            else tr.Attributes.Add("class", "AttachedToField" + AttachedToField + " AttachedToValue" + AttachedToValue);

            if (AttachedToField != "0")
            {
                tr.BgColor = "lightgray";
                //tr.Style.Add("display", "none");
            }

            bool IsConditional = tr.Attributes["class"].Contains("IsConditional");
            //VarControlType = ele.GetElementsByTagName(VariableCollection.Type)[0].InnerText;
            if (ele.GetElementsByTagName(VariableCollection.FieldTypeId)[0] != null)
            {
                FieldTypeId = ele.GetElementsByTagName(VariableCollection.FieldTypeId)[0].InnerText;
                question = ele.GetElementsByTagName(VariableCollection.Question)[0] == null ? "" : ele.GetElementsByTagName(VariableCollection.Question)[0].InnerText;
                question = question.Replace("_x000a_", "<br/>");
                ToolTip = ele.GetElementsByTagName(VariableCollection.ToolTip)[0] == null ? "" : ele.GetElementsByTagName(VariableCollection.ToolTip)[0].InnerText;
                INDEX += 1;
                ID = ele.Attributes[0].Value + "_" + INDEX;
                FieldName = ele.Attributes[1].Value;
                Answer = ele.GetElementsByTagName("Answer")[0] == null ? "" : ele.GetElementsByTagName("Answer")[0].InnerText;
                ContractQAId = ele.GetElementsByTagName("ContractQAId")[0] == null ? "" : ele.GetElementsByTagName("ContractQAId")[0].InnerText;

                var FieldLibraryId = ele.GetElementsByTagName(VariableCollection.FieldLibraryId)[0].InnerText;

                tr.Attributes["class"] = tr.Attributes["class"] + " TR_" + ele.Attributes[0].Value;
                HtmlTableCell tdQuestion = new HtmlTableCell();
                tdQuestion.Width = "35%";
                tdQuestion.Align = "right";
                tdQuestion.InnerHtml = "" + question + "";
                tdQuestion.Style.Add("vertical-align", "top");
                tdQuestion.Attributes.Add("class", "text-tab");
                tr.Cells.Add(tdQuestion);

                HtmlTableCell tdSeparator = new HtmlTableCell();
                tdSeparator.InnerText = ":";
                tdSeparator.Width = "1%";
                tdSeparator.Style.Add("vertical-align", "top");
                tr.Cells.Add(tdSeparator);

                HtmlTableCell tdInput = new HtmlTableCell();
                tdInput.Width = "65%";
                tdInput.Style.Add("vertical-align", "top");

                #region Create Controls
                string[] options = { };
                switch (FieldTypeId.ToUpper())
                {
                    case VariableCollection.TEXT:
                    case VariableCollection.ALPHANUMERIC:
                    case VariableCollection.NUMBERS:
                    case VariableCollection.CURRENCY:
                    case VariableCollection.LETTER:
                    case VariableCollection.DATE:
                        HtmlInputText txt = new HtmlInputText();
                        txt.ID = ID;
                        txt.ClientIDMode = ClientIDMode.Static;
                        txt.Style.Add("width", "350px");
                        if (ele.GetElementsByTagName(VariableCollection.DefaultSize)[0] != null && FieldTypeId != VariableCollection.DATE)
                        {
                            MaxLength = Convert.ToInt32(ele.GetElementsByTagName(VariableCollection.DefaultSize)[0].InnerText);
                            txt.MaxLength = MaxLength;
                        }
                        txt.Attributes.Add("class", IsConditional ? "required conditional pasteBlock" : "required pasteBlock");
                        txt.Attributes.Add("FTID", FieldTypeId);
                        txt.Attributes.Add("FLID", FieldLibraryId);
                        txt.Attributes.Add("ContractQAID", ContractQAId);
                        txt.Attributes.Add("IsUpdated", "N");
                        txt.Attributes.Add("onchange", "setUpdated(this)");

                        if (FieldTypeId == VariableCollection.ALPHANUMERIC)
                            txt.Attributes.Add("class", IsConditional ? "ValOnlyNumChar required conditional pasteBlock" : "ValOnlyNumChar required pasteBlock");
                        if (FieldTypeId == VariableCollection.NUMBERS)
                            txt.Attributes.Add("class", IsConditional ? "int required conditional pasteBlock" : "int required pasteBlock");
                        if (FieldTypeId == VariableCollection.CURRENCY)
                            txt.Attributes.Add("class", IsConditional ? "Currency required conditional pasteBlock" : "Currency required pasteBlock");
                        if (FieldTypeId == VariableCollection.LETTER)
                            txt.Attributes.Add("class", IsConditional ? "ValOnlyChar required conditional pasteBlock" : "ValOnlyChar required pasteBlock");
                        if (FieldTypeId == VariableCollection.DATE)
                        {
                            txt.Attributes.Add("class", IsConditional ? "datepicker required conditional pasteBlock" : "datepicker required pasteBlock");
                            txt.Style.Add("width", "100px");
                            txt.Attributes.Add("readonly", "true");
                        }
                        txt.Value = Answer;
                        tdInput.Controls.Add(txt);
                        break;

                    case VariableCollection.MULTILINE:
                        HtmlTextArea multiline = new HtmlTextArea();
                        multiline.ClientIDMode = ClientIDMode.Static;
                        multiline.ID = ID;
                        multiline.Rows = 2;
                        multiline.Cols = 20;
                        multiline.Style.Add("width", "350px");
                        //multiline.Style.Add("height", "70px");
                        multiline.Style.Add("overflow", "hidden");
                        multiline.Attributes.Add("class", IsConditional ? "required conditional pasteBlock" : "required pasteBlock");
                        multiline.Attributes.Add("FTID", FieldTypeId);
                        multiline.Attributes.Add("FLID", FieldLibraryId.ToString());
                        multiline.Attributes.Add("ContractQAID", ContractQAId);
                        multiline.Value = Answer;
                        multiline.Attributes.Add("IsUpdated", "N");
                        multiline.Attributes.Add("onchange", "setUpdated(this)");

                        if (ele.GetElementsByTagName(VariableCollection.DefaultSize)[0] != null)
                        {
                            MaxLength = Convert.ToInt32(ele.GetElementsByTagName(VariableCollection.DefaultSize)[0].InnerText);
                            multiline.Attributes.Add("MaxLength", MaxLength.ToString());
                        }
                        tdInput.Controls.Add(multiline);
                        break;

                    case VariableCollection.CBVALUES:
                    case VariableCollection.CBMASTERS:
                        System.Web.UI.WebControls.CheckBoxList cbList = new System.Web.UI.WebControls.CheckBoxList();
                        cbList.ClientIDMode = ClientIDMode.Static;
                        //cbList.RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Vertical;
                        //cbList.RepeatLayout = System.Web.UI.WebControls.RepeatLayout.Flow;
                        cbList.RepeatColumns = Convert.ToInt32(VariableCollection.RepeatColumns);
                        cbList.ID = ID;
                        cbList.Attributes.Add("class", IsConditional ? "required conditional" : "required");
                        cbList.Attributes.Add("FTID", FieldTypeId);
                        cbList.Attributes.Add("FLID", FieldLibraryId.ToString());
                        cbList.Attributes.Add("ContractQAID", ContractQAId);
                        cbList.Attributes.Add("IsUpdated", "N");
                        cbList.Attributes.Add("onchange", "setUpdated(this)");

                        cbList.DataSource = FillMasterData(ele.GetElementsByTagName(VariableCollection.Master)[0].InnerText, FieldTypeId);
                        cbList.DataTextField = "Name";
                        cbList.DataValueField = "Id";
                        cbList.DataBind();

                        tdInput.Controls.Add(cbList);
                        break;

                    case VariableCollection.RBVALUES:
                    case VariableCollection.RBMASTERS:
                        System.Web.UI.WebControls.RadioButtonList rdoList = new System.Web.UI.WebControls.RadioButtonList();
                        rdoList.ClientIDMode = ClientIDMode.Static;
                        //rdoList.RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Vertical;
                        rdoList.RepeatColumns = Convert.ToInt32(VariableCollection.RepeatColumns);
                        //rdoList.RepeatLayout = System.Web.UI.WebControls.RepeatLayout.Flow;
                        rdoList.ID = ID;
                        rdoList.Attributes.Add("class", IsConditional ? "required conditional" : "required");
                        rdoList.Attributes.Add("FTID", FieldTypeId);
                        rdoList.Attributes.Add("FLID", FieldLibraryId.ToString());
                        rdoList.Attributes.Add("ContractQAID", ContractQAId);
                        rdoList.Attributes.Add("IsUpdated", "N");
                        rdoList.Attributes.Add("onchange", "setUpdated(this)");

                        rdoList.DataSource = FillMasterData(ele.GetElementsByTagName(VariableCollection.Master)[0].InnerText, FieldTypeId);
                        rdoList.DataTextField = "Name";
                        rdoList.DataValueField = "Id";
                        rdoList.DataBind();

                        tdInput.Controls.Add(rdoList);
                        break;

                    case VariableCollection.CONDITIONALVALUES:
                    case VariableCollection.SINGLEVALUES:
                    case VariableCollection.MULTIVALUES:
                    case VariableCollection.SINGLEMASTERS:
                    case VariableCollection.MULTIMASTERS:
                        string MasterTable = ele.GetElementsByTagName(VariableCollection.Master)[0].InnerText;

                        System.Web.UI.HtmlControls.HtmlInputHidden hdnField = new System.Web.UI.HtmlControls.HtmlInputHidden();                        
                        hdnField.ClientIDMode = ClientIDMode.Static;
                        hdnField.ID = "hdn" + ID;
                        hdnField.Attributes.Add("FTID", FieldTypeId);
                        hdnField.Attributes.Add("FLID", FieldLibraryId.ToString());
                        hdnField.Attributes.Add("ContractQAID", ContractQAId);
                        hdnField.Attributes.Add("IsUpdated", "N");
                        hdnField.Attributes.Add("onchange", "setUpdated(this)");

                        HtmlSelect drpList = new HtmlSelect();
                        drpList.ClientIDMode = ClientIDMode.Static;
                        drpList.ID = ID;
                        drpList.Style.Add("width", "300px");
                        drpList.Attributes.Add("class", IsConditional ? "required chzn-select conditional" : "required chzn-select");
                        drpList.Attributes.Add("FTID", FieldTypeId);
                        drpList.Attributes.Add("FLID", FieldLibraryId.ToString());
                        drpList.Attributes.Add("ContractQAID", ContractQAId);
                        drpList.Attributes.Add("IsUpdated", "N");
                        drpList.Attributes.Add("onchange", "setUpdated(this)");
                        if (FieldTypeId == VariableCollection.SINGLEVALUES || FieldTypeId == VariableCollection.CONDITIONALVALUES)//&& option == 1
                            drpList.Attributes.Add("onchange", "ShowHideRow(this)");

                        if (FieldTypeId == VariableCollection.SINGLEMASTERS && MasterTable == "MstCity")
                        {
                            drpList.Attributes.Add("Master", MasterTable);
                            hdnField.Attributes.Add("Master", MasterTable);
                        }
                        else
                        {
                            drpList.Attributes.Add("Master", "");
                            hdnField.Attributes.Add("Master", "");
                        }

                        if (ele.GetElementsByTagName(VariableCollection.Master)[0] != null && !(FieldTypeId == VariableCollection.SINGLEMASTERS && MasterTable == "MstCity"))
                        {
                            drpList.DataSource = FillMasterData(MasterTable, FieldTypeId);
                            drpList.DataTextField = "Name";
                            drpList.DataValueField = "Id";
                            drpList.DataBind();
                        }
                        if (FieldTypeId == VariableCollection.MULTIMASTERS || FieldTypeId == VariableCollection.MULTIVALUES)
                        {
                            drpList.Multiple = true;
                            drpList.Size = Convert.ToInt32(VariableCollection.ListSize);
                        }
                        string[] s = Answer.TrimEnd(',').Split(new char[] { ',' });
                        hdnField.Value = Answer;
                        foreach (string lst in s)
                        {
                            if (!string.IsNullOrEmpty(lst) && drpList.Items.FindByValue(lst) != null)
                                drpList.Items.FindByValue(lst).Selected = true;
                        }
                        //drpList.Value = Answer.TrimEnd(',');
                        tdInput.Controls.Add(drpList);
                        tdInput.Controls.Add(hdnField);                        
                        break;

                    case VariableCollection.TABLES:
                        string TableMaster = ele.GetElementsByTagName(VariableCollection.Master)[0].InnerText;
                        HtmlTable htbl = new HtmlTable();
                        htbl.Attributes.Add("Style", "border:2px solid #ccc");
                        List<SelectControlFields> lstRowColumn = FillMasterData(TableMaster, FieldTypeId);
                        if (lstRowColumn.Count > 0)
                        {
                           int NoRows=int.Parse(lstRowColumn[1].Name);
                           int NoColumns = int.Parse(lstRowColumn[2].Name);

                           //int totalObjects = rivers.Count;
                           int totalCells = NoRows * NoColumns;
                           HtmlTableRow newRow = new HtmlTableRow();

                           for (int i = 0; i < NoRows; i++)
                           {
                               HtmlTableRow rowNew = new HtmlTableRow();
                               //rowNew.Attributes.Add("border", "solid red 1px");
                               htbl.Controls.Add(rowNew);
                               for (int j = 0; j < NoColumns; j++)
                               {
                                   HtmlTableCell cellNew = new HtmlTableCell();
                                   cellNew.Attributes.Add("Style", "border:1px solid #ccc");
                                   Label lblNew = new Label();
                                   lblNew.Text = "(" + i.ToString() + "," + j.ToString() + ")<br />";

                                   System.Web.UI.WebControls.Image imgNew = new System.Web.UI.WebControls.Image();
                                   imgNew.ImageUrl = "cellpic.png";

                                   cellNew.Controls.Add(lblNew);
                                   cellNew.Controls.Add(imgNew);

                                   ////if (chkBorder.Checked == true)
                                   ////{
                                   //cellNew.BorderStyle = BorderStyle.Inset;
                                   //cellNew.BorderWidth = Unit.Pixel(1);
                                   ////}

                                   rowNew.Controls.Add(cellNew);
                               }
                           }

                           //for (int i = 0; i < totalCells; i++)
                           //{
                           //    // make a new row when you get the desired number of columns
                           //    // skip the first, empty row
                           //    if (i % NoColumns == 0 && i != 0)
                           //    {
                           //        htbl.Rows.Add(newRow);
                           //        newRow = new HtmlTableRow();
                           //    }
                           //    //// keep putting in cells
                           //    //if (i < totalObjects)
                           //    //    row.Cells.Add(new HtmlTableCell(rivers[i]));
                           //    //// if we have no more objects, put in empty cells to fill out the table
                           //    //else
                           //    //    row.Cells.Add(new HtmlTableCell(""));
                           //}

                           tdInput.Controls.Add(htbl);
                        }

                        break;

                }
                tr.Cells.Add(tdInput);

                #endregion
            }
            return tr;
        }

        public Panel GenerateQuetionnair(string ContractTemplateId)
        {
            XmlDocument VariableDocument = new XmlDocument();
            FieldLibraryOperations fo = new FieldLibraryOperations();
            fo.ContractTemplateId = Convert.ToInt32(ContractTemplateId);
            fo.ContractId = ContractId;
            fo.RequestId = RequestId;
            fo.RequestMode = 0;
            VariableDocument.LoadXml(fo.GetQuestionnairXML());            
            //VariableDocument.Load("C:\\QuetionnaireXML.xml");
            //VariableDocument.Save("C:\\AsposeVariables.xml");

            System.Web.UI.WebControls.Panel pnl = new System.Web.UI.WebControls.Panel();
            pnl.ID = "pnlCtrl";

            if (VariableDocument != null)
            {
                XmlNodeList Variable = VariableDocument.GetElementsByTagName("Variable");

                HtmlTable tbl = new HtmlTable();
                tbl.ID = "ASPOSE_tbl";
                tbl.Attributes.Add("gen", "true");
                tbl.Width = "95%";
                tbl.Border = 0;
                tbl.Style.Add("table-layout", "fixed");
                tbl.Style.Add("text-align", "left");
                tbl.Attributes.Add("cellpadding", "5");
                tbl.Attributes.Add("cellspacing", "0");
                
                tbl.Attributes.Add("ContractId", Variable[0].Attributes["ContractId"].Value);
                tbl.Attributes.Add("ContractTemplateId", Variable[0].Attributes["ContractTemplateId"].Value);

                FetchAttachedFields(Variable, tbl);
                pnl.Controls.Add(tbl);
            }
            return pnl;
        }

        public void FetchAttachedFields(XmlNodeList Variable, HtmlTable tbl, int option = 0, string parentFLID = "")
        {
            for (int i = 0; i < Variable.Count; i++)
            {
                XmlElement ele = (XmlElement)Variable[i];
                string FLID = ele.GetElementsByTagName(VariableCollection.FieldLibraryId)[0].InnerText;
                XmlNodeList nodes = ele.OwnerDocument.DocumentElement.SelectNodes("//Variable[contains(@AttachedToField,'" + FLID + "')]");

                HtmlTableRow tr = null;
                if (ele.Attributes["AttachedToField"].Value != "0" && option == 1)
                {
                    string attr = ele.Attributes["AttachedToField"].Value;
                    string value = ele.Attributes["AttachedToValue"].Value;
                    string[] s = { "" };
                    string[] options = { "" };

                    if (value.Contains(','))
                        options = value.Split(',');
                    else
                        options[0] = value;

                    if (attr.Contains(','))
                        s = attr.Split(',');
                    else
                        s[0] = attr;

                    for (int k = 0; k < s.Length; k++)
                    {
                        if (parentFLID == s[k])
                        {
                            tr = DesignHtmlRow(ele, options[k]);
                            if (tr != null)
                            {
                                bool added = false;
                                foreach (HtmlTableRow tblrow in tbl.Rows)
                                {
                                    if (tr.Attributes["class"] == tblrow.Attributes["class"])//restricting to add same row with same attributes
                                    {
                                        added = true;
                                        break;
                                    }
                                }
                                if (!added)
                                    tbl.Rows.Add(tr);
                            }
                        }
                    }
                }
                else if (ele.Attributes["AttachedToField"].Value == "0" && option == 0)
                {
                    if (nodes.Count > 0)
                        tr = DesignHtmlRow(ele, "0", 1);
                    else
                        tr = DesignHtmlRow(ele, "0");

                    if (tr != null)
                        tbl.Rows.Add(tr);
                }
                if (nodes.Count > 0)
                {
                    FetchAttachedFields(nodes, tbl, 1, FLID);
                }               
            }
        }        

        public Boolean IsValidTemplate(string FilePath)
        {
            AsposeWord.Document oWordDoc = new AsposeWord.Document(FilePath);
            int VariableCount = oWordDoc.Variables.Count;
            if (VariableCount > 0 && oWordDoc.Variables["SIGNATURE"] == VariableCollection.Value_SIGNATURE)
                return true;
            else
                return false;
        }

        public Boolean IsValidContractDocument(string FilePath)
        {
            AsposeWord.Document oWordDoc = new AsposeWord.Document(FilePath);
            string ContractDocumentVersion = oWordDoc.Variables["ContractDocumentVersion"];
            string ContractDocument = oWordDoc.Variables["ContractDocument"];
            if (!string.IsNullOrEmpty(ContractDocumentVersion) && ContractDocument == "ORIGINAL")
                return true;
            else
                return false;
        }

        private void FindAndReplace(AsposeWord.Document wordApp, string findText, string replaceText)
        {
            //wordApp.Range.Replace(findText, replaceText, false, false);
            AsposeWord.Paragraph p = new AsposeWord.Paragraph(wordApp);
            //p.Range.Replace(

            wordApp.Range.Replace(new Regex(findText), new ReplaceEvaluatorFindAndInsertText(replaceText, p), false);
        }

        private string[] Split(string input)
        {
            string[] str = { };
            if (input.IndexOf("_x000a_") > -1)
            {
                str = Regex.Split(input, "_x000a_");
            }
            else if (input.IndexOf("\n") > -1)
            {
                str = Regex.Split(input, "\n");
            }
            else if (input.IndexOf("\\n") > -1)
            {
                str = Regex.Split(input, "\\n");
            }
            return str;
        }        

        //public void UpdateFieldLibrary_OLD(string FilePath, int ContractTemplateId)
        //{
        //    FileInfo file = new FileInfo(FilePath);
        //    string FileExtension = file.Extension;
        //    System.Web.UI.WebControls.Panel pnl = new System.Web.UI.WebControls.Panel();

        //   // if (IsValidTemplate(FilePath))
        //    {
        //        AsposeWord.Document oWordDoc = new AsposeWord.Document(FilePath);                
        //        AsposeWord.BookmarkCollection BookMarks = oWordDoc.Range.Bookmarks;

        //        XmlDocument VariableDocument = GetXML(oWordDoc.Variables);
        //        XmlNodeList Variable = VariableDocument.GetElementsByTagName("Variable");
        //        FieldLibraryOperations FieldLibrary = new FieldLibraryOperations();                

        //        if (VariableDocument != null)
        //        {
        //            #region Temporary table for Bookmarks
        //            DataTable dtBookMarks = new DataTable();
        //            dtBookMarks.Columns.Add("BookMarkID", Type.GetType("System.Int32"));
        //            dtBookMarks.Columns.Add("FieldLibraryID", Type.GetType("System.Int32"));
        //            dtBookMarks.Columns.Add("BookmarkName", Type.GetType("System.String"));
        //            dtBookMarks.Columns.Add("ClauseID", Type.GetType("System.Int32"));
        //            dtBookMarks.Columns.Add("ContractTemplateId", Type.GetType("System.Int32"));
        //            #endregion                  

        //            foreach (XmlElement ele in Variable)
        //            {                       
        //                string val = ele.Attributes.GetNamedItem(VariableCollection.Id).Value;
        //                string ID = "", FieldName = "", Question = "", ToolTip = "", MasterTableName = "", Options = "";
        //                char IsRequired = 'Y', IsRevisionField = 'Y', IsRenewField = 'Y', IsTerminateField = 'Y';

        //                int FieldTypeId, ContractTypeID, ClauseID, ClauseFieldID, VersionID, ParentClauseID, DefaultSize, MasterId, ClauseLibraryId;
        //                FieldTypeId = ContractTypeID = ClauseID = ClauseFieldID = VersionID = ParentClauseID = DefaultSize = MasterId = ClauseLibraryId = 0;

        //                if (ele.HasChildNodes)
        //                {
        //                    ID = ele.Attributes[0].Value;
        //                    FieldName = ele.Attributes[1].Value;

        //                    #region
        //                    FieldTypeId = Convert.ToInt32(ele.GetElementsByTagName(VariableCollection.FieldTypeId)[0].InnerText);
        //                    Question = ele.GetElementsByTagName(VariableCollection.Question)[0] == null ? "" : ele.GetElementsByTagName(VariableCollection.Question)[0].InnerText;
        //                    ToolTip = ele.GetElementsByTagName(VariableCollection.ToolTip)[0] == null ? "" : ele.GetElementsByTagName(VariableCollection.ToolTip)[0].InnerText;
        //                    MasterTableName = ele.GetElementsByTagName(VariableCollection.Master)[0] == null ? "" : ele.GetElementsByTagName(VariableCollection.Master)[0].InnerText;

        //                    if (ele.GetElementsByTagName(VariableCollection.IsRequired)[0] != null)
        //                        IsRequired = ele.GetElementsByTagName(VariableCollection.IsRequired)[0].InnerText == "true" ? 'Y' : 'N';

        //                    if (ele.GetElementsByTagName(VariableCollection.IsRevisionField)[0] != null)
        //                        IsRevisionField = Convert.ToChar(ele.GetElementsByTagName(VariableCollection.IsRevisionField)[0].InnerText);

        //                    if (ele.GetElementsByTagName(VariableCollection.IsRenewField)[0] != null)
        //                        IsRenewField = Convert.ToChar(ele.GetElementsByTagName(VariableCollection.IsRenewField)[0].InnerText);

        //                    if (ele.GetElementsByTagName(VariableCollection.IsTerminateField)[0] != null)
        //                        IsTerminateField = Convert.ToChar(ele.GetElementsByTagName(VariableCollection.IsTerminateField)[0].InnerText);

        //                    if (ele.GetElementsByTagName(VariableCollection.ContractTypeId)[0] != null)
        //                        ContractTypeID = Convert.ToInt32(ele.GetElementsByTagName(VariableCollection.ContractTypeId)[0].InnerText);

        //                    if (ele.GetElementsByTagName(VariableCollection.VersionID)[0] != null)
        //                        VersionID = Convert.ToInt32(ele.GetElementsByTagName(VariableCollection.VersionID)[0].InnerText);

        //                    if (ele.GetElementsByTagName(VariableCollection.ParentClauseId)[0] != null)
        //                        ParentClauseID = Convert.ToInt32(ele.GetElementsByTagName(VariableCollection.ParentClauseId)[0].InnerText);

        //                    if (ele.GetElementsByTagName(VariableCollection.DefaultSize)[0] != null)
        //                        DefaultSize = Convert.ToInt32(ele.GetElementsByTagName(VariableCollection.DefaultSize)[0].InnerText);

        //                    if (ele.GetElementsByTagName(VariableCollection.ClauseId)[0] != null)
        //                        ClauseID = Convert.ToInt32(ele.GetElementsByTagName(VariableCollection.ClauseId)[0].InnerText);

        //                    if (FieldName.Contains(':'))
        //                        ClauseFieldID = Convert.ToInt32(FieldName.Split(':')[1]);

        //                    if (ele.GetElementsByTagName(VariableCollection.Options)[0] != null)
        //                        Options = ele.GetElementsByTagName(VariableCollection.Options)[0].InnerText;

        //                    //Options = Split(ele.GetElementsByTagName(VariableCollection.Options)[0].InnerText);
        //                    if (ele.GetElementsByTagName(VariableCollection.MasterId)[0] != null)
        //                        MasterId = Convert.ToInt32(ele.GetElementsByTagName(VariableCollection.MasterId)[0].InnerText);

        //                    //if (ele.GetElementsByTagName(VariableCollection.ClauseId)[0] != null)
        //                    //    ClauseLibraryId = Convert.ToInt32(ele.GetElementsByTagName(VariableCollection.MasterId)[0].InnerText);
        //                    #endregion

        //                    FieldLibrary.ClauseID = ClauseID;
        //                    FieldLibrary.ContractId = ContractTypeID;
        //                    FieldLibrary.DefaultSize = DefaultSize;
        //                    FieldLibrary.FieldTypeID = FieldTypeId;
        //                    FieldLibrary.FieldName = ID;
        //                    FieldLibrary.FieldRealName = FieldName;

        //                    if (ID.IndexOf(':') > -1)
        //                    {
        //                        FieldLibrary.FieldID = Convert.ToInt32(ID.Split(':')[1]);
        //                        FieldLibrary.FieldName = ID.Split(':')[0];
        //                    }
        //                    FieldLibrary.IsRequired = IsRequired;
        //                    FieldLibrary.IsRevisionField = IsRevisionField;
        //                    FieldLibrary.IsRenewField = IsRenewField;
        //                    FieldLibrary.IsTerminateField = IsTerminateField;
        //                    FieldLibrary.FieldValue = Options == "" ? MasterId.ToString() : Options.Replace("\n", "_x000a_").Replace("'", string.Empty);
        //                    FieldLibrary.Question = Question;
        //                    FieldLibrary.HelpBubble = ToolTip;
        //                    FieldLibrary.AddedBy = UserId;
        //                    FieldLibrary.ModifiedBy = UserId;
        //                    FieldLibrary.IpAddress = IP;
        //                    FieldLibrary.ContractTemplateId = ContractTemplateId;
        //                    FieldLibrary.ClauseFieldID = ClauseFieldID;

        //                    #region Get Bookmarks
        //                    foreach (AsposeWord.Bookmark BookMark in BookMarks)
        //                    {
        //                        if (BookMark.Name.StartsWith("_"))
        //                        {
        //                            string BookMarkName = "", BOOKMARK = "";
        //                            string[] SplittedStr = BookMark.Name.Split('_');

        //                            if (BookMark.Name == "_GoBack")
        //                                BookMarkName = "";
        //                            else if (BookMark.Name.StartsWith("_ClauseField_"))
        //                                BookMarkName = SplittedStr[2].Replace(" ", "") + ":" + SplittedStr[3].Replace(" ", "");
        //                            else if (BookMark.Name.StartsWith("_Clause_"))
        //                            {
        //                                ClauseLibraryId = Convert.ToInt32(SplittedStr[2]);
        //                                FieldLibrary.FieldLibraryID = 0;
        //                                foreach (XmlNode node in ele.ChildNodes)
        //                                {
        //                                    if (node.Name.StartsWith("ClauseID_") && ClauseLibraryId.ToString() == node.InnerText)
        //                                    {
        //                                        dtBookMarks.Rows.Add(0, 0, BookMark.Name.Replace(" ", ""), ClauseLibraryId, ContractTemplateId);
        //                                    }
        //                                }
        //                            }
        //                            else
        //                                BookMarkName = SplittedStr[1].Replace(" ", "");

        //                            if (BookMarkName == FieldName.Replace(" ", ""))
        //                            {
        //                                BOOKMARK = BookMark.Name.Replace(" ", "");
        //                            }
        //                            else if (ClauseLibraryId == 0 && ClauseID != 0)
        //                            {
        //                                ClauseLibraryId = ClauseID;
        //                            }

        //                            if (!string.IsNullOrEmpty(BOOKMARK) && !BookMark.Name.Equals("_GoBack"))
        //                            {
        //                                dtBookMarks.Rows.Add(0, null, BOOKMARK, ClauseLibraryId, ContractTemplateId);
        //                            }
        //                        }
        //                    }
        //                    #endregion

        //                    FieldLibrary.BookMarkDetails = dtBookMarks;
        //                    string retVal = FieldLibrary.InsertRecord();
        //                    dtBookMarks.Rows.Clear();
        //                }
        //            }
        //            string result = FieldLibrary.FieldLibraryConditionalFieldsAdd();
        //        }
        //    }
        //}

        private DataTable CreateFieldLibraryDefinition()
        {
            DataTable dtFieldLibrary = new DataTable();
            dtFieldLibrary.Columns.Add("FieldName", Type.GetType("System.String"));
            dtFieldLibrary.Columns.Add("Question", Type.GetType("System.String"));
            dtFieldLibrary.Columns.Add("FieldTypeID", Type.GetType("System.Int32"));
            dtFieldLibrary.Columns.Add("HelpBubble", Type.GetType("System.String"));
            dtFieldLibrary.Columns.Add("ClauseFieldID", Type.GetType("System.String"));
            dtFieldLibrary.Columns.Add("ClauseID", Type.GetType("System.Int32"));
            dtFieldLibrary.Columns.Add("ClauseLibraryID", Type.GetType("System.Int32"));
            dtFieldLibrary.Columns.Add("FieldID", Type.GetType("System.Int32"));
            dtFieldLibrary.Columns.Add("IsRequired", Type.GetType("System.Char"));
            dtFieldLibrary.Columns.Add("DefaultSize", Type.GetType("System.Int32"));
            dtFieldLibrary.Columns.Add("FieldRealName", Type.GetType("System.String"));
            dtFieldLibrary.Columns.Add("IsRevisionField", Type.GetType("System.Char"));
            dtFieldLibrary.Columns.Add("IsRenewField", Type.GetType("System.Char"));
            dtFieldLibrary.Columns.Add("IsTerminateField", Type.GetType("System.Char"));
            dtFieldLibrary.Columns.Add("Sequence", Type.GetType("System.Decimal"));
            dtFieldLibrary.Columns.Add("FieldValue", Type.GetType("System.String"));
            return dtFieldLibrary;
        }

        public void UpdateFieldLibrary(string FilePath, int ContractTemplateId)
        {
            FileInfo file = new FileInfo(FilePath);
            string FileExtension = file.Extension;
            System.Web.UI.WebControls.Panel pnl = new System.Web.UI.WebControls.Panel();

            // if (IsValidTemplate(FilePath))
            {
                AsposeWord.Document oWordDoc = new AsposeWord.Document(FilePath);
                AsposeWord.BookmarkCollection BookMarks = oWordDoc.Range.Bookmarks;

                XmlDocument VariableDocument = GetXML(oWordDoc.Variables);
                XmlNodeList Variable = VariableDocument.GetElementsByTagName("Variable");
                FieldLibraryOperations FieldLibrary = new FieldLibraryOperations();

                if (VariableDocument != null)
                {
                    #region Temporary table for Bookmarks
                    DataTable dtBookMarks = new DataTable();
                    dtBookMarks.Columns.Add("BookMarkID", Type.GetType("System.Int32"));
                    dtBookMarks.Columns.Add("FieldLibraryID", Type.GetType("System.Int32"));
                    dtBookMarks.Columns.Add("BookmarkName", Type.GetType("System.String"));
                    dtBookMarks.Columns.Add("ClauseID", Type.GetType("System.Int32"));
                    dtBookMarks.Columns.Add("ContractTemplateId", Type.GetType("System.Int32"));
                    #endregion
                    DataTable dtFieldLibrary = CreateFieldLibraryDefinition();

                    foreach (XmlElement ele in Variable)
                    {
                        string val = ele.Attributes.GetNamedItem(VariableCollection.Id).Value;
                        string ID = "", FieldName = "", Question = "", ToolTip = "", MasterTableName = "", Options = "", FieldSequence = "0", ClauseSequence = "0";
                        char IsRequired = 'Y', IsRevisionField = 'Y', IsRenewField = 'Y', IsTerminateField = 'Y';

                        int FieldTypeId, ContractTypeID, ClauseID, ClauseFieldID, VersionID, ParentClauseID, DefaultSize, MasterId, ClauseLibraryId;
                        FieldTypeId = ContractTypeID = ClauseID = ClauseFieldID = VersionID = ParentClauseID = DefaultSize = MasterId = ClauseLibraryId = 0;

                        if (ele.HasChildNodes)
                        {
                            ID = ele.Attributes[0].Value;
                            FieldName = ele.Attributes[1].Value;

                            #region
                            FieldTypeId = Convert.ToInt32(ele.GetElementsByTagName(VariableCollection.FieldTypeId)[0].InnerText);
                            Question = ele.GetElementsByTagName(VariableCollection.Question)[0] == null ? "" : ele.GetElementsByTagName(VariableCollection.Question)[0].InnerText;
                            ToolTip = ele.GetElementsByTagName(VariableCollection.ToolTip)[0] == null ? "" : ele.GetElementsByTagName(VariableCollection.ToolTip)[0].InnerText;
                            MasterTableName = ele.GetElementsByTagName(VariableCollection.Master)[0] == null ? "" : ele.GetElementsByTagName(VariableCollection.Master)[0].InnerText;

                            if (ele.GetElementsByTagName(VariableCollection.IsRequired)[0] != null)
                                IsRequired = ele.GetElementsByTagName(VariableCollection.IsRequired)[0].InnerText == "true" ? 'Y' : 'N';

                            if (ele.GetElementsByTagName(VariableCollection.IsRevisionField)[0] != null)
                                IsRevisionField = Convert.ToChar(ele.GetElementsByTagName(VariableCollection.IsRevisionField)[0].InnerText);

                            if (ele.GetElementsByTagName(VariableCollection.IsRenewField)[0] != null)
                                IsRenewField = Convert.ToChar(ele.GetElementsByTagName(VariableCollection.IsRenewField)[0].InnerText);

                            if (ele.GetElementsByTagName(VariableCollection.IsTerminateField)[0] != null)
                                IsTerminateField = Convert.ToChar(ele.GetElementsByTagName(VariableCollection.IsTerminateField)[0].InnerText);

                            if (ele.GetElementsByTagName(VariableCollection.ContractTypeId)[0] != null)
                                ContractTypeID = Convert.ToInt32(ele.GetElementsByTagName(VariableCollection.ContractTypeId)[0].InnerText);

                            if (ele.GetElementsByTagName(VariableCollection.VersionID)[0] != null)
                                VersionID = Convert.ToInt32(ele.GetElementsByTagName(VariableCollection.VersionID)[0].InnerText);

                            if (ele.GetElementsByTagName(VariableCollection.ParentClauseId)[0] != null)
                                ParentClauseID = Convert.ToInt32(ele.GetElementsByTagName(VariableCollection.ParentClauseId)[0].InnerText);

                            if (ele.GetElementsByTagName(VariableCollection.DefaultSize)[0] != null)
                                DefaultSize = Convert.ToInt32(ele.GetElementsByTagName(VariableCollection.DefaultSize)[0].InnerText);

                            if (ele.GetElementsByTagName(VariableCollection.ClauseId)[0] != null)
                                ClauseID = Convert.ToInt32(ele.GetElementsByTagName(VariableCollection.ClauseId)[0].InnerText);

                            if (FieldName.Contains(':'))
                                ClauseFieldID = Convert.ToInt32(FieldName.Split(':')[1]);

                            if (ele.GetElementsByTagName(VariableCollection.Options)[0] != null)
                                Options = ele.GetElementsByTagName(VariableCollection.Options)[0].InnerText;                            

                            if (VariableDocument.GetElementsByTagName("ClauseID_" + ClauseID + "_Sequence")[0] != null)
                                ClauseSequence = VariableDocument.GetElementsByTagName("ClauseID_" + ClauseID + "_Sequence")[0].InnerText;

                            if (ele.GetElementsByTagName(VariableCollection.Sequence)[0] != null)
                                FieldSequence = ele.GetElementsByTagName(VariableCollection.Sequence)[0].InnerText;

                            if (ClauseSequence != "0")
                            {
                                FieldSequence = ClauseSequence + "." + FieldSequence;
                            }
                            //Options = Split(ele.GetElementsByTagName(VariableCollection.Options)[0].InnerText);
                            if (ele.GetElementsByTagName(VariableCollection.MasterId)[0] != null)
                                MasterId = Convert.ToInt32(ele.GetElementsByTagName(VariableCollection.MasterId)[0].InnerText);

                            //if (ele.GetElementsByTagName(VariableCollection.ClauseId)[0] != null)
                            //    ClauseLibraryId = Convert.ToInt32(ele.GetElementsByTagName(VariableCollection.MasterId)[0].InnerText);
                            #endregion

                            DataRow drFieldLibraryRow = dtFieldLibrary.NewRow();
                            drFieldLibraryRow["FieldName"] = ID;
                            drFieldLibraryRow["FieldRealName"] = FieldName;
                            drFieldLibraryRow["Question"] = Question;
                            drFieldLibraryRow["FieldTypeID"] = FieldTypeId;
                            drFieldLibraryRow["HelpBubble"] = ToolTip;
                            drFieldLibraryRow["ClauseID"] = ClauseID;
                            drFieldLibraryRow["ClauseFieldID"] = ClauseFieldID;
                            drFieldLibraryRow["ClauseLibraryID"] = ClauseLibraryId;
                            drFieldLibraryRow["IsRequired"] = IsRequired;
                            drFieldLibraryRow["IsRevisionField"] = IsRevisionField;
                            drFieldLibraryRow["IsRenewField"] = IsRenewField;
                            drFieldLibraryRow["IsTerminateField"] = IsTerminateField;
                            drFieldLibraryRow["DefaultSize"] = DefaultSize;
                            drFieldLibraryRow["Sequence"] = Convert.ToDecimal(FieldSequence);
                            drFieldLibraryRow["FieldValue"] = Options == "" ? MasterId.ToString() : Options.Replace("\n", "_x000a_").Replace("'", string.Empty);
                            if (ID.IndexOf(':') > -1)
                            {
                                drFieldLibraryRow["FieldID"] = Convert.ToInt32(ID.Split(':')[1]);
                                drFieldLibraryRow["FieldName"] = ID.Split(':')[0];
                            }
                            dtFieldLibrary.Rows.Add(drFieldLibraryRow);

                            #region commented
                            //FieldLibrary.ClauseID = ClauseID;
                            //FieldLibrary.ContractId = ContractTypeID;
                            //FieldLibrary.DefaultSize = DefaultSize;
                            //FieldLibrary.FieldTypeID = FieldTypeId;
                            //FieldLibrary.FieldName = ID;
                            //FieldLibrary.FieldRealName = FieldName;

                            //if (ID.IndexOf(':') > -1)
                            //{
                            //    FieldLibrary.FieldID = Convert.ToInt32(ID.Split(':')[1]);
                            //    FieldLibrary.FieldName = ID.Split(':')[0];
                            //}
                            //FieldLibrary.IsRequired = IsRequired;
                            //FieldLibrary.IsRevisionField = IsRevisionField;
                            //FieldLibrary.IsRenewField = IsRenewField;
                            //FieldLibrary.IsTerminateField = IsTerminateField;
                            //FieldLibrary.FieldValue = Options == "" ? MasterId.ToString() : Options.Replace("\n", "_x000a_").Replace("'", string.Empty);
                            //FieldLibrary.Question = Question;
                            //FieldLibrary.HelpBubble = ToolTip;
                            //FieldLibrary.ClauseFieldID = ClauseFieldID;                           

                            //FieldLibrary.AddedBy = UserId;
                            //FieldLibrary.ModifiedBy = UserId;
                            //FieldLibrary.IpAddress = IP;
                            //FieldLibrary.ContractTemplateId = ContractTemplateId;
                            //FieldLibrary.FieldLibrary = dtFieldLibrary;
                            #endregion

                            #region Get Bookmarks
                            //foreach (AsposeWord.Bookmark BookMark in BookMarks)
                            //{
                            //    if (BookMark.Name.StartsWith("_"))
                            //    {
                            //        string BookMarkName = "", BOOKMARK = "";
                            //        string[] SplittedStr = BookMark.Name.Split('_');

                            //        if (BookMark.Name == "_GoBack")
                            //            BookMarkName = "";
                            //        else if (BookMark.Name.StartsWith("_ClauseField_"))
                            //            BookMarkName = SplittedStr[2].Replace(" ", "") + ":" + SplittedStr[3].Replace(" ", "");
                            //        else if (BookMark.Name.StartsWith("_Clause_"))
                            //        {
                            //            ClauseLibraryId = Convert.ToInt32(SplittedStr[2]);
                            //            FieldLibrary.FieldLibraryID = 0;
                            //            foreach (XmlNode node in ele.ChildNodes)
                            //            {
                            //                if (node.Name.StartsWith("ClauseID_") && ClauseLibraryId.ToString() == node.InnerText)
                            //                {
                            //                    dtBookMarks.Rows.Add(0, 0, BookMark.Name.Replace(" ", ""), ClauseLibraryId, ContractTemplateId);
                            //                }
                            //            }
                            //        }
                            //        else
                            //            BookMarkName = SplittedStr[1].Replace(" ", "");

                            //        if (BookMarkName == FieldName.Replace(" ", ""))
                            //        {
                            //            BOOKMARK = BookMark.Name.Replace(" ", "");
                            //        }
                            //        else if (ClauseLibraryId == 0 && ClauseID != 0)
                            //        {
                            //            ClauseLibraryId = ClauseID;
                            //        }

                            //        if (!string.IsNullOrEmpty(BOOKMARK) && !BookMark.Name.Equals("_GoBack"))
                            //        {
                            //            dtBookMarks.Rows.Add(0, null, BOOKMARK, ClauseLibraryId, ContractTemplateId);
                            //        }
                            //    }
                            //}
                            #endregion
                            //FieldLibrary.BookMarkDetails = dtBookMarks;                            
                            //dtBookMarks.Rows.Clear();
                        }
                    }
                    FieldLibrary.AddedBy = UserId;
                    FieldLibrary.ModifiedBy = UserId;
                    FieldLibrary.IpAddress = IP;
                    FieldLibrary.ContractTemplateId = ContractTemplateId;
                    FieldLibrary.FieldLibrary = dtFieldLibrary;
                    FieldLibrary.BookMarkDetails = dtBookMarks;
                    string retVal = FieldLibrary.InsertRecord();
                    //string result = FieldLibrary.FieldLibraryConditionalFieldsAdd();
                }
            }
        }

        public bool SaveAnswers(Control Userctrl, bool IsDraft, bool isContractGenrate)
        {
            //Control ctrl = GetClosestParentControl(Userctrl);
            DataTable dt;
            Control ctrl = Userctrl;

            string InputValue = null, ID = "", FieldTypeId = "", Master="";
            int FieldLibraryId, ContractQAId;
            char IsUpdated = 'N';

            dt = new DataTable();
            dt.Columns.Add("SEQ", Type.GetType("System.Int32"));
            dt.Columns.Add("ContractQAId", Type.GetType("System.Int32"));
            dt.Columns.Add("FieldLibraryID", Type.GetType("System.Int32"));
            dt.Columns.Add("Question", Type.GetType("System.String"));
            dt.Columns.Add("TextValue", Type.GetType("System.String"));
            dt.Columns.Add("NumericValue", Type.GetType("System.Int64"));
            dt.Columns.Add("DecimalValue", Type.GetType("System.Decimal"));
            dt.Columns.Add("DateValue", Type.GetType("System.DateTime"));
            dt.Columns.Add("ForeignKeyValye", Type.GetType("System.String"));
            dt.Columns.Add("IsUpdated", Type.GetType("System.Char"));
            //dt.Columns.Add("IsVisible", Type.GetType("System.Char"));

            #region GetControlValue
            HtmlTable tbl = (HtmlTable)ctrl.FindControl("ASPOSE_tbl");
            int SEQ = 1;

            #region fill answers
            foreach (HtmlTableRow row in tbl.Rows)
            {
                if (row.Style.Value == null || (row.Style.Value != null && !row.Style.Value.Contains("display:none")))//(row.Visible)
                {
                    Control parentctrl = row.Cells[2];
                    string Question = row.Cells[0].InnerText;
                    InputValue = null;
                    foreach (var c in parentctrl.Controls.OfType<HtmlInputText>())
                    {
                        InputValue = string.IsNullOrEmpty(c.Value) ? InputValue : c.Value;
                        ID = c.ID;
                        FieldTypeId = c.Attributes["ftid"];
                        FieldLibraryId = Convert.ToInt32(c.Attributes["flid"]);
                        ContractQAId = Convert.ToInt32(c.Attributes["ContractQAId"]);
                        IsUpdated = Convert.ToChar(c.Attributes["IsUpdated"]);

                        switch (FieldTypeId)
                        {
                            case VariableCollection.TEXT:
                            case VariableCollection.MULTILINE:
                            case VariableCollection.ALPHANUMERIC:
                            case VariableCollection.LETTER:
                                dt.Rows.Add(SEQ,ContractQAId, FieldLibraryId, Question, InputValue, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, IsUpdated);
                                break;
                            case VariableCollection.NUMBERS:
                                dt.Rows.Add(SEQ,ContractQAId, FieldLibraryId, Question, DBNull.Value, InputValue, DBNull.Value, DBNull.Value, DBNull.Value, IsUpdated);
                                break;
                            case VariableCollection.CURRENCY:
                                dt.Rows.Add(SEQ, ContractQAId, FieldLibraryId, Question, DBNull.Value, DBNull.Value, InputValue, DBNull.Value, DBNull.Value, IsUpdated);
                                break;
                            case VariableCollection.DATE:
                                if (string.IsNullOrEmpty(InputValue))
                                    dt.Rows.Add(SEQ, ContractQAId, FieldLibraryId, Question, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, IsUpdated);
                                else
                                    dt.Rows.Add(SEQ, ContractQAId, FieldLibraryId, Question, DBNull.Value, DBNull.Value, DBNull.Value, InputValue, DBNull.Value, IsUpdated);
                                break;
                        }
                        SEQ += 1;
                    }

                    foreach (var c in parentctrl.Controls.OfType<HtmlTextArea>())
                    {
                        InputValue = c.Value;
                        ID = c.ID;
                        FieldTypeId = c.Attributes["ftid"];
                        FieldLibraryId = Convert.ToInt32(c.Attributes["flid"]);
                        ContractQAId = Convert.ToInt32(c.Attributes["ContractQAId"]);
                        IsUpdated = Convert.ToChar(c.Attributes["IsUpdated"]);

                        dt.Rows.Add(SEQ, ContractQAId, FieldLibraryId, Question, InputValue, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, IsUpdated);
                        SEQ += 1;
                    }

                    foreach (var ddlList in parentctrl.Controls.OfType<HtmlSelect>())
                    {
                        ID = ddlList.ID;
                        FieldTypeId = ddlList.Attributes["ftid"];
                        Master = ddlList.Attributes["master"];
                        FieldLibraryId = Convert.ToInt32(ddlList.Attributes["flid"]);
                        ContractQAId = Convert.ToInt32(ddlList.Attributes["ContractQAId"]);
                        IsUpdated = Convert.ToChar(ddlList.Attributes["IsUpdated"]);
                        InputValue = null;
                        if (string.IsNullOrEmpty(Master))
                        {
                            foreach (ListItem objItem in ddlList.Items)
                            {
                                if (objItem.Selected)
                                {
                                    InputValue += objItem.Value + ",";
                                }
                            }
                            dt.Rows.Add(SEQ, ContractQAId, FieldLibraryId, Question, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, InputValue, IsUpdated);
                            SEQ += 1;
                        }
                    }
                    foreach (var hdnField in parentctrl.Controls.OfType<HtmlInputHidden>())
                    {
                        ID = hdnField.ID;
                        FieldLibraryId = Convert.ToInt32(hdnField.Attributes["flid"]);
                        Master = hdnField.Attributes["master"];
                        ContractQAId = Convert.ToInt32(hdnField.Attributes["ContractQAId"]);
                        IsUpdated = 'Y';//Convert.ToChar(hdnField.Attributes["IsUpdated"]);
                        InputValue = hdnField.Value;
                        if (!string.IsNullOrEmpty(Master))
                            dt.Rows.Add(SEQ, ContractQAId, FieldLibraryId, Question, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, InputValue, IsUpdated);
                        SEQ += 1;
                    }

                    foreach (var cbList in parentctrl.Controls.OfType<CheckBoxList>())
                    {
                        ID = cbList.ID;
                        FieldTypeId = cbList.Attributes["ftid"];
                        FieldLibraryId = Convert.ToInt32(cbList.Attributes["flid"]);
                        ContractQAId = Convert.ToInt32(cbList.Attributes["ContractQAId"]);
                        IsUpdated = Convert.ToChar(cbList.Attributes["IsUpdated"]);
                        InputValue = null;
                        foreach (ListItem objItem in cbList.Items)
                        {
                            if (objItem.Selected)
                            {
                                InputValue += objItem.Value + ",";
                            }
                        }
                        dt.Rows.Add(SEQ, ContractQAId, FieldLibraryId, Question, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, InputValue, IsUpdated);
                        SEQ += 1;
                    }

                    foreach (var rbList in parentctrl.Controls.OfType<RadioButtonList>())
                    {
                        ID = rbList.ID;
                        FieldTypeId = rbList.Attributes["ftid"];
                        FieldLibraryId = Convert.ToInt32(rbList.Attributes["flid"]);
                        ContractQAId = Convert.ToInt32(rbList.Attributes["ContractQAId"]);
                        IsUpdated = Convert.ToChar(rbList.Attributes["IsUpdated"]);
                        InputValue = rbList.SelectedValue;
                        dt.Rows.Add(SEQ, ContractQAId, FieldLibraryId, Question, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, InputValue, IsUpdated);
                        SEQ += 1;
                    }
                }
            }
            #endregion

            QuestionAnswer QA = new QuestionAnswer();
            QA.ContractRequestId = RequestId;
            QA.ContractId = Convert.ToInt32(tbl.Attributes["ContractId"]);
            QA.ContractTemplateId = Convert.ToInt32(tbl.Attributes["ContractTemplateId"]); ;
            QA.Answers = dt;
            QA.AddedBy = UserId;
            QA.ModifiedBy = UserId;
            QA.IpAddress = IP;
            QA.IsDraft = IsDraft;
            QA.isContractGenrate = isContractGenrate;

            string flag = QA.InsertRecord();
            #endregion
            if (flag != "0")
                return true;
            else
                return false;
        }

        public string SaveAnswersFromBulkImport(Control Userctrl, bool IsDraft, string ContractTemplateId)
        {
            //Control ctrl = GetClosestParentControl(Userctrl);
            DataTable dt;
            Control ctrl = Userctrl;

            string InputValue = null, ID = "", FieldTypeId = "";
            int FieldLibraryId, ContractQAId;
            char IsUpdated = 'N';

            dt = new DataTable();
            dt.Columns.Add("SEQ", Type.GetType("System.Int32"));
            dt.Columns.Add("ContractQAId", Type.GetType("System.Int32"));
            dt.Columns.Add("FieldLibraryID", Type.GetType("System.Int32"));
            dt.Columns.Add("Question", Type.GetType("System.String"));
            dt.Columns.Add("TextValue", Type.GetType("System.String"));
            dt.Columns.Add("NumericValue", Type.GetType("System.Int64"));
            dt.Columns.Add("DecimalValue", Type.GetType("System.Decimal"));
            dt.Columns.Add("DateValue", Type.GetType("System.DateTime"));
            dt.Columns.Add("ForeignKeyValye", Type.GetType("System.String"));
            dt.Columns.Add("IsUpdated", Type.GetType("System.Char"));
            //dt.Columns.Add("IsVisible", Type.GetType("System.Char"));

            #region GetControlValue
            try
            {
                Panel tbl = (Panel)ctrl.FindControl("pnlCtrl");
                HtmlTable tblAspose = (HtmlTable)tbl.FindControl("ASPOSE_tbl");

                int SEQ = 1;

                #region fill answers
                foreach (HtmlTableRow row in tblAspose.Rows)
                {
                    if (row.Style.Value == null || (row.Style.Value != null && !row.Style.Value.Contains("display:none")))//(row.Visible)
                    {
                        Control parentctrl = row.Cells[2];
                        string Question = row.Cells[0].InnerText;
                        InputValue = null;
                        foreach (var c in parentctrl.Controls.OfType<HtmlInputText>())
                        {
                            InputValue = string.IsNullOrEmpty(c.Value) ? InputValue : c.Value;
                            ID = c.ID;
                            FieldTypeId = c.Attributes["ftid"];
                            FieldLibraryId = Convert.ToInt32(c.Attributes["flid"]);
                            ContractQAId = Convert.ToInt32(c.Attributes["ContractQAId"]);
                            IsUpdated = Convert.ToChar(c.Attributes["IsUpdated"]);

                            switch (FieldTypeId)
                            {
                                case VariableCollection.TEXT:
                                case VariableCollection.MULTILINE:
                                case VariableCollection.ALPHANUMERIC:
                                case VariableCollection.LETTER:
                                    dt.Rows.Add(SEQ, ContractQAId, FieldLibraryId, Question, InputValue, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, IsUpdated);
                                    break;
                                case VariableCollection.NUMBERS:
                                    dt.Rows.Add(SEQ, ContractQAId, FieldLibraryId, Question, DBNull.Value, InputValue, DBNull.Value, DBNull.Value, DBNull.Value, IsUpdated);
                                    break;
                                case VariableCollection.CURRENCY:
                                    dt.Rows.Add(SEQ, ContractQAId, FieldLibraryId, Question, DBNull.Value, DBNull.Value, InputValue, DBNull.Value, DBNull.Value, IsUpdated);
                                    break;
                                case VariableCollection.DATE:
                                    if (string.IsNullOrEmpty(InputValue))
                                        dt.Rows.Add(SEQ, ContractQAId, FieldLibraryId, Question, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, IsUpdated);
                                    else
                                        dt.Rows.Add(SEQ, ContractQAId, FieldLibraryId, Question, DBNull.Value, DBNull.Value, DBNull.Value, InputValue, DBNull.Value, IsUpdated);
                                    break;
                            }
                            SEQ += 1;
                        }

                        foreach (var c in parentctrl.Controls.OfType<HtmlTextArea>())
                        {
                            InputValue = c.Value;
                            ID = c.ID;
                            FieldTypeId = c.Attributes["ftid"];
                            FieldLibraryId = Convert.ToInt32(c.Attributes["flid"]);
                            ContractQAId = Convert.ToInt32(c.Attributes["ContractQAId"]);
                            IsUpdated = Convert.ToChar(c.Attributes["IsUpdated"]);

                            dt.Rows.Add(SEQ, ContractQAId, FieldLibraryId, Question, InputValue, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, IsUpdated);
                            SEQ += 1;
                        }

                        foreach (var ddlList in parentctrl.Controls.OfType<HtmlSelect>())
                        {
                            ID = ddlList.ID;
                            FieldTypeId = ddlList.Attributes["ftid"];
                            FieldLibraryId = Convert.ToInt32(ddlList.Attributes["flid"]);
                            ContractQAId = Convert.ToInt32(ddlList.Attributes["ContractQAId"]);
                            IsUpdated = Convert.ToChar(ddlList.Attributes["IsUpdated"]);
                            InputValue = null;

                            foreach (ListItem objItem in ddlList.Items)
                            {
                                if (objItem.Selected)
                                {
                                    InputValue += objItem.Value + ",";
                                }
                            }
                            dt.Rows.Add(SEQ, ContractQAId, FieldLibraryId, Question, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, InputValue, IsUpdated);
                            SEQ += 1;
                        }

                        foreach (var cbList in parentctrl.Controls.OfType<CheckBoxList>())
                        {
                            ID = cbList.ID;
                            FieldTypeId = cbList.Attributes["ftid"];
                            FieldLibraryId = Convert.ToInt32(cbList.Attributes["flid"]);
                            ContractQAId = Convert.ToInt32(cbList.Attributes["ContractQAId"]);
                            IsUpdated = Convert.ToChar(cbList.Attributes["IsUpdated"]);
                            InputValue = null;
                            foreach (ListItem objItem in cbList.Items)
                            {
                                if (objItem.Selected)
                                {
                                    InputValue += objItem.Value + ",";
                                }
                            }
                            dt.Rows.Add(SEQ, ContractQAId, FieldLibraryId, Question, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, InputValue, IsUpdated);
                            SEQ += 1;
                        }

                        foreach (var rbList in parentctrl.Controls.OfType<RadioButtonList>())
                        {
                            ID = rbList.ID;
                            FieldTypeId = rbList.Attributes["ftid"];
                            FieldLibraryId = Convert.ToInt32(rbList.Attributes["flid"]);
                            ContractQAId = Convert.ToInt32(rbList.Attributes["ContractQAId"]);
                            IsUpdated = Convert.ToChar(rbList.Attributes["IsUpdated"]);
                            InputValue = rbList.SelectedValue;
                            dt.Rows.Add(SEQ, ContractQAId, FieldLibraryId, Question, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, InputValue, IsUpdated);
                            SEQ += 1;
                        }
                    }
                }
           


            #endregion

            QuestionAnswer QA = new QuestionAnswer();
            QA.ContractRequestId = RequestId;
            QA.ContractId = Convert.ToInt32(tbl.Attributes["ContractId"]);
            QA.ContractTemplateId =Convert.ToInt32(ContractTemplateId);
            QA.Answers = dt;
            QA.AddedBy = UserId;
            QA.ModifiedBy = UserId;
            QA.IpAddress = IP;
            QA.IsDraft = IsDraft;

            string flag = QA.InsertRecordFromBulkImport();

            #endregion
            if (flag != "0")
                return flag;
            else
                return "";
            }
            catch (Exception ex)
            {
                QuestionAnswer QA = new QuestionAnswer();
                QA.ContractRequestId = RequestId;
                QA.ContractTemplateId = Convert.ToInt32(ContractTemplateId);
                QA.AddedBy = UserId;
                QA.ModifiedBy = UserId;
                QA.IpAddress = IP;
                QA.IsDraft = IsDraft;
                string flag = QA.InsertRecordFromBulkImport();
                return flag;
            }
        }                

        public List<SelectControlFields> FillMasterData(string tableName, string FieldTypeId)
        {
            ICommonMaster obj = FactoryMaster.GetCommonMasterDetail();
            obj.TableName = tableName;
            List<SelectControlFields> myList = FieldTypeId == VariableCollection.MULTIMASTERS || FieldTypeId == VariableCollection.MULTIVALUES ? obj.SelectDataNoSelect() : obj.SelectData();
            return myList;
        }

        public AsposeWord.Document GetWordDoc(string FilePath, int ContractTemplateId)
        {
            Control ctrl = null;
            AsposeWord.Document oWordDoc;

            oWordDoc = new AsposeWord.Document(FilePath);
            XmlDocument VariableDocument = new XmlDocument();

            FieldLibraryOperations fo = new FieldLibraryOperations();
            fo.ContractTemplateId = Convert.ToInt32(ContractTemplateId);
            fo.ContractId = ContractId;
            fo.RequestId = RequestId;
            fo.RequestMode = 1;
            VariableDocument.LoadXml(fo.GetQuestionnairXML());
            //VariableDocument.Load("C:\\QuetionnaireXML.xml");
            XmlNodeList Variable = VariableDocument.GetElementsByTagName("Variable");

            string InputValue = "";
            foreach (XmlElement ele in Variable)
            {
                string val = ele.Attributes.GetNamedItem(VariableCollection.Id).Value;
                string ID = "", question = "", FieldTypeId = "", FieldName = "";
                InputValue = "";
                string AttachedClauses = "", BookMarkName = "", ParagraphSeparator = "";

                if (ele.GetElementsByTagName(VariableCollection.FieldTypeId)[0] != null)
                {
                    FieldTypeId = ele.GetElementsByTagName(VariableCollection.FieldTypeId)[0].InnerText;
                    //VarControlType = ele.GetElementsByTagName(VariableCollection.Type)[0].InnerText;
                    question = ele.GetElementsByTagName(VariableCollection.Question)[0] == null ? "" : ele.GetElementsByTagName(VariableCollection.Question)[0].InnerText;
                    ID = ele.Attributes[0].Value;
                    FieldName = ele.Attributes[1].Value;
                    InputValue = ele.GetElementsByTagName(VariableCollection.AnswerText)[0] == null ? null : ele.GetElementsByTagName(VariableCollection.AnswerText)[0].InnerText;
                    string strToReplace = "";
                    if (!string.IsNullOrEmpty(InputValue))
                    {
                        InputValue = InputValue.TrimEnd(',');
                        //FindAndReplace(oWordDoc, "##" + FieldName + "##", InputValue);
                        
                        AttachedClauses = ele.GetElementsByTagName(VariableCollection.AttachedClauses)[0] == null ? null : ele.GetElementsByTagName(VariableCollection.AttachedClauses)[0].InnerText;
                        if (FieldTypeId != "16")
                            strToReplace = InputValue;

                        //AsposeWord.Paragraph curPar = ReplaceEvaluatorFindAndInsertText.CURRENTPARA;
                        //bool IsInCell = curPar.IsInCell;
                        //AsposeWord.Tables.Table t = curPar.ParentSection.Body.Tables[0];

                        if (AttachedClauses != null)
                        {
                            string[] AttachedClauseArray = { "" };
                            ParagraphSeparator = ele.GetElementsByTagName(VariableCollection.ParagraphSeparator)[0].InnerText;
                            if (AttachedClauses.Contains(ParagraphSeparator))
                                AttachedClauseArray = Regex.Split(AttachedClauses, ParagraphSeparator);
                            else
                                AttachedClauseArray[0] = AttachedClauses;
                            AsposeWord.DocumentBuilder docbuilder = new AsposeWord.DocumentBuilder(oWordDoc);
                            BookMarkName = ele.GetElementsByTagName(VariableCollection.BookMarks)[0] == null ? null : ele.GetElementsByTagName(VariableCollection.BookMarks)[0].InnerText;                            
                           
                            for (int clauseIndex = 0; clauseIndex < AttachedClauseArray.Length; clauseIndex++)
                            {
                                //if (!string.IsNullOrEmpty(BookMarkName))
                                //{
                                //    //docbuilder.MoveToBookmark(BookMarkName, false, true);
                                //    AsposeWord.BookmarkCollection bk = oWordDoc.Range.Bookmarks;
                                // AsposeWord.Paragraph curPar = ReplaceEvaluatorFindAndInsertText.CURRENTPARA;                                
                                //    AsposeWord.Paragraph p = new AsposeWord.Paragraph(oWordDoc);

                                //    if (curPar.ParentNode != null)
                                //        curPar.ParentNode.InsertAfter(docbuilder.InsertParagraph(), curPar);
                                //    else
                                //        docbuilder.InsertParagraph();
                                //}
                                //else
                                //{
                                //    docbuilder.MoveToDocumentEnd();
                                //}
                                if (clauseIndex == 0)
                                    strToReplace = "";
                                strToReplace += AttachedClauseArray[clauseIndex].Replace("<br/>", "\n").Replace("<br>", "\n").Replace("<br />", "\n").Replace("&lt;br&gt;", "\n").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&#09;", "\t");// +"\n";
                                //docbuilder.Write();                                
                            }                           
                        }
                        FindAndReplace(oWordDoc, "##" + FieldName + "##", strToReplace);
                    }
                }
                //string std = "Evaluation Only. Created with Aspose.Words. Copyright 2003-2014 Aspose Pty Ltd.";
                //FindAndReplace(oWordDoc, std, string.Empty);
            }
            oWordDoc.Variables.Add("ContractDocument", "ORIGINAL");
            return oWordDoc;
        }

        public AsposeWord.Document CreateWordDoc(string FilePath, int ContractTemplateId)
        {
            Control ctrl = null;
            AsposeWord.Document oWordDoc = new AsposeWord.Document();
            XmlDocument VariableDocument = new XmlDocument();

            FieldLibraryOperations fo = new FieldLibraryOperations();
            fo.ContractTemplateId = Convert.ToInt32(ContractTemplateId);
            fo.ContractId = ContractId;
            fo.RequestId = RequestId;
            fo.RequestMode = 1;
            VariableDocument.LoadXml(fo.GetQuestionnairXML());
            //VariableDocument.Load("C:\\QuetionnaireXML.xml");
            XmlNodeList Variable = VariableDocument.GetElementsByTagName("Variable");
            AsposeWord.DocumentBuilder docbuilder = new AsposeWord.DocumentBuilder(oWordDoc);

            foreach (XmlElement ele in Variable)
            {
                string val = ele.Attributes.GetNamedItem(VariableCollection.Id).Value;
                string ID = "", Question = "", Answer = "", FieldTypeId = "", FieldName = "";

                if (ele.GetElementsByTagName(VariableCollection.FieldTypeId)[0] != null)
                {
                    FieldTypeId = ele.GetElementsByTagName(VariableCollection.FieldTypeId)[0].InnerText;
                    ID = ele.Attributes[0].Value;
                    FieldName = ele.Attributes[1].Value;
                    Question = ele.GetElementsByTagName(VariableCollection.Question)[0] == null ? "" : ele.GetElementsByTagName(VariableCollection.Question)[0].InnerText;
                    Answer = ele.GetElementsByTagName(VariableCollection.AnswerText)[0] == null ? null : ele.GetElementsByTagName(VariableCollection.AnswerText)[0].InnerText;

                    if (!string.IsNullOrEmpty(Answer))
                    {
                        Answer = Answer.TrimEnd(',');
                        
                        // Set font formatting properties
                        Aspose.Words.Font font = docbuilder.Font;                        
                        //font.Color = System.Drawing.Color.DarkBlue;
                        //font.Italic = true;
                        //font.Name = "Arial";
                        //font.Size = 24;
                        //font.Spacing = 5;
                        //font.Underline = Underline.Double;
                        font.Bold = true;
                        docbuilder.Writeln(Question);

                        font.Bold = false;
                        docbuilder.Writeln(Answer);

                        docbuilder.Writeln();
                    }
                }               
            }
            oWordDoc.Variables.Add("ContractDocument", "ORIGINAL");
            return oWordDoc;
        }

        #region Not in use.
        #region GetMasterDataList
        //public List<MasterData> GetMasterDataList(string tableName)
        //{
        //    SqlDataReader rdr;
        //    SqlCommand cmd = new SqlCommand();
        //    string  constr = ConfigurationManager.ConnectionStrings["connString"].ConnectionString;
        //    SqlConnection con = new SqlConnection( constr);
        //    List<MasterData> MasterList = new List<MasterData>();
        //    try
        //    {
        //        con.Open();
        //        cmd.Connection = con;
        //        cmd.CommandTimeout = 0;
        //        cmd.CommandText = "CredentialsGetMasterData";
        //        cmd.Parameters.AddWithValue("@TableName", tableName);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

        //        if (rdr.HasRows)
        //        {
        //            while (rdr.Read())
        //            {
        //                MasterList.Add(new MasterData { Id = Convert.ToInt32(Regex.Split(rdr["Id"].ToString(), "##")[0]), Name = rdr["Name"].ToString() });
        //            }
        //        }
        //        con.Close();

        //    }
        //    catch (SqlException ex)
        //    {
        //        throw ex;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        if (cmd != null) { cmd.Dispose(); }
        //        con.Close();
        //    }
        //    return MasterList;
        //}
        #endregion
        #region GetClosestParentControl
        //private Control GetClosestParentControl(Control ctrl)
        //{
        //    Control cntrl = ctrl;
        //    if (!cntrl.GetType().ToString().Contains("HtmlTableCell"))
        //        return GetClosestParentControl(cntrl.Controls[0]);
        //    else
        //        return cntrl.Parent;
        //}
        #endregion
        //public AsposeWord.Document GetWordDoc(string FilePath, Control Userctrl)
        //{
        //    Control ctrl = Userctrl;
        //    AsposeWord.Document oWordDoc = null;
        //    //if (SaveAnswers(ctrl))
        //    {
        //        oWordDoc = new AsposeWord.Document(FilePath);
        //        AsposeWord.VariableCollection DocVariables = oWordDoc.Variables;
        //        AsposeWord.Range rng = oWordDoc.Range;

        //        XmlDocument VariableDocument = GetXML(DocVariables);
        //        XmlNodeList Variable = VariableDocument.GetElementsByTagName("Variable");

        //        string InputValue = "";
        //        foreach (XmlElement ele in Variable)
        //        {
        //            string val = ele.Attributes.GetNamedItem(VariableCollection.Id).Value;
        //            string ID = "", VarControlType = "", question = "", FieldTypeId = "", FieldName = "";
        //            InputValue = "";
        //            if (ele.GetElementsByTagName(VariableCollection.FieldTypeId)[0] != null)
        //            {
        //                FieldTypeId = ele.GetElementsByTagName(VariableCollection.FieldTypeId)[0].InnerText;
        //                VarControlType = ele.GetElementsByTagName(VariableCollection.Type)[0].InnerText;
        //                question = ele.GetElementsByTagName(VariableCollection.Question)[0] == null ? "" : ele.GetElementsByTagName("Question")[0].InnerText;
        //                ID = ele.Attributes[0].Value;
        //                FieldName = ele.Attributes[1].Value;

        //                #region GetControlValue
        //                switch (FieldTypeId.ToUpper())
        //                {
        //                    case VariableCollection.TEXT:
        //                    case VariableCollection.ALPHANUMERIC:
        //                    case VariableCollection.NUMBERS:
        //                    case VariableCollection.CURRENCY:
        //                    case VariableCollection.LETTER:
        //                    case VariableCollection.DATE:
        //                        if (ctrl.FindControl(ID) != null)
        //                            InputValue = ((HtmlInputText)ctrl.FindControl(ID)).Value;
        //                        break;
        //                    case VariableCollection.MULTILINE:
        //                        if (ctrl.FindControl(ID) != null)
        //                            InputValue = ((HtmlTextArea)ctrl.FindControl(ID)).Value;
        //                        break;
        //                    case VariableCollection.CBVALUES:
        //                    case VariableCollection.CBMASTERS:
        //                        if (ctrl.FindControl(ID) != null)
        //                        {
        //                            CheckBoxList cblist = (System.Web.UI.WebControls.CheckBoxList)ctrl.FindControl(ID);
        //                            foreach (ListItem objItem in cblist.Items)
        //                            {
        //                                if (objItem.Selected)
        //                                {
        //                                    InputValue += objItem.Text + ",";
        //                                }
        //                            }
        //                        }
        //                        break;
        //                    case VariableCollection.RBVALUES:
        //                    case VariableCollection.RBMASTERS:
        //                        if (ctrl.FindControl(ID) != null)
        //                            InputValue = ((System.Web.UI.WebControls.RadioButtonList)ctrl.FindControl(ID)).SelectedValue;
        //                        break;
        //                    case VariableCollection.SINGLEVALUES:
        //                    case VariableCollection.MULTIVALUES:
        //                    case VariableCollection.SINGLEMASTERS:
        //                    case VariableCollection.MULTIMASTERS:
        //                        if (ctrl.FindControl(ID) != null)
        //                        {
        //                            HtmlSelect ddlList = (System.Web.UI.HtmlControls.HtmlSelect)ctrl.FindControl(ID);
        //                            if (ddlList != null)
        //                            {
        //                                foreach (ListItem objItem in ddlList.Items)
        //                                {
        //                                    if (objItem.Selected)
        //                                    {
        //                                        InputValue += objItem.Text + ",";
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        break;
        //                }
        //                #endregion
        //                //InputValue = InputValue.Replace("\r\n", "/\r/\n");
        //                InputValue = InputValue.TrimEnd(',');
        //                FindAndReplace(oWordDoc, "##" + FieldName + "##", InputValue);
        //            }
        //            string std = "Evaluation Only. Created with Aspose.Words. Copyright 2003-2014 Aspose Pty Ltd.";
        //            FindAndReplace(oWordDoc, std, string.Empty);
        //        }
        //    }
        //    oWordDoc.Variables.Add("ContractDocument", "ORIGINAL");
        //    return oWordDoc;
        //}
        #endregion
    }

    public class MasterData
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class VariableCollection
    {
        public const string Key_SIGNATURE = "SIGNATURE";
        public const string Value_SIGNATURE = "USIL";

        #region Variables
        public const string Id = "ID";
        public const string FieldName = "FieldName";
        public const string Type = "Type";
        public const string Question = "Question";
        public const string Answer = "Answer";
        public const string AnswerText = "AnswerText";
        public const string TextType = "TextType";
        public const string DefaultSize = "DefaultSize";
        public const string Options = "Options";
        public const string FieldLibraryId = "FieldLibraryID";
        public const string Master = "Master";
        public const string FieldTypeId = "TypeID";
        public const string ToolTip = "Bubble";
        public const string ClauseId = "ClauseID";
        public const string ClauseFieldID = "ClauseFieldID";
        public const string MasterId = "MasterID";
        public const string ListSize = "5";
        public const string RepeatColumns = "7";
        public const string ContractTypeId = "ContractTypeID";
        public const string VersionID = "VersionID";
        public const string ParentClauseId = "ParentClauseID";
        public const string IsRequired = "IsRequired";
        public const string IsRevisionField = "IsRevisionField";
        public const string IsRenewField = "IsRenewField";
        public const string IsTerminateField = "IsTerminateField";
        public const string AttachedClauses = "AttachedClauses";
        public const string BookMarks = "BookMarks";
        public const string ParagraphSeparator = "ParagraphSeparator";
        public const string Sequence = "Sequence";
        public const string ClauseSequence = "Sequence";
        public const string IsTable = "Table";
        #endregion

        #region Field Datatypes
        public const string TEXT = "1";
        public const string ALPHANUMERIC = "2";
        public const string LETTER = "3";
        public const string NUMBERS = "4";
        public const string CURRENCY = "5";
        public const string MULTILINE = "6";
        public const string DATE = "7";
        public const string SINGLEVALUES = "8";
        public const string SINGLEMASTERS = "9";
        public const string MULTIVALUES = "10";
        public const string MULTIMASTERS = "11";
        public const string CBVALUES = "12";
        public const string CBMASTERS = "13";
        public const string RBVALUES = "14";
        public const string RBMASTERS = "15";
        public const string CONDITIONALVALUES = "16";
        public const string FILEUPLOAD = "FILE UPLOAD";
        public const string TABLES = "20";

        #endregion

    }
}