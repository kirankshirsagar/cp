﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocumentOperationsBLL
{
    public class FactoryAssembly
    {
        public static IContractProcedures RequestDocumentsDetails()
        {
            return new ContractProcedures();
        }
        public static IContractProcedures DeleteContractFile()
        {
            return new ContractProcedures();
        }
        public static IDocuSign DocusignDetails()
        {
            return new DocuSign();
        }

        public static ITrackDocumentChanges GetTrackDocumentChangesObject()
        {
            return new TrackDocumentChanges();
        }
    }
}
