﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonBLL;
using CrudBLL;
using System.Data;
using System.Data.SqlClient;

namespace DocumentOperationsBLL
{
    public interface IQuestionsAnswers : ICrud
    {
        int ContractRequestId { get; set; }
        int ContractQAId { get; set; }
        int ContractId { get; set; }
        int ContractTemplateId { get; set; }
        DataTable Answers { get; set; }
        string ModifiedByUserName { get; set; }
        string ModifiedOnFormattedDate { get; set; }
        string Question { get; set; }
        string Answer { get; set; }
        string TemplateName { get; set; }
        bool IsDraft { get; set; }
        bool isContractGenrate { get; set; }
    }
}
