﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using DocumentOperationsBLL;
using CommonBLL;
namespace DocumentOperationsBLL
{
   public class DocuSignDAL:DAL
    {
       public string SaveDetails(IDocuSign I)
        {
            Parameters.AddWithValue("@ContractID", I.ContractID);
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@Name", I.Name);
            Parameters.AddWithValue("@Email", I.Email);
            Parameters.AddWithValue("@EnvelopeID", I.EnvelopeID);
            Parameters.AddWithValue("@ContractFileId", I.ContractFileId);
            Parameters.AddWithValue("@DocStatus", I.DocStatus);
            Parameters.AddWithValue("@Addedby", I.Addedby);
            Parameters.AddWithValue("@Status", "0");
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("[InsertDocusignDetails]", "@Status");
        }
       public string UpdateDetails(IDocuSign I)
        {
            Parameters.AddWithValue("@Name", I.Name);
            Parameters.AddWithValue("@Email", I.Email);
            Parameters.AddWithValue("@EnvelopeID", I.EnvelopeID);
            Parameters.AddWithValue("@DocStatus", I.DocStatus);
            Parameters.AddWithValue("@RecievedDate", I.ReceivedDate);
            Parameters.AddWithValue("@Status", "0");
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("[UpdateDocusignDetails]", "@Status");

        }

       public string ReadEnvelopeId(IDocuSign I)
       {
       
           Parameters.AddWithValue("@RequestId", I.RequestId);
  
          // Parameters["@Status"].Direction = ParameterDirection.Output;
           return getExcuteScalar("GetDocusignDetails");

       }

       public void ReadDocusignDetails(IDocuSign I)
       {

       }



    }
}
