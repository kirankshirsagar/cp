﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonBLL;
using CrudBLL;
using System.Data;
using System.Data.SqlClient;

namespace DocumentOperationsBLL
{
    public interface IFieldLibrary : ICrud, INavigation
    {
        #region FieldLibrary
        int FieldLibraryID { get; set; }
        string FieldName { get; set; }
        string FieldRealName { get; set; }
        string Question { get; set; }
        int FieldTypeID { get; set; }
        string HelpBubble { get; set; }
        int ClauseID { get; set; }
        int ClauseFieldID { get; set; }
        int ClauseLibraryID { get; set; } //for clauses without any field
        string BookMarkName { get; set; }

        DataTable FieldLibrary { get; set; }
        DataTable BookMarkDetails { get; set; }
        #endregion

        #region FieldLibraryDetails
        int FieldDetailID { get; set; }
        int FieldID { get; set; }
        char IsRequired { get; set; }
        int DefaultSize { get; set; }
        int TextFieldTypeID { get; set; }
        char IsRevisionField { get; set; }
        char IsRenewField { get; set; }
        char IsTerminateField { get; set; }
        int RequestMode { get; set; }
        #endregion

        #region FieldLibraryValues
        int FieldValueID { get; set; }
        string FieldValue { get; set; }
        int Sequence { get; set; }
        #endregion

        #region ContractQuestionsAnswers
        int RequestId { get; set; }
        int ContractId { get; set; }
        int ContractTemplateId { get; set; }        
        string Answers { get; set; }
        #endregion

        string TableName { get; set; }
    }
}
