﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using CommonBLL;

namespace DocumentOperationsBLL
{
    public class QuestionAnswerDAL :DAL
    {        
        public string InsertRecord(IQuestionsAnswers I)
        {
            Parameters.AddWithValue("@Question", I.ContractQAId);
            Parameters.AddWithValue("@ContractRequestId", I.ContractRequestId);
            Parameters.AddWithValue("@ContractId", I.ContractId);
            Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
            Parameters.AddWithValue("@Answers", I.Answers);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@IsDraft", I.IsDraft);
            Parameters.AddWithValue("@isContractGenrate", I.isContractGenrate);
            Parameters.AddWithValue("@Status", "0");
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("ContractQuestionsAnswersAddUpdate", "@Status");            
        }
        public string InsertRecordFromBulkImport(IQuestionsAnswers I)
        {
       
            Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
            Parameters.AddWithValue("@Answers", I.Answers);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            return getExcuteScalar("ManualAdd");       
        }

        

        public string UpdateRecord(IQuestionsAnswers I)
        {
            return "";
        }
        public string DeleteRecord(IQuestionsAnswers I)
        {
            return "";
        }

        public SqlDataReader CredentialsMasterData(IQuestionsAnswers I)
        {
            return  getExecuteReader("CredentialsGetMasterData @TableName='MstCity'");
            //return getExecuteReader("CredentialsGetMasterData @TableName='" + I.TableName + "'");
        }

        public List<QuestionAnswer> ContractUpdatedQuestionniarDetails(IQuestionsAnswers I)
        {
            try
            {
                int i = 0;
                List<QuestionAnswer> List = new List<QuestionAnswer>();
                Parameters.AddWithValue("@ContractId", I.ContractId);
                Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
                Parameters.AddWithValue("@RequestId", I.ContractRequestId);
                SqlDataReader dr = getExecuteReader("ContractQuestionniarDetails");
                QuestionAnswer QA=new QuestionAnswer();
                while (dr.Read())
                {
                    List.Add(new QuestionAnswer());
                    List[i].ModifiedByUserName = dr["UpdatedBy"].ToString();
                    List[i].ModifiedOnFormattedDate = dr["UpdatedOn"].ToString();
                    List[i].TemplateName = dr["TemplateName"].ToString();
                    i++;
                }
                dr.NextResult();
                i = 0;
                while (dr.Read())
                {
                    List.Add(new QuestionAnswer());
                    List[i].Question = dr["Question"].ToString();
                    List[i].Answer = dr["Answer"].ToString();
                    
                    i = i + 1;
                }

                return List;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
