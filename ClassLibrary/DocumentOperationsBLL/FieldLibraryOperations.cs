﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using CrudBLL;

//using Elmah;

namespace DocumentOperationsBLL
{
    public class FieldLibraryOperations : IFieldLibrary
    {
        #region FieldLibrary
        public int FieldLibraryID { get; set; }
        public string FieldName { get; set; }
        public string FieldRealName { get; set; }
        public string Question { get; set; }
        public int FieldTypeID { get; set; }
        public string HelpBubble { get; set; }
        public int ClauseID { get; set; }
        public int ClauseFieldID { get; set; }
        public int ClauseLibraryID { get; set; } //for clauses without any field
        public string BookMarkName { get; set; }

        public DataTable FieldLibrary { get; set; }
        public DataTable BookMarkDetails { get; set; }
        #endregion

        #region FieldLibraryDetails
        public int FieldDetailID { get; set; }
        public int FieldID { get; set; }
        public char IsRequired { get; set; }
        public int DefaultSize { get; set; }
        public int TextFieldTypeID { get; set; }

        public char IsRevisionField { get; set; }
        public char IsRenewField { get; set; }
        public char IsTerminateField { get; set; }
        public int RequestMode { get; set; }
        #endregion

        #region FieldLibraryValues
        public int FieldValueID { get; set; }
        public string FieldValue { get; set; }
        public int Sequence { get; set; }
        #endregion

        #region ContractQuestionsAnswers
        public int ContractId { get; set; }
        public int RequestId { get; set; }
        public int ContractTemplateId { get; set; }   
        public string Answers { get; set; }
        #endregion

        #region Common
        public int AddedBy { get; set; }
        public int ModifiedBy { get; set; }
        public string IpAddress { get; set; }
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Description { get; set; }
        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public string isActive { get; set; }
        #endregion

        public int Direction { get; set; }
        public string SortColumn { get; set; }
        public string TableName { get; set; }

        FieldLibraryDAL obj;
        public string DeleteRecord()
        {
            try
            {
                obj = new FieldLibraryDAL();
                return obj.DeleteRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string InsertRecord()
        {
            try
            {
                obj = new FieldLibraryDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateRecord()
        {
            try
            {
                obj = new FieldLibraryDAL();
                return obj.UpdateRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string FieldLibraryConditionalFieldsAdd()
        {
            try
            {
                obj = new FieldLibraryDAL();
                return obj.FieldLibraryConditionalFieldsAdd(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SqlDataReader CredentialsMasterData()
        {
            try
            {
                obj = new FieldLibraryDAL();
                return obj.CredentialsMasterData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetQuestionnairXML()
        {
            try
            {
                obj = new FieldLibraryDAL();
                return obj.GetQuestionnairXML(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
