﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonBLL;
using CrudBLL;
using ClauseLibraryBLL.DocuSignAPI;
using ClauseLibraryBLL.CredentialAPI;
using DocumentOperationsBLL;

using System;
using System.Collections;
using System.Configuration;
using System.ServiceModel.Description;




namespace DocumentOperationsBLL
{
    public class DocuSign : IDocuSign
    {
        DocuSignDAL obj;
       public int RequestId { get; set; }
       public int ContractID { get; set; }
       public int ContractFileId { get; set; }
       public string Name { get; set; }
       public string Email { get; set; }
       public string EnvelopeID { get; set; }
       public string SignedCopyFileName { get; set; }
       public string Status { get; set; }
       public string SendDate { get; set; }
       public string ReceivedDate { get; set; }
       public string Addedon { get; set; }
       public int Addedby { get; set; }
       public string Modifiedon { get; set; }
       public int Modifiedby { get; set; }
       public string IP { get; set; }
       public string DocStatus { get; set; }

       public string SaveDetails()
       {
           try
           {
               obj = new DocuSignDAL();
               return obj.SaveDetails(this);
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       public string UpdateDetails()
       {
           try
           {
               obj = new DocuSignDAL();
               return obj.UpdateDetails(this);
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       public void ReadDocusignDetails()
       {
           try
           {
               obj = new DocuSignDAL();
               obj.ReadDocusignDetails(this);
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }


       public string ReadEnvelopeId()
       {
           try
           {
               obj = new DocuSignDAL();
               return obj.ReadEnvelopeId(this);
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }


       protected string logoutCtrlName = "ctl00$logoutBtn";
       // protected string logoutCtrlName = "ctl00$logoutBtn";

       public APIServiceSoapClient CreateAPIProxy()
       {
           var creds = GetAPICredentials();
           var apiClient = new APIServiceSoapClient(Keys.ApiServiceSoap, creds.ApiUrl);
           if (null != apiClient.ClientCredentials)
           {
               apiClient.ClientCredentials.UserName.UserName = creds.UserName;
               apiClient.ClientCredentials.UserName.Password = creds.Password;
           }
     
           return apiClient;
       }
      


       public DocuSignSample.AccountCredentials GetAPICredentials()
       {
           var credentials = new DocuSignSample.AccountCredentials();

           credentials.AccountId = System.Configuration.ConfigurationManager.AppSettings["DocuSignAPIAccountId"];
           credentials.UserName = "[" + System.Configuration.ConfigurationManager.AppSettings["DocuSignIntegratorsKey"] + "]";
           credentials.UserName += System.Configuration.ConfigurationManager.AppSettings["DocuSignAPIUserEmail"];
           credentials.Password = System.Configuration.ConfigurationManager.AppSettings["DocuSignPassword"];
           credentials.ApiUrl = System.Configuration.ConfigurationManager.AppSettings["DocuSignAPIUrl"];
           
        
           return credentials;
       }

       //public void GoToErrorPage(string errorMessage)
       //{
       //    Session[Keys.ErrorMessage] = errorMessage;
       //    Response.Redirect("error.aspx", true);
       //}

       //public bool SettingIsSet(string settingName)
       //{
       //    // check if a value is specified in the config file
       //    return (ConfigurationManager.AppSettings[settingName] != null &&
       //            ConfigurationManager.AppSettings[settingName].Length > 0);
       //}

       //public void RequireOrDie(string[] args)
       //{
       //    // check form post for required values 
       //    // if not found redirect to errorpage
       //    var missingFields = new ArrayList();
       //    foreach (string s in args)
       //    {
       //        if (null == Request.Form[s])
       //        {
       //            missingFields.Add(s);
       //        }
       //        break;
       //    }
       //    if (missingFields.Count > 0)
       //    {
       //        GoToErrorPage("Required fields missing: " +
       //                      String.Join(", ", (string[])missingFields.ToArray(typeof(string))));
       //    }
       //}

       //public void AddEnvelopeID(string id)
       //{
       //    if (null == Session[Keys.EnvelopeIds])
       //    {
       //        Session[Keys.EnvelopeIds] = id;
       //    }
       //    else
       //    {
       //        Session[Keys.EnvelopeIds] += "," + id;
       //    }
       //}

       //public string[] GetEnvelopeIDs()
       //{
       //    if (null == Session[Keys.EnvelopeIds])
       //    {
       //        return new string[0];
       //    }
       //    var ids = Session[Keys.EnvelopeIds].ToString();
       //    return ids.Split(',');
       //}

       //public bool LoggedIn()
       //{
       //    return (null != Session[Keys.ApiAccountId] &&
       //        null != Session[Keys.ApiEmail] &&
       //        null != Session[Keys.ApiPassword] &&
       //        null != Session[Keys.ApiIkey]);
       //}



       public List<DocuSign> DocumentSignatureDetails{get;set;}



    }

  
}
