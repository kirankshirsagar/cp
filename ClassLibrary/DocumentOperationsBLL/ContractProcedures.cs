﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using CrudBLL;



namespace DocumentOperationsBLL
{
    public class ContractProcedures : IContractProcedures
    {
        public int RequestId { get; set; }
        public int ContractId { get; set; }
        public int ContractTemplateId { get; set; }
        public string ModifiedByUserName { get; set; }
        public string ModifiedOnFormattedDate { get; set; }
        public string StageName { get; set; }
        public int StageId { get; set; }

        public char isPDFFile { get; set; }
        public char isUploaded { get; set; }
        public char IsMailSent { get; set; }
        public int ContractFileId { get; set; }
        public int ContractFileActivityTypeId { get; set; }        
        public string ContractFileActivityDetails { get; set; }
        public string FileName { get; set; }
        public string FileSizeKB { get; set; }
        public string MailSubject { get; set; }
        public string MailBody { get; set; }
        public string EmailId { get; set; }

        public char IsCheckOutDone { get; set; }
        public int ContractApprovalId { get; set; }
        public char IsApproved { get; set; }
        public char IsApprovalDone { get; set; }
        public char IsActive { get; set; }
        public string Remark { get; set; }
        public string Condition { get; set; }
        public string Answer { get; set; }
        public string linkToApproval { get; set; }
        public int AssignTo { get; set; }
        public string ApprovalActivtiyDetails { get; set; }

        public string ContractRequestDocumentIds { get; set; }
        public int ContractRequestDocumentId { get; set; }
        public string DocumentTypeName { get; set; }
        public string OrigionalFileName { get; set; }
        public string ActuallFileName { get; set; }
        public string FullName { get; set; }
        public string AddedDate { get; set; }

        public int AddedBy { get; set; }
        public int ModifiedBy { get; set; }
        public string IpAddress { get; set; }
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Description { get; set; }

        public string isFromDocuSign { get; set; }
        public char IsValidDocument { get; set; }
        public string GUID { get; set; }
        public string AssignedToUserName { get; set; }
        public string IsOCR { get; set; }
        public string Approvaluri { get; set; }
        public char IsFromAI { get; set; }

        public List<ContractProcedures> StageDetails { get; set; }
        public List<ContractProcedures> StageApprovalActivity { get; set; }
        public List<ContractProcedures> StagesPendingForApproval { get; set; }

        public List<ContractProcedures> ContractFiles { get; set; }
        public List<ContractProcedures> ContractFileActivity { get; set; }

        ContractProceduresDAL obj;

        public string InsertRecord()
        {
            try
            {
                obj = new ContractProceduresDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateRecord()
        {
            try
            {
                obj = new ContractProceduresDAL();
                return obj.UpdateRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DeleteRecord()
        {
            try
            {
                obj = new ContractProceduresDAL();
                return obj.DeleteRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void PendingForApprovals()
        {
            try
            {
                obj = new ContractProceduresDAL();
                obj.PendingForApprovals(this);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }
        public void ContractVersions()
        {
            try
            {
                obj = new ContractProceduresDAL();
                obj.ContractVersions(this);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        public string InsertRecordContractApproval()
        {
            try
            {
                obj = new ContractProceduresDAL();
                return obj.InsertRecordContractApproval(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string UpdateRecordContractApproval()
        {
            try
            {
                obj = new ContractProceduresDAL();
                return obj.UpdateRecordContractApproval(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ContractProcedures> ApprovalDetails()
        {
            try
            {
                obj = new ContractProceduresDAL();
                return obj.GetRecordContractApproval(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<ContractProcedures> RequestDocumentDetails()
        {
            try
            {
                obj = new ContractProceduresDAL();
                return obj.RequestDocumentsDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DeleteContractFile()
        {
            try
            {
                obj = new ContractProceduresDAL();
                return obj.DeleteContractFile(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string FileActivityInsertRecord()
        {
            try
            {
                obj = new ContractProceduresDAL();
                return obj.FileActivityInsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}