﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using CrudBLL;



namespace DocumentOperationsBLL
{
    public class QuestionAnswer : IQuestionsAnswers
    {
        public int ContractRequestId { get; set; }
        public int ContractQAId { get; set; }
        public int ContractId { get; set; }
        public int ContractTemplateId { get; set; }
        public DataTable Answers { get; set; }
        public bool IsDraft { get; set; }
        public bool isContractGenrate { get; set; }

        public string ModifiedByUserName { get; set; }
        public string ModifiedOnFormattedDate { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string TemplateName { get; set; }

        #region Common
        public int AddedBy { get; set; }
        public int ModifiedBy { get; set; }
        public string IpAddress { get; set; }
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Description { get; set; }
        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public string isActive { get; set; }
        #endregion

        public string TableName { get; set; }

        QuestionAnswerDAL obj;
        public string DeleteRecord()
        {
            try
            {
                obj = new QuestionAnswerDAL();
                return obj.DeleteRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string InsertRecord()
        {
            try
            {
                obj = new QuestionAnswerDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public string InsertRecordFromBulkImport()
        {
            try
            {
                obj = new QuestionAnswerDAL();
                return obj.InsertRecordFromBulkImport(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        

        public string UpdateRecord()
        {
            try
            {
                obj = new QuestionAnswerDAL();
                return obj.UpdateRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SqlDataReader CredentialsMasterData()
        {
            try
            {
                obj = new QuestionAnswerDAL();
                return obj.CredentialsMasterData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<QuestionAnswer> ContractUpdatedQuestionniarDetails()
        {
            try
            {
                obj = new QuestionAnswerDAL();
                return obj.ContractUpdatedQuestionniarDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
