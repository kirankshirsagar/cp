﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;


namespace DocumentOperationsBLL
{
  public  class TrackDocumentChangesDAL : DAL
    {
        public string InsertRecord(ITrackDocumentChanges I)
        {
            try
            {
                Parameters.AddWithValue("@dtTrackLineChanges", I.dtTrackLineChanges);
                //Parameters.AddWithValue("@dtTrackFieldChanges", I.dtTrackFieldChanges);
                //Parameters.AddWithValue("@dtTrackClauseChanges", I.dtTrackClauseChanges);
                Parameters.AddWithValue("@Status", 0);
                Parameters["@Status"].Direction = ParameterDirection.Output;
                return getExcuteQuery("TrackChangesInsert", "@Status");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }





        public string UpdateRecord(ITrackDocumentChanges I)
        {
            try
            {
                //Parameters.AddWithValue("@Status", 0);
                //Parameters["@Status"].Direction = ParameterDirection.Output;
                return getExcuteQuery("ContractRequestAddUpdate", "@Status");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DeleteRecord(ITrackDocumentChanges I)
        {
            try
            {
                //Parameters.AddWithValue("@RequestIds", I.RequestIDs);
                return getExcuteQuery("MasterRequestDelete");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataTable GetContractFileDetails(ITrackDocumentChanges I)
        {
            try
            {
            Parameters.AddWithValue("@RequestId", I.RequestID);
            return getDataTable("ContractFileDetailsForTrackChanges");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetFieldLibraryAndClausesDetails(ITrackDocumentChanges I)
        {
            try
            {
                Parameters.AddWithValue("@RequestId", I.RequestID);
                return getDataSet("FieldLibraryAndClausesDetailsForTrackChanges");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    
      public string  GetAlreadySavedTrackChangesCnt(ITrackDocumentChanges I)
      {
          try
          {
              Parameters.AddWithValue("@ContractFileOldId", I.ContractFileOldId);
              Parameters.AddWithValue("@ContractFileNewId", I.ContractFileNewId);
              return getExcuteScalar("TrackChangesAlreadySavedCnt");
          }
          catch (Exception ex)
          {
              throw ex;
          }
      }
     
      public List<TrackDocumentChanges> GetTrackChangesTermsDetails(ITrackDocumentChanges I)
      {
          try
          {              
              int i = 0;
              List<TrackDocumentChanges> myList = new List<TrackDocumentChanges>();
              Parameters.AddWithValue("@ContractFileOldId", I.ContractFileOldId);
              Parameters.AddWithValue("@ContractFileNewId", I.ContractFileNewId);

              Parameters.AddWithValue("@PageNo", I.PageNo);
              Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
              Parameters.AddWithValue("@Search", I.Search);
              Parameters.AddWithValue("@SortColumn", I.SortColumn);
              Parameters.AddWithValue("@Direction", I.Direction);

              SqlDataReader dr = getExecuteReader("TrackChangesTermsDetails");
              try
              {
                  while (dr.Read())
                  {

                      myList.Add(new TrackDocumentChanges());
                      myList[i].TrackChangesFieldID = Convert.ToInt32(dr["TrackChangesFieldID"].ToString());
                      myList[i].ContractFileOldId = Convert.ToInt32(dr["ContractFileOldId"].ToString());
                      myList[i].ContractFileNewId =Convert.ToInt32(dr["ContractFileNewId"].ToString());
                      myList[i].FieldLibraryID = Convert.ToInt32(dr["FieldLibraryID"].ToString());
                      myList[i].Action = dr["Action"].ToString();
                      myList[i].FieldName = dr["FieldName"].ToString();
                      myList[i].OldFileValue = dr["OldFileValue"].ToString();
                      myList[i].NewFileValue = dr["NewFileValue"].ToString();
                      myList[i].BookmarkName = dr["BookmarkName"].ToString();
                     
                      i = i + 1;
                  }

                  if (dr.NextResult())
                  {
                      while (dr.Read())
                      {
                          I.TotalRecords = int.Parse(dr[0].ToString());
                      }
                  }
              }
              catch (Exception ex)
              {
                  throw ex;
              }
              finally
              {
                  if (dr.IsClosed != true && dr != null)
                      dr.Close();
              }
              return myList;
          }
          catch (Exception ex)
          {
              throw ex;
          }
      }


      public DataTable GetTrackChangesTermsSubDetails(ITrackDocumentChanges I)
      {
          try
          {
              Parameters.AddWithValue("@TrackChangesFieldID", I.TrackChangesFieldID);              
              return getDataTable("TrackChangesTermsSubDetails");
          }
          catch (Exception ex)
          {
              throw ex;
          }
      }





      public List<TrackDocumentChanges> GetTrackChangesClauseDetails(ITrackDocumentChanges I)
      {
          try
          {
              int i = 0;
              List<TrackDocumentChanges> myList = new List<TrackDocumentChanges>();
              Parameters.AddWithValue("@ContractFileOldId", I.ContractFileOldId);
              Parameters.AddWithValue("@ContractFileNewId", I.ContractFileNewId);

              Parameters.AddWithValue("@PageNo", I.PageNo);
              Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
              Parameters.AddWithValue("@Search", I.Search);
              Parameters.AddWithValue("@SortColumn", I.SortColumn);
              Parameters.AddWithValue("@Direction", I.Direction);

              SqlDataReader dr = getExecuteReader("TrackChangesClauseDetails");
              try
              {
                  while (dr.Read())
                  {

                      myList.Add(new TrackDocumentChanges());
                      myList[i].TrackChangesClauseID = Convert.ToInt32(dr["TrackChangesClauseID"].ToString());
                      myList[i].ContractFileOldId = Convert.ToInt32(dr["ContractFileOldId"].ToString());
                      myList[i].ContractFileNewId = Convert.ToInt32(dr["ContractFileNewId"].ToString());
                      myList[i].ClauseID = Convert.ToInt32(dr["ClauseID"].ToString());
                      myList[i].Action = dr["Action"].ToString();
                      myList[i].ClauseName = dr["ClauseName"].ToString();
                      myList[i].OldFileValue = dr["OldFileValue"].ToString();
                      myList[i].NewFileValue = dr["NewFileValue"].ToString();
                      myList[i].BookmarkName = dr["BookmarkName"].ToString();

                      i = i + 1;
                  }

                  if (dr.NextResult())
                  {
                      while (dr.Read())
                      {
                          I.TotalRecords = int.Parse(dr[0].ToString());
                      }
                  }
              }
              catch (Exception ex)
              {
                  throw ex;
              }
              finally
              {
                  if (dr.IsClosed != true && dr != null)
                      dr.Close();
              }
              return myList;
          }
          catch (Exception ex)
          {
              throw ex;
          }
      }



      public DataTable GetTrackChangesClauseSubDetails(ITrackDocumentChanges I)
      {
          try
          {
              Parameters.AddWithValue("@TrackChangesClauseID", I.TrackChangesClauseID);
              return getDataTable("TrackChangesClauseSubDetails");
          }
          catch (Exception ex)
          {
              throw ex;
          }
      }





      public List<TrackDocumentChanges> GetTrackChangesLineDetails(ITrackDocumentChanges I)
      {
          try
          {
              int i = 0;
              List<TrackDocumentChanges> myList = new List<TrackDocumentChanges>();
              Parameters.AddWithValue("@ContractFileOldId", I.ContractFileOldId);
              Parameters.AddWithValue("@ContractFileNewId", I.ContractFileNewId);

              Parameters.AddWithValue("@PageNo", I.PageNo);
              Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
              Parameters.AddWithValue("@Search", I.Search);
              Parameters.AddWithValue("@SortColumn", I.SortColumn);
              Parameters.AddWithValue("@Direction", I.Direction);

              SqlDataReader dr = getExecuteReader("TrackChangesLineDetails");
              try
              {
                  while (dr.Read())
                  {

                      myList.Add(new TrackDocumentChanges());
                      myList[i].TrackChangesID = Convert.ToInt32(dr["TrackChangesID"].ToString());
                      myList[i].ContractFileOldId = Convert.ToInt32(dr["ContractFileOldId"].ToString());
                      myList[i].ContractFileNewId = Convert.ToInt32(dr["ContractFileNewId"].ToString());
                      myList[i].LineNo = Convert.ToInt32(dr["LineNo"].ToString());
                      myList[i].Action = dr["Action"].ToString();
                      myList[i].OriginalLineText = dr["OriginalLineText"].ToString();
                      myList[i].OldFileValue = dr["OldFileValue"].ToString();
                      myList[i].NewFileValue = dr["NewFileValue"].ToString();
                      myList[i].NewFileValueToEdit = dr["NewFileValueToEdit"].ToString();
                      myList[i].StatusFlag = dr["Status"].ToString();
                      myList[i].BookmarkName = dr["BookmarkName"].ToString();

                      i = i + 1;
                  }

                  if (dr.NextResult())
                  {
                      while (dr.Read())
                      {
                          I.TotalRecords = int.Parse(dr[0].ToString());
                      }
                  }
              }
              catch (Exception ex)
              {
                  throw ex;
              }
              finally
              {
                  if (dr.IsClosed != true && dr != null)
                      dr.Close();
              }
              return myList;
          }
          catch (Exception ex)
          {
              throw ex;
          }
      }



      public DataTable GetTrackChangesLineSubDetails(ITrackDocumentChanges I)
      {
          try
          {
              Parameters.AddWithValue("@TrackChangesID", I.TrackChangesID);
              return getDataTable("TrackChangesLineSubDetails");
          }
          catch (Exception ex)
          {
              throw ex;
          }
      }










    }
}
