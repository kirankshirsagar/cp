﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using CommonBLL;

namespace DocumentOperationsBLL
{
    public class FieldLibraryDAL :DAL
    {
        public string InsertRecord(IFieldLibrary I)
        {
            Parameters.AddWithValue("@ContractId", I.ContractId);
            Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@FieldLibraryDetails", I.FieldLibrary);
            Parameters.AddWithValue("@BookMarks", I.BookMarkDetails);

            Parameters.AddWithValue("@Status", "0");

            Parameters["@Status"].Direction = ParameterDirection.Output;
            //return getExcuteQuery("[FieldLibraryAddUpdate]", "@Status");            
            return getExcuteQuery("FieldLibraryAddUpdate_TABLETYPE", "@Status");
        }
        public string UpdateRecord(IFieldLibrary I)
        {
            return "";
        }
        public string DeleteRecord(IFieldLibrary I)
        {
            return "";
        }

        public string FieldLibraryConditionalFieldsAdd(IFieldLibrary I)
        {
            Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
            Parameters.AddWithValue("@UserId", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            return getExcuteQuery("[FieldLibraryConditionalFieldsAdd]");            
        }

        public SqlDataReader CredentialsMasterData(IFieldLibrary I)
        {
            return  getExecuteReader("CredentialsGetMasterData @TableName='MstCity'");
            //return getExecuteReader("CredentialsGetMasterData @TableName='" + I.TableName + "'");
        }

        public string GetQuestionnairXML(IFieldLibrary I)
        {
            string QuestionnairXML = "";
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
            Parameters.AddWithValue("@ContractId", I.ContractId);
            Parameters.AddWithValue("@RequestMode", I.RequestMode);
            DataTable dt = getDataTable("ContractQuetionnairXML");
            //for (int i = 0; i < dt.Rows.Count; i++)
            //{
            //    QuestionnairXML += dt.Rows[i][0].ToString();
            //}
            //return "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" + QuestionnairXML;
            QuestionnairXML = dt.Rows[0][0].ToString();
            return QuestionnairXML;
        }
    }
}
