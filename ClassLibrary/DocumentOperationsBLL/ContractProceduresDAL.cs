﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;
using CommonBLL;

namespace DocumentOperationsBLL
{
    class ContractProceduresDAL : DAL
    {
        public string InsertRecord(IContractProcedures I)
        {
            try
            {
                Parameters.AddWithValue("@RequestId", I.RequestId);
                Parameters.AddWithValue("@ContractId", I.ContractId);
                Parameters.AddWithValue("@FileName", I.FileName);
                Parameters.AddWithValue("@Remark", I.Remark);
                Parameters.AddWithValue("@FileSizeKB", I.FileSizeKB);
                Parameters.AddWithValue("@UserId", I.ModifiedBy);
                Parameters.AddWithValue("@IP", I.IpAddress);
                Parameters.AddWithValue("@isUploaded", I.isUploaded);
                Parameters.AddWithValue("@isPDFFile", I.isPDFFile);
                Parameters.AddWithValue("@isFromDocuSign", I.isFromDocuSign);
                Parameters.AddWithValue("@IsApprovalDone", I.IsApprovalDone);
                Parameters.AddWithValue("@IsValidDocument", I.IsValidDocument);
                Parameters.AddWithValue("@IsOCR", I.IsOCR);
                Parameters.AddWithValue("@IsFromAI", I.IsFromAI);
                Parameters.AddWithValue("@GUID", I.GUID);
                Parameters.AddWithValue("@Status", "0");
                Parameters["@Status"].Direction = ParameterDirection.Output;
                return getExcuteQuery("ContractVersionAdd", "@Status");
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }
        public string UpdateRecord(IContractProcedures I)
        {
            return "";
        }
        public string DeleteRecord(IContractProcedures I)
        {
            return "";
        }

        public string FileActivityInsertRecord(IContractProcedures I)
        {
            try
            {
                Parameters.AddWithValue("@ContractFileId", I.ContractFileId);
                Parameters.AddWithValue("@FileActivityTypeId", I.ContractFileActivityTypeId);
                Parameters.AddWithValue("@IsMailSent", I.IsMailSent);
                Parameters.AddWithValue("@UserId", I.ModifiedBy);
                Parameters.AddWithValue("@IP", I.IpAddress);
                Parameters.AddWithValue("@Status", "0");

                Parameters["@Status"].Direction = ParameterDirection.Output;
                return getExcuteQuery("ContractFileActivityAdd", "@Status");
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public void PendingForApprovals(IContractProcedures I)
        {
            try
            {
                Parameters.AddWithValue("@ContractId", I.ContractId);
                Parameters.AddWithValue("@RequestId", I.RequestId);
                Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
                Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);                
                Parameters.AddWithValue("@GenerateContractFlag", 1);

                Parameters.AddWithValue("@Approvaluri", I.Approvaluri); 

                SqlDataReader dr = getExecuteReader("StageApproval");
                int i = 0;
                List<ContractProcedures> List = new List<ContractProcedures>();
                while (dr.Read())
                {
                    List.Add(new ContractProcedures());
                    List[i].ContractApprovalId = Convert.ToInt32(dr["ContractApprovalId"]);
                    List[i].StageName = dr["StageName"].ToString();
                    List[i].ModifiedOnFormattedDate = dr["Modifiedon"].ToString();
                    List[i].ModifiedByUserName = dr["FullName"].ToString();
                    List[i].EmailId = dr["Email"].ToString();
                    List[i].MailBody = dr["MailBody"].ToString();
                    List[i].MailSubject = dr["MailSubject"].ToString();
                    List[i].linkToApproval = dr["linkToApproval"].ToString();
                    List[i].AssignTo = Convert.ToInt32(dr["AssignedTo"]);
                    List[i].IsActive = Convert.ToChar(dr["IsActive"]);
                    List[i].IsMailSent = Convert.ToChar(dr["IsMailSend"]);     
                    i++;
                }
                I.StageDetails = List;

                List<ContractProcedures> List2 = new List<ContractProcedures>();
                dr.NextResult();
                i = 0;
                while (dr.Read())
                {
                    List2.Add(new ContractProcedures());
                    List2[i].ApprovalActivtiyDetails = dr["ApprovalActivity"].ToString();
                    List2[i].ContractApprovalId = Convert.ToInt32(dr["ContractApprovalId"]);
                    i++;
                }
                I.StageApprovalActivity = List2;

                List<ContractProcedures> List3 = new List<ContractProcedures>();
                dr.NextResult();
                i = 0;
                while (dr.Read())
                {
                    List3.Add(new ContractProcedures());
                    List3[i].StageId = Convert.ToInt32(dr["StageID"].ToString());
                    List3[i].StageName = dr["StageName"].ToString();
                    List3[i].AssignedToUserName = Convert.ToString(dr["AssignedToUser"]);
                    i++;
                }
                I.StagesPendingForApproval = List3;
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public void ContractVersions(IContractProcedures I)
        {
            try
            {
                Parameters.AddWithValue("@RequestId", I.RequestId);
                Parameters.AddWithValue("@ContractId", I.ContractId);               
                SqlDataReader dr = getExecuteReader("ContractVersions");
                int i = 0;
                List<ContractProcedures> List = new List<ContractProcedures>();
                while (dr.Read())
                {
                    List.Add(new ContractProcedures());
                    List[i].ContractFileId = Convert.ToInt32(dr["ContractFileId"]);
                    List[i].FileName = dr["FileName"].ToString();
                    List[i].ModifiedOnFormattedDate = dr["Modifiedon"].ToString();
                    List[i].ModifiedByUserName = dr["FullName"].ToString();
                    List[i].FileSizeKB = dr["FileSizeKb"].ToString();
                    List[i].IsCheckOutDone = Convert.ToChar(dr["IsCheckOutDone"]);
                    List[i].isFromDocuSign = Convert.ToString(dr["isFromDocuSign"]);
                    List[i].isPDFFile = Convert.ToChar(dr["isPDFFile"]);
                    List[i].Remark = Convert.ToString(dr["Remark"]);
                    List[i].GUID = Convert.ToString(dr["GUID"]);
                    List[i].IsValidDocument = Convert.ToChar(dr["IsValidDocument"]);
                    List[i].isUploaded = Convert.ToChar(dr["isUploaded"]);
                    i++;
                }
                I.ContractFiles = List;

                List<ContractProcedures> List2 = new List<ContractProcedures>();
                dr.NextResult();
                i = 0;
                while (dr.Read())
                {
                    List2.Add(new ContractProcedures());
                    List2[i].ContractFileActivityDetails = dr["ContractFileActivity"].ToString();
                    List2[i].ContractFileId = Convert.ToInt32(dr["ContractFileId"]);
                    i++;
                }
                I.ContractFileActivity= List2;
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public string InsertRecordContractApproval(IContractProcedures I)
        {
            try
            {
                Parameters.AddWithValue("@RequestId", I.RequestId);
                Parameters.AddWithValue("@ContractId", I.ContractId);
                Parameters.AddWithValue("@ContractApprovalId", I.ContractApprovalId);
                Parameters.AddWithValue("@IsApproved", I.IsApproved);
                Parameters.AddWithValue("@Remark", I.Remark);
                Parameters.AddWithValue("@UserId", I.ModifiedBy);
                Parameters.AddWithValue("@IP", I.IpAddress);
                Parameters.AddWithValue("@Flag", 1);
               
                return getExcuteQuery("ContractApprovalActivityAdd");
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }            
        }

        public string UpdateRecordContractApproval(IContractProcedures I)
        {
            try
            {
                Parameters.AddWithValue("@RequestId", I.RequestId);
                Parameters.AddWithValue("@ContractId", I.ContractId);
                Parameters.AddWithValue("@ContractApprovalId", I.ContractApprovalId);                
                Parameters.AddWithValue("@Flag", 3);

                return getExcuteQuery("ContractApprovalActivityAdd");
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public List<ContractProcedures> GetRecordContractApproval(IContractProcedures I)
        {
            try
            {
                //Parameters.AddWithValue("@RequestId", I.RequestId);
                //Parameters.AddWithValue("@ContractId", I.ContractId);
                Parameters.AddWithValue("@ContractApprovalId", I.ContractApprovalId);
                Parameters.AddWithValue("@Flag", 2);
                SqlDataReader dr = getExecuteReader("ContractApprovalActivityAdd");
                int i = 0;
                List<ContractProcedures> List = new List<ContractProcedures>();
                while (dr.Read())
                {
                    List.Add(new ContractProcedures());
                    List[i].Condition = dr["Conditions"].ToString();
                    //List[i].Answer = dr["Answer"].ToString();
                    List[i].IsApproved = Convert.ToChar(dr["IsApproved"]);
                    List[i].Remark = dr["Remarks"].ToString();
                    i++;
                }
                //dr.NextResult();
                //i = 0;
                //while (dr.Read())
                //{
                //    List.Add(new ContractProcedures());
                //    List[i].IsApproved = Convert.ToChar(dr["IsApproved"]);
                //    List[i].Remark = dr["Remark"].ToString();                    
                //    i++;
                //}
                
                return List;
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public List<ContractProcedures> RequestDocumentsDetails(IContractProcedures I)
        {
            try
            {
                Parameters.AddWithValue("@RequestId", I.RequestId);
                SqlDataReader dr = getExecuteReader("RequestDocumentsDetails");
                int i = 0;
                List<ContractProcedures> List = new List<ContractProcedures>();
                while (dr.Read())
                {
                    List.Add(new ContractProcedures());
                    List[i].ContractRequestDocumentId = Convert.ToInt32(dr["ContractRequestDocumentId"]);
                    List[i].DocumentTypeName = dr["DocumentTypeName"].ToString();
                    List[i].OrigionalFileName = dr["OrigionalFileName"].ToString();
                    List[i].ActuallFileName = dr["ActuallFileName"].ToString();
                    List[i].FullName = dr["FullName"].ToString();
                    List[i].AddedDate = dr["AddedDate"].ToString();
                    i++;
                }
                return List;
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public string DeleteContractFile(IContractProcedures I)
        {
            try
            {
                Parameters.AddWithValue("@ContractFileId", I.ContractFileId);
                Parameters.AddWithValue("@isFromDocuSign", I.isFromDocuSign);
                Parameters.AddWithValue("@RequestId", I.RequestId);
                Parameters.AddWithValue("@GUID", I.GUID);
                Parameters.AddWithValue("@IpAddress", I.IpAddress);
                Parameters.AddWithValue("@AddedBy", I.AddedBy);
                return getExcuteQuery("ContractVersionDelete");                
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }
    }
}
