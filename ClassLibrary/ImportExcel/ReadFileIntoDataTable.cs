﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Text.RegularExpressions;
using Excel;

namespace ImportExcel
{
    public static class ReadFileIntoDataTable
    {
        private static string ReadFileIntoHtmlFormat(string filePath)
        {
            int cnt = 0;
            StringBuilder sb = new StringBuilder();
            using (StreamReader sr = new StreamReader(filePath))
            {
                String line;
                while ((line = sr.ReadLine()) != null)
                {
                    sb.AppendLine(line);
                    cnt = cnt++;
                }
            }
            return sb.ToString();
        }

        private static DataTable GetDataSetFromCreadedFile(string filePath)
        {
            IExcelDataReader excelReader=null;
            FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);
            string extensionWithDot = Path.GetExtension(stream.Name);

            if (extensionWithDot == ".xls")
            {
                //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                 excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
            }
            else if (extensionWithDot == ".xlsx")
            {
                
                //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                 excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            }

            DataTable dt = new DataTable();
            excelReader.IsFirstRowAsColumnNames = true;
            dt = excelReader.AsDataSet().Tables[0];
            excelReader.Close();
            return dt;
        }


        public static DataTable ConvertHTMLTablesToDataTable(string filePath)
        {
            string HTML = ReadFileIntoHtmlFormat(filePath);
            DataTable dt = null;
            DataRow dr = null;
            string TableExpression = "<table[^>]*>(.*?)</table>";
            string HeaderExpression = "<th[^>]*>(.*?)</th>";
            string RowExpression = "<tr[^>]*>(.*?)</tr>";
            string ColumnExpression = "<td[^>]*>(.*?)</td>";
            bool HeadersExist = false;
            int iCurrentColumn = 0;
            int iCurrentRow = 0;

            // Get a match for all the tables in the HTML 
            MatchCollection Tables = Regex.Matches(HTML, TableExpression, RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase);

            // Loop through each table element 
            foreach (Match Table in Tables)
            {
                // Reset the current row counter and the header flag 
                iCurrentRow = 0;
                HeadersExist = false;

                // Add a new table to the DataSet 
                dt = new DataTable();

                //Create the relevant amount of columns for this table (use the headers if they exist, otherwise use default names) 
                if (Table.Value.Contains("<th"))
                {
                    // Set the HeadersExist flag 
                    HeadersExist = true;

                    // Get a match for all the rows in the table 
                    MatchCollection Headers = Regex.Matches(Table.Value, HeaderExpression, RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase);

                    // Loop through each header element 
                    foreach (Match Header in Headers)
                    {
                        dt.Columns.Add(Header.Groups[1].ToString());
                    }
                }
                else
                {
                    for (int iColumns = 1; iColumns <= Regex.Matches(Regex.Matches(Regex.Matches(Table.Value, TableExpression, RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase)[0].ToString(), RowExpression, RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase)[0].ToString(), ColumnExpression, RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase).Count; iColumns++)
                    {
                        dt.Columns.Add("Column " + iColumns);
                    }
                }


                //Get a match for all the rows in the table 

                MatchCollection Rows = Regex.Matches(Table.Value, RowExpression, RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase);

                foreach (Match Row in Rows)
                {
                    // Only loop through the row if it isn't a header row 
                    if (!(iCurrentRow == 0 && HeadersExist))
                    {
                        dr = dt.NewRow();
                        iCurrentColumn = 0;

                        // Get a match for all the columns in the row 
                        MatchCollection Columns = Regex.Matches(Row.Value, ColumnExpression, RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase |RegexOptions.IgnorePatternWhitespace);

                        foreach (Match Column in Columns)
                        {
                            dr[iCurrentColumn] = Column.Groups[1].ToString();
                            iCurrentColumn++;
                        }
                        dt.Rows.Add(dr);
                    }
                    iCurrentRow++;
                }
            }

            if (dt == null)
            {
                dt = GetDataSetFromCreadedFile(filePath); 
            }

            return ChangeDataTypeOfColumns(dt);
            
        }


        private static DataTable ChangeDataTypeOfColumns(DataTable dt)
        {
            DataTable formatDtble = dt.Clone();
            int countformat = 0;            
            int EmptyColumnCount = 0;
            foreach (DataRow dr in dt.Rows)
            {
                object[] objData = dr.ItemArray;
                EmptyColumnCount = 0;
                for (int i = 0; i < objData.Length; i++)
                {
                    var val = objData[i].ToString();
                    if (string.IsNullOrEmpty(val.ToString()))
                    {
                        EmptyColumnCount += 1;
                    }
                    if (countformat == 0)
                    {
                        formatDtble.Columns[i].DataType = typeof(string);
                    }
                        objData[i] = val.ToString();
                }
                if (EmptyColumnCount < objData.Length)
                {
                    formatDtble.Rows.Add(objData);
                }
                else
                { }
                countformat = countformat + 1;
            }
            return formatDtble;
        }


    }
}
