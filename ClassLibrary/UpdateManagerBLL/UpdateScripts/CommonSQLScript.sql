/********************************************************************************************************
OBJECT    : SQL Patch Script
OBJECTIVE : DB Patch to Check and update version of DB and do schema changes
            1. Create New Table - CP_CacheString
			2. Create SP : dbo.ContractRequestBuildCacheString, dbo.CP_CacheAllRequestID
			3. Create Function : [dbo].[fnContractRequestLatestStatus]
********************************************************************************************************/
USE CMS_ASOSCopy
GO

/*******************CREATE SCHEMA VERSION TABLE*********************************************************/
BEGIN
CREATE TABLE db_Schema_Version(ID INT IDENTITY(1,1),DB_Version_Id varchar(25),Database_Name Varchar(25), UpdatedOn Datetime)
INSERT INTO db_Schema_Version(DB_Version_Id,Database_Name,UpdatedOn)
      VALUES ('2.0.1','CMS_ASOSCopy',GETDATE())
END

/*******************CREATE CACHE TABLE - CP_ContractCache***********************************************/
BEGIN TRAN
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CP_ContractCache](
	[RequestID] [int] NULL,
	[Cache_String] [varchar](5000) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
COMMIT TRAN

/*****************************************************************************************************/

/*******************CREATE FUNCTION - fnContractRequestLatestStatus***********************************/
CREATE FUNCTION [dbo].[fnContractRequestLatestStatus]
(@ContractID INT)
RETURNS VARCHAR(100)
AS
BEGIN 
DECLARE @ReqID INT, 
        @ContractStatus VARCHAR(100)
 SET @ReqID = (SELECT Max(RequestId) FROM ContractRequest
                WHERE ContractId=@ContractID
                GROUP BY ContractID)
 SET @ContractStatus = (SELECT VCS.StatusName FROM vwContractStatus VCS
                         INNER JOIN ContractRequest CR   
						    ON CR.ContractStatusID = VCS.ContractStatusID
                         WHERE CR.RequestId =@ReqID)
 RETURN @ContractStatus;
END

/*****************************************************************************************************/

/**************************************************************************************************************
SP NAME     : ContractRequestBuildCacheString
Description : This SP is to fetch the string of values stored and searched from contract request screen
***************************************************************************************************************/
CREATE PROCEDURE ContractRequestBuildCacheString(@RequestID INT)

AS

BEGIN
DECLARE @ContractId INT
DECLARE @ContractStatus Varchar(100)

SET @ContractId=(SELECT ContractId FROM ContractRequest
               WHERE RequestId = @RequestID)

SET @ContractStatus = (SELECT [dbo].[fnContractRequestLatestStatus] (@ContractId) AS CStatus)
 IF NOT EXISTS(SELECT 'X' FROM CP_ContractCache 
               WHERE RequestId = @RequestID)




   BEGIN
   INSERT INTO CP_ContractCache--(RequestId,Cache_String)  
   SELECT RequestId,
stuff(
(
    select RequestId,CONCAT(CR.RequestId,' ',CR.ContractId,' ',CR.ClientID,' ',CTY.ContractTypeName,' ',CR.RequestDescription,
              ' ',VRT.RequestTypeName,' ',@ContractStatus,' ',MDT.DepartmentName,' ',CAD.CityName,' ',CAD.StateName,
			  ' ',MCO.CountryName,' ',MC.CurrencyName)
  FROM ContractRequest CR
  INNER JOIN MstContractType CTY           ON CTY.ContractTypeId      = CR.ContractTypeId
  INNER JOIN vwRequestType   VRT           ON CR.RequestTypeId        = VRT.RequestTypeId
   LEFT JOIN ClientAddressDetails CAD      ON CR.ClientId             = CAD.ClientId
  INNER JOIN CMSCentral.dbo.MstCountry MCO ON CAD.CountryId           = MCO.CountryId
  INNER JOIN MstDepartment MDT             ON CR.AssignToDepartmentId = MDT.DepartmentId
 -- INNER JOIN vwContractStatus MCS          ON CR.ContractStatusID     = MCS.ContractStatusID
   LEFT JOIN MstCurrency MC                ON CR.CurrencyID           = MC.CurrencyId 
    where CR.RequestId = @RequestID for XML path('')

),1,1,'') as Cache_String
FROM ContractRequest WHERE
RequestId = @RequestID--@RequestID 
---ROLLBACK TRAN
 END
  ELSE
  BEGIN
       UPDATE CP_ContractCache
	      SET Cache_String = (select 
stuff(
(
    select RequestId,CONCAT(CR.RequestId,' ',CR.ContractId,' ',CR.ClientID,' ',CTY.ContractTypeName,' ',CR.RequestDescription,
              ' ',VRT.RequestTypeName,' ',@ContractStatus,' ',MDT.DepartmentName,' ',CAD.CityName,' ',CAD.StateName,
			  ' ',MCO.CountryName,' ',MC.CurrencyName)
  FROM ContractRequest CR
  INNER JOIN MstContractType CTY           ON CTY.ContractTypeId      = CR.ContractTypeId
  INNER JOIN vwRequestType   VRT           ON CR.RequestTypeId        = VRT.RequestTypeId
   LEFT JOIN ClientAddressDetails CAD      ON CR.ClientId             = CAD.ClientId
  INNER JOIN CMSCentral.dbo.MstCountry MCO ON CAD.CountryId           = MCO.CountryId
  INNER JOIN MstDepartment MDT             ON CR.AssignToDepartmentId = MDT.DepartmentId
  --INNER JOIN vwContractStatus MCS          ON CR.ContractStatusID     = MCS.ContractStatusID
   LEFT JOIN MstCurrency MC                ON CR.CurrencyID           = MC.CurrencyId 
    where CR.RequestId = @RequestID for XML path('')
),1,1,''))


END

END

/**************************************************************************************************************************/

/*********************************************************************************************************
SP NAME     : CP_CacheAllRequestID
DESCRIPTION : Cache all the existing Request ID's into new Cache Table for better advance search
*********************************************************************************************************/

CREATE PROCEDURE CP_CacheAllRequestID

AS
BEGIN
DECLARE @LoopCounter INT , @MaxRequestId INT

 SELECT @LoopCounter = min(RequestId) , @MaxRequestId = max(RequestId) 
   FROM ContractRequest


  WHILE(@LoopCounter IS NOT NULL
    AND @LoopCounter <= @MaxRequestId)

  BEGIN
       EXEC ContractRequestBuildCacheString @LoopCounter

        SET @LoopCounter  = @LoopCounter  + 1        
    END
END

/****************************************************END************************************************/


