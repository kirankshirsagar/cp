﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClauseLibraryBLL
{
    public class ClauseFactory
    {

        public static IClauseLibrary GetContractTypes()
        {
            return new ClauseLibrary();
        }

        public static IClauseLibrary GetFieldDataTypes()
        {
            return new ClauseLibrary();
        }
        public static IClauseFields GetFieldReference()
        {
            return new ClauseFields();
        }

    }
}
