﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using CrudBLL;
using CommonBLL;

namespace ClauseLibraryBLL
{
    public class ClauseLibrary : IClauseLibrary, ICrudClauseLibrary
    {
        ClauseLibraryDAL obj;
        public int CluaseID { get; set; }
        public string ClauseName { get; set; }
        public int ContractTypeId { get; set; }
        public bool isActive { get; set; }
        public string VersionID { get; set; }
        public string VersionName { get; set; }
        public string Language { get; set; }
        public string Addedon { get; set; }
        public int Addedby { get; set; }
        public string Modifiedon { get; set; }
        public int Modifiedby { get; set; }
        public string IP { get; set; }
        public string Search { get; set; }
        public string IsUsed { get; set; }
        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }

        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }

        public string IpAddress { get; set; }
        public string Description { get; set; }
        public DateTime AddedOn { get; set; }
        public string AddedBy { get; set; }

        public string FieldID { get; set; }
        public string FieldName { get; set; }
        public string isNewVersion { get; set; }
        public int ParentID { get; set; }
        public string ContractName { get; set; }
        public int Direction { get; set; }
        public string SortColumn { get; set; }


        public int RequestId { get; set; }
        public int DocumentTypeId { get; set; }
        public string FilePath { get; set; }
        public string OrigionalFileName { get; set; }
        public string ContractRequestDocumentID { get; set; }
        public string ClauseIds { get; set; }
        public string VariablesInUseIds { get; set; }
        public int UserID { get; set; }
        public string DeleteDocumentFile()
        {

            try
            {
                obj = new ClauseLibraryDAL();
                return obj.DeleteDocumentFile(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



        public string CheckDuplicateClause()
        {
            try
            {
                obj = new ClauseLibraryDAL();
                return obj.CheckDuplicateClause(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        }
        public string DeleteRecord()
        {
            try
            {
                obj = new ClauseLibraryDAL();
                return obj.DeleteRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string InsertRecord()
        {
            try
            {
                obj = new ClauseLibraryDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        public string UpdateRecord()
        {
            try
            {
                obj = new ClauseLibraryDAL();
                return obj.UpdateRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateClauseLanguage()
        {
            try
            {
                obj = new ClauseLibraryDAL();
                return obj.UpdateClauseLanguage(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string DeleteClauseField()
        {
            try
            {
                obj = new ClauseLibraryDAL();
                return obj.DeleteClauseField(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        }


        public List<ClauseLibrary> ReadData()
        {
            try
            {
                obj = new ClauseLibraryDAL();
                return obj.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public List<ClauseLibrary> ReadClauseDataData()
        {
            try
            {
                obj = new ClauseLibraryDAL();
                return obj.ReadClauseDataData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<ClauseLibrary> ReadClauseFieldData()
        {
            try
            {
                obj = new ClauseLibraryDAL();
                return obj.ReadClauseFieldData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        



        public List<SelectControlFields> SelectContractType()
        {
            try
            {
                obj = new ClauseLibraryDAL();
                return obj.SelectData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields> SelectFieldDataType()
        {
            try
            {
                obj = new ClauseLibraryDAL();
                return obj.SelectFieldData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }



        }


        public List<SelectControlFields> SelectDocumentType()
        {
            try
            {
                obj = new ClauseLibraryDAL();
                return obj.SelectDocumentType(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string InsertDocumentTypeRecord()
        {
            try
            {
                obj = new ClauseLibraryDAL();
                return obj.InsertDocumentTypeRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        public List<SelectControlFields> SelectMasterTables()
        {
            try
            {
                obj = new ClauseLibraryDAL();
                return obj.SelectMasterTableData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }



        }


        public List<SelectControlFields> SelectClauseName()
        {
            try
            {
                obj = new ClauseLibraryDAL();
                return obj.SelectMasterClauseName(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }



        }


    }
}
