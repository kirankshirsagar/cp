﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClauseLibraryBLL
{
    public interface ICrudClauseLibrary
    {
        /// <summary>
        /// This mehtod is used to insert data to the database and returns string.
        /// </summary>
        /// <returns>"1" Success; "2" Duplicate; "0" Fail </returns>
        string InsertRecord();

        /// <summary>
        /// This mehtod is used to update data to the database and returns string.
        /// </summary>
        /// <returns>"1" Success; "2" Duplicate; "0" Fail </returns>
        string UpdateRecord();

        /// <summary>
        /// This mehtod is used to delete data to the database and returns string.
        /// </summary>
        /// <returns>"1" Success; "0" Fail </returns>
        string DeleteRecord();
        string InsertDocumentTypeRecord();

        string UpdateClauseLanguage();


    }
}
