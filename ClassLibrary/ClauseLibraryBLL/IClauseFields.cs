﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClauseLibraryBLL
{
    public interface IClauseFields 
    {
        int FieldSequence { get; set; }

        int ClauseID { get; set; }
        int ClauseFieldID { get; set; }
        string CustomFieldName { get; set; }
        string CustomFieldDataTypeId { get; set; }
        string DefaultValue { get; set; }
        string FieldHelpBubble { get; set; }
        string isRequired { get; set; }
        //  char isRequiredString { get; set; }
        int DefaultSize { get; set; }
        string DefaultDate { get; set; }
        string CssClass { get; set; }
        string FromName { get; set; }


        //Table Name : ClauseFields
        string FieldName { get; set; }
        string Question { get; set; }
        int FieldTypeID { get; set; }
        string HelpBubble { get; set; }
        string Addedon { get; set; }
        int Addedby { get; set; }
        string Modifiedon { get; set; }
        int Modifiedby { get; set; }
        string IP { get; set; }

        //Table Name : ClauseFieldsDetails


        string ValidationMessage { get; set; }
        // int DefaultSize {get;set;}
        int TextFieldTypeID { get; set; }


        //Table Name : ClauseFieldValues

        string ClauseFieldValue { get; set; }
        int Sequence { get; set; }
        string InsertRecord();

        string isRevise { get; set; }
        string isRenew { get; set; }
        string isTerminate { get; set; }

        string FieldAttachedClasues { get; set; }

        List<ClauseFields> ReadClauseField{get;set;}
        List<ClauseFields> ReadClauseFieldDetails { get; set; }
        List<ClauseFields> ReadClauseFieldValue { get; set; }
        void ReadClauseFieldDetailsandValues();
        string UpdateFieldRecord();

        int GetMaxClauseFieldSequence();
    }
}
