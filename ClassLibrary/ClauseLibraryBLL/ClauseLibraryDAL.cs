﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using MasterBLL;
using CommonBLL;
using ClauseLibraryBLL;
using System.Data;
using System.Data.SqlClient;

namespace ClauseLibraryBLL
{
    public class ClauseLibraryDAL : DAL
    {


        public string UpdateFieldRecord(IClauseFields I)
        {

           // Parameters.AddWithValue("@ClauseID", I.ClauseID);
            Parameters.AddWithValue("@ClauseFieldID", I.ClauseFieldID);
            Parameters.AddWithValue("@FieldName", I.FieldName);
            Parameters.AddWithValue("@FieldHelpBubble", I.FieldHelpBubble);
            Parameters.AddWithValue("@FieldTypeID", I.FieldTypeID);
            Parameters.AddWithValue("@Question", I.Question);

            Parameters.AddWithValue("@isRequired", I.isRequired);
            Parameters.AddWithValue("@DefaultSize", I.DefaultSize);
            Parameters.AddWithValue("@ClauseFieldValue", I.ClauseFieldValue);
            Parameters.AddWithValue("@ValidationMessage", I.ValidationMessage);
            Parameters.AddWithValue("@TextTypeID", I.TextFieldTypeID);
            Parameters.AddWithValue("@isRevise", I.isRevise);
            Parameters.AddWithValue("@isRenew", I.isRenew);
            Parameters.AddWithValue("@isTerminate", I.isTerminate);
            Parameters.AddWithValue("@Addedby", I.Addedby);
            Parameters.AddWithValue("@Modifiedon", I.Modifiedon);
            Parameters.AddWithValue("@Modifiedby", I.Modifiedby);
            Parameters.AddWithValue("@IP", I.IP);
            Parameters.AddWithValue("@FieldAttachedClasues", I.FieldAttachedClasues);

            Parameters.AddWithValue("@Sequence", I.FieldSequence);

            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("UpdateClauseFields", "@Status");


        }


        public void ReadClauseFieldDetailsandValues(IClauseFields I)
        {
            int i = 0;
            List<ClauseFields> myList1 = new List<ClauseFields>();
            List<ClauseFields> myList2 = new List<ClauseFields>();
            List<ClauseFields> myList3 = new List<ClauseFields>();
            Parameters.AddWithValue("@ClauseFieldID", I.ClauseFieldID);
            SqlDataReader drReadClause = getExecuteReader("ReadClauseFieldsDetails");
            try
            {
                while (drReadClause.Read())
                {
                    myList1.Add(new ClauseFields());
                    myList1[i].ClauseFieldID= int.Parse(drReadClause["ClauseID"].ToString());
                    myList1[i].ClauseID = int.Parse(drReadClause["ClauseID"].ToString());
                    myList1[i].FieldName = drReadClause["FieldName"].ToString();
                    myList1[i].Question = drReadClause["Question"].ToString();
                    myList1[i].FieldTypeID =int.Parse(drReadClause["FieldTypeID"].ToString());
                    myList1[i].HelpBubble = drReadClause["HelpBubble"].ToString();
                   // myList1[i].IP = drReadClause["VersionName"].ToString();
                    myList1[i].isRevise = drReadClause["IsRevisionField"].ToString();
                    myList1[i].isTerminate =drReadClause["IsTerminateField"].ToString();
                    myList1[i].isRenew = drReadClause["IsRenewField"].ToString();

                    myList1[i].FieldSequence =Convert.ToInt32(drReadClause["Sequence"].ToString());

                    i = i + 1;
                }
                i = 0;
                while (drReadClause.NextResult())
                {
                    while (drReadClause.Read())
                    {
                        myList2.Add(new ClauseFields());
                        myList2[i].ClauseFieldID = int.Parse(drReadClause["ClauseFieldID"].ToString());
                        myList2[i].ClauseFieldID = int.Parse(drReadClause["ClauseFieldID"].ToString());
                        myList2[i].ClauseFieldValue = drReadClause["ClauseFieldValue"].ToString();
                        myList2[i].FieldAttachedClasues = drReadClause["FieldAttachedClasues"].ToString();
                        myList2[i].DefaultSize=int.Parse( drReadClause["DefaultSize"].ToString());

                        try
                        {
                            myList2[i].Sequence = int.Parse(drReadClause["Sequence"].ToString());
                        }
                        catch (Exception ex)
                        {
                            myList2[i].Sequence =1;
                        
                        }

                        i = i + 1;
                    }
                }
           

                I.ReadClauseField = myList1;
                I.ReadClauseFieldDetails = myList2;
                //I.ReadClauseFieldValue = myList3;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (drReadClause.IsClosed != true && drReadClause != null)
                    drReadClause.Close();
            }
           // return myList;
        
        
        }




        public string InsertRecord(IClauseFields I)
        {
            Parameters.AddWithValue("@ClauseID", I.ClauseID);
            Parameters.AddWithValue("@FieldName", I.FieldName);
            Parameters.AddWithValue("@FieldHelpBubble", I.FieldHelpBubble);
            Parameters.AddWithValue("@FieldTypeID", I.FieldTypeID);
            Parameters.AddWithValue("@Question", I.Question);

            Parameters.AddWithValue("@isRequired", I.isRequired);
            Parameters.AddWithValue("@DefaultSize", I.DefaultSize);
            Parameters.AddWithValue("@ClauseFieldValue", I.ClauseFieldValue);
            Parameters.AddWithValue("@ValidationMessage", I.ValidationMessage);
            Parameters.AddWithValue("@TextTypeID", I.TextFieldTypeID);
            Parameters.AddWithValue("@isRevise", I.isRevise);
            Parameters.AddWithValue("@isRenew", I.isRenew);
            Parameters.AddWithValue("@isTerminate", I.isTerminate);
            Parameters.AddWithValue("@Addedby", I.Addedby);
            Parameters.AddWithValue("@Modifiedon", I.Modifiedon);
            Parameters.AddWithValue("@Modifiedby", I.Modifiedby);
            Parameters.AddWithValue("@IP", I.IP);
            Parameters.AddWithValue("@FieldAttachedClasues", I.FieldAttachedClasues);

            Parameters.AddWithValue("@Sequence", I.FieldSequence);


            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("InsertClauseFields", "@Status");

        }

        public string InsertRecord(IClauseLibrary I)
        {
            //Parameters.AddWithValue("@CluaseID", I.CluaseID);
            Parameters.AddWithValue("@ClauseName", I.ClauseName);
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@VersionID", I.VersionID);
            Parameters.AddWithValue("@VersionName", I.VersionName);
            Parameters.AddWithValue("@Language", I.Language);
            Parameters.AddWithValue("@Addedon", I.Addedon);
            Parameters.AddWithValue("@Addedby", I.Addedby);
            Parameters.AddWithValue("@Modifiedon", I.Modifiedon);
            Parameters.AddWithValue("@Modifiedby", I.Modifiedby);
            Parameters.AddWithValue("@IP", I.IP);
            Parameters.AddWithValue("@Status", 0);
            Parameters.AddWithValue("@isNewVersion", I.isNewVersion);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("InsertClauseDetails", "@Status");

        }





        public string UpdateClauseLanguage(IClauseLibrary I)
        {

            Parameters.AddWithValue("@ClauseId", I.CluaseID);
            Parameters.AddWithValue("@ClauseName", I.ClauseName);

            Parameters.AddWithValue("@Language", I.Language);
            Parameters.AddWithValue("@Addedon", I.Addedon);
            Parameters.AddWithValue("@Addedby", I.Addedby);
            Parameters.AddWithValue("@Modifiedon", I.Modifiedon);
            Parameters.AddWithValue("@Modifiedby", I.Modifiedby);
            Parameters.AddWithValue("@IP", I.IP);
            Parameters.AddWithValue("@IsNewVersion", I.isNewVersion);
            Parameters.AddWithValue("@ParentID", I.ParentID);
            Parameters.AddWithValue("@Version", I.VersionName);
            Parameters.AddWithValue("@VariablesIdInUse", I.VariablesInUseIds);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("UpdateClauseLanguage", "@Status");

        }

        public string InsertDocumentTypeRecord(IClauseLibrary I)
        {

            //Parameters.AddWithValue("@CluaseID", I.CluaseID);
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@DocumentTypeId", I.DocumentTypeId);
            Parameters.AddWithValue("@FilePath", I.FilePath);
            Parameters.AddWithValue("@OrigionalFileName", I.OrigionalFileName);

            Parameters.AddWithValue("@Addedon", I.Addedon);
            Parameters.AddWithValue("@Addedby", I.Addedby);
            Parameters.AddWithValue("@Modifiedon", I.Modifiedon);
            Parameters.AddWithValue("@Modifiedby", I.Modifiedby);
            Parameters.AddWithValue("@IP", I.IP);
            return getExcuteScalar("InsertDocumentTypeDetails");

        }


        public string UpdateRecord(IClauseLibrary I)
        {

            Parameters.AddWithValue("@CluaseID", I.CluaseID);
            Parameters.AddWithValue("@ClauseName", I.ClauseName);
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@VersionID", I.VersionID);
            Parameters.AddWithValue("@VersionName", I.VersionName);
            Parameters.AddWithValue("@Language", I.Language);
            Parameters.AddWithValue("@Addedon", I.Addedon);
            Parameters.AddWithValue("@Addedby", I.Addedby);
            Parameters.AddWithValue("@Modifiedon", I.Modifiedon);
            Parameters.AddWithValue("@Modifiedby", I.Modifiedby);
            Parameters.AddWithValue("@IP", I.IP);

            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("InsertClauseDetails", "@Status");
        }

        public string DeleteClauseField(IClauseLibrary I)
        {
            Parameters.AddWithValue("@CluaseID", I.CluaseID);
            Parameters.AddWithValue("@FieldID", I.FieldID);
            Parameters.AddWithValue("@Language", I.Language);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("DeleteClauseField", "@Status");
        }


        public List<ClauseLibrary> ReadClauseFieldData(IClauseLibrary I)
        {

            int i = 0;
            List<ClauseLibrary> myList = new List<ClauseLibrary>();
            Parameters.AddWithValue("@ClauseID", I.CluaseID);
            SqlDataReader drReadClause = getExecuteReader("ReadClauseFieldsData");
            try
            {
                while (drReadClause.Read())
                {
                    myList.Add(new ClauseLibrary());
                    myList[i].FieldID =drReadClause["ClauseFieldID"].ToString();
                    myList[i].FieldName = drReadClause["FieldName"].ToString();
                    myList[i].ContractTypeId = int.Parse( drReadClause["ContractTypeID"].ToString());
                
                    i = i + 1;
                }

                

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (drReadClause.IsClosed != true && drReadClause != null)
                    drReadClause.Close();
            }
            return myList;


        
        }


        public List<ClauseLibrary> ReadClauseDataData(IClauseLibrary I)
        {
            int i = 0;
            List<ClauseLibrary> myList = new List<ClauseLibrary>();
            Parameters.AddWithValue("@ClauseID", I.CluaseID);
            SqlDataReader drReadClause = getExecuteReader("MasterClauseDataRead");
            try
            {
                while (drReadClause.Read())
                {
                    myList.Add(new ClauseLibrary());
                    myList[i].CluaseID = int.Parse(drReadClause["ClauseID"].ToString());
                    myList[i].ClauseName = drReadClause["ClauseName"].ToString();
                    myList[i].VersionID = drReadClause["VersionID"].ToString();
                    myList[i].VersionName = drReadClause["VersionName"].ToString();
                    myList[i].Language = drReadClause["Language"].ToString();
                    myList[i].ContractTypeId = int.Parse(drReadClause["ContractID"].ToString());
                    myList[i].ContractName = drReadClause["ContractName"].ToString();
                  
                    i = i + 1;
                }

                if (drReadClause.NextResult())
                {
                    while (drReadClause.Read())
                    {
                        I.TotalRecords = int.Parse(drReadClause[0].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (drReadClause.IsClosed != true && drReadClause != null)
                    drReadClause.Close();
            }
            return myList;
        }





        public List<ClauseLibrary> ReadData(IClauseLibrary I)
        {
            int i = 0;
            List<ClauseLibrary> myList = new List<ClauseLibrary>();
            Parameters.AddWithValue("@ClauseID", I.CluaseID);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@UserID", I.UserID);
            SqlDataReader drReadClause = getExecuteReader("MasterClauseRead");
            try
            {
                while (drReadClause.Read())
                {
                    myList.Add(new ClauseLibrary());
                    myList[i].CluaseID = int.Parse(drReadClause["ClauseID"].ToString());
                    myList[i].ClauseName = drReadClause["ClauseName"].ToString();
                    myList[i].ContractName = drReadClause["ContractName"].ToString();
                    myList[i].VersionID = drReadClause["VersionID"].ToString();
                    myList[i].VersionName = drReadClause["VersionName"].ToString();
                    myList[i].FieldID = drReadClause["FieldID"].ToString();
                    myList[i].FieldName = drReadClause["FieldName"].ToString();
                    myList[i].IsUsed = drReadClause["IsUsed"].ToString();
                    i = i + 1;
                }

                if (drReadClause.NextResult())
                {
                    while (drReadClause.Read())
                    {
                        I.TotalRecords = int.Parse(drReadClause[0].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (drReadClause.IsClosed != true && drReadClause != null)
                    drReadClause.Close();
            }
            return myList;
        }


        public string DeleteRecord(IClauseLibrary I)
        {
            Parameters.AddWithValue("@ClauseIds", I.ClauseIds);
            return getExcuteQuery("ClauseDelete");
        }

        public string DeleteDocumentFile(IClauseLibrary I)
        {
            Parameters.AddWithValue("@IpAddress", I.IP);
            Parameters.AddWithValue("@AddedBy", I.Addedby);
            Parameters.AddWithValue("@ContractDocumentID", I.ContractRequestDocumentID);
            return getExcuteQuery("DeleteContractDocuments");
        }

        public string CheckDuplicateClause(IClauseLibrary I)
        {
            Parameters.AddWithValue("@ClauseName", I.ClauseName);
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
            return getExcuteScalar("CheckDeplicateClause");
        }


        public List<SelectControlFields> SelectData(IClauseLibrary I)
        {
            Parameters.AddWithValue("@USerId", I.UserID);
            SqlDataReader dr = getExecuteReader("MasterSelectContractType");
            return SelectControlFields.SelectData(dr);
        }
        public List<SelectControlFields> SelectFieldData(IClauseLibrary I)
        {
            SqlDataReader dr = getExecuteReader("MasterSelectFieldDataType");
            return SelectControlFields.SelectData(dr);
        }

        public List<SelectControlFields> SelectMasterTableData(IClauseLibrary I)
        {
            SqlDataReader dr = getExecuteReader("MasterSelectMasterTables");
            return SelectControlFields.SelectData(dr);
        }


        public List<SelectControlFields> SelectMasterClauseName(IClauseLibrary I)
        {
            SqlDataReader dr = getExecuteReader("MasterSelectClauseName");
            return SelectControlFields.SelectData(dr);
        }
        public List<SelectControlFields> SelectDocumentType(IClauseLibrary I)
        {
            SqlDataReader dr = getExecuteReader("MasterSelectDocumentType");
            return SelectControlFields.SelectData(dr);
        }




        //*************** 21nOV2014

        public int GetMaxClauseFieldSequence(IClauseFields I)
        {
            int MaxSequence = 0;
            Parameters.AddWithValue("@ClauseID", I.ClauseID);
            DataTable dt = getDataTable("GetMaxClauseFieldSequence");

            if (dt.Rows.Count > 0)
            {
                MaxSequence = Convert.ToInt32(dt.Rows[0][0].ToString());
            }
            return MaxSequence;
        }




    }
}
