﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonBLL;
using CrudBLL;

namespace ClauseLibraryBLL
{
    public interface IClauseLibrary : ICrudClauseLibrary, INavigation
    {
        int CluaseID { get; set; }
        string ClauseName { get; set; }
        int ContractTypeId { get; set; }
        bool isActive { get; set; }
        string VersionID { get; set; }
        string VersionName { get; set; }
        string Language { get; set; }
        string Addedon { get; set; }
        int Addedby { get; set; }
        string Modifiedon { get; set; }
        int Modifiedby { get; set; }
        string IP { get; set; }
        string ContractName { get; set; }

        string IsUsed { get; set; }
        string Search { get; set; }
        string FieldID { get; set; }
        string FieldName { get; set; }
        string isNewVersion { get; set; }
        int ParentID { get; set; }


        int RequestId { get; set; }
        int DocumentTypeId { get; set; }
        string FilePath { get; set; }
        string OrigionalFileName { get; set; }
        string ContractRequestDocumentID { get; set; }
        string ClauseIds { get; set; }

        string VariablesInUseIds { get; set; }


        string DeleteDocumentFile();
        string CheckDuplicateClause();
        List<SelectControlFields> SelectMasterTables();
        List<SelectControlFields> SelectClauseName();
        List<SelectControlFields> SelectFieldDataType();
        List<SelectControlFields> SelectContractType();
        List<SelectControlFields> SelectDocumentType();
        List<ClauseLibrary> ReadData();
        List<ClauseLibrary> ReadClauseDataData();
        List<ClauseLibrary> ReadClauseFieldData();
        string DeleteClauseField();



        int UserID { get; set; }
    }
}
