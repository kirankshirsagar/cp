﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClauseLibraryBLL
{
    public class ClauseFields : IClauseFields
    {
        ClauseLibraryDAL obj;

        public int FieldSequence { get; set; }

        public int ClauseID { get; set; }
        public int ClauseFieldID { get; set; }
        public string CustomFieldName { get; set; }
        public string CustomFieldDataTypeId { get; set; }
        public string DefaultValue { get; set; }
        public string FieldHelpBubble { get; set; }
        public string isRequired { get; set; }
        public int DefaultSize { get; set; }
        public string DefaultDate { get; set; }
        public string CssClass { get; set; }
        public string FromName { get; set; }


        //Table Name : ClauseFields
        public string FieldName { get; set; }
        public string Question { get; set; }
        public int FieldTypeID { get; set; }
        public string HelpBubble { get; set; }
        public string Addedon { get; set; }
        public int Addedby { get; set; }
        public string Modifiedon { get; set; }
        public int Modifiedby { get; set; }
        public string IP { get; set; }

        //Table Name : ClauseFieldsDetails

        //   public string IsRequired { get; set; }
        public string ValidationMessage { get; set; }
        // public int DefaultSize { get; set; }
        public int TextFieldTypeID { get; set; }


        //Table Name : ClauseFieldValues

        public string ClauseFieldValue { get; set; }
        public int Sequence { get; set; }

        public string isRevise { get; set; }
        public string isRenew { get; set; }
        public string isTerminate { get; set; }
     //   public void ReadClauseFieldDetailsandValues();
        public List<ClauseFields> ReadClauseField { get; set; }
        public List<ClauseFields> ReadClauseFieldDetails { get; set; }
        public List<ClauseFields> ReadClauseFieldValue { get; set; }

        public string InsertRecord()
        {
            try
            {
                obj = new ClauseLibraryDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateFieldRecord()
        {
            try
            {
                obj = new ClauseLibraryDAL();
                return obj.UpdateFieldRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ReadClauseFieldDetailsandValues()
        {
            try
            {
                obj = new ClauseLibraryDAL();
                obj.ReadClauseFieldDetailsandValues(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        }

        public string FieldAttachedClasues { get; set; }

        
        //********** 20Nov2014 *********
        public int GetMaxClauseFieldSequence()
        {
            try
            {
                obj = new ClauseLibraryDAL();
                return obj.GetMaxClauseFieldSequence(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        }

    }
}
