﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClauseLibraryBLL
{
    public class FactoryClause
    {

        public static IClauseLibrary GetContractTypesDetails()
        {
            return new ClauseLibrary();
        }

        public static IClauseLibrary GetFieldDataTypesDetails()
        {
            return new ClauseLibrary();
        }
        public static IClauseFields GetFieldReferenceDetails()
        {
            return new ClauseFields();
        }

    }
}
