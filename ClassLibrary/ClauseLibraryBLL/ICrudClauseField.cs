﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClauseLibraryBLL
{
    public interface ICrudClauseField
    {
        string InsertFieldRecord();
    }
}
