﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DashboardBLL
{
    public interface IChart
    {
        string SenderName { get; set; }
        decimal PaymentStatus { get; set; }
        int Count { get; set; }
        string ContractTypeName { get; set; }
        string StatusName { get; set; }
        int TaskForTheDayNumber { get; set; }
        int TaskForTheWeekNumber { get; set; }
        int RequestAssignedNumber { get; set; }
        int ApprovalRequiredNumber { get; set; }
        string UserId { get; set; }
        string MonthYear { get; set; }
        int Revenue { get; set; }
        string isCustomer { get; set; }
        string Country { get; set; }
        string CountryCode { get; set; }
        decimal SupplierRevenue { get; set; }
        decimal CustomerRevenue { get; set; }
        int Color { get; set; }

        int CustomerCount { get; set; }
        int SupplierCount { get; set; }
        int TotalContracts  { get; set; }
        int DayActivities { get; set; }
        int WeekActivities { get; set; }
        int MyContracts { get; set; }
        int MyApprovals { get; set; }
        string Tenure { get; set; }
        string BaseCurrency { get; set; }
        int AwaitingsignaturesCount { get; set; }

        List<Chart> DrawPieChart();
        List<Chart> chartWorldMapData();
        List<Chart> DrawContractTypeVsContractStatus();
        List<Chart> DrawMyContrats();
        List<Chart> DrawCustomerSupplier();
        List<Chart> GetBaseCurrency();
    }
}
