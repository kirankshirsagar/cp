﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DashboardBLL
{
    public class Chart:IChart
    {
            ChartDAL obj;
            public string SenderName { get; set; }
            public decimal PaymentStatus { get; set; }
            public int Count { get; set; }
            public string ContractTypeName { get; set; }
            public string StatusName { get; set; }
            public int TaskForTheDayNumber { get; set; }
            public int TaskForTheWeekNumber { get; set; }
            public int RequestAssignedNumber { get; set; }
            public int ApprovalRequiredNumber { get; set; }
            public string UserId { get; set; }
            public string MonthYear { get; set; }
            public int Revenue { get; set; }
            public string isCustomer { get; set; }
            public string Country { get; set; }
            public string CountryCode { get; set; }
            public decimal SupplierRevenue { get; set; }
            public decimal CustomerRevenue { get; set; }
            public int Color { get; set; }
            public string BaseCurrency { get; set; }

            public int TotalContracts { get; set; }
            public int DayActivities { get; set; }
            public int WeekActivities { get; set; }
            public int MyContracts { get; set; }
            public int MyApprovals { get; set; }
            public int AwaitingsignaturesCount { get; set; }

            public int CustomerCount { get; set; }
            public int SupplierCount { get; set; }
            public string Tenure { get; set; }
            public  List<Chart> DrawPieChart()
            {
                try
                {
                    obj = new ChartDAL();
                    return obj.DrawPieChart(this);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            
            }

            public List<Chart> chartWorldMapData()
            {
                try
                {
                    obj = new ChartDAL();
                    return obj.chartWorldMapData(this);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            public List<Chart> DrawContractTypeVsContractStatus()
            {
                try
                {
                    obj = new ChartDAL();
                    return obj.DrawContractTypeVsContractStatus(this);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public List<Chart> DrawMyContrats()
            {
                try
                {
                    obj = new ChartDAL();
                    return obj.DrawMyContrats(this);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public List<Chart> DrawCustomerSupplier()
            {
                try
                {
                    obj = new ChartDAL();
                    return obj.DrawCustomerSupplier(this);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public List<Chart> GetBaseCurrency()
            {
                try
                {
                    obj = new ChartDAL();
                    return obj.GetBaseCurrency(this);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }


    }
}
