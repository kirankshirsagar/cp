﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;

namespace DashboardBLL
{
    class ChartDAL : DAL
    {
        public List<Chart> GetBaseCurrency(IChart I)
        {
            int i = 0;
            List<Chart> myList = new List<Chart>();
            SqlDataReader dr = getExecuteReader("SELECT DBO.GetBaseCurrency() AS BaseCurrency", CommandType.Text);           

            try
            {
                while (dr.Read())
                {
                    myList.Add(new Chart());
                    myList[i].BaseCurrency = dr["BaseCurrency"].ToString(); 
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
        }

        public List<Chart> DrawPieChart(IChart I)
        {
            int i = 0;
            List<Chart> myList = new List<Chart>();
            Parameters.AddWithValue("@UserId", I.UserId);//User ID
            Parameters.AddWithValue("@Tenure", I.Tenure);//Tenure
            SqlDataReader dr = getExecuteReader("DashboardByContractType");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new Chart());
                    myList[i].SenderName = dr["SenderName"].ToString();//1
                    myList[i].PaymentStatus =decimal.Parse( dr["PaymentStatus"].ToString());//2
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;

        }

        public List<Chart> chartWorldMapData(IChart I)
        {
            int i = 0;
            List<Chart> myList = new List<Chart>();
            Parameters.AddWithValue("@UserId", I.UserId);// User ID
            Parameters.AddWithValue("@Tenure", I.Tenure);// Tenure
            SqlDataReader dr = getExecuteReader("GetGeographicCSAnalysis");
            try
            {
                while (dr.Read())
                {
               
                    myList.Add(new Chart());
                    myList[i].Country = dr["CountryName"].ToString();//1
                    myList[i].CountryCode = dr["CountryCode"].ToString();//2
                    myList[i].CustomerRevenue =decimal.Parse( dr["CustomerValue"].ToString());//1
                    myList[i].SupplierRevenue = decimal.Parse(dr["SupplierValue"].ToString());//2
                    myList[i].Color = int.Parse(dr["ColorCode"].ToString());//2
                    myList[i].CustomerCount = int.Parse(dr["CustomerCount"].ToString());//2
                    myList[i].SupplierCount = int.Parse(dr["SupplierCount"].ToString());//2
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;

        }

        public List<Chart> DrawContractTypeVsContractStatus(IChart I)
        {
            int i = 0;
            List<Chart> myList = new List<Chart>();
            Parameters.AddWithValue("@UserId", I.UserId);// User Id 
            Parameters.AddWithValue("@Tenure", I.Tenure);// Tenure
            SqlDataReader dr = getExecuteReader("ContractTypeVsContractStatus");
            try
            {
                while (dr.Read())
                {
               
                    myList.Add(new Chart());
                    myList[i].Count = int.Parse( dr["ItemCount"].ToString());//1
                    myList[i].ContractTypeName = dr["ContractTypeName"].ToString();//2
                    myList[i].StatusName = dr["StatusName"].ToString();//1
            
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;

        }

        public List<Chart> DrawMyContrats(IChart I)
        {
            int i = 0;
            List<Chart> myList = new List<Chart>();
            Parameters.AddWithValue("@UserId", I.UserId);// User ID
            Parameters.AddWithValue("@Tenure", I.Tenure);//Tenure
            SqlDataReader dr = getExecuteReader("GetMyContractDashboard");
            try
            {
                while (dr.Read())
                {
               
                    myList.Add(new Chart());
                    myList[i].TotalContracts = int.Parse(dr["Totalcount"].ToString());//1
                    myList[i].WeekActivities = int.Parse( dr["WeekActivitiesCount"].ToString());//2
                    myList[i].DayActivities =int.Parse(  dr["DayActivitiesCount"].ToString());//1
                    myList[i].MyContracts = int.Parse( dr["MyContractCount"].ToString());//1
                    myList[i].MyApprovals = int.Parse( dr["MyApprovals"].ToString());//1
                    myList[i].AwaitingsignaturesCount = int.Parse(dr["AwaitingsignaturesCount"].ToString());//1
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;

        }

        public List<Chart> DrawCustomerSupplier(IChart I)
        {
            int i = 0;
            List<Chart> myList = new List<Chart>();
            Parameters.AddWithValue("@UserId", I.UserId);// User ID 
            Parameters.AddWithValue("@Tenure", I.Tenure);//Tenure
            SqlDataReader dr = getExecuteReader("GetCustomerSupplierAnalysis");
            try
            {
                while (dr.Read())
                {
             
                    myList.Add(new Chart());
                    myList[i].MonthYear = dr["MonthName"].ToString();//1
                    myList[i].CustomerRevenue = decimal.Parse(dr["CustomerRevenue"].ToString());//2
                    myList[i].SupplierRevenue = decimal.Parse(dr["SupplierRevenue"].ToString());//1
            
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;

        }


    }
}
