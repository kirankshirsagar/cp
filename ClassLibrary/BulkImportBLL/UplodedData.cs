﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace BulkImportBLL
{
    public class UplodedData :IUplodedData
    {
        UplodedDataDAL obj;
        public int BulkImportID { get; set; }
        public int ContractTemplateId { get; set; }
        public int ContractTypeId { get; set; }
        public int TotalCount { get; set; }
        public int ApprovedCount { get; set; }
        public int RejectedCount { get; set; }
        public string ContractTemplateName { get; set; }
        public string ContractTypeName { get; set; }
        public string Status { get; set; }
        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public int Direction { get; set; }
        public string SortColumn { get; set; }
        public string Search { get; set; }
        public string FileName { get; set; }
        public string ModifiedOn { get; set; }
        public string UserRole { get; set; }

        public void GetDocument(string filename, string filePath, System.Web.HttpResponse Response)
        {
            try
            {

                FileStream stream = CreateExcel.Generator.CreateExcelFile(filePath, GetUplodedInvalidData(),false);
                stream.Flush();
                stream.Position = 0;
                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";

                //  NOTE: If you get an "HttpCacheability does not exist" error on the following line, make sure you have
                //  manually added System.Web to this project's References.

                Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                Response.AddHeader("content-disposition", "attachment; filename=" + filename);


                byte[] data1 = new byte[stream.Length];
                stream.Read(data1, 0, data1.Length);
                stream.Close();
                Response.BinaryWrite(data1);
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {


            }

        }


        public DataTable GetUplodedInvalidData()
        {
            obj = new UplodedDataDAL();
            return obj.GetUplodedData(this);
        }
 
        public List<UplodedData> ReadData()
        {
            obj = new UplodedDataDAL();
            return obj.ReadData(this);
        }

        public string BulkImportProcessing()
        {
            obj = new UplodedDataDAL();
            return obj.BulkImportProcessing(this);
        }

    }
}
