﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using CommonBLL;
using System.IO;



namespace BulkImportBLL
{
    public class ContractTemplateField : IContractTemplateField
    {
        ContractTemplateFieldDAL obj;
        public int ContractTemplateId { get; set; }
        public DataTable TemplateFields { get; set; }
        public int AddedBy { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string IpAddress { get; set; }
        public string Description { get; set; }
        public List<ContractTemplate> ReadData()
        {
            throw new NotImplementedException();
        }


        public string InsertRecord()
        {
            try
            {
                obj = new ContractTemplateFieldDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateRecord()
        {
            throw new NotImplementedException();
        }

        public string DeleteRecord()
        {
            throw new NotImplementedException();
        }


        public List<SelectControlFields> SelectFieldTypeData()
        {
            try
            {
                obj = new ContractTemplateFieldDAL();
                return obj.SelectFieldTypeData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<SelectControlFields> SelectMasterData(int noSelect = 0)
        {
            try
            {
                obj = new ContractTemplateFieldDAL();
                return obj.SelectMasterData(this, noSelect);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void GetDataTableFilled(string str)
        {

            DataTable dt = new DataTable();
            dt.Columns.Add("FieldTypeID");
            dt.Columns.Add("FieldNameValue");
            dt.Columns.Add("FieldNameText");
            DataRow row;

            if (str.Trim() != string.Empty)
            {
                try
                {

                    string[] str1 = str.Split('#');
                    foreach (var item1 in str1)
                    {
                        string[] val1 = item1.Split(',');
                        row = dt.NewRow();
                        row["FieldTypeID"] = val1[0];
                        row["FieldNameValue"] = val1[1].Split('$')[0];
                        row["FieldNameText"] = val1[1].Split('$')[1];
                        dt.Rows.Add(row);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            TemplateFields = dt;
        }

        public void GetDocument(int ContractTemplateId, string filename, string filePath, System.Web.HttpResponse Response)
        {
            try
            {                
                FileStream stream = CreateExcel.Generator.CreateExcelFile(filePath,GetDocumentStructure(ContractTemplateId), true); 
                stream.Flush();
                stream.Position = 0;
                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";

                //  NOTE: If you get an "HttpCacheability does not exist" error on the following line, make sure you have
                //  manually added System.Web to this project's References.

                Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                Response.AddHeader("content-disposition", "attachment; filename=" + filename);

                byte[] data1 = new byte[stream.Length];
                stream.Read(data1, 0, data1.Length);
                stream.Close();
                Response.BinaryWrite(data1);
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {


            }

        }

        private DataTable GetDocumentStructure(int ContractTemplateId)
        {
            obj = new ContractTemplateFieldDAL();
            return obj.GetDocumentStructure(ContractTemplateId);
        }

        public string GetContractTemplateFieldRecord()
        {
            try
            {
                obj = new ContractTemplateFieldDAL();
                return obj.GetContractTemplateFieldRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}



