﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BulkImportBLL
{

  
   public class ImportData :IImportData
    {
        ImportDataDAL obj;
        public int ContractTemplateId { get; set; }
        public int AddedBy { get; set; }
        public DateTime AddedOn { get; set; }
        public string IpAddress { get; set; }
        public string InputFileName { get; set; }
        public DataTable BulkImportStaging { get; set; }
        public string UplodedColumns { get; set; }
        public string Error { get; set; }

        public string InsertRecord()
        {
            obj = new ImportDataDAL();
            return obj.InsertRecord(this);
        }

        public string CheckTemplate()
        {
            obj = new ImportDataDAL();
            return obj.CheckTemplate(this);
        }

        public List<ImportData> ReadData()
        {
            obj = new ImportDataDAL();
            return obj.ReadData(this);
        }

     

        public DataTable FillStagingTable(DataTable dtChild)
        {
            string[] columnNames = (from dc in dtChild.Columns.Cast<DataColumn>()
                                    where !dc.ColumnName.ToLower().StartsWith("column")
                                    select dc.ColumnName
                                    ).ToArray();
            

            DataTable dt = new DataTable();
            if (HasDuplicates(columnNames) == false)
            {
                DataRow dr = dt.NewRow();
                createTableStructure(ref dt);
                insertHeaderRows(columnNames, ref dr, ref dt);
                if (CheckTemplate() == "1")
                {
                    insertRows(dtChild, ref dr, ref dt);
                }
            }
            return dt;
        }


        private void createTableStructure(ref DataTable dt)
        {
            int i;
            dt.Columns.Add("Head", typeof(string));
            dt.Columns.Add("IsPass", typeof(string));
            dt.Columns.Add("Reason", typeof(string));
            for (i = 1; i <= 250; i++)
            {
                dt.Columns.Add("Col" + i.ToString().Trim(), typeof(string));
            }
        }

        private void insertHeaderRows(string[] columnNames, ref DataRow dr, ref DataTable dt)
        {
           int i = 3;
            foreach (string item in columnNames)
            {
                if (i == 3)
                {
                    dr["Head"] = "Y";
                    dr["IsPass"] = null;
                    dr["Reason"] = null;

                    UplodedColumns = item;
                }
                else
                { 
                    UplodedColumns = UplodedColumns +","+ item;
                }
                dr[i] = item;
                i += 1;
            }
            dt.Rows.Add(dr);
        }


        private void insertRows(DataTable dtChild, ref DataRow dr, ref DataTable dt)
        {

            int atleastFilledSingleColumn = 0;
            int i;
            
            foreach (DataRow drc in dtChild.Rows)
            {
                atleastFilledSingleColumn = 0;
                i = 3;
                dr = dt.NewRow();
                foreach (object cell in drc.ItemArray)
                {
                    if (i == 3)
                    {
                        dr["Head"] = "N";
                        dr["IsPass"] = null;
                        dr["Reason"] = null;
                    }

                    dr[i] = cell.ToString().Trim();
                    if (atleastFilledSingleColumn == 0 && cell.ToString().Trim() != string.Empty)
                    {
                        atleastFilledSingleColumn = 1;
                    }
                    i += 1;
                }

                if (atleastFilledSingleColumn == 0)
                {
                    dr["IsPass"] = "N";
                    dr["Reason"] = "All columns are empty.";
                }

                dt.Rows.Add(dr);
            }
        }



        private bool HasDuplicates(string[] arrayList)
        {
            List<string> vals = new List<string>();
            bool returnValue = false;
            foreach (string s in arrayList)
            {
                if (vals.Contains(s))
                {
                    returnValue = true;
                    break;
                }
                vals.Add(s);
            }


            return returnValue;
        }




    }
}
