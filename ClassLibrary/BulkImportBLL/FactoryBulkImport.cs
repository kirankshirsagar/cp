﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BulkImportBLL
{
    public class FactoryBulkImport
    {
        public static IContractTemplate GetContractTemplateDetail()
        {
            return new ContractTemplate();
        }

        public static IContractTemplateField GetContractTemplateFieldDetail()
        {
            return new ContractTemplateField();
        }

        public static IImportData GetImportDataDetail()
        {
            return new ImportData();
        }

        public static IUplodedData GetUplodedDataDetail()
        {
            return new UplodedData();
        }
 
    }
}
