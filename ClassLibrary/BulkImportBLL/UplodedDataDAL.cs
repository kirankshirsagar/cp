﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;


namespace BulkImportBLL
{
    public class UplodedDataDAL : DAL
    {


        public List<UplodedData> ReadData(IUplodedData I)
        {
            int i = 0;
            List<UplodedData> myList = new List<UplodedData>();
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            Parameters.AddWithValue("@UserRole", I.UserRole);
            SqlDataReader dr = getExecuteReader("BulkUplodedDataRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new UplodedData());
                    myList[i].ContractTemplateId = int.Parse(dr["ContractTemplateId"].ToString());
                    myList[i].ContractTemplateName = dr["ContractTemplateName"].ToString();
                    myList[i].ContractTypeId = int.Parse(dr["ContractTypeId"].ToString());
                    myList[i].ContractTypeName = dr["ContractTypeName"].ToString();
                    myList[i].TotalCount = int.Parse(dr["TotalCount"].ToString());
                    myList[i].ApprovedCount = int.Parse(dr["ApprovedCount"].ToString());
                    myList[i].RejectedCount = int.Parse(dr["RejectedCount"].ToString());
                    myList[i].Status = dr["Status"].ToString();
                    myList[i].BulkImportID = int.Parse(dr["BulkImportID"].ToString());
                    myList[i].FileName = dr["FileName"].ToString();
                    myList[i].ModifiedOn = dr["ModifiedOn"].ToString();


                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
        }


        public DataTable GetUplodedData(IUplodedData I, string isPass="N")
        {
            Parameters.AddWithValue("@BulkImportID", I.BulkImportID);
            Parameters.AddWithValue("@IsPass", isPass);
            return getDataTable("BulkGetUplodedData");
        }

        public string BulkImportProcessing(IUplodedData I)
        {
            return getExcuteScalar("BulkImportProcessing"); 
        }


    }
}
