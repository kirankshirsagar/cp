﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;


namespace BulkImportBLL
{
    public  class ContractTemplateDAL : DAL
    {
        public string InsertRecord(IContractTemplate I)
        {

            Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
            Parameters.AddWithValue("@ContractTemplateName", I.ContractTemplateName);
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            //Parameters.AddWithValue("@Status",0);
            //Parameters["@Status"].Direction = ParameterDirection.Output;
            //return getExcuteQuery("BulkContractTemplateAddUpdate", "@Status");

            SqlDataReader dr = getExecuteReader("BulkContractTemplateAddUpdate");
            I.ContractTemplateInformation = getContractTemplateInformation(dr);
            return "";

        }

   
        public string UpdateRecord(IContractTemplate I)
        {
            Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
            Parameters.AddWithValue("@ContractTemplateName", I.ContractTemplateName);
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
            Parameters.AddWithValue("@isActive", I.isActive);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Description", I.Description);
            ////Parameters.AddWithValue("@Status", 0);
            ////Parameters["@Status"].Direction = ParameterDirection.Output;
            //return getExcuteQuery("BulkContractTemplateAddUpdate", "@Status");
            SqlDataReader dr = getExecuteReader("BulkContractTemplateAddUpdate");
            I.ContractTemplateInformation = getContractTemplateInformation(dr);
            return "";
        }

        private List<ContractTemplate> getContractTemplateInformation(SqlDataReader dr)
        {
            int i = 0;
            List<ContractTemplate> myList = new List<ContractTemplate>();
            try
            {
                while (dr.Read())
                {
                    myList.Add(new ContractTemplate());
                    myList[i].ContractTemplateId = int.Parse(dr["ContractTemplateId"].ToString());
                    myList[i].ContractTemplateName = dr["ContractTemplateName"].ToString();
                    myList[i].IsUsed = dr["isUsed"].ToString();
                    myList[i].Status = int.Parse(dr["Status"].ToString());
                    myList[i].ContractTypeName = dr["ContractTypeName"].ToString();
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
        }



        public string DeleteRecord(IContractTemplate I)
        {
            Parameters.AddWithValue("@ContractTemplateIds", I.ContractTemplateIds);
            return getExcuteQuery("BulkContractTemplateDelete");
        }

        public string ChangeIsActive(IContractTemplate I)
        {
            Parameters.AddWithValue("@ContractTemplateIds", I.ContractTemplateIds);
            Parameters.AddWithValue("@isActive", I.isActive);
            return getExcuteQuery("BulkContractTemplateChangeIsActive");
        }

        public List<SelectControlFields>SelectData(IContractTemplate I)
        {
            Parameters.AddWithValue("@UserRole", I.UserRole);
            SqlDataReader dr = getExecuteReader("BulkContractTemplateSelect");
            return SelectControlFields.SelectData(dr);
        }

       
        public List<ContractTemplate> ReadData(IContractTemplate I)
        {
            int i = 0;
            List<ContractTemplate> myList = new List<ContractTemplate>();
            Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            Parameters.AddWithValue("@UserRole", I.UserRole);

            SqlDataReader dr = getExecuteReader("BulkContractTemplateRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new ContractTemplate());
                    myList[i].ContractTemplateId = int.Parse(dr["ContractTemplateId"].ToString());
                    myList[i].ContractTemplateName = dr["ContractTemplateName"].ToString();
                    myList[i].IsUsed = dr["isUsed"].ToString();
                    myList[i].isActive = dr["isActive"].ToString();
                    myList[i].Description = dr["Description"].ToString();
                    myList[i].ContractTypeId = int.Parse(dr["ContractTypeId"].ToString());
                    myList[i].ContractTypeName = dr["ContractTypeName"].ToString();
                    myList[i].isEditable = dr["isEditable"].ToString();
                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally{
                if (dr.IsClosed != true && dr != null)
                dr.Close();
            }
            return myList;
        }




    }
}
