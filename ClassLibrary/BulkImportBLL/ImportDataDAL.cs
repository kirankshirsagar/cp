﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;

namespace BulkImportBLL
{
    public class ImportDataDAL : DAL
    {
        public string InsertRecord(ImportData I)
        {
            Parameters.AddWithValue("@ContractTemplateId",I.ContractTemplateId);
            Parameters.AddWithValue("@InputFileName", I.InputFileName);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            //Parameters.AddWithValue("@BulkImportStaging", I.BulkImportStaging.RemoveInvalidHTMLData().ReplaceEmptyCellByNull());
            Parameters.AddWithValue("@BulkImportStaging", I.BulkImportStaging.extReplaceQuotes().ReplaceEmptyCellByNull());
            Parameters.AddWithValue("@UplodedColumns", I.UplodedColumns);
            Parameters.AddWithValue("@Error", I.Error);

            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("BulkImportData", "@Status");
        }

        public string CheckTemplate(ImportData I)
        { 
            Parameters.AddWithValue("@ContractTemplateId",I.ContractTemplateId);
            Parameters.AddWithValue("@UplodedColumns", I.UplodedColumns);
            Parameters.AddWithValue("@Status1", 0);
            Parameters["@Status1"].Direction = ParameterDirection.Output;
            return getExcuteQuery("BulkCheckTemplate", "@Status1");
        }
     
        public List<ImportData>ReadData(IImportData I)
        {
            List<ImportData> myList = new List<ImportData>();
            return myList;
        }

    }
}
