﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;

namespace BulkImportBLL
{
    public interface IContractTemplate : ICrud, INavigation
    {
        string UserRole { get; set; }
        int ContractTemplateId { get; set; }
        string ContractTemplateIds { get; set; }
        string ContractTemplateName { get; set; }
        int ContractTypeId { get; set; }
        string ContractTypeName { get; set; }
        string IsUsed { get; set; }
        List<ContractTemplate> ContractTemplateInformation{ get; set; }
        string Search{ get; set; }
        string isActive { get; set; }
        int Status { get; set; }
        string ChangeIsActive();
        List<SelectControlFields> SelectData();
        List<ContractTemplate> ReadData();
        string isEditable { get; set; }

    }

}
