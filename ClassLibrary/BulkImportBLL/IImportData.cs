﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;



namespace BulkImportBLL
{
    public interface IImportData
    {
        int ContractTemplateId { get; set; }
        string InsertRecord();
        DataTable BulkImportStaging { get; set; }
        string UplodedColumns { get; set; }
        string Error { get; set; }
        int AddedBy { get; set; }
        DateTime AddedOn { get; set; }
        string IpAddress { get; set; }
        string InputFileName { get; set; }
        List<ImportData>ReadData();
        DataTable FillStagingTable(DataTable dtChild);
        string CheckTemplate();
    }
}
