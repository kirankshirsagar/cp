﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrudBLL;
using System.Data;


namespace BulkImportBLL
{
    public interface IUplodedData : INavigation
    {
        string UserRole { get; set; }
        int BulkImportID { get; set; }
        int ContractTemplateId { get; set; } 
        int ContractTypeId { get; set; } 
        int TotalCount { get; set; } 
        int ApprovedCount { get; set; } 
        int RejectedCount { get; set; } 
        
        string ContractTemplateName { get; set; }
        string ContractTypeName { get; set; }
        string Status { get; set; }
        string Search { get; set; }
        string FileName { get; set; }
        void GetDocument(string filename, string filePath, System.Web.HttpResponse Response);
        List<UplodedData> ReadData();
        string BulkImportProcessing();
        string ModifiedOn { get; set; }

        DataTable GetUplodedInvalidData();
    }
}
