﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;
using System.IO;

namespace BulkImportBLL
{
    public interface IContractTemplateField : ICrud
    {
        int ContractTemplateId { get; set; } 
        DataTable TemplateFields { get; set; }
        List<ContractTemplate> ReadData();
        List<SelectControlFields> SelectFieldTypeData();
        List<SelectControlFields> SelectMasterData(int noSelect);
        void GetDataTableFilled(string str);
        void GetDocument(int ContractTemplateId, string filename, string filePath, System.Web.HttpResponse Response);
        string GetContractTemplateFieldRecord(); 
        

    }

}
