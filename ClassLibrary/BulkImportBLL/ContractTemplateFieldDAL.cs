﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;


namespace BulkImportBLL
{
    public class ContractTemplateFieldDAL : DAL
    {
        public string InsertRecord(IContractTemplateField I)
        {
            Parameters.AddWithValue("@ContractTemplateId",I.ContractTemplateId);
            Parameters.AddWithValue("@TemplateFields", I.TemplateFields);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@Status",0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("BulkContractTemplateFieldAddUpdate", "@Status");
        }

        public List<SelectControlFields> SelectFieldTypeData(IContractTemplateField I)
        {
            SqlDataReader dr = getExecuteReader("BulkFieldTypeSelect");
            return SelectControlFields.SelectData(dr);
        }

        public List<SelectControlFields> SelectMasterData(IContractTemplateField I, int noSelect = 0)
        {
            SqlDataReader dr = getExecuteReader("BulkMasterSelect");
            if (noSelect == 0)
            {
                return SelectControlFields.SelectData(dr);
            }
            else
            {
                return SelectControlFields.SelectDataNoSelect(dr);
            }
        }

        public DataTable GetDocumentStructure(int ContractTemplateId)
        {
            Parameters.AddWithValue("@ContractTemplateId", ContractTemplateId);
            return getDataTable("BulkContractTemplateFileDownload");
        }


        /*
        public List<ContractTemplateField> ReadData(IContractTemplateField I)
        {
            int i = 0;
            List<ContractTemplateField> myList = new List<ContractTemplateField>();
            Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
            SqlDataReader dr = getExecuteReader("BulkContractTemplateRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new ContractTemplateField());
                    myList[i].ContractTemplateId = int.Parse(dr["ContractTemplateId"].ToString());
                    i = i + 1;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally{
                if (dr.IsClosed != true && dr != null)
                dr.Close();
            }
            return myList;
        }
        */


        public string GetContractTemplateFieldRecord(IContractTemplateField I)
        {
            Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
            return getExcuteScalar("BulkContractTemplateFieldRead");
        }


    }
}
