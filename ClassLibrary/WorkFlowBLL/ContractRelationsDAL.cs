﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;

using System.Globalization;
using System.IO;
using System.Configuration;

namespace WorkflowBLL
{
    public class ContractRelationsDAL : DAL
    {
        public string InsertRecord(IContractRelations I)
        {
            Parameters.AddWithValue("@ContractId", I.ContractID);
            Parameters.AddWithValue("@RelationDescription", I.RelationsDescription);
            Parameters.AddWithValue("@ParentOfContractId", I.ParentOfContractId);
            Parameters.AddWithValue("@ChildOfContractId", I.ChildOfContractId);
          
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            string ret = getExcuteQuery("ContractRelationsAddUpdate", "@Status");
           
            return ret;
        }

        public string InsertContractRelationsRecord(IContractRelations I)
        {
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@ContractRelationsText", I.RelationsDescription);
            Parameters.AddWithValue("@ParentOfContractId", I.ContractID);
            Parameters.AddWithValue("@ChildOfContractId", I.ContractID);
           
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
           
            Parameters.AddWithValue("@Status", 0);
            Parameters.AddWithValue("@Ret_ContractRelationsId", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            Parameters["@Ret_ContractRelationsId"].Direction = ParameterDirection.Output;

            string ret= getExcuteQuery("ContractRelationsAddUpdate_ContractRelationsId", "@Status", "@Ret_ContractRelationsId");          
            return ret;
        }

        public string UpdateRecord(IContractRelations I)
        {
            Parameters.AddWithValue("@ContractId", I.ContractID);
            Parameters.AddWithValue("@ParentOfContractId", I.ParentOfContractId);
            Parameters.AddWithValue("@ChildOfContractId", I.ChildOfContractId);
            Parameters.AddWithValue("@RelationsId", I.ContractRelationsId);
            Parameters.AddWithValue("@RelationDescription", I.RelationsDescription);            
          
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
         
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;

            //var runningTask = System.Threading.Tasks.Task.Factory.StartNew(() => UpdateOutlookFile(I.ContractRelationsId));            
            string ret = getExcuteQuery("ContractRelationsAddUpdate", "@Status");
            return ret;
        }

        public string DeleteRecord(IContractRelations I)
        {
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@ContractID", I.ContractID);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            string retVal = getExcuteQuery("ContractRelationsDelete");           
            return retVal;
        }

        public List<ContractRelations> ReadData(IContractRelations I)
        {            
            List<ContractRelations> myListCR = new List<ContractRelations>();            

            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@ContractID", I.ContractID);          
            Parameters.AddWithValue("@UserId", I.AddedBy);
            Parameters.AddWithValue("@For", I.For);
            DataSet ds = getDataSet("ContractRelationsRead");          
            
            try
            {
                DataTable dtCR=ds.Tables[0];
                if (dtCR.Rows.Count>0)
                {
                    for (int i = 0; i < dtCR.Rows.Count; i++)
                    {
                       myListCR.Add(new ContractRelations());
                        myListCR[i].ContractRelationsId = int.Parse(dtCR.Rows[i]["RelationId"].ToString());
                        myListCR[i].RelationsDescription = dtCR.Rows[i]["RelationDescription"].ToString();
                        myListCR[i].ContractID = int.Parse(dtCR.Rows[i]["ContractId"].ToString());
                        myListCR[i].ChildOfContractId = int.Parse(dtCR.Rows[i]["ChildOfContractId"].ToString());
                        myListCR[i].ParentOfContractId = int.Parse(dtCR.Rows[i]["ParentOfContractID"].ToString());
                        myListCR[i].AddedByName = dtCR.Rows[i]["AddedBy"].ToString();
                        myListCR[i].AddedOn = Convert.ToDateTime(dtCR.Rows[i]["AddedOn"].ToString());
                        myListCR[i].IpAddress = dtCR.Rows[i]["IP"].ToString();
                        myListCR[i].IsUsed = dtCR.Rows[i]["IsUsed"].ToString();
                        myListCR[i].IsParentOfDisabled = Convert.ToBoolean(dtCR.Rows[i]["IsParentOfDisabled"].ToString());
                        myListCR[i].IsChildOfDisabled = Convert.ToBoolean(dtCR.Rows[i]["IsChildOfDisabled"].ToString());  
                    }                    
                }
                return myListCR;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                
            }
        }

        public List<ContractRelations> ReadDataForGraph(IContractRelations I)
        {            
            List<ContractRelations> myListCR = new List<ContractRelations>();
            
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@ContractID", I.ContractID);
            Parameters.AddWithValue("@URL", I.URL);
            Parameters.AddWithValue("@UserId", I.AddedBy);
           
            DataSet ds = getDataSet("ContractRelationsReadGraph");
            try
            {
               DataTable dtCR = ds.Tables[0];
                if (dtCR.Rows.Count > 0)
                {
                    for (int i = 0; i < dtCR.Rows.Count; i++)
                    {
                        //RelationId, RelationDescription, ContractId, ParentOfContractID, ChildOfContractId, AddedBy, AddedOn, ModifiedBy, ModifiedOn, IP 
                        myListCR.Add(new ContractRelations());
                        myListCR[i].ChildContractId = int.Parse(dtCR.Rows[i]["ChildContractId"].ToString());
                        myListCR[i].RequestId = int.Parse(dtCR.Rows[i]["RequestId"].ToString());
                        myListCR[i].ParentContractId = int.Parse(dtCR.Rows[i]["ParentContractID"].ToString());
                        myListCR[i].PIDClient = dtCR.Rows[i]["PIDClient"].ToString();
                        myListCR[i].CIDClient = dtCR.Rows[i]["CIDClient"].ToString();
                        myListCR[i].PIDClientHref = dtCR.Rows[i]["PIDClientHref"].ToString();
                        myListCR[i].CIDClientHref = dtCR.Rows[i]["CIDClientHref"].ToString();
                        myListCR[i].RelationsDescription = dtCR.Rows[i]["RelationsDescription"].ToString();
                        myListCR[i].ChildrenDropLevel = int.Parse(dtCR.Rows[i]["ChildrenDropLevel"].ToString());
                        myListCR[i].HtmlClass = dtCR.Rows[i]["HtmlClass"].ToString();
                    }
                }
                return myListCR;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public List<SelectControlFields> SelectContractRelationsAll(IContractRelations I)
        {
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@ContractID", I.ContractID);
            Parameters.AddWithValue("@UserId", I.AddedBy);
            SqlDataReader dr = getExecuteReader("ContractRelationsAllContracts");
            return SelectControlFields.SelectData(dr);
        }

        public List<SelectControlFields> SelectContractRelationsParentOf(IContractRelations I)
        {
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@ContractID", I.ContractID);
            Parameters.AddWithValue("@UserId", I.AddedBy);
            Parameters.AddWithValue("@Flag", I.Flag);
            SqlDataReader dr = getExecuteReader("ContractRelationsParentOf");
            return SelectControlFields.SelectData(dr);
        }

        public List<SelectControlFields> SelectContractRelationsChildOf(IContractRelations I)
        {
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@ContractID", I.ContractID);
            Parameters.AddWithValue("@UserId", I.AddedBy);
            Parameters.AddWithValue("@Flag", I.Flag);
            SqlDataReader dr = getExecuteReader("ContractRelationsChildOf");
            return SelectControlFields.SelectData(dr);
        }
    }
}