﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;
using System.Data.SqlClient;

namespace WorkflowBLL
{
    public interface IOutlookCalendar
    {
        string ActivityIds { get; set; }
        string RequestIds { get; set; }
        string ContractIDs { get; set; }
        int AssignToId { get; set; }
        string OutlookFileName { get; set; }
        string AssignToIds { get; set; }
        char IsBlank { get; set; } 
        //void SynchOutlookFile();
       // DateTime? StartWeekDate { get; set; }
        //DateTime? EndWeekDate { get; set; }

        string UpdateOutlookFile();
    }
}
