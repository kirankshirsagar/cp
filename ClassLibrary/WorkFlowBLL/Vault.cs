﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using CommonBLL;

namespace WorkflowBLL
{
    public class Vault : IVault
    {
        VaultDAL obj;
        public int RequestID { get; set; }
        public string RequestNo { get; set; }
        public int ContractID { get; set; }
        public string ContractIDs { get; set; }
        public int ContractTypeID { get; set; }
        public string ContractTypeName { get; set; }
        public string RequestType { get; set; }
        public int RequestTypeID { get; set; }
        public string RequestTypeName { get; set; }
        public int ClientID { get; set; }
        public string ClientName { get; set; }
        public string Address { get; set; }
        public int CountryID { get; set; }
        public string CountryName { get; set; }
        //public int StateID { get; set; }
        public string StateName { get; set; }
        //public int CityID { get; set; }
        public string CityName { get; set; }
        public string Pincode { get; set; }
        public int RequestUserID { get; set; }
        public string RequesterUserName { get; set; }
        public string ContractDescription { get; set; }  // RequestDescription
        public string DeadlineDate { get; set; }
        public int AssignToId { get; set; }
        public string AssignTo { get; set; }
        public string isApprovalRequried { get; set; }
        public DateTime ContractExpiryDate { get; set; }
        public string EstimatedValue { get; set; }
        public string TerminationReason { get; set; }
        public string DeletionReason { get; set; }
        public int AddedBy { get; set; }
        public int ModifiedBy { get; set; }
        public string IpAddress { get; set; }
        public string ContactNumber { get; set; }
        public string EmailID { get; set; }

        public int AddressID { get; set; }
        public int ContactDetailID { get; set; }
        public int EmailContactID { get; set; }


        public string isNewAddress { get; set; }
        public string isNewContactNumber { get; set; }
        public string isNewEmailID { get; set; }
        public int DepartmentId { get; set; }
   
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Description { get; set; }

        public int UserID { get; set; }
        public string AssignerUserName { get; set; }
        public string IsUsed { get; set; }
        public string isActive { get; set; }

        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public string DeadlineDateStr { get; set; }

        public string DocumentIDs { get; set; }

        public string RequestIDs { get; set; }
        public int Direction { get; set; }
        public string SortColumn { get; set; }
        public string ContractStatus { get; set; }
        public string IsBulkImport { get; set; }
        public string Status { get; set; }


        //********** added for question answer details on Vaultflow page ***********
        public string ModifiedByUserName { get; set; }
        public string ModifiedOnFormattedDate { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string TemplateName { get; set; }
        public int ContractRequestId { get; set; }
        public int ContractQAId { get; set; }
        public int ContractId { get; set; }
        public int ContractTemplateId { get; set; }
        //***************************************************************************

        public int ValidateContract()
        {
            try
            {
                obj = new VaultDAL();
                return obj.ValidateContract(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        
        }

        public string DeleteRecord()
        {
            try
            {
                obj = new VaultDAL();
                return obj.DeleteRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string InsertRecord()
        {
            try
            {
                obj = new VaultDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateRecord()
        {
            try
            {
                obj = new VaultDAL();
                return obj.UpdateRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }     


        public List<Vault> ReadData()
        {
            try
            {
                obj = new VaultDAL();
                return obj.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<SelectControlFields> ReadClientWithContracts()
        {
            try
            {
                obj = new VaultDAL();
                return obj.ReadClientWithContracts(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        


        public List<SelectControlFields> SelectData()
        {
            try
            {
                obj = new VaultDAL();
                return obj.SelectData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields> SelectOtherData()
        {
            try
            {
                obj = new VaultDAL();
                return obj.SelectOtherData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields> SelectDeptData()
        {
            try
            {
                obj = new VaultDAL();
                return obj.SelectDeptData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public List<Vault> ReadClientData()
        {
            try
            {
                obj = new VaultDAL();
                return obj.ReadClientData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        public DataSet ReadContractDetails() {

            try
            {
                obj = new VaultDAL();
                return obj.ReadContractDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Vault> ContractQuestionairDetailsForVaultFlow()
        {
            try
            {
                obj = new VaultDAL();
                return obj.ContractQuestionairDetailsForVaultFlow(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
