﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;

namespace WorkflowBLL
{
    public interface IAvailableClient : ICrud
    {

        int ClientID { get; set; }
        string ClientIDs { get; set; }
        string ClientName { get; set; }
        string Address { get; set; }
        int CountryID { get; set; }
        string CountryName { get; set; }
        int StateID { get; set; }
        string StateName { get; set; }
        int CityID { get; set; }
        string CityName { get; set; }
        string EmailID { get; set; }
        string ContactNo { get; set; }
        string Pincode { get; set; }
        string IsUsed { get; set; }
        long TotalRecords { get; set; }
        int PageNo { get; set; }
        int RecordsPerPage { get; set; }
        string Search { get; set; }
        int Direction { get; set; }
        string SortColumn { get; set; }
        int Param { get; set; }
        string isCustomer { get; set; }

        string Street { get; set; }

        List<SelectControlFields> SelectData(int withOutSelect = 0);

        List<AvailableClient> ReadData();

        void ReadEditData();
        List<AvailableClient> ReadClient { get; set; }
        List<AvailableClient> ReadAddress { get; set; }
        List<AvailableClient> ReadContact { get; set; }
        List<AvailableClient> ReadEmailID { get; set; }


    }
}
