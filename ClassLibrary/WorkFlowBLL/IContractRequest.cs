﻿using System;
using System.Collections.Generic;
using System.Data;
using CommonBLL;
using CrudBLL;

namespace WorkflowBLL
{
    public interface IContractRequest : ICrud, INavigation
    {
        #region Properties

        int RequestID { get; set; }
        string RequestNo { get; set; }
        int ContractID { get; set; }
        string ContractIDs { get; set; }
        int ContractTypeID { get; set; }
        string ContractTypeName { get; set; }
        string RequestType { get; set; }    // RequestTypeShortCode
        int RequestTypeID { get; set; }
        string RequestTypeName { get; set; }
        int ClientID { get; set; }
        string ClientName { get; set; }
        string Address { get; set; }
        int CountryID { get; set; }
        string CountryName { get; set; }
        int StateID { get; set; }
        string StateName { get; set; }
        int CityID { get; set; }
        string CityName { get; set; }
        string Pincode { get; set; }
        int RequestUserID { get; set; }
        string RequesterUserName { get; set; }
        string ContractDescription { get; set; }  // RequestDescription
        string DeadlineDate { get; set; }
        int AssignToId { get; set; }
        string AssignTo { get; set; }
        string isApprovalRequried { get; set; }
        DateTime ContractExpiryDate { get; set; }
        string EstimatedValue { get; set; }
        string TerminationReason { get; set; }
        string DeletionReason { get; set; }
        string ContactNumber { get; set; }
        string EmailID { get; set; }
        string isNewAddress { get; set; }
        string isNewContactNumber { get; set; }
        string isNewEmailID { get; set; }
        decimal dTotalValue { get; set; }
        int CurrencyID { get; set; }
        int AddressID { get; set; }
        int ContactDetailID { get; set; }
        int EmailContactID { get; set; }
        string ContractValuewithcurrency { get; set; }

        int DepartmentId { get; set; }

        int UserID { get; set; }
        string Description { get; set; }
        string AssignerUserName { get; set; }
        string IsUsed { get; set; }
        string isActive { get; set; }
        string DeadlineDateStr { get; set; }

        string DocumentIDs { get; set; }
        string RequestIDs { get; set; }
        string ContractStatus { get; set; }

        int ContractingpartyId { get; set; }
        string ContractingpartyName { get; set; }
        string isCustomer { get; set; }
        string Street1 { get; set; }

        string ContractTerm { get; set; }
        decimal ContractValue { get; set; }

        string DashboardRequestType { get; set; }
        string Tenure { get; set; }
        string BulkImportId { get; set; }
        string Others { get; set; }

        string AIOriginalFileName { get; set; }
        string AIUpdatedFileName { get; set; }
        string FileName { get; set; }
        string isPDF { get; set; }
        string FileLoc { get; set; }
        string docusign { get; set; }
        int ContractTemplateId { get; set; }
        string IsCustomerPortalEnable { get; set; }
        string IsNotified { get; set; }

        string Priority { get; set; }
        string PriorityReason { get; set; }
        bool IsContractTypeEditable { get; set; }
        bool IsTreeLinkVisible { get; set; }
        DataTable dtMatadataFieldValues { get; set; }
        string IsDocumentGenerated { get; set; }
        string IsBulkImport { get; set; }

        string FieldID { get; set; }
        string FieldValue { get; set; }
        decimal AIFileSize { get; set; }

        #endregion

        #region Methods

        List<SelectControlFields> SelectData();
        List<ContractRequest> ReadData();
        List<ContractRequest> ReadDataMyContracts();
        
        List<SelectControlFields> SelectOtherData();
        List<SelectControlFields> SelectDeptData();
        List<ContractRequest> ReadClientData();
        List<SelectControlFields> SelectContractingPartyData();
        List<SelectControlFields> SelectClientData();        
        List<SelectControlFields> SelectClientDataWithContract();

        string ReadMetaDataFieldsDetails();
        // List<ContractRequest> GetControlList();

        List<ContractRequest> ContractVersionsFile();
        // SqlDataReader ReadClientData();
        string ValidateContract();
        //Added by Bharati
        DataSet ReadContractDetails();
        string ReadAddressDetails();
        string ReadEmailDetails();
        string ReadContactDetails();
        string ReadPrimaryAddress();
        string ReadOtherDetails();
        
        string ContractIDAddressDetails();

        void InsertManualEntryDetails();

        List<SelectControlFields> ReadClientWithContracts();      

        List<SelectControlFields> SelectContractType();

        string ReadIsCustomerDetails();

        DataTable GetContractTypeIDByTemplateID();

        string AIInsertRecord();
        string IsClientExist();

        #endregion
    }
}
