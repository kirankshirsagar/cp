﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using CommonBLL;

namespace WorkflowBLL
{
    public class AutoRenewal : IAutoRenewal
    {
        AutoRenewalDAL obj;
        public int RequestID { get; set; }
        public string RequestNo { get; set; }
        public int ContractID { get; set; }
        public string ContractIDs { get; set; }
        public int ContractTypeID { get; set; }
        public string ContractTypeName { get; set; }
        public string RequestType { get; set; }
        public int RequestTypeID { get; set; }
        public string RequestTypeName { get; set; }
        public int ClientID { get; set; }
        public string ClientName { get; set; }
        public string Address { get; set; }
        public int CountryID { get; set; }
        public string CountryName { get; set; }
        public int StateID { get; set; }
        public string StateName { get; set; }
        public int CityID { get; set; }
        public string CityName { get; set; }
        public string Pincode { get; set; }
        public int RequestUserID { get; set; }
        public string RequesterUserName { get; set; }
        public string ContractDescription { get; set; }
        public string DeadlineDate { get; set; }
        public int AssignToId { get; set; }
        public string AssignTo { get; set; }
        public string isApprovalRequried { get; set; }
        public DateTime ContractExpiryDate { get; set; }
        public string EstimatedValue { get; set; }
        public string TerminationReason { get; set; }
        public string DeletionReason { get; set; }
        public int AddedBy { get; set; }
        public int ModifiedBy { get; set; }
        public string IpAddress { get; set; }
        public string ContactNumber { get; set; }
        public string EmailID { get; set; }

        public int AddressID { get; set; }
        public int ContactDetailID { get; set; }
        public int EmailContactID { get; set; }


        public string isNewAddress { get; set; }
        public string isNewContactNumber { get; set; }
        public string isNewEmailID { get; set; }
        public int DepartmentId { get; set; }

        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Description { get; set; }

        public int UserID { get; set; }
        public string AssignerUserName { get; set; }
        public string IsUsed { get; set; }
        public string isActive { get; set; }

        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public string DeadlineDateStr { get; set; }

        public string DocumentIDs { get; set; }

        public string RequestIDs { get; set; }
        public int Direction { get; set; }
        public string SortColumn { get; set; }

    

        public List<AutoRenewal> ReadData()
        {
            try
            {
                obj = new AutoRenewalDAL();
                return obj.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public string InsertRecord()
        {
            throw new NotImplementedException();
        }

        public string UpdateRecord()
        {
            throw new NotImplementedException();
        }

        public string DeleteRecord()
        {
            throw new NotImplementedException();
        }
    }
}
