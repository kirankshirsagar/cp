﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkflowBLL
{
    public class FactoryWorkflow
    {
     
        public static IClient GetClientDetails()
        {
            return new Client();
        }

        public static IContractRequest GetContractRequestDetails()
        {
            return new ContractRequest();
        }
         public static IContractRequest2 GetContractRequest2Details()
         {
             return new ContractRequest2();
         }

        public static IActivity GetActivityDetails()
        {
            return new Activity();
        }

        public static IAutoRenewal GetAutoRenewalDetails()
        {
            return new AutoRenewal();
        }

        public static IAvailableClient GetAvailableClientDetails()
        {
            return new AvailableClient();
        }

        public static IGlobalSearch GetGlobalSearchDetails()
        {
            return new GlobalSearch();
        }

        public static ITrackDocumentChanges GetTrackDocumentChangesObject()
        {
            return new TrackDocumentChanges();
        }

        public static IKeyFields GetKeyFieldsDetails()
        {
            return new KeyFeilds();
        }
        public static IVault GetVaultDetails()
        {
            return new Vault();
        }

        public static IDocumentSearch GetDocumentSearchDetails()
        {
            return new DocumentSearch();
        }

        public static IOutlookCalendar GetOutlookCalendarDetails()
        {
            return new OutlookCalendar();
        }

        public static IContractRelations GetContractRelations()
        {
            return new ContractRelations();
        }
    }
}
