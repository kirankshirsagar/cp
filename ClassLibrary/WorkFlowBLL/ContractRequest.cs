﻿using System;
using System.Collections.Generic;
using System.Data;
using CommonBLL;

namespace WorkflowBLL
{
    public class ContractRequest : IContractRequest
    {
        ContractRequestDAL obj;

        #region Properties
        public int RequestID { get; set; }
        public string RequestNo { get; set; }
        public int ContractID { get; set; }
        public string ContractIDs { get; set; }
        public int ContractTypeID { get; set; }
        public string ContractTypeName { get; set; }
        public string RequestType { get; set; }
        public int RequestTypeID { get; set; }
        public string RequestTypeName { get; set; }
        public int ClientID { get; set; }
        public string ClientName { get; set; }
        public string Address { get; set; }
        public int CountryID { get; set; }
        public string CountryName { get; set; }
        public int StateID { get; set; }
        public string StateName { get; set; }
        public int CityID { get; set; }
        public string CityName { get; set; }
        public string Pincode { get; set; }
        public int RequestUserID { get; set; }
        public string RequesterUserName { get; set; }
        public string ContractDescription { get; set; }  // RequestDescription
        public string DeadlineDate { get; set; }
        public int AssignToId { get; set; }
        public string AssignTo { get; set; }
        public string isApprovalRequried { get; set; }
        public DateTime ContractExpiryDate { get; set; }
        public string EstimatedValue { get; set; }
        public string TerminationReason { get; set; }
        public string DeletionReason { get; set; }
        public int AddedBy { get; set; }
        public int ModifiedBy { get; set; }
        public string IpAddress { get; set; }
        public string ContactNumber { get; set; }
        public string EmailID { get; set; }
        public decimal dTotalValue { get; set; }
        public int CurrencyID { get; set; }
        public string ContractValuewithcurrency { get; set; }
        public int AddressID { get; set; }
        public int ContactDetailID { get; set; }
        public int EmailContactID { get; set; }
         
        public string isNewAddress { get; set; }
        public string isNewContactNumber { get; set; }
        public string isNewEmailID { get; set; }
        public int DepartmentId { get; set; }
   
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Description { get; set; }

        public int UserID { get; set; }
        public string AssignerUserName { get; set; }
        public string IsUsed { get; set; }
        public string isActive { get; set; }

        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public string DeadlineDateStr { get; set; }

        public string DocumentIDs { get; set; }

        public string RequestIDs { get; set; }
        public int Direction { get; set; }
        public string SortColumn { get; set; }
        public string ContractStatus { get; set; }
         
        public int ContractingpartyId { get; set; }
        public string ContractingpartyName { get; set; }
        public string isCustomer { get; set; }
        public string Street1 { get; set; }

        public string ContractTerm { get; set; }
        public decimal ContractValue { get; set; }

        public string DashboardRequestType { get; set; }
        public string Tenure { get; set; }

        public string IsDocumentGenerated { get; set; }
        public string IsBulkImport { get; set; }
        public string BulkImportId { get; set; }
        public string Others { get; set; }

        public string AIOriginalFileName { get; set; }
        public string AIUpdatedFileName { get; set; }
        public string FileName { get; set; }
        public string isPDF { get; set; }
        public string FileLoc { get; set; }
        public string docusign { get; set; }
        public DataTable dtMatadataFieldValues { get; set; } // added by js

        public string FieldID { get; set; }
        public string FieldValue { get; set; }
        public int ContractTemplateId { get; set; }
        public string IsCustomerPortalEnable { get; set; }
        public string IsNotified { get; set; }

        public string Priority { get; set; }
        public string PriorityReason { get; set; }
        public bool IsContractTypeEditable { get; set; }
        public bool IsTreeLinkVisible { get; set; }

        public decimal AIFileSize { get; set; }

        #endregion

        #region Methods
        public string ValidateContract()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.ValidateContract(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        
        }

        public string DeleteRecord()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.DeleteRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertManualEntryDetails()
        {
            try
            {
                obj = new ContractRequestDAL();
                obj.InsertManualEntryDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        }

        public string InsertRecord()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string AIInsertRecord()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.AIInsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateRecord()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.UpdateRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }     
         
        public List<ContractRequest> ReadData()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ContractRequest> ReadDataMyContracts()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.ReadDataMyContracts(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
                 
        public List<SelectControlFields> ReadClientWithContracts()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.ReadClientWithContracts(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields> SelectClientData()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.SelectClientData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }        

        public List<SelectControlFields> SelectClientDataWithContract()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.SelectClientDataWithContract(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         
        public List<SelectControlFields> SelectData()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.SelectData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields> SelectOtherData()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.SelectOtherData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         
        public List<SelectControlFields> SelectContractingPartyData()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.SelectContractingPartyData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         
        public List<SelectControlFields> SelectDeptData()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.SelectDeptData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         
        public List<ContractRequest> ReadClientData()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.ReadClientData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ReadAddressDetails()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.ReadAddressDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         
        public string ContractIDAddressDetails()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.ContractIDAddressDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ReadEmailDetails()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.ReadEmailDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         
        public string ReadContactDetails()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.ReadContactDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         
        public string ReadPrimaryAddress()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.ReadPrimaryAddress(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ReadOtherDetails()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.ReadOtherDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ReadMetaDataFieldsDetails()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.ReadMetaDataFieldsDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet ReadContractDetails() {
            try
            {
                obj = new ContractRequestDAL();
                return obj.ReadContractDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ContractRequest> ContractVersionsFile()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.ContractVersionsFile(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields> SelectContractType()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.SelectContractType(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ReadIsCustomerDetails()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.ReadIsCustomerDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetContractTypeIDByTemplateID()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.GetContractTypeIDByTemplateID(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string IsClientExist()
        {
            try
            {
                obj = new ContractRequestDAL();
                return obj.IsClientExist(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }

}
