﻿using System;
using System.Collections.Generic;
using System.Data;
using CommonBLL;
using System.Data.SqlClient;


namespace WorkflowBLL
{
    public class ContractRequest2 : ContractRequest, IContractRequest2
    {
        ContractRequestDAL2 obj;
        public string CacheString { get; set; }      
        
        public string BuildCacheString()
        {
            obj = new ContractRequestDAL2();
            CacheString = obj.BuildCacheString(this);

            return CacheString;
        }
    }
}
