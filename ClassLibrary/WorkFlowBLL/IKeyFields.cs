﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;
using System.Data.SqlClient;

namespace WorkflowBLL
{
    public interface IKeyFields : ICrud, INavigation
    {

        Nullable<DateTime> RenewalDate { get; set; }
        Nullable<DateTime> EffectiveDate { get; set; }
        Nullable<DateTime> ExpirationDate { get; set; }

        string RenewalDatestring { get; set; }
        string EffectiveDatestring { get; set; }
        string ExpirationDatestring { get; set; }

        string ExpirationAlert180days { get; set; }//js
        string ExpirationAlert90days { get; set; }//js
        string ExpirationAlert60days { get; set; }//js
        string ExpirationAlert30days { get; set; }
        string ExpirationAlert7days { get; set; }
        string ExpirationAlert24Hrs { get; set; }

        string RenewalAlert180days { get; set; }//js
        string RenewalAlert90days { get; set; }//js
        string RenewalAlert60days { get; set; }//js
        string RenewalAlert24Hrs { get; set; }
        string RenewalAlert30days { get; set; }
        string RenewalAlert7days { get; set; }

        string LiabilityCap { get; set; }
        string Indemnity { get; set; }
        string IsChangeofControl { get; set; }
        string IsAgreementClause { get; set; }
        string IsStrictliability { get; set; }
        string Strictliability { get; set; }
        string IsGuaranteeRequired { get; set; }
        string Insurance { get; set; }
        string IsTerminationConvenience { get; set; }
        string TerminationConvenience { get; set; }
        string IsTerminationmaterial { get; set; }
        string Terminationmaterial { get; set; }
        string IsTerminationinsolvency { get; set; }
        string Terminationinsolvency { get; set; }
        string IsTerminationinChangeofcontrol { get; set; }
        string TerminationinChangeofcontrol { get; set; }
        string IsTerminationothercauses { get; set; }
        string Terminationothercauses { get; set; }
        string AssignmentNovation { get; set; }

        string Others { get; set; }

        int RequestId { get; set; }
        int Param { get; set; }

        int ExpirationAlert180daysUserID1 { get; set; }//js
        int ExpirationAlert180daysUserID2 { get; set; }//js
        int ExpirationAlert90daysUserID1 { get; set; }//js
        int ExpirationAlert90daysUserID2 { get; set; }//js
        int ExpirationAlert60daysUserID1 { get; set; }//js
        int ExpirationAlert60daysUserID2 { get; set; }//js

        int ExpirationAlert30daysUserID1 { get; set; }
        int ExpirationAlert30daysUserID2 { get; set; }
        int ExpirationAlert7daysUserID1 { get; set; }
        int ExpirationAlert7daysUserID2 { get; set; }
        int ExpirationAlert24HrsUserID1 { get; set; }
        int ExpirationAlert24HrsUserID2 { get; set; }

        int RenewalAlert180daysUserID1 { get; set; }//js
        int RenewalAlert180daysUserID2 { get; set; }//js
        int RenewalAlert90daysUserID1 { get; set; }//js
        int RenewalAlert90daysUserID2 { get; set; }//js
        int RenewalAlert60daysUserID1 { get; set; }//js
        int RenewalAlert60daysUserID2 { get; set; }//js

        int RenewalAlert30daysUserID1 { get; set; }
        int RenewalAlert30daysUserID2 { get; set; }
        int RenewalAlert7daysUserID1 { get; set; }
        int RenewalAlert7daysUserID2 { get; set; }
        int RenewalAlert24HrsUserID1 { get; set; }
        int RenewalAlert24HrsUserID2 { get; set; }

        string ExpirationAlert180daysUserName1 { get; set; }//js
        string ExpirationAlert180daysUserName2 { get; set; }//js
        string ExpirationAlert90daysUserName1 { get; set; }//js
        string ExpirationAlert90daysUserName2 { get; set; }//js
        string ExpirationAlert60daysUserName1 { get; set; }//js
        string ExpirationAlert60daysUserName2 { get; set; }//js
        string ExpirationAlert30daysUserName1 { get; set; }
        string ExpirationAlert30daysUserName2 { get; set; }
        string ExpirationAlert7daysUserName1 { get; set; }
        string ExpirationAlert7daysUserName2 { get; set; }
        string ExpirationAlert24HrsUserName1 { get; set; }
        string ExpirationAlert24HrsUserName2 { get; set; }

        string RenewalAlert180daysUserName1 { get; set; }//js
        string RenewalAlert180daysUserName2 { get; set; }//js
        string RenewalAlert90daysUserName1 { get; set; }//js
        string RenewalAlert90daysUserName2 { get; set; }//js
        string RenewalAlert60daysUserName1 { get; set; }//js
        string RenewalAlert60daysUserName2 { get; set; }//js
        string RenewalAlert30daysUserName1 { get; set; }
        string RenewalAlert30daysUserName2 { get; set; }
        string RenewalAlert7daysUserName1 { get; set; }
        string RenewalAlert7daysUserName2 { get; set; }
        string RenewalAlert24HrsUserName1 { get; set; }
        string RenewalAlert24HrsUserName2 { get; set; }

        int ActivityId { get; set; }
        int ActivityTypeId { get; set; }
        int ActivityStatusId { get; set; }
        string ActivityText { get; set; }
        DateTime ActivityDate { get; set; }
        Nullable<DateTime> ReminderDate { get; set; }
        string IsForCustomer { get; set; }
        int AssignToId { get; set; }
        int AddedBy { get; set; }
        int ModifiedBy { get; set; }
        string IpAddress { get; set; }
        string IsFromCalendar { get; set; }
        int ContractID { get; set; }
        string IsFromOther { get; set; }
        string ActivityTypeName { get; set; }
        string AssignUser { get; set; }
        string StatusName { get; set; }
        string User { get; set; }

         string ActReminderDate { get; set; }
         string ActActivityDate { get; set; }
         string TerminationDescription { get; set; }

         string Email { get; set; }
         int MetaDataFieldId { get; set; }
         int ReminderDays { get; set; }
         int ReminderUser1 { get; set; }
         int ReminderUser2 { get; set; }
        
         string URLForWindowsService { get; set; }
         DataTable dtMatadataFieldValues { get; set; } //added by jyoti 25-12-2015
         string UpdateURLLastRunTimeForWindowsService();
         int ContractTypeId { get; set; }
         string IsManualEntry { get; set; }

         string IsIndirectConsequentialLosses { get; set; }
         string IndirectConsequentialLosses { get; set; }
         string IsIndemnity { get; set; }
         string IsWarranties { get; set; }
         string Warranties { get; set; }
         string EntireAgreementClause { get; set; }
         string ThirdPartyGuaranteeRequired { get; set; }
         string IsForceMajeure { get; set; }
         string ForceMajeure { get; set; }
         string IsSetOffRights { get; set; }
         string SetOffRights { get; set; }
         string IsGoverningLaw { get; set; }
         string GoverningLaw { get; set; }

        List<SelectControlFields> SelectData();
        List<KeyFeilds> ReadData();

        List<KeyFeilds> ReadActivityReminderData();
        string createTextFile(string fileName);
        DataTable GetMetaDataDateUser();
        DataTable GetUsers();
    }
}
