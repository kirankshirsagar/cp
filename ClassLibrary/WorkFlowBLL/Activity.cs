﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using CommonBLL;

namespace WorkflowBLL
{
    public class Activity : IActivity 
    {
        ActivityDAL actdal;
        public int RNO { get; set; }
        public string OutlookFileName { get; set; }

        public int ActivityId { get; set; }
        public string ActivityIds { get; set; }
        public int ActivityTypeId { get; set; }
        public int ActivityStatusId { get; set; }
        public string ActivityText { get; set; }
        public DateTime ActivityDate { get; set; }
        public Nullable<DateTime> ReminderDate { get; set; }
        public string IsForCustomer { get; set; }
        public string ActivityTypeName { get; set; }
        public string ActivityStatusName { get; set; }
        public int AddedBy { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string IpAddress { get; set; }
        public string Description { get; set; }
        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public string Search { get; set; }
        public string ActivityTime { get; set; }
        public string AddedTime { get; set; }
        public string AddedByName { get; set; }
        public int Flag { get; set; }
        public int RequestId { get; set; }
        public int ReqID { get; set; }
        public DateTime OnlyDate { get; set; }
        public string ActivityDateDefaultFormat { get; set; }
        public string ReminderDateDefaultFormat { get; set; }
        public string ContractTypeId { get; set; } 
      
        public List<Activity> ActivityDetails { get; set; }
        public List<Activity> ActivityDates { get; set; }
        public List<Activity> ActivityOtherDetails { get; set; }
        public int Direction { get; set; }
        public string SortColumn { get; set; }
        public int AssignToId { get; set; }
        public string AssignToName { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }
       
        public int ContractID { get; set; }
        public string AddedOnDate { get; set; }

        public int ActivityMonth { get; set; }
        public int ActivityYear { get; set; }

        public DateTime? StartWeekDate { get; set; }
        public DateTime? EndWeekDate { get; set; }

        public int Snooze { get; set; }
        public string ActivityDateString { get; set; }
        
        public string FileUpload { get; set; }
        public string FileUploadOriginal { get; set; }
        public string FileSizeKb { get; set; }

        public string ActivityFileUpload { get; set; }
        public string ActivityFileUploadOriginal { get; set; }
        public string ActivityFileSizeKb { get; set; }


        public string EmailID { get; set; }

        public string isEditable { get; set; }
        public string IsOCR { get; set; }
        public string OpenActivityType { get; set; }
        public string ContractTypeIds { get; set; }
        public void ReadData()
        {
            try
            {
                actdal = new ActivityDAL();
                actdal.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Activity>GetUserNameAndPassword()
        {
            try
            {
                actdal = new ActivityDAL();
                return actdal.GetUserNameAndPassword(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ReadDataCalendar()
        {
            try
            {
                actdal = new ActivityDAL();
                actdal.ReadDataCalendar(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ReadAgendaData()
        {
            try
            {
                actdal = new ActivityDAL();
                actdal.ReadAgendaData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public string InsertRecord()
        {
            try
            {
                actdal = new ActivityDAL();
                return actdal.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string InsertActivityRecord()
        {
            try
            {
                actdal = new ActivityDAL();
                return actdal.InsertActivityRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DeleteRecord()
        {
            try
            {
                actdal = new ActivityDAL();
                return actdal.DeleteRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateRecord()
        {
            try
            {
                actdal = new ActivityDAL();
                return actdal.UpdateRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Activity> GetActivityPending()
        { 
             try
            {
                actdal = new ActivityDAL();
                return actdal.GetActivityPending(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }        
        }

        public string ActivitySnoozeAndDismiss()
        {
            try
            {
                actdal = new ActivityDAL();
                return actdal.ActivitySnoozeAndDismiss(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }        
            
        
        }

        public string IsFromCalendar { get; set; }

        public List<SelectControlFields> SelectContractIds()
        {
            try
            {
                actdal = new ActivityDAL();
                return actdal.SelectContractIds(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
        public string ErrorIfAny { get; set; }
        public string ContractDetailsByRequestId()
        {
            try
            {
                actdal = new ActivityDAL();
                return actdal.ContractDetailsByRequestId(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public string ReadAssineDetails()
        //{
        //    try
        //    {
        //        actdal = new ActivityDAL();
        //        return actdal.ReadAssineDetails(this);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}
