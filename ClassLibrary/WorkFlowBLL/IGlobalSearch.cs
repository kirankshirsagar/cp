﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonBLL;
using CrudBLL;

namespace WorkflowBLL
{
    public interface IGlobalSearch : INavigation
    {
        string StringToFind { get; set; }
        string ClientName { get; set; }
        string ContractType { get; set; }
        string AssignedTo { get; set; }
        string RequestDate { get; set; }
        string Link { get; set; }
        int UserId { get; set; }
        string Search { get; set; }
        List<GlobalSearch> ReadData();
        int RequestId { get; set; }

    }
}
