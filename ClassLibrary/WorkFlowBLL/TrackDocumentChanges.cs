﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;

namespace WorkflowBLL
{
    public class TrackDocumentChanges : ITrackDocumentChanges
    {

        TrackDocumentChangesDAL obj;
        public int RequestID { get; set; }

        public int TrackChangesClauseID { get; set; }
        public int TrackChangesFieldID { get; set; }
        public int FieldLibraryID { get; set; }
        public int ContractFileOldId { get; set; }
        public int ContractFileNewId { get; set; }
        public int ClauseID { get; set; }
        public string OldFileValue { get; set; }
        public string NewFileValue { get; set; }
        public string ModificationStatus { get; set; }
        public int Status { get; set; }

        public string Action { get; set; }
       public string StatusFlag { get; set; }
        public string FieldName { get; set; }
        public string ClauseName { get; set; }

        public string BookmarkName { get; set; }

        public int TrackChangesID { get; set; }
        public int LineNo { get; set; }
        public string OriginalLineText { get; set; }
        public string NewFileValueToEdit { get; set; }

        public DataTable dtTrackFieldChanges{ get; set; }
        public DataTable dtTrackClauseChanges { get; set; }
        public DataTable dtTrackLineChanges { get; set; }

        public int AddedBy { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string IpAddress { get; set; }
        public string Description { get; set; }

        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public int Direction { get; set; }
        public string SortColumn { get; set; }
        public string Search { get; set; }

        public DataTable DefineTrackChangesTable()
        {
            try
            {
               DataTable ldtTrackChanges = new DataTable();

               ldtTrackChanges.Columns.Add(new DataColumn("TrackChangesID"));

               ldtTrackChanges.Columns.Add(new DataColumn("TrackChangesFieldID"));
               ldtTrackChanges.Columns.Add(new DataColumn("TrackChangesClauseID"));
               ldtTrackChanges.Columns.Add(new DataColumn("ContractFileOldId"));
               ldtTrackChanges.Columns.Add(new DataColumn("ContractFileNewId"));
               ldtTrackChanges.Columns.Add(new DataColumn("FieldLibraryID"));
               ldtTrackChanges.Columns.Add(new DataColumn("ClauseID"));

               ldtTrackChanges.Columns.Add(new DataColumn("LineNo"));

               ldtTrackChanges.Columns.Add(new DataColumn("OldFileFieldValue"));
               ldtTrackChanges.Columns.Add(new DataColumn("NewFileFieldValue"));

               ldtTrackChanges.Columns.Add(new DataColumn("OldFileClauseValue"));
               ldtTrackChanges.Columns.Add(new DataColumn("NewFileClauseValue"));

               ldtTrackChanges.Columns.Add(new DataColumn("OriginalLineText"));
               ldtTrackChanges.Columns.Add(new DataColumn("OldFileLineValue"));
               ldtTrackChanges.Columns.Add(new DataColumn("NewFileLineValue"));

               ldtTrackChanges.Columns.Add(new DataColumn("NewFileValueToEdit"));
                

               ldtTrackChanges.Columns.Add(new DataColumn("ModificationStatus"));
               ldtTrackChanges.Columns.Add(new DataColumn("BookmarkName"));
               ldtTrackChanges.Columns.Add(new DataColumn("AddedBy"));
               ldtTrackChanges.Columns.Add(new DataColumn("IpAddress"));

               return ldtTrackChanges;
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }

        public DataTable GetContractFileDetails()
        {
            try
            {
                obj = new TrackDocumentChangesDAL();
                return obj.GetContractFileDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetFieldLibraryAndClausesDetails()
        {
            try
            {
                obj = new TrackDocumentChangesDAL();
                return obj.GetFieldLibraryAndClausesDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DeleteRecord()
        {
            try
            {
                obj = new TrackDocumentChangesDAL();
                return obj.DeleteRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string InsertRecord()
        {
            try
            {
                obj = new TrackDocumentChangesDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateRecord()
        {
            try
            {
                obj = new TrackDocumentChangesDAL();
                return obj.UpdateRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public bool IsValueExistInDatatable(DataTable dt, string Value, string ColumnName)
        {
            Boolean flg = false;
            try
            {
                if (dt.Columns.Contains(ColumnName))
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (Convert.ToString(dr[ColumnName]).Trim().Equals(Value.Trim()))
                        {
                            flg = true;
                            break;
                        }                    
                    }
                }
                return flg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public Boolean IsTrackedChangesAlreadySaved()
        {

            try
            {
                obj = new TrackDocumentChangesDAL();
                string cnt = obj.GetAlreadySavedTrackChangesCnt(this);
                if (Convert.ToInt32(cnt) > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       
        public List<TrackDocumentChanges> GetTrackChangesTermsDetails()
        {
            try
            {
                obj = new TrackDocumentChangesDAL();
                return obj.GetTrackChangesTermsDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetTrackChangesTermsSubDetails()
        {
            try
            {
                obj = new TrackDocumentChangesDAL();
                return obj.GetTrackChangesTermsSubDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<TrackDocumentChanges> GetTrackChangesClauseDetails()
        {
            try
            {
                obj = new TrackDocumentChangesDAL();
                return obj.GetTrackChangesClauseDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetTrackChangesClauseSubDetails()
        {
            try
            {
                obj = new TrackDocumentChangesDAL();
                return obj.GetTrackChangesClauseSubDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        public List<TrackDocumentChanges> GetTrackChangesLineDetails()
        {
            try
            {
                obj = new TrackDocumentChangesDAL();
                return obj.GetTrackChangesLineDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetTrackChangesLineSubDetails()
        {
            try
            {
                obj = new TrackDocumentChangesDAL();
                return obj.GetTrackChangesLineSubDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    }
}
