﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;


namespace WorkflowBLL
{
    public class AutoRenewalDAL : DAL
    {



        public List<AutoRenewal> ReadData(IAutoRenewal I)
        {
            int i = 0;
            List<AutoRenewal> myList = new List<AutoRenewal>();
            
            Parameters.AddWithValue("@UserID", I.UserID);
            Parameters.AddWithValue("@ContractID", I.ContractID);
            Parameters.AddWithValue("@RequestTypeShortCode", I.RequestTypeID);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@ClientName", I.ClientName);

            SqlDataReader dr = getExecuteReader("AutoRenewalRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new AutoRenewal());
                    myList[i].RequestID = int.Parse(dr["RequestId"].ToString());//1
                    myList[i].RequestNo = dr["RequestNo"].ToString();//2
                    if (dr["ContractID"].ToString() != "")
                    {
                        myList[i].ContractID = int.Parse(dr["ContractID"].ToString());//3
                    }
                    else 
                    {
                        myList[i].ContractID =0;//3
                    }
                    myList[i].ContractTypeID = int.Parse(dr["ContractTypeId"].ToString());//4
                    myList[i].ClientID = int.Parse(dr["ClientId"].ToString());//6
                    myList[i].ClientName = dr["ClientName"].ToString();//7
                    myList[i].Address = dr["Address"].ToString();//8
                    myList[i].CountryID = int.Parse(dr["CountryId"].ToString());//9
                    myList[i].StateID = int.Parse(dr["StateId"].ToString());//11
                    myList[i].CityID = int.Parse(dr["CityId"].ToString());//13
                    myList[i].Pincode = dr["PinCode"].ToString();//15
                    myList[i].RequestUserID = int.Parse(dr["RequesterUserId"].ToString());//17
                    myList[i].ContractDescription = dr["RequestDescription"].ToString();//request descrption 18
                    myList[i].DeadlineDate = Convert.ToString(dr["DeadlineDate"].ToString());//19
                    myList[i].DeadlineDateStr = dr["DeadlineDate"].ToString();

                    if (dr["AssignToUserID"].ToString() == "")
                    {
                        myList[i].AssignToId = 0;
                    }
                    else
                    {
                        myList[i].AssignToId = int.Parse(dr["AssignToUserID"].ToString());
                    }
                    if (dr["AssignToDepartmentId"].ToString() == "")
                    {
                        myList[i].DepartmentId = 0;
                    }
                    else
                    {
                        myList[i].DepartmentId = int.Parse(dr["AssignToDepartmentId"].ToString());
                    }

                    myList[i].RequestTypeName = dr["RequestTypeName"].ToString();
                    myList[i].Description = dr["RequestDescription"].ToString();
                    myList[i].IsUsed = dr["isUsed"].ToString();
                    myList[i].isActive = dr["isActive"].ToString();
                    myList[i].ContractTypeName = dr["ContractTypeName"].ToString();
                    myList[i].RequesterUserName = dr["RequesterUserName"].ToString();
                    myList[i].AssignerUserName = dr["AssignerUserName"].ToString();
                    myList[i].isApprovalRequried = dr["isApprovalRequired"].ToString();
                    myList[i].EstimatedValue = dr["EstimatedValue"].ToString();
                    myList[i].RequestTypeID = Convert.ToInt32(dr["RequestTypeID"].ToString());
                    

                    i = i + 1;
                }


                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;

        }

       

      

    }
}
