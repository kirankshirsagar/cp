﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using CommonBLL;

namespace WorkflowBLL
{
    public class ContractRequestDAL : DAL
    {
        private readonly static string TenantUploadFolderPath = System.Web.HttpContext.Current.Server.MapPath(@"~/Uploads/" + System.Web.HttpContext.Current.Request.Url.Segments[1].Replace("/", ""));
        //public ContractRequestDAL(string ConnString):base(ConnString)
        //{

        //}
        IOutlookCalendar OC = new OutlookCalendar();

        public string ValidateContract(IContractRequest I)
        {
            Parameters.AddWithValue("@ContractID", I.ContractID);//4

            return Convert.ToString(getExcuteScalar("ValidateContractID"));
        }

        public string ReadAddressDetails(IContractRequest I)
        {

            Parameters.AddWithValue("@ClientId", I.ClientID);//4

            return Convert.ToString(getExcuteScalar("SelectAddressDetails"));

        }

        public string ContractIDAddressDetails(IContractRequest I)
        {

            Parameters.AddWithValue("@ContractID", I.ContractID);//4

            return Convert.ToString(getExcuteScalar("SelectContractIDAddressDetails"));

        }

        public List<SelectControlFields> SelectContractType(IContractRequest I)
        {
            try
            {
                Parameters.AddWithValue("@UserId", I.UserID);
                SqlDataReader dr = getExecuteReader("MasterSelectContractType");
                return SelectControlFields.SelectData(dr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ReadEmailDetails(IContractRequest I)
        {

            Parameters.AddWithValue("@ClientId", I.ClientID);//4

            return Convert.ToString(getExcuteScalar("SelectEmailDetails"));

        }

        public string ReadContactDetails(IContractRequest I)
        {

            Parameters.AddWithValue("@ClientId", I.ClientID);//4

            return Convert.ToString(getExcuteScalar("SelectContactDetails"));

        }

        public string ReadPrimaryAddress(IContractRequest I)
        {

            Parameters.AddWithValue("@RequestId", I.RequestID);//4
            return Convert.ToString(getExcuteScalar("SelectPrimaryContacts"));

        }


        //public string ReadOtherDetails(IContractRequest I)
        //{

        //    Parameters.AddWithValue("@ContractId", I.ClientID);//4
        //    return Convert.ToString(getExcuteScalar("SelectOtherDetails"));
        //}

        public string ReadOtherDetails(IContractRequest I)
        {
            int i = 0;

            List<ContractRequest> myList = new List<ContractRequest>();

            Parameters.AddWithValue("@ContractId", I.ClientID);
            SqlDataReader dr = getExecuteReader("SelectOtherDetails");
            try
            {
                i = 0;

                while (dr.Read())
                {

                    myList.Add(new ContractRequest());

                    myList[i].ContractTypeID = Convert.ToInt32(dr["ContractTypeID"]);
                    myList[i].ContractingpartyId = Convert.ToInt32(dr["ContractingTypeId"]);
                    myList[i].ContractTerm = (dr["ContractTerm"].ToString());
                    myList[i].ContractValue = Convert.ToInt64(dr["ContractValue"]);
                    myList[i].isCustomer = dr["isCustomer"].ToString();
                    myList[i].DeadlineDate = dr["DeadlineDate"].ToString();
                    myList[i].CurrencyID = Convert.ToInt32(dr["CurrencyID"]);
                    myList[i].Priority = Convert.ToString(dr["Priority"]);
                    myList[i].PriorityReason = Convert.ToString(dr["PriorityReason"]);     
                    i = i + 1;
                }
                JavaScriptSerializer jsonSerializaer = new JavaScriptSerializer();

                return jsonSerializaer.Serialize(myList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
        }

        public string ReadMetaDataFieldsDetails(IContractRequest I)
        {
            int i = 0;

            List<ContractRequest> myList = new List<ContractRequest>();

            Parameters.AddWithValue("@ID", I.ContractID);
            Parameters.AddWithValue("@MetaDataType", "Request Form");
            Parameters.AddWithValue("@Type", "CONTRACTID");
            SqlDataReader dr = getExecuteReader("ReadContractFieldForContract");
            try
            {
                i = 0;

                while (dr.Read())
                {

                    myList.Add(new ContractRequest());
                    myList[i].FieldID = (dr["ID"].ToString());
                    myList[i].FieldValue = (dr["VALUE"].ToString());

                    //myList[i].ContractTypeID = Convert.ToInt32(dr["ContractTypeID"]);
                    //myList[i].ContractingpartyId = Convert.ToInt32(dr["ContractingTypeId"]);
                    //myList[i].ContractTerm = (dr["ContractTerm"].ToString());
                    //myList[i].ContractValue = Convert.ToInt64(dr["ContractValue"]);
                    //myList[i].isCustomer = dr["isCustomer"].ToString();
                    //myList[i].DeadlineDate = dr["DeadlineDate"].ToString();

                    i = i + 1;
                }
                JavaScriptSerializer jsonSerializaer = new JavaScriptSerializer();

                return jsonSerializaer.Serialize(myList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
        }

        public void InsertManualEntryDetails(IContractRequest I)
        {
            Parameters.AddWithValue("@BulkImportId", I.BulkImportId);//9
            Parameters.AddWithValue("@ClientName", I.ClientName);//9
            Parameters.AddWithValue("@Address", I.Address);//10
            Parameters.AddWithValue("@CountryId", I.CountryID);//13
            Parameters.AddWithValue("@ContactNumber", I.ContactNumber);//34
            Parameters.AddWithValue("@EmailId", I.EmailID);//34
            Parameters.AddWithValue("@ContractingpartyId", I.ContractingpartyId);//34
            Parameters.AddWithValue("@isCustomer", I.isCustomer);//34
            Parameters.AddWithValue("@Street1", I.Street1);//34
            Parameters.AddWithValue("@ContractTerm", I.ContractTerm);//34
            Parameters.AddWithValue("@ContractValue", I.ContractValue);//34
            Parameters.AddWithValue("@AddedBy", I.AddedBy);//34
            Parameters.AddWithValue("@Others", I.Others);//34
            Parameters.AddWithValue("@CityName", I.CityName);
            Parameters.AddWithValue("@StateName", I.StateName);
            Parameters.AddWithValue("@Pincode", I.Pincode);
            Parameters.AddWithValue("@CurrencyId", I.CurrencyID);
            Parameters.AddWithValue("@IsCustomerPortalEnable", I.IsCustomerPortalEnable);
            Parameters.AddWithValue("@MataDataTypeValue", I.dtMatadataFieldValues);
            Parameters.AddWithValue("@Priority", I.Priority);
            Parameters.AddWithValue("@PriorityReason", I.PriorityReason);
            string rvalue = getExcuteQuery("InsertRequestDetailsFromManualEntry");

        }

        public string InsertRecord(IContractRequest I)
        {
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeID);//4
            Parameters.AddWithValue("@Clientid", I.ClientID);//8
            Parameters.AddWithValue("@ClientName", I.ClientName);//9
            Parameters.AddWithValue("@Address", I.Address);//10
            Parameters.AddWithValue("@CityName", I.CityName);//11
            Parameters.AddWithValue("@StateName", I.StateName);//12
            Parameters.AddWithValue("@CountryId", I.CountryID);//13
            Parameters.AddWithValue("@Pincode", I.Pincode);//17
            Parameters.AddWithValue("@RequesterId", I.RequestUserID);//18
            Parameters.AddWithValue("@ContractDescription", I.ContractDescription);//20
            Parameters.AddWithValue("@deadlinedate", I.DeadlineDate);//21
            Parameters.AddWithValue("@assigntoid", I.AssignToId);//22
            Parameters.AddWithValue("@deptid", I.DepartmentId);//23
            Parameters.AddWithValue("@DecumnetIds", I.DocumentIDs);//23
            Parameters.AddWithValue("@AddedBy", I.AddedBy);//30
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);//32
            Parameters.AddWithValue("@IpAddress", I.IpAddress);//33
            Parameters.AddWithValue("@RequestType", I.RequestType);//34
            Parameters.AddWithValue("@ContactNumber", I.ContactNumber);//34
            Parameters.AddWithValue("@EmailID", I.EmailID);//34
            Parameters.AddWithValue("@AddressID", I.AddressID);//34
            Parameters.AddWithValue("@ContactDetailID", I.ContactDetailID);//34
            Parameters.AddWithValue("@EmailDetailID", I.EmailContactID);//34
            Parameters.AddWithValue("@isApprovalRequired", I.isApprovalRequried);//34
            Parameters.AddWithValue("@EstimatedValue", I.EstimatedValue);//34
            Parameters.AddWithValue("@ContractID", I.ContractID);//34
            Parameters.AddWithValue("@ContractingpartyId", I.ContractingpartyId);//34
            Parameters.AddWithValue("@isCustomer", I.isCustomer);//34
            Parameters.AddWithValue("@Street1", I.Street1);//34
            Parameters.AddWithValue("@ContractTerm", I.ContractTerm);//34
            Parameters.AddWithValue("@ContractValue", I.ContractValue);//34
            Parameters.AddWithValue("@MataDataTypeValue", I.dtMatadataFieldValues); // js 24-12-2015
            Parameters.AddWithValue("@CreatedRequestId", 0);//34
            Parameters.AddWithValue("@CurrencyID", I.CurrencyID);
            Parameters.AddWithValue("@IsCustomerPortalEnable", I.IsCustomerPortalEnable);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            Parameters["@CreatedRequestId"].Direction = ParameterDirection.Output;
            Parameters.AddWithValue("@Priority", I.Priority);
            Parameters.AddWithValue("@PriorityReason", I.PriorityReason);
            // sk 1 july 2014
            string rvalue = getExcuteQuery("ContractRequestAddUpdate", "@Status", "@CreatedRequestId");
            I.RequestID = int.Parse(rvalue.Split(',')[1]);
            return rvalue.Split(',')[0];


        }

        public string AIInsertRecord(IContractRequest I)
        {
            string rvalue = "";
            string ErrorLog = TenantUploadFolderPath + "/AIError.txt";
            try
            {
                Parameters.AddWithValue("@AIOriginalFileName", I.AIOriginalFileName);
                Parameters.AddWithValue("@AIUpdatedFileName", I.AIUpdatedFileName);
                Parameters.AddWithValue("@ContractTypeName", I.ContractTypeName);
                Parameters.AddWithValue("@ContractingPartyName", I.ContractingpartyName);
                Parameters.AddWithValue("@ClientName", I.ClientName);
                Parameters.AddWithValue("@ClientAddressId", I.AddressID);
                Parameters.AddWithValue("@ClientAddress", I.Street1);
                Parameters.AddWithValue("@CountryName", I.CountryName);
                Parameters.AddWithValue("@AIFileSize", I.AIFileSize);
                Parameters.AddWithValue("@IsPDF", I.isPDF);
                Parameters.AddWithValue("@UserId", I.UserID);
                Parameters.AddWithValue("@DepartmentId", I.DepartmentId);
                Parameters.AddWithValue("@IpAddress", I.IpAddress);
                Parameters.AddWithValue("@Status", 0);
                Parameters.Add("@ContractFileName", SqlDbType.VarChar, 500);
                Parameters["@ContractFileName"].Value = "";

                Parameters["@Status"].Direction = ParameterDirection.Output;
                Parameters["@ContractFileName"].Direction = ParameterDirection.Output;
                rvalue = getExcuteQuery("AIContractRequestAdd", "@Status", "@ContractFileName");
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText(ErrorLog, "\n\nAI Exception: " + ex.Message);
            }
            finally
            {
                ErrorLog = null;
            }
            return rvalue;
        }

        public string UpdateRecord(IContractRequest I)
        {
            Parameters.AddWithValue("@RequestId", I.RequestID);//1
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeID);//4
            Parameters.AddWithValue("@Clientid", I.ClientID);//8
            Parameters.AddWithValue("@ClientName", I.ClientName);//9
            Parameters.AddWithValue("@Address", I.Address);//10
            Parameters.AddWithValue("@CityName", I.CityName);//11
            Parameters.AddWithValue("@StateName", I.StateName);//12
            Parameters.AddWithValue("@CountryId", I.CountryID);//13
            Parameters.AddWithValue("@Pincode", I.Pincode);//17
            Parameters.AddWithValue("@RequesterId", I.RequestUserID);//18
            Parameters.AddWithValue("@ContractDescription", I.ContractDescription);//20
            Parameters.AddWithValue("@deadlinedate", I.DeadlineDate);//21
            Parameters.AddWithValue("@assigntoid", I.AssignToId);//22
            Parameters.AddWithValue("@deptid", I.DepartmentId);//23
            Parameters.AddWithValue("@DecumnetIds", I.DocumentIDs);//23
            Parameters.AddWithValue("@AddedBy", I.AddedBy);//30
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);//32
            Parameters.AddWithValue("@IpAddress", I.IpAddress);//33
            Parameters.AddWithValue("@RequestType", I.RequestType);//34
            Parameters.AddWithValue("@ContactNumber", I.ContactNumber);//34
            Parameters.AddWithValue("@EmailID", I.EmailID);//34
            Parameters.AddWithValue("@AddressID", I.AddressID);//34
            Parameters.AddWithValue("@ContactDetailID", I.ContactDetailID);//34
            Parameters.AddWithValue("@EmailDetailID", I.EmailContactID);//34
            Parameters.AddWithValue("@isApprovalRequired", I.isApprovalRequried);//34
            Parameters.AddWithValue("@EstimatedValue", I.EstimatedValue);//34
            Parameters.AddWithValue("@ContractID", I.ContractID);//34
            Parameters.AddWithValue("@ContractingpartyId", I.ContractingpartyId);//34
            Parameters.AddWithValue("@isCustomer", I.isCustomer);//34
            Parameters.AddWithValue("@Street1", I.Street1);//34
            Parameters.AddWithValue("@ContractTerm", I.ContractTerm);//34
            Parameters.AddWithValue("@ContractValue", I.ContractValue);//34
            Parameters.AddWithValue("@MataDataTypeValue", I.dtMatadataFieldValues); // js 24-12-2015
            Parameters.AddWithValue("@CreatedRequestId", 0);//34
            Parameters.AddWithValue("@CurrencyID", I.CurrencyID);
            Parameters.AddWithValue("@IsCustomerPortalEnable", I.IsCustomerPortalEnable);
            Parameters.AddWithValue("@Priority", I.Priority);
            Parameters.AddWithValue("@PriorityReason", I.PriorityReason);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            Parameters["@CreatedRequestId"].Direction = ParameterDirection.Output;
            // sk 1 july 2014
            string rvalue = getExcuteQuery("ContractRequestAddUpdate", "@Status", "@CreatedRequestId");
            I.RequestID = int.Parse(rvalue.Split(',')[1]);
            return rvalue.Split(',')[0];
        }

        public string DeleteRecord(IContractRequest I)
        {
            Parameters.AddWithValue("@RequestIds", I.RequestIDs);
            Parameters.AddWithValue("@UserID", I.UserID);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@RequestActivityAssignee", null);
            Parameters["@RequestActivityAssignee"].Size = 500;
            Parameters["@RequestActivityAssignee"].Direction = ParameterDirection.Output;

            string RequestActivityAssignee = getExcuteQuery("MasterRequestDelete", "@RequestActivityAssignee");
            OC.RequestIds = I.RequestIDs;
            OC.AssignToIds = RequestActivityAssignee;
            OC.UpdateOutlookFile();
            return RequestActivityAssignee;

        }

        public List<SelectControlFields> SelectContractingPartyData(IContractRequest I)
        {
            SqlDataReader dr = getExecuteReader("MasterContractingParty");
            return SelectControlFields.SelectData(dr);
        }

        public List<SelectControlFields> SelectClientData(IContractRequest I)
        {
            SqlDataReader dr = getExecuteReader("SelectClientData");
            return SelectControlFields.SelectData(dr);
        }

        public string IsClientExist(IContractRequest I)
        {
            Parameters.AddWithValue("@ClientName", I.ClientName);
            string IsClientExist = getExcuteScalar("IsClientExist");
            return IsClientExist;
        }

        public List<SelectControlFields> SelectClientDataWithContract(IContractRequest I)
        {
            Parameters.AddWithValue("@UserId", I.UserID);
            SqlDataReader dr = getExecuteReader("SelectClientDataWithContract");
            return SelectControlFields.SelectData(dr);
        }

        public List<SelectControlFields> SelectData(IContractRequest I)
        {
            SqlDataReader dr = getExecuteReader("ContractRequestSelect");
            return SelectControlFields.SelectData(dr);
        }

        public List<SelectControlFields> SelectOtherData(IContractRequest I)
        {
            SqlDataReader dr = getExecuteReader("MasterRequesterSelect");
            return SelectControlFields.SelectData(dr);
        }

        public List<SelectControlFields> SelectDeptData(IContractRequest I)
        {
            Parameters.AddWithValue("@RequestId", I.RequestID);
            SqlDataReader dr = getExecuteReader("MasterDepartmentSelect");
            return SelectControlFields.SelectData(dr);
        }

        public List<ContractRequest> ReadData(IContractRequest I)
        {
            int i = 0;
            List<ContractRequest> myList = new List<ContractRequest>();
            Parameters.AddWithValue("@RequestId", I.RequestID);
            Parameters.AddWithValue("@UserID", I.UserID);
            Parameters.AddWithValue("@ContractID", I.ContractID);
            Parameters.AddWithValue("@RequestTypeShortCode", I.RequestTypeID);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@ClientName", I.ClientName);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            Parameters.AddWithValue("@DashboardRequestType", I.DashboardRequestType);
            Parameters.AddWithValue("@ContractTypeName", I.ContractTypeName);
            Parameters.AddWithValue("@ContractStatusName", I.ContractStatus);
            Parameters.AddWithValue("@Tenure", I.Tenure);

            SqlDataReader dr = getExecuteReader("ContractRequestRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new ContractRequest());
                    myList[i].RequestID = int.Parse(dr["RequestId"].ToString());//1
                    myList[i].RequestNo = dr["RequestNo"].ToString();//2
                    if (dr["ContractID"].ToString() != "")
                    {
                        myList[i].ContractID = int.Parse(dr["ContractID"].ToString());//3
                    }
                    else
                    {
                        myList[i].ContractID = 0;//3
                    }
                    myList[i].ContractTypeID = int.Parse(dr["ContractTypeId"].ToString());//4
                    myList[i].ClientID = int.Parse(dr["ClientId"].ToString());//6
                    myList[i].ClientName = dr["ClientName"].ToString();//7
                    myList[i].Address = dr["Address"].ToString();//8
                    myList[i].CountryID = int.Parse(dr["CountryId"].ToString());//9
                    myList[i].StateName = dr["StateName"].ToString();//11
                    myList[i].CityName = dr["CityName"].ToString();//13
                    myList[i].Pincode = dr["PinCode"].ToString();//15
                    myList[i].RequestUserID = int.Parse(dr["RequesterUserId"].ToString());//17
                    myList[i].ContractDescription = dr["RequestDescription"].ToString();//request descrption 18
                    myList[i].DeadlineDate = Convert.ToString(dr["DeadlineDate"].ToString());//19
                    myList[i].DeadlineDateStr = dr["DeadlineDate"].ToString();
                    myList[i].ContractStatus = dr["ContractStatus"].ToString();
                    myList[i].ContractValuewithcurrency = dr["ContractValuewithcurrency"].ToString();
                    myList[i].IsNotified = dr["IsNotified"].ToString();
                    myList[i].IsContractTypeEditable = Convert.ToBoolean(dr["IsContractTypeEditable"].ToString());
                    myList[i].IsTreeLinkVisible = Convert.ToBoolean(dr["IsTreeLinkVisible"].ToString());

                    if (dr["AssignToUserID"].ToString() == "")
                    {
                        myList[i].AssignToId = 0;
                    }
                    else
                    {
                        myList[i].AssignToId = int.Parse(dr["AssignToUserID"].ToString());
                    }
                    if (dr["AssignToDepartmentId"].ToString() == "")
                    {
                        myList[i].DepartmentId = 0;
                    }
                    else
                    {
                        myList[i].DepartmentId = int.Parse(dr["AssignToDepartmentId"].ToString());
                    }

                    myList[i].RequestTypeName = dr["RequestTypeName"].ToString();
                    myList[i].Description = dr["RequestDescription"].ToString();
                    myList[i].IsUsed = dr["isUsed"].ToString();
                    myList[i].isActive = dr["isActive"].ToString();
                    myList[i].ContractTypeName = dr["ContractTypeName"].ToString();
                    myList[i].RequesterUserName = dr["RequesterUserName"].ToString();
                    myList[i].AssignerUserName = dr["AssignerUserName"].ToString();
                    myList[i].isApprovalRequried = dr["isApprovalRequired"].ToString();
                    myList[i].EstimatedValue = dr["EstimatedValue"].ToString();
                    myList[i].RequestTypeID = Convert.ToInt32(dr["RequestTypeID"].ToString());
                    myList[i].isCustomer = dr["isCustomer"].ToString();
                    myList[i].ContractingpartyId = Convert.ToInt32(dr["ContractingTypeId"].ToString());
                    myList[i].ContractTerm = dr["ContractTerm"].ToString();
                    myList[i].ContractValue = Convert.ToDecimal(dr["ContractValue"].ToString());
                    myList[i].CurrencyID = Convert.ToInt32(dr["CurrencyID"].ToString());
                    myList[i].IsDocumentGenerated = Convert.ToString(dr["IsDocumentGenerated"].ToString());
                    myList[i].IsBulkImport = Convert.ToString(dr["IsBulkImport"].ToString());
                    myList[i].IsCustomerPortalEnable = Convert.ToString(dr["IsCustomerPortalEnable"].ToString());
                    myList[i].Priority = Convert.ToString(dr["Priority"].ToString());
                    myList[i].PriorityReason = Convert.ToString(dr["PriorityReason"].ToString());
                    
                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
        }

        public List<ContractRequest> ReadDataMyContracts(IContractRequest I)
        {
            int i = 0;
            List<ContractRequest> myList = new List<ContractRequest>();
            Parameters.AddWithValue("@RequestId", I.RequestID);
            Parameters.AddWithValue("@UserID", I.UserID);
            Parameters.AddWithValue("@ContractID", I.ContractID);
            Parameters.AddWithValue("@RequestTypeShortCode", I.RequestTypeID);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@ClientName", I.ClientName);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            Parameters.AddWithValue("@DashboardRequestType", I.DashboardRequestType);
            Parameters.AddWithValue("@ContractTypeName", I.ContractTypeName);
            Parameters.AddWithValue("@ContractStatusName", I.ContractStatus);
            Parameters.AddWithValue("@Tenure", I.Tenure);

            SqlDataReader dr = getExecuteReader("MyContractsRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new ContractRequest());
                    myList[i].RequestID = int.Parse(dr["RequestId"].ToString());
                    myList[i].ContractID = int.Parse(dr["ContractID"].ToString());
                    myList[i].RequestTypeName = dr["RequestTypeName"].ToString();
                    myList[i].ContractTypeName = dr["ContractTypeName"].ToString();
                    myList[i].ClientName = dr["ClientName"].ToString();
                    myList[i].RequesterUserName = dr["RequesterUserName"].ToString();
                    myList[i].ContractValue = Convert.ToDecimal(dr["ContractValue"].ToString());
                    myList[i].AssignerUserName = dr["AssignerUserName"].ToString();
                    myList[i].ContractStatus = dr["StatusName"].ToString();
                    myList[i].IsUsed = dr["isUsed"].ToString();
                    myList[i].IsTreeLinkVisible = Convert.ToBoolean(dr["IsTreeLinkVisible"].ToString());

                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
        }       

        public string ChangeIsActive(IContractRequest I)
        {
            //Parameters.AddWithValue("@CityIds", I.CityIds);
            //Parameters.AddWithValue("@isActive", I.isActive);
            return getExcuteQuery("MasterContractRequestDetailsChangeIsActive");
        }

        #region Added by Bharati 02062014 3.44PM
        public DataSet ReadContractDetails(IContractRequest I)
        {
            Parameters.AddWithValue("@RequestId", I.RequestID);
            Parameters.AddWithValue("@UserID", I.UserID);
            return getDataSet("ContractRequestDetails");
        }
        #endregion

        /*
        public List<ContractRequest> ReadClientData(IContractRequest I)
       
        {
            int i = 0;
            Parameters.AddWithValue("@searchtxt", I.searchtxt);
         
            SqlDataReader dr = getExecuteReader("MasterCLientSelect");
            List<ContractRequest> myList = new List<ContractRequest>();
            try
            {
                while (dr.Read())
                {
                    myList.Add(new ContractRequest());
                    myList[i].ClientID = int.Parse(dr["ClientId"].ToString());//1
                    myList[i].ClientName = dr["ClientName"].ToString();//2
                    myList[i].Address = dr["Address"].ToString();
                    myList[i].CityID = int.Parse(dr["CityId"].ToString());
                    myList[i].StateID = int.Parse(dr["StateId"].ToString());
                    myList[i].CountryID = int.Parse(dr["CountryId"].ToString());
                    myList[i].Pincode = dr["PinCode"].ToString();
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
           
        }
         * 
*/

        public List<ContractRequest> ReadClientData(IContractRequest I)
        {
            int i = 0;
            // Parameters.AddWithValue("@searchtxt", I.searchtxt);
            SqlDataReader dr;
            if (I.RequestTypeID == 1)
            {
                dr = getExecuteReader("MasterCLientSelect");
            }
            else
            {
                dr = getExecuteReader("MasterClientSelectWithContractID");
            }


            List<ContractRequest> myList = new List<ContractRequest>();
            try
            {
                while (dr.Read())
                {
                    myList.Add(new ContractRequest());
                    myList[i].ClientID = int.Parse(dr["ClientId"].ToString());//1
                    myList[i].ClientName = dr["ClientName"].ToString();//2
                    myList[i].Address = dr["Address"].ToString();
                    myList[i].CityName = dr["CityName"].ToString();
                    myList[i].StateName = dr["StateName"].ToString();
                    myList[i].CountryID = int.Parse(dr["CountryId"].ToString());
                    myList[i].Pincode = dr["PinCode"].ToString();
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;

        }

        public List<SelectControlFields> ReadClientWithContracts(IContractRequest I)
        {
            SqlDataReader dr = getExecuteReader("ContractRequestReadForCalendar");
            return SelectControlFields.SelectData(dr);
        }

        public List<ContractRequest> ContractVersionsFile(IContractRequest I)
        {
            try
            {
                int i = 0;
                List<ContractRequest> List = new List<ContractRequest>();
                Parameters.AddWithValue("@RequestId", I.RequestIDs);
                SqlDataReader dr = getExecuteReader("MasterRequestDeletefile");
                while (dr.Read())
                {
                    List.Add(new ContractRequest());
                    List[i].FileName = dr["FullFileName"].ToString();
                    List[i].isPDF = dr["isPDFFile"].ToString();
                    List[i].FileLoc = dr["FileLocation"].ToString();
                    List[i].docusign = dr["isFromDocuSign"].ToString();
                    i++;
                }
                return List;
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public string ReadIsCustomerDetails(IContractRequest I)
        {

            Parameters.AddWithValue("@ClientId", I.ClientID);//4

            return Convert.ToString(getExcuteScalar("SelectIsCustomerDetails"));

        }

        public DataTable GetContractTypeIDByTemplateID(IContractRequest I)
        {
            try
            {
                DataTable dt = new DataTable();
                Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
                dt = getDataTable("GetContractTypeIDByTemplateID");
                try
                {
                    return dt;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields> SelectRequestType(IContractRequest I)
        {
            SqlDataReader dr = getExecuteReader("ContractRequestTypeSelect");
            return SelectControlFields.SelectData(dr, 1);
        }

        public string BuildCacheString(IContractRequest I)
        {
            Parameters.AddWithValue("@RequestId", I.RequestID);
            string RetValue = getExcuteScalar("ContractRequestBuildCacheString");
            return RetValue;
        }
    }
}
