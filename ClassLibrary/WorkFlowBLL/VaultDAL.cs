﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;


namespace WorkflowBLL
{
    public class VaultDAL : DAL
    {

        public int ValidateContract(IVault I)
        {
            Parameters.AddWithValue("@ContractID", I.ContractID);//4
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return Convert.ToInt32(getExcuteQuery("ValidateContractID", "@Status"));
        }


        public string InsertRecord(IVault I)
        {
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeID);//4
            Parameters.AddWithValue("@Clientid", I.ClientID);//8
            Parameters.AddWithValue("@ClientName", I.ClientName);//9
            Parameters.AddWithValue("@Address", I.Address);//10
            Parameters.AddWithValue("@CityName", I.CityName);//11
            Parameters.AddWithValue("@StateName", I.StateName);//12
            Parameters.AddWithValue("@CountryId", I.CountryID);//13
            Parameters.AddWithValue("@Pincode", I.Pincode);//17
            Parameters.AddWithValue("@RequesterId", I.RequestUserID);//18
            Parameters.AddWithValue("@ContractDescription", I.ContractDescription);//20
            Parameters.AddWithValue("@deadlinedate", I.DeadlineDate);//21
            Parameters.AddWithValue("@assigntoid", I.AssignToId);//22
            Parameters.AddWithValue("@deptid", I.DepartmentId);//23
            Parameters.AddWithValue("@DecumnetIds", I.DocumentIDs);//23
            Parameters.AddWithValue("@AddedBy", I.AddedBy);//30
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);//32
            Parameters.AddWithValue("@IpAddress", I.IpAddress);//33
            Parameters.AddWithValue("@RequestType", I.RequestType);//34
            Parameters.AddWithValue("@ContactNumber", I.ContactNumber);//34
            Parameters.AddWithValue("@EmailID", I.EmailID);//34
            Parameters.AddWithValue("@AddressID", I.AddressID);//34
            Parameters.AddWithValue("@ContactDetailID", I.ContactDetailID);//34
            Parameters.AddWithValue("@EmailDetailID", I.EmailContactID);//34
            Parameters.AddWithValue("@isApprovalRequired", I.isApprovalRequried);//34
            Parameters.AddWithValue("@EstimatedValue", I.EstimatedValue);//34
            Parameters.AddWithValue("@ContractID", I.ContractID);//34
            Parameters.AddWithValue("@CreatedRequestId", 0);//34
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            Parameters["@CreatedRequestId"].Direction = ParameterDirection.Output;
            // sk 1 july 2014
            string rvalue = getExcuteQuery("ContractRequestAddUpdate", "@Status", "@CreatedRequestId");
            I.RequestID = int.Parse(rvalue.Split(',')[1]);
            return rvalue.Split(',')[0];


        }

        public string UpdateRecord(IVault I)
        {
            Parameters.AddWithValue("@RequestId", I.RequestID);//1
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeID);//4
            Parameters.AddWithValue("@Clientid", I.ClientID);//8
            Parameters.AddWithValue("@ClientName", I.ClientName);//9
            Parameters.AddWithValue("@Address", I.Address);//10
            Parameters.AddWithValue("@CityName", I.CityName);//11
            Parameters.AddWithValue("@StateName", I.StateName);//12
            Parameters.AddWithValue("@CountryId", I.CountryID);//13
            Parameters.AddWithValue("@Pincode", I.Pincode);//17
            Parameters.AddWithValue("@RequesterId", I.RequestUserID);//18
            Parameters.AddWithValue("@ContractDescription", I.ContractDescription);//20
            Parameters.AddWithValue("@deadlinedate", I.DeadlineDate);//21
            Parameters.AddWithValue("@assigntoid", I.AssignToId);//22
            Parameters.AddWithValue("@deptid", I.DepartmentId);//23
            Parameters.AddWithValue("@DecumnetIds", I.DocumentIDs);//23
            Parameters.AddWithValue("@AddedBy", I.AddedBy);//30
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);//32
            Parameters.AddWithValue("@IpAddress", I.IpAddress);//33
            Parameters.AddWithValue("@RequestType", I.RequestType);//34
            Parameters.AddWithValue("@ContactNumber", I.ContactNumber);//34
            Parameters.AddWithValue("@EmailID", I.EmailID);//34
            Parameters.AddWithValue("@AddressID", I.AddressID);//34
            Parameters.AddWithValue("@ContactDetailID", I.ContactDetailID);//34
            Parameters.AddWithValue("@EmailDetailID", I.EmailContactID);//34
            Parameters.AddWithValue("@isApprovalRequired", I.isApprovalRequried);//34
            Parameters.AddWithValue("@EstimatedValue", I.EstimatedValue);//34
            Parameters.AddWithValue("@ContractID", I.ContractID);//34
            Parameters.AddWithValue("@CreatedRequestId", 0);//34
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            Parameters["@CreatedRequestId"].Direction = ParameterDirection.Output;
            // sk 1 july 2014
            string rvalue = getExcuteQuery("ContractRequestAddUpdate", "@Status", "@CreatedRequestId");
            I.RequestID = int.Parse(rvalue.Split(',')[1]);
            return rvalue.Split(',')[0];
        }

        public string DeleteRecord(IVault I)
        {
            Parameters.AddWithValue("@RequestIds", I.RequestIDs);
            return getExcuteQuery("MasterRequestDelete");
        }

        public List<SelectControlFields> SelectData(IVault I)
        {
            SqlDataReader dr = getExecuteReader("ContractRequestSelect");
            return SelectControlFields.SelectData(dr);
        }

        public List<SelectControlFields> SelectOtherData(IVault I)
        {
            SqlDataReader dr = getExecuteReader("MasterRequesterSelect");
            return SelectControlFields.SelectData(dr);
        }

        public List<SelectControlFields> SelectDeptData(IVault I)
        {
            SqlDataReader dr = getExecuteReader("MasterDepartmentSelect");
            return SelectControlFields.SelectData(dr);
        }


        public List<Vault> ReadData(IVault I)
        {
            int i = 0;
            List<Vault> myList = new List<Vault>();
         
            Parameters.AddWithValue("@ContractID", I.ContractID);
            Parameters.AddWithValue("@UserID", I.UserID); 
          
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@ClientName", I.ClientName);

            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);


            SqlDataReader dr = getExecuteReader("ValultRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new Vault());
                    myList[i].RequestID = int.Parse(dr["RequestId"].ToString());//1


                    myList[i].RequestNo = Convert.ToString(dr["RequestNo"]);//2

                    if (dr["ContractID"].ToString() != "")
                    {
                        myList[i].ContractID = int.Parse(dr["ContractID"].ToString());//3
                    }
                    else 
                    {
                        myList[i].ContractID =0;//3
                    }

                    if (Convert.ToString(dr["ContractTypeId"]).Trim().Length > 0)
                        myList[i].ContractTypeID = int.Parse(dr["ContractTypeId"].ToString());//4
                    else
                        myList[i].ContractTypeID = 0;

                    if (Convert.ToString(dr["ClientId"]).Trim().Length > 0)
                        myList[i].ClientID = int.Parse(dr["ClientId"].ToString());//6
                    else
                        myList[i].ClientID = 0;



                    myList[i].ClientName = Convert.ToString(dr["ClientName"]);//7
                    myList[i].Address = Convert.ToString(dr["Address"]);//8

                    if (Convert.ToString(dr["CountryId"]).Trim().Length > 0)
                        myList[i].CountryID = int.Parse(dr["CountryId"].ToString());//9
                    else
                        myList[i].CountryID = 0;

                    if (Convert.ToString(dr["StateName"]).Trim().Length > 0)
                        myList[i].StateName = dr["StateName"].ToString();//11
                    else
                        myList[i].StateName = "";

                    if (Convert.ToString(dr["CityName"]).Trim().Length > 0)
                        myList[i].CityName = dr["CityName"].ToString();//13
                    else
                        myList[i].CityName = "";


                    myList[i].Pincode = Convert.ToString(dr["PinCode"]);//15


                    if (Convert.ToString(dr["RequesterUserId"]).Trim().Length > 0)
                        myList[i].RequestUserID = int.Parse(dr["RequesterUserId"].ToString());//17
                    else
                        myList[i].RequestUserID = 0;


                    myList[i].ContractDescription = dr["RequestDescription"].ToString();//request descrption 18
                    myList[i].DeadlineDate = Convert.ToString(dr["DeadlineDate"].ToString());//19
                    myList[i].DeadlineDateStr = Convert.ToString(dr["DeadlineDate"]);
                    //myList[i].ContractStatus = dr["ContractStatus"].ToString();

                    myList[i].IsBulkImport = Convert.ToString(dr["IsBulkImport"]);

                    if (Convert.ToString(dr["AssignToUserID"]) == "")
                    {
                        myList[i].AssignToId = 0;
                    }
                    else
                    {
                        myList[i].AssignToId = int.Parse(Convert.ToString(dr["AssignToUserID"]));
                    }
                    if (Convert.ToString(dr["AssignToDepartmentId"]) == "")
                    {
                        myList[i].DepartmentId = 0;
                    }
                    else
                    {
                        myList[i].DepartmentId = int.Parse(dr["AssignToDepartmentId"].ToString());
                    }

                    myList[i].RequestTypeName = Convert.ToString(dr["RequestTypeName"]);
                    myList[i].Description = Convert.ToString(dr["RequestDescription"]);
                    myList[i].IsUsed = Convert.ToString(dr["isUsed"]);
                    myList[i].isActive = Convert.ToString(dr["isActive"]);
                    myList[i].ContractTypeName = Convert.ToString(dr["ContractTypeName"]);
                    myList[i].RequesterUserName = Convert.ToString(dr["RequesterUserName"]);
                    myList[i].AssignerUserName = Convert.ToString(dr["AssignerUserName"]);
                    myList[i].isApprovalRequried = Convert.ToString(dr["isApprovalRequired"]);
                    myList[i].EstimatedValue = Convert.ToString(dr["EstimatedValue"]);
                    myList[i].Status = Convert.ToString(dr["Status"]);

                    if (Convert.ToString(dr["RequestTypeID"]).Trim().Length > 0)
                        myList[i].RequestTypeID = Convert.ToInt32(Convert.ToString(dr["RequestTypeID"]));
                    else
                        myList[i].RequestTypeID = 0;
                    
                    

                    i = i + 1;
                }


                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;

        }

        public string ChangeIsActive(IVault I)
        {
            //Parameters.AddWithValue("@CityIds", I.CityIds);
            //Parameters.AddWithValue("@isActive", I.isActive);
            return getExcuteQuery("MasterContractRequestDetailsChangeIsActive");
        }

      
        public DataSet ReadContractDetails(IVault I)
        {
            Parameters.AddWithValue("@RequestId", I.RequestID);
            return getDataSet("ContractVaultDetails");
        }
      

        /*
        public List<ContractRequest> ReadClientData(IContractRequest I)
       
        {
            int i = 0;
            Parameters.AddWithValue("@searchtxt", I.searchtxt);
         
            SqlDataReader dr = getExecuteReader("MasterCLientSelect");
            List<ContractRequest> myList = new List<ContractRequest>();
            try
            {
                while (dr.Read())
                {
                    myList.Add(new ContractRequest());
                    myList[i].ClientID = int.Parse(dr["ClientId"].ToString());//1
                    myList[i].ClientName = dr["ClientName"].ToString();//2
                    myList[i].Address = dr["Address"].ToString();
                    myList[i].CityID = int.Parse(dr["CityId"].ToString());
                    myList[i].StateID = int.Parse(dr["StateId"].ToString());
                    myList[i].CountryID = int.Parse(dr["CountryId"].ToString());
                    myList[i].Pincode = dr["PinCode"].ToString();
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
           
        }
         * 
*/


        public List<Vault> ReadClientData(IVault I)
        {
            int i = 0;
            // Parameters.AddWithValue("@searchtxt", I.searchtxt);
            SqlDataReader dr;
            if (I.RequestTypeID == 1)
            {
                dr = getExecuteReader("MasterCLientSelect");
            }
            else 
            {
                dr = getExecuteReader("MasterClientSelectWithContractID");
            }

          
            List<Vault> myList = new List<Vault>();
            try
            {
                while (dr.Read())
                {
                    myList.Add(new Vault());
                    myList[i].ClientID = int.Parse(dr["ClientId"].ToString());//1
                    myList[i].ClientName = dr["ClientName"].ToString();//2
                    myList[i].Address = dr["Address"].ToString();
                    myList[i].CityName = dr["CityName"].ToString();
                    myList[i].StateName = dr["StateName"].ToString();
                    myList[i].CountryID = int.Parse(dr["CountryId"].ToString());
                    myList[i].Pincode = dr["PinCode"].ToString();
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;

        }

        public List<SelectControlFields> ReadClientWithContracts(IVault I)
        {
            SqlDataReader dr = getExecuteReader("ContractRequestReadForCalendar");
            return SelectControlFields.SelectData(dr);
        }



        public List<Vault> ContractQuestionairDetailsForVaultFlow(IVault I)
        {
            try
            {
                int i = 0;
                List<Vault> List = new List<Vault>();
                Parameters.AddWithValue("@ContractId", I.ContractId);
                Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
                Parameters.AddWithValue("@RequestId", I.ContractRequestId);
                SqlDataReader dr = getExecuteReader("ContractQuestionairDetailsForVaultFlow");
                Vault QA = new Vault();
                while (dr.Read())
                {
                    List.Add(new Vault());
                    List[i].ModifiedByUserName = dr["UpdatedBy"].ToString();
                    List[i].ModifiedOnFormattedDate = dr["UpdatedOn"].ToString();
                    List[i].TemplateName = dr["TemplateName"].ToString();
                    i++;
                }
                dr.NextResult();
                i = 0;
                while (dr.Read())
                {
                    List.Add(new Vault());
                    List[i].Question = dr["Question"].ToString();
                    List[i].Answer = dr["Answer"].ToString();

                    i = i + 1;
                }

                return List;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


    }
}
