﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;

using System.Globalization;
using System.IO;
using System.Configuration;

namespace WorkflowBLL
{
    public class ActivityDAL : DAL
    {
        string dirname = System.Web.HttpContext.Current.Request.Url.Segments[1].Replace("/", "");
        IOutlookCalendar OC = new OutlookCalendar();

        public string InsertRecord(IActivity I)
        {
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@ActivityTypeId", I.ActivityTypeId);
            Parameters.AddWithValue("@ActivityStatusId", I.ActivityStatusId);
            Parameters.AddWithValue("@ActivityText", I.ActivityText);
            Parameters.AddWithValue("@ActivityDate", I.ActivityDate);
            Parameters.AddWithValue("@IsForCustomer", I.IsForCustomer);
            Parameters.AddWithValue("@ReminderDate", I.ReminderDate);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@AssignToId", I.AssignToId);
            Parameters.AddWithValue("@IsFromCalendar", I.IsFromCalendar);

            Parameters.AddWithValue("@FileUpload", I.FileUpload);
            Parameters.AddWithValue("@FileUploadOriginal", I.FileUploadOriginal);
            Parameters.AddWithValue("@FileSizeKb", I.FileSizeKb);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            string ret = getExcuteQuery("ActivityAddUpdate", "@Status");

            OC.AssignToId = I.AssignToId;// Convert.ToInt32(System.Web.HttpContext.Current.Session[Declarations.User].ToString());
            OC.UpdateOutlookFile();
            return ret;
        }


        public string InsertActivityRecord(IActivity I)
        {
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@ActivityTypeId", I.ActivityTypeId);
            Parameters.AddWithValue("@ActivityStatusId", I.ActivityStatusId);
            Parameters.AddWithValue("@ActivityText", I.ActivityText);
            Parameters.AddWithValue("@ActivityDate", I.ActivityDate);
            Parameters.AddWithValue("@IsForCustomer", I.IsForCustomer);
            Parameters.AddWithValue("@ReminderDate", I.ReminderDate);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@AssignToId", I.AssignToId);
            Parameters.AddWithValue("@IsFromCalendar", I.IsFromCalendar);
            Parameters.AddWithValue("@IsOCR", I.IsOCR);

            Parameters.AddWithValue("@FileUpload", I.FileUpload);
            Parameters.AddWithValue("@FileUploadOriginal", I.FileUploadOriginal);
            Parameters.AddWithValue("@FileSizeKb", I.FileSizeKb);
            Parameters.AddWithValue("@Status", 0);
            Parameters.AddWithValue("@Ret_ActivityId", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            Parameters["@Ret_ActivityId"].Direction = ParameterDirection.Output;

            string ret= getExcuteQuery("ActivityAddUpdate_ActivityId", "@Status", "@Ret_ActivityId");
            OC.AssignToId = I.AssignToId;// Convert.ToInt32(System.Web.HttpContext.Current.Session[Declarations.User].ToString());
            OC.UpdateOutlookFile();
            //var runningTask = System.Threading.Tasks.Task.Factory.StartNew(() => UpdateOutlookFile(Convert.ToInt32(ret.Split(',')[1].ToString().Trim())));
            
            return ret;
        }

        public string UpdateRecord(IActivity I)
        {
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@ActivityId", I.ActivityId);
            Parameters.AddWithValue("@ActivityTypeId", I.ActivityTypeId);
            Parameters.AddWithValue("@ActivityStatusId", I.ActivityStatusId);
            Parameters.AddWithValue("@ActivityText", I.ActivityText);
            Parameters.AddWithValue("@ActivityDate", I.ActivityDate);
            Parameters.AddWithValue("@IsForCustomer", I.IsForCustomer);
            Parameters.AddWithValue("@ReminderDate", I.ReminderDate);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@AssignToId", I.AssignToId);
            Parameters.AddWithValue("@IsFromCalendar", I.IsFromCalendar);
            Parameters.AddWithValue("@IsOCR", I.IsOCR);

            Parameters.AddWithValue("@FileUpload", I.FileUpload);
            Parameters.AddWithValue("@FileUploadOriginal", I.FileUploadOriginal);
            Parameters.AddWithValue("@FileSizeKb", I.FileSizeKb);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;

            //var runningTask = System.Threading.Tasks.Task.Factory.StartNew(() => UpdateOutlookFile(I.ActivityId));            
            string ret = getExcuteQuery("ActivityAddUpdate", "@Status");

            OC.AssignToId = I.AssignToId;//Convert.ToInt32(System.Web.HttpContext.Current.Session[Declarations.User].ToString());
            OC.UpdateOutlookFile();

            if (I.AssignToId != I.ModifiedBy)
            {
                OC.AssignToId = I.ModifiedBy;
                OC.UpdateOutlookFile();
            }
            return ret;
        }

        public string DeleteRecord(IActivity I)
        {
            Parameters.AddWithValue("@ActivityId", I.ActivityIds);
            string retVal = getExcuteQuery("ActivityDelete");

            OC.AssignToId = Convert.ToInt32(System.Web.HttpContext.Current.Session[Declarations.User].ToString());
            OC.OutlookFileName = System.Web.HttpContext.Current.Session[Declarations.OutlookFileName].ToString();
            OC.UpdateOutlookFile();
            return retVal;
        }

        public List<SelectControlFields> SelectData(IActivity I)
        {
            SqlDataReader dr = getExecuteReader("ContractRequestSelect");
            return SelectControlFields.SelectData(dr);
        }

        public void ReadData(IActivity I)
        {
            int i = 0;

            List<Activity> myList4 = new List<Activity>();
            List<Activity> myList5 = new List<Activity>();
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@Flag", I.Flag);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@UserId", I.AddedBy);
            SqlDataReader dr = getExecuteReader("ActivityRead");
            try
            {

                //Parent Repeater
                while (dr.Read())
                {
                    myList4.Add(new Activity());
                    myList4[i].OnlyDate = Convert.ToDateTime(dr["OnlyDate"].ToString());
                    i = i + 1;
                }
                I.ActivityDates = myList4;

                //Child Repeater
                i = 0;
                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        myList5.Add(new Activity());
                        myList5[i].ActivityId = int.Parse(dr["ActivityId"].ToString());
                        myList5[i].ActivityTypeId = int.Parse(dr["ActivityTypeId"].ToString());
                        myList5[i].ActivityTypeName = dr["TypeName"].ToString();
                        myList5[i].ActivityText = dr["ActivityText"].ToString();
                        myList5[i].ActivityDate = Convert.ToDateTime(dr["ActivityDate"].ToString());
                        myList5[i].ActivityTime = dr["ActivityTime"].ToString();
                        //myList5[i].ActivityDateDefaultFormat = Convert.ToDateTime(dr["ActivityDateDefaultFormat"].ToString());
                        myList5[i].ActivityDateDefaultFormat = dr["ActivityDateDefaultFormat"].ToString();
                        myList5[i].AddedByName = dr["AddedBy"].ToString();
                        myList5[i].AddedOn = Convert.ToDateTime(dr["AddedOn"].ToString());
                        myList5[i].AddedTime = dr["AddedTime"].ToString();
                        myList5[i].OnlyDate = Convert.ToDateTime(dr["OnlyDate"].ToString());
                        myList5[i].ActivityStatusName = dr["StatusName"].ToString();
                        myList5[i].ActivityStatusId = int.Parse(dr["ActivityStatusId"].ToString());
                        myList5[i].IsForCustomer = dr["isForCustomer"].ToString();
                        myList5[i].AssignToId = int.Parse(dr["AssignToId"].ToString());
                        myList5[i].AssignToName = dr["AssignToName"].ToString();
                        myList5[i].isEditable = dr["isEditable"].ToString();
                        myList5[i].ReminderDateDefaultFormat = dr["ReminderDate"].ToString();
                        myList5[i].FileUpload = "../Uploads/" + dirname + "/" + dr["FileUpload"].ToString();
                        myList5[i].FileUploadOriginal = dr["FileUploadOriginal"].ToString();
                        myList5[i].FileSizeKb = dr["FileSizeKb"].ToString();
                        myList5[i].IsOCR = dr["IsOCR"].ToString();
                        //if (dr["ReminderDate"].ToString() != "" && dr["ReminderDate"].ToString() != null)
                        //    myList5[i].ReminderDate = Convert.ToDateTime(dr["ReminderDate"].ToString());
                        i = i + 1;
                    }
                }
                I.ActivityOtherDetails = myList5;

                //Total Records

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }

        }

        public List<Activity> GetUserNameAndPassword(IActivity I)
        {
            int i = 0;
            List<Activity> myList = new List<Activity>();
            Parameters.AddWithValue("@RequestId", I.RequestId);
            SqlDataReader dr = getExecuteReader("ClientGetUserNameAndPassword");
            try
            {

                while (dr.Read())
                {
                    myList.Add(new Activity());
                    myList[i].UserName = dr["UserName"].ToString();
                    myList[i].EmailID = dr["EmailID"].ToString();
                    myList[i].Password = dr["Password"].ToString();
                    myList[i].AddedOnDate = dr["AddedOnDate"].ToString();
                    myList[i].ContractID = int.Parse(dr["ContractID"].ToString());

                    i = i + 1;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return myList;
        }

        public void ReadDataCalendar(IActivity I)
        {
            int i = 0;
            List<Activity> myList1 = new List<Activity>();
            List<Activity> myList2 = new List<Activity>();
            List<Activity> myList4 = new List<Activity>();
            List<Activity> myList5 = new List<Activity>();
            Parameters.AddWithValue("@ActivityMonth", I.ActivityMonth);
            Parameters.AddWithValue("@ActivityYear", I.ActivityYear);
            Parameters.AddWithValue("@StartWeekDate", I.StartWeekDate);
            Parameters.AddWithValue("@EndWeekDate", I.EndWeekDate);
            Parameters.AddWithValue("@ActivityId", I.ActivityId);
            Parameters.AddWithValue("@UserId", I.AddedBy);
            SqlDataReader dr = getExecuteReader("CalendarActivityRead");
            try
            {

                //Parent Repeater
                while (dr.Read())
                {
                    myList1.Add(new Activity());
                    myList1[i].ActivityId = int.Parse(dr["ActivityId"].ToString());
                    myList1[i].ActivityTypeId = int.Parse(dr["ActivityTypeId"].ToString());
                    myList1[i].ActivityTypeName = dr["TypeName"].ToString();
                    myList1[i].ActivityText = dr["ActivityText"].ToString();
                    myList1[i].ActivityDate = Convert.ToDateTime(dr["ActivityDate"].ToString());
                    myList1[i].ActivityTime = dr["ActivityTime"].ToString();
                    //myList1[i].ActivityDateDefaultFormat = Convert.ToDateTime(dr["ActivityDateDefaultFormat"].ToString());
                    myList1[i].ActivityDateDefaultFormat = dr["ActivityDateDefaultFormat"].ToString();
                    myList1[i].AddedByName = dr["AddedBy"].ToString();
                    myList1[i].AddedOn = Convert.ToDateTime(dr["AddedOn"].ToString());
                    myList1[i].AddedTime = dr["AddedTime"].ToString();
                    myList1[i].OnlyDate = Convert.ToDateTime(dr["OnlyDate"].ToString());
                    myList1[i].ActivityStatusName = dr["StatusName"].ToString();
                    myList1[i].ActivityStatusId = int.Parse(dr["ActivityStatusId"].ToString());
                    myList1[i].IsForCustomer = dr["isForCustomer"].ToString();
                    myList1[i].AssignToId = int.Parse(dr["AssignToId"].ToString());
                    myList1[i].AssignToName = dr["AssignToName"].ToString();
                    myList1[i].Description = dr["Description"].ToString();
                    myList1[i].FileUpload = "../Uploads/" + dirname + "/" + dr["FileUpload"].ToString();
                    myList1[i].FileUploadOriginal = dr["FileUploadOriginal"].ToString();
                    myList1[i].FileSizeKb = dr["FileSizeKb"].ToString();
                    myList1[i].ReqID = Convert.ToInt32(dr["ReqId"].ToString());
                    myList1[i].IsOCR = dr["IsOCR"].ToString();

                    myList1[i].ReminderDateDefaultFormat = dr["ReminderDate"].ToString();
                    myList1[i].isEditable = dr["isEditable"].ToString();
                    myList1[i].ContractTypeId = Convert.ToString(dr["ContractTypeId"]);
                    myList1[i].ContractTypeIds = Convert.ToString(dr["ContractTypeIds"]);
                    //if (dr["ReminderDate"].ToString() != "" && dr["ReminderDate"].ToString() != null)
                    //    myList1[i].ReminderDate = Convert.ToDateTime(dr["ReminderDate"].ToString());

                    i = i + 1;
                }
                I.ActivityDates = myList1;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }

        }

        public List<Activity> GetActivityPending(IActivity I)
        {
            int i = 0;
            List<Activity> myList = new List<Activity>();
            Parameters.AddWithValue("@UserId", I.AddedBy);
            SqlDataReader dr = getExecuteReader("ActivityPending");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new Activity());
                    myList[i].ActivityTypeName = dr["ActivityTypeName"].ToString();
                    myList[i].ActivityText = dr["ActivityText"].ToString();
                    myList[i].ActivityId = int.Parse(dr["ActivityId"].ToString());
                    myList[i].ActivityDateString = dr["ActivityDateString"].ToString();
                    myList[i].AddedByName = dr["AddedByName"].ToString();
                    myList[i].ActivityStatusName = dr["ActivityStatusName"].ToString();
                    myList[i].IsForCustomer = dr["IsForCustomer"].ToString();

                    i = i + 1;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return myList;
        }

        public string ActivitySnoozeAndDismiss(IActivity I)
        {
            Parameters.AddWithValue("@ActivityId", I.ActivityId);
            Parameters.AddWithValue("@Snooze", I.Snooze);
            return getExcuteQuery("ActivitySnoozeAndDismiss");
        }

        public void ReadAgendaData(IActivity I)
        {

            int i = 0;
            List<Activity> myList1 = new List<Activity>();
            List<Activity> myList2 = new List<Activity>();
            List<Activity> myList4 = new List<Activity>();
            List<Activity> myList5 = new List<Activity>();
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            Parameters.AddWithValue("@OpenActivityType", I.OpenActivityType);
            Parameters.AddWithValue("@UserId", I.AddedBy);
            SqlDataReader dr = getExecuteReader("CalendarActivityAgendaRead");
            try
            {

                //Parent Repeater
                while (dr.Read())
                {
                    myList1.Add(new Activity());
                    myList1[i].ActivityId = int.Parse(dr["ActivityId"].ToString());
                    myList1[i].ActivityTypeId = int.Parse(dr["ActivityTypeId"].ToString());
                    myList1[i].ActivityTypeName = dr["TypeName"].ToString();
                    myList1[i].ActivityText = dr["ActivityText"].ToString();
                    myList1[i].ActivityDate = Convert.ToDateTime(dr["ActivityDate"].ToString());
                    myList1[i].ActivityTime = dr["ActivityTime"].ToString();
                    //myList1[i].ActivityDateDefaultFormat = Convert.ToDateTime(dr["ActivityDateDefaultFormat"].ToString());
                    myList1[i].ActivityDateDefaultFormat = dr["ActivityDateDefaultFormat"].ToString();
                    myList1[i].AddedByName = dr["AddedBy"].ToString();
                    myList1[i].AddedOn = Convert.ToDateTime(dr["AddedOn"].ToString());
                    myList1[i].AddedTime = dr["AddedTime"].ToString();
                    myList1[i].OnlyDate = Convert.ToDateTime(dr["OnlyDate"].ToString());
                    myList1[i].ActivityStatusName = dr["StatusName"].ToString();
                    myList1[i].ActivityStatusId = int.Parse(dr["ActivityStatusId"].ToString());
                    myList1[i].IsForCustomer = dr["isForCustomer"].ToString();
                    myList1[i].AssignToId = int.Parse(dr["AssignToId"].ToString());
                    myList1[i].AssignToName = dr["AssignToName"].ToString();
                    //myList1[i].Description = myList1[i].ActivityDateDefaultFormat.ToString("dd-MMM-yyyy hh:mm tt", CultureInfo.InvariantCulture);
                    myList1[i].Description = myList1[i].ActivityDateDefaultFormat.ToString();
                    myList1[i].ReqID = Convert.ToInt32(dr["ReqID"]);
                    myList1[i].ReminderDateDefaultFormat = dr["ReminderDate"].ToString();
                    myList1[i].isEditable = dr["isEditable"].ToString();
                    //if (dr["ReminderDate"].ToString() != "" && dr["ReminderDate"].ToString() != null)
                    //    myList1[i].ReminderDate = Convert.ToDateTime(dr["ReminderDate"].ToString());

                    i = i + 1;
                }
                I.ActivityDates = myList1;


                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }

        }

        public List<SelectControlFields> SelectContractIds(IActivity I)
        {
            SqlDataReader dr = getExecuteReader("SelectContractIds");
            return SelectControlFields.SelectData(dr);
        }

//        #region OutlookCalendarFile

//        private readonly static string FilePath = System.Web.HttpContext.Current.Server.MapPath(@"~/OutlookCalendar/" + System.Web.HttpContext.Current.Request.Url.Segments[1].Replace("/", ""));

//        private List<Activity> GetActivityDetails(int AssignedToId)
//        {
//            List<Activity> act=new List<Activity>();
//            Parameters.Clear();
//            Parameters.AddWithValue("@AssignedToId", AssignedToId);
//            Parameters.AddWithValue("@AssignedToId", AssignedToId);
//            SqlDataReader dr = getExecuteReader("ActivityDetailsRead");

//            int i = 0;
//            if (dr.HasRows)
//            {
//                while (dr.Read())
//                {
//                    act.Add(new Activity());
//                    act[i].ActivityId = int.Parse(dr["ActivityId"].ToString());
//                    act[i].ActivityTypeId = int.Parse(dr["ActivityTypeId"].ToString());
//                    act[i].ActivityTypeName = dr["TypeName"].ToString();
//                    act[i].ActivityText = dr["ActivityText"].ToString();
//                    act[i].ActivityDate = Convert.ToDateTime(dr["ActivityDate"].ToString());
//                    act[i].ActivityTime = dr["ActivityTime"].ToString();
//                    //act[i].ActivityDateDefaultFormat = Convert.ToDateTime(dr["ActivityDateDefaultFormat"].ToString());
//                    act[i].ActivityDateDefaultFormat = dr["ActivityDateDefaultFormat"].ToString();
//                    act[i].AddedByName = dr["AssignByName"].ToString();
//                    act[i].AddedOn = Convert.ToDateTime(dr["AddedOn"].ToString());
//                    act[i].AddedTime = dr["AddedTime"].ToString();
//                    act[i].OnlyDate = Convert.ToDateTime(dr["OnlyDate"].ToString());
//                    act[i].ActivityStatusName = dr["StatusName"].ToString();
//                    act[i].ActivityStatusId = int.Parse(dr["ActivityStatusId"].ToString());
//                    act[i].IsForCustomer = dr["isForCustomer"].ToString();
//                    act[i].AssignToId = int.Parse(dr["AssignToId"].ToString());
//                    act[i].AssignToName = dr["AssignToName"].ToString();
//                    act[i].isEditable = dr["isEditable"].ToString();
//                    act[i].ReminderDateDefaultFormat = dr["ReminderDate"].ToString();
//                    act[i].FileUpload = dr["FileUpload"].ToString();
//                    act[i].FileUploadOriginal = dr["FileUploadOriginal"].ToString();
//                    act[i].FileSizeKb = dr["FileSizeKb"].ToString();
//                    if (!string.IsNullOrEmpty(Convert.ToString(dr["RequestId"])))
//                    act[i].RequestId =Convert.ToInt32(dr["RequestId"].ToString());
//                    if (!string.IsNullOrEmpty(Convert.ToString(dr["ContractId"])))
//                    act[i].ContractID = Convert.ToInt32(dr["ContractId"].ToString());

//                    //if (dr["ReminderDate"].ToString() != "" && dr["ReminderDate"].ToString() != null)
//                    //    myList5[i].ReminderDate = Convert.ToDateTime(dr["ReminderDate"].ToString());
//                    i = i + 1;
//                }
//            }
//            return act;
//        }

//        private string UpdateUserLog(IActivity I)
//        {
//            Parameters.AddWithValue("@FileName", I.OutlookFileName);
//            Parameters.AddWithValue("@UsersId", I.AssignToId);
//            Parameters.AddWithValue("@ErrorIfAny", I.ErrorIfAny);

//            Parameters.AddWithValue("@Status", 0);
//            Parameters["@Status"].Direction = ParameterDirection.Output;
//            return getExcuteQuery("OutlookCalendarAddUpdate", "@Status");
//        }

//        private void UpdateOutlookFile(int AssignedToId) 
//        {
//            string EncryptedText = "", FileName = "", filePath = "", ErrorIfAny = "";
//            List<Activity> ActivityDetails = null;
//            try
//            {
//                //string OutlookCalendar = ConfigurationManager.AppSettings["OutlookCalendar"].ToUpper();
//                string IsOutlookEnable = IsOutlookCalendarEnable().ToUpper();
//                if (IsOutlookEnable.Equals("Y"))
//                {
//                    ActivityDetails = new List<Activity>();
//                    ActivityDetails = GetActivityDetails(AssignedToId);

//                    EncryptedText = SSTCryptographer.Encrypt(ActivityDetails[0].AssignToName + "_" + Convert.ToString(ActivityDetails[0].AssignToId), "Uberall*1");
//                    EncryptedText = EncryptedText.Replace("*", "").Replace("?", "").Replace("<", "").Replace(">", "").Replace("|", "").Replace(":", "").Replace("/", "").Replace("\\", "").Replace("\"", "").Replace("+", "1");
//                    FileName = EncryptedText + ".ics";
//                    filePath = FilePath + "/" + EncryptedText + ".ics";
//                    StreamWriter writer;
//                    FileInfo fileexist = new FileInfo(filePath);

//                    if (!Directory.Exists(FilePath))
//                    {
//                        Directory.CreateDirectory(FilePath);
//                    }

//                    /*if (fileexist.Exists)
//                    {
//                       writer = File.AppendText(filePath);
//                    }
//                    else
//                    {
//                        writer = File.CreateText(filePath);                       
//                    }*/

//                    writer = File.CreateText(filePath);
//                    writer.WriteLine("BEGIN:VCALENDAR");
//                    writer.WriteLine("PRODID:-//NewGalexy/ContractPod/v2");

//                    for (int i = 0; i < ActivityDetails.Count; i++)
//                    {
//                        DateTime dtmStartTime = ActivityDetails[i].ActivityDate;
//                        Nullable<DateTime> dtmEndTime = Convert.ToDateTime(ActivityDetails[i].ReminderDateDefaultFormat == "" ? null : ActivityDetails[i].ReminderDateDefaultFormat);
//                        Nullable<TimeSpan> ts = new TimeSpan();
//                        ts = (dtmStartTime - dtmEndTime);
                      
//                        string myLocation = "";

//                        writer.WriteLine("BEGIN:VEVENT");
//                        writer.WriteLine("UID:" + ActivityDetails[i].ActivityId.ToString());
//                        //writer.WriteLine("DTSTART:" + DateTime.Parse(ActivityDetails[i].ActivityDate.ToString()).ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z"));
//                        //writer.WriteLine("DTEND:" + DateTime.Parse(dtmEndTime.ToString()).ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z"));
//                        writer.WriteLine("DTSTART:" + ActivityDetails[i].ActivityDate.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z"));
//                        writer.WriteLine("DTEND:" + ActivityDetails[i].ActivityDate.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z"));
//                        writer.WriteLine("LOCATION:" + myLocation);

//                        string AddToCal = "", BinaryString = "";

//                        AddToCal += "Assigned By : " + ActivityDetails[i].AddedByName + "<br /><br />";
//                        AddToCal += "Request Id : " + ActivityDetails[i].RequestId + "<br /><br />";
//                        AddToCal += "Contract Id : " + ActivityDetails[i].ContractID + "<br /><br />";
//                        AddToCal += "Activity Type : " + ActivityDetails[i].ActivityTypeName + "<br /><br />";
//                        AddToCal += "Activity Description : " + ActivityDetails[i].ActivityText + "<br /><br />";
//                        AddToCal += "Activity Status : " + ActivityDetails[i].ActivityStatusName + "<br /><br />";

//                        /*if (!string.IsNullOrEmpty(ActivityDetails[i].FileUpload))
//                        {
//                            BinaryString = FileToBinary(System.Web.HttpContext.Current.Server.MapPath(@"" + ActivityDetails[i].FileUpload.Replace("..", "~")));
//                            BinaryString = "ATTACH;ENCODING=BASE64;VALUE=BINARY;X-FILENAME=" + ActivityDetails[i].FileUploadOriginal + ":" + BinaryString + "<br /><br />";
//                        }
//                        writer.WriteLine(BinaryString);*/
//                        writer.WriteLine("X-ALT-DESC;FMTTYPE=text/html:<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 3.2//EN'>\\n<HTML>\\n<HEAD>\\n<META NAME='Generator' CONTENT='MS Exchange Server version 08.01.0240.003'>\\n<TITLE></TITLE>\\n</HEAD>\\n<BODY>\\n\\n\\n<P DIR=LTR><SPAN LANG='en-us'></SPAN>" + AddToCal + "<SPAN LANG='en-us'></SPAN></A><SPAN LANG='en-us'></SPAN></P>\\n\\n</BODY></HTML>");
//                        //writer.WriteLine(BinaryString);
//                        writer.WriteLine("SUMMARY:" + ActivityDetails[i].ActivityTypeName);
//                        //writer.WriteLine("DECSCRIPTION:" + AddToCal.Replace("<br /><br />", "\n"));
//                        writer.WriteLine("DECSCRIPTION:" + ActivityDetails[i].ActivityTypeName);
//                        writer.WriteLine("PRIORITY:3");
//                        writer.WriteLine("CLASS:PUBLIC");
//                        if (ts != null)
//                        {
//                            writer.WriteLine("BEGIN:VALARM");
//                            writer.WriteLine("TRIGGER:-PT" + Math.Round(ts.GetValueOrDefault().TotalMinutes) + "M");
//                            writer.WriteLine("ACTION:DISPLAY");
//                            writer.WriteLine("DESCRIPTION:Reminder");
//                            writer.WriteLine("END:VALARM");
//                        }
//                        writer.WriteLine("END:VEVENT");                        
//                    }
//                    writer.WriteLine("END:VCALENDAR");
//                    writer.Flush();
//                    writer.Close();
//                }
//            }
//            catch (Exception ex)
//            {
//                ErrorIfAny = ex.Message;
//            }
//            Parameters.Clear();
//            Parameters.AddWithValue("@FileName", filePath);
//            Parameters.AddWithValue("@UsersId", ActivityDetails[0].AssignToId);
//            Parameters.AddWithValue("@ErrorIfAny", ErrorIfAny);

//            Parameters.AddWithValue("@Status", 0);
//            Parameters["@Status"].Direction = ParameterDirection.Output;
//            string ret = getExcuteQuery("OutlookCalendarAddUpdate", "@Status");
//        }

//        private static string IsOutlookCalendarEnable()
//        {
//            string dirname = System.Web.HttpContext.Current.Request.Url.Segments[1].Replace("/", "");
//            DomainActivity.GetDBInformation(dirname);
//            string IsEnable = DomainActivity.IsOutlookCalendarEnable;
//            return IsEnable;
//        }

///// <summary>
//    /// Function to get byte array from a file
//    /// </summary>
//    /// <param name="_FileName">File name to get byte array</param>
//    /// <returns>Byte Array</returns>
//        public string FileToBinary(string _FileName)
//        {
//            byte[] _Buffer = null;
//            try
//            {
//                // Open file for reading

//                System.IO.FileStream _FileStream = new System.IO.FileStream(_FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
//                // attach filestream to binary reader

//                System.IO.BinaryReader _BinaryReader = new System.IO.BinaryReader(_FileStream);
//                // get total byte length of the file
//                long _TotalBytes = new System.IO.FileInfo(_FileName).Length;

//                // read entire file into buffer
//                _Buffer = _BinaryReader.ReadBytes((Int32)_TotalBytes);
//                // close file reader
//                _FileStream.Close();
//                _FileStream.Dispose();
//                _BinaryReader.Close();
//            }
//            catch (Exception _Exception)
//            {
//                // Error
//                Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());
//            }
//            return Convert.ToBase64String(_Buffer, 0, _Buffer.Length);
//        }
//        #endregion

        public string ContractDetailsByRequestId(IActivity I)
        {
            /* Return a data reader content information related to Contract Request whose file is going to be indexed.
               Sequence of parameters are - > RequestId, ContractId, RequestDate, ContractTypeId, Customer_SupplierId, Requester_Assigner, Assign_User, Assigned_Dept, ContractValue, SignatureDate */
            string ContractCustomInfo = "";

            Parameters.AddWithValue("@RequestID", I.RequestId);
            SqlDataReader dr = getExecuteReader("ContractDetailsByRequestId");
            while (dr.Read())
            {
                string SignatureDate = "-";
                if (dr["SignatureDate"] != DBNull.Value)
                    SignatureDate = Convert.ToDateTime(dr["SignatureDate"]).ToShortDateString();
                ContractCustomInfo = dr["RequestId"].ToString() + "#" + dr["ContractId"].ToString() + "#" + Convert.ToDateTime(dr["requestDate"]).ToShortDateString() + "#" + dr["ContractTypeId"].ToString() + "#" + dr["Customer_SupplierId"].ToString() + "#" + dr["Requester_Assigner"].ToString() + "#" + dr["Assign_User"].ToString() + "#" + dr["Assigned_Dept"].ToString() + "#" + dr["ContractValue"].ToString() + "#" + SignatureDate;
            }
            return ContractCustomInfo;
        }

        //public string ReadAssineDetails(IActivity I)
        //{

        //    Parameters.AddWithValue("@RequestId", I.RequestId);//4

        //    return Convert.ToString(getExcuteScalar("SelectContractTypeId"));

        //}
    }
}