﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace WorkflowBLL
{
    public class OutlookCalendar :IOutlookCalendar
    {
        OutlookCalendarDAL outlookDAL;
        public string ActivityIds { get; set; }
        public string RequestIds { get; set; }
        public string ContractIDs { get; set; }
        public string OutlookFileName { get; set; } 
        public int AssignToId { get; set; }
        public string AssignToIds { get; set; }

        public char IsBlank { get; set; } 

        public string UpdateOutlookFile()
        {
            try
            {
              outlookDAL = new OutlookCalendarDAL();
              return outlookDAL.UpdateOutlookFile(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
