﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using CommonBLL;

namespace WorkflowBLL
{
    public class Client : IClient
    {
        ClientDAL obj;
       
        public int ClientID { get; set; }
        public string ClientName { get; set; }
        public string Address { get; set; }
        public int CountryID { get; set; }
        public string CountryName { get; set; }
        public int StateID { get; set; }
        public string StateName { get; set; }
        public int CityID { get; set; }
        public string CityName { get; set; }
        public string Pincode { get; set; }
        public int AddedBy { get; set; }
        public int ModifiedBy { get; set; }
        public string IpAddress { get; set; }
        //private int _AddedBy;

        //public int AddedBy
        //{
        //    get { return _AddedBy; }
        //    set
        //    {
        //        if (value == 0)
        //        {
        //            throw new CustomException("Session Out");
        //        }
        //        _AddedBy = value;
        //    }
        //}

        //private int _ModifiedBy;

        //public int ModifiedBy
        //{
        //    get { return _ModifiedBy; }
        //    set
        //    {
        //        if (value == 0)
        //        {
        //            throw new CustomException("Session Out");
        //        }
        //        _ModifiedBy = value;
        //    }
        //}

        //private string _IpAddress;

        //public string IpAddress
        //{
        //    get { return _IpAddress; }
        //    set
        //    {
        //        if (value.ToString() == "")
        //        {
        //            throw new CustomException("IP not captured");
        //        }
        //        _IpAddress = value;
        //    }
        //}
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Description { get; set; }
         
        public string InsertRecord()
        {
            try
            {
                obj = new ClientDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         
        public string DeleteRecord()
        {
            return "";
        }

        public string UpdateRecord()
        {
            return "";
        }

        public List<Client> ReadData()
        {
            try
            {
                obj = new ClientDAL();
                return obj.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields> SelectData()
        {
            try
            {
                obj = new ClientDAL();
                return obj.SelectData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public List<SelectControlFields> SelectClientData()
        {
            try
            {
                obj = new ClientDAL();
                return obj.SelectClientData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
