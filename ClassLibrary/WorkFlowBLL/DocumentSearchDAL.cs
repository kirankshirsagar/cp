﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;

namespace WorkflowBLL
{
    public class DocumentSearchDAL : DAL
    { 
        public List<DocumentSearch> ReadData(IDocumentSearch I)
        {
            int i = 0;
            List<DocumentSearch> myList = new List<DocumentSearch>();
            Parameters.AddWithValue("@Doctitle", I.Doctitle);
            Parameters.AddWithValue("@docfilePath", I.Docname);
            SqlDataReader dr = getExecuteReader("DocumentSearch");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new DocumentSearch());
                    myList[i].RequestId = int.Parse(dr["requestId"].ToString());
                    myList[i].Docname = dr["contractfileNm"].ToString();
                    myList[i].ContrctId = int.Parse(dr["ContractID"].ToString());
                    myList[i].Doctitle = dr["flName"].ToString();
                    myList[i].ContractType = dr["ContractTypeName"].ToString();
                    myList[i].RequestDate = dr["requestDate"].ToString();
                    myList[i].AssignToUserId = int.Parse(dr["AssignToUserID"].ToString());
                    myList[i].RequesterUserId = int.Parse(dr["RequesterUserId"].ToString());
                    i = i + 1;
                }
                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
        }

        public DataSet ReadFilterData(IDocumentSearch I)
        {
            Parameters.AddWithValue("@UserID", I.UserId);

            Parameters.AddWithValue("@RequestFromDate", I.RequestFromDate);
            Parameters.AddWithValue("@RequestToDate", I.RequestToDate);
            Parameters.AddWithValue("@SignatureFromDate", I.SignatureFromDate);
            Parameters.AddWithValue("@SignatureToDate", I.SignatureToDate);
            Parameters.AddWithValue("@EffectiveFromDate", I.EffectiveFromDate);
            Parameters.AddWithValue("@EffectiveToDate", I.EffectiveToDate);
            Parameters.AddWithValue("@ExpiryFromDate", I.ExpiryFromDate);
            Parameters.AddWithValue("@ExpiryToDate", I.ExpiryToDate);

            Parameters.AddWithValue("@ContractTypeID", I.ContractTypeID);
            Parameters.AddWithValue("@ClientID", I.ClientID);
            Parameters.AddWithValue("@ContractStatus", I.ContractStatus);
            Parameters.AddWithValue("@ContractID", I.ContractID);

            Parameters.AddWithValue("@AssignToId", I.AssignToId);
            Parameters.AddWithValue("@RequestUserID", I.RequestUserID);
            Parameters.AddWithValue("@DepartmentId", I.DepartmentId);
            Parameters.AddWithValue("@ContractValue", I.ContractValue);
            Parameters.AddWithValue("@SearchFor", I.SearchFor);

            DataSet ds = new DataSet();
            ds = getDataSet("ContractRequestDocSearch");

            return ds;
        }

        public List<DocumentSearch> MetaDataSearch(IDocumentSearch I)
        {
            int i = 0;
            List<DocumentSearch> myList = new List<DocumentSearch>();
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            Parameters.AddWithValue("@SearchText", I.Search);
            Parameters.AddWithValue("@SearchFor", I.SearchFor);

            Parameters.AddWithValue("@UserID", I.UserId);
            Parameters.AddWithValue("@RequestFromDate", I.RequestFromDate);
            Parameters.AddWithValue("@RequestToDate", I.RequestToDate);
            Parameters.AddWithValue("@SignatureFromDate", I.SignatureFromDate);
            Parameters.AddWithValue("@SignatureToDate", I.SignatureToDate);
            Parameters.AddWithValue("@EffectiveFromDate", I.EffectiveFromDate);
            Parameters.AddWithValue("@EffectiveToDate", I.EffectiveToDate);
            Parameters.AddWithValue("@ExpiryFromDate", I.ExpiryFromDate);
            Parameters.AddWithValue("@ExpiryToDate", I.ExpiryToDate);
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeID);
            Parameters.AddWithValue("@CustomerSupplierId", I.ClientID);
            Parameters.AddWithValue("@ContractStatus", I.ContractStatus);
            Parameters.AddWithValue("@ContractId", I.ContractID);            
            Parameters.AddWithValue("@DepartmentId", I.DepartmentId);
            Parameters.AddWithValue("@RequesterUserId", I.RequesterUserId);
            Parameters.AddWithValue("@AssignToUserId", I.AssignToUserId);
            Parameters.AddWithValue("@ContractValue", I.ContractValue);
            Parameters.AddWithValue("@GETALL", I.GetAll);

            SqlDataReader dr = getExecuteReader("MetaDataSearch");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new DocumentSearch());
                    myList[i].RequestId = int.Parse(dr["RequestId"].ToString());//1                    
                    if (dr["ContractID"].ToString() != "")
                    {
                        myList[i].ContractID = int.Parse(dr["ContractID"].ToString());//3
                    }
                    else
                    {
                        myList[i].ContractID = 0;//3
                    }
                    myList[i].RequestDate = dr["RequestDate"].ToString();
                    myList[i].ContractType = dr["ContractTypeName"].ToString();
                    myList[i].ContractStatus = dr["StatusName"].ToString();
                    myList[i].ClientName = dr["CustomerSupplier"].ToString();
                    myList[i].CustomerSupplierType = dr["CustomerSupplierType"].ToString();
                    myList[i].AssignedTo = dr["AssignedUser"].ToString();
                    myList[i].SignatureDate = dr["SignatureDate"].ToString();
                    myList[i].FoundIn = dr["FoundIn"].ToString();
                    I.TotalRecords = int.Parse(dr["TotalRecords"].ToString());
                    i = i + 1;
                }
                //if (dr.NextResult())
                //{
                //    while (dr.Read())
                //    {
                //        I.TotalRecords = int.Parse(dr[0].ToString());
                //    }
                //}
                //SqlDataReader dr = null;
                //System.Threading.Tasks.Task.Factory.StartNew(() => dr = getExecuteReader("MetaDataSearch"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
        }
    }
}
