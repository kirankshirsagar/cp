﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;
using System.Data.SqlClient;

namespace WorkflowBLL
{
    public interface IVault : ICrud, INavigation
    {
        // sk 30 may
         int RequestID { get; set; }
         string RequestNo { get; set; }
         int ContractID { get; set; }
         string ContractIDs { get; set; }
         int ContractTypeID { get; set; }
         string ContractTypeName { get; set; }
         string RequestType { get; set; }    // RequestTypeShortCode
         int RequestTypeID { get; set; }  
         string RequestTypeName { get; set; }
         int ClientID { get; set; }
         string ClientName { get; set; }
         string Address { get; set; }
         int CountryID { get; set; }
         string CountryName { get; set; }
         //int StateID { get; set; }
         string StateName { get; set; }
         //int CityID { get; set; }
         string CityName { get; set; }
         string Pincode { get; set; }
         int RequestUserID { get; set; }
         string RequesterUserName { get; set; }
         string ContractDescription { get; set; }  // RequestDescription
         string DeadlineDate { get; set; }
         int AssignToId { get; set; }
         string AssignTo { get; set; }
         string isApprovalRequried { get; set; }
         DateTime ContractExpiryDate { get; set; }
         string EstimatedValue { get; set; }
         string TerminationReason { get; set; }
         string DeletionReason { get; set; }
         string ContactNumber { get; set; }
         string EmailID { get; set; }
         string isNewAddress { get; set; }
         string isNewContactNumber { get; set; }
         string isNewEmailID { get; set; }

        int AddressID { get; set; }
        int ContactDetailID { get; set; }
        int EmailContactID { get; set; }

         int  DepartmentId { get; set; }
         
         int UserID { get; set; }
         string Description { get; set; }
         string AssignerUserName { get; set; }
         string IsUsed { get; set; }
         string isActive { get; set; }
         string DeadlineDateStr { get; set; }

         string DocumentIDs { get; set; }
         string RequestIDs { get; set; }
         string ContractStatus { get; set; }
         string IsBulkImport { get; set; }
         string Status { get; set; }



        //********** added for question answer details on Vaultflow page ***********
         string ModifiedByUserName { get; set; }
         string ModifiedOnFormattedDate { get; set; }
         string Question { get; set; }
         string Answer { get; set; }
         string TemplateName { get; set; }
         int ContractRequestId { get; set; }
         int ContractQAId { get; set; }
         int ContractId { get; set; }
         int ContractTemplateId { get; set; }
        //***************************************************************************
       
        List<SelectControlFields> SelectData();
        List<Vault> ReadData();
        List<SelectControlFields> SelectOtherData();
        List<SelectControlFields> SelectDeptData();
        List<Vault> ReadClientData();
        List<Vault> ContractQuestionairDetailsForVaultFlow();
   
        int ValidateContract();
      
        DataSet ReadContractDetails();

        List<SelectControlFields> ReadClientWithContracts(); 

    }
}
