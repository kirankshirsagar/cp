﻿using System;
using System.Collections.Generic;
using System.Data;
using CommonBLL;
using CrudBLL;

namespace WorkflowBLL
{
    public interface IContractRequest2 : IContractRequest
    {       
        string CacheString { get; set; }
        string BuildCacheString();
    }
}
