﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;

namespace WorkflowBLL
{
    public class AvailbleClientDAL : DAL
    {
        public string InsertRecord(IAvailableClient I)
        {
            Parameters.AddWithValue("@ClientId", I.ClientID);
            Parameters.AddWithValue("@ClientName", I.ClientName);//9
            Parameters.AddWithValue("@isCustomer", I.isCustomer);//9
            Parameters.AddWithValue("@Address", I.Address);//10
            Parameters.AddWithValue("@CityName", I.CityName);//11
            Parameters.AddWithValue("@StateName", I.StateName);//12
            Parameters.AddWithValue("@ContactNo", I.ContactNo);//12
            Parameters.AddWithValue("@CountryID", I.CountryID);//13
            Parameters.AddWithValue("@EmailID", I.EmailID);//13
            Parameters.AddWithValue("@Pincode", I.Pincode);//17
            Parameters.AddWithValue("@AddedBy", I.AddedBy);//18
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);//19
            Parameters.AddWithValue("@IpAddress", I.IpAddress);//20
            Parameters.AddWithValue("@Param", I.Param);
            Parameters.AddWithValue("@Street", I.Street);//10
          
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("AvailableClientsAddUpdateNew", "@Status");

        }

        public string UpdateRecord(IAvailableClient I)
        {
            Parameters.AddWithValue("@ClientId", I.ClientID);
            Parameters.AddWithValue("@ClientName", I.ClientName);//9
            Parameters.AddWithValue("@isCustomer", I.isCustomer);//9
            Parameters.AddWithValue("@Address", I.Address);//10
            Parameters.AddWithValue("@CityName", I.CityName);//11
            Parameters.AddWithValue("@StateName", I.StateName);//12
            Parameters.AddWithValue("@ContactNo", I.ContactNo);//12
            Parameters.AddWithValue("@CountryID", I.CountryID);//13
            Parameters.AddWithValue("@EmailID", I.EmailID);//13
            Parameters.AddWithValue("@Pincode", I.Pincode);//17
            Parameters.AddWithValue("@AddedBy", I.AddedBy);//18
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);//19
            Parameters.AddWithValue("@IpAddress", I.IpAddress);//20
            Parameters.AddWithValue("@Param", I.Param);
            Parameters.AddWithValue("@Street", I.Street);//10
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;

            return getExcuteQuery("AvailableClientsAddUpdateNew", "@Status");
        }

        public List<AvailableClient> ReadData(IAvailableClient I)
        {
            int i = 0;
            List<AvailableClient> myList = new List<AvailableClient>();
            Parameters.AddWithValue("@ClientId", I.ClientID);
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            SqlDataReader dr = getExecuteReader("AvailableClientsRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new AvailableClient());
                    myList[i].ClientID = int.Parse(dr["ClientID"].ToString());//6
                    myList[i].ClientName = dr["ClientName"].ToString();//7
                    myList[i].Address = dr["Address"].ToString();//8
                    myList[i].CountryID = int.Parse(dr["CountryID"].ToString());//9
                    myList[i].CountryName = dr["CountryName"].ToString();//10
                    //myList[i].StateID = int.dr["StateID"].ToString());//11
                    myList[i].StateName = dr["StateName"].ToString();//12
                 //   myList[i].CityID = int.Parse(dr["CityID"].ToString());//13
                    myList[i].CityName = dr["CityName"].ToString();//14
                    myList[i].Pincode = dr["Pincode"].ToString();//15
                    myList[i].ContactNo = dr["ContactNumber"].ToString();//14
                    myList[i].EmailID = dr["EmailID"].ToString();//15
                    myList[i].IsUsed = dr["IsUsed"].ToString();//15
                    myList[i].isCustomer = dr["isCustomer"].ToString();//15

                    i = i + 1;
                }
                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;

        }

        public void ReadEditData(IAvailableClient I)
        {
            int i = 0;
            List<AvailableClient> myList1 = new List<AvailableClient>();
            List<AvailableClient> myList2 = new List<AvailableClient>();
            List<AvailableClient> myList3 = new List<AvailableClient>();
            List<AvailableClient> myList4 = new List<AvailableClient>();

            Parameters.AddWithValue("@ClientId", I.ClientID);
            Parameters.AddWithValue("@Status", 0);
            Parameters.AddWithValue("@Param", I.Param);
            SqlDataReader dr = getExecuteReader("AvailableClientsAddUpdateNew");
            try
            {
                while (dr.Read())
                {
                    myList1.Add(new AvailableClient());
                    myList1[i].ClientName = dr["ClientName"].ToString();
                    myList1[i].isCustomer = dr["isCustomer"].ToString();
                    myList1[i].ClientID = int.Parse(dr["ClientId"].ToString());
                    i = i + 1;
                }
                I.ReadClient = myList1;

                i = 0;

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        myList2.Add(new AvailableClient());
                        myList2[i].Address = dr["Address"].ToString();
                        myList2[i].Street = dr["Street2"].ToString();
                        myList2[i].CountryName = dr["Country"].ToString();
                        myList2[i].StateName = dr["State"].ToString();
                        myList2[i].CityName = dr["City"].ToString();
                        myList2[i].Pincode = dr["PinCode"].ToString();
                        myList2[i].CountryID = int.Parse(dr["CountryID"].ToString());
                        myList2[i].IsUsed = dr["IsUsed"].ToString();
                        i = i + 1;
                    }
                }
                I.ReadAddress = myList2;

                i = 0;

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        myList3.Add(new AvailableClient());
                        myList3[i].ContactNo = dr["Mobile"].ToString();
                        myList3[i].IsUsed = dr["IsUsed"].ToString();
                        i = i + 1;
                    }
                }
                I.ReadContact = myList3;

                i = 0;

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        myList4.Add(new AvailableClient());
                        myList4[i].EmailID = dr["EmailID"].ToString();
                        myList4[i].IsUsed = dr["IsUsed"].ToString();
                        i = i + 1;
                    }
                }
                I.ReadEmailID = myList4;

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }

        }

        public string DeleteRecord(IAvailableClient I)
        {
            Parameters.AddWithValue("@ClientIds", I.ClientIDs);
            return getExcuteQuery("AvailableClientDelete");
        }
        
        public List<SelectControlFields> SelectData(IAvailableClient I, int withOutSelect = 0)
        {
            if (withOutSelect == 0)
            {
                SqlDataReader dr = getExecuteReader("AvailableClientsSelect");
                return SelectControlFields.SelectData(dr);
            }
            else
            {
                SqlDataReader dr = getExecuteReader("AvailableClientsSelect");
                return SelectControlFields.SelectDataNoSelect(dr);
            }

        }

    }
}
