﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;

namespace WorkflowBLL
{
    public interface IClient : ICrud
    {
        
         int ClientID { get; set; }
         string ClientName { get; set; }
         string Address { get; set; }
         int CountryID { get; set; }
         string CountryName { get; set; }
         int StateID { get; set; }
         string StateName { get; set; }
         int CityID { get; set; }
         string CityName { get; set; }
         string Pincode { get; set; }

        //string ChangeIsActive();
        List<SelectControlFields> SelectData();

        List<SelectControlFields> SelectClientData();
    }
}
