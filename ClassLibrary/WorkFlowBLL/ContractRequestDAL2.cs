﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using CommonBLL;
using System.Configuration;

namespace WorkflowBLL
{
    public class ContractRequestDAL2 : ContractRequestDAL
    {
        public string BuildCacheString(IContractRequest2 I)
        {
            Parameters.AddWithValue("@RequestId", I.RequestID);
            string RetValue = getExcuteScalar("ContractRequestBuildCacheString");
            return RetValue;
        }       
    }
}
