﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;
using System.Data.SqlClient;

namespace WorkflowBLL
{
    public interface ITrackDocumentChanges : ICrud, INavigation
    {
        int RequestID { get; set; }

        int TrackChangesClauseID { get; set; }
        int TrackChangesFieldID { get; set; }

        int TrackChangesID { get; set; }
        int LineNo { get; set; }
        string OriginalLineText { get; set; }
        string NewFileValueToEdit { get; set; }

        int FieldLibraryID { get; set; }
        int ContractFileOldId { get; set; }
        int ContractFileNewId { get; set; }
        int ClauseID { get; set; }


        string OldFileValue { get; set; }
        string NewFileValue { get; set; }
        string ModificationStatus { get; set; }

        string Action { get; set; }
        string StatusFlag { get; set; }
        string FieldName{get;set;}
        string ClauseName { get; set; }
        string BookmarkName { get; set; }
       
        int Status { get; set; }
        string Search { get; set; }

        DataTable dtTrackFieldChanges { get; set; }
        DataTable dtTrackClauseChanges { get; set; }
        DataTable dtTrackLineChanges { get; set; }

        DataTable DefineTrackChangesTable();
        DataTable GetContractFileDetails();
        DataSet GetFieldLibraryAndClausesDetails();

        Boolean IsTrackedChangesAlreadySaved();

        List<TrackDocumentChanges> GetTrackChangesTermsDetails();
        DataTable GetTrackChangesTermsSubDetails();

        List<TrackDocumentChanges> GetTrackChangesClauseDetails();
        DataTable GetTrackChangesClauseSubDetails();

        List<TrackDocumentChanges> GetTrackChangesLineDetails();
        DataTable GetTrackChangesLineSubDetails();
    }
}
