﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;
using System.Data.SqlClient;

namespace WorkflowBLL
{
    public interface IContractRelations : ICrud, INavigation
    {        
        int ContractRelationsId { get; set; }
        int ParentOfContractId { get; set; }
        int ChildOfContractId { get; set; }
        string ContractRelationsIds { get; set; }
        string RelationsDescription { get; set; }
        string For { get; set; }
        int Flag { get; set; }
        string IsUsed { get; set; }

        int ParentContractId { get; set; }
        int ChildContractId { get; set; }
        string PIDClient { get; set; }
        string CIDClient { get; set; }
        string PIDClientHref { get; set; }
        string CIDClientHref { get; set; }
        int ChildrenDropLevel { get; set; }
        string HtmlClass { get; set; }
        string URL { get; set; }
        bool IsParentOfDisabled { get; set; }
        bool IsChildOfDisabled { get; set; }

        DateTime ContractRelationsDate { get; set; }   
        string AddedOnDate { get; set; }
        string AddedByName { get; set; }
        int RequestId { get; set; }        
        int ContractID { get; set; }
        string ContractTypeId { get; set; }

        int AddedBy { get; set; }
        int ModifiedBy { get; set; }
        DateTime AddedOn { get; set; }
        DateTime ModifiedOn { get; set; }
        string IpAddress { get; set; }
        long TotalRecords { get; set; }
        int PageNo { get; set; }
        int RecordsPerPage { get; set; }
        string Search { get; set; }

        List<SelectControlFields> ContractRelationsAll();
        List<SelectControlFields> ParentOf();
        List<SelectControlFields> ChildOf();

        List<ContractRelations> ReadData();
        List<ContractRelations> ReadDataForGraph();
    }       
}
