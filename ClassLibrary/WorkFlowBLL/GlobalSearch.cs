﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkflowBLL
{
    public class GlobalSearch : IGlobalSearch
    {

       GlobalSearchDAL obj; 
       public string StringToFind { get; set; }
       public string ClientName { get; set; }
       public string ContractType { get; set; }
       public string AssignedTo { get; set; }
       public string RequestDate { get; set; }
       public string Link { get; set; }
       public int UserId { get; set; }
       public long TotalRecords { get; set; }
       public int PageNo { get; set; }
       public int RecordsPerPage { get; set; }
       public int Direction { get; set; }
       public string SortColumn { get; set; }
       public string Search { get; set; }
       public int RequestId { get; set; }

       public List<GlobalSearch> ReadData()
       {
           obj = new GlobalSearchDAL();
           return obj.ReadData(this);
       }
    }
}
