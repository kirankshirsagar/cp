﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace WorkflowBLL
{
    public class DocumentSearch : IDocumentSearch
    {

        DocumentSearchDAL obj;
        public string ClientName { get; set; }
        public string ContractType { get; set; }
        public string AssignedTo { get; set; }
        public string RequestDate { get; set; }
        public string Link { get; set; }
        public int UserId { get; set; }
        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public int Direction { get; set; }
        public string SortColumn { get; set; }
        public string Search { get; set; }
        public int RequestId { get; set; }
        public int ContrctId { get; set; }
        public string Docname { get; set; }
        public string Doctitle { get; set; }

        public int AssignToUserId { get; set; }
        public int RequesterUserId { get; set; }
        public string SearchFor { get; set; }


        public int ContractTypeID { get; set; }
        public int ClientID { get; set; }
        public string ContractStatus { get; set; }
        public int ContractID { get; set; }
        public int AssignToId { get; set; }
        public int RequestUserID { get; set; }
        public int DepartmentId { get; set; }
        public decimal ContractValue { get; set; }

        public DateTime? RequestFromDate { get; set; }
        public DateTime? RequestToDate { get; set; }
        public DateTime? SignatureFromDate { get; set; }
        public DateTime? SignatureToDate { get; set; }
        public DateTime? EffectiveFromDate { get; set; }
        public DateTime? EffectiveToDate { get; set; }
        public DateTime? ExpiryFromDate { get; set; }
        public DateTime? ExpiryToDate { get; set; }
        public string CustomerSupplierType { get; set; }

        public string SignatureDate { get; set; }
        public string ActivityFileUpload { get; set; }
        public string FoundIn { get; set; }
        public string GetAll { get; set; }

        public List<DocumentSearch> ReadData()
        {
            obj = new DocumentSearchDAL();
            return obj.ReadData(this);
        }

        public DataSet ReadFilterData()
        {
            try
            {
                obj = new DocumentSearchDAL();
                return obj.ReadFilterData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<DocumentSearch> MetaDataSearch()
        {
            obj = new DocumentSearchDAL();
            return obj.MetaDataSearch(this);
        }
    }
}
