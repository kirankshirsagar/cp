﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;

using System.Globalization;
using System.IO;

namespace WorkflowBLL
{
    public class KeyFieldsDAL : DAL
    {
        IOutlookCalendar OC = new OutlookCalendar();

        public string InsertRecord(IKeyFields I)
        {
            Parameters.AddWithValue("@RequestId", I.RequestId);

            if (I.Param == 1)
            {
                Parameters.AddWithValue("@EffectiveDate", I.EffectiveDate);
                Parameters.AddWithValue("@ExpirationDate", I.ExpirationDate);
                Parameters.AddWithValue("@ExpirationAlert30days", I.ExpirationAlert30days);
                Parameters.AddWithValue("@ExpirationAlert7days", I.ExpirationAlert7days);
                Parameters.AddWithValue("@ExpirationAlert24Hrs", I.ExpirationAlert24Hrs);

                Parameters.AddWithValue("@ExpirationAlert180days", I.ExpirationAlert180days);//js
                Parameters.AddWithValue("@ExpirationAlert90days", I.ExpirationAlert90days);//js
                Parameters.AddWithValue("@ExpirationAlert60days", I.ExpirationAlert60days);//js

                Parameters.AddWithValue("@RenewalAlert180days", I.RenewalAlert180days);//js
                Parameters.AddWithValue("@RenewalAlert90days", I.RenewalAlert90days);//js
                Parameters.AddWithValue("@RenewalAlert60days", I.RenewalAlert60days);//js

                Parameters.AddWithValue("@RenewalAlert24Hrs", I.RenewalAlert24Hrs);
                Parameters.AddWithValue("@RenewalAlert30days", I.RenewalAlert30days);
                Parameters.AddWithValue("@RenewalAlert7days", I.RenewalAlert7days);
                Parameters.AddWithValue("@RenewalDate", I.RenewalDate);
            }

            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@IsManualEntry", I.IsManualEntry);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@MetaDataFieldId", I.MetaDataFieldId);
            Parameters.AddWithValue("@LiabilityCap", I.LiabilityCap);
            Parameters.AddWithValue("@Indemnity", I.Indemnity);
            Parameters.AddWithValue("@IsChangeofControl", I.IsChangeofControl);
            Parameters.AddWithValue("@IsAgreementClause", I.IsAgreementClause);
            Parameters.AddWithValue("@EntireAgreementClause", I.EntireAgreementClause);
            Parameters.AddWithValue("@IsStrictliability", I.IsStrictliability);
            Parameters.AddWithValue("@Strictliability", I.Strictliability);
            Parameters.AddWithValue("@IsGuaranteeRequired", I.IsGuaranteeRequired);
            Parameters.AddWithValue("@Insurance", I.Insurance);
            Parameters.AddWithValue("@IsTerminationConvenience", I.IsTerminationConvenience);
            Parameters.AddWithValue("@IsTerminationmaterial", I.IsTerminationmaterial);
            Parameters.AddWithValue("@IsTerminationinsolvency", I.IsTerminationinsolvency);
            Parameters.AddWithValue("@IsTerminationinChangeofcontrol", I.IsTerminationinChangeofcontrol);
            Parameters.AddWithValue("@IsTerminationothercauses", I.IsTerminationothercauses);
            Parameters.AddWithValue("@TerminationDescription", I.TerminationDescription);
            Parameters.AddWithValue("@AssignmentNovation", I.AssignmentNovation);

            Parameters.AddWithValue("@Others", I.Others);

            Parameters.AddWithValue("@IsIndirectConsequentialLosses", I.IsIndirectConsequentialLosses);
            Parameters.AddWithValue("@IndirectConsequentialLosses", I.IndirectConsequentialLosses);
            Parameters.AddWithValue("@IsIndemnity", I.IsIndemnity);
            Parameters.AddWithValue("@IsWarranties", I.IsWarranties);
            Parameters.AddWithValue("@IsForceMajeure", I.IsForceMajeure);
            Parameters.AddWithValue("@ForceMajeure", I.ForceMajeure);
            Parameters.AddWithValue("@IsSetOffRights", I.IsSetOffRights);
            Parameters.AddWithValue("@SetOffRights", I.SetOffRights);
            Parameters.AddWithValue("@IsGoverningLaw", I.IsGoverningLaw);
            Parameters.AddWithValue("@GoverningLaw", I.GoverningLaw);

            Parameters.AddWithValue("@Param", I.Param);

            Parameters.AddWithValue("@ExpirationAlert180daysUserID1", I.ExpirationAlert180daysUserID1);//js
            Parameters.AddWithValue("@ExpirationAlert180daysUserID2", I.ExpirationAlert180daysUserID2);//js
            Parameters.AddWithValue("@ExpirationAlert90daysUserID1", I.ExpirationAlert90daysUserID1);//js
            Parameters.AddWithValue("@ExpirationAlert90daysUserID2", I.ExpirationAlert90daysUserID2);//js
            Parameters.AddWithValue("@ExpirationAlert60daysUserID1", I.ExpirationAlert60daysUserID1);//js
            Parameters.AddWithValue("@ExpirationAlert60daysUserID2", I.ExpirationAlert60daysUserID2);//js

            Parameters.AddWithValue("@ExpirationAlert30daysUserID1", I.ExpirationAlert30daysUserID1);
            Parameters.AddWithValue("@ExpirationAlert30daysUserID2", I.ExpirationAlert30daysUserID2);
            Parameters.AddWithValue("@ExpirationAlert7daysUserID1", I.ExpirationAlert7daysUserID1);
            Parameters.AddWithValue("@ExpirationAlert7daysUserID2", I.ExpirationAlert7daysUserID2);
            Parameters.AddWithValue("@ExpirationAlert24HrsUserID1", I.ExpirationAlert24HrsUserID1);
            Parameters.AddWithValue("@ExpirationAlert24HrsUserID2", I.ExpirationAlert24HrsUserID2);

            Parameters.AddWithValue("@RenewalAlert180daysUserID1", I.RenewalAlert180daysUserID1);
            Parameters.AddWithValue("@RenewalAlert180daysUserID2", I.RenewalAlert180daysUserID2);
            Parameters.AddWithValue("@RenewalAlert90daysUserID1", I.RenewalAlert90daysUserID1);
            Parameters.AddWithValue("@RenewalAlert90daysUserID2", I.RenewalAlert90daysUserID2);
            Parameters.AddWithValue("@RenewalAlert60daysUserID1", I.RenewalAlert60daysUserID1);
            Parameters.AddWithValue("@RenewalAlert60daysUserID2", I.RenewalAlert60daysUserID2);

            Parameters.AddWithValue("@RenewalAlert30daysUserID1", I.RenewalAlert30daysUserID1);
            Parameters.AddWithValue("@RenewalAlert30daysUserID2", I.RenewalAlert30daysUserID2);
            Parameters.AddWithValue("@RenewalAlert7daysUserID1", I.RenewalAlert7daysUserID1);
            Parameters.AddWithValue("@RenewalAlert7daysUserID2", I.RenewalAlert7daysUserID2);
            Parameters.AddWithValue("@RenewalAlert24HrsUserID1", I.RenewalAlert24HrsUserID1);
            Parameters.AddWithValue("@RenewalAlert24HrsUserID2", I.RenewalAlert24HrsUserID2);

            if (I.Param == 3)
            {

                Parameters.AddWithValue("@ActivityTypeId", I.ActivityTypeId);
                Parameters.AddWithValue("@ActivityStatusId", I.ActivityStatusId);
                Parameters.AddWithValue("@ActivityText", I.ActivityText);
                Parameters.AddWithValue("@ActivityDate", I.ActivityDate);
                Parameters.AddWithValue("@IsForCustomer", I.IsForCustomer);
                Parameters.AddWithValue("@ReminderDate", I.ReminderDate);
                //Parameters.AddWithValue("@AddedBy", I.AddedBy);
                //Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
                //Parameters.AddWithValue("@IpAddress", I.IpAddress);
                Parameters.AddWithValue("@AssignToId", I.AssignToId);
                Parameters.AddWithValue("@IsFromCalendar", I.IsFromCalendar);
                Parameters.AddWithValue("@IsFromOther", I.IsFromOther);
            }

            if (I.Param == 5)
            {
                Parameters.AddWithValue("@ContractIDs", I.ContractID);
                Parameters.AddWithValue("@ReminderDays", I.ReminderDays);
                Parameters.AddWithValue("@ReminderUser1", I.ReminderUser1);
                Parameters.AddWithValue("@ReminderUser2", I.ReminderUser2);
            }
            if (I.dtMatadataFieldValues != null)
            {
                Parameters.AddWithValue("@MataDataTypeValue", I.dtMatadataFieldValues); // js 24-12-2015
            }
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;

            string status = getExcuteQuery("KeyFieldsAddUpdate", "@Status");
            if (I.Param == 3)
            {
                OC.RequestIds = I.RequestId.ToString();
                OC.UpdateOutlookFile();
            }
            return status;
        }

        public string UpdateRecord(IKeyFields I)
        {
            Parameters.AddWithValue("@RequestId", I.RequestId);

            Parameters.AddWithValue("@EffectiveDate", I.EffectiveDate);
            Parameters.AddWithValue("@ExpirationDate", I.ExpirationDate);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("KeyFieldsAddUpdate", "@Status");

        }

        public string DeleteRecord(IKeyFields I)
        {
            Parameters.AddWithValue("@RequestId", I.RequestId);
            return getExcuteQuery("ActivityDelete");
        }

        public List<SelectControlFields> SelectData(IKeyFields I)
        {
            SqlDataReader dr = getExecuteReader("ContractRequestSelect");
            return SelectControlFields.SelectData(dr);
        }

        public List<KeyFeilds> ReadData(IKeyFields I)
        {
            int i = 0;

            DataTable tableEX = new DataTable();
            tableEX.Columns.Add("ExAlerts", typeof(string));
            tableEX.Columns.Add("ExAlertsDay", typeof(string));
            tableEX.Columns.Add("ExUser1", typeof(string));
            tableEX.Columns.Add("ExUser2", typeof(string));
            tableEX.Columns.Add("ExUserName1", typeof(string));
            tableEX.Columns.Add("ExUserName2", typeof(string));

            DataTable tableRen = new DataTable();
            tableRen.Columns.Add("RenAlerts", typeof(string));
            tableRen.Columns.Add("RenAlertsDay", typeof(string));
            tableRen.Columns.Add("RenUser1", typeof(string));
            tableRen.Columns.Add("RenUser2", typeof(string));
            tableRen.Columns.Add("RenUserName1", typeof(string));
            tableRen.Columns.Add("RenUserName2", typeof(string));

            List<KeyFeilds> myList = new List<KeyFeilds>();

            Parameters.AddWithValue("@RequestId", I.RequestId);
            SqlDataReader dr = getExecuteReader("KeyFieldRead");
            try
            {
                i = 0;

                while (dr.Read())
                {
                    myList.Add(new KeyFeilds());
                    myList[i].EffectiveDatestring = dr["EffectiveDate"].ToString();
                    myList[i].ExpirationDatestring = dr["ExpirationDate"].ToString();

                    myList[i].ExpirationAlert180days = dr["ExpirationAlert180days"].ToString();//js
                    myList[i].ExpirationAlert90days = dr["ExpirationAlert90days"].ToString();//js
                    myList[i].ExpirationAlert60days = dr["ExpirationAlert60days"].ToString();//js
                    myList[i].RenewalAlert180days = dr["RenewalAlert180days"].ToString();//js
                    myList[i].RenewalAlert90days = dr["RenewalAlert90days"].ToString();//js
                    myList[i].RenewalAlert60days = dr["RenewalAlert60days"].ToString();//js

                    myList[i].ExpirationAlert30days = dr["ExpirationAlert30days"].ToString();
                    myList[i].ExpirationAlert7days = dr["ExpirationAlert7days"].ToString();
                    myList[i].ExpirationAlert24Hrs = dr["ExpirationAlert24Hrs"].ToString();
                    myList[i].RenewalAlert30days = dr["RenewalAlert30days"].ToString();
                    myList[i].RenewalAlert7days = dr["RenewalAlert7days"].ToString();
                    myList[i].RenewalAlert24Hrs = dr["RenewalAlert24Hrs"].ToString();
                    myList[i].RenewalDatestring = dr["RenewalDate"].ToString();

                    myList[i].LiabilityCap = dr["LiabilityCap"].ToString();
                    myList[i].Indemnity = dr["Indemnity"].ToString();
                    myList[i].IsChangeofControl = dr["IsChangeofControl"].ToString();
                    myList[i].IsAgreementClause = dr["IsAgreementClause"].ToString();
                    myList[i].IsStrictliability = dr["IsStrictliability"].ToString();
                    myList[i].Strictliability = dr["Strictliability"].ToString();
                    myList[i].IsGuaranteeRequired = dr["IsGuaranteeRequired"].ToString();
                    myList[i].Insurance = dr["Insurance"].ToString();
                    myList[i].IsTerminationConvenience = dr["IsTerminationConvenience"].ToString();
                    myList[i].IsTerminationmaterial = dr["IsTerminationmaterial"].ToString();
                    myList[i].IsTerminationinsolvency = dr["IsTerminationinsolvency"].ToString();
                    myList[i].IsTerminationinChangeofcontrol = dr["IsTerminationinChangeofcontrol"].ToString();
                    myList[i].IsTerminationothercauses = dr["IsTerminationothercauses"].ToString();
                    myList[i].AssignmentNovation = dr["AssignmentNovation"].ToString();

                    myList[i].Others = dr["Others"].ToString();

                    myList[i].TerminationDescription = dr["TerminationDescription"].ToString();


                    //userID
                    myList[i].ExpirationAlert180daysUserID1 = Convert.ToInt32(dr["ExpirationAlert180daysUserID1"]); //js
                    myList[i].ExpirationAlert180daysUserID2 = Convert.ToInt32(dr["ExpirationAlert180daysUserID2"]); //js
                    myList[i].ExpirationAlert90daysUserID1 = Convert.ToInt32(dr["ExpirationAlert90daysUserID1"]);   //js
                    myList[i].ExpirationAlert90daysUserID2 = Convert.ToInt32(dr["ExpirationAlert90daysUserID2"]);   //js
                    myList[i].ExpirationAlert60daysUserID1 = Convert.ToInt32(dr["ExpirationAlert60daysUserID1"]);   //js
                    myList[i].ExpirationAlert60daysUserID2 = Convert.ToInt32(dr["ExpirationAlert60daysUserID2"]);   //js

                    myList[i].ExpirationAlert30daysUserID1 = Convert.ToInt32(dr["ExpirationAlert30daysUserID1"]);
                    myList[i].ExpirationAlert30daysUserID2 = Convert.ToInt32(dr["ExpirationAlert30daysUserID2"]);
                    myList[i].ExpirationAlert7daysUserID1 = Convert.ToInt32(dr["ExpirationAlert7daysUserID1"]);
                    myList[i].ExpirationAlert7daysUserID2 = Convert.ToInt32(dr["ExpirationAlert7daysUserID2"]);
                    myList[i].ExpirationAlert24HrsUserID1 = Convert.ToInt32(dr["ExpirationAlert24HrsUserID1"]);
                    myList[i].ExpirationAlert24HrsUserID2 = Convert.ToInt32(dr["ExpirationAlert24HrsUserID2"]);

                    myList[i].RenewalAlert180daysUserID1 = Convert.ToInt32(dr["RenewalAlert180daysUserID1"]); //js
                    myList[i].RenewalAlert180daysUserID2 = Convert.ToInt32(dr["RenewalAlert180daysUserID2"]);//js
                    myList[i].RenewalAlert90daysUserID1 = Convert.ToInt32(dr["RenewalAlert90daysUserID1"]);//js
                    myList[i].RenewalAlert90daysUserID2 = Convert.ToInt32(dr["RenewalAlert90daysUserID2"]);//js
                    myList[i].RenewalAlert60daysUserID1 = Convert.ToInt32(dr["RenewalAlert60daysUserID1"]);//js
                    myList[i].RenewalAlert60daysUserID2 = Convert.ToInt32(dr["RenewalAlert60daysUserID2"]);//js

                    myList[i].RenewalAlert30daysUserID1 = Convert.ToInt32(dr["RenewalAlert30daysUserID1"]);
                    myList[i].RenewalAlert30daysUserID2 = Convert.ToInt32(dr["RenewalAlert30daysUserID2"]);
                    myList[i].RenewalAlert7daysUserID1 = Convert.ToInt32(dr["RenewalAlert7daysUserID1"]);
                    myList[i].RenewalAlert7daysUserID2 = Convert.ToInt32(dr["RenewalAlert7daysUserID2"]);
                    myList[i].RenewalAlert24HrsUserID1 = Convert.ToInt32(dr["RenewalAlert24HrsUserID1"]);
                    myList[i].RenewalAlert24HrsUserID2 = Convert.ToInt32(dr["RenewalAlert24HrsUserID2"]);

                    //username
                    myList[i].ExpirationAlert180daysUserName1 = (dr["ExpirationAlert180daysUserName1"].ToString()); //js
                    myList[i].ExpirationAlert180daysUserName2 = (dr["ExpirationAlert180daysUserName2"].ToString());//js
                    myList[i].ExpirationAlert90daysUserName1 = (dr["ExpirationAlert90daysUserName1"].ToString());//js
                    myList[i].ExpirationAlert90daysUserName2 = (dr["ExpirationAlert90daysUserName2"].ToString());//js
                    myList[i].ExpirationAlert60daysUserName1 = (dr["ExpirationAlert60daysUserName1"].ToString());//js
                    myList[i].ExpirationAlert60daysUserName2 = (dr["ExpirationAlert60daysUserName2"].ToString());//js

                    myList[i].ExpirationAlert30daysUserName1 = (dr["ExpirationAlert30daysUserName1"].ToString());
                    myList[i].ExpirationAlert30daysUserName2 = (dr["ExpirationAlert30daysUserName2"].ToString());
                    myList[i].ExpirationAlert7daysUserName1 = (dr["ExpirationAlert7daysUserName1"].ToString());
                    myList[i].ExpirationAlert7daysUserName2 = (dr["ExpirationAlert7daysUserName2"].ToString());
                    myList[i].ExpirationAlert24HrsUserName1 = (dr["ExpirationAlert24HrsUserName1"].ToString());
                    myList[i].ExpirationAlert24HrsUserName2 = (dr["ExpirationAlert24HrsUserName2"].ToString());

                    myList[i].RenewalAlert180daysUserName1 = (dr["RenewalAlert180daysUserName1"].ToString());//js
                    myList[i].RenewalAlert180daysUserName2 = (dr["RenewalAlert180daysUserName2"].ToString());//js
                    myList[i].RenewalAlert90daysUserName1 = (dr["RenewalAlert90daysUserName1"].ToString());//js
                    myList[i].RenewalAlert90daysUserName2 = (dr["RenewalAlert90daysUserName2"].ToString());//js
                    myList[i].RenewalAlert60daysUserName1 = (dr["RenewalAlert60daysUserName1"].ToString());//js
                    myList[i].RenewalAlert60daysUserName2 = (dr["RenewalAlert60daysUserName2"].ToString());//js

                    myList[i].RenewalAlert30daysUserName1 = (dr["RenewalAlert30daysUserName1"].ToString());
                    myList[i].RenewalAlert30daysUserName2 = (dr["RenewalAlert30daysUserName2"].ToString());
                    myList[i].RenewalAlert7daysUserName1 = (dr["RenewalAlert7daysUserName1"].ToString());
                    myList[i].RenewalAlert7daysUserName2 = (dr["RenewalAlert7daysUserName2"].ToString());
                    myList[i].RenewalAlert24HrsUserName1 = (dr["RenewalAlert24HrsUserName1"].ToString());
                    myList[i].RenewalAlert24HrsUserName2 = (dr["RenewalAlert24HrsUserName2"].ToString());

                    myList[i].ContractID = Convert.ToInt32(dr["ContractID"].ToString());

                    myList[i].IsIndirectConsequentialLosses = (dr["IsIndirectConsequentialLosses"].ToString());
                    myList[i].IndirectConsequentialLosses = (dr["IndirectConsequentialLosses"].ToString());
                    myList[i].IsIndemnity = (dr["IsIndemnity"].ToString());
                    myList[i].IsWarranties = (dr["IsWarranties"].ToString());
                    myList[i].Warranties = (dr["Warranties"].ToString());
                    myList[i].EntireAgreementClause = (dr["EntireAgreementClause"].ToString());
                    myList[i].ThirdPartyGuaranteeRequired = (dr["ThirdPartyGuaranteeRequired"].ToString());
                    myList[i].IsForceMajeure = (dr["IsForceMajeure"].ToString());
                    myList[i].ForceMajeure = (dr["ForceMajeure"].ToString());
                    myList[i].IsSetOffRights = (dr["IsSetOffRights"].ToString());
                    myList[i].SetOffRights = (dr["SetOffRights"].ToString());
                    myList[i].IsGoverningLaw = (dr["IsGoverningLaw"].ToString());
                    myList[i].GoverningLaw = (dr["GoverningLaw"].ToString());

                    tableEX.Rows.Add(dr["ExpirationAlert180days"].ToString(), "180", dr["ExpirationAlert180daysUserID1"].ToString(), dr["ExpirationAlert180daysUserID2"].ToString(), dr["ExpirationAlert180daysUserName1"].ToString(), dr["ExpirationAlert180daysUserName2"].ToString());
                    tableEX.Rows.Add(dr["ExpirationAlert90days"].ToString(), "90", dr["ExpirationAlert90daysUserID1"].ToString(), dr["ExpirationAlert90daysUserID2"].ToString(), dr["ExpirationAlert90daysUserName1"].ToString(), dr["ExpirationAlert90daysUserName2"].ToString());
                    tableEX.Rows.Add(dr["ExpirationAlert60days"].ToString(), "60", dr["ExpirationAlert60daysUserID1"].ToString(), dr["ExpirationAlert60daysUserID2"].ToString(), dr["ExpirationAlert60daysUserName1"].ToString(), dr["ExpirationAlert60daysUserName2"].ToString());
                    tableEX.Rows.Add(dr["ExpirationAlert30days"].ToString(), "30", dr["ExpirationAlert30daysUserID1"].ToString(), dr["ExpirationAlert30daysUserID2"].ToString(), dr["ExpirationAlert30daysUserName1"].ToString(), dr["ExpirationAlert30daysUserName2"].ToString());
                    tableEX.Rows.Add(dr["ExpirationAlert7days"].ToString(), "7", dr["ExpirationAlert7daysUserID1"].ToString(), dr["ExpirationAlert7daysUserID2"].ToString(), dr["ExpirationAlert7daysUserName1"].ToString(), dr["ExpirationAlert7daysUserName2"].ToString());
                    tableEX.Rows.Add(dr["ExpirationAlert24Hrs"].ToString(), "1", dr["ExpirationAlert24HrsUserID1"].ToString(), dr["ExpirationAlert24HrsUserID2"].ToString(), dr["ExpirationAlert24HrsUserName1"].ToString(), dr["ExpirationAlert24HrsUserName2"].ToString());

                    tableRen.Rows.Add(dr["RenewalAlert180days"].ToString(), "180", dr["RenewalAlert180daysUserID1"].ToString(), dr["RenewalAlert180daysUserID2"].ToString(), dr["RenewalAlert180daysUserName1"].ToString(), dr["RenewalAlert180daysUserName2"].ToString());
                    tableRen.Rows.Add(dr["RenewalAlert90days"].ToString(), "90", dr["RenewalAlert90daysUserID1"].ToString(), dr["RenewalAlert90daysUserID2"].ToString(), dr["RenewalAlert90daysUserName1"].ToString(), dr["RenewalAlert90daysUserName2"].ToString());
                    tableRen.Rows.Add(dr["RenewalAlert60days"].ToString(), "60", dr["RenewalAlert60daysUserID1"].ToString(), dr["RenewalAlert60daysUserID2"].ToString(), dr["RenewalAlert60daysUserName1"].ToString(), dr["RenewalAlert60daysUserName2"].ToString());
                    tableRen.Rows.Add(dr["RenewalAlert30days"].ToString(), "30", dr["RenewalAlert30daysUserID1"].ToString(), dr["RenewalAlert30daysUserID2"].ToString(), dr["RenewalAlert30daysUserName1"].ToString(), dr["RenewalAlert30daysUserName2"].ToString());
                    tableRen.Rows.Add(dr["RenewalAlert7days"].ToString(), "7", dr["RenewalAlert7daysUserID1"].ToString(), dr["RenewalAlert7daysUserID2"].ToString(), dr["RenewalAlert7daysUserName1"].ToString(), dr["RenewalAlert7daysUserName2"].ToString());
                    tableRen.Rows.Add(dr["RenewalAlert24Hrs"].ToString(), "1", dr["RenewalAlert24HrsUserID1"].ToString(), dr["RenewalAlert24HrsUserID2"].ToString(), dr["RenewalAlert24HrsUserName1"].ToString(), dr["RenewalAlert24HrsUserName2"].ToString());

                    System.Web.HttpContext.Current.Session["ImportDateDataEx"] = tableEX;
                    System.Web.HttpContext.Current.Session["ImportDateDataRen"] = tableRen;

                    i = i + 1;
                }
                return myList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }

        }

        public List<KeyFeilds> ReadActivityReminderData(IKeyFields I)
        {
            int i = 0;

            List<KeyFeilds> myList = new List<KeyFeilds>();

            Parameters.AddWithValue("@ReminderDate", I.ReminderDate);
            Parameters.AddWithValue("@Param", I.Param);
            Parameters.AddWithValue("@AssignToId", I.AssignToId);
            SqlDataReader dr = getExecuteReader("ActivityReminderSchedular");
            try
            {
                i = 0;

                while (dr.Read())
                {

                    myList.Add(new KeyFeilds());

                    if (I.Param == 2)
                    {
                        myList[i].ActActivityDate = (dr["ActActivityDate"].ToString());
                        myList[i].Description = dr["Description"].ToString();
                        myList[i].ActReminderDate = (dr["ActReminderDate"].ToString());
                        myList[i].ActivityTypeName = dr["ActivityTypeName"].ToString();
                        myList[i].StatusName = dr["StatusName"].ToString();
                        myList[i].User = dr["User"].ToString();
                    }
                    myList[i].AssignUser = dr["AssignUser"].ToString();
                    myList[i].AssignToId = Convert.ToInt32(dr["AssignToId"]);
                    if (I.Param == 1)
                    {
                        myList[i].Email = (dr["Email"].ToString());
                    }

                    i = i + 1;
                }
                return myList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }

        }

        public string UpdateURLLastRunTimeForWindowsService(IKeyFields I)
        {
            Parameters.AddWithValue("@URLForWindowsService", I.URLForWindowsService);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("UpdateURLLastRunTimeForWindowsService", "@Status");
        }

        public string createTextFile(IKeyFields I, string fileName)
        {

            string FilePath = System.Web.HttpContext.Current.Server.MapPath(@"~/Uploads/" + System.Web.HttpContext.Current.Session["TenantDIR"] + "/Key_Obligation/");

            TextWriter tw = File.CreateText(FilePath + fileName);
            tw.WriteLine("Liability Cap: " + I.LiabilityCap);
            tw.WriteLine("Indemnity: " + I.Indemnity);
            if (I.IsChangeofControl == "N")
                tw.WriteLine("Change Of Control: No");
            else
                tw.WriteLine("Change Of Control: Yes");

            if (I.IsAgreementClause == "N")
                tw.WriteLine("Entire Agreement Clause : No");
            else
                tw.WriteLine("Entire Agreement Clause: Yes");

            if (I.IsStrictliability == "N")
                tw.WriteLine("Strict liability : No");
            else
                tw.WriteLine("Strict liability : Yes");

            tw.WriteLine("Strict liability description: " + I.Strictliability);

            if (I.IsGuaranteeRequired == "N")
                tw.WriteLine("Third Party Guarantee Required : No");
            else
                tw.WriteLine("Third Party Guarantee Required : Yes");

            tw.WriteLine("Insurance: " + I.Insurance);
            tw.WriteLine("Termination: " + I.TerminationDescription);
            tw.WriteLine("Assignment/Novation/Subcontract: " + I.AssignmentNovation);
            tw.WriteLine("Others: " + I.Others);
            tw.Close();
            Console.WriteLine(Console.Read());
            return FilePath + fileName;
        }

        public DataTable GetUsers(IKeyFields I)
        {
            DataTable dt = new DataTable();
            //string se = System.Web.HttpContext.Current.Session[Declarations.User].ToString();
            //Parameters.AddWithValue("@UserId", se);
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
            SqlDataReader dr = getExecuteReader("SelectAllUsers");
            try
            {
                dt.Load(dr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return dt;
        }

        public DataTable GetMetaDataDateUser(IKeyFields I)
        {
            DataTable dt = new DataTable();
            //string se = System.Web.HttpContext.Current.Session[Declarations.User].ToString();
            Parameters.AddWithValue("@RequestId", I.RequestId);
            SqlDataReader dr = getExecuteReader("SelectMetaDataDateUser");
            try
            {
                dt.Load(dr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return dt;
        }

    }
}
