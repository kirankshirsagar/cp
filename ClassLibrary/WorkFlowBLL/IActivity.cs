﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;
using System.Data.SqlClient;

namespace WorkflowBLL
{
    public interface IActivity :ICrud , INavigation 
    {
        int RNO { get; set; }
        string OutlookFileName { get; set; }

        int ActivityId { get; set; }
        string ActivityIds { get; set; }
        int ActivityTypeId { get; set; }
        int ActivityStatusId { get; set; }
        string ActivityText { get; set; }
        string ActivityTypeName { get; set; }
        string ActivityStatusName { get; set; }
        DateTime ActivityDate { get; set; }
        Nullable<DateTime> ReminderDate { get; set; }
        string ActivityDateDefaultFormat { get; set; }
        string ReminderDateDefaultFormat { get; set; }
        DateTime OnlyDate { get; set; }
        string  IsForCustomer { get; set; }
        string Search { get; set; }
        string ActivityTime { get; set; }
        string AddedTime { get; set; }
        string AddedByName { get; set; }
        int RequestId { get; set; }
        int ReqID { get; set; }
        int ContractID { get; set; }
        int Flag { get; set; }
        string OpenActivityType { get; set; }
        string ContractTypeId { get; set; } 
        List<Activity> ActivityDetails { get; set; }
        List<Activity> ActivityDates { get; set; }
        List<Activity> ActivityOtherDetails { get; set; }
        int AssignToId { get; set; }
        string AssignToName { get; set; }
        void ReadData();
        List<Activity> GetUserNameAndPassword();
        string UserName { get; set; }
        string Password { get; set; }
        string AddedOnDate { get; set; }

        DateTime? StartWeekDate { get; set; }
        DateTime? EndWeekDate { get; set; }

        int ActivityMonth { get; set; }
        int ActivityYear { get; set; }
        void ReadDataCalendar();
        void ReadAgendaData();

        List<Activity> GetActivityPending();
        string ActivitySnoozeAndDismiss();
        int Snooze { get; set; }
        string ActivityDateString { get; set; }
        string EmailID { get; set; }
        string isEditable { get; set; }
        string IsFromCalendar { get; set; }
        string IsOCR { get; set; }

        string FileUpload { get; set; }
        string FileUploadOriginal { get; set; }
        string FileSizeKb { get; set; }

        string ActivityFileUpload { get; set; }
        string ActivityFileUploadOriginal { get; set; }
        string ActivityFileSizeKb { get; set; }

        string ContractTypeIds { get; set; }

        string InsertActivityRecord();

        List<SelectControlFields> SelectContractIds();

        
        string ErrorIfAny { get; set; }
        string ContractDetailsByRequestId();

        //string ReadAssineDetails();
    }
}
