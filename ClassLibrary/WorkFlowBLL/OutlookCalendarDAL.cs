﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;

using System.Globalization;
using System.IO;
using System.Configuration;

namespace WorkflowBLL
{
    public class OutlookCalendarDAL : DAL
    {
        #region OutlookCalendarFile

        private readonly static string FilePath = System.Web.HttpContext.Current.Server.MapPath(@"~/OutlookCalendar/" + System.Web.HttpContext.Current.Request.Url.Segments[1].Replace("/", ""));

        private List<Activity> GetActivityDetails(IOutlookCalendar I)
        {
            List<Activity> act = new List<Activity>();
            Parameters.Clear();            
            Parameters.AddWithValue("@AssignedToId", I.AssignToId); //int single user id
            Parameters.AddWithValue("@RequestIDs", I.RequestIds);
            Parameters.AddWithValue("@AssignToIds", I.AssignToIds);//string comma separated multiple user ids
            Parameters.AddWithValue("@IsBlank", I.IsBlank);
            SqlDataReader dr = getExecuteReader("ActivityDetailsRead");

            int i = 0;
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    act.Add(new Activity());
                    if (I.IsBlank == 'Y')
                    {
                        act[i].RNO = int.Parse(dr["RNO"].ToString());
                        act[i].AssignToId = int.Parse(dr["AssignToId"].ToString());                        
                        act[i].OutlookFileName = dr["OutlookFileName"].ToString();
                    }
                    else
                    {                        
                        act[i].RNO = int.Parse(dr["RNO"].ToString());
                        act[i].OutlookFileName = dr["OutlookFileName"].ToString();

                        act[i].ActivityId = int.Parse(dr["ActivityId"].ToString());
                        act[i].ActivityTypeId = int.Parse(dr["ActivityTypeId"].ToString());
                        act[i].ActivityTypeName = dr["TypeName"].ToString();
                        act[i].ActivityText = dr["ActivityText"].ToString();
                        act[i].ActivityDate = Convert.ToDateTime(dr["ActivityDate"].ToString());
                        act[i].ActivityTime = dr["ActivityTime"].ToString();
                        //act[i].ActivityDateDefaultFormat = Convert.ToDateTime(dr["ActivityDateDefaultFormat"].ToString());
                        act[i].ActivityDateDefaultFormat = dr["ActivityDateDefaultFormat"].ToString();
                        act[i].AddedByName = dr["AssignByName"].ToString();
                        act[i].AddedOn = Convert.ToDateTime(dr["AddedOn"].ToString());
                        act[i].AddedTime = dr["AddedTime"].ToString();
                        act[i].OnlyDate = Convert.ToDateTime(dr["OnlyDate"].ToString());
                        act[i].ActivityStatusName = dr["StatusName"].ToString();
                        act[i].ActivityStatusId = int.Parse(dr["ActivityStatusId"].ToString());
                        act[i].IsForCustomer = dr["isForCustomer"].ToString();
                        act[i].AssignToId = int.Parse(dr["AssignToId"].ToString());
                        act[i].AssignToName = dr["AssignToName"].ToString();
                        act[i].isEditable = dr["isEditable"].ToString();
                        act[i].ReminderDateDefaultFormat = dr["ReminderDate"].ToString();
                        act[i].FileUpload = dr["FileUpload"].ToString();
                        act[i].FileUploadOriginal = dr["FileUploadOriginal"].ToString();
                        act[i].FileSizeKb = dr["FileSizeKb"].ToString();
                        if (!string.IsNullOrEmpty(Convert.ToString(dr["RequestId"])))
                            act[i].RequestId = Convert.ToInt32(dr["RequestId"].ToString());
                        if (!string.IsNullOrEmpty(Convert.ToString(dr["ContractId"])))
                            act[i].ContractID = Convert.ToInt32(dr["ContractId"].ToString());
                    }
                    //if (dr["ReminderDate"].ToString() != "" && dr["ReminderDate"].ToString() != null)
                    //myList5[i].ReminderDate = Convert.ToDateTime(dr["ReminderDate"].ToString());
                    i = i + 1;
                }
            }
            return act;
        }

        private string UpdateUserLog(IActivity I)
        {
            Parameters.AddWithValue("@FileName", I.OutlookFileName);
            Parameters.AddWithValue("@UsersId", I.AssignToId);
            Parameters.AddWithValue("@ErrorIfAny", I.ErrorIfAny);

            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("OutlookCalendarAddUpdate", "@Status");
        }

        public string UpdateOutlookFile(IOutlookCalendar I)
        {
            string EncryptedText = "", FileName = "", filePath = "", ErrorIfAny = "", status = "";
            List<Activity> ActivityDetails = null;
            try
            {
                //string OutlookCalendar = ConfigurationManager.AppSettings["OutlookCalendar"].ToUpper();
                string IsOutlookEnable = IsOutlookCalendarEnable().ToUpper();
                if (IsOutlookEnable.Equals("Y"))
                {
                    ActivityDetails = new List<Activity>();
                    ActivityDetails = GetActivityDetails(I);

                    StreamWriter writer = null;
                    string AddToCal = "", BinaryString = null;
                    DateTime date = new DateTime(1700, 1, 1);

                    for (int i = 0; i < ActivityDetails.Count; i++)
                    {
                        if (ActivityDetails[i].ActivityId==0 || ActivityDetails[i].ActivityId == 0)
                        {
                            if (ActivityDetails[i].RNO == 1 && !string.IsNullOrEmpty(ActivityDetails[i].OutlookFileName))
                            {
                                //filePath = FilePath + "/" + ActivityDetails[i].OutlookFileName;
                                writer = File.CreateText(ActivityDetails[i].OutlookFileName);
                                writer.WriteLine("BEGIN:VCALENDAR");
                                writer.WriteLine("PRODID:-//NewGalexy/ContractPod/v2");
                                writer.WriteLine("BEGIN:VEVENT");
                                writer.WriteLine("UID:0");
                                writer.WriteLine("DTSTART:" + date.ToString("yyyyMMdd\\THHmmss\\Z"));
                                writer.WriteLine("DTEND:" + date.ToString("yyyyMMdd\\THHmmss\\Z"));
                                writer.WriteLine("LOCATION:");
                                //writer.WriteLine("X-ALT-DESC;FMTTYPE=text/html:<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 3.2//EN'>\\n<HTML>\\n<HEAD>\\n<META NAME='Generator' CONTENT='MS Exchange Server version 08.01.0240.003'>\\n<TITLE></TITLE>\\n</HEAD>\\n<BODY>\\n\\n\\n<P DIR=LTR><SPAN LANG='en-us'></SPAN>" + AddToCal + "<SPAN LANG='en-us'></SPAN></A><SPAN LANG='en-us'></SPAN></P>\\n\\n</BODY></HTML>");
                                writer.WriteLine("SUMMARY:");
                                writer.WriteLine("DECSCRIPTION:");
                                writer.WriteLine("PRIORITY:3");
                                writer.WriteLine("CLASS:PUBLIC");
                                writer.WriteLine("END:VEVENT");
                                writer.WriteLine("END:VCALENDAR");
                                writer.Flush();
                                writer.Close();
                            }
                        }
                        else
                        {
                            try
                            {
                                if (ActivityDetails[i].RNO == 1)
                                {
                                    EncryptedText = SSTCryptographer.Encrypt(ActivityDetails[i].AssignToName + "_" + Convert.ToString(ActivityDetails[i].AssignToId), "Uberall*1");
                                    EncryptedText = EncryptedText.Replace("*", "").Replace("?", "").Replace("<", "").Replace(">", "").Replace("|", "").Replace(":", "").Replace("/", "").Replace("\\", "").Replace("\"", "").Replace("+", "1");
                                    FileName = EncryptedText + ".ics";
                                    filePath = FilePath + "/" + EncryptedText + ".ics";
                                    if (!Directory.Exists(FilePath))
                                    {
                                        Directory.CreateDirectory(FilePath);
                                    }

                                    FileInfo fileexist = new FileInfo(filePath);
                                    writer = File.CreateText(filePath);
                                    writer.WriteLine("BEGIN:VCALENDAR");
                                    writer.WriteLine("PRODID:-//NewGalexy/ContractPod/v2");
                                }

                                DateTime dtmStartTime = ActivityDetails[i].ActivityDate;
                                Nullable<DateTime> dtmEndTime = Convert.ToDateTime(ActivityDetails[i].ReminderDateDefaultFormat == "" ? null : ActivityDetails[i].ReminderDateDefaultFormat);
                                Nullable<TimeSpan> ts = new TimeSpan();
                                ts = (dtmStartTime - dtmEndTime);

                                string myLocation = "";
                                AddToCal = "";

                                writer.WriteLine("BEGIN:VEVENT");
                                writer.WriteLine("UID:" + ActivityDetails[i].ActivityId.ToString());
                                //writer.WriteLine("DTSTART:" + DateTime.Parse(ActivityDetails[i].ActivityDate.ToString()).ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z"));
                                //writer.WriteLine("DTEND:" + DateTime.Parse(dtmEndTime.ToString()).ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z"));
                                writer.WriteLine("DTSTART:" + ActivityDetails[i].ActivityDate.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z"));
                                writer.WriteLine("DTEND:" + ActivityDetails[i].ActivityDate.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z"));
                                writer.WriteLine("LOCATION:" + myLocation);                                

                                AddToCal += "Assigned By : " + ActivityDetails[i].AddedByName + "<br /><br />";
                                AddToCal += "Request Id : " + ActivityDetails[i].RequestId + "<br /><br />";
                                AddToCal += "Contract Id : " + ActivityDetails[i].ContractID + "<br /><br />";
                                AddToCal += "Activity Type : " + ActivityDetails[i].ActivityTypeName + "<br /><br />";
                                AddToCal += "Activity Description : " + ActivityDetails[i].ActivityText + "<br /><br />";
                                AddToCal += "Activity Status : " + ActivityDetails[i].ActivityStatusName + "<br /><br />";

                                /*if (!string.IsNullOrEmpty(ActivityDetails[i].FileUpload))
                                {
                                    BinaryString = FileToBinary(System.Web.HttpContext.Current.Server.MapPath(@"" + ActivityDetails[i].FileUpload.Replace("..", "~")));
                                    BinaryString = "ATTACH;ENCODING=BASE64;VALUE=BINARY;X-FILENAME=" + ActivityDetails[i].FileUploadOriginal + ":" + BinaryString + "<br /><br />";
                                }
                                writer.WriteLine(BinaryString);*/
                                writer.WriteLine("X-ALT-DESC;FMTTYPE=text/html:<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 3.2//EN'>\\n<HTML>\\n<HEAD>\\n<META NAME='Generator' CONTENT='MS Exchange Server version 08.01.0240.003'>\\n<TITLE></TITLE>\\n</HEAD>\\n<BODY>\\n\\n\\n<P DIR=LTR><SPAN LANG='en-us'></SPAN>" + AddToCal + "<SPAN LANG='en-us'></SPAN></A><SPAN LANG='en-us'></SPAN></P>\\n\\n</BODY></HTML>");
                                //writer.WriteLine(BinaryString);
                                writer.WriteLine("SUMMARY:" + ActivityDetails[i].ActivityTypeName);
                                //writer.WriteLine("DECSCRIPTION:" + AddToCal.Replace("<br /><br />", "\n"));
                                writer.WriteLine("DECSCRIPTION:" + ActivityDetails[i].ActivityTypeName);
                                writer.WriteLine("PRIORITY:3");
                                writer.WriteLine("CLASS:PUBLIC");
                                if (ts != null)
                                {
                                    writer.WriteLine("BEGIN:VALARM");
                                    writer.WriteLine("TRIGGER:-PT" + Math.Round(ts.GetValueOrDefault().TotalMinutes) + "M");
                                    writer.WriteLine("ACTION:DISPLAY");
                                    writer.WriteLine("DESCRIPTION:Reminder");
                                    writer.WriteLine("END:VALARM");
                                }
                                writer.WriteLine("END:VEVENT");

                                if (i < ActivityDetails.Count)
                                {
                                    if (i == ActivityDetails.Count - 1)
                                    {
                                        writer.WriteLine("END:VCALENDAR");
                                        writer.Flush();
                                        writer.Close();
                                        Parameters.Clear();
                                        Parameters.AddWithValue("@FileName", filePath);
                                        Parameters.AddWithValue("@UsersId", ActivityDetails[i].AssignToId);
                                        Parameters.AddWithValue("@ErrorIfAny", ErrorIfAny);

                                        Parameters.AddWithValue("@Status", 0);
                                        Parameters["@Status"].Direction = ParameterDirection.Output;
                                        status = getExcuteQuery("OutlookCalendarAddUpdate", "@Status");
                                        if (!string.IsNullOrEmpty(ErrorIfAny))
                                        {
                                            status = ErrorIfAny;
                                        }
                                    }
                                    else if (ActivityDetails[i + 1].RNO == 1)
                                    {
                                        writer.WriteLine("END:VCALENDAR");
                                        writer.Flush();
                                        writer.Close();

                                        Parameters.Clear();
                                        Parameters.AddWithValue("@FileName", filePath);
                                        Parameters.AddWithValue("@UsersId", ActivityDetails[i].AssignToId);
                                        Parameters.AddWithValue("@ErrorIfAny", ErrorIfAny);

                                        Parameters.AddWithValue("@Status", 0);
                                        Parameters["@Status"].Direction = ParameterDirection.Output;
                                        status = getExcuteQuery("OutlookCalendarAddUpdate", "@Status");
                                        if (!string.IsNullOrEmpty(ErrorIfAny))
                                        {
                                            status = ErrorIfAny;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                ErrorIfAny = ex.Message;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                status = ex.Message;
            }

            return status;
        }

        private static string IsOutlookCalendarEnable()
        {
            string dirname = System.Web.HttpContext.Current.Request.Url.Segments[1].Replace("/", "");
            string IsEnable = "Y";
            return IsEnable;
        }

        /// <summary>
        /// Function to get byte array from a file
        /// </summary>
        /// <param name="_FileName">File name to get byte array</param>
        /// <returns>Byte Array</returns>
        public string FileToBinary(string _FileName)
        {
            byte[] _Buffer = null;
            try
            {
                // Open file for reading

                System.IO.FileStream _FileStream = new System.IO.FileStream(_FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                // attach filestream to binary reader

                System.IO.BinaryReader _BinaryReader = new System.IO.BinaryReader(_FileStream);
                // get total byte length of the file
                long _TotalBytes = new System.IO.FileInfo(_FileName).Length;

                // read entire file into buffer
                _Buffer = _BinaryReader.ReadBytes((Int32)_TotalBytes);
                // close file reader
                _FileStream.Close();
                _FileStream.Dispose();
                _BinaryReader.Close();
            }
            catch (Exception _Exception)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());
            }
            return Convert.ToBase64String(_Buffer, 0, _Buffer.Length);
        }

      
        #endregion

    }
}
