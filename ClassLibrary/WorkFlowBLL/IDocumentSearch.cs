﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonBLL;
using CrudBLL;
using System.Data;

namespace WorkflowBLL
{
    public interface IDocumentSearch : INavigation
    {
        string ClientName { get; set; }
        string ContractType { get; set; }
        string AssignedTo { get; set; }
        string RequestDate { get; set; }
        string Link { get; set; }
        int UserId { get; set; }
        string Search { get; set; }
        string Docname { get; set; }
        string Doctitle { get; set; }

        int AssignToUserId { get; set; }
        int RequesterUserId { get; set; }
        List<DocumentSearch> ReadData();
        int RequestId { get; set; }
        int ContrctId { get; set; }

        int ContractTypeID { get; set; }
        int ClientID { get; set; }
        string ContractStatus { get; set; }
        int ContractID { get; set; }
        int AssignToId { get; set; }
        int RequestUserID { get; set; }
        int DepartmentId { get; set; }
        decimal ContractValue { get; set; }
        string CustomerSupplierType { get; set; }
        string SearchFor { get; set; }

        DateTime? RequestFromDate { get; set; }
        DateTime? RequestToDate { get; set; }
        DateTime? SignatureFromDate { get; set; }
        DateTime? SignatureToDate { get; set; }
        DateTime? EffectiveFromDate { get; set; }
        DateTime? EffectiveToDate { get; set; }
        DateTime? ExpiryFromDate { get; set; }
        DateTime? ExpiryToDate { get; set; }

        string SignatureDate { get; set; }
        string ActivityFileUpload { get; set; }
        string FoundIn { get; set; }
        string GetAll { get; set; }

        DataSet ReadFilterData();
        List<DocumentSearch> MetaDataSearch();

    }
}
