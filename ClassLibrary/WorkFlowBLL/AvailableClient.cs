﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using CommonBLL;

namespace WorkflowBLL
{
    public class AvailableClient : IAvailableClient
    {
        AvailbleClientDAL obj;
       
        public int ClientID { get; set; }
        public string ClientIDs { get; set; }
        public string ClientName { get; set; }
        public string Address { get; set; }
        public int CountryID { get; set; }
        public string CountryName { get; set; }
        public int StateID { get; set; }
        public string StateName { get; set; }
        public int CityID { get; set; }
        public string CityName { get; set; }
        public string Pincode { get; set; }
        public string ContactNo { get; set; }
        public string EmailID { get; set; }
        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public string Search { get; set; }
        public int Direction { get; set; }
        public string SortColumn { get; set; }
        public string IsUsed { get; set; }
        private int _AddedBy;
        public int Param { get; set; }
        public string isCustomer { get; set; }

        public string Street { get; set; }
      
        public int AddedBy
        {
            get { return _AddedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _AddedBy = value;
            }
        }

        private int _ModifiedBy;

        public int ModifiedBy
        {
            get { return _ModifiedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _ModifiedBy = value;
            }
        }

        private string _IpAddress;

        public string IpAddress
        {
            get { return _IpAddress; }
            set
            {
                if (value.ToString() == "")
                {
                    throw new CustomException("IP not captured");
                }
                _IpAddress = value;
            }
        }
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Description { get; set; }


       

        public string InsertRecord()
        {
            try
            {
                obj = new AvailbleClientDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public string DeleteRecord()
        {
            try
            {
                obj = new AvailbleClientDAL();
                return obj.DeleteRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateRecord()
        {
             try
            {
                obj = new AvailbleClientDAL();
                return obj.UpdateRecord(this);
            }
             catch (Exception ex)
             {
                 throw ex;
             }
        }

        public List<AvailableClient> ReadData()
        {
            try
            {
                obj = new AvailbleClientDAL();
                return obj.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ReadEditData()
        {
            try
            {
                obj = new AvailbleClientDAL();
                obj.ReadEditData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<AvailableClient> ReadClient { get; set; }
        public List<AvailableClient> ReadAddress { get; set; }
        public List<AvailableClient> ReadContact { get; set; }
        public List<AvailableClient> ReadEmailID { get; set; }
        public List<SelectControlFields> SelectData(int withOutSelect = 0)
        {
            try
            {
                obj = new AvailbleClientDAL();
                return obj.SelectData(this, withOutSelect);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
     
    }
}
