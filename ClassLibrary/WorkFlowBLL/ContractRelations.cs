﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using CommonBLL;

namespace WorkflowBLL
{
    public class ContractRelations : IContractRelations 
    {
        ContractRelationsDAL objContractRelations;

        public int ContractRelationsId { get; set; }
        public int ParentOfContractId { get; set; }
        public int ChildOfContractId { get; set; }
        public string ContractRelationsIds { get; set; }
        public string RelationsDescription { get; set; }
        public string For { get; set; }
        public int Flag { get; set; }
        public string IsUsed { get; set; }

        public int ParentContractId { get; set; }
        public int ChildContractId { get; set; }
        public string PIDClient { get; set; }
        public string CIDClient { get; set; }
        public string PIDClientHref { get; set; }
        public string CIDClientHref { get; set; }
        public int ChildrenDropLevel { get; set; }
        public string HtmlClass { get; set; }
        public string URL { get; set; }
        public bool IsParentOfDisabled { get; set; }
        public bool IsChildOfDisabled { get; set; }

        public DateTime ContractRelationsDate { get; set; }
        public string AddedOnDate { get; set; }
        public string AddedByName { get; set; }
        public int RequestId { get; set; }
        public int ContractID { get; set; }
        public string ContractTypeId { get; set; }

        public int AddedBy { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string IpAddress { get; set; }
        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public string Search { get; set; }
        public string Description { get; set; }
        public int Direction { get; set; }
        public string SortColumn { get; set; }

        public List<SelectControlFields> ContractRelationsAll()
        {
            try
            {
                objContractRelations = new ContractRelationsDAL();
                return objContractRelations.SelectContractRelationsAll(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<SelectControlFields> ParentOf()
        {
            try
            {
                objContractRelations = new ContractRelationsDAL();
                return objContractRelations.SelectContractRelationsParentOf(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<SelectControlFields> ChildOf()
        {
            try
            {
                objContractRelations = new ContractRelationsDAL();
                return objContractRelations.SelectContractRelationsChildOf(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }        

        public List<ContractRelations> ReadData()
        {
            try
            {
                objContractRelations = new ContractRelationsDAL();
                return objContractRelations.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ContractRelations> ReadDataForGraph()
        {
            try
            {
                objContractRelations = new ContractRelationsDAL();
                return objContractRelations.ReadDataForGraph(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string InsertRecord()
        {
            try
            {
                objContractRelations = new ContractRelationsDAL();
                return objContractRelations.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string InsertContractRelationsRecord()
        {
            try
            {
                objContractRelations = new ContractRelationsDAL();
                return objContractRelations.InsertContractRelationsRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DeleteRecord()
        {
            try
            {
                objContractRelations = new ContractRelationsDAL();
                return objContractRelations.DeleteRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateRecord()
        {
            try
            {
                objContractRelations = new ContractRelationsDAL();
                return objContractRelations.UpdateRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }    
}
