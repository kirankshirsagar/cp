﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using CommonBLL;

namespace WorkflowBLL
{
    public class KeyFeilds : IKeyFields
    {

        KeyFieldsDAL actdal;

        public Nullable<DateTime> EffectiveDate { get; set; }
        public Nullable<DateTime> ExpirationDate { get; set; }
        public Nullable<DateTime> RenewalDate { get; set; }

        public string RenewalDatestring { get; set; }
        public string EffectiveDatestring { get; set; }
        public string ExpirationDatestring { get; set; }

        public int RequestId { get; set; }
        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public string Search { get; set; }

        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Description { get; set; }
        public int AddedBy { get; set; }
        public int ModifiedBy { get; set; }
        public string IpAddress { get; set; }
        public string ExpirationAlert180days { get; set; }//js
        public string ExpirationAlert90days { get; set; }//js
        public string ExpirationAlert60days { get; set; }//js
        public string ExpirationAlert30days { get; set; }
        public string ExpirationAlert7days { get; set; }
        public string ExpirationAlert24Hrs { get; set; }

        public string RenewalAlert180days { get; set; }//js
        public string RenewalAlert90days { get; set; }//js
        public string RenewalAlert60days { get; set; }//js
        public string RenewalAlert30days { get; set; }
        public string RenewalAlert7days { get; set; }
        public string RenewalAlert24Hrs { get; set; }

        public int Direction { get; set; }
        public string SortColumn { get; set; }

        public string LiabilityCap { get; set; }
        public string Indemnity { get; set; }
        public string IsChangeofControl { get; set; }
        public string IsAgreementClause { get; set; }
        public string IsStrictliability { get; set; }
        public string Strictliability { get; set; }
        public string IsGuaranteeRequired { get; set; }
        public string Insurance { get; set; }
        public string IsTerminationConvenience { get; set; }
        public string TerminationConvenience { get; set; }
        public string IsTerminationmaterial { get; set; }
        public string Terminationmaterial { get; set; }
        public string IsTerminationinsolvency { get; set; }
        public string Terminationinsolvency { get; set; }
        public string IsTerminationinChangeofcontrol { get; set; }
        public string TerminationinChangeofcontrol { get; set; }
        public string IsTerminationothercauses { get; set; }
        public string Terminationothercauses { get; set; }
        public string AssignmentNovation { get; set; }

        public string Others { get; set; }
        public int Param { get; set; }

        public int ExpirationAlert180daysUserID1 { get; set; }//js
        public int ExpirationAlert180daysUserID2 { get; set; }//js
        public int ExpirationAlert90daysUserID1 { get; set; }//js
        public int ExpirationAlert90daysUserID2 { get; set; }//js
        public int ExpirationAlert60daysUserID1 { get; set; }//js
        public int ExpirationAlert60daysUserID2 { get; set; }//js

        public int ExpirationAlert30daysUserID1 { get; set; }
        public int ExpirationAlert30daysUserID2 { get; set; }
        public int ExpirationAlert7daysUserID1 { get; set; }
        public int ExpirationAlert7daysUserID2 { get; set; }
        public int ExpirationAlert24HrsUserID1 { get; set; }
        public int ExpirationAlert24HrsUserID2 { get; set; }

        public int RenewalAlert180daysUserID1 { get; set; }//js
        public int RenewalAlert180daysUserID2 { get; set; }//js
        public int RenewalAlert90daysUserID1 { get; set; }//js
        public int RenewalAlert90daysUserID2 { get; set; }//js
        public int RenewalAlert60daysUserID1 { get; set; }//js
        public int RenewalAlert60daysUserID2 { get; set; }//js

        public int RenewalAlert30daysUserID1 { get; set; }
        public int RenewalAlert30daysUserID2 { get; set; }
        public int RenewalAlert7daysUserID1 { get; set; }
        public int RenewalAlert7daysUserID2 { get; set; }
        public int RenewalAlert24HrsUserID1 { get; set; }
        public int RenewalAlert24HrsUserID2 { get; set; }

        public string ExpirationAlert180daysUserName1 { get; set; }//js
        public string ExpirationAlert180daysUserName2 { get; set; }//js
        public string ExpirationAlert90daysUserName1 { get; set; }//js
        public string ExpirationAlert90daysUserName2 { get; set; }//js
        public string ExpirationAlert60daysUserName1 { get; set; }//js
        public string ExpirationAlert60daysUserName2 { get; set; }//js
        public string ExpirationAlert30daysUserName1 { get; set; }
        public string ExpirationAlert30daysUserName2 { get; set; }
        public string ExpirationAlert7daysUserName1 { get; set; }
        public string ExpirationAlert7daysUserName2 { get; set; }
        public string ExpirationAlert24HrsUserName1 { get; set; }
        public string ExpirationAlert24HrsUserName2 { get; set; }
        public string RenewalAlert180daysUserName1 { get; set; }//js
        public string RenewalAlert180daysUserName2 { get; set; }//js
        public string RenewalAlert90daysUserName1 { get; set; }//js
        public string RenewalAlert90daysUserName2 { get; set; }//js
        public string RenewalAlert60daysUserName1 { get; set; }//js
        public string RenewalAlert60daysUserName2 { get; set; }//js
        public string RenewalAlert30daysUserName1 { get; set; }
        public string RenewalAlert30daysUserName2 { get; set; }
        public string RenewalAlert7daysUserName1 { get; set; }
        public string RenewalAlert7daysUserName2 { get; set; }
        public string RenewalAlert24HrsUserName1 { get; set; }
        public string RenewalAlert24HrsUserName2 { get; set; }
        public string TerminationDescription { get; set; }

        public int ActivityId { get; set; }
        public int ActivityTypeId { get; set; }
        public int ActivityStatusId { get; set; }
        public string ActivityText { get; set; }
        public DateTime ActivityDate { get; set; }
        public Nullable<DateTime> ReminderDate { get; set; }
        public string IsForCustomer { get; set; }
        public int AssignToId { get; set; }
        public int ContractID { get; set; }
        public string IsFromCalendar { get; set; }
        public string IsFromOther { get; set; }
        public string ActivityTypeName { get; set; }

        public string AssignUser { get; set; }
        public string StatusName { get; set; }
        public string User { get; set; }

        public string ActReminderDate { get; set; }
        public string ActActivityDate { get; set; }

        public string Email { get; set; }

        public int MetaDataFieldId { get; set; }
        public int ReminderDays { get; set; }
        public int ReminderUser1 { get; set; }
        public int ReminderUser2 { get; set; }
        public string IsManualEntry { get; set; }

        public string URLForWindowsService { get; set; }
        public DataTable dtMatadataFieldValues { get; set; } //added by jyoti 25-12-2015
        public int ContractTypeId { get; set; }

        public string IsIndirectConsequentialLosses { get; set; }
        public string IndirectConsequentialLosses { get; set; }
        public string IsIndemnity { get; set; }
        public string IsWarranties { get; set; }
        public string Warranties { get; set; }
        public string EntireAgreementClause { get; set; }
        public string ThirdPartyGuaranteeRequired { get; set; }
        public string IsForceMajeure { get; set; }
        public string ForceMajeure { get; set; }
        public string IsSetOffRights { get; set; }
        public string SetOffRights { get; set; }
        public string IsGoverningLaw { get; set; }
        public string GoverningLaw { get; set; }

        public List<KeyFeilds> ReadData()
        {
            try
            {
                actdal = new KeyFieldsDAL();
                return actdal.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string InsertRecord()
        {
            try
            {
                actdal = new KeyFieldsDAL();
                return actdal.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields> SelectData()
        {
            try
            {
                actdal = new KeyFieldsDAL();
                return actdal.SelectData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DeleteRecord()
        {
            try
            {
                actdal = new KeyFieldsDAL();
                return actdal.DeleteRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateRecord()
        {
            try
            {
                actdal = new KeyFieldsDAL();
                return actdal.UpdateRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<KeyFeilds> ReadActivityReminderData()
        {
            try
            {
                actdal = new KeyFieldsDAL();
                return actdal.ReadActivityReminderData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateURLLastRunTimeForWindowsService()
        {
            try
            {
                actdal = new KeyFieldsDAL();
                return actdal.UpdateURLLastRunTimeForWindowsService(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string createTextFile(string fileName)
        {
            try
            {
                actdal = new KeyFieldsDAL();
                return actdal.createTextFile(this, fileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetMetaDataDateUser()
        {
            try
            {

                actdal = new KeyFieldsDAL();
                return actdal.GetMetaDataDateUser(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetUsers()
        {
            try
            {

                actdal = new KeyFieldsDAL();
                return actdal.GetUsers(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
