﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;


namespace WorkflowBLL
{
    public class ClientDAL : DAL
    {
        public string InsertRecord(IClient I)
        {
            Parameters.AddWithValue("@ClientName", I.ClientName);//9
            Parameters.AddWithValue("@Address", I.Address);//10
            Parameters.AddWithValue("@CityID", I.CityID);//11
            Parameters.AddWithValue("@StateID", I.StateID);//12
            Parameters.AddWithValue("@CountryID", I.CountryID);//13
            Parameters.AddWithValue("@Pincode", I.Pincode);//17
            Parameters.AddWithValue("@AddedBy", I.AddedBy);//18
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);//19
            Parameters.AddWithValue("@IpAddress", I.IpAddress);//20
           // Parameters.AddWithValue("@Status", 0);
            Parameters.AddWithValue("@ClientID", 0);
           // Parameters["@Status"].Direction = ParameterDirection.Output;
            Parameters["@ClientID"].Direction = ParameterDirection.Output;
            return getExcuteQuery("ClientAddUpdate", "@ClientID");
        }

        public List<Client> ReadData(IClient  I)
        {
            int i = 0;
            List<Client> myList = new List<Client>();
            Parameters.AddWithValue("@ClientID", I.ClientID);
            SqlDataReader dr = getExecuteReader("MasterCountryAddUpdate");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new Client());
                   
                    myList[i].ClientID = int.Parse(dr["ClientID"].ToString());//6
                    myList[i].ClientName = dr["ClientName"].ToString();//7
                    myList[i].Address = dr["Address"].ToString();//8
                    myList[i].CountryID = int.Parse(dr["CountryID"].ToString());//9
                    myList[i].CountryName = dr["CountryName"].ToString();//10
                    myList[i].StateID = int.Parse(dr["StateID"].ToString());//11
                    myList[i].StateName = dr["StateName"].ToString();//12
                    myList[i].CityID = int.Parse(dr["CityID"].ToString());//13
                    myList[i].CityName = dr["CityName"].ToString();//14
                    myList[i].Pincode = dr["Pincode"].ToString();//15
                    myList[i].AddedBy = int.Parse (dr["AddedBy"].ToString());//16
                    myList[i].ModifiedBy = int.Parse(dr["ModifiedBy"].ToString());//17
                    myList[i].IpAddress = dr["IpAddress"].ToString();
                    i = i + 1;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;

        }

        public List<SelectControlFields> SelectData(IClient I)
        {
            SqlDataReader dr = getExecuteReader("ContractRequestRead");
            return SelectControlFields.SelectData(dr);
        }
        
        public List<SelectControlFields> SelectClientData(IClient I)
        {
            SqlDataReader dr = getExecuteReader("MasterClientTypeSelect");
            return SelectControlFields.SelectData(dr);
        }

    }
}
