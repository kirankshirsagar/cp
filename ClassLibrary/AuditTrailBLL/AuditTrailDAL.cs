﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CommonBLL;
using System.Data.SqlClient;
using System.Web;

namespace AuditTrailBLL
{
    public class AuditTrailDAL : DAL
    {
        public DataTable GetFullReport(IAuditTrail I)
        {
            DataTable dt = new DataTable();
            Parameters.AddWithValue("@ContractId", I.ContractId);
            return getDataTable("AuditTrailRead");
        }


        public List<AuditTrail> GetReport(IAuditTrail I)
        {
            int i = 0;
            List<AuditTrail> myList = new List<AuditTrail>();
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            Parameters.AddWithValue("@ContractId", I.ContractId);
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@UserIds", I.UserIds);
            SqlDataReader dr = getExecuteReader("AuditTrailRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new AuditTrail());
                    myList[i].ActionDateTime = dr["Date Time"].ToString();
                    //myList[i].ActionTaken = dr["Contract ID"].ToString();
                    //myList[i].AuditTrailId = int.Parse(dr["Expiration Date"].ToString());
                    myList[i].ContractId = dr["Contract ID"].ToString();
                    //myList[i].IP = dr["IP"].ToString();
                    myList[i].RequestId = int.Parse(dr["Request ID"].ToString());
                    //myList[i].SectionName = dr["SectionName"].ToString();
                    //myList[i].UserId = dr["Competition Name"].ToString();
                    myList[i].FullName = dr["User Name"].ToString();
                    myList[i].DisplayMessage = dr["Change Details"].ToString();

                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;


        }

    }
}
