﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace AuditTrailBLL
{
   public class AuditTrail:IAuditTrail
    {
       AuditTrailDAL obj;
       public int AuditTrailId { get; set; }
       public int RequestId { get; set; }
       public string ContractId { get; set; } 
       public int UserId { get; set; }
       public string ActionTaken { get; set; }
       public string ActionDateTime { get; set; }
       public string SectionName { get; set; }
       public string IP { get; set; }
       public string FullName { get; set; }
       public string DisplayMessage { get; set; }
       public string ReportName { get; set; }
       public long TotalRecords { get; set; }
       public int PageNo { get; set; }
       public int RecordsPerPage { get; set; }
       public int Direction { get; set; }
       public string SortColumn { get; set; }
       public string Search { get; set; }
       public string UserIds { get; set; }

       public List<AuditTrail> GetReport()
       {
           obj = new AuditTrailDAL();
           return obj.GetReport(this);

       }

       public DataTable GetFullReport()
       {
           obj = new AuditTrailDAL();
           return obj.GetFullReport(this);

       }
    }
}
