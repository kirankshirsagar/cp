﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrudBLL;
using System.Data;

namespace AuditTrailBLL
{
    public interface IAuditTrail :  INavigation
    {
        int AuditTrailId { get; set; }
        int RequestId { get; set; }
        string ContractId { get; set; }
        int UserId { get; set; }
        string ActionTaken { get; set; }
        string ActionDateTime { get; set; }
        string SectionName { get; set; }
        string IP { get; set; }
        string FullName { get; set; }
        string DisplayMessage { get; set; }
        string ReportName { get; set; }

        long TotalRecords { get; set; }
        int PageNo { get; set; }
        int RecordsPerPage { get; set; }
        int Direction { get; set; }
        string SortColumn { get; set; }
        string Search { get; set; }
        string UserIds { get; set; }

        List<AuditTrail> GetReport();
        DataTable GetFullReport();
    }
}
