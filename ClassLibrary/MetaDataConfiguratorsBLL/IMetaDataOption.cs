﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;
using System.IO;

namespace MetaDataConfiguratorsBLL
{
    public interface IMetaDataOption : ICrud
    {
        int FieldID { get; set; }
        string OptionTypeText { get; set; }
        string FieldName { get; set; }

        List<MetaDataOption> ReadClauseField { get; set; }
        List<MetaDataOption> ReadClauseFieldDetails { get; set; }
        List<MetaDataOption> ReadClauseFieldValue { get; set; }
        void ReadMetaDataOptionDetails();
    }
}
