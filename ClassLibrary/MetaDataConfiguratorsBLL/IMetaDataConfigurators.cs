﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;
using System.IO;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace MetaDataConfiguratorsBLL
{
    public interface IMetaDataConfigurators :ICrud
    {
        int ContractTemplateId { get; set; }
        int MetaDataId { get; set; }
        DataTable TemplateFields { get; set; }
        //List<ContractTemplate> ReadData();
        List<SelectControlFields> SelectFieldTypeData();
        List<SelectControlFields> SelectEmailTemplateData();
        void GetDataTableFilled(string str, string strMetaData,int AddedBy,string AddedOn,string IPAddress);
        //void GetDocument(int ContractTemplateId, string filename, string filePath, System.Web.HttpResponse Response);
        string GetContractTemplateFieldRecord(); 
        string MetaDataType { get; set; }
        int FieldID { get; set; }
        #region added by jyoti
        int ID { get; set; }
        int EmailTemplateId { get; set; }
        string FieldName { get; set; }
        string FieldTypeName { get; set; }
        string FieldValue { get; set; }
        DataTable dtMatadataFieldValues { get; set; }
        List<MetaData> ControlList { get; set; }

        List<MetaData> GetControlList();
        //Panel GenerateRequestForm(string MetaType, string Id, string type);
        HtmlTable GenerateRequestForm(string MetaType, string Id, string type);
        HtmlTable GenerateRequestPdfForm(string MetaType, string Id, string type);
        #endregion jyoti
    }

}
