﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using CommonBLL;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI;
using MasterBLL;
using WorkflowBLL;


namespace MetaDataConfiguratorsBLL
{
    public class MetaData : IMetaDataConfigurators
    {
        MetaDataDAL obj;
        public int AddedBy { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string IpAddress { get; set; }
        public string Description { get; set; }
        public DataTable TemplateFields { get; set; }
        public int ContractTemplateId { get; set; }
        public int MetaDataId { get; set; }
        public string MetaDataType { get; set; }
        public int EmailTemplateId { get; set; }
        public int FieldID { get; set; }

        //#region added by jyoti
        public int ID { get; set; }
        public string FieldName { get; set; }
        public string FieldTypeName { get; set; }
        public string FieldValue { get; set; }
        public string DefaultSize { get; set; }
        public DataTable dtMatadataFieldValues { get; set; }
        public List<MetaData> ControlList { get; set; }
        //#endregion

        public List<SelectControlFields> SelectFieldTypeData()
        {
            try
            {
                obj = new MetaDataDAL();
                return obj.SelectFieldTypeData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<SelectControlFields> SelectEmailTemplateData()
        {
            try
            {
                obj = new MetaDataDAL();
                return obj.SelectEmailTemplateData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string InsertRecord()
        {
            try
            {
                obj = new MetaDataDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateRecord()
        {
            throw new NotImplementedException();
        }

        public string DeleteRecord()
        {
            throw new NotImplementedException();
        }

        public void GetDataTableFilled(string str, string strMetaData, int AddedbyValue, string AddedOnValue, string IPAddressValue)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("FieldTypeID");
            dt.Columns.Add("FieldNameValue");
            dt.Columns.Add("FieldNameText");
            dt.Columns.Add("MetaDataType");
            //dt.Columns.Add("EmailTempId");

            dt.Columns.Add("Addedby");
            dt.Columns.Add("AddedOn");
            dt.Columns.Add("IPAddress");
            DataRow row;

            if (str.Trim() != string.Empty)
            {
                try
                {

                    string[] str1 = str.Split('#');
                    foreach (var item1 in str1)
                    {
                        string[] val1 = item1.Split(',');
                        row = dt.NewRow();
                        row["FieldTypeID"] = val1[0];
                        row["FieldNameValue"] = val1[1].Split('$')[0];
                        row["FieldNameText"] = val1[1].Split('$')[1];
                        row["MetaDataType"] = strMetaData;
                        //row["EmailTempId"] = val1[2];

                        row["Addedby"] = AddedbyValue;
                        row["AddedOn"] = AddedOnValue;
                        row["IPAddress"] = IPAddressValue;

                        dt.Rows.Add(row);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            TemplateFields = dt;
        }

        public string GetContractTemplateFieldRecord()
        {
            try
            {
                obj = new MetaDataDAL();
                return obj.GetContractTemplateFieldRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //#region added by jyoti
        public List<MetaData> GetControlList()
        {
            try
            {
                obj = new MetaDataDAL();
                return obj.GetControlList(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HtmlTable GenerateRequestForm(string MetaType, string Id, string type = "")
        {
            try
            {
                List<MetaData> controlsList = new List<MetaData>();
                IMetaDataConfigurators objContractRequest;
                objContractRequest = FactoryMedaData.GetContractTemplateDetail();
                objContractRequest.MetaDataType = MetaType;

                if (Id == "")
                    objContractRequest.ID = 0;
                else
                    objContractRequest.ID = Convert.ToInt32(Id);
                objContractRequest.Description = type;
                controlsList = objContractRequest.GetControlList();
                int count = controlsList.Count;
                System.Web.UI.WebControls.Panel pnl = new System.Web.UI.WebControls.Panel();
                HtmlTable tbl = new HtmlTable();

                if (type == "view")
                {
                    tbl.ID = MetaType + "RequestFormview_tbl";
                    ViewFields(controlsList, tbl, Id, MetaType);
                }
                else
                {
                    tbl.ID = "MetaDataTable";
                    FetchFields(controlsList, tbl, MetaType);

                }

                pnl.Controls.Add(tbl);
                //return pnl;
                return tbl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ViewFields(List<MetaData> controlsList, HtmlTable tbl, string Id, string MetaType)
        {
            try
            {
                tbl.Attributes.Add("gen", "true");
                tbl.Width = "100%";
                tbl.Border = 0;
                tbl.Style.Add("table-layout", "fixed");
                tbl.Style.Add("text-align", "left");
                tbl.Attributes.Add("cellpadding", "0");
                tbl.Attributes.Add("cellspacing", "0");
                for (int i = 0; i < controlsList.Count; i++)
                {
                    string ele = Convert.ToString(controlsList[i].FieldTypeName);
                    string FLID = controlsList[i].FieldID.ToString();
                    int FieldId = Convert.ToInt32(FLID);
                    HtmlTableRow tr = null;

                    string ControlType = Convert.ToString(controlsList[i].FieldTypeName);
                    string fieldname = controlsList[i].FieldName.ToString();

                    if (ControlType == "Date" && MetaType == "Important Dates")
                    {
                        tr = new HtmlTableRow();
                        HtmlTableCell tdCtrls1 = new HtmlTableCell();
                        tdCtrls1.Width = "100%";
                        tdCtrls1.Align = "left";
                        tdCtrls1.InnerHtml = "<br>";
                        tdCtrls1.ColSpan = 3;
                        tr.Cells.Add(tdCtrls1);

                        if (tr != null)
                        {
                            bool added = false;
                            if (!added)
                                tbl.Rows.Add(tr);
                        }

                        tr = new HtmlTableRow();
                        HtmlTableCell tdCtrl = new HtmlTableCell();
                        tdCtrl.Width = "100%";
                        tdCtrl.Align = "left";
                        tdCtrl.InnerHtml = "<ul><li><strong>" + fieldname + " : </strong> <span style='font-size: 12px;color: #777675;' > " + controlsList[i].FieldValue + "</span></li></ul>";
                        tdCtrl.ColSpan = 3;
                        tr.Cells.Add(tdCtrl);

                        if (tr != null)
                        {
                            bool added = false;
                            if (!added)
                                tbl.Rows.Add(tr);
                        }

                        //if (ControlType == "Date")
                        //{
                        HtmlTableRow tr1 = new HtmlTableRow();

                        HtmlTableCell tdCtrls = new HtmlTableCell();
                        tdCtrls.Width = "100%";
                        tdCtrls.Align = "left";
                        tdCtrls.ColSpan = 3;

                        tr = new HtmlTableRow();
                        IKeyFields objusers = FactoryWorkflow.GetKeyFieldsDetails();
                        objusers.RequestId = Convert.ToInt32(Id);
                        DataTable dt = objusers.GetMetaDataDateUser();
                        DataTable dtCustR = new DataTable();
                        IEnumerable<DataRow> rowsrpt = dt.AsEnumerable()
                        .Where(r => r.Field<int>("MetaDataFieldId") == int.Parse(FLID));

                        if (rowsrpt.Count() > 0)
                            dtCustR = rowsrpt.CopyToDataTable();

                        if (dtCustR.Rows.Count > 0)
                        {
                            tdCtrls.InnerHtml = "<ul><li><strong>Alert For " + fieldname + " </strong></li></ul>";
                            tr1.Cells.Add(tdCtrls);

                            if (tr1 != null)
                            {
                                bool added = false;
                                if (!added)
                                    tbl.Rows.Add(tr1);
                            }

                            HtmlTable tblCust = new HtmlTable();
                            tblCust.ID = "CustTable";
                            tblCust.Width = "52%";
                            tblCust.BgColor = "#cccccc";
                            HtmlTableRow trCust = new HtmlTableRow();
                            trCust.BgColor = "#ffffff";
                            HtmlTableCell tdCust1 = new HtmlTableCell();
                            tdCust1.Width = "5%";
                            tdCust1.Align = "center";

                            HtmlTableCell tdCust2 = new HtmlTableCell();
                            tdCust2.Width = "20%";

                            Label lblCust1 = new Label();
                            lblCust1.ID = "Period";
                            lblCust1.Text = "Period";
                            tdCust2.Controls.Add(lblCust1);

                            HtmlTableCell tdCust3 = new HtmlTableCell();
                            tdCust3.Width = "35%";

                            Label lblCust2 = new Label();
                            lblCust2.ID = "User 1";
                            lblCust2.Text = "User 1";
                            tdCust3.Controls.Add(lblCust2);

                            HtmlTableCell tdCust4 = new HtmlTableCell();
                            tdCust4.Width = "35%";

                            Label lblCust3 = new Label();
                            lblCust3.ID = "User 2";
                            lblCust3.Text = "User 2";
                            tdCust4.Controls.Add(lblCust3);

                            trCust.Cells.Add(tdCust1);
                            trCust.Cells.Add(tdCust2);
                            trCust.Cells.Add(tdCust3);
                            trCust.Cells.Add(tdCust4);

                            tblCust.Rows.Add(trCust);

                            HtmlImage imge = null;

                            for (int k = 0; k < dtCustR.Rows.Count; k++)
                            {
                                trCust = new HtmlTableRow();
                                trCust.BgColor = "#ffffff";
                                trCust.Align = "left";

                                tdCust1 = new HtmlTableCell();
                                tdCust1.Align = "center";
                                imge = new HtmlImage();
                                imge.Src = "../Images/AvailYesIco.gif";
                                tdCust1.Controls.Add(imge);

                                tdCust2 = new HtmlTableCell();

                                lblCust1 = new Label();
                                lblCust1.ID = "Period" + Convert.ToString(dtCustR.Rows[k]["ReminderDays"]);
                                if (Convert.ToString(dtCustR.Rows[k]["ReminderDays"]) != "1")
                                    lblCust1.Text = Convert.ToString(dtCustR.Rows[k]["ReminderDays"]) + " days";
                                else
                                    lblCust1.Text = Convert.ToString("24") + " Hrs";
                                tdCust2.Controls.Add(lblCust1);

                                tdCust3 = new HtmlTableCell();

                                lblCust2 = new Label();
                                lblCust2.ID = "User1" + Convert.ToString(dtCustR.Rows[k]["ReminderUserName1"]);
                                lblCust2.Text = Convert.ToString(dtCustR.Rows[k]["ReminderUserName1"]);
                                tdCust3.Controls.Add(lblCust2);

                                tdCust4 = new HtmlTableCell();

                                lblCust3 = new Label();
                                lblCust3.ID = "User2" + Convert.ToString(dtCustR.Rows[k]["ReminderUserName2"]);
                                lblCust3.Text = Convert.ToString(dtCustR.Rows[k]["ReminderUserName2"]);
                                tdCust4.Controls.Add(lblCust3);

                                trCust.Cells.Add(tdCust1);
                                trCust.Cells.Add(tdCust2);
                                trCust.Cells.Add(tdCust3);
                                trCust.Cells.Add(tdCust4);

                                tblCust.Rows.Add(trCust);

                            }
                            HtmlTableCell tdCtrl1 = new HtmlTableCell();
                            tdCtrl1.Width = "100%";
                            tdCtrl1.ColSpan = 3;
                            tdCtrl1.Controls.Add(tblCust);
                            tr.Cells.Add(tdCtrl1);
                            tbl.Rows.Add(tr);
                        }
                        else
                        {
                            tdCtrls.InnerHtml = "<ul><li><strong>Alert For " + fieldname + " </strong><span style='font-size: 12px;color: #777675;' > : No Reminder</span></li></ul>";
                            tr1.Cells.Add(tdCtrls);

                            if (tr1 != null)
                            {
                                bool added = false;
                                if (!added)
                                    tbl.Rows.Add(tr1);
                            }
                        }
                    }
                    else
                    {
                        if (MetaType == "Important Dates")
                        {
                            tr = new HtmlTableRow();
                            HtmlTableCell tdCtrls1 = new HtmlTableCell();
                            tdCtrls1.Width = "100%";
                            tdCtrls1.Align = "left";
                            tdCtrls1.InnerHtml = "<br>";
                            tdCtrls1.ColSpan = 3;
                            tr.Cells.Add(tdCtrls1);

                            if (tr != null)
                            {
                                bool added = false;
                                if (!added)
                                    tbl.Rows.Add(tr);
                            }

                            tr = new HtmlTableRow();
                            HtmlTableCell tdCtrl = new HtmlTableCell();
                            tdCtrl.Width = "100%";
                            tdCtrl.Align = "left";
                            tdCtrl.InnerHtml = "<ul><li><strong>" + fieldname + " </strong> <span style='font-size: 12px;color: #777675;' > : " + controlsList[i].FieldValue + "</span></li></ul>";
                            tdCtrl.ColSpan = 3;
                            tr.Cells.Add(tdCtrl);

                            if (tr != null)
                            {
                                bool added = false;
                                if (!added)
                                    tbl.Rows.Add(tr);
                            }

                            //tr = new HtmlTableRow();
                            //HtmlTableCell tdCtrl = new HtmlTableCell();
                            ////tdCtrl.Width = "24.99%";
                            //tdCtrl.Width = "10.99%";
                            //tdCtrl.Align = "left";
                            //tdCtrl.InnerHtml = "<ul><li><strong>" + fieldname + "</strong></li></ul>";
                            //tdCtrl.Style.Add("vertical-align", "top");
                            //tdCtrl.Attributes.Add("class", "text-tab");
                            //tr.Cells.Add(tdCtrl);

                            //HtmlTableCell tdSeparator = new HtmlTableCell();
                            //tdSeparator.InnerText = ":";
                            //tdSeparator.Width = "1%";
                            //tdSeparator.Style.Add("vertical-align", "top");
                            //tr.Cells.Add(tdSeparator);

                            //HtmlTableCell tdInput = new HtmlTableCell();
                            //tdInput.Width = "73%";
                            //tdInput.Style.Add("vertical-align", "top");
                            ////  tdCtrl.InnerHtml = "" + controlsList[i].FieldValue + "";
                            //Label lbl = new Label();
                            //lbl.ID = fieldname;
                            //lbl.Text = controlsList[i].FieldValue;
                            ////lbl.Text = controlsList[i].FieldTypeName;
                            //tdInput.Controls.Add(lbl);
                            ////#region Create Controls
                            //tr.Cells.Add(tdInput);
                            //if (tr != null)
                            //{
                            //    bool added = false;
                            //    if (!added)
                            //        tbl.Rows.Add(tr);
                            //}
                        }
                        else
                        {
                            tr = new HtmlTableRow();
                            HtmlTableCell tdCtrl = new HtmlTableCell();
                            tdCtrl.Width = "25%";
                            //tdCtrl.Width = "10.99%";
                            tdCtrl.Align = "left";
                            tdCtrl.InnerHtml = "<ul><li><strong>" + fieldname.Replace("<span style='color: #289aa1;'>", "").Replace("</span>", "") + "</strong></li></ul>";
                            tdCtrl.Style.Add("vertical-align", "top");
                            //tdCtrl.Attributes.Add("class", "text-tab"); //comment on 15 jun 2016
                            tr.Cells.Add(tdCtrl);

                            HtmlTableCell tdSeparator = new HtmlTableCell();
                            tdSeparator.InnerText = ":";
                            tdSeparator.Width = "1%";
                            tdSeparator.Style.Add("vertical-align", "top");
                            tr.Cells.Add(tdSeparator);

                            HtmlTableCell tdInput = new HtmlTableCell();
                            tdInput.Width = "74%";
                            tdInput.Style.Add("vertical-align", "top");
                            //  tdCtrl.InnerHtml = "" + controlsList[i].FieldValue + "";
                            Label lbl = new Label();
                            lbl.ID = fieldname;
                            lbl.Text = controlsList[i].FieldValue;
                            lbl.Style.Add("padding-left", "11px");
                            lbl.Style.Add("display", "inherit");
                            tdInput.Controls.Add(lbl);
                            //#region Create Controls
                            tr.Cells.Add(tdInput);
                            if (tr != null)
                            {
                                bool added = false;
                                if (!added)
                                    tbl.Rows.Add(tr);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FetchFields(List<MetaData> controlsList, HtmlTable tbl, string MetaType)
        {
            try
            {
                tbl.Attributes.Add("gen", "true");
                tbl.Width = "100%";
                tbl.Border = 0;
                tbl.Style.Add("table-layout", "fixed");
                tbl.Style.Add("text-align", "left");
                tbl.Attributes.Add("cellpadding", "0");
                tbl.Attributes.Add("cellspacing", "0");
                for (int i = 0; i < controlsList.Count; i++)
                {
                    string ele = Convert.ToString(controlsList[i].FieldTypeName);
                    string FLID = controlsList[i].FieldID.ToString();
                    int FieldId = Convert.ToInt32(FLID);
                    string val = "";
                    HtmlTableRow tr = null;
                    string ControlType = Convert.ToString(controlsList[i].FieldTypeName);
                    string fieldname = controlsList[i].FieldName.ToString().Trim().Replace(" ", "");
                    string FieldCaption = controlsList[i].FieldName.ToString().Trim();
                    string FieldMaxSize = controlsList[i].DefaultSize.ToString().Trim();

                    tr = new HtmlTableRow();
                    bool IsConditional = true;
                    HtmlTableCell tdCtrl = new HtmlTableCell();
                    HtmlGenericControl label = new HtmlGenericControl("label");
                    label.Attributes.Add("for", "time_entry_issue_id");
                    label.InnerHtml = "" + FieldCaption + "";

                    tdCtrl.Width = "28%";
                    tdCtrl.Align = "right";
                    //tdCtrl.InnerHtml = "" + fieldname + "";
                    tdCtrl.Style.Add("vertical-align", "top");
                    tdCtrl.Style.Add("padding-top", "5px");
                    //tdCtrl.Attributes.Add("class", "text-tab");
                    tdCtrl.Controls.Add(label);
                    tr.Cells.Add(tdCtrl);
                    if (MetaType == "Important Dates")
                        tr.Attributes.Add("class", "trdynamic trimportantDate");
                    else
                        tr.Attributes.Add("class", "trdynamic");

                    HtmlTableCell tdInput = new HtmlTableCell();
                    tdInput.Style.Add("vertical-align", "top");

                    //#region Create Controls
                    string[] options = { };
                    switch (ControlType.ToUpper())
                    {
                        case "TEXT":
                        case "ALPHANUMERIC":
                        case "NUMBER":
                        case "CURRENCY":
                        case "LETTER":
                        case "DATE":
                            HtmlInputText txt = new HtmlInputText();
                            txt.ID = "txt" + fieldname + "_" + FieldId;
                            txt.ClientIDMode = ClientIDMode.Static;
                            txt.Style.Add("width", "29%");
                            txt.Value = controlsList[i].FieldValue;
                            txt.Attributes.Add("class", IsConditional ? " pasteBlock" : " pasteBlock");
                            txt.Attributes.Add("FieldID", FLID);
                            txt.Attributes.Add("FieldType", ControlType);
                            if (ControlType.ToUpper() == "TEXT")
                            {
                                txt.Attributes.Add("class", "ValOnlyNumChar");
                                txt.MaxLength = Convert.ToInt32(FieldMaxSize);
                            }
                            if (ControlType.ToUpper() == "NUMBER")
                            {
                                txt.Attributes.Add("class", "CustomCurrency");
                                txt.MaxLength = Convert.ToInt32(FieldMaxSize);
                            }
                            if (ControlType.ToUpper() == "DATE")
                            {
                                txt.Attributes.Add("class", "datepicker txtDate");
                                txt.Style.Add("width", "20%");
                                //txt.Attributes.Add("readonly", "true");
                            }
                            tdInput.Controls.Add(txt);
                            break;

                        case "MULTILINE TEXT":
                            HtmlTextArea multiline = new HtmlTextArea();
                            multiline.ClientIDMode = ClientIDMode.Static;
                            multiline.ID = "txt" + fieldname + "_" + FieldId;
                            multiline.Rows = 10;
                            //multiline.Cols = 20;                    
                            multiline.Value = controlsList[i].FieldValue;
                            multiline.Style.Add("width", "280px");
                            multiline.Style.Add("height", "40px");
                            //multiline.Style.Add("overflow", "hidden");
                            multiline.Attributes.Add("FieldID", FLID);                            
                            multiline.Attributes.Add("FieldType", ControlType);
                            multiline.Attributes.Add("MaxLength", "1000");//FieldMaxSize
                            multiline.Attributes.Add("class", "pasteBlock");                            

                            tdInput.Controls.Add(multiline);
                            break;

                        case "CHECKBOX":
                            System.Web.UI.WebControls.CheckBoxList cbList = new System.Web.UI.WebControls.CheckBoxList();
                            cbList.ClientIDMode = ClientIDMode.Static;
                            cbList.RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Vertical;
                            cbList.RepeatLayout = System.Web.UI.WebControls.RepeatLayout.Table;
                            cbList.TextAlign = TextAlign.Right;
                            cbList.ID = "chk" + fieldname + "_" + FieldId;
                            cbList.Attributes.Add("FieldID", FLID);
                            cbList.Attributes.Add("FieldType", ControlType);
                            cbList.DataSource = FillMasterData("MetaDataConfiguratorValues", FieldId, ControlType);
                            cbList.DataTextField = "Name";
                            cbList.DataValueField = "Id";
                            cbList.DataBind();
                            if (controlsList[i].FieldValue != "")
                            {
                                string[] SelectedValue = controlsList[i].FieldValue.Split(',');
                                //cbList.SelectedValue = (cbList.Items.FindByText(controlsList[i].FieldValue)).Value;
                                (from chklist in cbList.Items.Cast<ListItem>() where SelectedValue.Contains(chklist.Text) select chklist).ToList().ForEach(chklist => chklist.Selected = true);

                                for (int j = 0; j < cbList.Items.Count; j++)
                                {
                                    cbList.Items[j].Attributes.CssStyle.Add("width", "100%");
                                    cbList.Items[j].Attributes.CssStyle.Add("display", "block");
                                }
                            }
                            tdInput.Controls.Add(cbList);
                            break;

                        case "RADIOBUTTON":
                            System.Web.UI.WebControls.RadioButtonList rdoList = new System.Web.UI.WebControls.RadioButtonList();
                            rdoList.ClientIDMode = ClientIDMode.Static;
                            rdoList.RepeatLayout = System.Web.UI.WebControls.RepeatLayout.Table;
                            rdoList.ID = "rdo" + fieldname + "_" + FieldId;

                            rdoList.Attributes.Add("FieldID", FLID);
                            rdoList.Attributes.Add("FieldType", ControlType);
                            DataTable dt = new DataTable();
                            rdoList.DataSource = FillMasterData("MetaDataConfiguratorValues", FieldId, ControlType);
                            rdoList.DataTextField = "Name";
                            rdoList.DataValueField = "Id";
                            rdoList.DataBind();
                            for (int j = 0; j < rdoList.Items.Count; j++)
                            {
                                rdoList.Items[j].Attributes.CssStyle.Add("width", "100%");
                                rdoList.Items[j].Attributes.CssStyle.Add("display", "block");
                            }
                            if (controlsList[i].FieldValue != "")
                                rdoList.SelectedValue = (rdoList.Items.FindByText(controlsList[i].FieldValue)).Value;
                            // rdoList.Items[0].Attributes.CssStyle.Add("text-align", "right");
                            //rdoList.Attributes.CssStyle.Add("list-style", "none");
                            //rdoList.Attributes.CssStyle.Add("margin-left", "60px");
                            //rdoList.Items[0].Attributes.CssStyle.Add("margin", "0");
                            //rdoList.Items[0].Attributes.CssStyle.Add("padding", "0");
                            //rdoList.Items[0].Attributes.CssStyle.Add("display", "inline");
                            tdInput.Controls.Add(rdoList);
                            break;

                        case "DROPDOWN":
                            System.Web.UI.HtmlControls.HtmlInputHidden hdnField = new System.Web.UI.HtmlControls.HtmlInputHidden();
                            hdnField.ClientIDMode = ClientIDMode.Static;
                            hdnField.ID = "hdn" + fieldname + "_" + FieldId;
                            hdnField.Attributes.Add("FieldType", ControlType);

                            HtmlSelect drpList = new HtmlSelect();
                            drpList.ClientIDMode = ClientIDMode.Static;
                            drpList.ID = "ddl" + fieldname + "_" + FieldId;
                            drpList.Style.Add("width", "30%");
                            drpList.Attributes.Add("class", IsConditional ? "chzn-select conditional" : "chzn-select");
                            drpList.Attributes.Add("FieldID", FLID);
                            drpList.Attributes.Add("FieldType", ControlType);

                            drpList.DataSource = FillMasterData("MetaDataConfiguratorValues", FieldId, ControlType);
                            drpList.DataTextField = "Name";
                            drpList.DataValueField = "Id";
                            drpList.DataBind();
                            if (controlsList[i].FieldValue != "")
                            {
                                //   (drpList.Items.FindByText(controlsList[i].FieldValue)).Selected = true;

                                string[] s = controlsList[i].FieldValue.TrimEnd(',').Split(new char[] { ',' });
                                hdnField.Value = controlsList[i].FieldValue;
                                foreach (string lst in s)
                                {
                                    if (!string.IsNullOrEmpty(lst) && drpList.Items.FindByText(lst) != null)
                                        drpList.Items.FindByText(lst).Selected = true;
                                }
                            }


                            tdInput.Controls.Add(drpList);
                            tdInput.Controls.Add(hdnField);
                            break;
                    }
                    tr.Cells.Add(tdInput);
                    if (tr != null)
                    {
                        bool added = false;
                        if (!added)
                            tbl.Rows.Add(tr);
                    }
                    //#endregion
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<SelectControlFields> FillMasterData(string TableName, int FieldTypeId, string ControlType)
        {
            try
            {
                ICommonMaster obj = FactoryMaster.GetCommonMasterDetail();
                obj.TableName = TableName;
                obj.FieldId = FieldTypeId;
                List<SelectControlFields> myList = ControlType.ToUpper() != "DROPDOWN" ? obj.SelectDataNoSelect() : obj.SelectData();
                return myList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // #endregion

        public HtmlTable GenerateRequestPdfForm(string MetaType, string Id, string type = "")
        {
            try
            {
                List<MetaData> controlsList = new List<MetaData>();
                IMetaDataConfigurators objContractRequest;
                objContractRequest = FactoryMedaData.GetContractTemplateDetail();
                objContractRequest.MetaDataType = MetaType;

                if (Id == "")
                    objContractRequest.ID = 0;
                else
                    objContractRequest.ID = Convert.ToInt32(Id);
                objContractRequest.Description = type;
                controlsList = objContractRequest.GetControlList();
                int count = controlsList.Count;
                System.Web.UI.WebControls.Panel pnl = new System.Web.UI.WebControls.Panel();
                HtmlTable tbl = new HtmlTable();

                if (type == "view")
                {
                    tbl.ID = MetaType + "RequestFormview_tbl";
                    ViewFieldsPdf(controlsList, tbl, Id, MetaType);
                }
                else
                {
                    tbl.ID = "MetaDataTable";

                    FetchFields(controlsList, tbl, MetaType);

                }

                pnl.Controls.Add(tbl);
                //return pnl;
                return tbl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void ViewFieldsPdf(List<MetaData> controlsList, HtmlTable tbl, string Id, string MetaType)
        {
            try
            {
                tbl.Attributes.Add("gen", "true");
                tbl.Width = "100%";
                tbl.Border = 0;
                tbl.Style.Add("table-layout", "fixed");
                tbl.Style.Add("text-align", "left");
                tbl.Attributes.Add("cellpadding", "0");
                tbl.Attributes.Add("cellspacing", "0");
                for (int i = 0; i < controlsList.Count; i++)
                {
                    string ele = Convert.ToString(controlsList[i].FieldTypeName);
                    string FLID = controlsList[i].FieldID.ToString();
                    int FieldId = Convert.ToInt32(FLID);
                    HtmlTableRow tr = null;

                    string ControlType = Convert.ToString(controlsList[i].FieldTypeName);
                    string fieldname = controlsList[i].FieldName.ToString();

                    if (ControlType == "Date" && MetaType == "Important Dates")
                    {
                        tr = new HtmlTableRow();
                        HtmlTableCell tdCtrls1 = new HtmlTableCell();
                        tdCtrls1.Width = "100%";
                        tdCtrls1.Align = "left";
                        tdCtrls1.InnerHtml = "<br>";
                        tdCtrls1.ColSpan = 3;
                        tr.Cells.Add(tdCtrls1);

                        if (tr != null)
                        {
                            bool added = false;
                            if (!added)
                                tbl.Rows.Add(tr);
                        }

                        tr = new HtmlTableRow();
                        HtmlTableCell tdCtrl = new HtmlTableCell();
                        tdCtrl.Width = "100%";
                        tdCtrl.Align = "left";
                        tdCtrl.InnerHtml = "<ul><li><strong>" + fieldname + " : </strong> <span style='font-size: 12px;color: #777675;' > " + controlsList[i].FieldValue + "</span></li></ul>";
                        tdCtrl.ColSpan = 3;
                        tr.Cells.Add(tdCtrl);

                        if (tr != null)
                        {
                            bool added = false;
                            if (!added)
                                tbl.Rows.Add(tr);
                        }

                        //if (ControlType == "Date")
                        //{
                        HtmlTableRow tr1 = new HtmlTableRow();

                        HtmlTableCell tdCtrls = new HtmlTableCell();
                        tdCtrls.Width = "100%";
                        tdCtrls.Align = "left";
                        tdCtrls.ColSpan = 3;

                        tr = new HtmlTableRow();
                        IKeyFields objusers = FactoryWorkflow.GetKeyFieldsDetails();
                        objusers.RequestId = Convert.ToInt32(Id);
                        DataTable dt = objusers.GetMetaDataDateUser();
                        DataTable dtCustR = new DataTable();
                        IEnumerable<DataRow> rowsrpt = dt.AsEnumerable()
                        .Where(r => r.Field<int>("MetaDataFieldId") == int.Parse(FLID));

                        if (rowsrpt.Count() > 0)
                            dtCustR = rowsrpt.CopyToDataTable();

                        if (dtCustR.Rows.Count > 0)
                        {
                            tdCtrls.InnerHtml = "<ul><li ><strong style='color: #289aa1;'>Alert For" + fieldname + " </strong></li></ul>";
                            tr1.Cells.Add(tdCtrls);

                            if (tr1 != null)
                            {
                                bool added = false;
                                if (!added)
                                    tbl.Rows.Add(tr1);
                            }

                            HtmlTable tblCust = new HtmlTable();
                            tblCust.ID = "CustTable";
                            tblCust.Width = "52%";
                            tblCust.BgColor = "#cccccc";
                            HtmlTableRow trCust = new HtmlTableRow();
                            trCust.BgColor = "#ffffff";
                            HtmlTableCell tdCust1 = new HtmlTableCell();
                            tdCust1.Width = "5%";
                            tdCust1.Align = "center";

                            HtmlTableCell tdCust2 = new HtmlTableCell();
                            tdCust2.Width = "20%";

                            Label lblCust1 = new Label();
                            lblCust1.ID = "Period";
                            lblCust1.Text = "Period";
                            tdCust2.Controls.Add(lblCust1);

                            HtmlTableCell tdCust3 = new HtmlTableCell();
                            tdCust3.Width = "35%";

                            Label lblCust2 = new Label();
                            lblCust2.ID = "User 1";
                            lblCust2.Text = "User 1";
                            tdCust3.Controls.Add(lblCust2);

                            HtmlTableCell tdCust4 = new HtmlTableCell();
                            tdCust4.Width = "35%";

                            Label lblCust3 = new Label();
                            lblCust3.ID = "User 2";
                            lblCust3.Text = "User 2";
                            tdCust4.Controls.Add(lblCust3);

                            trCust.Cells.Add(tdCust1);
                            trCust.Cells.Add(tdCust2);
                            trCust.Cells.Add(tdCust3);
                            trCust.Cells.Add(tdCust4);

                            tblCust.Rows.Add(trCust);

                            Label imge = null;

                            for (int k = 0; k < dtCustR.Rows.Count; k++)
                            {
                                trCust = new HtmlTableRow();
                                trCust.BgColor = "#ffffff";
                                trCust.Align = "left";

                                tdCust1 = new HtmlTableCell();
                                tdCust1.Align = "center";
                                imge = new Label();
                                imge.Text = "Yes";
                                tdCust1.Controls.Add(imge);

                                tdCust2 = new HtmlTableCell();

                                lblCust1 = new Label();
                                lblCust1.ID = "Period" + Convert.ToString(dtCustR.Rows[k]["ReminderDays"]);
                                if (Convert.ToString(dtCustR.Rows[k]["ReminderDays"]) != "1")
                                    lblCust1.Text = Convert.ToString(dtCustR.Rows[k]["ReminderDays"]) + " days";
                                else
                                    lblCust1.Text = Convert.ToString("24") + " Hrs";
                                tdCust2.Controls.Add(lblCust1);

                                tdCust3 = new HtmlTableCell();

                                lblCust2 = new Label();
                                lblCust2.ID = "User1" + Convert.ToString(dtCustR.Rows[k]["ReminderUserName1"]);
                                lblCust2.Text = Convert.ToString(dtCustR.Rows[k]["ReminderUserName1"]);
                                tdCust3.Controls.Add(lblCust2);

                                tdCust4 = new HtmlTableCell();

                                lblCust3 = new Label();
                                lblCust3.ID = "User2" + Convert.ToString(dtCustR.Rows[k]["ReminderUserName2"]);
                                lblCust3.Text = Convert.ToString(dtCustR.Rows[k]["ReminderUserName2"]);
                                tdCust4.Controls.Add(lblCust3);

                                trCust.Cells.Add(tdCust1);
                                trCust.Cells.Add(tdCust2);
                                trCust.Cells.Add(tdCust3);
                                trCust.Cells.Add(tdCust4);

                                tblCust.Rows.Add(trCust);

                            }
                            HtmlTableCell tdCtrl1 = new HtmlTableCell();
                            tdCtrl1.Width = "100%";
                            tdCtrl1.ColSpan = 3;
                            tdCtrl1.Controls.Add(tblCust);
                            tr.Cells.Add(tdCtrl1);
                            tbl.Rows.Add(tr);
                        }
                        else
                        {
                            tdCtrls.InnerHtml = "<ul><li><strong style='color: #289aa1;'>Alert For " + fieldname + " </strong><span style='font-size: 12px;color: #777675;' > : No Reminder</span></li></ul>";
                            tr1.Cells.Add(tdCtrls);

                            if (tr1 != null)
                            {
                                bool added = false;
                                if (!added)
                                    tbl.Rows.Add(tr1);
                            }
                        }
                    }
                    else
                    {
                        tr = new HtmlTableRow();
                        HtmlTableCell tdCtrl = new HtmlTableCell();
                        tdCtrl.Width = "24.99%";
                        //tdCtrl.Width = "10.99%";
                        tdCtrl.Align = "left";
                        tdCtrl.InnerHtml = "<ul><li><strong>" + fieldname + "</strong></li></ul>";
                        tdCtrl.Style.Add("vertical-align", "top");
                        tdCtrl.Attributes.Add("class", "text-tab");
                        tr.Cells.Add(tdCtrl);

                        HtmlTableCell tdSeparator = new HtmlTableCell();
                        tdSeparator.InnerText = ":";
                        tdSeparator.Width = "1%";
                        tdSeparator.Style.Add("vertical-align", "top");
                        tr.Cells.Add(tdSeparator);

                        HtmlTableCell tdInput = new HtmlTableCell();
                        tdInput.Width = "73%";
                        tdInput.Style.Add("vertical-align", "top");
                        //  tdCtrl.InnerHtml = "" + controlsList[i].FieldValue + "";
                        Label lbl = new Label();
                        lbl.ID = fieldname;
                        lbl.Text = controlsList[i].FieldValue;
                        //lbl.Text = controlsList[i].FieldTypeName;
                        tdInput.Controls.Add(lbl);
                        //#region Create Controls
                        tr.Cells.Add(tdInput);
                        if (tr != null)
                        {
                            bool added = false;
                            if (!added)
                                tbl.Rows.Add(tr);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }

}
