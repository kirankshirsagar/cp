﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using CommonBLL;
using System.IO;

namespace MetaDataConfiguratorsBLL
{
    public class MetaDataOption:IMetaDataOption
    {
        MetaDataDAL obj;

        public int AddedBy { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string IpAddress { get; set; }
        public string Description { get; set; }
        public DataTable TemplateFields { get; set; }
        public int ContractTemplateId { get; set; }
        public int MetaDataId { get; set; }
        public string MetaDataType { get; set; }

        public int FieldID { get; set; }
        public string OptionTypeText { get; set; }
        public string FieldName { get; set; }

        public List<MetaDataOption> ReadClauseField { get; set; }
        public List<MetaDataOption> ReadClauseFieldDetails { get; set; }
        public List<MetaDataOption> ReadClauseFieldValue { get; set; }

        public int EmailTemplateId { get; set; }

        public string InsertRecord()
        {
            try
            {
                obj = new MetaDataDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string UpdateRecord()
        {
            throw new NotImplementedException();
        }
        public string DeleteRecord()
        {
            try
            {
                obj = new MetaDataDAL();
                return obj.DeletRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ReadMetaDataOptionDetails()
        {
            try
            {
                obj = new MetaDataDAL();
                obj.ReadMetaDataOptionDetails(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
