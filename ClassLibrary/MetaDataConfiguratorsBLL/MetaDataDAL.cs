﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;

namespace MetaDataConfiguratorsBLL
{
    public class MetaDataDAL : DAL
    {
        public List<SelectControlFields> SelectFieldTypeData(IMetaDataConfigurators I)
        {
            Parameters.AddWithValue("@MetaDataTypeID", I.MetaDataId);
            SqlDataReader dr = getExecuteReader("FieldTypeNameSelect");
            return SelectControlFields.SelectData(dr);
        }

        public List<SelectControlFields> SelectEmailTemplateData(IMetaDataConfigurators I)
        {
            Parameters.AddWithValue("@FieldId", I.FieldID);
            SqlDataReader dr = getExecuteReader("IDEmailTemplateSelect");
            return SelectControlFields.SelectData(dr);
        }


        public string InsertRecord(IMetaDataConfigurators I)
        {
            Parameters.AddWithValue("@TemplateFields", I.TemplateFields);
            Parameters.AddWithValue("@MetaDataType", I.MetaDataType);
            Parameters.AddWithValue("@FieldId", I.FieldID);
            Parameters.AddWithValue("@EmailTemplateId", I.EmailTemplateId);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            //return getExcuteQuery("MetaDataConfiguratorAddUpdate");
            return getExcuteQuery("MetaDataConfiguratorAddUpdate", "@Status");
        }

        public string InsertRecord(IMetaDataOption I)
        {
            Parameters.AddWithValue("@FieldId", I.FieldID);
            Parameters.AddWithValue("@FieldOptionValueText", I.OptionTypeText);
            return getExcuteQuery("InsertOptionsDetails");
        }

        public string DeletRecord(IMetaDataOption I)
        {
            Parameters.AddWithValue("@FieldId", I.FieldID);
            return getExcuteQuery("DeleteMetaDataAndValues");
        }

        public string GetContractTemplateFieldRecord(IMetaDataConfigurators I)
        {
            //Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
            Parameters.AddWithValue("@MetaDataType", I.MetaDataType);
            return getExcuteScalar("MetaDataConfiguratorRead");
        }


        public void ReadMetaDataOptionDetails(IMetaDataOption I)
        {
            string CommaSep = string.Empty;
            int i = 0;
            List<MetaDataOption> myList1 = new List<MetaDataOption>();
            List<MetaDataOption> myList2 = new List<MetaDataOption>();
            List<MetaDataOption> myList3 = new List<MetaDataOption>();
            Parameters.AddWithValue("@FieldId", I.FieldID);
            SqlDataReader drReadClause = getExecuteReader("ReadMetaDataOptionsDetails");
            try
            {
                while (drReadClause.Read())
                {
                    if (CommaSep.Length == 0)
                    //if (!string.IsNullOrEmpty(Convert.ToString(drReadClause["FieldOptionValue"])))
                    {
                        CommaSep = drReadClause["FieldOptionValue"].ToString();
                    }
                    else
                    {
                        CommaSep = CommaSep + "," + drReadClause["FieldOptionValue"].ToString();
                    }
                    myList1.Add(new MetaDataOption());
                    myList1[i].FieldName = drReadClause["FieldName"].ToString();
                    myList1[i].EmailTemplateId = int.Parse(drReadClause["EmailTemplateId"].ToString());
                }
                myList1[i].OptionTypeText = CommaSep;
                I.ReadClauseFieldDetails = myList1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (drReadClause.IsClosed != true && drReadClause != null)
                    drReadClause.Close();
            }
        }

        #region jyoti


        public List<MetaData> GetControlList(IMetaDataConfigurators I)
        {
            try
            {
                List<MetaData> controlsList = new List<MetaData>();

                Parameters.AddWithValue("@MetaDataType", I.MetaDataType);
                Parameters.AddWithValue("@ID", I.ID);
                Parameters.AddWithValue("@Type", I.Description);
                SqlDataReader dr = getExecuteReader("ReadContractField");
                int i = 0;
                while (dr.Read())
                {
                    controlsList.Add(new MetaData());
                    controlsList[i].FieldID = int.Parse(dr["FieldId"].ToString());
                    controlsList[i].FieldName = dr["FieldName"].ToString();
                    if (I.Description == "view")
                        controlsList[i].FieldName = "<span style='color: #289aa1;'>" + dr["FieldName"].ToString() + "</span>";
                    else
                        controlsList[i].FieldName = dr["FieldName"].ToString();
                    controlsList[i].FieldTypeName = dr["FieldTypeName"].ToString();
                    controlsList[i].FieldValue = dr["VALUE"].ToString();
                    controlsList[i].DefaultSize = dr["DefaultSize"].ToString();
                    if (I.MetaDataType == "Important Dates" && controlsList[i].FieldTypeName == "Date")
                    {
                        System.Web.HttpContext.Current.Session["IDFieldId"] += "," + dr["FieldId"].ToString();
                    }
                    i = i + 1;
                }
                I.ControlList = controlsList;
                return controlsList;
            }
            catch (Exception ex)
            {
                // Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        //ReadContractFieldForContract

        #endregion jyoti
    }
}
