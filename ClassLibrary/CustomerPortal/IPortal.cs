﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CommonBLL;
using CrudBLL;

namespace CustomerPortal
{
    public interface IPortal : INavigation
    {
        string UserName { get; set; }
        string Password { get; set; }
        long RequestId { get; set; }
        long ActivityTypeId { get; set; }
        long ActivityStatusId { get; set; }
        string ActivityText { get; set; }
        DateTime ActivityDate { get; set; }
        string IsForCustomer { get; set; }
        string IsCustomer { get; set; }
        DateTime ReminderDate { get; set; }

        long ContractID { get; set; }
        string ContractTypeName { get; set; }
        string CustomerName { get; set; }
        string CustomerDesignationName { get; set; }
        string CustomerDescription { get; set; }
        string CustomerImageURL { get; set; }
        string Date { get; set; }
        string Time { get; set; }
        string ContractTypeDiscription { get; set; }
        void CheckLogin();
        List<Portal> ReadData();
        List<Portal> ReadDataRepeter();
        List<Portal> PoralDetails { get; set; }
        string InsertRecord();
        int MessageCount { get; set; }
        int FileCount { get; set; }
        string EmailID { get; set; }
        string FileUpload { get; set; }
        string FileUploadOriginal { get; set; }
        decimal FileSizeKb { get; set; }
        string CustomerTenantDIR { get; set; }

    }

}
