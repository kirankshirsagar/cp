﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;


namespace CustomerPortal
{
    public class PortalDAL : DAL
    {

        public void CheckLogin(IPortal I)
        {
            Parameters.AddWithValue("@EmailID", I.EmailID);
            Parameters.AddWithValue("@Password", I.Password);
            SqlDataReader dr = getExecuteReader("ClientLoginCheck");
            try
            {
                while (dr.Read())
                {
                    I.RequestId = long.Parse(dr["RequestId"].ToString());
                    I.ContractID = long.Parse(dr["ContractID"].ToString());
                    I.CustomerName = dr["UserName"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
        }
        public List<Portal> ReadData(IPortal I)
        {
            int i = 0;
            List<Portal> myList = new List<Portal>();
            Parameters.AddWithValue("@RequestId", I.RequestId);
            SqlDataReader dr = getExecuteReader("PortalRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new Portal());
                    myList[i].ContractTypeName = dr["ContractTypeName"].ToString();
                    myList[i].ContractTypeDiscription = dr["Description"].ToString();
                    //i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        //I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
        }
     
        public string InsertRecord(IPortal I)
        {

            Parameters.AddWithValue("@ActivityTypeId", I.ActivityTypeId);
            Parameters.AddWithValue("@ActivityStatusId", I.ActivityStatusId);
            Parameters.AddWithValue("@ActivityText", I.ActivityText);
            Parameters.AddWithValue("@IsForCustomer", I.IsForCustomer);
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@IsCustomer", I.IsCustomer);
            Parameters.AddWithValue("@FileUpload", I.FileUpload);
            Parameters.AddWithValue("@FileUploadOriginal", I.FileUploadOriginal);
            Parameters.AddWithValue("@FileSizeKb", I.FileSizeKb);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("CustomerPortalADD", "@Status");
        }

        public List<Portal> ReadDataRepeter(IPortal I)
        {
            int i = 0;
            List<Portal> myList = new List<Portal>();
            Parameters.AddWithValue("@RequestId", I.RequestId);
            Parameters.AddWithValue("@CustomerTenantDIR", I.CustomerTenantDIR);
            SqlDataReader dr = getExecuteReader("CustomerPortalRead");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new Portal());
                    myList[i].CustomerName = dr["UserName"].ToString();
                    myList[i].CustomerDesignationName = dr["RoleName"].ToString();
                    myList[i].CustomerImageURL = dr["ImageUrl"].ToString();
                    myList[i].Date = dr["Addedon"].ToString();
                    myList[i].CustomerDescription = dr["ActivityText"].ToString();
                    myList[i].Time = dr["AddedonTime"].ToString();
                    i = i + 1;
                }
                I.PoralDetails = myList;
                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.MessageCount = int.Parse(dr["MessageCount"].ToString());
                        I.FileCount = int.Parse(dr["FileCount"].ToString());
                    }
                }




            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;
        }

     


    }
}
