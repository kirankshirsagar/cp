﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using CommonBLL;

namespace CustomerPortal

{

    public class Portal : IPortal
    {
        PortalDAL obj;
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ContractTypeName { get; set; }
        public string ContractTypeDiscription { get; set; }
        public long RequestId { get; set; }
        public long ContractID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerDesignationName { get; set; }
        public string CustomerDescription { get; set; }
        public string CustomerImageURL { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public List<Portal> PoralDetails { get; set; }

        public long ActivityTypeId { get; set; }
        public long ActivityStatusId { get; set; }
        public string ActivityText { get; set; }
        public DateTime ActivityDate { get; set; }
        public string IsForCustomer { get; set; }
        public string IsCustomer { get; set; }
        public DateTime ReminderDate { get; set; }

        public int Direction { get; set; }
        public string SortColumn { get; set; }

        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public string FileUpload { get; set; }
        public string FileUploadOriginal { get; set; }
        public decimal FileSizeKb { get; set; }
        public string EmailID { get; set; }
        public string CustomerTenantDIR { get; set; }

        public void CheckLogin()
        {
            obj = new PortalDAL();
            obj.CheckLogin(this);
        }
        public List<Portal> ReadData()
        {
            try
            {
                obj = new PortalDAL();
                return obj.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string InsertRecord()
        {
            try
            {
                obj = new PortalDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Portal> ReadDataRepeter()
        {
            try
            {
                obj = new PortalDAL();
                return obj.ReadDataRepeter(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private int _AddedBy;

        public int AddedBy
        {
            get { return _AddedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _AddedBy = value;
            }
        }

        private int _ModifiedBy;

        public int ModifiedBy
        {
            get { return _ModifiedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _ModifiedBy = value;
            }
        }

        private string _IpAddress;
        public string IpAddress
        {
            get { return _IpAddress; }
            set
            {
                if (value.ToString() == "")
                {
                    throw new CustomException("IP not captured");
                }
                _IpAddress = value;
            }
        }



        public int MessageCount { get; set; }
        public int FileCount { get; set; }
       




    }
}

