﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using StageConfigueBLL;

namespace StageConfigueBLL
{
    public class StageConfigurator : IStageConfigurator
    {
        StageConfiguratorDAL obj;


        public int FieldLibraryID { get; set; }
        public int OperatorID { get; set; }
        public string txtBox1 { get; set; }
        public string txtBox2 { get; set; }
        public string dtpick1 { get; set; }
        public string dtpick2 { get; set; }
        public string ForeignKeyValue { get; set; }

        public int ContractTypeId { get; set; }
        public int ContractTemplateId { get; set; }

        public int LevelID { get; set; }
        public string StageName { get; set; }

        public int DepartmentId { get; set; }
        public int UsersId { get; set; }
        public string isActive { get; set; }

        public string FieldName { get; set; }
        public int FieldTypeID { get; set; }
        public string DataType { get; set; }
        public int StageId { get; set; }
        public List<StageConfigurator> ReadParent { get; set; }
        public List<StageConfigurator> ReadChild { get; set; }

        public List<StageConfigurator> DepartmentList { get; set; }
        public List<StageConfigurator> UserNameList { get; set; }

        public string OperatorName { get; set; }

        public string IP { get; set; }
        public int Addedby { get; set; }
        public int Modifiedby { get; set; }
        public string DefaultSize { get; set; }
        public string FID { get; set; }
        public string PicklistObject { get; set; }
        public string UserName { get; set; }
        public DataTable StageConditions { get; set; }
        public string DepartmentName { get; set; }
        public string ANDOR { get; set; }
        public string ContractName { get; set; }
        public string StageIds { get; set; }
        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public string Search { get; set; }
        public string IsUsed { get; set; }



        public string LevelStr { get; set; }

        public int StageId1 { get; set; }
        public string StageName1 { get; set; }
        public string LevelStr1 { get; set; }
        public int LevelID1 { get; set; }
        public string AssignedTo1 { get; set; }

        public int StageId2 { get; set; }
        public string StageName2 { get; set; }
        public string LevelStr2 { get; set; }
        public int LevelID2 { get; set; }
        public string AssignedTo2 { get; set; }

        public int StageId3 { get; set; }
        public string StageName3 { get; set; }
        public string LevelStr3 { get; set; }
        public int LevelID3 { get; set; }
        public string AssignedTo3 { get; set; }

        public string conditions1 { get; set; }
        public string conditions2 { get; set; }
        public string conditions3 { get; set; }

        public List<StageConfigurator> Levels { get; set; }
        public List<StageConfigurator> Stages { get; set; }
        public List<StageConfigurator> Templates { get; set; }

        public DataTable CreateStageConditions(List<StageConfigurator> lst)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ANDOR");
            dt.Columns.Add("FieldLibraryID");
            dt.Columns.Add("OperatorID");
            dt.Columns.Add("TextValue");
            dt.Columns.Add("NumericValue");
            dt.Columns.Add("DecimalValue");
            dt.Columns.Add("DateValue");
            dt.Columns.Add("ForeignKeyValue");
            dt.Columns.Add("NumericValue2");
            dt.Columns.Add("DecimalValue2");
            dt.Columns.Add("DateValue2");
            dt.Columns.Add("DataType");
            DataRow row;


            foreach (var item in lst)
            {
                row = dt.NewRow();
                row["ANDOR"] = item.ANDOR;
                row["FieldLibraryID"] = item.FieldLibraryID;
                row["OperatorID"] = item.OperatorID;

                if (item.txtBox2 == "")
                {
                    if (item.DataType == "4")//Number
                    {
                        row["TextValue"] = "";
                        row["NumericValue"] = item.txtBox1;
                        row["NumericValue2"] = 0;
                        row["DecimalValue"] = 0;
                        row["DecimalValue2"] = 0;
                    }
                    else if (item.DataType == "5")//Currency
                    {
                        row["TextValue"] = "";
                        row["NumericValue"] = 0;
                        row["NumericValue2"] = 0;
                        row["DecimalValue"] = item.txtBox1;
                        row["DecimalValue2"] = 0;
                    }
                    else
                    {
                        row["TextValue"] = item.txtBox1;
                        row["NumericValue"] = 0;
                        row["NumericValue2"] = 0;
                        row["DecimalValue"] = 0;
                        row["DecimalValue2"] = 0;
                    }
                }
                else
                {
                    if (item.DataType == "4")
                    {
                        row["TextValue"] = "";
                        row["NumericValue"] = item.txtBox1;
                        row["NumericValue2"] = item.txtBox2;
                        row["DecimalValue"] = 0;
                        row["DecimalValue2"] = 0;
                    }
                    else if (item.DataType == "5")
                    {
                        row["TextValue"] = "";
                        row["NumericValue"] = 0;
                        row["NumericValue2"] = 0;
                        row["DecimalValue"] = item.txtBox1;
                        row["DecimalValue2"] = item.txtBox2;
                    }

                }


                row["ForeignKeyValue"] = item.ForeignKeyValue;

                row["DateValue"] = item.dtpick1;
                row["DateValue2"] = item.dtpick2;
                dt.Rows.Add(row);


            }

            return dt;

        }



        public void OtherDroDropdowns()
        {
            try
            {
                obj = new StageConfiguratorDAL();
                obj.OtherDroDropdowns(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ReadData()
        {
            try
            {
                obj = new StageConfiguratorDAL();
                obj.ReadData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectControlFields> ReadTemplates()
        {
            try
            {
                obj = new StageConfiguratorDAL();
                return obj.ReadTemplates(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void FieldReadMethod()
        {
            try
            {
                obj = new StageConfiguratorDAL();
                obj.FieldReadMethod(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<SelectControlFields> SelectData()
        {
            try
            {
                obj = new StageConfiguratorDAL();
                return obj.SelectData(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string InsertRecord()
        {
            try
            {
                obj = new StageConfiguratorDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

     

        private int _AddedBy;

        public int AddedBy
        {
            get { return _AddedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _AddedBy = value;
            }
        }

        private int _ModifiedBy;

        public int ModifiedBy
        {
            get { return _ModifiedBy; }
            set
            {
                if (value == 0)
                {
                    throw new CustomException("Session Out");
                }
                _ModifiedBy = value;
            }
        }



        private string _IpAddress;

        public string IpAddress
        {
            get { return _IpAddress; }
            set
            {
                if (value.ToString() == "")
                {
                    throw new CustomException("IP not captured");
                }
                _IpAddress = value;
            }
        }
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Description { get; set; }


        public string DeleteRecord()
        {
            try
            {
                obj = new StageConfiguratorDAL();
                return obj.DeleteRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string UpdateRecord()
        {
            try
            {
                obj = new StageConfiguratorDAL();
                return obj.UpdateRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
