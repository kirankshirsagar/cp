﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CommonBLL;
using Elmah;
using StageConfigueBLL;

namespace StageConfigueBLL
{
    public class StageConfiguratorDAL : DAL
    {
        public List<SelectControlFields> SelectData(IStageConfigurator I)
        {
            SqlDataReader dr = getExecuteReader("MasterCommonSelect");
            return SelectControlFields.SelectData(dr);
        }

        public string InsertRecord(IStageConfigurator I)
        {
            Parameters.AddWithValue("@StageConfigurator_Type", I.StageConditions);
            Parameters.AddWithValue("@StageName", I.StageName);
            Parameters.AddWithValue("@DepartmentId", I.DepartmentId);
            Parameters.AddWithValue("@UsersId", I.UsersId);
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
            Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
            Parameters.AddWithValue("@LevelID", I.LevelID);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("StageConfiguratorAddUpdate", "@Status");
        }

        public string UpdateRecord(IStageConfigurator I)
        {
            Parameters.AddWithValue("@StageConfigurator_Type", I.StageConditions);
            Parameters.AddWithValue("@StageName", I.StageName);
            Parameters.AddWithValue("@DepartmentId", I.DepartmentId);
            Parameters.AddWithValue("@UsersId", I.UsersId);
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
            Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
            Parameters.AddWithValue("@LevelID", I.LevelID);
            Parameters.AddWithValue("@StageId", I.StageId);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@Status", 0);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            return getExcuteQuery("StageConfiguratorAddUpdate", "@Status");
        }

        public string DeleteRecord(IStageConfigurator I)
        {
            Parameters.AddWithValue("@StageId", I.StageId);
            return getExcuteQuery("StageConfiguratorDelete");
        }

        public void FieldReadMethod(IStageConfigurator I)
        {
            int i = 0;
            List<StageConfigurator> myList1 = new List<StageConfigurator>();
            List<StageConfigurator> myList2 = new List<StageConfigurator>();
            Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
            SqlDataReader dr = getExecuteReader("StageConfiguratorSelect");
            try
            {
                while (dr.Read())
                {
                    myList1.Add(new StageConfigurator());
                    myList1[i].FieldLibraryID = int.Parse(dr["FieldLibraryID"].ToString());
                    myList1[i].FieldName = dr["FieldName"].ToString();
                    myList1[i].FID = dr["FID"].ToString();
                    myList1[i].FieldTypeID = int.Parse(dr["FieldTypeID"].ToString());
                    myList1[i].DataType = dr["DataType"].ToString();
                    myList1[i].DefaultSize = dr["DefaultSize"].ToString();
                    myList1[i].PicklistObject = dr["PicklistObject"].ToString();
                    i = i + 1;
                }
                I.ReadParent = myList1;

                i = 0;

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        myList2.Add(new StageConfigurator());
                        myList2[i].FieldLibraryID = int.Parse(dr["FieldLibraryID"].ToString());
                        myList2[i].FieldTypeID = int.Parse(dr["FieldTypeID"].ToString());
                        myList2[i].OperatorID = int.Parse(dr["OperatorID"].ToString());
                        myList2[i].OperatorName = dr["OperatorName"].ToString();
                        i = i + 1;
                    }
                }
                I.ReadChild = myList2;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
        }

        public void OtherDroDropdowns(IStageConfigurator I)
        {
            int i = 0;
            List<StageConfigurator> myList1 = new List<StageConfigurator>();
            List<StageConfigurator> myList2 = new List<StageConfigurator>();

            // Parameters.AddWithValue("@RoleId", I.RoleId);
            Parameters.AddWithValue("@ContractTypeID", I.ContractTypeId);
            SqlDataReader dr = getExecuteReader("StageOtherDropdownRead");
            try
            {
                while (dr.Read())
                {
                    myList1.Add(new StageConfigurator());
                    myList1[i].UsersId = int.Parse(dr["UsersId"].ToString());
                    myList1[i].UserName = dr["UserName"].ToString();
                    myList1[i].DepartmentId = int.Parse(dr["DepartmentId"].ToString());
                    i = i + 1;
                }
                I.UserNameList = myList1;

                i = 0;

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        myList2.Add(new StageConfigurator());
                        myList2[i].DepartmentId = int.Parse(dr["DepartmentId"].ToString());
                        myList2[i].DepartmentName = dr["DepartmentName"].ToString();
                        i = i + 1;
                    }
                }
                I.DepartmentList = myList2;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }

        }

        public void ReadData(IStageConfigurator I)
        {
            int i = 0;
            List<StageConfigurator> myList1 = new List<StageConfigurator>();
            List<StageConfigurator> myList2 = new List<StageConfigurator>();

            Parameters.AddWithValue("@StageId", I.StageId);
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
            Parameters.AddWithValue("@ContractTemplateId", I.ContractTemplateId);
            SqlDataReader dr = getExecuteReader("StageConfiguratorRead");
            if (I.StageId > 0)
            {

                try
                {
                    while (dr.Read())
                    {
                        myList1.Add(new StageConfigurator());
                        myList1[i].ANDOR = dr["ANDOR"].ToString();
                        myList1[i].dtpick1 = dr["DATEVALUE1"].ToString();
                        myList1[i].dtpick2 = dr["DATEVALUE2"].ToString();
                        if (dr["DECIMALVALUE1"].ToString() != "" && dr["DECIMALVALUE1"].ToString() != null)
                            myList1[i].txtBox1 = dr["DECIMALVALUE1"].ToString();
                        if (dr["DECIMALVALUE2"].ToString() != "" && dr["DECIMALVALUE2"].ToString() != null)
                            myList1[i].txtBox2 = dr["DECIMALVALUE2"].ToString();
                        if (dr["NUMERICVALUE1"].ToString() != "" && dr["NUMERICVALUE1"].ToString() != null)
                            myList1[i].txtBox1 = dr["NUMERICVALUE1"].ToString();
                        if (dr["NUMERICVALUE2"].ToString() != "" && dr["NUMERICVALUE2"].ToString() != null)
                            myList1[i].txtBox2 = dr["NUMERICVALUE2"].ToString();
                        if (dr["TEXTVALUE"].ToString() != "" && dr["TEXTVALUE"].ToString() != null)
                        {
                            myList1[i].txtBox1 = dr["TEXTVALUE"].ToString();
                            myList1[i].txtBox2 = "";
                        }

                        myList1[i].OperatorID = int.Parse(dr["OPERATORID"].ToString());
                        myList1[i].StageName = dr["STAGENAME"].ToString();
                        myList1[i].FieldLibraryID = int.Parse(dr["FIELDLIBRARYID"].ToString());
                        myList1[i].ForeignKeyValue = dr["FOREIGNKEYVALUE"].ToString();
                        myList1[i].ContractName = dr["CONTRACTTYPENAME"].ToString();
                        myList1[i].LevelID = int.Parse(dr["LEVELID"].ToString());
                        myList1[i].DepartmentId = int.Parse(dr["DEPARTMENTID"].ToString());
                        myList1[i].UsersId = int.Parse(dr["UsersId"].ToString());

                        i = i + 1;
                    }
                    I.ReadParent = myList1;
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (dr.IsClosed != true && dr != null)
                        dr.Close();
                }
            }

            else
            {

                try
                {
                    while (dr.Read())
                    {
                        myList1.Add(new StageConfigurator());
                        myList1[i].LevelStr = dr["LevelStr"].ToString();
                        i = i + 1;
                    }
                    I.Levels = myList1;

                    i = 0;

                    if (dr.NextResult())
                    {
                        while (dr.Read())
                        {
                            myList2.Add(new StageConfigurator());
                            myList2[i].StageId1 = int.Parse(dr["StageId1"].ToString());
                            myList2[i].StageName1 = dr["StageName1"].ToString();
                            myList2[i].LevelStr1 = dr["LevelStr1"].ToString();
                            myList2[i].LevelID1 = int.Parse(dr["LevelID1"].ToString());
                            myList2[i].AssignedTo1 = dr["AssignedTo1"].ToString();
                            myList2[i].conditions1 = dr["conditions1"].ToString(); 


                            myList2[i].StageId2 = int.Parse(dr["StageId2"].ToString());
                            myList2[i].StageName2 = dr["StageName2"].ToString();
                            myList2[i].LevelStr2 = dr["LevelStr2"].ToString();
                            myList2[i].LevelID2 = int.Parse(dr["LevelID2"].ToString());
                            myList2[i].AssignedTo2 = dr["AssignedTo2"].ToString();
                            myList2[i].conditions2 = dr["conditions2"].ToString(); 

                            myList2[i].StageId3 = int.Parse(dr["StageId3"].ToString());
                            myList2[i].StageName3 = dr["StageName3"].ToString();
                            myList2[i].LevelStr3 = dr["LevelStr3"].ToString();
                            myList2[i].LevelID3 = int.Parse(dr["LevelID3"].ToString());
                            myList2[i].AssignedTo3 = dr["AssignedTo3"].ToString();
                            myList2[i].conditions3 = dr["conditions3"].ToString(); 

                            i = i + 1;
                        }
                    }
                    I.Stages = myList2;

                   

                }

                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (dr.IsClosed != true && dr != null)
                        dr.Close();
                }
            }

        }

        public List<SelectControlFields> ReadTemplates(IStageConfigurator I)
        {
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeId);
            SqlDataReader dr = getExecuteReader("ContractTemplateTypeSelect");
            return SelectControlFields.SelectData(dr);
        }

    }
}
