﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;
using CommonBLL;
using StageConfigueBLL;

namespace StageConfigueBLL
{
    public interface IStageConfigurator : ICrud
    {
        int FieldLibraryID { get; set; }
        int OperatorID { get; set; }
        string txtBox1 { get; set; }
        string txtBox2 { get; set; }
        string dtpick1 { get; set; }
        string dtpick2 { get; set; }
        string ForeignKeyValue { get; set; }
        string StageIds { get; set; }
        int ContractTypeId { get; set; }
        int ContractTemplateId { get; set; }
        int LevelID { get; set; }
        string StageName { get; set; }
        int DepartmentId { get; set; }
        int UsersId { get; set; }
        string isActive { get; set; }
        string IsUsed { get; set; }
        string Search { get; set; }
      
        string IP { get; set; }
      
        string FieldName { get; set; }
        int FieldTypeID { get; set; }
        string DataType { get; set; }
        string FID { get; set; }
        string DefaultSize { get; set; }
        string DepartmentName { get; set; }
        string PicklistObject { get; set; }
        string OperatorName { get; set; }
        string UserName { get; set; }
        string ANDOR { get; set; }
        string ContractName { get; set; }
        int StageId { get; set; }
        List<StageConfigurator> UserNameList { get; set; }
        List<StageConfigurator> DepartmentList { get; set; }

        List<StageConfigurator> ReadParent { get; set; }
        List<StageConfigurator> ReadChild { get; set; }

        DataTable StageConditions { get; set; }
        DataTable CreateStageConditions(List<StageConfigurator> lst);

        void FieldReadMethod();
        void OtherDroDropdowns();
       
        List<SelectControlFields> SelectData();
        void ReadData();
        List<SelectControlFields> ReadTemplates(); 


        string LevelStr { get; set; }

        int StageId1 { get; set; }
        string StageName1 { get; set; }
        string LevelStr1 { get; set; }
        int LevelID1 { get; set; }
        string AssignedTo1 { get; set; }

        int StageId2 { get; set; }
        string StageName2 { get; set; }
        string LevelStr2 { get; set; }
        int LevelID2 { get; set; }
        string AssignedTo2 { get; set; }

        int StageId3 { get; set; }
        string StageName3 { get; set; }
        string LevelStr3 { get; set; }
        int LevelID3 { get; set; }
        string AssignedTo3 { get; set; }
        string conditions1 { get; set; }
        string conditions2 { get; set; }
        string conditions3 { get; set; }

        List<StageConfigurator> Levels { get; set; }
        List<StageConfigurator> Stages { get; set; }
        
    }

}
