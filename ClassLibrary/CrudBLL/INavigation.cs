﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CrudBLL
{
    public interface INavigation
    {
        long TotalRecords { get; set; }
        int PageNo { get; set; }
        int RecordsPerPage { get; set; }
        int Direction { get; set; }
        string SortColumn { get; set; }
      
    }
}
