﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace CrudBLL
{
    public interface ICrud
    {
        /// <summary>
        /// This mehtod is used to insert data to the database and returns string.
        /// </summary>
        /// <returns>"1" Success; "2" Duplicate; "0" Fail </returns>
        string InsertRecord();

        /// <summary>
        /// This mehtod is used to update data to the database and returns string.
        /// </summary>
        /// <returns>"1" Success; "2" Duplicate; "0" Fail </returns>
        string UpdateRecord();

        /// <summary>
        /// This mehtod is used to delete data to the database and returns string.
        /// </summary>
        /// <returns>"1" Success; "0" Fail </returns>
        string DeleteRecord();

        int AddedBy { get; set; }
        int ModifiedBy { get; set; }
        DateTime AddedOn { get; set; }
        DateTime ModifiedOn { get; set; }
        string IpAddress { get; set; }
        string Description { get; set; }
       
    }

}
