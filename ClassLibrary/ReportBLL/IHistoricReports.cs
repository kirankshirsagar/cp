﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrudBLL;

namespace ReportBLL
{
    public interface IHistoricReports : IReportParameters, INavigation
    {
        string Contract_ID { get; set; }
        string Request_Type { get; set; }
        string Request_generation_Date { get; set; }
        string Contract_Type { get; set; }
        string Effective_Date { get; set; }
        string Expiration_Date { get; set; }
        string Requestor_Name { get; set; }
        int? Flag { get; set; }
        int UserID { get; set; }
        List<HistoricReports> GetReport();


    }
}
