﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrudBLL;
using System.Data;

namespace ReportBLL
{
    public interface ICustomReports : ICrud, IReportParameters, INavigation
    {
        string Customer_Name { get; set; }
        string Requestor_Name { get; set; }
        string Contract_ID { get; set; }
        string Effective_Date { get; set; }
        string Expiry_Date { get; set; }
        string SelectQuery { get; set; }
        int UserID { get; set; }
        string CustomReportName { get; set; }
        string ReportFixedFields { get; set; }
        string RequestFormReportFields { get; set; }
        string ImportantDatesReportFields { get; set; }
        string KeyObligationReportFields { get; set; }
        string ReportWhereClauseFields { get; set; }
        string CustomDescription { get; set; }
        int CustomReportId { get; set; }

        string ScheduleType { get; set; }
        string ScheduleTime { get; set; }
        string ScheduleDay { get; set; }
        string ScheduleWeek { get; set; }
        string ScheduleMonth { get; set; }
        string ScheduleYear { get; set; }
        int Flag { get; set; }
        string MailId { get; set; }
        string PredefinedReportSP { get; set; }
        string IsCustomReport { get; set; }
        string RecipiantIds { get; set; }
        string RecipiantMailId { get; set; }

        string EffectiveDate { get; set; }
        string ExpiryDate { get; set; }
        string RenewalDate { get; set; }
        string RequestDate { get; set; }
        string SignatureDate { get; set; }
        string EffectiveToDate { get; set; }
        string ExpiryToDate { get; set; }
        string RenewalToDate { get; set; }
        string RequestToDate { get; set; }
        string SignatureToDate { get; set; }
        string Season { get; set; }
        string Quarter { get; set; }

        int ContractingPartyId { get; set; }
        string AnnualValue { get; set; }
        string TotalValue { get; set; }
        string AnnualValueTo { get; set; }
        string TotalValueTo { get; set; }
        int TerritoryId { get; set; }
        int ContractStatusId { get; set; }
        int AssignedDepartmentId { get; set; }
        int AssignedUserId { get; set; }
        int ClientId { get; set; }
        string Parentcompanyguarantee { get; set; }
        string Bankguarantee { get; set; }
        string Originalsignature { get; set; }

        string ChangeOfControl { get; set; }
        string EntireAgreementClause { get; set; }
        string Strictliability { get; set; }
        string ThirdPartyGuaranteeRequired { get; set; }
        string Priority { get; set; }

        List<CustomReports> GetReport();
        DataTable GetReports();
        DataTable GetCustomRecord();
        DataTable GetContractType();
        DataTable GetCustomData();
        DataTable GetCustomContractType();
        DataTable GetRecipentMailIdData();
        DataTable GetCustomerSupplierName();
        DataTable GetReportAccessId();
        int GetReportName();

    }
}
