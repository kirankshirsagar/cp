﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;

namespace ReportBLL
{
   public  class CustomerContractValueDAL : DAL
    {

       public DataTable GetFullReport()
       {
           DataTable dt = new DataTable();
           int UserID = int.Parse(System.Web.HttpContext.Current.Session[Declarations.User].ToString());
           Parameters.AddWithValue("@UserID", UserID);
           return getDataTable("CustomerContractValue_Report");
       }


       public List<CustomerContractValue> GetReport(ICustomerContractValue I)
       {
           int i = 0;
           List<CustomerContractValue> myList = new List<CustomerContractValue>();
           Parameters.AddWithValue("@PageNo", I.PageNo);
           Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
           Parameters.AddWithValue("@FromDate", I.FromDate);
           Parameters.AddWithValue("@ToDate", I.ToDate);
           Parameters.AddWithValue("@ContractTypeId", I.ContractTypeID);
           Parameters.AddWithValue("@Search", I.Search);
           Parameters.AddWithValue("@SortColumn", I.SortColumn);
           Parameters.AddWithValue("@Direction", I.Direction);
           Parameters.AddWithValue("@UserID", I.UserID);
           SqlDataReader dr = getExecuteReader("CustomerContractValue_Report");
           try
           {
               while (dr.Read())
               {
                   myList.Add(new CustomerContractValue());
                   myList[i].Customer_Name = dr["Customer Name"].ToString();
                   myList[i].Requestor_Name = dr["Requester Name"].ToString();
                   myList[i].Contract_ID = dr["Contract ID"].ToString();
                   myList[i].Effective_Date = dr["Effective Date"].ToString();
                   myList[i].Expiry_Date = dr["Expiration Date"].ToString();
                   myList[i].Contract_Value = dr["Contract Value"].ToString();
                   i = i + 1;
               }

               if (dr.NextResult())
               {
                   while (dr.Read())
                   {
                       I.TotalRecords = int.Parse(dr[0].ToString());
                   }
               }

           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
               if (dr.IsClosed != true && dr != null)
                   dr.Close();
           }
           return myList;

          
       }

    }
}
