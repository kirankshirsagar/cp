﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ReportBLL
{
   public class HistoricReports : IHistoricReports
    {
       HistoricReportsDAL obj;
       public DateTime? FromDate { get; set; }
       public DateTime? ToDate { get; set; }
       public string Search { get; set; }

       public long TotalRecords { get; set; }
       public int PageNo { get; set; }
       public int RecordsPerPage { get; set; }
       public int Direction { get; set; }
       public string SortColumn { get; set; }
       public int UserID { get; set; }
       public string Contract_ID { get; set; }
       public string Request_Type { get; set; }
       public string Request_generation_Date { get; set; }
       public string Contract_Type { get; set; }
       public string Effective_Date { get; set; }
       public string Expiration_Date { get; set; }
       public string Requestor_Name { get; set; }
       
       public int? ContractTypeID { get; set; }
       public int? Flag { get; set; } 


       public List<HistoricReports> GetReport()
       {
           obj = new HistoricReportsDAL();
           return obj.GetReport(this);
          
       }

       public DataTable GetFullReport()
       {
           obj = new HistoricReportsDAL();
           return obj.GetFullReport(this);
       }


    }
}
