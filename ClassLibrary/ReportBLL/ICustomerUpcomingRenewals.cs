﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrudBLL;

namespace ReportBLL
{
    public interface ICustomerUpcomingRenewals : IReportParameters, INavigation
    {
        
        string Customer_Name { get; set; }
        string Requestor_Name { get; set; }
        string Contract_ID { get; set; }
        string Effective_Date { get; set; }
        string Expiry_Date { get; set; }
       
        string Contract_Type { get; set; }
        string Renewal_Notification_Days { get; set; }
        string Renewal_Notification_Date { get; set; }
        int UserID { get; set; }
        List<CustomerUpcomingRenewals> GetReport();
       

    }
}
