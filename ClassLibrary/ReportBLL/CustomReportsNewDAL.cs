﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonBLL;
using System.Data;
using System.Data.SqlClient;

namespace ReportBLL
{
    class CustomReportsNewDAL : DAL
    {
        public DataTable GetFullReport()
        {
            string CustomReportId = "0";
            int i = 0;
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            if (Convert.ToString(System.Web.HttpContext.Current.Request.QueryString["ReportId"]) != "")
                CustomReportId = Convert.ToString(System.Web.HttpContext.Current.Request.QueryString["ReportId"]);

            Parameters.AddWithValue("@CustomReportId", CustomReportId);
            int UserID = int.Parse(System.Web.HttpContext.Current.Session[Declarations.User].ToString());
            Parameters.AddWithValue("@UserID", UserID);

            SqlDataReader dr = getExecuteReader("Custom_Reports");
            try
            {
                dt.Load(dr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return dt;
            //return getDataTable("Master_Reports");
        }

        public List<CustomReportsNew> GetReport(ICustomReportsNew I)
        {
            int i = 0;
            List<CustomReportsNew> myList = new List<CustomReportsNew>();
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@FromDate", I.FromDate);
            Parameters.AddWithValue("@ToDate", I.ToDate);
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeID);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            Parameters.AddWithValue("@UserID", I.UserID);
            SqlDataReader dr = getExecuteReader("Master_Reportes");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new CustomReportsNew());
                    myList[i].Customer_Name = dr["Customer Name"].ToString();
                    myList[i].Requestor_Name = dr["Requester Name"].ToString();
                    myList[i].Contract_ID = dr["Contract ID"].ToString();
                    myList[i].Effective_Date = dr["Effective Date"].ToString();
                    myList[i].Expiry_Date = dr["Expiration Date"].ToString();

                    myList[i].Contract_Type = dr["Contract Type"].ToString();
                    myList[i].Contract_Template = dr["Contract Template"].ToString();
                    myList[i].Client_Name = dr["Client Name"].ToString();
                    myList[i].Address = dr["Address"].ToString();
                    myList[i].State_Name = dr["State Name"].ToString();
                    myList[i].City_Name = dr["City Name"].ToString();
                    myList[i].Country_Name = dr["Country Name"].ToString();
                    myList[i].Pin_Code = dr["Pin Code"].ToString();
                    myList[i].Request_Description = dr["Request Description"].ToString();
                    myList[i].Deadline_Date = dr["Deadline Date"].ToString();
                    myList[i].Department_Name = dr["Department Name"].ToString();
                    myList[i].Assign_User = dr["Assign User"].ToString();
                    myList[i].Is_Approve = dr["Is Approve"].ToString();
                    myList[i].Estimated_Value = dr["Estimated Value"].ToString();
                    myList[i].Contact_Number = dr["Contact Number"].ToString();
                    myList[i].Email_Id = dr["Email Id"].ToString();
                    myList[i].Status = dr["Status"].ToString();

                    myList[i].Liability_Cap = dr["Liability Cap"].ToString();
                    myList[i].Indemnity = dr["Indemnity"].ToString();
                    myList[i].Is_Change_of_Control = dr["Is Change of Control"].ToString();
                    myList[i].Is_Agreement_Clause = dr["Is Agreement Clause"].ToString();
                    myList[i].Is_Strictliability = dr["Is Strictliability"].ToString();
                    myList[i].Strictliability = dr["Strictliability"].ToString();
                    myList[i].Is_Guarantee = dr["Is Guarantee"].ToString();
                    myList[i].Insurance = dr["Insurance"].ToString();
                    myList[i].Is_Termination_Convenience = dr["Is Termination Convenience"].ToString();
                    myList[i].Termination_Description = dr["Termination Description"].ToString();
                    myList[i].Is_Termination_material = dr["Is Termination material"].ToString();
                    myList[i].Is_Termination_insolvency = dr["Is Termination insolvency"].ToString();
                    myList[i].Is_Terminationin_Change_of_control = dr["Is Terminationin Change of control"].ToString();
                    myList[i].Is_Termination_other_causes = dr["Is Termination other causes"].ToString();
                    myList[i].Assignment_Novation = dr["Assignment Novation"].ToString();
                    myList[i].Others = dr["Others"].ToString();


                    myList[i].Contracting_Party = dr["Contracting Party"].ToString();
                    myList[i].Is_Customer = dr["Is Customer"].ToString();
                    myList[i].Street2 = dr["Street2"].ToString();
                    myList[i].Bulk_Import = dr["Bulk Import"].ToString();
                    myList[i].Is_Bulk_Import = dr["Is Bulk Import"].ToString();
                    myList[i].Is_Doc_Sign_Completed = dr["Is Doc Sign Completed"].ToString();
                    myList[i].Contract_Term = dr["Contract Term"].ToString();
                    myList[i].Contract_Value = dr["Contract Value"].ToString();


                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;


        }

        public DataTable GetReports(CustomReportsNew I)
        {
            int i = 0;
            List<CustomReportsNew> myList = new List<CustomReportsNew>();
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            SqlDataReader dr = getExecuteReader("CustomMaster_ColumnReports");

            try
            {
                dt.Load(dr);

                List<DataRow> rowsToDelete = new List<DataRow>();
                foreach (DataRow row in dt.Rows)
                {
                    if (row["Name"].ToString() == "ClientId" || row["Name"].ToString() == "CityId" || row["Name"].ToString() == "StateId"
                        || row["Name"].ToString() == "ContractExpiryDate" || row["Name"].ToString() == "Addedon" || row["Name"].ToString() == "Modifiedon"
                        || row["Name"].ToString() == "Modifiedby" || row["Name"].ToString() == "IP" || row["Name"].ToString() == "ExpirationAlert30daysUserID1"
                        || row["Name"].ToString() == "ExpirationAlert30daysUserID2" || row["Name"].ToString() == "ExpirationAlert7days"
                        || row["Name"].ToString() == "ExpirationAlert7daysUserID1" || row["Name"].ToString() == "ExpirationAlert7daysUserID2"
                        || row["Name"].ToString() == "ExpirationAlert24Hrs" || row["Name"].ToString() == "ExpirationAlert24HrsUserID1"
                        || row["Name"].ToString() == "ExpirationAlert24HrsUserID2" || row["Name"].ToString() == "RenewalAlert30daysUserID1"
                        || row["Name"].ToString() == "RenewalAlert30daysUserID2" || row["Name"].ToString() == "RenewalAlert7days"
                        || row["Name"].ToString() == "RenewalAlert7daysUserID1" || row["Name"].ToString() == "RenewalAlert7daysUserID2"
                        || row["Name"].ToString() == "RenewalAlert24Hrs" || row["Name"].ToString() == "RenewalAlert24HrsUserID1"
                        || row["Name"].ToString() == "RenewalAlert24HrsUserID2" || row["Name"].ToString() == "ContractingPartyName"
                        || row["Name"].ToString() == "ExpirationAlert120days" || row["Name"].ToString() == "ExpirationAlert120daysUserID1"
                        || row["Name"].ToString() == "ExpirationAlert120daysUserID2" || row["Name"].ToString() == "ExpirationAlert90days"
                        || row["Name"].ToString() == "ExpirationAlert90daysUserID1" || row["Name"].ToString() == "ExpirationAlert90daysUserID2"
                        || row["Name"].ToString() == "ExpirationAlert60days" || row["Name"].ToString() == "ExpirationAlert60daysUserID1"
                        || row["Name"].ToString() == "ExpirationAlert60daysUserID2" || row["Name"].ToString() == "RenewalAlert120days"
                        || row["Name"].ToString() == "RenewalAlert120daysUserID1" || row["Name"].ToString() == "RenewalAlert120daysUserID2"
                        || row["Name"].ToString() == "RenewalAlert90days" || row["Name"].ToString() == "RenewalAlert90daysUserID1"
                        || row["Name"].ToString() == "RenewalAlert90daysUserID2" || row["Name"].ToString() == "RenewalAlert60days"
                        || row["Name"].ToString() == "RenewalAlert60daysUserID1" || row["Name"].ToString() == "RenewalAlert60daysUserID2"
                        || row["Name"].ToString() == "RequestContractID" || row["Name"].ToString() == "isCustomer" || row["Name"].ToString() == "Street2"
                        || row["Name"].ToString() == "Addedby" || row["Name"].ToString() == "BulkImportStagingId" || row["Name"].ToString() == "CountryName"
                        || row["Name"].ToString() == "RequestTypeId" || row["Name"].ToString() == "RequestId"
                        //  
                        )
                    {
                        rowsToDelete.Add(row);
                    }
                }
                foreach (DataRow row in rowsToDelete)
                {
                    dt.Rows.Remove(row);
                }
                dt.AcceptChanges();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return dt;
        }

        public DataTable GetCustomRecord(ICustomReportsNew I)
        {
            int i = 0;
            List<CustomReportsNew> myList = new List<CustomReportsNew>();
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@CustomReportId", I.CustomReportId);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            Parameters.AddWithValue("@UserID", I.UserID);

            SqlDataReader dr = getExecuteReader("Custom_Reports");

            try
            {
                dt.Load(dr);
                I.TotalRecords = int.Parse(dt.Rows[0]["Maxcount"].ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return dt;
        }

        public DataTable GetCustomiesRecord(ICustomReportsNew I)
        {
            int i = 0;
            List<CustomReportsNew> myList = new List<CustomReportsNew>();
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            Parameters.AddWithValue("@CustomReportId", I.CustomReportId);
            SqlDataReader dr = getExecuteReader("CustomiesSelect_Reports");

            try
            {
                dt.Load(dr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return dt;
        }

        public DataTable GetCustomFieldDetails(ICustomReportsNew I)
        {
            int i = 0;
            List<CustomReportsNew> myList = new List<CustomReportsNew>();
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            Parameters.AddWithValue("@CustomReportName", I.CustomReportName);
            SqlDataReader dr = getExecuteReader("CustomField_Details");

            try
            {
                dt.Load(dr);
                //I.TotalRecords = int.Parse(dt.Rows[0]["Maxcount"].ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return dt;
        }

        public DataTable GetScheduleRecord(ICustomReportsNew I)
        {
            int i = 0;
            List<CustomReportsNew> myList = new List<CustomReportsNew>();
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            Parameters.AddWithValue("@Status", 0);
            Parameters.AddWithValue("@Flag", 5);
            Parameters.AddWithValue("@CustomReportId", I.CustomReportId);
            SqlDataReader dr = getExecuteReader("CustomReportAddUpdate");
            //string rvalue = getExcuteQuery("CustomReportAddUpdate", "@Status");
            try
            {
                dt.Load(dr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return dt;
        }

        public DataTable GetPredefineScheduleRecord(ICustomReportsNew I)
        {
            int i = 0;
            List<CustomReportsNew> myList = new List<CustomReportsNew>();
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            Parameters.AddWithValue("@Status", 0);
            Parameters.AddWithValue("@Flag", 7);
            Parameters.AddWithValue("@CustomReportId", I.CustomReportId);
            SqlDataReader dr = getExecuteReader("CustomReportAddUpdate");
            //string rvalue = getExcuteQuery("CustomReportAddUpdate", "@Status");
            try
            {
                dt.Load(dr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return dt;
        }

        public DataSet GetDSCustomiesRecords(ICustomReportsNew I)
        {
            List<CustomReportsNew> myList = new List<CustomReportsNew>();
            Parameters.AddWithValue("@CustomReportId", I.CustomReportId);
            DataSet dr = getDataSet("CustomiesSelect_Reports");
            return dr;
        }


    }
}
