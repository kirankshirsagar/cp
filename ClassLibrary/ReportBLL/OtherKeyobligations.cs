﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ReportBLL
{
    public class OtherKeyobligations : IOtherKeyobligations
    {
        OtherKeyobligationsDAL obj;
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Search { get; set; }

        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public int Direction { get; set; }
        public string SortColumn { get; set; }

        public int UserID { get; set; }
        public string Contract_ID { get; set; }
        public string Contract_Type { get; set; }
        public string Effective_Date { get; set; }
        public string Expiry_Date { get; set; }
        public string Customer_Name { get; set; }
        public string Requestor_Name { get; set; }
        public string Liability_Cap { get; set; }
        public string Indemnity { get; set; }
        public string Change_of_Control { get; set; }
        public string Entire_agreement { get; set; }
        public string Strict_liability { get; set; }
        public string Third_party_guarantee { get; set; }
        public string Insurance { get; set; }
        public string Termination { get; set; }
        public string Assignment_Novation_Subcontracting { get; set; }
        public string Others { get; set; }
        public int? ContractTypeID { get; set; }

        public string IsIndirectConsequentialLosses { get; set; }
        public string IndirectConsequentialLosses { get; set; }
        public string IsIndemnity { get; set; }
        public string IsWarranties { get; set; }
        public string Warranties { get; set; }
        public string EntireAgreementClause { get; set; }
        public string ThirdPartyGuaranteeRequired { get; set; }
        public string IsForceMajeure { get; set; }
        public string ForceMajeure { get; set; }
        public string IsSetOffRights { get; set; }
        public string SetOffRights { get; set; }
        public string IsGoverningLaw { get; set; }
        public string GoverningLaw { get; set; }

        public List<OtherKeyobligations> GetReport()
        {
            obj = new OtherKeyobligationsDAL();
            return obj.GetReport(this);

        }

        public DataTable GetFullReport()
        {
            obj = new OtherKeyobligationsDAL();
            return obj.GetFullReport();
        }


    }
}
