﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrudBLL;

namespace ReportBLL
{
    public interface IOtherKeyobligations : IReportParameters, INavigation
    {
        string Contract_ID { get; set; }
        string Contract_Type { get; set; }
        string Effective_Date { get; set; }
        string Expiry_Date { get; set; }
        string Customer_Name { get; set; }
        string Requestor_Name { get; set; }

        string Liability_Cap { get; set; }
        string Indemnity { get; set; }
        string Change_of_Control { get; set; }

        string Entire_agreement { get; set; }
        string Strict_liability { get; set; }
        string Third_party_guarantee { get; set; }

        string Insurance { get; set; }
        string Termination { get; set; }
        string Assignment_Novation_Subcontracting { get; set; }
        int UserID { get; set; }
        string Others { get; set; }

        string IsIndirectConsequentialLosses { get; set; }
        string IndirectConsequentialLosses { get; set; }
        string IsIndemnity { get; set; }
        string IsWarranties { get; set; }
        string Warranties { get; set; }
        string EntireAgreementClause { get; set; }
        string ThirdPartyGuaranteeRequired { get; set; }
        string IsForceMajeure { get; set; }
        string ForceMajeure { get; set; }
        string IsSetOffRights { get; set; }
        string SetOffRights { get; set; }
        string IsGoverningLaw { get; set; }
        string GoverningLaw { get; set; }

        List<OtherKeyobligations> GetReport();
    }
}
