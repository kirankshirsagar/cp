﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AuditTrailBLL;

namespace ReportBLL
{
    public static class FactoryReports
    {
        public static ICustomerActiveContracts CustomerActiveContractsDetail()
        {
            return new CustomerActiveContracts();
        }

        public static IOtherActiveContracts OtherActiveContractsDetail()
        {
            return new OtherActiveContracts();
        }

        public static IMasterReports MasterReportDetail()
        {
            return new MasterReports();
        }

        public static ICustomReports CustomReportDetail()
        {
            return new CustomReports();
        }

        public static ICustomReportsNew CustomReportDetails()
        {
            return new CustomReportsNew();
        }

        public static ISupplierActiveContracts SupplierActiveContractsDetail()
        {
            return new SupplierActiveContracts();
        }

        public static ICustomerExpiredTerminatedContracts CustomerExpiredTerminatedContractsDetail()
        {
            return new CustomerExpiredTerminatedContracts();
        }

        public static IOtherExpiredTerminatedContracts OtherExpiredTerminatedContractsDetail()
        {
            return new OtherExpiredTerminatedContracts();
        }

        public static ISupplierExpiredTerminatedContracts SupplierExpiredTerminatedContractsDetail()
        {
            return new SupplierExpiredTerminatedContracts();
        }

        public static ICustomerContractValue CustomerContractValueDetail()
        {
            return new CustomerContractValue();
        }
        public static IOtherContractValue OtherContractValueDetail()
        {
            return new OtherContractValue();
        }
        public static ISupplierContractValue SupplierContractValueDetail()
        {
            return new SupplierContractValue();
        }


        public static CustomerGeography CustomerGeographyContractDetail()
        {
            return new CustomerGeography();
        }

        public static OtherGeography OtherGeographyContractDetail()
        {
            return new OtherGeography();
        }

        public static ISupplierGeography SupplierGeographyDetail()
        {
            return new SupplierGeography();
        }

        public static CustomerTypeValueGeography CustomerTypeValueGeographyContractDetail()
        {
            return new CustomerTypeValueGeography();
        }

        public static OtherTypeValueGeography OtherTypeValueGeographyContractDetail()
        {
            return new OtherTypeValueGeography();
        }

        public static ISupplierTypeValueGeography SupplierTypeValueGeographyDetail()
        {
            return new SupplierTypeValueGeography();
        }

        public static ICustomerUpcomingRenewals CustomerUpcomingRenewalsDetail()
        {
            return new CustomerUpcomingRenewals();
        }

        public static IOtherUpcomingRenewals OtherUpcomingRenewalsDetail()
        {
            return new OtherUpcomingRenewals();
        }

        public static ISupplierUpcomingRenewals SupplierUpcomingRenewalsDetail()
        {
            return new SupplierUpcomingRenewals();
        }

        public static ICustomerContractTypeAnalysis CustomerContractTypeAnalysis()
        {
            return new CustomerContractTypeAnalysis();
        }

        public static IOtherContractTypeAnalysis OtherContractTypeAnalysis()
        {
            return new OtherContractTypeAnalysis();
        }

        public static ISupplierContractTypeAnalysis SupplierContractTypeAnalysis()
        {
            return new SupplierContractTypeAnalysis();
        }

        public static ICustomerKeyobligations CustomerKeyobligations()
        {
            return new CustomerKeyobligations();
        }

        public static IOtherKeyobligations OtherKeyobligations()
        {
            return new OtherKeyobligations();
        }


        public static ISupplierKeyobligations SupplierKeyobligations()
        {
            return new SupplierKeyobligations();
        }

        public static ICustomerInprocess CustomerInprocess()
        {
            return new CustomerInprocess();
        }

        public static IOtherInprocess OtherInprocess()
        {
            return new OtherInprocess();
        }

        public static ISupplierInprocess SupplierInprocess()
        {
            return new SupplierInprocess();
        }

        public static ICustomerChangeControl CustomerChangeControl()
        {
            return new CustomerChangeControl();
        }

        public static IOtherChangeControl OtherChangeControl()
        {
            return new OtherChangeControl();
        }

        public static ISupplierChangeControl SupplierChangeControl()
        {
            return new SupplierChangeControl();
        }

        public static ICustomerClientInprocess CustomerClientInprocess()
        {
            return new CustomerClientInprocess();
        }

        public static ISupplierClientInprocess SupplierClientInprocess()
        {
            return new SupplierClientInprocess();
        }

        public static ICustomerActivieContactValue CustomerActivieContactValue()
        {
            return new CustomerActivieContactValue();
        }

        public static ISupplierActivieContactValue SupplierActivieContactValue()
        {
            return new SupplierActivieContactValue();
        }

        public static ICustomerRenewalDate CustomerRenewalDate()
        {
            return new CustomerRenewalDate();
        }

        public static ISupplierRenewalDate SupplierRenewalDate()
        {
            return new SupplierRenewalDate();
        }

        public static ICustomerDepartment CustomerDepartment()
        {
            return new CustomerDepartment();
        }

        public static IOtherDepartment OtherDepartment()
        {
            return new OtherDepartment();
        }

        public static ISupplierDepartment SupplierDepartment()
        {
            return new SupplierDepartment();
        }

        public static ICustomerRequesterStatus CustomerRequesterStatus()
        {
            return new CustomerRequesterStatus();
        }

        public static ICustomerImportantDates CustomerImportantDates()
        {
            return new CustomerImportantDates();
        }

        public static IOtherImportantDates OtherImportantDates()
        {
            return new OtherImportantDates();
        }

        public static ISupplierImportantDates SupplierImportantDates()
        {
            return new SupplierImportantDates();
        }

        public static ICustomerTerminatedContracts CustomerTerminatedContracts()
        {
            return new CustomerTerminatedContracts();
        }

        public static IOtherTerminatedContracts OtherTerminatedContracts()
        {
            return new OtherTerminatedContracts();
        }

        public static ISupplierTerminatedContracts SupplierTerminatedContracts()
        {
            return new SupplierTerminatedContracts();
        }

        public static IHistoricReports HistoricReports()
        {
            return new HistoricReports();
        }

        public static ICustomerStatusAnalysis CustomerStatusAnalysis()
        {
            return new CustomerStatusAnalysis();
        }

        public static ICustomerContractsTerminated CustomerContractsTerminated()
        {
            return new CustomerContractsTerminated();
        }

        public static IOtherContractsTerminated OtherContractsTerminated()
        {
            return new OtherContractsTerminated();
        }

        public static ISupplierContractsTerminated SupplierContractsTerminated()
        {
            return new SupplierContractsTerminated();
        }

        public static IAuditTrail AuditTrailDetails()
        {
            return new AuditTrail();
        }
    }
}
