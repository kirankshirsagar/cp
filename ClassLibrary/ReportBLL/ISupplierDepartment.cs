﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrudBLL;

namespace ReportBLL
{
    public interface ISupplierDepartment : IReportParameters, INavigation
    {
        string Contract_ID { get; set; }
        string Contract_Type { get; set; }
        string Effective_Date { get; set; }
        string Expiration_Date { get; set; }
        string Requestor_Name { get; set; }
        string Contract_Value { get; set; }
        string Supplier_Name { get; set; }
        string Department { get; set; }
        int UserID { get; set; }
        List<SupplierDepartment> GetReport();
    }
}
