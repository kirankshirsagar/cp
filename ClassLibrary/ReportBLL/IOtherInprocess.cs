﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrudBLL;

namespace ReportBLL
{
    public interface IOtherInprocess : IReportParameters, INavigation
    {
        string Contract_ID { get; set; }
        string Contract_Type { get; set; }
        string Customer_Name { get; set; }
        string Deadline_Date { get; set; }
        string Contract_Status { get; set; }
        string Requestor_Name { get; set; }
        string Assigned_To { get; set; }
        int UserID { get; set; }
        List<OtherInprocess> GetReport();
    }
}
