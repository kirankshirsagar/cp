﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrudBLL;

namespace ReportBLL
{
    public interface ISupplierContractTypeAnalysis : IReportParameters, INavigation
    {
        
        string Supplier_Name { get; set; }
        string Requestor_Name { get; set; }
        string Contract_ID { get; set; }
        string Contract_Type { get; set; }
        string Effective_Date { get; set; }
        string Expiry_Date { get; set; }
        List<SupplierContractTypeAnalysis>GetReport();
        int UserID { get; set; }
    }
}
