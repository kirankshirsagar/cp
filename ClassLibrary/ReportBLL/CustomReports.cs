﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CrudBLL;

namespace ReportBLL
{
    public class CustomReports : ICustomReports
    {
        CustomReportDAL obj;
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Search { get; set; }
        public string SelectQuery { get; set; }

        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public int Direction { get; set; }
        public string SortColumn { get; set; }

        public string Customer_Name { get; set; }
        public string Requestor_Name { get; set; }
        public string Contract_ID { get; set; }
        public string Effective_Date { get; set; }
        public string Expiry_Date { get; set; }
        public int? ContractTypeID { get; set; }
        public int UserID { get; set; }
        public string Contract_Type { get; set; }
        public string Contract_Template { get; set; }
        public string Client_Name { get; set; }
        public string Address { get; set; }
        public string State_Name { get; set; }
        public string City_Name { get; set; }
        public string Country_Name { get; set; }
        public string Pin_Code { get; set; }
        public string Request_Description { get; set; }
        public string Deadline_Date { get; set; }
        public string Department_Name { get; set; }
        public string Assign_User { get; set; }
        public string Is_Approve { get; set; }
        public string Estimated_Value { get; set; }
        public string Contact_Number { get; set; }
        public string Email_Id { get; set; }
        public string Status { get; set; }

        public string Liability_Cap { get; set; }
        public string Indemnity { get; set; }
        public string Is_Change_of_Control { get; set; }
        public string Is_Agreement_Clause { get; set; }
        public string Is_Strictliability { get; set; }
        public string Is_Guarantee { get; set; }
        public string Insurance { get; set; }
        public string Is_Termination_Convenience { get; set; }
        public string Termination_Description { get; set; }
        public string Is_Termination_material { get; set; }
        public string Is_Termination_insolvency { get; set; }
        public string Is_Terminationin_Change_of_control { get; set; }
        public string Is_Termination_other_causes { get; set; }
        public string Assignment_Novation { get; set; }
        public string Others { get; set; }
        public int ClientId { get; set; }
        public string Contracting_Party { get; set; }
        public string Is_Customer { get; set; }
        public string Street2 { get; set; }
        public string Bulk_Import { get; set; }
        public string Is_Bulk_Import { get; set; }
        public string Is_Doc_Sign_Completed { get; set; }
        public string Contract_Term { get; set; }
        public string Contract_Value { get; set; }

        public string CustomReportName { get; set; }
        public string ReportFixedFields { get; set; }
        public string RequestFormReportFields { get; set; }
        public string ImportantDatesReportFields { get; set; }
        public string KeyObligationReportFields { get; set; }
        public string ReportWhereClauseFields { get; set; }
        public int AddedBy { get; set; }
        public int ModifiedBy { get; set; }
        public string IpAddress { get; set; }
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Description { get; set; }
        public int CustomReportId { get; set; }
        public string CustomDescription { get; set; }

        public string ScheduleType { get; set; }
        public string ScheduleTime { get; set; }
        public string ScheduleDay { get; set; }
        public string ScheduleWeek { get; set; }
        public string ScheduleMonth { get; set; }
        public string ScheduleYear { get; set; }
        public int Flag { get; set; }
        public string MailId { get; set; }
        public string PredefinedReportSP { get; set; }
        public string IsCustomReport { get; set; }
        public string RecipiantIds { get; set; }
        public string RecipiantMailId { get; set; }

        public string EffectiveDate { get; set; }
        public string ExpiryDate { get; set; }
        public string RenewalDate { get; set; }
        public string RequestDate { get; set; }
        public string SignatureDate { get; set; }
        public string EffectiveToDate { get; set; }
        public string ExpiryToDate { get; set; }
        public string RenewalToDate { get; set; }
        public string RequestToDate { get; set; }
        public string SignatureToDate { get; set; }
        public string Season { get; set; }
        public string Quarter { get; set; }

        public int ContractingPartyId { get; set; }
        public string AnnualValue { get; set; }
        public string TotalValue { get; set; }
        public string AnnualValueTo { get; set; }
        public string TotalValueTo { get; set; }
        public int TerritoryId { get; set; }
        public int ContractStatusId { get; set; }
        public int AssignedDepartmentId { get; set; }
        public int AssignedUserId { get; set; }
        public string Parentcompanyguarantee { get; set; }
        public string Bankguarantee { get; set; }
        public string Originalsignature { get; set; }
        public string ChangeOfControl { get; set; }
        public string EntireAgreementClause { get; set; }
        public string Strictliability { get; set; }
        public string ThirdPartyGuaranteeRequired { get; set; }
        public string Priority { get; set; }

        public List<CustomReports> GetReport()
        {
            obj = new CustomReportDAL();
            return obj.GetReport(this);

        }

        public DataTable GetFullReport()
        {
            obj = new CustomReportDAL();
            return obj.GetFullReport();
        }

        public DataTable GetReports()
        {
            obj = new CustomReportDAL();
            return obj.GetReports(this);
        }

        public DataTable GetContractType()
        {
            obj = new CustomReportDAL();
            return obj.GetContractType(this);
        }

        public DataTable GetCustomContractType()
        {
            obj = new CustomReportDAL();
            return obj.GetCustomContractType(this);
        }

        public DataTable GetCustomRecord()
        {
            obj = new CustomReportDAL();
            return obj.GetCustomRecord(this);
        }

        public DataTable GetCustomData()
        {
            obj = new CustomReportDAL();
            return obj.GetCustomData(this);
        }

        public string InsertRecord()
        {
            try
            {
                obj = new CustomReportDAL();
                return obj.InsertRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DeleteRecord()
        {
            try
            {
                obj = new CustomReportDAL();
                return obj.DeleteRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateRecord()
        {
            try
            {
                obj = new CustomReportDAL();
                return obj.UpdateRecord(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetRecipentMailIdData()
        {
            obj = new CustomReportDAL();
            return obj.GetRecipentMailIdData(this);
        }

        public int GetReportName()
        {
            try
            {
                obj = new CustomReportDAL();
                return obj.GetReportName(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetCustomerSupplierName()
        {
            obj = new CustomReportDAL();
            return obj.GetCustomerSupplierName(this);
        }

        public DataTable GetReportAccessId()
        {
            obj = new CustomReportDAL();
            return obj.GetReportAccessId(this);
        }

    }
}
