﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ReportBLL
{
    public class CustomReportsNew : ICustomReportsNew
    {
        CustomReportsNewDAL obj;
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Search { get; set; }
        public string SelectQuery { get; set; }

        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public int Direction { get; set; }
        public string SortColumn { get; set; }

        public string Customer_Name { get; set; }
        public string Requestor_Name { get; set; }
        public string Contract_ID { get; set; }
        public string Effective_Date { get; set; }
        public string Expiry_Date { get; set; }
        public int? ContractTypeID { get; set; }
        public int UserID { get; set; }
        public string Contract_Type { get; set; }
        public string Contract_Template { get; set; }
        public string Client_Name { get; set; }
        public string Address { get; set; }
        public string State_Name { get; set; }
        public string City_Name { get; set; }
        public string Country_Name { get; set; }
        public string Pin_Code { get; set; }
        public string Request_Description { get; set; }
        public string Deadline_Date { get; set; }
        public string Department_Name { get; set; }
        public string Assign_User { get; set; }
        public string Is_Approve { get; set; }
        public string Estimated_Value { get; set; }
        public string Contact_Number { get; set; }
        public string Email_Id { get; set; }
        public string Status { get; set; }

        public string Liability_Cap { get; set; }
        public string Indemnity { get; set; }
        public string Is_Change_of_Control { get; set; }
        public string Is_Agreement_Clause { get; set; }
        public string Is_Strictliability { get; set; }
        public string Strictliability { get; set; }
        public string Is_Guarantee { get; set; }
        public string Insurance { get; set; }
        public string Is_Termination_Convenience { get; set; }
        public string Termination_Description { get; set; }
        public string Is_Termination_material { get; set; }
        public string Is_Termination_insolvency { get; set; }
        public string Is_Terminationin_Change_of_control { get; set; }
        public string Is_Termination_other_causes { get; set; }
        public string Assignment_Novation { get; set; }
        public string Others { get; set; }

        public string Contracting_Party { get; set; }
        public string Is_Customer { get; set; }
        public string Street2 { get; set; }
        public string Bulk_Import { get; set; }
        public string Is_Bulk_Import { get; set; }
        public string Is_Doc_Sign_Completed { get; set; }
        public string Contract_Term { get; set; }
        public string Contract_Value { get; set; }

        public string CustomReportName { get; set; }
        public string ReportFixedFields { get; set; }
        public string RequestFormReportFields { get; set; }
        public string ImportantDatesReportFields { get; set; }
        public string KeyObligationReportFields { get; set; }
        public string ReportWhereClauseFields { get; set; }
        public int AddedBy { get; set; }
        public int ModifiedBy { get; set; }
        public string IpAddress { get; set; }
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Description { get; set; }
        public int CustomReportId { get; set; }

        public List<CustomReportsNew> GetReport()
        {
            obj = new CustomReportsNewDAL();
            return obj.GetReport(this);

        }

        public DataTable GetFullReport()
        {
            obj = new CustomReportsNewDAL();
            return obj.GetFullReport();
        }

        public DataTable GetReports()
        {
            obj = new CustomReportsNewDAL();
            return obj.GetReports(this);
        }

        public DataTable GetCustomRecord()
        {
            obj = new CustomReportsNewDAL();
            return obj.GetCustomRecord(this);
        }

        public DataTable GetCustomFieldDetails()
        {
            obj = new CustomReportsNewDAL();
            return obj.GetCustomFieldDetails(this);
        }

        public DataTable GetCustomiesRecord()
        {
            obj = new CustomReportsNewDAL();
            return obj.GetCustomiesRecord(this);
        }

        public DataTable GetScheduleRecord() {
            obj = new CustomReportsNewDAL();
            return obj.GetScheduleRecord(this);
        }

        public DataTable GetPredefineScheduleRecord()
        {
            obj = new CustomReportsNewDAL();
            return obj.GetPredefineScheduleRecord(this);
        }

        public DataSet GetDSCustomiesRecords()
        {
            obj = new CustomReportsNewDAL();
            return obj.GetDSCustomiesRecords(this);
        }
    }
}
