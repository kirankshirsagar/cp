﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrudBLL;

namespace ReportBLL
{
    public interface IOtherExpiredTerminatedContracts : IReportParameters, INavigation
    {
        string Customer_Name { get; set; }
        string Requestor_Name { get; set; }
        string Contract_ID { get; set; }
        string Effective_Date { get; set; }
        string Expiry_Date { get; set; }
        List<OtherExpiredTerminatedContracts> GetReport();
        int UserID { get; set; }
    }
}
