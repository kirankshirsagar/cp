﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrudBLL;

namespace ReportBLL
{
    public interface ISupplierTerminatedContracts : IReportParameters, INavigation
    {

        string Supplier_Name { get; set; }
        string Requestor_Name { get; set; }
        string Contract_ID { get; set; }
        string Contract_Type { get; set; }
        string Effective_Date { get; set; }
        string Expiration_Date { get; set; }
        string Termination_Request_Date { get; set; }
        int UserID { get; set; }
        List<SupplierTerminatedContracts> GetReport();
    }
}
