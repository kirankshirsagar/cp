﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ReportBLL
{
    public interface IReportParameters
    {
        DateTime? FromDate { get; set; }
        DateTime? ToDate { get; set; }
        string Search { get; set; }
        DataTable GetFullReport();
        int? ContractTypeID { get; set; } 
    }
}
