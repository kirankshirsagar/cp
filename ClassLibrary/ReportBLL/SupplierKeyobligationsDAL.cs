﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;

namespace ReportBLL
{
    public class SupplierKeyobligationsDAL : DAL
    {

       public DataTable GetFullReport()
       {
           DataTable dt = new DataTable();
           int UserID = int.Parse(System.Web.HttpContext.Current.Session[Declarations.User].ToString());
           Parameters.AddWithValue("@UserID", UserID);
           return getDataTable("SupplierKeyobligations_Report");
       }


       public List<SupplierKeyobligations> GetReport(ISupplierKeyobligations I)
       {
           int i = 0;
           List<SupplierKeyobligations> myList = new List<SupplierKeyobligations>();
           Parameters.AddWithValue("@PageNo", I.PageNo);
           Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
           Parameters.AddWithValue("@FromDate", I.FromDate);
           Parameters.AddWithValue("@ToDate", I.ToDate);
           Parameters.AddWithValue("@ContractTypeId", I.ContractTypeID);
           Parameters.AddWithValue("@Search", I.Search);
           Parameters.AddWithValue("@SortColumn", I.SortColumn);
           Parameters.AddWithValue("@Direction", I.Direction);
           Parameters.AddWithValue("@UserID", I.UserID);
           SqlDataReader dr = getExecuteReader("SupplierKeyobligations_Report");
           try
           {
               while (dr.Read())
               {
                   myList.Add(new SupplierKeyobligations());
                   myList[i].Contract_ID = dr["Contract ID"].ToString();
                   myList[i].Contract_Type = dr["Contract Type"].ToString();
                   myList[i].Effective_Date = dr["Effective Date"].ToString();
                   myList[i].Expiry_Date = dr["Expiration Date"].ToString();
                   myList[i].Supplier_Name = dr["Supplier Name"].ToString();
                   myList[i].Requestor_Name = dr["Requester Name"].ToString();

                   myList[i].Liability_Cap = dr["Liability Cap"].ToString();
                   myList[i].Indemnity = dr["Indemnity"].ToString();
                   myList[i].Change_of_Control = dr["Change Of Control"].ToString();
                   myList[i].Entire_agreement = dr["Entire Agreement"].ToString();
                   myList[i].Strict_liability = dr["Strict Liability"].ToString();
                   myList[i].Third_party_guarantee = dr["Third Party Guarantee"].ToString();

                   myList[i].Insurance = dr["Insurance"].ToString();
                   myList[i].Termination = dr["Termination"].ToString();
                   myList[i].Assignment_Novation_Subcontracting = dr["Assignment/Novation/Subcontracting"].ToString();
                   myList[i].Others = dr["Others"].ToString();

                   myList[i].IsIndirectConsequentialLosses = dr["IsIndirectConsequentialLosses"].ToString();
                   myList[i].IndirectConsequentialLosses = dr["IndirectConsequentialLosses"].ToString();
                   myList[i].IsIndemnity = dr["IsIndemnity"].ToString();
                   myList[i].Indemnity = dr["Indemnity"].ToString();
                   myList[i].IsWarranties = dr["IsWarranties"].ToString();
                   myList[i].Warranties = dr["Warranties"].ToString();
                   myList[i].IsForceMajeure = dr["IsForceMajeure"].ToString();
                   myList[i].ForceMajeure = dr["ForceMajeure"].ToString();
                   myList[i].IsSetOffRights = dr["IsSetOffRights"].ToString();
                   myList[i].SetOffRights = dr["SetOffRights"].ToString();
                   myList[i].IsGoverningLaw = dr["IsGoverningLaw"].ToString();
                   myList[i].GoverningLaw = dr["GoverningLaw"].ToString();

                   i = i + 1;
               }

               if (dr.NextResult())
               {
                   while (dr.Read())
                   {
                       I.TotalRecords = int.Parse(dr[0].ToString());
                   }
               }

           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
               if (dr.IsClosed != true && dr != null)
                   dr.Close();
           }
           return myList;

          
       }

    }
}
