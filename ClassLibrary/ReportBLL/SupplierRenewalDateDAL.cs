﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;

namespace ReportBLL
{
    public class SupplierRenewalDateDAL : DAL
    {

       public DataTable GetFullReport()
       {
           DataTable dt = new DataTable();
           int UserID = int.Parse(System.Web.HttpContext.Current.Session[Declarations.User].ToString());
           Parameters.AddWithValue("@UserID", UserID);
           return getDataTable("SupplierRenewalDate_Report");
       }


       public List<SupplierRenewalDate> GetReport(ISupplierRenewalDate I)
       {
           int i = 0;
           List<SupplierRenewalDate> myList = new List<SupplierRenewalDate>();
           Parameters.AddWithValue("@PageNo", I.PageNo);
           Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
           Parameters.AddWithValue("@FromDate", I.FromDate);
           Parameters.AddWithValue("@ToDate", I.ToDate);
           Parameters.AddWithValue("@ContractTypeId", I.ContractTypeID);
           Parameters.AddWithValue("@Search", I.Search);
           Parameters.AddWithValue("@SortColumn", I.SortColumn);
           Parameters.AddWithValue("@Direction", I.Direction);
           Parameters.AddWithValue("@UserID", I.UserID);
           SqlDataReader dr = getExecuteReader("SupplierRenewalDate_Report");
           try
           {
               while (dr.Read())
               {
                   myList.Add(new SupplierRenewalDate());
                   myList[i].Contract_ID = dr["Contract ID"].ToString();
                   myList[i].Contract_Type = dr["Contract Type"].ToString();
                   myList[i].Effective_Date = dr["Effective Date"].ToString();
                   myList[i].Expiration_Date = dr["Expiration Date"].ToString();
                   myList[i].Requestor_Name = dr["Requester Name"].ToString();
                   myList[i].Contract_Value = dr["Contract Value"].ToString();
                   myList[i].Supplier_Name = dr["Supplier Name"].ToString();
                   myList[i].Renewal_Notification_Date = dr["Renewal Notification Date"].ToString(); 
                   myList[i].Assigned_To = dr["Assigned To"].ToString();
                   i = i + 1;
               }

               if (dr.NextResult())
               {
                   while (dr.Read())
                   {
                       I.TotalRecords = int.Parse(dr[0].ToString());
                   }
               }

           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
               if (dr.IsClosed != true && dr != null)
                   dr.Close();
           }
           return myList;

          
       }

    }
}
