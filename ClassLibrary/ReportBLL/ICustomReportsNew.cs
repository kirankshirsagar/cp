﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrudBLL;
using System.Data;

namespace ReportBLL
{
   public interface ICustomReportsNew:IReportParameters, INavigation
    {
        string Customer_Name { get; set; }
        string Requestor_Name { get; set; }
        string Contract_ID { get; set; }
        string Effective_Date { get; set; }
        string Expiry_Date { get; set; }
        string SelectQuery { get; set; }
        int UserID { get; set; }
        string CustomReportName { get; set; }
        string ReportFixedFields { get; set; }
        string RequestFormReportFields { get; set; }
        string ImportantDatesReportFields { get; set; }
        string KeyObligationReportFields { get; set; }
        string ReportWhereClauseFields { get; set; }
        int CustomReportId { get; set; }

        List<CustomReportsNew> GetReport();
        DataTable GetReports();
        DataTable GetCustomRecord();
        DataTable GetCustomFieldDetails();
        DataTable GetCustomiesRecord();
        DataTable GetScheduleRecord();
        DataTable GetPredefineScheduleRecord();
        DataSet GetDSCustomiesRecords();
    }
}
