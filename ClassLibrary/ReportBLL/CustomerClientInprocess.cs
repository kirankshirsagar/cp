﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ReportBLL
{
   public class CustomerClientInprocess : ICustomerClientInprocess
    {
       CustomerClientInprocessDAL obj;
       public DateTime? FromDate { get; set; }
       public DateTime? ToDate { get; set; }
       public string Search { get; set; }

       public long TotalRecords { get; set; }
       public int PageNo { get; set; }
       public int RecordsPerPage { get; set; }
       public int Direction { get; set; }
       public string SortColumn { get; set; }

       public string Contract_ID { get; set; }
       public string Contract_Type { get; set; }
       public string Customer_Name { get; set; }
       public string Deadline_Date { get; set; }
       public string Contract_Status { get; set; }
       public string Requestor_Name { get; set; }
       public string Effective_Date { get; set; }
       public string Expiration_Date { get; set; }
       public int UserID { get; set; }
       public string Assigned_To { get; set; }
       public int? ContractTypeID { get; set; } 

       public List<CustomerClientInprocess> GetReport()
       {
           obj = new CustomerClientInprocessDAL();
           return obj.GetReport(this);
          
       }

       public DataTable GetFullReport()
       {
           obj = new CustomerClientInprocessDAL();
           return obj.GetFullReport();
       }


    }
}
