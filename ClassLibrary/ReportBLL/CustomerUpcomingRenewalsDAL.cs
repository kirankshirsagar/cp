﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;

namespace ReportBLL
{
    public class CustomerUpcomingRenewalsDAL : DAL
    {

       public DataTable GetFullReport()
       {
           DataTable dt = new DataTable();
           int UserID = int.Parse(System.Web.HttpContext.Current.Session[Declarations.User].ToString());
           Parameters.AddWithValue("@UserID", UserID);
           return getDataTable("CustomerUpcomingRenewals_Report");
       }


       public List<CustomerUpcomingRenewals> GetReport(ICustomerUpcomingRenewals I)
       {
           int i = 0;
           List<CustomerUpcomingRenewals> myList = new List<CustomerUpcomingRenewals>();
           Parameters.AddWithValue("@PageNo", I.PageNo);
           Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
           Parameters.AddWithValue("@FromDate", I.FromDate);
           Parameters.AddWithValue("@ToDate", I.ToDate);
           Parameters.AddWithValue("@ContractTypeId", I.ContractTypeID);
           Parameters.AddWithValue("@Search", I.Search);
           Parameters.AddWithValue("@SortColumn", I.SortColumn);
           Parameters.AddWithValue("@Direction", I.Direction);
           Parameters.AddWithValue("@UserID", I.UserID);
           SqlDataReader dr = getExecuteReader("CustomerUpcomingRenewals_Report");
           try
           {
               while (dr.Read())
               {
                   myList.Add(new CustomerUpcomingRenewals());
                   myList[i].Customer_Name = dr["Customer Name"].ToString();
                   myList[i].Requestor_Name = dr["Requester Name"].ToString();
                   myList[i].Contract_ID = dr["Contract ID"].ToString();
                   myList[i].Effective_Date = dr["Effective Date"].ToString();
                   myList[i].Expiry_Date = dr["Expiration Date"].ToString();
                   myList[i].Contract_Type = dr["Contract Type"].ToString();
                   myList[i].Renewal_Notification_Days = dr["Renewal Notification Days"].ToString();
                   myList[i].Renewal_Notification_Date = dr["Renewal Notification Date"].ToString();
                   i = i + 1;
               }

               if (dr.NextResult())
               {
                   while (dr.Read())
                   {
                       I.TotalRecords = int.Parse(dr[0].ToString());
                   }
               }

           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
               if (dr.IsClosed != true && dr != null)
                   dr.Close();
           }
           return myList;

          
       }

    }
}
