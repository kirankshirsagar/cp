﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ReportBLL
{
    public class OtherGeography : IOtherGeography
    {
        OtherGeographyDAL obj;
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Search { get; set; }

        public long TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int RecordsPerPage { get; set; }
        public int Direction { get; set; }
        public string SortColumn { get; set; }

        public int UserID { get; set; }
        public string Customer_Name { get; set; }
        public string Requestor_Name { get; set; }
        public string Contract_ID { get; set; }
        public string Effective_Date { get; set; }
        public string Expiry_Date { get; set; }
        public string Geography { get; set; }
        public string Contract_Type { get; set; }
        public int? ContractTypeID { get; set; }

        public List<OtherGeography> GetReport()
        {
            obj = new OtherGeographyDAL();
            return obj.GetReport(this);

        }

        public DataTable GetFullReport()
        {
            obj = new OtherGeographyDAL();
            return obj.GetFullReport();
        }


    }
}
