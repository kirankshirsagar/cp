﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ReportBLL
{
    public static class FactoryExport
    {

        public static string ReportName { get; set; }

        public static DataTable ExportData(int caseSwitch)
        {
            IReportParameters obj = null;
            switch (caseSwitch)
            {
                case 35:
                    obj = new CustomerActiveContracts();
                    ReportName = "Customer Contracts - Active";
                    break;
                case 36:
                    obj = new SupplierActiveContracts();
                    ReportName = "Supplier Contracts - Active";
                    break;
                case 37:
                    obj = new CustomerExpiredTerminatedContracts();
                    //ReportName = "Customer Contracts - Expired/Terminated";
                    ReportName = "Customer Contracts - Expired";
                    break;
                case 38:
                    obj = new SupplierExpiredTerminatedContracts();
                    //ReportName = "Supplier Contracts - Expired/Terminated";
                    ReportName = "Supplier Contracts - Expired";
                    break;
                case 39:
                    obj = new CustomerContractValue();
                    ReportName = "Customer Contract Value Analysis";
                    break;
                case 40:
                    obj = new SupplierContractValue();
                    ReportName = "Supplier Contract Value Analysis";
                    break;
                case 41:
                    obj = new CustomerGeography();
                    ReportName = "Customer Contracts - Geography Analysis";
                    break;
                case 42:
                    obj = new SupplierGeography();
                    ReportName = "Supplier Contracts - Geography Analysis";
                    break;
                case 43:
                    obj = new CustomerTypeValueGeography();
                    ReportName = "Customer Contracts - Type, Value and Geography Analysis";
                    break;
                case 44:
                    obj = new SupplierTypeValueGeography();
                    ReportName = "Supplier Contracts - Type, Value and Geography Analysis";
                    break;
                case 45:
                    obj = new CustomerUpcomingRenewals();
                    ReportName = "Customer Contracts - Upcoming Renewals";
                    break;
                case 46:
                    obj = new SupplierUpcomingRenewals();
                    ReportName = "Supplier Contracts - Upcoming Renewals";
                    break;

                case 47:
                    obj = new CustomerContractTypeAnalysis();
                    ReportName = "Customer Contract Type Analysis";
                    break;

                case 48:
                    obj = new SupplierContractTypeAnalysis();
                    ReportName = "Supplier Contract Type Analysis";
                    break;

                case 50:
                    obj = new CustomerKeyobligations();
                    ReportName = "Customer Contracts - Key Obligations Analysis";
                    break;

                case 51:
                    obj = new SupplierKeyobligations();
                    ReportName = "Supplier Contracts - Key Obligations Analysis";
                    break;

                case 52:
                    obj = new CustomerInprocess();
                    ReportName = "Customer Contracts-In Process";
                    break;

                case 53:
                    obj = new SupplierInprocess();
                    ReportName = "Supplier Contracts-In Process";
                    break;

                case 54:
                    obj = new CustomerChangeControl();
                    ReportName = "Customer Contracts - Change Of Control";
                    break;

                case 55:
                    obj = new SupplierChangeControl();
                    ReportName = "Supplier Contracts - Change Of Control";
                    break;

                case 56:
                    obj = new CustomerClientInprocess();
                    ReportName = "Supplier Client Inprocess";
                    break;

                case 57:
                    obj = new SupplierClientInprocess();
                    ReportName = "Supplier Client Inprocess";
                    break;

                case 58:
                    obj = new CustomerActivieContactValue();
                    ReportName = "Customer Activie Contact Value";
                    break;

                case 59:
                    obj = new SupplierActivieContactValue();
                    ReportName = "Supplier Activie Contact Value";
                    break;

                case 60:
                    obj = new CustomerRenewalDate();
                    ReportName = "Customer RenewalDate";
                    break;

                case 61:
                    obj = new SupplierRenewalDate();
                    ReportName = "Supplier RenewalDate";
                    break;

                case 62:
                    obj = new CustomerDepartment();
                    ReportName = "Department Analysis for Customer Contracts";
                    break;

                case 63:
                    obj = new SupplierDepartment();
                    ReportName = "Department Analysis for Supplier Contracts";
                    break;

                case 64:
                    obj = new CustomerRequesterStatus();
                    ReportName = "Contract Requester Analysis";
                    break;

                case 65:
                    obj = new CustomerImportantDates();
                    ReportName = "Customer Contracts - Important Dates";
                    break;

                case 66:
                    obj = new SupplierImportantDates();
                    ReportName = "Supplier Contracts - Important Dates";
                    break;

                case 67:
                    obj = new CustomerTerminatedContracts();
                    ReportName = "Customer Contracts - Termination Requests";
                    break;

                case 68:
                    obj = new SupplierTerminatedContracts();
                    ReportName = "Supplier Contracts - Termination Requests";
                    break;

                case 69: 
                    obj = new HistoricReports();
                    ReportName = "Request History";
                    break;

                case 70:
                    obj = new CustomerStatusAnalysis();
                    ReportName = "Customer Status Analysis";
                    break;
                
                case 71:
                    obj = new CustomerContractsTerminated();
                    ReportName = "Customer Contracts - Terminated";
                    break;

                case 72:
                    obj = new SupplierContractsTerminated();
                    ReportName = "Supplier Contracts - Terminated";
                    break;

                case 75:
                    obj = new MasterReports();
                    ReportName = "Master Reports";
                    break;

                case 78:
                    obj = new OtherActiveContracts();
                    ReportName = "Other Contracts - Active";
                    break;

                case 79:
                    obj = new OtherExpiredTerminatedContracts();
                    ReportName = "Other Contracts - Expired";
                    break;

                case 80:
                    obj = new OtherContractValue();
                    ReportName = "Other Contract Value Analysis";
                    break;

                case 81:
                    obj = new OtherGeography();
                    ReportName = "Other Contracts - Geography Analysis";
                    break;

                case 82:
                    obj = new OtherContractTypeAnalysis();
                    ReportName = "Other Contracts - Type, Value and Geography Analysis";
                    break;

                case 83:
                    obj = new OtherUpcomingRenewals();
                    ReportName = "Other Contracts - Upcoming Renewals";
                    break;

                case 84:
                    obj = new OtherContractTypeAnalysis();
                    ReportName = "Other Contract Type Analysis";
                    break;

                case 85:
                    obj = new OtherKeyobligations();
                    ReportName = "Other Contracts - Key Obligations Analysis";
                    break;

                case 86:
                    obj = new OtherInprocess();
                    ReportName = "Other Contracts - In Process";
                    break;

                case 87:
                    obj = new OtherChangeControl();
                    ReportName = "Other Contracts - Change Of Control";
                    break;

                case 88:
                    obj = new OtherDepartment();
                    ReportName = "Department Analysis for Other Contracts";
                    break;

                case 89:
                    obj = new OtherImportantDates();
                    ReportName = "Other Contracts - Important Dates";
                    break;

                case 90:
                    obj = new OtherTerminatedContracts();
                    ReportName = "Other Contracts - Termination Requests";
                    break;

                case 91:
                    obj = new OtherContractsTerminated();
                    ReportName = "Other Contracts - Terminated";
                    break;

                default:
                    obj = new CustomReportsNew();
                    if (Convert.ToString(System.Web.HttpContext.Current.Request.QueryString["Name"]) != "")
                        ReportName = Convert.ToString(System.Web.HttpContext.Current.Request.QueryString["Name"]);
                    else
                        break;

                    break;
            }
            return obj.GetFullReport();
        }
    }
}
