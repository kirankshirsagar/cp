﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonBLL;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Configuration;
using System.Data;

namespace ReportBLL
{
    public class CustomReportDAL : DAL
    {
        public DataTable GetFullReport()
        {
            int i = 0;
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            string se = System.Web.HttpContext.Current.Session[Declarations.SelectQuery].ToString();
            Parameters.AddWithValue("@Search", se);
            SqlDataReader dr = getExecuteReader("CustomMaster_Reports");
            try
            {
                dt.Load(dr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return dt;
            //return getDataTable("Master_Reports");
        }

        public List<CustomReports> GetReport(ICustomReports I)
        {
            int i = 0;
            List<CustomReports> myList = new List<CustomReports>();
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@FromDate", I.FromDate);
            Parameters.AddWithValue("@ToDate", I.ToDate);
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeID);
            Parameters.AddWithValue("@Search", I.Search);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            Parameters.AddWithValue("@UserID", I.UserID);
            SqlDataReader dr = getExecuteReader("Master_Reportes");
            try
            {
                while (dr.Read())
                {
                    myList.Add(new CustomReports());
                    myList[i].Customer_Name = dr["Customer Name"].ToString();
                    myList[i].Requestor_Name = dr["Requester Name"].ToString();
                    myList[i].Contract_ID = dr["Contract ID"].ToString();
                    myList[i].Effective_Date = dr["Effective Date"].ToString();
                    myList[i].Expiry_Date = dr["Expiration Date"].ToString();

                    myList[i].Contract_Type = dr["Contract Type"].ToString();
                    myList[i].Contract_Template = dr["Contract Template"].ToString();
                    myList[i].Client_Name = dr["Client Name"].ToString();
                    myList[i].Address = dr["Address"].ToString();
                    myList[i].State_Name = dr["State Name"].ToString();
                    myList[i].City_Name = dr["City Name"].ToString();
                    myList[i].Country_Name = dr["Country Name"].ToString();
                    myList[i].Pin_Code = dr["Pin Code"].ToString();
                    myList[i].Request_Description = dr["Request Description"].ToString();
                    myList[i].Deadline_Date = dr["Deadline Date"].ToString();
                    myList[i].Department_Name = dr["Department Name"].ToString();
                    myList[i].Assign_User = dr["Assign User"].ToString();
                    myList[i].Is_Approve = dr["Is Approve"].ToString();
                    myList[i].Estimated_Value = dr["Estimated Value"].ToString();
                    myList[i].Contact_Number = dr["Contact Number"].ToString();
                    myList[i].Email_Id = dr["Email Id"].ToString();
                    myList[i].Status = dr["Status"].ToString();

                    myList[i].Liability_Cap = dr["Liability Cap"].ToString();
                    myList[i].Indemnity = dr["Indemnity"].ToString();
                    myList[i].Is_Change_of_Control = dr["Is Change of Control"].ToString();
                    myList[i].Is_Agreement_Clause = dr["Is Agreement Clause"].ToString();
                    myList[i].Is_Strictliability = dr["Is Strictliability"].ToString();
                    myList[i].Strictliability = dr["Strictliability"].ToString();
                    myList[i].Is_Guarantee = dr["Is Guarantee"].ToString();
                    myList[i].Insurance = dr["Insurance"].ToString();
                    myList[i].Is_Termination_Convenience = dr["Is Termination Convenience"].ToString();
                    myList[i].Termination_Description = dr["Termination Description"].ToString();
                    myList[i].Is_Termination_material = dr["Is Termination material"].ToString();
                    myList[i].Is_Termination_insolvency = dr["Is Termination insolvency"].ToString();
                    myList[i].Is_Terminationin_Change_of_control = dr["Is Terminationin Change of control"].ToString();
                    myList[i].Is_Termination_other_causes = dr["Is Termination other causes"].ToString();
                    myList[i].Assignment_Novation = dr["Assignment Novation"].ToString();
                    myList[i].Others = dr["Others"].ToString();


                    myList[i].Contracting_Party = dr["Contracting Party"].ToString();
                    myList[i].Is_Customer = dr["Is Customer"].ToString();
                    myList[i].Street2 = dr["Street2"].ToString();
                    myList[i].Bulk_Import = dr["Bulk Import"].ToString();
                    myList[i].Is_Bulk_Import = dr["Is Bulk Import"].ToString();
                    myList[i].Is_Doc_Sign_Completed = dr["Is Doc Sign Completed"].ToString();
                    myList[i].Contract_Term = dr["Contract Term"].ToString();
                    myList[i].Contract_Value = dr["Contract Value"].ToString();


                    i = i + 1;
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        I.TotalRecords = int.Parse(dr[0].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return myList;


        }

        public DataTable GetReports(CustomReports I)
        {
            int i = 0;
            List<CustomReports> myList = new List<CustomReports>();
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            SqlDataReader dr = getExecuteReader("CustomMaster_ColumnReports");

            try
            {
                dt.Load(dr);

                //List<DataRow> rowsToDelete = new List<DataRow>();
                //foreach (DataRow row in dt.Rows)
                //{
                //    if (row["Name"].ToString() == "ClientId" || row["Name"].ToString() == "CityId" || row["Name"].ToString() == "StateId"
                //        || row["Name"].ToString() == "ContractExpiryDate" || row["Name"].ToString() == "Addedon" || row["Name"].ToString() == "Modifiedon"
                //        || row["Name"].ToString() == "Modifiedby" || row["Name"].ToString() == "IP" || row["Name"].ToString() == "ExpirationAlert30daysUserID1"
                //        || row["Name"].ToString() == "ExpirationAlert30daysUserID2" || row["Name"].ToString() == "ExpirationAlert7days"
                //        || row["Name"].ToString() == "ExpirationAlert7daysUserID1" || row["Name"].ToString() == "ExpirationAlert7daysUserID2"
                //        || row["Name"].ToString() == "ExpirationAlert24Hrs" || row["Name"].ToString() == "ExpirationAlert24HrsUserID1"
                //        || row["Name"].ToString() == "ExpirationAlert24HrsUserID2" || row["Name"].ToString() == "RenewalAlert30daysUserID1"
                //        || row["Name"].ToString() == "RenewalAlert30daysUserID2" || row["Name"].ToString() == "RenewalAlert7days"
                //        || row["Name"].ToString() == "RenewalAlert7daysUserID1" || row["Name"].ToString() == "RenewalAlert7daysUserID2"
                //        || row["Name"].ToString() == "RenewalAlert24Hrs" || row["Name"].ToString() == "RenewalAlert24HrsUserID1"
                //        || row["Name"].ToString() == "RenewalAlert24HrsUserID2" || row["Name"].ToString() == "ContractingPartyName"
                //        || row["Name"].ToString() == "ExpirationAlert120days" || row["Name"].ToString() == "ExpirationAlert120daysUserID1"
                //        || row["Name"].ToString() == "ExpirationAlert120daysUserID2" || row["Name"].ToString() == "ExpirationAlert90days"
                //        || row["Name"].ToString() == "ExpirationAlert90daysUserID1" || row["Name"].ToString() == "ExpirationAlert90daysUserID2"
                //        || row["Name"].ToString() == "ExpirationAlert60days" || row["Name"].ToString() == "ExpirationAlert60daysUserID1"
                //        || row["Name"].ToString() == "ExpirationAlert60daysUserID2" || row["Name"].ToString() == "RenewalAlert120days"
                //        || row["Name"].ToString() == "RenewalAlert120daysUserID1" || row["Name"].ToString() == "RenewalAlert120daysUserID2"
                //        || row["Name"].ToString() == "RenewalAlert90days" || row["Name"].ToString() == "RenewalAlert90daysUserID1"
                //        || row["Name"].ToString() == "RenewalAlert90daysUserID2" || row["Name"].ToString() == "RenewalAlert60days"
                //        || row["Name"].ToString() == "RenewalAlert60daysUserID1" || row["Name"].ToString() == "RenewalAlert60daysUserID2"
                //        || row["Name"].ToString() == "RequestContractID" || row["Name"].ToString() == "isCustomer" || row["Name"].ToString() == "Street2"
                //        || row["Name"].ToString() == "Addedby" || row["Name"].ToString() == "BulkImportStagingId" || row["Name"].ToString() == "CountryName"
                //        || row["Name"].ToString() == "RequestTypeId"
                //        //  
                //        )
                //    {
                //        rowsToDelete.Add(row);
                //    }
                //}
                //foreach (DataRow row in rowsToDelete)
                //{
                //    dt.Rows.Remove(row);
                //}
                //dt.AcceptChanges();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return dt;
        }

        public DataTable GetCustomContractType(CustomReports I)
        {
            int i = 0;
            List<CustomReports> myList = new List<CustomReports>();
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            Parameters.AddWithValue("@UserID", I.UserID);
            SqlDataReader dr = getExecuteReader("CustomContractTypeSelect");

            try
            {
                dt.Load(dr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return dt;
        }

        public DataTable GetContractType(CustomReports I)
        {
            int i = 0;
            List<CustomReports> myList = new List<CustomReports>();
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            SqlDataReader dr = getExecuteReader("MasterContractTypeSelect");

            try
            {
                dt.Load(dr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return dt;
        }

        public DataTable GetCustomData(CustomReports I)
        {
            int i = 0;
            List<CustomReports> myList = new List<CustomReports>();
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);

            SqlDataReader dr = getExecuteReader("CustomDataSelect");

            try
            {
                dt.Load(dr);
                I.TotalRecords = Convert.ToInt32(dt.Rows[0]["Maxcount"]);
            }
            catch (Exception ex)
            {
                //throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return dt;
        }

        public DataTable GetCustomRecord(ICustomReports I)
        {
            int i = 0;
            List<CustomReports> myList = new List<CustomReports>();
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            Parameters.AddWithValue("@PageNo", I.PageNo);
            Parameters.AddWithValue("@RecordsPerPage", I.RecordsPerPage);
            Parameters.AddWithValue("@FromDate", I.FromDate);
            Parameters.AddWithValue("@ToDate", I.ToDate);
            Parameters.AddWithValue("@ContractTypeId", I.ContractTypeID);
            Parameters.AddWithValue("@Search", I.SelectQuery);
            Parameters.AddWithValue("@SortColumn", I.SortColumn);
            Parameters.AddWithValue("@Direction", I.Direction);

            SqlDataReader dr = getExecuteReader("CustomMaster_Reports");

            try
            {
                dt.Load(dr);
                I.TotalRecords = int.Parse(dt.Rows[0]["Maxcount"].ToString());
            }
            catch (Exception ex)
            {
                //throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return dt;
        }

        public string InsertRecord(ICustomReports I)
        {
            Parameters.AddWithValue("@RequestFormReportFields", I.RequestFormReportFields);
            Parameters.AddWithValue("@ImportantDatesReportFields", I.ImportantDatesReportFields);
            Parameters.AddWithValue("@KeyObligationReportFields", I.KeyObligationReportFields);
            Parameters.AddWithValue("@ReportWhereClauseFields", I.ReportWhereClauseFields);
            Parameters.AddWithValue("@CustomReportName", I.CustomReportName);
            Parameters.AddWithValue("@ReportFixedFields", I.ReportFixedFields);
            Parameters.AddWithValue("@CustomReportId", I.CustomReportId);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@CustomDescription", I.CustomDescription);
            Parameters.AddWithValue("@ScheduleDay", I.ScheduleDay);
            Parameters.AddWithValue("@ScheduleTime", I.ScheduleTime);
            Parameters.AddWithValue("@ScheduleWeek", I.ScheduleWeek);
            Parameters.AddWithValue("@ScheduleMonth", I.ScheduleMonth);
            Parameters.AddWithValue("@ScheduleYear", I.ScheduleYear);
            Parameters.AddWithValue("@ScheduleType", I.ScheduleType);
            Parameters.AddWithValue("@MailId", I.MailId);
            Parameters.AddWithValue("@RecipiantMailId", I.RecipiantMailId);
            Parameters.AddWithValue("@Status", 0);

            Parameters.AddWithValue("@IsCustomReport", I.IsCustomReport);
            Parameters.AddWithValue("@PredefinedReportSP", I.PredefinedReportSP);

            #region WhereCondition Data Except Contract Type

            Parameters.AddWithValue("@EffectiveDate", I.EffectiveDate);
            Parameters.AddWithValue("@ExpiryDate", I.ExpiryDate);
            Parameters.AddWithValue("@RenewalDate", I.RenewalDate);
            Parameters.AddWithValue("@RequestDate", I.RequestDate);
            Parameters.AddWithValue("@EffectiveToDate", I.EffectiveToDate);
            Parameters.AddWithValue("@ExpiryToDate", I.ExpiryToDate);
            Parameters.AddWithValue("@RenewalToDate", I.RenewalToDate);
            Parameters.AddWithValue("@RequestToDate", I.RequestToDate);
            Parameters.AddWithValue("@SignatureDate", I.SignatureDate);
            Parameters.AddWithValue("@SignatureToDate", I.SignatureToDate);
            Parameters.AddWithValue("@AnnualValue", I.AnnualValue);
            Parameters.AddWithValue("@TotalValue", I.TotalValue);
            Parameters.AddWithValue("@AnnualValueTo", I.AnnualValueTo);
            Parameters.AddWithValue("@TotalValueTo", I.TotalValueTo);

            Parameters.AddWithValue("@Season", I.Season);
            Parameters.AddWithValue("@Quarter", I.Quarter);

            Parameters.AddWithValue("@TerritoryId", I.TerritoryId);
            Parameters.AddWithValue("@ContractingPartyId", I.ContractingPartyId);
            Parameters.AddWithValue("@ContractStatusId", I.ContractStatusId);
            Parameters.AddWithValue("@AssignedDepartmentId", I.AssignedDepartmentId);
            Parameters.AddWithValue("@AssignedUserId", I.AssignedUserId);
            Parameters.AddWithValue("@ClientId", I.ClientId);
            Parameters.AddWithValue("@ChangeOfControl", I.ChangeOfControl);
            Parameters.AddWithValue("@EntireAgreementClause", I.EntireAgreementClause);
            Parameters.AddWithValue("@Strictliability", I.Strictliability);
            Parameters.AddWithValue("@ThirdPartyGuaranteeRequired", I.ThirdPartyGuaranteeRequired);
            Parameters.AddWithValue("@Parentcompanyguarantee", I.Parentcompanyguarantee);

            Parameters.AddWithValue("@PriorityContract", I.Priority);

            #endregion

            if (I.Flag == 4)
                Parameters.AddWithValue("@Flag", 4);
            else if (I.Flag == 6)
                Parameters.AddWithValue("@Flag", 6);
            else
                Parameters.AddWithValue("@Flag", 1);

            Parameters["@Status"].Direction = ParameterDirection.Output;
            // kiran 17 feb 2016
            string rvalue = getExcuteQuery("CustomReportAddUpdate", "@Status");
            //I.RequestID = int.Parse(rvalue.Split(',')[1]);
            return rvalue.Split(',')[0];


        }

        public string UpdateRecord(ICustomReports I)
        {
            Parameters.AddWithValue("@RequestFormReportFields", I.RequestFormReportFields);
            Parameters.AddWithValue("@ImportantDatesReportFields", I.ImportantDatesReportFields);
            Parameters.AddWithValue("@KeyObligationReportFields", I.KeyObligationReportFields);
            Parameters.AddWithValue("@ReportWhereClauseFields", I.ReportWhereClauseFields);
            Parameters.AddWithValue("@CustomReportName", I.CustomReportName);
            Parameters.AddWithValue("@ReportFixedFields", I.ReportFixedFields);
            Parameters.AddWithValue("@CustomReportId", I.CustomReportId);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@CustomDescription", I.CustomDescription);
            Parameters.AddWithValue("@Status", 0);
            Parameters.AddWithValue("@Flag", 2);

            #region WhereCondition Data Except Contract Type

            Parameters.AddWithValue("@EffectiveDate", I.EffectiveDate);
            Parameters.AddWithValue("@ExpiryDate", I.ExpiryDate);
            Parameters.AddWithValue("@RenewalDate", I.RenewalDate);
            Parameters.AddWithValue("@RequestDate", I.RequestDate);
            Parameters.AddWithValue("@EffectiveToDate", I.EffectiveToDate);
            Parameters.AddWithValue("@ExpiryToDate", I.ExpiryToDate);
            Parameters.AddWithValue("@RenewalToDate", I.RenewalToDate);
            Parameters.AddWithValue("@RequestToDate", I.RequestToDate);
            Parameters.AddWithValue("@SignatureDate", I.SignatureDate);
            Parameters.AddWithValue("@SignatureToDate", I.SignatureToDate);


            Parameters.AddWithValue("@AnnualValue", I.AnnualValue);
            Parameters.AddWithValue("@TotalValue", I.TotalValue);
            Parameters.AddWithValue("@AnnualValueTo", I.AnnualValueTo);
            Parameters.AddWithValue("@TotalValueTo", I.TotalValueTo);

            Parameters.AddWithValue("@Season", I.Season);
            Parameters.AddWithValue("@Quarter", I.Quarter);

            Parameters.AddWithValue("@TerritoryId", I.TerritoryId);
            Parameters.AddWithValue("@ContractingPartyId", I.ContractingPartyId);
            Parameters.AddWithValue("@ContractStatusId", I.ContractStatusId);
            Parameters.AddWithValue("@AssignedDepartmentId", I.AssignedDepartmentId);
            Parameters.AddWithValue("@AssignedUserId", I.AssignedUserId);
            Parameters.AddWithValue("@ClientId", I.ClientId);
            Parameters.AddWithValue("@ChangeOfControl", I.ChangeOfControl);
            Parameters.AddWithValue("@EntireAgreementClause", I.EntireAgreementClause);
            Parameters.AddWithValue("@Strictliability", I.Strictliability);
            Parameters.AddWithValue("@ThirdPartyGuaranteeRequired", I.ThirdPartyGuaranteeRequired);
            Parameters.AddWithValue("@Parentcompanyguarantee", I.Parentcompanyguarantee);
            Parameters.AddWithValue("@PriorityContract", I.Priority);

            #endregion

            Parameters["@Status"].Direction = ParameterDirection.Output;
            //Parameters["@CreatedRequestId"].Direction = ParameterDirection.Output;
            // kiran 17 feb 2016
            string rvalue = getExcuteQuery("CustomReportAddUpdate", "@Status");
            //I.RequestID = int.Parse(rvalue.Split(',')[1]);
            return rvalue.Split(',')[0];


        }

        public string DeleteRecord(ICustomReports I)
        {
            Parameters.AddWithValue("@RequestFormReportFields", I.RequestFormReportFields);
            Parameters.AddWithValue("@ImportantDatesReportFields", I.ImportantDatesReportFields);
            Parameters.AddWithValue("@KeyObligationReportFields", I.KeyObligationReportFields);
            Parameters.AddWithValue("@ReportWhereClauseFields", I.ReportWhereClauseFields);
            Parameters.AddWithValue("@CustomReportName", I.CustomReportName);
            Parameters.AddWithValue("@ReportFixedFields", I.ReportFixedFields);
            Parameters.AddWithValue("@AddedBy", I.AddedBy);
            Parameters.AddWithValue("@ModifiedBy", I.ModifiedBy);
            Parameters.AddWithValue("@IpAddress", I.IpAddress);
            Parameters.AddWithValue("@CustomReportId", I.CustomReportId);
            Parameters.AddWithValue("@CustomDescription", I.CustomDescription);
            Parameters.AddWithValue("@Status", 0);
            Parameters.AddWithValue("@Flag", 3);
            Parameters["@Status"].Direction = ParameterDirection.Output;
            // kiran 17 feb 2016
            string rvalue = getExcuteQuery("CustomReportAddUpdate", "@Status");
            return rvalue.Split(',')[0];
        }

        public DataTable GetRecipentMailIdData(ICustomReports I)
        {
            int i = 0;
            DataTable dt = new DataTable();
            SqlDataReader dr;
            if (I.Flag == 1)
            {
                Parameters.AddWithValue("@RecipiantIds", I.RecipiantIds);
                dr = getExecuteReader("GetRecipeantMailIds");
            }
            else
                dr = getExecuteReader("MasterUserRead");
            try
            {
                dt.Load(dr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public int GetReportName(ICustomReports I)
        {
            Parameters.AddWithValue("@ReportName", I.Customer_Name);
            Parameters.AddWithValue("@Status", 0);

            Parameters["@Status"].Direction = ParameterDirection.Output;
            return int.Parse(getExcuteQuery("CustomReportNameExists", "@Status"));
        }

        public DataTable GetCustomerSupplierName(ICustomReports I)
        {
            int i = 0;
            DataTable dt = new DataTable();
            SqlDataReader dr;
            dr = getExecuteReader("CustomerSupplierRead");
            try
            {
                dt.Load(dr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetReportAccessId(CustomReports I)
        {
            int i = 0;
            List<CustomReports> myList = new List<CustomReports>();
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            Parameters.AddWithValue("@flag", I.Flag);
            Parameters.AddWithValue("@UserID", I.UserID);
            SqlDataReader dr = getExecuteReader("GetReportAccessIds");

            try
            {
                dt.Load(dr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dr.IsClosed != true && dr != null)
                    dr.Close();
            }
            return dt;
        }

    }
}
