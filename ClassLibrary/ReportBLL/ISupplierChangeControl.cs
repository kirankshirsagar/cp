﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrudBLL;

namespace ReportBLL
{
    public interface ISupplierChangeControl : IReportParameters, INavigation
    {
        
        string Supplier_Name { get; set; }
        string Requestor_Name { get; set; }
        string Contract_ID { get; set; }
        string Effective_Date { get; set; }
        string Expiry_Date { get; set; }
        string Change_of_Control { get; set; }
        string Contract_Type { get; set; }
        string Contract_Status { get; set; }
        List<SupplierChangeControl> GetReport();
        int UserID { get; set; }

    }
}
