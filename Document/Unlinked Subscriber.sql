
ALTER PROCEDURE [dbo].[ContractRequestDetails]
@RequestID INT,
@UserID INT=0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	SET NOCOUNT ON;

		DECLARE @ContractTypeId INT, @RequestTypeID INT, @ContractID BIGINT,@StatusName VARCHAR(100)
		SELECT @ContractTypeId=ContractTypeId, @RequestTypeID=RequestTypeID, @ContractID=ContractID FROM ContractRequest WHERE RequestId=@RequestID		

		SELECT TOP 1 @StatusName=StatusName FROM ContractRequest 
		INNER JOIN vwContractStatus ON ContractRequest.ContractStatusId=vwContractStatus.ContractStatusId
		WHERE (ContractID=@ContractID AND @ContractID IS NOT NULL)  
						OR (RequestId=@RequestId AND @ContractID IS NULL)  
		ORDER BY RequestId DESC

		----Resultset 1--------
		SELECT MstContractType.ContractTypeId, ContractTypeName,ContractRequest.ContractId,
		ClientName,
		ContactNumber,
		EmailID,
		--(ContractRequest.Address+ (CASE  WHEN Street2 IS NOT NULL AND Street2<> '' THEN ', '+Street2 ELSE '' END)+',<br/>'+ ContractRequest.CityName +',<br/>'+ContractRequest.StateName+',<br/>'+MstCountry.CountryName) ClientAddress,
		dbo.GetRequestAddress(@RequestId)ClientAddress,
		CASE ISNULL(ContractRequest.IsApprovalRequired,'N') WHEN 'N' THEN 'No' ELSE 'Yes' END IsApprovalRequired,dbo.DateFormat(ContractRequest.DeadlineDate)DeadlineDate,MstUsers.FullName,ISNULL(FORMAT(ContractRequest.Addedon,'d-MMM-yyyy h:mm tt'),GETDATE())AddedDate,
		(SELECT FullName FROM MstUsers WHERE UsersId=ContractRequest.AssignToUserID) AssignedTo, ISNULL(ContractRequest.EstimatedValue,0) EstimatedValue,
		ContractRequest.RequestDescription,
		ContractRequest.RequestTypeId,
		vwRequestType.RequestTypeName
		--,vwContractStatus.StatusName
		,@StatusName StatusName,
		ContractRequest.ContractValue,
		ContractRequest.ContractTerm,
		ContractRequest.ContractStatusID,
		ContractRequest.IsBulkImport,
		MstContractType.ContractTypeId,
		--Format(ContractValue ,'N','en-US' ) + ' (' + (SELECT CurrencyCode FROM MstCurrency  WHERE MstCurrency.CurrencyID=ContractRequest.CurrencyId OR (IsBaseCurrency='Y' AND ContractRequest.CurrencyId = 0 AND ContractRequest.ContractValue > 0)) +')' AS ContractValuewithcurrency
		dbo.[GetContractValueWithCurrencyCode](ContractRequest.RequestId) AS ContractValuewithcurrency
		,(CASE WHEN ContractRequest.Priority='N' THEN 'No' WHEN  ContractRequest.Priority='Y' THEN 'Yes' WHEN  ContractRequest.Priority='N/A' THEN 'N/A' END) Priority
		,ContractRequest.PriorityReason
		FROM MstContractType
		INNER JOIN ContractRequest ON ContractRequest.ContractTypeId=MstContractType.ContractTypeId
		INNER JOIN vwRequestType ON vwRequestType.RequestTypeId=ContractRequest.RequestTypeId		
		LEFT OUTER JOIN CMSCentral.dbo.MstCountry ON MstCountry.CountryId=ISNULL(ContractRequest.CountryId,0)
		INNER JOIN MstUsers ON MstUsers.UsersId=ContractRequest.Addedby
		INNER JOIN vwContractStatus ON vwContractStatus.ContractStatusId=ContractRequest.ContractStatusId
		WHERE ContractRequest.RequestId=@RequestID

		----Resultset 2--------Contract Documents--------------------------------------
		EXEC RequestDocumentsDetails @RequestID

		----Resultset 3--------Contract Templates----------------------------------------
		SELECT Id, Name FROM
			(SELECT CONVERT(VARCHAR(200), CAST(ContractTemplateId AS VARCHAR(2000))+'#'+TemplateFileName) AS Id, ContractTemplateName  AS Name  
			,ROW_NUMBER() OVER ( ORDER BY ContractTemplate.ContractTemplateName ) AS seq
			FROM ContractTemplate 
			WHERE ContractTypeId=@ContractTypeId  AND ContractTemplate.isActive='Y' --AND (ContractTemplate.IsBulkImport IS NULL OR ContractTemplate.IsBulkImport='N')
							AND @RequestTypeID IN (SELECT ITEMS FROM dbo.SPLIT(RequestTypeIDs,','))
			UNION
			SELECT '0','--Select--', 0 seq
			)tmp ORDER BY seq ASC

		----Resultset 4--------
		IF EXISTS(SELECT 1 FROM ContractQuestionsAnswers WHERE RequestId=@RequestID)
			SELECT 'Y' IsAnswered
		ELSE
			SELECT 'N' IsAnswered
		
		----Resultset 5--------
		SELECT TOP(1)ContractTemplate.ContractTemplateId,ContractTemplate.TemplateFileName  FROM ContractTemplate
		LEFT OUTER JOIN  ContractQuestionsAnswers  ON ContractTemplate.ContractTemplateId=ContractQuestionsAnswers.ContractTemplateId
		WHERE ContractQuestionsAnswers.RequestId=@RequestID

		----Resultset 6--------Contract Versions----------------
		SELECT dbo.GenerateContractFileName(@RequestId) NextContractFileName

		----Resultset 7--------IsApprovalDone----------------
		IF EXISTS(SELECT 1 FROM ContractApprovals WHERE RequestId=@RequestId AND  ISNULL(IsActive,'Y')='Y')
			SELECT 'N' IsApprovalDone
		ELSE SELECT 'Y' IsApprovalDone

		----Resultset 8--------IsSignatureStatusSent----------------
		IF EXISTS(SELECT 1 FROM ContractDocSignDetails WHERE RequestId=@RequestId AND [Status]='Sent')
			SELECT 'Y' IsSignatureStatusSent
		ELSE SELECT 'N' IsSignatureStatusSent


		----Resultset 9--------Contract History------ Added by Santosh Jadhav----------

		SELECT cR.RequestID,		
		CAST(vwRequestType.RequestTypeName AS VARCHAR(500)) RequestType,
		MstContractType.ContractTypeName AS ContractType,
		ISNULL(vwContractStatus.StatusName,'')  AS ContractStatus,

		'RequestFlow.aspx?Status=Workflow&RequestId='+CAST(RequestId AS VARCHAR) AS URL,
		CAST(cR.RequestId AS VARCHAR) +' - '+  
		vwRequestType.RequestTypeName +' - '+  
		MstContractType.ContractTypeName +' - '+  
		ISNULL(vwContractStatus.StatusName,'') AS [Contract History]
		FROM ContractRequest cR	
		INNER JOIN vwRequestType ON vwRequestType.RequestTypeID = cR.RequestTypeId
		INNER JOIN MstContractType ON MstContractType.ContractTypeId = cR.ContractTypeId
		LEFT JOIN vwContractStatus ON vwContractStatus.ContractStatusId = cR.ContractStatusId
		WHERE cR.RequestId<@RequestID 
		AND cR.ContractID = ( SELECT ContractID FROM ContractRequest
	                          WHERE RequestId=@RequestID)
		ORDER BY RequestId DESC
 
		----Resultset 10--------IsFirstCheckOutDone---------------------------
        SELECT CASE WHEN COUNT(1)=0 AND @RequestID NOT IN (SELECT RequestId FROM EmailSentLog WHERE Mode ='Contract Generated' AND isSent='Y') 
									THEN 'N' ELSE 'Y' END IsFirstCheckOutDone  
		FROM ContractFileActivity 
		INNER JOIN  ContractFiles ON ContractFileActivity.ContractFileId=ContractFiles.ContractFileId
		WHERE ContractFiles.RequestId=@RequestID AND ContractFileActivity.FileActivityTypeId=2


		----Resultset 11--------Show Expiry Date Message For Terminated Contract---------------------------
		SELECT CASE WHEN EXISTS
		(SELECT 1 FROM ContractRequest
		WHERE ExpirationDate IS NOT NULL AND ExpirationDate > dbo.DateOnly(GETDATE())
		AND ContractStatusID =11
		AND RequestId = (SELECT TOP(1) RequestId FROM ContractRequest WHERE ContractID=(SELECT ContractID FROM ContractRequest WHERE RequestId=@RequestID)ORDER BY RequestId DESC )
		) THEN 'Y' ELSE 'N' END ShowExpiryDateMessage,'Kindly change the Expiry Date field information in order to reflect contract Termination Date.' MessageText

		----Resultset 12--------Show Expiry Date Message For Renewal Contract---------------------------
		SELECT CASE WHEN EXISTS
		(SELECT 1 FROM ContractRequest
		WHERE ExpirationDate IS NOT NULL AND ExpirationDate < dbo.DateOnly(GETDATE())
		AND ContractStatusID =10
		AND RequestId = (SELECT TOP(1) RequestId FROM ContractRequest WHERE ContractID=(SELECT ContractID FROM ContractRequest WHERE RequestId=@RequestID)ORDER BY RequestId DESC )
		) THEN 'Y' ELSE 'N' END ShowExpiryDateMessageForRenewal,'Kindly change the Expiration Date field information in order to reflect updated contract Expiration Date.'MessageText

		----Resultset 13--------Retrieving BrandId---------------------------
		SELECT  'f31017cb-5d2c-4a0a-9b1b-3c81238d47ca' DocuSignBrandId,
		'80a35ae2-a56e-4aab-bfc7-ef13fd3f0be9'  DocuSignUsername ,
		'NewGalexy123'  DocuSignPassword		

		----Resultset 14--------IsFinalDocument----------------
		SELECT COALESCE(MAX('Y'),'N') IsFinalDocument FROM ContractFiles WHERE RequestId=@RequestID AND isFromDocuSign='Y'


		-----ResulSet 15---------isContractTypeExits ------
		BEGIN
			DECLARE @Role VARCHAR(50)
			SET @Role=(SELECT MstRole.RoleName FROM MstUsers INNER JOIN MstRole ON MstUsers.RoleId=MstRole.RoleId AND MstUsers.UsersId=@UserID)

			IF dbo.isTableExists('#tempContratType') > 0 DROP TABLE #tempContratType
			CREATE table #tempContratType (ContractType Int)

			INSERT INTO #tempContratType
			SELECT  ContractTypeId 
			FROM MSTContractType  
			WHERE  MstContractType.ContractTypeId in(SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),MstContractType.ContractTypeId)
			ELSE dbo.GetContractTypeAccess(@Role) END),','))
			AND
			EXISTS ((SELECT 1 FROM vwRequestAccess uA WHERE uA.RequestId = @RequestID AND uA.UsersId = @UserID) UNION (SELECT 1 FROM Activity  WHERE AssignToId=@UserID AND RequestId = @RequestID)) 

			IF EXISTS (SELECT 1 FROM #tempContratType WHERE ContractType=@ContractTypeId)	
			SELECT 'Y' isContractTypeExits,'' CTMessage;
			ELSE
			SELECT 'N' isContractTypeExits,'You do not have access to records of this contract type. Please contact your administrator.' CTMessage;

			IF dbo.isTableExists('#tempContratType') > 0 DROP TABLE #tempContratType
	END

	-----ResulSet 16---------For Contract RelationShips ------

	BEGIN		
		EXEC AccessGet @PageName='contractrelations.aspx',@UserId=@UserID		
	END

END


GO






		

		ALTER FUNCTION [dbo].[GetUserRemainingCount]
		(
			
		) 
		
		RETURNS INT
		AS
		BEGIN
			DECLARE @Result INT = 0
			
			SET @Result =3 - (	SELECT ISNULL(COUNT(1),0) FROM MstUsers WHERE IsActive= 'Y' OR IsActive IS NULL)

			RETURN @Result
		END
		GO






/*
SELECT * FROM mstUsers
exec MasterUserGetUsersId 'usil'


*/
GO

ALTER PROCEDURE [dbo].[MasterUserGetUsersId]
	@UserName VARCHAR(50)
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @UserId INT = 0
	DECLARE @FullName VARCHAR(100) 
	DECLARE @RoleName VARCHAR(100) 
	DECLARE @OutlookFileName VARCHAR(1000)
	DECLARE @IsOutlookSynchEnable CHAR(1)

	SELECT @UserId= AU.UsersID, @FullName = FullName ,@OutlookFileName=RIGHT(COALESCE(MU.OutlookCalendarFileName,''), CHARINDEX('/', REVERSE('/' + COALESCE(MU.OutlookCalendarFileName,''))) - 1) 
	FROM aspnet_Users AU
	INNER JOIN MstUsers MU ON AU.UsersId=MU.UsersId
	WHERE MU.UserName = LTRIM(@UserName)AND MU.IsDeleted IS NULL AND MU.IsActive !='N'
	
	SELECT @IsOutlookSynchEnable ='Y'

	
	SET @RoleName  =( SELECT TOP 1 RoleName FROM vwUsers WHERE UsersId = @UserId)

	SELECT ISNULL(@UserId,0) UserId, @FullName FullName, @RoleName RoleName,@OutlookFileName OutlookFileName ,@IsOutlookSynchEnable IsOutlookSynchEnable
	
	EXEC AccessDynamicGet @UserId

	DECLARE @DepartmnetID INT =( SELECT TOP 1 DepartmentId FROM vwUsers WHERE UsersId = @UserId)
	set @DepartmnetID=(select ISNULL(@DepartmnetID,0))
	SELECT @DepartmnetID DepartmentID

		
	EXEC AccessGetMainSection @UserId

   

END

GO


/*
SELECT * FROM aspnet_Membership
exec MasterUserGetUsersIdByEmailId 'pchavan@practiceleague.com'
*/

ALTER PROCEDURE [dbo].[MasterUserGetUsersIdByEmailId]
	@EmailId VARCHAR(50)
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @UserId INT = 0
	DECLARE @UserName VARCHAR(100) 
	DECLARE @FullName VARCHAR(100) 
	DECLARE @RoleName VARCHAR(100) 
	DECLARE @OutlookFileName VARCHAR(1000)
	DECLARE @IsOutlookSynchEnable CHAR(1)

	SELECT  @UserName=AU.UserName,
	@UserId= AU.UsersID, 
	@FullName = FullName ,
	@OutlookFileName=RIGHT(COALESCE(MU.OutlookCalendarFileName,''), CHARINDEX('/', REVERSE('/' + COALESCE(MU.OutlookCalendarFileName,''))) - 1) 
	FROM aspnet_Users AU
	INNER JOIN MstUsers MU ON AU.UsersId=MU.UsersId
	INNER JOIN aspnet_Membership ON AU.UserId=aspnet_Membership.UserId
	WHERE aspnet_Membership.Email=RTRIM(LTRIM(@EmailId))
	AND MU.IsDeleted IS NULL AND MU.IsActive !='N'
	
	SELECT @IsOutlookSynchEnable ='Y'
	
	SET @RoleName  =( SELECT TOP 1 RoleName FROM vwUsers WHERE UsersId = @UserId)

	SELECT ISNULL(@UserId,0) UserId, @UserName UserName, @FullName FullName, @RoleName RoleName,@OutlookFileName OutlookFileName ,@IsOutlookSynchEnable IsOutlookSynchEnable
	
	EXEC AccessDynamicGet @UserId

	DECLARE @DepartmnetID INT =( SELECT TOP 1 DepartmentId FROM vwUsers WHERE UsersId = @UserId)
	set @DepartmnetID=(select ISNULL(@DepartmnetID,0))
	SELECT @DepartmnetID DepartmentID
		
	EXEC AccessGetMainSection @UserId  

END


GO


		

		ALTER PROCEDURE [dbo].[MasterUserStillAdd] 
	
		AS
		BEGIN
			DECLARE @TotalUsersAllowed INT, @UsersCreated INT
			SELECT	@TotalUsersAllowed=	3

			SELECT @UsersCreated=
			(			
			SELECT ISNULL(COUNT(1),0) FROM MstUsers WHERE (IsActive= 'Y' OR IsActive IS NULL)  AND COALESCE(IsDeleted,'N')='N'
			)

			SELECT CAST(@TotalUsersAllowed AS VARCHAR(100))+'_'+ CAST(@TotalUsersAllowed-@UsersCreated AS VARCHAR(100)) RemainingCount
			
		END

GO

/*
EXEC StageApproval 0,0,90032, 1083,1
SELECT * FROM ##StageStatus
SELECT * FROM ContractTemplate
EXEC StageApproval @ContractId=0, @ContractTemplateId=0,@RequestId=90032,@ModifiedBy=1083,@GenerateContractFlag=0
*/
ALTER PROCEDURE [dbo].[StageApproval] --100004,1,110081,1083,1
@ContractId INT=NULL,
@ContractTemplateId INT,
@RequestId BIGINT=NULL,
@ModifiedBy INT=NULL,
@GenerateContractFlag INT=0,
@Approvaluri varchar(2000)='',
@UpdateFlag INT=0,
@Sender VARCHAR(50)=''
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @URL VARCHAR(2000)
	
	IF(@Approvaluri !='')
	 SET @URL=@Approvaluri;
	ELSE
	 SET @URL='https://app.contractpod.com/CP'

	EXEC StageApprovalStatus @ContractId, @RequestId,@ContractTemplateId,@Sender

	IF @GenerateContractFlag = 0
	BEGIN	
			DECLARE @MinLevel INT =  (
											SELECT MIN(sC.LevelID)
											FROM
											##StageStatus t
											INNER JOIN StageConfigurator sC ON sC.StageID = t.StageId 
											LEFT OUTER JOIN ContractApprovals cA ON cA.StageID = t.StageId  AND cA.RequestId = @RequestId
											WHERE t.[Status]> 0  AND ISNULL(cA.IsActive,'Y')='Y' 
										)
				
				UPDATE ContractApprovals
				SET ModifiedOn=GETDATE(),
					ModifiedBy=@ModifiedBy
				WHERE StageId IN (SELECT StageId FROM ContractApprovals WHERE RequestId=@RequestId AND ContractId=@ContractId AND RequestId=@RequestId)

				INSERT INTO  ContractApprovals(ContractID,RequestId,StageID,AssignedTo,IP,Addedon,Addedby,IsMailSend,ModifiedOn,Modifiedby)
				SELECT @ContractId,@RequestId RequestId,tmp.StageID,CA.UsersId, ISNULL(CA.IP,''), GETDATE()[Date], COALESCE(CA.Modifiedby ,CA.Addedby,0)Addedby,'N',GETDATE(),	CA.Modifiedby
				FROM  ##StageStatus tmp
				INNER JOIN StageConfigurator  CA ON tmp.StageId = CA.StageID
				WHERE tmp.[Status]>0 AND tmp.StageId NOT IN(Select StageId From ContractApprovals WHERE RequestId=@RequestId AND ContractId=@ContractId)
				AND CA.LevelID = @MinLevel
				
			--IF @UpdateFlag=1
				UPDATE ContractApprovals 
				SET IsActive='Y'
				WHERE ContractApprovalId IN (
				SELECT ContractApprovalId FROM
				(
				SELECT ApprovalActivityId, ContractApprovals.ContractApprovalId,
				ROW_NUMBER()OVER (PARTITION BY ContractApprovals.ContractApprovalId ORDER BY ContractApprovalActivity.ApprovalActivityId DESC) RNo,
				ContractApprovalActivity.isApproved
				FROM ContractApprovals
				LEFT OUTER JOIN ContractApprovalActivity ON ContractApprovals.ContractApprovalId= ContractApprovalActivity.ContractApprovalId
				WHERE StageID IN
				(SELECT StageID FROM ##StageStatus WHERE [Status]>0) 
				AND ContractApprovals.RequestId=1                     --- comment by kk on 7 july 2016
				--AND ContractApprovals.RequestId=@RequestId 
				)tmp WHERE RNO = 1 AND COALESCE(isApproved,'N')='N'    --- comment by kk on 7 july 2016
				--AND COALESCE(isApproved,'Y')='Y'
				)
				IF (SELECT ContractStatusID FROM ContractRequest WHERE RequestId=@RequestId)!=7 --Dont get triggered if in Awaiting Approval stage
				BEGIN
						UPDATE ContractApprovals SET IsActive='N' WHERE StageID IN(SELECT StageID FROM ##StageStatus WHERE [Status]=0) AND RequestId=@RequestId  ---AND RequestId=@RequestId condition added by kk on 6 july 2016

						INSERT INTO ContractApprovalActivity(ContractApprovalId,isApproved,Remarks,Addedon,Addedby,IP, IsDefaultApproved)
						SELECT CA.ContractApprovalId,'Y','Default approved',GETDATE(), CA.Addedby, CA.IP,'Y'
						FROM ContractApprovals CA INNER JOIN  ##StageStatus tmp
						ON CA.StageID=tmp.StageID  AND CA.RequestId=@RequestId		
						WHERE  ISNULL(CA.IsActive,'Y')='N' AND CA.ContractApprovalId NOT IN (SELECT ContractApprovalActivity.ContractApprovalId FROM ContractApprovalActivity WHERE isApproved='Y')
				--IF dbo.IsTableExists('##StageStatus') > 0  DROP TABLE ##StageStatus
				END 

				IF NOT EXISTS(SELECT 1 FROM ContractApprovals WHERE (ISNULL(IsActive,'Y')='Y') AND RequestId=@RequestId)
				BEGIN	
				    DECLARE @ContractStatusIds int;
					SET @ContractStatusIds= (SELECT Top 1 ContractStatusID FROM ContractRequest_ht WHERE RequestId=@RequestId AND ContractStatusID!=7 ORDER BY RequesthtId DESC)

					----- add on 15 july 2016 
					IF @ContractStatusIds=1 
					 IF EXISTS (SELECT 1 FROM ContractRequest_ht WHERE RequestId=@RequestId AND ContractStatusID!=7 AND ContractStatusID!=1 ) 
					    SET @ContractStatusIds= (SELECT Top 1 ContractStatusID FROM ContractRequest_ht WHERE RequestId=@RequestId AND ContractStatusID!=7 AND ContractStatusID!=1 ORDER BY RequesthtId DESC)


						---EXEC ContractStatusAddUpdate @RequestId, 12, @ModifiedBy,NULL --Negotiation Stage
						EXEC ContractStatusAddUpdate @RequestId, @ContractStatusIds, @ModifiedBy,NULL 
				END
				ELSE
						EXEC ContractStatusAddUpdate @RequestId, 7, @ModifiedBy,NULL --Awaiting Approval
	END

	ELSE
	BEGIN					
						SELECT CA.ContractApprovalId,CA.RequestId, CA.ContractID,CA.StageID,StageName,CA.AssignedTo,CA.IP,CA.Addedon,CA.Addedby,
						MU.FullName
						,ISNULL(CA.IsActive,'Y') IsActive, CA.IsMailSend
						, FORMAT(COALESCE(CA.Modifiedon,GETDATE()),'d-MMM-yyyy h:mm tt') Modifiedon
						,'Approval.aspx?ContractApprovalId='+CAST(CA.ContractApprovalId AS VARCHAR(10)) +'&RequestId='+CAST(CA.RequestId AS VARCHAR(10))+'&ContractID='+ CAST(CA.ContractID AS VARCHAR(10))+'&StageId='+CAST(CA.StageID AS VARCHAR(10)) linkToApproval
						,MU.Email Email --,'vdudhal@myuberall.com, vinod@myuberall.com' Email--MU.Email
						,'Approval Request (Request ID: '+CAST(CA.RequestId AS VARCHAR(10)) +')' MailSubject
						,'<font face=Arial size=2 color=#006666> Hello<br/><br/>' + 

						'<u><b>Approval Required for Request ID:</b>�'+CAST(CA.RequestId AS VARCHAR(10)) + '</u><br/><br/>' + 

						'A request was completed by '+ (SELECT FullName FROM vwUsers WHERE vwUsers.UsersId=CR.Addedby) +
						' for a contract (request ID '+CAST(CA.RequestId AS VARCHAR(10)) + ').  At least one response requires your approval.  You can view the approval request by following the link <a href='''+@URL+'''>'+@URL +' </a> and logging on to ContractPod�.<br><br>  '+
						'Kind regards<br/><br/>The ContractPod� team</font><br/><br/>' +						
						'<font face=Arial size=1 color=#006666>Need help? Please do not hesitate to contact our technical support�team on 0800 699 0045 or at help@contractpod.com.' +
						'The information in this email is confidential and for use by the addressee(s) only. If you are not the intended recipient, please notify us immediately on 0800 699 0045 and delete the message from your computer. You may not copy or forward the e-mail, or use it or disclose its contents to any other person. We do not accept any liability or responsibility for changes made to this email after it was sent, or viruses transmitted through this e-mail or any attachment.</font>' MailBody


						--,'Dear '+MU.FullName+',<br> The '+StageName+' of contract ID '+CAST(CA.ContractID AS VARCHAR(10)) +' of '+CR.ClientName +' is waiting for your approval.<br><br>Thanks,<br>Contract Management Team.' MailBody
				FROM ContractApprovals CA
				INNER JOIN vwUsers MU ON CA.AssignedTo=MU.UsersId
				INNER JOIN StageConfigurator SC ON CA.StageID=SC.StageID
				INNER JOIN ContractRequest CR ON CR.RequestId=CA.RequestId
				WHERE CA.RequestId=@RequestId AND CA.ContractId=@ContractId
				ORDER BY SC.LevelID,SC.StageID

				SELECT * FROM 
				(SELECT CA.ContractApprovalId, 'Sent for approval on '+CAST(FORMAT(CA.AddedOn, 'd-MMM-yyyy h:mm tt') AS VARCHAR(30)) +' to '+ MU.FullName ApprovalActivity,  0 SRNO
				FROM ContractApprovals CA
				INNER JOIN vwUsers MU ON CA.AssignedTo=MU.UsersId
				WHERE CA.RequestId=@RequestId AND CA.ContractId=@ContractId

				UNION

				SELECT CA.ContractApprovalId, CASE WHEN ISNULL(IsDefaultApproved,'N')='Y' THEN 'Stage eliminated by system due to change in metadata.'
				WHEN CAA.isApproved='Y' THEN 'Approved'
				WHEN CAA.isApproved='N' THEN 'Disapproved'				
				ELSE CHAR(32) END + ' on '+ CAST(FORMAT(CAA.Addedon, 'd-MMM-yyyy h:mm tt') AS VARCHAR(30))
				+'<br/>- '+CAA.Remarks
					ApprovalActivity, --+' by '+ MU.FullName
				ROW_NUMBER() OVER(ORDER BY CAA.ApprovalActivityId ASC) SRNO
				FROM ContractApprovals CA
				LEFT OUTER JOIN ContractApprovalActivity CAA ON CA.ContractApprovalId=CAA.ContractApprovalId
				INNER JOIN vwUsers MU ON CA.AssignedTo=MU.UsersId
				WHERE CA.RequestId=@RequestId AND CA.ContractId=@ContractId	
				)TMP 
				WHERE ApprovalActivity IS NOT NULL
				ORDER BY SRNO ASC

				--DECLARE @ContractStatusIds INT;
				SELECT @ContractStatusIds=ContractStatusID FROM ContractRequest WHERE RequestId=@RequestId

				IF(@ContractStatusIds!=8 AND @ContractStatusIds!=9 AND @ContractStatusIds!=10 AND @ContractStatusIds!=11)    ----Awaiting signatures,Active Contract,Expired Contract,Terminated Contract
				BEGIN
					SELECT SC.StageID, SC.StageName, 'Assigned to '+MU.FullName AssignedToUser
					FROM  ##StageStatus tmp
					INNER JOIN StageConfigurator  SC ON tmp.StageId = SC.StageID
					INNER JOIN vwUsers MU ON SC.UsersId=MU.UsersId
					WHERE tmp.[Status]>0 AND tmp.StageId NOT IN(Select StageId From ContractApprovals WHERE RequestId=@RequestId )
					--AND SC.ContractTypeId= @ContractTypeId      --- added by kk on 4 July 2016 ::  "AND SC.ContractTypeId= @ContractTypeId"
				END
				ELSE
				BEGIN
				    SELECT SC.StageID, SC.StageName, 'Assigned to '+MU.FullName AssignedToUser
					FROM  ##StageStatus tmp
					INNER JOIN StageConfigurator  SC ON tmp.StageId = SC.StageID
					INNER JOIN vwUsers MU ON SC.UsersId=MU.UsersId
					WHERE tmp.[Status]>0 AND 1=0
				END
				
			END		
END










