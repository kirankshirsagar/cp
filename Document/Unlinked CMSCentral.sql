/*DROP TABLE MstCountry
SELECT * INTO MstCountry
FROM CMSCentral.DBO.MstCountry

DROP TABLE MstState
SELECT * INTO MstState
FROM CMSCentral.DBO.MstState

DROP TABLE MstCity
SELECT * INTO MstCity
FROM CMSCentral.DBO.MstCity
GO
*/
GO

ALTER PROCEDURE [dbo].[AvailableClientsAddUpdateNew]
@ClientId INT=0,
@StateName VARCHAR(2000)=0,
@ClientName VARCHAR(50)=null,
@Address Varchar(max)=null,
@CountryId INT=0,
@CityName  VARCHAR(2000)=0,
@Pincode Varchar(10)=null,
@ContactNo VARCHAR(50)=NULL,
@EmailID VARCHAR(50)=NULL,
@AddedBy VARCHAR(50)=NULL,
@ModifiedBy VARCHAR(50)=NULL,
@IpAddress VARCHAR(50)=NULL,
@isCustomer varchar(2)=null,
@Param Int=0,
@Street Varchar(max)=null,
@Status INT OUT

AS
BEGIN


 IF @Param=1--Insert Client Name in Client table
    BEGIN
	IF NOT EXISTS(SELECT ClientName FROM Client WHERE ClientName = @ClientName )
	BEGIN
	INSERT INTO  Client (isCustomer,ClientName,Addedby,Addedon,IP) VALUES (@isCustomer,@ClientName,@AddedBy,GETDATE(),@IpAddress)
	SET @Status = (SELECT MAX(ClientId) FROM Client);
	END
	ELSE
	BEGIN
	
	SET @Status =0
	END

	END
		 
      IF @Param=2--Insert Address of client  deatils in ClientAddressDetails table 
	  BEGIN
			INSERT INTO ClientAddressDetails( ClientId,IsPrimary,[Address],Street2,CountryId,StateName,CityName,Pincode )  
			VALUES (@ClientID,'Y',@Address,@Street,@CountryId,@StateName,@CityName,@Pincode)
       END

      IF @Param=3--Insert Contact details in ClientContactDetails table 
	  BEGIN
			INSERT INTO  ClientContactDetails(ClientId,IsPrimary,ContactNumber)  
			VALUES (@ClientID,'Y',@ContactNo)
      END

	  IF @Param=4--Insert Email  details in ClientEmaildetails table 
	  BEGIN
			INSERT INTO  ClientEmaildetails(ClientId,IsPrimary,EmailID)  
			VALUES (@ClientID,'Y',@EmailID)
					
	END

	 IF @Param=5--View ClientAddressDetails,ClientContactDetails,ClientEmaildetails table details
	  BEGIN
	      SET @Status =0;
	       select isCustomer,ClientName,ClientId from Client where ClientId=@ClientId
		   select addr.[Address],addr.[Street2],country.CountryName as Country, addr.StateName as [State] , addr.CityName as [City], addr.PinCode as PinCode, addr.CountryId as CountryID, addr.StateName as StateName,addr.CityName as CityName
		   ,dbo.GetAddressIsUsed(@ClientId,addr.ClientAddressdetailId,'A') IsUsed
		   from  dbo.ClientAddressDetails as addr LEFT OUTER JOIN		 
		   MstCountry as country ON addr.CountryId = country.CountryId
		   where addr.ClientId=@ClientId

		   select ContactNumber as Mobile,dbo.GetAddressIsUsed(@ClientId,ClientContactDetails.ClientContactdetailId,'M') IsUsed from ClientContactDetails where ClientId=@ClientId

		   select EmailID,dbo.GetAddressIsUsed(@ClientId,ClientEmaildetails.ClientEmaildetailId,'E') IsUsed from ClientEmaildetails where ClientId=@ClientId

 	 END

	IF @Param=6-- Update Client
	Begin
	IF NOT EXISTS(SELECT ClientName FROM Client WHERE ClientName=@ClientName and  ClientId <> @ClientId   )
     BEGIN
			UPDATE Client SET isCustomer=@isCustomer,ClientName=@ClientName,Modifiedby=@ModifiedBy,Modifiedon=GETDATE(),IP=@IpAddress WHERE ClientId=@ClientId 
			 SET @Status = 1
			SELECT @Status 
		END
		ELSE
	BEGIN
	   SET @Status =2
	    SELECT @Status
	END
	END

	IF @Param=7-- delete all child table with respect to clientid
	BEGIN     
	        delete from  ClientAddressDetails where ClientId=@ClientId 
			delete from  ClientContactDetails where ClientId=@ClientId 
            delete from  ClientEmaildetails where ClientId=@ClientId  	
	END

		
END





GO

/*
AvailableClientsRead 0,1,100,'','',0
*/

ALTER PROCEDURE [dbo].[AvailableClientsRead] 
	@ClientId INT = 0,
	@PageNo INT = 1 ,
	@RecordsPerPage INT = 10,
	@Search VARCHAR(100)= '',
	@SortColumn VARCHAR(100)='',
	@Direction INT = 0
AS
BEGIN
	SET NOCOUNT ON;
		
	IF @PageNo = 0 SET @PageNo = 1
	IF @RecordsPerPage = 0 SET @RecordsPerPage = 25


	IF @ClientId> 0 
		BEGIN
			SELECT    c.ClientId,c.ClientName,ca.Address,ca.StateName,ca.CountryId,CountryName,ca.CityName,
			ca.PinCode,cc.ContactNumber,ce.EmailID,isCustomer
			FROM  
			Client c
			INNER JOIN ClientAddressDetails ca ON c.ClientId = ca.ClientId AND ca.ClientAddressdetailId=(SELECT TOP 1 ClientAddressdetailId FROM ClientAddressDetails WHERE ClientId =C.ClientId)
			INNER JOIN ClientContactDetails cc ON c.ClientId = cc.ClientId AND cc.ClientContactdetailId=(SELECT TOP 1 ClientContactdetailId FROM ClientContactDetails WHERE ClientId =C.ClientId)
			INNER JOIN ClientEmaildetails ce ON c.ClientId = ce.ClientId AND ce.ClientEmaildetailId=(SELECT TOP 1 ClientEmaildetailId FROM ClientEmaildetails WHERE ClientId =C.ClientId)
			
			INNER JOIN MstCountry mc ON mc.CountryId=ca.CountryId
			
		    WHERE c.ClientId=@ClientId and c.ClientName is not null
		END
	ELSE
	BEGIN
		IF @PageNo < 1
		BEGIN
			SET @PageNo = 1
		END

			IF dbo.IsTableExists('TA')=1 DROP TABLE TA
			SELECT 
			cl.ClientName,
			dbo.GetClientAddress(cl.ClientId,'A') [Address],
			dbo.GetClientAddress(cl.ClientId,'E') AS EmailID,
			dbo.GetClientAddress(cl.ClientId,'C')ContactNumber ,
			'' AS PinCode,
			ISNULL(cl.ClientId,0)ClientID,
			'' AS StateName,
			'0' AS CountryId,
			'' AS CountryName,
			'' AS CityName,				
			(SELECT COALESCE((SELECT MAX('Y') FROM ContractRequest WHERE ContractRequest.ClientId = cl.ClientId), 'N'))isUsed
			,ROW_NUMBER() OVER (PARTITION BY cl.ClientId ORDER BY cl.ClientId ASC) RNO,
			cl.isCustomer,
			cl.Addedon
			INTO TA
			FROM Client cl
			GROUP BY cl.ClientName ,dbo.GetClientAddress(cl.ClientId,'A'),dbo.GetClientAddress(cl.ClientId,'E'),dbo.GetClientAddress(cl.ClientId,'C'),cl.ClientId,
			cl.isCustomer,cl.Addedon
		
				SELECT * FROM TA			
				WHERE ClientName IS NOT NULL AND
				( ClientName LIKE '%'+ @Search +'%' 
				OR [Address] LIKE '%'+ @Search +'%'		
				OR ContactNumber LIKE '%'+ @Search +'%' 
				OR EmailID LIKE '%'+ @Search +'%' )				
				ORDER BY
			    CASE WHEN @SortColumn='' AND @Direction = 0  THEN TA.Addedon END DESC,
				CASE WHEN @SortColumn =  'Customer/Supplier /Others Name'  AND @Direction = 0 THEN ClientName END ASC, 
				CASE WHEN @SortColumn =  'Contact Number(s)'  AND @Direction = 0 THEN TA.ContactNumber END ASC,
				CASE WHEN @SortColumn =  'Email ID(s)'  AND @Direction = 0 THEN TA.EmailID END ASC, 
				CASE WHEN @SortColumn =  'Address'  AND @Direction = 0 THEN TA.[Address] END ASC, 				
		 			   
				CASE WHEN @SortColumn =  'Customer/Supplier /Others Name'  AND @Direction = 1 THEN ClientName END DESC, 
				CASE WHEN @SortColumn =  'Contact Number(s)'  AND @Direction = 1 THEN TA.ContactNumber END DESC, 
				CASE WHEN @SortColumn =  'Email ID(s)'  AND @Direction = 1 THEN TA.EmailID  END DESC, 
				CASE WHEN @SortColumn =  'Address'  AND @Direction = 1 THEN TA.[Address] END DESC
				
				OFFSET ( @PageNo - 1 ) * @RecordsPerPage ROWS
		         FETCH NEXT @RecordsPerPage ROWS ONLY
			 		
				SELECT  COUNT(*)  AS TotalRecords
                FROM TA
				WHERE ClientName IS NOT NULL AND
				( ClientName LIKE '%'+ @Search +'%'
				OR [Address] LIKE '%'+ @Search +'%'
				OR ContactNumber LIKE '%'+ @Search +'%'
				OR EmailID LIKE '%'+ @Search +'%' )
	
	END
	
END







GO



/*

exec BulkImportAdd 10119



SELECT * FROm Client

SELECT * FROM ClientAddressDetails

SELECT * FROM ClientContactDetails

SELECT * FROm ContractRequest

*/
GO


ALTER PROCEDURE [dbo].[BulkImportAdd]
@BulkImportID INT
	
AS
BEGIN
	
	SET NOCOUNT ON

	IF dbo.isTableExists('#tmpBulk') > 0 DROP TABLE #tmpBulk
	DECLARE @trancount INT;
    SET @trancount = @@trancount;

	CREATE TABLE #tmpBulk (BulkImportID INT, ContractTemplateId INT, BulkImportStagingId INT, asAnswer VARCHAR (MAX), 
	ColumnName VARCHAR(500), ColumnNameValue VARCHAR(1000),Addedby INT, Addedon DATETIME)


	DECLARE @Qry VARCHAR(MAX)
	DECLARE @Columns VARCHAR(MAX)
	DECLARE @RequestContractIDMax BIGINT
	DECLARE @ContractIDMax BIGINT
	
	DECLARE @ContractTemplateId INT = (SELECT MIN(ContractTemplateId) FROM BulkImport WHERE BulkImportID = @BulkImportID)
	DECLARE @ContractTypeId INT = ( SELECT ContractTypeId FROM ContractTemplate WHERE ContractTemplateId = @ContractTemplateId)
	
	DECLARE @AddedById INT
	DECLARE @IPAddress VARCHAR(100)

	DECLARE @FullName VARCHAR(1000)
	DECLARE @DepartmentId INT
	DECLARE @RoleID INT
    SELECT @AddedById = MIN(ModifiedBy), @IPAddress = MIN(IPAddress) FROM BulkImport WHERE BulkImportID = @BulkImportID
    SELECT @FullName = FullName, @DepartmentId = DepartmentId FROM MstUsers WHERE UsersId = @AddedById

	SET @RoleID=(SELECT TOP 1 RoleId FROM MstUsers WHERE UsersId=@AddedById)
	IF (SELECT COUNT(1) FROM BulkImportStagingHeader WHERE BulkImportId= @BulkImportId) = (SELECT COUNT(1) FROM vwBulkImportKeyFields)
	BEGIN
	--	SELECT COALESCE(@Columns + ', ','') + ColumnName FROM BulkImportStagingHeader WHERE BulkImportID = @BulkImportID 
		SELECT @Columns = COALESCE(@Columns + ', ','') + ColumnName FROM BulkImportStagingHeader WHERE BulkImportID = @BulkImportID 
		--AND ColumnNameValue NOT IN (SELECT FieldName FROM vwBulkImportKeyFields) 
	END
	ELSE
	BEGIN
		
		SELECT @Columns = COALESCE(@Columns + ', ','') + ColumnName FROM BulkImportStagingHeader WHERE BulkImportID = @BulkImportID 
		AND ColumnNameValue NOT IN (SELECT FieldName FROM vwBulkImportKeyFields) 
	END
	
	SET @Qry = ' SELECT unPVT.BulkImportID, unPVT.ContractTemplateId, unPVT.BulkImportStagingId, unPVT.asAnswer, 
						   unPVT.ColumnName,uH.ColumnNameValue,unPVT.Addedby, unPVT.Addedon FROM BulkImportStaging 
				UNPIVOT 
				(
				 asAnswer FOR ColumnName IN ('+ @Columns +') 
				) unPVT
				INNER JOIN BulkImportStagingHeader uH ON uH.ColumnName = unPVT.ColumnName AND uH.BulkImportID = unPVT.BulkImportID
				WHERE unPVT.BulkImportID = ' + CAST(@BulkImportID AS VARCHAR(10)) + ' AND unPVT.Head=''N'' AND unPVT.IsPass= ''Y''		
				AND  unPVT.BulkImportStagingId NOT IN(SELECT ISNULL(ContractRequest.BulkImportStagingId,0) FROM ContractRequest)'

	INSERT INTO #tmpBulk (BulkImportID, ContractTemplateId, BulkImportStagingId, asAnswer, 
						  ColumnName, ColumnNameValue, Addedby, Addedon)
	EXEC (@Qry)


	IF (SELECT COUNT(1) FROm #tmpBulk)> 0
	BEGIN
			
		--SELECT  @RequestContractIDMax = MAX(ISNULL(RequestContractID,100000))+1 , @ContractIDMax = MAX(ISNULL(ContractID,100000))+1 FROM ContractRequest

		SELECT  @RequestContractIDMax = ISNULL(MAX(RequestContractID),100000)+1 , @ContractIDMax = ISNULL(MAX(ContractID),100000)+1 FROM ContractRequest

		BEGIN TRY
	
		IF @trancount = 0
            BEGIN TRANSACTION
		
        ELSE
            SAVE TRANSACTION usp_my_procedure_name;
		
		
		INSERT INTO ContractRequest 
		(
		RequestContractID, 
		ContractID, 
		ContractTypeId, RequestTypeId, ContractTemplateId, RequesterUserId, 
		RequestDescription, AssignToDepartmentId, AssignToUserID, 
		IsApprovalRequired, Addedon, Addedby,Modifiedon,Modifiedby,  IP, BulkImportStagingId, IsBulkImport, 
		ContractStatusID
		)
		
		SELECT 
		ROW_NUMBER() OVER (ORDER BY s.BulkImportStagingId)+ @RequestContractIDMax, 
		ROW_NUMBER() OVER (ORDER BY s.BulkImportStagingId)+ @ContractIDMax, 
		@ContractTypeId, 1, @ContractTemplateId, s.AddedBy, 
		'Bulk Imported by: '+ @FullName + ' on: '+ dbo.DateFormat(s.Addedon), @DepartmentId, s.AddedBy, 
		'Y',s.Addedon,s.Addedby,GETDATE(),s.Addedby, s.IP, s.BulkImportStagingId, 'Y', 
		9 
		-- (All bulk imported are bydefault Active Contracts )
		FROM BulkImportStaging s
		WHERE s.BulkImportID = @BulkImportID AND s.IsPass = 'Y' 
		
		INSERT INTO ContractQuestionsAnswers 
		(
		ContractId, RequestId, ContractTemplateId, FieldLibraryID, Question,
		TextValue, NumericValue, DecimalValue, DateValue, ForeignKeyValye,
		IsDraft, Addedon, Addedby, Modifiedon, Modifiedby, IP, IsBulkImport
		)
    
		SELECT cR.ContractId,cR.RequestId, t.ContractTemplateId, uF.FieldLibraryID, ColumnNameValue,
		IIF (uF.FieldTypeID = 1, t.asAnswer, NULL) TextValue, 
		IIF (uF.FieldTypeID = 4, t.asAnswer, NULL) NumericValue,
		IIF (uF.FieldTypeID = 5, t.asAnswer, NULL) DecimalValue, 
		IIF (uF.FieldTypeID = 7, t.asAnswer, NULL) DateValue, 
		IIF (uF.FieldTypeID = 9,
						(SELECT MAX(MstView.Id) FROM FieldLibraryValues v 
						 INNER JOIN MstView ON MstView.PickListMasterId = v.FieldValue 
						 AND MstView.Name = t.asAnswer COLLATE SQL_Latin1_General_CP1_CI_AS 
						 WHERE v.FieldLibraryID = uF.FieldLibraryID), NULL) ForeignKeyValye, 
		NULL,t.Addedon,t.Addedby,GETDATE(),t.Addedby, @IPAddress,'Y'
		FROM #tmpBulk t
		INNER JOIN FieldLibrary uF ON uF.FieldName = t.ColumnNameValue COLLATE SQL_Latin1_General_CP1_CI_AS 
		AND uF.ContractTemplateId = t.ContractTemplateId
		INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = t.BulkImportStagingId
		ORDER  BY cR.RequestId


-------------------- Added by kiran for Audit Trail Log on 16 May 2016	
BEGIN
		INSERT INTO AuditTrail 
		(
		RequestId,ContractId,UserId,ActionTaken,ActionDateTime,SectionName,IP
		)    
		SELECT DISTINCT cR.RequestId, cR.ContractId,t.Addedby,'Add',GETDATE(),'Contract','Bulk Import'
		FROM #tmpBulk t
		INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = t.BulkImportStagingId
		ORDER  BY cR.RequestId
END
-------------------- End----------------------------


		-- updating key fields
		
		SET @Columns = NULL
		SELECT @Columns = COALESCE(@Columns + ', ','') +'['+ TableColumnName + ']='+  ColumnName 
		FROM
		(
		SELECT 
		uK.TableColumnName, 
		CASE WHEN  uK.Ans ='user' THEN 'dbo.GetUserId('+ uH.ColumnName +')' 
			 WHEN  uK.Ans ='Status' THEN 'dbo.GetContractStatusId('+ uH.ColumnName +')' 
			 WHEN  uk.Ans='Currency' THEN 'CAST(ISNULL((SELECT TOP 1 CurrencyId FROM MstCurrency WHERE CurrencyCode=uB.'+ uH.ColumnName +'),0) AS VARCHAR(5))'
		     WHEN  uK.Ans ='Yes,No' AND uK.TableColumnName<>'IsCustomerPortalEnable' THEN ' CASE WHEN '+ uH.ColumnName + '=''Yes'' THEN ''Y'' ELSE ''N'' END '
			 WHEN  uK.Ans ='Yes,No' AND uK.TableColumnName='IsCustomerPortalEnable' THEN ' CASE WHEN '+ uH.ColumnName + '=''No'' THEN ''N'' 
			 WHEN ISNULL(dbo.IsHaveAccess('''','+CONVERT(VARCHAR(50),ISNULL(@ContractTypeId,0))+','+CONVERT(VARCHAR(50),ISNULL(@RoleID,0))+'),'''')='''' THEN ''N'' ELSE ''Y'' END '
			 ELSE 'uB.'+ uH.ColumnName END ColumnName
		FROM BulkImportStagingHeader uH
		INNER JOIN vwBulkImportKeyFields uK ON uK.FieldName = uH.ColumnNameValue  
		WHERE uH.BulkImportID = @BulkImportID
		AND uH.ColumnNameValue IN (SELECT FieldName FROM vwBulkImportKeyFields WHERE vwBulkImportKeyFields.Flag in('U','B')  AND ParentID=0)
		AND LEN(uK.TableColumnName)>0
		)tmp

			print 1

		SET @Qry = 'UPDATE ContractRequest SET ' + @Columns + ' FROM BulkImportStaging uB INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = uB.BulkImportStagingId WHERE uB.Head=''N'' AND uB.BulkImportID = ' + CAST(@BulkImportID AS VARCHAR(10))
	    
		EXEC (@Qry)
		

        UPDATE ContractRequest SET
		ContractRequest.ContractStatusId= 12  FROM 
	    (SELECT cR.RequestId  RequestId FROM BulkImportStaging uB INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = uB.BulkImportStagingId WHERE uB.Head='N' AND uB.BulkImportID =CAST(@BulkImportID AS VARCHAR(10))) tbl WHERE tbl.[RequestId]=ContractRequest.[RequestId]



		 UPDATE ContractRequest SET
		 ContractRequest.CountryId= (SELECT CountryId FROM MstCountry WHERE CountryName=RTRIM(LTRIM(ContractRequest.CountryName)))  FROM 
	    (SELECT cR.RequestId  RequestId FROM BulkImportStaging uB INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = uB.BulkImportStagingId WHERE uB.Head='N' AND uB.BulkImportID =CAST(@BulkImportID AS VARCHAR(10))) tbl WHERE tbl.[RequestId]=ContractRequest.[RequestId]

				--------------------Meta Fields INSERT----------------------------------------
		SET @Columns=NULL
		SET @Qry=NULL
	SELECT @Columns = COALESCE(@Columns + ', ','') + ColumnName FROM BulkImportStagingHeader WHERE BulkImportID = @BulkImportID
		AND ColumnNameValue IN (SELECT FieldName FROM vwBulkImportKeyFields where Flag NOT IN('U','B') AND ParentID=0) 


	SET @Qry='SELECT BulkImportStagingId, Col, Val
	FROM
	(SELECT BulkImportStagingId,
	'+@Columns+' 
	FROM BulkImportStaging WHERE Head=''N'' AND  BulkImportID = '+CAST(@BulkImportID AS VARCHAR(10))+' ) stu  
	UNPIVOT
	(VAL FOR Col IN ('+@Columns+')
	) AS val1'
	IF dbo.isTableExists('#Tmp') > 0 DROP TABLE #Tmp
	CREATE TABLE #Tmp(BulkImportStagingId INT,Col VARCHAR(150),Val VARCHAR(500),FieldId INT,Ans VARCHAR(150))

	INSERT INTO #Tmp(BulkImportStagingId,Col,Val)
	EXEC(@Qry)

	UPDATE #Tmp SET #Tmp.FieldId=uK.SrNo,#Tmp.Ans=uK.Ans
	 from 
	 #Tmp INNER JOIN BulkImportStagingHeader uH ON #Tmp.Col=uH.ColumnName
	INNER JOIN [vwBulkImportKeyFields] uK ON uK.FieldName= uH.ColumnNameValue AND uK.Flag NOT IN('U','B') AND uH.BulkImportID=@BulkImportID

	INSERT INTO ContractMetaData
	SELECT cR.RequestId,cR.ContractId,#Tmp.FieldId,
	CASE WHEN Ans='Checkbox' THEN	
		STUFF((SELECT ',' + CAST(FieldOptionId AS varchar(150)) FROM MetaDataConfiguratorValues WHERE MetaDataConfiguratorValues.FieldId=#Tmp.FieldId 
		AND MetaDataConfiguratorValues.FieldOptionValue in(SELECT items FROM dbo.Split(CONVERT(VARCHAR(50),#Tmp.Val),','))
		FOR XML PATH('')), 1, 1, '')
	WHEN Ans='Dropdown' OR Ans='RadioButton' THEN 
	CAST((SELECT TOP 1 FieldOptionId FROM MetaDataConfiguratorValues WHERE MetaDataConfiguratorValues.FieldId=#Tmp.FieldId AND MetaDataConfiguratorValues.FieldOptionValue=CONVERT(VARCHAR(50),#Tmp.Val))AS varchar(150))
	--WHEN ISDATE(Val)=1 THEN dbo.DateFormat(Val)
	WHEN ISDATE(Val)=1 THEN IIF(LEN(Val)=4,Val, dbo.DateFormat(Val))
	ELSE Val END Val,
	m.MetaDataType,
	cR.AddedBy,
	cR.AddedOn,
	cR.IP FROM #Tmp
	INNER JOIN MetaDataConfigurator m ON m.FieldId=#Tmp.FieldId
	INNER JOIN ContractRequest cR ON cR.BulkImportStagingId=#Tmp.BulkImportStagingId  

		--------------------END Meta Fields INSERT------------------------------------
		-----------------------INSERT Metadata Reminder ----------------------------------
SET @Columns=NULL
SET @Qry=NULL
SELECT @Columns = COALESCE(@Columns + ', ','') + ColumnName FROM BulkImportStagingHeader WHERE BulkImportID = @BulkImportID
	AND ColumnNameValue IN (SELECT FieldName FROM vwBulkImportKeyFields where Flag IN('U','B') AND ParentID<>0 ) 

	SET @Qry='SELECT BulkImportStagingId, Col, Val
	FROM
	(SELECT BulkImportStagingId,
	'+@Columns+' 
	FROM BulkImportStaging WHERE Head=''N'' AND  BulkImportID = '+CAST(@BulkImportID AS VARCHAR(10))+' ) stu  
	UNPIVOT
	(VAL FOR Col IN ('+@Columns+')
	) AS val1'

	IF dbo.isTableExists('#Tmp1') > 0 DROP TABLE #Tmp1
	CREATE TABLE #Tmp1(ID INT IDENTITY(1,1),BulkImportStagingId INT,Col VARCHAR(1000),Val VARCHAR(1000),FieldId INT,Ans VARCHAR(1000),reminder  VARCHAR(1000))
	
	INSERT INTO #Tmp1(BulkImportStagingId,Col,Val)
	EXEC(@Qry)
	UPDATE #Tmp1 SET #Tmp1.FieldId=uK.ParentID,#Tmp1.Ans=uK.Ans,#Tmp1.reminder=uk.TableColumnName
	 from 
	 #Tmp1 INNER JOIN BulkImportStagingHeader uH ON #Tmp1.Col=uH.ColumnName
	INNER JOIN [vwBulkImportKeyFields] uK ON uK.FieldName= uH.ColumnNameValue AND uK.Flag IN('U','B')  AND uK.ParentID<>0 AND uH.BulkImportID=@BulkImportID

IF dbo.isTableExists('#TmpReminder') > 0 DROP TABLE #TmpReminder
	CREATE TABLE #TmpReminder(ID INT IDENTITY(1,1),RequestId INT,ContractId INT,FieldId INT,[Days] INT,User1 INT,User2 INT,Reminder VARCHAR(500))
	DECLARE @ID INT=1,@Maxcount INT
	SELECT @Maxcount=COUNT(1) FROM  #Tmp1
	

	WHILE @ID<=@Maxcount
	BEGIN
		INSERT INTO #TmpReminder

		SELECT cR.RequestId,cR.ContractId,#Tmp1.FieldId,
			CASE WHEN Ans='Yes,No' AND reminder='ReminderAlert1days' THEN 1
			WHEN  Ans='Yes,No' AND reminder='ReminderAlert7days' THEN 7
			WHEN  Ans='Yes,No' AND reminder='ReminderAlert30days' THEN 30
			WHEN  Ans='Yes,No' AND reminder='ReminderAlert60days' THEN 60
			WHEN  Ans='Yes,No' AND reminder='ReminderAlert90days' THEN 90
			WHEN  Ans='Yes,No' AND reminder='ReminderAlert180days' THEN 180	 END [days],	
			CASE WHEN Ans ='user' AND (reminder='ReminderAlert1dayUserID1' OR reminder='ReminderAlert7daysUserID1' OR
			reminder='ReminderAlert30daysUserID1' OR reminder='ReminderAlert60daysUserID1' OR reminder='ReminderAlert90daysUserID1'OR
			reminder='ReminderAlert180daysUserID1') THEN dbo.GetUserId(#Tmp1.Val) END [User1],
			CASE WHEN Ans ='user' AND (reminder='ReminderAlert1dayUserID2' OR reminder='ReminderAlert7daysUserID2' OR
			reminder='ReminderAlert30daysUserID2' OR reminder='ReminderAlert60daysUserID2' OR reminder='ReminderAlert90daysUserID2'OR
			reminder='ReminderAlert180daysUserID2') THEN dbo.GetUserId(#Tmp1.Val) END [User2]
			,reminder
		FROM #Tmp1 INNER JOIN ContractRequest cR ON cR.BulkImportStagingId=#Tmp1.BulkImportStagingId AND #Tmp1.ID=@ID

		SET @ID=@ID+1;
	END

IF dbo.isTableExists('#TmpFinalReminder') > 0 DROP TABLE #TmpFinalReminder
	CREATE TABLE #TmpFinalReminder(ID INT IDENTITY(1,1),RequestId INT,ContractId INT,FieldId INT,[Days] INT,User1 INT,User2 INT,PreID INT)

	INSERT INTO #TmpFinalReminder(RequestId,ContractId,FieldId,[Days],PreID)
	SELECT RequestId,ContractId,FieldId,[Days],ID FROM #TmpReminder WHERE Reminder IN('ReminderAlert1days',
	'ReminderAlert7days','ReminderAlert30days','ReminderAlert60days','ReminderAlert90days','ReminderAlert180days')

UPDATE #TmpFinalReminder SET #TmpFinalReminder.User1= #TmpReminder.User1 FROM #TmpReminder
 INNER JOIN  #TmpFinalReminder ON  #TmpFinalReminder.PreID=#TmpReminder.ID-1 AND Reminder 
	IN('ReminderAlert1dayUserID1',
	'ReminderAlert7daysUserID1','ReminderAlert30daysUserID1','ReminderAlert60daysUserID1','ReminderAlert90daysUserID1','ReminderAlert180daysUserID1')

UPDATE #TmpFinalReminder SET #TmpFinalReminder.User2= #TmpReminder.User2 
FROM #TmpReminder
 INNER JOIN  #TmpFinalReminder ON  #TmpFinalReminder.PreID= CASE WHEN #TmpFinalReminder.user1 is not null THEN #TmpReminder.ID-2 ELSE #TmpReminder.ID-1 END  AND Reminder 
	IN('ReminderAlert1dayUserID2',
	'ReminderAlert7daysUserID2','ReminderAlert30daysUserID2','ReminderAlert60daysUserID2','ReminderAlert90daysUserID2','ReminderAlert180daysUserID2')

	INSERT INTO ImportantDatesMetaDataReminders(RequestId,ContractId,MetaDataFieldId,ReminderDays,ReminderUser1,ReminderUser2)
		select RequestId,ContractId,FieldId,Days,User1,User2 from #TmpFinalReminder
-------------------------END MetaData Reminder-------------------------------------------
		/**Added By Nilesh to add Client Details*****/
		
		
		CREATE TABLE #tmpClientInformation (ID INT IDENTITY (1,1), RequestId INT, ClientName VARCHAR(MAX), ContactNumber VARCHAR(MAX), EmailId VARCHAR (MAX), isCustomer VARCHAR(MAX),
	    AddedBy INT, Addedon DATETIME,ModifiedBy INT,ModifiedOn DATETIME,IP VARCHAR(MAX),Address VARCHAR(MAX),CountryName VARCHAR(MAX),ContractingPartyName VARCHAR(MAX),CityName VARCHAR(MAX),StateName VARCHAR(MAX),Pincode VARCHAR(MAX))



		INSERT INTO #tmpClientInformation  
        SELECT cR.RequestId ,cR.ClientName,cR.ContactNumber,cR.EmailId,cR.isCustomer,cR.AddedBy,cR.Addedon,cR.Modifiedby,cR.Modifiedon,cR.IP,cR.Address,cR.CountryName,cR.ContractingPartyName,cR.CityName,cR.StateName,cR.PinCode
		FROM BulkImportStaging uB INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = uB.BulkImportStagingId
		WHERE uB.Head='N' AND uB.BulkImportID =CAST(@BulkImportID AS VARCHAR(10))

	
	DECLARE @Cnt INT =1
	DECLARE @MaxCnt INT = (SELECT COUNT(1) FROM #tmpClientInformation)
	DECLARE @ClientId INT
	DECLARE @CountryId INT
	DECLARE @RequestId INT, @ClientName VARCHAR(MAX), @ContactNumber VARCHAR(MAX), @EmailId VARCHAR (MAX),@isCustomer VARCHAR(MAX), @AddedBy INT, @Addedon DATETIME,@ModifiedBy INT,@ModifiedOn DATETIME,@IP VARCHAR(MAX),@Address VARCHAR(MAX),@CountryName VARCHAR(MAX),@ContractingPartyName VARCHAR(MAX),
	@CityName VARCHAR(MAX),@StateName VARCHAR(MAX),@Pincode VARCHAR(MAX)

	DECLARE @ActivityTypeID INT
		SET @ActivityTypeID=(SELECT ActivityTypeId FROM MstActivityType WHERE  isSystemType='K')

	WHILE @Cnt <=@MaxCnt
	BEGIN
			BEGIN

			SELECT  @RequestId=t.[RequestId] , @ClientName=t.[ClientName],@isCustomer=CASE WHEN t.[isCustomer]='Supplier' THEN 'N' WHEN t.[isCustomer]='Customer' THEN 'Y' ELSE 'O' END , @ContactNumber=t.[ContactNumber] , 
			@EmailId=t.[EmailId], @AddedBy=t.[AddedBy], @Addedon=t.[Addedon],@ModifiedBy=t.[ModifiedBy],@ModifiedOn=t.[ModifiedOn],@IP=t.[IP],@Address=t.[Address],@CountryName=t.[CountryName],@ContractingPartyName=t.[ContractingPartyName],
			@CityName=t.[CityName],@StateName=t.[StateName],@Pincode=t.[PinCode]
			FROM #tmpClientInformation t WHERE ID=@Cnt

			SET @CountryId=(SELECT CountryId FROm MstCountry WHERE CountryName=@CountryName)

			IF NOT EXISTS(SELECT 1 FROm Client WHERE ClientName=@ClientName)
			BEGIN
				INSERT INTO Client (isCustomer,ClientName,Addedon,Addedby,Modifiedon,Modifiedby,IP)
				SELECT @isCustomer,@ClientName,@Addedon,@AddedBy,@ModifiedOn,@ModifiedBy,@IP
				SET @ClientId=SCOPE_IDENTITY()
			END
			ELSE
			BEGIN
			  SET @ClientId=(SELECT TOP 1 ClientId FROM Client WHERE ClientName=@ClientName)
			END

			IF NOT EXISTS(SELECT 1 FROM ClientAddressDetails WHERE ISNULL(Address,'')=ISNULL(@Address,'') AND CountryId=@CountryId AND ISNULL(PinCode,'')=ISNULL(@PinCode,'') AND ISNULL(CityName,'')=ISNULL(@CityName,'') AND ISNULL(StateName,'')=ISNULL(@StateName,'') AND ClientId=@ClientId)
			BEGIN
				INSERT INTO ClientAddressDetails(ClientId,IsPrimary,Address,CountryId,StateId,CityId,PinCode,Street2,CityName,StateName)
				SELECT @ClientId,'Y',@Address,@CountryId,NULL,NULL,@Pincode,NULL,@CityName,@StateName
			END

		IF(ISNULL(@ContactNumber,'')!='')
			IF NOT EXISTS(SELECT 1 FROM ClientContactDetails WHERE ContactNumber=@ContactNumber AND ClientId=@ClientId)
			BEGIN	
				INSERT INTO ClientContactDetails(ClientId,IsPrimary,ContactNumber)
				SELECT @ClientId,'Y',@ContactNumber
			END
	
	    IF(ISNULL(@EmailId,'')!='')
			IF NOT EXISTS(SELECT 1 FROM ClientEmailDetails WHERE EmailID=@EmailId AND ClientId=@ClientId)
			BEGIN		
				INSERT INTO ClientEmailDetails(ClientId,IsPrimary,EmailID)
				SELECT @ClientId,'Y',@EmailId
			END

		DECLARE @ContractingPartyId INT
		SET @ContractingPartyId=(SELECT TOP 1 ContractingPartyId  FROM mstContractingParty WHERE RTRIM(LTRIM(ContractingPartyName))=@ContractingPartyName)

		UPDATE ContractRequest  SET ClientName=@ClientName, ClientId=@ClientId,isCustomer=@isCustomer,ContractingTypeId=@ContractingPartyId,CountryId=@CountryId WHERE RequestId=@RequestId
		END
		SET @Cnt = @Cnt + 1

		INSERT INTO Activity 
							(RequestId, ActivityTypeId, ActivityStatusId, ActivityText, ActivityDate,  ReminderDate, isForCustomer, Addedby, Addedon, IP, AssignToId, isDismissed,IsFromCalendar,IsFromOther) 
			 SELECT RequestId,  ActivityTypeId, ActivityStatusId, ActivityText, ActivityDate,ReminderDate, IsForCustomer, AddedBy, AddedOn, IP, AssignedTo, isDismissed, IsFromCalendar, IsFromOther
			 FROM(
					SELECT @RequestId RequestId, @ActivityTypeID ActivityTypeId, 1 ActivityStatusId, 'Contract expiration on  '+dbo.DateFormat(ExpirationDate) ActivityTExt, ExpirationDate ActivityDate,
						DATEADD(DD, -180, dbo.DateOnly(ExpirationDate)) ReminderDate, 'Y' IsForCustomer, @AddedBy AddedBy,GETDATE() AddedOn,@IpAddress IP, ExpirationAlert180daysUserID1 AssignedTo, 'N' isDismissed,'Y' IsFromCalendar,'KeyField'  IsFromOther
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert180days,'N')='Y' AND ISNULL(ExpirationAlert180daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -180, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert180daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert180days,'N')='Y'  AND ISNULL(ExpirationAlert180daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -90, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert90daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert90days,'N')='Y'  AND ISNULL(ExpirationAlert90daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -90, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert90daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert90days,'N')='Y'  AND ISNULL(ExpirationAlert90daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -60, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert60daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert60days,'N')='Y'  AND ISNULL(ExpirationAlert60daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -60, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert60daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert60days,'N')='Y'  AND ISNULL(ExpirationAlert60daysUserID2,0)<>0
					UNION ALL  
					SELECT @RequestId RequestId, @ActivityTypeID ActivityTypeId, 1 ActivityStatusId, 'Contract expiration on  '+dbo.DateFormat(ExpirationDate) ActivityTExt, ExpirationDate ActivityDate,	DATEADD(DD, -30, dbo.DateOnly(ExpirationDate)) ReminderDate, 'Y' IsForCustomer, @AddedBy AddedBy,GETDATE() AddedOn,@IpAddress IP, ExpirationAlert30daysUserID1 AssignedTo, 'N' isDismissed,'Y' IsFromCalendar,'KeyField'  IsFromOther
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert30days,'N')='Y' AND ISNULL(ExpirationAlert30daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -30, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert30daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert30days,'N')='Y'  AND ISNULL(ExpirationAlert30daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -7, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert7daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert7days,'N')='Y'  AND ISNULL(ExpirationAlert7daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -7, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert7daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert7days,'N')='Y'  AND ISNULL(ExpirationAlert7daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on  '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -1, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert24HrsUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert24Hrs,'N')='Y'  AND ISNULL(ExpirationAlert24HrsUserID1,0)<>0
					UNION ALL 
					SELECT  @RequestId, @ActivityTypeID, 1, 'Contract expiration on  '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -1, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert24HrsUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert24Hrs,'N')='Y'  AND ISNULL(ExpirationAlert24HrsUserID2,0)<>0

					UNION ALL
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -180, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert180daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert180days,'N')='Y'  AND ISNULL(RenewalAlert180daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -180, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert180daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert180days,'N')='Y'  AND ISNULL(RenewalAlert180daysUserID2,0)<>0
					UNION ALL
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -90, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert90daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert90days,'N')='Y'  AND ISNULL(RenewalAlert90daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -90, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert90daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert90days,'N')='Y'  AND ISNULL(RenewalAlert90daysUserID2,0)<>0
					UNION ALL
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -60, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert60daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert60days,'N')='Y'  AND ISNULL(RenewalAlert60daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -60, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert60daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert60days,'N')='Y'  AND ISNULL(RenewalAlert60daysUserID2,0)<>0
					

					UNION ALL
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -30, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert30daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert30days,'N')='Y'  AND ISNULL(RenewalAlert30daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -30, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert30daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert30days,'N')='Y'  AND ISNULL(RenewalAlert30daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -7, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert7daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert7days,'N')='Y'  AND ISNULL(RenewalAlert7daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -7, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert7daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert7days,'N')='Y'  AND ISNULL(RenewalAlert7daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -1, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert24HrsUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert24Hrs,'N')='Y'  AND ISNULL(RenewalAlert24HrsUserID1,0)<>0
					UNION ALL 
					SELECT  @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -1, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert24HrsUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert24Hrs,'N')='Y' AND ISNULL(RenewalAlert24HrsUserID2,0)<>0
					UNION ALL 
					SELECT DISTINCT ContractMetaData.requestid, @ActivityTypeID, 1 ActivityStatusId,fieldname +' reminder on '+ FieldValue As ActivityTExt,FieldValue AS ActivityDate,
					DATEADD(DD, -ReminderDays, dbo.DateOnly(FieldValue)) ReminderDate, 'Y' IsForCustomer,AddedBy,AddedOn,ContractMetaData.IPAddress AS IP,ReminderUser1 AS AssignedTo,
					'N' isDismissed,'Y' IsFromCalendar,'KeyField'  IsFromOther
					FROM ImportantDatesMetaDataReminders
					INNER JOIN ContractMetaData ON ContractMetaData.MetaDataFieldId=ImportantDatesMetaDataReminders.MetaDataFieldId and 
					ContractMetaData.requestid=ImportantDatesMetaDataReminders.requestid  and MetaDataType='Important Dates'
					INNER JOIN MetaDataConfigurator ON FieldId=ContractMetaData.MetaDataFieldId
					WHERE ContractMetaData.requestid=@RequestId  AND ISNULL(ReminderUser1,0)<>0
					UNION ALL 
					SELECT DISTINCT ContractMetaData.requestid, 1 ActivityTypeId, 1 ActivityStatusId,fieldname +' reminder on '+ FieldValue As ActivityTExt,FieldValue AS ActivityDate,DATEADD(DD, -ReminderDays, dbo.DateOnly(FieldValue)) ReminderDate, 'Y' IsForCustomer,
					AddedBy,AddedOn,ContractMetaData.IPAddress AS IP,ReminderUser2 AS AssignedTo,'N' isDismissed,'Y' IsFromCalendar,'KeyField'  IsFromOther
					FROM ImportantDatesMetaDataReminders
					INNER JOIN ContractMetaData ON ContractMetaData.MetaDataFieldId=ImportantDatesMetaDataReminders.MetaDataFieldId and 
					ContractMetaData.requestid=ImportantDatesMetaDataReminders.requestid  and MetaDataType='Important Dates'
					INNER JOIN MetaDataConfigurator ON FieldId=ContractMetaData.MetaDataFieldId
					WHERE ContractMetaData.requestid=@RequestId AND ISNULL(ReminderUser2,0)<>0
			)tmp

END -- end of while


		IF @trancount = 0
            COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			DECLARE @error INT, @message VARCHAR(4000), @xstate INT;
			SELECT @error = ERROR_NUMBER(), @message = ERROR_MESSAGE(), @xstate = XACT_STATE();
			IF @xstate = -1
				ROLLBACK;
			IF @xstate = 1 and @trancount = 0
				ROLLBACK
			IF @xstate = 1 and @trancount > 0
				ROLLBACK TRANSACTION usp_my_procedure_name;

			RAISERROR ('usp_my_procedure_name: %d: %s', 16, 1, @error, @message) ;

		END CATCH

		IF dbo.isTableExists('#tmpBulk') > 0 DROP TABLE #tmpBulk
	
	END



	/*
	SELECT unPVT.BulkImportID, unPVT.ContractTemplateId, unPVT.BulkImportStagingId, unPVT.asAnswer, 
						   unPVT.ColumnName,uH.ColumnNameValue,unPVT.Addedby, unPVT.Addedon
	FROM BulkImportStaging 
	UNPIVOT 
	(
	 asAnswer FOR ColumnName IN (Col1,Col2,Col3) -- dynamic column str
	) unPVT
	INNER JOIN BulkImportStagingHeader uH ON uH.ColumnName = unPVT.ColumnName AND uH.BulkImportID = unPVT.BulkImportID
	WHERE unPVT.BulkImportID = @BulkImportID AND unPVT.Head='N' AND unPVT.IsPass= 'Y'
	*/

	/*
		SET @Columns = NULL
		SELECT @Columns = COALESCE(@Columns + ', ','') +'['+ TableColumnName + ']='+  ColumnName 
		FROM
		(
		SELECT 
		uK.TableColumnName, 
		CASE WHEN  uK.Ans ='user' THEN 'dbo.GetUserId('+ uH.ColumnName +')' ELSE 'uB.'+ uH.ColumnName END ColumnName
		FROM BulkImportStagingHeader uH
		INNER JOIN vwBulkImportKeyFields uK ON uK.FieldName = uH.ColumnNameValue  
		WHERE uH.BulkImportID = @BulkImportID
		AND uH.ColumnNameValue IN (SELECT FieldName FROM vwBulkImportKeyFields)
		AND LEN(uK.TableColumnName)>0
		)tmp
		*/

END


















/*

exec BulkImportAdd 10119



SELECT * FROm Client

SELECT * FROM ClientAddressDetails

SELECT * FROM ClientContactDetails

SELECT * FROm ContractRequest

*/
GO


ALTER PROCEDURE [dbo].[BulkImportAdd_BKP_5Aug2016]
@BulkImportID INT
	
AS
BEGIN
	
	SET NOCOUNT ON

	IF dbo.isTableExists('#tmpBulk') > 0 DROP TABLE #tmpBulk
	DECLARE @trancount INT;
    SET @trancount = @@trancount;

	CREATE TABLE #tmpBulk (BulkImportID INT, ContractTemplateId INT, BulkImportStagingId INT, asAnswer VARCHAR (MAX), 
	ColumnName VARCHAR(500), ColumnNameValue VARCHAR(1000),Addedby INT, Addedon DATETIME)


	DECLARE @Qry VARCHAR(MAX)
	DECLARE @Columns VARCHAR(MAX)
	DECLARE @RequestContractIDMax BIGINT
	DECLARE @ContractIDMax BIGINT
	
	DECLARE @ContractTemplateId INT = (SELECT MIN(ContractTemplateId) FROM BulkImport WHERE BulkImportID = @BulkImportID)
	DECLARE @ContractTypeId INT = ( SELECT ContractTypeId FROM ContractTemplate WHERE ContractTemplateId = @ContractTemplateId)
	
	DECLARE @AddedById INT
	DECLARE @IPAddress VARCHAR(100)

	DECLARE @FullName VARCHAR(1000)
	DECLARE @DepartmentId INT
	DECLARE @RoleID INT
    SELECT @AddedById = MIN(ModifiedBy), @IPAddress = MIN(IPAddress) FROM BulkImport WHERE BulkImportID = @BulkImportID
    SELECT @FullName = FullName, @DepartmentId = DepartmentId FROM MstUsers WHERE UsersId = @AddedById

	SET @RoleID=(SELECT TOP 1 RoleId FROM MstUsers WHERE UsersId=@AddedById)
	IF (SELECT COUNT(1) FROM BulkImportStagingHeader WHERE BulkImportId= @BulkImportId) = (SELECT COUNT(1) FROM vwBulkImportKeyFields)
	BEGIN
	--	SELECT COALESCE(@Columns + ', ','') + ColumnName FROM BulkImportStagingHeader WHERE BulkImportID = @BulkImportID 
		SELECT @Columns = COALESCE(@Columns + ', ','') + ColumnName FROM BulkImportStagingHeader WHERE BulkImportID = @BulkImportID 
		--AND ColumnNameValue NOT IN (SELECT FieldName FROM vwBulkImportKeyFields) 
	END
	ELSE
	BEGIN
		
		SELECT @Columns = COALESCE(@Columns + ', ','') + ColumnName FROM BulkImportStagingHeader WHERE BulkImportID = @BulkImportID 
		AND ColumnNameValue NOT IN (SELECT FieldName FROM vwBulkImportKeyFields) 
	END
	
	SET @Qry = ' SELECT unPVT.BulkImportID, unPVT.ContractTemplateId, unPVT.BulkImportStagingId, unPVT.asAnswer, 
						   unPVT.ColumnName,uH.ColumnNameValue,unPVT.Addedby, unPVT.Addedon FROM BulkImportStaging 
				UNPIVOT 
				(
				 asAnswer FOR ColumnName IN ('+ @Columns +') 
				) unPVT
				INNER JOIN BulkImportStagingHeader uH ON uH.ColumnName = unPVT.ColumnName AND uH.BulkImportID = unPVT.BulkImportID
				WHERE unPVT.BulkImportID = ' + CAST(@BulkImportID AS VARCHAR(10)) + ' AND unPVT.Head=''N'' AND unPVT.IsPass= ''Y''		
				AND  unPVT.BulkImportStagingId NOT IN(SELECT ISNULL(ContractRequest.BulkImportStagingId,0) FROM ContractRequest)'

	INSERT INTO #tmpBulk (BulkImportID, ContractTemplateId, BulkImportStagingId, asAnswer, 
						  ColumnName, ColumnNameValue, Addedby, Addedon)
	EXEC (@Qry)


	IF (SELECT COUNT(1) FROm #tmpBulk)> 0
	BEGIN
			
		--SELECT  @RequestContractIDMax = MAX(ISNULL(RequestContractID,100000))+1 , @ContractIDMax = MAX(ISNULL(ContractID,100000))+1 FROM ContractRequest

		SELECT  @RequestContractIDMax = ISNULL(MAX(RequestContractID),100000)+1 , @ContractIDMax = ISNULL(MAX(ContractID),100000)+1 FROM ContractRequest

		BEGIN TRY
	
		IF @trancount = 0
            BEGIN TRANSACTION
		
        ELSE
            SAVE TRANSACTION usp_my_procedure_name;
		
		
		INSERT INTO ContractRequest 
		(
		RequestContractID, 
		ContractID, 
		ContractTypeId, RequestTypeId, ContractTemplateId, RequesterUserId, 
		RequestDescription, AssignToDepartmentId, AssignToUserID, 
		IsApprovalRequired, Addedon, Addedby,Modifiedon,Modifiedby,  IP, BulkImportStagingId, IsBulkImport, 
		ContractStatusID
		)
		
		SELECT 
		ROW_NUMBER() OVER (ORDER BY s.BulkImportStagingId)+ @RequestContractIDMax, 
		ROW_NUMBER() OVER (ORDER BY s.BulkImportStagingId)+ @ContractIDMax, 
		@ContractTypeId, 1, @ContractTemplateId, s.AddedBy, 
		'Bulk Imported by: '+ @FullName + ' on: '+ dbo.DateFormat(s.Addedon), @DepartmentId, s.AddedBy, 
		'Y',s.Addedon,s.Addedby,GETDATE(),s.Addedby, s.IP, s.BulkImportStagingId, 'Y', 
		9 
		-- (All bulk imported are bydefault Active Contracts )
		FROM BulkImportStaging s
		WHERE s.BulkImportID = @BulkImportID AND s.IsPass = 'Y' 
		
		INSERT INTO ContractQuestionsAnswers 
		(
		ContractId, RequestId, ContractTemplateId, FieldLibraryID, Question,
		TextValue, NumericValue, DecimalValue, DateValue, ForeignKeyValye,
		IsDraft, Addedon, Addedby, Modifiedon, Modifiedby, IP, IsBulkImport
		)
    
		SELECT cR.ContractId,cR.RequestId, t.ContractTemplateId, uF.FieldLibraryID, ColumnNameValue,
		IIF (uF.FieldTypeID = 1, t.asAnswer, NULL) TextValue, 
		IIF (uF.FieldTypeID = 4, t.asAnswer, NULL) NumericValue,
		IIF (uF.FieldTypeID = 5, t.asAnswer, NULL) DecimalValue, 
		IIF (uF.FieldTypeID = 7, t.asAnswer, NULL) DateValue, 
		IIF (uF.FieldTypeID = 9,
						(SELECT MAX(MstView.Id) FROM FieldLibraryValues v 
						 INNER JOIN MstView ON MstView.PickListMasterId = v.FieldValue 
						 AND MstView.Name = t.asAnswer COLLATE SQL_Latin1_General_CP1_CI_AS 
						 WHERE v.FieldLibraryID = uF.FieldLibraryID), NULL) ForeignKeyValye, 
		NULL,t.Addedon,t.Addedby,GETDATE(),t.Addedby, @IPAddress,'Y'
		FROM #tmpBulk t
		INNER JOIN FieldLibrary uF ON uF.FieldName = t.ColumnNameValue COLLATE SQL_Latin1_General_CP1_CI_AS 
		AND uF.ContractTemplateId = t.ContractTemplateId
		INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = t.BulkImportStagingId
		ORDER  BY cR.RequestId


		-------------------- Added by kiran for Audit Trail Log on 16 May 2016	
BEGIN			
	BEGIN TRY 
	BEGIN TRAN
		INSERT INTO AuditTrail 
		(
		RequestId,ContractId,UserId,ActionTaken,ActionDateTime,SectionName,IP
		)    
		SELECT DISTINCT cR.RequestId, cR.ContractId,t.Addedby,'Add',GETDATE(),'Contract','Bulk Import'
		FROM #tmpBulk t
		INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = t.BulkImportStagingId
		ORDER  BY cR.RequestId
	COMMIT TRAN
  END TRY
  BEGIN CATCH
	ROLLBACK TRAN
  END CATCH
END
-------------------- End----------------------------


		-- updating key fields
		
		SET @Columns = NULL
		SELECT @Columns = COALESCE(@Columns + ', ','') +'['+ TableColumnName + ']='+  ColumnName 
		FROM
		(
		SELECT 
		uK.TableColumnName, 
		CASE WHEN  uK.Ans ='user' THEN 'dbo.GetUserId('+ uH.ColumnName +')' 
			 WHEN  uK.Ans ='Status' THEN 'dbo.GetContractStatusId('+ uH.ColumnName +')' 
			 WHEN  uk.Ans='Currency' THEN 'CAST(ISNULL((SELECT TOP 1 CurrencyId FROM MstCurrency WHERE CurrencyCode=uB.'+ uH.ColumnName +'),0) AS VARCHAR(5))'
		     WHEN  uK.Ans ='Yes,No' AND uK.TableColumnName<>'IsCustomerPortalEnable' THEN ' CASE WHEN '+ uH.ColumnName + '=''Yes'' THEN ''Y'' ELSE ''N'' END '
			 WHEN  uK.Ans ='Yes,No' AND uK.TableColumnName='IsCustomerPortalEnable' THEN ' CASE WHEN '+ uH.ColumnName + '=''No'' THEN ''N'' 
			 WHEN ISNULL(dbo.IsHaveAccess('''','+CONVERT(VARCHAR(50),ISNULL(@ContractTypeId,0))+','+CONVERT(VARCHAR(50),ISNULL(@RoleID,0))+'),'''')='''' THEN ''N'' ELSE ''Y'' END '
			 ELSE 'uB.'+ uH.ColumnName END ColumnName
		FROM BulkImportStagingHeader uH
		INNER JOIN vwBulkImportKeyFields uK ON uK.FieldName = uH.ColumnNameValue  
		WHERE uH.BulkImportID = @BulkImportID
		AND uH.ColumnNameValue IN (SELECT FieldName FROM vwBulkImportKeyFields WHERE vwBulkImportKeyFields.Flag in('U','B')  AND ParentID=0)
		AND LEN(uK.TableColumnName)>0
		)tmp

			print 1

		SET @Qry = 'UPDATE ContractRequest SET ' + @Columns + ' FROM BulkImportStaging uB INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = uB.BulkImportStagingId WHERE uB.Head=''N'' AND uB.BulkImportID = ' + CAST(@BulkImportID AS VARCHAR(10))
	    
		EXEC (@Qry)
		

        UPDATE ContractRequest SET
		ContractRequest.ContractStatusId= 12  FROM 
	    (SELECT cR.RequestId  RequestId FROM BulkImportStaging uB INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = uB.BulkImportStagingId WHERE uB.Head='N' AND uB.BulkImportID =CAST(@BulkImportID AS VARCHAR(10))) tbl WHERE tbl.[RequestId]=ContractRequest.[RequestId]



		 UPDATE ContractRequest SET
		 ContractRequest.CountryId= (SELECT CountryId FROM MstCountry WHERE CountryName=RTRIM(LTRIM(ContractRequest.CountryName)))  FROM 
	    (SELECT cR.RequestId  RequestId FROM BulkImportStaging uB INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = uB.BulkImportStagingId WHERE uB.Head='N' AND uB.BulkImportID =CAST(@BulkImportID AS VARCHAR(10))) tbl WHERE tbl.[RequestId]=ContractRequest.[RequestId]

				--------------------Meta Fields INSERT----------------------------------------
		SET @Columns=NULL
		SET @Qry=NULL
	SELECT @Columns = COALESCE(@Columns + ', ','') + ColumnName FROM BulkImportStagingHeader WHERE BulkImportID = @BulkImportID
		AND ColumnNameValue IN (SELECT FieldName FROM vwBulkImportKeyFields where Flag NOT IN('U','B') AND ParentID=0) 


	SET @Qry='SELECT BulkImportStagingId, Col, Val
	FROM
	(SELECT BulkImportStagingId,
	'+@Columns+' 
	FROM BulkImportStaging WHERE Head=''N'' AND  BulkImportID = '+CAST(@BulkImportID AS VARCHAR(10))+' ) stu  
	UNPIVOT
	(VAL FOR Col IN ('+@Columns+')
	) AS val1'
	IF dbo.isTableExists('#Tmp') > 0 DROP TABLE #Tmp
	CREATE TABLE #Tmp(BulkImportStagingId INT,Col VARCHAR(150),Val VARCHAR(500),FieldId INT,Ans VARCHAR(150))

	INSERT INTO #Tmp(BulkImportStagingId,Col,Val)
	EXEC(@Qry)

	UPDATE #Tmp SET #Tmp.FieldId=uK.SrNo,#Tmp.Ans=uK.Ans
	 from 
	 #Tmp INNER JOIN BulkImportStagingHeader uH ON #Tmp.Col=uH.ColumnName
	INNER JOIN [vwBulkImportKeyFields] uK ON uK.FieldName= uH.ColumnNameValue AND uK.Flag NOT IN('U','B') AND uH.BulkImportID=@BulkImportID

	INSERT INTO ContractMetaData
	SELECT cR.RequestId,cR.ContractId,#Tmp.FieldId,
	CASE WHEN Ans='Checkbox' THEN	
		STUFF((SELECT ',' + CAST(FieldOptionId AS varchar(150)) FROM MetaDataConfiguratorValues WHERE MetaDataConfiguratorValues.FieldId=#Tmp.FieldId 
		AND MetaDataConfiguratorValues.FieldOptionValue in(SELECT items FROM dbo.Split(CONVERT(VARCHAR(50),#Tmp.Val),','))
		FOR XML PATH('')), 1, 1, '')
	WHEN Ans='Dropdown' OR Ans='RadioButton' THEN 
	CAST((SELECT TOP 1 FieldOptionId FROM MetaDataConfiguratorValues WHERE MetaDataConfiguratorValues.FieldId=#Tmp.FieldId AND MetaDataConfiguratorValues.FieldOptionValue=CONVERT(VARCHAR(50),#Tmp.Val))AS varchar(150))
	WHEN ISDATE(Val)=1 THEN dbo.DateFormat(Val)
	ELSE Val END Val,
	m.MetaDataType,
	cR.AddedBy,
	cR.AddedOn,
	cR.IP FROM #Tmp
	INNER JOIN MetaDataConfigurator m ON m.FieldId=#Tmp.FieldId
	INNER JOIN ContractRequest cR ON cR.BulkImportStagingId=#Tmp.BulkImportStagingId  

		--------------------END Meta Fields INSERT------------------------------------
		-----------------------INSERT Metadata Reminder ----------------------------------
SET @Columns=NULL
SET @Qry=NULL
SELECT @Columns = COALESCE(@Columns + ', ','') + ColumnName FROM BulkImportStagingHeader WHERE BulkImportID = @BulkImportID
	AND ColumnNameValue IN (SELECT FieldName FROM vwBulkImportKeyFields where Flag IN('U','B') AND ParentID<>0 ) 

	SET @Qry='SELECT BulkImportStagingId, Col, Val
	FROM
	(SELECT BulkImportStagingId,
	'+@Columns+' 
	FROM BulkImportStaging WHERE Head=''N'' AND  BulkImportID = '+CAST(@BulkImportID AS VARCHAR(10))+' ) stu  
	UNPIVOT
	(VAL FOR Col IN ('+@Columns+')
	) AS val1'

	IF dbo.isTableExists('#Tmp1') > 0 DROP TABLE #Tmp1
	CREATE TABLE #Tmp1(ID INT IDENTITY(1,1),BulkImportStagingId INT,Col VARCHAR(1000),Val VARCHAR(1000),FieldId INT,Ans VARCHAR(1000),reminder  VARCHAR(1000))
	
	INSERT INTO #Tmp1(BulkImportStagingId,Col,Val)
	EXEC(@Qry)
	UPDATE #Tmp1 SET #Tmp1.FieldId=uK.ParentID,#Tmp1.Ans=uK.Ans,#Tmp1.reminder=uk.TableColumnName
	 from 
	 #Tmp1 INNER JOIN BulkImportStagingHeader uH ON #Tmp1.Col=uH.ColumnName
	INNER JOIN [vwBulkImportKeyFields] uK ON uK.FieldName= uH.ColumnNameValue AND uK.Flag IN('U','B')  AND uK.ParentID<>0 AND uH.BulkImportID=@BulkImportID

IF dbo.isTableExists('#TmpReminder') > 0 DROP TABLE #TmpReminder
	CREATE TABLE #TmpReminder(ID INT IDENTITY(1,1),RequestId INT,ContractId INT,FieldId INT,[Days] INT,User1 INT,User2 INT,Reminder VARCHAR(500))
	DECLARE @ID INT=1,@Maxcount INT
	SELECT @Maxcount=COUNT(1) FROM  #Tmp1
	

	WHILE @ID<=@Maxcount
	BEGIN
		INSERT INTO #TmpReminder

		SELECT cR.RequestId,cR.ContractId,#Tmp1.FieldId,
			CASE WHEN Ans='Yes,No' AND reminder='ReminderAlert1days' THEN 1
			WHEN  Ans='Yes,No' AND reminder='ReminderAlert7days' THEN 7
			WHEN  Ans='Yes,No' AND reminder='ReminderAlert30days' THEN 30
			WHEN  Ans='Yes,No' AND reminder='ReminderAlert60days' THEN 60
			WHEN  Ans='Yes,No' AND reminder='ReminderAlert90days' THEN 90
			WHEN  Ans='Yes,No' AND reminder='ReminderAlert180days' THEN 180	 END [days],	
			CASE WHEN Ans ='user' AND (reminder='ReminderAlert1dayUserID1' OR reminder='ReminderAlert7daysUserID1' OR
			reminder='ReminderAlert30daysUserID1' OR reminder='ReminderAlert60daysUserID1' OR reminder='ReminderAlert90daysUserID1'OR
			reminder='ReminderAlert180daysUserID1') THEN dbo.GetUserId(#Tmp1.Val) END [User1],
			CASE WHEN Ans ='user' AND (reminder='ReminderAlert1dayUserID2' OR reminder='ReminderAlert7daysUserID2' OR
			reminder='ReminderAlert30daysUserID2' OR reminder='ReminderAlert60daysUserID2' OR reminder='ReminderAlert90daysUserID2'OR
			reminder='ReminderAlert180daysUserID2') THEN dbo.GetUserId(#Tmp1.Val) END [User2]
			,reminder
		FROM #Tmp1 INNER JOIN ContractRequest cR ON cR.BulkImportStagingId=#Tmp1.BulkImportStagingId AND #Tmp1.ID=@ID

		SET @ID=@ID+1;
	END

IF dbo.isTableExists('#TmpFinalReminder') > 0 DROP TABLE #TmpFinalReminder
	CREATE TABLE #TmpFinalReminder(ID INT IDENTITY(1,1),RequestId INT,ContractId INT,FieldId INT,[Days] INT,User1 INT,User2 INT,PreID INT)

	INSERT INTO #TmpFinalReminder(RequestId,ContractId,FieldId,[Days],PreID)
	SELECT RequestId,ContractId,FieldId,[Days],ID FROM #TmpReminder WHERE Reminder IN('ReminderAlert1days',
	'ReminderAlert7days','ReminderAlert30days','ReminderAlert60days','ReminderAlert90days','ReminderAlert180days')

UPDATE #TmpFinalReminder SET #TmpFinalReminder.User1= #TmpReminder.User1 FROM #TmpReminder
 INNER JOIN  #TmpFinalReminder ON  #TmpFinalReminder.PreID=#TmpReminder.ID-1 AND Reminder 
	IN('ReminderAlert1dayUserID1',
	'ReminderAlert7daysUserID1','ReminderAlert30daysUserID1','ReminderAlert60daysUserID1','ReminderAlert90daysUserID1','ReminderAlert180daysUserID1')

UPDATE #TmpFinalReminder SET #TmpFinalReminder.User2= #TmpReminder.User2 
FROM #TmpReminder
 INNER JOIN  #TmpFinalReminder ON  #TmpFinalReminder.PreID= CASE WHEN #TmpFinalReminder.user1 is not null THEN #TmpReminder.ID-2 ELSE #TmpReminder.ID-1 END  AND Reminder 
	IN('ReminderAlert1dayUserID2',
	'ReminderAlert7daysUserID2','ReminderAlert30daysUserID2','ReminderAlert60daysUserID2','ReminderAlert90daysUserID2','ReminderAlert180daysUserID2')

	INSERT INTO ImportantDatesMetaDataReminders(RequestId,ContractId,MetaDataFieldId,ReminderDays,ReminderUser1,ReminderUser2)
		select RequestId,ContractId,FieldId,Days,User1,User2 from #TmpFinalReminder
-------------------------END MetaData Reminder-------------------------------------------
		/**Added By Nilesh to add Client Details*****/
		
		
		CREATE TABLE #tmpClientInformation (ID INT IDENTITY (1,1), RequestId INT, ClientName VARCHAR(MAX), ContactNumber VARCHAR(MAX), EmailId VARCHAR (MAX), isCustomer VARCHAR(MAX),
	    AddedBy INT, Addedon DATETIME,ModifiedBy INT,ModifiedOn DATETIME,IP VARCHAR(MAX),Address VARCHAR(MAX),CountryName VARCHAR(MAX),ContractingPartyName VARCHAR(MAX),CityName VARCHAR(MAX),StateName VARCHAR(MAX),Pincode VARCHAR(MAX))



		INSERT INTO #tmpClientInformation  
        SELECT cR.RequestId ,cR.ClientName,cR.ContactNumber,cR.EmailId,cR.isCustomer,cR.AddedBy,cR.Addedon,cR.Modifiedby,cR.Modifiedon,cR.IP,cR.Address,cR.CountryName,cR.ContractingPartyName,cR.CityName,cR.StateName,cR.PinCode
		FROM BulkImportStaging uB INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = uB.BulkImportStagingId
		WHERE uB.Head='N' AND uB.BulkImportID =CAST(@BulkImportID AS VARCHAR(10))

	
	DECLARE @Cnt INT =1
	DECLARE @MaxCnt INT = (SELECT COUNT(1) FROM #tmpClientInformation)
	DECLARE @ClientId INT
	DECLARE @CountryId INT
	DECLARE @RequestId INT, @ClientName VARCHAR(MAX), @ContactNumber VARCHAR(MAX), @EmailId VARCHAR (MAX),@isCustomer VARCHAR(MAX), @AddedBy INT, @Addedon DATETIME,@ModifiedBy INT,@ModifiedOn DATETIME,@IP VARCHAR(MAX),@Address VARCHAR(MAX),@CountryName VARCHAR(MAX),@ContractingPartyName VARCHAR(MAX),
	@CityName VARCHAR(MAX),@StateName VARCHAR(MAX),@Pincode VARCHAR(MAX)

	DECLARE @ActivityTypeID INT
		SET @ActivityTypeID=(SELECT ActivityTypeId FROM MstActivityType WHERE  isSystemType='K')

	WHILE @Cnt <=@MaxCnt
	BEGIN
			BEGIN

			SELECT  @RequestId=t.[RequestId] , @ClientName=t.[ClientName],@isCustomer=CASE WHEN t.[isCustomer]='Supplier' THEN 'N' WHEN t.[isCustomer]='Customer' THEN 'Y' ELSE 'O' END , @ContactNumber=t.[ContactNumber] , 
			@EmailId=t.[EmailId], @AddedBy=t.[AddedBy], @Addedon=t.[Addedon],@ModifiedBy=t.[ModifiedBy],@ModifiedOn=t.[ModifiedOn],@IP=t.[IP],@Address=t.[Address],@CountryName=t.[CountryName],@ContractingPartyName=t.[ContractingPartyName],
			@CityName=t.[CityName],@StateName=t.[StateName],@Pincode=t.[PinCode]
			FROM #tmpClientInformation t WHERE ID=@Cnt

			SET @CountryId=(SELECT CountryId FROm MstCountry WHERE CountryName=@CountryName)

			IF NOT EXISTS(SELECT 1 FROm Client WHERE ClientName=@ClientName)
			BEGIN
				INSERT INTO Client (isCustomer,ClientName,Addedon,Addedby,Modifiedon,Modifiedby,IP)
				SELECT @isCustomer,@ClientName,@Addedon,@AddedBy,@ModifiedOn,@ModifiedBy,@IP
				SET @ClientId=SCOPE_IDENTITY()
			END
			ELSE
			BEGIN
			  SET @ClientId=(SELECT TOP 1 ClientId FROM Client WHERE ClientName=@ClientName)
			END

			IF NOT EXISTS(SELECT 1 FROM ClientAddressDetails WHERE Address=@Address AND CountryId=@CountryId AND PinCode=@PinCode AND CityName=@CityName AND StateName=@StateName AND ClientId=@ClientId)
			BEGIN
			INSERT INTO ClientAddressDetails(ClientId,IsPrimary,Address,CountryId,StateId,CityId,PinCode,Street2,CityName,StateName)
			SELECT @ClientId,'Y',@Address,@CountryId,NULL,NULL,@Pincode,NULL,@CityName,@StateName
			END

			IF NOT EXISTS(SELECT 1 FROM ClientContactDetails WHERE ContactNumber=@ContactNumber AND ClientId=@ClientId)
			BEGIN	
			INSERT INTO ClientContactDetails(ClientId,IsPrimary,ContactNumber)
			SELECT @ClientId,'Y',@ContactNumber
			END
	
			IF NOT EXISTS(SELECT 1 FROM ClientEmailDetails WHERE EmailID=@EmailId AND ClientId=@ClientId)
			BEGIN		
			INSERT INTO ClientEmailDetails(ClientId,IsPrimary,EmailID)
			SELECT @ClientId,'Y',@EmailId
			END

		DECLARE @ContractingPartyId INT
		SET @ContractingPartyId=(SELECT TOP 1 ContractingPartyId  FROM mstContractingParty WHERE RTRIM(LTRIM(ContractingPartyName))=@ContractingPartyName)

		UPDATE ContractRequest  SET ClientName=@ClientName, ClientId=@ClientId,isCustomer=@isCustomer,ContractingTypeId=@ContractingPartyId,CountryId=@CountryId WHERE RequestId=@RequestId
		END
		SET @Cnt = @Cnt + 1

		INSERT INTO Activity 
							(RequestId, ActivityTypeId, ActivityStatusId, ActivityText, ActivityDate,  ReminderDate, isForCustomer, Addedby, Addedon, IP, AssignToId, isDismissed,IsFromCalendar,IsFromOther) 
			 SELECT RequestId,  ActivityTypeId, ActivityStatusId, ActivityText, ActivityDate,ReminderDate, IsForCustomer, AddedBy, AddedOn, IP, AssignedTo, isDismissed, IsFromCalendar, IsFromOther
			 FROM(
					SELECT @RequestId RequestId, @ActivityTypeID ActivityTypeId, 1 ActivityStatusId, 'Contract expiration on  '+dbo.DateFormat(ExpirationDate) ActivityTExt, ExpirationDate ActivityDate,
						DATEADD(DD, -180, dbo.DateOnly(ExpirationDate)) ReminderDate, 'Y' IsForCustomer, @AddedBy AddedBy,GETDATE() AddedOn,@IpAddress IP, ExpirationAlert180daysUserID1 AssignedTo, 'N' isDismissed,'Y' IsFromCalendar,'KeyField'  IsFromOther
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert180days,'N')='Y' AND ISNULL(ExpirationAlert180daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -180, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert180daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert180days,'N')='Y'  AND ISNULL(ExpirationAlert180daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -90, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert90daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert90days,'N')='Y'  AND ISNULL(ExpirationAlert90daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -90, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert90daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert90days,'N')='Y'  AND ISNULL(ExpirationAlert90daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -60, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert60daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert60days,'N')='Y'  AND ISNULL(ExpirationAlert60daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -60, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert60daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert60days,'N')='Y'  AND ISNULL(ExpirationAlert60daysUserID2,0)<>0
					UNION ALL  
					SELECT @RequestId RequestId, @ActivityTypeID ActivityTypeId, 1 ActivityStatusId, 'Contract expiration on  '+dbo.DateFormat(ExpirationDate) ActivityTExt, ExpirationDate ActivityDate,	DATEADD(DD, -30, dbo.DateOnly(ExpirationDate)) ReminderDate, 'Y' IsForCustomer, @AddedBy AddedBy,GETDATE() AddedOn,@IpAddress IP, ExpirationAlert30daysUserID1 AssignedTo, 'N' isDismissed,'Y' IsFromCalendar,'KeyField'  IsFromOther
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert30days,'N')='Y' AND ISNULL(ExpirationAlert30daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -30, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert30daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert30days,'N')='Y'  AND ISNULL(ExpirationAlert30daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -7, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert7daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert7days,'N')='Y'  AND ISNULL(ExpirationAlert7daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -7, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert7daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert7days,'N')='Y'  AND ISNULL(ExpirationAlert7daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on  '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -1, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert24HrsUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert24Hrs,'N')='Y'  AND ISNULL(ExpirationAlert24HrsUserID1,0)<>0
					UNION ALL 
					SELECT  @RequestId, @ActivityTypeID, 1, 'Contract expiration on  '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -1, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert24HrsUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert24Hrs,'N')='Y'  AND ISNULL(ExpirationAlert24HrsUserID2,0)<>0

					UNION ALL
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -180, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert180daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert180days,'N')='Y'  AND ISNULL(RenewalAlert180daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -180, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert180daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert180days,'N')='Y'  AND ISNULL(RenewalAlert180daysUserID2,0)<>0
					UNION ALL
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -90, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert90daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert90days,'N')='Y'  AND ISNULL(RenewalAlert90daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -90, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert90daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert90days,'N')='Y'  AND ISNULL(RenewalAlert90daysUserID2,0)<>0
					UNION ALL
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -60, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert60daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert60days,'N')='Y'  AND ISNULL(RenewalAlert60daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -60, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert60daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert60days,'N')='Y'  AND ISNULL(RenewalAlert60daysUserID2,0)<>0
					

					UNION ALL
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -30, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert30daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert30days,'N')='Y'  AND ISNULL(RenewalAlert30daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -30, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert30daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert30days,'N')='Y'  AND ISNULL(RenewalAlert30daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -7, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert7daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert7days,'N')='Y'  AND ISNULL(RenewalAlert7daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -7, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert7daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert7days,'N')='Y'  AND ISNULL(RenewalAlert7daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -1, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert24HrsUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert24Hrs,'N')='Y'  AND ISNULL(RenewalAlert24HrsUserID1,0)<>0
					UNION ALL 
					SELECT  @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -1, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert24HrsUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert24Hrs,'N')='Y' AND ISNULL(RenewalAlert24HrsUserID2,0)<>0
					UNION ALL 
					SELECT DISTINCT ContractMetaData.requestid, @ActivityTypeID, 1 ActivityStatusId,fieldname +' reminder on '+ FieldValue As ActivityTExt,FieldValue AS ActivityDate,
					DATEADD(DD, -ReminderDays, dbo.DateOnly(FieldValue)) ReminderDate, 'Y' IsForCustomer,AddedBy,AddedOn,ContractMetaData.IPAddress AS IP,ReminderUser1 AS AssignedTo,
					'N' isDismissed,'Y' IsFromCalendar,'KeyField'  IsFromOther
					FROM ImportantDatesMetaDataReminders
					INNER JOIN ContractMetaData ON ContractMetaData.MetaDataFieldId=ImportantDatesMetaDataReminders.MetaDataFieldId and 
					ContractMetaData.requestid=ImportantDatesMetaDataReminders.requestid  and MetaDataType='Important Dates'
					INNER JOIN MetaDataConfigurator ON FieldId=ContractMetaData.MetaDataFieldId
					WHERE ContractMetaData.requestid=@RequestId  AND ISNULL(ReminderUser1,0)<>0
					UNION ALL 
					SELECT DISTINCT ContractMetaData.requestid, 1 ActivityTypeId, 1 ActivityStatusId,fieldname +' reminder on '+ FieldValue As ActivityTExt,FieldValue AS ActivityDate,DATEADD(DD, -ReminderDays, dbo.DateOnly(FieldValue)) ReminderDate, 'Y' IsForCustomer,
					AddedBy,AddedOn,ContractMetaData.IPAddress AS IP,ReminderUser2 AS AssignedTo,'N' isDismissed,'Y' IsFromCalendar,'KeyField'  IsFromOther
					FROM ImportantDatesMetaDataReminders
					INNER JOIN ContractMetaData ON ContractMetaData.MetaDataFieldId=ImportantDatesMetaDataReminders.MetaDataFieldId and 
					ContractMetaData.requestid=ImportantDatesMetaDataReminders.requestid  and MetaDataType='Important Dates'
					INNER JOIN MetaDataConfigurator ON FieldId=ContractMetaData.MetaDataFieldId
					WHERE ContractMetaData.requestid=@RequestId AND ISNULL(ReminderUser2,0)<>0
			)tmp

END -- end of while


		IF @trancount = 0
            COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			DECLARE @error INT, @message VARCHAR(4000), @xstate INT;
			SELECT @error = ERROR_NUMBER(), @message = ERROR_MESSAGE(), @xstate = XACT_STATE();
			IF @xstate = -1
				ROLLBACK;
			IF @xstate = 1 and @trancount = 0
				ROLLBACK
			IF @xstate = 1 and @trancount > 0
				ROLLBACK TRANSACTION usp_my_procedure_name;

			RAISERROR ('usp_my_procedure_name: %d: %s', 16, 1, @error, @message) ;

		END CATCH

		IF dbo.isTableExists('#tmpBulk') > 0 DROP TABLE #tmpBulk
	
	END



	/*
	SELECT unPVT.BulkImportID, unPVT.ContractTemplateId, unPVT.BulkImportStagingId, unPVT.asAnswer, 
						   unPVT.ColumnName,uH.ColumnNameValue,unPVT.Addedby, unPVT.Addedon
	FROM BulkImportStaging 
	UNPIVOT 
	(
	 asAnswer FOR ColumnName IN (Col1,Col2,Col3) -- dynamic column str
	) unPVT
	INNER JOIN BulkImportStagingHeader uH ON uH.ColumnName = unPVT.ColumnName AND uH.BulkImportID = unPVT.BulkImportID
	WHERE unPVT.BulkImportID = @BulkImportID AND unPVT.Head='N' AND unPVT.IsPass= 'Y'
	*/

	/*
		SET @Columns = NULL
		SELECT @Columns = COALESCE(@Columns + ', ','') +'['+ TableColumnName + ']='+  ColumnName 
		FROM
		(
		SELECT 
		uK.TableColumnName, 
		CASE WHEN  uK.Ans ='user' THEN 'dbo.GetUserId('+ uH.ColumnName +')' ELSE 'uB.'+ uH.ColumnName END ColumnName
		FROM BulkImportStagingHeader uH
		INNER JOIN vwBulkImportKeyFields uK ON uK.FieldName = uH.ColumnNameValue  
		WHERE uH.BulkImportID = @BulkImportID
		AND uH.ColumnNameValue IN (SELECT FieldName FROM vwBulkImportKeyFields)
		AND LEN(uK.TableColumnName)>0
		)tmp
		*/

END













/*

exec BulkImportAdd 10119



SELECT * FROm Client

SELECT * FROM ClientAddressDetails

SELECT * FROM ClientContactDetails

SELECT * FROm ContractRequest

*/

GO

ALTER PROCEDURE [dbo].[BulkImportAdd_Live]
@BulkImportID INT
	
AS
BEGIN
	
	SET NOCOUNT ON

	IF dbo.isTableExists('#tmpBulk') > 0 DROP TABLE #tmpBulk
	DECLARE @trancount INT;
    SET @trancount = @@trancount;

	CREATE TABLE #tmpBulk (BulkImportID INT, ContractTemplateId INT, BulkImportStagingId INT, asAnswer VARCHAR (MAX), 
	ColumnName VARCHAR(500), ColumnNameValue VARCHAR(1000),Addedby INT, Addedon DATETIME)


	DECLARE @Qry VARCHAR(MAX)
	DECLARE @Columns VARCHAR(MAX)
	DECLARE @RequestContractIDMax BIGINT
	DECLARE @ContractIDMax BIGINT
	
	DECLARE @ContractTemplateId INT = (SELECT MIN(ContractTemplateId) FROM BulkImport WHERE BulkImportID = @BulkImportID)
	DECLARE @ContractTypeId INT = ( SELECT ContractTypeId FROM ContractTemplate WHERE ContractTemplateId = @ContractTemplateId)
	
	DECLARE @AddedById INT
	DECLARE @IPAddress VARCHAR(100)

	DECLARE @FullName VARCHAR(1000)
	DECLARE @DepartmentId INT
	DECLARE @RoleID INT
    SELECT @AddedById = MIN(ModifiedBy), @IPAddress = MIN(IPAddress) FROM BulkImport WHERE BulkImportID = @BulkImportID
    SELECT @FullName = FullName, @DepartmentId = DepartmentId FROM MstUsers WHERE UsersId = @AddedById
	
	SET @RoleID=(SELECT TOP 1 RoleId FROM MstUsers WHERE UsersId=@AddedById)
	
	IF (SELECT COUNT(1) FROM BulkImportStagingHeader WHERE BulkImportId= @BulkImportId) = (SELECT COUNT(1) FROM vwBulkImportKeyFields)
	BEGIN
	--	SELECT COALESCE(@Columns + ', ','') + ColumnName FROM BulkImportStagingHeader WHERE BulkImportID = @BulkImportID 
		SELECT @Columns = COALESCE(@Columns + ', ','') + ColumnName FROM BulkImportStagingHeader WHERE BulkImportID = @BulkImportID 
		--AND ColumnNameValue NOT IN (SELECT FieldName FROM vwBulkImportKeyFields) 
	END
	ELSE
	BEGIN
		
		SELECT @Columns = COALESCE(@Columns + ', ','') + ColumnName FROM BulkImportStagingHeader WHERE BulkImportID = @BulkImportID 
		AND ColumnNameValue NOT IN (SELECT FieldName FROM vwBulkImportKeyFields) 
	END
	
	SET @Qry = ' SELECT unPVT.BulkImportID, unPVT.ContractTemplateId, unPVT.BulkImportStagingId, unPVT.asAnswer, 
						   unPVT.ColumnName,uH.ColumnNameValue,unPVT.Addedby, unPVT.Addedon FROM BulkImportStaging 
				UNPIVOT 
				(
				 asAnswer FOR ColumnName IN ('+ @Columns +') 
				) unPVT
				INNER JOIN BulkImportStagingHeader uH ON uH.ColumnName = unPVT.ColumnName AND uH.BulkImportID = unPVT.BulkImportID
				WHERE unPVT.BulkImportID = ' + CAST(@BulkImportID AS VARCHAR(10)) + ' AND unPVT.Head=''N'' AND unPVT.IsPass= ''Y''		
				'

	INSERT INTO #tmpBulk (BulkImportID, ContractTemplateId, BulkImportStagingId, asAnswer, 
						  ColumnName, ColumnNameValue, Addedby, Addedon)
	EXEC (@Qry)


	IF (SELECT COUNT(1) FROm #tmpBulk)> 0
	BEGIN
			
		--SELECT  @RequestContractIDMax = MAX(ISNULL(RequestContractID,100000))+1 , @ContractIDMax = MAX(ISNULL(ContractID,100000))+1 FROM ContractRequest

		SELECT  @RequestContractIDMax = ISNULL(MAX(RequestContractID),100000)+1 , @ContractIDMax = ISNULL(MAX(ContractID),100000)+1 FROM ContractRequest

		BEGIN TRY
	
		IF @trancount = 0
            BEGIN TRANSACTION
		
        ELSE
            SAVE TRANSACTION usp_my_procedure_name;
		
		
		INSERT INTO ContractRequest 
		(
		RequestContractID, 
		ContractID, 
		ContractTypeId, RequestTypeId, ContractTemplateId, RequesterUserId, 
		RequestDescription, AssignToDepartmentId, AssignToUserID, 
		IsApprovalRequired, Addedon, Addedby,Modifiedon,Modifiedby,  IP, BulkImportStagingId, IsBulkImport, 
		ContractStatusID
		)
		
		SELECT 
		ROW_NUMBER() OVER (ORDER BY s.BulkImportStagingId)+ @RequestContractIDMax, 
		ROW_NUMBER() OVER (ORDER BY s.BulkImportStagingId)+ @ContractIDMax, 
		@ContractTypeId, 1, @ContractTemplateId, s.AddedBy, 
		'Bulk Imported by: '+ @FullName + ' on: '+ dbo.DateFormat(s.Addedon), @DepartmentId, s.AddedBy, 
		'Y',s.Addedon,s.Addedby,GETDATE(),s.Addedby, s.IP, s.BulkImportStagingId, 'Y', 
		9 
		-- (All bulk imported are bydefault Active Contracts )
		FROM BulkImportStaging s
		WHERE s.BulkImportID = @BulkImportID AND s.IsPass = 'Y' 
		
		


		INSERT INTO ContractQuestionsAnswers 
		(
		ContractId, RequestId, ContractTemplateId, FieldLibraryID, Question,
		TextValue, NumericValue, DecimalValue, DateValue, ForeignKeyValye,
		IsDraft, Addedon, Addedby, Modifiedon, Modifiedby, IP, IsBulkImport
		)
    
		SELECT cR.ContractId,cR.RequestId, t.ContractTemplateId, uF.FieldLibraryID, ColumnNameValue,
		IIF (uF.FieldTypeID = 1, t.asAnswer, NULL) TextValue, 
		IIF (uF.FieldTypeID = 4, t.asAnswer, NULL) NumericValue,
		IIF (uF.FieldTypeID = 5, t.asAnswer, NULL) DecimalValue, 
		IIF (uF.FieldTypeID = 7, t.asAnswer, NULL) DateValue, 
		IIF (uF.FieldTypeID = 9,
						(SELECT MAX(MstView.Id) FROM FieldLibraryValues v 
						 INNER JOIN MstView ON MstView.PickListMasterId = v.FieldValue 
						 AND MstView.Name = t.asAnswer COLLATE SQL_Latin1_General_CP1_CI_AS 
						 WHERE v.FieldLibraryID = uF.FieldLibraryID), NULL) ForeignKeyValye, 
		NULL,t.Addedon,t.Addedby,GETDATE(),t.Addedby, @IPAddress,'Y'
		FROM #tmpBulk t
		INNER JOIN FieldLibrary uF ON uF.FieldName = t.ColumnNameValue COLLATE SQL_Latin1_General_CP1_CI_AS 
		AND uF.ContractTemplateId = t.ContractTemplateId
		INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = t.BulkImportStagingId
		ORDER  BY cR.RequestId

		-- updating key fields
		
		SET @Columns = NULL
		SELECT @Columns = COALESCE(@Columns + ', ','') +'['+ TableColumnName + ']='+  ColumnName 
		FROM
		(
		SELECT 
		uK.TableColumnName, 
		CASE WHEN  uK.Ans ='user' THEN 'dbo.GetUserId('+ uH.ColumnName +')' 
			 WHEN  uK.Ans ='Status' THEN 'dbo.GetContractStatusId('+ uH.ColumnName +')' 
			 WHEN  uk.Ans='Currency' THEN 'CAST(ISNULL((SELECT TOP 1 CurrencyId FROM MstCurrency WHERE CurrencyCode=uB.'+ uH.ColumnName +'),0) AS VARCHAR(5))'
		     WHEN  uK.Ans ='Yes,No' AND uK.TableColumnName<>'IsCustomerPortalEnable' THEN ' CASE WHEN '+ uH.ColumnName + '=''Yes'' THEN ''Y'' ELSE ''N'' END '
			 WHEN  uK.Ans ='Yes,No' AND uK.TableColumnName='IsCustomerPortalEnable' THEN ' CASE WHEN '+ uH.ColumnName + '=''No'' THEN ''N'' 
			 WHEN ISNULL(dbo.IsHaveAccess('''','+CONVERT(VARCHAR(50),ISNULL(@ContractTypeId,0))+','+CONVERT(VARCHAR(50),ISNULL(@RoleID,0))+'),'''')='''' THEN ''N'' ELSE ''Y'' END '
			 ELSE 'uB.'+ uH.ColumnName END ColumnName
		FROM BulkImportStagingHeader uH
		INNER JOIN vwBulkImportKeyFields uK ON uK.FieldName = uH.ColumnNameValue  
		WHERE uH.BulkImportID = @BulkImportID
		AND uH.ColumnNameValue IN (SELECT FieldName FROM vwBulkImportKeyFields WHERE vwBulkImportKeyFields.Flag in('U','B')  AND ParentID=0)
		AND LEN(uK.TableColumnName)>0
		)tmp

			print 1

		SET @Qry = 'UPDATE ContractRequest SET ' + @Columns + ' FROM BulkImportStaging uB INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = uB.BulkImportStagingId WHERE uB.Head=''N'' AND uB.BulkImportID = ' + CAST(@BulkImportID AS VARCHAR(10))
	    
		EXEC (@Qry)
		

        UPDATE ContractRequest SET
		ContractRequest.ContractStatusId= 12  FROM 
	    (SELECT cR.RequestId  RequestId FROM BulkImportStaging uB INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = uB.BulkImportStagingId WHERE uB.Head='N' AND uB.BulkImportID =CAST(@BulkImportID AS VARCHAR(10))) tbl WHERE tbl.[RequestId]=ContractRequest.[RequestId]



		 UPDATE ContractRequest SET
		 ContractRequest.CountryId= (SELECT CountryId FROM MstCountry WHERE CountryName=RTRIM(LTRIM(ContractRequest.CountryName)))  FROM 
	    (SELECT cR.RequestId  RequestId FROM BulkImportStaging uB INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = uB.BulkImportStagingId WHERE uB.Head='N' AND uB.BulkImportID =CAST(@BulkImportID AS VARCHAR(10))) tbl WHERE tbl.[RequestId]=ContractRequest.[RequestId]

				--------------------Meta Fields INSERT----------------------------------------
		SET @Columns=NULL
		SET @Qry=NULL
	SELECT @Columns = COALESCE(@Columns + ', ','') + ColumnName FROM BulkImportStagingHeader WHERE BulkImportID = @BulkImportID
		AND ColumnNameValue IN (SELECT FieldName FROM vwBulkImportKeyFields where Flag NOT IN('U','B') AND ParentID=0) 


	SET @Qry='SELECT BulkImportStagingId, Col, Val
	FROM
	(SELECT BulkImportStagingId,
	'+@Columns+' 
	FROM BulkImportStaging WHERE Head=''N'' AND  BulkImportID = '+CAST(@BulkImportID AS VARCHAR(10))+' ) stu  
	UNPIVOT
	(VAL FOR Col IN ('+@Columns+')
	) AS val1'
	IF dbo.isTableExists('#Tmp') > 0 DROP TABLE #Tmp
	CREATE TABLE #Tmp(BulkImportStagingId INT,Col VARCHAR(150),Val VARCHAR(500),FieldId INT,Ans VARCHAR(150))

	INSERT INTO #Tmp(BulkImportStagingId,Col,Val)
	EXEC(@Qry)

	UPDATE #Tmp SET #Tmp.FieldId=uK.SrNo,#Tmp.Ans=uK.Ans
	 from 
	 #Tmp INNER JOIN BulkImportStagingHeader uH ON (#Tmp.Col  COLLATE SQL_Latin1_General_CP1_CI_AI)=uH.ColumnName
	INNER JOIN [vwBulkImportKeyFields] uK ON uK.FieldName= uH.ColumnNameValue AND uK.Flag NOT IN('U','B') AND uH.BulkImportID=@BulkImportID

	INSERT INTO ContractMetaData
	SELECT cR.RequestId,cR.ContractId,#Tmp.FieldId,
	CASE WHEN Ans='Checkbox' THEN	
		STUFF((SELECT ',' + CAST(FieldOptionId AS varchar(150)) FROM MetaDataConfiguratorValues WHERE MetaDataConfiguratorValues.FieldId=#Tmp.FieldId 
		AND MetaDataConfiguratorValues.FieldOptionValue in(SELECT items FROM dbo.Split(CONVERT(VARCHAR(50),(#Tmp.Val  COLLATE SQL_Latin1_General_CP1_CI_AI)),','))
		FOR XML PATH('')), 1, 1, '')
	WHEN Ans='Dropdown' OR Ans='RadioButton' THEN 
	CAST((SELECT TOP 1 FieldOptionId FROM MetaDataConfiguratorValues WHERE MetaDataConfiguratorValues.FieldId=#Tmp.FieldId AND MetaDataConfiguratorValues.FieldOptionValue=CONVERT(VARCHAR(50),(#Tmp.Val  COLLATE SQL_Latin1_General_CP1_CI_AI)))AS varchar(150))
	WHEN ISDATE(Val)=1 THEN dbo.DateFormat(Val)
	ELSE Val END Val,
	m.MetaDataType,
	cR.AddedBy,
	cR.AddedOn,
	cR.IP FROM #Tmp
	INNER JOIN MetaDataConfigurator m ON m.FieldId=#Tmp.FieldId
	INNER JOIN ContractRequest cR ON cR.BulkImportStagingId=#Tmp.BulkImportStagingId  

		--------------------END Meta Fields INSERT------------------------------------
		-----------------------INSERT Metadata Reminder ----------------------------------
SET @Columns=NULL
SET @Qry=NULL
SELECT @Columns = COALESCE(@Columns + ', ','') + ColumnName FROM BulkImportStagingHeader WHERE BulkImportID = @BulkImportID
	AND ColumnNameValue IN (SELECT FieldName FROM vwBulkImportKeyFields where Flag IN('U','B') AND ParentID<>0 ) 

	SET @Qry='SELECT BulkImportStagingId, Col, Val
	FROM
	(SELECT BulkImportStagingId,
	'+@Columns+' 
	FROM BulkImportStaging WHERE Head=''N'' AND  BulkImportID = '+CAST(@BulkImportID AS VARCHAR(10))+' ) stu  
	UNPIVOT
	(VAL FOR Col IN ('+@Columns+')
	) AS val1'

	IF dbo.isTableExists('#Tmp1') > 0 DROP TABLE #Tmp1
	CREATE TABLE #Tmp1(ID INT IDENTITY(1,1),BulkImportStagingId INT,Col VARCHAR(1000),Val VARCHAR(1000),FieldId INT,Ans VARCHAR(1000),reminder  VARCHAR(1000))
	
	INSERT INTO #Tmp1(BulkImportStagingId,Col,Val)
	EXEC(@Qry)
	UPDATE #Tmp1 SET #Tmp1.FieldId=uK.ParentID,#Tmp1.Ans=uK.Ans,#Tmp1.reminder =uk.TableColumnName
	 from 
	 #Tmp1 INNER JOIN BulkImportStagingHeader uH ON (#Tmp1.Col COLLATE SQL_Latin1_General_CP1_CI_AI)=uH.ColumnName
	INNER JOIN [vwBulkImportKeyFields] uK ON uK.FieldName= uH.ColumnNameValue AND uK.Flag IN('U','B')  AND uK.ParentID<>0 AND uH.BulkImportID=@BulkImportID

IF dbo.isTableExists('#TmpReminder') > 0 DROP TABLE #TmpReminder
	CREATE TABLE #TmpReminder(ID INT IDENTITY(1,1),RequestId INT,ContractId INT,FieldId INT,[Days] INT,User1 INT,User2 INT,Reminder VARCHAR(500))
	DECLARE @ID INT=1,@Maxcount INT
	SELECT @Maxcount=COUNT(1) FROM  #Tmp1
	

	WHILE @ID<=@Maxcount
	BEGIN
		INSERT INTO #TmpReminder

		SELECT cR.RequestId,cR.ContractId,#Tmp1.FieldId,
			CASE WHEN Ans='Yes,No' AND reminder='ReminderAlert1days' THEN 1
			WHEN  Ans='Yes,No' AND reminder='ReminderAlert7days' THEN 7
			WHEN  Ans='Yes,No' AND reminder='ReminderAlert30days' THEN 30
			WHEN  Ans='Yes,No' AND reminder='ReminderAlert60days' THEN 60
			WHEN  Ans='Yes,No' AND reminder='ReminderAlert90days' THEN 90
			WHEN  Ans='Yes,No' AND reminder='ReminderAlert180days' THEN 180	 END [days],	
			CASE WHEN Ans ='user' AND (reminder='ReminderAlert1dayUserID1' OR reminder='ReminderAlert7daysUserID1' OR
			reminder='ReminderAlert30daysUserID1' OR reminder='ReminderAlert60daysUserID1' OR reminder='ReminderAlert90daysUserID1'OR
			reminder='ReminderAlert180daysUserID1') THEN dbo.GetUserId(#Tmp1.Val) END [User1],
			CASE WHEN Ans ='user' AND (reminder='ReminderAlert1dayUserID2' OR reminder='ReminderAlert7daysUserID2' OR
			reminder='ReminderAlert30daysUserID2' OR reminder='ReminderAlert60daysUserID2' OR reminder='ReminderAlert90daysUserID2'OR
			reminder='ReminderAlert180daysUserID2') THEN dbo.GetUserId(#Tmp1.Val) END [User2]
			,reminder
		FROM #Tmp1 INNER JOIN ContractRequest cR ON cR.BulkImportStagingId=#Tmp1.BulkImportStagingId AND #Tmp1.ID=@ID

		SET @ID=@ID+1;
	END

IF dbo.isTableExists('#TmpFinalReminder') > 0 DROP TABLE #TmpFinalReminder
	CREATE TABLE #TmpFinalReminder(ID INT IDENTITY(1,1),RequestId INT,ContractId INT,FieldId INT,[Days] INT,User1 INT,User2 INT,PreID INT)

	INSERT INTO #TmpFinalReminder(RequestId,ContractId,FieldId,[Days],PreID)
	SELECT RequestId,ContractId,FieldId,[Days],ID FROM #TmpReminder WHERE Reminder IN('ReminderAlert1days',
	'ReminderAlert7days','ReminderAlert30days','ReminderAlert60days','ReminderAlert90days','ReminderAlert180days')

UPDATE #TmpFinalReminder SET #TmpFinalReminder.User1= #TmpReminder.User1 FROM #TmpReminder
 INNER JOIN  #TmpFinalReminder ON  #TmpFinalReminder.PreID=#TmpReminder.ID-1 AND Reminder 
	IN('ReminderAlert1dayUserID1',
	'ReminderAlert7daysUserID1','ReminderAlert30daysUserID1','ReminderAlert60daysUserID1','ReminderAlert90daysUserID1','ReminderAlert180daysUserID1')

UPDATE #TmpFinalReminder SET #TmpFinalReminder.User2= #TmpReminder.User2 
FROM #TmpReminder
 INNER JOIN  #TmpFinalReminder ON  #TmpFinalReminder.PreID= CASE WHEN #TmpFinalReminder.user1 is not null THEN #TmpReminder.ID-2 ELSE #TmpReminder.ID-1 END  AND Reminder 
	IN('ReminderAlert1dayUserID2',
	'ReminderAlert7daysUserID2','ReminderAlert30daysUserID2','ReminderAlert60daysUserID2','ReminderAlert90daysUserID2','ReminderAlert180daysUserID2')

	INSERT INTO ImportantDatesMetaDataReminders(RequestId,ContractId,MetaDataFieldId,ReminderDays,ReminderUser1,ReminderUser2)
		select RequestId,ContractId,FieldId,Days,User1,User2 from #TmpFinalReminder
-------------------------END MetaData Reminder-------------------------------------------
		/**Added By Nilesh to add Client Details*****/
		
		
		CREATE TABLE #tmpClientInformation (ID INT IDENTITY (1,1), RequestId INT, ClientName VARCHAR(MAX), ContactNumber VARCHAR(MAX), EmailId VARCHAR (MAX), isCustomer VARCHAR(MAX),
	    AddedBy INT, Addedon DATETIME,ModifiedBy INT,ModifiedOn DATETIME,IP VARCHAR(MAX),Address VARCHAR(MAX),CountryName VARCHAR(MAX),ContractingPartyName VARCHAR(MAX),CityName VARCHAR(MAX),StateName VARCHAR(MAX),Pincode VARCHAR(MAX))



		INSERT INTO #tmpClientInformation  
        SELECT cR.RequestId ,cR.ClientName,cR.ContactNumber,cR.EmailId,cR.isCustomer,cR.AddedBy,cR.Addedon,cR.Modifiedby,cR.Modifiedon,cR.IP,cR.Address,cR.CountryName,cR.ContractingPartyName,cR.CityName,cR.StateName,cR.PinCode
		FROM BulkImportStaging uB INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = uB.BulkImportStagingId
		WHERE uB.Head='N' AND uB.BulkImportID =CAST(@BulkImportID AS VARCHAR(10))

	
	DECLARE @Cnt INT =1
	DECLARE @MaxCnt INT = (SELECT COUNT(1) FROM #tmpClientInformation)
	DECLARE @ClientId INT
	DECLARE @CountryId INT
	DECLARE @RequestId INT, @ClientName VARCHAR(MAX), @ContactNumber VARCHAR(MAX), @EmailId VARCHAR (MAX),@isCustomer VARCHAR(MAX), @AddedBy INT, @Addedon DATETIME,@ModifiedBy INT,@ModifiedOn DATETIME,@IP VARCHAR(MAX),@Address VARCHAR(MAX),@CountryName VARCHAR(MAX),@ContractingPartyName VARCHAR(MAX),
	@CityName VARCHAR(MAX),@StateName VARCHAR(MAX),@Pincode VARCHAR(MAX)

	DECLARE @ActivityTypeID INT
		SET @ActivityTypeID=(SELECT ActivityTypeId FROM MstActivityType WHERE  isSystemType='K')

	WHILE @Cnt <=@MaxCnt
	BEGIN
			BEGIN

			SELECT  @RequestId=t.[RequestId] , @ClientName=t.[ClientName],@isCustomer=CASE WHEN t.[isCustomer]='Supplier' THEN 'N' WHEN t.[isCustomer]='Customer' THEN 'Y' ELSE 'O' END , @ContactNumber=t.[ContactNumber] , 
			@EmailId=t.[EmailId], @AddedBy=t.[AddedBy], @Addedon=t.[Addedon],@ModifiedBy=t.[ModifiedBy],@ModifiedOn=t.[ModifiedOn],@IP=t.[IP],@Address=t.[Address],@CountryName=t.[CountryName],@ContractingPartyName=t.[ContractingPartyName],
			@CityName=t.[CityName],@StateName=t.[StateName],@Pincode=t.[PinCode]
			FROM #tmpClientInformation t WHERE ID=@Cnt

			SET @CountryId=(SELECT CountryId FROm MstCountry WHERE CountryName=@CountryName)

			IF NOT EXISTS(SELECT 1 FROm Client WHERE ClientName=@ClientName)
			BEGIN
				INSERT INTO Client (isCustomer,ClientName,Addedon,Addedby,Modifiedon,Modifiedby,IP)
				SELECT @isCustomer,@ClientName,@Addedon,@AddedBy,@ModifiedOn,@ModifiedBy,@IP
				SET @ClientId=SCOPE_IDENTITY()
			END
			ELSE
			BEGIN
			  SET @ClientId=(SELECT TOP 1 ClientId FROM Client WHERE ClientName=@ClientName)
			END

			IF NOT EXISTS(SELECT 1 FROM ClientAddressDetails WHERE Address=@Address AND CountryId=@CountryId AND PinCode=@PinCode AND CityName=@CityName AND StateName=@StateName AND ClientId=@ClientId)
			BEGIN
			INSERT INTO ClientAddressDetails(ClientId,IsPrimary,Address,CountryId,StateId,CityId,PinCode,Street2,CityName,StateName)
			SELECT @ClientId,'Y',@Address,@CountryId,NULL,NULL,@Pincode,NULL,@CityName,@StateName
			END

			IF NOT EXISTS(SELECT 1 FROM ClientContactDetails WHERE ContactNumber=@ContactNumber AND ClientId=@ClientId)
			BEGIN	
			INSERT INTO ClientContactDetails(ClientId,IsPrimary,ContactNumber)
			SELECT @ClientId,'Y',@ContactNumber
			END
	
			IF NOT EXISTS(SELECT 1 FROM ClientEmailDetails WHERE EmailID=@EmailId AND ClientId=@ClientId)
			BEGIN		
			INSERT INTO ClientEmailDetails(ClientId,IsPrimary,EmailID)
			SELECT @ClientId,'Y',@EmailId
			END

		DECLARE @ContractingPartyId INT
		SET @ContractingPartyId=(SELECT TOP 1 ContractingPartyId  FROM mstContractingParty WHERE RTRIM(LTRIM(ContractingPartyName))=@ContractingPartyName)

		UPDATE ContractRequest  SET ClientName=@ClientName, ClientId=@ClientId,isCustomer=@isCustomer,ContractingTypeId=@ContractingPartyId,CountryId=@CountryId WHERE RequestId=@RequestId
		END
		SET @Cnt = @Cnt + 1

		INSERT INTO Activity 
							(RequestId, ActivityTypeId, ActivityStatusId, ActivityText, ActivityDate,  ReminderDate, isForCustomer, Addedby, Addedon, IP, AssignToId, isDismissed,IsFromCalendar,IsFromOther) 
			 SELECT RequestId,  ActivityTypeId, ActivityStatusId, ActivityText, ActivityDate,ReminderDate, IsForCustomer, AddedBy, AddedOn, IP, AssignedTo, isDismissed, IsFromCalendar, IsFromOther
			 FROM(
					SELECT @RequestId RequestId, @ActivityTypeID ActivityTypeId, 1 ActivityStatusId, 'Contract expiration on  '+dbo.DateFormat(ExpirationDate) ActivityTExt, ExpirationDate ActivityDate,
						DATEADD(DD, -180, dbo.DateOnly(ExpirationDate)) ReminderDate, 'Y' IsForCustomer, @AddedBy AddedBy,GETDATE() AddedOn,@IpAddress IP, ExpirationAlert180daysUserID1 AssignedTo, 'N' isDismissed,'Y' IsFromCalendar,'KeyField'  IsFromOther
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert180days,'N')='Y' AND ISNULL(ExpirationAlert180daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -180, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert180daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert180days,'N')='Y'  AND ISNULL(ExpirationAlert180daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -90, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert90daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert90days,'N')='Y'  AND ISNULL(ExpirationAlert90daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -90, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert90daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert90days,'N')='Y'  AND ISNULL(ExpirationAlert90daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -60, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert60daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert60days,'N')='Y'  AND ISNULL(ExpirationAlert60daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -60, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert60daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert60days,'N')='Y'  AND ISNULL(ExpirationAlert60daysUserID2,0)<>0
					UNION ALL  
					SELECT @RequestId RequestId, @ActivityTypeID ActivityTypeId, 1 ActivityStatusId, 'Contract expiration on  '+dbo.DateFormat(ExpirationDate) ActivityTExt, ExpirationDate ActivityDate,	DATEADD(DD, -30, dbo.DateOnly(ExpirationDate)) ReminderDate, 'Y' IsForCustomer, @AddedBy AddedBy,GETDATE() AddedOn,@IpAddress IP, ExpirationAlert30daysUserID1 AssignedTo, 'N' isDismissed,'Y' IsFromCalendar,'KeyField'  IsFromOther
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert30days,'N')='Y' AND ISNULL(ExpirationAlert30daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -30, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert30daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert30days,'N')='Y'  AND ISNULL(ExpirationAlert30daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -7, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert7daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert7days,'N')='Y'  AND ISNULL(ExpirationAlert7daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -7, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert7daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert7days,'N')='Y'  AND ISNULL(ExpirationAlert7daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on  '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -1, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert24HrsUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert24Hrs,'N')='Y'  AND ISNULL(ExpirationAlert24HrsUserID1,0)<>0
					UNION ALL 
					SELECT  @RequestId, @ActivityTypeID, 1, 'Contract expiration on  '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -1, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert24HrsUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert24Hrs,'N')='Y'  AND ISNULL(ExpirationAlert24HrsUserID2,0)<>0

					UNION ALL
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -180, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert180daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert180days,'N')='Y'  AND ISNULL(RenewalAlert180daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -180, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert180daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert180days,'N')='Y'  AND ISNULL(RenewalAlert180daysUserID2,0)<>0
					UNION ALL
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -90, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert90daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert90days,'N')='Y'  AND ISNULL(RenewalAlert90daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -90, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert90daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert90days,'N')='Y'  AND ISNULL(RenewalAlert90daysUserID2,0)<>0
					UNION ALL
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -60, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert60daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert60days,'N')='Y'  AND ISNULL(RenewalAlert60daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -60, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert60daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert60days,'N')='Y'  AND ISNULL(RenewalAlert60daysUserID2,0)<>0
					

					UNION ALL
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -30, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert30daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert30days,'N')='Y'  AND ISNULL(RenewalAlert30daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -30, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert30daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert30days,'N')='Y'  AND ISNULL(RenewalAlert30daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -7, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert7daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert7days,'N')='Y'  AND ISNULL(RenewalAlert7daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -7, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert7daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert7days,'N')='Y'  AND ISNULL(RenewalAlert7daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -1, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert24HrsUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert24Hrs,'N')='Y'  AND ISNULL(RenewalAlert24HrsUserID1,0)<>0
					UNION ALL 
					SELECT  @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -1, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert24HrsUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert24Hrs,'N')='Y' AND ISNULL(RenewalAlert24HrsUserID2,0)<>0
					UNION ALL 
					SELECT DISTINCT ContractMetaData.requestid, @ActivityTypeID, 1 ActivityStatusId,fieldname +' reminder on '+ FieldValue As ActivityTExt,FieldValue AS ActivityDate,
					DATEADD(DD, -ReminderDays, dbo.DateOnly(FieldValue)) ReminderDate, 'Y' IsForCustomer,AddedBy,AddedOn,ContractMetaData.IPAddress AS IP,ReminderUser1 AS AssignedTo,
					'N' isDismissed,'Y' IsFromCalendar,'KeyField'  IsFromOther
					FROM ImportantDatesMetaDataReminders
					INNER JOIN ContractMetaData ON ContractMetaData.MetaDataFieldId=ImportantDatesMetaDataReminders.MetaDataFieldId and 
					ContractMetaData.requestid=ImportantDatesMetaDataReminders.requestid  and MetaDataType='Important Dates'
					INNER JOIN MetaDataConfigurator ON FieldId=ContractMetaData.MetaDataFieldId
					WHERE ContractMetaData.requestid=@RequestId  AND ISNULL(ReminderUser1,0)<>0
					UNION ALL 
					SELECT DISTINCT ContractMetaData.requestid, 1 ActivityTypeId, 1 ActivityStatusId,fieldname +' reminder on '+ FieldValue As ActivityTExt,FieldValue AS ActivityDate,DATEADD(DD, -ReminderDays, dbo.DateOnly(FieldValue)) ReminderDate, 'Y' IsForCustomer,
					AddedBy,AddedOn,ContractMetaData.IPAddress AS IP,ReminderUser2 AS AssignedTo,'N' isDismissed,'Y' IsFromCalendar,'KeyField'  IsFromOther
					FROM ImportantDatesMetaDataReminders
					INNER JOIN ContractMetaData ON ContractMetaData.MetaDataFieldId=ImportantDatesMetaDataReminders.MetaDataFieldId and 
					ContractMetaData.requestid=ImportantDatesMetaDataReminders.requestid  and MetaDataType='Important Dates'
					INNER JOIN MetaDataConfigurator ON FieldId=ContractMetaData.MetaDataFieldId
					WHERE ContractMetaData.requestid=@RequestId AND ISNULL(ReminderUser2,0)<>0
			)tmp

END -- end of while


		IF @trancount = 0
            COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			DECLARE @error INT, @message VARCHAR(4000), @xstate INT;
			SELECT @error = ERROR_NUMBER(), @message = ERROR_MESSAGE(), @xstate = XACT_STATE();
			IF @xstate = -1
				ROLLBACK;
			IF @xstate = 1 and @trancount = 0
				ROLLBACK
			IF @xstate = 1 and @trancount > 0
				ROLLBACK TRANSACTION usp_my_procedure_name;

			RAISERROR ('usp_my_procedure_name: %d: %s', 16, 1, @error, @message) ;

		END CATCH

		IF dbo.isTableExists('#tmpBulk') > 0 DROP TABLE #tmpBulk
	
	END



	/*
	SELECT unPVT.BulkImportID, unPVT.ContractTemplateId, unPVT.BulkImportStagingId, unPVT.asAnswer, 
						   unPVT.ColumnName,uH.ColumnNameValue,unPVT.Addedby, unPVT.Addedon
	FROM BulkImportStaging 
	UNPIVOT 
	(
	 asAnswer FOR ColumnName IN (Col1,Col2,Col3) -- dynamic column str
	) unPVT
	INNER JOIN BulkImportStagingHeader uH ON uH.ColumnName = unPVT.ColumnName AND uH.BulkImportID = unPVT.BulkImportID
	WHERE unPVT.BulkImportID = @BulkImportID AND unPVT.Head='N' AND unPVT.IsPass= 'Y'
	*/

	/*
		SET @Columns = NULL
		SELECT @Columns = COALESCE(@Columns + ', ','') +'['+ TableColumnName + ']='+  ColumnName 
		FROM
		(
		SELECT 
		uK.TableColumnName, 
		CASE WHEN  uK.Ans ='user' THEN 'dbo.GetUserId('+ uH.ColumnName +')' ELSE 'uB.'+ uH.ColumnName END ColumnName
		FROM BulkImportStagingHeader uH
		INNER JOIN vwBulkImportKeyFields uK ON uK.FieldName = uH.ColumnNameValue  
		WHERE uH.BulkImportID = @BulkImportID
		AND uH.ColumnNameValue IN (SELECT FieldName FROM vwBulkImportKeyFields)
		AND LEN(uK.TableColumnName)>0
		)tmp
		*/

END













/*

exec BulkImportAdd 10119



SELECT * FROm Client

SELECT * FROM ClientAddressDetails

SELECT * FROM ClientContactDetails

SELECT * FROm ContractRequest

*/

GO

ALTER PROCEDURE [dbo].[BulkImportAdd_Live742016]
@BulkImportID INT
	
AS
BEGIN
	
	SET NOCOUNT ON

	IF dbo.isTableExists('#tmpBulk') > 0 DROP TABLE #tmpBulk
	DECLARE @trancount INT;
    SET @trancount = @@trancount;

	CREATE TABLE #tmpBulk (BulkImportID INT, ContractTemplateId INT, BulkImportStagingId INT, asAnswer VARCHAR (MAX), 
	ColumnName VARCHAR(500), ColumnNameValue VARCHAR(1000),Addedby INT, Addedon DATETIME)


	DECLARE @Qry VARCHAR(MAX)
	DECLARE @Columns VARCHAR(MAX)
	DECLARE @RequestContractIDMax BIGINT
	DECLARE @ContractIDMax BIGINT
	
	DECLARE @ContractTemplateId INT = (SELECT MIN(ContractTemplateId) FROM BulkImport WHERE BulkImportID = @BulkImportID)
	DECLARE @ContractTypeId INT = ( SELECT ContractTypeId FROM ContractTemplate WHERE ContractTemplateId = @ContractTemplateId)
	
	DECLARE @AddedById INT
	DECLARE @IPAddress VARCHAR(100)

	DECLARE @FullName VARCHAR(1000)
	DECLARE @DepartmentId INT
		
    SELECT @AddedById = MIN(ModifiedBy), @IPAddress = MIN(IPAddress) FROM BulkImport WHERE BulkImportID = @BulkImportID
    SELECT @FullName = FullName, @DepartmentId = DepartmentId FROM MstUsers WHERE UsersId = @AddedById

	
	IF (SELECT COUNT(1) FROM BulkImportStagingHeader WHERE BulkImportId= @BulkImportId) = (SELECT COUNT(1) FROM vwBulkImportKeyFields)
	BEGIN
	--	SELECT COALESCE(@Columns + ', ','') + ColumnName FROM BulkImportStagingHeader WHERE BulkImportID = @BulkImportID 
		SELECT @Columns = COALESCE(@Columns + ', ','') + ColumnName FROM BulkImportStagingHeader WHERE BulkImportID = @BulkImportID 
		--AND ColumnNameValue NOT IN (SELECT FieldName FROM vwBulkImportKeyFields) 
	END
	ELSE
	BEGIN
		
		SELECT @Columns = COALESCE(@Columns + ', ','') + ColumnName FROM BulkImportStagingHeader WHERE BulkImportID = @BulkImportID 
		AND ColumnNameValue NOT IN (SELECT FieldName FROM vwBulkImportKeyFields) 
	END
	
	SET @Qry = ' SELECT unPVT.BulkImportID, unPVT.ContractTemplateId, unPVT.BulkImportStagingId, unPVT.asAnswer, 
						   unPVT.ColumnName,uH.ColumnNameValue,unPVT.Addedby, unPVT.Addedon FROM BulkImportStaging 
				UNPIVOT 
				(
				 asAnswer FOR ColumnName IN ('+ @Columns +') 
				) unPVT
				INNER JOIN BulkImportStagingHeader uH ON uH.ColumnName = unPVT.ColumnName AND uH.BulkImportID = unPVT.BulkImportID
				WHERE unPVT.BulkImportID = ' + CAST(@BulkImportID AS VARCHAR(10)) + ' AND unPVT.Head=''N'' AND unPVT.IsPass= ''Y''		
				'

	INSERT INTO #tmpBulk (BulkImportID, ContractTemplateId, BulkImportStagingId, asAnswer, 
						  ColumnName, ColumnNameValue, Addedby, Addedon)
	EXEC (@Qry)


	IF (SELECT COUNT(1) FROm #tmpBulk)> 0
	BEGIN
			
		--SELECT  @RequestContractIDMax = MAX(ISNULL(RequestContractID,100000))+1 , @ContractIDMax = MAX(ISNULL(ContractID,100000))+1 FROM ContractRequest

		SELECT  @RequestContractIDMax = ISNULL(MAX(RequestContractID),100000)+1 , @ContractIDMax = ISNULL(MAX(ContractID),100000)+1 FROM ContractRequest

		BEGIN TRY
	
		IF @trancount = 0
            BEGIN TRANSACTION
		
        ELSE
            SAVE TRANSACTION usp_my_procedure_name;
		
		
		INSERT INTO ContractRequest 
		(
		RequestContractID, 
		ContractID, 
		ContractTypeId, RequestTypeId, ContractTemplateId, RequesterUserId, 
		RequestDescription, AssignToDepartmentId, AssignToUserID, 
		IsApprovalRequired, Addedon, Addedby,Modifiedon,Modifiedby,  IP, BulkImportStagingId, IsBulkImport, 
		ContractStatusID
		)
		
		SELECT 
		ROW_NUMBER() OVER (ORDER BY s.BulkImportStagingId)+ @RequestContractIDMax, 
		ROW_NUMBER() OVER (ORDER BY s.BulkImportStagingId)+ @ContractIDMax, 
		@ContractTypeId, 1, @ContractTemplateId, s.AddedBy, 
		'Bulk Imported by: '+ @FullName + ' on: '+ dbo.DateFormat(s.Addedon), @DepartmentId, s.AddedBy, 
		'Y',s.Addedon,s.Addedby,GETDATE(),s.Addedby, s.IP, s.BulkImportStagingId, 'Y', 
		9 
		-- (All bulk imported are bydefault Active Contracts )
		FROM BulkImportStaging s
		WHERE s.BulkImportID = @BulkImportID AND s.IsPass = 'Y' 
		
		INSERT INTO ContractQuestionsAnswers 
		(
		ContractId, RequestId, ContractTemplateId, FieldLibraryID, Question,
		TextValue, NumericValue, DecimalValue, DateValue, ForeignKeyValye,
		IsDraft, Addedon, Addedby, Modifiedon, Modifiedby, IP, IsBulkImport
		)
    
		SELECT cR.ContractId,cR.RequestId, t.ContractTemplateId, uF.FieldLibraryID, ColumnNameValue,
		IIF (uF.FieldTypeID = 1, t.asAnswer, NULL) TextValue, 
		IIF (uF.FieldTypeID = 4, t.asAnswer, NULL) NumericValue,
		IIF (uF.FieldTypeID = 5, t.asAnswer, NULL) DecimalValue, 
		IIF (uF.FieldTypeID = 7, t.asAnswer, NULL) DateValue, 
		IIF (uF.FieldTypeID = 9,
						(SELECT MAX(MstView.Id) FROM FieldLibraryValues v 
						 INNER JOIN MstView ON MstView.PickListMasterId = v.FieldValue 
						 AND MstView.Name = t.asAnswer COLLATE SQL_Latin1_General_CP1_CI_AS 
						 WHERE v.FieldLibraryID = uF.FieldLibraryID), NULL) ForeignKeyValye, 
		NULL,t.Addedon,t.Addedby,GETDATE(),t.Addedby, @IPAddress,'Y'
		FROM #tmpBulk t
		INNER JOIN FieldLibrary uF ON uF.FieldName = t.ColumnNameValue COLLATE SQL_Latin1_General_CP1_CI_AS 
		AND uF.ContractTemplateId = t.ContractTemplateId
		INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = t.BulkImportStagingId
		ORDER  BY cR.RequestId

		-- updating key fields
		
		SET @Columns = NULL
		SELECT @Columns = COALESCE(@Columns + ', ','') +'['+ TableColumnName + ']='+  ColumnName 
		FROM
		(
		SELECT 
		uK.TableColumnName, 
		CASE WHEN  uK.Ans ='user' THEN 'dbo.GetUserId('+ uH.ColumnName +')' 
			 WHEN  uK.Ans ='Status' THEN 'dbo.GetContractStatusId('+ uH.ColumnName +')' 
			 WHEN  uk.Ans='Currency' THEN 'CAST(ISNULL((SELECT TOP 1 CurrencyId FROM MstCurrency WHERE CurrencyCode=uB.'+ uH.ColumnName +'),0) AS VARCHAR(5))'
		     WHEN  uK.Ans ='Yes,No' AND uK.TableColumnName<>'IsCustomerPortalEnable' THEN ' CASE WHEN '+ uH.ColumnName + '=''Yes'' THEN ''Y'' ELSE ''N'' END '
			 WHEN  uK.Ans ='Yes,No' AND uK.TableColumnName='IsCustomerPortalEnable' THEN ' CASE WHEN '+ uH.ColumnName + '=''No'' THEN ''N'' ELSE ''Y'' END '
			 ELSE 'uB.'+ uH.ColumnName END ColumnName
		FROM BulkImportStagingHeader uH
		INNER JOIN vwBulkImportKeyFields uK ON uK.FieldName = uH.ColumnNameValue  
		WHERE uH.BulkImportID = @BulkImportID
		AND uH.ColumnNameValue IN (SELECT FieldName FROM vwBulkImportKeyFields WHERE vwBulkImportKeyFields.Flag in('U','B')  AND ParentID=0)
		AND LEN(uK.TableColumnName)>0
		)tmp

			print 1

		SET @Qry = 'UPDATE ContractRequest SET ' + @Columns + ' FROM BulkImportStaging uB INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = uB.BulkImportStagingId WHERE uB.Head=''N'' AND uB.BulkImportID = ' + CAST(@BulkImportID AS VARCHAR(10))
	    
		EXEC (@Qry)
		

        UPDATE ContractRequest SET
		ContractRequest.ContractStatusId= 12  FROM 
	    (SELECT cR.RequestId  RequestId FROM BulkImportStaging uB INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = uB.BulkImportStagingId WHERE uB.Head='N' AND uB.BulkImportID =CAST(@BulkImportID AS VARCHAR(10))) tbl WHERE tbl.[RequestId]=ContractRequest.[RequestId]



		 UPDATE ContractRequest SET
		 ContractRequest.CountryId= (SELECT CountryId FROM MstCountry WHERE CountryName=RTRIM(LTRIM(ContractRequest.CountryName)))  FROM 
	    (SELECT cR.RequestId  RequestId FROM BulkImportStaging uB INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = uB.BulkImportStagingId WHERE uB.Head='N' AND uB.BulkImportID =CAST(@BulkImportID AS VARCHAR(10))) tbl WHERE tbl.[RequestId]=ContractRequest.[RequestId]

				--------------------Meta Fields INSERT----------------------------------------
		SET @Columns=NULL
		SET @Qry=NULL
	SELECT @Columns = COALESCE(@Columns + ', ','') + ColumnName FROM BulkImportStagingHeader WHERE BulkImportID = @BulkImportID
		AND ColumnNameValue IN (SELECT FieldName FROM vwBulkImportKeyFields where Flag NOT IN('U','B') AND ParentID=0) 


	SET @Qry='SELECT BulkImportStagingId, Col, Val
	FROM
	(SELECT BulkImportStagingId,
	'+@Columns+' 
	FROM BulkImportStaging WHERE Head=''N'' AND  BulkImportID = '+CAST(@BulkImportID AS VARCHAR(10))+' ) stu  
	UNPIVOT
	(VAL FOR Col IN ('+@Columns+')
	) AS val1'
	IF dbo.isTableExists('#Tmp') > 0 DROP TABLE #Tmp
	CREATE TABLE #Tmp(BulkImportStagingId INT,Col VARCHAR(150),Val VARCHAR(500),FieldId INT,Ans VARCHAR(150))

	INSERT INTO #Tmp(BulkImportStagingId,Col,Val)
	EXEC(@Qry)

	UPDATE #Tmp SET #Tmp.FieldId=uK.SrNo,#Tmp.Ans=uK.Ans
	 from 
	 #Tmp INNER JOIN BulkImportStagingHeader uH ON (#Tmp.Col  COLLATE SQL_Latin1_General_CP1_CI_AI)=uH.ColumnName
	INNER JOIN [vwBulkImportKeyFields] uK ON uK.FieldName= uH.ColumnNameValue AND uK.Flag NOT IN('U','B') AND uH.BulkImportID=@BulkImportID

	INSERT INTO ContractMetaData
	SELECT cR.RequestId,cR.ContractId,#Tmp.FieldId,
	CASE WHEN Ans='Checkbox' THEN	
		STUFF((SELECT ',' + CAST(FieldOptionId AS varchar(150)) FROM MetaDataConfiguratorValues WHERE MetaDataConfiguratorValues.FieldId=#Tmp.FieldId 
		AND MetaDataConfiguratorValues.FieldOptionValue in(SELECT items FROM dbo.Split(CONVERT(VARCHAR(50),(#Tmp.Val  COLLATE SQL_Latin1_General_CP1_CI_AI)),','))
		FOR XML PATH('')), 1, 1, '')
	WHEN Ans='Dropdown' OR Ans='RadioButton' THEN 
	CAST((SELECT TOP 1 FieldOptionId FROM MetaDataConfiguratorValues WHERE MetaDataConfiguratorValues.FieldId=#Tmp.FieldId AND MetaDataConfiguratorValues.FieldOptionValue=CONVERT(VARCHAR(50),(#Tmp.Val  COLLATE SQL_Latin1_General_CP1_CI_AI)))AS varchar(150))
	WHEN ISDATE(Val)=1 THEN dbo.DateFormat(Val)
	ELSE Val END Val,
	m.MetaDataType,
	cR.AddedBy,
	cR.AddedOn,
	cR.IP FROM #Tmp
	INNER JOIN MetaDataConfigurator m ON m.FieldId=#Tmp.FieldId
	INNER JOIN ContractRequest cR ON cR.BulkImportStagingId=#Tmp.BulkImportStagingId  

		--------------------END Meta Fields INSERT------------------------------------
		-----------------------INSERT Metadata Reminder ----------------------------------
SET @Columns=NULL
SET @Qry=NULL
SELECT @Columns = COALESCE(@Columns + ', ','') + ColumnName FROM BulkImportStagingHeader WHERE BulkImportID = @BulkImportID
	AND ColumnNameValue IN (SELECT FieldName FROM vwBulkImportKeyFields where Flag IN('U','B') AND ParentID<>0 ) 

	SET @Qry='SELECT BulkImportStagingId, Col, Val
	FROM
	(SELECT BulkImportStagingId,
	'+@Columns+' 
	FROM BulkImportStaging WHERE Head=''N'' AND  BulkImportID = '+CAST(@BulkImportID AS VARCHAR(10))+' ) stu  
	UNPIVOT
	(VAL FOR Col IN ('+@Columns+')
	) AS val1'

	IF dbo.isTableExists('#Tmp1') > 0 DROP TABLE #Tmp1
	CREATE TABLE #Tmp1(ID INT IDENTITY(1,1),BulkImportStagingId INT,Col VARCHAR(1000),Val VARCHAR(1000),FieldId INT,Ans VARCHAR(1000),reminder  VARCHAR(1000))
	
	INSERT INTO #Tmp1(BulkImportStagingId,Col,Val)
	EXEC(@Qry)
	UPDATE #Tmp1 SET #Tmp1.FieldId=uK.ParentID,#Tmp1.Ans=uK.Ans,#Tmp1.reminder =uk.TableColumnName
	 from 
	 #Tmp1 INNER JOIN BulkImportStagingHeader uH ON (#Tmp1.Col COLLATE SQL_Latin1_General_CP1_CI_AI)=uH.ColumnName
	INNER JOIN [vwBulkImportKeyFields] uK ON uK.FieldName= uH.ColumnNameValue AND uK.Flag IN('U','B')  AND uK.ParentID<>0 AND uH.BulkImportID=@BulkImportID

IF dbo.isTableExists('#TmpReminder') > 0 DROP TABLE #TmpReminder
	CREATE TABLE #TmpReminder(ID INT IDENTITY(1,1),RequestId INT,ContractId INT,FieldId INT,[Days] INT,User1 INT,User2 INT,Reminder VARCHAR(500))
	DECLARE @ID INT=1,@Maxcount INT
	SELECT @Maxcount=COUNT(1) FROM  #Tmp1
	

	WHILE @ID<=@Maxcount
	BEGIN
		INSERT INTO #TmpReminder

		SELECT cR.RequestId,cR.ContractId,#Tmp1.FieldId,
			CASE WHEN Ans='Yes,No' AND reminder='ReminderAlert1days' THEN 1
			WHEN  Ans='Yes,No' AND reminder='ReminderAlert7days' THEN 7
			WHEN  Ans='Yes,No' AND reminder='ReminderAlert30days' THEN 30
			WHEN  Ans='Yes,No' AND reminder='ReminderAlert60days' THEN 60
			WHEN  Ans='Yes,No' AND reminder='ReminderAlert90days' THEN 90
			WHEN  Ans='Yes,No' AND reminder='ReminderAlert180days' THEN 180	 END [days],	
			CASE WHEN Ans ='user' AND (reminder='ReminderAlert1dayUserID1' OR reminder='ReminderAlert7daysUserID1' OR
			reminder='ReminderAlert30daysUserID1' OR reminder='ReminderAlert60daysUserID1' OR reminder='ReminderAlert90daysUserID1'OR
			reminder='ReminderAlert180daysUserID1') THEN dbo.GetUserId(#Tmp1.Val) END [User1],
			CASE WHEN Ans ='user' AND (reminder='ReminderAlert1dayUserID2' OR reminder='ReminderAlert7daysUserID2' OR
			reminder='ReminderAlert30daysUserID2' OR reminder='ReminderAlert60daysUserID2' OR reminder='ReminderAlert90daysUserID2'OR
			reminder='ReminderAlert180daysUserID2') THEN dbo.GetUserId(#Tmp1.Val) END [User2]
			,reminder
		FROM #Tmp1 INNER JOIN ContractRequest cR ON cR.BulkImportStagingId=#Tmp1.BulkImportStagingId AND #Tmp1.ID=@ID

		SET @ID=@ID+1;
	END

IF dbo.isTableExists('#TmpFinalReminder') > 0 DROP TABLE #TmpFinalReminder
	CREATE TABLE #TmpFinalReminder(ID INT IDENTITY(1,1),RequestId INT,ContractId INT,FieldId INT,[Days] INT,User1 INT,User2 INT,PreID INT)

	INSERT INTO #TmpFinalReminder(RequestId,ContractId,FieldId,[Days],PreID)
	SELECT RequestId,ContractId,FieldId,[Days],ID FROM #TmpReminder WHERE Reminder IN('ReminderAlert1days',
	'ReminderAlert7days','ReminderAlert30days','ReminderAlert60days','ReminderAlert90days','ReminderAlert180days')

UPDATE #TmpFinalReminder SET #TmpFinalReminder.User1= #TmpReminder.User1 FROM #TmpReminder
 INNER JOIN  #TmpFinalReminder ON  #TmpFinalReminder.PreID=#TmpReminder.ID-1 AND Reminder 
	IN('ReminderAlert1dayUserID1',
	'ReminderAlert7daysUserID1','ReminderAlert30daysUserID1','ReminderAlert60daysUserID1','ReminderAlert90daysUserID1','ReminderAlert180daysUserID1')

UPDATE #TmpFinalReminder SET #TmpFinalReminder.User2= #TmpReminder.User2 
FROM #TmpReminder
 INNER JOIN  #TmpFinalReminder ON  #TmpFinalReminder.PreID= CASE WHEN #TmpFinalReminder.user1 is not null THEN #TmpReminder.ID-2 ELSE #TmpReminder.ID-1 END  AND Reminder 
	IN('ReminderAlert1dayUserID2',
	'ReminderAlert7daysUserID2','ReminderAlert30daysUserID2','ReminderAlert60daysUserID2','ReminderAlert90daysUserID2','ReminderAlert180daysUserID2')

	INSERT INTO ImportantDatesMetaDataReminders(RequestId,ContractId,MetaDataFieldId,ReminderDays,ReminderUser1,ReminderUser2)
		select RequestId,ContractId,FieldId,Days,User1,User2 from #TmpFinalReminder
-------------------------END MetaData Reminder-------------------------------------------
		/**Added By Nilesh to add Client Details*****/
		
		
		CREATE TABLE #tmpClientInformation (ID INT IDENTITY (1,1), RequestId INT, ClientName VARCHAR(MAX), ContactNumber VARCHAR(MAX), EmailId VARCHAR (MAX), isCustomer VARCHAR(MAX),
	    AddedBy INT, Addedon DATETIME,ModifiedBy INT,ModifiedOn DATETIME,IP VARCHAR(MAX),Address VARCHAR(MAX),CountryName VARCHAR(MAX),ContractingPartyName VARCHAR(MAX),CityName VARCHAR(MAX),StateName VARCHAR(MAX),Pincode VARCHAR(MAX))



		INSERT INTO #tmpClientInformation  
        SELECT cR.RequestId ,cR.ClientName,cR.ContactNumber,cR.EmailId,cR.isCustomer,cR.AddedBy,cR.Addedon,cR.Modifiedby,cR.Modifiedon,cR.IP,cR.Address,cR.CountryName,cR.ContractingPartyName,cR.CityName,cR.StateName,cR.PinCode
		FROM BulkImportStaging uB INNER JOIN ContractRequest cR ON cR.BulkImportStagingId = uB.BulkImportStagingId
		WHERE uB.Head='N' AND uB.BulkImportID =CAST(@BulkImportID AS VARCHAR(10))

	
	DECLARE @Cnt INT =1
	DECLARE @MaxCnt INT = (SELECT COUNT(1) FROM #tmpClientInformation)
	DECLARE @ClientId INT
	DECLARE @CountryId INT
	DECLARE @RequestId INT, @ClientName VARCHAR(MAX), @ContactNumber VARCHAR(MAX), @EmailId VARCHAR (MAX),@isCustomer VARCHAR(MAX), @AddedBy INT, @Addedon DATETIME,@ModifiedBy INT,@ModifiedOn DATETIME,@IP VARCHAR(MAX),@Address VARCHAR(MAX),@CountryName VARCHAR(MAX),@ContractingPartyName VARCHAR(MAX),
	@CityName VARCHAR(MAX),@StateName VARCHAR(MAX),@Pincode VARCHAR(MAX)

	DECLARE @ActivityTypeID INT
		SET @ActivityTypeID=(SELECT ActivityTypeId FROM MstActivityType WHERE  isSystemType='K')

	WHILE @Cnt <=@MaxCnt
	BEGIN
			BEGIN

			SELECT  @RequestId=t.[RequestId] , @ClientName=t.[ClientName],@isCustomer=CASE WHEN t.[isCustomer]='Supplier' THEN 'N' WHEN t.[isCustomer]='Customer' THEN 'Y' ELSE 'O' END , @ContactNumber=t.[ContactNumber] , 
			@EmailId=t.[EmailId], @AddedBy=t.[AddedBy], @Addedon=t.[Addedon],@ModifiedBy=t.[ModifiedBy],@ModifiedOn=t.[ModifiedOn],@IP=t.[IP],@Address=t.[Address],@CountryName=t.[CountryName],@ContractingPartyName=t.[ContractingPartyName],
			@CityName=t.[CityName],@StateName=t.[StateName],@Pincode=t.[PinCode]
			FROM #tmpClientInformation t WHERE ID=@Cnt

			SET @CountryId=(SELECT CountryId FROm MstCountry WHERE CountryName=@CountryName)

			IF NOT EXISTS(SELECT 1 FROm Client WHERE ClientName=@ClientName)
			BEGIN
				INSERT INTO Client (isCustomer,ClientName,Addedon,Addedby,Modifiedon,Modifiedby,IP)
				SELECT @isCustomer,@ClientName,@Addedon,@AddedBy,@ModifiedOn,@ModifiedBy,@IP
				SET @ClientId=SCOPE_IDENTITY()
			END
			ELSE
			BEGIN
			  SET @ClientId=(SELECT TOP 1 ClientId FROM Client WHERE ClientName=@ClientName)
			END

			IF NOT EXISTS(SELECT 1 FROM ClientAddressDetails WHERE Address=@Address AND CountryId=@CountryId AND PinCode=@PinCode AND CityName=@CityName AND StateName=@StateName AND ClientId=@ClientId)
			BEGIN
			INSERT INTO ClientAddressDetails(ClientId,IsPrimary,Address,CountryId,StateId,CityId,PinCode,Street2,CityName,StateName)
			SELECT @ClientId,'Y',@Address,@CountryId,NULL,NULL,@Pincode,NULL,@CityName,@StateName
			END

			IF NOT EXISTS(SELECT 1 FROM ClientContactDetails WHERE ContactNumber=@ContactNumber AND ClientId=@ClientId)
			BEGIN	
			INSERT INTO ClientContactDetails(ClientId,IsPrimary,ContactNumber)
			SELECT @ClientId,'Y',@ContactNumber
			END
	
			IF NOT EXISTS(SELECT 1 FROM ClientEmailDetails WHERE EmailID=@EmailId AND ClientId=@ClientId)
			BEGIN		
			INSERT INTO ClientEmailDetails(ClientId,IsPrimary,EmailID)
			SELECT @ClientId,'Y',@EmailId
			END

		DECLARE @ContractingPartyId INT
		SET @ContractingPartyId=(SELECT TOP 1 ContractingPartyId  FROM mstContractingParty WHERE RTRIM(LTRIM(ContractingPartyName))=@ContractingPartyName)

		UPDATE ContractRequest  SET ClientName=@ClientName, ClientId=@ClientId,isCustomer=@isCustomer,ContractingTypeId=@ContractingPartyId,CountryId=@CountryId WHERE RequestId=@RequestId
		END
		SET @Cnt = @Cnt + 1

		INSERT INTO Activity 
							(RequestId, ActivityTypeId, ActivityStatusId, ActivityText, ActivityDate,  ReminderDate, isForCustomer, Addedby, Addedon, IP, AssignToId, isDismissed,IsFromCalendar,IsFromOther) 
			 SELECT RequestId,  ActivityTypeId, ActivityStatusId, ActivityText, ActivityDate,ReminderDate, IsForCustomer, AddedBy, AddedOn, IP, AssignedTo, isDismissed, IsFromCalendar, IsFromOther
			 FROM(
					SELECT @RequestId RequestId, @ActivityTypeID ActivityTypeId, 1 ActivityStatusId, 'Contract expiration on  '+dbo.DateFormat(ExpirationDate) ActivityTExt, ExpirationDate ActivityDate,
						DATEADD(DD, -180, dbo.DateOnly(ExpirationDate)) ReminderDate, 'Y' IsForCustomer, @AddedBy AddedBy,GETDATE() AddedOn,@IpAddress IP, ExpirationAlert180daysUserID1 AssignedTo, 'N' isDismissed,'Y' IsFromCalendar,'KeyField'  IsFromOther
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert180days,'N')='Y' AND ISNULL(ExpirationAlert180daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -180, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert180daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert180days,'N')='Y'  AND ISNULL(ExpirationAlert180daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -90, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert90daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert90days,'N')='Y'  AND ISNULL(ExpirationAlert90daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -90, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert90daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert90days,'N')='Y'  AND ISNULL(ExpirationAlert90daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -60, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert60daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert60days,'N')='Y'  AND ISNULL(ExpirationAlert60daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -60, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert60daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert60days,'N')='Y'  AND ISNULL(ExpirationAlert60daysUserID2,0)<>0
					UNION ALL  
					SELECT @RequestId RequestId, @ActivityTypeID ActivityTypeId, 1 ActivityStatusId, 'Contract expiration on  '+dbo.DateFormat(ExpirationDate) ActivityTExt, ExpirationDate ActivityDate,	DATEADD(DD, -30, dbo.DateOnly(ExpirationDate)) ReminderDate, 'Y' IsForCustomer, @AddedBy AddedBy,GETDATE() AddedOn,@IpAddress IP, ExpirationAlert30daysUserID1 AssignedTo, 'N' isDismissed,'Y' IsFromCalendar,'KeyField'  IsFromOther
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert30days,'N')='Y' AND ISNULL(ExpirationAlert30daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -30, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert30daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert30days,'N')='Y'  AND ISNULL(ExpirationAlert30daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -7, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert7daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert7days,'N')='Y'  AND ISNULL(ExpirationAlert7daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on   '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -7, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert7daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert7days,'N')='Y'  AND ISNULL(ExpirationAlert7daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Contract expiration on  '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -1, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert24HrsUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert24Hrs,'N')='Y'  AND ISNULL(ExpirationAlert24HrsUserID1,0)<>0
					UNION ALL 
					SELECT  @RequestId, @ActivityTypeID, 1, 'Contract expiration on  '+dbo.DateFormat(ExpirationDate), ExpirationDate,	DATEADD(DD, -1, dbo.DateOnly(ExpirationDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, ExpirationAlert24HrsUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(ExpirationAlert24Hrs,'N')='Y'  AND ISNULL(ExpirationAlert24HrsUserID2,0)<>0

					UNION ALL
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -180, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert180daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert180days,'N')='Y'  AND ISNULL(RenewalAlert180daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -180, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert180daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert180days,'N')='Y'  AND ISNULL(RenewalAlert180daysUserID2,0)<>0
					UNION ALL
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -90, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert90daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert90days,'N')='Y'  AND ISNULL(RenewalAlert90daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -90, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert90daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert90days,'N')='Y'  AND ISNULL(RenewalAlert90daysUserID2,0)<>0
					UNION ALL
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -60, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert60daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert60days,'N')='Y'  AND ISNULL(RenewalAlert60daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -60, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert60daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert60days,'N')='Y'  AND ISNULL(RenewalAlert60daysUserID2,0)<>0
					

					UNION ALL
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -30, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert30daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert30days,'N')='Y'  AND ISNULL(RenewalAlert30daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -30, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert30daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert30days,'N')='Y'  AND ISNULL(RenewalAlert30daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -7, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert7daysUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert7days,'N')='Y'  AND ISNULL(RenewalAlert7daysUserID1,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -7, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert7daysUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert7days,'N')='Y'  AND ISNULL(RenewalAlert7daysUserID2,0)<>0
					UNION ALL 
					SELECT @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -1, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert24HrsUserID1, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert24Hrs,'N')='Y'  AND ISNULL(RenewalAlert24HrsUserID1,0)<>0
					UNION ALL 
					SELECT  @RequestId, @ActivityTypeID, 1, 'Renewal date of contract is '+dbo.DateFormat(RenewalDate), RenewalDate,	DATEADD(DD, -1, dbo.DateOnly(RenewalDate)), 'Y', @AddedBy,GETDATE(),@IpAddress, RenewalAlert24HrsUserID2, 'N','Y','KeyField'
					FROM  ContractRequest	WHERE RequestId=@RequestId AND COALESCE(RenewalAlert24Hrs,'N')='Y' AND ISNULL(RenewalAlert24HrsUserID2,0)<>0
					UNION ALL 
					SELECT DISTINCT ContractMetaData.requestid, @ActivityTypeID, 1 ActivityStatusId,fieldname +' reminder on '+ FieldValue As ActivityTExt,FieldValue AS ActivityDate,
					DATEADD(DD, -ReminderDays, dbo.DateOnly(FieldValue)) ReminderDate, 'Y' IsForCustomer,AddedBy,AddedOn,ContractMetaData.IPAddress AS IP,ReminderUser1 AS AssignedTo,
					'N' isDismissed,'Y' IsFromCalendar,'KeyField'  IsFromOther
					FROM ImportantDatesMetaDataReminders
					INNER JOIN ContractMetaData ON ContractMetaData.MetaDataFieldId=ImportantDatesMetaDataReminders.MetaDataFieldId and 
					ContractMetaData.requestid=ImportantDatesMetaDataReminders.requestid  and MetaDataType='Important Dates'
					INNER JOIN MetaDataConfigurator ON FieldId=ContractMetaData.MetaDataFieldId
					WHERE ContractMetaData.requestid=@RequestId  AND ISNULL(ReminderUser1,0)<>0
					UNION ALL 
					SELECT DISTINCT ContractMetaData.requestid, 1 ActivityTypeId, 1 ActivityStatusId,fieldname +' reminder on '+ FieldValue As ActivityTExt,FieldValue AS ActivityDate,DATEADD(DD, -ReminderDays, dbo.DateOnly(FieldValue)) ReminderDate, 'Y' IsForCustomer,
					AddedBy,AddedOn,ContractMetaData.IPAddress AS IP,ReminderUser2 AS AssignedTo,'N' isDismissed,'Y' IsFromCalendar,'KeyField'  IsFromOther
					FROM ImportantDatesMetaDataReminders
					INNER JOIN ContractMetaData ON ContractMetaData.MetaDataFieldId=ImportantDatesMetaDataReminders.MetaDataFieldId and 
					ContractMetaData.requestid=ImportantDatesMetaDataReminders.requestid  and MetaDataType='Important Dates'
					INNER JOIN MetaDataConfigurator ON FieldId=ContractMetaData.MetaDataFieldId
					WHERE ContractMetaData.requestid=@RequestId AND ISNULL(ReminderUser2,0)<>0
			)tmp

END -- end of while


		IF @trancount = 0
            COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			DECLARE @error INT, @message VARCHAR(4000), @xstate INT;
			SELECT @error = ERROR_NUMBER(), @message = ERROR_MESSAGE(), @xstate = XACT_STATE();
			IF @xstate = -1
				ROLLBACK;
			IF @xstate = 1 and @trancount = 0
				ROLLBACK
			IF @xstate = 1 and @trancount > 0
				ROLLBACK TRANSACTION usp_my_procedure_name;

			RAISERROR ('usp_my_procedure_name: %d: %s', 16, 1, @error, @message) ;

		END CATCH

		IF dbo.isTableExists('#tmpBulk') > 0 DROP TABLE #tmpBulk
	
	END



	/*
	SELECT unPVT.BulkImportID, unPVT.ContractTemplateId, unPVT.BulkImportStagingId, unPVT.asAnswer, 
						   unPVT.ColumnName,uH.ColumnNameValue,unPVT.Addedby, unPVT.Addedon
	FROM BulkImportStaging 
	UNPIVOT 
	(
	 asAnswer FOR ColumnName IN (Col1,Col2,Col3) -- dynamic column str
	) unPVT
	INNER JOIN BulkImportStagingHeader uH ON uH.ColumnName = unPVT.ColumnName AND uH.BulkImportID = unPVT.BulkImportID
	WHERE unPVT.BulkImportID = @BulkImportID AND unPVT.Head='N' AND unPVT.IsPass= 'Y'
	*/

	/*
		SET @Columns = NULL
		SELECT @Columns = COALESCE(@Columns + ', ','') +'['+ TableColumnName + ']='+  ColumnName 
		FROM
		(
		SELECT 
		uK.TableColumnName, 
		CASE WHEN  uK.Ans ='user' THEN 'dbo.GetUserId('+ uH.ColumnName +')' ELSE 'uB.'+ uH.ColumnName END ColumnName
		FROM BulkImportStagingHeader uH
		INNER JOIN vwBulkImportKeyFields uK ON uK.FieldName = uH.ColumnNameValue  
		WHERE uH.BulkImportID = @BulkImportID
		AND uH.ColumnNameValue IN (SELECT FieldName FROM vwBulkImportKeyFields)
		AND LEN(uK.TableColumnName)>0
		)tmp
		*/

END







GO




/**************************************************************************************************************
SP NAME     : ContractRequestBuildCacheString
Description : This SP is to fetch the string of values stored and searched from contract request screen
***************************************************************************************************************/

ALTER PROCEDURE ContractRequestBuildCacheString(@RequestID INT)

AS

BEGIN

DECLARE @ContractId   INT,
        @Activities   VARCHAR(5000),
	@Addresses    VARCHAR(5000),
	@Question     VARCHAR(5000),
        @FullAddress  VARCHAR(5000),
        @City         VARCHAR(5000),
	@CityName     VARCHAR(5000),
	@State        VARCHAR(5000),
	@Pincode      Varchar(5000),
        @Pincode1     Varchar(5000),
	@StateName    VARCHAR(5000),
	@Country      VARCHAR(5000),
        @AssignedToUserName   VARCHAR(5000),
        @AssignedToUserName1  VARCHAR(5000),
	@CountryName    VARCHAR(5000),
        @Question1      VARCHAR(5000),
        @TextValue      VARCHAR(5000),
	@TextValue1     VARCHAR(5000),
	@NumericValue   VARCHAR(5000),
	@NumericValue1  VARCHAR(5000),
	@DecimalValue   VARCHAR(5000),
	@DecimalValue1  VARCHAR(5000),
	@ContractStatus Varchar(5000)



SET @ContractId=(SELECT ContractId FROM ContractRequest
                  WHERE RequestId = @RequestID)

BEGIN
SET @ContractStatus = (SELECT [dbo].[fnContractRequestLatestStatus] (@ContractId) AS CStatus)
END

IF EXISTS(SELECT 'X' FROM Activity
           WHERE RequestId = @RequestID)

BEGIN
     DECLARE @ActText    VARCHAR(5000)
         SET @Activities = ''


      SELECT @Activities =  @Activities+ ISNULL(ActivityText,'') + ',' FROM Activity ---ISNULL(CAD.Address,'') + ',' 
       WHERE RequestId = @RequestID

      SELECT @Activities=REPLACE(@Activities,'"','')

      SELECT @ActText = CONCAT('"',LEFT(@Activities,LEN(@Activities) - 1), '"') 
END

ELSE

BEGIN
SELECT @ActText = '""'
END

IF EXISTS(SELECT 'X' FROM MstUsers MSU
           INNER JOIN ContractRequest CRT ON CRT.AssignToUserID = MSU.UsersId 
           WHERE CRT.RequestId = @RequestID)
BEGIN
    SET @AssignedToUserName1 = ''

 SELECT @AssignedToUserName1 =  @AssignedToUserName1+ ISNULL(CONCAT(MSU.FirstName,' ',MSU.LastName),'') + ',' 
   FROM MstUsers MSU
  INNER JOIN ContractRequest CRT ON CRT.AssignToUserID = MSU.UsersId 
  WHERE CRT.RequestId = @RequestID

SELECT @AssignedToUserName1=REPLACE(@AssignedToUserName1,'"','')

  SELECT @AssignedToUserName = QUOTENAME(LEFT(@AssignedToUserName1,LEN(@AssignedToUserName1) - 1), '"') 
END

ELSE

BEGIN
SELECT @AssignedToUserName = '""'
END

IF EXISTS(SELECT 'X'
            FROM ClientAddressDetails CAD
           INNER JOIN ContractRequest CRT ON CRT.ClientId = CAD.ClientId
           INNER JOIN MstCountry MCT      ON CRT.CountryId = MCT.CountryId
           WHERE CRT.RequestId = @RequestId)

BEGIN
	SET @City = ''
    SET @Addresses = ''
	SET @State = ''
	SET @Country = ''
	SET @Pincode1 = ''
 SELECT @Addresses = @Addresses + ISNULL(CAD.Address,'') + ',' ,
        @City      = @City + ISNULL(CAD.CityName,'') + ',' ,
	@State     = @State + ISNULL(CAD.StateName,'') + ',' ,
	@Pincode1  = @Pincode1 + ISNULL(CAD.Pincode,'')+ ','--,
	--@Country   = @Country + ISNULL(MCT.CountryName,'') + ','
   FROM ClientAddressDetails CAD
  INNER JOIN ContractRequest CRT
     ON CRT.ClientId = CAD.ClientId
  INNER JOIN MstCountry MCT
     ON CRT.CountryId = MCT.CountryId
  WHERE CRT.RequestId = @RequestId

SELECT * INTO #Temp FROM(
                         SELECT DISTINCT(ISNULL(MCT.CountryName,'')) AS CountryName 
						   FROM MstCountry MCT
                          INNER JOIN ClientAddressDetails CAD ON CAD.CountryId = MCT.CountryId
                          INNER JOIN ContractRequest CRT      ON CAD.ClientId = CRT.ClientId
                          WHERE CRT.RequestId = @RequestId) As CountryName

SELECT @Country = @Country + ISNULL(CountryName,'') + ','
  FROM #Temp

SELECT @Addresses=REPLACE(@Addresses,'"',''),
       @City = REPLACE(@City,'"',''),
	   @State = REPLACE(@State,'"',''),
	   @Pincode1 = REPLACE(@Pincode1,'"',''),
	   @Country = REPLACE(@Country,'"','')

 SELECT @FullAddress = CONCAT('"',LEFT(@Addresses,LEN(@Addresses) -1),'"'),
        @CityName    = CONCAT('"',LEFT(@City,LEN(@City) -1),'"'),
	    @StateName   = CONCAT('"',LEFT(@State,LEN(@State) -1),'"'),
	    @Pincode     = CONCAT('"',LEFT(@Pincode1,LEN(@Pincode1) -1),'"'),
        @CountryName = CONCAT('"',LEFT(@Country,LEN(@Country) -1),'"')
DROP TABLE #Temp
--SELECT @FullAddress,@CityName,@StateName,@Pincode,@CountryName

END

ELSE

BEGIN
SELECT @FullAddress='""',@CityName='""',@StateName='""',@CountryName='""',@Pincode='""'
END

IF EXISTS(SELECT 'X' FROM ContractQuestionsAnswers A
           INNER JOIN  ContractRequest B
	      ON A.RequestId = B.RequestId
	   WHERE B.RequestId = @RequestId)
BEGIN

        SET @Question1 = ''
        SET @TextValue1 = ''
	SET @NumericValue1 = ''
	SET @DecimalValue1 = ''
 SELECT @Question1       = @Question1     + ISNULL(CQA.Question,'') + ',' ,
        @TextValue1      = @TextValue1    + ISNULL(CQA.TextValue,'') + ',' ,
	@NumericValue1   = @NumericValue1 + CONVERT(VARCHAR(50),ISNULL(CAST(NumericValue AS money),'')) + ',' ,
	@DecimalValue1   = @DecimalValue1 + CONVERT(VARCHAR(50),ISNULL(CAST(DecimalValue AS money),'')) + ',' 
   FROM ContractQuestionsAnswers CQA
  INNER JOIN ContractRequest CRT
     ON CRT.RequestId = CQA.RequestId
  WHERE CRT.RequestId = @RequestId

SELECT @Question1=REPLACE(@Question1,'"',''),
       @TextValue1 = REPLACE(@TextValue1,'"',''),
	   @NumericValue1 = REPLACE(@NumericValue1,'"',''),
	   @DecimalValue1 = REPLACE(@DecimalValue1,'"','')

 SELECT @Question     = CONCAT('"',LEFT(@Question1,LEN(@Question1) -1),'"'),
        @TextValue    = CONCAT('"',LEFT(@TextValue1,LEN(@TextValue1) -1),'"'),
	@NumericValue     = CONCAT('"',LEFT(@NumericValue1,LEN(@NumericValue1) -1),'"'),
        @DecimalValue = CONCAT('"',LEFT(@DecimalValue1,LEN(@DecimalValue1) -1),'"')
 --SELECT @Question,@TextValue,@NumericValue,@DecimalValue
END

ELSE 

BEGIN
SELECT @Question='""',@TextValue='""',@NumericValue='""',@DecimalValue='""'
END

IF EXISTS(SELECT 'X' FROM mstContractingParty CPN
           INNER JOIN ContractRequest CRT ON CRT.ContractingTypeId = CPN.ContractingPartyId
           WHERE RequestId = @RequestID)
BEGIN
     DECLARE @ContractingPartyName    VARCHAR(5000),
             @ContractingPartyName1    VARCHAR(5000)
         SET @ContractingPartyName1 = ''

      SELECT @ContractingPartyName1 =   @ContractingPartyName1 + ISNULL(CPN.ContractingPartyName,'') + ',' 
        FROM mstContractingParty CPN
       INNER JOIN ContractRequest CRT ON CRT.ContractingTypeId = CPN.ContractingPartyId
       WHERE RequestId = @RequestId 

      SELECT @ContractingPartyName1 = REPLACE(@ContractingPartyName1,'"','')

      SELECT @ContractingPartyName = CONCAT('"',LEFT(@ContractingPartyName1,LEN(@ContractingPartyName1) - 1),'"')
  --SELECT @ContractingPartyName
END

ELSE 

BEGIN
SELECT @ContractingPartyName = '""'
END

IF EXISTS(SELECT 'X' FROM ContractRequest WHERE RequestId = @RequestID)
BEGIN
     DECLARE @Indemnity VARCHAR(5000),
             @Strictliability VARCHAR(5000),
             @Insurance VARCHAR(5000),
             @TerminationDescription VARCHAR(5000),
             @AssignmentNovation VARCHAR(5000),
             @Others VARCHAR(5000)
SELECT @Indemnity=CONCAT('"',ISNULL(REPLACE(Indemnity,'\',' '),''),'"'),
       @Strictliability=CONCAT('"',ISNULL(REPLACE(Strictliability,'\',' '),''),'"'),
       @Insurance=CONCAT('"',ISNULL(REPLACE(Insurance,'\',' '),''),'"'),
       @TerminationDescription=CONCAT('"',ISNULL(REPLACE(TerminationDescription,'\',' '),''),'"'),
       @AssignmentNovation=CONCAT('"',ISNULL(REPLACE(AssignmentNovation,'\',' '),''),'"'),
       @Others=CONCAT('"',ISNULL(REPLACE(Others,'\',' '),''),'"')
  FROM ContractRequest WHERE RequestId=@RequestID
END


IF NOT EXISTS(SELECT 'X' FROM CP_ContractCache 
               WHERE RequestId = @RequestID)
   BEGIN
   INSERT INTO CP_ContractCache--(RequestId,Cache_String)
   SELECT RequestId,CONCAT('{','"RequestId":',QUOTENAME(CR.RequestId,'"')
          ,',','"ContractId":',ISNULL(QUOTENAME(CR.ContractId,'"'),'""')
          ,',','"ClientID":',ISNULL(QUOTENAME(CR.ClientID,'"'),'""')
          ,',','"ClientName":',ISNULL(QUOTENAME(CR.ClientName,'"'),'""')
          ,',','"ContractTypeName":',ISNULL(QUOTENAME(CTY.ContractTypeName,'"'),'""')
          ,',','"RequestDescription":',ISNULL(QUOTENAME(CR.RequestDescription,'"'),'""')
          ,',','"RequestTypeName":',ISNULL(QUOTENAME(VRT.RequestTypeName,'"'),'""')
		  ,',','"ContractStatus":',ISNULL(QUOTENAME(@ContractStatus,'"'),'""')
		  ,',','"DepartmentName":',ISNULL(QUOTENAME(MDT.DepartmentName,'"'),'""')
		  ,',','"CityName":',@CityName
		  ,',','"StateName":',@StateName
		  ,',','"Pincode":',@Pincode
		  ,',','"CountryName":',@CountryName
		  ,',','"Addresses":',@FullAddress
		  ,',','"CurrencyName":',ISNULL(QUOTENAME(MC.CurrencyName,'"'),'""')
		  ,',','"EmailID":',ISNULL(QUOTENAME(CR.EmailID,'"'),'""')
		  ,',','"ContractingPartyName":',@ContractingPartyName--ISNULL(QUOTENAME(MCP.ContractingPartyName,'"'),'""')
		  ,',','"ContractValue":',ISNULL(QUOTENAME(CR.ContractValue,'"'),'""')
		  ,',','"ContactNumber":',ISNULL(QUOTENAME(CR.ContactNumber,'"'),'""')
		  ,',','"ContractTerm":',ISNULL(QUOTENAME(CR.ContractTerm,'"'),'""')
		  ,',','"LiabilityCap":',CONCAT('"',ISNULL(CR.LiabilityCap,''),'"')
		  ,',','"Indemnity":',@Indemnity
		  ,',','"Strictliability":',@Strictliability
		  ,',','"Insurance":',@Insurance
		  ,',','"TerminationDescription":',@TerminationDescription
		  ,',','"AssignmentNovation":',@AssignmentNovation
		  ,',','"Others":',@Others
		  ,',','"ActivityText":',@ActText
		  ,',','"Question":',@Question
		  ,',','"TextValue":',@TextValue
		  ,',','"NumericValue":',@NumericValue
		  ,',','"DecimalValue":',@DecimalValue
		  ,',','"AssignedToUser":',@AssignedToUserName,'}')
   FROM ContractRequest CR
  INNER JOIN MstContractType CTY           ON CTY.ContractTypeId      = CR.ContractTypeId
  INNER JOIN vwRequestType   VRT           ON CR.RequestTypeId        = VRT.RequestTypeId
 -- LEFT JOIN ClientAddressDetails CAD      ON CR.ClientId             = CAD.ClientId
  --INNER JOIN MstCountry MCO ON CAD.CountryId           = MCO.CountryId
  INNER JOIN MstDepartment MDT             ON CR.AssignToDepartmentId = MDT.DepartmentId
  --INNER JOIN vwContractStatus MCS          ON CR.ContractStatusID     = MCS.ContractStatusID
   LEFT JOIN MstCurrency MC                ON CR.CurrencyID           = MC.CurrencyId 
   --LEFT JOIN mstContractingParty MCP       ON CR.ContractingTypeId    = MCP.ContractingPartyId
   --LEFT JOIN Activity ACT                  ON CR.RequestId            = ACT.RequestId
   --LEFT JOIN ContractQuestionsAnswers CQA  ON CR.RequestId            = CQA.RequestId
  WHERE CR.RequestId = @RequestId
  END

  ELSE

  BEGIN
       UPDATE CPC
	      SET CPC.Cache_String = (
                                  SELECT CONCAT('{','"RequestId":',QUOTENAME(CR.RequestId,'"')
          ,',','"ContractId":',ISNULL(QUOTENAME(CR.ContractId,'"'),'""')
          ,',','"ClientID":',ISNULL(QUOTENAME(CR.ClientID,'"'),'""')
          ,',','"ClientName":',ISNULL(QUOTENAME(CR.ClientName,'"'),'""')
          ,',','"ContractTypeName":',ISNULL(QUOTENAME(CTY.ContractTypeName,'"'),'""')
          ,',','"RequestDescription":',ISNULL(QUOTENAME(CR.RequestDescription,'"'),'""')
          ,',','"RequestTypeName":',ISNULL(QUOTENAME(VRT.RequestTypeName,'"'),'""')
		  ,',','"ContractStatus":',ISNULL(QUOTENAME(@ContractStatus,'"'),'""')
		  ,',','"DepartmentName":',ISNULL(QUOTENAME(MDT.DepartmentName,'"'),'""')
		  ,',','"CityName":',@CityName
		  ,',','"StateName":',@StateName
		  ,',','"Pincode":',@Pincode
		  ,',','"CountryName":',@CountryName
		  ,',','"Addresses":',@FullAddress
		  ,',','"CurrencyName":',ISNULL(QUOTENAME(MC.CurrencyName,'"'),'""')
		  ,',','"EmailID":',ISNULL(QUOTENAME(CR.EmailID,'"'),'""')
		  ,',','"ContractingPartyName":',@ContractingPartyName--ISNULL(QUOTENAME(MCP.ContractingPartyName,'"'),'""')
		  ,',','"ContractValue":',ISNULL(QUOTENAME(CR.ContractValue,'"'),'""')
		  ,',','"ContactNumber":',ISNULL(QUOTENAME(CR.ContactNumber,'"'),'""')
		  ,',','"ContractTerm":',ISNULL(QUOTENAME(CR.ContractTerm,'"'),'""')
		  ,',','"LiabilityCap":',CONCAT('"',ISNULL(CR.LiabilityCap,''),'"')
		  ,',','"Indemnity":',@Indemnity
		  ,',','"Strictliability":',@Strictliability
		  ,',','"Insurance":',@Insurance
		  ,',','"TerminationDescription":',@TerminationDescription
		  ,',','"AssignmentNovation":',@AssignmentNovation
		  ,',','"Others":',@Others
		  ,',','"ActivityText":',@ActText
		  ,',','"Question":',@Question
		  ,',','"TextValue":',@TextValue
		  ,',','"NumericValue":',@NumericValue
		  ,',','"DecimalValue":',@DecimalValue
		  ,',','"AssignedToUser":',@AssignedToUserName,'}'))
   FROM CP_ContractCache CPC
  INNER JOIN ContractRequest CR            ON CPC.RequestID           = CR.RequestId
  INNER JOIN MstContractType CTY           ON CTY.ContractTypeId      = CR.ContractTypeId
  INNER JOIN vwRequestType   VRT           ON CR.RequestTypeId        = VRT.RequestTypeId
  -- LEFT JOIN ClientAddressDetails CAD      ON CR.ClientId             = CAD.ClientId
  --INNER JOIN MstCountry MCO ON CAD.CountryId           = MCO.CountryId
  INNER JOIN MstDepartment MDT             ON CR.AssignToDepartmentId = MDT.DepartmentId
  --INNER JOIN vwContractStatus MCS          ON CR.ContractStatusID     = MCS.ContractStatusID
   LEFT JOIN MstCurrency MC                ON CR.CurrencyID           = MC.CurrencyId 
   LEFT JOIN mstContractingParty MCP       ON CR.ContractingTypeId    = MCP.ContractingPartyId
   --LEFT JOIN Activity ACT                  ON CR.RequestId            = ACT.RequestId
   --LEFT JOIN ContractQuestionsAnswers CQA  ON CR.RequestId            = CQA.RequestId
  WHERE CR.RequestId = @RequestId

END

END

GO


ALTER PROCEDURE [dbo].[ContractRequestDetails]
@RequestID INT,
@UserID INT=0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	SET NOCOUNT ON;

		DECLARE @ContractTypeId INT, @RequestTypeID INT, @ContractID BIGINT,@StatusName VARCHAR(100)
		SELECT @ContractTypeId=ContractTypeId, @RequestTypeID=RequestTypeID, @ContractID=ContractID FROM ContractRequest WHERE RequestId=@RequestID		

		SELECT TOP 1 @StatusName=StatusName FROM ContractRequest 
		INNER JOIN vwContractStatus ON ContractRequest.ContractStatusId=vwContractStatus.ContractStatusId
		WHERE (ContractID=@ContractID AND @ContractID IS NOT NULL)  
						OR (RequestId=@RequestId AND @ContractID IS NULL)  
		ORDER BY RequestId DESC

		----Resultset 1--------
		SELECT MstContractType.ContractTypeId, ContractTypeName,ContractRequest.ContractId,
		ClientName,
		ContactNumber,
		EmailID,
		--(ContractRequest.Address+ (CASE  WHEN Street2 IS NOT NULL AND Street2<> '' THEN ', '+Street2 ELSE '' END)+',<br/>'+ ContractRequest.CityName +',<br/>'+ContractRequest.StateName+',<br/>'+MstCountry.CountryName) ClientAddress,
		dbo.GetRequestAddress(@RequestId)ClientAddress,
		CASE ISNULL(ContractRequest.IsApprovalRequired,'N') WHEN 'N' THEN 'No' ELSE 'Yes' END IsApprovalRequired,dbo.DateFormat(ContractRequest.DeadlineDate)DeadlineDate,MstUsers.FullName,ISNULL(FORMAT(ContractRequest.Addedon,'d-MMM-yyyy h:mm tt'),GETDATE())AddedDate,
		(SELECT FullName FROM MstUsers WHERE UsersId=ContractRequest.AssignToUserID) AssignedTo, ISNULL(ContractRequest.EstimatedValue,0) EstimatedValue,
		ContractRequest.RequestDescription,
		ContractRequest.RequestTypeId,
		vwRequestType.RequestTypeName
		--,vwContractStatus.StatusName
		,@StatusName StatusName,
		ContractRequest.ContractValue,
		ContractRequest.ContractTerm,
		ContractRequest.ContractStatusID,
		ContractRequest.IsBulkImport,
		MstContractType.ContractTypeId,
		--Format(ContractValue ,'N','en-US' ) + ' (' + (SELECT CurrencyCode FROM MstCurrency  WHERE MstCurrency.CurrencyID=ContractRequest.CurrencyId OR (IsBaseCurrency='Y' AND ContractRequest.CurrencyId = 0 AND ContractRequest.ContractValue > 0)) +')' AS ContractValuewithcurrency
		dbo.[GetContractValueWithCurrencyCode](ContractRequest.RequestId) AS ContractValuewithcurrency
		,(CASE WHEN ContractRequest.Priority='N' THEN 'No' WHEN  ContractRequest.Priority='Y' THEN 'Yes' WHEN  ContractRequest.Priority='N/A' THEN 'N/A' END) Priority
		,ContractRequest.PriorityReason
		FROM MstContractType
		INNER JOIN ContractRequest ON ContractRequest.ContractTypeId=MstContractType.ContractTypeId
		INNER JOIN vwRequestType ON vwRequestType.RequestTypeId=ContractRequest.RequestTypeId		
		LEFT OUTER JOIN MstCountry ON MstCountry.CountryId=ISNULL(ContractRequest.CountryId,0)
		INNER JOIN MstUsers ON MstUsers.UsersId=ContractRequest.Addedby
		INNER JOIN vwContractStatus ON vwContractStatus.ContractStatusId=ContractRequest.ContractStatusId
		WHERE ContractRequest.RequestId=@RequestID

		----Resultset 2--------Contract Documents--------------------------------------
		EXEC RequestDocumentsDetails @RequestID

		----Resultset 3--------Contract Templates----------------------------------------
		SELECT Id, Name FROM
			(SELECT CONVERT(VARCHAR(200), CAST(ContractTemplateId AS VARCHAR(2000))+'#'+TemplateFileName) AS Id, ContractTemplateName  AS Name  
			,ROW_NUMBER() OVER ( ORDER BY ContractTemplate.ContractTemplateName ) AS seq
			FROM ContractTemplate 
			WHERE ContractTypeId=@ContractTypeId  AND ContractTemplate.isActive='Y' --AND (ContractTemplate.IsBulkImport IS NULL OR ContractTemplate.IsBulkImport='N')
							AND @RequestTypeID IN (SELECT ITEMS FROM dbo.SPLIT(RequestTypeIDs,','))
			UNION
			SELECT '0','--Select--', 0 seq
			)tmp ORDER BY seq ASC

		----Resultset 4--------
		IF EXISTS(SELECT 1 FROM ContractQuestionsAnswers WHERE RequestId=@RequestID)
			SELECT 'Y' IsAnswered
		ELSE
			SELECT 'N' IsAnswered
		
		----Resultset 5--------
		SELECT TOP(1)ContractTemplate.ContractTemplateId,ContractTemplate.TemplateFileName  FROM ContractTemplate
		LEFT OUTER JOIN  ContractQuestionsAnswers  ON ContractTemplate.ContractTemplateId=ContractQuestionsAnswers.ContractTemplateId
		WHERE ContractQuestionsAnswers.RequestId=@RequestID

		----Resultset 6--------Contract Versions----------------
		SELECT dbo.GenerateContractFileName(@RequestId) NextContractFileName

		----Resultset 7--------IsApprovalDone----------------
		IF EXISTS(SELECT 1 FROM ContractApprovals WHERE RequestId=@RequestId AND  ISNULL(IsActive,'Y')='Y')
			SELECT 'N' IsApprovalDone
		ELSE SELECT 'Y' IsApprovalDone

		----Resultset 8--------IsSignatureStatusSent----------------
		IF EXISTS(SELECT 1 FROM ContractDocSignDetails WHERE RequestId=@RequestId AND [Status]='Sent')
			SELECT 'Y' IsSignatureStatusSent
		ELSE SELECT 'N' IsSignatureStatusSent


		----Resultset 9--------Contract History------ Added by Santosh Jadhav----------

		SELECT cR.RequestID,		
		CAST(vwRequestType.RequestTypeName AS VARCHAR(500)) RequestType,
		MstContractType.ContractTypeName AS ContractType,
		ISNULL(vwContractStatus.StatusName,'')  AS ContractStatus,

		'RequestFlow.aspx?Status=Workflow&RequestId='+CAST(RequestId AS VARCHAR) AS URL,
		CAST(cR.RequestId AS VARCHAR) +' - '+  
		vwRequestType.RequestTypeName +' - '+  
		MstContractType.ContractTypeName +' - '+  
		ISNULL(vwContractStatus.StatusName,'') AS [Contract History]
		FROM ContractRequest cR	
		INNER JOIN vwRequestType ON vwRequestType.RequestTypeID = cR.RequestTypeId
		INNER JOIN MstContractType ON MstContractType.ContractTypeId = cR.ContractTypeId
		LEFT JOIN vwContractStatus ON vwContractStatus.ContractStatusId = cR.ContractStatusId
		WHERE cR.RequestId<@RequestID 
		AND cR.ContractID = ( SELECT ContractID FROM ContractRequest
	                          WHERE RequestId=@RequestID)
		ORDER BY RequestId DESC
 
		----Resultset 10--------IsFirstCheckOutDone---------------------------
        SELECT CASE WHEN COUNT(1)=0 AND @RequestID NOT IN (SELECT RequestId FROM EmailSentLog WHERE Mode ='Contract Generated' AND isSent='Y') 
									THEN 'N' ELSE 'Y' END IsFirstCheckOutDone  
		FROM ContractFileActivity 
		INNER JOIN  ContractFiles ON ContractFileActivity.ContractFileId=ContractFiles.ContractFileId
		WHERE ContractFiles.RequestId=@RequestID AND ContractFileActivity.FileActivityTypeId=2


		----Resultset 11--------Show Expiry Date Message For Terminated Contract---------------------------
		SELECT CASE WHEN EXISTS
		(SELECT 1 FROM ContractRequest
		WHERE ExpirationDate IS NOT NULL AND ExpirationDate > dbo.DateOnly(GETDATE())
		AND ContractStatusID =11
		AND RequestId = (SELECT TOP(1) RequestId FROM ContractRequest WHERE ContractID=(SELECT ContractID FROM ContractRequest WHERE RequestId=@RequestID)ORDER BY RequestId DESC )
		) THEN 'Y' ELSE 'N' END ShowExpiryDateMessage,'Kindly change the Expiry Date field information in order to reflect contract Termination Date.' MessageText

		----Resultset 12--------Show Expiry Date Message For Renewal Contract---------------------------
		SELECT CASE WHEN EXISTS
		(SELECT 1 FROM ContractRequest
		WHERE ExpirationDate IS NOT NULL AND ExpirationDate < dbo.DateOnly(GETDATE())
		AND ContractStatusID =10
		AND RequestId = (SELECT TOP(1) RequestId FROM ContractRequest WHERE ContractID=(SELECT ContractID FROM ContractRequest WHERE RequestId=@RequestID)ORDER BY RequestId DESC )
		) THEN 'Y' ELSE 'N' END ShowExpiryDateMessageForRenewal,'Kindly change the Expiration Date field information in order to reflect updated contract Expiration Date.'MessageText

		----Resultset 13--------Retrieving BrandId---------------------------
		SELECT CASE WHEN DocuSignBrandId IS NULL OR DocuSignBrandId='' THEN 'f31017cb-5d2c-4a0a-9b1b-3c81238d47ca' ELSE DocuSignBrandId END DocuSignBrandId,
		CASE WHEN DocuSignUsername IS NULL OR DocuSignUsername='' THEN  '80a35ae2-a56e-4aab-bfc7-ef13fd3f0be9' ELSE DocuSignUsername END DocuSignUsername ,
		CASE WHEN DocuSignPassword IS NULL OR DocuSignPassword='' THEN 'NewGalexy123' ELSE DocuSignPassword END DocuSignPassword
		FROM  Subscribers WHERE DBName=DB_NAME()

		----Resultset 14--------IsFinalDocument----------------
		SELECT COALESCE(MAX('Y'),'N') IsFinalDocument FROM ContractFiles WHERE RequestId=@RequestID AND isFromDocuSign='Y'


		-----ResulSet 15---------isContractTypeExits ------
		BEGIN
			DECLARE @Role VARCHAR(50)
			SET @Role=(SELECT MstRole.RoleName FROM MstUsers INNER JOIN MstRole ON MstUsers.RoleId=MstRole.RoleId AND MstUsers.UsersId=@UserID)

			IF dbo.isTableExists('#tempContratType') > 0 DROP TABLE #tempContratType
			CREATE table #tempContratType (ContractType Int)

			INSERT INTO #tempContratType
			SELECT  ContractTypeId 
			FROM MSTContractType  
			WHERE  MstContractType.ContractTypeId in(SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),MstContractType.ContractTypeId)
			ELSE dbo.GetContractTypeAccess(@Role) END),','))
			AND
			EXISTS ((SELECT 1 FROM vwRequestAccess uA WHERE uA.RequestId = @RequestID AND uA.UsersId = @UserID) UNION (SELECT 1 FROM Activity  WHERE AssignToId=@UserID AND RequestId = @RequestID)) 

			IF EXISTS (SELECT 1 FROM #tempContratType WHERE ContractType=@ContractTypeId)	
			SELECT 'Y' isContractTypeExits,'' CTMessage;
			ELSE
			SELECT 'N' isContractTypeExits,'You do not have access to records of this contract type. Please contact your administrator.' CTMessage;

			IF dbo.isTableExists('#tempContratType') > 0 DROP TABLE #tempContratType
	END

	-----ResulSet 16---------For Contract RelationShips ------

	BEGIN		
		EXEC AccessGet @PageName='contractrelations.aspx',@UserId=@UserID		
	END

END




GO



-- =============================================
-- Author:		<Bharati R Gujarathi>
-- Create date: <02Jun2014>
-- Description:	<To Get details respective to Request Id>
-- =============================================
/*
EXEC [ContractVaultDetails] 5      

*/

ALTER PROCEDURE [dbo].[ContractVaultDetails]
@RequestID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	SET NOCOUNT ON;

		DECLARE @ContractTypeId INT, @RequestTypeID INT
		SELECT @ContractTypeId=ContractTypeId, @RequestTypeID=RequestTypeID FROM ContractRequest WHERE RequestId=@RequestID		

		----Resultset 1--------
		SELECT MstContractType.ContractTypeId, ContractTypeName,ContractRequest.ContractId,
		ClientName,
		ContactNumber,
		EmailID,
		(ContractRequest.Address+',<br/>'+ ContractRequest.CityName +',<br/>'+ContractRequest.StateName+',<br/>'+MstCountry.CountryName) ClientAddress,
		CASE ISNULL(ContractRequest.IsApprovalRequired,'N') WHEN 'N' THEN 'No' ELSE 'Yes' END IsApprovalRequired,dbo.DateFormat(ContractRequest.DeadlineDate)DeadlineDate,MstUsers.FullName,ISNULL(FORMAT(ContractRequest.Addedon,'d-MMM-yyyy h:mm tt'),GETDATE())AddedDate
		,(SELECT FullName FROM MstUsers WHERE UsersId=ContractRequest.AssignToUserID) AssignedTo, ISNULL(ContractRequest.EstimatedValue,0) EstimatedValue
		,ContractRequest.RequestDescription
		,ContractRequest.RequestTypeId
		,vwRequestType.RequestTypeName
		,vwContractStatus.StatusName
		FROM MstContractType
		LEFT JOIN ContractRequest ON ContractRequest.ContractTypeId=MstContractType.ContractTypeId
		LEFT JOIN vwRequestType ON vwRequestType.RequestTypeId=ContractRequest.RequestTypeId		
		LEFT JOIN MstCountry ON MstCountry.CountryId=ContractRequest.CountryId
		LEFT JOIN MstUsers ON MstUsers.UsersId=ContractRequest.Addedby
		LEFT JOIN vwContractStatus on vwContractStatus.ContractStatusId=ContractRequest.ContractStatusId
		WHERE ContractRequest.RequestId=@RequestID

		----Resultset 2--------Contract Documents--------------------------------------
		EXEC RequestDocumentsDetails @RequestID

		----Resultset 3--------Contract Templates----------------------------------------
		SELECT Id, Name FROM
			(SELECT CONVERT(VARCHAR(200), CAST(ContractTemplateId AS VARCHAR(2000))+'#'+TemplateFileName) AS Id, ContractTemplateName  AS Name  
			,ROW_NUMBER() OVER ( ORDER BY ContractTemplate.ContractTemplateName ) AS seq
			FROM ContractTemplate 
			WHERE ContractTypeId=@ContractTypeId  AND ContractTemplate.isActive='Y' AND (ContractTemplate.IsBulkImport IS NULL OR ContractTemplate.IsBulkImport='N')
							AND @RequestTypeID IN (SELECT ITEMS FROM dbo.SPLIT(RequestTypeIDs,','))
			UNION
			SELECT '0','--Select--', 0 seq
			)tmp ORDER BY seq ASC

		----Resultset 4--------
		IF EXISTS(SELECT 1 FROM ContractQuestionsAnswers WHERE RequestId=@RequestID)
			SELECT 'Y' IsAnswered
		ELSE
			SELECT 'N' IsAnswered
		
		----Resultset 5--------
		SELECT TOP(1)ContractTemplate.ContractTemplateId,ContractTemplate.TemplateFileName  FROM ContractTemplate
		LEFT OUTER JOIN  ContractQuestionsAnswers  ON ContractTemplate.ContractTemplateId=ContractQuestionsAnswers.ContractTemplateId
		WHERE ContractQuestionsAnswers.RequestId=@RequestID

		----Resultset 6--------Contract Versions----------------
		SELECT dbo.GenerateContractFileName(@RequestId) NextContractFileName

		----Resultset 7--------IsApprovalDone----------------
		IF EXISTS(SELECT 1 FROM ContractApprovals WHERE RequestId=@RequestId AND  ISNULL(IsActive,'Y')='Y')
			SELECT 'N' IsApprovalDone
		ELSE SELECT 'Y' IsApprovalDone
END
GO


/*
EXEC CustomerActiveContracts_Report
*/

ALTER PROCEDURE [dbo].[Custom_Reports]
	@PageNo INT = 0 ,   
	@RecordsPerPage INT = 0,
	@CustomReportId INT = 0,
	@SortColumn VARCHAR(200)='Request ID',
	@Direction INT = 0,
	@UserID Int=null
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @sql AS nvarchar(MAX);
	DECLARE @Search AS nvarchar(MAX);
	DECLARE @AllFiels Varchar(max);
	DECLARE @ContractType varchar(1000);
	DECLARE @FixedField Varchar(max);DECLARE @RequestFormFields Varchar(max);DECLARE @ImportantDatesFields Varchar(max);DECLARE @KeyObligationFields Varchar(max);

	BEGIN TRY 
    BEGIN TRAN

	  -- Call sp for metadata configuration columns with passing request id.
		EXEC [rptMetaDataConfigurColumns] 0

        SET @FixedField=( SELECT 
		ISNULL(( STUFF( 
		( 
		SELECT  ', '+(a.value) FROM vwCustomColumns a
		WHERE a.Identitys='cR' AND  a.Name  IN (SELECT items FROM dbo.Split(CustomReports.ReportFixedFields,',') )
		FOR    XML path('')
		)
		, 1,1,''))	,'cR.ContractID [Contract ID]') FixedField

		FROM CustomReports WHERE CustomReportId=@CustomReportId )

		SET @RequestFormFields=( SELECT 
		ISNULL(( STUFF( 
		( 
		SELECT  ', '+(a.value) FROM vwCustomColumns a
		WHERE a.Identitys='rpt' AND a.Name  COLLATE Latin1_General_CS_AS IN (SELECT items FROM dbo.Split(CustomReports.RequestFormReportFields,',') )
		FOR    XML path('')
		)
		, 1,1,'')),'') RequestFormFields
		FROM CustomReports WHERE CustomReportId=@CustomReportId )

		SET @ImportantDatesFields=( SELECT 
		ISNULL(( STUFF( 
		( 
		SELECT  ', '+(a.value) FROM vwCustomColumns a
		WHERE a.Identitys='rptD' AND a.Name  COLLATE Latin1_General_CS_AS IN (SELECT items FROM dbo.Split(CustomReports.ImportantDatesReportFields,',') )
		FOR    XML path('')
		)
		, 1,1,'')),'') ImportantDatesFields	
		FROM CustomReports WHERE CustomReportId=@CustomReportId )
		
		SET @KeyObligationFields=( SELECT 
		ISNULL((STUFF( 
		( 
		SELECT  ', '+(a.value) FROM vwCustomColumns a
		WHERE a.Identitys='rptK' AND a.Name  COLLATE Latin1_General_CS_AS IN (SELECT items FROM dbo.Split(CustomReports.KeyObligationReportFields,',') )
		FOR    XML path('')
		)
		, 1,1,'')),'')	
		KeyObligationFields	
		FROM CustomReports WHERE CustomReportId=@CustomReportId )

		SET @AllFiels=@FixedField;

		IF (@RequestFormFields!='')
		BEGIN
		SET @AllFiels=@AllFiels+','+@RequestFormFields;
		END
		IF (@ImportantDatesFields!='')
		BEGIN
		SET @AllFiels=@AllFiels+','+@ImportantDatesFields;
		END
		IF (@KeyObligationFields!='')
		BEGIN
		SET @AllFiels=@AllFiels+','+@KeyObligationFields;
		END

	IF (RIGHT(RTRIM(@AllFiels),1) = ',')
	BEGIN
	SET @AllFiels=LEFT(@AllFiels, LEN(@AllFiels) - 1);
		IF (RIGHT(RTRIM(@AllFiels),1) = ',')
		BEGIN
			SET @AllFiels=LEFT(@AllFiels, LEN(@AllFiels) - 1);
		END
	END

	-------Get role Access, dependent on Contract Type

		DECLARE @Role VARCHAR(50)
		IF @UserID IS NULL
		SET @Role='Super Admin'
	ELSE
		SET @Role=(SELECT MstRole.RoleName FROM MstUsers INNER JOIN MstRole ON MstUsers.RoleId=MstRole.RoleId AND MstUsers.UsersId=@UserID)


	SET @ContractType=(SELECT ReportWhereClauseFields FROM CustomReports WHERE CustomReportId=@CustomReportId )

	IF @ContractType =''
	BEGIN
		--SELECT @ContractType = @ContractType +CONVERT(VARCHAR(200), ContractTypeId) + ','
		--FROM MstContractType
		SELECT @ContractType = @ContractType +CONVERT(VARCHAR(200), ContractTypeId) + ','
		FROM MstContractType WHERE ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),',')) 
		SET @ContractType=LEFT(@ContractType, LEN(@ContractType) - 1);
	END
	ELSE
	BEGIN
	DECLARE @ContractType1 VARCHAR(MAX);
	SET @ContractType1='';
	    SELECT @ContractType1 = @ContractType1 +CONVERT(VARCHAR(5000), ContractTypeId) + ','
		FROM MstContractType WHERE ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),',')) 
		SET @ContractType1=LEFT(@ContractType1, LEN(@ContractType1) - 1);
		--Select @ContractType1;

		BEGIN
			create table #SomeList
			(
			MyValues varchar(MAX)
			)
			insert #SomeList
			select @ContractType 
			--The above represent your table of denormalized data

			--Now we parse this into a temp table so we can work with it
			select * into #FirstList from #SomeList
			cross apply dbo.Split(MyValues, ',')

			--select * from #FirstList

			--Here is your parameter of delimited data
			declare @MyParms varchar(MAX) = @ContractType1

			--Again we parse this into a temp table to work with
			select * into #SecondList from (select @MyParms as Listing) x
			cross apply dbo.Split(Listing, ',')

			--select * from #SecondList

			Set @ContractType='';
			SELECT @ContractType = @ContractType +CONVERT(VARCHAR(1000), ContractTypeId) + ','
		    FROM MstContractType WHERE ContractTypeId IN (
			select sl.items from #FirstList fl
			join #SecondList sl on sl.items = fl.items
			);

			SET @ContractType=LEFT(@ContractType, LEN(@ContractType) - 1);
			--SELECT @ContractType;

			drop table #SomeList
			drop table #FirstList
			drop table #SecondList
		END

	END

	        DECLARE @WhereCondition Varchar(MAX);
			SET @WhereCondition=' ';

			DECLARE @ED1 DATE;DECLARE @EDT2 DATE; DECLARE @ExD1 DATE;DECLARE @ExDT2 DATE; DECLARE @RD1 DATE;DECLARE @RDT2 DATE; DECLARE @SD1 DATE;DECLARE @SDT2 DATE; 
			DECLARE @RqD1 DATE;DECLARE @RqDT2 DATE; DECLARE @ContractPartyId INT =0;DECLARE @AV1 VARCHAR(100);DECLARE @AVT2 VARCHAR(100); DECLARE @TerritaryId INT =0;
			DECLARE @ContractStatusId INT =0;DECLARE @DepartmentId INT =0;DECLARE @AssignUserId INT =0;DECLARE @IsThirdGuaranteeRequired VARCHAR(25);DECLARE @ClientId INT=0;
			DECLARE @ChangeOfControl VARCHAR(3)=NULL;DECLARE @EntireAgreementClause VARCHAR(3)=NULL;DECLARE @IsStrictliability VARCHAR(3)=NULL;DECLARE @IsPriority VARCHAR(3)=NULL;

			SELECT @ED1=EffectiveDate,@EDT2=EffectiveToDate ,
			@ExD1=ExpiryDate,
			@ExDT2=ExpiryToDate,
			@RD1=RenewalDate,
			@RDT2=RenewalToDate,
			@SD1=SignatureDate,
			@SDT2=SignatureToDate,
			@RqD1=RequestDate,
			@RqDT2=RequestToDate,
			@ContractPartyId= ContractingPartyId,
			@AV1= AnnualValue,
			@AVT2= AnnualValueTo,
			@TerritaryId=TerritoryId,
			@ContractStatusId=ContractStatusId,
			@DepartmentId=AssignedDepartmentId,
			@AssignUserId= AssignedUserId,
			@IsThirdGuaranteeRequired= ThirdPartyGuaranteeRequired,
			@ClientId=ClientId,
			@ChangeOfControl=ChangeOfControl,
			@EntireAgreementClause= EntireAgreementClause,
			@IsStrictliability=Strictliability,
			@IsPriority=PriorityContract
			FROM CustomReportFilters WHERE CustomReportId=@CustomReportId


------------------------------------------------- Where Condition Start ------------------------------------
	BEGIN
			
			--SET @ED1=(SELECT EffectiveDate FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			--SET @EDT2=(SELECT EffectiveToDate FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			
			IF(@ED1 IS NOT NULL OR @ED1 !='')
			 SET @WhereCondition=@WhereCondition+' AND (cR.EffectiveDate BETWEEN dbo.DateOnly('''+CONVERT(varchar(100), @ED1)+''') AND dbo.DateOnly('''+CONVERT(varchar(100), @EDT2)+'''))'
-------------------------
			
			--SET @ExD1=(SELECT ExpiryDate FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			--SET @ExDT2=(SELECT ExpiryToDate FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);

			IF(@ExD1 IS NOT NULL OR @ExD1 !='')
			 SET @WhereCondition=@WhereCondition+' AND (cR.ExpirationDate BETWEEN dbo.DateOnly('''+CONVERT(varchar(100), @ExD1)+''') AND dbo.DateOnly('''+CONVERT(varchar(100), @ExDT2)+'''))'
--------------------------
			
			--SET @RD1=(SELECT RenewalDate FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			--SET @RDT2=(SELECT RenewalToDate FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			IF(@RD1 IS NOT NULL OR @RD1 !='')
			 SET @WhereCondition=@WhereCondition+' AND (cR.RenewalDate BETWEEN dbo.DateOnly('''+CONVERT(varchar(100), @RD1)+''') AND dbo.DateOnly('''+CONVERT(varchar(100), @RDT2)+'''))'
----------------------------
			
			--SET @SD1=(SELECT SignatureDate FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			--SET @SDT2=(SELECT SignatureToDate FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);

			IF(@SD1 IS NOT NULL OR @SD1 !='')
			 SET @WhereCondition=@WhereCondition+' AND (cR.SignatureDate BETWEEN dbo.DateOnly('''+CONVERT(varchar(100), @SD1)+''') AND dbo.DateOnly('''+CONVERT(varchar(100), @SDT2)+'''))'
-------------------

			
			--SET @RqD1=(SELECT RequestDate FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			--SET @RqDT2=(SELECT RequestToDate FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);

			 IF(@RqD1 IS NOT NULL OR @RqD1 !='')
			  SET @WhereCondition=@WhereCondition+' AND (dbo.DateOnly(cR.Addedon) BETWEEN dbo.DateOnly('''+CONVERT(varchar(100), @RqD1)+''') AND dbo.DateOnly('''+CONVERT(varchar(100), @RqDT2)+'''))'

----------------------
			
			--SET @ContractPartyId=(SELECT ContractingPartyId FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			IF(@ContractPartyId !=0 AND @ContractPartyId IS NOT NULL)
			 SET @WhereCondition=@WhereCondition+' AND (cp.ContractingPartyId =(CASE WHEN '+CONVERT(varchar(100),@ContractPartyId)+' IS NULL OR '+CONVERT(varchar(100),@ContractPartyId)+' =0 THEN cp.ContractingPartyId ELSE '+CONVERT(varchar(100),@ContractPartyId)+' END))'

-------------------
			
			--SET @AV1=(SELECT AnnualValue FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			--SET @AVT2=(SELECT AnnualValueTo FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			IF(@AV1 IS NOT NULL OR @AV1 !='')
			 SET @WhereCondition=@WhereCondition+' AND (cr.ContractValue BETWEEN '+CONVERT(varchar(100), @AV1)+' AND '+CONVERT(varchar(100), @AVT2)+')'

------------------------
			
			--SET @TerritaryId=(SELECT TerritoryId FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			SET @WhereCondition=@WhereCondition+' AND (cr.CountryId =(CASE WHEN '+CONVERT(varchar(100),@TerritaryId)+' IS NULL OR '+CONVERT(varchar(100),@TerritaryId)+' =0 THEN cr.CountryId ELSE '+CONVERT(varchar(100),@TerritaryId)+' END))'

----------------------
			
			--SET @ContractStatusId=(SELECT ContractStatusId FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			SET @WhereCondition=@WhereCondition+' AND (cr.ContractStatusID =(CASE WHEN '+CONVERT(varchar(100),@ContractStatusId)+' IS NULL OR '+CONVERT(varchar(100),@ContractStatusId)+' =0 THEN cr.ContractStatusID ELSE '+CONVERT(varchar(100),@ContractStatusId)+' END))'

----------------------
			
			--SET @DepartmentId=(SELECT AssignedDepartmentId FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			SET @WhereCondition=@WhereCondition+' AND (cr.AssignToDepartmentId =(CASE WHEN '+CONVERT(varchar(100),@DepartmentId)+' IS NULL OR '+CONVERT(varchar(100),@DepartmentId)+' =0 THEN cr.AssignToDepartmentId ELSE '+CONVERT(varchar(100),@DepartmentId)+' END))'
----------------------
			
			--SET @AssignUserId=(SELECT AssignedUserId FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			SET @WhereCondition=@WhereCondition+' AND (cr.AssignToUserID =(CASE WHEN '+CONVERT(varchar(100),@AssignUserId)+' IS NULL OR '+CONVERT(varchar(100),@AssignUserId)+' =0 THEN cr.AssignToUserID ELSE '+CONVERT(varchar(100),@AssignUserId)+' END))'

----------------------
			
			--SET @IsThirdGuaranteeRequired=(SELECT ThirdPartyGuaranteeRequired FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			IF(@IsThirdGuaranteeRequired IS NOT NULL AND @IsThirdGuaranteeRequired !='')
			 SET @WhereCondition=@WhereCondition+' AND (cR.IsGuaranteeRequired=(CASE WHEN '''+CONVERT(varchar(100),@IsThirdGuaranteeRequired)+''' =''Y'' THEN ''Y'' ELSE ''N'' END))'

----------------------
			
			--SET @ClientId=(SELECT ClientId FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			IF(@ClientId !=0 AND @ClientId IS NOT NULL)
			 SET @WhereCondition=@WhereCondition+' AND (cR.ClientId='+CONVERT(varchar(100),@ClientId)+')'

----------------------
			
			--SET @ChangeOfControl=(SELECT ChangeOfControl FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			IF(@ChangeOfControl !='' AND @ChangeOfControl IS NOT NULL)
			 SET @WhereCondition=@WhereCondition+' AND (cR.IsChangeofControl=(CASE WHEN '''+CONVERT(varchar(100),@ChangeOfControl)+''' =''Y'' THEN ''Y'' ELSE ''N'' END))'

----------------------
			
			--SET @EntireAgreementClause=(SELECT EntireAgreementClause FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			IF(@EntireAgreementClause !='' AND @EntireAgreementClause IS NOT NULL)
			 SET @WhereCondition=@WhereCondition+' AND (cR.IsAgreementClause=(CASE WHEN '''+CONVERT(varchar(100),@EntireAgreementClause)+''' =''Y'' THEN ''Y'' ELSE ''N'' END))'


----------------------
			
			--SET @IsStrictliability=(SELECT Strictliability FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			IF(@IsStrictliability !='' AND @IsStrictliability IS NOT NULL)
			 SET @WhereCondition=@WhereCondition+' AND (cR.IsStrictliability=(CASE WHEN '''+CONVERT(varchar(100),@IsStrictliability)+''' =''Y'' THEN ''Y'' ELSE ''N'' END))'

---------------------- For Priority
			
			--SET @IsPriority=(SELECT PriorityContract FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			IF(@IsPriority !='' AND @IsPriority IS NOT NULL)
			 SET @WhereCondition=@WhereCondition+' AND (cR.Priority=(CASE WHEN '''+CONVERT(varchar(100),@IsPriority)+''' =''Y'' THEN ''Y'' ELSE ''N'' END))'

	END
--------------------------------------------------Where Condition end--------------------



	IF @PageNo <= 0 SET @PageNo = 1
	IF @RecordsPerPage = 0  OR @RecordsPerPage IS NULL SET @RecordsPerPage = (SELECT COUNT(1) FROM ContractRequest)

	BEGIN
		
		SET @sql ='SELECT count(*) OVER()  AS Maxcount ,'+@AllFiels+'
	    FROM  vwContractRequestLatest cR 
		INNER JOIN MstUsers ON MstUsers.UsersId = cR.Addedby
		LEFT OUTER JOIN MstContractType ct ON ct.ContractTypeId=cR.ContractTypeId
		LEFT OUTER JOIN ContractTemplate cte ON cte.ContractTemplateId=cR.ContractTemplateId
		LEFT OUTER JOIN MstCountry co ON co.CountryId=cR.CountryId
		LEFT OUTER JOIN MstDepartment de ON de.DepartmentId=cR.AssignToDepartmentId
		LEFT OUTER JOIN MstUsers us ON us.UsersId = cR.AssignToUserID
		LEFT OUTER JOIN MstUsers RN ON RN.UsersId = cR.Addedby
		LEFT OUTER JOIN vwContractStatus cs ON cs.ContractStatusID = cR.ContractStatusID
		LEFT OUTER JOIN mstContractingParty cp ON cp.ContractingPartyId = cR.ContractingTypeId
		LEFT OUTER JOIN BulkImportStaging bi ON bi.BulkImportStagingId=cR.BulkImportStagingId
		LEFT OUTER JOIN MstCurrency cu on cu.CurrencyId=cR.CurrencyID
		LEFT OUTER JOIN RptMetaDataDynamicTbl rpt ON rpt.RequestId=cr.RequestId
		LEFT OUTER JOIN RptMetaDataImportDateDynamicTbl rptD ON rptD.RequestId_ID=cr.RequestId
		LEFT OUTER JOIN RptMetaDataKeyObligationDynamicTbl rptK ON rptK.RequestId_KO=cr.RequestId
		WHERE cr.ContractTypeId IN ('+@ContractType+') '+@WhereCondition+'
		ORDER BY
	';	
	
	IF (@SortColumn = '') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cR.RequestId ASC '
		ELSE
		SET @sql =@sql  + ' cR.RequestId DESC '
	END
	ELSE IF (@SortColumn = 'Request ID') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cR.RequestId ASC '
		ELSE
		SET @sql =@sql  + ' cR.RequestId DESC '
	END
	ELSE IF (@SortColumn = 'Customer Name') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cR.ClientName ASC '
		ELSE
		SET @sql =@sql  + ' cR.ClientName DESC '
	END
	ELSE IF (@SortColumn = 'Contract Type') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' ct.ContractTypeName ASC '
		ELSE
		SET @sql =@sql  + ' ct.ContractTypeName DESC '
	END
	ELSE IF (@SortColumn = 'Requester Name') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' MstUsers.FullName ASC '
		ELSE
		SET @sql =@sql  + ' MstUsers.FullName DESC '
	END
	ELSE IF (@SortColumn = 'Contract ID') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cR.ContractID ASC '
		ELSE
		SET @sql =@sql  + ' cR.ContractID DESC '
	END
	ELSE IF (@SortColumn = 'Effective Date') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cR.EffectiveDate ASC '
		ELSE
		SET @sql =@sql  + ' cR.EffectiveDate DESC '
	END
	ELSE IF (@SortColumn = 'Expiration Date') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.ExpirationDate ASC '
		ELSE
		SET @sql =@sql  + ' cr.ExpirationDate DESC '
	END
	ELSE IF (@SortColumn = 'Renewal Date') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.RenewalDate ASC '
		ELSE
		SET @sql =@sql  + ' cr.RenewalDate DESC '
	END
	ELSE IF (@SortColumn = 'Contract Type') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' ct.ContractTypeName ASC '
		ELSE
		SET @sql =@sql  + ' ct.ContractTypeName DESC '
	END
	ELSE IF (@SortColumn = 'Contract Template') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cte.ContractTemplateName ASC '
		ELSE
		SET @sql =@sql  + ' cte.ContractTemplateName DESC '
	END
	ELSE IF (@SortColumn = 'Address') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.Address ASC '
		ELSE
		SET @sql =@sql  + ' cr.Address DESC '
	END
	ELSE IF (@SortColumn = 'State Name') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.StateName ASC '
		ELSE
		SET @sql =@sql  + ' cr.StateName DESC '
	END
	ELSE IF (@SortColumn = 'City Name') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.CityName ASC '
		ELSE
		SET @sql =@sql  + ' cr.CityName DESC '
	END
	ELSE IF (@SortColumn = 'Country Name') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' co.CountryName ASC '
		ELSE
		SET @sql =@sql  + ' co.CountryName DESC '
	END
	ELSE IF (@SortColumn = 'Pin Code') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.PinCode ASC '
		ELSE
		SET @sql =@sql  + ' cr.PinCode DESC '
	END
	ELSE IF (@SortColumn = 'Request Description') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.RequestDescription ASC '
		ELSE
		SET @sql =@sql  + ' cr.RequestDescription DESC '
	END
	ELSE IF (@SortColumn = 'Deadline Date') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.DeadlineDate ASC '
		ELSE
		SET @sql =@sql  + ' cr.DeadlineDate DESC '
	END
	ELSE IF (@SortColumn = 'Department Name') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' de.DepartmentName ASC '
		ELSE
		SET @sql =@sql  + ' de.DepartmentName DESC '
	END
	ELSE IF (@SortColumn = 'Assign User') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' us.FullName ASC '
		ELSE
		SET @sql =@sql  + ' us.FullName DESC '
	END
	ELSE IF (@SortColumn = 'Is Approve') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsApprovalRequired ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsApprovalRequired DESC '
	END
	ELSE IF (@SortColumn = 'Estimated Value') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.EstimatedValue ASC '
		ELSE
		SET @sql =@sql  + ' cr.EstimatedValue DESC '
	END
	ELSE IF (@SortColumn = 'Contact Number') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.ContactNumber ASC '
		ELSE
		SET @sql =@sql  + ' cr.ContactNumber DESC '
	END
	ELSE IF (@SortColumn = 'Email Id') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.EmailID ASC '
		ELSE
		SET @sql =@sql  + ' cr.EmailID DESC '
	END
	ELSE IF (@SortColumn = 'Contract Status') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cs.StatusName ASC '
		ELSE
		SET @sql =@sql  + ' cs.StatusName DESC '
	END
	ELSE IF (@SortColumn = 'Liability Cap') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.LiabilityCap ASC '
		ELSE
		SET @sql =@sql  + ' cr.LiabilityCap DESC '
	END
	ELSE IF (@SortColumn = 'Indemnity') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.Indemnity ASC '
		ELSE
		SET @sql =@sql  + ' cr.Indemnity DESC '
	END
	ELSE IF (@SortColumn = 'Change Of Control') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsChangeofControl ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsChangeofControl DESC '
	END
	ELSE IF (@SortColumn = 'Entire Agreement Clause') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsAgreementClause ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsAgreementClause DESC '
	END
	ELSE IF (@SortColumn = 'Strict liability') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsStrictliability ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsStrictliability DESC '
	END
	ELSE IF (@SortColumn = 'Strict liability description') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.Strictliability ASC '
		ELSE
		SET @sql =@sql  + ' cr.Strictliability DESC '
	END
	ELSE IF (@SortColumn = 'Third Party Guarantee Required') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsGuaranteeRequired ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsGuaranteeRequired DESC '
	END
	ELSE IF (@SortColumn = 'Insurance') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.Insurance ASC '
		ELSE
		SET @sql =@sql  + ' cr.Insurance DESC '
	END
	ELSE IF (@SortColumn = 'Termination for convenience') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsTerminationConvenience ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsTerminationConvenience DESC '
	END
	ELSE IF (@SortColumn = 'Termination') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.TerminationDescription ASC '
		ELSE
		SET @sql =@sql  + ' cr.TerminationDescription DESC '
	END
	ELSE IF (@SortColumn = 'Termination for material breach') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsTerminationmaterial ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsTerminationmaterial DESC '
	END
	ELSE IF (@SortColumn = 'Termination for insolvency') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsTerminationinsolvency ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsTerminationinsolvency DESC '
	END
	ELSE IF (@SortColumn = 'Termination for change of control') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsTerminationinChangeofcontrol ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsTerminationinChangeofcontrol DESC '
	END
	ELSE IF (@SortColumn = 'Termination for other reasons') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsTerminationothercauses ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsTerminationothercauses DESC '
	END
	ELSE IF (@SortColumn = 'Assignment/Novation/Subcontract') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.AssignmentNovation ASC '
		ELSE
		SET @sql =@sql  + ' cr.AssignmentNovation DESC '
	END
	ELSE IF (@SortColumn = 'Others') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.Others ASC '
		ELSE
		SET @sql =@sql  + ' cr.Others DESC '
	END
	ELSE IF (@SortColumn = 'Contracting Party') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cp.ContractingPartyName ASC '
		ELSE
		SET @sql =@sql  + ' cp.ContractingPartyName DESC '
	END
	ELSE IF (@SortColumn = 'Bulk Import') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' bi.Head ASC '
		ELSE
		SET @sql =@sql  + ' bi.Head DESC '
	END
	ELSE IF (@SortColumn = 'Is Bulk Import') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsBulkImport ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsBulkImport DESC '
	END
	ELSE IF (@SortColumn = 'Is Doc Sign Completed') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsDocSignCompleted ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsDocSignCompleted DESC '
	END
	ELSE IF (@SortColumn = 'Contract Term') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.ContractTerm ASC '
		ELSE
		SET @sql =@sql  + ' cr.ContractTerm DESC '
	END
	ELSE IF (@SortColumn = 'Contract Value') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.ContractValue ASC '
		ELSE
		SET @sql =@sql  + ' cr.ContractValue DESC '
	END
	ELSE IF (@SortColumn = 'Priority Contract') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.Priority ASC '
		ELSE
		SET @sql =@sql  + ' cr.Priority DESC '
	END
	ELSE IF (@SortColumn = 'Priority Reason') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.PriorityReason ASC '
		ELSE
		SET @sql =@sql  + ' cr.PriorityReason DESC '
	END
	ELSE
	BEGIN
	IF (@Direction = 0)
		SET @sql =@sql  + ' ['+@SortColumn+'] ASC '
		ELSE
		SET @sql =@sql  + ' ['+@SortColumn+'] DESC '
	END

	SET @sql =@sql  +'OFFSET ( '+CONVERT(VARCHAR(100),@PageNo)+' - 1 ) * '+CONVERT(VARCHAR(100),@RecordsPerPage)+' ROWS FETCH NEXT '+CONVERT(VARCHAR(100),@RecordsPerPage)+' ROWS ONLY'

	EXEC(@sql);	

	END
	

	COMMIT TRAN
	
  END TRY
  BEGIN CATCH
	ROLLBACK TRAN
  END CATCH

END













GO





/*
EXEC CustomerActiveContracts_Report
*/

ALTER PROCEDURE [dbo].[Custom_Reports_BKP_18July2016]
	@PageNo INT = 0 ,   
	@RecordsPerPage INT = 0,
	@CustomReportId INT = 0,
	@SortColumn VARCHAR(200)='Request ID',
	@Direction INT = 0,
	@UserID Int=null
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @sql AS nvarchar(MAX);
	DECLARE @Search AS nvarchar(MAX);
	DECLARE @AllFiels Varchar(max);
	DECLARE @ContractType varchar(1000);
	DECLARE @FixedField Varchar(max);DECLARE @RequestFormFields Varchar(max);DECLARE @ImportantDatesFields Varchar(max);DECLARE @KeyObligationFields Varchar(max);

	BEGIN TRY 
    BEGIN TRAN

	  -- Call sp for metadata configuration columns with passing request id.
		EXEC [rptMetaDataConfigurColumns] 0

        SET @FixedField=( SELECT 
		ISNULL(( STUFF( 
		( 
		SELECT  ', '+(a.value) FROM vwCustomColumns a
		WHERE a.Identitys='cR' AND  a.Name  IN (SELECT items FROM dbo.Split(CustomReports.ReportFixedFields,',') )
		FOR    XML path('')
		)
		, 1,1,''))	,'cR.ContractID [Contract ID]') FixedField

		FROM CustomReports WHERE CustomReportId=@CustomReportId )

		SET @RequestFormFields=( SELECT 
		ISNULL(( STUFF( 
		( 
		SELECT  ', '+(a.value) FROM vwCustomColumns a
		WHERE a.Identitys='rpt' AND a.Name  COLLATE Latin1_General_CS_AS IN (SELECT items FROM dbo.Split(CustomReports.RequestFormReportFields,',') )
		FOR    XML path('')
		)
		, 1,1,'')),'') RequestFormFields
		FROM CustomReports WHERE CustomReportId=@CustomReportId )

		SET @ImportantDatesFields=( SELECT 
		ISNULL(( STUFF( 
		( 
		SELECT  ', '+(a.value) FROM vwCustomColumns a
		WHERE a.Identitys='rptD' AND a.Name  COLLATE Latin1_General_CS_AS IN (SELECT items FROM dbo.Split(CustomReports.ImportantDatesReportFields,',') )
		FOR    XML path('')
		)
		, 1,1,'')),'') ImportantDatesFields	
		FROM CustomReports WHERE CustomReportId=@CustomReportId )
		
		SET @KeyObligationFields=( SELECT 
		ISNULL((STUFF( 
		( 
		SELECT  ', '+(a.value) FROM vwCustomColumns a
		WHERE a.Identitys='rptK' AND a.Name  COLLATE Latin1_General_CS_AS IN (SELECT items FROM dbo.Split(CustomReports.KeyObligationReportFields,',') )
		FOR    XML path('')
		)
		, 1,1,'')),'')	
		KeyObligationFields	
		FROM CustomReports WHERE CustomReportId=@CustomReportId )

		SET @AllFiels=@FixedField;

		IF (@RequestFormFields!='')
		BEGIN
		SET @AllFiels=@AllFiels+','+@RequestFormFields;
		END
		IF (@ImportantDatesFields!='')
		BEGIN
		SET @AllFiels=@AllFiels+','+@ImportantDatesFields;
		END
		IF (@KeyObligationFields!='')
		BEGIN
		SET @AllFiels=@AllFiels+','+@KeyObligationFields;
		END

	IF (RIGHT(RTRIM(@AllFiels),1) = ',')
	BEGIN
	SET @AllFiels=LEFT(@AllFiels, LEN(@AllFiels) - 1);
		IF (RIGHT(RTRIM(@AllFiels),1) = ',')
		BEGIN
			SET @AllFiels=LEFT(@AllFiels, LEN(@AllFiels) - 1);
		END
	END

	-------Get role Access, dependent on Contract Type

		DECLARE @Role VARCHAR(50)
		IF @UserID IS NULL
		SET @Role='Super Admin'
	ELSE
		SET @Role=(SELECT MstRole.RoleName FROM MstUsers INNER JOIN MstRole ON MstUsers.RoleId=MstRole.RoleId AND MstUsers.UsersId=@UserID)


	SET @ContractType=(SELECT ReportWhereClauseFields FROM CustomReports WHERE CustomReportId=@CustomReportId )

	IF @ContractType =''
	BEGIN
		--SELECT @ContractType = @ContractType +CONVERT(VARCHAR(200), ContractTypeId) + ','
		--FROM MstContractType
		SELECT @ContractType = @ContractType +CONVERT(VARCHAR(200), ContractTypeId) + ','
		FROM MstContractType WHERE ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),',')) 
		SET @ContractType=LEFT(@ContractType, LEN(@ContractType) - 1);
	END
	ELSE
	BEGIN
	DECLARE @ContractType1 VARCHAR(MAX);
	SET @ContractType1='';
	    SELECT @ContractType1 = @ContractType1 +CONVERT(VARCHAR(5000), ContractTypeId) + ','
		FROM MstContractType WHERE ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),',')) 
		SET @ContractType1=LEFT(@ContractType1, LEN(@ContractType1) - 1);
		--Select @ContractType1;

		BEGIN
			create table #SomeList
			(
			MyValues varchar(MAX)
			)
			insert #SomeList
			select @ContractType 
			--The above represent your table of denormalized data

			--Now we parse this into a temp table so we can work with it
			select * into #FirstList from #SomeList
			cross apply dbo.Split(MyValues, ',')

			--select * from #FirstList

			--Here is your parameter of delimited data
			declare @MyParms varchar(MAX) = @ContractType1

			--Again we parse this into a temp table to work with
			select * into #SecondList from (select @MyParms as Listing) x
			cross apply dbo.Split(Listing, ',')

			--select * from #SecondList

			Set @ContractType='';
			SELECT @ContractType = @ContractType +CONVERT(VARCHAR(1000), ContractTypeId) + ','
		    FROM MstContractType WHERE ContractTypeId IN (
			select sl.items from #FirstList fl
			join #SecondList sl on sl.items = fl.items
			);

			SET @ContractType=LEFT(@ContractType, LEN(@ContractType) - 1);
			--SELECT @ContractType;

			drop table #SomeList
			drop table #FirstList
			drop table #SecondList
		END

	END

	        DECLARE @WhereCondition Varchar(MAX);
			SET @WhereCondition=' ';
------------------------------------------------- Where Condition Start ------------------------------------
	BEGIN
			DECLARE @ED1 DATE;DECLARE @EDT2 DATE; 
			SET @ED1=(SELECT EffectiveDate FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			SET @EDT2=(SELECT EffectiveToDate FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			
			IF(@ED1 IS NOT NULL OR @ED1 !='')
			 SET @WhereCondition=@WhereCondition+' AND (cR.EffectiveDate BETWEEN dbo.DateOnly('''+CONVERT(varchar(100), @ED1)+''') AND dbo.DateOnly('''+CONVERT(varchar(100), @EDT2)+'''))'
-------------------------
			DECLARE @ExD1 DATE;DECLARE @ExDT2 DATE; 
			SET @ExD1=(SELECT ExpiryDate FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			SET @ExDT2=(SELECT ExpiryToDate FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);

			IF(@ExD1 IS NOT NULL OR @ExD1 !='')
			 SET @WhereCondition=@WhereCondition+' AND (cR.ExpirationDate BETWEEN dbo.DateOnly('''+CONVERT(varchar(100), @ExD1)+''') AND dbo.DateOnly('''+CONVERT(varchar(100), @ExDT2)+'''))'
--------------------------
			DECLARE @RD1 DATE;DECLARE @RDT2 DATE; 
			SET @RD1=(SELECT RenewalDate FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			SET @RDT2=(SELECT RenewalToDate FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			IF(@RD1 IS NOT NULL OR @RD1 !='')
			 SET @WhereCondition=@WhereCondition+' AND (cR.RenewalDate BETWEEN dbo.DateOnly('''+CONVERT(varchar(100), @RD1)+''') AND dbo.DateOnly('''+CONVERT(varchar(100), @RDT2)+'''))'
----------------------------
			DECLARE @SD1 DATE;DECLARE @SDT2 DATE; 
			SET @SD1=(SELECT SignatureDate FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			SET @SDT2=(SELECT SignatureToDate FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);

			IF(@SD1 IS NOT NULL OR @SD1 !='')
			 SET @WhereCondition=@WhereCondition+' AND (cR.SignatureDate BETWEEN dbo.DateOnly('''+CONVERT(varchar(100), @SD1)+''') AND dbo.DateOnly('''+CONVERT(varchar(100), @SDT2)+'''))'
-------------------

			DECLARE @RqD1 DATE;DECLARE @RqDT2 DATE; 
			SET @RqD1=(SELECT RequestDate FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			SET @RqDT2=(SELECT RequestToDate FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);

			 IF(@RqD1 IS NOT NULL OR @RqD1 !='')
			  SET @WhereCondition=@WhereCondition+' AND (dbo.DateOnly(cR.Addedon) BETWEEN dbo.DateOnly('''+CONVERT(varchar(100), @RqD1)+''') AND dbo.DateOnly('''+CONVERT(varchar(100), @RqDT2)+'''))'

----------------------
			DECLARE @ContractPartyId INT =0
			SET @ContractPartyId=(SELECT ContractingPartyId FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			IF(@ContractPartyId !=0 AND @ContractPartyId IS NOT NULL)
			 SET @WhereCondition=@WhereCondition+' AND (cp.ContractingPartyId =(CASE WHEN '+CONVERT(varchar(100),@ContractPartyId)+' IS NULL OR '+CONVERT(varchar(100),@ContractPartyId)+' =0 THEN cp.ContractingPartyId ELSE '+CONVERT(varchar(100),@ContractPartyId)+' END))'

-------------------
			DECLARE @AV1 VARCHAR(100);DECLARE @AVT2 VARCHAR(100); 
			SET @AV1=(SELECT AnnualValue FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			SET @AVT2=(SELECT AnnualValueTo FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			IF(@AV1 IS NOT NULL OR @AV1 !='')
			 SET @WhereCondition=@WhereCondition+' AND (cr.ContractValue BETWEEN '+CONVERT(varchar(100), @AV1)+' AND '+CONVERT(varchar(100), @AVT2)+')'

------------------------
			DECLARE @TerritaryId INT =0
			SET @TerritaryId=(SELECT TerritoryId FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			SET @WhereCondition=@WhereCondition+' AND (cr.CountryId =(CASE WHEN '+CONVERT(varchar(100),@TerritaryId)+' IS NULL OR '+CONVERT(varchar(100),@TerritaryId)+' =0 THEN cr.CountryId ELSE '+CONVERT(varchar(100),@TerritaryId)+' END))'

----------------------
			DECLARE @ContractStatusId INT =0
			SET @ContractStatusId=(SELECT ContractStatusId FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			SET @WhereCondition=@WhereCondition+' AND (cr.ContractStatusID =(CASE WHEN '+CONVERT(varchar(100),@ContractStatusId)+' IS NULL OR '+CONVERT(varchar(100),@ContractStatusId)+' =0 THEN cr.ContractStatusID ELSE '+CONVERT(varchar(100),@ContractStatusId)+' END))'

----------------------
			DECLARE @DepartmentId INT =0
			SET @DepartmentId=(SELECT AssignedDepartmentId FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			SET @WhereCondition=@WhereCondition+' AND (cr.AssignToDepartmentId =(CASE WHEN '+CONVERT(varchar(100),@DepartmentId)+' IS NULL OR '+CONVERT(varchar(100),@DepartmentId)+' =0 THEN cr.AssignToDepartmentId ELSE '+CONVERT(varchar(100),@DepartmentId)+' END))'
----------------------
			DECLARE @AssignUserId INT =0
			SET @AssignUserId=(SELECT AssignedUserId FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			SET @WhereCondition=@WhereCondition+' AND (cr.AssignToUserID =(CASE WHEN '+CONVERT(varchar(100),@AssignUserId)+' IS NULL OR '+CONVERT(varchar(100),@AssignUserId)+' =0 THEN cr.AssignToUserID ELSE '+CONVERT(varchar(100),@AssignUserId)+' END))'

----------------------
			DECLARE @IsThirdGuaranteeRequired VARCHAR(25);
			SET @IsThirdGuaranteeRequired=(SELECT ThirdPartyGuaranteeRequired FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			IF(@IsThirdGuaranteeRequired IS NOT NULL AND @IsThirdGuaranteeRequired !='')
			 SET @WhereCondition=@WhereCondition+' AND (cR.IsGuaranteeRequired=(CASE WHEN '''+CONVERT(varchar(100),@IsThirdGuaranteeRequired)+''' =''Y'' THEN ''Y'' ELSE ''N'' END))'

----------------------
			DECLARE @ClientId INT=0;
			SET @ClientId=(SELECT ClientId FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			IF(@ClientId !=0 AND @ClientId IS NOT NULL)
			 SET @WhereCondition=@WhereCondition+' AND (cR.ClientId='+CONVERT(varchar(100),@ClientId)+')'

----------------------
			DECLARE @ChangeOfControl VARCHAR(3)=NULL;
			SET @ChangeOfControl=(SELECT ChangeOfControl FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			IF(@ChangeOfControl !='' AND @ChangeOfControl IS NOT NULL)
			 SET @WhereCondition=@WhereCondition+' AND (cR.IsChangeofControl=(CASE WHEN '''+CONVERT(varchar(100),@ChangeOfControl)+''' =''Y'' THEN ''Y'' ELSE ''N'' END))'

----------------------
			DECLARE @EntireAgreementClause VARCHAR(3)=NULL;
			SET @EntireAgreementClause=(SELECT EntireAgreementClause FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			IF(@EntireAgreementClause !='' AND @EntireAgreementClause IS NOT NULL)
			 SET @WhereCondition=@WhereCondition+' AND (cR.IsAgreementClause=(CASE WHEN '''+CONVERT(varchar(100),@EntireAgreementClause)+''' =''Y'' THEN ''Y'' ELSE ''N'' END))'


----------------------
			DECLARE @IsStrictliability VARCHAR(3)=NULL;
			SET @IsStrictliability=(SELECT Strictliability FROM CustomReportFilters WHERE CustomReportId=@CustomReportId);
			IF(@IsStrictliability !='' AND @IsStrictliability IS NOT NULL)
			 SET @WhereCondition=@WhereCondition+' AND (cR.IsStrictliability=(CASE WHEN '''+CONVERT(varchar(100),@IsStrictliability)+''' =''Y'' THEN ''Y'' ELSE ''N'' END))'



			----SELECT @WhereCondition 
	END
--------------------------------------------------Where Condition end--------------------



	IF @PageNo <= 0 SET @PageNo = 1
	IF @RecordsPerPage = 0 SET @RecordsPerPage = (SELECT COUNT(1) FROM ContractRequest WHERE  isCustomer = 'Y')

	BEGIN
		
		SET @sql ='SELECT count(*) OVER()  AS Maxcount ,'+@AllFiels+'
	    FROM  vwContractRequestLatest cR 
		INNER JOIN MstUsers ON MstUsers.UsersId = cR.Addedby
		LEFT OUTER JOIN MstContractType ct ON ct.ContractTypeId=cR.ContractTypeId
		LEFT OUTER JOIN ContractTemplate cte ON cte.ContractTemplateId=cR.ContractTemplateId
		LEFT OUTER JOIN MstCountry co ON co.CountryId=cR.CountryId
		LEFT OUTER JOIN MstDepartment de ON de.DepartmentId=cR.AssignToDepartmentId
		LEFT OUTER JOIN MstUsers us ON us.UsersId = cR.AssignToUserID
		LEFT OUTER JOIN MstUsers RN ON RN.UsersId = cR.Addedby
		LEFT OUTER JOIN vwContractStatus cs ON cs.ContractStatusID = cR.ContractStatusID
		LEFT OUTER JOIN mstContractingParty cp ON cp.ContractingPartyId = cR.ContractingTypeId
		LEFT OUTER JOIN BulkImportStaging bi ON bi.BulkImportStagingId=cR.BulkImportStagingId
		LEFT OUTER JOIN MstCurrency cu on cu.CurrencyId=cR.CurrencyID
		LEFT OUTER JOIN RptMetaDataDynamicTbl rpt ON rpt.RequestId=cr.RequestId
		LEFT OUTER JOIN RptMetaDataImportDateDynamicTbl rptD ON rptD.RequestId_ID=cr.RequestId
		LEFT OUTER JOIN RptMetaDataKeyObligationDynamicTbl rptK ON rptK.RequestId_KO=cr.RequestId
		WHERE cr.ContractTypeId IN ('+@ContractType+') '+@WhereCondition+'
		ORDER BY
	';	
	
	IF (@SortColumn = '') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cR.RequestId ASC '
		ELSE
		SET @sql =@sql  + ' cR.RequestId DESC '
	END
	ELSE IF (@SortColumn = 'Request ID') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cR.RequestId ASC '
		ELSE
		SET @sql =@sql  + ' cR.RequestId DESC '
	END
	ELSE IF (@SortColumn = 'Customer Name') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cR.ClientName ASC '
		ELSE
		SET @sql =@sql  + ' cR.ClientName DESC '
	END
	ELSE IF (@SortColumn = 'Contract Type') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' ct.ContractTypeName ASC '
		ELSE
		SET @sql =@sql  + ' ct.ContractTypeName DESC '
	END
	ELSE IF (@SortColumn = 'Requester Name') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' MstUsers.FullName ASC '
		ELSE
		SET @sql =@sql  + ' MstUsers.FullName DESC '
	END
	ELSE IF (@SortColumn = 'Contract ID') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cR.ContractID ASC '
		ELSE
		SET @sql =@sql  + ' cR.ContractID DESC '
	END
	ELSE IF (@SortColumn = 'Effective Date') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cR.EffectiveDate ASC '
		ELSE
		SET @sql =@sql  + ' cR.EffectiveDate DESC '
	END
	ELSE IF (@SortColumn = 'Expiration Date') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.ExpirationDate ASC '
		ELSE
		SET @sql =@sql  + ' cr.ExpirationDate DESC '
	END
	ELSE IF (@SortColumn = 'Renewal Date') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.RenewalDate ASC '
		ELSE
		SET @sql =@sql  + ' cr.RenewalDate DESC '
	END
	ELSE IF (@SortColumn = 'Contract Type') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' ct.ContractTypeName ASC '
		ELSE
		SET @sql =@sql  + ' ct.ContractTypeName DESC '
	END
	ELSE IF (@SortColumn = 'Contract Template') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cte.ContractTemplateName ASC '
		ELSE
		SET @sql =@sql  + ' cte.ContractTemplateName DESC '
	END
	ELSE IF (@SortColumn = 'Address') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.Address ASC '
		ELSE
		SET @sql =@sql  + ' cr.Address DESC '
	END
	ELSE IF (@SortColumn = 'State Name') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.StateName ASC '
		ELSE
		SET @sql =@sql  + ' cr.StateName DESC '
	END
	ELSE IF (@SortColumn = 'City Name') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.CityName ASC '
		ELSE
		SET @sql =@sql  + ' cr.CityName DESC '
	END
	ELSE IF (@SortColumn = 'Country Name') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' co.CountryName ASC '
		ELSE
		SET @sql =@sql  + ' co.CountryName DESC '
	END
	ELSE IF (@SortColumn = 'Pin Code') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.PinCode ASC '
		ELSE
		SET @sql =@sql  + ' cr.PinCode DESC '
	END
	ELSE IF (@SortColumn = 'Request Description') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.RequestDescription ASC '
		ELSE
		SET @sql =@sql  + ' cr.RequestDescription DESC '
	END
	ELSE IF (@SortColumn = 'Deadline Date') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.DeadlineDate ASC '
		ELSE
		SET @sql =@sql  + ' cr.DeadlineDate DESC '
	END
	ELSE IF (@SortColumn = 'Department Name') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' de.DepartmentName ASC '
		ELSE
		SET @sql =@sql  + ' de.DepartmentName DESC '
	END
	ELSE IF (@SortColumn = 'Assign User') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' us.FullName ASC '
		ELSE
		SET @sql =@sql  + ' us.FullName DESC '
	END
	ELSE IF (@SortColumn = 'Is Approve') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsApprovalRequired ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsApprovalRequired DESC '
	END
	ELSE IF (@SortColumn = 'Estimated Value') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.EstimatedValue ASC '
		ELSE
		SET @sql =@sql  + ' cr.EstimatedValue DESC '
	END
	ELSE IF (@SortColumn = 'Contact Number') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.ContactNumber ASC '
		ELSE
		SET @sql =@sql  + ' cr.ContactNumber DESC '
	END
	ELSE IF (@SortColumn = 'Email Id') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.EmailID ASC '
		ELSE
		SET @sql =@sql  + ' cr.EmailID DESC '
	END
	ELSE IF (@SortColumn = 'Contract Status') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cs.StatusName ASC '
		ELSE
		SET @sql =@sql  + ' cs.StatusName DESC '
	END
	ELSE IF (@SortColumn = 'Liability Cap') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.LiabilityCap ASC '
		ELSE
		SET @sql =@sql  + ' cr.LiabilityCap DESC '
	END
	ELSE IF (@SortColumn = 'Indemnity') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.Indemnity ASC '
		ELSE
		SET @sql =@sql  + ' cr.Indemnity DESC '
	END
	ELSE IF (@SortColumn = 'Change Of Control') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsChangeofControl ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsChangeofControl DESC '
	END
	ELSE IF (@SortColumn = 'Entire Agreement Clause') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsAgreementClause ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsAgreementClause DESC '
	END
	ELSE IF (@SortColumn = 'Strict liability') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsStrictliability ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsStrictliability DESC '
	END
	ELSE IF (@SortColumn = 'Strict liability description') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.Strictliability ASC '
		ELSE
		SET @sql =@sql  + ' cr.Strictliability DESC '
	END
	ELSE IF (@SortColumn = 'Third Party Guarantee Required') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsGuaranteeRequired ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsGuaranteeRequired DESC '
	END
	ELSE IF (@SortColumn = 'Insurance') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.Insurance ASC '
		ELSE
		SET @sql =@sql  + ' cr.Insurance DESC '
	END
	ELSE IF (@SortColumn = 'Termination for convenience') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsTerminationConvenience ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsTerminationConvenience DESC '
	END
	ELSE IF (@SortColumn = 'Termination') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.TerminationDescription ASC '
		ELSE
		SET @sql =@sql  + ' cr.TerminationDescription DESC '
	END
	ELSE IF (@SortColumn = 'Termination for material breach') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsTerminationmaterial ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsTerminationmaterial DESC '
	END
	ELSE IF (@SortColumn = 'Termination for insolvency') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsTerminationinsolvency ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsTerminationinsolvency DESC '
	END
	ELSE IF (@SortColumn = 'Termination for change of control') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsTerminationinChangeofcontrol ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsTerminationinChangeofcontrol DESC '
	END
	ELSE IF (@SortColumn = 'Termination for other reasons') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsTerminationothercauses ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsTerminationothercauses DESC '
	END
	ELSE IF (@SortColumn = 'Assignment/Novation/Subcontract') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.AssignmentNovation ASC '
		ELSE
		SET @sql =@sql  + ' cr.AssignmentNovation DESC '
	END
	ELSE IF (@SortColumn = 'Others') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.Others ASC '
		ELSE
		SET @sql =@sql  + ' cr.Others DESC '
	END
	ELSE IF (@SortColumn = 'Contracting Party') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cp.ContractingPartyName ASC '
		ELSE
		SET @sql =@sql  + ' cp.ContractingPartyName DESC '
	END
	ELSE IF (@SortColumn = 'Bulk Import') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' bi.Head ASC '
		ELSE
		SET @sql =@sql  + ' bi.Head DESC '
	END
	ELSE IF (@SortColumn = 'Is Bulk Import') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsBulkImport ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsBulkImport DESC '
	END
	ELSE IF (@SortColumn = 'Is Doc Sign Completed') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.IsDocSignCompleted ASC '
		ELSE
		SET @sql =@sql  + ' cr.IsDocSignCompleted DESC '
	END
	ELSE IF (@SortColumn = 'Contract Term') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.ContractTerm ASC '
		ELSE
		SET @sql =@sql  + ' cr.ContractTerm DESC '
	END
	ELSE IF (@SortColumn = 'Contract Value') 
	BEGIN
		IF (@Direction = 0)
		SET @sql =@sql  + ' cr.ContractValue ASC '
		ELSE
		SET @sql =@sql  + ' cr.ContractValue DESC '
	END
	ELSE
	BEGIN
	IF (@Direction = 0)
		SET @sql =@sql  + ' ['+@SortColumn+'] ASC '
		ELSE
		SET @sql =@sql  + ' ['+@SortColumn+'] DESC '
	END

	SET @sql =@sql  +'OFFSET ( '+CONVERT(VARCHAR(100),@PageNo)+' - 1 ) * '+CONVERT(VARCHAR(100),@RecordsPerPage)+' ROWS FETCH NEXT '+CONVERT(VARCHAR(100),@RecordsPerPage)+' ROWS ONLY'

	EXEC(@sql);	

	END
	

	COMMIT TRAN
	
  END TRY
  BEGIN CATCH
	ROLLBACK TRAN
  END CATCH

END















GO



/*
EXEC CustomerGeography_Report
*/


ALTER PROCEDURE [dbo].[CustomerGeography_Report]
@Flags INT = 0 ,
	@PageNo INT = 0 ,   
	@RecordsPerPage INT = 0,
	@FromDate DATETIME=NULL,
	@ToDate DATETIME=NULL,
	@ContractTypeId INT = NULL,
	@Search VARCHAR(10)='',
	@SortColumn VARCHAR(20)='Customer Name',
	@Direction INT = 0,
	@UserID INT=NULL
AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @TmpContractType TABLE (ContractTypeId INT)
	IF @ContractTypeId IS NULL
		BEGIN
		INSERT INTO @TmpContractType (ContractTypeId) 
		SELECT ContractTypeId FROM MstContractType
		END
	ELSE
	BEGIN
		INSERT INTO @TmpContractType (ContractTypeId) 
		SELECT @ContractTypeId
	END

	  		DECLARE @Role VARCHAR(50)
		IF @UserID IS NULL
		SET @Role='Super Admin'
	ELSE
		SET @Role=(SELECT MstRole.RoleName FROM MstUsers INNER JOIN MstRole ON MstUsers.RoleId=MstRole.RoleId AND MstUsers.UsersId=@UserID)

	IF @FromDate IS NULL SET @FromDate = (SELECT MIN(Addedon) FROM ContractRequest WHERE  isCustomer = 'Y')
	IF @ToDate IS NULL SET @ToDate = GETDATE()
	IF @PageNo <= 0 SET @PageNo = 1
	IF @RecordsPerPage = 0 SET @RecordsPerPage = (SELECT COUNT(1) FROM ContractRequest WHERE isCustomer = 'Y')

	BEGIN
		
		SELECT 
		cR.ClientName [Customer Name], 
		MstUsers.FullName [Requester Name], 
		cR.ContractID [Contract ID], 
		ct.ContractTypeName [Contract Type],
		dbo.DateFormat(cR.EffectiveDate) [Effective Date],
		dbo.DateFormat(cr.ExpirationDate) [Expiration Date],
		con.CountryName [Geography]
		FROM vwContractRequestLatest cR  --ContractRequest cR    COMMENTED ON 11 NOV 14 AFTER WORKFLOW CHANGES
		INNER JOIN MstUsers ON MstUsers.UsersId = cR.Addedby 
		LEFT OUTER JOIN MstContractType ct ON cR.ContractTypeId =ct.ContractTypeId 
		LEFT OUTER JOIN MstCountry con ON cR.CountryId=con.CountryId
		WHERE cr.ContractTypeId IN (SELECT ContractTypeId FROM @TmpContractType ) AND cr.ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),cr.ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),','))  AND  
		(cR.isCustomer = 'Y')
		AND dbo.DateFormat(Cr.Addedon) BETWEEN dbo.DateOnly(@FromDate) AND dbo.DateOnly(@ToDate)
		AND cr.ContractID IS NOT NULL
		AND
		( ClientName LIKE '%'+ @Search +'%'  )
		ORDER BY 
		CASE WHEN @SortColumn='' AND @Direction = 0  THEN cR.RequestId END DESC,

		CASE WHEN @SortColumn =  'Customer Name'  AND @Direction = 0 THEN cR.ClientName END ASC, 
		CASE WHEN @SortColumn =  'Requester Name'  AND @Direction = 0 THEN MstUsers.FullName  END ASC, 
		CASE WHEN @SortColumn =  'Contract ID'  AND @Direction = 0 THEN cR.ContractID END ASC, 
		CASE WHEN @SortColumn =  'Contract Type'  AND @Direction = 0 THEN ct.ContractTypeName END ASC,
		CASE WHEN @SortColumn =  'Effective Date'  AND @Direction = 0 THEN cR.EffectiveDate  END ASC, 
		CASE WHEN @SortColumn =  'Expiration Date'  AND @Direction = 0 THEN cr.ExpirationDate END ASC,
		CASE WHEN @SortColumn =  'Geography'  AND @Direction = 0 THEN con.CountryName END ASC,

			
		CASE WHEN @SortColumn =  'Customer Name'  AND @Direction = 1 THEN cR.ClientName END DESC, 
		CASE WHEN @SortColumn =  'Requester Name'  AND @Direction = 1 THEN MstUsers.FullName  END DESC, 
		CASE WHEN @SortColumn =  'Contract ID'  AND @Direction = 1 THEN cR.ContractID END DESC, 
		CASE WHEN @SortColumn =  'Contract Type'  AND @Direction = 1 THEN ct.ContractTypeName END DESC,
		CASE WHEN @SortColumn =  'Effective Date'  AND @Direction = 1 THEN cR.EffectiveDate  END DESC, 
		CASE WHEN @SortColumn =  'Expiration Date'  AND @Direction = 1 THEN cr.ExpirationDate END DESC,
	    CASE WHEN @SortColumn =  'Geography'  AND @Direction = 1 THEN con.CountryName END DESC

		OFFSET ( @PageNo - 1 ) * @RecordsPerPage ROWS
		FETCH NEXT @RecordsPerPage ROWS ONLY
 IF @Flags=0
	  BEGIN
		SELECT ISNULL(COUNT(1),0)TotalRecords  FROM vwContractRequestLatest   --ContractRequest     COMMENTED ON 11 NOV 14 AFTER WORKFLOW CHANGES  
		WHERE ContractTypeId IN (SELECT ContractTypeId FROM @TmpContractType ) AND ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),','))  AND  
		(isCustomer = 'Y') AND ContractID IS NOT NULL
		AND dbo.DateOnly(Addedon) BETWEEN dbo.DateOnly(@FromDate) AND dbo.DateOnly(@ToDate)
		AND
		( ClientName LIKE '%'+ @Search +'%'  )

	END
		END

END












GO



/*
EXEC CustomerTypeValueGeography_Report
*/


ALTER PROCEDURE [dbo].[CustomerTypeValueGeography_Report]
@Flags INT = 0 ,
	@PageNo INT = 0 ,   
	@RecordsPerPage INT = 0,
	@FromDate DATETIME=NULL,
	@ToDate DATETIME=NULL,
	@ContractTypeId INT = NULL,
	@Search VARCHAR(10)='',
	@SortColumn VARCHAR(20)='Customer Name',
	@Direction INT = 0,
	@UserID INT=NULL
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @TmpContractType TABLE (ContractTypeId INT)
	IF @ContractTypeId IS NULL
		BEGIN
		INSERT INTO @TmpContractType (ContractTypeId) 
		SELECT ContractTypeId FROM MstContractType
		END
	ELSE
	BEGIN
		INSERT INTO @TmpContractType (ContractTypeId) 
		SELECT @ContractTypeId
	END

	 			DECLARE @Role VARCHAR(50)
		IF @UserID IS NULL
		SET @Role='Super Admin'
	ELSE
		SET @Role=(SELECT MstRole.RoleName FROM MstUsers INNER JOIN MstRole ON MstUsers.RoleId=MstRole.RoleId AND MstUsers.UsersId=@UserID)

	IF @FromDate IS NULL SET @FromDate = (SELECT MIN(Addedon) FROM ContractRequest WHERE  isCustomer = 'Y')
	IF @ToDate IS NULL SET @ToDate = GETDATE()
	IF @PageNo <= 0 SET @PageNo = 1
	IF @RecordsPerPage = 0 SET @RecordsPerPage = (SELECT COUNT(1) FROM ContractRequest WHERE  isCustomer = 'Y')

	BEGIN
		
		SELECT 
		cR.ClientName [Customer Name], 
		MstUsers.FullName [Requester Name], 
		cR.ContractID [Contract ID], 
		ct.ContractTypeName [Contract Type],
		dbo.DateFormat(cR.EffectiveDate) [Effective Date],
		dbo.DateFormat(cr.ExpirationDate) [Expiration Date],
		--cR.ContractValue [Contract Value],
		CONVERT(VARCHAR(200), Format(cr.ContractValue ,'N','en-US' ))+' ('+cu.CurrencyCode+')' [Contract Value],
		con.CountryName [Geography]
		FROM vwContractRequestLatest cR  --ContractRequest cR    COMMENTED ON 11 NOV 14 AFTER WORKFLOW CHANGES
		INNER JOIN MstUsers ON MstUsers.UsersId = cR.Addedby 
		LEFT OUTER JOIN MstContractType ct ON cR.ContractTypeId =ct.ContractTypeId 
		LEFT OUTER JOIN MstCountry con ON cR.CountryId=con.CountryId
		LEFT OUTER JOIN MstCurrency cu on cu.CurrencyId=cR.CurrencyID
		WHERE cr.ContractTypeId IN (SELECT ContractTypeId FROM @TmpContractType ) AND cr.ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),cr.ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),','))  AND  
		 cR.isCustomer = 'Y'
		AND dbo.DateFormat(Cr.Addedon) BETWEEN dbo.DateOnly(@FromDate) AND dbo.DateOnly(@ToDate)
		AND cr.ContractID IS NOT NULL
		AND
		( ClientName LIKE '%'+ @Search +'%'  )
		ORDER BY 
		CASE WHEN @SortColumn='' AND @Direction = 0  THEN cR.RequestId END DESC,

		CASE WHEN @SortColumn =  'Customer Name'  AND @Direction = 0 THEN cR.ClientName END ASC, 
		CASE WHEN @SortColumn =  'Requester Name'  AND @Direction = 0 THEN MstUsers.FullName  END ASC, 
		CASE WHEN @SortColumn =  'Contract ID'  AND @Direction = 0 THEN cR.ContractID END ASC, 
		CASE WHEN @SortColumn =  'Contract Type'  AND @Direction = 0 THEN ct.ContractTypeName END ASC,
		CASE WHEN @SortColumn =  'Effective Date'  AND @Direction = 0 THEN cR.EffectiveDate  END ASC, 
		CASE WHEN @SortColumn =  'Expiration Date'  AND @Direction = 0 THEN cr.ExpirationDate END ASC,
		CASE WHEN @SortColumn =  'Contract Value'  AND @Direction = 0 THEN cr.ContractValue END ASC,
		CASE WHEN @SortColumn =  'Geography'  AND @Direction = 0 THEN con.CountryName END ASC,

			
		CASE WHEN @SortColumn =  'Customer Name'  AND @Direction = 1 THEN cR.ClientName END DESC, 
		CASE WHEN @SortColumn =  'Requester Name'  AND @Direction = 1 THEN MstUsers.FullName  END DESC, 
		CASE WHEN @SortColumn =  'Contract ID'  AND @Direction = 1 THEN cR.ContractID END DESC, 
		CASE WHEN @SortColumn =  'Contract Type'  AND @Direction = 1 THEN ct.ContractTypeName END DESC,
		CASE WHEN @SortColumn =  'Effective Date'  AND @Direction = 1 THEN cR.EffectiveDate  END DESC, 
		CASE WHEN @SortColumn =  'Expiration Date'  AND @Direction = 1 THEN cr.ExpirationDate END DESC,
		CASE WHEN @SortColumn =  'Contract Value'  AND @Direction = 1 THEN cr.ContractValue END DESC,
	    CASE WHEN @SortColumn =  'Geography'  AND @Direction = 1 THEN con.CountryName END DESC

		OFFSET ( @PageNo - 1 ) * @RecordsPerPage ROWS
		FETCH NEXT @RecordsPerPage ROWS ONLY
 IF @Flags=0
	  BEGIN
		SELECT ISNULL(COUNT(1),0)TotalRecords  FROM vwContractRequestLatest   --ContractRequest     COMMENTED ON 11 NOV 14 AFTER WORKFLOW CHANGES  
		WHERE ContractTypeId IN (SELECT ContractTypeId FROM @TmpContractType ) AND ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),','))  AND        
		 isCustomer = 'Y' AND ContractID IS NOT NULL
		AND dbo.DateOnly(Addedon) BETWEEN dbo.DateOnly(@FromDate) AND dbo.DateOnly(@ToDate)
		AND
		( ClientName LIKE '%'+ @Search +'%'  )

	END
	END

END













GO

/*

EXEC CustomerUpcomingRenewals_Report

*/


ALTER PROCEDURE [dbo].[CustomerUpcomingRenewals_Report]
@Flags INT = 0 ,
	@PageNo INT = 0 ,   
	@RecordsPerPage INT = 0,
	@FromDate DATETIME=NULL,
	@ToDate DATETIME=NULL,
	@ContractTypeId INT = NULL,
	@Search VARCHAR(10)='',
	@SortColumn VARCHAR(50)='Customer Name',
	@Direction INT = 0,
	@UserID INT=NULL
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @TmpContractType TABLE (ContractTypeId INT)
	IF @ContractTypeId IS NULL
		BEGIN
		INSERT INTO @TmpContractType (ContractTypeId) 
		SELECT ContractTypeId FROM MstContractType
		END
	ELSE
	BEGIN
		INSERT INTO @TmpContractType (ContractTypeId) 
		SELECT @ContractTypeId
	END

		DECLARE @Role VARCHAR(50)
		IF @UserID IS NULL
		SET @Role='Super Admin'
	ELSE
		SET @Role=(SELECT MstRole.RoleName FROM MstUsers INNER JOIN MstRole ON MstUsers.RoleId=MstRole.RoleId AND MstUsers.UsersId=@UserID)

	IF @FromDate IS NULL SET @FromDate = (SELECT MIN(Addedon) FROM ContractRequest WHERE isCustomer = 'Y')
	IF @ToDate IS NULL SET @ToDate = GETDATE()
	IF @PageNo <= 0 SET @PageNo = 1
	IF @RecordsPerPage = 0 SET @RecordsPerPage = (SELECT COUNT(1) FROM ContractRequest WHERE isCustomer = 'Y')

	BEGIN
		
		SELECT [Renewal Notification Date],[Customer Name], [Requester Name], [Contract ID], [Contract Type], 
		[Effective Date], [Expiration Date], [Renewal Notification Days]
		FROM
		(
		SELECT
		dbo.DateFormat(cR.RenewalDate) [Renewal Notification Date],
		cR.ClientName [Customer Name], 
		MstUsers.FullName [Requester Name], 
		cR.ContractID [Contract ID], 
		ct.ContractTypeName [Contract Type],
		dbo.DateFormat(cR.EffectiveDate) [Effective Date],
		dbo.DateFormat(cr.ExpirationDate) [Expiration Date],
		--(CASE WHEN cr.RenewalAlert7days='Y' THEN '7,' else ' ' END  +''+ CASE WHEN cr.RenewalAlert30days='Y'  THEN '30,' ELSE ' ' END+''+CASE WHEN cr.RenewalAlert24Hrs='Y'  THEN '1' ELSE ' ' END)
		--[Renewal Notification Days] ,
		(CASE WHEN cr.RenewalAlert24Hrs='Y'  THEN '1,' ELSE ' ' END
		+''+CASE WHEN cr.RenewalAlert7days='Y' THEN '7,' else ' ' END  
		+''+ CASE WHEN cr.RenewalAlert30days='Y'  THEN '30,' ELSE ' ' END
		+''+ CASE WHEN cr.RenewalAlert60days='Y'  THEN '60,' ELSE ' ' END
		+''+ CASE WHEN cr.RenewalAlert90days='Y'  THEN '90,' ELSE ' ' END
		+''+ CASE WHEN cr.RenewalAlert180days='Y'  THEN '180' ELSE ' ' END	
		)[Renewal Notification Days] ,
		cr.RenewalDate,
		cr.RequestId,
		cR.ClientName,
		MstUsers.FullName, 
		cR.ContractID,
		ct.ContractTypeName,
		cR.EffectiveDate,
		cr.ExpirationDate
		
		
		FROM vwContractRequestLatest cR  --ContractRequest cR    COMMENTED ON 11 NOV 14 AFTER WORKFLOW CHANGES
		INNER JOIN MstUsers ON MstUsers.UsersId = cR.Addedby 
		LEFT OUTER JOIN MstContractType ct ON cR.ContractTypeId =ct.ContractTypeId 
		LEFT OUTER JOIN MstCountry con ON cR.CountryId=con.CountryId
		WHERE cr.ContractTypeId IN (SELECT ContractTypeId FROM @TmpContractType ) AND cr.ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),cr.ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),','))  AND  
		cR.isCustomer = 'Y' 
		AND dbo.DateOnly(cR.RenewalDate) IS NOT NULL
		AND dbo.DateFormat(Cr.Addedon) BETWEEN dbo.DateOnly(@FromDate) AND dbo.DateOnly(@ToDate)
		AND cr.ContractID IS NOT NULL
		AND
		( ClientName LIKE '%'+ @Search +'%'  )
		)TMP
		ORDER BY 
		CASE WHEN @SortColumn='' AND @Direction = 0  THEN RenewalDate END ASC,
		CASE WHEN @SortColumn =  'Customer Name'  AND @Direction = 0 THEN ClientName END ASC, 
		CASE WHEN @SortColumn =  'Requester Name'  AND @Direction = 0 THEN FullName  END ASC, 
		CASE WHEN @SortColumn =  'Contract ID'  AND @Direction = 0 THEN ContractID END ASC, 
		CASE WHEN @SortColumn =  'Contract Type'  AND @Direction = 0 THEN ContractTypeName END ASC,
		CASE WHEN @SortColumn =  'Effective Date'  AND @Direction = 0 THEN EffectiveDate  END ASC, 
		CASE WHEN @SortColumn =  'Expiration Date'  AND @Direction = 0 THEN ExpirationDate END ASC,
		CASE WHEN @SortColumn =  'Renewal Notification Date'  AND @Direction = 0 THEN RenewalDate END ASC,
		CASE WHEN @SortColumn =  'Renewal Notification Days'  AND @Direction = 0 THEN [Renewal Notification Days] END ASC,
		
		
		CASE WHEN @SortColumn =  'Customer Name'  AND @Direction = 1 THEN ClientName END DESC, 
		CASE WHEN @SortColumn =  'Requester Name'  AND @Direction = 1 THEN FullName  END DESC, 
		CASE WHEN @SortColumn =  'Contract ID'  AND @Direction = 1 THEN ContractID END DESC, 
		CASE WHEN @SortColumn =  'Contract Type'  AND @Direction = 1 THEN ContractTypeName END DESC,
		CASE WHEN @SortColumn =  'Effective Date'  AND @Direction = 1 THEN EffectiveDate  END DESC, 
		CASE WHEN @SortColumn =  'Expiration Date'  AND @Direction = 1 THEN ExpirationDate END DESC,
		CASE WHEN @SortColumn =  'Renewal Notification Date'  AND @Direction = 1 THEN RenewalDate END DESC,
		CASE WHEN @SortColumn =  'Renewal Notification Days'  AND @Direction = 1 THEN [Renewal Notification Days] END DESC
		
		
		OFFSET ( @PageNo - 1 ) * @RecordsPerPage ROWS
		FETCH NEXT @RecordsPerPage ROWS ONLY
		IF @Flags=0
	  BEGIN
		SELECT ISNULL(COUNT(1),0)TotalRecords  FROM vwContractRequestLatest   --ContractRequest     COMMENTED ON 11 NOV 14 AFTER WORKFLOW CHANGES  
		WHERE  ContractTypeId IN (SELECT ContractTypeId FROM @TmpContractType ) AND ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),','))  AND  
		isCustomer = 'Y' AND ContractID IS NOT NULL
		AND dbo.DateOnly(RenewalDate) IS NOT NULL
		AND dbo.DateOnly(Addedon) BETWEEN dbo.DateOnly(@FromDate) AND dbo.DateOnly(@ToDate)
		AND
		( ClientName LIKE '%'+ @Search +'%'  )

	END
	END

END












GO





ALTER FUNCTION [dbo].[GetClientAddress]
(  
 @ClientId int=0,  
 @Flag VARCHAR(2)=null  
)  
RETURNS  VARCHAR(8000)  
AS  
BEGIN  
DECLARE @Result VARCHAR(8000)    
If @Flag='A'  
BEGIN  
   --SELECT @Result = COALESCE(@Result +'',' ') +'<br>'+ cad.Address + '<br>'+(case when Street2='' then '' else  (case when street2 is not null then isnull(Street2,'') +',<br>' else '' end)End) +mc.CountryName+',<br>'+StateName+',<br>'+CityName+',<br>'+cad.PinCode+'<br><br>'  
   --FROM Client c INNER JOIN ClientAddressDetails cad ON c.ClientId=cad.ClientId  
  
   --INNER JOIN MstCountry mc ON mc.CountryId=cad.CountryId  
   
   --WHERE c.ClientId=@ClientId  
   --SET @Result=LTRIM(RTRIM(@Result))  
   ----RETURN @Result  
   --RETURN (SELECT substring(@Result, (len(@Result)-(len(@Result) - len(',<br>'))),(len(@Result))))  
   SELECT @Result = IIF(cad.Address IS NULL OR cad.Address='' ,'',cad.Address+'<br/>') +   
           IIF(Street2 IS  NULL OR Street2=''  ,'',Street2++'<br/>')+  
           IIF(CityName IS NULL OR CityName='' ,'',CityName+'<br/>')+  
           IIF(StateName IS NULL OR StateName='' ,'',StateName+'<br/>')+  
           IIF(cad.PinCode IS NULL OR cad.PinCode='' ,'',cad.PinCode+'<br/>')+             
           ISNULL((SELECT DISTINCT ''+CountryName FROM MstCountry WHERE CountryId=cad.CountryId),'')   
           FROM Client c INNER JOIN ClientAddressDetails cad ON c.ClientId=cad.ClientId  
           INNER JOIN MstCountry mc ON mc.CountryId=cad.CountryId   
           WHERE c.ClientId=@ClientId   
    RETURN @Result  
END  
ELSE IF @Flag='C'  
BEGIN 
   SELECT @Result = COALESCE(@Result +'',' ') + case when  (cad.ContactNumber is not null and  cad.ContactNumber<>'') then + cad.ContactNumber +',<br>'  else + cad.ContactNumber +'<br>' end 
   --SELECT @Result = COALESCE(@Result +'',' ') + cad.ContactNumber +',<br>'  
   FROM Client c INNER JOIN ClientContactDetails cad ON c.ClientId=cad.ClientId  
   WHERE c.ClientId=@ClientId  
   SET @Result=LTRIM(RTRIM(@Result))  
      RETURN @Result
   --RETURN (SELECT substring(@Result, 1, len(@Result) - len(',<br>'))+'<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;')  
END  
ELSE IF @Flag='E'  
BEGIN  
	SELECT @Result = COALESCE(@Result +'',' ') + case when  (cad.EmailID is not null and  cad.EmailID<>'') then + cad.EmailID +',<br>'  else + cad.EmailID +'<br>' end 
    --SELECT @Result = COALESCE(@Result +'',' ') + cad.EmailID +',<br>'  
   FROM Client c INNER JOIN ClientEmaildetails cad ON c.ClientId=cad.ClientId  
   WHERE c.ClientId=@ClientId  
   SET @Result=LTRIM(RTRIM(@Result))  
   RETURN @Result
   --RETURN (SELECT substring(@Result, 1, len(@Result) - len(',<br>'))+'<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;')  
END  
RETURN @Result  
END
GO


ALTER PROCEDURE [dbo].[GetGeographicCSAnalysis]
@UserId INT=NULL,
@Tenure VARCHAR(1)=NULL
AS
BEGIN

DECLARE @CustomerTable TABLE(CustomerCount INT,Id INT)
DECLARE @SupplierTable TABLE(SupplierCount INT,Id INT)

DECLARE @UserRole VARCHAR(50)
SET @UserRole=(SELECT MstRole.RoleName FROM MstUsers INNER JOIN MstRole ON MstUsers.RoleId=MstRole.RoleId AND MstUsers.UsersId=@UserID)

	IF dbo.IsTableExists('#AccessibleContractType')> 0 DROP TABLE #AccessibleContractType;	
	CREATE TABLE #AccessibleContractType
	(
	ContractTypeId INT
	);

	IF @UserID=1083
	BEGIN
		INSERT INTO #AccessibleContractType SELECT ContractTypeId FROM MstContractType
	END
	ELSE
	INSERT INTO #AccessibleContractType
	SELECT DISTINCT MstContractType.ContractTypeId 
	FROM RoleAccessDynamic 
	INNER JOIN vwDynamicAccess ON RoleAccessDynamic.AccessId= vwDynamicAccess.AccessId
	INNER JOIN MstContractType ON vwDynamicAccess.ControlName=MstContractType.ContractTypeName
	INNER JOIN MstRole ON MstRole.RoleId=RoleAccessDynamic.RoleId
	INNER JOIN MstUsers ON MstUsers.RoleId=MstRole.RoleId AND MstUsers.UsersId=@UserID
	WHERE vwDynamicAccess.ChildId=603  AND RoleAccessDynamic.RoleId=MstUsers.RoleId

INSERT INTO @CustomerTable 

SELECT COUNT(tbl.[ClientId]),tbl.[CountryId] FROM
 (
		SELECT vwContractRequestLatest.ClientId ClientId,vwContractRequestLatest.CountryId
		FROM vwContractRequestLatest 
		RIGHT OUTER JOIN MstCountry 
		ON MstCountry.CountryId=vwContractRequestLatest.CountryId 
		WHERE ISNULL(vwContractRequestLatest.isCustomer,'Y')='Y' AND
		EffectiveDate IS NOT NULL AND vwContractRequestLatest.ContractStatusID=9
		AND ISNULL(ExpirationDate,dbo.DateOnly(GETDATE())) >= dbo.DateOnly(GETDATE()) AND
		EXISTS ( SELECT 1 FROM vwRequestAccess uA WHERE uA.RequestId = vwContractRequestLatest.RequestId AND uA.UsersId = @UserId)		
		AND vwContractRequestLatest.EffectiveDate BETWEEN (SELECT CASE WHEN @Tenure='1' THEN DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()), 0) WHEN @Tenure='3' THEN DATEADD(year,-3,GETDATE()) ELSE DATEADD(year,-5,GETDATE()) END ) AND GETDATE()		 
		--GROUP BY MstCountry.CountryName,MstCountry.CountryCode,MstCountry.CountryId,vwContractRequestLatest.ClientId,vwContractRequestLatest.CountryId
) tbl GROUP BY tbl.[CountryId]
		

INSERT INTO @SupplierTable 

SELECT COUNT(tbl.[ClientId]),tbl.[CountryId] FROM
  (
		SELECT vwContractRequestLatest.ClientId ClientId,vwContractRequestLatest.CountryId
		FROM vwContractRequestLatest
		INNER JOIN #AccessibleContractType act ON vwContractRequestLatest.ContractingTypeId=act.ContractTypeId
		LEFT OUTER JOIN MstCountry 
		ON MstCountry.CountryId=vwContractRequestLatest.CountryId 
		WHERE vwContractRequestLatest.isCustomer='N' AND
		EffectiveDate IS NOT NULL AND vwContractRequestLatest.ContractStatusID=9
		AND ISNULL(ExpirationDate,dbo.DateOnly(GETDATE())) >= dbo.DateOnly(GETDATE()) AND
		EXISTS ( SELECT 1 FROM vwRequestAccess uA WHERE uA.RequestId = vwContractRequestLatest.RequestId AND uA.UsersId = @UserId)
		--AND ISNULL(IsBulkImport,'N')='N' 
		AND vwContractRequestLatest.EffectiveDate BETWEEN (SELECT CASE WHEN @Tenure='1' THEN DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()), 0) WHEN @Tenure='3' THEN DATEADD(year,-3,GETDATE()) ELSE DATEADD(year,-5,GETDATE()) END ) AND GETDATE()
		----AND vwContractRequestLatest.ContractTypeId IN(SELECT items FROM dbo.Split((CASE WHEN @UserRole='Super Admin' THEN CONVERT(VARCHAR(10),vwContractRequestLatest.ContractTypeId)
		----ELSE dbo.GetContractTypeAccess(@UserRole) END),','))
		--GROUP BY MstCountry.CountryName,MstCountry.CountryCode,MstCountry.CountryId,vwContractRequestLatest.ClientId,vwContractRequestLatest.CountryId
)  tbl GROUP BY tbl.[CountryId]
		

	SELECT	
	Con.[CountryName] CountryName,
	ISNULL(CON.[CountryCode],'') CountryCode,
	SUM(ISNULL(tbl.[CustomerValue],0)) CustomerValue,
	SUM(ISNULL(tbl.[SupplierValue],0)) SupplierValue,
	ISNULL((SELECT [CustomerCount] FROM @CustomerTable WHERE Id=Con.[CountryId] ),0) CustomerCount,
	ISNULL((SELECT [SupplierCount] FROM @SupplierTable WHERE Id=Con.[CountryId] ),0) SupplierCount,
	ISNULL(tbl.ColorCode,0)  ColorCode
	FROM
	(
		SELECT  MstCountry.CountryName,
		MstCountry.CountryCode, 
		MstCountry.CountryId,
		DBO.ConvertCurrency(	SUM(IIF(ISNULL(vwContractRequestLatest.isCustomer,'Y')='Y', vwContractRequestLatest.ContractValue,0)), vwContractRequestLatest.CurrencyID) CustomerValue,
		DBO.ConvertCurrency(SUM(IIF(vwContractRequestLatest.isCustomer='N',vwContractRequestLatest.ContractValue,0)), 	vwContractRequestLatest.CurrencyID) SupplierValue, 
		SUM(IIF(ISNULL(vwContractRequestLatest.isCustomer,'Y')='Y',1,0)) CustomerCountNumber,
		SUM(IIF(vwContractRequestLatest.isCustomer='N',1,0)) SupplierCountNumber,
		(SELECT COUNT(1) FROM vwContractRequestLatest WHERE vwContractRequestLatest.CountryId=MstCountry.CountryId AND
		ISNULL(vwContractRequestLatest.isCustomer,'Y')='Y'  ) CustomerCount,
		(SELECT COUNT(1) FROM vwContractRequestLatest WHERE vwContractRequestLatest.CountryId=MstCountry.CountryId AND
		vwContractRequestLatest.isCustomer='N'  ) SupplierCount,
		--ISNULL((CASE  WHEN ISNULL(SUM(IIF(ISNULL(vwContractRequestLatest.isCustomer,'Y')='Y',1,0)),0)>=1 THEN 2 
		--							  WHEN  ISNULL(SUM(IIF(vwContractRequestLatest.isCustomer='N',1,0)),0)>0 THEN 1 
		--							  ELSE 0 
		--					END),0)  
		0ColorCode
		FROM vwContractRequestLatest 
		 INNER JOIN #AccessibleContractType act ON vwContractRequestLatest.ContractingTypeId=act.ContractTypeId
		LEFT OUTER JOIN MstCountry 
		ON MstCountry.CountryId=vwContractRequestLatest.CountryId 
		WHERE
		EffectiveDate IS NOT NULL AND vwContractRequestLatest.ContractStatusID=9
		AND ISNULL(ExpirationDate,dbo.DateOnly(GETDATE())) >= dbo.DateOnly(GETDATE()) AND
		EXISTS ( SELECT 1 FROM vwRequestAccess uA WHERE uA.RequestId = vwContractRequestLatest.RequestId AND uA.UsersId = @UserId)		
		AND vwContractRequestLatest.EffectiveDate BETWEEN (SELECT CASE WHEN @Tenure='1' THEN DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()), 0) WHEN @Tenure='3' THEN DATEADD(year,-3,GETDATE()) ELSE DATEADD(year,-5,GETDATE()) END ) AND GETDATE()		
		----AND vwContractRequestLatest.ContractTypeId IN(SELECT items FROM dbo.Split((CASE WHEN @UserRole='Super Admin' THEN CONVERT(VARCHAR(10),vwContractRequestLatest.ContractTypeId)
		----ELSE dbo.GetContractTypeAccess(@UserRole) END),','))
		GROUP BY MstCountry.CountryName,MstCountry.CountryCode,MstCountry.CountryId,vwContractRequestLatest.CurrencyID
	) tbl RIGHT OUTER JOIN MstCountry Con ON con.CountryId=tbl.CountryId

	GROUP BY Con.[CountryCode],Con.[CountryName],Con.[CountryId], tbl.ColorCode
	ORDER BY CountryName
END








/*
SELECT dbo.GetRequestAddress(90939)
*/

GO


ALTER FUNCTION [dbo].[GetRequestAddress]
(
	@RequestId int=0	
)
RETURNS  VARCHAR(8000)
AS
BEGIN
DECLARE @Result VARCHAR(8000) 

		SELECT @Result = IIF(ContractRequest.Address IS NULL OR ContractRequest.Address='' ,'',ContractRequest.Address+'<br/>') + 
											IIF(ContractRequest.Street2 IS 	NULL OR ContractRequest.Street2=''  ,'',ContractRequest.Street2++'<br/>')+
											IIF(ContractRequest.CityName IS NULL OR ContractRequest.CityName='' ,'',ContractRequest.CityName+'<br/>')+
											IIF(ContractRequest.StateName IS NULL OR ContractRequest.StateName='' ,'',ContractRequest.StateName+'<br/>')+
											IIF(ContractRequest.PinCode IS NULL OR ContractRequest.PinCode='' ,'',ContractRequest.PinCode+'<br/>')+											
											ISNULL((SELECT DISTINCT ''+CountryName FROM MstCountry WHERE CountryId=ContractRequest.CountryId),'') 
											FROM ContractRequest
											INNER JOIN MstCountry mc ON mc.CountryId=ContractRequest.CountryId		
											WHERE ContractRequest.RequestId=@RequestId

RETURN @Result
END

GO


ALTER FUNCTION [dbo].[GetSearchTextFoundIn]  
(
@SearchText VARCHAR(50), @RequestId INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN

DECLARE  @FoundIn VARCHAR(MAX)=NULL
;WITH CTE AS(
		SELECT ContractRequest.RequestId, 'Request Form' FoundIn
		FROM ContractRequest
		LEFT OUTER JOIN Client ON Client.ClientId = ContractRequest.ClientId
		LEFT OUTER JOIN mstContractingParty ON mstContractingParty.ContractingPartyId = ContractRequest.ContractingTypeId
		LEFT OUTER JOIN MstCurrency ON MstCurrency.CurrencyID=ContractRequest.CurrencyID
		
		WHERE ContractRequest.RequestId=@RequestId 
		AND (
		CAST(ContractRequest.RequestId AS VARCHAR(30)) LIKE '%'+ @SearchText+'%' 		
		OR CAST(ContractRequest.ContractID AS VARCHAR(30)) LIKE '%'+ @SearchText+'%' 
		OR DBO.DATEFORMAT(ContractRequest.Addedon)  LIKE '%'+ @SearchText+'%' 
		OR ContractRequest.Address  LIKE '%'+ @SearchText+'%' 
		OR ContractRequest.ContactNumber  LIKE '%'+ @SearchText+'%' 
		OR ContractRequest.ContractTerm  LIKE '%'+ @SearchText+'%' 
		OR ContractRequest.RequestDescription  LIKE '%'+ @SearchText+'%' 
		OR DBO.DATEFORMAT(ContractRequest.DeadlineDate)   LIKE '%'+ @SearchText+'%' 
		OR ContractRequest.EmailID  LIKE '%'+ @SearchText+'%' 
		OR ContractRequest.EstimatedValue  LIKE '%'+ @SearchText+'%' 
		OR DBO.[GetUserIdByEmpName](ContractRequest.AddedBy ) LIKE '%'+ @SearchText+'%' 		
		OR CAST(ContractRequest.ContractValue AS VARCHAR(50)) LIKE '%'+ @SearchText+'%' 
		OR ContractRequest.ContractValue  LIKE '%'+ @SearchText+'%' 
		OR ContractRequest.ClientName  LIKE '%'+ @SearchText+'%'  
		OR ContractRequest.EmailID  LIKE '%'+ @SearchText+'%'  
		OR ContractRequest.ContactNumber  LIKE '%'+ @SearchText+'%'
		OR ContractRequest.ContractValue  LIKE '%'+ @SearchText+'%'
		OR mstContractingParty.ContractingPartyName  LIKE '%'+ @SearchText+'%'
		OR DBO.GetRequestStatus(ContractRequest.RequestId) LIKE '%'+ @SearchText+'%'
		OR DBO.GetUserFullName(ContractRequest.AssignToUserID) LIKE '%'+ @SearchText+'%'
		OR ContractRequest.ClientName LIKE '%'+ @SearchText+'%'
		OR [dbo].[GetRequestFormMetaDataField](ContractRequest.RequestId, 'Request Form')  LIKE '%'+ @SearchText+'%'
		OR MstCurrency.CurrencyCode LIKE '%'+ @SearchText+'%'
		)

		UNION ALL
		SELECT RequestId, 'Important Dates' FoundIn  
		FROM ContractRequest 
		WHERE RequestId=@RequestId 
		AND (
		DBO.DATEFORMAT(ContractRequest.ExpirationDate)  LIKE '%'+ @SearchText+'%'
		OR DBO.DATEFORMAT(ContractRequest.EffectiveDate)  LIKE '%'+ @SearchText+'%' 
		OR DBO.DATEFORMAT(ContractRequest.RenewalDate)  LIKE '%'+ @SearchText+'%' 
		OR DBO.GetUserFullName(ContractRequest.ExpirationAlert30daysUserID1) LIKE  '%'+ @SearchText+'%' 
		OR DBO.GetUserFullName(ContractRequest.ExpirationAlert30daysUserID2) LIKE  '%'+ @SearchText+'%' 
		OR DBO.GetUserFullName(ContractRequest.ExpirationAlert7daysUserID1) LIKE  '%'+ @SearchText+'%' 
		OR DBO.GetUserFullName(ContractRequest.ExpirationAlert7daysUserID2) LIKE  '%'+ @SearchText+'%' 
		OR DBO.GetUserFullName(ContractRequest.ExpirationAlert24HrsUserID1) LIKE  '%'+ @SearchText+'%' 
		OR DBO.GetUserFullName(ContractRequest.ExpirationAlert24HrsUserID2) LIKE  '%'+ @SearchText+'%' 
		OR DBO.GetUserFullName(ContractRequest.RenewalAlert30daysUserID1) LIKE  '%'+ @SearchText+'%' 
		OR DBO.GetUserFullName(ContractRequest.RenewalAlert30daysUserID2) LIKE  '%'+ @SearchText+'%' 
		OR DBO.GetUserFullName(ContractRequest.RenewalAlert7daysUserID1) LIKE  '%'+ @SearchText+'%' 
		OR DBO.GetUserFullName(ContractRequest.RenewalAlert7daysUserID2) LIKE  '%'+ @SearchText+'%' 
		OR DBO.GetUserFullName(ContractRequest.RenewalAlert24HrsUserID1) LIKE  '%'+ @SearchText+'%' 
		OR DBO.GetUserFullName(ContractRequest.RenewalAlert24HrsUserID2) LIKE  '%'+ @SearchText+'%'
		OR [dbo].[GetRequestFormMetaDataField](ContractRequest.RequestId, 'Important Dates')  LIKE '%'+ @SearchText+'%'
		)

		UNION ALL
		SELECT RequestId, 'Key Obligations' FoundIn  
		FROM ContractRequest 
		WHERE RequestId = @RequestId 
		AND (
				[dbo].[GetRequestFormMetaDataField](ContractRequest.RequestId, 'Key Obligations')  LIKE '%'+ @SearchText+'%'
				OR ContractRequest.LiabilityCap LIKE '%'+ @SearchText+'%'
				OR ContractRequest.Indemnity LIKE '%'+ @SearchText+'%'
				OR ContractRequest.Strictliability LIKE '%'+ @SearchText+'%'
				OR ContractRequest.Insurance LIKE '%'+ @SearchText+'%'
				OR ContractRequest.TerminationDescription LIKE '%'+ @SearchText+'%'
				OR ContractRequest.AssignmentNovation  LIKE '%'+ @SearchText+'%'
				OR ContractRequest.Others LIKE '%'+ @SearchText+'%'
		)

		UNION ALL
		SELECT RequestId, 'Contract Type' FoundIn  
		FROM ContractRequest 
		INNER JOIN MstContractType ON  ContractRequest.ContractTypeId=MstContractType.ContractTypeId 		
		WHERE RequestId=@RequestId 
		AND (
		ContractTypeName LIKE '%'+ @SearchText+'%'
		)
		
		UNION ALL	
		SELECT RequestId, 'Signaure Date' FoundIn
		FROM vwContractRequestLatest
		WHERE RequestId=@RequestId 
		AND (
		SignatureDate LIKE '%'+ @SearchText+'%'
		)

		UNION ALL
		SELECT RequestId, 'Vendor' FoundIn  
		FROM ContractRequest INNER JOIN  Client ON ContractRequest.ClientId = Client.ClientId
		LEFT OUTER  JOIN ClientAddressDetails ON Client.ClientId = ClientAddressDetails.ClientId
		LEFT OUTER  JOIN MstCountry CM ON ClientAddressDetails.CountryId = CM.CountryId		
		WHERE RequestId=@RequestId 
		AND (
		--ClientName LIKE '%'+ @SearchText+'%'
			ClientAddressDetails.PinCode LIKE '%'+ @SearchText+'%'
		OR ClientAddressDetails.Street2 LIKE '%'+ @SearchText+'%'
		OR ClientAddressDetails.CityName LIKE '%'+ @SearchText+'%'
		OR ClientAddressDetails.[Address] LIKE '%'+ @SearchText+'%'
		OR CM.CountryName LIKE '%'+ @SearchText+'%'
		OR ClientAddressDetails.StateName LIKE '%'+ @SearchText+'%'
		)

		UNION ALL
		SELECT RequestId, ' Activities and Notes' FoundIn  
		FROM Activity 
		WHERE RequestId=@RequestId 
		AND (
		ActivityText LIKE '%'+ @SearchText+'%'
		)

		UNION ALL
		SELECT RequestId, 'Questionnaire' FoundIn 
		FROM ContractQuestionsAnswers qA 
		LEFT OUTER JOIN MstCountry ON qA.ForeignKeyValye IS NOT NULL AND  CAST(MstCountry.CountryId AS VARCHAR(50)) IN (SELECT ITEMS FROM DBO.SPLIT(qA.ForeignKeyValye,','))
		WHERE RequestId=@RequestId 
		AND (
		qA.Question  LIKE '%'+ @SearchText+'%'
		OR qA.TextValue  LIKE '%'+ @SearchText+'%'
		OR CAST(qA.NumericValue AS VARCHAR(50))  LIKE '%'+ @SearchText+'%'
		OR CAST(qA.DecimalValue AS VARCHAR(50))  LIKE '%'+ @SearchText+'%'
		OR dbo.DateFormat (qA.DateValue)  LIKE '%'+ @SearchText+'%'
		OR (qA.ForeignKeyValye IS NOT NULL AND  MstCountry.CountryName LIKE '%'+  @SearchText +'%')
		)
		)--SELECT * FROM CTE
		SELECT @FoundIn=COALESCE(@FoundIn+', ',' ')+ FoundIn FROM CTE
		WHERE RequestId=@RequestId
		GROUP BY RequestId,FoundIn

		RETURN @FoundIn
END

		GO


		ALTER FUNCTION [dbo].[GetUserRemainingCount]
		(
			
		) 
		
		RETURNS INT
		AS
		BEGIN
			DECLARE @Result INT = 0
			
			SET @Result =
			(
			SELECT TOP(1) ISNULL(Subscriptions.NumberOfUsers, 0) NumberOfUsers
			FROM Subscribers
			INNER JOIN Subscriptions ON Subscriptions.SubscriberID = Subscribers.SubscriberID
			AND Subscriptions.SubscriptionID = (SELECT MAX (SubscriptionID) FROM Subscriptions WHERE SubscriberID =Subscribers.SubscriberID)
			WHERE DBName = DB_NAME() 
			ORDER BY Subscribers.SubscriberID DESC
			)
			-
			(			
			SELECT ISNULL(COUNT(1),0) FROM MstUsers WHERE IsActive= 'Y' OR IsActive IS NULL
			)

			RETURN @Result
		END


GO




/*
EXEC CustomerActiveContracts_Report
*/

ALTER PROCEDURE [dbo].[Master_Reports]
@Flags INT = 0 ,
	@PageNo INT = 0 ,   
	@RecordsPerPage INT = 0,
	@FromDate DATETIME=NULL,
	@ToDate DATETIME=NULL,
	@ContractTypeId INT = NULL,
	@Search VARCHAR(50)='',
	@SortColumn VARCHAR(50)='',
	@Direction INT = 0,
	@UserID INT=NULL
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @TmpContractType TABLE (ContractTypeId INT)
	IF @ContractTypeId IS NULL
		BEGIN
		INSERT INTO @TmpContractType (ContractTypeId) 
		SELECT ContractTypeId FROM MstContractType
		END
	ELSE
	BEGIN
		INSERT INTO @TmpContractType (ContractTypeId) 
		SELECT @ContractTypeId
	END

		DECLARE @Role VARCHAR(50)
		IF @UserID IS NULL
		SET @Role='Super Admin'
	ELSE
		SET @Role=(SELECT MstRole.RoleName FROM MstUsers INNER JOIN MstRole ON MstUsers.RoleId=MstRole.RoleId AND MstUsers.UsersId=@UserID)


	IF @FromDate IS NULL SET @FromDate = (SELECT MIN(Addedon) FROM ContractRequest )
	IF @ToDate IS NULL SET @ToDate = GETDATE()
	IF @PageNo <= 0 SET @PageNo = 1
	IF @RecordsPerPage = 0 SET @RecordsPerPage = (SELECT COUNT(1) FROM ContractRequest )

	BEGIN
		
		-- Call sp for metadata configuration columns with passing request id.
		EXEC [rptMetaDataConfigurColumns] 0

		SELECT count(*) OVER()  AS Maxcount ,
		cR.RequestId [Request ID],
		cR.ContractID [Contract ID] 
		,RN.FullName [Request By]
		,ct.ContractTypeName [Contract Type]
		,cp.ContractingPartyName [Contracting Party]
		,(CASE WHEN cR.isCustomer='Y' THEN 'Customer' WHEN cR.isCustomer='N' THEN 'Supplier' ELSE 'Other' END) [Customer/supplier/others]
		,cR.ClientName [Customer/supplier/others Name] 
		,co.CountryName [Country Name]
		,cR.StateName [State Name]
		,cR.CityName [City Name]
		,cR.PinCode [Post Code]
		--,cR.Address [Address]
		,cR.ContactNumber [Contact Number]
		,cR.EmailID [Email Id]
		--,RN.FullName [Requester Name] 
		--,cR.ContractValue [Contract Value]
		,CONVERT(VARCHAR(200), Format(cr.ContractValue ,'N','en-US' ))+' ('+cu.CurrencyCode+')' [Contract Value]
		,de.DepartmentName [Assign Department]
		,us.FullName [Assign User] 
		,cte.ContractTemplateName [Contract Template]
		,cR.RequestDescription [Request Description]
		--,(CASE WHEN cR.IsApprovalRequired='Y' THEN 'Yes' ELSE 'No' END) [Is Approval Required]
		,cR.EstimatedValue [Estimated Value]
		,(CASE WHEN cR.IsBulkImport='Y' THEN 'Yes' ELSE 'No' END) [Is Bulk Import]
		,(CASE WHEN cR.IsDocSignCompleted='Y' THEN 'Yes' ELSE 'No' END) [Is Doc Sign Completed]
		,cs.StatusName [Status]
		,dbo.DateFormat(cR.EffectiveDate) [Effective Date],
		dbo.DateFormat(cr.ExpirationDate) [Expiration Date]
		--,(CASE WHEN cr.ExpirationAlert7days='Y' THEN '7,' else ' ' END  +''+ CASE WHEN cr.ExpirationAlert30days='Y'  THEN '30,' ELSE ' ' END+''+CASE WHEN cr.ExpirationAlert24Hrs='Y'  THEN '1' ELSE ' ' END) [Expiration Alert days]
		,(CASE WHEN cr.ExpirationAlert24Hrs='Y'  THEN '1,' ELSE ' ' END
		+''+CASE WHEN cr.ExpirationAlert7days='Y' THEN '7,' else ' ' END  
		+''+ CASE WHEN cr.ExpirationAlert30days='Y'  THEN '30,' ELSE ' ' END
		+''+ CASE WHEN cr.ExpirationAlert60days='Y'  THEN '60,' ELSE ' ' END
		+''+ CASE WHEN cr.ExpirationAlert90days='Y'  THEN '90,' ELSE ' ' END
		+''+ CASE WHEN cr.ExpirationAlert180days='Y'  THEN '180' ELSE ' ' END
		) [Expiration Alert days]
		,dbo.DateFormat(cR.RenewalDate) [Renewal Date]
		--,(CASE WHEN cr.RenewalAlert7days='Y' THEN '7,' else ' ' END  +''+ CASE WHEN cr.RenewalAlert30days='Y'  THEN '30,' ELSE ' ' END+''+CASE WHEN cr.RenewalAlert24Hrs='Y'  THEN '1' ELSE ' ' END) [Renewal Alert days]
		,(CASE WHEN cr.RenewalAlert24Hrs='Y'  THEN '1,' ELSE ' ' END
		+''+CASE WHEN cr.RenewalAlert7days='Y' THEN '7,' else ' ' END  
		+''+ CASE WHEN cr.RenewalAlert30days='Y'  THEN '30,' ELSE ' ' END
		+''+ CASE WHEN cr.RenewalAlert60days='Y'  THEN '60,' ELSE ' ' END
		+''+ CASE WHEN cr.RenewalAlert90days='Y'  THEN '90,' ELSE ' ' END
		+''+ CASE WHEN cr.RenewalAlert180days='Y'  THEN '180' ELSE ' ' END	
		) [Renewal Alert days]
		,cR.LiabilityCap [Liability Cap]
		,cR.Indemnity [Indemnity]
		,(CASE WHEN cR.IsChangeofControl='Y' THEN 'Yes' WHEN cR.IsChangeofControl='N' THEN 'No' ELSE '' END) [Change Of Control]
		,(CASE WHEN cR.IsAgreementClause='Y' THEN 'Yes' WHEN cR.IsAgreementClause='N' THEN 'No' ELSE '' END) [Entire Agreement Clause]
		,(CASE WHEN cR.IsStrictliability='Y' THEN 'Yes' WHEN cR.IsStrictliability='N' THEN 'No' ELSE '' END) [Strictliability]
		,cR.Strictliability[Strict liability description]
		,(CASE WHEN cR.IsGuaranteeRequired='Y' THEN 'Yes' WHEN cR.IsGuaranteeRequired='N' THEN 'No' ELSE '' END) [Third Party Guarantee Required]
		,cr.Insurance [Insurance]
		,cR.TerminationDescription [Termination]
		--,cR.TerminationDescription [Termination Description]
		--,(CASE WHEN cR.IsTerminationmaterial='Y' THEN 'Yes' ELSE 'No' END) [Is Termination material]
		--,(CASE WHEN cR.IsTerminationinsolvency='Y' THEN 'Yes' ELSE 'No' END) [Is Termination in solvency]
		--,(CASE WHEN cR.IsTerminationinChangeofcontrol='Y' THEN 'Yes' ELSE 'No' END) [Is Termination in Change]
		--,(CASE WHEN cR.IsTerminationmaterial='Y' THEN 'Yes' ELSE 'No' END) [Is Termination material]
		--,(CASE WHEN cR.IsTerminationothercauses='Y' THEN 'Yes' ELSE 'No' END) [Is Termination causes]
		,cR.AssignmentNovation [Assignment Novation]
		,cR.Others [Others]
		,(CASE WHEN cR.Priority='Y' THEN 'Yes' ELSE 'No' END) [Priority Contract]
		,cR.PriorityReason [Priority Reason]
		--,cR.BulkImportStagingId
		--,bi.Head [Bulk Import]
		--,cR.ContractTerm [Contract Term]
		,rpt.*
		,rptD.*
		,rptK.*

		FROM  vwContractRequestLatest cR 
		INNER JOIN MstUsers ON MstUsers.UsersId = cR.Addedby
		LEFT OUTER JOIN MstContractType ct ON ct.ContractTypeId=cR.ContractTypeId
		LEFT OUTER JOIN ContractTemplate cte ON cte.ContractTemplateId=cR.ContractTemplateId
		LEFT OUTER JOIN MstCountry co ON co.CountryId=cR.CountryId
		LEFT OUTER JOIN MstDepartment de ON de.DepartmentId=cR.AssignToDepartmentId
		LEFT OUTER JOIN MstUsers us ON us.UsersId = cR.AssignToUserID
		LEFT OUTER JOIN MstUsers RN ON RN.UsersId = cR.Addedby
		LEFT OUTER JOIN vwContractStatus cs ON cs.ContractStatusID = cR.ContractStatusID
		LEFT OUTER JOIN mstContractingParty cp ON cp.ContractingPartyId = cR.ContractingTypeId
		LEFT OUTER JOIN BulkImportStaging bi ON bi.BulkImportStagingId=cR.BulkImportStagingId
		LEFT OUTER JOIN RptMetaDataDynamicTbl rpt ON rpt.RequestId=cr.RequestId
		LEFT OUTER JOIN RptMetaDataImportDateDynamicTbl rptD ON rptD.RequestId_ID=cr.RequestId
		LEFT OUTER JOIN RptMetaDataKeyObligationDynamicTbl rptK ON rptK.RequestId_KO=cr.RequestId
		LEFT OUTER JOIN MstCurrency cu on cu.CurrencyId=cR.CurrencyID

		WHERE cr.ContractTypeId IN (SELECT ContractTypeId FROM @TmpContractType ) 
		AND cr.ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),cr.ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),',')) 
		--AND cR.ContractStatusID = 9   
		--AND (cR.isCustomer IS NULL OR cR.isCustomer = 'Y')
		--AND Cr.Addedon BETWEEN @FromDate AND @ToDate
		--AND cr.ContractID IS NOT NULL
		--AND	( ClientName LIKE '%'+ @Search +'%'  )
		ORDER BY 
		CASE WHEN @SortColumn='' AND @Direction = 0  THEN cR.RequestId END DESC,
		CASE WHEN @SortColumn =  'Customer/supplier/others Name'  AND @Direction = 0 THEN cR.ClientName END ASC, 
		CASE WHEN @SortColumn =  'Requester Name'  AND @Direction = 0 THEN MstUsers.FullName  END ASC, 
		CASE WHEN @SortColumn =  'Contract ID'  AND @Direction = 0 THEN cR.ContractID END ASC, 
		CASE WHEN @SortColumn =  'Effective Date'  AND @Direction = 0 THEN cR.EffectiveDate  END ASC, 
		CASE WHEN @SortColumn =  'Expiration Date'  AND @Direction = 0 THEN cr.ExpirationDate END ASC,	
		CASE WHEN @SortColumn =  'Contract Type'  AND @Direction = 0 THEN ct.ContractTypeName END ASC,
		CASE WHEN @SortColumn =  'Contract Template'  AND @Direction = 0 THEN cte.ContractTemplateName END ASC,
		CASE WHEN @SortColumn =  'Client Name'  AND @Direction = 0 THEN cr.ClientName END ASC,
		CASE WHEN @SortColumn =  'Address'  AND @Direction = 0 THEN cr.Address END ASC,
		CASE WHEN @SortColumn =  'State Name'  AND @Direction = 0 THEN cr.StateName END ASC,
		CASE WHEN @SortColumn =  'City Name'  AND @Direction = 0 THEN cr.CityName END ASC,
		CASE WHEN @SortColumn =  'Country Name'  AND @Direction = 0 THEN co.CountryName END ASC,
		CASE WHEN @SortColumn =  'Pin Code'  AND @Direction = 0 THEN cr.PinCode END ASC,
		CASE WHEN @SortColumn =  'Request Description'  AND @Direction = 0 THEN cr.RequestDescription END ASC,
		CASE WHEN @SortColumn =  'Deadline Date'  AND @Direction = 0 THEN cr.DeadlineDate END ASC,
		CASE WHEN @SortColumn =  'Assign Department'  AND @Direction = 0 THEN de.DepartmentName END ASC,
		CASE WHEN @SortColumn =  'Assign User'  AND @Direction = 0 THEN us.FullName END ASC,
		CASE WHEN @SortColumn =  'Is Approval Required'  AND @Direction = 0 THEN cr.IsApprovalRequired END ASC,
		CASE WHEN @SortColumn =  'Estimated Value'  AND @Direction = 0 THEN cr.EstimatedValue END ASC,
		CASE WHEN @SortColumn =  'Contact Number'  AND @Direction = 0 THEN cr.ContactNumber END ASC,
		CASE WHEN @SortColumn =  'Email Id'  AND @Direction = 0 THEN cr.EmailID END ASC,
		CASE WHEN @SortColumn =  'Status'  AND @Direction = 0 THEN cs.StatusName END ASC,
		CASE WHEN @SortColumn =  'Liability Cap'  AND @Direction = 0 THEN cr.LiabilityCap END ASC,
		CASE WHEN @SortColumn =  'Indemnity'  AND @Direction = 0 THEN cr.Indemnity END ASC,
		CASE WHEN @SortColumn =  'Change Of Control'  AND @Direction = 0 THEN cr.IsChangeofControl END ASC,
		CASE WHEN @SortColumn =  'Entire Agreement Clause'  AND @Direction = 0 THEN cr.IsAgreementClause END ASC,
		CASE WHEN @SortColumn =  'Strictliability'  AND @Direction = 0 THEN cr.IsStrictliability END ASC,
		CASE WHEN @SortColumn =  'Strict liability description'  AND @Direction = 0 THEN cr.Strictliability END ASC,
		CASE WHEN @SortColumn =  'Third Party Guarantee Required'  AND @Direction = 0 THEN cr.IsGuaranteeRequired END ASC,
		CASE WHEN @SortColumn =  'Insurance'  AND @Direction = 0 THEN cr.Insurance END ASC,
		CASE WHEN @SortColumn =  'Termination'  AND @Direction = 0 THEN cr.IsTerminationConvenience END ASC,
		CASE WHEN @SortColumn =  'Termination Description'  AND @Direction = 0 THEN cr.TerminationDescription END ASC,
		CASE WHEN @SortColumn =  'Is Termination material'  AND @Direction = 0 THEN cr.IsTerminationmaterial END ASC,
		CASE WHEN @SortColumn =  'Is Termination in solvency'  AND @Direction = 0 THEN cr.IsTerminationinsolvency END ASC,
		CASE WHEN @SortColumn =  'Is Termination in Change'  AND @Direction = 0 THEN cr.IsTerminationinChangeofcontrol END ASC,
		CASE WHEN @SortColumn =  'Is Termination causes'  AND @Direction = 0 THEN cr.IsTerminationothercauses END ASC,
		CASE WHEN @SortColumn =  'Assignment Novation'  AND @Direction = 0 THEN cr.AssignmentNovation END ASC,
		CASE WHEN @SortColumn =  'Others'  AND @Direction = 0 THEN cr.Others END ASC,
		CASE WHEN @SortColumn =  'Contracting Party'  AND @Direction = 0 THEN cp.ContractingPartyName END ASC,
		CASE WHEN @SortColumn =  'Customer/supplier/others'  AND @Direction = 0 THEN cr.isCustomer END ASC,
		CASE WHEN @SortColumn =  'Street2'  AND @Direction = 0 THEN cr.Street2 END ASC,
		CASE WHEN @SortColumn =  'Bulk Import'  AND @Direction = 0 THEN bi.Head END ASC,
		CASE WHEN @SortColumn =  'Is Bulk Import'  AND @Direction = 0 THEN cr.IsBulkImport END ASC,
		CASE WHEN @SortColumn =  'Is Doc Sign Completed'  AND @Direction = 0 THEN cr.IsDocSignCompleted END ASC,
		CASE WHEN @SortColumn =  'Contract Term'  AND @Direction = 0 THEN cr.ContractTerm END ASC,
		CASE WHEN @SortColumn =  'Contract Value'  AND @Direction = 0 THEN cr.ContractValue END ASC,
		CASE WHEN @SortColumn =  'Request ID'  AND @Direction = 0 THEN cr.RequestId END ASC,
		CASE WHEN @SortColumn =  'Priority Contract'  AND @Direction = 0 THEN cr.Priority END ASC,
		CASE WHEN @SortColumn =  'Priority Reason'  AND @Direction = 0 THEN cR.PriorityReason END ASC,
				
		CASE WHEN @SortColumn =  'Customer/supplier/others Name'  AND @Direction = 1 THEN cR.ClientName END DESC, 
		CASE WHEN @SortColumn =  'Requester Name'  AND @Direction = 1 THEN MstUsers.FullName  END DESC, 
		CASE WHEN @SortColumn =  'Contract ID'  AND @Direction = 1 THEN cR.ContractID END DESC, 
		CASE WHEN @SortColumn =  'Effective Date'  AND @Direction = 1 THEN cR.EffectiveDate  END DESC, 
		CASE WHEN @SortColumn =  'Expiration Date'  AND @Direction = 1 THEN cr.ExpirationDate END DESC,
		CASE WHEN @SortColumn =  'Contract Type'  AND @Direction = 1 THEN ct.ContractTypeName END DESC,
		CASE WHEN @SortColumn =  'Contract Template'  AND @Direction = 1 THEN cte.ContractTemplateName END DESC,
		CASE WHEN @SortColumn =  'Client Name'  AND @Direction = 1 THEN cr.ClientName END DESC,
		CASE WHEN @SortColumn =  'Address'  AND @Direction = 1 THEN cr.Address END DESC,
		CASE WHEN @SortColumn =  'State Name'  AND @Direction = 1 THEN cr.StateName END DESC,
		CASE WHEN @SortColumn =  'City Name'  AND @Direction = 1 THEN cr.CityName END DESC,
		CASE WHEN @SortColumn =  'Country Name'  AND @Direction = 1 THEN co.CountryName END DESC,
		CASE WHEN @SortColumn =  'Pin Code'  AND @Direction = 1 THEN cr.PinCode END DESC,
		CASE WHEN @SortColumn =  'Request Description'  AND @Direction = 1 THEN cr.RequestDescription END DESC,
		CASE WHEN @SortColumn =  'Deadline Date'  AND @Direction = 1 THEN cr.DeadlineDate END DESC,
		CASE WHEN @SortColumn =  'Assign Department'  AND @Direction = 1 THEN de.DepartmentName END DESC,
		CASE WHEN @SortColumn =  'Assign User'  AND @Direction = 1 THEN us.FullName END DESC,
		CASE WHEN @SortColumn =  'Is Approval Required'  AND @Direction = 1 THEN cr.IsApprovalRequired END DESC,
		CASE WHEN @SortColumn =  'Estimated Value'  AND @Direction = 1 THEN cr.EstimatedValue END DESC,
		CASE WHEN @SortColumn =  'Contact Number'  AND @Direction = 1 THEN cr.ContactNumber END DESC,
		CASE WHEN @SortColumn =  'Email Id'  AND @Direction = 1 THEN cr.EmailID END DESC,
		CASE WHEN @SortColumn =  'Status'  AND @Direction = 1 THEN cs.StatusName END DESC,
		CASE WHEN @SortColumn =  'Liability Cap'  AND @Direction = 1 THEN cr.LiabilityCap END DESC,
		CASE WHEN @SortColumn =  'Indemnity'  AND @Direction = 1 THEN cr.Indemnity END DESC,
		CASE WHEN @SortColumn =  'Change Of Control'  AND @Direction = 1 THEN cr.IsChangeofControl END DESC,
		CASE WHEN @SortColumn =  'Entire Agreement Clause'  AND @Direction = 1 THEN cr.IsAgreementClause END DESC,
		CASE WHEN @SortColumn =  'Strictliability'  AND @Direction = 1 THEN cr.IsStrictliability END DESC,
		CASE WHEN @SortColumn =  'Strict liability description'  AND @Direction = 1 THEN cr.Strictliability END DESC,
		CASE WHEN @SortColumn =  'Third Party Guarantee Required'  AND @Direction = 1 THEN cr.IsGuaranteeRequired END DESC,
		CASE WHEN @SortColumn =  'Insurance'  AND @Direction = 1 THEN cr.Insurance END DESC,
		CASE WHEN @SortColumn =  'Termination'  AND @Direction = 1 THEN cr.IsTerminationConvenience END DESC,
		CASE WHEN @SortColumn =  'Termination Description'  AND @Direction = 1 THEN cr.TerminationDescription END DESC,
		CASE WHEN @SortColumn =  'Is Termination material'  AND @Direction = 1 THEN cr.IsTerminationmaterial END DESC,
		CASE WHEN @SortColumn =  'Is Termination in solvency'  AND @Direction = 1 THEN cr.IsTerminationinsolvency END DESC,
		CASE WHEN @SortColumn =  'Is Termination in Change'  AND @Direction = 1 THEN cr.IsTerminationinChangeofcontrol END DESC,
		CASE WHEN @SortColumn =  'Is Termination causes'  AND @Direction = 1 THEN cr.IsTerminationothercauses END DESC,
		CASE WHEN @SortColumn =  'Assignment Novation'  AND @Direction = 1 THEN cr.AssignmentNovation END DESC,
		CASE WHEN @SortColumn =  'Others'  AND @Direction = 1 THEN cr.Others END DESC,
		CASE WHEN @SortColumn =  'Contracting Party'  AND @Direction = 1 THEN cp.ContractingPartyName END DESC,
		CASE WHEN @SortColumn =  'Customer/supplier/others'  AND @Direction = 1 THEN cr.isCustomer END DESC,
		CASE WHEN @SortColumn =  'Street2'  AND @Direction = 1 THEN cr.Street2 END DESC,
		CASE WHEN @SortColumn =  'Bulk Import'  AND @Direction = 1 THEN bi.Head END DESC,
		CASE WHEN @SortColumn =  'Is Bulk Import'  AND @Direction = 1 THEN cr.IsBulkImport END DESC,
		CASE WHEN @SortColumn =  'Is Doc Sign Completed'  AND @Direction = 1 THEN cr.IsDocSignCompleted END DESC,
		CASE WHEN @SortColumn =  'Contract Term'  AND @Direction = 1 THEN cr.ContractTerm END DESC,
		CASE WHEN @SortColumn =  'Contract Value'  AND @Direction = 1 THEN cr.ContractValue END DESC,
		CASE WHEN @SortColumn =  'Request ID'  AND @Direction = 1 THEN cr.RequestId END DESC,
		CASE WHEN @SortColumn =  'Priority Contract'  AND @Direction = 1 THEN cr.Priority END DESC,
		CASE WHEN @SortColumn =  'Priority Reason'  AND @Direction = 1 THEN cR.PriorityReason END DESC

		OFFSET ( @PageNo - 1 ) * @RecordsPerPage ROWS
		FETCH NEXT @RecordsPerPage ROWS ONLY
		IF @Flags=0
	  BEGIN
		SELECT ISNULL(COUNT(1),0)TotalRecords  FROM vwContractRequestLatest 
		WHERE ContractTypeId IN (SELECT ContractTypeId FROM @TmpContractType )  
		AND ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),',')) 
		--AND ContractStatusID = 9 
		--AND (isCustomer IS NULL OR isCustomer = 'Y') AND ContractID IS NOT NULL
		--AND Addedon BETWEEN @FromDate AND @ToDate
		--AND ( ClientName LIKE '%'+ @Search +'%'  )

	END
	END

END


















GO

ALTER PROCEDURE [dbo].[MasterCityAddUpdate]
@CityId INT=0,
@CityName VARCHAR(50),
@StateId INT,
@CountryId INT,
@isActive CHAR(1)='Y',
@AddedBy VARCHAR(50),
@ModifiedBy VARCHAR(50),
@IpAddress VARCHAR(50),
@Description VARCHAR(100),
@Status INT OUT

AS
BEGIN
	IF @CityId =0
	BEGIN
		IF NOT EXISTS (SELECT CityName FROM MstCity WHERE CityName = @CityName)
		BEGIN
			INSERT INTO MstCity (CityName, isActive, Addedby, Addedon, IP, [Description],StateId, CountryId) 
			VALUES (@CityName, @isActive, @AddedBy, GETDATE(), @IpAddress, @Description, @StateId, @CountryId)
			SET @Status = 1
		END
		ELSE
		BEGIN
			SET @Status = 2
		END
	END
	ELSE
	BEGIN
		IF NOT EXISTS (SELECT CityName FROM MstCity WHERE CityName = @CityName AND CityId <> @CityId )
		BEGIN
			UPDATE MstCity 
			SET CityName = @CityName, 
			isActive = @isActive,
			Modifiedby = @ModifiedBy,
			Modifiedon = GETDATE(),
			[Description] =  @Description,
			StateId = @StateId,
			CountryId = @CountryId,
			IP = @IpAddress
			WHERE CityId = @CityId
			SET @Status = 1
		END
			ELSE
			BEGIN
				SET @Status = 2
			END
	END

END

GO


ALTER PROCEDURE [dbo].[MasterCityChangeIsActive]
@CityIds VARCHAR(2000)=NULL,
@isActive CHAR(1)='Y'

AS
BEGIN

	UPDATE MstCity SET isActive = @isActive 
	WHERE CityId IN (SELECT ITEMS COLLATE SQL_Latin1_General_CP1_CI_AS FROM dbo.Split(@CityIds,','))  

END

 



 GO

ALTER PROCEDURE [dbo].[MasterCityDelete]
@CityIds VARCHAR(2000)=NULL

AS
BEGIN

	DELETE MstCity WHERE CityId IN (SELECT ITEMS COLLATE SQL_Latin1_General_CP1_CI_AS FROM dbo.Split(@CityIds,',')) 

END




/*



exec MasterCityRead 1,1,25,'p'



*/
GO



ALTER PROCEDURE [dbo].[MasterCityRead]
	@CityId INT = 0,
	@PageNo INT = 1 ,   
	@RecordsPerPage INT = 25,
	@Search VARCHAR(100)= '',
	@SortColumn VARCHAR(20)='',
	@Direction INT = 0

AS
BEGIN

	SET NOCOUNT ON;
	
	IF @PageNo = 0 SET @PageNo = 1
	IF @RecordsPerPage = 0 SET @RecordsPerPage = 25


	IF @CityId > 0 
		BEGIN
			SELECT  CityId , CityName, MstCity.isActive,'Y'isUsed, MstCity.[Description],
			MstCity.CountryId, MstCity.StateId,
			CASE WHEN MstCountry.isActive='N' THEN MstCountry.CountryName ELSE '' END CountryName,
			CASE WHEN MstState.isActive='N' THEN MstState.StateName ELSE '' END StateName
			FROM MstCity
			INNER JOIN MstState ON MstState.StateId = MstCity.StateId
			INNER JOIN MstCountry ON MstCountry.CountryId = MstState.CountryId
			  
			WHERE CityId = @CityId


		END
	ELSE
	BEGIN
		IF @PageNo < 1
		BEGIN
			SET @PageNo = 1
		END
		
		SELECT CityId, CityName,
		CASE WHEN MstCity.isActive='N' THEN 'Inactive' ELSE 'Active' END isActive, 
		COALESCE((
			SELECT MAX('Y') FROM MstUsers WHERE MstUsers.CityId = MstCity.CityId
			), 'N') isUsed ,
		MstCity.[Description],
		MstCity.CountryId, MstCity.StateId, MstCountry.CountryName, MstState.StateName
		FROM MstCity
		INNER JOIN MstCountry ON MstCountry.CountryId = MstCity.CountryId  
		INNER JOIN MstState ON MstState.StateId = MstCity.StateId
		WHERE ( CityName LIKE '%'+ @Search +'%' 
		OR MstCountry.CountryName LIKE '%'+ @Search +'%' 
		OR MstState.StateName LIKE '%'+ @Search +'%' 
		)
		ORDER BY 
		CASE WHEN @SortColumn = ''  AND @Direction = 0 THEN CityId END DESC, 
		CASE WHEN @SortColumn =  'City Name'  AND @Direction = 0 THEN CityName END ASC, 
		CASE WHEN @SortColumn =  'State Name'  AND @Direction = 0 THEN MstState.StateName END ASC, 
		CASE WHEN @SortColumn =  'Country Name'  AND @Direction = 0 THEN MstCountry.CountryName END ASC, 
		CASE WHEN @SortColumn =  'Description'  AND @Direction = 0 THEN MstCity.[Description]  END ASC, 
		CASE	WHEN @SortColumn =  'Status'  AND @Direction = 0 THEN MstCity.isActive END ASC, 
			
		CASE WHEN @SortColumn =  'City Name'  AND @Direction = 1 THEN CityName END DESC, 
		CASE	WHEN @SortColumn =  'State Name'  AND @Direction = 1 THEN MstState.StateName END DESC, 
		CASE	WHEN @SortColumn =  'Country Name'  AND @Direction = 1 THEN MstCountry.CountryName END DESC, 
		CASE WHEN @SortColumn =  'Description'  AND @Direction = 1 THEN MstCity.[Description]  END DESC, 
		CASE	WHEN @SortColumn =  'Status'  AND @Direction = 1 THEN MstCity.isActive END DESC 

		OFFSET ( @PageNo - 1 ) * @RecordsPerPage ROWS
		FETCH NEXT @RecordsPerPage ROWS ONLY

		SELECT ISNULL(COUNT(1),0)TotalRecords  
		FROM MstCity
		INNER JOIN MstCountry ON MstCountry.CountryId = MstCity.CountryId  
		INNER JOIN MstState ON MstState.StateId = MstCity.StateId
		WHERE ( CityName LIKE '%'+ @Search +'%' 
		OR MstCountry.CountryName LIKE '%'+ @Search +'%' 
		OR MstState.StateName LIKE '%'+ @Search +'%' 
		)
		
	END
	



	--BEGIN CATCH
	--	THROW
	----DECLARE 
	----@ErrorNumber INT = ERROR_NUMBER(), @ErrorSeverity VARCHAR(200) = ERROR_SEVERITY(), 
	----@ErrorState VARCHAR(1000) = ERROR_STATE(), @ErrorProcedure VARCHAR(100)= ERROR_PROCEDURE(), 
	----@ErrorLine INT = ERROR_LINE(), @ErrorMessage VARCHAR(1000)= ERROR_MESSAGE()
	----RAISERROR('Can not read city data', @ErrorSeverity, 1)
	--END  CATCH


END



GO

ALTER PROCEDURE [dbo].[MasterCitySelect] 

AS
BEGIN
	SELECT CityId,CityName FROM MstCity WHERE isActive='Y' ORDER BY CityName
END




GO



ALTER PROCEDURE [dbo].[MasterCitySelect_tmp] 

AS
BEGIN
	SELECT CityId,CityName FROM MstCity  ORDER BY CityName
END








GO



ALTER PROCEDURE [dbo].[MasterClientSelect] 
 @searchtxt varchar(MAX)=NULL
AS
BEGIN
	--SELECT 
	--ClientId,
	--ClientName,
	--Address,
	--CityName,
	--StateName,
	--CountryId,
	--PinCode 
	--FROM Client WHERE ClientName like '%' + @searchtxt +'%';



	SELECT DISTINCT
    ClientId,
	Address Address1,
	CityName,
	StateName,
	CountryId,
	PinCode,
	CONVERT(VARCHAR,ClientID)+'~'+
	'&nbsp;'+Address+IIF(Street2 IS NULL,'',','+Street2)+'<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+(SELECT DISTINCT CityName FROM MstCity WHERE CityName=vwClient.CityName)+'-'+CONVERT(VARCHAR,PinCode)+','+StateName+','+
	(SELECT DISTINCT CountryName FROM MstCountry WHERE CountryId=vwClient.CountryId) 
	 +'~'+
	CONVERT(VARCHAR,CityName)+'~'+
	CONVERT(VARCHAR,StateName)+'~'+
	CONVERT(VARCHAR,CountryID)+'~'+
	CONVERT(VARCHAR,PinCode)+'~'+
	ClientName+'~'+
	ContactNumber+'~'+EmailID+'~'+CONVERT(VARCHAR,ClientAddressdetailId)+'#'+isPrimaryAddress+'~'+CONVERT(VARCHAR,ClientContactdetailId)+'#'+isPrimaryContact+'~'+CONVERT(VARCHAR,ClientEmaildetailId)+'#'+isPrimaryEmail+'~'+ISNULL(isCustomer,'Y') AS Address, 
    ClientName ClientName
    FROM vwClient


END











GO



ALTER PROCEDURE [dbo].[MasterClientSelectWithContractID] 
 @searchtxt varchar(MAX)=NULL
AS
BEGIN
	--SELECT 
	--ClientId,
	--ClientName,
	--Address,
	--CityName,
	--StateName,
	--CountryId,
	--PinCode 
	--FROM Client WHERE ClientName like '%' + @searchtxt +'%';



	SELECT DISTINCT
    ClientId,
	Address Address1,
	CityName,
	StateName,
	CountryId,
	PinCode,
	CONVERT(VARCHAR,ClientID)+'~'+
	'&nbsp;'+Address+IIF(Street2='','',','+Street2)+'<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+(SELECT DISTINCT CityName FROM MstCity WHERE CityName=vwClientAndContract.CityName)+'-'+CONVERT(VARCHAR,PinCode)+','+StateName+','+
	(SELECT DISTINCT CountryName FROM MstCountry WHERE CountryId=vwClientAndContract.CountryId) 
	 +'~'+
	CONVERT(VARCHAR,CityName)+'~'+
	CONVERT(VARCHAR,StateName)+'~'+
	CONVERT(VARCHAR,CountryID)+'~'+
	CONVERT(VARCHAR,PinCode)+'~'+
	ClientName+'~'+
	ContactNumber+'~'+EmailID+'~'+CONVERT(VARCHAR,ClientAddressdetailId)+'#'+isPrimaryAddress+'~'+CONVERT(VARCHAR,ClientContactdetailId)+'#'+isPrimaryContact+'~'+CONVERT(VARCHAR,ClientEmaildetailId)+'#'+isPrimaryEmail+'~'+ISNULL(isCustomer,'Y')+'~'+ISNULL(CONVERT(VARCHAR,ContractTypeId),'')+'~'+ ISNULL(CONVERT(VARCHAR,ContractingTypeId),'')+'~'+ ISNULL(ContractTerm,'')+'~'+ ISNULL(CONVERT(VARCHAR,ContractValue),'') AS Address, 	 
	
    ClientName ClientName
    FROM vwClientAndContract 


END







GO

/*
exec MasterCommonSelect 'MstContractType',@StageId=96,@UserId=1083
*/

ALTER PROCEDURE [dbo].[MasterCommonSelect] 
@TableName VARCHAR(100)='',
@PartentId INT = 0,
@Search VARCHAR(10)= '',
@StageId INT = 0 ,
@RequestId INT = 0,
@FieldID INT=0,
@UserId INT=0
AS
BEGIN
	

	/* This section is only for pulling City table*/

DECLARE @Role VARCHAR(50)
SET @Role=(SELECT MstRole.RoleName FROM MstUsers INNER JOIN MstRole ON MstUsers.RoleId=MstRole.RoleId AND MstUsers.UsersId=@UserId)


	DECLARE @FlgNameDesc INT = 0

	DECLARE @tmpCityMasterIds TABLE (Id INT)
	IF @TableName = 'MstCity' AND @StageId > 0 
	BEGIN
		INSERT INTO @tmpCityMasterIds (Id)
		SELECT DISTINCT ForeignKeyValue from vwPreQualification WHERE StageId = @StageId AND PicklistObject = 'MstCity'
		SET @FlgNameDesc = 1
	END
	ELSE IF @TableName = 'MstCity' AND @RequestId<>0
	BEGIN
		INSERT INTO @tmpCityMasterIds (Id)
					SELECT DISTINCT ContractQuestionsAnswers.ForeignKeyValye   FROM ContractQuestionsAnswers 
					INNER JOIN FieldLibrary ON ContractQuestionsAnswers.FieldLibraryID=FieldLibrary.FieldLibraryID
					INNER JOIN FieldLibraryValues ON FieldLibraryValues.FieldLibraryID=ContractQuestionsAnswers.FieldLibraryID
					WHERE ContractQuestionsAnswers.RequestId=@RequestId AND FieldLibrary.FieldTypeID=9 AND FieldLibraryValues.FieldValue='1'
					AND ContractQuestionsAnswers.ForeignKeyValye IS NOT NULL
				    SET @FlgNameDesc = 1
	END

	IF @TableName = 'MstCity' AND (@PartentId IS NULL OR @PartentId = 0) SET @FlgNameDesc = 1  
	/* This section is only for pulling City table*/


	IF @TableName='MstCurrency'
	BEGIN
		SELECT CurrencyId Id,CurrencyCode Name FROM MstCurrency WHERE isActive='Y'

		UNION

		SELECT StageConditions.ForeignKeyValue Id, MstCurrency.CurrencyCode Name
		FROM StageConfigurator
		INNER JOIN StageConditions ON StageConfigurator.StageID=StageConditions.StageID
		INNER JOIN MstCurrency ON MstCurrency.CurrencyId IN(StageConditions.ForeignKeyValue)
		WHERE StageConditions.StageID=@StageId AND StageConditions.FieldLibraryID=100009
	END
	--- added by js
	IF @TableName='MetaDataConfiguratorValues'
	BEGIN
		select FieldOptionId Id,FieldOptionValue Name from MetaDataConfiguratorValues WHERE FieldId=@FieldID
	END
	--- added by js

	---- Added By kk on 12 May 2016
	IF @TableName='Client'
	BEGIN
		SELECT ClientId Id,ClientName Name FROM Client --order by ClientName ASC  --WHERE isActive='Y'
		UNION
		SELECT StageConditions.ForeignKeyValue Id, Client.ClientName Name
		FROM StageConfigurator
		INNER JOIN StageConditions ON StageConfigurator.StageID=StageConditions.StageID
		INNER JOIN Client ON Client.ClientId IN(StageConditions.ForeignKeyValue)
		WHERE StageConditions.StageID=@StageId AND StageConditions.FieldLibraryID=100003

	END

	IF @TableName='MstTerritory'
	BEGIN
		--SELECT CountryId Id,CountryName Name FROM CountryMst WHERE isActive='Y'
		SELECT CountryId Id,CountryName Name FROM MstCountry WHERE isActive='Y' --ORDER BY CountryName
		UNION
		SELECT StageConditions.ForeignKeyValue Id, MstCountry.CountryName Name
		FROM StageConfigurator
		INNER JOIN StageConditions ON StageConfigurator.StageID=StageConditions.StageID
		INNER JOIN  MstCountry ON  MstCountry.CountryId IN(StageConditions.ForeignKeyValue)
		WHERE StageConditions.StageID=@StageId
	END

	IF @TableName='MstSeason'
	BEGIN
		SELECT DISTINCT 1 AS Id,season AS Name FROM dbo.GetSeasonDateRange(0)
	END

	IF @TableName='mstContractingParty'
	BEGIN
		 SELECT ContractingPartyId Id, ContractingPartyName Name , 0 ParentId, 1 flg, 0 Sequence FROM mstContractingParty WHERE isActive='Y' 
		 UNION
		SELECT StageConditions.ForeignKeyValue Id, mstContractingParty.ContractingPartyName Name, 0 ParentId, 1 flg, 0 Sequence
		FROM StageConfigurator
		INNER JOIN StageConditions ON StageConfigurator.StageID=StageConditions.StageID
		INNER JOIN  mstContractingParty ON  mstContractingParty.ContractingPartyId IN(StageConditions.ForeignKeyValue)
		WHERE StageConditions.StageID=@StageId AND StageConditions.FieldLibraryID=100002
	END
	
	IF @TableName='mstPriority'
	BEGIN
	 SELECT (CASE WHEN Id='NA' THEN '0' 
	             WHEN Id='Y' THEN '1' 
				 WHEN Id='N' THEN '2' END)Id
	 ,Name FROM MstView WHERE PickListMasterName ='Options' 	
	END

	IF @TableName='MstContractType'
	BEGIN
	 	SELECT  ContractTypeId Id, ContractTypeName Name,0 ParentId, 1 flg, 0 Sequence
		FROM MSTContractType  
		WHERE  isActive='Y'  AND MstContractType.ContractTypeId in(SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),MstContractType.ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),','))
	
		UNION
		SELECT StageConditions.ForeignKeyValue ContractTypeId, MstContractType.ContractTypeName,0 ParentId, 1 flg, 0 Sequence
		FROM StageConfigurator
		INNER JOIN StageConditions ON StageConfigurator.StageID=StageConditions.StageID
		INNER JOIN MSTContractType ON MstContractType.ContractTypeId IN(StageConditions.ForeignKeyValue)
		WHERE StageConditions.StageID=@StageId AND StageConditions.FieldLibraryID=100001

	END

	IF @TableName='vwContractStatus'
	BEGIN
		SELECT ContractStatusID Id,StatusName Name FROM vwContractStatus
		WHERE ContractStatusID  NOT IN(7,8,10,11)
	END

	IF @Search IS NULL OR @Search=''
	BEGIN	
			IF @PartentId IS NULL OR @PartentId = 0
				-- stage config requirement -- sk 15 sept 2014
					SELECT TOP 500 Id,Name, ParentId FROM
					(
					SELECT Id, IIF(@FlgNameDesc = 0, Name, NameDescription) Name , ParentId, 1 flg, Sequence FROM MstView WHERE TableName = @TableName AND isActive='Y' 
					UNION 
					SELECT Id, IIF(@FlgNameDesc = 0, Name, NameDescription) Name, ParentId, 0 flg, Sequence FROM MstView WHERE TableName = @TableName AND ID IN (SELECT Id FROM @tmpCityMasterIds)
					UNION
					SELECT DISTINCT StageConditions.ForeignKeyValue Id, MstView.Name, ParentId, 1 flg, Sequence
					FROM StageConfigurator
					INNER JOIN StageConditions ON StageConfigurator.StageID=StageConditions.StageID
					INNER JOIN MstView ON MstView.Id IN(StageConditions.ForeignKeyValue)
					WHERE StageConditions.StageID=@StageId AND ParentId=0 AND MstView.TableName=@TableName
					)tmp
					ORDER BY flg ,Sequence, Name
			ELSE
					SELECT Id,Name, ParentId FROM MstView WHERE TableName = @TableName AND (ParentId = @PartentId OR ParentId = -1) AND isActive='Y' 
					ORDER BY Sequence, Name
	END
	ELSE	
	BEGIN	
			IF @PartentId IS NULL OR @PartentId = 0
				SELECT TOP 500 Id,Name, ParentId FROM
					(
					SELECT Id, IIF(@FlgNameDesc = 0, Name,NameDescription) Name, ParentId,  Sequence FROM MstView WHERE TableName = @TableName AND Name LIKE '%'+ @Search +'%' AND isActive='Y' 
					UNION
					SELECT DISTINCT StageConditions.ForeignKeyValue Id, MstView.Name, ParentId, Sequence
					FROM StageConfigurator
					INNER JOIN StageConditions ON StageConfigurator.StageID=StageConditions.StageID
					INNER JOIN MstView ON MstView.Id IN(StageConditions.ForeignKeyValue)
					WHERE StageConditions.StageID=@StageId AND ParentId=0 AND MstView.TableName=@TableName
					AND Name LIKE '%'+ @Search +'%'
					)tmp
				ORDER BY Sequence, Name
			ELSE
				SELECT Id,Name, ParentId FROM MstView WHERE TableName = @TableName AND (ParentId = @PartentId OR ParentId = -1)  AND Name LIKE '%'+ @Search +'%' AND isActive='Y' 
				ORDER BY Sequence, Name
	END

END





/*

IF @PartentId IS NULL OR @PartentId = 0
		SELECT Id,Name, ParentId FROM MstView WHERE TableName = @TableName AND isActive='Y' 
		ORDER BY Sequence, Name
	ELSE
		SELECT Id,Name, ParentId FROM MstView WHERE TableName = @TableName AND (ParentId = @PartentId OR ParentId = -1) AND isActive='Y' 
		ORDER BY Sequence, Name
END



*/










GO



ALTER PROCEDURE [dbo].[MasterCountryAddUpdate]
@CountryId INT=0,
@CountryName VARCHAR(50),
@isActive CHAR(1)='Y',
@AddedBy VARCHAR(50),
@ModifiedBy VARCHAR(50),
@IpAddress VARCHAR(50),
@Description VARCHAR(100),
@Status INT OUT

AS
BEGIN
	IF @CountryId =0
	BEGIN
		IF NOT EXISTS (SELECT CountryName FROM MstCountry WHERE CountryName = @CountryName)
		BEGIN
			INSERT INTO MstCountry (CountryName, isActive, Addedby, Addedon, IP, [Description])  
			VALUES (@CountryName, @isActive ,@AddedBy, GETDATE(), @IpAddress, @Description)
			SET @Status = 1
		END
		ELSE
		BEGIN
			SET @Status = 2
		END
	END
	ELSE
	BEGIN
		IF NOT EXISTS (SELECT CountryName FROM MstCountry WHERE CountryName = @CountryName and CountryId <> @CountryId)
		BEGIN
			UPDATE MstCountry 
			SET CountryName = @CountryName,  
			isActive = @isActive,
			Modifiedby = @ModifiedBy,
			Modifiedon = GETDATE(),
			IP = @IpAddress,
			[Description] = @Description
			WHERE CountryId = @CountryId
			SET @Status = 1
		END
		ELSE
		BEGIN
			SET @Status = 2
		END
	END

END


GO



ALTER PROCEDURE [dbo].[MasterCountryChangeIsActive]
@CountryIds VARCHAR(2000)=NULL,
@isActive CHAR(1)='Y'

AS
BEGIN

	UPDATE MstCountry SET isActive = @isActive 
	WHERE CountryId IN (SELECT ITEMS COLLATE SQL_Latin1_General_CP1_CI_AS FROM dbo.Split(@CountryIds,','))  

END

 



 GO



ALTER PROCEDURE [dbo].[MasterCountryDelete]
@CountryIds VARCHAR(2000)=NULL

AS
BEGIN

	DELETE MstCountry WHERE CountryId IN (SELECT ITEMS COLLATE SQL_Latin1_General_CP1_CI_AS FROM dbo.Split(@CountryIds,',')) 

END




/*



exec MasterCountryRead 

2
,1,1000000000




*/
GO



ALTER PROCEDURE [dbo].[MasterCountryRead]
	@CountryId INT = 0,
	@PageNo INT = 1 ,   
	@RecordsPerPage INT = 10,
	@Search VARCHAR(10)='',
	@SortColumn VARCHAR(20)='',
	@Direction INT = 0
AS
BEGIN

	SET NOCOUNT ON;
	
	IF @PageNo = 0 SET @PageNo = 1
	IF @RecordsPerPage = 0 SET @RecordsPerPage = 25


	IF @CountryId > 0 
		BEGIN
			SELECT  CountryId , CountryName,isActive,'Y'isUsed, [Description] FROM MSTCountry WHERE CountryId = @CountryId
		END
	ELSE
	BEGIN
		IF @PageNo < 1
		BEGIN
			SET @PageNo = 1
		END
		SELECT  CountryId , CountryName,
		CASE WHEN isActive='N' THEN 'Inactive' ELSE 'Active' END isActive, 
		COALESCE((
			SELECT MAX('Y') FROM MstState WHERE MstState.CountryId = MSTCountry.CountryId
			), 'N') isUsed ,
		[Description] 
		FROM MSTCountry 
		WHERE ( CountryName LIKE '%'+ @Search +'%'  )
		ORDER BY 
		CASE WHEN @SortColumn='' AND @Direction = 0  THEN CountryId END DESC,
		CASE WHEN @SortColumn =  'Country Name'  AND @Direction = 0 THEN CountryName END ASC, 
		CASE WHEN @SortColumn =  'Description'  AND @Direction = 0 THEN [Description]  END ASC, 
		CASE WHEN @SortColumn =  'Status'  AND @Direction = 0 THEN isActive END ASC, 
			
		CASE WHEN @SortColumn =  'Country Name'  AND @Direction = 1 THEN CountryName END DESC, 
		CASE WHEN @SortColumn =  'Description'  AND @Direction = 1 THEN [Description]  END DESC, 
		CASE WHEN @SortColumn =  'Status'  AND @Direction = 1 THEN isActive END DESC 

		OFFSET ( @PageNo - 1 ) * @RecordsPerPage ROWS
		FETCH NEXT @RecordsPerPage ROWS ONLY

		SELECT ISNULL(COUNT(1),0)TotalRecords  FROM MSTCountry  
		WHERE ( CountryName LIKE '%'+ @Search +'%'  )

	END
	



	--BEGIN CATCH
	--	THROW
	----DECLARE 
	----@ErrorNumber INT = ERROR_NUMBER(), @ErrorSeverity VARCHAR(200) = ERROR_SEVERITY(), 
	----@ErrorState VARCHAR(1000) = ERROR_STATE(), @ErrorProcedure VARCHAR(100)= ERROR_PROCEDURE(), 
	----@ErrorLine INT = ERROR_LINE(), @ErrorMessage VARCHAR(1000)= ERROR_MESSAGE()
	----RAISERROR('Can not read Country data', @ErrorSeverity, 1)
	--END  CATCH


END

GO



ALTER PROCEDURE [dbo].[MasterCountrySelect] 

AS
BEGIN
	SELECT CountryId,CountryName,[Description] FROM MstCountry WHERE isActive='Y' ORDER BY CountryName
END





GO



ALTER PROCEDURE [dbo].[MasterStateAddUpdate]
@StateId INT=0,
@StateName VARCHAR(50),
@CountryId INT,
@isActive CHAR(1)='Y',
@AddedBy VARCHAR(50),
@ModifiedBy VARCHAR(50),
@IpAddress VARCHAR(50),
@Description VARCHAR(100),
@Status INT OUT

AS
BEGIN
	IF @StateId =0
	BEGIN
		IF NOT EXISTS (SELECT StateName FROM MstState INNER JOIN MstCountry ON MstCountry.CountryId=MstState.CountryId WHERE MstState.StateName =@StateName  AND MstCountry.CountryId=@CountryId )
		BEGIN
			INSERT INTO MstState (StateName, isActive, Addedby, Addedon, IP, [Description], CountryId)  
			VALUES (@StateName, @isActive ,@AddedBy, GETDATE(), @IpAddress, @Description, @CountryId)
			SET @Status = 1
		END
		ELSE
		BEGIN
			SET @Status = 2
		END
	END
	ELSE
	BEGIN
		IF NOT EXISTS (SELECT StateName FROM MstState WHERE StateName = @StateName AND StateId <> @StateId)
		BEGIN
		UPDATE MstState 
		SET StateName = @StateName,  
		isActive = @isActive,
		Modifiedby = @ModifiedBy,
		Modifiedon = GETDATE(),
		IP = @IpAddress,
		CountryId = @CountryId,
		[Description] = @Description
		WHERE StateId = @StateId
		SET @Status = 1
	END
		ELSE
		BEGIN
			SET @Status = 2
		END
	END

END




GO



ALTER PROCEDURE [dbo].[MasterStateChangeIsActive]
@StateIds VARCHAR(2000)=NULL,
@isActive CHAR(1)='Y'

AS
BEGIN

	UPDATE MstState SET isActive = @isActive 
	WHERE StateId IN (SELECT ITEMS COLLATE SQL_Latin1_General_CP1_CI_AS FROM dbo.Split(@StateIds,','))  

END

 





 GO



ALTER PROCEDURE [dbo].[MasterStateDelete]
@StateIds VARCHAR(2000)=NULL

AS
BEGIN

	DELETE MstState WHERE StateId IN (SELECT ITEMS COLLATE SQL_Latin1_General_CP1_CI_AS FROM dbo.Split(@StateIds,',')) 

END





/*



exec MasterStateRead 1
,1,1000000000



*/
GO



ALTER PROCEDURE [dbo].[MasterStateRead]
	@StateId INT = 0,
	@PageNo INT = 1 ,   
	@RecordsPerPage INT = 25,
	@Search VARCHAR(10)='',
	@SortColumn VARCHAR(20)='',
	@Direction INT = 0

AS
BEGIN

	SET NOCOUNT ON;
	
	IF @PageNo = 0 SET @PageNo = 1
	IF @RecordsPerPage = 0 SET @RecordsPerPage = 25
	


	IF @StateId > 0 
		BEGIN
			SELECT  StateId , StateName,MSTState.isActive,'Y'isUsed, MSTState.[Description], MSTState.CountryId, MstCountry.CountryName 
			FROM MSTState 
			INNER JOIN MstCountry ON MstCountry.CountryId = MstState.CountryId
			WHERE StateId = @StateId
		END
	ELSE
	BEGIN
		IF @PageNo < 1
		BEGIN
			SET @PageNo = 1
		END
		SELECT  StateId , StateName,
		CASE WHEN MstState.isActive='N' THEN 'Inactive' ELSE 'Active' END isActive, 
			COALESCE((
			SELECT MAX('Y') FROM MstCity WHERE MstCity.StateId = MstState.StateId
			), 'N') isUsed ,

		MstState.[Description] ,
		MstState.CountryId,
		MstCountry.CountryName
		FROM MstState 
		INNER JOIN MstCountry ON MstCountry.CountryId = MstState.CountryId	
		WHERE ( StateName LIKE '%'+ @Search +'%'  )  OR ( MstState.isActive LIKE '%'+ @Search +'%'  ) 
					OR ( MstState.[Description] LIKE '%'+ @Search +'%'  )  OR ( MstCountry.CountryName LIKE '%'+ @Search +'%'  ) 
		
		ORDER BY 
		CASE WHEN @SortColumn = ''  AND @Direction = 0 THEN StateId END DESC, 
		CASE WHEN @SortColumn =  'State Name'  AND @Direction = 0 THEN StateName END ASC, 
		CASE	WHEN @SortColumn =  'Country'  AND @Direction = 0 THEN MstCountry.CountryName END ASC, 
		CASE WHEN @SortColumn =  'Description'  AND @Direction = 0 THEN MstState.[Description]  END ASC, 
		CASE	WHEN @SortColumn =  'Status'  AND @Direction = 0 THEN MstState.isActive END ASC, 
			
		CASE WHEN @SortColumn =  'State Name'  AND @Direction = 1 THEN StateName END DESC, 
		CASE	WHEN @SortColumn =  'Country'  AND @Direction = 1 THEN MstCountry.CountryName END DESC, 
		CASE WHEN @SortColumn =  'Description'  AND @Direction = 1 THEN MstState.[Description]  END DESC, 
		CASE	WHEN @SortColumn =  'Status'  AND @Direction = 1 THEN MstState.isActive END DESC 

		OFFSET ( @PageNo - 1 ) * @RecordsPerPage ROWS
		FETCH NEXT @RecordsPerPage ROWS ONLY

		SELECT ISNULL(COUNT(1),0)TotalRecords  FROM MSTState  
		WHERE ( StateName LIKE '%'+ @Search +'%'  )

	END
	



	--BEGIN CATCH
	--	THROW
	----DECLARE 
	----@ErrorNumber INT = ERROR_NUMBER(), @ErrorSeverity VARCHAR(200) = ERROR_SEVERITY(), 
	----@ErrorState VARCHAR(1000) = ERROR_STATE(), @ErrorProcedure VARCHAR(100)= ERROR_PROCEDURE(), 
	----@ErrorLine INT = ERROR_LINE(), @ErrorMessage VARCHAR(1000)= ERROR_MESSAGE()
	----RAISERROR('Can not read State data', @ErrorSeverity, 1)
	--END  CATCH


END



GO



ALTER PROCEDURE [dbo].[MasterStateSelect] --@CountryId=1
@CountryId INT = NULL
AS
BEGIN
	IF @CountryId IS NULL
	SELECT StateId,StateName,[Description] FROM MstState WHERE isActive='Y' ORDER BY StateName
	ELSE
	SELECT StateId,StateName,[Description] FROM MstState WHERE CountryId = @CountryId AND isActive='Y' ORDER BY StateName

END










/*
SELECT * FROM mstUsers
exec MasterUserGetUsersId 'usil'


*/

GO

ALTER PROCEDURE [dbo].[MasterUserGetUsersId]
	@UserName VARCHAR(50)
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @UserId INT = 0
	DECLARE @FullName VARCHAR(100) 
	DECLARE @RoleName VARCHAR(100) 
	DECLARE @OutlookFileName VARCHAR(1000)
	DECLARE @IsOutlookSynchEnable CHAR(1)

	SELECT @UserId= AU.UsersID, @FullName = FullName ,@OutlookFileName=RIGHT(COALESCE(MU.OutlookCalendarFileName,''), CHARINDEX('/', REVERSE('/' + COALESCE(MU.OutlookCalendarFileName,''))) - 1) 
	FROM aspnet_Users AU
	INNER JOIN MstUsers MU ON AU.UsersId=MU.UsersId
	WHERE MU.UserName = LTRIM(@UserName)AND MU.IsDeleted IS NULL AND MU.IsActive !='N'
	
	SELECT @IsOutlookSynchEnable =COALESCE(IsOutlookCalendarEnable,'N') FROM Subscribers WHERE DBName=DB_NAME()

	
	SET @RoleName  =( SELECT TOP 1 RoleName FROM vwUsers WHERE UsersId = @UserId)

	SELECT ISNULL(@UserId,0) UserId, @FullName FullName, @RoleName RoleName,@OutlookFileName OutlookFileName ,@IsOutlookSynchEnable IsOutlookSynchEnable
	
	EXEC AccessDynamicGet @UserId

	DECLARE @DepartmnetID INT =( SELECT TOP 1 DepartmentId FROM vwUsers WHERE UsersId = @UserId)
	set @DepartmnetID=(select ISNULL(@DepartmnetID,0))
	SELECT @DepartmnetID DepartmentID

		
	EXEC AccessGetMainSection @UserId

   

END


GO

/*
SELECT * FROM aspnet_Membership
exec MasterUserGetUsersIdByEmailId 'pchavan@practiceleague.com'
*/

ALTER PROCEDURE [dbo].[MasterUserGetUsersIdByEmailId]
	@EmailId VARCHAR(50)
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @UserId INT = 0
	DECLARE @UserName VARCHAR(100) 
	DECLARE @FullName VARCHAR(100) 
	DECLARE @RoleName VARCHAR(100) 
	DECLARE @OutlookFileName VARCHAR(1000)
	DECLARE @IsOutlookSynchEnable CHAR(1)

	SELECT  @UserName=AU.UserName,
	@UserId= AU.UsersID, 
	@FullName = FullName ,
	@OutlookFileName=RIGHT(COALESCE(MU.OutlookCalendarFileName,''), CHARINDEX('/', REVERSE('/' + COALESCE(MU.OutlookCalendarFileName,''))) - 1) 
	FROM aspnet_Users AU
	INNER JOIN MstUsers MU ON AU.UsersId=MU.UsersId
	INNER JOIN aspnet_Membership ON AU.UserId=aspnet_Membership.UserId
	WHERE aspnet_Membership.Email=RTRIM(LTRIM(@EmailId))
	AND MU.IsDeleted IS NULL AND MU.IsActive !='N'
	
	SELECT @IsOutlookSynchEnable =COALESCE(IsOutlookCalendarEnable,'N') FROM Subscribers WHERE DBName=DB_NAME()
	
	SET @RoleName  =( SELECT TOP 1 RoleName FROM vwUsers WHERE UsersId = @UserId)

	SELECT ISNULL(@UserId,0) UserId, @UserName UserName, @FullName FullName, @RoleName RoleName,@OutlookFileName OutlookFileName ,@IsOutlookSynchEnable IsOutlookSynchEnable
	
	EXEC AccessDynamicGet @UserId

	DECLARE @DepartmnetID INT =( SELECT TOP 1 DepartmentId FROM vwUsers WHERE UsersId = @UserId)
	set @DepartmnetID=(select ISNULL(@DepartmnetID,0))
	SELECT @DepartmnetID DepartmentID
		
	EXEC AccessGetMainSection @UserId  

END







/*

MasterUserRead 0,1,10,'','user name',0,'',0

*/
GO


ALTER PROCEDURE [dbo].[MasterUserRead] 
	@UsersId INT = 0,
	@PageNo INT = 1 ,   
	@RecordsPerPage INT = 25,
	@Search VARCHAR(100)='',
	@SortColumn VARCHAR(20)='',
	@Direction INT = 0,
	@DepartmentId int=0,
	@RoleId int=0
AS
BEGIN

	SET NOCOUNT ON;
	
	IF @PageNo = 0 OR @PageNo < 1 SET @PageNo = 1
	IF @RecordsPerPage = 0 SET @RecordsPerPage = 25

	IF dbo.IsTableExists('#tmpUser')> 0 DROP TABLE #tmpUser
	CREATE TABLE #tmpUser (UsersId INT) 


	IF (@DepartmentId=0 AND @RoleId=0)
		BEGIN
			INSERT INTO #tmpUser (UsersId)
			SELECT UsersId FROM MstUsers
		END
	ELSE IF(@DepartmentId>0 AND @RoleId=0)
		BEGIN
			INSERT INTO #tmpUser (UsersId)
			SELECT UsersId FROM MstUsers WHERE DepartmentId = @DepartmentId
		END
	ELSE IF(@DepartmentId= 0 AND @RoleId > 0)
		BEGIN
			INSERT INTO #tmpUser (UsersId)
			SELECT UsersId FROM MstUsers WHERE RoleId = @RoleId
		END
	ELSE IF(@DepartmentId> 0 AND @RoleId > 0)
		BEGIN
			INSERT INTO #tmpUser (UsersId)
			SELECT UsersId FROM MstUsers WHERE RoleId = @RoleId AND DepartmentId = @DepartmentId
		END


	IF @UsersId > 0 
		BEGIN
		SELECT 
		MstUsers.CityName CityName, 
		MstDepartment.DepartmentId, MstDepartment.DepartmentName, 		
		MstUsers.StateName StateName, 
		MstTitles.TitleName,MstUsers.ImageUrl, MstRole.RoleId, MstRole.RoleName, ISNULL(MstCountry.CountryId,0)as CountryId, MstCountry.CountryName, MstUsers.UsersId, 
		MstTitles.TitleId, MstUsers.UserName, MstUsers.FirstName, MstUsers.LastName, MstUsers.MiddleName, MstUsers.[Address], MstUsers.Phone, 
		MstUsers.Pincode, MstUsers.MobileNo, MstUsers.IsActive, 'Y' AS isUsed, aspnet_Membership.Email,MstUsers.[Description],MstUsers.IsDeleted,FullName
		FROM  aspnet_Users 
		INNER JOIN  aspnet_Membership ON aspnet_Membership.UserId = aspnet_Users.UserId 
		INNER JOIN  MstUsers ON MstUsers.UsersId = aspnet_Users.UsersId 
		LEFT OUTER JOIN  MstCountry ON MstCountry.CountryId = MstUsers.CountryId 
		INNER JOIN  MstRole ON MstRole.RoleId  = MstUsers.RoleId  
		INNER JOIN  MstDepartment ON MstDepartment.DepartmentId  = MstUsers.DepartmentId 
		INNER JOIN  MstTitles ON MstTitles.TitleId = MstUsers.TitleId 		
		WHERE  MstUsers.UsersId = @UsersId and  MstUsers.IsDeleted IS NULL
		AND MstUsers.UsersId!=1083
			
		END
	ELSE
	BEGIN
		
	
		SELECT  MU.CityName, MstDepartment.DepartmentId, MstDepartment.DepartmentName, 
		MU.StateName, 
		MstTitles.TitleName,MstTitles.TitleId,MU.ImageUrl ,
        MstRole.RoleId, MstRole.RoleName, ISNULL(MstCountry.CountryId,0)as CountryId,MstCountry.CountryName, MU.UsersId, MstTitles.TitleId, MU.UserName, 
        MU.FirstName, MU.LastName, MU.MiddleName, MU.Address, MU.Phone, MU.Pincode, MU.MobileNo,MU.[Description],
		aspnet_Membership.Email,
		CASE WHEN MU.isActive='N' THEN 'Inactive' ELSE 'Active' END isActive, 
		--	( 
		--		SELECT COALESCE(MAX('Y'),
		--		(SELECT COALESCE(MAX('Y'),
		--		(SELECT COALESCE(MAX('Y'),'N') FROM ContractRequest WHERE [RequesterUserId]  = MU.UsersId)
		--		) 
		--		 FROM ContractRequest WHERE [Addedby]  = MU.UsersId)
		--		 ,(      -- add by kk on 14 july 2016
		--		SELECT COALESCE(MAX('Y'),'N') FROM StageConditions WHERE (FieldLibraryID=100007 OR FieldLibraryID=100012) AND ForeignKeyValue=MU.UsersId
		--		)
		--		) FROM ContractRequest WHERE AssignToUserID  = MU.UsersId
		--	)isUsed,
		COALESCE(
			   (
				SELECT MAX('Y') FROM ContractRequest WHERE AssignToUserID  = MU.UsersId
				),
			    (
				SELECT MAX('Y') FROM ContractRequest WHERE [Addedby]  = MU.UsersId
				),
				(
				SELECT MAX('Y') FROM StageConfigurator WHERE StageConfigurator.UsersId  = MU.UsersId
				),
				(
				SELECT MAX('Y') FROM StageConditions WHERE (FieldLibraryID=100007 OR FieldLibraryID=100012) AND ForeignKeyValue=CAST(MU.UsersId AS varchar(10))
				),
				(SELECT MAX('Y') FROM ContractRequest WHERE [RequesterUserId]  = MU.UsersId
				), 'N') isUsed ,
		MU.FullName as FullName,MU.IsDeleted
		FROM            
		aspnet_Users 
		INNER JOIN aspnet_Membership ON aspnet_Users.UserId = aspnet_Membership.UserId 
		INNER JOIN MstUsers MU ON  MU.UsersId =aspnet_Users.UsersId  
		LEFT OUTER JOIN	MstCountry ON MU.CountryId = MstCountry.CountryId 
		INNER JOIN 	MstRole ON MU.RoleId = MstRole.RoleId 
		INNER JOIN 	MstDepartment ON MU.DepartmentId = MstDepartment.DepartmentId 
		INNER JOIN 	MstTitles ON MU.TitleId = MstTitles.TitleId 		
		WHERE  
		MU.UsersId IN (SELECT UsersId FROM #TmpUser) AND
		MU.IsDeleted IS NULL  AND (MU.UserName LIKE '%'+ @Search+'%' or MU.FullName LIKE '%'+ @Search+'%'  or MstDepartment.DepartmentName LIKE '%'+ @Search+'%' or  MstCountry.CountryName LIKE '%'+ @Search+'%'
		or  MU.Phone LIKE '%'+ @Search+'%' or  MU.StateName LIKE '%'+ @Search+'%' or  MU.CityName LIKE '%'+ @Search+'%' or  MU.Pincode LIKE '%'+ @Search+'%'
		 or  MU.MobileNo LIKE '%'+ @Search+'%'  or  aspnet_Membership.Email LIKE '%'+ @Search+'%'  
		)
		AND MU.UsersId!=1083
		ORDER BY 
		
		CASE WHEN @SortColumn =  'User Name'  AND @Direction = 0 THEN MU.UserName END ASC, 
		CASE WHEN @SortColumn =  'Name'  AND @Direction = 0 THEN  MU.FullName END ASC, 
		CASE WHEN @SortColumn =  'Mobile'  AND @Direction = 0 THEN  MU.MobileNo  END ASC, 
		CASE WHEN @SortColumn =  'Email'  AND @Direction = 0 THEN aspnet_Membership.Email END ASC, 
		CASE WHEN @SortColumn =  'Department'  AND @Direction = 0 THEN MstDepartment.DepartmentName END ASC, 
		CASE WHEN @SortColumn =  'Role'  AND @Direction = 0 THEN MstRole.RoleName END ASC, 
		CASE WHEN @SortColumn =  'Status'  AND @Direction = 0 THEN  MU.isActive  END ASC,
			
		CASE WHEN @SortColumn =  'User Name'  AND @Direction = 1 THEN MU.UserName END DESC, 
		CASE WHEN @SortColumn =  'Name'  AND @Direction = 1 THEN  MU.FullName END DESC, 
		CASE WHEN @SortColumn =  'Mobile'  AND @Direction =1 THEN  MU.MobileNo  END DESC, 
		CASE WHEN @SortColumn =  'Email'  AND @Direction = 1 THEN aspnet_Membership.Email END DESC, 
		CASE WHEN @SortColumn =  'Department'  AND @Direction = 1 THEN MstDepartment.DepartmentName END DESC, 
		CASE WHEN @SortColumn =  'Role'  AND @Direction = 1 THEN MstRole.RoleName END DESC, 
		CASE WHEN @SortColumn =  'Status'  AND @Direction = 1 THEN  MU.isActive  END DESC,
		UserName ASC
		OFFSET ( @PageNo - 1 ) * @RecordsPerPage ROWS
		FETCH NEXT @RecordsPerPage ROWS ONLY
		
		SELECT ISNULL(COUNT(1),0)TotalRecords  
		FROM 
		aspnet_Users 
		INNER JOIN aspnet_Membership ON aspnet_Users.UserId = aspnet_Membership.UserId 
		INNER JOIN MstUsers MU  ON  MU.UsersId =aspnet_Users.UsersId 
		LEFT OUTER JOIN	MstCountry ON MU.CountryId = MstCountry.CountryId 
		INNER JOIN 	MstRole ON MU.RoleId = MstRole.RoleId 
		INNER JOIN 	MstDepartment ON MU.DepartmentId = MstDepartment.DepartmentId 
		INNER JOIN 	MstTitles ON MU.TitleId = MstTitles.TitleId 		
		WHERE MU.UsersId IN (SELECT UsersId FROM #TmpUser) AND  
		MU.IsDeleted IS NULL AND(MU.UserName LIKE '%'+ @Search+'%' or MU.FullName LIKE '%'+ @Search+'%'  or MstDepartment.DepartmentName LIKE '%'+ @Search+'%' or  MstCountry.CountryName LIKE '%'+ @Search+'%'
		OR MU.Phone LIKE '%'+ @Search+'%' or  MU.StateName LIKE '%'+ @Search+'%' or  MU.CityName LIKE '%'+ @Search+'%' or  MU.Pincode LIKE '%'+ @Search+'%'
		OR MU.MobileNo LIKE '%'+ @Search+'%'  or  aspnet_Membership.Email LIKE '%'+ @Search+'%'  
		)
		AND MU.UsersId!=1083
	   

	END
	
	


END

	
	



	GO



		

		ALTER PROCEDURE [dbo].[MasterUserStillAdd] 
	
		AS
		BEGIN
			DECLARE @TotalUsersAllowed INT, @UsersCreated INT
			SELECT
			@TotalUsersAllowed=
			(
			SELECT TOP(1) ISNULL(Subscriptions.NumberOfUsers, 0) NumberOfUsers
			FROM Subscribers
			INNER JOIN Subscriptions ON Subscriptions.SubscriberID = Subscribers.SubscriberID
			AND Subscriptions.SubscriptionID = (SELECT MAX (SubscriptionID) FROM Subscriptions WHERE SubscriberID =Subscribers.SubscriberID)
			WHERE DBName = DB_NAME() 
			ORDER BY Subscribers.SubscriberID DESC
			)

			SELECT @UsersCreated=
			(			
			--SELECT ISNULL(COUNT(1),0) FROM MstUsers WHERE IsActive= 'Y' OR IsActive IS NULL
			SELECT ISNULL(COUNT(1),0) FROM MstUsers WHERE (IsActive= 'Y' OR IsActive IS NULL)  AND COALESCE(IsDeleted,'N')='N'
			)

			SELECT CAST(@TotalUsersAllowed AS VARCHAR(100))+'_'+ CAST(@TotalUsersAllowed-@UsersCreated AS VARCHAR(100)) RemainingCount
			
		END






		GO


/*
[MetaDataSearch_PP] @UserID = 1083, @SearchText = 'contract',@SearchFor='Y'
*/

ALTER PROCEDURE [dbo].[MetaDataSearch_B4Optimization_03Jul17] 
@UserID INT,
@SearchText VARCHAR(30)='',
@Direction INT=0,
@SortColumn VARCHAR(30)='',
@PageNo INT=1,
@RecordsPerPage INT=50,

 @RequestFromDate DATETIME  = NULL,
 @RequestToDate DATETIME  = NULL,
 @SignatureFromDate DATETIME  = NULL,
 @SignatureToDate DATETIME  = NULL,
 @EffectiveFromDate DATETIME  = NULL,
 @EffectiveToDate DATETIME  = NULL,
 @ExpiryFromDate DATETIME  = NULL,
 @ExpiryToDate DATETIME  = NULL,
 
 @ContractTypeID INT = 0,
 @CustomerSupplierId INT = 0 ,
 @ContractStatus INT = 0,
 @ContractID INT = NULL,
 
 @ContractValue INT = 0,
 @DepartmentId  INT = 0,
 @RequestID INT = 0,
 @AssignToUserId INT = NULL,
 @RequesterUserId INT = NULL,
 @GETALL CHAR(1)='N',
 @SearchFor VARCHAR(50)  = NULL
 AS
 DECLARE @TotalRecords INT

			SET @SearchFor=(CASE @SearchFor WHEN 'Y' THEN 'PostSignature' WHEN 'N' THEN 'PreSignature' ELSE NULL END)

			IF @PageNo = 0
			BEGIN
				SET @PageNo = 1
			END

			IF @GETALL = 'Y'
			BEGIN
				SET  @PageNo = 1
				SET @RecordsPerPage = 10000000
			END

			DECLARE @UserRole VARCHAR(50)
			SET @UserRole=(SELECT MstRole.RoleName FROM MstUsers INNER JOIN MstRole ON MstUsers.RoleId=MstRole.RoleId AND MstUsers.UsersId=@UserID)

			DECLARE @FoundIn VARCHAR(200)=NULL	, @ContractStatusName VARCHAR(50)=NULL
			SELECT @ContractStatusName=StatusName FROM vwContractStatus WHERE ContractStatusID=@ContractStatus
			
		
			;WITH _CTE (RowNo, 
				RequestId, 
				ContractID, 
				RequestDate, 
				ContractTypeName, 
				StatusName,
				CustomerSupplier,
				CustomerSupplierType ,
				AssignedUser,
				SignatureDate,
				FoundIn,
				Addedon,
				SignDate )
AS 
(SELECT * FROM
			(
				SELECT ROW_NUMBER() OVER (PARTITION BY ContractRequest.RequestId ORDER BY ContractRequest.RequestId DESC) AS RowNo, 
				ContractRequest.RequestId, ContractRequest.ContractID,
				ContractRequest.Addedon RequestDate,
				MstContractType.ContractTypeName,
				--DBO.GetRequestStatus(ContractRequest.RequestId)StatusName,
				vwContractStatus.StatusName,
				ContractRequest.ClientName CustomerSupplier, 
				CASE ContractRequest.isCustomer WHEN 'Y' THEN 'Customer' WHEN 'N' THEN  'Supplier' ELSE 'Others' END CustomerSupplierType ,
				Assigner.FullName AssignedUser, 
				--CAST([dbo].[SignatureDateByRequestId](ContractRequest.RequestId) AS DATETIME) AS SignatureDate,
				dbo.DateOnly(COALESCE(Cf.Modifiedon,Cf.Addedon)) AS SignatureDate,
				[dbo].[GetSearchTextFoundIn](@SearchText,ContractRequest.RequestId)FoundIn
				,ContractRequest.Addedon
				--,CAST([dbo].[SignatureDateByRequestId](ContractRequest.RequestId) AS DATETIME) AS SignDate
				,dbo.DateOnly(COALESCE(Cf.Modifiedon,Cf.Addedon)) SignDate
				FROM ContractRequest
				LEFT OUTER JOIN (SELECT MAX(RequestId) RequestId, ContractID FROM ContractRequest GROUP BY ContractID) CR1 ON ISNULL(ContractRequest.ContractID,0) = ISNULL(CR1.ContractID,0)
				LEFT OUTER JOIN ContractRequest CRStatus ON CR1.RequestId = CRStatus.RequestId
				LEFT OUTER JOIN vwContractStatus ON ContractRequest.ContractStatusID=vwContractStatus.ContractStatusID
				LEFT JOIN ContractFiles Cf ON ContractRequest.RequestId = Cf.RequestId AND IsFromDocuSign='Y'
				LEFT JOIN ContractDocSignDetails CDocsing ON ContractRequest.RequestId = CDocsing.RequestId AND CDocsing.Status = 'Completed'
				INNER JOIN vwRequestAccess ON CRStatus.RequestId = vwRequestAccess.RequestId AND vwRequestAccess.UsersId = @UserID
				INNER JOIN MstContractType ON ContractRequest.ContractTypeId = MstContractType.ContractTypeId
				INNER JOIN MstUsers ON vwRequestAccess.UsersId = MstUsers.UsersId
				INNER JOIN MstUsers Assigner ON ContractRequest.AssignToUserID = Assigner.UsersId
				LEFT OUTER JOIN (SELECT RequestId, MAX(ReceivedDate)ReceivedDate FROM ContractDocSignDetails 
								WHERE ContractDocSignDetails.[Status] = 'Completed'
								GROUP BY RequestId)ContractDocSignDetails ON ContractRequest.RequestId = ContractDocSignDetails.RequestId
				LEFT OUTER JOIN ContractFiles ON ContractRequest.RequestId = ContractFiles.RequestId AND ISNULL(ContractFiles.isUploaded, 'N') = 'Y' AND ISNULL(ContractFiles.isFromDocuSign, 'N') = 'Y'
				INNER JOIN Client ON Client.ClientId = ContractRequest.ClientId
				LEFT OUTER JOIN ClientAddressDetails CA ON CA.ClientId = Client.ClientId
				LEFT OUTER JOIN ContractQuestionsAnswers qA ON qA.RequestId = ContractRequest.RequestId
			    LEFT OUTER JOIN MstCountry ON CA.CountryId  = MstCountry.CountryId  OR (qA.ForeignKeyValye IS NOT NULL AND  CAST(MstCountry.CountryId AS VARCHAR(50)) IN (SELECT ITEMS FROM DBO.SPLIT(qA.ForeignKeyValye,',')))
			    LEFT OUTER JOIN Activity ON Activity.RequestId = ContractRequest.RequestId		
				LEFT OUTER  JOIN mstContractingParty ON  mstContractingParty.ContractingPartyId = ContractRequest.ContractingTypeId
				LEFT OUTER JOIN MstCurrency ON MstCurrency.CurrencyID = ContractRequest.CurrencyID
				WHERE
				--Matching SearchText
				(  CAST(ContractRequest.RequestId AS VARCHAR(30)) LIKE '%'+ @SearchText+'%'
				OR mstContractingParty.ContractingPartyName  LIKE '%'+ @SearchText+'%'
				OR CAST(ContractRequest.ContractID AS VARCHAR(30)) LIKE '%'+ @SearchText+'%'				
				OR DBO.DATEFORMAT(ContractRequest.Addedon)  LIKE '%'+ @SearchText+'%'
				OR ContractRequest.Address  LIKE '%'+ @SearchText+'%'
				OR ContractRequest.ContactNumber  LIKE '%'+ @SearchText+'%'
				OR ContractRequest.ContractTerm  LIKE '%'+ @SearchText+'%'
				OR ContractRequest.RequestDescription  LIKE '%'+ @SearchText+'%'
				OR DBO.DATEFORMAT(ContractRequest.DeadlineDate)   LIKE '%'+ @SearchText+'%'
				OR ContractRequest.EmailID  LIKE '%'+ @SearchText+'%'				
				OR DBO.[GetUserIdByEmpName](ContractRequest.Addedby ) LIKE '%'+ @SearchText+'%'
				OR ContractRequest.ContractingPartyName  LIKE '%'+ @SearchText+'%'
				OR CAST(ContractRequest.ContractValue  AS VARCHAR(50)) LIKE '%'+ @SearchText+'%'				
				OR DBO.DATEFORMAT(ContractRequest.ExpirationDate) LIKE '%'+ @SearchText+'%'
				OR DBO.DATEFORMAT(ContractRequest.EffectiveDate)  LIKE '%'+ @SearchText+'%'
				OR DBO.DATEFORMAT(ContractRequest.RenewalDate)  LIKE '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.ExpirationAlert30daysUserID1) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.ExpirationAlert30daysUserID2) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.ExpirationAlert7daysUserID1) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.ExpirationAlert7daysUserID2) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.ExpirationAlert24HrsUserID1) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.ExpirationAlert24HrsUserID2) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.RenewalAlert30daysUserID1) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.RenewalAlert30daysUserID2) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.RenewalAlert7daysUserID1) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.RenewalAlert7daysUserID2) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.RenewalAlert24HrsUserID1) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.RenewalAlert24HrsUserID2) LIKE  '%'+ @SearchText+'%'
				OR MstContractType.ContractTypeName LIKE '%'+ @SearchText+'%' 
				---OR DBO.GetRequestStatus(ContractRequest.RequestId) LIKE '%'+ @SearchText+'%'
				OR vwContractStatus.StatusName LIKE '%'+ @SearchText+'%'
				OR MstUsers.FullName  LIKE '%'+ @SearchText+'%'
				OR DBO.DATEONLY(ContractFiles.Modifiedon) LIKE '%'+ @SearchText+'%'
				--OR dbo.[SignatureDateByRequestId] (ContractRequest.RequestId) LIKE '%'+ @SearchText+'%'
				OR dbo.DateFormat(COALESCE(ContractFiles.Modifiedon,ContractFiles.Addedon)) LIKE '%'+ @SearchText+'%'
				OR DBO.DATEFORMAT(ContractFiles.Addedon)  LIKE '%'+ @SearchText+'%'
				OR Client.ClientName  LIKE '%'+ @SearchText+'%'
				OR CA.Address LIKE '%'+ @SearchText+'%'
				OR CA.PinCode LIKE '%'+ @SearchText+'%'
				OR CA.Street2 LIKE '%'+ @SearchText+'%'
				OR CA.CityName LIKE '%'+ @SearchText+'%'
				OR CA.StateName LIKE '%'+ @SearchText+'%'
				OR MstCountry.CountryName LIKE '%'+ @SearchText+'%'

				OR ContractRequest.ClientName LIKE '%'+ @SearchText+'%'
				OR ContractRequest.EmailID LIKE '%'+ @SearchText+'%'
				OR ContractRequest.ContactNumber LIKE '%'+ @SearchText+'%'

				OR ActivityText LIKE '%'+ @SearchText+'%'
				OR qA.Question  LIKE '%'+ @SearchText+'%'
				OR qA.TextValue  LIKE '%'+ @SearchText+'%'
				OR CAST(qA.NumericValue AS VARCHAR(50))  = @SearchText
				OR CAST(qA.DecimalValue AS VARCHAR(50))  =  @SearchText
				OR dbo.DateFormat (qA.DateValue)  LIKE '%'+  @SearchText +'%'
				OR (qA.ForeignKeyValye IS NOT NULL AND  MstCountry.CountryName LIKE '%'+  @SearchText +'%')
				OR [dbo].[GetRequestFormMetaDataField](ContractRequest.RequestId, 'Request Form')  LIKE '%'+ @SearchText+'%'
				OR [dbo].[GetRequestFormMetaDataField](ContractRequest.RequestId, 'Key Obligations')  LIKE '%'+ @SearchText+'%'
				OR [dbo].[GetRequestFormMetaDataField](ContractRequest.RequestId, 'Important Dates')  LIKE '%'+ @SearchText+'%'
				OR ContractRequest.LiabilityCap LIKE '%'+ @SearchText+'%'
				OR ContractRequest.Indemnity LIKE '%'+ @SearchText+'%'
				OR ContractRequest.Strictliability LIKE '%'+ @SearchText+'%'
				OR ContractRequest.Insurance LIKE '%'+ @SearchText+'%'
				OR ContractRequest.TerminationDescription LIKE '%'+ @SearchText+'%'
				OR ContractRequest.AssignmentNovation  LIKE '%'+ @SearchText+'%'
				OR ContractRequest.Others LIKE '%'+ @SearchText+'%'
				OR MstCurrency.CurrencyCode LIKE '%'+ @SearchText+'%'
				)
				--Adding Filters
				AND  ((DBO.DATEONLY(ContractRequest.Addedon) BETWEEN @RequestFromDate AND  @RequestToDate) OR @RequestFromDate IS NULL)
				AND  ((DBO.DATEONLY(ContractRequest.EffectiveDate) BETWEEN @EffectiveFromDate AND  @EffectiveToDate) OR @EffectiveFromDate IS NULL)
				AND  ((DBO.DATEONLY(ContractRequest.ExpirationDate) BETWEEN @ExpiryFromDate AND  @ExpiryToDate) OR @ExpiryFromDate IS NULL)
				AND  (ContractRequest.ContractTypeId = @ContractTypeID OR @ContractTypeID IS NULL OR @ContractTypeID = 0)
				AND  (ContractRequest.ClientId = @CustomerSupplierId OR @CustomerSupplierId IS NULL OR @CustomerSupplierId = 0)
				AND  (ContractRequest.ContractID = @ContractID OR @ContractID IS NULL OR @ContractID = 0)
				AND  (ContractRequest.ContractValue = @ContractValue OR @ContractValue IS NULL OR @ContractValue = 0)
				AND  (ContractRequest.RequestId = @RequestID OR @RequestID IS NULL OR @RequestID=0)
				AND  (ContractRequest.AssignToUserID = @AssignToUserId OR @AssignToUserId IS NULL OR @AssignToUserId=0)
				--AND  (DBO.GetRequestStatus(ContractRequest.RequestId) = @ContractStatusName OR @ContractStatus IS NULL OR @ContractStatus = '0')
				AND (vwContractStatus.StatusName = @ContractStatusName OR @ContractStatus IS NULL OR @ContractStatus = '0')
				AND  (ContractRequest.AddedBy = @RequesterUserId OR @RequesterUserId IS NULL OR @RequesterUserId = 0)
				AND  (Assigner.DepartmentId = @DepartmentId OR @DepartmentId IS NULL OR @DepartmentId = 0)
				--AND  (DBO.SignatureDateByRequestId(ContractRequest.RequestId) BETWEEN @SignatureFromDate AND  @SignatureToDate OR @SignatureFromDate IS NULL)
				AND (COALESCE(Cf.Modifiedon,Cf.Addedon) BETWEEN @SignatureFromDate AND  @SignatureToDate OR @SignatureFromDate IS NULL)
				AND (DBO.GetSignatureStatus(ContractRequest.RequestId) =  @SearchFor OR @SearchFor IS NULL)

				AND ContractRequest.ContractTypeId IN(SELECT items FROM dbo.Split((CASE WHEN @UserRole='Super Admin' THEN CONVERT(VARCHAR(10),ContractRequest.ContractTypeId)
				ELSE dbo.GetContractTypeAccess(@UserRole) END),','))
				
				)tmp WHERE RowNo = 1
				)
					 
				
				SELECT
				RowNo, 
				RequestId, 
				ContractID, 
				DBO.DateFormat(Addedon) RequestDate, 
				ContractTypeName, 
				StatusName, 
				CustomerSupplier,
				CustomerSupplierType ,
				AssignedUser,
				DBO.DateFormat(SignDate) SignatureDate,
				FoundIn
				FROM _CTE				
				ORDER BY 
				CASE WHEN @SortColumn=''  AND @Direction=0 THEN RequestId END DESC,
				CASE WHEN @SortColumn= 'Request ID'  AND @Direction=0 THEN RequestId END ASC,
				CASE WHEN @SortColumn= 'Contract ID'  AND @Direction=0 THEN ContractID END ASC,
				CASE WHEN @SortColumn= 'Request Date'  AND @Direction=0 THEN Addedon END ASC,
				CASE WHEN @SortColumn= 'Contract Type'  AND @Direction=0 THEN ContractTypeName END ASC,
				CASE WHEN @SortColumn= 'Contract Status'  AND @Direction=0 THEN StatusName END ASC,
				CASE WHEN @SortColumn= 'Customer / Supplier'  AND @Direction=0 THEN CustomerSupplier END ASC,
				CASE WHEN @SortColumn= 'Assigned User'  AND @Direction=0 THEN AssignedUser END ASC,
				CASE WHEN @SortColumn= 'Signature Date'  AND @Direction=0 THEN SignDate END ASC,

				CASE WHEN @SortColumn= 'Request ID'  AND @Direction=1 THEN RequestId END DESC,
				CASE WHEN @SortColumn= 'Contract ID'  AND @Direction=1 THEN ContractID END DESC,
				CASE WHEN @SortColumn= 'Request Date'  AND @Direction=1 THEN Addedon END DESC,
				CASE WHEN @SortColumn= 'Contract Type'  AND @Direction=1 THEN ContractTypeName END DESC,
				CASE WHEN @SortColumn= 'Contract Status'  AND @Direction=1 THEN StatusName END DESC,
				CASE WHEN @SortColumn= 'Customer / Supplier'  AND @Direction=1 THEN CustomerSupplier  END DESC,
				CASE WHEN @SortColumn= 'Assigned User'  AND @Direction=1 THEN AssignedUser END DESC,
				CASE WHEN @SortColumn= 'Signature Date'  AND @Direction=1 THEN SignDate END DESC
	
				OFFSET ( @PageNo - 1 ) * @RecordsPerPage ROWS
				FETCH NEXT @RecordsPerPage ROWS ONLY
				
				/*********************************************Pagination***************************************/

				
		SELECT COUNT(*) TotalRecords FROM
			(
				SELECT ROW_NUMBER() OVER (PARTITION BY ContractRequest.RequestId ORDER BY ContractRequest.RequestId DESC) AS RowNo, 
				ContractRequest.RequestId 
				--,ContractRequest.ContractID,
				--ContractRequest.Addedon RequestDate,
				--MstContractType.ContractTypeName,
				----DBO.GetRequestStatus(ContractRequest.RequestId)StatusName,
				--vwContractStatus.StatusName,
				--ContractRequest.ClientName CustomerSupplier, 
				--CASE ContractRequest.isCustomer WHEN 'Y' THEN 'Customer' WHEN 'N' THEN  'Supplier' ELSE 'Others' END CustomerSupplierType ,
				--Assigner.FullName AssignedUser, 
				--COALESCE(ContractDocSignDetails.ReceivedDate , ContractFiles.Addedon)SignatureDate,
				--CAST([dbo].[SignatureDateByRequestId](ContractRequest.RequestId) AS DATETIME) AS SignatureDate,
				--COALESCE(Cf.Modifiedon,Cf.Addedon) AS SignatureDate
				--,[dbo].[GetSearchTextFoundIn](@SearchText,ContractRequest.RequestId)FoundIn
				--,ContractRequest.Addedon
				--,ContractFiles.Addedon SignDate
				--,CAST([dbo].[SignatureDateByRequestId](ContractRequest.RequestId) AS DATETIME) AS SignDate,
				FROM ContractRequest 
				LEFT OUTER JOIN (SELECT MAX(RequestId) RequestId, ContractID FROM ContractRequest GROUP BY ContractID) CR1 ON ISNULL(ContractRequest.ContractID,0) = ISNULL(CR1.ContractID,0)
				LEFT OUTER JOIN ContractRequest CRStatus ON CR1.RequestId = CRStatus.RequestId
				LEFT OUTER JOIN vwContractStatus ON ContractRequest.ContractStatusID=vwContractStatus.ContractStatusID
				LEFT JOIN ContractFiles Cf ON ContractRequest.RequestId = Cf.RequestId AND IsFromDocuSign='Y'
				LEFT JOIN ContractDocSignDetails CDocsing ON ContractRequest.RequestId = CDocsing.RequestId AND CDocsing.Status = 'Completed'
				INNER JOIN vwRequestAccess ON ContractRequest.RequestId = vwRequestAccess.RequestId AND vwRequestAccess.UsersId = @UserID
				INNER JOIN MstContractType ON ContractRequest.ContractTypeId = MstContractType.ContractTypeId
				INNER JOIN MstUsers ON vwRequestAccess.UsersId = MstUsers.UsersId
				INNER JOIN MstUsers Assigner ON ContractRequest.AssignToUserID = Assigner.UsersId
				LEFT OUTER JOIN (SELECT RequestId, MAX(ReceivedDate)ReceivedDate FROM ContractDocSignDetails 
								WHERE UPPER(ContractDocSignDetails.[Status]) = UPPER('Completed')
								GROUP BY RequestId)ContractDocSignDetails ON ContractRequest.RequestId = ContractDocSignDetails.RequestId
				LEFT OUTER JOIN ContractFiles ON ContractRequest.RequestId = ContractFiles.RequestId AND ISNULL(ContractFiles.isUploaded, 'N') = 'Y' AND ISNULL(ContractFiles.isFromDocuSign, 'N') = 'Y'
				INNER JOIN Client ON Client.ClientId = ContractRequest.ClientId
				LEFT OUTER JOIN ClientAddressDetails CA ON CA.ClientId = Client.ClientId
				LEFT OUTER JOIN ContractQuestionsAnswers qA ON qA.RequestId = ContractRequest.RequestId
			    LEFT OUTER JOIN MstCountry ON CA.CountryId  = MstCountry.CountryId  OR (qA.ForeignKeyValye IS NOT NULL AND  CAST(MstCountry.CountryId AS VARCHAR(50)) IN (SELECT ITEMS FROM DBO.SPLIT(qA.ForeignKeyValye,',')))
			    LEFT OUTER JOIN Activity ON Activity.RequestId = ContractRequest.RequestId		
				LEFT OUTER  JOIN mstContractingParty ON  mstContractingParty.ContractingPartyId = ContractRequest.ContractingTypeId
				LEFT OUTER JOIN MstCurrency ON MstCurrency.CurrencyID = ContractRequest.CurrencyID
				WHERE
				--Matching SearchText
				(  CAST(ContractRequest.RequestId AS VARCHAR(30)) LIKE '%'+ @SearchText+'%'
				OR mstContractingParty.ContractingPartyName  LIKE '%'+ @SearchText+'%'
				OR CAST(ContractRequest.ContractID AS VARCHAR(30)) LIKE '%'+ @SearchText+'%'				
				OR DBO.DATEFORMAT(ContractRequest.Addedon)  LIKE '%'+ @SearchText+'%'
				OR ContractRequest.Address  LIKE '%'+ @SearchText+'%'
				OR ContractRequest.ContactNumber  LIKE '%'+ @SearchText+'%'
				OR ContractRequest.ContractTerm  LIKE '%'+ @SearchText+'%'
				OR ContractRequest.RequestDescription  LIKE '%'+ @SearchText+'%'
				OR DBO.DATEFORMAT(ContractRequest.DeadlineDate)   LIKE '%'+ @SearchText+'%'
				OR ContractRequest.EmailID  LIKE '%'+ @SearchText+'%'				
				OR DBO.[GetUserIdByEmpName](ContractRequest.Addedby ) LIKE '%'+ @SearchText+'%'
				OR ContractRequest.ContractingPartyName  LIKE '%'+ @SearchText+'%'
				OR CAST(ContractRequest.ContractValue  AS VARCHAR(50)) LIKE '%'+ @SearchText+'%'
				--OR CAST(vwContractRequestLatest.ContractValuewithcurrency  AS VARCHAR(50)) LIKE '%'+ @SearchText+'%'
 		
				OR DBO.DATEFORMAT(ContractRequest.ExpirationDate) LIKE '%'+ @SearchText+'%'
				OR DBO.DATEFORMAT(ContractRequest.EffectiveDate)  LIKE '%'+ @SearchText+'%'
				OR DBO.DATEFORMAT(ContractRequest.RenewalDate)  LIKE '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.ExpirationAlert30daysUserID1) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.ExpirationAlert30daysUserID2) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.ExpirationAlert7daysUserID1) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.ExpirationAlert7daysUserID2) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.ExpirationAlert24HrsUserID1) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.ExpirationAlert24HrsUserID2) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.RenewalAlert30daysUserID1) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.RenewalAlert30daysUserID2) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.RenewalAlert7daysUserID1) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.RenewalAlert7daysUserID2) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.RenewalAlert24HrsUserID1) LIKE  '%'+ @SearchText+'%'
				OR DBO.GetUserFullName(ContractRequest.RenewalAlert24HrsUserID2) LIKE  '%'+ @SearchText+'%'
				OR MstContractType.ContractTypeName LIKE '%'+ @SearchText+'%' 
				--OR DBO.GetRequestStatus(ContractRequest.RequestId) LIKE '%'+ @SearchText+'%'
				OR vwContractStatus.StatusName LIKE '%'+ @SearchText+'%'
				OR MstUsers.FullName  LIKE '%'+ @SearchText+'%'
				OR DBO.DATEONLY(ContractFiles.Modifiedon) LIKE '%'+ @SearchText+'%'
				--OR dbo.[SignatureDateByRequestId] (ContractRequest.RequestId) LIKE '%'+ @SearchText+'%'
				OR DBO.DATEFORMAT(COALESCE(Cf.Modifiedon,Cf.Addedon)) LIKE '%'+ @SearchText+'%'
				OR DBO.DATEFORMAT(ContractFiles.Addedon)  LIKE '%'+ @SearchText+'%'
				OR Client.ClientName  LIKE '%'+ @SearchText+'%'
				OR CA.Address LIKE '%'+ @SearchText+'%'
				OR CA.PinCode LIKE '%'+ @SearchText+'%'
				OR CA.Street2 LIKE '%'+ @SearchText+'%'
				OR CA.CityName LIKE '%'+ @SearchText+'%'
				OR CA.StateName LIKE '%'+ @SearchText+'%'
				OR MstCountry.CountryName LIKE '%'+ @SearchText+'%'

				OR ContractRequest.ClientName LIKE '%'+ @SearchText+'%'
				OR ContractRequest.EmailID LIKE '%'+ @SearchText+'%'
				OR ContractRequest.ContactNumber LIKE '%'+ @SearchText+'%'

				OR ActivityText LIKE '%'+ @SearchText+'%'
				OR qA.Question  LIKE '%'+ @SearchText+'%'
				OR qA.TextValue  LIKE '%'+ @SearchText+'%'
				OR CAST(qA.NumericValue AS VARCHAR(50))  = @SearchText
				OR CAST(qA.DecimalValue AS VARCHAR(50))  =  @SearchText
				OR dbo.DateFormat (qA.DateValue)  LIKE '%'+  @SearchText +'%'
				OR (qA.ForeignKeyValye IS NOT NULL AND  MstCountry.CountryName LIKE '%'+  @SearchText +'%')
				OR [dbo].[GetRequestFormMetaDataField](ContractRequest.RequestId, 'Request Form')  LIKE '%'+ @SearchText+'%'
				OR [dbo].[GetRequestFormMetaDataField](ContractRequest.RequestId, 'Key Obligations')  LIKE '%'+ @SearchText+'%'
				OR [dbo].[GetRequestFormMetaDataField](ContractRequest.RequestId, 'Important Dates')  LIKE '%'+ @SearchText+'%'
				OR ContractRequest.LiabilityCap LIKE '%'+ @SearchText+'%'
				OR ContractRequest.Indemnity LIKE '%'+ @SearchText+'%'
				OR ContractRequest.Strictliability LIKE '%'+ @SearchText+'%'
				OR ContractRequest.Insurance LIKE '%'+ @SearchText+'%'
				OR ContractRequest.TerminationDescription LIKE '%'+ @SearchText+'%'
				OR ContractRequest.AssignmentNovation  LIKE '%'+ @SearchText+'%'
				OR ContractRequest.Others LIKE '%'+ @SearchText+'%'
				OR MstCurrency.CurrencyCode LIKE '%'+ @SearchText+'%'
				)
				--Adding Filters
				AND  ((DBO.DATEONLY(ContractRequest.Addedon) BETWEEN @RequestFromDate AND  @RequestToDate) OR @RequestFromDate IS NULL)
				AND  ((DBO.DATEONLY(ContractRequest.EffectiveDate) BETWEEN @EffectiveFromDate AND  @EffectiveToDate) OR @EffectiveFromDate IS NULL)
				AND  ((DBO.DATEONLY(ContractRequest.ExpirationDate) BETWEEN @ExpiryFromDate AND  @ExpiryToDate) OR @ExpiryFromDate IS NULL)
				AND  (ContractRequest.ContractTypeId = @ContractTypeID OR @ContractTypeID IS NULL OR @ContractTypeID = 0)
				AND  (ContractRequest.ClientId = @CustomerSupplierId OR @CustomerSupplierId IS NULL OR @CustomerSupplierId = 0)
				AND  (ContractRequest.ContractID = @ContractID OR @ContractID IS NULL OR @ContractID = 0)
				AND  (ContractRequest.ContractValue = @ContractValue OR @ContractValue IS NULL OR @ContractValue = 0)
				AND  (ContractRequest.RequestId = @RequestID OR @RequestID IS NULL OR @RequestID=0)
				AND  (ContractRequest.AssignToUserID = @AssignToUserId OR @AssignToUserId IS NULL OR @AssignToUserId=0)
				--AND  (DBO.GetRequestStatus(ContractRequest.RequestId) = @ContractStatusName OR @ContractStatus IS NULL OR @ContractStatus = '0')
				AND  (vwContractStatus.StatusName = @ContractStatusName OR @ContractStatus IS NULL OR @ContractStatus = '0')
				AND  (ContractRequest.AddedBy = @RequesterUserId OR @RequesterUserId IS NULL OR @RequesterUserId = 0)
				AND  (Assigner.DepartmentId = @DepartmentId OR @DepartmentId IS NULL OR @DepartmentId = 0)
				AND  (DBO.SignatureDateByRequestId(ContractRequest.RequestId) BETWEEN @SignatureFromDate AND  @SignatureToDate OR @SignatureFromDate IS NULL)
				AND  (DBO.GetSignatureStatus(ContractRequest.RequestId) =  @SearchFor OR @SearchFor IS NULL)

				AND ContractRequest.ContractTypeId IN(SELECT items FROM dbo.Split((CASE WHEN @UserRole='Super Admin' THEN CONVERT(VARCHAR(10),ContractRequest.ContractTypeId)
				ELSE dbo.GetContractTypeAccess(@UserRole) END),','))
				
				)tmp WHERE RowNo = 1
				


				GO



/*
EXEC [OtherGeography_Report]
*/


ALTER PROCEDURE [dbo].[OtherGeography_Report]
@Flags INT = 0 ,
	@PageNo INT = 0 ,   
	@RecordsPerPage INT = 0,
	@FromDate DATETIME=NULL,
	@ToDate DATETIME=NULL,
	@ContractTypeId INT = NULL,
	@Search VARCHAR(10)='',
	@SortColumn VARCHAR(20)='Other Name',
	@Direction INT = 0,
	@UserID INT=NULL
AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @TmpContractType TABLE (ContractTypeId INT)
	IF @ContractTypeId IS NULL
		BEGIN
		INSERT INTO @TmpContractType (ContractTypeId) 
		SELECT ContractTypeId FROM MstContractType
		END
	ELSE
	BEGIN
		INSERT INTO @TmpContractType (ContractTypeId) 
		SELECT @ContractTypeId
	END

	  		DECLARE @Role VARCHAR(50)
		IF @UserID IS NULL
		SET @Role='Super Admin'
	ELSE
		SET @Role=(SELECT MstRole.RoleName FROM MstUsers INNER JOIN MstRole ON MstUsers.RoleId=MstRole.RoleId AND MstUsers.UsersId=@UserID)

	IF @FromDate IS NULL SET @FromDate = (SELECT MIN(Addedon) FROM ContractRequest WHERE  isCustomer = 'O')
	IF @ToDate IS NULL SET @ToDate = GETDATE()
	IF @PageNo <= 0 SET @PageNo = 1
	IF @RecordsPerPage = 0 SET @RecordsPerPage = (SELECT COUNT(1) FROM ContractRequest WHERE isCustomer = 'O')

	BEGIN
		
		SELECT 
		cR.ClientName [Other Name], 
		MstUsers.FullName [Requester Name], 
		cR.ContractID [Contract ID], 
		ct.ContractTypeName [Contract Type],
		dbo.DateFormat(cR.EffectiveDate) [Effective Date],
		dbo.DateFormat(cr.ExpirationDate) [Expiration Date],
		con.CountryName [Geography]
		FROM vwContractRequestLatest cR  --ContractRequest cR    COMMENTED ON 11 NOV 14 AFTER WORKFLOW CHANGES
		INNER JOIN MstUsers ON MstUsers.UsersId = cR.Addedby 
		LEFT OUTER JOIN MstContractType ct ON cR.ContractTypeId =ct.ContractTypeId 
		LEFT OUTER JOIN MstCountry con ON cR.CountryId=con.CountryId
		WHERE cr.ContractTypeId IN (SELECT ContractTypeId FROM @TmpContractType ) AND cr.ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),cr.ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),','))  AND  
		(cR.isCustomer = 'O')
		AND dbo.DateFormat(Cr.Addedon) BETWEEN dbo.DateOnly(@FromDate) AND dbo.DateOnly(@ToDate)
		AND cr.ContractID IS NOT NULL
		AND
		( ClientName LIKE '%'+ @Search +'%'  )
		ORDER BY 
		CASE WHEN @SortColumn='' AND @Direction = 0  THEN cR.RequestId END DESC,

		CASE WHEN @SortColumn =  'Other Name'  AND @Direction = 0 THEN cR.ClientName END ASC, 
		CASE WHEN @SortColumn =  'Requester Name'  AND @Direction = 0 THEN MstUsers.FullName  END ASC, 
		CASE WHEN @SortColumn =  'Contract ID'  AND @Direction = 0 THEN cR.ContractID END ASC, 
		CASE WHEN @SortColumn =  'Contract Type'  AND @Direction = 0 THEN ct.ContractTypeName END ASC,
		CASE WHEN @SortColumn =  'Effective Date'  AND @Direction = 0 THEN cR.EffectiveDate  END ASC, 
		CASE WHEN @SortColumn =  'Expiration Date'  AND @Direction = 0 THEN cr.ExpirationDate END ASC,
		CASE WHEN @SortColumn =  'Geography'  AND @Direction = 0 THEN con.CountryName END ASC,

			
		CASE WHEN @SortColumn =  'Other Name'  AND @Direction = 1 THEN cR.ClientName END DESC, 
		CASE WHEN @SortColumn =  'Requester Name'  AND @Direction = 1 THEN MstUsers.FullName  END DESC, 
		CASE WHEN @SortColumn =  'Contract ID'  AND @Direction = 1 THEN cR.ContractID END DESC, 
		CASE WHEN @SortColumn =  'Contract Type'  AND @Direction = 1 THEN ct.ContractTypeName END DESC,
		CASE WHEN @SortColumn =  'Effective Date'  AND @Direction = 1 THEN cR.EffectiveDate  END DESC, 
		CASE WHEN @SortColumn =  'Expiration Date'  AND @Direction = 1 THEN cr.ExpirationDate END DESC,
	    CASE WHEN @SortColumn =  'Geography'  AND @Direction = 1 THEN con.CountryName END DESC

		OFFSET ( @PageNo - 1 ) * @RecordsPerPage ROWS
		FETCH NEXT @RecordsPerPage ROWS ONLY
		 IF @Flags=0
	  BEGIN
		SELECT ISNULL(COUNT(1),0)TotalRecords  FROM vwContractRequestLatest   --ContractRequest     COMMENTED ON 11 NOV 14 AFTER WORKFLOW CHANGES  
		WHERE ContractTypeId IN (SELECT ContractTypeId FROM @TmpContractType ) AND ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),','))  AND  
		(isCustomer = 'O') AND ContractID IS NOT NULL
		AND dbo.DateOnly(Addedon) BETWEEN dbo.DateOnly(@FromDate) AND dbo.DateOnly(@ToDate)
		AND
		( ClientName LIKE '%'+ @Search +'%'  )

	END
	END

END











GO




/*
EXEC [OtherTypeValueGeography_Report]
*/


ALTER PROCEDURE [dbo].[OtherTypeValueGeography_Report]
@Flags INT = 0 , 
	@PageNo INT = 0 ,   
	@RecordsPerPage INT = 0,
	@FromDate DATETIME=NULL,
	@ToDate DATETIME=NULL,
	@ContractTypeId INT = NULL,
	@Search VARCHAR(10)='',
	@SortColumn VARCHAR(20)='Other Name',
	@Direction INT = 0,
	@UserID INT=NULL
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @TmpContractType TABLE (ContractTypeId INT)
	IF @ContractTypeId IS NULL
		BEGIN
		INSERT INTO @TmpContractType (ContractTypeId) 
		SELECT ContractTypeId FROM MstContractType
		END
	ELSE
	BEGIN
		INSERT INTO @TmpContractType (ContractTypeId) 
		SELECT @ContractTypeId
	END

	 			DECLARE @Role VARCHAR(50)
		IF @UserID IS NULL
		SET @Role='Super Admin'
	ELSE
		SET @Role=(SELECT MstRole.RoleName FROM MstUsers INNER JOIN MstRole ON MstUsers.RoleId=MstRole.RoleId AND MstUsers.UsersId=@UserID)

	IF @FromDate IS NULL SET @FromDate = (SELECT MIN(Addedon) FROM ContractRequest WHERE  isCustomer = 'O')
	IF @ToDate IS NULL SET @ToDate = GETDATE()
	IF @PageNo <= 0 SET @PageNo = 1
	IF @RecordsPerPage = 0 SET @RecordsPerPage = (SELECT COUNT(1) FROM ContractRequest WHERE  isCustomer = 'O')

	BEGIN
		
		SELECT 
		cR.ClientName [Other Name], 
		MstUsers.FullName [Requester Name], 
		cR.ContractID [Contract ID], 
		ct.ContractTypeName [Contract Type],
		dbo.DateFormat(cR.EffectiveDate) [Effective Date],
		dbo.DateFormat(cr.ExpirationDate) [Expiration Date],
		--cR.ContractValue [Contract Value],
		CONVERT(VARCHAR(200), Format(cr.ContractValue ,'N','en-US' ))+' ('+cu.CurrencyCode+')' [Contract Value],
		con.CountryName [Geography]
		FROM vwContractRequestLatest cR  --ContractRequest cR    COMMENTED ON 11 NOV 14 AFTER WORKFLOW CHANGES
		INNER JOIN MstUsers ON MstUsers.UsersId = cR.Addedby 
		LEFT OUTER JOIN MstContractType ct ON cR.ContractTypeId =ct.ContractTypeId 
		LEFT OUTER JOIN MstCountry con ON cR.CountryId=con.CountryId
		LEFT OUTER JOIN MstCurrency cu on cu.CurrencyId=cR.CurrencyID
		WHERE cr.ContractTypeId IN (SELECT ContractTypeId FROM @TmpContractType ) AND cr.ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),cr.ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),','))  AND  
		 cR.isCustomer = 'O'
		AND dbo.DateFormat(Cr.Addedon) BETWEEN dbo.DateOnly(@FromDate) AND dbo.DateOnly(@ToDate)
		AND cr.ContractID IS NOT NULL
		AND
		( ClientName LIKE '%'+ @Search +'%'  )
		ORDER BY 
		CASE WHEN @SortColumn='' AND @Direction = 0  THEN cR.RequestId END DESC,

		CASE WHEN @SortColumn =  'Other Name'  AND @Direction = 0 THEN cR.ClientName END ASC, 
		CASE WHEN @SortColumn =  'Requester Name'  AND @Direction = 0 THEN MstUsers.FullName  END ASC, 
		CASE WHEN @SortColumn =  'Contract ID'  AND @Direction = 0 THEN cR.ContractID END ASC, 
		CASE WHEN @SortColumn =  'Contract Type'  AND @Direction = 0 THEN ct.ContractTypeName END ASC,
		CASE WHEN @SortColumn =  'Effective Date'  AND @Direction = 0 THEN cR.EffectiveDate  END ASC, 
		CASE WHEN @SortColumn =  'Expiration Date'  AND @Direction = 0 THEN cr.ExpirationDate END ASC,
		CASE WHEN @SortColumn =  'Contract Value'  AND @Direction = 0 THEN cr.ContractValue END ASC,
		CASE WHEN @SortColumn =  'Geography'  AND @Direction = 0 THEN con.CountryName END ASC,

			
		CASE WHEN @SortColumn =  'Other Name'  AND @Direction = 1 THEN cR.ClientName END DESC, 
		CASE WHEN @SortColumn =  'Requester Name'  AND @Direction = 1 THEN MstUsers.FullName  END DESC, 
		CASE WHEN @SortColumn =  'Contract ID'  AND @Direction = 1 THEN cR.ContractID END DESC, 
		CASE WHEN @SortColumn =  'Contract Type'  AND @Direction = 1 THEN ct.ContractTypeName END DESC,
		CASE WHEN @SortColumn =  'Effective Date'  AND @Direction = 1 THEN cR.EffectiveDate  END DESC, 
		CASE WHEN @SortColumn =  'Expiration Date'  AND @Direction = 1 THEN cr.ExpirationDate END DESC,
		CASE WHEN @SortColumn =  'Contract Value'  AND @Direction = 1 THEN cr.ContractValue END DESC,
	    CASE WHEN @SortColumn =  'Geography'  AND @Direction = 1 THEN con.CountryName END DESC

		OFFSET ( @PageNo - 1 ) * @RecordsPerPage ROWS
		FETCH NEXT @RecordsPerPage ROWS ONLY
		 IF @Flags=0
	  BEGIN
		SELECT ISNULL(COUNT(1),0)TotalRecords  FROM vwContractRequestLatest   --ContractRequest     COMMENTED ON 11 NOV 14 AFTER WORKFLOW CHANGES  
		WHERE ContractTypeId IN (SELECT ContractTypeId FROM @TmpContractType ) AND ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),','))  AND        
		 isCustomer = 'O' AND ContractID IS NOT NULL
		AND dbo.DateOnly(Addedon) BETWEEN dbo.DateOnly(@FromDate) AND dbo.DateOnly(@ToDate)
		AND
		( ClientName LIKE '%'+ @Search +'%'  )

	END
	END

END













GO

/*

EXEC [OtherUpcomingRenewals_Report]

*/


ALTER PROCEDURE [dbo].[OtherUpcomingRenewals_Report]
@Flags INT = 0 , 
	@PageNo INT = 0 ,   
	@RecordsPerPage INT = 0,
	@FromDate DATETIME=NULL,
	@ToDate DATETIME=NULL,
	@ContractTypeId INT = NULL,
	@Search VARCHAR(10)='',
	@SortColumn VARCHAR(50)='Other Name',
	@Direction INT = 0,
	@UserID INT=NULL
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @TmpContractType TABLE (ContractTypeId INT)
	IF @ContractTypeId IS NULL
		BEGIN
		INSERT INTO @TmpContractType (ContractTypeId) 
		SELECT ContractTypeId FROM MstContractType
		END
	ELSE
	BEGIN
		INSERT INTO @TmpContractType (ContractTypeId) 
		SELECT @ContractTypeId
	END

		DECLARE @Role VARCHAR(50)
		IF @UserID IS NULL
		SET @Role='Super Admin'
	ELSE
		SET @Role=(SELECT MstRole.RoleName FROM MstUsers INNER JOIN MstRole ON MstUsers.RoleId=MstRole.RoleId AND MstUsers.UsersId=@UserID)

	IF @FromDate IS NULL SET @FromDate = (SELECT MIN(Addedon) FROM ContractRequest WHERE isCustomer = 'O')
	IF @ToDate IS NULL SET @ToDate = GETDATE()
	IF @PageNo <= 0 SET @PageNo = 1
	IF @RecordsPerPage = 0 SET @RecordsPerPage = (SELECT COUNT(1) FROM ContractRequest WHERE isCustomer = 'O')

	BEGIN
		
		SELECT [Renewal Notification Date],[Other Name], [Requester Name], [Contract ID], [Contract Type], 
		[Effective Date], [Expiration Date], [Renewal Notification Days]
		FROM
		(
		SELECT
		dbo.DateFormat(cR.RenewalDate) [Renewal Notification Date],
		cR.ClientName [Other Name], 
		MstUsers.FullName [Requester Name], 
		cR.ContractID [Contract ID], 
		ct.ContractTypeName [Contract Type],
		dbo.DateFormat(cR.EffectiveDate) [Effective Date],
		dbo.DateFormat(cr.ExpirationDate) [Expiration Date],
		--(CASE WHEN cr.RenewalAlert7days='Y' THEN '7,' else ' ' END  +''+ CASE WHEN cr.RenewalAlert30days='Y'  THEN '30,' ELSE ' ' END+''+CASE WHEN cr.RenewalAlert24Hrs='Y'  THEN '1' ELSE ' ' END)
		--[Renewal Notification Days] ,
        (CASE WHEN cr.RenewalAlert24Hrs='Y'  THEN '1,' ELSE ' ' END
		+''+CASE WHEN cr.RenewalAlert7days='Y' THEN '7,' else ' ' END  
		+''+ CASE WHEN cr.RenewalAlert30days='Y'  THEN '30,' ELSE ' ' END
		+''+ CASE WHEN cr.RenewalAlert60days='Y'  THEN '60,' ELSE ' ' END
		+''+ CASE WHEN cr.RenewalAlert90days='Y'  THEN '90,' ELSE ' ' END
		+''+ CASE WHEN cr.RenewalAlert180days='Y'  THEN '180' ELSE ' ' END	
		)[Renewal Notification Days] ,
		cr.RenewalDate,
		cr.RequestId,
		cR.ClientName,
		MstUsers.FullName, 
		cR.ContractID,
		ct.ContractTypeName,
		cR.EffectiveDate,
		cr.ExpirationDate
		
		
		FROM vwContractRequestLatest cR  --ContractRequest cR    COMMENTED ON 11 NOV 14 AFTER WORKFLOW CHANGES
		INNER JOIN MstUsers ON MstUsers.UsersId = cR.Addedby 
		LEFT OUTER JOIN MstContractType ct ON cR.ContractTypeId =ct.ContractTypeId 
		LEFT OUTER JOIN MstCountry con ON cR.CountryId=con.CountryId
		WHERE cr.ContractTypeId IN (SELECT ContractTypeId FROM @TmpContractType ) AND cr.ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),cr.ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),','))  AND  
		cR.isCustomer = 'O' 
		AND dbo.DateOnly(cR.RenewalDate) IS NOT NULL
		AND dbo.DateFormat(Cr.Addedon) BETWEEN dbo.DateOnly(@FromDate) AND dbo.DateOnly(@ToDate)
		AND cr.ContractID IS NOT NULL
		AND
		( ClientName LIKE '%'+ @Search +'%'  )
		)TMP
		ORDER BY 
		CASE WHEN @SortColumn='' AND @Direction = 0  THEN RenewalDate END ASC,
		CASE WHEN @SortColumn =  'Other Name'  AND @Direction = 0 THEN ClientName END ASC, 
		CASE WHEN @SortColumn =  'Requester Name'  AND @Direction = 0 THEN FullName  END ASC, 
		CASE WHEN @SortColumn =  'Contract ID'  AND @Direction = 0 THEN ContractID END ASC, 
		CASE WHEN @SortColumn =  'Contract Type'  AND @Direction = 0 THEN ContractTypeName END ASC,
		CASE WHEN @SortColumn =  'Effective Date'  AND @Direction = 0 THEN EffectiveDate  END ASC, 
		CASE WHEN @SortColumn =  'Expiration Date'  AND @Direction = 0 THEN ExpirationDate END ASC,
		CASE WHEN @SortColumn =  'Renewal Notification Date'  AND @Direction = 0 THEN RenewalDate END ASC,
		CASE WHEN @SortColumn =  'Renewal Notification Days'  AND @Direction = 0 THEN [Renewal Notification Days] END ASC,
		
		
		CASE WHEN @SortColumn =  'Other Name'  AND @Direction = 1 THEN ClientName END DESC, 
		CASE WHEN @SortColumn =  'Requester Name'  AND @Direction = 1 THEN FullName  END DESC, 
		CASE WHEN @SortColumn =  'Contract ID'  AND @Direction = 1 THEN ContractID END DESC, 
		CASE WHEN @SortColumn =  'Contract Type'  AND @Direction = 1 THEN ContractTypeName END DESC,
		CASE WHEN @SortColumn =  'Effective Date'  AND @Direction = 1 THEN EffectiveDate  END DESC, 
		CASE WHEN @SortColumn =  'Expiration Date'  AND @Direction = 1 THEN ExpirationDate END DESC,
		CASE WHEN @SortColumn =  'Renewal Notification Date'  AND @Direction = 1 THEN RenewalDate END DESC,
		CASE WHEN @SortColumn =  'Renewal Notification Days'  AND @Direction = 1 THEN [Renewal Notification Days] END DESC
		
		
		OFFSET ( @PageNo - 1 ) * @RecordsPerPage ROWS
		FETCH NEXT @RecordsPerPage ROWS ONLY
		 IF @Flags=0
	  BEGIN
		SELECT ISNULL(COUNT(1),0)TotalRecords  FROM vwContractRequestLatest   --ContractRequest     COMMENTED ON 11 NOV 14 AFTER WORKFLOW CHANGES  
		WHERE  ContractTypeId IN (SELECT ContractTypeId FROM @TmpContractType ) AND ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),','))  AND  
		isCustomer = 'O' AND ContractID IS NOT NULL
		AND dbo.DateOnly(RenewalDate) IS NOT NULL
		AND dbo.DateOnly(Addedon) BETWEEN dbo.DateOnly(@FromDate) AND dbo.DateOnly(@ToDate)
		AND
		( ClientName LIKE '%'+ @Search +'%'  )

	END
	END

END










GO








ALTER PROCEDURE [dbo].[SelectAddressDetails] --[SelectAddressDetails]  3095
@ClientId INT =NULL      
AS      
BEGIN     
DECLARE @strAddress VARCHAR(MAX)      
SELECT @strAddress= coalesce(@strAddress + '####', '') +CONVERT(VARCHAR,ClientAddressDetails.ClientId)+'~'+ Cast(ClientAddressDetails.CountryId as varchar) + ',' +  IIF(Address IS NULL OR Address='' ,' ',Address) + IIF(Street2 IS  
NULL OR Street2=''  ,' ',','+Street2+',<br>')+ISNULL(CityName+' ','')+ IIF(CityName IS NULL OR CityName='' ,' ','')+IIF(CONVERT(VARCHAR,PinCode) IS NULL OR CONVERT(VARCHAR,PinCode)='' ,' ','')+ISNULL(CONVERT(VARCHAR,PinCode),'')+  
IIF(CONVERT(VARCHAR,PinCode) IS NULL OR CONVERT(VARCHAR,PinCode)='' ,' ',',')+      
ISNULL(StateName,'')+IIF(StateName IS NULL OR StateName='' ,'',',')+      
ISNULL((SELECT DISTINCT ' '+CountryName FROM MstCountry WHERE CountryId=ClientAddressDetails.CountryId),' ') +'~'+CONVERT(VARCHAR,ClientAddressDetails.ClientAddressdetailId)  FROM ClientAddressDetails WHERE ClientId=@ClientId    
SELECT  '##AddressStart##'+ @strAddress +'##AddressEnd##'    
END
GO

ALTER PROCEDURE [dbo].[SelectContractIDAddressDetails] --[SelectContractIDAddressDetails] 100006
@ContractID INT=NULL
AS
BEGIN
DECLARE @Contact1 VARCHAR(MAX)
DECLARE @Contact2 VARCHAR(MAX)
DECLARE @Contact3 VARCHAR(MAX)

--SET @Contact1=(SELECT '--Contact 1--'+ ISNULL(Address,'')+IIF(Address IS NULL,'',',')+ IIF(Street2 IS
--NULL,'',','+Street2)+'<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ISNULL(CityName,'')+IIF(CityName IS NULl,'','-')+ISNULL(CONVERT(VARCHAR,PinCode),'')+IIF(CONVERT(VARCHAR,PinCode) IS NULL,'',',')+ISNULL(StateName,'')+IIF(StateName IS NULL,'',',')+  
--ISNULL((SELECT DISTINCT CountryName FROM MstCountry WHERE CountryId=vw.CountryId),'') +'~'+(SELECT TOP 1 CONVERT(VARCHAR(MAX),ClientAddressdetailId) FROM ClientAddressDetails WHERE Address=vw.Address AND ClientId=vw.ClientId)  
--FROM vwContractRequestLatest vw WHERE ContractID=@ContractID)

SET @Contact1=(SELECT '--Contact 1--'+ +IIF(Address IS NULL OR Address='' ,' ',Address) + IIF(Street2 IS
NULL OR Street2=''  ,' ',','+Street2+',<br>')+ISNULL(CityName+' ','')+ IIF(CityName IS NULL OR CityName='' ,' ','')+IIF(CONVERT(VARCHAR,PinCode) IS NULL OR CONVERT(VARCHAR,PinCode)='' ,' ','')+ISNULL(CONVERT(VARCHAR,PinCode),'')
+IIF(CONVERT(VARCHAR,PinCode) IS NULL OR CONVERT(VARCHAR,PinCode)='' ,' ',',')+  
ISNULL(StateName,'')+IIF(StateName IS NULL OR StateName='' ,'',',')+  
ISNULL((SELECT DISTINCT ' '+CountryName FROM MstCountry WHERE CountryId=vw.CountryId),' ')+'~'+(SELECT TOP 1 CONVERT(VARCHAR(MAX),ClientAddressdetailId) FROM ClientAddressDetails 
 WHERE  ISNULL(Address,'') = ISNULL(vw.Address,'')  
AND ISNULL(Street2,'')=ISNULL(vw.Street2,'') AND ISNULL(CityName,'')=ISNULL(vw.CityName,'') AND ISNULL(StateName,'')=ISNULL(vw.StateName,'') AND ISNULL(PinCode,'')=ISNULL(vw.PinCode,'') AND CountryId=vw.CountryId AND
ClientId=vw.ClientId)
FROM vwContractRequestLatest vw WHERE ContractID=@ContractID)

SET @Contact2='--Contact 2--'+ (SELECT ISNULL(CONVERT(VARCHAR(MAX),ContactNumber),'')+'~'+(SELECT TOP 1 CONVERT(VARCHAR(MAX),ClientContactdetailId) FROM ClientContactDetails INNER JOIN vwContractRequestLatest 
ON vwContractRequestLatest.ClientId=ClientContactDetails.ClientId WHERE ClientContactDetails.ContactNumber=vwContractRequestLatest.ContactNumber and vwContractRequestLatest.ContractID=@ContractID) FROM vwContractRequestLatest WHERE ContractID=@ContractID)  
SET @Contact3='--Contact 3--'+(SELECT ISNULL(EmailID,'')+'~'+(SELECT TOP 1 CONVERT(VARCHAR(MAX),ClientEmaildetailId) FROM ClientEmaildetails INNER JOIN vwContractRequestLatest ON vwContractRequestLatest.ClientId=ClientEmaildetails.ClientId 
 WHERE ClientEmaildetails.EmailID=vwContractRequestLatest.EmailID AND vwContractRequestLatest.ContractID=@ContractID ) FROM vwContractRequestLatest WHERE ContractID=@ContractID)+'--Contact 4--'  

SELECT ISNULL(@Contact1,'')+ISNULL(@Contact2,'')+ISNULL(@Contact3,'')  
END
GO


ALTER PROCEDURE [dbo].[SelectPrimaryContacts]
@RequestId INT=NULL
AS
BEGIN

DECLARE @Contact1 VARCHAR(MAX)
DECLARE @Contact2 VARCHAR(MAX)
DECLARE @Contact3 VARCHAR(MAX)

SET @Contact1=(SELECT '--Contact 1--'+ ISNULL(Address,'') +IIF (Address IS NULL,'',',')+ IIF(Street2 IS 

NULL,'',','+Street2)+'<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ISNULL(CityName,'')+IIF(CityName IS NULL,'','-')+ISNULL(CONVERT(VARCHAR,PinCode),'')+IIF(CONVERT(VARCHAR,PinCode) IS NULL,'',',')+ISNULL(StateName,'')+IIF(StateName IS NULL,'',',')+
----ISNULL((SELECT DISTINCT CountryName FROM MstCountry WHERE CountryId=ContractRequest.CountryId),'') +'~'+(SELECT TOP 1 CONVERT(VARCHAR(MAX),ClientAddressdetailId) FROM ClientAddressDetails WHERE Address=ContractRequest.Address AND CityName=ContractRequest.CityName AND StateName=ContractRequest.StateName AND PinCode=ContractRequest.PinCode AND CountryId=ContractRequest.CountryId   AND ClientAddressDetails.ClientId=ContractRequest.ClientId)
ISNULL((SELECT DISTINCT CountryName FROM CountryMst WHERE CountryId=ContractRequest.CountryId),'') +'~'+(SELECT TOP 1 CONVERT(VARCHAR(MAX),ClientAddressdetailId) FROM ClientAddressDetails WHERE ISNULL(Address,'')=ISNULL(ContractRequest.Address,'') AND ISNULL(CityName,'')=ISNULL(ContractRequest.CityName,'') AND ISNULL(StateName,'')=ISNULL(ContractRequest.StateName,'') AND ISNULL(PinCode,'')=ISNULL(ContractRequest.PinCode,'') AND CountryId=ContractRequest.CountryId   AND ClientAddressDetails.ClientId=ContractRequest.ClientId)
FROM ContractRequest WHERE RequestId=@RequestId)

SET @Contact2='--Contact 2--'+ (SELECT ISNULL(CONVERT(VARCHAR(MAX),ContactNumber),'')+'~'+(SELECT TOP 1 ISNULL(CONVERT(VARCHAR(MAX),ClientContactdetailId),0) FROM ClientContactDetails INNER JOIN ContractRequest ON ContractRequest.ClientId=ClientContactDetails.ClientId WHERE ClientContactDetails.ContactNumber=ContractRequest.ContactNumber and ContractRequest.RequestId=@RequestId) FROM ContractRequest WHERE RequestId=@RequestId)
SET @Contact3='--Contact 3--'+(SELECT ISNULL(EmailID,'')+'~'+(SELECT TOP 1 ISNULL(CONVERT(VARCHAR(MAX),ClientEmaildetailId),0) FROM ClientEmaildetails INNER JOIN ContractRequest ON ContractRequest.ClientId=ClientEmaildetails.ClientId  WHERE ClientEmaildetails.EmailID=ContractRequest.EmailID AND ContractRequest.RequestId=@RequestId ) FROM ContractRequest WHERE RequestId=@RequestId)+'--Contact 4--'

SELECT ISNULL(@Contact1,'')+ISNULL(@Contact2,'')+ISNULL(@Contact3,'')

END




GO


/*
EXEC StageApproval 0,0,90032, 1083,1
SELECT * FROM ##StageStatus
SELECT * FROM ContractTemplate
EXEC StageApproval @ContractId=0, @ContractTemplateId=0,@RequestId=90032,@ModifiedBy=1083,@GenerateContractFlag=0
*/
ALTER PROCEDURE [dbo].[StageApproval] --100004,1,110081,1083,1
@ContractId INT=NULL,
@ContractTemplateId INT,
@RequestId BIGINT=NULL,
@ModifiedBy INT=NULL,
@GenerateContractFlag INT=0,
@Approvaluri varchar(2000)='',
@UpdateFlag INT=0,
@Sender VARCHAR(50)=''
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @URL VARCHAR(2000)
	--SET @URL='https://app.contractpod.com/'+(SELECT TOP 1 URL FROM Subscribers WHERE DBName=DB_NAME())
	IF(@Approvaluri !='')
	 SET @URL=@Approvaluri;
	ELSE
	 SET @URL='https://app.contractpod.com/'+(SELECT TOP 1 URL FROM Subscribers WHERE DBName=DB_NAME())

	EXEC StageApprovalStatus @ContractId, @RequestId,@ContractTemplateId,@Sender

	IF @GenerateContractFlag = 0
	BEGIN	
			DECLARE @MinLevel INT =  (
											SELECT MIN(sC.LevelID)
											FROM
											##StageStatus t
											INNER JOIN StageConfigurator sC ON sC.StageID = t.StageId 
											LEFT OUTER JOIN ContractApprovals cA ON cA.StageID = t.StageId  AND cA.RequestId = @RequestId
											WHERE t.[Status]> 0  AND ISNULL(cA.IsActive,'Y')='Y' 
										)
				
				UPDATE ContractApprovals
				SET ModifiedOn=GETDATE(),
					ModifiedBy=@ModifiedBy
				WHERE StageId IN (SELECT StageId FROM ContractApprovals WHERE RequestId=@RequestId AND ContractId=@ContractId AND RequestId=@RequestId)

				INSERT INTO  ContractApprovals(ContractID,RequestId,StageID,AssignedTo,IP,Addedon,Addedby,IsMailSend,ModifiedOn,Modifiedby)
				SELECT @ContractId,@RequestId RequestId,tmp.StageID,CA.UsersId, ISNULL(CA.IP,''), GETDATE()[Date], COALESCE(CA.Modifiedby ,CA.Addedby,0)Addedby,'N',GETDATE(),	CA.Modifiedby
				FROM  ##StageStatus tmp
				INNER JOIN StageConfigurator  CA ON tmp.StageId = CA.StageID
				WHERE tmp.[Status]>0 AND tmp.StageId NOT IN(Select StageId From ContractApprovals WHERE RequestId=@RequestId AND ContractId=@ContractId)
				AND CA.LevelID = @MinLevel
				
			--IF @UpdateFlag=1
				UPDATE ContractApprovals 
				SET IsActive='Y'
				WHERE ContractApprovalId IN (
				SELECT ContractApprovalId FROM
				(
				SELECT ApprovalActivityId, ContractApprovals.ContractApprovalId,
				ROW_NUMBER()OVER (PARTITION BY ContractApprovals.ContractApprovalId ORDER BY ContractApprovalActivity.ApprovalActivityId DESC) RNo,
				ContractApprovalActivity.isApproved
				FROM ContractApprovals
				LEFT OUTER JOIN ContractApprovalActivity ON ContractApprovals.ContractApprovalId= ContractApprovalActivity.ContractApprovalId
				WHERE StageID IN
				(SELECT StageID FROM ##StageStatus WHERE [Status]>0) 
				AND ContractApprovals.RequestId=1                     --- comment by kk on 7 july 2016
				--AND ContractApprovals.RequestId=@RequestId 
				)tmp WHERE RNO = 1 AND COALESCE(isApproved,'N')='N'    --- comment by kk on 7 july 2016
				--AND COALESCE(isApproved,'Y')='Y'
				)
				IF (SELECT ContractStatusID FROM ContractRequest WHERE RequestId=@RequestId)!=7 --Dont get triggered if in Awaiting Approval stage
				BEGIN
						UPDATE ContractApprovals SET IsActive='N' WHERE StageID IN(SELECT StageID FROM ##StageStatus WHERE [Status]=0) AND RequestId=@RequestId  ---AND RequestId=@RequestId condition added by kk on 6 july 2016

						INSERT INTO ContractApprovalActivity(ContractApprovalId,isApproved,Remarks,Addedon,Addedby,IP, IsDefaultApproved)
						SELECT CA.ContractApprovalId,'Y','Default approved',GETDATE(), CA.Addedby, CA.IP,'Y'
						FROM ContractApprovals CA INNER JOIN  ##StageStatus tmp
						ON CA.StageID=tmp.StageID  AND CA.RequestId=@RequestId		
						WHERE  ISNULL(CA.IsActive,'Y')='N' AND CA.ContractApprovalId NOT IN (SELECT ContractApprovalActivity.ContractApprovalId FROM ContractApprovalActivity WHERE isApproved='Y')
				--IF dbo.IsTableExists('##StageStatus') > 0  DROP TABLE ##StageStatus
				END 

				IF NOT EXISTS(SELECT 1 FROM ContractApprovals WHERE (ISNULL(IsActive,'Y')='Y') AND RequestId=@RequestId)
				BEGIN	
				    DECLARE @ContractStatusIds int;
					SET @ContractStatusIds= (SELECT Top 1 ContractStatusID FROM ContractRequest_ht WHERE RequestId=@RequestId AND ContractStatusID!=7 ORDER BY RequesthtId DESC)

					----- add on 15 july 2016 
					IF @ContractStatusIds=1 
					 IF EXISTS (SELECT 1 FROM ContractRequest_ht WHERE RequestId=@RequestId AND ContractStatusID!=7 AND ContractStatusID!=1 ) 
					    SET @ContractStatusIds= (SELECT Top 1 ContractStatusID FROM ContractRequest_ht WHERE RequestId=@RequestId AND ContractStatusID!=7 AND ContractStatusID!=1 ORDER BY RequesthtId DESC)


						---EXEC ContractStatusAddUpdate @RequestId, 12, @ModifiedBy,NULL --Negotiation Stage
						EXEC ContractStatusAddUpdate @RequestId, @ContractStatusIds, @ModifiedBy,NULL 
				END
				ELSE
						EXEC ContractStatusAddUpdate @RequestId, 7, @ModifiedBy,NULL --Awaiting Approval
	END

	ELSE
	BEGIN					
						SELECT CA.ContractApprovalId,CA.RequestId, CA.ContractID,CA.StageID,StageName,CA.AssignedTo,CA.IP,CA.Addedon,CA.Addedby,
						MU.FullName
						,ISNULL(CA.IsActive,'Y') IsActive, CA.IsMailSend
						, FORMAT(COALESCE(CA.Modifiedon,GETDATE()),'d-MMM-yyyy h:mm tt') Modifiedon
						,'Approval.aspx?ContractApprovalId='+CAST(CA.ContractApprovalId AS VARCHAR(10)) +'&RequestId='+CAST(CA.RequestId AS VARCHAR(10))+'&ContractID='+ CAST(CA.ContractID AS VARCHAR(10))+'&StageId='+CAST(CA.StageID AS VARCHAR(10)) linkToApproval
						,MU.Email Email --,'vdudhal@myuberall.com, vinod@myuberall.com' Email--MU.Email
						,'Approval Request (Request ID: '+CAST(CA.RequestId AS VARCHAR(10)) +')' MailSubject
						,'<font face=Arial size=2 color=#006666> Hello<br/><br/>' + 

						'<u><b>Approval Required for Request ID:</b>�'+CAST(CA.RequestId AS VARCHAR(10)) + '</u><br/><br/>' + 

						'A request was completed by '+ (SELECT FullName FROM vwUsers WHERE vwUsers.UsersId=CR.Addedby) +
						' for a contract (request ID '+CAST(CA.RequestId AS VARCHAR(10)) + ').  At least one response requires your approval.  You can view the approval request by following the link <a href='''+@URL+'''>'+@URL +' </a> and logging on to ContractPod�.<br><br>  '+
						'Kind regards<br/><br/>The ContractPod� team</font><br/><br/>' +						
						'<font face=Arial size=1 color=#006666>Need help? Please do not hesitate to contact our technical support�team on 0800 699 0045 or at help@contractpod.com.' +
						'The information in this email is confidential and for use by the addressee(s) only. If you are not the intended recipient, please notify us immediately on 0800 699 0045 and delete the message from your computer. You may not copy or forward the e-mail, or use it or disclose its contents to any other person. We do not accept any liability or responsibility for changes made to this email after it was sent, or viruses transmitted through this e-mail or any attachment.</font>' MailBody


						--,'Dear '+MU.FullName+',<br> The '+StageName+' of contract ID '+CAST(CA.ContractID AS VARCHAR(10)) +' of '+CR.ClientName +' is waiting for your approval.<br><br>Thanks,<br>Contract Management Team.' MailBody
				FROM ContractApprovals CA
				INNER JOIN vwUsers MU ON CA.AssignedTo=MU.UsersId
				INNER JOIN StageConfigurator SC ON CA.StageID=SC.StageID
				INNER JOIN ContractRequest CR ON CR.RequestId=CA.RequestId
				WHERE CA.RequestId=@RequestId AND CA.ContractId=@ContractId
				ORDER BY SC.LevelID,SC.StageID

				SELECT * FROM 
				(SELECT CA.ContractApprovalId, 'Sent for approval on '+CAST(FORMAT(CA.AddedOn, 'd-MMM-yyyy h:mm tt') AS VARCHAR(30)) +' to '+ MU.FullName ApprovalActivity,  0 SRNO
				FROM ContractApprovals CA
				INNER JOIN vwUsers MU ON CA.AssignedTo=MU.UsersId
				WHERE CA.RequestId=@RequestId AND CA.ContractId=@ContractId

				UNION

				SELECT CA.ContractApprovalId, CASE WHEN ISNULL(IsDefaultApproved,'N')='Y' THEN 'Stage eliminated by system due to change in metadata.'
				WHEN CAA.isApproved='Y' THEN 'Approved'
				WHEN CAA.isApproved='N' THEN 'Disapproved'				
				ELSE CHAR(32) END + ' on '+ CAST(FORMAT(CAA.Addedon, 'd-MMM-yyyy h:mm tt') AS VARCHAR(30))
				+'<br/>- '+CAA.Remarks
					ApprovalActivity, --+' by '+ MU.FullName
				ROW_NUMBER() OVER(ORDER BY CAA.ApprovalActivityId ASC) SRNO
				FROM ContractApprovals CA
				LEFT OUTER JOIN ContractApprovalActivity CAA ON CA.ContractApprovalId=CAA.ContractApprovalId
				INNER JOIN vwUsers MU ON CA.AssignedTo=MU.UsersId
				WHERE CA.RequestId=@RequestId AND CA.ContractId=@ContractId	
				)TMP 
				WHERE ApprovalActivity IS NOT NULL
				ORDER BY SRNO ASC

				--DECLARE @ContractStatusIds INT;
				SELECT @ContractStatusIds=ContractStatusID FROM ContractRequest WHERE RequestId=@RequestId

				IF(@ContractStatusIds!=8 AND @ContractStatusIds!=9 AND @ContractStatusIds!=10 AND @ContractStatusIds!=11)    ----Awaiting signatures,Active Contract,Expired Contract,Terminated Contract
				BEGIN
					SELECT SC.StageID, SC.StageName, 'Assigned to '+MU.FullName AssignedToUser
					FROM  ##StageStatus tmp
					INNER JOIN StageConfigurator  SC ON tmp.StageId = SC.StageID
					INNER JOIN vwUsers MU ON SC.UsersId=MU.UsersId
					WHERE tmp.[Status]>0 AND tmp.StageId NOT IN(Select StageId From ContractApprovals WHERE RequestId=@RequestId )
					--AND SC.ContractTypeId= @ContractTypeId      --- added by kk on 4 July 2016 ::  "AND SC.ContractTypeId= @ContractTypeId"
				END
				ELSE
				BEGIN
				    SELECT SC.StageID, SC.StageName, 'Assigned to '+MU.FullName AssignedToUser
					FROM  ##StageStatus tmp
					INNER JOIN StageConfigurator  SC ON tmp.StageId = SC.StageID
					INNER JOIN vwUsers MU ON SC.UsersId=MU.UsersId
					WHERE tmp.[Status]>0 AND 1=0
				END
				
			END		
END








GO



/*
EXEC SupplierGeography_Report
*/


ALTER PROCEDURE [dbo].[SupplierGeography_Report]
@Flags INT = 0 ,
	@PageNo INT = 0 ,   
	@RecordsPerPage INT = 0,
	@FromDate DATETIME=NULL,
	@ToDate DATETIME=NULL,
	@ContractTypeId INT = NULL,
	@Search VARCHAR(10)='',
	@SortColumn VARCHAR(20)='Supplier Name',
	@Direction INT = 0,
	@UserID int=null
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @TmpContractType TABLE (ContractTypeId INT)
	IF @ContractTypeId IS NULL
		BEGIN
		INSERT INTO @TmpContractType (ContractTypeId) 
		SELECT ContractTypeId FROM MstContractType
		END
	ELSE
	BEGIN
		INSERT INTO @TmpContractType (ContractTypeId) 
		SELECT @ContractTypeId
	END

	DECLARE @Role VARCHAR(50)
	IF @UserID IS NULL
		SET @Role='Super Admin'
	ELSE
		SET @Role=(SELECT MstRole.RoleName FROM MstUsers INNER JOIN MstRole ON MstUsers.RoleId=MstRole.RoleId AND MstUsers.UsersId=@UserID)

	IF @FromDate IS NULL SET @FromDate = (SELECT MIN(Addedon) FROM ContractRequest WHERE isCustomer IS NULL OR isCustomer = 'N')
	IF @ToDate IS NULL SET @ToDate = GETDATE()
	IF @PageNo <= 0 SET @PageNo = 1
	IF @RecordsPerPage = 0 SET @RecordsPerPage = (SELECT COUNT(1) FROM ContractRequest WHERE isCustomer IS NULL OR isCustomer = 'N')

	BEGIN
		
		SELECT 
		cR.ClientName [Supplier Name], 
		MstUsers.FullName [Requester Name], 
		cR.ContractID [Contract ID], 
		ct.ContractTypeName [Contract Type],
		dbo.DateFormat(cR.EffectiveDate) [Effective Date],
		dbo.DateFormat(cr.ExpirationDate) [Expiration Date],
		con.CountryName [Geography]
		FROM  vwContractRequestLatest cR  --ContractRequest cR    COMMENTED ON 11 NOV 14 AFTER WORKFLOW CHANGES
		INNER JOIN MstUsers ON MstUsers.UsersId = cR.Addedby 
		LEFT OUTER JOIN MstContractType ct ON cR.ContractTypeId =ct.ContractTypeId 
		LEFT OUTER JOIN MstCountry con ON cR.CountryId=con.CountryId
		WHERE cr.ContractTypeId IN (SELECT ContractTypeId FROM @TmpContractType ) AND cr.ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),cr.ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),','))  AND  
		(cR.isCustomer IS NULL OR cR.isCustomer = 'N')
		AND dbo.DateFormat(Cr.Addedon) BETWEEN dbo.DateOnly(@FromDate) AND dbo.DateOnly(@ToDate)
		AND cr.ContractID IS NOT NULL
		AND
		( ClientName LIKE '%'+ @Search +'%'  )
		ORDER BY 
		CASE WHEN @SortColumn='' AND @Direction = 0  THEN cR.RequestId END DESC,

		CASE WHEN @SortColumn =  'Supplier Name'  AND @Direction = 0 THEN cR.ClientName END ASC, 
		CASE WHEN @SortColumn =  'Requester Name'  AND @Direction = 0 THEN MstUsers.FullName  END ASC, 
		CASE WHEN @SortColumn =  'Contract ID'  AND @Direction = 0 THEN cR.ContractID END ASC, 
		CASE WHEN @SortColumn =  'Contract Type'  AND @Direction = 0 THEN ct.ContractTypeName END ASC,
		CASE WHEN @SortColumn =  'Effective Date'  AND @Direction = 0 THEN cR.EffectiveDate  END ASC, 
		CASE WHEN @SortColumn =  'Expiration Date'  AND @Direction = 0 THEN cr.ExpirationDate END ASC,
		CASE WHEN @SortColumn =  'Geography'  AND @Direction = 0 THEN con.CountryName END ASC,

			
		CASE WHEN @SortColumn =  'Supplier Name'  AND @Direction = 1 THEN cR.ClientName END DESC, 
		CASE WHEN @SortColumn =  'Requester Name'  AND @Direction = 1 THEN MstUsers.FullName  END DESC, 
		CASE WHEN @SortColumn =  'Contract ID'  AND @Direction = 1 THEN cR.ContractID END DESC, 
		CASE WHEN @SortColumn =  'Contract Type'  AND @Direction = 1 THEN ct.ContractTypeName END DESC,
		CASE WHEN @SortColumn =  'Effective Date'  AND @Direction = 1 THEN cR.EffectiveDate  END DESC, 
		CASE WHEN @SortColumn =  'Expiration Date'  AND @Direction = 1 THEN cr.ExpirationDate END DESC,
	    CASE WHEN @SortColumn =  'Geography'  AND @Direction = 1 THEN con.CountryName END DESC

		OFFSET ( @PageNo - 1 ) * @RecordsPerPage ROWS
		FETCH NEXT @RecordsPerPage ROWS ONLY
IF @Flags=0
	  BEGIN
		SELECT ISNULL(COUNT(1),0)TotalRecords  FROM  vwContractRequestLatest   --ContractRequest     COMMENTED ON 11 NOV 14 AFTER WORKFLOW CHANGES  
		WHERE ContractTypeId IN (SELECT ContractTypeId FROM @TmpContractType ) AND ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),','))  AND  
		(isCustomer IS NULL OR isCustomer = 'N') AND ContractID IS NOT NULL
		AND dbo.DateOnly(Addedon) BETWEEN dbo.DateOnly(@FromDate) AND dbo.DateOnly(@ToDate)
		AND
		( ClientName LIKE '%'+ @Search +'%'  )

	END
	END

END












GO


/*
EXEC [SupplierTypeValueGeography_Report]
*/


ALTER PROCEDURE [dbo].[SupplierTypeValueGeography_Report]
@Flags INT = 0 ,
	@PageNo INT = 0 ,   
	@RecordsPerPage INT = 0,
	@FromDate DATETIME=NULL,
	@ToDate DATETIME=NULL,
	@ContractTypeId INT = NULL,
	@Search VARCHAR(10)='',
	@SortColumn VARCHAR(20)='Supplier Name',
	@Direction INT = 0,
	@UserID int=null
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @TmpContractType TABLE (ContractTypeId INT)
	IF @ContractTypeId IS NULL
		BEGIN
		INSERT INTO @TmpContractType (ContractTypeId) 
		SELECT ContractTypeId FROM MstContractType
		END
	ELSE
	BEGIN
		INSERT INTO @TmpContractType (ContractTypeId) 
		SELECT @ContractTypeId
	END

	  		DECLARE @Role VARCHAR(50)
		IF @UserID IS NULL
		SET @Role='Super Admin'
	ELSE
		SET @Role=(SELECT MstRole.RoleName FROM MstUsers INNER JOIN MstRole ON MstUsers.RoleId=MstRole.RoleId AND MstUsers.UsersId=@UserID)

	IF @FromDate IS NULL SET @FromDate = (SELECT MIN(Addedon) FROM ContractRequest WHERE isCustomer IS NULL OR isCustomer = 'N')
	IF @ToDate IS NULL SET @ToDate = GETDATE()
	IF @PageNo <= 0 SET @PageNo = 1
	IF @RecordsPerPage = 0 SET @RecordsPerPage = (SELECT COUNT(1) FROM ContractRequest WHERE isCustomer IS NULL OR isCustomer = 'N')

	BEGIN
		
		SELECT 
		cR.ClientName [Supplier Name], 
		MstUsers.FullName [Requester Name], 
		cR.ContractID [Contract ID], 
		ct.ContractTypeName [Contract Type],
		dbo.DateFormat(cR.EffectiveDate) [Effective Date],
		dbo.DateFormat(cr.ExpirationDate) [Expiration Date],
		--cR.ContractValue [Contract Value],
		CONVERT(VARCHAR(200), Format(cr.ContractValue ,'N','en-US' ))+' ('+cu.CurrencyCode+')' [Contract Value],
		con.CountryName [Geography]
		FROM vwContractRequestLatest cR  --ContractRequest cR    COMMENTED ON 11 NOV 14 AFTER WORKFLOW CHANGES
		INNER JOIN MstUsers ON MstUsers.UsersId = cR.Addedby 
		LEFT OUTER JOIN MstContractType ct ON cR.ContractTypeId =ct.ContractTypeId 
		LEFT OUTER JOIN MstCountry con ON cR.CountryId=con.CountryId
		LEFT OUTER JOIN MstCurrency cu on cu.CurrencyId=cR.CurrencyID
		WHERE cr.ContractTypeId IN (SELECT ContractTypeId FROM @TmpContractType ) AND cr.ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),cr.ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),','))  AND     
		(cR.isCustomer IS NULL OR cR.isCustomer = 'N')
		AND dbo.DateFormat(Cr.Addedon) BETWEEN dbo.DateOnly(@FromDate) AND dbo.DateOnly(@ToDate)
		AND cr.ContractID IS NOT NULL
		AND
		( ClientName LIKE '%'+ @Search +'%'  )
		ORDER BY 
		CASE WHEN @SortColumn='' AND @Direction = 0  THEN cR.RequestId END DESC,

		CASE WHEN @SortColumn =  'Supplier Name'  AND @Direction = 0 THEN cR.ClientName END ASC, 
		CASE WHEN @SortColumn =  'Requester Name'  AND @Direction = 0 THEN MstUsers.FullName  END ASC, 
		CASE WHEN @SortColumn =  'Contract ID'  AND @Direction = 0 THEN cR.ContractID END ASC, 
		CASE WHEN @SortColumn =  'Contract Type'  AND @Direction = 0 THEN ct.ContractTypeName END ASC,
		CASE WHEN @SortColumn =  'Effective Date'  AND @Direction = 0 THEN cR.EffectiveDate  END ASC, 
		CASE WHEN @SortColumn =  'Expiration Date'  AND @Direction = 0 THEN cr.ExpirationDate END ASC,
		CASE WHEN @SortColumn =  'Contract Value'  AND @Direction = 0 THEN cr.ContractValue END ASC,
		CASE WHEN @SortColumn =  'Geography'  AND @Direction = 0 THEN con.CountryName END ASC,

			
		CASE WHEN @SortColumn =  'Supplier Name'  AND @Direction = 1 THEN cR.ClientName END DESC, 
		CASE WHEN @SortColumn =  'Requester Name'  AND @Direction = 1 THEN MstUsers.FullName  END DESC, 
		CASE WHEN @SortColumn =  'Contract ID'  AND @Direction = 1 THEN cR.ContractID END DESC, 
		CASE WHEN @SortColumn =  'Contract Type'  AND @Direction = 1 THEN ct.ContractTypeName END DESC,
		CASE WHEN @SortColumn =  'Effective Date'  AND @Direction = 1 THEN cR.EffectiveDate  END DESC, 
		CASE WHEN @SortColumn =  'Expiration Date'  AND @Direction = 1 THEN cr.ExpirationDate END DESC,
		CASE WHEN @SortColumn =  'Contract Value'  AND @Direction = 1 THEN cr.ContractValue END DESC,
	    CASE WHEN @SortColumn =  'Geography'  AND @Direction = 1 THEN con.CountryName END DESC

		OFFSET ( @PageNo - 1 ) * @RecordsPerPage ROWS
		FETCH NEXT @RecordsPerPage ROWS ONLY
		  IF @Flags=0
	  BEGIN
		SELECT ISNULL(COUNT(1),0)TotalRecords  FROM vwContractRequestLatest cR  --ContractRequest cR    COMMENTED ON 11 NOV 14 AFTER WORKFLOW CHANGES  
		WHERE ContractTypeId IN (SELECT ContractTypeId FROM @TmpContractType ) AND cr.ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),cr.ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),','))  AND     
		(isCustomer IS NULL OR isCustomer = 'N') AND ContractID IS NOT NULL
		AND dbo.DateOnly(Addedon) BETWEEN dbo.DateOnly(@FromDate) AND dbo.DateOnly(@ToDate)
		AND
		( ClientName LIKE '%'+ @Search +'%'  )

	END
	END

END












GO


/*
EXEC SupplierUpcomingRenewals_Report
*/


ALTER PROCEDURE [dbo].[SupplierUpcomingRenewals_Report]
@Flags INT = 0 ,
	@PageNo INT = 0 ,   
	@RecordsPerPage INT = 0,
	@FromDate DATETIME=NULL,
	@ToDate DATETIME=NULL,
	@ContractTypeId INT = NULL,
	@Search VARCHAR(10)='',
	@SortColumn VARCHAR(50)='Supplier Name',
	@Direction INT = 0,
	@UserID int=null
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @TmpContractType TABLE (ContractTypeId INT)
	IF @ContractTypeId IS NULL
		BEGIN
		INSERT INTO @TmpContractType (ContractTypeId) 
		SELECT ContractTypeId FROM MstContractType
		END
	ELSE
	BEGIN
		INSERT INTO @TmpContractType (ContractTypeId) 
		SELECT @ContractTypeId
	END

		DECLARE @Role VARCHAR(50)
		IF @UserID IS NULL
		SET @Role='Super Admin'
	ELSE
		SET @Role=(SELECT MstRole.RoleName FROM MstUsers INNER JOIN MstRole ON MstUsers.RoleId=MstRole.RoleId AND MstUsers.UsersId=@UserID)


	IF @FromDate IS NULL SET @FromDate = (SELECT MIN(Addedon) FROM ContractRequest WHERE isCustomer IS NULL OR isCustomer = 'N')
	IF @ToDate IS NULL SET @ToDate = GETDATE()
	IF @PageNo <= 0 SET @PageNo = 1
	IF @RecordsPerPage = 0 SET @RecordsPerPage = (SELECT COUNT(1) FROM ContractRequest WHERE isCustomer IS NULL OR isCustomer = 'N')

	BEGIN
		
		SELECT [Renewal Notification Date], [Supplier Name], [Requester Name], [Contract ID], [Contract Type], 
		[Effective Date], [Expiration Date], [Renewal Notification Days]
		FROM
		(
		SELECT
		dbo.DateFormat(cR.RenewalDate) [Renewal Notification Date],
		cR.ClientName [Supplier Name], 
		MstUsers.FullName [Requester Name], 
		cR.ContractID [Contract ID], 
		ct.ContractTypeName [Contract Type],
		dbo.DateFormat(cR.EffectiveDate) [Effective Date],
		dbo.DateFormat(cr.ExpirationDate) [Expiration Date],
		--(CASE WHEN cr.RenewalAlert7days='Y' THEN '7,' else ' ' END  +''+CASE WHEN cr.RenewalAlert30days='Y'  THEN '30,' ELSE ' ' END+''+CASE WHEN cr.RenewalAlert24Hrs='Y'  THEN '1' ELSE ' ' END)
		--[Renewal Notification Days],
        (CASE WHEN cr.RenewalAlert24Hrs='Y'  THEN '1,' ELSE ' ' END
		+''+CASE WHEN cr.RenewalAlert7days='Y' THEN '7,' else ' ' END  
		+''+ CASE WHEN cr.RenewalAlert30days='Y'  THEN '30,' ELSE ' ' END
		+''+ CASE WHEN cr.RenewalAlert60days='Y'  THEN '60,' ELSE ' ' END
		+''+ CASE WHEN cr.RenewalAlert90days='Y'  THEN '90,' ELSE ' ' END
		+''+ CASE WHEN cr.RenewalAlert180days='Y'  THEN '180' ELSE ' ' END	
		)[Renewal Notification Days],
		cr.RenewalDate,
		cr.RequestId,
		cR.ClientName,
		MstUsers.FullName, 
		cR.ContractID,
		ct.ContractTypeName,
		cR.EffectiveDate,
		cr.ExpirationDate
		
		
		FROM vwContractRequestLatest cR  --ContractRequest cR    COMMENTED ON 11 NOV 14 AFTER WORKFLOW CHANGES
		INNER JOIN MstUsers ON MstUsers.UsersId = cR.Addedby 
		LEFT OUTER JOIN MstContractType ct ON cR.ContractTypeId =ct.ContractTypeId 
		LEFT OUTER JOIN MstCountry con ON cR.CountryId=con.CountryId
		WHERE cr.ContractTypeId IN (SELECT ContractTypeId FROM @TmpContractType ) AND cr.ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),cr.ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),','))  AND     
		(cR.isCustomer IS NULL OR cR.isCustomer = 'N')
		AND dbo.DateOnly(cR.RenewalDate) IS NOT NULL
		AND dbo.DateOnly(Cr.Addedon) BETWEEN dbo.DateOnly(@FromDate) AND dbo.DateOnly(@ToDate)
		AND cr.ContractID IS NOT NULL
		AND
		( ClientName LIKE '%'+ @Search +'%'  )
		)TMP
		ORDER BY 
		CASE WHEN @SortColumn='' AND @Direction = 0  THEN RenewalDate END ASC,
		CASE WHEN @SortColumn =  'Supplier Name'  AND @Direction = 0 THEN ClientName END ASC, 
		CASE WHEN @SortColumn =  'Requester Name'  AND @Direction = 0 THEN FullName  END ASC, 
		CASE WHEN @SortColumn =  'Contract ID'  AND @Direction = 0 THEN ContractID END ASC, 
		CASE WHEN @SortColumn =  'Contract Type'  AND @Direction = 0 THEN ContractTypeName END ASC,
		CASE WHEN @SortColumn =  'Effective Date'  AND @Direction = 0 THEN EffectiveDate  END ASC, 
		CASE WHEN @SortColumn =  'Expiration Date'  AND @Direction = 0 THEN ExpirationDate END ASC,
		CASE WHEN @SortColumn =  'Renewal Notification Date'  AND @Direction = 0 THEN RenewalDate END ASC,
		CASE WHEN @SortColumn =  'Renewal Notification Days'  AND @Direction = 0 THEN [Renewal Notification Days] END ASC,
		
		
		CASE WHEN @SortColumn =  'Supplier Name'  AND @Direction = 1 THEN ClientName END DESC, 
		CASE WHEN @SortColumn =  'Requester Name'  AND @Direction = 1 THEN FullName  END DESC, 
		CASE WHEN @SortColumn =  'Contract ID'  AND @Direction = 1 THEN ContractID END DESC, 
		CASE WHEN @SortColumn =  'Contract Type'  AND @Direction = 1 THEN ContractTypeName END DESC,
		CASE WHEN @SortColumn =  'Effective Date'  AND @Direction = 1 THEN EffectiveDate  END DESC, 
		CASE WHEN @SortColumn =  'Expiration Date'  AND @Direction = 1 THEN ExpirationDate END DESC,
		CASE WHEN @SortColumn =  'Renewal Notification Date'  AND @Direction = 1 THEN RenewalDate END DESC,
		CASE WHEN @SortColumn =  'Renewal Notification Days'  AND @Direction = 1 THEN [Renewal Notification Days] END DESC
		
		
		OFFSET ( @PageNo - 1 ) * @RecordsPerPage ROWS
		FETCH NEXT @RecordsPerPage ROWS ONLY
		 IF @Flags=0
	  BEGIN
		SELECT ISNULL(COUNT(1),0)TotalRecords  FROM vwContractRequestLatest   --ContractRequest     COMMENTED ON 11 NOV 14 AFTER WORKFLOW CHANGES  
		WHERE ContractTypeId IN (SELECT ContractTypeId FROM @TmpContractType ) AND ContractTypeId IN (SELECT items FROM dbo.Split((CASE WHEN @Role='Super Admin' THEN CONVERT(VARCHAR(10),ContractTypeId)
		ELSE dbo.GetContractTypeAccess(@Role) END),','))  AND     
		(isCustomer IS NULL OR isCustomer = 'N') AND ContractID IS NOT NULL
		AND dbo.DateOnly(RenewalDate) IS NOT NULL
		AND dbo.DateOnly(Addedon) BETWEEN dbo.DateOnly(@FromDate) AND dbo.DateOnly(@ToDate)
		AND
		( ClientName LIKE '%'+ @Search +'%'  )

	END
	END

END













GO







ALTER PROCEDURE [dbo].[UpdateURLLastRunTimeForWindowsService]

@URLForWindowsService VARCHAR(500)=null,
@Status INT OUT

AS
BEGIN

UPDATE [CMSCentral].[dbo].[WindowsServices]
			SET [LastRunTime]=GETDATE()
			WHERE LTRIM(RTRIM([URL]))= LTRIM(RTRIM(@URLForWindowsService))

			SET @Status = 1

END










/*

SELECT * FROm vwRequestDetails 

*/

GO
ALTER VIEW [dbo].[vwGlobalSearchDetails]
	AS

	
	SELECT DISTINCT
	cR.RequestId, cR.ContractID, MstContractType.ContractTypeName, vwRequestType.RequestTypeName ,
	ContractTemplate.ContractTemplateName ,cR.ClientName,
	cR.[Address], cR.CityName, cR.StateName, MstCountry.CountryName, cR.PinCode, cR.RequestDescription, dbo.DateOnly(cR.DeadlineDate)DeadlineDate,
	dbo.DateOnly(cR.ContractExpiryDate) ContractExpiryDate, cR.EstimatedValue,
	cR.ContactNumber, cR.EmailID,
	qA.Question ansQuestion,
	qA.TextValue ansTextValue ,
	qA.NumericValue ansNumericValue,
	qA.DecimalValue ansDecimalValue,
	qA.DateValue ansDateValue,
	MstActivityType.ActivityTypeName,
	ActivityStatus.StatusName ActivityStatusName,
	dbo.StripHTML(ActivityText) ActivityText,
	dbo.DateOnly(ActivityDate) ActivityDate,
	dbo.DateOnly(ReminderDate)ReminderDate,
	dbo.DateOnly(cR.Addedon)Addedon,
	MstUsers.FullName AssignedTo
	FROM dbo.vwGlobalSearch cR
	LEFT OUTER JOIN MstCountry ON MstCountry.CountryId = cR.CountryId
	LEFT OUTER JOIN vwRequestType ON vwRequestType.RequestTypeID = cR.RequestTypeId
	LEFT OUTER JOIN MstContractType ON MstContractType.ContractTypeId = cR.ContractTypeId
	LEFT OUTER JOIN ContractTemplate ON ContractTemplate.ContractTemplateId = cR.ContractTemplateId
	LEFT OUTER JOIN ContractQuestionsAnswers qA ON qA.RequestId = cR.RequestId
	LEFT OUTER JOIN Activity ON Activity.RequestId = cR.RequestId
	LEFT OUTER JOIN MstActivityType ON MstActivityType.ActivityTypeId = Activity.ActivityTypeId
	LEFT OUTER JOIN ActivityStatus ON ActivityStatus.ActivityStatusId = Activity.ActivityStatusId
	LEFT OUTER JOIN MstUsers ON ( MstUsers.UsersId = cR.AssignToUserID OR MstUsers.UsersId = cR.RequesterUserId)
	LEFT OUTER JOIN Client ON CLient.ClientId=cR.ClientId


	
	
























--exec FillMstViewData
	/*	
	SELECT * FROM [vwMstView] WHERE TableName = 'MstContractType'	
	*/
	GO
	ALTER VIEW [dbo].[vwMstView]
	AS

	SELECT TOP 1000000 Id, Name, TableName, ParentId, isActive,PickListMasterId,PickListMasterName,  Sequence, NameDescription
	FROM 
	(	
	
	SELECT  CONVERT(VARCHAR(200), CountryId) AS Id, CountryName  AS Name, 'MstCountry' AS TableName, 0 AS ParentId, isActive, 3 PickListMasterId , 'Country' PickListMasterName, 0 Sequence, NULL NameDescription
	FROM MstCountry
	UNION ALL
	SELECT  CONVERT(VARCHAR(200), DepartmentId) AS Id, DepartmentName  AS Name, 'MstDepartment' AS TableName, 0 AS ParentId, isActive, 4 PickListMasterId , 'Department' PickListMasterName, 0 Sequence, NULL NameDescription
	FROM MstDepartment
	UNION ALL
	--SELECT  CONVERT(VARCHAR(200), ContractTemplateId) AS Id, ContractTemplateName  AS Name, 'ContractTemplate' AS TableName, ContractTypeId AS ParentId, isActive, 5 PickListMasterId , 'ContractTemplate' PickListMasterName, 0 Sequence, NULL NameDescription
	--FROM ContractTemplate
	SELECT  CONVERT(VARCHAR(200), CAST(ContractTemplateId AS VARCHAR(2000))+'##'+TemplateFileName) AS Id, ContractTemplateName  AS Name, 'ContractTemplate' AS TableName, ContractTypeId AS ParentId, isActive, 5 PickListMasterId , 'ContractTemplate' PickListMasterName, 0 Sequence, NULL NameDescription
	FROM ContractTemplate --WHERE (IsBulkImport IS NULL OR IsBulkImport='N')
	UNION ALL	
	SELECT  CONVERT(VARCHAR(200), UsersId) AS Id, FullName  AS Name, 'vwUsersWithDepartMent' AS TableName, DepartmentId AS ParentId,isActive, 6 PickListMasterId , 'Users' PickListMasterName, 0 Sequence, NULL NameDescription
	FROM vwUsersWithDepartMent
	UNION ALL
	SELECT CONVERT(VARCHAR(200),ContractTypeId) AS Id, ContractTypeName AS Name, 'MstContractType' AS TableName,0 AS ParentId, isActive, 7 PickListMasterId , 'ContractType' PickListMasterName, 0 Sequence, NULL NameDescription
	FROM MstContractType  
	UNION ALL
	SELECT CONVERT(VARCHAR(200),ContractTemplateId) AS Id, ContractTemplateName AS Name, 'ContractTemplateWithInt' AS TableName,ContractTypeId AS ParentId, isActive, 8 PickListMasterId , 'ContractTemplateWithInt' PickListMasterName, 0 Sequence, NULL NameDescription
	FROM ContractTemplate WHERE (IsBulkImport IS NULL OR IsBulkImport='N')
	UNION ALL	
	
	SELECT CONVERT(VARCHAR(200),FieldLibraryID) AS Id, FieldName AS Name, 'vwFieldList' AS TableName,ContractTemplateId AS ParentId, 'Y' isActive, 9 PickListMasterId , 'vwFieldList' PickListMasterName, 0 Sequence, NULL NameDescription
	FROM vwFieldList  

	UNION ALL	
	SELECT   CONVERT(VARCHAR(200), ClauseID) AS Id, ClauseName AS Name, 'Clause' AS TableName, ContractTypeId AS ParentId, isActive, 10 PickListMasterId, 'Clause' PickListMasterName, 0 Sequence, NULL NameDescription
	FROM Clause 

	UNION ALL	
	SELECT   CONVERT(VARCHAR(200), UsersId) AS Id, FullName AS Name, 'MstUsers' AS TableName, 0 AS ParentId, isActive, 11 PickListMasterId, 'MstUsers' PickListMasterName, 0 Sequence, NULL NameDescription
	FROM MstUsers where isnull(isdeleted,'N')<>'Y'

	UNION ALL	
	SELECT   CONVERT(VARCHAR(200), ContractStatusID) AS Id, StatusName AS Name, 'vwContractStatus' AS TableName, 0 AS ParentId, 'Y' isActive, 12 PickListMasterId, 'ContractStatus' PickListMasterName, 0 Sequence, NULL NameDescription
	FROM vwContractStatus 

	
	UNION ALL	
	SELECT   CONVERT(VARCHAR(200), Id) AS Id, CustomerType AS Name, 'mstCustomerType' AS TableName, 0 AS ParentId, 'Y' isActive, 13 PickListMasterId, 'CustomerType' PickListMasterName, 0 Sequence, NULL NameDescription
	FROM mstCustomerType 

	UNION ALL	
	SELECT   CONVERT(VARCHAR(200), ContractingPartyId) AS Id, ContractingPartyName AS Name, 'mstContractingParty' AS TableName, 0 AS ParentId, isActive, 14 PickListMasterId, 'ContractingParty' PickListMasterName, 0 Sequence, NULL NameDescription
	FROM mstContractingParty 

	UNION ALL	
	SELECT   CONVERT(VARCHAR(200), CurrencyId) AS Id, CurrencyCode AS Name, 'mstCurrency' AS TableName, 0 AS ParentId,isActive, 16 PickListMasterId, 'Currency' PickListMasterName, 0 Sequence, NULL NameDescription
	FROM mstCurrency

	UNION ALL
	SELECT   CONVERT(VARCHAR(20), OptionValue) AS Id, OptionText AS Name, 'vwOptions' AS TableName, 0 AS ParentId,  isActive, 17 PickListMasterId, 'Options' PickListMasterName,  Sequence, NULL NameDescription
	FROM vwOptions --ORDER BY Sequence
	--SELECT   CONVERT(VARCHAR(20), OptionID) AS Id, OptionText AS Name, 'vwOptions' AS TableName, 0 AS ParentId,  isActive, 17 PickListMasterId, 'Options' PickListMasterName,  Sequence, NULL NameDescription
	--FROM vwOptions --ORDER BY Sequence

	UNION ALL
	SELECT   CONVERT(VARCHAR(20), PriorityValue) AS Id, PriorityText AS Name, 'MstPriority' AS TableName, 0 AS ParentId,  isActive, 18 PickListMasterId, 'Priority' PickListMasterName,  Sequence, NULL NameDescription
	FROM MstPriority --ORDER BY Sequence

	UNION ALL	

	-- dynamic fields put it last
	SELECT CONVERT(VARCHAR(200), FieldLibraryValues.FieldValueID ) AS Id,
	FieldLibraryValues.FieldValue,FieldLibrary.FieldName+'_' + CAST(FieldLibrary.FieldLibraryID AS VARCHAR(10)) TableName, 0 ParentId,'Y',NULL,FieldLibrary.FieldName PickListMasterName, ISNULL(FieldLibraryValues.Sequence,0) Sequence, NULL NameDescription
	FROM FieldLibrary
	INNER JOIN FieldLibraryValues ON FieldLibraryValues.FieldLibraryID  = FieldLibrary.FieldLibraryID
	WHERE FieldTypeID IN (8,10,16,20)

	UNION ALL	
	select CONVERT(VARCHAR(200), FieldOptionId) Id,FieldOptionValue Name,'MetaDataConfiguratorValues' TableName, 0 ParentId,'Y' isActive,FieldId PickListMasterId,
	(SELECT FieldName FROM MetaDataConfigurator WHERE MetaDataConfigurator.FieldId=MetaDataConfiguratorValues.FieldId)
	PickListMasterName, 0 Sequence, NULL NameDescription from MetaDataConfiguratorValues
	WHERE FieldId IN(SELECT FieldId FROM MetaDataConfigurator)
	
	)tmp 
	
	ORDER BY TableName, Name 

	































/*

SELECT * FROm vwRequestDetails 

*/
GO

ALTER VIEW [dbo].[vwRequestDetails]
	AS

	
	SELECT DISTINCT
	cR.RequestId, cR.ContractID, MstContractType.ContractTypeName, vwRequestType.RequestTypeName ,
	ContractTemplate.ContractTemplateName ,cR.ClientName,
	cR.[Address], cR.CityName, cR.StateName, MstCountry.CountryName, cR.PinCode, cR.RequestDescription, dbo.DateOnly(cR.DeadlineDate)DeadlineDate,
	dbo.DateOnly(cR.ContractExpiryDate) ContractExpiryDate, cR.EstimatedValue,
	cR.ContactNumber, cR.EmailID,
	qA.Question ansQuestion,
	qA.TextValue ansTextValue ,
	qA.NumericValue ansNumericValue,
	qA.DecimalValue ansDecimalValue,
	qA.DateValue ansDateValue,
	MstActivityType.ActivityTypeName,
	ActivityStatus.StatusName ActivityStatusName,
	dbo.StripHTML(ActivityText) ActivityText,
	dbo.DateOnly(ActivityDate) ActivityDate,
	dbo.DateOnly(ReminderDate)ReminderDate,
	dbo.DateOnly(cR.Addedon)Addedon,
	MstUsers.FullName AssignedTo
	FROM vwContractRequestLatest cR
	LEFT OUTER JOIN MstCountry ON MstCountry.CountryId = cR.CountryId
	LEFT OUTER JOIN vwRequestType ON vwRequestType.RequestTypeID = cR.RequestTypeId
	LEFT OUTER JOIN MstContractType ON MstContractType.ContractTypeId = cR.ContractTypeId
	LEFT OUTER JOIN ContractTemplate ON ContractTemplate.ContractTemplateId = cR.ContractTemplateId
	LEFT OUTER JOIN ContractQuestionsAnswers qA ON qA.RequestId = cR.RequestId
	LEFT OUTER JOIN Activity ON Activity.RequestId = cR.RequestId
	LEFT OUTER JOIN MstActivityType ON MstActivityType.ActivityTypeId = Activity.ActivityTypeId
	LEFT OUTER JOIN ActivityStatus ON ActivityStatus.ActivityStatusId = Activity.ActivityStatusId
	LEFT OUTER JOIN MstUsers ON ( MstUsers.UsersId = cR.AssignToUserID OR MstUsers.UsersId = cR.RequesterUserId)
	LEFT OUTER JOIN Client ON CLient.ClientId=cR.ClientId


	
	





















