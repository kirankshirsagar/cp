﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="AuditTrailDetails.aspx.cs" Inherits="AuditTrail_AuditTrailDetails" %>

<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridNavigation.js" type="text/javascript"></script>
    <link href="../Styles/chosen.css" rel="stylesheet" type="text/css" />
    <script src="../scripts/chosen.jquery.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script src="../JQueryValidations/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    <script src="../CommonScripts/datePickerReports.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../scripts/jquery-ui-1.8.16.custom.css" />
    <script type="text/javascript">
        $("#reportLink").addClass("menulink");
        $("#reportTab").addClass("selectedtab");
        $("#sidebar").html($("#rightlinks").html());
        $(document).ready(function () {
            $('.sort').click(function () {
                try {
                    debugger;
                    $('#hdnNumberButtonClick').val('');
                    $('#hdnLastVisibletButton').val('10');
                } catch (e) { }
            });
        });
        function callsPaging() {
            try {
                $("#MainContent_PaginationButtons1_Button_4").text('4 ');
            } catch (e) { }
        }
    </script>
    <div class="contextual" style="display: none;">
        <asp:LinkButton ID="btnExportToExcel" ClientIDMode="Static" CssClass="icon icon-library areadOnly"
            OnClientClick="return false;" runat="server" OnClick="btnExportToExcel_Click">Export to Excel</asp:LinkButton>
        <asp:LinkButton ID="btnExportToPDF" ClientIDMode="Static" CssClass="icon icon-masters areadOnly"
            OnClientClick="return false;" runat="server" OnClick="btnExportToPDF_Click">Export to PDF</asp:LinkButton></div>
    <h2 style="font-size: medium">
        Audit Trail Log</h2>
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnReportFlag" runat="server" clientidmode="Static" type="hidden" />
    <div style="margin: 0; padding: 0; display: inline">
        <div id="reportFilter" style="display: block">
            <div id="query_form_content" class="hide-when-print">
                <fieldset id="filters" class="collapsible">
                    <legend onclick="toggleFieldset(this);">Filters</legend>
                    <div style="">
                        <table style="width: 100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <table id="filters-table" width="100%" cellpadding="3" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        Contract Id :
                                                    </td>
                                                    <td>
                                                        <p>
                                                            <input class="name number" id="txtContractId" runat="server" placeholder="Contract Id"
                                                                maxlength="15" clientidmode="static" type="text" style="width: 50%;" />&nbsp;
                                                            <span id="loading"></span><em></em><em></em>
                                                        </p>
                                                    </td>
                                                    <td>
                                                        Request Id :
                                                    </td>
                                                    <td>
                                                        <p>
                                                            <input class="name number" id="txtRequestId" runat="server" placeholder="Request Id"
                                                                maxlength="15" clientidmode="static" type="text" style="width: 50%;" />&nbsp;
                                                            <span id="Span1"></span><em></em><em></em>
                                                        </p>
                                                    </td>
                                                    <td>
                                                        User Name :
                                                    </td>
                                                    <td>
                                                        <select id="ddlAssignedUser" clientidmode="Static" runat="server" style="width: 90%"
                                                            class="chzn-select chzn-select">
                                                        </select>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </fieldset>
            </div>
            <p class="buttons hide-when-print">
                <asp:LinkButton ID="btnSearch" runat="server" CssClass="icon icon-checked" OnClick="btnSearch_Click">Filter</asp:LinkButton>
                <asp:LinkButton ID="btnShowAll" runat="server" CssClass="icon icon-reload showallreset"
                    OnClick="btnShowAll_Click">Reset Filter</asp:LinkButton>
            </p>
        </div>
        <div class="autoscroll">
            <table width="100%">
                <asp:Repeater ID="rptReports" runat="server">
                    <HeaderTemplate>
                        <table id="masterDataTable" class="reportTable list issues" width="100%">
                            <thead>
                                <tr>
                                    <th width="10%">
                                        <asp:LinkButton ID="btnCustomerName" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Request ID</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="btnRequestorName" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Contract ID</asp:LinkButton>
                                    </th>
                                    <th width="15%">
                                        <asp:LinkButton ID="btnCompetition" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">User Name</asp:LinkButton>
                                    </th>
                                    <th width="15%">
                                        <asp:LinkButton ID="btnEffectiveDate" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Date Time</asp:LinkButton>
                                    </th>
                                    <th width="50%">
                                        <asp:LinkButton ID="btnProject" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Change Details</asp:LinkButton>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <%#Eval("RequestId")%>
                            </td>
                            <td>
                                <%#Eval("ContractId")%>
                            </td>
                            <td>
                                <%#Eval("FullName")%>
                            </td>
                            <td>
                                <%#Eval("ActionDateTime")%>
                            </td>
                            <td>
                                <%#Eval("DisplayMessage")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
            <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
        </div>
    </div>
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <div style="display: none">
        <asp:GridView ID="gvDetails" runat="server">
        </asp:GridView>
    </div>
    <asp:HiddenField ID="hdnContractTypeID" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnContractCatID" runat="server" Value="100" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnContractId" runat="server" Value="0" ClientIDMode="Static" />
    <script type="text/javascript">
        function SetWaterMark() {
            $('#ContractId').attr('placeholder', 'Search by Contract ID');
        }
        $("#sidebar").html($("#rightlinks").html());
        function GetContractTypeID() {
            var ID = $("#ddlContracttype").val();
            $("#hdnContractTypeID").val(ID);
        }
        function GetContractCatID() {
            var ID = $("#ddlContractCategory").val();
            $("#hdnContractCatID").val(ID);
        }
//        $(document).ready(function () {
//            $(".name").autocomplete({
//                source: src,
//                select: function (event, ui) {
//                    $('#txtContractId').val(ui.item.label);
//                    var ContractID = "";
//                    var CustomerName = ui.item.label.split('(');
//                    var n = ui.item.label.lastIndexOf('(');
//                    var result = ui.item.label.substring(n + 1).replace(')', '');
//                    try {
//                        if (parseInt(result).toString().trim() != NaN.toString().trim()) {
//                            $('#hdnContractId').val(result.trim());
//                            ContractID = $('#hdnContractId').val();
//                            //$('#txtThirdpartyname').val(ui.item.label.substr(0, ui.item.label.lastIndexOf('(')));
//                        }
//                        else {
//                            $('#hdnContractId').val('0');
//                            ContractID = $('#hdnContractId').val();
//                        }
//                    }
//                    catch (err) {
//                        ContractID = "0";
//                    }
//                    return false;
//                }
//            });

//            //$('#lnkPredefineSheduleReport').css('display', 'none');
//            //$('#ddlReport').hide().trigger("liszt:updated");
////            $('.chzn-container .chzn-container-single').css('display', 'none').trigger("liszt:updated");
////            //$('#ddlReport').css('visibility', 'hidden').trigger("liszt:updated");
//        });
        function SetNextButton() {
            $('#hdnNumberButtonClick').val('');
        }
    </script>
</asp:Content>
