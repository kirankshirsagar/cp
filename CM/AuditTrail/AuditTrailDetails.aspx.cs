﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;
using System.Data;
using ReportBLL;
using System.IO;
using ImportExcel;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;//This name space present in "itextsharp-dll-core"
using UserManagementBLL;
using System.Web.UI.HtmlControls;
using ClosedXML.Excel;
using AuditTrailBLL;


public partial class AuditTrail_AuditTrailDetails : System.Web.UI.Page
{
    // navigation//
    public static int RecordsPerPage = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["RecordsPerPage"]);
    public static int VisibleButtonNumbers = 10;
    // navigation//

    IAuditTrail objReport;
    public bool View;

    protected void Page_Load(object sender, EventArgs e)
    {
        hdnReportFlag.Value = "79";
        CreateObjects();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            ViewState[Declarations.View] = Convert.ToBoolean(Access.isCustomised(int.Parse(hdnReportFlag.Value)));
        }

        setAccessValues();
        if (!IsPostBack || hdnSearch.Value.Trim() != string.Empty)
        {
            IUsers objuser;
            objuser = FactoryUser.GetUsersDetail();
            if (ddlAssignedUser.Items.Count == 0)
            {
                ddlAssignedUser.extDataBind(objuser.SelectData());
            }
            Session[Declarations.SortControl] = null;
            ReadData(1, RecordsPerPage);
            //Session["ReportAccessId"] = "76";
        }
        else
        {
            SetNavigationButtonParameters();
        }

    }

    protected void btnSort_Click(object sender, EventArgs e)
    {
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "SetNextButton()", true);
        hdnSearch.Value = "";
        LinkButton clickBtn = (LinkButton)sender;
        Session.Add(Declarations.SortControl, clickBtn);
        ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
    }

    public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
    {

    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objReport = FactoryReports.AuditTrailDetails();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    private void setAccessValues()
    {
        if (ViewState[Declarations.View] != null)
        {
            View = Convert.ToBoolean(ViewState[Declarations.View].ToString());
        }
    }

    void SetNavigationButtonParameters()
    {
        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);

        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        ReadData(1, RecordsPerPage);
    }

    protected void btnShowAll_Click(object sender, EventArgs e)
    {
        txtContractId.Value = "";
        txtRequestId.Value = "";
        ddlAssignedUser.Value = "0";
        ReadData(1, RecordsPerPage);
        Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "callsPaging()", true);
    }

    void ReadData(int pageNo, int recordsPerPage)
    {
        //if (View == true)
        //{
            Page.TraceWrite("ReadData starts.");
            try
            {
                objReport.PageNo = pageNo;
                objReport.RecordsPerPage = recordsPerPage;
                objReport.Search = txtContractId.Value.Trim();
                if (txtContractId.Value.Trim() != "")
                    objReport.ContractId = txtContractId.Value.Trim();

                if (txtRequestId.Value.Trim() != "")
                    objReport.RequestId =int.Parse(txtRequestId.Value.Trim());

                if (ddlAssignedUser.Value.Trim() != "0")
                    objReport.UserIds = ddlAssignedUser.Value;

                LinkButton btnSort = null;
                if (Session[Declarations.SortControl] != null && Session[Declarations.SortControl] != string.Empty)
                {
                    btnSort = (LinkButton)Session[Declarations.SortControl];
                    objReport.Direction = btnSort.CssClass.ToLower() == "sort desc" ? 1 : 0;
                    objReport.SortColumn = btnSort.Text.Trim();
                }
                objReport.UserId = Convert.ToInt32(Session[Declarations.User].ToString());

                rptReports.DataSource = objReport.GetReport();
                rptReports.DataBind();
                if (btnSort != null)
                {
                    rptReports.ClassChange(btnSort);
                }
                Page.TraceWrite("ReadData ends.");
                ViewState[Declarations.TotalRecord] = objReport.TotalRecords;
                PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
                PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
                PaginationButtons1.RecordsPerPage = RecordsPerPage;
                PaginationButtons1.AddpagingButton();
            }
            catch (Exception ex)
            {
                Page.TraceWarn("ReadData fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        //}
    }

    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        if (hdnReportFlag.Value != "")
        {
            try
            {
                IAuditTrail objAudit = FactoryReports.AuditTrailDetails();
                objAudit.ReportName = "Audit Trail Log";
                if (txtContractId.Value.Trim() != "")
                    objAudit.ContractId = txtContractId.Value.Trim();
                DataTable dt = objAudit.GetFullReport();
                if (dt.Rows.Count > 0)
                {
                    string fileName = objAudit.ReportName.Replace(" ", "-").Replace(",", "-").extTimeStamp() + ".xls";
                    gvDetails.Caption = "<div align=center><font size=4 ><b>" + objAudit.ReportName + "</b></font></div>";
                    gvDetails.DataSource = dt.extReplaceExcelQuotes();
                    gvDetails.DataBind();
                    //ExportToExcellnew(fileName,dt);
                    ExportToExcell(fileName);
                }
            }
            catch { }
        }
    }

    protected void btnExportToPDF_Click(object sender, EventArgs e)
    {
        if (hdnReportFlag.Value != "")
        {
            try
            {
                IAuditTrail objAudit = FactoryReports.AuditTrailDetails();
                objAudit.ReportName = "Audit Trail Log";
                if (txtContractId.Value.Trim() != "")
                    objAudit.ContractId = txtContractId.Value.Trim();
                DataTable dt = objAudit.GetFullReport();
                if (dt.Rows.Count > 0)
                {
                    string fileName = objAudit.ReportName.Replace(" ", "-").Replace(",", "-").extTimeStamp() + ".pdf";
                    gvDetails.Caption = "<div align=center><font size=4 ><b>" + objAudit.ReportName + "</b></font></div>";
                    gvDetails.DataSource = dt.extReplaceQuotes();
                    gvDetails.DataBind();
                    ExportToPdf(fileName, objAudit.ReportName);
                }
            }
            catch { }
        }
    }

    void ExportToPdf(string pdfFileName, string reportTitle)
    {
        Response.ContentType = "application/pdf";
        string strAttachmnet = "attachment;filename=" + pdfFileName;
        Response.AddHeader("content-disposition", strAttachmnet);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter s_w = new StringWriter();
        HtmlTextWriter h_w = new HtmlTextWriter(s_w);
        HtmlForm frm = new HtmlForm();
        gvDetails.AllowPaging = false;
        //gvDetails.RenderControl(h_w);
        this.Controls.Add(frm);
        frm.Controls.Add(gvDetails);
        frm.RenderControl(h_w);

        string strOldString = "scope=\"col\"";
        string strReplace = strOldString + " style=\"font-weight:bold; font-size:12px;\" ";
        string strOut = s_w.ToString().Replace(strOldString, strReplace);
        strOut = "<div align=center><font size=4 ><b>" + reportTitle + "</b></font></div><br>" + strOut;

        StringReader sr = new StringReader(strOut);
        Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        MemoryStream ms = new MemoryStream();
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        pdfDoc.Open();
        htmlparser.Parse(sr);
        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
    }

    void ExportToExcellnew(string excelFileName, DataTable dt)
    {
        XLWorkbook wb = new XLWorkbook();
        //Range(int firstCellRow,int firstCellColumn,int lastCellRow,int lastCellColumn)
        int CCount = gvDetails.HeaderRow.Cells.Count;
        int RCount = gvDetails.Rows.Count;
        var ws = wb.Worksheets.Add("Report"); //add worksheet to workbook
        ws.Row(1).Cell(1).RichText.AddText(FactoryExport.ReportName);
        ws.Range(1, 1, 1, CCount).Merge();
        ws.Range(1, 1, 1, CCount).Style.Font.Bold = true;//Range(int firstCellRow,int firstCellColumn,int lastCellRow,int lastCellColumn)
        ws.Range(1, 1, 1, CCount).Style.Font.FontSize = 15;
        //ws.Range(2, 1, 1, CCount).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
        ws.Range(1, 1, 1, CCount).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
        ws.Range(1, 1, 1, CCount).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
        ////ws.Range("A1:Z1").Style.Font.Bold = true;
        ws.Row(2).Cells(1, CCount).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
        //ws.Range(2, 1, 1, CCount).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
        ws.Row(2).Cell(1).InsertTable(dt);
        ws.Range("A2:Z2").Style.Font.Bold = true;
        ws.Range(2, 1, RCount + 2, CCount).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
        //wb.SaveAs(FolderPath + filename + ".xlsx");
        //ws.Dispose();

        using (var memoryStream = new MemoryStream())
        {
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", string.Format("attachment;  filename={0}", excelFileName));
            wb.SaveAs(memoryStream);
            memoryStream.WriteTo(Response.OutputStream);
            Response.Flush();
            Response.End();
        }
    }

    void ExportToExcell(string excelFileName)
    {
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", excelFileName));
        Response.ContentType = "application/ms-excel";
        //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        HtmlForm frm = new HtmlForm();
        gvDetails.AllowPaging = false;
        //Change the Header Row back to white color
        gvDetails.HeaderRow.Style.Add("background-color", "#E1E1E1");
        //Applying stlye to gridview header cells
        for (int i = 0; i < gvDetails.HeaderRow.Cells.Count; i++)
        {
            gvDetails.HeaderRow.Cells[i].Style.Add("background-color", "#E1E1E1");
            gvDetails.HeaderRow.Cells[i].Style.Add("font-weight", "bold");

        }

        this.Controls.Add(frm);
        frm.Controls.Add(gvDetails);
        frm.RenderControl(htw);

        int j = 1;
        //Set alternate row color
        foreach (GridViewRow gvrow in gvDetails.Rows)
        {
            gvrow.BackColor = System.Drawing.Color.White;
            if (j <= gvDetails.Rows.Count)
            {
                if (j % 2 != 0)
                {
                    for (int k = 0; k < gvrow.Cells.Count; k++)
                    {
                        gvrow.Cells[k].Style.Add("background-color", "#EFF3FB");
                    }
                }
            }
            j++;
        }
        //gvDetails.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }


}