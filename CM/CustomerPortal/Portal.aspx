﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Portal.aspx.cs" Inherits="CustomerPortal_Portal" %>

<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%--<%@ Register Src="../UserControl/masterrightlinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>--%>
<script src="../JQueryValidations/jquery-1.7.2.min.js" type="text/javascript"></script>
<link href="../JQueryValidations/jquery-ui-1.8.19.custom.css" rel="stylesheet" type="text/css" />
<script src="../JQueryValidations/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
<link href="../Styles/chosen.css" rel="stylesheet" type="text/css" />
<script src="../scripts/chosen.jquery.js" type="text/javascript"></script>
<script src="../CommonScripts/dropdownlist.js" type="text/javascript"></script>
<script src="../JQueryValidations/mastervalidations.js" type="text/javascript"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script type="text/javascript">
    window.setTimeout(function () {
        // This will execute 5 seconds later
        var label = document.getElementById('lblmessage');
        if (label != null) {
            label.style.display = 'none';
        }
    }, 5000);
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="UTF-8" />
    <title>Customer Portal</title>
    <link rel="stylesheet" type="text/css" media="all" href="Styles/style.css" />
    <script src="Styles/jquery-1.4.1.js" type="text/javascript"></script>
    <style type="text/css" id="custom-background-css">
        body.custom-background
        {
            background-color: #f1f1f1;
            background-image: url('Styles/bback5.png');
            background-repeat: repeat-y;
            background-position: top center;
            background-attachment: fixed;
        }
    </style>
    <script type="text/javascript" language="javascript">

        $(document).ready(function () {
            //$("#Reply1").hide();

            for (i = 1; i <= 5; i++) {
                $("#Reply" + i).hide();
            }
        });
        function ShowReplyDiv(ch) {
            $("#Reply" + ch).show();
        }
        function HideReplyDiv(ch) {
            $("#Reply" + ch).hide();
        }
    </script>
    <script type="text/javascript">
        function countChar(val) {
            try {
                var len = val.value.length;
                if (len >= 500) {
                    val.value = val.value.substring(0, 500);
                } else {
                    return false;
                    //$('#txtPostArea').text(500 - len);
                }
            } catch (e) {

            }
        };
    </script>
</head>
<body class="single single-post postid-522 single-format-standard custom-background mp6 single-author highlander-enabled highlander-light">
    <div id="wrapper" class="hfeed">
        <div id="main">
            <div id="container">
                <form id="Form1" runat="server">
                 <p style="float: left">
                 <img src="../Images/logo.png"/></p>
                <p style="float: right">
                    <asp:Label ID="lblUserName" runat="server" Text=""></asp:Label>
                    <asp:LinkButton ID="lnklogout" runat="server" OnClick="lnklogout_Click">Log Out</asp:LinkButton>
                </p>
                <div id="content" role="main">
                    <div id="entry-author-info">
                        
                        <!-- #author-avatar -->
                        
                        <div >
                            <h2>
                                <asp:Label ID="lblContractTypeName" runat="server" Text=""></asp:Label>
                            </h2>
                            <asp:Label ID="lblContractTypeDescription" runat="server" Text=""></asp:Label>
                            <br />
                            <asp:Label ID="lblContractId" runat="server" Text=""></asp:Label>
                            <!-- #author-link	-->
                        </div>
                        <!-- #author-description -->
                    </div>
                    <!-- #nav-below -->
                    <div id="comments">
                        <div class="comment-form-field comment-textarea">
                            <asp:TextBox ID="txtPostArea" onkeyup="countChar(this)" runat="server" TextMode="MultiLine"
                                title="Enter your comment here..." placeholder="Enter your comment here..." Style="height: 70px;
                                width: 912px; overflow: hidden; word-wrap: break-word; resize: horizontal;"></asp:TextBox>
                            <p style="text-align: center">
                                <asp:Label ID="lblmessage" runat="server" ClientIDMode="Static" ForeColor="#FF3300"
                                    Visible="False"></asp:Label></span>
                            </p>
                            <asp:FileUpload ID="FileUpload1" ClientIDMode="Static" runat="server" class="btn_validate" />
                            <p class="form-submit" style="display: block; float: right;">
                                <asp:Button ID="btnPostComment" runat="server" Text="Post Comment" class="submitbutton"
                                    OnClick="btnPostComment_Click" />
                            </p>
                        </div>
                        
                        
                        <h3 id="comments-title">
                            <asp:Label ID="lblMessageCount" runat="server" ClientIDMode="Static"></asp:Label>
                        </h3>
                        <div id="activity">
                            <table width="100%">
                                <asp:Repeater ID="rptCustomerPortal" runat="server">
                                    <ItemTemplate>
                                        <table id="CustomerPortalTable" width="100%">
                                            <ol class="commentlist">
                                                <li class="comment byuser comment-author-kendylau even thread-even depth-1 highlander-comment"
                                                    id="li6">
                                                    <div id="Div6">
                                                        <div class="comment-author vcard">
                                                            <asp:Image ID="Image1" runat="server" class="avatar avatar-40 grav-hashed grav-hijack"
                                                                Height="50" Width="50" ImageUrl='<%# Eval("CustomerImageURL") %>' />
                                                            <cite class="fn" style="color: #0066CF">
                                                                <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("CustomerName") %>' ClientIDMode="Static"></asp:Label></span>
                                                                <asp:Label ID="lbluserdesignation" runat="server" Text='<%# Eval("CustomerDesignationName") %>'
                                                                    ClientIDMode="Static"></asp:Label>
                                                            </cite><span class="says">says:</span>
                                                        </div>
                                                        <!-- .comment-author .vcard -->
                                                        <div class="comment-meta commentmetadata">                                                           
                                                                <asp:Label ID="lbladdedon" runat="server" Text='<%# Eval("Date") %>' ClientIDMode="Static"></asp:Label>
                                                                at
                                                                <asp:Label ID="lbltime" runat="server" Text='<%# Eval("Time") %>' ClientIDMode="Static"></asp:Label></span>
                                                        </div>
                                                        <!-- .comment-meta .commentmetadata -->
                                                        <div class="comment-body">
                                                            <p>
                                                                <asp:Label ID="lblActivityText" runat="server" Text='<%# Eval("CustomerDescription").ToString().Replace("\n", "<br>") %>'
                                                                    ClientIDMode="Static"></asp:Label></span>
                                                            </p>
                                                        </div>
                                                        <div class="reply" style="display: none;">
                                                            <a class="comment-reply-link" href="#" onclick="ShowReplyDiv(1)">Reply</a>
                                                        </div>
                                                        <!-- .reply -->
                                                    </div>
                                                    <!-- #comment-##  -->
                                                </li>
                                            </ol>
                                        </table>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                            <%-- <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />--%>
                        </div>
                        <div style="clear: both">
                        </div>
                    </div>
                </div>
                </form>
                <!-- #comments -->
            </div>
            <!-- #content -->
        </div>
        <!-- #container -->
    </div>
   <%-- <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>--%>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
    <!-- #main -->
    <div id="footer" role="contentinfo">
        <div id="colophon">
            <div id="site-info">
                <a href="#" title="Code Jotter" rel="home">Contract Management Customer Portal </a>
            </div>
            <!-- #site-info -->
            <div id="site-generator">
                Powered by <a href="http://www.uberall.in/" target="_blank" title="Uberall">Ubeall Solutions
                    (I) Ltd. </a>
            </div>
            <!-- #site-generator -->
        </div>
        <!-- #colophon -->
    </div>
    <!-- #footer -->
    <!-- #wrapper -->
    

    <script type="text/javascript">
        
    </script>



</body>
</html>
