﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CustomerPortal;
using CommonBLL;

public partial class CustomerPortal_Login : System.Web.UI.Page
{
    IPortal objPortal;
    HttpCookie myCookie = new HttpCookie("LoginCookie");
    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();

        if (!IsPostBack)
        {
            //if (Request.Cookies["LoginCookie"] != null)
            //{
            //    //Response.Cookies.Remove("LoginCookie");
            //    myCookie = Request.Cookies.Get("LoginCookie");
            //    txtUserName.Value = myCookie.Values["txtUserName"].ToString();
            //    //HtmlInputText txtbox = new HtmlInputText();
            //    //txtbox.Attributes["Value"] = objEncrypt.Decrypt(myCookie.Values["Password"].ToString());
            //    hdnPassword.Value = myCookie.Values["txtPassword"].ToString();
            //    hdnsavedpassword.Value = hdnPassword.Value;
            //    txtPassword.Value = myCookie.Values["txtPassword"].ToString();
            //    //hdnrequestid.Value = Convert.ToString(Session[Declarations.RequestId]);
            //    hdnrequestid.Value = myCookie.Values["RequestID"].ToString();
            //    rememberme.Checked = true;

            //     //Server.Transfer("Portal.aspx");
            //    Response.Redirect("Portal.aspx");
            //}

            HttpCookie cookie = null;
            if (Request.Cookies["LoginCookie"] != null)
            {
                cookie = HttpContext.Current.Request.Cookies["LoginCookie"];
                cookie.Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
            Session.Clear();
        }

    }
    protected void lnkLogin_Click(object sender, EventArgs e)
    {

        if (txtUserName.Value == "" && txtPassword.Value == "")
        {
            lblusername.Visible = true;
            lblerrormessage.Visible = false;
            lblmessage.Visible = false;
            lblusername.Text = "Please enter user name and password.";
            lblerrormessage.Visible = false;

        }
        else if (txtUserName.Value != "")
        {
            if (txtPassword.Value != "")
            {
                objPortal.EmailID = txtUserName.Value.Trim();
                objPortal.Password = txtPassword.Value.Trim();


                //objPortal.Password = txtPassword.Text.Trim();
                objPortal.CheckLogin();
                // hdnrequestid.Value =Convert.ToString(objPortal.RequestId);
                //if (objPortal.RequestId > 0 || hdnrequestid.Value != null)
                if (objPortal.RequestId > 0)
                {

                    Session[Declarations.UserFullName] = objPortal.CustomerName;
                    Session[Declarations.RequestId] = objPortal.RequestId;
                    Session[Declarations.ContractID] = objPortal.ContractID;
                    Session["CustomerTenantDIR"] = Request.Url.Segments[1].Replace("/", "");

                    if (Request.Cookies["LoginCookie"] != null)
                    {
                        if (hdnPassword.Value != "")
                        {
                            objPortal.Password = hdnPassword.Value;
                            Session[Declarations.RequestId] = hdnrequestid.Value;
                        }
                        else
                        {
                            if (txtPassword.Value == "")
                            {
                                objPortal.Password = hdnsavedpassword.Value;
                            }
                            else
                            {
                                objPortal.Password = txtPassword.Value;
                            }
                        }
                    }

                    if (rememberme.Checked == true)
                    {
                        // HttpCookie myCookie = new HttpCookie("myCookie");
                        Response.Cookies.Remove("LoginCookie");

                        myCookie.Values.Add("txtUserName", txtUserName.Value.ToString());
                        if (objPortal.RequestId > 0)
                        {
                            myCookie.Values.Add("RequestID", Convert.ToString(objPortal.RequestId));
                        }
                        else
                        {
                            myCookie.Values.Add("RequestID", hdnrequestid.Value);

                        }
                        if (txtPassword.Value == "")
                        {
                            myCookie.Values.Add("txtPassword", hdnPassword.Value);
                        }
                        else
                        {
                            myCookie.Values.Add("txtPassword", txtPassword.Value.ToString());
                        }
                        DateTime dtxpiry = DateTime.Now.AddDays(15);

                        myCookie.Expires = dtxpiry;
                        Response.Cookies.Add(myCookie);
                    }
                    else
                    {
                        HttpCookie cookie = null;
                        if (Request.Cookies["LoginCookie"] != null)
                        {
                            cookie = HttpContext.Current.Request.Cookies["LoginCookie"];
                            //You can't directly delete cookie you should 
                            //set its expiry date to earlier date 
                            cookie.Expires = DateTime.Now.AddDays(-1);
                            HttpContext.Current.Response.Cookies.Add(cookie);
                        }

                    }

                    //Server.Transfer("Portal.aspx");
                    Response.Redirect("Portal.aspx");

                }
                else
                {
                   
                    lblerrormessage.Visible = false;
                    lblusername.Visible = false;
                    lblmessage.Text = "Invalid User";
                    lblmessage.Visible = true;
                    //lblusername.Visible = true;
                    //lblusername.Text = "Please enter user name and password.";
                    txtPassword.Value = "";
                    txtUserName.Value = "";
                    Session[Declarations.RequestId] = null;
                    Session[Declarations.ContractID] = null;
                }
            }
            else
            {
                lblerrormessage.Visible = true;
                lblusername.Visible = false;
                lblerrormessage.Text = "Please enter password.";
            }
        }
        else
        {
            //string password = txtPassword.Value;
            lblusername.Visible = true;
            lblusername.Text = "Please enter user name.";
            lblerrormessage.Visible = false;
            //txtPassword.Value = password;
            //txtPassword.Attributes["Value"] = password;
        }
    }


    #region MyRegion
    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objPortal = FactoryPortal.GetPortalDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }
    #endregion



}