﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="CustomerPortal_Login" %>
<%@ OutputCache Duration="1" VaryByCustom="browser" VaryByParam="none" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script type="text/javascript">
    window.setTimeout(function () {
        // This will execute 5 seconds later
        var label = document.getElementById('lblerrormessage');
        var labelusername = document.getElementById('lblusername');
        var labelpassword = document.getElementById('lblmessage');
        if (label != null) {
            label.style.display = 'none';
        }
        if (labelusername != null) {
            labelusername.style.display = 'none';
        }
        if (labelpassword != null) {
            labelpassword.style.display = 'none';
        }
    }, 5000);
    
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Customer Portal Login</title>
    <link href="Styles/buttons.min.css" rel="stylesheet" type="text/css" />
    <link href="Styles/saved_resource.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="Styles/saved_resource(1)"></script>
    <style type="text/css">
        body, form .input
        {
            font-family: "Open Sans" , "Helvetica Neue" , "Arial" , sans;
        }
        body.login.recover-password, body.login.recover-password-success
        {
            height: auto;
            min-height: 100%;
        }
        .recover-password #recovery, .recover-password-success #recovery, .recover-password #login_error
        {
            width: 500px;
        }
        #recovery
        {
            padding-bottom: 24px;
        }
        .recover-password #login_error
        {
            width: auto;
        }
        #login_error ul
        {
            list-style: square outside none;
            margin-left: 15px;
        }
        #login_error ol
        {
            margin-left: 25px;
        }
        .message ol
        {
            margin: 10px 0px 10px 20px;
        }
        #login_error h2, #recovery h2
        {
            padding-bottom: 10px;
        }
        #login_error h3
        {
            padding-bottom: 5px;
        }
        #login_error h3
        {
            margin-top: 10px;
        }
        .recover-password #recovery h2
        {
            margin-bottom: 5px;
        }
        .recover-password-success #recovery h2
        {
            margin-bottom: .5em;
        }
        .recover-password #recovery p
        {
            line-height: 1.6;
            padding-bottom: 10px;
            border-top: none;
        }
        .recover-password-success #recovery p
        {
            line-height: 1.6;
            padding-bottom: 1em;
        }
        .recovery-stage
        {
            margin: 14px 0px 27px 0px;
        }
        .login .recovery-stage input[type="text"]
        {
            margin-bottom: 5px;
        }
        .recovery-ownership-message
        {
            background-color: #fff;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,0.1);
            box-shadow: 0 1px 2px rgba(0,0,0,0.1);
            padding: 7px 15px 0 15px;
            margin: 1em 0;
            background: #ffffe0;
        }
        .recovery-error-message
        {
            display: none;
            background-color: #f1831e;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,0.1);
            box-shadow: 0 1px 2px rgba(0,0,0,0.1);
            padding: 7px 15px 0 15px;
            margin: 1em 0;
            background: #f1831e;
        }
        .recover-password #recovery textarea
        {
            width: 100%;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -ms-box-sizing: border-box;
        }
        .recover-password #recovery .recovery-ownership-title
        {
            position: relative;
            padding-bottom: 0;
        }
        .recovery-ownership-help
        {
            float: right;
            font-size: 11px;
            margin-top: -10px;
            margin-bottom: 5px;
        }
        .recovery-ownership
        {
            clear: both;
        }
        .recovery-ownership-help
        {
            margin: 3px 0 0 0;
        }
        .recovery-ownership-help-text
        {
            display: none;
            position: absolute;
            top: 25px;
            right: 0;
            z-index: 100;
            border: 1px solid #e6db55;
            background-color: lightyellow;
            width: 250px;
            padding: 8px;
            box-shadow: 0 4px 10px -1px rgba(200, 200, 200, 0.7);
        }
        .account-recovery-link
        {
            float: left;
            margin-top: 4px;
        }
    </style>
    <style type="text/css">
        .twostep-login-details strong
        {
            white-space: nowrap;
        }
    </style>
    <script language="javascript">
        function GotoPortal() {
            location.href = "portal.htm";
        }
        function ShowMessage() {
            alert('Valide user.');
            window.location.href = 'PortalDetails.aspx';
        }
        function ErrorMessageHide(obj) {
            
            var btn_value = $('#txtPassword').val();

            if (btn_value != '') {
                $("#lblerrormessage").hide();
            }
        }
    </script>
    <meta name="robots" content="noindex,follow">
</head>
<body class="login login-action-login wp-core-ui  locale-en">
    <form id="form1" runat="server" defaultbutton="lnkLogin">
    <div id="login">
        <h1>
            <%--<img src="Styles/customerportal.png" width="100" height="100" />--%>
              <img src="../Images/logo.png"/>
              <br /><br /><br />
        </h1>
        <form name="loginform" id="loginform" action="#" method="post">
        <p>
            <label for="user_login">
                User name<br>
                <input type="text" runat="server" name="log" id="txtUserName" class="input" value=""
                    size="20" style="height: 42px;"></label>
        </p>
        <p>
            <label for="user_pass">
                Password<br>
                <input type="password" runat="server" name="pwd" id="txtPassword" class="input" value=""
                    size="20" style="height: 42px;">
            </label>
            <asp:HiddenField ID="hdnsavedpassword" ClientIDMode="Static" runat="server" />
            <asp:HiddenField ID="hdnPassword" ClientIDMode="Static" runat="server" />
        </p>
        <p class="forgetmenot" style="display:none">
            <label for="rememberme" title="Select this option to stay signed in on this device.">
                <input name="rememberme" runat="server" type="checkbox" id="rememberme" value="forever"
                    checked="checked" />
                Stay signed in</label>
            <asp:HiddenField ID="hdnrequestid" runat="server" />
        </p>
        <p class="submit">
            <asp:LinkButton ID="lnkLogin" CssClass="button button-primary button-large" runat="server"
                OnClick="lnkLogin_Click">Log In</asp:LinkButton>
        </p>
        <p style="text-align: center">
            <asp:Label ID="lblmessage" runat="server" Text="" ForeColor="#FF3300" Visible="False"></asp:Label>
            <asp:Label ID="lblusername" runat="server" Text="" ForeColor="#FF3300" Visible="False"></asp:Label>
            <asp:Label ID="lblerrormessage" runat="server" Text="Invalid user" ForeColor="#FF3300"
                Visible="False"></asp:Label>
        </p>
        </form>
    
    </div>
    </form>
</body>
</html>
