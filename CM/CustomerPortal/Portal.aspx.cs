﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;
using CustomerPortal;
using WorkflowBLL;
using System.IO;


public partial class CustomerPortal_Portal : System.Web.UI.Page
{
    IPortal objportal;
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 10;
    HttpCookie myCookie = new HttpCookie("LoginCookie");
    string fileNameUpload = "";
    string fileNameOriginal;
    decimal FileSizeKb;
    bool IsPageRefresh = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        myCookie = Request.Cookies.Get("LoginCookie");        

        if (!IsPostBack)
        {
            ViewState["ViewStateId"] = System.Guid.NewGuid().ToString();
            Session["SessionId"] = ViewState["ViewStateId"].ToString();
        }
        else
        {
            if (ViewState["ViewStateId"].ToString() != Session["SessionId"].ToString())
            {
                IsPageRefresh = true;
            }
            Session["SessionId"] = System.Guid.NewGuid().ToString();
            ViewState["ViewStateId"] = Session["SessionId"].ToString();
        }     

        if (!IsPostBack)
        {
            if (Request.Cookies["LoginCookie"] != null)
            {
                //if (myCookie.Values["RequestID"].ToString() != null)
                //{
                //    ReadData();
                //    ReadDataRepeter();
                //}
                //else if (Session[Declarations.RequestId] != null)
             if (Session[Declarations.RequestId] != null)
                {
                    ReadData();
                    ReadDataRepeter();
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
            else
            {
                if (Session[Declarations.RequestId] != null)
                {
                    ReadData();
                    ReadDataRepeter();
                    //SessionClear();
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }

        }
        else
        {
            if (Request.Cookies["LoginCookie"] != null)
            {

                if (myCookie.Values["RequestID"].ToString() != null)
                {
                    ReadData();
                    ReadDataRepeter();
                }
            }
            else if (Session[Declarations.RequestId] == null)
            {
                HttpCookie cookie = null;
                if (Request.Cookies["LoginCookie"] != null)
                {
                    cookie = HttpContext.Current.Request.Cookies["LoginCookie"];
                    //You can't directly delete cookie you should 
                    //set its expiry date to earlier date 
                    cookie.Expires = DateTime.Now.AddDays(-1);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                Response.Redirect("Login.aspx");
            }
        }
       
    }
   

    protected void SessionClear()
    {
        //Session.Clear();
        Session.Timeout = 4;//minutes
    }

    void ReadDataRepeter()
    {
        Page.TraceWrite("ReadData customer portal details page starts.");
        try
        {
            if (Session[Declarations.RequestId] != null)
            {
                objportal.RequestId = int.Parse(Session[Declarations.RequestId].ToString());
            }
            else
            {
                if (myCookie.Values["RequestID"].ToString() == "")
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    objportal.RequestId = int.Parse(myCookie.Values["RequestID"].ToString());
                }
            }
            objportal.CustomerTenantDIR = Convert.ToString(Session["CustomerTenantDIR"]);
            rptCustomerPortal.DataSource = objportal.ReadDataRepeter();
            rptCustomerPortal.DataBind();

            ViewState[Declarations.TotalRecord] = objportal.TotalRecords;
            lblMessageCount.Text = objportal.MessageCount.ToString() + " Messages, " +
                                   objportal.FileCount.ToString() + " files till now.";

        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData customer portal details page fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData customer portal details page ends.");
    }


    void ReadData()
    {
        Page.TraceWrite("ReadData customer portal details page starts.");
        try
        {
            if (Session[Declarations.RequestId] != null)
            {
                objportal.RequestId = int.Parse(Session[Declarations.RequestId].ToString());
            }
            else
            {
                if (myCookie.Values["RequestID"].ToString() == "")
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    objportal.RequestId = int.Parse(myCookie.Values["RequestID"].ToString());
                }
            }
            List<Portal> Portal = objportal.ReadData();
            lblContractTypeName.Text = Portal[0].ContractTypeName;
            lblContractTypeDescription.Text = (Portal[0].ContractTypeDiscription).ToString().Replace("\n", "<br>");

            if (Session[Declarations.UserFullName] != null)
            {
                lblUserName.Text = "<b>Welcome,</b> " + Session[Declarations.UserFullName].ToString() + "&nbsp;";
            }

            if (Session[Declarations.ContractID] != null)
            {
                lblContractId.Text = "Contract ID : " + Session[Declarations.ContractID].ToString();
            }


        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData customer portal details page fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData customer portal details page ends.");
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objportal = FactoryPortal.GetPortalDetail();
            //objActivity = FactoryWorkflow.GetActivityDetails();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void btnPostComment_Click(object sender, EventArgs e)
    {        
        Page.TraceWrite("Save post button clicked event starts.");
        if (txtPostArea.Text.Trim() == "" )
        {
            lblmessage.Text = "Please enter your comments.";
            lblmessage.Visible = true;
        }
        else if(!IsPageRefresh)
        {
            Insert();
        }
        Page.TraceWrite("Save post button clicked event ends.");
    }

    void Insert()
    {
        
        Page.TraceWrite("Insert post comment for customer portal starts.");
        string status = "";
        objportal.ActivityTypeId = 4;
        objportal.ActivityStatusId = 2;
        objportal.ActivityText = txtPostArea.Text;
        objportal.IsForCustomer = "Y";
        if (Session[Declarations.RequestId] != null)
        {
            objportal.RequestId = int.Parse(Session[Declarations.RequestId].ToString());
           
        }
        else
        {
            if (myCookie.Values["RequestID"].ToString() == "")
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                objportal.RequestId = int.Parse(myCookie.Values["RequestID"].ToString());
            }
        }

        UploadFile(objportal.RequestId.ToString());
        objportal.IsCustomer = "Y";
       

        if (fileNameUpload != "invalidformat")
        {
            objportal.FileUpload = fileNameUpload;
            objportal.FileUploadOriginal = fileNameOriginal;
            objportal.FileSizeKb = FileSizeKb;

            status = objportal.InsertRecord();
            ViewState["ActivityId"] = status;
            if (int.Parse(status) >0)
            {
                lblmessage.Text = "Post saved successfully.";
                lblmessage.Visible = true;
                txtPostArea.Text = "";
            }
        }
        else
        {
            lblmessage.Visible = true;
        }
        ViewState["RequestId"] = objportal.RequestId.ToString();
        SendEmail();
        ReadDataRepeter();
        Page.TraceWrite("Insert post comment for customer portal ends.");
    }

    protected void lnklogout_Click(object sender, EventArgs e)
    {
        HttpCookie cookie = null;
        if (Request.Cookies["LoginCookie"] != null)
        {
            cookie = HttpContext.Current.Request.Cookies["LoginCookie"];
            //You can't directly delete cookie you should 
            //set its expiry date to earlier date 
            cookie.Expires = DateTime.Now.AddDays(-1);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }
        Session.Clear();
        Response.Redirect("Login.aspx");
    }


    void UploadFile(string requestId)    {
       
        if (FileUpload1.HasFile)
        {
            try
            {  
                FileInfo fInfo = new FileInfo(FileUpload1.FileName);
                string fileExtention = fInfo.Extension.ToLower();

                var lstFileTypes = new List<string>();
                lstFileTypes.Add(".xls");
                lstFileTypes.Add(".xlsx");
                lstFileTypes.Add(".pdf");
                lstFileTypes.Add(".txt");
                lstFileTypes.Add(".csv");
                lstFileTypes.Add(".docx");
                lstFileTypes.Add(".doc");

                lstFileTypes.Add(".swf");
                lstFileTypes.Add(".ppt");
                lstFileTypes.Add(".pptx");

                lstFileTypes.Add(".jpg");
                lstFileTypes.Add(".gif");
                lstFileTypes.Add(".png");

                lstFileTypes.Add(".bmp");
                lstFileTypes.Add(".tiff");
                lstFileTypes.Add(".jpeg");
                lstFileTypes.Add(".zip");
                lstFileTypes.Add(".blob");
                lstFileTypes.Add(".msg");//added by Bharati 27Oct2014 on client demand
                lstFileTypes.Add(".eml");//added by Bharati 27Oct2014 on client demand

                var validFiles = string.Join(",", lstFileTypes.Where(p => p != "").Select(p => p.ToString()));

                if (lstFileTypes.Contains(fileExtention))
                {
                    if (FileUpload1.PostedFile.ContentLength < 15728641)
                    {
                        fileNameOriginal = FileUpload1.FileName.Replace(" ","-");
                        fileNameUpload = Path.GetFileName(fileNameOriginal).extTimeStamp(requestId);
                        fileNameUpload = fileNameUpload.Substring(0, fileNameUpload.LastIndexOf('.')) + "_" + Guid.NewGuid() + fileExtention;

                        FileSizeKb = FileUpload1.PostedFile.ContentLength;
                        var serverPath = Server.MapPath("../Uploads/" + Session["CustomerTenantDIR"] + "/CustomerPortal/" + fileNameUpload);
                        FileUpload1.SaveAs(serverPath);
                    }
                    else
                        lblmessage.Text = "The file has to be less than 15 MB";
                }
                else
                {
                    fileNameUpload = "invalidformat";
                    lblmessage.Text = "Accepted files are " + validFiles;
                }
            }
            catch (Exception ex)
            {
                fileNameUpload = "invalidformat";
                lblmessage.Text = "The file could not be uploaded.";

            }
        }
      
    }


    //Adedd BY Bharati 15Dec2014
    void SendEmail()
    {
        try
        {
            Page.TraceWrite("send emails.");
            Page.TraceWrite("send scheduled emails.");
            EmailTemplateBLL.IEmailTrigger objEmail = EmailTemplateBLL.FactoryEmailTemplate.GetEmailTriggerDetail();
            objEmail.ActivityId = Convert.ToInt32(ViewState["ActivityId"]);
            objEmail.EmailTriggerId = 12;
            objEmail.userId = 0;
            objEmail.BulkEmail();
        }
        catch (Exception)
        {
            Page.TraceWarn("send emails fail.");
        }
    }
   

}