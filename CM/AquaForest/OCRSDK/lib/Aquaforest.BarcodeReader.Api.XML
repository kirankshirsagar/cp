<?xml version="1.0"?>
<doc>
  <assembly>
    <name>Aquaforest.BarcodeReader.Api</name>
  </assembly>
  <members>
    <member name="T:Aquaforest.BarcodeReader.Api.BarcodeReader">
      <summary>
            The main class used to decode barcodes inside an image or PDF document.
            </summary>
    </member>
    <member name="M:Aquaforest.BarcodeReader.Api.BarcodeReader.#ctor(Aquaforest.BarcodeReader.Common.BarcodeReaderSettings)">
      <summary>
            Initialises a new instance of the BarcodeReader class.
            </summary>
      <param name="settings">The settings to use for barcode recognition.</param>
    </member>
    <member name="M:Aquaforest.BarcodeReader.Api.BarcodeReader.#ctor(System.Collections.Generic.List{Aquaforest.BarcodeReader.Common.BarcodeReaderSettings})">
      <summary>
            Initialises a new instance of the BarcodeReader class.
            </summary>
      <param name="settingsList">
            The list of settings to use for barcode recognition. This is useful for poor quality images where different pre-processing settings are required
            for different images/pages. The barcode reader will go through each setting in the list until one is successful.
            </param>
    </member>
    <member name="M:Aquaforest.BarcodeReader.Api.BarcodeReader.DecodeImage(System.String)">
      <summary>
            Scans the specified source image file for barcodes.
            </summary>
      <param name="sourceImage">The source image file to decode.</param>
      <returns>True if at least one barcode is found.</returns>
    </member>
    <member name="M:Aquaforest.BarcodeReader.Api.BarcodeReader.DecodeImage(System.IO.Stream)">
      <summary>
            Scans an image <see cref="T:System.IO.Stream" /> for barcodes.
            </summary>
      <param name="source">The source image <see cref="T:System.IO.Stream" />.</param>
      <returns>True if at least one barcode is found.</returns>
    </member>
    <member name="M:Aquaforest.BarcodeReader.Api.BarcodeReader.DecodeImage(System.Drawing.Image)">
      <summary>
            Scans an <see cref="T:System.Drawing.Image" /> for barcodes.
            </summary>
      <param name="image">The source <see cref="T:System.Drawing.Image" />.</param>
      <returns>True if at least one barcode is found.</returns>
    </member>
    <member name="M:Aquaforest.BarcodeReader.Api.BarcodeReader.DecodePdf(System.String)">
      <summary>
            Scans the specified PDF file for barcodes.
            </summary>
      <param name="sourcePdf">The source PDF file to decode.</param>
      <returns>True if at least one barcode is found.</returns>
    </member>
    <member name="M:Aquaforest.BarcodeReader.Api.BarcodeReader.DecodePdf(System.String,System.String)">
      <summary>
            Scans the specified password protected PDF file for barcodes.
            </summary>
      <param name="sourcePdf">The source PDF file to decode.</param>
      <param name="password">The password for the source PDF file.</param>
      <returns>True if at least one barcode is found.</returns>
    </member>
    <member name="M:Aquaforest.BarcodeReader.Api.BarcodeReader.DecodePdf(System.IO.Stream)">
      <summary>
            Scans a PDF <see cref="T:System.IO.Stream" /> for barcodes.
            </summary>
      <param name="source">The source PDF <see cref="T:System.IO.Stream" />.</param>
      <returns>True if at least one barcode is found.</returns>
    </member>
    <member name="M:Aquaforest.BarcodeReader.Api.BarcodeReader.DecodePdf(System.IO.Stream,System.String)">
      <summary>
            Scans a password protected PDF <see cref="T:System.IO.Stream" /> for barcodes.
            </summary>
      <param name="source">The source PDF <see cref="T:System.IO.Stream" />.</param>
      <param name="password">The password for the source PDF file.</param>
      <returns>True if at least one barcode is found.</returns>
    </member>
    <member name="M:Aquaforest.BarcodeReader.Api.BarcodeReader.GetAllDecodeResults">
      <summary>
            Get all the decoded results.
            </summary>
      <returns>Returns the decoded results in a dictionary, where the dictionary key is the page number and the dictionary value is the <see cref="T:Aquaforest.BarcodeReader.Common.DecodeResult" />.</returns>
    </member>
    <member name="M:Aquaforest.BarcodeReader.Api.BarcodeReader.GetDecodeResult(System.Int32)">
      <summary>
            Get all the decoded result(s) of a particular page.
            </summary>
      <param name="pageNumber">Teh page number to get the result(s) for.</param>
      <returns>The <see cref="T:Aquaforest.BarcodeReader.Common.DecodeResult" /> of the specified page.</returns>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.BlankPageThreshold">
      <summary>
             Use this to set the minimum number of "On Pixels" that must be present in the image for a page not to be considered blank.
             A value of -1 will turn off blank page detection. The default value is -1.
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.DecodeResults">
      <summary>
            Returns the decoded results in a dictionary, where the dictionary key is the page number and the dictionary value is the <see cref="T:Aquaforest.BarcodeReader.Common.DecodeResult" />.
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.EnableConsoleOutput">
      <summary>
            If enabled then progress messages will be sent to the console. Default is true.
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.EnableDebugOutput">
      <summary>
             If enabled then debug messages will be written to the console output. Default is false.
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.EndPage">
      <summary>
            Sets the last page of the source file where barcode recognition will end (for a multipage source).
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.LastErrorMessage">
      <summary>
             Stores the last error message caught by the BarcodeReader object.
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.LastException">
      <summary>
             Stores the last <see cref="T:System.Exception" /> caught by the BarcodeReader object.
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.License">
      <summary>
            Specifies the license key.
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.LogFilePath">
      <summary>
            The path of the log file where output is written to if <see cref="P:Aquaforest.BarcodeReader.Api.BarcodeReader.LogToFile" /> property is set to true.
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.LogToFile">
      <summary>
            Set this to true if you want to log output to a file. The file will be stored in the <see cref="P:Aquaforest.BarcodeReader.Api.BarcodeReader.LogFilePath" /> if specified or the <see cref="P:Aquaforest.BarcodeReader.Api.BarcodeReader.TempFolder" /> if not.
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.PdfToImageCompression">
      <summary>
            The compression to set to the TIFF images generated or converted from the source PDF file.
            These images are then OCRed to create the searchable PDF.
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.PdfToImageDpi">
      <summary>
            The DPI of TIFF images generated from the source PDF file.
            These images are then used for barcode recognition.
            The default value for this is taken from each page in the source PDF file.
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.PdfToImageIncludeText">
      <summary>
            When set to False this will prevent the conversion of real text (i.e. electronically generated as opposed to text that is part of a scanned image)
            from being rendered in the page images extracted from the PDF.
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.PdfToImageMaxRes">
      <summary>
            The minimum resolution to use when saving the extracted images from a PDF file.
            The default value for this is 300.
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.PdfToImageMinRes">
      <summary>
            The minimum resolution to use when saving the extracted images from a PDF file.
            The default value for this is 200.
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.PerformPreprocessing">
      <summary>
            When set to true, the barcode reader will perform pre-processing on the image before barcode recognition is performed.
            The pre-processing settings are found in the <see cref="T:Aquaforest.BarcodeReader.Common.BarcodeReaderSettings" /> supplied in the constructor of <see cref="T:Aquaforest.BarcodeReader.Api.BarcodeReader" />.
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.PInvoke">
      <summary>
            Set this to true if you want to launch the pre-processing process through pinvoke.
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.ResourceFolder">
      <summary>
            The folder containing all the assemblies required for barcode recognition. If the SDK is installed in the default location, this value is 'C:\Aquaforest\OCRSDK\bin'.
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.SplitByBarcode">
      <summary>
            Set this to true to split multi-page PDF or TIFF documents by barcode identified on the pages.
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.SplitInvalidPathCharSubstitute">
      <summary>
            If the barcode value that is used to replace %VALUE% in <see cref="P:Aquaforest.BarcodeReader.Api.BarcodeReader.SplitOutputPath" /> contains invalid path characters, they will be replaced by the value set in this property.
            The default is a space ' '.
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.SplitMode">
      <summary>
            The split mode to use.
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.SplitNoBarcodeTemplate">
      <summary>
        <para>The renaming template to use for page ranges where no barcodes were identified.</para>
        <para>Allowed templates:</para>
        <para>     %FILENAME%    Replaced by the filename of the source file. Not applicable for <see cref="T:System.IO.Stream" /> and <see cref="T:System.Drawing.Image" /> sources.</para>
        <para>     %INDEX%       Replaced by the current split index.</para>
        <para>The default value is %FILENAME%_NO_BARCODE_%INDEX%</para>
      </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.SplitOutputPath">
      <summary>
        <para>The output file path template where the split files will be saved.</para>
        <para>Example path: C:\Output\%VALUE%_%INDEX%.pdf </para>
        <para>     %VALUE%      Replaced by the barcode value found.</para>
        <para>     %INDEX%      Replaced by the current split index.</para>
        <para>     %FILENAME%   Replaced by the filename of the source file. Not applicable for <see cref="T:System.IO.Stream" /> and <see cref="T:System.Drawing.Image" /> sources.</para>
        <para>If the path does not contain any of the above templates, the %INDEX% template will be added to the path.</para>
      </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.StartPage">
      <summary>
            Sets the first page of the source file where barcode recognition will begin from (for a multipage source).
            </summary>
    </member>
    <member name="P:Aquaforest.BarcodeReader.Api.BarcodeReader.TempFolder">
      <summary>
            The temporary folder used to keep the intermediary processing files.
            </summary>
    </member>
  </members>
</doc>