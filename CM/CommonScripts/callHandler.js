﻿/*

    $.ajax({
        type: "POST",
        contentType: "text/html; charset=utf-8",
        data: "{}",
        url: '../Handlers/GetCityList.ashx',
        cache: false,
        dataType: "json",
        async: false,
        success: function (data) {
            varHandlerReturnedData = data;
        }
    });

*/


var varHandlerReturnedData;

function callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync) {
    this.HandlerReturnedData = '';


    $.ajax({
        type: strType,
        contentType: strContentType,
        data: strData,
        url: strURL,
        cache: strCatch,
        dataType: strDataType,
        async: strAsync,
        success: function (data) {
            varHandlerReturnedData = data;
        },
        error: function (err) {
           
            
        }
    });

    this.HandlerReturnedData = varHandlerReturnedData;
}



function JQSelectBind(ddlParentId, ddlChildId, TableName) {

    var tableName = TableName;
  
    var mySelect = $('#' + ddlChildId);
    $(mySelect).empty();
    var parentId = $('#' + ddlParentId).val();
    if (parentId > 0) {
        var strType = "POST";
        var strContentType = "text/html; charset=utf-8";
        var strData = "{}";
        var strURL = '../Handlers/GetMasterList.ashx?TableName=' + tableName + '&ParentId=' + parentId;
        var strCatch = false;
        var strDataType = 'json';
        var strAsync = false;
        var objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
        var arr = objHandler.HandlerReturnedData;
        bindSelect(arr, mySelect);
    }
    else {
        $(mySelect).empty();
        $(mySelect).trigger("liszt:updated");
    }
}




function JQSelectBindWithPrompt(ddlParentId, ddlChildId, TableName, PromptName) {

    var tableName = TableName;
    var promptName = PromptName;
    var mySelect = $('#' + ddlChildId);
        $(mySelect).empty();
        var parentId = $('#' + ddlParentId).val();
        if (parentId > 0) {
            var strType = "POST";
            var strContentType = "text/html; charset=utf-8";
            var strData = "{}";
            var strURL = '../Handlers/GetMasterList.ashx?TableName=' + tableName + '&ParentId=' + parentId + '&PromptName=' + promptName; 
            var strCatch = false;
            var strDataType = 'json';
            var strAsync = false;
            var objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
            var arr = objHandler.HandlerReturnedData;
            bindSelect(arr, mySelect);
        }
        else {
            $(mySelect).empty();
            $(mySelect).trigger("liszt:updated");
        }
}

function JQSelectBindStr(ddlParentId, ddlChildId, TableName) {

    var tableName = TableName;
    var mySelect = $('#' + ddlChildId);
    $(mySelect).empty();
    var parentId = $('#' + ddlParentId).val();
    if (parentId > 0) {
        var strType = "POST";
        var strContentType = "text/html; charset=utf-8";
        var strData = "{}";
        var strURL = '../Handlers/GetMasterListStr.ashx?TableName=' + tableName + '&ParentId=' + parentId; //'../Handlers/GetCityList.ashx';
        var strCatch = false;
        var strDataType = 'json';
        var strAsync = false;
        var objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
        var arr = objHandler.HandlerReturnedData;
        bindSelectStr(arr, mySelect);
    }
    else {
        $(mySelect).empty();
        $(mySelect).trigger("liszt:updated");
    }
}


function JQSelectBindDefalut(parentId, ddlChildId, TableName) {

    var tableName = TableName;
    var mySelect = $('#' + ddlChildId);
    $(mySelect).empty();
   
    if (parentId > 0) {
        var strType = "POST";
        var strContentType = "text/html; charset=utf-8";
        var strData = "{}";
        var strURL = '../Handlers/GetMasterList.ashx?TableName=' + tableName + '&ParentId=' + parentId; 
        var strCatch = false;
        var strDataType = 'json';
        var strAsync = false;
        var objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
        var arr = objHandler.HandlerReturnedData;
        bindSelect(arr, mySelect);
    }
    else {
        $(mySelect).empty();
        $(mySelect).trigger("liszt:updated");
    }
}


function JQSelectBindDefalutWithPrompt(parentId, ddlChildId, TableName, PromptName) {

    var tableName = TableName;
    var promptName = PromptName;
    var mySelect = $('#' + ddlChildId);
    $(mySelect).empty();

    if (parentId > 0) {
        var strType = "POST";
        var strContentType = "text/html; charset=utf-8";
        var strData = "{}";
        var strURL = '../Handlers/GetMasterList.ashx?TableName=' + tableName + '&ParentId=' + parentId + '&PromptName=' + promptName; 
        var strCatch = false;
        var strDataType = 'json';
        var strAsync = false;
        var objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
        var arr = objHandler.HandlerReturnedData;
        bindSelect(arr, mySelect);
    }
    else {
        $(mySelect).empty();
        $(mySelect).trigger("liszt:updated");
    }
}



function JQSubordinateBind(userId, ddlDepartment, ddlUser ) {


    var mySelect = $('#' + ddlUser);
    $(mySelect).empty();
    var departmentId = $('#' + ddlDepartment).val();

    if (departmentId > 0) {
        var strType = "POST";
        var strContentType = "text/html; charset=utf-8";
        var strData = "{}";
        var strURL = '../Handlers/GetSubordinateUsersList.ashx?DepartmentId=' + departmentId + '&UserId=' + userId;
        var strCatch = false;
        var strDataType = 'json';
        var strAsync = false;
        var objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
        var arr = objHandler.HandlerReturnedData;
        bindSelect(arr, mySelect);
    }
    else {
        $(mySelect).empty();
        $(mySelect).trigger("liszt:updated");
    }
}
