﻿




function RowColor(tbClass) {
    try {
        var tableClass = tbClass;
        $("." + tableClass).find('tbody').find('tr').each(function (t) {

            if (t % 2 === 0) {
                $(this).closest('tr').addClass('even');
            }
            else {
                $(this).closest('tr').addClass('odd');
            }
        });

    } catch (e) {

    }
}



$('.InputBlock').live('keydown', function (e) {
    if (e.keyCode == 46) {
        $(this).val('');
    }
    else {
        return false;
    }
});





$(document).ready(function () {

    $(".datepicker").datepicker({ showOn: "both",
        buttonImage: "../Styles/css/icons/16/calendar_1.png",
        buttonImageOnly: true,
        prevText: 'Previous',
        yearRange: "-100:+10",
        changeMonth: true,
        dateFormat: 'dd-M-yy',
        buttonText: '',
        onClose: function (dateText, inst) {
        },
        changeYear: true
    }).attr('placeholder', 'dd-MMM-yyyy').blur(function () {
    });
});


$(document).ready(function () {
    if ($('#lblRecordStatus').html() != '') {
//        $('#btnExportToExcel, #btnExportToPDF').attr('onclick', 'return false;').addClass('areadOnly');
//    }
//    else {
        $('#btnExportToExcel, #btnExportToPDF').removeAttr('onclick').removeClass('areadOnly');
    }

    $('#btnExportToExcel, #btnExportToPDF').click(function () {
        $('#hdnGlobalReportFlag').val($('#hdnReportFlag').val());
    });

    RowColor('reportTable');

});
