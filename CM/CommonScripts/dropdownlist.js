﻿


function bindSelectCustom(arr, mySelect, valueNameofArray, textNameofArray) {
  
    try {
        $(mySelect).empty();
        $(arr).each(function (i, val) {
            $(mySelect).append($('<option>', {
                value: eval('val.' + valueNameofArray),
                text: eval('val.' + textNameofArray)
            }));
        });
        $(mySelect).trigger("liszt:updated");
    }
    catch (err) {

    }
}


function updateSettings(mySelect) {
    $(mySelect).trigger("liszt:updated");
}




function bindSelect(arr, mySelect) {
   
    try {
        $(mySelect).empty();
        $(arr).each(function (i, val) {
            $(mySelect).append($('<option>', {
                value: val.Id,
                text: val.Name
            }));
        });
        $(mySelect).trigger("liszt:updated");
    }
    catch (err) {

    }
}


function bindSelectStr(arr, mySelect) {

    try {
        $(mySelect).empty();
        $(arr).each(function (i, val) {
            $(mySelect).append($('<option>', {
                value: val.Ids,
                text: val.Name
            }));
        });
        $(mySelect).trigger("liszt:updated");
    }
    catch (err) {

    }
}




function SetChosen() {
    var config = {
        '.chzn-select': {},
        '.chzn-select-deselect': { allow_single_deselect: true },
        '.chzn-select-no-single': { disable_search_threshold: 10 },
        '.chzn-select-no-results': { no_results_text: 'Oops, nothing found!' },
        '.chzn-select-width': { width: "150%" }
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
}


        function getCSVFromArray(arr) {
        var _val = '';

        try {

        for (var i = 0; i < arr.length; i++) {
            if (i == 0) {
                _val = arr[i].toString();
            }
            else {
                _val = _val + ',' + arr[i].toString();
            }
        }

        } catch (e) {
        }

        return _val;
        }



        function bindSelectCustomSingleSelect(arr, mySelect, valueNameofArray, textNameofArray) {

            try {
                $(mySelect).empty();

                $(mySelect).append($('<option>', {
                    value: 0,
                    text: 'Select an option'
                }));
                $(arr).each(function (i, val) {

                    $(mySelect).append($('<option>', {
                        value: eval('val.' + valueNameofArray),
                        text: eval('val.' + textNameofArray)
                    }));
                });
                $(mySelect).trigger("liszt:updated");
            }
            catch (err) {

            }
        }


        // used for added disable items in edit mode -- aslo required proper changes in corresponding stored proc.  sk 2 Aug 2014

        function addItemToSelectIfNotFound(mySelect, itemValue, itemText) {
            try {
                $('#' + mySelect).append($('<option>', {
                    value: itemValue,
                    text: itemText
                }));
                $(mySelect).trigger("liszt:updated");
            } catch (e) {
            }
        }
