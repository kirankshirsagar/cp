﻿

/// grid select starts

var myLabelId = 0;          // 
var MasterTable = "";       // master table name
var PrimaryId = 0;          // prime key filed of master table

function defaultTableValueSetting(labelId, masterTableName, primaryFiledName) {
    try {
        myLabelId = labelId;
        MasterTable = masterTableName;
        PrimaryId = primaryFiledName;
    } catch (e) {
    }

}

function CkeckBoxSelect(tableClass, deleteLabelId, deleteLabelName) {
    try {

        this.tableClass = tableClass;
        this.deleteLabelId = deleteLabelId;
        this.deleteLabelName = deleteLabelName;
        this.DeletedIds = '';
        this.UsedNames = '';
        var PrimaryIds = '';
        var Used = '';
        var cnt = 0;
        var LtableClass = this.tableClass;
        var LdeleteLabelId = this.deleteLabelId;
        var LdeleteLabelName = this.deleteLabelName;

        $("." + LtableClass).find('tbody').find('td').find(':checkbox').each(function () {

            if ($(this).is(":checked")) {
                var CurrentId = $(this).closest('tr').find('[id^="' + LdeleteLabelId + '"]').html();
                if (CurrentId != null) {
                    if (cnt == 0) {
                        PrimaryIds += CurrentId;
                    }
                    else {
                        PrimaryIds += ',' + CurrentId;
                    }
                }
                else {
                    cnt--;
                }
                var UsedId;
                if ($(this).closest('tr').find('#lblIsUsed').length == 0) {
                    UsedId = "N";
                }
                else {
                    UsedId = ($(this).closest('tr').find('#lblIsUsed').html().trim());
                }

                if (UsedId == 'Y') {
                    //var usedPrimary = ($(this).closest('tr').find('#' + LdeleteLabelName).html().trim());
                    var usedPrimary = ($(this).closest('tr').find('[id^="' + LdeleteLabelName + '"]').html());
                    Used += $.trim(usedPrimary) + ', ';
                }
                cnt++;
            }
        });
        Used = Used.trim();

        if (Used != '') {
            Used = Used.substring(0, Used.lastIndexOf(","));
        }
        this.DeletedIds = PrimaryIds;
        this.UsedNames = Used;
    } catch (e) {
    }

}



function SetMainCheckbox(obj) {
    try {
        if (!$(obj).attr('checked')) {
            $(obj).closest('table').find('thead').find('.maincheckbox').prop('checked', false);
            $(obj).closest('table').find('thead').find(".maincheckbox").removeClass('checked');
        }
    } catch (e) {
    }
}

$(document).ready(function () {
    try {
        $('.maincheckbox').on("change", function (event) {
            if ($(this).is(":checked")) {
                $(this).closest('table').find('tbody').find('tr').find(':checkbox').each(function () {
                    if ($(this).prop('disabled') == false) {
                        $(this).prop('checked', true);
                        $(this).addClass('checked');
                    }
                });
            }
            else {
                $(this).closest('table').find('tbody').find('tr').find(':checkbox').each(function () {
                    $(this).prop('checked', false);
                    $(this).removeClass('checked');
                });
            }
        });
    } catch (e) {
    }
});

function RowColor(tbClass) {
    try {
        var tableClass = tbClass;
        $("." + tableClass).find('tbody').find('tr').each(function (t) {

            if (t % 2 === 0) {
                $(this).closest('tr').addClass('even');
            }
            else {
                $(this).closest('tr').addClass('odd');
            }
        });

    } catch (e) {

    } 
}






function LockUnLockImage(tbClass) {
   
   
    try {
        var tableClass = tbClass;
        $("." + tableClass).find('tbody').find('tr').each(function (t) {
            
            if (t % 2 === 0) {
                $(this).closest('tr').addClass('even');
            }
            else {
                $(this).closest('tr').addClass('odd'); 
            }

            var img = ($(this).closest('tr').find('#' + 'imgLock'));
            var UsedId = ($(this).find('#lblIsUsed').html().trim());
            if (UsedId == 'Y') {
                $(img).attr('src', '../Images/link.png').attr('title', 'used');
            }
            else {
                $(img).attr('src', '../Images/link_break.png').attr('title', 'unused');
            }
        });

    } catch (e) {

    }
}



//For alternate color
function RowColor(tbClass) {


    try {
        var tableClass = tbClass;
        $("." + tableClass).find('tbody').find('tr').each(function (t) {

            if (t % 2 === 0) {
                $(this).closest('tr').addClass('even');
            }
            else {
                $(this).closest('tr').addClass('odd');
            }
        });

    } catch (e) {

    }
}







/// grid select ends.


function resetPrimeID() {
    $('#hdnPrimeIds').val('');
}





