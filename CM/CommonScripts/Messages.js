﻿
        var delayTime = 1000;

        function msgSaveSuccess() {
            MessageMasterDiv('Record saved successfully.', 0, delayTime);
        }

        function msgUpdateSuccess() {
            MessageMasterDiv('Record updated successfully.', 0, delayTime);
        }

        function msgExistsRecord() {
            MessageMasterDiv('Record already exists.', 1, delayTime);
        }

        function msgSaveFail() {
            MessageMasterDiv('Record saved failed.', 1, delayTime);
        }

        function msgUpdateFail() {
            MessageMasterDiv('Record update failed.', 1, delayTime);
        }

        function msgInvalidTemplate() {
            MessageMasterDiv('Invalid file uploaded.', 1, delayTime);
        }
        function msgAllColumnsEmpty() {
            MessageMasterDiv('All columns are empty.', 1, delayTime);
        }        


        function MessageMasterDiv(msg, flg, delayTime) {
            setTimeout(function () {
                if (flg == 0) {
                    $(".divMasterMsg").attr("id", "flash_notice").addClass('flash notice').show();
                    $('.divMasterMsg').html(msg);
                }
                else {
                    $(".divMasterMsg").attr("id", "errorExplanation").removeClass('flash notice').show();
                    $('.divMasterMsg').html(msg);
                }
            }, delayTime);


            setTimeout(function () {
                $('.divMasterMsg').fadeOut('slow');
            }, 8000);
        }
        //Added by Bharati 25062014 2PM
        function SectionMessageDiv(DivId, msg, flg, delayTime) {
            setTimeout(function () {                               
                if (flg == 0) {
                    $("#" + DivId).addClass('flash notice').show();
                    $("#" + DivId).html(msg);
                }
                else {
                    $("#" + DivId).removeClass('flash notice').show();
                    $("#" + DivId).html(msg);
                }
            }, delayTime);
            setTimeout(function () {
                $("#" + DivId).fadeOut('slow');
            }, 8000);
        }
        //ended here



        // sk 18Aug2014
        function PageWriteMessage(msg, flg) {
            if (flg == 0) {
                // success
                MessageMasterDiv(msg, 0, delayTime);
            }
            else {
                // fail
                MessageMasterDiv(msg, 1, delayTime);
            }
        }
        // sk 18Aug2014




        function PageGetMessage(flg) {
          
            if (flg == "0") {
                MessageMasterDiv('Record saved successfully.', 0);
            }
            else if (flg == "1") {
                MessageMasterDiv('Record updated successfully.', 0);
            }
            else if (flg == "SS") {
                MessageMasterDiv('Status updated successfully.', 0);
            }
            else if (flg == "SF") {
                MessageMasterDiv('Status updated successfully.', 0);
            }
            else if (flg == "DS") {
                MessageMasterDiv('Record(s) deleted successfully.', 0);
            }
            else if (flg == "DF") {
                MessageMasterDiv('Record(s) delete failed.', 1);
            }
            else if (flg == "MS") {
                MessageMasterDiv('Email(s) Sent', 0);
            }
            else if (flg == "MF") {
                MessageMasterDiv('Email(s) Sent Failed', 1);
            }
            else if (flg == "US") {
                MessageMasterDiv('Record(s) unlocked successfully.', 0);
            }
            else if (flg == "UF") {
                MessageMasterDiv('Record(s) unlocked failed.', 1);
            }
            else if (flg == "RP") {
                MessageMasterDiv('Reset password request email has been sent.', 0);
            } 

        }
       





        $(document).ready(function () {
            $('.divMasterMsg').click(function () {
                $('.divMasterMsg').fadeOut('slow');
            });



            $("ul#topnav li").hover(function () { //Hover over event on list item    

                $(this).each(function () {
                    if (!$(this).hasClass('selectedtab')) {
                        $(this).css({ 'background': '#759FCF' });  //Add background color + image on hovered list item          
                    }
                    else {
                        $(this).addClass("selectedtab");
                    }
                });
                $(this).find("span").show(); //Show the subnav
            }, function () { //on hover out...               

                $(this).each(function () {
                    if (!$(this).hasClass('selectedtab')) {
                        $(this).css({ 'background': 'none' }); //Ditch the background      
                    }
                    else {
                        $(this).addClass("selectedtab");
                    }
                });

                $(this).find("span").hide(); //Hide the subnav
            });


        });


        function DeleteConfrim() {
            var result = confirm('Do you want to delete this record(s)?');
            if (result == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function HideShow() {
            if ($("#toggleimage").attr('src') == "../images/bullet_arrow_right.png") {

                $("#main").addClass("nosidebar");
                $("#toggleimage").attr('src', '../images/bullet_arrow_left.png');
            }
            else {
                $("#toggleimage").attr('src', '../images/bullet_arrow_right.png');
                $("#main").removeClass("nosidebar");
            }
        }
        
 