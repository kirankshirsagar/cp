﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WorkflowBLL;
using CommonBLL;
using MetaDataConfiguratorsBLL;

public partial class UserControl_KeyObligationsPdf : System.Web.UI.UserControl
{
    public static int RecordsPerPage = 30;
    public static int VisibleButtonNumbers = 10;

    IKeyFields objkey;
    public static string trueimg = "../Images/AvailYesIco.gif";
    public static string falseimg = "../Images/AvailNoIco.png";

    string LinkStatus = string.Empty;

    public string UserControlProperty { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        BindValues();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            setAccessValues();
        }
        BindContractRequest();
    }

    private void setAccessValues()
    {
        #region KeyField Tab Permission
        Access.PageAccess("WorkflowKeyObligationsTab", Session[Declarations.User].ToString());
        ViewState[Declarations.Update] = Access.Update.Trim();
        #endregion
    }

    void CreateObjects()
    {
        objkey = FactoryWorkflow.GetKeyFieldsDetails();
    }

    public void BindValues()
    {
        try
        {
            List<KeyFeilds> mylist;
            int requestId = int.Parse(Request.QueryString["RequestId"]);
            ViewState["RequestId"] = requestId;
            objkey.RequestId = requestId;
            mylist = objkey.ReadData();
            lblLiabilityCap.Text = mylist[0].LiabilityCap;
            //lblIndemnity.Text = mylist[0].Indemnity;
            lblStrictliability.Text = mylist[0].Strictliability;
            lblInsurance.Text = mylist[0].Insurance;
            lblTerminationdesc.Text = mylist[0].TerminationDescription;
            lblNovation.Text = mylist[0].AssignmentNovation;
            lblOthers.Text = mylist[0].Others;
            lblForceMajeureDescription.Text = mylist[0].ForceMajeure;
            lblSetoffrightsDescription.Text = mylist[0].SetOffRights;
            lblGoverningDescription.Text = mylist[0].GoverningLaw;
            keyImage(mylist);
        }
        catch { }
    }

    private void BindContractRequest()
    {
        try
        {
            IMetaDataConfigurators objMetaDataConfig = FactoryMedaData.GetContractTemplateDetail();
            Control container = objMetaDataConfig.GenerateRequestForm("Key Obligations", ViewState["RequestId"].ToString(), "view");
            pnlnewadd.Controls.Add(container);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    void keyImage(List<KeyFeilds> mylist)
    {
        try
        {
            lblchangeofControlYesNo.Text = mylist[0].IsChangeofControl == "Y" ? "Yes" : "No";
            lblIsAgreementClauseYesNo.Text = mylist[0].IsAgreementClause == "Y" ? "Yes" : "No";
            lblStrictliabilityYesNo.Text = mylist[0].IsStrictliability == "Y" ? "Yes" : "No";
            lblThirdPartyGuaranteeRequiredYesNo.Text = mylist[0].IsGuaranteeRequired == "Y" ? "Yes" : "No";
            lblTerminationConvenience.Text = mylist[0].IsTerminationConvenience == "Y" ? "Yes" : "No";
            lblTerminationMaterialYesNo.Text = mylist[0].IsTerminationmaterial == "Y" ? "Yes" : "No";
            lblTerminationinsolvencyYesNo.Text = mylist[0].IsTerminationinsolvency == "Y" ? "Yes" : "No";
            lblTerminationControlYesNo.Text = mylist[0].IsTerminationinChangeofcontrol == "Y" ? "Yes" : "No";
            lblTerminationothercausesYesNo.Text = mylist[0].IsTerminationothercauses == "Y" ? "Yes" : "No";
            //lblIndirectConsequentialLosses.Text = mylist[0].IsIndirectConsequentialLosses == "Y" ? "Yes" : "No";
            lblIndirectConsequentialLosses.Text = mylist[0].IndirectConsequentialLosses;
            lblIndemnity.Text = mylist[0].IsIndemnity == "Y" ? "Yes" : "No";
            lblWarranties.Text = mylist[0].IsWarranties == "Y" ? "Yes" : "No";
            lblForceMajeure.Text = mylist[0].IsForceMajeure == "Y" ? "Yes" : "No";
            lblSetoffrights.Text = mylist[0].IsSetOffRights == "Y" ? "Yes" : "No";
            lblGoverningLaw.Text = mylist[0].IsGoverningLaw == "Y" ? "Yes" : "No";
        }
        catch { }

    }
}