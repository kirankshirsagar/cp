﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Configuratorlinks.ascx.cs"
    Inherits="UserControl_Configuratorlinks" %>
<h3>
    Configurators</h3>
<p style="line-height: 20px;">
    <asp:Repeater ID="rptLinks" runat="server">
        <HeaderTemplate>
            <table width="100%">
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <a href='<%#Eval("PageLink") %>' class='<%#Eval("PageName").ToString().Replace(" ","") %>'>
                        <%#Eval("PageName") %>
                    </a>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody> </table>
        </FooterTemplate>
    </asp:Repeater>


     <script language="javascript" type="text/javascript">
         $(window).load(function () {
             var linkToDownload = $(".DownloadTemplateGenerator").prop("href");
             if (linkToDownload.indexOf('.zip') == -1)
                 $(".DownloadTemplateGenerator").prop("href", linkToDownload + location.pathname.split('/')[1]+".zip");
         });
     </script>
    
  <%--  <h3>
        Bulk Import</h3>
    <asp:Repeater ID="rptBulkImport" runat="server">
        <HeaderTemplate>
            <table width="100%">
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <a href='<%#Eval("PageLink") %>'>
                        <%#Eval("PageName") %>
                    </a>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody> </table>
        </FooterTemplate>
    </asp:Repeater>--%>
  
</p>
