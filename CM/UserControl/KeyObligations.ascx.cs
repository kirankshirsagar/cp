﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WorkflowBLL;
using CommonBLL;
using MetaDataConfiguratorsBLL;

public partial class UserControl_KeyObligations : System.Web.UI.UserControl
{
    public static int RecordsPerPage = 30;
    public static int VisibleButtonNumbers = 10;

    IKeyFields objkey;
    public static string trueimg = "../Images/AvailYesIco.gif";
    public static string falseimg = "../Images/AvailNoIco.png";

    public string UserControlProperty { get; set; }

    string LinkStatus = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        LinkStatus = UserControlProperty;
        CreateObjects();
        BindValues();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            setAccessValues();
            BindContractRequest();
        }
    }

    private void setAccessValues()
    {
        //Added by Dilip 17-12-2014
        #region KeyField Tab Permission
        Access.PageAccess("WorkflowKeyObligationsTab", Session[Declarations.User].ToString());
        ViewState[Declarations.Update] = Access.Update.Trim();
        if (ViewState[Declarations.Update].ToString() == "N")
        {
            aEditKeyObligations.Visible = false;
        }

        #endregion
    }

    void CreateObjects()
    {

        objkey = FactoryWorkflow.GetKeyFieldsDetails();
    }
    public void BindValues()
    {
        try
        {
            List<KeyFeilds> mylist;
            int requestId = int.Parse(Request.QueryString["RequestId"]);
            ViewState["RequestId"] = requestId;
            objkey.RequestId = requestId;
            mylist = objkey.ReadData();
            lblLiabilityCap.Text = mylist[0].LiabilityCap;
            lblIndemnity.Text = mylist[0].Indemnity;
            lblStrictliability.Text = mylist[0].Strictliability;
            lblInsurance.Text = mylist[0].Insurance;
            lblTerminationdesc.Text = mylist[0].TerminationDescription;
            lblNovation.Text = mylist[0].AssignmentNovation;
            lblOthers.Text = mylist[0].Others;

            lblForceMajeure.Text = mylist[0].ForceMajeure;
            lblSetoffrightsDescription.Text = mylist[0].SetOffRights;
            lblGoverningLaw.Text = mylist[0].GoverningLaw;

            keyImage(mylist);

            if (mylist[0].IsIndirectConsequentialLosses == "Y")
            {
                aIndirectConsequentialLosses.Visible = true;
            }
            if (mylist[0].IsIndemnity == "Y")
            {
                aIndemnity.Visible = true;
            }

            if (mylist[0].IsWarranties == "Y")
            {
                aWarranties.Visible = true;
            }

            if (mylist[0].IsAgreementClause == "Y")
                aAgreementClause.Visible = true;
            if (mylist[0].IsGuaranteeRequired == "Y")
                aGuaranteeRequired.Visible = true;

            lblIndirectConsequentialLosses.Text = mylist[0].IndirectConsequentialLosses;
            hdnIndirectConsequentialLosses.Value = mylist[0].IndirectConsequentialLosses;
            hdnIndemnity.Value = mylist[0].Indemnity;
            hdnWarranties.Value = mylist[0].Warranties;
            hdnAgreementClause.Value = mylist[0].EntireAgreementClause;
            hdnGuaranteeRequired.Value = mylist[0].ThirdPartyGuaranteeRequired;

            if (mylist[0].IndirectConsequentialLosses == "")
                aIndirectConsequentialLosses.Visible = false;
            if (mylist[0].Indemnity == "")
                aIndemnity.Visible = false;
            if (mylist[0].Warranties == "")
                aWarranties.Visible = false;
            if (mylist[0].EntireAgreementClause == "")
                aAgreementClause.Visible = false;
            if (mylist[0].ThirdPartyGuaranteeRequired == "")
                aGuaranteeRequired.Visible = false;
        }
        catch { }
    }

    private void BindContractRequest()
    {
        try
        {
            IMetaDataConfigurators objMetaDataConfig = FactoryMedaData.GetContractTemplateDetail();
            Control container = objMetaDataConfig.GenerateRequestForm("Key Obligations", ViewState["RequestId"].ToString(), "view");
            pnlnewadd.Controls.Add(container);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    void keyImage(List<KeyFeilds> mylist)
    {
        try
        {
            rdIsChangeofControlYes.ImageUrl = mylist[0].IsChangeofControl == "Y" ? trueimg : falseimg;
            rdIsChangeofControlNo.ImageUrl = mylist[0].IsChangeofControl == "N" ? trueimg : falseimg;

            rdIsAgreementClauseYes.ImageUrl = mylist[0].IsAgreementClause == "Y" ? trueimg : falseimg;
            rdIsAgreementClauseNo.ImageUrl = mylist[0].IsAgreementClause == "N" ? trueimg : falseimg;

            rdIsStrictliabilityYes.ImageUrl = mylist[0].IsStrictliability == "Y" ? trueimg : falseimg;
            rdIsStrictliabilityNo.ImageUrl = mylist[0].IsStrictliability == "N" ? trueimg : falseimg;

            rdIsGuaranteeRequiredYes.ImageUrl = mylist[0].IsGuaranteeRequired == "Y" ? trueimg : falseimg;
            rdIsGuaranteeRequiredNo.ImageUrl = mylist[0].IsGuaranteeRequired == "N" ? trueimg : falseimg;

            rdIsTerminationConvenience.ImageUrl = mylist[0].IsTerminationConvenience == "Y" ? trueimg : falseimg;
            rdIsTerminationMaterial.ImageUrl = mylist[0].IsTerminationmaterial == "Y" ? trueimg : falseimg;
            rdIsTerminationinsolvency.ImageUrl = mylist[0].IsTerminationinsolvency == "Y" ? trueimg : falseimg;
            rdIsTerminationControl.ImageUrl = mylist[0].IsTerminationinChangeofcontrol == "Y" ? trueimg : falseimg;
            rdIsTerminationothercauses.ImageUrl = mylist[0].IsTerminationothercauses == "Y" ? trueimg : falseimg;

            rdIndirectConsequentialLossesYes.ImageUrl = mylist[0].IsIndirectConsequentialLosses == "Y" ? trueimg : falseimg;
            rdIndirectConsequentialLossesNo.ImageUrl = mylist[0].IsIndirectConsequentialLosses == "N" ? trueimg : falseimg;

            rdIndemnityYes.ImageUrl = mylist[0].IsIndemnity == "Y" ? trueimg : falseimg;
            rdIndemnityNo.ImageUrl = mylist[0].IsIndemnity == "N" ? trueimg : falseimg;

            rdWarrantiesYes.ImageUrl = mylist[0].IsWarranties == "Y" ? trueimg : falseimg;
            rdWarrantiesNo.ImageUrl = mylist[0].IsWarranties == "N" ? trueimg : falseimg;

            rdForceMajeureYes.ImageUrl = mylist[0].IsForceMajeure == "Y" ? trueimg : falseimg;
            rdForceMajeureNo.ImageUrl = mylist[0].IsForceMajeure == "N" ? trueimg : falseimg;

            rdSetOffRightsYes.ImageUrl = mylist[0].IsSetOffRights == "Y" ? trueimg : falseimg;
            rdSetOffRightsNo.ImageUrl = mylist[0].IsSetOffRights == "N" ? trueimg : falseimg;

            rdGoverningLawYes.ImageUrl = mylist[0].IsGoverningLaw == "Y" ? trueimg : falseimg;
            rdGoverningLawNo.ImageUrl = mylist[0].IsGoverningLaw == "N" ? trueimg : falseimg;

        }
        catch { }
    }

    protected void EditKeyObligations_Click(object sender, EventArgs e)
    {
        //if (!string.IsNullOrEmpty(ViewState["ContractId"].ToString()))
        //{
        Server.Transfer("~/Workflow/KeyObligationMaster.aspx?LinkStatus=" + LinkStatus);
        // Response.Redirect("~/Workflow/KeyFieldsMaster.aspx", true);
        //}

    }


}