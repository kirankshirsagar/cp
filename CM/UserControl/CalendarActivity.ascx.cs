﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;
using WorkflowBLL;
using System.Security.Cryptography;
using System.Globalization;
using UserManagementBLL;
using System.Data;
using System.Web.Services;
using System.IO;
using System.Data.SqlClient;


public partial class UserControl_CalendarActivity : System.Web.UI.UserControl
{
    public static int RecordsPerPage = 30;
    public static int VisibleButtonNumbers = 10;
    IUsers objuser;
    IActivity objActivity;
    public string Add;
    public string View;
    public string Update;
    public string Delete;
    public string Id { get; set; }
    string ActivityId = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();

        try
        {
            ActivityId = Request.QueryString["ActivityId"].ToString();
            ViewState["RequestId"] = Request.QueryString["RequestId"];
            ReadData(1, 1);

            btnActivitySave.Text = "Update Activity";
        }
        catch
        {
            txtactivitydate.Value = Convert.ToDateTime(Request.QueryString["ActivityDate"]).ToString("dd-MMM-yyyy hh:mm tt");
            ActivityId = "0";
            ReadData(1, 1);
            btnActivitySave.Text = "Add Activity";
        }
        if (!IsPostBack)
        {

            if (Session[Declarations.User] != null)
            {
                ViewState["RequestId"] = Request.QueryString["RequestId"];
                objuser.UserId = 1083;
                objuser.DepartmentId = 0;
                //objuser.Re
                ddlUsers.extDataBind(objuser.SelectAllUsers());
                ddlUsers.extSelectedValues(Session[Declarations.User].ToString());
                ddlUsers.Items.Remove(ddlUsers.Items.FindByValue("0"));
                // hdnAssigneTo.Value = Convert.ToString(objuser.UserId.ToString());
                Access.PageAccess(this, Session[Declarations.User].ToString(), "UserControl_");
                ViewState[Declarations.Add] = Access.Add.Trim();
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
                ReadData();
                //ViewState[Declarations.Update]
            }

            AccessVisibility();
            hdnPrimeId.Value = "0";

        }


        Page.LoadComplete += new EventHandler(Page_LoadComplete);
        SetNavigationButtonParameters();
    }

    // navigation //

    #region Navigation
    void SetNavigationButtonParameters()
    {

    }


    protected void Page_LoadComplete(object sender, EventArgs e)
    {

    }


    #endregion

    void ReadData()
    {//ddlUsers.Multiple=true;
        Page.TraceWrite("ReadData starts.");
        try
        {
            objActivity.ActivityId = Convert.ToInt32(ActivityId);
            objActivity.ReadDataCalendar();
            List<Activity> ListA = objActivity.ActivityDates;
            foreach (var item in ListA)
            {
                ViewState["RequestId"] = item.ReqID.ToString();
                string Assinged = item.AssignToId.ToString();
                txtactivitydate.Value = Convert.ToString(item.ActivityDate.ToString("dd-MMM-yyyy hh:mm tt"));

                txtreminderdate.Value = item.ReminderDateDefaultFormat;
                if (txtreminderdate.Value.Contains("01-Jan-0001"))
                {
                    txtreminderdate.Value = "";
                }
                ddlUsers.Value = Assinged;
                hdnAssingedToDefault.Value = Assinged;
                // ddlactivitytype.Items.FindByValue(item.ActivityTypeId.ToString()).Selected = true;
                ddlactivitytype.extSelectedValues(item.ActivityTypeId.ToString(), item.ActivityTypeName.ToString());
                //   ddlstatus.Items.FindByValue(item.ActivityStatusId.ToString()).Selected = true;
                ddlstatus.extSelectedValues(item.ActivityStatusId.ToString(), item.ActivityStatusName.ToString());
                txtremarks.Value = item.ActivityText.Replace("<br>", "\r\n");
                txtRequestId.Value = item.Description;
                FileDownload.HRef = item.FileUpload;
                FileDownload.InnerText = item.FileUploadOriginal;
                FileDownload.Attributes.Add("target", "_self");
                if (item.IsForCustomer == "Y")
                {
                    rdNo.Checked = false;
                    rdYes.Checked = true;

                }
                else
                {
                    rdNo.Checked = true;
                    rdYes.Checked = false;
                }
                if (item.IsOCR.ToUpper() == "Y")
                {
                    chkIsOCR.Checked = true;
                    hdnisocr.Value = "Y";
                    //chkIsOCR.Disabled = false;
                }
                else
                    hdnisocr.Value = "N";
                hdnSelectedRequestId.Value = item.ReqID.ToString();
            }

        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData ends.");


    }
    void ReadData(int pageNo, int recordsPerPage)
    {

        Page.TraceWrite("ReadData starts.");
        try
        {
            ActivityTypeBind();
            ActivityStatusBind();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData ends.");

    }

    //Repeater Bind
    #region
    void BindActivities()
    {

        ChildBind();
    }
    public void ChildBind()
    {

    }
    #endregion

    //DropdownBind
    #region
    public void ActivityTypeBind()
    {
        MasterBLL.IActivityType objA = MasterBLL.FactoryMaster.GetActivityTypeDetail();
        objA.ActivityTypeId = int.Parse(ActivityId);
        ddlactivitytype.extDataBind(objA.SelectData());
    }
    public void ActivityStatusBind()
    {
        MasterBLL.IActivityStatus objActivityStatus = MasterBLL.FactoryMaster.GetActivityStatusDetail();
        ddlstatus.extDataBind(objActivityStatus.SelectData());
    }

    #endregion


    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objActivity = FactoryWorkflow.GetActivityDetails();
            objuser = FactoryUser.GetUsersDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    //Adding And Updating data
    #region
    protected void btnActivitySave_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        InsertUpdate();

        Page.TraceWrite("Save Update button clicked event ends.");
    }


    protected string UploadFile(string flag = "")
    {

        string filename = "";
        string NewFileName = "";
        string ext = "";
        string subPath = "";
        string GUID = Guid.NewGuid().ToString();
        string isvalid = "";
        if (FUContractTemplate.HasFile)
        {
            subPath = @"~/Uploads/" + Session["TenantDIR"] + "/ActivitiesDocuments";
            bool isExists = System.IO.Directory.Exists(Server.MapPath(subPath));
            if (!isExists)
            {
                System.IO.Directory.CreateDirectory(Server.MapPath(subPath));
            }
            filename = System.IO.Path.GetFileName(FUContractTemplate.PostedFile.FileName);
            ext = Path.GetExtension(filename);

            if (ext == ".pdf" && chkIsOCR.Checked)
            {
                subPath = @"~/Uploads/" + Session["TenantDIR"] + "/ActivitiesDocuments/ScannedActivitiesDocuments";
                isExists = System.IO.Directory.Exists(Server.MapPath(subPath));
                if (!isExists)
                {
                    System.IO.Directory.CreateDirectory(Server.MapPath(subPath));
                }
            }
            else
            {
                subPath = @"~/Uploads/" + Session["TenantDIR"] + "/ActivitiesDocuments";
            }

            if (filename.Contains('.'))
            {
                NewFileName = filename.Substring(0, filename.LastIndexOf('.'));
            }

            //  string[] AllowedExtensions = { "jpg", "gif", "png", "bmp", "tiff", "jpeg", "TIF", "blob", "xls", "xlsx", "doc", "docx", "pdf", "swf", "ppt", "pptx", "txt", "csv", "odt", "odp", "odg", "ods", "mp3", "mp4", "wmv", "msg", "zip", "avi" };


            if (ext == ".jpg" || ext == ".gif" || ext == ".png" || ext == ".bmp" || ext == ".tiff" || ext == ".jpeg" || ext == ".TIF" || ext == ".blob" || ext == ".xls" || ext == ".xlsx" || ext == ".doc" || ext == ".docx" || ext == ".pdf" || ext == ".swf" || ext == ".ppt" || ext == ".pptx" || ext == ".txt" || ext == ".csv" || ext == ".odt" || ext == ".odp" || ext == ".odg" || ext == ".ods" || ext == ".mp3" || ext == ".mp4" || ext == ".wmv" || ext == ".msg" || ext == ".zip" || ext == ".avi")
            {
                FUContractTemplate.PostedFile.SaveAs(Server.MapPath(subPath + "/" + NewFileName.Replace(" ", "") + "_" + GUID.ToString() + ext));
                isvalid = "true";

                if (ext == ".pdf" && chkIsOCR.Checked)
                {
                    AsyncIndexer.GetInstance().ConvertOCRFile(NewFileName.Replace(" ", "") + "_" + GUID.ToString(), ext, "activity");
                }

                string strUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + HttpContext.Current.Request.Url.Segments[1] + "Uploads/" + HttpContext.Current.Request.Url.Segments[1].Replace("/", "") + "/ActivitiesDocuments/" + NewFileName.Replace(" ", "") + "_" + GUID.ToString() + ext;
                if (ext == ".xls" || ext == ".xlsx" || ext == ".doc" || ext == ".docx" || ext == ".pdf" || ext == ".ppt" || ext == ".pptx" || ext == ".txt" || ext == ".csv" || ext == ".odt")
                {
                    AsyncIndexer.GetInstance().QueueForIndexing(strUrl, 1, ViewState["ContractInfo"].ToString());
                }
            }
        }

        if (flag.ToLower() == "update")
        {
            if (hdnActivityFileLocation.Value != "" && FUContractTemplate.HasFile)
            {
                DeleteFileFromPhysicalLocation.DeleteFile(hdnActivityFileLocation.Value);
                DeleteFileFromPhysicalLocation.DeleteFile(hdnActivityFileLocation.Value.Replace("ActivitiesDocuments", "ActivitiesDocuments/ScannedActivitiesDocuments"));
            }
        }

        if (FUContractTemplate.HasFile && isvalid == "true")
        {
            return NewFileName.Replace(" ", "") + "_" + GUID.ToString() + ext + "#" + NewFileName.Replace(" ", "") + ext + "#" + (FUContractTemplate.PostedFile.ContentLength / 1024).ToString();
        }
        else
        {
            return "";
        }
    }

    void InsertUpdate(int flg = 0)
    {
        Page.TraceWrite("Insert, Update starts.");
        string status = "";
        string[] UploadFileDetails;
        objActivity.RequestId = hdnSelectedRequestId.Value == "" || hdnSelectedRequestId.Value == "0" ? Convert.ToInt32(ViewState       ["RequestId"]) : Convert.ToInt32(hdnSelectedRequestId.Value);

        if (objActivity.RequestId > 0 && FUContractTemplate.HasFile)
        {
            ViewState["ContractInfo"] = objActivity.ContractDetailsByRequestId();
        }
        else
        {
            ViewState["ContractInfo"] = Session[Declarations.User];
        }

        objActivity.ActivityDate = Convert.ToDateTime(hdnActivityDate.Value);
        if (hdnReminderDate.Value != "")
        {
            if (Convert.ToDateTime(hdnReminderDate.Value) != null)
                objActivity.ReminderDate = Convert.ToDateTime(hdnReminderDate.Value);
        }
        objActivity.ActivityStatusId = Convert.ToInt32(hdnStatusId.Value);
        objActivity.ActivityTypeId = Convert.ToInt32(hdnActivityTypeId.Value);
        objActivity.ActivityText = hdnRemark.Value.Replace("\r\n", "<br>").Replace("'", "''");
        objActivity.IsForCustomer = hdnIsOnCustomerPortal.Value;
        objActivity.IsFromCalendar = "Y";
        try
        {
            ViewState["ContractId"] = hdnrequestId.Value.ToString() == "" ? "0" : hdnrequestId.Value.Split(new char[] { '(', ')' })[1];
            objActivity.RequestId = Convert.ToInt32(ViewState["ContractId"]);
        }
        catch (Exception ex)
        {
            objActivity.RequestId = 0;
        }

        if (hdnAssigneTo.Value == "")
        {
            objActivity.AssignToId = Convert.ToInt32(ddlUsers.Value);
        }
        else
        {
            objActivity.AssignToId = int.Parse(hdnAssigneTo.Value);

        }

        //objActivity.AssignToId = int.Parse(ddlAssignToUser.Value);
        objActivity.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objActivity.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objActivity.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
        objActivity.IsOCR = chkIsOCR.Checked ? "Y" : "N";
        try
        {
            if (ActivityId == "0")
            {
                objActivity.ActivityId = 0;
                UploadFileDetails = UploadFile().Split('#');
                if (UploadFileDetails.Length > 1)
                {
                    objActivity.FileUpload = UploadFileDetails[0].ToString();
                    objActivity.FileUploadOriginal = UploadFileDetails[1].ToString();
                    objActivity.FileSizeKb = UploadFileDetails[2].ToString();
                }
                //status = objActivity.InsertRecord();
                status = objActivity.InsertActivityRecord();
                ViewState["ActivityId"] = status.Split(',')[1].ToString().Trim();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tytyttrrr", "CloseForm('','1');", true);
                // Page.JavaScriptClientScriptBlock("SaveDraft", "CloseForm('"+objActivity.ActivityText+"')");
            }
            else
            {
                objActivity.ActivityId = int.Parse(ActivityId);
                UploadFileDetails = UploadFile("update").Split('#');
                if (UploadFileDetails.Length > 1)
                {
                    objActivity.FileUpload = UploadFileDetails[0].ToString();
                    objActivity.FileUploadOriginal = UploadFileDetails[1].ToString();
                    objActivity.FileSizeKb = UploadFileDetails[2].ToString();
                }
                objActivity.RequestId = hdnSelectedRequestId.Value == "" || hdnSelectedRequestId.Value == "0" ? Convert.ToInt32(ViewState["RequestId"]) : Convert.ToInt32(hdnSelectedRequestId.Value);
                status = objActivity.UpdateRecord();
                ViewState["ActivityId"] = int.Parse(ActivityId);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tytyttrrry", "CloseForm('','2');", true);
            }
        }
        catch (Exception ex)
        {
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Insert, Update ends.");

        if (objActivity.ActivityStatusId == 2)
        {
            SendEmail(6);
        }
        else
        {
            SendEmail(7);
        }
        if (objActivity.ActivityDate < DateTime.Today)
        {
            SendEmail(4);
        }
        //SendScheduledEmail();
        ClearData(status);
    }
    #endregion

    void ClearData(string status)
    {
        if (status == "1")
        {
            Page.TraceWrite("clearData starts.");
            hdnPrimeId.Value = "0";
            ddlstatus.SelectedIndex = 0;
            ddlactivitytype.SelectedIndex = 0;
            txtreminderdate.Value = "";
            txtactivitydate.Value = "";
            txtremarks.Value = "";
            rdYes.Checked = true;
            Page.TraceWrite("clear Data ends.");

        }

    }

    //Access Control
    #region
    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }
    void AccessVisibility()
    {
        setAccessValues();

        Page.TraceWrite("AccessVisibility starts.");
        if (Add == "N" && (hdnPrimeId.Value == "" || hdnPrimeId.Value == "0" || hdnPrimeId.Value == null))
        {
            //Page.extDisableControls();
            hdnAdd.Value = "N";
            //  ActivityAdd.Enabled = false;


        }
        else
        {
            hdnAdd.Value = "Y";
            //ActivityAdd.Enabled = true;
        }

        if (Update == "N")
            hdnUpdate.Value = "N";
        else
            hdnUpdate.Value = "Y";


        if (View == "N")
        {
            //Page.extDisableControls();
        }

        Page.TraceWrite("AccessVisibility starts.");
    }
    #endregion

    protected void rptChild_ItemDataBound(object Sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            LinkButton lnkEdit = (LinkButton)e.Item.FindControl("lblActivityText");
            if (Update == "N")
                lnkEdit.Enabled = false;
            else
                lnkEdit.Enabled = true;
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/WorkFlow/ContractRequestData.aspx");
    }

    void SendEmail(int EmailTriggerId)
    {
        try
        {
            //Page.TraceWrite("send emails.");
            EmailTemplateBLL.IEmailTrigger objEmail = EmailTemplateBLL.FactoryEmailTemplate.GetEmailTriggerDetail();
            objEmail.RequestId = Convert.ToInt32(ViewState["RequestId"]);
            objEmail.ContractId = Convert.ToInt32(ViewState["ContractId"]);
            objEmail.ContractTypeId = 99999;
            objEmail.ContractTemplateId = 99999;
            objEmail.EmailTriggerId = EmailTriggerId;
            objEmail.ActivityId = Convert.ToInt32(ViewState["ActivityId"]);
            objEmail.userId = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            objEmail.AssignedToUserId = objActivity.AssignToId;
            objEmail.BulkEmail();
        }
        catch (Exception)
        {
            //Page.TraceWarn("send emails fail.");
        }
    }

    void SendScheduledEmail() //Task Deadline
    {
        try
        {
            Page.TraceWrite("send scheduled emails.");
            EmailTemplateBLL.IEmailTrigger objEmail = EmailTemplateBLL.FactoryEmailTemplate.GetEmailTriggerDetail();
            objEmail.ActivityId = Convert.ToInt32(ViewState["ActivityId"]);
            objEmail.AlertDate = Convert.ToDateTime(txtactivitydate.Value.Trim());
            objEmail.EmailTriggerId = 4;
            if (Session[Declarations.User] != null)
            {
                objEmail.userId = int.Parse(Session[Declarations.User].ToString());
                objEmail.BulkEmailScheduler();
            }
        }
        catch (Exception ex)
        {
            Page.TraceWarn("send scheduled emails fail.");
        }
    }

}