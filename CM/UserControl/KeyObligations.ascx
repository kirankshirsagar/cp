﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KeyObligations.ascx.cs"
    Inherits="UserControl_KeyObligations" %>
<%-- <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    <link href="../Styles/jquery-ui-1.8.21.css" media="all" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../scripts/jquery-ui-1.8.16.custom.css" />
    <link rel="stylesheet" href="../modalfiles/modal.css" type="text/css" />
    <script src="../JQueryValidations/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../scripts/jquery-ui-timepicker-addon.js"></script>
    <link rel="stylesheet" href="../scripts/jquery-ui-timepicker-addon.css" type="text/css" />--%>
<script src="../scripts/tooltip/bootstrap.min.js" type="text/javascript"></script>
<link href="../scripts/tooltip/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script>
    $(function () {
        $('[data-toggle="popover"]').popover()
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
<style type="text/css">
    .clsToolTip
    {
        cursor: pointer;
    }
</style>
<div>
    <table width="100%">
        <tr>
            <td>
                <div class="wiki editable" id="journal-22716-notes" style="float: right">
                    <div>
                        <a id="aEditKeyObligations" href="#" title="Key Obligations Update" runat="server"
                            onserverclick="EditKeyObligations_Click" style="font-weight: normal;">
                            <img alt="Edit" src="../images/edit.png?1349001717" />
                            Key Obligations Update </a>&nbsp;&nbsp;
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <h3>
        <span style="margin-left: -15px;"><strong>Key Obligations </strong></span>
    </h3>
    <table width="100%">
        <tr>
            <td style="width: 25%" valign="top">
                <ul>
                    <li><strong>Liability Cap</strong> </li>
                </ul>
            </td>
            <td style="width: 2%" valign="top">
                :
            </td>
            <td style="width: 73%" valign="top">
                <asp:Label ID="lblLiabilityCap" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr style="display:none">
            <td style="width: 25%" valign="top">
                <ul>
                    <li><strong>Indirect/Consequential Losses</strong> </li>
                </ul>
            </td>
            <td style="width: 2%" valign="top">
                :
            </td>
            <td style="width: 73%" valign="top">
                <asp:Image ID="rdIndirectConsequentialLossesYes" runat="server" />Yes
                <asp:Image ID="rdIndirectConsequentialLossesNo" runat="server" />No <a class="clsToolTip"
                    onclick="a1Onclick(this)" data-toggle="popover" data-placement="right" data-container="body"
                    role="button" clientidmode="Static" id="aIndirectConsequentialLosses" runat="server"
                    visible="false" data-clipboard-text="1">
                    <div class="information">
                        !</div>
                </a>
                <input id="hdnIndirectConsequentialLosses" runat="server" type="hidden" clientidmode="Static"
                    value="" />
            </td>
        </tr>
        <tr >
            <td style="width: 25%" valign="top">
                <ul>
                    <li><strong>Liability for Indirect/Consequential Losses</strong> </li>
                </ul>
            </td>
            <td style="width: 2%" valign="top">
                :
            </td>
            <td style="width: 73%" valign="top">
                <asp:Label ID="lblIndirectConsequentialLosses" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
         
        <tr>
            <td valign="top">
                <ul>
                    <li><strong>Indemnity</strong> </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Image ID="rdIndemnityYes" runat="server" />Yes
                <asp:Image ID="rdIndemnityNo" runat="server" />No <a class="clsToolTip" data-clipboard-text="1"
                    id="aIndemnity" runat="server" onclick="a1Onclick(this)" data-toggle="popover"
                    data-placement="right" data-container="body" role="button" clientidmode="Static"
                    visible="false">
                    <div class="information">
                        !</div>
                </a>
                <input id="hdnIndemnity" runat="server" type="hidden" clientidmode="Static" value="" />
            </td>
        </tr>
        <tr style="display: none;">
            <td valign="top">
                <ul>
                    <li><strong>Indemnity</strong> </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblIndemnity" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr style="display: none;">
            <td valign="top">
                <ul>
                    <li><strong>Warranties</strong> </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Image ID="rdWarrantiesYes" runat="server" />Yes
                <asp:Image ID="rdWarrantiesNo" runat="server" />No
                 <a class="clsToolTip" data-clipboard-text="1" id="aWarranties" runat="server"
                    onclick="a1Onclick(this)" data-toggle="popover" data-placement="right" data-container="body"
                    role="button" clientidmode="Static" visible="false">
                    <div class="information">
                        !</div>
                </a>
                <input id="hdnWarranties" runat="server" type="hidden" clientidmode="Static" value="" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong>Change Of Control</strong></li></ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Image ID="rdIsChangeofControlYes" runat="server" />Yes
                <asp:Image ID="rdIsChangeofControlNo" runat="server" />No
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong>Entire Agreement Clause</strong> </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Image ID="rdIsAgreementClauseYes" runat="server" />Yes
                <asp:Image ID="rdIsAgreementClauseNo" runat="server" />No
                <a class="clsToolTip" data-clipboard-text="1" id="aAgreementClause"
                        onclick="a1Onclick(this)" data-toggle="popover" data-placement="right" data-container="body"
                        role="button" runat="server" visible="false">
                        <div class="information">
                            !</div>
                    </a>
                    <input id="hdnAgreementClause" runat="server" type="hidden" clientidmode="Static"
                    value="" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong>Strict liability</strong> </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Image ID="rdIsStrictliabilityYes" runat="server" />Yes
                <asp:Image ID="rdIsStrictliabilityNo" runat="server" />No
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong>Strict liability description</strong></li></ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblStrictliability" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong>Third Party Guarantee Required</strong> </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Image ID="rdIsGuaranteeRequiredYes" runat="server" />Yes
                <asp:Image ID="rdIsGuaranteeRequiredNo" runat="server" />No
                <a class="clsToolTip" clientidmode="Static" data-clipboard-text="1"
                        onclick="a1Onclick(this)" data-toggle="popover" data-placement="right" data-container="body"
                        role="button" id="aGuaranteeRequired" runat="server" visible="false">
                        <div class="information">
                            !</div>
                    </a>
                    <input id="hdnGuaranteeRequired" runat="server" type="hidden" clientidmode="Static"
                    value="" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong>Insurance</strong> </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblInsurance" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong>Termination</strong></li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblTerminationdesc" runat="server" ClientIDMode="Static"></asp:Label>
                <br />
                <asp:Image ID="rdIsTerminationConvenience" runat="server" />Termination for convenience<br />
                <asp:Image ID="rdIsTerminationMaterial" runat="server" />Termination for breach
                <br />
                <asp:Image ID="rdIsTerminationinsolvency" runat="server" />Termination for insolvency
                <br />
                <asp:Image ID="rdIsTerminationControl" runat="server" />Termination for change of
                control
                <br />
                <asp:Image ID="rdIsTerminationothercauses" runat="server" style="display:none" />
                <%--Termination for other reasons--%>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong>Assignment/Novation/Subcontract</strong> </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblNovation" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong>Force Majeure</strong> </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Image ID="rdForceMajeureYes" runat="server" />Yes
                <asp:Image ID="rdForceMajeureNo" runat="server" />No
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong>Force Majeure Description</strong></li></ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblForceMajeure" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong>Set off rights</strong> </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Image ID="rdSetOffRightsYes" runat="server" />Yes
                <asp:Image ID="rdSetOffRightsNo" runat="server" />No
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong>Set off rights Description</strong></li></ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblSetoffrightsDescription" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong>Governing Law / Applicable Law</strong> </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Image ID="rdGoverningLawYes" runat="server" />Yes
                <asp:Image ID="rdGoverningLawNo" runat="server" />No
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong>Governing Law / Applicable Law Description</strong></li></ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblGoverningLaw" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong>Others</strong></li></ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblOthers" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:PlaceHolder ID="pnlnewadd" runat="server"></asp:PlaceHolder>
</div>
<style type="text/css">
    ul
    {
        list-style-position: initial;
    }
</style>
<script type="text/javascript">
    function clickme() {
        alert('1');
    }

    var aId = '', clickId = '';
    function a1Onclick(obj) {
        clickId = '1';
        var data = '';
        if ($(obj).next('input').val() != undefined) {
            data = $(obj).next('input').val();
        }
        else {
            data = $(obj).next().next('input').val();
        }

        if (aId == '' || $(obj).attr('id') != aId) {
            aId = $(obj).attr('id');
            removepopoverp();
        }
        var t = false;
        $("div").each(function () {
            if ($(this).hasClass('popover fade right in')) {
                t = true;
            }
        });

        if (t == true) {
            $(obj).attr('data-content', data).popover('show');
        }
        else
            $(obj).attr('data-content', data).popover('hide');
    }

    function removepopoverp() {
        $("div").each(function () {
            if ($(this).hasClass('popover fade right in')) {
                $(this).remove();
            }
        });
    }
    $('#wrapper').click(function (e) {
        if (clickId != '1') {
            removepopoverp();
        }
        clickId = '';
    });
</script>
