﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WorkflowBLL;
using CommonBLL;
using MetaDataConfiguratorsBLL;
using System.Data;

public partial class UserControl_KeyFields : System.Web.UI.UserControl
{
    public static int RecordsPerPage = 30;
    public static int VisibleButtonNumbers = 10;

    IKeyFields objkey;
    public static string trueimg = "../Images/AvailYesIco.gif";
    public static string falseimg = "../Images/AvailNoIco.png";

    public string UserControlProperty { get; set; }
    string LinkStatus = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        LinkStatus = UserControlProperty;

        CreateObjects();
        BindValues();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            setAccessValues();
            BindContractRequest();
        }
    }

    private void setAccessValues()
    {
        //Added by Dilip 17-12-2014
        #region KeyField Tab Permission
        Access.PageAccess("WorkflowImportantDatesTab", Session[Declarations.User].ToString());
        ViewState[Declarations.Update] = Access.Update.Trim();
        if (ViewState[Declarations.Update].ToString() == "N")
        {
            aEditQuestionnair.Visible = false;
        }

        #endregion
    }

    void CreateObjects()
    {

        objkey = FactoryWorkflow.GetKeyFieldsDetails();
    }
    public void BindValues()
    {
        try
        {
            List<KeyFeilds> mylist;
            int requestId = int.Parse(Request.QueryString["RequestId"]);
            ViewState["RequestId"] = requestId;
            objkey.RequestId = requestId;
            mylist = objkey.ReadData();
            lblEffectivedate.Text = (mylist[0].EffectiveDatestring).ToString();
            lblexpirationdate.Text = (mylist[0].ExpirationDatestring).ToString();
            lblRenewal.Text = (mylist[0].RenewalDatestring).ToString();
            //dateImages(mylist);

            DataTable dtEx = (DataTable)Session["ImportDateDataEx"];
            DataTable dtRen = (DataTable)Session["ImportDateDataRen"];

            IEnumerable<DataRow> rowsrpt = dtEx.AsEnumerable()
             .Where(r => r.Field<string>("ExAlerts") == "Y");

            DataTable dtE = new DataTable();
            if (rowsrpt.Count() > 0)
                dtE = rowsrpt.CopyToDataTable();

            if (dtE.Rows.Count > 0)
            {
                try
                {
                    if (Convert.ToString(dtE.Rows[0]["ExAlertsDay"]) != "1")
                        lblDays1.Text = Convert.ToString(dtE.Rows[0]["ExAlertsDay"]) + " days";
                    else
                        lblDays1.Text = Convert.ToString("24") + " Hrs";
                    lblexp180user1.Text = Convert.ToString(dtE.Rows[0]["ExUserName1"]);
                    lblexp180user2.Text = Convert.ToString(dtE.Rows[0]["ExUserName2"]);
                    ExImg180.ImageUrl = trueimg;

                    if (Convert.ToString(dtE.Rows[1]["ExAlertsDay"]) != "1")
                        lblDays2.Text = Convert.ToString(dtE.Rows[1]["ExAlertsDay"]) + " days";
                    else
                        lblDays2.Text = Convert.ToString("24") + " Hrs";
                    lblexp90user1.Text = Convert.ToString(dtE.Rows[1]["ExUserName1"]);
                    lblexp90user2.Text = Convert.ToString(dtE.Rows[1]["ExUserName2"]);
                    ExImg90.ImageUrl = trueimg;

                    if (Convert.ToString(dtE.Rows[2]["ExAlertsDay"]) != "1")
                        lblDays3.Text = Convert.ToString(dtE.Rows[2]["ExAlertsDay"]) + " days";
                    else
                        lblDays3.Text = Convert.ToString("24") + " Hrs";
                    lblexp60user1.Text = Convert.ToString(dtE.Rows[2]["ExUserName1"]);
                    lblexp60user2.Text = Convert.ToString(dtE.Rows[2]["ExUserName2"]);
                    ExImg60.ImageUrl = trueimg;

                }
                catch (Exception ex) { }
            }
            else
            {
                lblExpireAlert.Text = " : No Reminder";
                tblExperationAlert.Style.Add("display", "none");
            }

            IEnumerable<DataRow> rowsrpt1 = dtRen.AsEnumerable()
           .Where(r => r.Field<string>("RenAlerts") == "Y");

            DataTable dtR = new DataTable();
            if (rowsrpt1.Count() > 0)
                dtR = rowsrpt1.CopyToDataTable();

            if (dtR.Rows.Count > 0)
            {
                try
                {
                    if (Convert.ToString(dtR.Rows[0]["RenAlertsDay"]) != "1")
                        lblRenDays1.Text = Convert.ToString(dtR.Rows[0]["RenAlertsDay"]) + " days";
                    else
                        lblRenDays1.Text = Convert.ToString("24") + " Hrs";
                    lblren180user1.Text = Convert.ToString(dtR.Rows[0]["RenUserName1"]);
                    lblren180user2.Text = Convert.ToString(dtR.Rows[0]["RenUserName2"]);
                    EffImg180.ImageUrl = trueimg;

                    if (Convert.ToString(dtR.Rows[1]["RenAlertsDay"]) != "1")
                        lblRenDays2.Text = Convert.ToString(dtR.Rows[1]["RenAlertsDay"]) + " days";
                    else
                        lblRenDays2.Text = Convert.ToString("24") + " Hrs";
                    lblren90user1.Text = Convert.ToString(dtR.Rows[1]["RenUserName1"]);
                    lblren90user2.Text = Convert.ToString(dtR.Rows[1]["RenUserName2"]);
                    EffImg90.ImageUrl = trueimg;

                    if (Convert.ToString(dtR.Rows[2]["RenAlertsDay"]) != "1")
                        lblRenDays3.Text = Convert.ToString(dtR.Rows[2]["RenAlertsDay"]) + " days";
                    else
                        lblRenDays3.Text = Convert.ToString("24") + " Hrs";
                    lblren60user1.Text = Convert.ToString(dtR.Rows[2]["RenUserName1"]);
                    lblren60user2.Text = Convert.ToString(dtR.Rows[2]["RenUserName2"]);
                    EffImg60.ImageUrl = trueimg;
                }
                catch (Exception ex) { }
            }
            else
            {
                lblRenewalDate.Text = "&nbsp; : No Reminder";
                tblRenewaldate.Style.Add("display", "none");
            }
        }
        catch { }
    }

    private void BindContractRequest()
    {
        try
        {
            IMetaDataConfigurators objMetaDataConfig = FactoryMedaData.GetContractTemplateDetail();
            Control container = objMetaDataConfig.GenerateRequestForm("Important Dates", ViewState["RequestId"].ToString(), "view");
            pnlnewadd.Controls.Add(container);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    void dateImages(List<KeyFeilds> mylist)
    {
        try
        {
            ExImg180.ImageUrl = mylist[0].ExpirationAlert180days == "Y" ? trueimg : falseimg;
            ExImg90.ImageUrl = mylist[0].ExpirationAlert90days == "Y" ? trueimg : falseimg;
            ExImg60.ImageUrl = mylist[0].ExpirationAlert60days == "Y" ? trueimg : falseimg;

            ExImg24.ImageUrl = mylist[0].ExpirationAlert24Hrs == "Y" ? trueimg : falseimg;
            ExImg30.ImageUrl = mylist[0].ExpirationAlert30days == "Y" ? trueimg : falseimg;
            ExImg7.ImageUrl = mylist[0].ExpirationAlert7days == "Y" ? trueimg : falseimg;
            EffImg24.ImageUrl = mylist[0].RenewalAlert24Hrs == "Y" ? trueimg : falseimg;
            EffImg30.ImageUrl = mylist[0].RenewalAlert30days == "Y" ? trueimg : falseimg;
            EffImg7.ImageUrl = mylist[0].RenewalAlert7days == "Y" ? trueimg : falseimg;

            EffImg180.ImageUrl = mylist[0].RenewalAlert180days == "Y" ? trueimg : falseimg;
            EffImg90.ImageUrl = mylist[0].RenewalAlert90days == "Y" ? trueimg : falseimg;
            EffImg60.ImageUrl = mylist[0].RenewalAlert60days == "Y" ? trueimg : falseimg;
        }
        catch { }
    }
    protected void EditQuestionnair_Click(object sender, EventArgs e)
    {
        //if (!string.IsNullOrEmpty(ViewState["ContractId"].ToString()))
        //{
        Server.Transfer("../Workflow/KeyFieldsMaster.aspx?LinkStatus=" + LinkStatus);
        // Response.Redirect("~/Workflow/KeyFieldsMaster.aspx", true);
        //}

    }


}