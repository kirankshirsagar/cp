﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="reportrightactions.ascx.cs"
    Inherits="UserControl_reportrightactions" %>
<div class="contextual">
    <input id="hdnGlobalReportFlag" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnGlobalReportLink" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnReportName" runat="server" clientidmode="Static" type="hidden" />
    <%-- <asp:LinkButton ID="lnkCustomisedReport" ClientIDMode="Static" CssClass="icon icon-library areadOnly" style="cursor:pointer"
        runat="server" PostBackUrl="~/Reports/CustomReports.aspx" >Customised Report</asp:LinkButton>--%>
    <asp:LinkButton ID="lnkPredefineSheduleReport" ClientIDMode="Static" CssClass="icon icon-library areadOnly"
        Style="cursor: pointer" runat="server" OnClick="lnkPredefineSheduleReport_Click">Schedule this report</asp:LinkButton>
    <asp:LinkButton ID="btnExportToExcel" ClientIDMode="Static" CssClass="icon icon-library areadOnly"
        OnClientClick="return false;" runat="server" OnClick="btnExportToExcel_Click">Export to Excel</asp:LinkButton>
    <asp:LinkButton ID="btnExportToPDF" ClientIDMode="Static" CssClass="icon icon-masters areadOnly"
        OnClientClick="return false;" runat="server" OnClick="btnExportToPDF_Click">Export to PDF</asp:LinkButton>
    <div style="display: none">
        <asp:GridView ID="gvDetails" runat="server">
        </asp:GridView>
    </div>
    <asp:DropDownList ID="ddlReport" ClientIDMode="Static" Width="350px" CssClass="chzn-select required chzn-select"
        runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlReport_SelectedIndexChanged">
    </asp:DropDownList>
    <script type="text/javascript">
        $('#ddlReport').change(function () {
            $('#hdnGlobalReportLink').val($('#ddlReport').val());
            $('#hdnReportName').val($('#ddlReport :selected').text());
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            //alert($('#hdnGlobalReportFlag').val());
            $.ajax({
                type: "POST",
                url: "../Reports/PredefinedScheduleReport.aspx/GetReportIDs",
                data: "{ReportId:'" + $('#hdnGlobalReportFlag').val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (output) {
                    if (output.d != "") {
                        $('#lnkPredefineSheduleReport').text('Scheduled report');
                    }
                }
            });
        });
    </script>
</div>
