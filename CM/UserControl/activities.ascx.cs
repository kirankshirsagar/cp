﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;
using WorkflowBLL;
using System.Security.Cryptography;
using UserManagementBLL;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class UserControl_activities : System.Web.UI.UserControl
{
    public static int RecordsPerPage = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["RecordsPerPage"]);
    public static int VisibleButtonNumbers = 10;

    IActivity objActivity;
    IUsers objUser;

    public string Add;
    public string View;
    public string Update;
    public string Delete;
    string LinkStatus = string.Empty;
    public string UserControlProperty { get; set; }
    public string ContractInfo { get; set; }
    public int ContractTypeId { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.HtmlControls.HtmlInputHidden hdnRecordsPerPage = (System.Web.UI.HtmlControls.HtmlInputHidden)PaginationButtons1.FindControl("hdnRecordsPerPage");
        RecordsPerPage = hdnRecordsPerPage.Value == "" ? RecordsPerPage : Convert.ToInt32(hdnRecordsPerPage.Value);

        CreateObjects();

        if (!IsPostBack)
        {
            hdnLinkStatus.Value = UserControlProperty;
            if (Session[Declarations.User] != null)
            {
                Access.PageAccess(this, Session[Declarations.User].ToString(), "UserControl_");
                ViewState[Declarations.Add] = Access.Add.Trim();
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
                ViewState["RequestId"] = Request.QueryString["RequestId"];
                UserBind();
            }
            AccessVisibility();
            hdnPrimeId.Value = "0";
            ReadData(1, RecordsPerPage);
            ViewState["ContractInfo"] = ContractInfo;
            string Date = DateTime.Now.ToString("dd-MMM-yyy h:mm tt");
            txtactivitydate.Value = Date;
        }
        Page.LoadComplete += new EventHandler(Page_LoadComplete);
        SetNavigationButtonParameters();

        if (Session[Declarations.Message] != null)
        {
            var msg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "delete1", "setActivityMessageFlag('" + msg + "');", true);
        }

        if (FileDownload.InnerHtml != "")
        {

            FUContractTemplate.Style.Add("color", "transparent");

        }
        else
        {
            FUContractTemplate.Style.Add("color", "");
        }


    }

    #region Navigation
    void SetNavigationButtonParameters()
    {

        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            AccessVisibility();
            ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage, 1);
        }
    }

    #endregion

    void ReadData(int pageNo, int recordsPerPage, int flag)
    {
        if (View == "Y")
        {
            Page.TraceWrite("ReadData starts.");
            try
            {
                objActivity.Flag = flag;
                objActivity.PageNo = pageNo;
                objActivity.RecordsPerPage = RecordsPerPage;
                objActivity.RequestId = Convert.ToInt32(ViewState["RequestId"]);
                objActivity.ReadData();
                BindActivities();
                ViewState[Declarations.TotalRecord] = objActivity.TotalRecords;
                PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
                PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
                PaginationButtons1.RecordsPerPage = RecordsPerPage;
            }
            catch (Exception ex)
            {
                Page.TraceWarn("ReadData fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            Page.TraceWrite("ReadData ends.");
        }
    }

    void ReadData(int pageNo, int recordsPerPage)
    {

        Page.TraceWrite("ReadData starts.");
        try
        {
            objActivity.Flag = 0;
            objActivity.PageNo = pageNo;
            objActivity.RecordsPerPage = RecordsPerPage;
            objActivity.RequestId = Convert.ToInt32(ViewState["RequestId"]);
            objActivity.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            objActivity.ReadData();
            if (View == "Y")
            {
                BindActivities();
                ViewState[Declarations.TotalRecord] = objActivity.TotalRecords;
                PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
                PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
                PaginationButtons1.RecordsPerPage = RecordsPerPage;
            }
            ActivityTypeBind();
            ActivityStatusBind();

        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData ends.");

    }

    //Repeater Bind
    #region
    void BindActivities()
    {
        rptParent.DataSource = objActivity.ActivityDates;
        rptParent.DataBind();
        ChildBind();
    }

    public void ChildBind()
    {
        List<Activity> child = new List<Activity>();
        child = objActivity.ActivityOtherDetails;
        for (int i = 0; i <= rptParent.Items.Count - 1; i++)
        {
            Label dateval = (Label)rptParent.Items[i].FindControl("lblactivitydate");

            Repeater rptChild = (Repeater)rptParent.Items[i].FindControl("rptChild");
            var result = child.FindAll(x => x.OnlyDate == Convert.ToDateTime(dateval.Text));
            //var result = child.FindAll(x =>1==1);

            rptChild.DataSource = result;
            rptChild.DataBind();
        }
    }
    #endregion

    //DropdownBind
    #region
    public void ActivityTypeBind()
    {
        MasterBLL.IActivityType objActivityType = MasterBLL.FactoryMaster.GetActivityTypeDetail();
        ddlactivitytype.extDataBind(objActivityType.SelectData());
    }

    public void ActivityStatusBind()
    {
        MasterBLL.IActivityStatus objActivityStatus = MasterBLL.FactoryMaster.GetActivityStatusDetail();
        ddlstatus.extDataBind(objActivityStatus.SelectData());
    }

    public void UserBind()
    {
        if (Session[Declarations.User] != null)
        {
            try
            {
                objUser.UserId = 1083;// int.Parse(Session[Declarations.User].ToString());
                objUser.ContractTypeId = ContractTypeId;
                ddlAssignToUser.extDataBind(objUser.SelectAllUsers());
                ddlAssignToUser.extSelectedValues(Session[Declarations.User].ToString());
            }
            catch (Exception ex)
            {
                if (ex.Message == "Some recipients were removed as their email ids have changed.")
                {
                    //req
                    //ContractTypeMessage
                }
            }
        }
    }

    #endregion

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objActivity = FactoryWorkflow.GetActivityDetails();
            objUser = FactoryUser.GetUsersDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    //Adding And Updating data
    #region
    protected void btnActivitySave_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        InsertUpdate();
        Page.TraceWrite("Save Update button clicked event ends.");
    }

    protected void lnkDeleteActivity_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Delete starts.");
        string status = "";
        objActivity.RequestId = Convert.ToInt32(ViewState["RequestId"]);
        objActivity.ActivityIds = hdnDeleteActivityId.Value;
        objActivity.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        try
        {
            status = objActivity.DeleteRecord();
            DeleteFileFromPhysicalLocation.DeleteFile(hdnActivityFileLocation.Value);

            if (hdnisocr.Value.ToUpper() == "Y")
            {
                DeleteFileFromPhysicalLocation.DeleteFile(hdnActivityFileLocation.Value.Replace("ActivitiesDocuments", "ActivitiesDocuments/ScannedActivitiesDocuments"));
            }
            string strUrl = "";
            strUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + HttpContext.Current.Session["TenantDIR"] + hdnActivityFileLocation.Value.Replace("../Uploads", "/Uploads");
            AsyncIndexer.GetInstance().QueueForIndexing(strUrl, 2);

            Session[Declarations.Message] = "Activity deleted successfully.";
        }
        catch (Exception ex)
        {
            hdnDeleteActivityId.Value = string.Empty;
            Page.TraceWarn("Delete fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        hdnDeleteActivityId.Value = string.Empty;
        Page.TraceWrite("Delete ends.");
        ClearData(status);

        if (status == "1")
        {
            try
            {
                // Response.Redirect("~/WorkFlow/RequestFlow.aspx?selectedTab=tab-activity_" + hdnPrimeId.Value + "&RequestId=" + ViewState["RequestId"]);
                ViewState["ParentRequestID"] = ViewState["RequestId"].ToString() == hdnRFParentRequestID.Value ? "" : "&ParentRequestID=" + hdnRFParentRequestID.Value;
                if (hdnLinkStatus.Value == "Volts")
                {
                    Response.Redirect("~/WorkFlow/VaultFlow.aspx?selectedTab=tab-activity_" + hdnPrimeId.Value + "&RequestId=" + ViewState["RequestId"]);
                }
                else
                {
                    Response.Redirect("~/WorkFlow/RequestFlow.aspx?selectedTab=tab-activity_" + hdnPrimeId.Value + "&RequestId=" + ViewState["RequestId"] + ViewState["ParentRequestID"]);
                }
            }
            catch (Exception ex)
            {
            }
        }
    }

    void InsertUpdate(int flg = 0)
    {
        Page.TraceWrite("Insert, Update starts.");
        string status = "";
        string[] UploadFileDetails;
        objActivity.RequestId = Convert.ToInt32(ViewState["RequestId"]);
        objActivity.ActivityDate = Convert.ToDateTime(txtactivitydate.Value.Trim());
        if (txtreminderdate.Value != "")
        {
            objActivity.ReminderDate = Convert.ToDateTime(txtreminderdate.Value.Trim());
        }
        objActivity.ActivityStatusId = int.Parse(ddlstatus.Value);
        objActivity.ActivityTypeId = int.Parse(ddlactivitytype.Value);
        objActivity.ActivityText = txtremarks.Value;
        objActivity.IsForCustomer = rdYes.Checked == true ? "Y" : "N";
        objActivity.AssignToId = int.Parse(ddlAssignToUser.Value);
        objActivity.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objActivity.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objActivity.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
        objActivity.IsOCR = chkIsOCR.Checked ? "Y" : "N";
        try
        {
            if (hdnPrimeId.Value == "0")
            {
                objActivity.ActivityId = 0;
                UploadFileDetails = UploadFile().Split('#');
                if (UploadFileDetails.Length > 1)
                {
                    objActivity.FileUpload = UploadFileDetails[0].ToString();
                    objActivity.FileUploadOriginal = UploadFileDetails[1].ToString();
                    objActivity.FileSizeKb = UploadFileDetails[2].ToString();
                }
                //status = objActivity.InsertRecord();
                status = objActivity.InsertActivityRecord();
                ViewState["ActivityId"] = status.Split(',')[1].ToString().Trim();
                Session[Declarations.Message] = "Activity saved successfully.";
            }
            else
            {
                objActivity.ActivityId = int.Parse(hdnPrimeId.Value);
                UploadFileDetails = UploadFile("update").Split('#');
                if (UploadFileDetails.Length > 1)
                {
                    objActivity.FileUpload = UploadFileDetails[0].ToString();
                    objActivity.FileUploadOriginal = UploadFileDetails[1].ToString();
                    objActivity.FileSizeKb = UploadFileDetails[2].ToString();
                }
                status = objActivity.UpdateRecord();
                ViewState["ActivityId"] = hdnPrimeId.Value.Trim();
                Session[Declarations.Message] = "Activity updated successfully.";
            }
        }
        catch (Exception ex)
        {
            Page.TraceWarn("Insert, Update fails.");

            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Insert, Update ends.");

        if (objActivity.ActivityStatusId == 2)
        {
            SendEmail(6);
        }
        else
        {
            SendEmail(7);
        }
        if (objActivity.ActivityDate < DateTime.Today)
        {
            SendEmail(4);
        }
        if (objActivity.ActivityDate <= DateTime.Now)
        {
            //SendEmail(10);
        }
        //SendScheduledEmail();

        ClearData(status);
        if (flg == 0 && status.Split(',')[0].ToString().Trim() == "1")
        {
            try
            {
                // Response.Redirect("~/WorkFlow/RequestFlow.aspx?selectedTab=tab-activity_" + hdnPrimeId.Value + "&RequestId=" + ViewState["RequestId"]);
                ViewState["ParentRequestID"] = ViewState["RequestId"].ToString() == hdnRFParentRequestID.Value ? "" : "&ParentRequestID=" + hdnRFParentRequestID.Value;
                Response.Redirect("~/WorkFlow/RequestFlow.aspx?selectedTab=tab-activity_" + hdnPrimeId.Value + "&RequestId=" + ViewState["RequestId"] + ViewState["ParentRequestID"]);
            }
            catch (Exception ex)
            {


            }
        }
    }
    #endregion

    protected string UploadFile(string flag = "")
    {
        string filename = "";
        string NewFileName = "";
        string FinalFileName = "";
        string ext = "";
        string subPath = "";
        string GUID = Guid.NewGuid().ToString();
        string isvalid = "";
        if (FUContractTemplate.HasFile)
        {
            subPath = @"~/Uploads/" + Session["TenantDIR"] + "/ActivitiesDocuments/ScannedActivitiesDocuments";
            bool isExists = System.IO.Directory.Exists(Server.MapPath(subPath));
            if (!isExists)
            {
                System.IO.Directory.CreateDirectory(Server.MapPath(subPath));
            }

            filename = System.IO.Path.GetFileName(FUContractTemplate.PostedFile.FileName);
            ext = Path.GetExtension(filename);

            if (ext == ".pdf" && chkIsOCR.Checked)
            {
                subPath = @"~/Uploads/" + Session["TenantDIR"] + "/ActivitiesDocuments/ScannedActivitiesDocuments";
            }
            else
            {
                subPath = @"~/Uploads/" + Session["TenantDIR"] + "/ActivitiesDocuments";
            }

            if (filename.Contains('.'))
            {
                NewFileName = filename.Substring(0, filename.LastIndexOf('.'));
            }

            //  string[] AllowedExtensions = { "jpg", "gif", "png", "bmp", "tiff", "jpeg", "TIF", "blob", "xls", "xlsx", "doc", "docx", "pdf", "swf", "ppt", "pptx", "txt", "csv", "odt", "odp", "odg", "ods", "mp3", "mp4", "wmv", "msg", "zip", "avi" };

            if (ext == ".jpg" || ext == ".gif" || ext == ".png" || ext == ".bmp" || ext == ".tiff" || ext == ".jpeg" || ext == ".TIF" || ext == ".blob" || ext == ".xls" || ext == ".xlsx" || ext == ".doc" || ext == ".docx" || ext == ".pdf" || ext == ".swf" || ext == ".ppt" || ext == ".pptx" || ext == ".txt" || ext == ".csv" || ext == ".odt" || ext == ".odp" || ext == ".odg" || ext == ".ods" || ext == ".mp3" || ext == ".mp4" || ext == ".wmv" || ext == ".msg" || ext == ".zip" || ext == ".avi")
            {
                FUContractTemplate.PostedFile.SaveAs(Server.MapPath(subPath + "/" + NewFileName.Replace(" ", "") + "_" + GUID.ToString() + ext));

                isvalid = "true";
                string strUrl = "";
                if (ext == ".pdf" && chkIsOCR.Checked)
                {
                    AsyncIndexer.GetInstance().ConvertOCRFile(NewFileName.Replace(" ", "") + "_" + GUID.ToString(), ext, "activity");
                }

                strUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + HttpContext.Current.Request.Url.Segments[1] + "Uploads/" + HttpContext.Current.Request.Url.Segments[1].Replace("/", "") + "/ActivitiesDocuments/" + NewFileName.Replace(" ", "") + "_" + GUID.ToString() + ext;
                if (ext == ".xls" || ext == ".xlsx" || ext == ".doc" || ext == ".docx" || ext == ".pdf" || ext == ".ppt" || ext == ".pptx" || ext == ".txt" || ext == ".csv" || ext == ".odt")
                {
                    AsyncIndexer.GetInstance().QueueForIndexing(strUrl, 1, ViewState["ContractInfo"].ToString());
                }
            }
        }

        if (flag.ToLower() == "update")
        {
            /*if (hdnActivityFileLocation.Value != "" && !FUContractTemplate.HasFile)
            {
                if (chkIsOCR.Checked == false)
                {
                    DeleteFileFromPhysicalLocation.DeleteFile(hdnActivityFileLocation.Value.Replace("ActivitiesDocuments", "ActivitiesDocuments/ScannedActivitiesDocuments"));
                }
                else if (chkIsOCR.Checked == true && hdnisocr.Value == "N")
                {
                    if (hdnActivityFileLocation.Value.Substring(hdnActivityFileLocation.Value.LastIndexOf(".")) == ".pdf" && chkIsOCR.Checked)
                    {
                        string fileName = hdnActivityFileLocation.Value.Substring(hdnActivityFileLocation.Value.LastIndexOf("/") + 1);
                        AsyncIndexer.GetInstance().ConvertOCRFile(fileName.Substring(0, fileName.LastIndexOf(".")), hdnActivityFileLocation.Value.Substring(hdnActivityFileLocation.Value.LastIndexOf(".")), "activity");
                    }
                }
            } else*/

            if (hdnActivityFileLocation.Value != "" && FUContractTemplate.HasFile)
            {
                DeleteFileFromPhysicalLocation.DeleteFile(hdnActivityFileLocation.Value);
                DeleteFileFromPhysicalLocation.DeleteFile(hdnActivityFileLocation.Value.Replace("ActivitiesDocuments", "ActivitiesDocuments/ScannedActivitiesDocuments"));
            }
        }

        if (FUContractTemplate.HasFile && isvalid == "true")
        {
            return NewFileName.Replace(" ", "") + "_" + GUID.ToString() + ext + "#" + NewFileName.Replace(" ", "") + ext + "#" + (FUContractTemplate.PostedFile.ContentLength / 1024).ToString();
        }
        else
        {
            return "";
        }
    }

    void ClearData(string status)
    {
        if (status == "1")
        {
            Page.TraceWrite("clearData starts.");
            hdnPrimeId.Value = "0";
            ddlstatus.SelectedIndex = 0;
            ddlactivitytype.SelectedIndex = 0;
            txtreminderdate.Value = "";
            txtactivitydate.Value = "";
            txtremarks.Value = "";
            rdYes.Checked = true;
            Page.TraceWrite("clear Data ends.");

        }

    }

    //Access Control
    #region
    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }

    void AccessVisibility()
    {
        setAccessValues();

        Page.TraceWrite("AccessVisibility starts.");
        if (Add == "N" && (hdnPrimeId.Value == "" || hdnPrimeId.Value == "0" || hdnPrimeId.Value == null))
        {
            hdnAdd.Value = "N";
            lnkActivityAdd.Enabled = false;
            lnkActivityAdd.Visible = false;
        }
        else
        {
            hdnAdd.Value = "Y";
            lnkActivityAdd.Enabled = true;
            lnkActivityAdd.Visible = true;
        }

        if (Update == "N")
            hdnUpdate.Value = "N";
        else
            hdnUpdate.Value = "Y";


        if (View == "N")
        {
            //Page.extDisableControls();
        }

        Page.TraceWrite("AccessVisibility starts.");
    }
    #endregion

    protected void rptChild_ItemDataBound(object Sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            LinkButton lnkEdit = (LinkButton)e.Item.FindControl("lblActivityText");
            LinkButton lnkDeleteActivity = (LinkButton)e.Item.FindControl("lnkDeleteActivity");

            if (Delete == "N")
            {
                lnkDeleteActivity.Visible = false;
            }
            else
            {
                lnkDeleteActivity.Visible = true;
            }

            if (Update == "N")
            {
                lnkEdit.Enabled = false;
            }
            else
            {
                lnkEdit.Enabled = true;
            }
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Response.Redirect("~/WorkFlow/ContractRequestData.aspx");
        if (hdnLinkStatus.Value == "Volts")
        {
            Server.Transfer("~/Workflow/VaultData.aspx");
        }
        else
        {
            Response.Redirect("~/WorkFlow/ContractRequestData.aspx");
        }
    }

    void SendEmail(int EmailTriggerId)
    {
        try
        {
            //Page.TraceWrite("send emails.");
            EmailTemplateBLL.IEmailTrigger objEmail = EmailTemplateBLL.FactoryEmailTemplate.GetEmailTriggerDetail();
            objEmail.RequestId = Convert.ToInt32(ViewState["RequestId"]);
            objEmail.ContractTypeId = 99999;
            objEmail.ContractTemplateId = 99999;
            objEmail.EmailTriggerId = EmailTriggerId; // hardcoded for 
            objEmail.userId = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            objEmail.AssignedToUserId = objActivity.AssignToId;// int.Parse(ddlAssignToUser.Value); 
            objEmail.ActivityId = Convert.ToInt32(ViewState["ActivityId"]);
            objEmail.AlertDate = Convert.ToDateTime(txtactivitydate.Value.Trim());
            objEmail.BulkEmail();
        }
        catch (Exception)
        {
            //Page.TraceWarn("send emails fail.");
        }
    }

    void SendScheduledEmail() //Task Deadline
    {
        try
        {
            Page.TraceWrite("send scheduled emails.");
            EmailTemplateBLL.IEmailTrigger objEmail = EmailTemplateBLL.FactoryEmailTemplate.GetEmailTriggerDetail();
            objEmail.ActivityId = Convert.ToInt32(ViewState["ActivityId"]);
            objEmail.AlertDate = Convert.ToDateTime(txtactivitydate.Value.Trim());
            objEmail.EmailTriggerId = 4;
            if (Session[Declarations.User] != null)
            {
                objEmail.userId = int.Parse(Session[Declarations.User].ToString());
                objEmail.BulkEmailScheduler();
            }
        }
        catch (Exception)
        {
            Page.TraceWarn("send emails fail.");
        }
    }
}