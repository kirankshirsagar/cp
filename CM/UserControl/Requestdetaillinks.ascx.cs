﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UserManagementBLL;
using CommonBLL;

using Keyoti.SearchEngine;
using Keyoti.SearchEngine.Index;
using Keyoti.SearchEngine.DataAccess.IndexableSourceRecords;
using Keyoti.SearchEngine.Search;

public partial class UserControl_Requestdetaillinks : System.Web.UI.UserControl
{
    IAccessLink obj = FactoryUser.GetAccessDetail();
    protected void Page_Load(object sender, EventArgs e)
    {

        /*Keyoti Changes Starts*/
        //string tenant = "CM";
        //string PhysPath = Server.MapPath("~");
        //PhysPath = PhysPath + @"\Uploads" + @"\" + tenant;
        //string indexDirPath = PhysPath + @"\IndexDirectory";

        //Configuration configuration = new Configuration();
        //configuration.IndexDirectory = indexDirPath;
        //SearchBox1.AutoCompleteQueryIndexDirectory = indexDirPath;
        /*Keyoti Changes End*/


        if (!IsPostBack)
        {
            setAccessTabs();
            if (Session[Declarations.User] != null)
            {
                Access.PageAccess("../ContractTemplateGeneratorSetup/", Session[Declarations.User].ToString());
                hdnDownloadSetupLink.Value = Access.View;

                obj.UserId = int.Parse(Session[Declarations.User].ToString());
                obj.Flag = "Reports";
                ddlRlist.extDataBind(obj.SelectData(), "");
            }


        }
    }

    protected void lnkGlobalSearch_Click(object sender, EventArgs e)
    {
        Response.Redirect("../Workflow/GlobalSearch.aspx?GlobalSearch=" + txtGlobalSearch.Value);

    }

    void setAccessTabs()
    {
        IAccessHome obj = FactoryUser.GetAccessHomeDetail();
        obj.UserId = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;

        if (obj.UserId > 0)
        {
            List<AccessHome> lst = obj.ReadData();
            hdnMyGlobalVaultTable.Value = lst[0].AutoRenewal; // flip
            hdnMyGlobalCalenderTable.Value = lst[0].Calender;
            hdnMyGlobalClientsTable.Value = lst[0].MyClients;
            hdnMyGlobalContractsTable.Value = lst[0].MyContracts;
            hdnMyGlobalCreateNewContractTable.Value = lst[0].CreateNewContract;
            hdnMyGlobalCreateNewReviewRequestTable.Value = lst[0].NewReviewRequest;
            hdnMyGlobalDashboardTable.Value = lst[0].MyDashBoard;
            hdnMyGlobalRenewContractTable.Value = lst[0].RenewContract;
            hdnMyGlobalReportsTable.Value = lst[0].MyReports;
            hdnMyGlobalReviseContractTable.Value = lst[0].ReviseContract;
            hdnMyGlobalTerminateContractTable.Value = lst[0].TerminateContract;
            hdnMyGlobalAddContractTable.Value = lst[0].MyVault; // flip
            hdnMyGlobalCalenderTable.Value = lst[0].Calender;
            hdnCustomGlobalReportsTable.Value = lst[0].CustomReport;
            hdnAdvanceSearchTable.Value = lst[0].AdvanceSearch;
            hdnAuditTrail.Value = lst[0].AuditTrail;

            if (lst.Count == 2)
            {
                if (lst[1].ParentId == 5)
                    hdnMyGlobalVaultHref.Value = "../ClauseLiabrary/" + lst[1].PageLink;
                else if (lst[1].ParentId == 2)
                    hdnMyGlobalVaultHref.Value = "../Masters/" + lst[1].PageLink;
                else if (lst[1].ParentId == 3)
                    hdnMyGlobalVaultHref.Value = "../UserManagement/" + lst[1].PageLink;
                else if (lst[1].ParentId == 4)
                    hdnMyGlobalVaultHref.Value = lst[1].PageLink;
            }
            else
            {
                hdnMyGlobalVaultHref.Value = "../Masters/ContractTypeMasterData.aspx";
            }
        }
    }

    #region Added by Dilip
    protected void btnReportActionList_Click(object sender, EventArgs e)
    {
        string Reportlink = string.Empty;
        string Path = string.Empty;
        try
        {
            foreach (ListItem item in ddlRlist.Items)
            {
                Reportlink = ddlRlist.Items[1].Value;
                break;
            }
            Path = "../Reports/" + Reportlink;
        }
        catch { Path = "../WorkFlow/Home.aspx"; }

        Server.Transfer(Path);
    }

    #endregion


}