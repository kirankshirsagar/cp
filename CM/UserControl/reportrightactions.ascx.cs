﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;
using System.Data;
using ReportBLL;
using System.IO;
using ImportExcel;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;//This name space present in "itextsharp-dll-core"
using UserManagementBLL;
using System.Web.UI.HtmlControls;





public partial class UserControl_reportrightactions : System.Web.UI.UserControl
{

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IAccessLink obj = FactoryUser.GetAccessDetail();
            if (Session[Declarations.User] != null && Session[Declarations.User] != string.Empty)
            {
                obj.UserId = int.Parse(Session[Declarations.User].ToString());
            }
            obj.Flag = "Reports";
            ddlReport.extDataBind(obj.SelectData(),"");
            
        }

        ddlReport_SelectedIndexChanged(this.ddlReport, EventArgs.Empty);

    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //string acessId = Convert.ToString(Session["ReportAccessId"]);
            //if (acessId != "35" && acessId != "76" && acessId != "75" && acessId != "64" && acessId != "70" && acessId != "47" && acessId != "39" && acessId != "37"
            //     && acessId != "41" && acessId != "43" && acessId != "45" && acessId != "77")
            //{
            //    lnkPredefineSheduleReport.Visible = false;
            //}

            //ICustomReportsNew objReport = FactoryReports.CustomReportDetails();
            //objReport.PageNo = 1;
            //objReport.RecordsPerPage = 100;
            //objReport.CustomReportId = Convert.ToInt32(Session["ReportAccessId"]);

            //DataTable dtSchedule = objReport.GetPredefineScheduleRecord();
            //if (dtSchedule.Rows.Count > 0)
            //{
            //    lnkPredefineSheduleReport.Text = "Scheduled report";
            //}

        }
    }

    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        if (hdnGlobalReportFlag.Value != "")
        {

            DataTable dt = FactoryExport.ExportData(int.Parse(hdnGlobalReportFlag.Value));
            if (dt.Rows.Count > 0)
            {
                string fileName = FactoryExport.ReportName.Replace(" ", "-").Replace(",", "-").extTimeStamp() + ".xls";
                gvDetails.Caption = "<div align=center><font size=4 ><b>" + FactoryExport.ReportName + "</b></font></div>";
                gvDetails.DataSource = dt.extReplaceExcelQuotes();
                gvDetails.DataBind();
                ExportToExcell(fileName);
            }
        }
    }

    protected void btnExportToPDF_Click(object sender, EventArgs e)
    {
        if (hdnGlobalReportFlag.Value != "")
        {
            DataTable dt = FactoryExport.ExportData(int.Parse(hdnGlobalReportFlag.Value));
            if (dt.Rows.Count > 0)
            {
                string fileName = FactoryExport.ReportName.Replace(" ", "-").Replace(",", "-").extTimeStamp() + ".pdf";
                gvDetails.Caption = "<div align=center><font size=4 ><b>" + fileName + "</b></font></div>";
                gvDetails.DataSource = dt.extReplaceQuotes();
                gvDetails.DataBind();
                ExportToPdf(fileName, FactoryExport.ReportName);
            }
        }
    }

    void ExportToPdf(string pdfFileName, string reportTitle)
    {
        Response.ContentType = "application/pdf";
        string strAttachmnet = "attachment;filename=" + pdfFileName;
        Response.AddHeader("content-disposition", strAttachmnet);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter s_w = new StringWriter();
        HtmlTextWriter h_w = new HtmlTextWriter(s_w);
        HtmlForm frm = new HtmlForm();


        gvDetails.AllowPaging = false;
        this.Controls.Add(frm);
        frm.Controls.Add(gvDetails);
        frm.RenderControl(h_w);
        //gvDetails.RenderControl(h_w);

        string strOldString = "scope=\"col\"";
        string strReplace = strOldString + " style=\"font-weight:bold; font-size:12px\" ";
        string strOut = s_w.ToString().Replace(strOldString, strReplace);
        strOut = "<div align=center><font size=4 ><b>" + reportTitle + "</b></font></div><br>" + strOut;

        StringReader sr = new StringReader(strOut);
        Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        pdfDoc.Open();
        htmlparser.Parse(sr);
        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
    }

    void ExportToExcell(string excelFileName)
    {
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", excelFileName));
        Response.ContentType = "application/ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        HtmlForm frm = new HtmlForm();
        gvDetails.AllowPaging = false;
        //Change the Header Row back to white color
        gvDetails.HeaderRow.Style.Add("background-color", "#E1E1E1");
        //Applying stlye to gridview header cells
        for (int i = 0; i < gvDetails.HeaderRow.Cells.Count; i++)
        {
            gvDetails.HeaderRow.Cells[i].Style.Add("background-color", "#E1E1E1");
            gvDetails.HeaderRow.Cells[i].Style.Add("font-weight", "bold");

        }

        this.Controls.Add(frm);
        frm.Controls.Add(gvDetails);
        frm.RenderControl(htw);

        int j = 1;
        //Set alternate row color
        foreach (GridViewRow gvrow in gvDetails.Rows)
        {
            gvrow.BackColor = System.Drawing.Color.White;
            if (j <= gvDetails.Rows.Count)
            {
                if (j % 2 != 0)
                {
                    for (int k = 0; k < gvrow.Cells.Count; k++)
                    {
                        gvrow.Cells[k].Style.Add("background-color", "#EFF3FB");
                    }
                }
            }
            j++;
        }
        //gvDetails.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    protected void ddlReport_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (hdnGlobalReportLink.Value != "")
        {
            //if (Convert.ToString(hdnGlobalReportLink.Value) == "CustomReportsNew.aspx")
            if (Convert.ToString(hdnGlobalReportLink.Value).Contains("CustomReportsNew.aspx") )//== "CustomReportsNew.aspx")
            {
                string CustomReportId = "0";
                string AccessId = "0";
                ICustomReportsNew objReport = FactoryReports.CustomReportDetails();
                objReport.CustomReportName = Convert.ToString(hdnReportName.Value).Trim();
                DataTable dt = objReport.GetCustomFieldDetails();
                if (dt.Rows.Count > 0)
                {
                    CustomReportId = Convert.ToString(dt.Rows[0]["Id"]);
                    AccessId = Convert.ToString(dt.Rows[1]["Id"]);
                }

                //Server.Transfer(hdnGlobalReportLink.Value + "?ReportId=" + CustomReportId + "&AccessId=" + AccessId + "&Name=" + Convert.ToString(hdnReportName.Value).Trim());
                Server.Transfer("CustomReportsNew.aspx?ReportId=" + CustomReportId + "&AccessId=" + AccessId + "&Name=" + Convert.ToString(hdnReportName.Value).Trim());

            }
            else
            {
                Server.Transfer(hdnGlobalReportLink.Value);
            }
        }
    }

    protected override void Render(HtmlTextWriter writer)
    {
        // Ensure that the control is nested in a server form.
        if (Page != null)
        {
            Page.VerifyRenderingInServerForm(this);
        }
        base.Render(writer);
    }

    public void LoadddlReport()
    {
        IAccessLink obj = FactoryUser.GetAccessDetail();
        if (Session[Declarations.User] != null && Session[Declarations.User] != string.Empty)
        {
            obj.UserId = int.Parse(Session[Declarations.User].ToString());
        }
        obj.Flag = "Reports";
        ddlReport.extDataBind(obj.SelectData(), "");
    }

    protected void lnkPredefineSheduleReport_Click(object sender, EventArgs e)
    {
        Server.Transfer("PredefinedScheduleReport.aspx");
    }

    /*
    private void GetDocument(string filename, string filePath, DataTable dt)
    {
        try
        {
            FileStream stream = CreateExcel.Generator.CreateExcelFile(filePath, dt, false);
            stream.Flush();
            stream.Position = 0;
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.AddHeader("content-disposition", "attachment; filename=" + filename);
            byte[] data1 = new byte[stream.Length];
            stream.Read(data1, 0, data1.Length);
            stream.Close();
            Response.BinaryWrite(data1);
            Response.Flush();
            Response.End();

        }
        catch (Exception ex)
        {


        }

    }
    */





}