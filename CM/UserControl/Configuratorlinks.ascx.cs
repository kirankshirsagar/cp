﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UserManagementBLL;
using CommonBLL;

public partial class UserControl_Configuratorlinks : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IAccessLink obj = FactoryUser.GetAccessDetail();
            if (Session[Declarations.User] != null && Session[Declarations.User] != string.Empty)
            {
                obj.UserId = int.Parse(Session[Declarations.User].ToString());
            }

            obj.Flag = "Configurators";
            rptLinks.DataSource = obj.ReadData();
            rptLinks.DataBind();

            //obj.Flag = "BulkImport";
            //rptBulkImport.DataSource = obj.ReadData();
            //rptBulkImport.DataBind();

        }
    }
}