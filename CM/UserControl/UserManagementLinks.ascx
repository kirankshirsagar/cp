﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserManagementLinks.ascx.cs" Inherits="UserControl_UserManagementLinks" %>
<h3>User Management</h3>


<p style="line-height: 20px;">
    <asp:Repeater ID="rptLinks" runat="server">
        <HeaderTemplate>
            <table width="100%">
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <a href='<%#Eval("PageLink") %>'>
                        <%#Eval("PageName") %>
                    </a>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody> </table>
        </FooterTemplate>
    </asp:Repeater>
</p>