﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Requestdetaillinks.ascx.cs"
    Inherits="UserControl_Requestdetaillinks" %>

<script type="text/javascript">
    function myFunction() {

    }
    function myMoveFunction(obj) {
        $(obj).css('background', '#777675');
    }
    function mouseoutFunction(obj) {
        $(obj).css('background', '#2d9da1');
    }
    function mouseoutFunction1(obj) {
        $(obj).css('background', '#0b355c');
    }
</script>
<style type="text/css">
    .Homeovers
    {
        background: #2d9da1;
        padding: 10px 35px 22px;
        text-align: center;
        border: none;
        cursor: pointer;
    }
    .Searchovers
    {
        background: #0b355c;
        padding: 10px 35px 22px;
        text-align: center;
        border: none;
        cursor: pointer;
    }
</style>
<div class="fl left-pannel-container" style="padding-left: 1px;">
    <div id="divDbSearch">
        <div class="fl container-100" style="padding-bottom: 11px;">
            <input id="txtGlobalSearch" runat="server" placeholder="Enter text here" clientidmode="Static"
                maxlength="100" style="width: 100%; height: 25px;" type="text" />
        </div>
        <div class="fl container-100">
            <div class="fl" style="width: 50%">
                <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/WorkFlow/Home.aspx"
                    onmouseover="myMoveFunction(this)" onmouseout="mouseoutFunction(this)" class="Homeovers">
             <span class="icon-home"></span>
                </asp:LinkButton>
            </div>
            <div class="fr" style="width: 50%">              
                <asp:LinkButton ID="lnkGlobalSearch" runat="server" OnClientClick=" return checkGlobalSearch();"
                    onmouseover="myMoveFunction(this)" onmouseout="mouseoutFunction1(this)" class="Searchovers"
                    OnClick="lnkGlobalSearch_Click">
             <span class="search-icon"></span>
                </asp:LinkButton>
            </div>
        </div>
        <div class="clb">
        </div>
        <br />
    </div>   
    <p style="padding-bottom: 10px;">
    </p>
    <div class="fl container-100 top-5">
        <div id="divDownLoadSetup">
            <a href="../ContractTemplateGeneratorSetup/" class="DownloadTemplateGenerator"><span
                id="Span1" runat="server" clientidmode="static" class="fl metro-btn2 metro-btn-bg-1">
                <div class="fl metro-btn2-icon">
                    <img src="../images/metro-btn-icon2-1.png" /></div>
                <div class="fl metro-btn2-icon-title">
                    Download Setup</div>
            </span></a>
        </div>
         <div id="divArtificialIntelligence">
            <a href="../ArtificialIntelligence/AIAnalysis.aspx"><span class="fl metro-btn2 metro-btn-bg-2">
                <div class="fl metro-btn2-icon">
                    <img src="../images/metro-btn-icon2-13.png" /></div>
                <div class="fl metro-btn2-icon-title">
                    Artificial Intelligence </div>
            </span></a>
        </div>
        <div id="divDiscoverySearch" >
            <a href="../ArtificialIntelligence/DiscoverySearch.aspx"><span class="fl metro-btn2 metro-btn-bg-3">
                <div class="fl metro-btn2-icon">
                    <img src="../images/metro-btn-icon2-12.png" /></div>
                <div class="fl metro-btn2-icon-title">
                    Discovery Search</div>
            </span></a>
        </div>
        <div id="divDocSearch" style="display: none;">
            <a href="../Workflow/searchresults.aspx"><span id="Span2" runat="server" clientidmode="static"
                class="fl metro-btn2 metro-btn-bg-5">
                <div class="fl metro-btn2-icon">
                    <img src="../images/metro-btn-icon2-12.png" /></div>
                <div class="fl metro-btn2-icon-title">
                    Advanced Search</div>
            </span></a>
        </div>
        <div class="clb">
        </div>
        <div id="divDashboard" style="display: none">
            <a href="../Dashboard/Dasboard.aspx"><span runat="server" clientidmode="static" class="fl metro-btn2 metro-btn-bg-1">
                <div class="fl metro-btn2-icon">
                    <img src="../images/metro-btn-icon2-1.png" /></div>
                <div class="fl metro-btn2-icon-title">
                    My Dashboard</div>
            </span></a>
        </div>
        <div class="clb">
        </div>
        <div id="divMyCustomersSuppliers" style="display: none">
            <a href="../Workflow/AvailableClientsData.aspx"><span class="fl metro-btn2 metro-btn-bg-2">
                <div class="fl metro-btn2-icon">
                    <img src="../images/metro-btn-icon2-2.png" /></div>
                <div class="fl metro-btn2-icon-title">
                    My Customers, Suppliers & Others</div>
            </span></a>
        </div>
        <div class="clb">
        </div>
        <div id="divMyReport" style="display: none">
            <a onclick="GoToReport();"><span class="fl metro-btn2 metro-btn-bg-3">
                <div class="fl metro-btn2-icon">
                    <img src="../images/metro-btn-icon2-3.png" /></div>
                <div class="fl metro-btn2-icon-title">
                    My Reports</div>
            </span></a>
        </div>
        <div class="clb">
        </div>
        <div id="divCustomReport" style="display: none">
            <a href="../Reports/CustomReports.aspx"><span class="fl metro-btn2 metro-btn-bg-4">
                <div class="fl metro-btn2-icon">
                    <img src="../images/metro-btn-icon2-3.png" /></div>
                <div class="fl metro-btn2-icon-title">
                    Custom Reports</div>
            </span></a>
        </div>
        <div class="clb">
        </div>
        <div id="divMyVault" style="display: none">
            <%--<a href="../Masters/ContractTypeMasterData.aspx" id="aMyvault" runat="server">--%>
           <%-- <a id="aMyvault" onclick="callMyValut();"><span class="fl metro-btn2 metro-btn-bg-5">--%>
            <a id="aMyvault" ><span class="fl metro-btn2 metro-btn-bg-5">
                <div class="fl metro-btn2-icon">
                    <img src="../images/metro-btn-icon2-4.png" /></div>
                <div class="fl metro-btn2-icon-title">
                    My Vault</div>
            </span></a>
        </div>
        <div class="clb">
        </div>
        <div id="divMyCalander" style="display: none">
            <a href="../Calendar/Calendar.aspx"><span class="fl metro-btn2 metro-btn-bg-7">
                <div class="fl metro-btn2-icon">
                    <img src="../images/metro-btn-icon2-5.png" /></div>
                <div class="fl metro-btn2-icon-title">
                    My Calendar</div>
            </span></a>
        </div>
        <div class="clb">
        </div>
        <div id="divContract" style="display: none">
            <a href="../Workflow/ContractRequestData.aspx"><span class="fl metro-btn2 metro-btn-bg-1">
                <div class="fl metro-btn2-icon">
                    <img src="../images/metro-btn-icon2-6.png" /></div>
                <div class="fl metro-btn2-icon-title">
                    My Contracts</div>
            </span></a>
        </div>
        <div class="clb">
        </div>
        <div id="divRenewContract" style="display: none">
            <a href="../Workflow/ContractRequest.aspx?ch=ren"><span class="fl metro-btn2 metro-btn-bg-2">
                <div class="fl metro-btn2-icon">
                    <img src="../images/metro-btn-icon2-7.png" /></div>
                <div class="fl metro-btn2-icon-title">
                    Renew Contract</div>
            </span></a>
        </div>
        <div class="clb">
        </div>
        <div id="divReviseContract" style="display: none">
            <a href="../Workflow/ContractRequest.aspx?ch=rev"><span class="fl metro-btn2 metro-btn-bg-3">
                <div class="fl metro-btn2-icon">
                    <img src="../images/metro-btn-icon2-8.png" /></div>
                <div class="fl metro-btn2-icon-title">
                    Revise Contract</div>
            </span></a>
        </div>
        <div class="clb">
        </div>
        <div id="divTerminateContract" style="display: none">
            <a href="../Workflow/ContractRequest.aspx?ch=tc"><span class="fl metro-btn2 metro-btn-bg-4">
                <div class="fl metro-btn2-icon">
                    <img src="../images/metro-btn-icon2-9.png" /></div>
                <div class="fl metro-btn2-icon-title">
                    Terminate Contract</div>
            </span></a>
        </div>
        <div class="clb">
        </div>
        <div id="divGenerateContracts" style="display: none">
            <a href="../Workflow/ContractRequest.aspx?ch=cnc"><span class="fl metro-btn2 metro-btn-bg-5">
                <div class="fl metro-btn2-icon">
                    <img src="../images/metro-btn-icon2-10.png" /></div>
                <div class="fl metro-btn2-icon-title">
                    Generate Contract</div>
            </span></a>
        </div>
        <div class="clb">
        </div>
        <%-- ../Workflow/VaultData.aspx--%>
        <div id="divAddContractRecord" style="display: none">
            <a href="../BulkImport/BulkContractTemplateData.aspx"><span class="fl metro-btn2 metro-btn-bg-7">
                <div class="fl metro-btn2-icon">
                    <img src="../images/metro-btn-icon2-11.png" /></div>
                <div class="fl metro-btn2-icon-title">
                    Add Contract Record</div>
            </span></a>
        </div>
        <div class="clb">
        </div>
        <div id="divContractReviewRequest" style="display: none">
            <a href="../Workflow/ContractRequest.aspx?ch=nrr"><span class="fl metro-btn2 metro-btn-bg-1">
                <div class="fl metro-btn2-icon">
                    <img src="../images/metro-btn-icon2-12.png" /></div>
                <div class="fl metro-btn2-icon-title">
                    Contract Review Request</div>
            </span></a>
        </div>
        <div class="clb">
            <div id="divAuditTrailLog" style="display: none">
                <a href="../AuditTrail/AuditTrailDetails.aspx"><span class="fl metro-btn2 metro-btn-bg-2">
                    <div class="fl metro-btn2-icon">
                        <img src="../images/metro-btn-icon2-3.png" /></div>
                    <div class="fl metro-btn2-icon-title">
                        Audit Trail Log</div>
                </span></a>
            </div>
        </div>
        <div class="clb">
        </div>
        <div id="divSetting" style="display: none">
            <a href="../UserManagement/RegisterData.aspx"><span class="fl metro-btn2 metro-btn-bg-2">
                <div class="fl metro-btn2-icon">
                    <img src="../images/metro-btn-icon2-13.png" /></div>
                <div class="fl metro-btn2-icon-title">
                    Setting</div>
            </span></a>
        </div>
        <div class="clb">
        </div>
       
    </div>
    <div class="clb">
    </div>
</div>
<h3>
    <input id="hdnAdvanceSearchTable" type="hidden" clientidmode="Static" runat="server" />
    <input id="hdnCustomGlobalReportsTable" type="hidden" clientidmode="Static" runat="server" />
    <input id="hdnMyGlobalDashboardTable" type="hidden" clientidmode="Static" runat="server" />
    <input id="hdnMyGlobalContractsTable" type="hidden" clientidmode="Static" runat="server" />
    <input id="hdnMyGlobalReportsTable" type="hidden" clientidmode="Static" runat="server" />
    <input id="hdnMyGlobalCreateNewContractTable" type="hidden" clientidmode="Static"
        runat="server" />
    <input id="hdnMyGlobalCreateNewReviewRequestTable" type="hidden" clientidmode="Static"
        runat="server" />
    <input id="hdnMyGlobalClientsTable" type="hidden" clientidmode="Static" runat="server" />
    <input id="hdnMyGlobalRenewContractTable" type="hidden" clientidmode="Static" runat="server" />
    <input id="hdnMyGlobalReviseContractTable" type="hidden" clientidmode="Static" runat="server" />
    <input id="hdnMyGlobalVaultTable" type="hidden" clientidmode="Static" runat="server" />
    <input id="hdnMyGlobalTerminateContractTable" type="hidden" clientidmode="Static"
        runat="server" />
    <input id="hdnMyGlobalCalenderTable" type="hidden" clientidmode="Static" runat="server" />
    <input id="hdnMyGlobalSettingTable" type="hidden" clientidmode="Static" runat="server" />
    <input id="hdnMyGlobalAddContractTable" type="hidden" clientidmode="Static" runat="server" />
    <input id="hdnDownloadSetupLink" type="hidden" clientidmode="Static" runat="server" />
    <input id="hdnMyGlobalVaultHref" type="hidden" clientidmode="Static" runat="server" />
    <input id="hdnAuditTrail" type="hidden" clientidmode="Static" runat="server" />
</h3>
<div style="display: none">
    <asp:DropDownList ID="ddlRlist" ClientIDMode="Static" Width="100px" runat="server">
    </asp:DropDownList>
    <asp:Button ID="btnReportActionList" runat="server" ClientIDMode="Static" OnClick="btnReportActionList_Click" />
</div>
<script type="text/javascript" language="javascript">
    function GoToReport() {
        $("#btnReportActionList").click();
    }
</script>

<script type="text/javascript">
    function checkGlobalSearch() {
        var search = $('#txtGlobalSearch').val();
        if ((search).length < 3) {
            alert("Please enter minimum three characters for search");
            return false;
        }
        else {
            return true;
        }
    }

    //    $('#rdSearchopt').change(function () {
    //        if ($("#rdSearchopt input:checked").val() == '1') {
    //            $("#divDbSearch").css('display', 'block');
    //            $("#divKeyotiSeatch").css('display', 'none');
    //        }
    //        else {
    //            $("#divDbSearch").css('display', 'none');
    //            $("#divKeyotiSeatch").css('display', 'block');
    //        }
    //    });


    $(window).load(function () {
        if ($('#hdnMyGlobalDashboardTable').val() == 'Y') {
            $('#divDashboard').show();
        }
        if ($('#hdnAdvanceSearchTable').val() == 'Y') {
            $('#divDocSearch').show();
        }

        if ($('#hdnMyGlobalContractsTable').val() == 'Y') {
            $('#divContract').show();
        }
        else {
            $('#txtGlobalSearch').attr('disabled', 'disabled');
            $('#lnkGlobalSearch').removeAttr('OnClientClick');
            $('#MainContent_rightactions_lnkGlobalSearch').removeAttr('onclick');
            $('#MainContent_rightactions_lnkGlobalSearch').attr('href', '#');
            $('#MainContent_rightactions_lnkGlobalSearch').attr('disabled', 'disabled');
            $('#MainContent_RequestLinksID_lnkGlobalSearch').removeAttr('onclick');
            $('#MainContent_RequestLinksID_lnkGlobalSearch').attr('href', '#');
            $('#MainContent_RequestLinksID_lnkGlobalSearch').attr('disabled', 'disabled');
        }

        if ($('#hdnMyGlobalCreateNewContractTable').val() == 'Y') {
            $('#divGenerateContracts').show();
        }

        if ($('#hdnMyGlobalCreateNewReviewRequestTable').val() == 'Y') {
            $('#divContractReviewRequest').show();
        }

        if ($('#hdnMyGlobalClientsTable').val() == 'Y') {
            $('#divMyCustomersSuppliers').show();
        }

        if ($('#hdnMyGlobalRenewContractTable').val() == 'Y') {
            $('#divRenewContract').show();
        }

        if ($('#hdnMyGlobalReviseContractTable').val() == 'Y') {
            $('#divReviseContract').show();
        }

        if ($('#hdnMyGlobalAddContractTable').val() == 'Y') {
            $('#divAddContractRecord').show();
        }

        if ($('#hdnMyGlobalTerminateContractTable').val() == 'Y') {
            $('#divTerminateContract').show();
        }

        if ($('#hdnMyGlobalVaultTable').val() == 'Y') {
            $('#divMyVault').show();
            $('#aMyvault').attr('href', $('#hdnMyGlobalVaultHref').val());
        }

        if ($('#hdnMyGlobalCalenderTable').val() == 'Y') {
            $('#divMyCalander').show();
        }

        if ($('#hdnMyGlobalReportsTable').val() == 'Y') {
            $('#divMyReport').show();
        }

        if ($('#hdnMyGlobalSettingTable').val() == 'Y') {
            $('#divSetting').show();
        }

        if ($('#hdnAuditTrail').val() == 'Y') {
            $('#divAuditTrailLog').show();
        }

        if ($('#hdnCustomGlobalReportsTable').val() == 'Y') {
            $('#divCustomReport').show();
        }


        $('#divSetting').hide();
        HideSideMenus();
    });

    function HideSideMenus() {
        var pageName = location.href.split("/").slice(-1);

        if (pageName == 'Home.aspx' || pageName == 'Home.aspx#') {
            $('#divDashboard').hide();
            //$('#divDocSearch').hide();
            $('#divContract').hide();
            $('#divGenerateContracts').hide();
            $('#divContractReviewRequest').hide();
            $('#divMyCustomersSuppliers').hide();
            $('#divRenewContract').hide();
            $('#divReviseContract').hide();
            $('#divAddContractRecord').hide();
            $('#divTerminateContract').hide();
            $('#divMyVault').hide();
            $('#divMyCalander').hide();
            $('#divMyReport').hide();
            $('#divSetting').hide();
            $('#divCustomReport').hide();

            if ($("#hdnDownloadSetupLink").val() == "Y")
                $('#divDownLoadSetup').show();
            else
                $('#divDownLoadSetup').hide();

            if ($('#hdnAuditTrail').val() == 'Y')
                $('#divAuditTrailLog').show();
            else
                $('#divAuditTrailLog').hide();
        }
        else {
            $('#divDownLoadSetup').hide();

        }
    }

    $(window).load(function () {
        var linkToDownload = $(".DownloadTemplateGenerator").prop("href");
        if (linkToDownload.indexOf('.zip') == -1)
            $(".DownloadTemplateGenerator").prop("href", linkToDownload + location.pathname.split('/')[1] + ".zip");
    });

    HideSideMenus();

</script>
