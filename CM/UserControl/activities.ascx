﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="activities.ascx.cs" Inherits="UserControl_activities" %>
<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<script src="../JQueryValidations/mastervalidations.js" type="text/javascript"></script>
<script src="../JQueryValidations/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
<link href="../Styles/jquery-ui-1.8.21.css" media="all" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../scripts/jquery-ui-timepicker-addon.js"></script>
<link rel="stylesheet" href="../scripts/jquery-ui-timepicker-addon.css" type="text/css" />
<%--<link rel="stylesheet" href="../JQueryValidations/jquery.ui.slider.css" type="text/css" />
<script type="text/javascript" src="../JQueryValidations/jquery.ui.slider.js"></script>
<script type="text/javascript" src="../JQueryValidations/jquery.ui.slider.min.js"></script>--%>
<script src="../scripts/jquery.ui.core.min.js"></script>
<script src="../scripts/jquery.ui.widget.min.js"></script>
<script src="../scripts/jquery.ui.position.min.js"></script>
<script src="../scripts/jquery.ui.autocomplete.min.js"></script>
<link rel="stylesheet" href="../scripts/jquery-ui-1.8.16.custom.css" />
<link rel="stylesheet" href="../scripts/jquery-ui-timepicker-addon.css" type="text/css" />
<link rel="stylesheet" href="../JQueryValidations/jquery.ui.slider.css" type="text/css" />
<script type="text/javascript" src="../JQueryValidations/jquery.ui.slider.js"></script>
<script type="text/javascript" src="../JQueryValidations/jquery.ui.slider.min.js"></script>
<script type="text/javascript">
    function countChar(val) {
        var len = val.value.length;

        if (len >= 500) {
            val.value = val.value.substring(0, 500);
        } else {
            return false;
        }
    };
</script>
<script type="text/javascript">

    $(document).ready(function () {
        $("#actdiv").hide();
        $("#hdnRFParentRequestID").val($("#hdnParentRequestID").val());
    });

    function Cancel() {
        $("#actdiv").hide();
        $('#actdiv input:text, a.chzn-single, textarea').css('border-color', '');
        $('#actdiv div.tooltip_outer').hide();
        $('#hdnActivityFileLocation').val('');
        $("#hdnisocr").val('');
        $("#chkIsOCR").attr('checked', false);
    }

    function Displayactdiv() {
        if ($('#hdnUpdate').val() == "Y") {
            $("#actdiv").show();
            $("#actdiv div.chzn-container").css("width", "200px");
            $('a[class^="clsActivityText"]').attr('href', '#actdiv');
        }
    }

    function loadUserControl() {
        $.ajax({
            type: "POST",
            url: "../Workflow/LoadUserControl.asmx/RenderUC",
            data: "{path: '" + page + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function (jqXHR) {
                $.xhrPool.push(jqXHR);
                $(body).html('<div style="text-align: center;width:100%;">  <img src="../images/circular.gif" />  </div> ');
            },
            success: function (r) {
                $(body).html('<table width="100%"><tr style="height:25px;vertical-align: middle;"><td style="text-align:right">  <a id="hrefNewChildWindow" href="#"><img style="width:16px;height:16px;" src="../Styles/css/icons/16/application_double.png" title="New windnw" class="mws-tooltip-e" /></a> <a id="hrefChildControlPopup" href="#"><img style="width:16px;height:16px;" title="Detach" class="mws-tooltip-e" src="../Styles/css/icons/32/application_tile_vertical.png" /></a>  <a href="#" id="hrefChildRefresh"><img src="../Styles/css/icons/16/arrow_refresh.png" title="Refresh" class="mws-tooltip-e" /></a> </td> </tr> <tr> <td>' + r.d + '</td> </tr> </table>').attr('data', 'fill');                
                $.xhrPool.pop();
            },
            error: function () {
                $(body).html('<label class="error"><strong>Oops!</strong> Try that again in a few moments.</label>');
            }
        });
    }

    $(function () {
        $("#txtactivitydate").datetimepicker({
            showOn: "both",
            buttonImage: "../Images/calendar.png",
            buttonImageOnly: true,
            minDate: "-10M -10D -10Y",
            maxDate: "+10M +10D +10Y",
            dateFormat: "dd-M-yy",
            timeFormat: "hh:mm TT",
            //minuteGrid: 10,
            onClose: function (selectedDate) {
                var myDAte = $('#txtreminderdate').datetimepicker("getDate");
                var curDate = $('#txtactivitydate').datetimepicker("getDate");

                if (curDate < myDAte) {
                    alert('Activity date should be greater than reminder date');
                    $("#txtreminderdate").datetimepicker("setDate", curDate);
                }
            }
        });

        $("#txtreminderdate").datetimepicker({
            showOn: "both",
            buttonImage: "../Images/calendar.png",
            buttonImageOnly: true,
            minDate: "-10M -10D -10Y",
            maxDate: "+10M +10D +10Y",
            dateFormat: "dd-M-yy",
            timeFormat: "hh:mm TT",
            // minuteGrid: 10,
            onClose: function (selectedDate) {
                var myDAte = $('#txtreminderdate').datetimepicker("getDate");
                var curDate = $('#txtactivitydate').datetimepicker("getDate");
                if (curDate > myDAte) {
                    //alert('current date is high');
                }
                else {
                    alert('Reminder date should be less than activity date');
                    $("#txtreminderdate").datetimepicker("setDate", curDate);
                }
            }
        });
    });

    function Clearhdn() {
        $('#NoData').hide();
        $('#hdnPrimeId').val(0);
        $('#ddlactivitytype').val('0');
        $('#ddlactivitytype').trigger("liszt:updated");
        $('#ddlstatus').val('0');
        $('#ddlstatus').trigger("liszt:updated");
        $('#<%=txtremarks.ClientID %>').val('');
        // $('#txtactivitydate').val('');
        $("#txtreminderdate").val('');
        $("#<%=btnActivitySave.ClientID%>").val('Add Activity');
        document.getElementById('<%=rdNo.ClientID%>').checked = true;
    }

    function setSelectedId(obj) {        
        try {

            if ($('#hdnUpdate').val() == "Y") {
                Clearhdn();
                $("#<%=btnActivitySave.ClientID%>").val('Update');
                var pId = $(obj).attr('actid');
                var atId = $(obj).attr('pdt');
                var actremarks = $(obj).attr('actremarks');
                var actdate = $(obj).attr('activityautoformatdate');
                var status = $(obj).attr('status');
                var reminderdate = $(obj).attr('reminderdate');
                var isForCustomer = $(obj).attr('isforCustomer');
                var assignToId = $(obj).attr('assignToId');
                var isEditable = $(obj).attr('isEditable');
                var IsOCR = $(obj).attr('isocr');
                var ActivityType = $(obj).parent().next().find('#lblactivitytype').html();

                if (pId == '' || pId == '0' || isEditable == 'N') {
                    return false;
                }
                else {
                    if (isForCustomer == 'Y') {
                        document.getElementById('<%=rdYes.ClientID%>').checked = true;
                    }
                    else {
                        document.getElementById('<%=rdNo.ClientID%>').checked = true;
                    }
                    $('#hdnPrimeId').val(pId);
                    $('#ddlstatus').val(status);
                    $('#ddlstatus').trigger("liszt:updated");
                    $('#ddlAssignToUser').val(assignToId);
                    $('#ddlAssignToUser').trigger("liszt:updated");
                    $('#<%=txtremarks.ClientID %>').val(actremarks);
                    $("#txtactivitydate").val(actdate);
                    $("#txtreminderdate").val(reminderdate);
                    $('#FileDownload').attr('href', $(obj).attr('DownloadLinkUrl'));
                    $('#FileDownload').html($(obj).attr('DownloadLinkName'));
                    $('#hdnActivityFileLocation').val($(obj).attr('DownloadLinkUrl'));
                    $("#hdnisocr").val($(obj).attr('isocr'));

                  //  $("#chkIsOCR").attr('disabled', 'disabled');
                    Displayactdiv();

                    $('#ddlactivitytype').val(atId);
                    $('#ddlactivitytype').trigger("liszt:updated");

                    //  alert($('#FUContractTemplate').next('a').html());
                    if ($('#FUContractTemplate').next('a').html() != "") {
                        $('#FUContractTemplate').css('color', 'transparent')
                    }
                    else {
                        $('#FUContractTemplate').css('color', '');
                    }
                    if (IsOCR == "Y") {
                        $("#chkIsOCR").attr("checked", true);
                       // $("#chkIsOCR").prop("disabled", false);
                    }
                    else {
                        $("#chkIsOCR").attr("checked", false);
                       // $("#chkIsOCR").prop("disabled", true);
                    }
                }
                return true;
            }
        }
        catch (e) {
            return false;
        }
    }

    function ScrollBottom() {
        $('#FileDownload').html('');
        $('#hdnActivityFileLocation').val('');
        $("#hdnisocr").val('');
        $('#FileDownload').attr('href', '');
        if ($('#hdnAdd').val() == "Y") {
            $("#actdiv").show();
            $("#actdiv div.chzn-container").css("width", "200px");
            $('#lnkActivityAdd').attr('href', '#actdiv');
        }
    }

    $('.InputBlock').live('keydown', function (e) {
        if (e.keyCode == 46) {
            $(this).val('');
        }
        else {
            return false;
        }
    });

    function setval() {
        if (new Date($('#txtactivitydate').val().replace(/-/g, ' ')) < new Date($('#txtreminderdate').val().replace(/-/g, ' '))) {
            alert('Reminder date should be less than activity date.');
            return false;
        }
    }

</script>
<div align="right">
    <asp:LinkButton runat="server" ID="lnkActivityAdd" ClientIDMode="Static" OnClientClick="Clearhdn();ScrollBottom();">Add Activity</asp:LinkButton>
</div>
<div class="contextual">
    <input id="hdnAdd" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnUpdate" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnActivityFileLocation" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnDeleteActivityId" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnRFParentRequestID" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnisocr" runat="server" clientidmode="Static" type="hidden" />
</div>
<div id="activity">
    <div id="msgActivity" style="display: none;">
    </div>
    <asp:Repeater ID="rptParent" runat="server">
        <ItemTemplate>
            <h3>
                <asp:Label ID="lblactivitydate" runat="server" Text='<%# Eval("OnlyDate","{0:dd-MMM-yyyy}") %>'
                    ClientIDMode="Static"></asp:Label></h3>
            <asp:Repeater ID="rptChild" runat="server" OnItemDataBound="rptChild_ItemDataBound">
                <ItemTemplate>
                    <dl>
                        <input id="hdnActivityId" runat="server" clientidmode="Static" type="hidden" value='<%# Eval("ActivityId") %>' />
                        <dt class="issue"><span class="time">
                            <asp:Label runat="server" Text='<%# Eval("ActivityTime") %>' ClientIDMode="Static"></asp:Label></span>
                            <asp:LinkButton CssClass="clsActivityText" runat="server" ID="lblActivityText" ClientIDMode="Static"
                                OnClientClick="return setSelectedId(this);" Text='<%# Eval("ActivityText") %>'
                                activitydate='<%# Eval("ActivityDate") %>' activityautoformatdate='<%# Eval("ActivityDateDefaultFormat") %>'
                                actremarks='<%# Eval("ActivityText") %>' actid='<%# Eval("ActivityId") %>' assignToId='<%# Eval("AssignToId") %>'
                                pdt='<%# Eval("ActivityTypeId") %>' status='<%# Eval("ActivityStatusId") %>'
                                reminderdate='<%# Eval("ReminderDateDefaultFormat") %>' isforCustomer='<%# Eval("IsForCustomer")%>'
                                isEditable='<%# Eval("isEditable") %>' DownloadLinkUrl='<%# Eval("FileUpload") %>'
                                DownloadLinkName='<%# Eval("FileUploadOriginal")%>' IsOCR='<%# Eval("IsOCR")%>'>
                            </asp:LinkButton>
                            <span class="deleteActivitySpan">
                                <asp:LinkButton ID="lnkDeleteActivity" OnClick="lnkDeleteActivity_Click" DownloadLinkUrl='<%# Eval("FileUpload") %>'
                                    deleteactid='<%# Eval("ActivityId") %>' OnClientClick="return DeleteActivity(this);"
                                    ClientIDMode="Static" runat="server" IsOCR='<%# Eval("IsOCR")%>'><img alt="Delete" src="../images/icon-del.jpg?1349001717"/></asp:LinkButton>
                            </span></dt>
                        <dd>
                            <input id="hdnActivityTypeId" runat="server" clientidmode="Static" type="hidden"
                                value='<%# Eval("ActivityTypeId") %>' />
                            <span class="description">
                                <asp:Label ID="lblactivitytype" runat="server" Text='<%# Eval("ActivityTypeName") %>'
                                    ClientIDMode="Static"></asp:Label></span> <span class="author">Assigned by :
                                        <asp:Label ID="lbladdedby" runat="server" Text='<%# Eval("AddedByName") %>' ClientIDMode="Static"></asp:Label>
                                        on
                                        <asp:Label ID="lbladdedon" runat="server" Text='<%# Eval("Addedon","{0:dd-MMM-yyyy}") %>'
                                            ClientIDMode="Static"></asp:Label>
                                        <asp:Label ID="lbladdedtime" runat="server" Text='<%# Eval("AddedTime") %>' ClientIDMode="Static">
                                        </asp:Label>
                                        <br />
                                        Assigned to :
                                        <asp:Label ID="lblAssignToName" runat="server" Text='<%# Eval("AssignToName") %>'
                                            ClientIDMode="Static">
                                        </asp:Label>
                                        <br />
                                        Attachment : <a href=<%# Eval("FileUpload") %>>
                                            <%# Eval("FileUploadOriginal")%></a> </span>
                        </dd>
                    </dl>
                </ItemTemplate>
            </asp:Repeater>
        </ItemTemplate>
    </asp:Repeater>
    <br />
    <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
</div>
<div style="clear: both;">
</div>
<div style="clear: both;">
</div>
&nbsp;&nbsp;&nbsp;
<asp:Button ID="btnBack" Style="width: 140px;" runat="server" OnClick="btnBack_Click"
    Text="Back to My Contracts" />
<div id="actdiv">
    <div class="box">
        <a name="actname"></a>
        <fieldset class="tabular">
            <legend>Activity</legend>
            <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
            <p>
                <label for="issue_tracker_id">
                    <span class="required">*</span> Activity Date</label>
                <input id="txtactivitydate" runat="server" clientidmode="static" type="text" style="width: 15.5%;"
                    class="required datepicker InputBlock" />
                <em></em>
            </p>
            <p>
                <label for="issue_tracker_id">
                    <span class="required">*</span>Activity Type</label>
                <select id="ddlactivitytype" clientidmode="Static" runat="server" style="width: 200px"
                    class="chzn-select required chzn-select">
                    <option></option>
                </select>
            </p>
            <p>
                <label for="issue_tracker_id">
                    <span class="required">*</span>Status</label>
                <select id="ddlstatus" clientidmode="Static" runat="server" style="width: 200px"
                    class="chzn-select required chzn-select">
                    <option></option>
                </select>
                <em></em>
            </p>
            <p>
                <label for="issue_tracker_id">
                    <span class="required">*</span>Activity Remarks</label>
                <textarea id="txtremarks" runat="server" class="required" maxlength="500" onkeyup="countChar(this)"
                    clientidmode="static" cols="40" rows="10" type="text" />
                <em></em>
            </p>
            <p>
                <label for="issue_tracker_id">
                    Reminder Date</label>
                <input id="txtreminderdate" runat="server" clientidmode="static" type="text" style="width: 15.5%;"
                    class="datepicker InputBlock" />
                <em></em>
            </p>
            <p>
                <label for="time_entry_comments">
                    Post on customer portal</label>
                <input id="rdYes" name="ActiveInactive" runat="server" type="radio" class="required"
                    clientidmode="Static" style="margin-top: 7px" />Yes
                <input id="rdNo" name="ActiveInactive" runat="server" type="radio" class="required"
                    checked="true" clientidmode="Static" style="margin-top: 8px" />No
            </p>
            <p>
                <label for="issue_tracker_id">
                    Upload File</label>
                <asp:FileUpload ID="FUContractTemplate" runat="server" Onchange="return ValidateFile();"
                    Width="180px" /><br>
                <asp:Label ID="lblfilename" runat="server" Visible="False" ClientIDMode="Static"></asp:Label>
                <a href="#" runat="server" id="FileDownload"></a>
                <%-- <asp:LinkButton ID="lnkDOwnload" runat="server" class="icon icon-attachment"></asp:LinkButton>--%>
            </p>
            <p>
                <label for="chkIsOCR">
                    OCR</label>
                <input type="checkbox" runat="server" clientidmode="Static" id="chkIsOCR" style="margin-top: 8px"
                    checked="false" disabled="disabled" />
            </p>
            <p>
                <label for="issue_tracker_id">
                    <span class="required">*</span>Assign To</label>
                <select id="ddlAssignToUser" clientidmode="Static" runat="server" style="width: 200px"
                    class="chzn-select required chzn-select">
                    <option></option>
                </select>
                <em></em>
            </p>
        </fieldset>
    </div>
    <input id="hdnLinkStatus" runat="server" clientidmode="Static" type="hidden" />
    <asp:Button ID="btnActivitySave" runat="server" OnClientClick="return setval()" Text="Add Activity"
        OnClick="btnActivitySave_Click" class="btn_validate" />
    <input id="btnCancel" type="button" value="Cancel" onclick="Cancel();" />
    <script src="../CommonScripts/dropdownlist.js" type="text/javascript"></script>
    <link href="../Styles/chosen.css" rel="stylesheet" type="text/css" />
    <script src="../scripts/chosen.jquery.js" type="text/javascript"></script>
    <script type="text/javascript">

        SetChosen();
    </script>
</div>
<script type="text/javascript">
    var validFilesTypes = ["jpg", "gif", "png", "bmp", "tiff", "jpeg", "TIF", "blob", "xls", "xlsx", "doc", "docx", "pdf", "swf", "ppt", "pptx", "txt", "csv", "odt", "odp", "odg", "ods", "mp3", "mp4", "wmv", "msg", "zip", "avi"];
    function ValidateFile() {
        //alert('Hi');

        $('#FUContractTemplate').css('color', '');
        var file = document.getElementById("<%=FUContractTemplate.ClientID%>");

        var path = file.value;
        var ext = path.substring(path.lastIndexOf(".") + 1, path.length).toLowerCase();
        if (ext == "") {

            if ($("#hdnisocr").val() == "Y")
                $("#chkIsOCR").attr('checked', true);
            else
                $("#chkIsOCR").attr('checked', false);

            $("#chkIsOCR").attr('disabled', 'disabled');
            return false;
        }
        var isValidFile = false;
        for (var i = 0; i < validFilesTypes.length; i++) {
            if (ext == validFilesTypes[i]) {
                isValidFile = true;
                if (ext.toLowerCase() == "pdf")
                    $("#chkIsOCR").removeAttr('disabled');
                else {
                    $("#chkIsOCR").attr('disabled', 'disabled');
                    $("#chkIsOCR").attr('checked', false);
                }
                break;
            }
        }

        if (!isValidFile) {

            $("#chkIsOCR").attr('checked', false);
            $("#chkIsOCR").attr('disabled', 'disabled');

            alert("Invalid File. Please upload a File with" +
         " extension:\n\n" + validFilesTypes.join(", "));
        }
        return isValidFile;
    }
</script>
<script type="text/javascript">
    $(window).load(function () {
        $('#activity').find('dl').each(function () {
            if ($(this).find('a').attr('isEditable') == 'N') {
                //$(this).find('a').css('text-decoration', 'none');
                $(this).find('a').addClass('areadOnly');
                $(this).find('span[class^="deleteActivitySpan"]').hide();
            }
        });
    });

    function ExtensionErrorMessage() {
        alert('Invalid file extension !');
        return false;
    }

    function DeleteActivity(obj) {
        $('#hdnDeleteActivityId').val('');
        if (DeleteConfrim() == true) {
            $('#hdnDeleteActivityId').val($(obj).attr('deleteactid'));
            $("#hdnActivityFileLocation").val($(obj).attr('downloadlinkurl'));
            $("#hdnisocr").val($(obj).attr('isocr'));
            return true;
        }
        else {
            return false;
        }
    }
</script>
