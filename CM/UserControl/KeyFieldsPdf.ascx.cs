﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WorkflowBLL;
using CommonBLL;
using MetaDataConfiguratorsBLL;
using System.Data;

public partial class UserControl_KeyFieldsPdf : System.Web.UI.UserControl
{
    public static int RecordsPerPage = 30;
    public static int VisibleButtonNumbers = 10;

    IKeyFields objkey;
    public static string trueimg = "../Images/AvailYesIco.gif";
    public static string falseimg = "../Images/AvailNoIco.png";

    public string UserControlProperty { get; set; }
    string LinkStatus = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        LinkStatus = UserControlProperty;

        CreateObjects();
        BindValues();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            setAccessValues();

        }
        BindContractRequest();
    }

    private void setAccessValues()
    {
        //Added by Dilip 17-12-2014
        #region KeyField Tab Permission
        Access.PageAccess("WorkflowImportantDatesTab", Session[Declarations.User].ToString());
        ViewState[Declarations.Update] = Access.Update.Trim();
       

        #endregion
    }

    void CreateObjects()
    {

        objkey = FactoryWorkflow.GetKeyFieldsDetails();
    }

    public void BindValues()
    {
        try
        {
            List<KeyFeilds> mylist;
            int requestId = int.Parse(Request.QueryString["RequestId"]);
            ViewState["RequestId"] = requestId;
            objkey.RequestId = requestId;
            mylist = objkey.ReadData();
            lblEffectivedate.Text = (mylist[0].EffectiveDatestring).ToString();
            lblexpirationdate.Text = (mylist[0].ExpirationDatestring).ToString();
            lblRenewal.Text = (mylist[0].RenewalDatestring).ToString();
            //dateImages(mylist);

            DataTable dtEx = (DataTable)Session["ImportDateDataEx"];
            DataTable dtRen = (DataTable)Session["ImportDateDataRen"];

            IEnumerable<DataRow> rowsrpt = dtEx.AsEnumerable()
             .Where(r => r.Field<string>("ExAlerts") == "Y");

            DataTable dtE = new DataTable();
            if (rowsrpt.Count() > 0)
                dtE = rowsrpt.CopyToDataTable();

            if (dtE.Rows.Count > 0)
            {
                try
                {
                    if (Convert.ToString(dtE.Rows[0]["ExAlertsDay"]) != "1")
                        lblDays1.Text = Convert.ToString(dtE.Rows[0]["ExAlertsDay"]) + " days";
                    else
                        lblDays1.Text = Convert.ToString("24") + " Hrs";
                    lblexp180user1.Text = Convert.ToString(dtE.Rows[0]["ExUserName1"]);
                    lblexp180user2.Text = Convert.ToString(dtE.Rows[0]["ExUserName2"]);
                    ExImg180.Text = "Yes";

                    if (Convert.ToString(dtE.Rows[1]["ExAlertsDay"]) != "1")
                        lblDays2.Text = Convert.ToString(dtE.Rows[1]["ExAlertsDay"]) + " days";
                    else
                        lblDays2.Text = Convert.ToString("24") + " Hrs";
                    lblexp90user1.Text = Convert.ToString(dtE.Rows[1]["ExUserName1"]);
                    lblexp90user2.Text = Convert.ToString(dtE.Rows[1]["ExUserName2"]);
                    ExImg90.Text = "Yes";


                    if (Convert.ToString(dtE.Rows[2]["ExAlertsDay"]) != "1")
                        lblDays3.Text = Convert.ToString(dtE.Rows[2]["ExAlertsDay"]) + " days";
                    else
                        lblDays3.Text = Convert.ToString("24") + " Hrs";
                    lblexp60user1.Text = Convert.ToString(dtE.Rows[2]["ExUserName1"]);
                    lblexp60user2.Text = Convert.ToString(dtE.Rows[2]["ExUserName2"]);
                    ExImg60.Text = "Yes";

                }
                catch (Exception ex) { }
            }
            else
            {
                lblExpireAlert.Text = " : No Reminder";
                tblExperationAlert.Style.Add("display", "none");
            }

            IEnumerable<DataRow> rowsrpt1 = dtRen.AsEnumerable()
           .Where(r => r.Field<string>("RenAlerts") == "Y");

            DataTable dtR = new DataTable();
            if (rowsrpt1.Count() > 0)
                dtR = rowsrpt1.CopyToDataTable();

            if (dtR.Rows.Count > 0)
            {
                try
                {
                    if (Convert.ToString(dtR.Rows[0]["RenAlertsDay"]) != "1")
                        lblRenDays1.Text = Convert.ToString(dtR.Rows[0]["RenAlertsDay"]) + " days";
                    else
                        lblRenDays1.Text = Convert.ToString("24") + " Hrs";
                    lblren180user1.Text = Convert.ToString(dtR.Rows[0]["RenUserName1"]);
                    lblren180user2.Text = Convert.ToString(dtR.Rows[0]["RenUserName2"]);
                    EffImg180.Text = "Yes";

                    if (Convert.ToString(dtR.Rows[1]["RenAlertsDay"]) != "1")
                        lblRenDays2.Text = Convert.ToString(dtR.Rows[1]["RenAlertsDay"]) + " days";
                    else
                        lblRenDays2.Text = Convert.ToString("24") + " Hrs";
                    lblren90user1.Text = Convert.ToString(dtR.Rows[1]["RenUserName1"]);
                    lblren90user2.Text = Convert.ToString(dtR.Rows[1]["RenUserName2"]);
                    EffImg90.Text = "Yes";

                    if (Convert.ToString(dtR.Rows[2]["RenAlertsDay"]) != "1")
                        lblRenDays3.Text = Convert.ToString(dtR.Rows[2]["RenAlertsDay"]) + " days";
                    else
                        lblRenDays3.Text = Convert.ToString("24") + " Hrs";
                    lblren60user1.Text = Convert.ToString(dtR.Rows[2]["RenUserName1"]);
                    lblren60user2.Text = Convert.ToString(dtR.Rows[2]["RenUserName2"]);
                    EffImg60.Text = "Yes";
                }
                catch (Exception ex) { }
            }
            else
            {
                lblRenewalDate.Text = "&nbsp; : No Reminder";
                tblRenewaldate.Style.Add("display", "none");
            }
        }
        catch { }
    }

    private void BindContractRequest()
    {
        try
        {
            IMetaDataConfigurators objMetaDataConfig = FactoryMedaData.GetContractTemplateDetail();
            Control container = objMetaDataConfig.GenerateRequestPdfForm("Important Dates", ViewState["RequestId"].ToString(), "view");
            pnlnewadd.Controls.Add(container);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    void dateImages(List<KeyFeilds> mylist)
    {
        try
        {

            ////   if(mylist[0].ExpirationAlert180days.Count )
            //if (mylist[0].ExpirationAlert180days == "Y")
                ExImg180.Text = mylist[0].ExpirationAlert180days == "Y" ? "Yes" : "No";
           // if (mylist[0].ExpirationAlert90days == "Y")
                ExImg90.Text = mylist[0].ExpirationAlert90days == "Y" ? "Yes" : "No";
           // if (mylist[0].ExpirationAlert60days == "Y")
                ExImg60.Text = mylist[0].ExpirationAlert60days == "Y" ? "Yes" : "No";

            //   if (mylist[0].ExpirationAlert24Hrs == "Y")
            ExImg24.Text = mylist[0].ExpirationAlert24Hrs == "Y" ? "Yes" : "No";
            ExImg30.Text = mylist[0].ExpirationAlert30days == "Y" ? "Yes" : "No";
            ExImg7.Text = mylist[0].ExpirationAlert7days == "Y" ? "Yes" : "No";
            EffImg24.Text = mylist[0].RenewalAlert24Hrs == "Y" ? "Yes" : "No";
            EffImg30.Text = mylist[0].RenewalAlert30days == "Y" ? "Yes" : "";
            EffImg7.Text = mylist[0].RenewalAlert7days == "Y" ? "Yes" : "";

            EffImg180.Text = mylist[0].RenewalAlert180days == "Y" ? "Yes" : "";
            EffImg90.Text = mylist[0].RenewalAlert90days == "Y" ? "Yes" : "";
            EffImg60.Text = mylist[0].RenewalAlert60days == "Y" ? "Yes" : "";
        }
        catch { }
    }

    protected void EditQuestionnair_Click(object sender, EventArgs e)
    {
        Server.Transfer("../Workflow/KeyFieldsMaster.aspx?LinkStatus=" + LinkStatus);

    }
}