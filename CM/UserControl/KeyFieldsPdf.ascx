﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KeyFieldsPdf.ascx.cs"
    Inherits="UserControl_KeyFieldsPdf" %>
<div>
    <h3>
        <span style="font-size: 20px; color: #de6528"><strong>Important Dates</strong></span></h3>
    <ul class="Details">
        <li><strong style="font-size: 12px; color: #289aa1">Date of Agreement : </strong>
            <asp:Label ID="lblEffectivedate" runat="server" ClientIDMode="Static"></asp:Label></li></ul>
    <ul class="Details">
        <li><strong style="font-size: 12px; color: #289aa1">Expiration Date : </strong>
            <asp:Label ID="lblexpirationdate" runat="server" ClientIDMode="Static"></asp:Label></li></ul>
    <ul>
        <li><strong style="font-size: 12px; color: #289aa1">Alert For Expiration Date
            <asp:Label ID="lblExpireAlert" runat="server" ClientIDMode="Static" Text="" Style="font-size: 12px;
                color: #777675; font-weight: normal;" />
        </strong>
            <br />
            <table width="50%" cellspacing="1" cellpadding="2" border="0" bgcolor="#cccccc" id="tblExperationAlert"
                runat="server">
                <tr bgcolor="#ffffff">
                    <td width="5%" align="center">
                        &nbsp;
                    </td>
                    <td width="20%">
                        Period
                    </td>
                    <td width="35%">
                        User 1
                    </td>
                    <td width="35%">
                        User 2
                    </td>
                </tr>
                <tr bgcolor="#ffffff" align="left" id="trDays1" clientidmode="Static">
                    <td align="center">
                        <%-- <asp:Image ID="ExImg180" runat="server" />--%>
                        <asp:Label ID="ExImg180" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblDays1" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblexp180user1" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblexp180user2" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                </tr>
                <tr bgcolor="#ffffff" align="left" id="trDays2" clientidmode="Static">
                    <td align="center">
                        <asp:Label ID="ExImg90" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                        <%-- <asp:Image ID="ExImg90" runat="server" />--%>
                    </td>
                    <td>
                        <asp:Label ID="lblDays2" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblexp90user1" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblexp90user2" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                </tr>
                <tr bgcolor="#ffffff" align="left" id="trDays3" clientidmode="Static">
                    <td align="center">
                        <asp:Label ID="ExImg60" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                        <%--  <asp:Image ID="ExImg60" runat="server" />--%>
                    </td>
                    <td>
                        <asp:Label ID="lblDays3" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblexp60user1" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblexp60user2" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                </tr>
                <tr bgcolor="#ffffff" align="left" style="display: none;" clientidmode="Static">
                    <td align="center">
                        <asp:Label ID="ExImg30" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                        <%--<asp:Image ID="ExImg30" runat="server" />--%>
                    </td>
                    <td>
                        30 days
                    </td>
                    <td>
                        <asp:Label ID="lblexp30user1" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblexp30user2" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                </tr>
                <tr bgcolor="#ffffff" align="left" style="display: none;">
                    <td align="center">
                        <asp:Label ID="ExImg7" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                        <%--  <asp:Image ID="ExImg7" runat="server" />--%>
                    </td>
                    <td>
                        7 days
                    </td>
                    <td>
                        <asp:Label ID="lblexp7user1" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblexp7user2" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                </tr>
                <tr bgcolor="#ffffff" align="left" style="display: none;">
                    <td align="center">
                        <asp:Label ID="ExImg24" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                        <%--    <asp:Image ID="ExImg24" runat="server" />--%>
                    </td>
                    <td>
                        24 Hrs
                    </td>
                    <td>
                        <asp:Label ID="lblexp24user1" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblexp24user2" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                </tr>
            </table>
        </li>
    </ul>
    <br />
    <ul>
        <li><strong style="font-size: 12px; color: #289aa1">Renewal Date : </strong>
            <asp:Label ID="lblRenewal" runat="server" ClientIDMode="Static"></asp:Label></li></ul>
    <ul>
        <li><strong style="font-size: 12px; color: #289aa1">Alert For Renewal Date
            <asp:Label ID="lblRenewalDate" runat="server" ClientIDMode="Static" Text="" Style="font-size: 12px;
                color: #777675; font-weight: normal;" />
        </strong>
            <br />
            <table width="50%" cellspacing="1" cellpadding="2" border="0" bgcolor="#cccccc" id="tblRenewaldate"
                runat="server">
                <tr bgcolor="#ffffff">
                    <td width="5%" align="center">
                        &nbsp;
                    </td>
                    <td width="20%">
                        Period
                    </td>
                    <td width="35%">
                        User 1
                    </td>
                    <td width="35%">
                        User 2
                    </td>
                </tr>
                <tr bgcolor="#ffffff" align="left" id="trRenDays1" clientidmode="Static">
                    <td align="center">
                        <asp:Label ID="EffImg180" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                        <%-- <asp:Image ID="EffImg180" runat="server" />--%>
                    </td>
                    <td>
                        <asp:Label ID="lblRenDays1" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblren180user1" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblren180user2" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                </tr>
                <tr bgcolor="#ffffff" align="left" id="trRenDays2" clientidmode="Static">
                    <td align="center">
                        <%-- <asp:Image ID="EffImg90" runat="server" />--%>
                        <asp:Label ID="EffImg90" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblRenDays2" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblren90user1" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblren90user2" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                </tr>
                <tr bgcolor="#ffffff" align="left" id="trRenDays3" clientidmode="Static">
                    <td align="center">
                        <asp:Label ID="EffImg60" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                        <%--   <asp:Image ID="EffImg60" runat="server" />--%>
                    </td>
                    <td>
                        <asp:Label ID="lblRenDays3" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblren60user1" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblren60user2" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                </tr>
                <tr bgcolor="#ffffff" align="left" style="display: none;" clientidmode="Static">
                    <td align="center">
                        <asp:Label ID="EffImg30" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                        <%--     <asp:Image ID="EffImg30" runat="server" />--%>
                    </td>
                    <td>
                        30 days
                    </td>
                    <td>
                        <asp:Label ID="lblren30user1" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblren30user2" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                </tr>
                <tr bgcolor="#ffffff" align="left" style="display: none;" clientidmode="Static">
                    <td align="center">
                        <asp:Label ID="EffImg7" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                        <%-- <asp:Image ID="EffImg7" runat="server" />--%>
                    </td>
                    <td>
                        7 days
                    </td>
                    <td>
                        <asp:Label ID="lblren7user1" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblren7user2" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                </tr>
                <tr bgcolor="#ffffff" align="left" style="display: none;" clientidmode="Static">
                    <td align="center">
                        <asp:Label ID="EffImg24" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                        <%--   <asp:Image ID="EffImg24" runat="server" />--%>
                    </td>
                    <td>
                        24 Hrs
                    </td>
                    <td>
                        <asp:Label ID="lblren24user1" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblren24user2" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                </tr>
            </table>
        </li>
    </ul>
    <asp:PlaceHolder ID="pnlnewadd" runat="server"></asp:PlaceHolder>
    <script type="text/javascript">
      
        $(document).ready(function () {

            if ($('#lblDays1').text() == '') {
                $('#trDays1').css("display", "none");
            }
            if ($('#lblDays2').text() == '') {
                $('#trDays2').css("display", "none");
            }
            if ($('#lblDays3').text() == '') {
                $('#trDays3').css("display", "none");
            }


            if ($('#lblRenDays1').text() == '') {
                $('#trRenDays1').css("display", "none");
            }
            if ($('#lblRenDays2').text() == '') {
                $('#trRenDays2').css("display", "none");
            }
            if ($('#lblRenDays3').text() == '') {
                $('#trRenDays3').css("display", "none");
            }
        });     
    </script>
</div>
