﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PaginationButtons.ascx.cs"
    Inherits="UserControl_PaginationButtons" %>
<script src="../CommonScripts/GridNavigation.js" type="text/javascript"></script>
<link href="../Styles/application.css" rel="stylesheet" type="text/css" />
<input id="hdnFirstVisibleButton" runat="server" clientidmode="Static" type="hidden" />
<input id="hdnLastVisibletButton" runat="server" clientidmode="Static" type="hidden" />
<input id="hdnNumberButtonClick" runat="server" clientidmode="Static" type="hidden" />
<input id="hdnPreviousButtonClick" runat="server" clientidmode="Static" type="hidden" />
<input id="hdnNextButtonClick" runat="server" clientidmode="Static" type="hidden" />
<input id="hdnFirstNextPreLastFlag" runat="server" clientidmode="Static" type="hidden" />
<input id="hdnHighlightButtonClick" runat="server" clientidmode="Static" type="hidden" />
<input id="hdnRecordsPerPage" runat="server" clientidmode="Static" type="hidden" />
<input id="hdnEventArgument" runat="server" clientidmode="Static" type="hidden" value="" />
<style type="text/css">
    a, a:link, a:visited{ color: #169; text-decoration: none; }
.selected{color:Gray; font-style:italic;background-color:#de6528; text-decoration:none;}
</style>
<div class="pagination">
</div>
  
<%--<asp:Panel ID="panel1" runat="server"></asp:Panel>--%>
<table width="100%" id="tblPaging" runat="server" style="font-weight: bold" class="pagination">
    <tr>
        <td align="left" style="text-align: left; width: 42%; font-size: 12px;" class="paginationtd">
           
            <asp:Panel ID="panel1" runat="server" CssClass="paginationPanel" ></asp:Panel>
         <%--   <asp:Label ID="panel1"  class="pagination"  runat="server"></asp:Label>--%>
            <asp:Label ID="lblPageNo" style="display:none" runat="server"></asp:Label>           
            <asp:Label Style="display: none; font-size: 13px" ClientIDMode="Static" class="pagination1"
                ID="lblRecordStatus1" runat="server" Text=""></asp:Label>
        </td>
        <td style="text-align: left; width: 58%">
            <span>Per Page:</span> <b>
                <asp:LinkButton ID="lnkbtn10Records" runat="server" Text="10" OnClick="lnkbtn10Records_Click"  OnClientClick="SetPageSize(10)"></asp:LinkButton>,&nbsp
                <asp:LinkButton ID="lnkbtn50Records" runat="server" Text="50" OnClick="lnkbtn50Records_Click"  OnClientClick="SetPageSize(50)" CssClass="selected"></asp:LinkButton>,&nbsp
                <asp:LinkButton ID="lnkbtn100Records" runat="server" Text="100" OnClick="lnkbtn100Records_Click"  OnClientClick="SetPageSize(100)"></asp:LinkButton></b>
        </td>
        <td align="left">
        </td>
    </tr>
</table>
<p id="NoData" runat="server" visible="false" class="nodata">
    No data to display
</p>

<script type="text/javascript">
    $(window).load(function () {
        try {
            var no = $('#hdnHighlightButtonClick').val();
            if (no == '') {
                no = '1';
            }
            var id = "#MainContent_PaginationButtons1_Button_" + no;
            $(id).addClass('selected');
            $('#Panel1').append('&nbsp;');
            $('#Panel1').append($('#lblRecordStatus').show());
        } catch (e) {
        }
    });
    $(document).ready(function () {
        var R = $("#hdnRecordsPerPage").val();
        if (R == '') {
            R = '50';
        }
        //alert($("span[id$=lblRecordStatus]").text());
        //var a = '<%= ViewState["lblRecordStatus"] %>';
        $("span[id$=lblRecordStatus]").html('<%= ViewState["lblRecordStatus"] %>');

        switch (R) {
            case '10':
                $('a[id*="lnkbtn10Records"]').addClass("selected");
                $('a[id*="lnkbtn50Records"]').removeClass("selected");
                $('a[id*="lnkbtn100Records"]').removeClass("selected");
                break;
            case '50':
                $('a[id*="lnkbtn50Records"]').addClass("selected");
                $('a[id*="lnkbtn10Records"]').removeClass("selected");
                $('a[id*="lnkbtn100Records"]').removeClass("selected");

                break;
            case '100':
                $('a[id*="lnkbtn100Records"]').addClass("selected");
                $('a[id*="lnkbtn50Records"]').removeClass("selected");
                $('a[id*="lnkbtn10Records"]').removeClass("selected");
                break;
            default:
                $('a[id*="lnkbtn50Records"]').addClass("selected");
                $('a[id*="lnkbtn10Records"]').removeClass("selected");
                $('a[id*="lnkbtn100Records"]').removeClass("selected");
                break;
        }
    });
       
</script>
