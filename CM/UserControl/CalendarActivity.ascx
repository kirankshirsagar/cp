﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CalendarActivity.ascx.cs"
    Inherits="UserControl_CalendarActivity" %>
<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<style type="text/css">
    .tool_tip
    {
        color: Red;
    }
    textarea
    {
        font-family: Verdana,sans-serif;
        font-size: 12px;
    }
</style>
<script src="../JQueryValidations/mastervalidations.js" type="text/javascript"></script>
<script src="../Scripts/application.js" type="text/javascript"></script>
<link href="../Styles/application.css" media="all" rel="stylesheet" type="text/css">
<script src="../JQueryValidations/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../scripts/jquery-ui-timepicker-addon.js"></script>
<script src="../JQueryValidations/jquery.validate-min.js" type="text/javascript"></script>
<link rel="stylesheet" href="../scripts/jquery-ui-timepicker-addon.css" type="text/css" />
<script src="../JQueryValidations/jquery.autocomplete.js" type="text/javascript"></script>
<link href="../Styles/jquery-ui-1.8.21.css" media="all" rel="stylesheet" type="text/css">
<script src="../scripts/jquery.ui.core.min.js"></script>
<script src="../scripts/jquery.ui.widget.min.js"></script>
<script src="../scripts/jquery.ui.position.min.js"></script>
<script src="../scripts/jquery.ui.autocomplete.min.js"></script>
<link rel="stylesheet" href="../scripts/jquery-ui-1.8.16.custom.css" />
<link rel="stylesheet" href="../scripts/jquery-ui-timepicker-addon.css" type="text/css" />
<link rel="stylesheet" href="../JQueryValidations/jquery.ui.slider.css" type="text/css" />
<script type="text/javascript" src="../JQueryValidations/jquery.ui.slider.js"></script>
<script type="text/javascript" src="../JQueryValidations/jquery.ui.slider.min.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $('#hdnActivityFileLocation').val($("#FileDownload").attr('href'));
    });

    var validFilesTypes = ["jpg", "gif", "png", "bmp", "tiff", "jpeg", "TIF", "blob", "xls", "xlsx", "doc", "docx", "pdf", "swf", "ppt", "pptx", "txt", "csv", "odt", "odp", "odg", "ods", "mp3", "mp4", "wmv", "msg", "zip", "avi"];
    function ValidateFile() {
        //  alert('Hi');
        $('#activitiescontrolid_FUContractTemplate').css('color', '');
        var file = document.getElementById("<%=FUContractTemplate.ClientID%>");

        var path = file.value;
        var ext = path.substring(path.lastIndexOf(".") + 1, path.length).toLowerCase();
        if (ext == "") {

            if ($("#hdnisocr").val() == "Y")
                $("#chkIsOCR").attr('checked', true);
            else
                $("#chkIsOCR").attr('checked', false);

            $("#chkIsOCR").attr('disabled', 'disabled');
            return false;
        }
        var isValidFile = false;
        for (var i = 0; i < validFilesTypes.length; i++) {
            if (ext == validFilesTypes[i]) {
                isValidFile = true;
                if (ext.toLowerCase() == "pdf")
                    $("#chkIsOCR").removeAttr('disabled');
                else {
                    $("#chkIsOCR").attr('disabled', 'disabled');
                    $("#chkIsOCR").attr('checked', false);
                }
                break;
            }
        }

        if (!isValidFile) {
            $("#chkIsOCR").attr('checked', false);
            $("#chkIsOCR").attr('disabled', 'disabled');

            alert("Invalid File. Please upload a File with" +
         " extension:\n\n" + validFilesTypes.join(", "));
        }
        return isValidFile;
    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        if ($('#activitiescontrolid_FUContractTemplate').next('a').html() != "") {
            $('#activitiescontrolid_FUContractTemplate').css('color', 'transparent')
        }
        else {
            $('#activitiescontrolid_FUContractTemplate').css('color', '');
        }
        $('#ui-datepicker-div').hide();
    });

    function Cancel() {
        $("#actdiv").hide();
        $("#hdnisocr").val('');
        $("#chkIsOCR").attr('checked', false);
        $('#hdnActivityFileLocation').val(''); 
    }

    function Displayactdiv() {
        if ($('#hdnUpdate').val() == "Y") {
            $("#actdiv").show();
            $("#actdiv div.chzn-container").css("width", "200px");
            $('a').attr('href', '#actdiv');
        }
    }

    function loadUserControl() {
        $.ajax({
            type: "POST",
            url: "../Workflow/LoadUserControl.asmx/RenderUC",
            data: "{path: '" + page + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function (jqXHR) {
                $.xhrPool.push(jqXHR);
                $(body).html('<div style="text-align: center;width:100%;"><img src="../images/circular.gif" />  </div> ');
            },
            success: function (r) {
                //                if (location.href.indexOf('WorkFlow.aspx') == -1) {
                //                    $(body).html('<table width="100%"><tr style="height:25px;vertical-align: middle;"><td style="text-align:right">  <a href="#" id="hrefChildRefresh"><img src="../Styles/css/icons/16/arrow_refresh.png" title="Refresh" class="mws-tooltip-e" /></a> </td> </tr> <tr> <td>' + r.d + '</td> </tr> </table>').attr('data', 'fill');
                //                }
                //                else {
                $(body).html('<table width="100%"><tr style="height:25px;vertical-align: middle;"><td style="text-align:right"><a id="hrefNewChildWindow" href="#"><img style="width:16px;height:16px;" src="../Styles/css/icons/16/application_double.png" title="New windnw" class="mws-tooltip-e" /></a> <a id="hrefChildControlPopup" href="#"><img style="width:16px;height:16px;" title="Detach" class="mws-tooltip-e" src="../Styles/css/icons/32/application_tile_vertical.png" /></a>  <a href="#" id="hrefChildRefresh"><img src="../Styles/css/icons/16/arrow_refresh.png" title="Refresh" class="mws-tooltip-e" /></a> </td> </tr> <tr> <td>' + r.d + '</td> </tr> </table>').attr('data', 'fill');
                //}
                $.xhrPool.pop();
            },
            error: function () {
                $(body).html('<label class="error"><strong>Oops!</strong>Try that again in a few moments.</label>');
            }
        });
    }
    function Clearhdn() {
        $('#hdnPrimeId').val(0);
        $('#ddlactivitytype').val('0');
        $('#ddlactivitytype').trigger("liszt:updated");
        $('#ddlstatus').val('0');
        $('#ddlstatus').trigger("liszt:updated");
        $('#<%=txtremarks.ClientID %>').val('');
        $('#txtactivitydate').val('');
        $("#txtreminderdate").val('');
        $("#<%=btnActivitySave.ClientID%>").val('Add Activity');
        // $("#rdNo").attr("checked", true);
        document.getElementById('<%=rdNo.ClientID%>').checked = true;
    }

    function setSelectedId(obj) {
        if ($('#hdnUpdate').val() == "Y") {
            Clearhdn();
            $("#<%=btnActivitySave.ClientID%>").val('Update');
            var pId = $(obj).attr('actid');
            var atId = $(obj).attr('pdt');
            var actremarks = $(obj).attr('actremarks');
            var actdate = $(obj).attr('activityautoformatdate');
            var status = $(obj).attr('status');
            var reminderdate = $(obj).attr('reminderdate');
            var isForCustomer = $(obj).attr('isforCustomer');
            //alert(isForCustomer);
            if (pId == '' || pId == '0') {
                return false;
            }

            else {
                if (isForCustomer == 'Y') {
                    //$("#rdYes").attr("checked", true);
                    document.getElementById('<%=rdYes.ClientID%>').checked = true;
                }
                else {
                    //$("#rdNo").attr("checked", true);
                    document.getElementById('<%=rdNo.ClientID%>').checked = true;
                }
                $('#hdnPrimeId').val(pId);
                $('#ddlactivitytype').val(atId);
                $('#ddlactivitytype').trigger("liszt:updated");
                $('#ddlstatus').val(status);
                $('#ddlstatus').trigger("liszt:updated");
                $('#<%=txtremarks.ClientID %>').val(actremarks);

                var dt = actdate.split(' ')[0];
                var time = actdate.split(' ')[1].split(':');
                var t = actdate.split(' ')[1].split(':')[0] + ':' + actdate.split(' ')[1].split(':')[1] + ' ' + actdate.split(' ')[2];
                var DateArray = new Array();
                DateArray = actdate.split(' ')[0].split('/');
                $("#txtactivitydate").val('');
                $("#txtreminderdate").val('');
                var activitydate = Number(DateArray[0]) + '-' + Number(DateArray[1]) + '-' + Number(DateArray[2]) + ' ' + actdate.split(' ')[1].split(':')[0] + ':' + actdate.split(' ')[1].split(':')[1];
                $("#txtactivitydate").datetimepicker("setDate", new Date(activitydate));

                if (reminderdate != null && reminderdate != "") {
                    var rDateArray = new Array();
                    rDateArray = reminderdate.split(' ')[0].split('/');
                    var reminderadate = Number(rDateArray[0]) + '-' + Number(rDateArray[1]) + '-' + Number(rDateArray[2]) + ' ' + reminderdate.split(' ')[1].split(':')[0] + ':' + reminderdate.split(' ')[1].split(':')[1];
                    $("#txtreminderdate").datetimepicker("option", "maxDate", new Date(activitydate));
                    $("#txtreminderdate").datetimepicker("setDate", new Date(reminderadate));
                }
                Displayactdiv();
            }
            return true;
        }
    }

    function ScrollBottom() {
        if ($('#hdnAdd').val() == "Y") {
            $("#actdiv").show();
            $("#actdiv div.chzn-container").css("width", "200px");
            $('#ActivityAdd').attr('href', '#actdiv');
        }
    }

    function settextVal(obj) {
        $('#txtremarks').val($(obj).val());
    }

    function SetVal() {
        var ActivityDate = $('#txtactivitydate').val();
        var ActivityTypeId = $('#ddlactivitytype').val();
        var StatusId = $('#ddlstatus').val();
        var txtRemarks = $('#txtremarks').val();
        var reminderDate = $('#txtreminderdate').val();
        var IsOnCustomerPortal = $('#rdYes').prop('checked');
        if (IsOnCustomerPortal == true) {
            IsOnCustomerPortal = 'Y'
        }
        else {
            IsOnCustomerPortal = 'N'
        }
        $('#hdnActivityDate').val(ActivityDate);
        $('#hdnActivityTypeId').val(ActivityTypeId);
        $('#hdnStatusId').val(StatusId);
        $('#hdnRemark').val(txtRemarks);
        $('#hdnReminderDate').val(reminderDate);
        $('#hdnIsOnCustomerPortal').val(IsOnCustomerPortal);
        $('#hdnAssigneTo').val($('#hdnAssigneTo').val());
        $('#hdnrequestId').val($('#txtRequestId').val());
        //alert($('#txtactivitydate').val().replace(/-/g, ' ') + '--' + $('#txtreminderdate').val().replace(/-/g, ' '));
        //alert(new Date($('#txtactivitydate').val().replace(/-/g, ' ')) + '--' + new Date($('#txtreminderdate').val().replace(/-/g, ' ')));
        if (new Date($('#txtactivitydate').val().replace(/-/g, ' ')) < new Date($('#txtreminderdate').val().replace(/-/g, ' '))) {
            alert('Reminder date should be less than activity date.');
            return false;
        }
    }

    function SetUserValue(obj) {
        $('#hdnAssigneTo').val($(obj).val());
        //    alert($('#hdnAssigneTo').val());  
        return false;

    }
    function ExtensionErrorMessage() {
        alert('Invalid file extension !');
        return false;
    }

    function SetValidation(obj) {
        if ($(obj).val() == "0") {
            $(obj).next('.tooltip_outer').show();
        }
        else {
            $(obj).next('.tooltip_outer').hide();
        }
    }
    function countChar(val) {
        var len = val.value.length;

        if (len >= 500) {
            val.value = val.value.substring(0, 500);
        } else {
            return false;
        }
    };
</script>

<div class="contextual">
    <input id="hdnAdd" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnUpdate" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnActivityDate" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnActivityTypeId" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnStatusId" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnRemark" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnReminderDate" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnIsOnCustomerPortal" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnAssingedToDefault" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnAssigneTo" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnrequestId" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnActivityFileLocation" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnisocr" runat="server" clientidmode="Static" type="hidden" />
</div>
<div id="msgmsg" style="display: none">
</div>
<div id="activity" style="width: 100%">
    <div id="actdiv">
        <div class="box">
            <a name="actname"></a>
            <fieldset class="box tabularr">
                <legend>
                    <h2>
                        Activity</h2>
                </legend>
                <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
                <p>
                    <label for="issue_tracker_id">
                        Contract ID:</label>
                    <input id="txtRequestId" runat="server" maxlength="100" clientidmode="Static" type="text"
                        style="width: 200px;" class="name autocom mws-autotextinput" />
                    <asp:HiddenField ID="hdnSelectedRequestId" runat="server" ClientIDMode="Static" Value="0" />
                    <em></em>
                </p>
                <p>
                    <label for="issue_tracker_id">
                        <span class="required">*</span>Activity Date</label>
                    <input id="txtactivitydate" readonly="readonly" runat="server" maxlength="100" clientidmode="Static"
                        type="text" style="width: 150px;" class="required mws-dtpicker" />
                    <em></em>
                </p>
                <p>
                    <label for="issue_tracker_id">
                        <span class="required">*</span> Activity Type</label>
                    <select id="ddlactivitytype" onchange="SetValidation(this)" clientidmode="Static"
                        runat="server" style="width: 203px; margin-top: 5px" class="chzn-select required chzn-select">
                        <option></option>
                    </select>
                </p>
                <p>
                    <label for="issue_tracker_id">
                        <span class="required">*</span>Status</label>
                    <select id="ddlstatus" onchange="SetValidation(this)" clientidmode="Static" runat="server"
                        style="width: 203px; margin-top: 5px" class="chzn-select required chzn-select">
                        <option></option>
                    </select>
                    <em></em>
                </p>
                <p>
                    <label for="issue_tracker_id">
                        <span class="required">*</span> Activity Remarks</label>
                    <%--  <textarea id="txtremarks"  runat="server" type="text" class="ui-autocomplete-input required" maxlength="500"
                    cols="30" rows="2" autocomplete="off"  aria-autocomplete="list" aria-haspopup="true"></textarea>
                <em></em>--%>
                    <textarea id="txtremarks" runat="server" style="width: 197px" class="required" onkeyup="countChar(this);settextVal(this);"
                        clientidmode="Static" />
                    <em></em>
                </p>
                <p>
                    <label for="issue_tracker_id">
                        Reminder Date</label>
                    <input id="txtreminderdate" onkeypress="javascript:return false" readonly="readonly"
                        runat="server" maxlength="100" clientidmode="Static" type="text" style="width: 150px;"
                        class="mws-dtpicker" />
                    <em></em>
                </p>
                <p style="display: none">
                    <label for="time_entry_comments">
                        Post on customer portal</label>
                    <input id="rdYes" name="ActiveInactive" runat="server" type="radio" class="required"
                        clientidmode="Static" />Yes
                    <input id="rdNo" name="ActiveInactive" runat="server" type="radio" class="required"
                        checked="true" clientidmode="Static" />No
                </p>
                <p>
                    <label for="issue_tracker_id">
                        Upload File</label>
                    <asp:FileUpload onchange="return ValidateFile()" ID="FUContractTemplate" runat="server"
                        Width="180px" />
                    <asp:Label ID="lblfilename" runat="server" Visible="False" ClientIDMode="Static"></asp:Label>
                    <a href="#" runat="server" target="_blank" id="FileDownload" clientidmode="Static">
                    </a>
                    <%-- <asp:LinkButton ID="lnkDOwnload" runat="server" class="icon icon-attachment"></asp:LinkButton>--%>
                </p>
                <p>
                    <label for="issue_tracker_id">
                        OCR</label>
                    <input type="checkbox" runat="server" clientidmode="Static" id="chkIsOCR" style="margin-top: 8px"
                        checked="false" disabled="disabled" />
                </p>
                <p>
                    <label for="time_entry_comments">
                        <span class="required">*</span> Assign To</label>
                    <select id="ddlUsers" clientidmode="Static" runat="server" style="width: 203px; margin-top: 5px"
                        class="chzn-select required chzn-select">
                        <option></option>
                    </select>
                </p>
            </fieldset>
        </div>
        <asp:Button ID="btnActivitySave" runat="server" Text="Add Activity" OnClientClick="return SetVal()"
            OnClick="btnActivitySave_Click" class="btn_validate" />
        <input id="btnCancel" type="button" value="Cancel" onclick="return CloseActivityWindow();" />
        <script src="../scripts/jquery.ui.autocomplete.min.js"></script>
        <script src="../CommonScripts/dropdownlist.js" type="text/javascript"></script>
        <link href="../Styles/chosen.css" rel="stylesheet" type="text/css" />
        <script src="../scripts/chosen.jquery.js" type="text/javascript"></script>
    </div>
