﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KeyObligationsPdf.ascx.cs"
    Inherits="UserControl_KeyObligationsPdf" %>
<div>
    <h3>
        <strong style="font-size: 20px; color: #de6528">Key Obligations </strong>
    </h3>
    <table width="100%">
        <tr>
            <td style="width: 25%" valign="top">
                <ul class="details">
                    <li><strong style="font-size: 12px; color: #289aa1">Liability Cap</strong> </li>
                </ul>
            </td>
            <td style="width: 2%" valign="top">
                :
            </td>
            <td style="width: 73%" valign="top">
                <asp:Label ID="lblLiabilityCap" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 25%" valign="top">
                <ul class="details">
                    <li><strong style="font-size: 12px; color: #289aa1">Liability for Indirect/Consequential Losses</strong> </li>
                </ul>
            </td>
            <td style="width: 2%" valign="top">
                :
            </td>
            <td style="width: 73%" valign="top">
                <asp:Label ID="lblIndirectConsequentialLosses" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong style="font-size: 12px; color: #289aa1">Indemnity</strong> </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblIndemnity" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
         <tr style="display:none">
            <td valign="top">
                <ul class="details">
                    <li><strong style="font-size: 12px; color: #289aa1">Warranties</strong></li></ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblWarranties" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul class="details">
                    <li><strong style="font-size: 12px; color: #289aa1">Change Of Control</strong></li></ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblchangeofControlYesNo" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul class="details">
                    <li><strong style="font-size: 12px; color: #289aa1">Entire Agreement Clause</strong>
                    </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblIsAgreementClauseYesNo" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul class="details">
                    <li><strong style="font-size: 12px; color: #289aa1">Strict liability</strong> </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblStrictliabilityYesNo" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul class="details">
                    <li><strong style="font-size: 12px; color: #289aa1">Strict liability description</strong></li></ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblStrictliability" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul class="details">
                    <li><strong style="font-size: 12px; color: #289aa1">Third Party Guarantee Required</strong>
                    </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblThirdPartyGuaranteeRequiredYesNo" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong style="font-size: 12px; color: #289aa1">Insurance</strong> </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblInsurance" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul class="Details">
                    <li><strong style="font-size: 12px; color: #289aa1">Termination</strong></li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblTerminationdesc" runat="server" ClientIDMode="Static"></asp:Label>
                <br />
                <asp:Label ID="lblTerminationConvenience" runat="server" ClientIDMode="Static"></asp:Label>
                -
                <%--       <asp:Image ID="rdIsTerminationConvenience" runat="server" />--%>Termination
                for convenience<br />
                <asp:Label ID="lblTerminationMaterialYesNo" runat="server" ClientIDMode="Static"></asp:Label>
                -
                <%-- <asp:Image ID="rdIsTerminationMaterial" runat="server" />--%>Termination for
                breach
                <br />
                <asp:Label ID="lblTerminationinsolvencyYesNo" runat="server" ClientIDMode="Static"></asp:Label>
                -
                <%-- <asp:Image ID="rdIsTerminationinsolvency" runat="server" />--%>Termination
                for insolvency
                <br />
                <asp:Label ID="lblTerminationControlYesNo" runat="server" ClientIDMode="Static"></asp:Label>
                -
                <%-- <asp:Image ID="rdIsTerminationControl" runat="server" />--%>Termination for
                change of control
                <br />
                <asp:Label ID="lblTerminationothercausesYesNo" runat="server" ClientIDMode="Static"  style="display:none"></asp:Label>
                <%--- Termination for other reasons--%>
                
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong style="font-size: 12px; color: #289aa1">Assignment/Novation/Subcontract</strong>
                    </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblNovation" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong style="font-size: 12px; color: #289aa1">Force Majeure</strong>
                    </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblForceMajeure" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong style="font-size: 12px; color: #289aa1">Force Majeure Description</strong>
                    </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblForceMajeureDescription" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong style="font-size: 12px; color: #289aa1">Set off rights</strong>
                    </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblSetoffrights" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong style="font-size: 12px; color: #289aa1">Set off rights Description</strong>
                    </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblSetoffrightsDescription" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong style="font-size: 12px; color: #289aa1">Governing Law / Applicable Law</strong>
                    </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblGoverningLaw" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong style="font-size: 12px; color: #289aa1">Governing Law / Applicable Law Description</strong>
                    </li>
                </ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblGoverningDescription" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ul>
                    <li><strong style="font-size: 12px; color: #289aa1">Others</strong></li></ul>
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:Label ID="lblOthers" runat="server" ClientIDMode="Static"></asp:Label>
            </td>
        </tr>
        <tr>
            <asp:Label ID="pnlnewadd" Style="font-size: 12px; color: #289aa1" runat="server"></asp:Label>
        </tr>
    </table>
    <div style="font-size: 12px; color: #289aa1; mar">
        <%--  <asp:PlaceHolder ID="pnlnewadd"  runat="server"></asp:PlaceHolder>--%>
    </div>
</div>
