﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;
using UserManagementBLL;
using System.Data;
public partial class UserControl_setuplinks : System.Web.UI.UserControl
{
    IAccessLink obj;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string Path = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            string PageName = Path.Substring(Path.LastIndexOf("/"));
            try
            {
                if (Session["MasterSeelcted"] != null && Session["MasterSeelcted"].ToString() != "")
                {
                    ddlChildlink.Visible = true;
                    //ddlParentlink.SelectedValue = Session["MasterSeelcted"].ToString();
                    obj = FactoryUser.GetAccessDetail();
                    if (Session[Declarations.User] != null && Session[Declarations.User] != string.Empty)
                    {
                        obj.UserId = int.Parse(Session[Declarations.User].ToString());
                    }
                    //obj.Flag = ddlParentlink.SelectedValue;
                    obj.Flag = Session["MasterSeelcted"].ToString();
                    DataTable table = ConvertListToDataTable(obj.ReadData());
                    ddlChildlink.DataSource = table;
                    ddlChildlink.DataTextField = "PageName";
                    ddlChildlink.DataValueField = "PageLink";
                    ddlChildlink.DataBind();
                    ddlChildlink.Items.Add(new ListItem() { Text = "--Select link--", Value = "0" });

                    if (Session["ChlidSeelcted"] != null)
                    {
                        ddlChildlink.SelectedValue = Session["ChlidSeelcted"].ToString();
                    }
                    else
                    {
                        ddlChildlink.SelectedIndex = 0;
                    }
                }

                //Added By Prashant
                obj = FactoryUser.GetAccessDetail();
                if (Session[Declarations.User] != null && Session[Declarations.User] != string.Empty)
                {
                    obj.UserId = int.Parse(Session[Declarations.User].ToString());
                }
                DataTable table1 = ConvertListToDataTable(obj.ReadParentData());
                ddlParentlink.DataSource = table1;
                ddlParentlink.DataTextField = "PageName";
                ddlParentlink.DataValueField = "PageLink";
                ddlParentlink.DataBind();

                //if (Session["MasterSeelcted"] == null)
                //{
                //    ddlParentlink.Items.FindByText("Configurators").Selected = true;
                //    ddlParentlink_SelectedIndexChanged(sender, e);
                //}
                //else
                ddlParentlink.SelectedValue = Session["MasterSeelcted"].ToString();
            }
            catch (Exception ex)
            {

            }
            //if (PageName == "/RegisterData.aspx" && ddlParentlink.SelectedValue == "User")
            //{
            //    ddlParentlink.SelectedValue = "User";
            //    ddlChildlink.Visible = true;
            //    Session["ChlidSeelcted"] = "";
            //    ddlChildlink.SelectedValue = "RegisterData.aspx";
            //}
        }
    }

    public static DataTable ConvertListToDataTable(List<AccessLink> list)
    {
        // New table.
        DataTable table = new DataTable();

        // Get max columns.

        table.Columns.Add("PageName");
        table.Columns.Add("Pagelink");

        // Add rows.
        foreach (var array in list)
        {
            table.Rows.Add(array.PageName, array.PageLink);
        }

        return table;
    }

    protected void ddlParentlink_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlParentlink.SelectedValue != "0")
        {
            Session["MasterSeelcted"] = ddlParentlink.SelectedValue;
            string FolderPath = "";
            obj = FactoryUser.GetAccessDetail();
            if (Session[Declarations.User] != null && Session[Declarations.User] != string.Empty)
            {
                obj.UserId = int.Parse(Session[Declarations.User].ToString());
            }
            ddlChildlink.Visible = true;

            if (Session["MasterSeelcted"] != null && Session["MasterSeelcted"] != "")
            {

                obj.Flag = Session["MasterSeelcted"].ToString();

            }
            else
            {
                obj.Flag = ddlParentlink.SelectedValue;
            }

            DataTable table = ConvertListToDataTable(obj.ReadData());
            //if (table.Rows.Count > 0)
            //{
                ddlChildlink.DataSource = table;
                ddlChildlink.DataTextField = "PageName";
                ddlChildlink.DataValueField = "PageLink";
                ddlChildlink.DataBind();
                ddlChildlink.Items.Add(new ListItem() { Text = "--Select link--", Value = "0" });

                bool StruserMgnt = false;

                if (ddlParentlink.SelectedValue == "Master")
                {
                    //FolderPath = "../Masters/TitleMasterData.aspx";
                    FolderPath = "../Masters/" + ddlChildlink.Items[0].Value;
                }
                else if (ddlParentlink.SelectedValue == "ClauseLibrary")
                {
                    FolderPath = "../ClauseLiabrary/ClauseData.aspx";
                }
                else if (ddlParentlink.SelectedValue == "User")
                {
                    foreach (ListItem item in ddlChildlink.Items)
                    {
                        if (item.Text == "Users")
                        {
                            StruserMgnt = true;
                            break;
                        }
                    }
                    if (StruserMgnt == true)
                    {
                        FolderPath = "../UserManagement/RegisterData.aspx";
                        Session["ChlidSeelcted"] = "RegisterData.aspx";
                    }
                    else
                    {
                        FolderPath = "../UserManagement/" + ddlChildlink.Items[0].Value;
                    }
                    //FolderPath = "../UserManagement/RegisterData.aspx";
                }
                else if (ddlParentlink.SelectedValue == "Configurators")
                {
                    //FolderPath = "../Masters/ContractTypeMasterData.aspx";
                    FolderPath = ddlChildlink.Items[0].Value;
                }
                else if (ddlParentlink.SelectedValue == "MetaData Configurators" || ddlParentlink.SelectedValue == "MetaData Configurator")
                {
                    //FolderPath = "../Configurators/MetaDataConfigurators.aspx";
                    FolderPath = "../Configurators/MetaData.aspx";
                }
                Session["DrpSelectText"] = "Key Obligations";
                string Path = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                string PageName = Path.Substring(Path.LastIndexOf("/"));
                Response.Redirect(FolderPath);
            //}
            //else
            //{
            //    ddlChildlink.Items.Clear();
            //    ddlChildlink.Items.Add(new ListItem() { Text = "--Select link--", Value = "0" });
            //}
        }

        else
        {
            ddlChildlink.Items.Clear();
        }
    }
    protected void ddlChildlink_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlChildlink.SelectedValue != "0")
        {
            string FolderPath = "";
            Session["MasterSeelcted"] = ddlParentlink.SelectedValue;
            Session["ChlidSeelcted"] = ddlChildlink.SelectedValue;
            if (ddlParentlink.SelectedValue == "Master")
            {
                FolderPath = "Masters";
            }
            else if (ddlParentlink.SelectedValue == "ClauseLibrary")
            {
                FolderPath = "ClauseLibrary";
            }
            else if (ddlParentlink.SelectedValue == "User")
            {
                FolderPath = "UserManagement";

            }
            else if (ddlParentlink.SelectedValue == "Configurators")
            {
                FolderPath = "Configurators";
            }
            else if (ddlParentlink.SelectedValue == "MetaData Configurator" || ddlParentlink.SelectedValue == "MetaData Configurators")
            {
                FolderPath = "Configurators";
            }
            Session["DrpData"] = ddlChildlink.SelectedValue;
            Session["DrpSelectText"] = ddlChildlink.SelectedItem.Text;
            Response.Redirect("../" + FolderPath + "/" + ddlChildlink.SelectedValue);
        }
        else
        {
            Session["DrpSelectText"] = "--Select link--";
        }
    }
}