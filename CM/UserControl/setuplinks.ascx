﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="setuplinks.ascx.cs" Inherits="UserControl_setuplinks" %>
<div class="contextual">
    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
    <link href="../JQueryValidations/jquery-ui-1.8.19.custom.css" rel="stylesheet" type="text/css" />


    <asp:DropDownList CssClass="chzn-select" Width="200px" ID="ddlParentlink"
        runat="Server" OnSelectedIndexChanged="ddlParentlink_SelectedIndexChanged" AutoPostBack="true">
        <%--<asp:ListItem Text="--Select link--" Value="0"></asp:ListItem>
        <asp:ListItem Text="Clause Library" Value="ClauseLibrary"></asp:ListItem>
        <asp:ListItem Text="Masters" Value="Master"></asp:ListItem>
        <asp:ListItem Text="User Management" Value="User"></asp:ListItem>
        <asp:ListItem Text="Configurators" Value="Configurators"></asp:ListItem>--%>

    </asp:DropDownList>
    <asp:DropDownList ID="ddlChildlink" Width="200px" CssClass="chzn-select"
        runat="Server" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlChildlink_SelectedIndexChanged">
    </asp:DropDownList>
    <%-- <a id="idClaseLibrary" visible="false" runat="server" href="../ClauseLiabrary/ClauseData.aspx" class="icon icon-library">
        Clause Library</a>&nbsp;&nbsp;<a id="idMasters" visible="false" runat="server" href="../Masters/CountryMasterData.aspx"
            accesskey="e" class="icon icon-masters">Masters</a>&nbsp;&nbsp;<a id="idUserManagement" visible="false"
                runat="server" href="../UserManagement/RegisterData.aspx" class="icon icon-users">User
                Management</a>&nbsp;&nbsp;<a id="idConfigurators" visible="false" runat="server" href="../Masters/ContractTypeMasterData.aspx"
                    class="icon icon-configurators">Configurators</a>&nbsp;&nbsp;--%>
</div>
