﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;

public partial class UserControl_PaginationButtons : System.Web.UI.UserControl
{

    public int VisibleButtonNumbers { get; set; }// number must be >=5
    public int pages { get; set; }
    public long TotalRecord { get; set; }
    public int PageNumber { get; set; }
    public int RecordsPerPage { get; set; }
    Label lblRecordStatus = new Label();
    protected void Page_Init(object sender, System.EventArgs e)
    {
        string strEventArg = "";
        if (Page.Request.Params.Get("__EVENTTARGET") == null)
        {
            strEventArg = "";
        }
        else
        {
            strEventArg = Page.Request.Params.Get("__EVENTTARGET").ToLower();
        }
        if (hdnRecordsPerPage.Value == "" || strEventArg.Contains("btnshowall") || strEventArg == "")
        {
            hdnRecordsPerPage.Value = System.Configuration.ConfigurationManager.AppSettings["RecordsPerPage"];
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string strEventArg = "";
        if (Page.Request.Params.Get("__EVENTTARGET") == null)
        {
            strEventArg = "";
        }
        else
        {
            strEventArg = Page.Request.Params.Get("__EVENTTARGET").ToLower();
        }
        if (hdnEventArgument.Value == "")
        {
            if (hdnRecordsPerPage.Value == "" || strEventArg.Contains("btnshowall") || strEventArg == "")
            {
                hdnRecordsPerPage.Value = System.Configuration.ConfigurationManager.AppSettings["RecordsPerPage"];
            }
        }
        AddpagingButton();
    }

    public void AddpagingButton()
    {
        // this method for generate custom button for Custom paging in Gridview         
        if (string.IsNullOrEmpty(hdnRecordsPerPage.Value))
            RecordsPerPage = 50;
        else
            RecordsPerPage = Convert.ToInt16(hdnRecordsPerPage.Value);
        panel1.Controls.Clear();

        if (TotalRecord > 0 && RecordsPerPage > 0)
        {
            NoData.Visible = false;
            tblPaging.Visible = true;
            // Count no of pages 
            pages = (int)(TotalRecord / RecordsPerPage) + ((TotalRecord % RecordsPerPage) > 0 ? 1 : 0);
            Label lblPageNumber = new Label();
            lblPageNumber.Text = "Page&nbsp;";

            Label lblColon = new Label();
            lblColon.Text = "&nbsp;:&nbsp;";

            panel1.Controls.Add(lblPageNumber);
            if (pages > 1)
            {
                lblColon.Text = "&nbsp;:&nbsp;";
                CreateNavigations();
            }
            else
            {
                lblColon.Text = "1&nbsp;:&nbsp;";
                SetRecordStatus();
            }
            panel1.Controls.Add(lblColon);
            panel1.Controls.Add(lblRecordStatus);
        }
        else
        {
            //lblRecordStatus.Text = "Showing 0 to 0 of 0 entries";
            lblRecordStatus.Text = "";
            NoData.Visible = true;
            tblPaging.Visible = false;
        }
    }

    private void CreateNavigations()
    {
        long FirstButtons = 1;
        long LastButtons = VisibleButtonNumbers;

        if (!IsPostBack)
        {
            hdnFirstVisibleButton.Value = "3";
            hdnPreviousButtonClick.Value = "1";
        }

        if (hdnPreviousButtonClick.Value == "1")
        {
            FirstButtons = Convert.ToInt64(hdnFirstVisibleButton.Value);
            LastButtons = FirstButtons + VisibleButtonNumbers - 1;
            if (FirstButtons <= 3)
            {
                FirstButtons = 0;
            }
        }
        else if (hdnNextButtonClick.Value == "1")
        {
            LastButtons = Convert.ToInt64(hdnLastVisibletButton.Value);
            FirstButtons = LastButtons - VisibleButtonNumbers + 1;
            if (LastButtons >= pages - 2)
            {
                LastButtons = pages;
            }
        }

        else if (hdnNumberButtonClick.Value == "1")
        {
            FirstButtons = Convert.ToInt64(hdnFirstVisibleButton.Value);
            LastButtons = Convert.ToInt64(hdnLastVisibletButton.Value);
            if (FirstButtons <= 3)
            {
                FirstButtons = 0;
            }
        }

        hdnFirstVisibleButton.Value = FirstButtons.ToString();
        hdnLastVisibletButton.Value = LastButtons.ToString();

        if (hdnFirstVisibleButton.Value == "")
        {
            hdnFirstVisibleButton.Value = FirstButtons.ToString();
        }
        if (hdnLastVisibletButton.Value == "")
        {
            hdnLastVisibletButton.Value = LastButtons.ToString();
        }

        FirstButtons = Convert.ToInt64(hdnFirstVisibleButton.Value);
        LastButtons = Convert.ToInt64(hdnLastVisibletButton.Value);


        if (FirstButtons > 0 && pages > VisibleButtonNumbers + 4)
        {
            AddNavigation(Declarations.FirstStr, VisibleButtonNumbers, pages);
            AddNavigation(Declarations.PreviousStr, VisibleButtonNumbers, pages);
        }

        for (long i = 0; i < pages; i++)
        {
            if (i >= FirstButtons - 1 && (i < LastButtons || pages <= VisibleButtonNumbers + 4))
            {
                AddNavigation(i);
            }
        }

        if (LastButtons != pages && pages > VisibleButtonNumbers + 4)
        {
            AddNavigation(Declarations.NextStr, VisibleButtonNumbers, pages);
            AddNavigation(Declarations.LastStr, VisibleButtonNumbers, pages);
        }

        if (hdnFirstNextPreLastFlag.Value != "")
        {
            b_click();
        }
        else
        {
            SetRecordStatus();
        }

    }

    private void AddNavigation(long i)
    {
        LinkButton b = new LinkButton();
        b.Text = (i + 1).ToString() + " ";
        b.CommandArgument = (i + 1).ToString();
        b.ID = "Button_" + (i + 1).ToString();
        b.Attributes.Add("onclick", "paginationNumberClick(" + b.ID + ");");
        b.Click += new EventHandler(this.b_click);
        b.CssClass = "";
        panel1.Controls.Add(b);
    }

    private void AddNavigation(string str, int btnVisibleNumber, long pagesNumber)
    {
        LinkButton b = new LinkButton();
        b.Text = str + " ";
        b.CommandArgument = str;
        b.ID = "Button_" + str;
        b.Attributes.Add("onclick", "paginationJumpClick('" + b.ID + "'," + btnVisibleNumber + "," + pagesNumber + ");");
        b.Click += new EventHandler(this.b_click);
        b.CssClass = "";
        panel1.Controls.Add(b);
    }

    protected void b_click(object sender, EventArgs e)
    {
        string pageStr = ((LinkButton)sender).CommandArgument;
        int n;
        bool isNumeric = int.TryParse(pageStr, out n);
        if (isNumeric == true)
        {
            PageNumber = int.Parse(pageStr);
            hdnHighlightButtonClick.Value = PageNumber.ToString();
        }
        SetRecordStatus();
    }

    void b_click()
    {
        int PageNo = 0;
        if (hdnFirstNextPreLastFlag.Value == "F")
        {
            PageNo = 1;
        }
        else if (hdnFirstNextPreLastFlag.Value == "P")
        {
            PageNo = int.Parse(hdnLastVisibletButton.Value);
        }
        else if (hdnFirstNextPreLastFlag.Value == "N")
        {
            PageNo = int.Parse(hdnFirstVisibleButton.Value);
        }
        else if (hdnFirstNextPreLastFlag.Value == "L")
        {
            PageNo = pages;
        }
        PageNumber = PageNo;
        hdnHighlightButtonClick.Value = PageNumber.ToString();
        hdnFirstNextPreLastFlag.Value = "";
        SetRecordStatus();
    }

    void SetRecordStatus()
    {
        int currentPage = 0;
        currentPage = PageNumber - 1;

        long toRecords;
        if (currentPage < 0 || currentPage == 0)
        {
            if (currentPage < 0)
            {
                {
                    currentPage = 0;
                    hdnHighlightButtonClick.Value = "1";
                }
            }
        }
        //toRecords = (currentPage * VisibleButtonNumbers) + VisibleButtonNumbers;
        toRecords = (currentPage * RecordsPerPage) + RecordsPerPage;
        if (toRecords > TotalRecord)
        {
            toRecords = TotalRecord;
        }
        lblRecordStatus.ID = "lblRecordStatus";
        lblRecordStatus.Text = "&nbsp;Showing " + ((currentPage * RecordsPerPage) + 1).ToString() +
        " to " + toRecords.ToString() +
        " of " + TotalRecord.ToString() + " results";
        lblRecordStatus.Attributes.Add("class", "pagination");
        ViewState["lblRecordStatus"] = lblRecordStatus.Text;
    }
    protected void lnkbtn10Records_Click(object sender, EventArgs e)
    {
        RecordsPerPage = 10;
        hdnRecordsPerPage.Value = "10";
        PageNumber = 1;
        AddpagingButton();
        SetRecordStatus();
        lnkbtn10Records.CssClass = "selected";
        lnkbtn50Records.CssClass = "";
        lnkbtn100Records.CssClass = "";
    }

    protected void lnkbtn50Records_Click(object sender, EventArgs e)
    {
        RecordsPerPage = 50;
        hdnRecordsPerPage.Value = "50";
        hdnNumberButtonClick.Value = "";
        PageNumber = 1;
        AddpagingButton();
        SetRecordStatus();
        lnkbtn10Records.CssClass = "";
        lnkbtn50Records.CssClass = "selected";
        lnkbtn100Records.CssClass = "";
    }

    protected void lnkbtn100Records_Click(object sender, EventArgs e)
    {
        RecordsPerPage = 100;
        hdnRecordsPerPage.Value = "100";
        hdnNumberButtonClick.Value = "";
        PageNumber = 1;
        AddpagingButton();
        SetRecordStatus();
        lnkbtn10Records.CssClass = "";
        lnkbtn50Records.CssClass = "";
        lnkbtn100Records.CssClass = "selected";
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        AddpagingButton();
    }

}