﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="System.Threading" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup

    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started
        //Session.Clear();
    }
    protected void Application_BeginRequest(Object sender, EventArgs e)
    {
        CultureInfo newCulture = (CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
        newCulture.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy";
        newCulture.DateTimeFormat.DateSeparator = "/";
        Thread.CurrentThread.CurrentCulture = newCulture;
    }
    void Session_End(object sender, EventArgs e) 
    {
        Session.Clear();
        Response.Redirect("~/Login.aspx");

    }
       
</script>
