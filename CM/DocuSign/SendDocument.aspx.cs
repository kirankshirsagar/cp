﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Web;
using System.Net;
using System.Xml;
using System.Web.UI.WebControls;
using ClauseLibraryBLL.DocuSignAPI;
using ClauseLibraryBLL.CredentialAPI;
using DocumentOperationsBLL;
using CommonBLL;
using EmailBLL;
using System.Configuration;

public partial class DocuSign_SendDocument : System.Web.UI.Page
{

    IDocuSign objDocuSign;
    string username = "";//System.Configuration.ConfigurationManager.AppSettings["DocuSignAPIUserEmail"];			// your account email
    string password = ""; //System.Configuration.ConfigurationManager.AppSettings["DocuSignPassword"];			// your account password
    string integratorKey = System.Configuration.ConfigurationManager.AppSettings["DocuSignIntegratorsKey"];  // your account Integrator Key (found on Preferences -> API page)
    string contentType = "application/pdf";		// default content type is PDF
    //---------------------------------------------------------------------------------------------------

    string ClientName = "";
    string ClientEmail = "";
    // additional variable declarations
    string baseURL = "";			// - we will retrieve this through the Login API call
    string FileNameOrigional = "";
    string fileName = "";
    string path2 = "";

    protected void Page_Load(object sender, EventArgs e)
    {

        username = Session["DocuSignUsername"].ToString();		// your account email
        password = Session["DocuSignPassword"].ToString(); 		// your account password
        objDocuSign = FactoryAssembly.DocusignDetails();
        string RequestID = Request.QueryString["RequestId"];
        string isPDF = Request.QueryString["isPDF"];
        string ContractId = Request.QueryString["ContractId"];
        string FileName = Server.MapPath(@"..//Uploads/" + Session["TenantDIR"] + "//Contract Documents//");
        FileNameOrigional = Request.QueryString["Filename"] + ".docx";
        string ContractFileId = Request.QueryString["ContractFileId"];
        string isMessageAlreadySent = Request.QueryString["isMessageAlreadySent"];

        hdnRequestId.Value = RequestID;
        hdnContractId.Value = ContractId;
        hdnFileName.Value = FileName + "/" + Request.QueryString["Filename"] + ".docx";
        hdnFileId.Value = ContractFileId;
        ClientName = Request.QueryString["ClientName"];
        ClientEmail = Request.QueryString["ClientEmail"];
        ReciepientName.Text = ClientName;
        ReciepientEmail.Text = ClientEmail;
        txtOrder.Text = "1";
        txtOrderNo1.Text = "2";
        if (isPDF == "Y")
        {
            fileName = Request.QueryString["Filename"] + ".pdf";
            path2 = HttpContext.Current.Server.MapPath(@"..//Uploads/" + Session["TenantDIR"] + "//Contract Documents//PDF//" + fileName);
        }
        else
        {
            fileName = Request.QueryString["Filename"] + ".docx";
            path2 = HttpContext.Current.Server.MapPath(@"..//Uploads/" + Session["TenantDIR"] + "//Contract Documents//" + fileName);
        }

        hdnismailalreadysent.Value = isMessageAlreadySent;

        ViewState["ParentRequestID"] = "";
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["ParentRequestID"])))
        {
            ViewState["ParentRequestID"] = "&ParentRequestID=" + Request.QueryString["ParentRequestID"];
        }
        if (!IsPostBack)
        {
            SetSignatoryEmailDetails();
        }
    }

    protected void Back_Click(object sender, EventArgs e)
    {
        Response.Redirect("../Workflow/RequestFlow.aspx?ScrollTo=DivContractVersions&RequestId=" + hdnRequestId.Value + "&Status=Workflow" + ViewState["ParentRequestID"]);
    }

    protected void SendDoc_Click(object sender, EventArgs e)
    {
        try
        {
            List<RecipientDetails> rd = new List<RecipientDetails>();

            string url = "https://www.docusign.net/restapi/v2/login_information";
            HttpWebRequest request = initializeRequest(url, "GET", null, username, password, integratorKey);
            string response = getResponseBody(request);
            baseURL = parseDataFromResponse(response, "baseUrl");
            url = baseURL + "/envelopes";

            string[] RecipientConstruct = hdnRecipientDetails.Value.Split('#');
            string recipients = "";
            string DocumentString = "";
            int counterRecipient = 1;

            int Xaxis = 1;
            foreach (var item in RecipientConstruct)
            {
                try
                {
                    if (item.Split('$')[0] != "" || item.Split('$')[1] != "")
                    {
                        recipients = recipients + "<signer>" +
                       "<recipientId>" + counterRecipient.ToString() + "</recipientId>" +
                       "<email>" + item.Split('$')[1] + "</email>" +
                       "<name>" + item.Split('$')[0] + "</name>" +
                       "<routingOrder>" + item.Split('$')[2] + "</routingOrder>" +
                       "</signer>";

                        rd.Add(new RecipientDetails() { Name = item.Split('$')[0], Email = item.Split('$')[1] });
                        Xaxis = Xaxis + 1;
                    }
                }
                catch (Exception ex)
                {
                }
                counterRecipient++;
            }


            recipients = "<recipients><signers>" +
            recipients +
             "</signers>" +
             "</recipients>";
            DocumentString = DocumentString +
            "<documents>" +
            "<document>" +
            "<documentId>1</documentId>" +
            "<name>" + fileName + "</name>" +
            "</document>" +
            "</documents>";
            string Subject = "Contract Document of Contract Id-" + Request.QueryString["ContractId"] + " for Signing on " + System.DateTime.Now.ToString("dd-MMM-yyyy");
            //string emailBodys = "<emailBlurb> Please DocuSign " + fileName +"\n"+ txtEmailMessage.Text + "</emailBlurb>";
            string emailBodys = "<emailBlurb>" + txtEmailMessage.Text + "</emailBlurb><emailBlurb>fgfdg'gfgdsg</emailBlurb>";
            string xmlBody =
           "<envelopeDefinition xmlns=\"http://www.docusign.com/restapi\">" +
           "<emailSubject>" + Subject + "</emailSubject>" +
           emailBodys +
           "<brandId>" + Session["DocuSignBrandId"] + "</brandId> " +
            "<status>sent</status>" + DocumentString + recipients + "</envelopeDefinition>";

            request = initializeRequest(url, "POST", null, username, password, integratorKey);
            configureMultiPartFormDataRequest(request, xmlBody, path2, contentType);
            response = getResponseBody(request);
            string STR = response;
            string STRFirst = "<envelopeId>";
            string STRLast = "</envelopeId>";
            string FinalString;
            int Pos1 = STR.IndexOf(STRFirst) + STRFirst.Length;
            int Pos2 = STR.IndexOf(STRLast);
            FinalString = STR.Substring(Pos1, Pos2 - Pos1);
            foreach (var item in rd)
            {
                objDocuSign.Name = item.Name;
                objDocuSign.Email = item.Email;
                objDocuSign.ContractID = int.Parse(hdnContractId.Value);
                objDocuSign.RequestId = int.Parse(hdnRequestId.Value);
                objDocuSign.ContractFileId = int.Parse(hdnFileId.Value);
                objDocuSign.Addedby = Convert.ToInt32(Session[Declarations.User]);
                objDocuSign.DocStatus = "Sent";
                objDocuSign.EnvelopeID = FinalString;
                objDocuSign.SaveDetails();
            }
            //SendSignaotyEmail();

            Response.Redirect("../Workflow/RequestFlow.aspx?Status=Workflow&Section=ContractVersions&Mode=DocuSign&ScrollTo=DivContractVersions&RequestId=" + hdnRequestId.Value + ViewState["ParentRequestID"]);
        }
        catch (WebException ex)
        {
            using (WebResponse response = ex.Response)
            {
                HttpWebResponse httpResponse = (HttpWebResponse)response;
                Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                using (Stream data = response.GetResponseStream())
                {
                    Response.Write(ex.Message);
                    string indexDirPath = HttpContext.Current.Server.MapPath(@"~/DocuSign" );
                    File.AppendAllText(indexDirPath + "/Error.txt", "\n\n " +ex.Message);
                    string text = new StreamReader(data).ReadToEnd();
                    //Page.JavaScriptClientScriptBlock("msg", "MessageMasterDiv('Error while sending document for signature. Please try later.')");
                    if (ex.Message.Contains("400"))
                        Page.JavaScriptClientScriptBlock("msg", "MessageMasterDiv('Recipient name and Email-Id should not be duplicate.')");
                    else
                        Page.JavaScriptClientScriptBlock("msg", "MessageMasterDiv('" + ex.Message + "')");
                }
            }
        }
    }

    protected void SetSignatoryEmailDetails()
    {
        trSendEmail.Visible = false;
        chkSendMail.Checked = false;
        txtEmailMessage.Text = "";
        txtEmailMessage.Text = "Contract initiated by " + Session[Declarations.UserFullName];
    }
    protected void SendSignaotyEmail()
    {
        System.Threading.Thread.Sleep(1000);
        SendMail sm = new SendMail();
        sm.Parameters.Clear();
        sm.MailTo = GetRecipientEmailIds();
        sm.MailFrom = ConfigurationManager.AppSettings["MailFrom"];
        sm.Subject = "ContractPod - Document sent for digital signature";
        sm.Body = txtEmailMessage.Text.Replace("\n", "<br/>");
        bool IsSend = sm.SendSimpleMail();
    }
    protected string GetRecipientEmailIds()
    {
        string EmailIds = "";
        string[] RecipientConstruct = hdnRecipientDetails.Value.Split('#');
        foreach (var item in RecipientConstruct)
        {
            try
            {
                if (!string.IsNullOrEmpty(item.Split('$')[1]))
                    EmailIds += !string.IsNullOrEmpty(EmailIds) ? "," + item.Split('$')[1].ToString() : item.Split('$')[1].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("Error in sending mail \n" + ex.Message);
            }
        }
        return EmailIds;
    }
    public class RecipientDetails
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }

    public static HttpWebRequest initializeRequest(string url, string method, string body, string email, string password, string intKey)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        request.Method = method;
        addRequestHeaders(request, email, password, intKey);
        if (body != null)
            addRequestBody(request, body);
        return request;
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static void addRequestHeaders(HttpWebRequest request, string email, string password, string intKey)
    {
        // authentication header can be in JSON or XML format.  XML used for this walkthrough:
        string authenticateStr =
            "<DocuSignCredentials>" +
                "<Username>" + email + "</Username>" +
                "<Password>" + password + "</Password>" +
                "<IntegratorKey>" + intKey + "</IntegratorKey>" +
                "</DocuSignCredentials>";
        request.Headers.Add("X-DocuSign-Authentication", authenticateStr);
        request.Accept = "application/xml";
        request.ContentType = "application/xml";
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static void addRequestBody(HttpWebRequest request, string requestBody)
    {
        // create byte array out of request body and add to the request object
        byte[] body = System.Text.Encoding.UTF8.GetBytes(requestBody);
        Stream dataStream = request.GetRequestStream();
        dataStream.Write(body, 0, requestBody.Length);
        dataStream.Close();
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static void configureMultiPartFormDataRequest(HttpWebRequest request, string xmlBody, string docName, string contentType)
    {
        // overwrite the default content-type header and set a boundary marker
        request.ContentType = "multipart/form-data; boundary=BOUNDARY";

        // start building the multipart request body
        string requestBodyStart = "\r\n\r\n--BOUNDARY\r\n" +
            "Content-Type: application/xml\r\n" +
                "Content-Disposition: form-data\r\n" +
                "\r\n" +
                xmlBody + "\r\n\r\n--BOUNDARY\r\n" + 	// our xml formatted envelopeDefinition
                "Content-Type: " + contentType + "\r\n" +
                "Content-Disposition: file; filename=\"" + docName + "\"; documentId=1\r\n" +
                "\r\n";
        string requestBodyEnd = "\r\n--BOUNDARY--\r\n\r\n";

        // read contents of provided document into the request stream
        FileStream fileStream = File.OpenRead(docName);
        // write the body of the request
        byte[] bodyStart = System.Text.Encoding.UTF8.GetBytes(requestBodyStart.ToString());
        byte[] bodyEnd = System.Text.Encoding.UTF8.GetBytes(requestBodyEnd.ToString());
        Stream dataStream = request.GetRequestStream();
        dataStream.Write(bodyStart, 0, requestBodyStart.ToString().Length);

        // Read the file contents and write them to the request stream.  We read in blocks of 4096 bytes
        byte[] buf = new byte[4096];
        int len;
        while ((len = fileStream.Read(buf, 0, 4096)) > 0)
        {
            dataStream.Write(buf, 0, len);
        }
        dataStream.Write(bodyEnd, 0, requestBodyEnd.ToString().Length);
        dataStream.Close();
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static string getResponseBody(HttpWebRequest request)
    {
        // read the response stream into a local string
        HttpWebResponse webResponse = (HttpWebResponse)request.GetResponse();
        StreamReader sr = new StreamReader(webResponse.GetResponseStream());
        string responseText = sr.ReadToEnd();
        return responseText;
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static string parseDataFromResponse(string response, string searchToken)
    {
        // look for "searchToken" in the response body and parse its value
        using (XmlReader reader = XmlReader.Create(new StringReader(response)))
        {
            while (reader.Read())
            {
                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == searchToken))
                    return reader.ReadString();
            }
        }
        return null;
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////




}