﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CM.master" CodeFile="SendDocument.aspx.cs"
    Inherits="DocuSign_SendDocument" %>

<%@ Register Src="../UserControl/requestnewlinks.ascx" TagName="requestnewlinks"
    TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        $("#toggleimage").hide();
        function AddRow(obj) {
            if ($(obj).closest('tr').find('td:eq(1)').find('input').val() != '' && $(obj).closest('tr').find('td:eq(2)').find('input').val() != '') {
                $(obj).closest('tr').after('<tr class="RecipientClass"><td><input type="text" class="order onlyNum" runat="server" style="width:50px" placeholder="OrderNo"></input></td> <td><input type="text" runat="server" style="width:300px" placeholder="Name" clientidmode="static"></input></td><td><input type="text" style="width:300px" runat="server" class="email" placeholder="Email"  clientidmode="static"></input></td><td><a href="#" onclick="AddRow(this)">Add recipient</a></td><td><a href="#" onclick="RemoveRow(this)">remove</a></td></tr>');
                SetOrderNo();
            }
            else {
                //alert('Please Enter Name and Email Address');
                MessageMasterDiv('Please Enter Name and Email Address.', 1, 100);
            }
        }

        function SetOrderNo() {
            var order = [];
            $("#recipientList tbody tr.RecipientClass").each(function (index, value) {
                $(this).find(".order").val(index + 1);
            });
        }

        function RemoveRow(obj) {
            $(obj).closest('tr').remove();
            SetOrderNo();
        }

        $(document).ready(function () {
            if ($('#hdnismailalreadysent').val() == 'Y') {
                $('#recipientList').find('input').val('');
            }
            HideShow();
            $('.onlyNum').live('keypress', function (evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode == 8) {
                    return true;
                }
                else if ((charCode < 48) || (charCode > 57)) {
                    return false;
                }
            });

            $('.onlyNum').live('paste', function (evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                $obj = $(this);
                setTimeout(function () {
                    myNum = $obj.val();

                    var pattern = /^[0-9]+$/;

                    if (!pattern.test(myNum)) {
                        myNum = myNum.replace(/[^0-9]/g, '');
                        $obj.val(myNum);
                        return false;
                    }
                }, 0);
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="scrpmgr1" runat="server">
    </asp:ScriptManager>
    <input type="hidden" runat="Server" id="hdnRequestId" />
    <input type="hidden" runat="Server" id="hdnContractId" />
    <input type="hidden" runat="Server" id="hdnFileId" />
    <input type="hidden" runat="Server" id="hdnFileName" />
    <input type="hidden" runat="Server" id="hdnismailalreadysent" clientidmode="static" />
    <input type="hidden" runat="Server" id="hdnRecipientDetails" clientidmode="Static" />
    <label runat="Server" id="lblRName" clientidmode="Static">
    </label>
    <h2>
        <asp:Label ID="lblContractIDHeader" runat="server" Text="Electronic Signature" ClientIDMode="Static"></asp:Label></h2>
    <br />
    <table id="recipientList" name="recipientList" class="recipientList" clientidmode="static">
        <tr id="trSendEmail" runat="Server">
            <th style="text-align: left; vertical-align: top">
                Send Email
            </th>
            <td colspan="2">
                <asp:CheckBox ID="chkSendMail" runat="server" Checked="true"></asp:CheckBox>
            </td>
        </tr>
        <tr id="Tr2" runat="Server" name="Recipient1">
            <th style="text-align: left; vertical-align: top; padding-top: 5px;">
                Message
            </th>
            <td colspan="2">
                <asp:TextBox ID="txtEmailMessage" placeholder="Message" runat="server" Width="100%" CssClass="TxtNumSplCharValidation"
                    onkeypress="return this.value.length<=500" onkeyup="return this.value.length<=500"
                    TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr class="recipientListHeader">
            <th style="text-align: left">
                Order No
            </th>
            <th>
                Recipient
            </th>
            <th>
                E-mail
            </th>
            <th style="display: none">
                Security and Setting
                <!--<img alt="" src="" class="helplink" />-->
            </th>
            <th style="display: none">
                Send E-mail Invite
            </th>
        </tr>
        <tr id="Recipient1" class="RecipientClass" runat="Server" name="Recipient1">
            <td>
                <asp:TextBox ID="txtOrder" placeholder="OrderNo" runat="server" Width="50px" class="order int"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="ReciepientName" placeholder="Name" runat="server" Width="300px"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="ReciepientEmail" placeholder="Email" CssClass="email" runat="server"
                    Width="300px"></asp:TextBox>
            </td>
            <td style="display: none">
                <select id="RecipientSecurity1" name="RecipientSecurity1" onchange="javascript:EnableDisableInput(1)">
                    <option value="None">None</option>
                    <option value="AccessCode">Access Code:</option>
                    <option value="PhoneAuthentication">Phone Authentication</option>
                    <option value="IDCheck">ID Check</option>
                </select><input id="RecipientSecuritySetting1" type="text" name="RecipientSecuritySetting1"
                    style="display: none;" />
            </td>
            <td style="display: none">
                <ul class="switcher" id="RecipientInvite1">
                    <li id="RecipientInviteon1" class="active"><a href="#" title="On">ON</a></li>
                    <li id="RecipientInviteoff1"><a href="#" title="OFF">OFF</a></li>
                    <input id="RecipientInviteToggle1" name="RecipientInviteToggle1" value="RecipientInviteToggle1"
                        type="checkbox" checked style="display: none;" />
                </ul>
            </td>
            <td>
            </td>
        </tr>
        <tr id="Tr1" runat="Server" class="RecipientClass" name="Recipient2">
            <td>
                <asp:TextBox ID="txtOrderNo1" placeholder="OrderNo" runat="server" Width="50px" class="order int"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="ReciepientName1" placeholder="Name" runat="server" Width="300px"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="ReciepientEmail1" placeholder="Email" CssClass="email" runat="server"
                    Width="300px"></asp:TextBox>
            </td>
            <td style="display: none">
                <select id="Select1" name="RecipientSecurity1" onchange="javascript:EnableDisableInput(1)">
                    <option value="None">None</option>
                    <option value="AccessCode">Access Code:</option>
                    <option value="PhoneAuthentication">Phone Authentication</option>
                    <option value="IDCheck">ID Check</option>
                </select><input id="Text1" type="text" name="RecipientSecuritySetting1" style="display: none;" />
            </td>
            <td style="display: none">
                <ul class="switcher" id="Ul1">
                    <li id="Li1" class="active"><a href="#" title="On">ON</a></li>
                    <li id="Li2"><a href="#" title="OFF">OFF</a></li>
                    <input id="Checkbox1" name="RecipientInviteToggle1" value="RecipientInviteToggle1"
                        type="checkbox" checked style="display: none;" />
                </ul>
            </td>
            <td>
                <a href="#" onclick="AddRow(this)">Add recipient</a>
            </td>
        </tr>
    </table>
    <asp:PlaceHolder ID="plc" runat="server"></asp:PlaceHolder>
    <table class="submit">
        <tr>
            <td class="fourcolumn">
            </td>
            <td class="fourcolumn">
                <asp:Button ID="SendDoc" OnClick="SendDoc_Click" Text="Send Document" runat="Server"
                    ClientIDMode="Static" OnClientClick="return ConstructRecipientDetails();" class="btn_validate submit" />
            </td>
            <td class="fourcolumn">
                <asp:Button ID="Button1" OnClick="Back_Click" Text="Back" runat="Server" ClientIDMode="Static" />
            </td>
            <td class="fourcolumn">
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        function ConstructRecipientDetails() {
            var Details = "";
            var OrderNo = "1";
            var isDublicate = true;
            var ismailId = true;
            var isName = true;
            var CountOrderNo = '';
            var CountOrderNoforMail = '';
            var isValidmailId = true;
            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            debugger;
            var EmptyCounter = 0;
            $('.RecipientClass').each(function () {
                OrderNo = $(this).find('td:eq(0)').find('input').val();
                var Name = $(this).find('td:eq(1)').find('input').val();
                var Email = $(this).find('td:eq(2)').find('input').val();
                debugger;
                if (Name == '' || Email == '') {
                    EmptyCounter++;
                }

                if (Name != '') {
                    if (Email != '') {
                        if (filter.test(Email) === false) {
                            isValidmailId = false
                        }
                        if (Details != '') {
                            var RecipientConstruct = $('#hdnRecipientDetails').val().split('#');
                            for (var i = 0; i < RecipientConstruct.length; i++) {
                                if (RecipientConstruct[i].split('$')[0] != "" || RecipientConstruct[i].split('$')[1] != "") {
                                    if (RecipientConstruct[i].split('$')[0] == Name) {
                                        MessageMasterDiv('Recipient name should not be same.', 1, 100);
                                        isDublicate = false;
                                    }
                                    if (RecipientConstruct[i].split('$')[1] == Email) {
                                        MessageMasterDiv('Recipient email should not be same.', 1, 100);
                                        isDublicate = false;
                                    }
                                }
                            }
                        }
                    } else {
                        CountOrderNoforMail = CountOrderNoforMail + OrderNo + ", ";
                        ismailId = false;
                    }
                } else {
                    if (Email != '') {
                        CountOrderNo = CountOrderNo + OrderNo + ", ";
                        isName = false;
                    }
                }

                Details = Details + '#' + Name + "$" + Email + "$" + OrderNo;
                $('#hdnRecipientDetails').val(Details);
            });

            CountOrderNo = CountOrderNo.slice(0, -2);
            CountOrderNoforMail = CountOrderNoforMail.slice(0, -2);

            if (ismailId == false) {
                MessageMasterDiv('Please add recipient email.', 1, 100);
                return false;
            }
            if (isName == false) {
                MessageMasterDiv('Please add recipient.', 1, 100);
                return false;
            }
            if ($('.RecipientClass').length == EmptyCounter) {
                MessageMasterDiv('Please add at least one recipient.', 1, 100);
                return false;
            }
            $('#hdnRecipientDetails').val(Details);
            if (isDublicate == false)
                return isDublicate

            if (isValidmailId == true)
                ShowProgress();

            return true;
        }
        $("#requestLink").addClass("menulink");
        $("#requesttab").addClass("selectedtab");
    </script>
</asp:Content>
