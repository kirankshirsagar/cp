﻿<%@ Page Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true" CodeFile="StageConfigurator.aspx.cs"
    Inherits="StageConfigurator" %>

<%@ Register Src="../UserControl/configuratorlinks.ascx" TagName="Masterlinks"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../JQueryValidations/jquery-ui-1.8.19.custom.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
 <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
<link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
<link href="../JQueryValidations/jquery-ui-1.8.19.custom.css" rel="stylesheet" type="text/css" />
<script src="../JQueryValidations/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="../scripts/jquery-ui-1.8.16.custom.css"/>
    <script language="javascript" type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");       
    </script>
    <style type="text/css">
    .tool_tip{background-color:transparent}
    </style>
    <script type="text/javascript">

        var entityMap = {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': '&quot;',
            "'": '&#39;',
            "/": '&#x2F;',
            "\\": '\\\\'
        };
        function escapeHtml(string, mode) {
            if (mode == 0) {
                return String(string).replace(/[&<>"'\/\\]/g, function (s) {
                    return entityMap[s];
                });
            }
            else if (mode == 1) {
                var escaped = "";
                var findReplace = [["&", /&amp;/g], ["<", /&lt;/g], [">", /&gt;/g], ["\"", /&quot;/g], ["'", /&#39;/g], ["/", /&#x2F;/g]];
                for (var item = 0; item < findReplace.length; item++) {
                    string = String(string).replace(findReplace[item][1], findReplace[item][0]);
                }
                return string;
            }
        }





        $(".Alphanumeric").live('keyup', function (e) {
            if (this.value.match(/[^a-zA-Z0-9 ]/g)) {
                this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');
            }
        });

        $('.NumericOnly').live('keypress', function (evt) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode == 8) {
                return true;
            }
            else if ((charCode != 46 || $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57)) {
                return false;
            }
        });


        $('.Currency').live('keypress', function (evt) {
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                return false;
            }
            if (($(this).val().indexOf('.') != -1) && ($(this).val().substring($(this).val().indexOf('.'), $(this).val().indexOf('.').length).length > 2)) {
                return false;
            }
        });

    </script>
    <script type="text/javascript" language="javascript">

        var globalFieldsItems;
        var globalOperatorItems;
        var globalDeptList;
        var globalUserList;
        var globalMasterFieldIds = new Array();
        var globalDateFieldIds = new Array();
        var globalDataType;
        var EditItems;

        function adddelete(obj) {

            var cls = $(obj).text().trim();
            var myFlag = 0;
            if (cls == "Add more") {
                myFlag = 1;
            }

            var flg = addRowValidation(myFlag);

            if (flg == true) {

                if (cls == "Add more") {

                    var allTrs = $(obj).closest('tr').closest('table').find('tr');
                    var lastTr = allTrs[allTrs.length - 1]; //last row for appending
                    var $clone = $('#tabledata tbody tr:first').clone();   //

                    if (!$(lastTr).find('a.icon').hasClass('icon-del')) {
                        $(lastTr).find("a[class='icon icon-add']").removeClass().addClass('icon icon-del').text('Delete');
                    }
                    else {
                        $(lastTr).find("a[class='icon icon-add']").remove();

                    }
                    $clone.find('input:text').val('');
                    $clone.show();
                    $(lastTr).closest('table').append($clone);
                    $clone.find('select[id^="FieldLibraryID"]').addClass("chzn-select required chzn-select").chosen();
                    $clone.find('select[id^="OperatorID"]').addClass("chzn-select required chzn-select").chosen();
                    $clone.find('select[id^="Operand"]').addClass("chzn-select required chzn-select").chosen();
                    $clone.find('select[id^="ForeignKeyValue"]').addClass('chzn-select required chzn-select').chosen();
                    $clone.find('select[id^="ForeignKeyValue"]').next('div').css('width', '344px');
                    $clone.find('select[id^="ForeignKeyValueS"]').addClass('chzn-select required chzn-select').chosen();
                    $clone.find('select[id^="ForeignKeyValueS"]').next('div').css('width', '344px');
                    $clone.find('select[id^="ForeignKeyValueS"]').next('div').next('div').remove();
      
                    var rowindex = $(lastTr).index();

                    if ($clone.find('.icon').html() == "Delete") {

                        $clone.find('.icon').removeClass().addClass('icon icon-add').text('Add more').show();

                        $clone.find('.icon').after('<a href="#" class="icon icon-del" id="addButton_0" clientidmode="Static" onclick="adddelete(this);">Delete</a>')

                    }
                    resetControlsIds();
                }
                else {

                    if ($(obj).closest("tr").prev('tr').index() == 0 && $(obj).closest("tr").next('tr').length == 0) {

                        return false;
                    }

                    if (!$(obj).prev('tr').find('a.icon').hasClass('icon-add') && !$(obj).closest("tr").next('tr').find('a.icon').hasClass('icon-del')) {
                        if ($(obj).closest("tr").prev('tr').index() == 1) {

                            $(obj).closest("tr").prev('tr').find('.icon').removeClass().addClass('icon icon-add').text('Add more').show();

                            $(obj).closest("tr").prev('tr').find('.icon').after('<a href="#" class="icon icon-del" id="addButton_0" clientidmode="Static" onclick="adddelete(this);">Delete</a>')

                        }
                        else {
                            $(obj).closest("tr").prev('tr').find('.icon').before('<a href="#" class="icon icon-add" id="addButton_0" clientidmode="Static" onclick="adddelete(this);">Add more</a>')
                        }
                    }

                    if ($(obj).closest("tr").prev('tr').index() > 0 || $(obj).closest("tr").next('tr').length >= 1) {

                        if (($(obj).closest("tr").next('tr').length > 1 && $(obj).closest("tr").prev('tr').length > 1) || $(obj).closest("tr").prev('tr').index() == 0) {
                            $(obj).closest("tr").next('tr').find('td:eq(0)').find('select').next('div').css('display', 'none');
                        }

                        $(obj).closest("tr").remove();
                    }

                    resetControlsIds();

                }
            }
        }

        $(function () {
            $('.req').keyup(function () {
                $(this).next('.tooltip_outer').hide();
            });


            $('.btnvalidate').click(function (e) {
                var empty_count = 0;
                $('.req').next('.tooltip_outer').hide();

                $('.req').each(function () {

                    if ($(this).val().length === 0) {
                        $(this).after("<div class='tooltip_outer'  style='filter:alpha(opacity=50); opacity:0.9;'><div class='tool_tip' style='filter:alpha(opacity=50); opacity:0.5;'>This field is required.</div></div>").show("slow");
                        empty_count = 1;
                    }
                    else {
                        $(this).next('.tooltip_outer').hide();
                    }
                    var isUserEmpty = 0;
                    //  alert($('#ddlUsers').find('option').length);
                    if ($('#ddlUsers').find('option').length == 0) {

                        $('#ddlUsers').next('div').next('.tooltip_outer').hide();
                        $('#ddlUsers').next('div').after("<div class='tooltip_outer'  style='filter:alpha(opacity=50); opacity:0.9;'><div class='tool_tip' style='filter:alpha(opacity=50); opacity:0.5;'>This field is required.</div></div>").show("slow");
                        isUserEmpty = 1;
                    }

                    if (empty_count > 0 || isUserEmpty > 0) {
                        e.preventDefault();
                    }
                    else {
                        $('.tooltip_outer').hide();
                    }
                });


            });


            $('#btnSave').click(function () {
                var isValid = true;

                var flg = addRowValidation(1);

                if (flg == false) {
                    return false;
                }
                if ($('#txtStageName').val() == "") {
                    flg = false;
                }
                if (flg == true) {
                    var rows = [];
                    var j = 0;
                    var n = 0;

                    $('#tabledata tbody tr').each(function (index, value) {
                        var multival = "";

                        if (index > 0) {
                            var $row = $(value);
                            var operand;
                            if ($row.index() > 1) {


                                operand = $row.find('select.Operand').val()
                            }
                            else {
                                operand = "";
                            }


                            if ($row.find('select[id^="ForeignKeyValueS_"]').next('div').css('display') == 'inline-block') {
                                multival = ($row.find("input[id^='hdnmultival_']").val());
                            }
                            else {
                                multival = ($row.find("input[id^='hdnmultivalS_']").val());
                            }

                            rows.push({
                                ANDOR: operand,
                                FieldLibraryID: $row.find('select.FieldLibraryID').val(),
                                OperatorID: ($row.find('select.OperatorID').val()),
                                txtBox1: escapeHtml(($row.find("input[id^='txtBox1']").val()), 0),
                                txtBox2: escapeHtml(($row.find("input[id^='txtBox2']").val()), 0),
                                ForeignKeyValue: multival,

                                dtpick1: ($row.find("input[id^='dtpick1']").val()),
                                dtpick2: ($row.find("input[id^='dtpick2']").val()),
                                DataType: getDataType($row.find('select.FieldLibraryID').val())
                            });
                        }
                    });

                    var jsonData = JSON.stringify(rows);
                    var ContractTemplateId = $('#hdnTemplateId').val();
                    var lblContractType = $('#HdnCotractTypeId').val();
                    var lblLevel = $('#HdnLevelId').val();
                    var txtStageName = encodeURIComponent($('#txtStageName').val());
                    var ddldept = $('#ddldept').val();
                    var ddlUsers = $('#ddlUsers').val();
                    var AddedBy = $('#hdnAddedBy').val();
                    var StageId = 0;
                    if ($('#HdnStageId').val() != "") {
                        StageId = $('#HdnStageId').val();
                    }
                    //alert(txtStageName);
                    $.ajax({
                        type: 'POST',
                        contentType: "text/html; charset=utf-8",
                        url: "../Handlers/StageConfiguratorHandler.ashx?lblContractType=" + lblContractType + "&lblLevel=" + lblLevel + "&txtStageName=" + txtStageName + "&ddldept=" + ddldept + "&ddlUsers=" + ddlUsers + "&StageId=" + StageId + "&ContractTemplateId=" + ContractTemplateId + "&AddedBy=" + AddedBy,
                        data: jsonData,
                        datatype: "html",
                        async: false,
                        success: function (response) {

                            if (response != '2') {
                                isValid = true;
                            }
                            else {

                                alert('Stage name already exists.');

                                isValid = false;

                            }
                            // window.location.href = 'ContractType.aspx';

                        },
                        error: function () {
                            alert("some problem in saving data");
                            return false;
                        }
                    });
                }


                return isValid; //Added by nilesh for stage name validation

            });


        });
    </script>
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <h2>
        Stage configurator</h2>
    <div class="issue status-1 priority-3 child created-by-me details">
        <div class="subject">
            <div>
                <p>
                    <asp:LinkButton ID="lnkStageBack" OnClick="lnkStageBack_Click" CssClass="issue status-1 priority-4 parent"
                        ClientIDMode="Static" runat="server">Approval stage configurator</asp:LinkButton>
                    </a>: Add Stage</p>
            </div>
        </div>
        <br />
        <table width="100%" id="tblotherdetails" cellpadding="2">
            <tr>
                <td>
                    <input id="hdnTemplateName" type="hidden" runat="server" clientidmode="Static" />
                    <input id="hdnTemplateId" type="hidden" runat="server" clientidmode="Static" />
                    <input id="HdnCotractType" type="hidden" runat="server" clientidmode="Static" />
                    <input id="HdnFieldArray" type="hidden" runat="server" clientidmode="Static" />
                    <input id="HdnCotractTypeId" type="hidden" runat="server" clientidmode="Static" />
                    <input id="HdnLevelId" type="hidden" runat="server" clientidmode="Static" />
                    <input id="HdnStageId" type="hidden" runat="server" clientidmode="Static" />
                    <input id="HdnCotractTemplateType" type="hidden" runat="server" clientidmode="Static" />
                    <input id="hdnAddedBy" type="hidden" runat="server" clientidmode="Static" />
                </td>
            </tr>
            <tr>
                <td width="10%">
                    <b>Contract Type</b>
                </td>
                <td width="1%">
                    :
                </td>
                <td width="89%">
                    <asp:Label ID="lblConType" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                </td>
            </tr>
            <tr>
                <td width="10%">
                    <b>Contract Template</b>
                </td>
                <td width="1%">
                    :
                </td>
                <td width="89%">
                    <asp:Label ID="lblTemplate" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                </td>
            </tr>
            <tr>
                <td width="10%">
                    <b>Level</b>
                </td>
                <td width="1%">
                    :
                </td>
                <td width="89%">
                    Level-
                    <asp:Label ID="lbllevel" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                </td>
            </tr>
            <tr>
                <td width="10%">
                <span style="margin-left:-9px" class="required"> *</span><b>Stage Name</b>    
                  
                </td>
                <td width="1%">
                    :
                </td>
                <td width="89%">
                    <input id="txtStageName" style="width: 300px" class="req" type="text" clientidmode="Static" />
                </td>
            </tr>
            <tr>
                <td width="10%">
                    <b>Assign to department</b>
                </td>
                <td width="1%">
                    :
                </td>
                <td width="89%">
                    <select id="ddldept" name="ddldept" style="width: 300px" class="chzn-select required chzn-select"
                        onchange="return fillusers()" clientidmode="Static">
                    </select>
                </td>
            </tr>
            <tr>
                <td width="10%">
                    <b>Assign to user</b>
                </td>
                <td width="1%">
                    :
                </td>
                <td width="89%">
                    <select id="ddlUsers" name="ddlUsers" style="width: 300px" class="chzn-select req chzn-select"
                        clientidmode="Static">
                    </select>

                </td>
            </tr>
        </table>
    </div>
    <br />
    <table width="100%" id="tabledata" class="configlist" clientidmode="Static">
        <thead>
            <tr>
                <th style="text-align: left; width: 10%;">
                    <b>Add/Or</b>
                </th>
                <th style="text-align: left; width: 22%;">
                    <b>Fields</b>
                </th>
                <th style="text-align: left; width: 20%;">
                    <b>Operator</b>
                </th>
                <th style="text-align: left; width: 34%;">
                    <b>Value</b>
                </th>
                <th style="text-align: center; width: 10%;">
                    &nbsp;
                </th>
                <th style="text-align: center; width: 10%;">
                    &nbsp;
                </th>
            </tr>
        </thead>
        <tbody>
            <tr class="odd" style="display: none;">
                <td>
                    <select id="Operand_0" name="Operand" class="Operand" style="width: 65px" clientidmode="Static"
                        runat="server">
                        <option>AND</option>
                        <option>OR</option>
                    </select>
                </td>
                <td>
                    <select id="FieldLibraryID_0" style="width:220px" onchange="return checkValidOperators(this);" name="FieldLibraryID"
                        class="FieldLibraryID" clientidmode="Static" runat="server">
                        <option></option>
                    </select>
                </td>
                <td >
                    <select id="OperatorID_0" style="width:200px" name="OperatorID" class="OperatorID" onchange="return HideShowOp(this);"
                        clientidmode="Static">
                    </select>
                </td>
                <td>
                    <div id="DivDDL_0" class="DivDDL">
                        <select id="ForeignKeyValue_0" onchange="getMultipleValues(this)" name="ForeignKeyValue" multiple="multiple"
                       clientidmode="Static" style="width: 95%">
                        </select>
                        <input id="hdnmultival_0" type="hidden" runat="server" clientidmode="Static" />
                    </div>
                       <div id="DivDDLS_0" class="DivDDLS">
                        <select id="ForeignKeyValueS_0" class='chzn-select required chzn-select' onchange="getMultipleValuesS(this)" name="ForeignKeyValueS"
                       clientidmode="Static" style="width: 95%">
                        </select>
                        <input id="hdnmultivalS_0" type="hidden" runat="server" clientidmode="Static" />
                    </div>
                    <div id="TextDiv_0" class="TextDiv">
                        <input type="text" id="txtBox1_0" name="txtBox1" clientidmode="Static" />
                        <input type="text" id="txtBox2_0" name="txtBox2" clientidmode="Static" />
                    </div>
                    <div id="DateDiv_0" class="DateDiv">
                        <input type="text" id="dtpick1_0" readonly="readonly" name="dtpick1" class="required datepicker"
                            clientidmode="Static" />
                        <input type="text" id="dtpick2_0" readonly="readonly" name="dtpick2" class="required datepicker"
                            clientidmode="Static" /></div>
                </td>
                <td style="width: 15%">
                    <a href="#" class="icon icon-add" id="addButton_0" clientidmode="Static" onclick="adddelete(this);">
                        Add more</a>
                </td>
                <td>
                    <input id="hdnDataType_0" type="hidden" clientidmode="Static" />
                </td>
            </tr>
            <tr class="odd">
                <td>
                    <select id="Operand_1" name="Operand" class="Operand chzn-select required chzn-select"
                        clientidmode="Static" runat="server">
                        <option selected="selected">AND</option>
                        <option>OR</option>
                    </select>
                </td>
                <td>
                    <select id="FieldLibraryID_1" style="width:220px" onchange="return checkValidOperators(this);" name="FieldLibraryID"
                        class="FieldLibraryID chzn-select required chzn-select" clientidmode="Static"
                        runat="server">
                        <option></option>
                    </select>
                </td>
                <td>
                    <select id="OperatorID_1" style="width:200px" name="OperatorID" class="OperatorID chzn-select required chzn-select"
                        onchange="return HideShowOp(this);" clientidmode="Static">
                    </select>

                </td>
                <td>
                    <div id="DivDDL_1" class="DivDDL">
                        <select id="ForeignKeyValue_1" onchange="getMultipleValues(this)"  multiple="multiple"
                            name="ForeignKeyValue" clientidmode="Static" style="width: 95%" class='chzn-select required chzn-select'>
                        </select>
                        <input id="hdnmultival_1" type="hidden" runat="server" clientidmode="Static" />
                    </div>

                     <div id="DivDDLS_1" class="DivDDLS">
                        <select id="ForeignKeyValueS_1" class='chzn-select required chzn-select' onchange="getMultipleValuesS(this)"  name="ForeignKeyValueS"
                       clientidmode="Static" style="width: 95%">

                        </select>
                         <input id="hdnmultivalS_1" type="hidden" runat="server" clientidmode="Static" />
                    </div>
                    <div id="TextDiv_1" class="TextDiv">
                        <input type="text" id="txtBox1_1" name="txtBox1" clientidmode="Static" />
                        <input type="text" id="txtBox2_1" name="txtBox2" clientidmode="Static" />
                    </div>
                    <div id="DateDiv_1" class="DateDiv">
                        <input type="text" id="dtpick1_1" readonly="readonly" name="dtpick1" class="required datepicker"
                            clientidmode="Static" />
                        <input type="text" id="dtpick2_1" readonly="readonly" name="dtpick2" class="required datepicker"
                            clientidmode="Static" /></div>
                </td>
                <td style="width: 15%">
                    <a href="#" class="icon icon-add" id="addButton_1" clientidmode="Static" onclick="adddelete(this);">
                        Add more</a>
                </td>
                <td>
                    <input id="hdnDataType_1" type="hidden" clientidmode="Static" />
                </td>
            </tr>
        </tbody>
    </table>
    <asp:Button ID="btnSave" runat="server" Text="Save" ClientIDMode="Static" class="btnvalidate"
        OnClick="btnSave_Click" />
    <asp:Button ID="btnBack" runat="server" Text="Back" ClientIDMode="Static"
        OnClick="btnBack_Click" />
    
   
    <script type="text/javascript">

        function addRowValidation(flg) {

            var rValue = true;


            if (flg > 0) {

                $('#tabledata tbody tr').each(function (index, value) {

                    if (index > 0) {

                        $(value).find('td').eq('3').find('div').each(function () {

                            var divType = $(this).attr('class');  //$(this).attr('id').split('_')[0];
                            var rNo;

                            try {
                                rNo = $(this).attr('id').split('_')[1];
                            } catch (e) {
                                rNo = 0;
                            }


                            /* text */
                            if (divType == 'TextDiv' && rNo > 0 && $(this).css('display') != 'none') {
                                var txt1 = $(this).find("input[id^='txtBox1']");
                                var txt2 = $(this).find("input[id^='txtBox2']");

                                if (txt2.css('display') == 'none') {
                                    if (txt1.val().trim() == '' && rValue == true) {

                                        alert('Select value');
                                        txt1.focus();
                                        txt1.val('');
                                        rValue = false;
                                    }
                                }

                                else {
                                    if (txt1.val().trim() == '' && rValue == true) {

                                        alert('Select value');
                                        txt1.focus();
                                        txt1.val('');
                                        rValue = false;
                                    }
                                    if (txt2.val().trim() == '' && rValue == true) {

                                        alert('Select value');
                                        txt2.focus();
                                        txt2.val('');
                                        rValue = false;
                                    }

                                    if (rValue == true) {
                                        if (parseFloat(txt2.val().trim()) < parseFloat(txt1.val().trim())) {
                                            alert("To value must be greater than or equal to from value.");
                                            txt2.focus();
                                            txt2.val('');
                                            rValue = false;
                                        }
                                    } //else
                                } //div eq
                            }

                            /* date */
                            if (divType == 'DateDiv' && rNo > 0 && $(this).css('display') != 'none' && rValue == true) {

                                var txt1 = $(this).find("input[id^='dtpick1']");
                                var txt2 = $(this).find("input[id^='dtpick2']");

                                if (txt2.css('display') == 'none') {
                                    if (txt1.val().trim() == '' && rValue == true) {

                                        alert('Select value');
                                        txt1.focus();
                                        txt1.val('');
                                        rValue = false;
                                    }
                                }

                                else {

                                    if (txt1.val().trim() == '' && rValue == true) {

                                        alert('Select value');
                                        txt1.focus();
                                        txt1.val('');
                                        rValue = false;
                                    }
                                    if (txt2.val().trim() == '' && rValue == true) {

                                        alert('Select value');
                                        txt2.focus();
                                        txt2.val('');
                                        rValue = false;
                                    }

                                    if (rValue == true) {

                                        var d1 = new Date(myDateFormat(txt1.val()));
                                        var d2 = new Date(myDateFormat(txt2.val()));

                                        if (d1 > d2) {
                                            alert("To date must be greater than or equal to from date.");
                                            rValue = false;
                                            txt2.val('').focus();
                                        }
                                    }
                                }
                            }


                            // master

                            if (divType == 'DivDDL' && rNo > 0 && $(this).css('display') != 'none' && rValue == true) {

                                var ctrl = $(this).find('select[id^="ForeignKeyValue_"]');
                                // alert($(ctrl).next('div').find('ul li:eq(0).search-choice').find('span').length);
                      
                                if ((ctrl.val() == null || ctrl.val() == '') && $(ctrl).next('div').find('ul li:eq(0).search-choice').find('span').length <= 0) {

                                    alert('Select value');
                                    ctrl.focus();
                                    rValue = false;
                                }
                            }



                            if (divType == 'DivDDLS' && rNo > 0 && $(this).css('display') != 'none' && rValue == true) {

                                var ctrl = $(this).find('select[id^="ForeignKeyValueS_"]');
                             
                                if ((ctrl.val() == null || ctrl.val() == '0' || ctrl.val() == '') && $(ctrl).next('div').find('ul li:eq(0).search-choice').find('span').length <= 0) {

                                    alert('Select value');
                                    ctrl.focus();
                                    rValue = false;
                                }
                            }




                        });
                    }
                });
            }

            return rValue;
        }
        function SetDefaultOptions() 
        {
            $('#ForeignKeyValue_1').val('');
            $('#ForeignKeyValueS_1').val('');
        }

        $(window).load(function () {

            $('#tabledata tbody tr:eq(1)').find('td').eq(0).find('select').hide();
            $('#tabledata tbody tr:eq(1)').find('td').eq(0).find('div').hide();
            $('#Operand_1_chzn').hide();
            var Counter = 0;
            $('#tabledata tbody tr').each(function () {

                if (Counter < $('#tabledata tbody tr').length - 1) {
                    $(this).find("a[class='icon icon-add']").removeClass().addClass('icon icon-del').text('Delete');
                }
                else {

                    $(this).find("a[class='icon icon-del']").removeClass().addClass('icon icon-add').text('Add more');
                    $(this).find('.icon').after('<a href="#" class="icon icon-del" id="addButton_0" clientidmode="Static" onclick="adddelete(this);">Delete</a>')
                }
                Counter++;

            });
            if ($('#tabledata tbody tr').length == 2) {
                $('#tabledata tbody tr:eq(1)').find('a.icon-add').show().text('Add more');

            }

            $('#tabledata tbody tr:eq(1)').find('a.icon-del').show();
            if ($('#tabledata tbody tr').length == 2) {

                SetDefaultOptions();

            }
        });
       
               
    </script>
    <script type="text/javascript">

        DefaultSetUps();

        if ($('#HdnStageId').val() != "" && $('#HdnStageId').val() > 0) {

            var type = 'Edit';
            var strType = "POST";
            var strContentType = "text/html; charset=utf-8";
            var strData = "{}";
            var strURL = '../Handlers/GetStageConfiguratorDropDownHandler.ashx?Type=' + type + '&StageId=' + $('#HdnStageId').val() + '&ContractTemplateID=' + $('#hdnTemplateId').val();
            var strCatch = false;
            var strDataType = 'json';
            var strAsync = false;
            var objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
            EditItems = objHandler.HandlerReturnedData;
            for (var j = 0; j < EditItems.length - 1; j++) {

                var allTrs = $('#tabledata').find('tr');
                var lastTr = allTrs[allTrs.length - 1]; //last row for appending
                var $clone = $('#tabledata tbody tr:first').clone();   //


                $(lastTr).find(".icon icon-add").removeClass().addClass('icon icon-del').attr('value', 'Delete');
                $clone.find('input:text').val('');
                $clone.show();
                $('#tabledata').append($clone);
                $clone.find('select[id^="FieldLibraryID"]').addClass("chzn-select required chzn-select").chosen();
                $clone.find('select[id^="OperatorID"]').addClass("chzn-select required chzn-select").chosen();
                $clone.find('select[id^="Operand"]').addClass("chzn-select required chzn-select").chosen();
                $clone.find('select[id^="ForeignKeyValue"]').addClass("chzn-select required chzn-select").chosen();
                 resetControlsIds();

            }

            ReadData();


        }


        function resetControlsIds() {

            var rowId = 0;

            $('#tabledata').find('tr').each(function (item1, value1) {

                if ($(value1).index() > 0) {
                    rowId = rowId + 1;

                    var FieldLibraryID = $(this).find('select[id^="FieldLibraryID"]');
                    var OperatorID = $(this).find('select[id^="OperatorID"]');
                    var Operand = $(this).find('select[id^="Operand"]');
                    var hdnDataType = $(this).find('input[id^="hdnDataType"]');
                    var hdnmultival = $(this).find('input[id^="hdnmultival"]');
                    var hdnmultivalS = $(this).find('input[id^="hdnmultivalS"]');
                    var addButton = $(this).find('a[id^="addButton"]');

                    var searchEles = $(this).find('.TextDiv').children();
                    var searchDate = $(this).find('.DateDiv').children();
                    var searchDDL = $(this).find('.DivDDL').children();
                    var searchDDLS = $(this).find('.DivDDLS').children();

                    searchEles[0].id = (searchEles[0].id.split('_'))[0] + '_' + rowId;
                    searchEles[1].id = (searchEles[1].id.split('_'))[0] + '_' + rowId;

                    searchDate[0].id = (searchDate[0].id.split('_'))[0] + '_' + rowId;
                    searchDate[1].id = (searchDate[1].id.split('_'))[0] + '_' + rowId;

                    searchDDL[0].id = (searchDDL[0].id.split('_'))[0] + '_' + rowId;
                    searchDDL[1].id = (searchDDL[1].id.split('_'))[0] + '_' + rowId;

                    searchDDLS[0].id = (searchDDLS[0].id.split('_'))[0] + '_' + rowId;
                    searchDDLS[1].id = (searchDDLS[1].id.split('_'))[0] + '_' + rowId;


                    ($(this).find('.TextDiv')).attr('id', 'TextDiv_' + rowId);
                    ($(this).find('.DateDiv')).attr('id', 'DateDiv_' + rowId);
                    ($(this).find('.DivDDL')).attr('id', 'DivDDL_' + rowId);
                    ($(this).find('.DivDDLS')).attr('id', 'DivDDLS_' + rowId);

                    FieldLibraryID.attr('id', 'FieldLibraryID_' + rowId);
                    OperatorID.attr('id', 'OperatorID_' + rowId);
                    Operand.attr('id', 'Operand_' + rowId);
                    hdnDataType.attr('id', 'hdnDataType_' + rowId);
                    hdnmultival.attr('id', 'hdnmultival_' + rowId);
                    hdnmultivalS.attr('id', 'hdnmultivalS_' + rowId);
                    addButton.attr('id', 'addButton_' + rowId);
                }
            });
        }





        function ReadData() {

            $('#tabledata tbody tr').each(function (index, value) {
                var $row = $(value);

                var arr = EditItems;
                $.map(arr, function (value, index) {

                    if (index + 1 == $row.index()) {
                
                        $row.find('select.Operand').val(value.ANDOR);
                        $row.find('select.Operand').trigger("liszt:updated");
                        $row.find('select.FieldLibraryID').val(value.FieldLibraryID);
                        updateSettings($row.find('select.FieldLibraryID'));
                        $row.find('select.FieldLibraryID').change();
                        $row.find('select.OperatorID').val(value.OperatorID);
                        updateSettings($row.find('select.OperatorID'));
                        $row.find('select.OperatorID').change();
                        ($row.find("input[id^='txtBox1']").val(value.txtBox1));
                        ($row.find("input[id^='txtBox2']").val(value.txtBox2));
                        ($row.find("input[id^='hdnmultival_']").val(value.ForeignKeyValue));
                        ($row.find("input[id^='hdnmultivalS_']").val(value.ForeignKeyValue));
                        ($row.find("input[id^='dtpick1']").val(value.dtpick1));
                        ($row.find("input[id^='dtpick2']").val(value.dtpick2));
                        ($row.find("input[id^='hdnDataType_']").val(value.DataType));
                        $('#ddldept').val(value.DepartmentId);
                        $('#ddldept').change();
                        $('#ddlUsers').val(value.UsersId);
                        $('#txtStageName').val(value.StageName);


                        $('#btnSave').val("Update");
                    }
                });
            });
            setmultiples();
            if ($('#txtStageName').val() == "") {

                SetDefaultOptions();

            }

        }



        function setmultiples() {

            $('#tabledata tbody tr').each(function (index, value) {
                var $row = $(value);
                var DivDDL = $row.find('div[id^="DivDDL"]');
                var DivDDLS = $row.find('div[id^="DivDDLS"]');
                var TextDiv = $row.find('div[id^="TextDiv"]');
                var DateDiv = $row.find('div[id^="DateDiv"]');
                if (DivDDL.css('display') != 'none') {
                    var abc = new Array();
                    var hiddenValue = $row.find("input[id^='hdnmultival_']").val();
                    if (hiddenValue != '') {
                        abc = (hiddenValue).split(',');
                        $row.find('.DivDDLS').hide();
                        $row.find('.DivDDL').show();
                        $row.find('.TextDiv').hide();
                        $row.find('.DateDiv').hide();
                        $row.find('.DivDDL').find('select').attr('multiple', 'true');
                        $row.find('.DivDDL').find('select').val(abc);
                        updateSettings($row.find('.DivDDL').find('select'));
                    }
                }

                if (DivDDLS.css('display') != 'none') {
                    var abc = new Array();
                    var hiddenValueS = $row.find("input[id^='hdnmultivalS_']").val();
                    abc = (hiddenValueS).split(',');
                    $row.find('.DivDDLS').find('select').removeAttr('multiple');
                    $row.find('.DivDDLS').show();
                    $row.find('.DivDDL').hide();
                    $row.find('.TextDiv').hide();
                    $row.find('.DateDiv').hide();
                    $row.find('.DivDDLS').find('select').val(abc);
                    updateSettings($row.find('.DivDDLS').find('select'));
           
                }
            });
        }


        function DefaultSetUps() {
            // alert();
            var TextDiv = $('#tabledata').find('div[id^="TextDiv"]');
            var DateDiv = $('#tabledata').find('div[id^="DateDiv"]');
            var DivDDL = $('#tabledata').find('div[id^="DivDDL"]');
            var DivDDLS = $('#tabledata').find('div[id^="DivDDLS"]');
            $(TextDiv).hide();
            $(DateDiv).hide();

            var type = 'fields';
            var strType = "POST";
            var strContentType = "text/html; charset=utf-8";
            var strData = "{}";
            var strURL = '../Handlers/GetStageConfiguratorDropDownHandler.ashx?Type=' + type + '&ContractTemplateID=' + $('#hdnTemplateId').val();
            var strCatch = false;
            var strDataType = 'json';
            var strAsync = false;
            var objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
            globalFieldsItems = objHandler.HandlerReturnedData;

            OnlyMastersInformationFill();
            OnlyDateInformationFill();

            type = 'operator';
            strURL = '../Handlers/GetStageConfiguratorDropDownHandler.ashx?Type=' + type + '&ContractTemplateID=' + $('#hdnTemplateId').val();
            objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
            globalOperatorItems = objHandler.HandlerReturnedData;

            $('#tabledata tbody tr').each(function (index, value) {

                fieldBind(index);
            });
            type = 'dept';

            strURL = '../Handlers/GetStageConfiguratorDropDownHandler.ashx?Type=' + type + '&ContractTemplateID=' + $('#hdnTemplateId').val();
            objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
            globalDeptList = objHandler.HandlerReturnedData;
            bindSelectCustom(globalDeptList, '#ddldept', 'DepartmentId', 'DepartmentName');
            $('#ddldept').change();
        }

        function OnlyMastersInformationFill() {

            var position = 0;
            try {
                $(globalFieldsItems).each(function (i, val) {
                    if (val.PicklistObject != '' && val.PicklistObject != null && val.PicklistObject != undefined) {
                        globalMasterFieldIds[position] = new Array();
                        globalMasterFieldIds[position]["FieldLibraryID"] = val.FieldLibraryID;
                        globalMasterFieldIds[position]["PicklistObject"] = val.PicklistObject;
                        globalMasterFieldIds[position]["FieldTypeID"] = val.FieldTypeID;
                        position = position + 1;
                    }
                });
            } catch (e) {
                alert(e);
            }
        }


        function getDataType(FieldLibraryID) {
            var rValue = 0;
            $(globalFieldsItems).each(function (i, val) {

                if (parseInt(FieldLibraryID) == val.FieldLibraryID) {
                    rValue = parseInt((val.FID).split('_FTId_')[1].toString());
                }
            });
            return rValue;
        }



        function OnlyDateInformationFill() {
            var position = 0;

            try {
                $(globalFieldsItems).each(function (i, val) {
                    if ((val.FID).split('_FTId_')[1] == '7') {
                        globalDateFieldIds[position] = new Array();
                        globalDateFieldIds[position]["FieldLibraryID"] = val.FieldLibraryID;
                        globalDateFieldIds[position]["FieldTypeID"] = val.FieldTypeID;
                        position = position + 1;
                    }
                });
            } catch (e) {
                alert(e);
            }
        }



        function fieldBind(index) {
            try {
                bindSelectCustom(globalFieldsItems, '#FieldLibraryID_' + index, 'FieldLibraryID', 'FieldName');
                $('#FieldLibraryID_' + index).change();
                updateSettings($('#FieldLibraryID_' + index));
                $('#OperatorID_' + index).change();
                updateSettings($('#OperatorID_' + index));

            } catch (e) {
                alert(e);
            }
        }


        function fillusers() {
            var type;
            var strType = "POST";
            var strContentType = "text/html; charset=utf-8";
            var strData = "{}";
            var strCatch = false;
            var strDataType = 'json';
            var strAsync = false;

            type = 'user';
            parentId = $('#ddldept');
            var strURL = '../Handlers/GetStageConfiguratorDropDownHandler.ashx?Type=' + type + '&ContractTemplateID=' + $('#hdnTemplateId').val();
            objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
            globalUserList = objHandler.HandlerReturnedData;
            var userArray = new Array();

            var position = 0;
            $(globalUserList).each(function (i, val) {
                if (parseInt(parentId.val()) == parseInt(val.DepartmentId)) {
                    userArray[position] = new Array();
                    userArray[position]["UsersId"] = val.UsersId;
                    userArray[position]["UserName"] = val.UserName;
                    position = position + 1;
                }
            });

            bindSelectCustom(userArray, '#ddlUsers', 'UsersId', 'UserName');
            if ($('#ddlUsers').find('option').length > 0) {
                $('#ddlUsers').next('div').next('.tooltip_outer').hide();
            }

        }


        function checkValidOperators(obj) {

            var MasterId = '';
            var parentId = parseInt($(obj).val());

            $(globalMasterFieldIds).each(function (i, val) {
                if (parentId == parseInt(val.FieldLibraryID)) {
                    MasterId = val.PicklistObject;
                }
            });
            if (MasterId != '') {
                fillMastersData(MasterId, obj);
            }
            fillOperators(parentId, obj);
            HideShowOp(obj);
            return true;
        }

        function fillOperators(parentId, obj) {

            try {
                var index = $(obj).closest('tr').index();
                var mySelect = '#OperatorID_' + index;
                var oppArray = new Array();
                var position = 0;

                $(globalOperatorItems).each(function (i, val) {

                    if (parentId == parseInt(val.FieldLibraryID)) {
                        oppArray[position] = new Array();
                        oppArray[position]["OperatorID"] = val.OperatorID;
                        oppArray[position]["OperatorName"] = val.OperatorName;
                        position = position + 1;
                    }
                });

                bindSelectCustom(oppArray, '#OperatorID_' + index, 'OperatorID', 'OperatorName');

            }
            catch (err) {

            }

        }


        function fillMastersData(MasterId, obj) {
            var strType = "POST";
            var strContentType = "text/html; charset=utf-8";
            var strData = "{}";
            var strCatch = false;
            var strDataType = 'json';
            var strAsync = false;
            var MasterList;
            var type = 'FillMaster';
            var index = $(obj).closest('tr').index();
            var parentId = 0;
            var strURL = '../Handlers/GetStageConfiguratorDropDownHandler.ashx?Type=' + type + "&MasterName=" + MasterId + "&parentId=" + parentId + '&ContractTemplateID=' + $('#hdnTemplateId').val();
            objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
            MasterList = objHandler.HandlerReturnedData;
            bindSelectCustom(MasterList, '#ForeignKeyValue_' + index, 'Id', 'Name');
            bindSelectCustomSingleSelect(MasterList, '#ForeignKeyValueS_' + index, 'Id', 'Name');
        }




        function checkType(checkValue) {

            var rvalue = '';
            try {
                $(globalDateFieldIds).each(function (i, val) {
                    if (checkValue == val.FieldLibraryID.toString()) {
                        rvalue = 'D';
                    }
                });

                $(globalMasterFieldIds).each(function (i, val) {
                    if (checkValue == val.FieldLibraryID.toString()) {
                        rvalue = 'M';
                    }
                });
            }
            catch (e) {
                rvalue = -1;
            }
            return rvalue;
        }

        function checkMasterType(checkValue) {
            var rvalue = '';
            try {
                $(globalMasterFieldIds).each(function (i, val) {
                    if (checkValue == val.FieldLibraryID.toString()) {
                        if (val.FieldTypeID.toString() == "10" || val.FieldTypeID.toString() == "11") {
                            rvalue = 'M';
                        }
                        else
                            rvalue = 'S';
                    }
                });
            }
            catch (e) {
                rvalue = -1;
            }
            return rvalue;
        }


        function HideShowOp(obj) {

            var FieldLibraryID = $(obj).closest('tr').find('select[id^="FieldLibraryID"]');
            var op = $(obj).closest('tr').find('select[id^="OperatorID"]').find('option:selected').val();
            var flg = '';
            var TextDiv = $(obj).closest('tr').find('.TextDiv');
            var DateDiv = $(obj).closest('tr').find('.DateDiv');
            var DivDDL = $(obj).closest('tr').find('.DivDDL');
            var DivDDLS = $(obj).closest('tr').find('.DivDDLS');
            var type = checkType(FieldLibraryID.val());
            var mastertype = checkMasterType(FieldLibraryID.val());

            $(DateDiv).hide();
            $(TextDiv).hide();
            $(DivDDL).hide();
            $(DivDDLS).hide();

            $(TextDiv).find("input[id^='txtBox1']").val('');
            $(TextDiv).find("input[id^='txtBox2']").val('');
            $(DateDiv).find("input[id^='dtpick1']").val('');
            $(DateDiv).find("input[id^='dtpick2']").val('');


            if (type == 'D' && op != "9" && op != "10") {
                dateVisiblity(DateDiv, TextDiv, DivDDL, op, DivDDLS);
            }
            else if (type == 'M' && op != "9" && op != "10") {

                masterVisiblity(DateDiv, TextDiv, DivDDL, obj, mastertype, DivDDLS);
            }

            else if (op != "9" && op != "10") {
                textVisibility(DateDiv, TextDiv, DivDDL, op, obj, DivDDLS);
            }
        }


        function textVisibility(DateDiv, TextDiv, DivDDL, op, obj, DivDDLS) {
            $(DateDiv).hide();
            $(TextDiv).show();
            $(DivDDL).hide();
            $(DivDDLS).hide();
            var FieldLibraryID = $(obj).closest('tr').find('select[id^="FieldLibraryID"]');
            var dataType = getDataType(FieldLibraryID.val());

            if (op == '11') {
                $(TextDiv).find("input[id^='txtBox2']").show();
            }
            else {
                $(TextDiv).find("input[id^='txtBox2']").hide();
            }

            if (dataType == '4') {
                $(TextDiv).find("input[id^='txtBox1']").removeClass().addClass("NumericOnly");
                $(TextDiv).find("input[id^='txtBox2']").removeClass().addClass("NumericOnly");
            }

            else if (dataType == '5') {
                $(TextDiv).find("input[id^='txtBox1']").removeClass().addClass("Currency");
                $(TextDiv).find("input[id^='txtBox2']").removeClass().addClass("Currency");
            }
            else {
                $(TextDiv).find("input[id^='txtBox1']").removeClass().addClass("Alphanumeric");
                $(TextDiv).find("input[id^='txtBox2']").removeClass().addClass("Alphanumeric");
            }


        }

        function masterVisiblity(DateDiv, TextDiv, DivDDL, obj, mastertype, DivDDLS) {
            $(DateDiv).hide();
            $(TextDiv).hide();

            var ddl1;
            var op;

            if (mastertype == 'S') {
                ddl1 = $(obj).closest('tr').find('select[id^="ForeignKeyValueS"]');
                ddl1.next('div').next('div.chzn-container-single-nosearch').remove();
                op = $(obj).closest('tr').find('select[id^="OperatorID"]').find('option:selected').val();
                $(DivDDL).hide();
                $(DivDDLS).show();
             
             
            }
            else {
                ddl1 = $(obj).closest('tr').find('select[id^="ForeignKeyValue"]');
                ddl1.next('div').next('div.chzn-container-single-nosearch').remove();
                op = $(obj).closest('tr').find('select[id^="OperatorID"]').find('option:selected').val();
                $(DivDDL).show();
                $(DivDDLS).hide();

            }
           ddl1.trigger('litsz:updated');
           ddl1.next('div').css('width', '350px');

        }


        function getMultipleValues(ctrlId) {

           
            var strSelText = '';

            try {
                var control = document.getElementById($(ctrlId).attr('id'));
                var hdnmultival = $(control).closest('tr').find('input[id^="hdnmultival_"]');
                var cnt = 0;
                for (var i = 0; i < control.length; i++) {
                    if (control.options[i].selected) {
                        if (cnt == 0) {
                            strSelText += control.options[i].value;
                        }
                        else {
                            strSelText += ',' + control.options[i].value;
                        }
                        cnt++;
                    }
                }
            } catch (e) {
                alert(e);
            }
            $(hdnmultival).val(strSelText);

        }


        function getMultipleValuesS(ctrlId) {

           
            var strSelText = '';

            try {
                var control = document.getElementById($(ctrlId).attr('id'));
                var hdnmultival = $(control).closest('tr').find('input[id^="hdnmultival_"]');
                var cnt = 0;
                for (var i = 0; i < control.length; i++) {
                    if (control.options[i].selected) {
                        if (cnt == 0) {
                            strSelText += control.options[i].value;
                        }
                        else {
                            strSelText += ',' + control.options[i].value;
                        }
                        cnt++;
                    }
                }
            } catch (e) {
                alert(e);
            }
            $(hdnmultival).val(strSelText);

        }




        function dateVisiblity(DateDiv, TextDiv, DivDDL, op, DivDDLS) {

            $(DateDiv).show();
            $(TextDiv).hide();
            $(DivDDL).hide();
            $(DivDDLS).hide();

            if (op == '11') {
                $(DateDiv).find("input[id^='dtpick2']").show();
            }
            else {
                $(DateDiv).find("input[id^='dtpick2']").hide();
            }
            $(DateDiv).find("input[id^='dtpick1']").datepicker({ defaultDate: "+1d",
                changeMonth: true,
                changeYear: true,
                numberOfMonths: 1,
                dateFormat: 'dd-MM-yy',
                yearRange: "-100:+10"
            });
            $(DateDiv).find("input[id^='dtpick2']").datepicker({ defaultDate: "+1d",
                changeMonth: true,
                changeYear: true,
                numberOfMonths: 1,
                dateFormat: 'dd-MM-yy',
                yearRange: "-100:+10"
            });
        }

        function myDateFormat(strDate) {
            var tempDate = new Array();
            tempDate = strDate.split('-');
            var dt = tempDate[0];
            var mt = getMonth(tempDate[1]);
            var yr = tempDate[2];
            return (yr + '/' + mt + '/' + dt);
        }

        function getMonth(monthName) {
            var val = 0;
            switch (monthName.toLowerCase()) {
                case "jan":
                    val = 1;
                    break;
                case "feb":
                    val = 2;
                    break;
                case "mar":
                    val = 3;
                    break;
                case "apr":
                    val = 4;
                    break;
                case "may":
                    val = 5;
                    break;
                case "jun":
                    val = 6;
                    break;
                case "jul":
                    val = 7;
                    break;
                case "aug":
                    val = 8;
                    break;
                case "sep":
                    val = 9;
                    break;
                case "oct":
                    val = 10;
                    break;
                case "nov":
                    val = 11;
                    break;
                case "dec":
                    val = 12;
                    break;
            }
            return val;
        }
 
    </script>
 <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>

</asp:Content>
