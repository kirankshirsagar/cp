﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="passwordforgot.aspx.cs" Inherits="Account_passwordforgot" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="apple-mobile-web-app-capable" content="no" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1" />
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
      <meta name="keywords" content="issue,bug,tracker" />
    <link href="../Styles/jquery-ui-1.8.21.css"  media="all" rel="stylesheet" type="text/css" />
    
    <link href="../Styles/Font.css" rel="stylesheet" type="text/css" />
     <link href="../Styles/stylesheet.css" media="screen" rel="stylesheet" type="text/css"/>
     <link href="../Styles/application.css" media="all" rel="stylesheet" type="text/css" />
    <title>Contract Management</title>
</head>
<body>
<form id="formForgotPassword" accept-charset="UTF-8" method="post" runat="server" >
  <div id="wrapper">
        <div id="wrapper2">
            <div class="fl top-border" align="right" style="padding-top: 3px;">                    
            </div>
            <div class="fl header top-15">
                    <table width="90%" align="center">
                        <tr>
                            <td width="40%"  align="right">
                                <img src="../images/logo.png" alt="logo" />
                            </td>
                            <td align="right" width="60%">                                
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    
  
    <div style="margin: 0; padding: 0; display: inline">
        <input name="utf8" type="hidden" value="&#x2713;" /><input name="authenticity_token"
            type="hidden" value="0VKkNZdXRLDC30YJdlBjScGVQIs3c9132jH7yYMJo6w=" /></div>
             <br /> <br />
    <div>
     
        <p>
            <h2>
                Forgot your password? </h2>
        </p>
      
                   
            <div id="errorExplanation" clientidmode="Static" runat="server">
             </div>
             <div id="flash_notice" clientidmode="Static" runat="server">
             </div>
            </div>
       
        <p>
            <label for="time_entry_issue_id">
                To receive your password, enter the full Email Address or User Name which you use
                to sign in to your Account.</label>
        </p>
  
    <div class="box tabular">
        <p>
            <label for="time_entry_issue_id">
                User name</label>
            <input id="txtUserName" runat="server" type="text" class="ui-autocomplete-input"
                maxlength="100" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" />
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id">
                OR</label>
        </p>
        <p>
            <label for="time_entry_issue_id">
                Email Address</label>
            <input id="txtEmail" runat="server" type="text" class="ui-autocomplete-input" maxlength="100"
                autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" />
            <em></em>
        </p>
    </div>
    <asp:Button ID="btnEmailRequest" runat="server" Text="Send Request" OnClick="btnEmailRequest_Click"
        ValidationGroup="E1" class="mws-button red" ClientIDMode="Static" />&nbsp;
    <input type="button" value="Cancel" id="Submit7" class="mws-button red" onclick="javascript:location.href='../login.aspx'" />
    </form>
    <script type="text/javascript">
        $("#btnEmailRequest").click(function (evt) {

            var username = $('#txtUserName').val();
            var emailId = $('#txtEmail').val();

            if ($.trim(username) == '' && $.trim(emailId) == '') {

                $('#errorExplanation').addClass('flash error');
                $('#errorExplanation').html('Email Address or User Name both should not be blank.').show();
                $(".flash notice").delay(3000).fadeOut("slow");
                $("#errorExplanation").delay(3000).fadeOut("slow");
              
                evt.preventDefault();
                return false;

            }
            else {

                $('#errorExplanation').hide();
                return true;
            }
           
        });
    </script>
     
    <script type="text/javascript">
        $('#errorExplanation').click(function () {
            hideShowDiv(0);
        });

        function hideShowDiv(flg) {
            if (flg == 0) {
                $('#errorExplanation').hide();
            }
            else {
                $('#errorExplanation').show();
            }
            return false;
        }
//        function togglemessage() {
//            $('#ErrorDivMessagePassForget').slideToggle('slow');
//        }
    </script><br />
    <br />
    <br />
    <br />
    <br />
    <br />
     <br />
    <br />
    <br />
   <div id="footer">
                <div class="bgl">
                    <div class="bgr">
                        Powered by <a href="http://www.uberall.in/" target="_blank">Uberall Solutions (I) Ltd.</a> © 2014-2015<br />                        
                    </div>
                </div>
            </div>
</body>
</html>
