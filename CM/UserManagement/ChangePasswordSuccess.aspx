﻿<%@ Page Title="Change Password" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="ChangePasswordSuccess.aspx.cs" Inherits="Account_ChangePasswordSuccess" %>
   <%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="usermanagementcntrl"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
  <script type="text/javascript">
      $("#setupLink").addClass("menulink");
      $("#setuptab").addClass("selectedtab");
    </script>
     <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <h2>
        Change Password
    </h2>  
    <p style="color:Green">
        Your password has been changed successfully.
    </p>
    <div id="rightlinks" style="display: none;">
        <uc1:usermanagementcntrl ID="usermanagementcntrl" runat="server" />
    </div>
    <script language="javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
</asp:Content>
