﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/CM.master"
    AutoEventWireup="true" CodeFile="RoleMasterData.aspx.cs" Inherits="Masters_RoleMasterData" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="usermanagementcntrl"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridSelect.js" type="text/javascript"></script>
    <script src="../scripts/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../scripts/chosen.jquery.js" type="text/javascript"></script>
    <link href="../Styles/Font.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script language="javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");
    </script>
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <h2>
        Roles</h2>
    <input id="hdnPrimeIds" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <div style="margin: 0; padding: 0; display: inline">
        <asp:LinkButton ID="btnAddRecord" CssClass="icon icon-add" OnClientClick="resetPrimeID();"
            runat="server" OnClick="btnAddRecord_Click">Add new Role</asp:LinkButton>
        <br />
        <asp:TreeView ID="tvMenu" runat="server" ShowCheckBoxes="All">
        </asp:TreeView>
    </div>
    <br />
    <br />
    <script type="text/javascript">

        function goBack() {
            $('#hdnPrimeIds').val('1');
        }


    </script>
    <div id="rightlinks" style="display: none;">
        <uc1:usermanagementcntrl ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
    <script type="text/javascript">

        function SetChosen() {

            var config = {
                '.chzn-select': {},
                '.chzn-select-deselect': { allow_single_deselect: true },
                '.chzn-select-no-single': { disable_search_threshold: 10 },
                '.chzn-select-no-results': { no_results_text: 'Oops, nothing found!' },
                '.chzn-select-width': { width: "150%" }
            }
            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }
        }

        $(document).ready(function () {
            if ("<%=Update%>" == 'N') {
                $('#MainContent_tvMenu').each(function () {
                    $(this).find('a').addClass('areadOnly');
                });
            }

            SetChosen();
        });


    
    </script>
</asp:Content>
