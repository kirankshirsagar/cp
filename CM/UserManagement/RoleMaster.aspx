﻿<%@ Page Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true" CodeFile="RoleMaster.aspx.cs"
    Inherits="Masters_RoleMaster" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="usermanagementcntrl"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../JQueryValidations/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script language="javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");
    </script>
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
    </h2>
    <div class="formMessage" style="display: none; width: 95%" id="MydivMessage" clientidmode="Static"
        runat="server">
    </div>
    <div class="box tabular">
        <p>
            <label for="time_entry_issue_id">
                <span class="required">*</span>Role Name</label>

            <input id="txtRoleName" runat="server" type="text" class="required" style="width: 35%" 
                maxlength="45" />
            <%--<asp:RegularExpressionValidator ID="RegExp111" runat="server" ErrorMessage="Password length must be between 7 to 10 characters"
                ControlToValidate=" txtRoleName " ValidationExpression="^[a-zA-Z0-9'@&#.\s]{7,10}$" />--%>
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id">
                Reporting To</label>
            <select id="ddlReporting" runat="server" class="chzn-select" style="width: 35.5%">
                <option></option>
            </select>
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id">
                Description</label>
            <textarea id="txtDescription" placeholder="Role description" runat="server" maxlength="100"
                clientidmode="static" type="text" autocomplete="false" style="width: 320px; height: 100px"
                rows="10" />
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id">
                Status</label>
            <input id="rdActive" name="ActiveInactive" runat="server" type="radio" class="required" />Active
            <input id="rdInactive" name="ActiveInactive" runat="server" type="radio" class="required" />Inactive
            <em></em>
        </p>
    </div>
    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" class="btn_validate" />
    <asp:Button ID="btnSaveAndContinue" runat="server" Text="Save and Add more" OnClick="SaveAndContinue_Click"
        class="btn_validate" />
    <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_Click"
        OnClientClick="return confirm('Are you sure you want to delete?');" />
    <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back_Click" />
    <div id="rightlinks" style="display: none;">
        <uc1:usermanagementcntrl ID="usermanagementcntrl" runat="server" />
    </div>
    <script language="javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
</asp:Content>
