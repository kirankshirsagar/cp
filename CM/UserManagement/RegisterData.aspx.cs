﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UserManagementBLL;
using CommonBLL;
using System.Web.Security;
using EmailBLL;
using System.Net.Mail;

public partial class RegisterData : System.Web.UI.Page
{
    // navigation//
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 10;
    // navigation//

    IUsers objUser;
    IRole objRole;
    IDepartment objDepartment;

    public string Add;
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {

        CreateObjects();
       
        deleteMessage();

      

        if (Session[Declarations.User] != null && !IsPostBack)
        {
           
            Access.PageAccess(this, Session[Declarations.User].ToString(), "usermanagement_");
            ViewState[Declarations.Add] = Access.Add.Trim();
            ViewState[Declarations.View] = Access.View.Trim();
            ViewState[Declarations.Delete] = Access.Delete.Trim();
            ViewState[Declarations.Update] = Access.Update.Trim();
            ViewState["ResetPassword"] = Access.isCustomised(26);
            ViewState["UnlockUser"] = Access.isCustomised(27);

            //if (Access.isCustomised(26) == false)
            //{
            //    btnResetPassword.Visible = false;
            //}
            //if (Access.isCustomised(27) == false)
            //{
            //    btnUnlockUser.Visible = false;
            //}

            string retVal=objUser.RemainingCount();
            hdnUserRemainingCount.Value = retVal.Split('_')[1];
            hdnTotalUsersAllowed.Value = retVal.Split('_')[0];
        }
        AccessVisibility();
        if (!IsPostBack)
        {
            RoleBind();
            DepartmentBind();
        }
        if (!IsPostBack || hdnSearch.Value.Trim() != string.Empty || hdncheckflg.Value=="1")
        {
            Session[Declarations.SortControl] = null;
            rdActive.Checked = true;
            ReadData(1, RecordsPerPage);
            AccessVisibility();
            Message();
           
        }
        else
        {
            SetNavigationButtonParameters();
        }

    }

    void RoleBind()
    {
        Page.TraceWrite("Role dropdown bind starts.");
        try
        {
            ddlRole.extDataBind(objRole.SelectData());
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Role dropdown bind ends.");
    }
    void DepartmentBind()
    {
        Page.TraceWrite("Department dropdown bind starts.");
        try
        {
            ddlDepartment.extDataBind(objDepartment.SelectData());
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Department dropdown bind ends.");
    }

    // navigation//

    void Message()
    {
        if (Session[Declarations.Message] != null)
        {
            var flg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }

    }

    // navigation//

    #region Navigation
    void SetNavigationButtonParameters()
    {
        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }


    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
            AccessVisibility();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

    }
    #endregion

    // navigation//

    protected void btnSort_Click(object sender, EventArgs e)
    {
        hdnSearch.Value = "";
        LinkButton clickBtn = (LinkButton)sender;
        Session.Add(Declarations.SortControl, clickBtn);
        ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
    }

    void ReadData(int pageNo, int recordsPerPage)
    {
        if (View == "Y")
        {
            Page.TraceWrite("ReadData starts.");
            try
            {
                objUser.PageNo = pageNo;
                objUser.RecordsPerPage = recordsPerPage;
                objUser.Search = txtSearch.Value.Trim();

                LinkButton btnSort = null;
                if (Session[Declarations.SortControl] != null && Session[Declarations.SortControl] != string.Empty)
                {
                    btnSort = (LinkButton)Session[Declarations.SortControl];
                    objUser.Direction = btnSort.CssClass.ToLower() == "sort desc" ? 1 : 0;
                    objUser.SortColumn = btnSort.Text.Trim();
                }

                objUser.DepartmentId = int.Parse(ddlDepartment.Value);
                objUser.RoleId=int.Parse(ddlRole.Value);
                rptUser.DataSource = objUser.ReadData();
                rptUser.DataBind();
                if (btnSort != null)
                {
                    rptUser.ClassChange(btnSort);
                }

                ViewState[Declarations.TotalRecord] = objUser.TotalRecords;
                PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
                PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
                PaginationButtons1.RecordsPerPage = RecordsPerPage;
                Page.TraceWrite("ReadData ends.");
            }
            catch (Exception ex)
            {
                Page.TraceWarn("ReadData fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objUser = FactoryUser.GetUsersDetail();
            objDepartment = FactoryUser.GetDepartmentDetail();
            objRole = FactoryUser.GetRoleDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Delete starts.");
        string status = "";
        objUser.UserIds = hdnPrimeIds.Value;
        try
        {
            status = objUser.DeleteRecord();
        }
        catch (Exception ex)
        {
            hdnPrimeIds.Value = string.Empty;
            Session[Declarations.DeleteMSG] = "PageGetMessage('DF')";
            Page.TraceWarn("Delete fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        hdnPrimeIds.Value = string.Empty;
        Session[Declarations.DeleteMSG] = "PageGetMessage('DS')";
        Page.TraceWrite("Delete ends.");
        Server.Transfer("RegisterData.aspx");
    }

    void deleteMessage()
    {
        if (Session[Declarations.DeleteMSG] != null)
        {
            Page.JavaScriptClientScriptBlock("status", Session[Declarations.DeleteMSG].ToString());
            Session[Declarations.DeleteMSG] = null;
        }
    }

    protected void btnUnlockUser_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("unlock starts.");
        string status = "";
        objUser.UserIds = hdnPrimeIds.Value;
        try
        {
            status = objUser.UnlockUsers();
        }
        catch (Exception ex)
        {
            hdnPrimeIds.Value = string.Empty;
            Page.JavaScriptClientScriptBlock("status1", "PageGetMessage('UF')");
            Page.TraceWarn("unlock fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        hdnPrimeIds.Value = string.Empty;
        Page.JavaScriptClientScriptBlock("status2", "PageGetMessage('US')");
        Page.TraceWrite("unlock ends.");
    }

    protected void btnChangeStatus_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Change Status starts.");
        string status = "";
        objUser.UserIds = hdnPrimeIds.Value;
        try
        {
            objUser.isActive = rdActive.Checked == true ? "Y" : "N";
            status = objUser.ChangeIsActive();
            ReadData(1, RecordsPerPage);
        }
        catch (Exception ex)
        {
            hdnPrimeIds.Value = string.Empty;
            Page.JavaScriptClientScriptBlock("status", "PageGetMessage('SF')");
            Page.TraceWarn("Change Status fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.JavaScriptClientScriptBlock("status", "PageGetMessage('SS')");
        Page.TraceWrite("Change Status ends.");
    }


    protected void imgEdit_Click(object sender, EventArgs e)
    {
        if (Update == "Y")
            Server.Transfer("Register.aspx");
    }

    protected void btnAddRecord_Click(object sender, EventArgs e)
    {
        Server.Transfer("Register.aspx");
    }
    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }
    }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (View == "N")
        {
            Page.extDisableControls();
            btnChangeStatus.Enabled = false;
        }
        else
        {
            btnSearch.Enabled = true;
            txtSearch.Disabled = false;
        }

        if (Update == "N")
        {
            foreach (RepeaterItem item in rptUser.Items)
            {
                LinkButton imgEdit = (LinkButton)item.FindControl("imgEdit");
                imgEdit.Enabled = false;
                imgEdit.CssClass = "areadOnly";
            }
            btnDelete.Enabled = false;
            btnChangeStatus.Enabled = false;
        }
        if (Add == "N")
        {
            Page.extDisableControls();
            btnChangeStatus.Enabled = false;
            btnAddRecord.Enabled = false;
        }
        else
        {
            btnAddRecord.Enabled = true;
        }

        if (Delete == "N")
        {
            btnDelete.Enabled = false;
            btnDelete.OnClientClick = null;
        }
        else if (Delete == "Y")
        {
            btnDelete.Enabled = true;
        }

        if (View == "Y")
        {
            txtSearch.Disabled = false;
            btnSearch.Enabled = true;
            btnShowAll.Enabled = true;
        }

        if (ViewState["ResetPassword"] != null && Convert.ToBoolean(ViewState["ResetPassword"].ToString()))
        {
            btnResetPassword.Enabled = true;
        }
        else
        {
            btnResetPassword.Enabled = false;
        }
        if (ViewState["UnlockUser"] != null && Convert.ToBoolean(ViewState["UnlockUser"].ToString()))
        {
            btnUnlockUser.Enabled = true;
        }
        else
        {
            btnUnlockUser.Enabled = false;
        }

        Page.TraceWrite("AccessVisibility starts.");
    }


    protected void btnResetPassword_Click(object sender, EventArgs e)
    {
        
        EmailPassword objEmail = new EmailPassword();
        if (hdnPrimeIds.Value != "")
        {
            string[] userInfo = (hdnEmailIdsWithUserName.Value).Split(',');
            string usedNames = "";
            for (int i = 0; i < userInfo.Length; i++)
            {
                string UserName = userInfo[i].Split('#')[1];
                string email = userInfo[i].Split('#')[0];
                string pathURL = (HttpContext.Current.Request.Url.ToString()).Replace("RegisterData.aspx", "ResetPassword.aspx");
                bool status = objEmail.SendMail(UserName.Trim().ToLower(), email, pathURL);
                if (status == true)
                {
                    LogPasswordActivity(email, UserName);
                    Page.JavaScriptClientScriptBlock("status", "PageGetMessage('RP',1000)");


                }
                else
                {
                    if (usedNames == "")
                    {
                        usedNames = UserName.Trim();
                    }
                    else
                    {
                        usedNames = usedNames + "," + UserName.Trim();
                    }

                }
                if (usedNames != "")
                    Page.JavaScriptClientScriptBlock("status", "MessageMasterDiv('Email not sent to.'" + usedNames + "', 1,1000)'");
            }
        }
      
    }


    private void LogPasswordActivity(string emailId, string userName)
    {
        objUser.UserName = userName;
        objUser.Email = emailId;
        objUser.MailType = "Reset Password";
        objUser.UserPasswordActivityLog();
    }
         

}










