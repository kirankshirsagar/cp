﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GeneratePassword.aspx.cs"
    Inherits="Account_GeneratePassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/mastervalidations.js" type="text/javascript"></script>

    <link href="../Styles/application.css" media="all" rel="stylesheet" type="text/css" />
    <link href="../Styles/stylesheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #result
        {
            margin-left: 5px;
        }
        
        #register .short
        {
            color: #FF0000;
        }
        
        #register .weak
        {
            color: #E66C2C;
        }
        
        #register .good
        {
            color: #2D98F3;
        }
        
        #register .strong
        {
            color: #006400;
        }
    </style>
    <script language="javascript" type="text/javascript">


        $('#txtNewPassword').live('keyup', function (evt) {

            if ($('#txtNewPassword').val().trim().length == 0) {
                $('#result').html('');
                $('#strength').hide();
            }
            else {
                $('#result').html(checkStrength($('#txtNewPassword').val()));
            }

        });

        function checkStrength(password) {
            
            var strength = 0;
            if (password.length < 8) {
                $('#result').removeClass();
                $('#result').addClass('short');
                $('#strength').hide();
                $('#strength').css('width', '200px');
                $('#strength').css('height', '20px');
                $('#strength').css('background-color', 'transparent');
                $('#strength').show();
                //                $('#strength').animate({ width: '200px', height: '20px' }, 'medium').css('backgroundColor', 'transparent');
                return 'Password Too short';
            }
            if (password.length > 7)
                strength += 1;
            if (password.match(/(^[A-Z])/) && password.match(/([0-9])/) && strength == 1)
                strength += 1;
            if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/) && strength == 2)
                strength += 1;
            if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/) && strength == 3)
                strength += 1;
            if (strength < 2) {
                $('#result').removeClass();
                $('#result').addClass('weak');

                $('#strength').hide();
                $('#strength').css('width', '50px');
                $('#strength').css('height', '20px');
                $('#strength').css('background-color', 'red');
                $('#strength').show();
                //                $('#strength').animate({ width: '50px', height: '20px' }, 'medium').css('backgroundColor', 'red');
                //                $('#strength').show();
                return 'Weak';
            } else if (strength == 2) {
                $('#result').removeClass();
                $('#result').addClass('good');
                $('#strength').hide();
                $('#strength').css('width', '120px');
                $('#strength').css('height', '20px');
                $('#strength').css('background-color', 'Orange');
                $('#strength').show();
                //                $('#strength').animate({ width: '100px', height: '20px' }, 'medium').css('backgroundColor', '#FFBF00');
                //                $('#strength').show();
                return 'Good';
            } else {
                $('#result').removeClass();
                $('#result').addClass('strong');
                $('#strength').hide();
                $('#strength').css('width', '180px');
                $('#strength').css('height', '20px');
                $('#strength').css('background-color', 'green');
                $('#strength').show();
                //                $('#strength').animate({ width: '150px', height: '20px' }, 'medium').css('backgroundColor', 'green');
                //                $('#strength').show();
                return 'Strong';
            }
        }
       


        
    </script>
    <script type="text/javascript">
        //function onlyChar(evt) {


        $('.matchPwd').live('keyup', function (evt) {



            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode == 8)
                $('.matchPwd').trigger('keypress');

        }).live('keypress', function (evt) {

            // for trans-browser compatibility
            var charCode = (evt.which) ? evt.which : event.keyCode
            var RegExp = /^[a-zA-Z0-9]*$/;
            var char = String.fromCharCode(charCode);
            var caretpos = $(this).caret().start;
            valuestart = $(this).val().substring(0, caretpos);
            valueend = $(this).val().substring(caretpos);
            var myNum = valuestart + String.fromCharCode(charCode) + valueend;
            if (myNum == '') {
                $('#lblIsMatched').css('display', 'none');
                return false;
            }
            //             if (!RegExp.test(myNum)) {
            //                 //return myNum;
            //                 $('#lblIsMatched').css('display', 'none');
            //                 if (charCode != 8)
            //                     return false;
            //             }
            // 
            var Pwd1, Pwd2;
            var id = $(this).attr('id');
            if (id != 'txtNewPassword')
                Pwd1 = $.trim($('#txtNewPassword').val());
            else
                Pwd1 = $.trim($('#txtConfirmPassword').val());
            Pwd2 = myNum;

            if (Pwd1 == Pwd2) {
                $('#lblIsMatched').html('Password matched.');
                $('#lblIsMatched').css('color', 'green');
                $('#lblIsMatched').css('display', 'block');
                $('#CompareValidator1').css('display', 'none');
            }
            else
            { $('#lblIsMatched').css('display', 'none'); }

            return passwordChanged();
        });
        function CheckUserName(ctl) {
            //             var hdnUserName = document.getElementById('hdnusername').value;
            //             var mytextbox = document.getElementById('txtNewPassword').value;
            //             var username = ctl.value;

            //             if (mytextbox.search(hdnUserName) != -1) {
            //                 document.getElementById('txtNewPassword').value = '';
            //                 alert('User name not allowed in password.');
            //                 document.getElementById('txtNewPassword').focus();
            //                 return false;
            //             }
            return true;
        }
        //         function CheckPassword(event) {
        //             
        //             var hdnPwd = $('#hdnPassword').val();
        //             var mytextbox = $('#txtNewPassword').val();
        //             if (hdnPwd == mytextbox) {
        //                 alert('New Password is the same as previous password. Please enter different password.');
        //                 $('#txtNewPassword').val('');
        //                 event.preventDefault();
        //             }
        //         }


    </script>
    <title>Contract Managment</title>
</head>
<body>
  <div id="wrapper">
        <div id="wrapper2">
            <div class="fl top-border" align="right" style="padding-top: 3px;">                    
            </div>
            <div class="fl header top-15">
                    <table width="90%" align="center">
                        <tr>
                            <td width="40%"  align="right">
                                <img src="../images/logo.png" />
                            </td>
                            <td align="right" width="60%">                                
                            </td>
                        </tr>
                    </table>
            </div>
        </div>
    </div>
    <form id="register" runat="server">
    <div style="margin-left:30px">
   
            <h2>
                Generate password </h2>
      
    <div class="box tabular">
        <!-- Table starts from here-->

        <div id="errorExplanation" clientidmode="Static" runat="server">
             </div>
             <div id="flash_notice" clientidmode="Static" runat="server">
             </div>

        <div class="mws-panel-body" id="divResetPassword" runat="server">
            <br />
            <div class="box tabular">
            <table id="tblpwd" runat="server" style="width: 50%">
                <tr>
              
                     <td  style="text-align: right; width: 15%;vertical-align:top">
                          <label for="time_entry_issue_id" style="margin-left:0px;">
                            <span class="required">*</span>New Password</label>
                    </td>
                    <td style="text-align:left;width:35%">
                        <asp:TextBox ID="txtNewPassword" CssClass="mws-textinput matchPwd pasteBlock Password"
                            runat="server" TextMode="Password" ClientIDMode="Static" Columns="15" onBlur="return CheckUserName(this)"
                            Width="100%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="P"
                                ErrorMessage="Please enter new password." ControlToValidate="txtNewPassword" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                    </td>
                    <td valign="baseline" align="left">
                        <div id="strength" style="margin-left: 10px; display: none" class="mws-panel-header">
                            <center>
                                <b><span id="result" style="color: black"></span></b>
                            </center>
                        </div>
                    </td>
                    <td>
                        <asp:HiddenField ID="hdnusername" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hdnPassword" runat="server" ClientIDMode="Static" />
                    </td>
                </tr>
                <tr>
               
                    <td  style="text-align: right; width: 15%;vertical-align:top">
                          <label for="time_entry_issue_id" style="margin-left:0px;">
                            <span class="required">*</span>Confirm Password</label>
                    </td>
                    <td style="text-align:left;width:35%">
                        <asp:TextBox ID="txtConfirmPassword" CssClass="mws-textinput matchPwd pasteBlock Password"
                            runat="server" TextMode="Password" MaxLength="15" Columns="15" Width="100%">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="P"
                                ErrorMessage="Please enter confirm password." ControlToValidate="txtConfirmPassword"
                                ForeColor="Red">
                        </asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator1" ForeColor="Red" runat="server" ErrorMessage="Password does not match."
                            ValidationGroup="P" ControlToValidate="txtConfirmPassword" ControlToCompare="txtNewPassword"
                            SetFocusOnError="True" Display="Dynamic">
                        </asp:CompareValidator>
                    </td>
                    <td valign="baseline" align="left">
                        <asp:Label runat="server" ID="lblIsMatched" Style="display: none; text-align: center"></asp:Label>
                       
                    </td>
                    <td >
                    </td>
                </tr>
            </table>
            </div>
            <br />
           
            <asp:Button ID="btnChangePassword" runat="server" OnClick="btnChangePassword_Click"
                Text="Change Password" ValidationGroup="P" class="mws-button red" />
            <asp:Button ID="BtnLogout" runat="server" OnClick="BtnLogout_Click" Text="Cancel"
                class="mws-button red" />
            <div>
                <asp:HyperLink ID="lnklogin" Visible="false" NavigateUrl="~/Login.aspx" runat="server">Click here for login</asp:HyperLink>
            </div>
        </div>
        <div id="ErrorMessage" runat="server" class="error mws-form-message" clientidmode="Static"
            visible="false">
        </div>
    </div>
    </div>
    <br /><br /><br />
     <div id="footer">
                <div class="bgl">
                    <div class="bgr">
                        Powered by <a href="http://www.uberall.in/" target="_blank">Uberall Solutions (I) Ltd.</a> © 2014-2015<br />
                    </div>
                </div>
            </div>
    <!-- Table Ends here-->
    <script type="text/javascript">
        function togglemessageerror() {
            $('#ErrorMsgConfirmPassword').slideToggle('slow');
        }


        function togglemessagesucess() {

            $('#SucessMsgConfirmPassword').slideToggle('slow');
        }

    </script>
    </form>
</body>
</html>
