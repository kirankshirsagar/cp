﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UserManagementBLL;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;



public partial class Masters_RoleMasterData : System.Web.UI.Page
{

    IRole objRole;
    public string Add;
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {

        CreateObjects();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            Access.PageAccess(this, Session[Declarations.User].ToString(), "usermanagement_");
            ViewState[Declarations.Add] = Access.Add.Trim();
            ViewState[Declarations.View] = Access.View.Trim();
            ViewState[Declarations.Delete] = Access.Delete.Trim();
            ViewState[Declarations.Update] = Access.Update.Trim();
        }

        AccessVisibility();
        if (!IsPostBack || hdnSearch.Value.Trim() != string.Empty)
        {
            ReadData();
            Message();
        }
    }

    void Message()
    {
        if (Session[Declarations.Message] != null)
        {
            var flg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }

    }

    private void CreateTreeView(List<Role> source, int parentID, TreeNode parentNode)
    {
        List<Role> newSource = source.Where(aa => aa.ReportingId.Equals(parentID)).ToList();

        foreach (var i in newSource)
        {
            string activestatus = i.isActive;
            TreeNode newnode = new TreeNode(i.RoleName, i.RoleId.ToString());
            if (parentNode == null)
            {
                newnode.ShowCheckBox = false;
                if (Update == "N")
                {
                    newnode.NavigateUrl = "javascript:void(0);";
                }
                else
                {
                    //newnode.NavigateUrl = i.RoleId == 1 ? "#" : "RoleMaster.aspx?RoleId=" + i.RoleId;
                    //if (i.RoleId == 1)
                    if (i.RoleId == 99999)
                    {
                        newnode.NavigateUrl = "#";
                    }
                    else
                    {
                        newnode.NavigateUrl = "RoleMaster.aspx?RoleId=" + i.RoleId;
                    }
                }
                newnode.ImageUrl = "../Images/user_suit.png";
                newnode.Value = Convert.ToString(i.RoleId);
                tvMenu.Nodes.Add(newnode);
            }
            else
            {
                newnode.ShowCheckBox = false;
                if (Update == "N")
                {
                    newnode.NavigateUrl = "javascript:void(0);";
                }
                else
                {
                    newnode.NavigateUrl = "RoleMaster.aspx?RoleId=" + i.RoleId;
                }
                newnode.Value = Convert.ToString(i.RoleId);
                if (activestatus == "Active")
                {
                    newnode.ImageUrl = "../Images/user_green.png";
                }
                else
                {
                    newnode.ImageUrl = "../Images/user_orange.png";
                }
                parentNode.ChildNodes.Add(newnode);
            }
            CreateTreeView(source, i.RoleId, newnode);
        }
    }

    void ReadData()
    {

        Page.TraceWrite("ReadData starts.");
        try
        {
            List<Role> allMenu = new List<Role>();
            allMenu = objRole.ReadData().ToList();

            // Call function here for bind treeview 

            CreateTreeView(allMenu, 0, null);

            Page.TraceWrite("ReadData ends.");
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }


    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objRole = FactoryUser.GetRoleDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }
    protected void btnAddRecord_Click(object sender, EventArgs e)
    {
        Response.Redirect("RoleMaster.aspx");
    }

    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }


    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (View == "N")
        {
            Page.extDisableControls();
            btnAddRecord.Enabled = false;
        }
        else
        {
            btnAddRecord.Enabled = true;
        }

        if (Add == "N")
        {
            Page.extDisableControls();
            btnAddRecord.Enabled = false;
        }
        else
        {
            btnAddRecord.Enabled = true;
        }

        Page.TraceWrite("AccessVisibility starts.");
    }
}










