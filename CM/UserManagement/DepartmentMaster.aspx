﻿<%@ Page Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true" CodeFile="DepartmentMaster.aspx.cs"
    Inherits="Masters_DepartmentMaster" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="usermanagementcntrl"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script language="javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");
    </script>
  
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
    </h2>
    <div class="box tabular">
        <p>
            <label for="time_entry_issue_id">
                <span class="required">*</span>Department Name</label>
            <input id="txtDepartmentName" runat="server" type="text" class="required ui-autocomplete-input" style="width: 220px;"
                maxlength="50 " autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" />
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id">
                Description </label>
            <textarea id="txtDescription" placeholder="Department description" runat="server" maxlength="150" 
                clientidmode="static" ntype="text" autocomplete="false" style="width: 220px;height: 100px;" rows="10"
                class="myclass"></textarea>
            <em></em>
        </p>
        <p><label for="time_entry_issue_id">
                Status </label>
            <input id="rdActive" name="ActiveInactive" runat="server" type="radio" class="required" />Active
            <input id="rdInactive" name="ActiveInactive" runat="server" type="radio" class="required" />Inactive
            <em></em>
        </p>
    </div>
    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" class="btn_validate" />
    <asp:Button ID="btnSaveAndContinue" runat="server" Text="Save and Add more" OnClick="SaveAndContinue_Click"
        class="btn_validate" />
    <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back_Click" />
    <div id="rightlinks" style="display: none;">
        <uc1:usermanagementcntrl ID="usermanagementcntrl" runat="server" />
    </div>
    <script language="javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
</asp:Content>
