﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using EmailBLL;
using System.Net.Mail;
using UserManagementBLL;
using CommonBLL;

public partial class Account_passwordforgot : System.Web.UI.Page
{
    IUsers obj;
    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "backqq", "disableBackButton();", true);
       
        errorExplanation.Style.Add("display", "none");
        flash_notice.Style.Add("display", "none");
    }
    void CreateObjects()
    {
            obj = FactoryUser.GetUsersDetail();
    }
    protected void btnEmailRequest_Click(object sender, EventArgs e)
    {
        try
        {
            string email = "";

            EmailPassword objEmail = new EmailPassword();
            IUsers obj = FactoryUser.GetUsersDetail();
            obj.UserName = txtUserName.Value;
            obj.GetUsersDetails();
            objEmail.UserId = Convert.ToString(obj.UserId);

            if (txtEmail.Value == null || txtEmail.Value == "")
            {
                obj.UserName = txtUserName.Value;
                email = obj.GetEmailId();
            }
            else
            {
                obj.UserName = null;
                email = txtEmail.Value;
                obj.Email = txtEmail.Value;
                obj.UserName = obj.GetEmailId();
            }

            string recoveryLink = HttpContext.Current.Request.Url.AbsoluteUri.Remove(HttpContext.Current.Request.Url.AbsoluteUri.Length - HttpContext.Current.Request.Url.Segments.Last().Length);

            recoveryLink = recoveryLink.TrimStart('{').TrimEnd('}');
            recoveryLink = recoveryLink + "confirmpassword.aspx";
           
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            bool EmailIsValid = true;
            if (txtEmail.Value != "")
            {
                EmailIsValid = match.Success;
            }


            if (EmailIsValid == true)
            {

                if (objEmail.SendMail(obj.UserName.Trim().ToLower(), email, recoveryLink))
                {
                    LogPasswordActivity(email, obj.UserName.Trim());
                    HttpCookie myCookie = new HttpCookie("myCookie");
                    Response.Cookies.Remove("myCookie");
                    txtEmail.Value = "";
                    txtUserName.Value = "";
                    flash_notice.Style.Add("display", "block");
                    flash_notice.Attributes.Add("class", "flash notice");
                    flash_notice.InnerHtml = "Password reset instructions has been sent to your registered email Id " + email;
                    errorExplanation.Style.Add("display", "none");
               
                }
                else
                {
                    flash_notice.Style.Add("display", "none");
                    errorExplanation.Style.Add("display", "block");
                    errorExplanation.Attributes.Add("id", "flash_notice");
                    errorExplanation.Attributes.Add("class", "errorExplanation");
                    errorExplanation.InnerHtml = "Invalid User Name or Email Address.";
                    errorExplanation.Style.Add("fadeout", "slow");
                }
            }
            else
            {
                flash_notice.Style.Add("display", "none");
                errorExplanation.Style.Add("display", "block");
                errorExplanation.Attributes.Add("class", "flash error");
                errorExplanation.InnerHtml = "Please enter a valid email address.";
                errorExplanation.Style.Add("fadeout", "slow");
            }
            
        
        }
        
        catch(Exception ex)
        {

            //Log.WriteLog(ex);
        
        }
    }


    private void LogPasswordActivity(string emailId, string userName)
    {
        obj.UserName = userName;
        obj.Email = emailId;
        obj.MailType = "Forgot Password";
        obj.UserPasswordActivityLog();
    }


}