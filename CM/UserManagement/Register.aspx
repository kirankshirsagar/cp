﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    EnableViewState="true" CodeFile="Register.aspx.cs" Inherits="Users_Register" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="UserManagementLinks"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="../scripts/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="../JQueryValidations/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    <link href="../Styles/chosen.css" rel="stylesheet" type="text/css" />
    <script src="../scripts/chosen.jquery.js" type="text/javascript"></script>
    <script src="../CommonScripts/dropdownlist.js" type="text/javascript"></script>
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Font.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/mastervalidations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function NewWindow(mypage, myname, w, h, scroll, pos) {
            if (pos == "random") { LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100; TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100; }
            if (pos == "center") { LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100; TopPosition = (screen.height) ? (screen.height - h) / 2 : 100; }
            else if ((pos != "center" && pos != "random") || pos == null) { LeftPosition = 0; TopPosition = 20 }
            settings = 'width=' + w + ',height=' + h + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=' + scroll + ',location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no';
            win = window.open(mypage, myname, settings);
        }
        function setStateValue() {
            $('#hdnStateId').val('');
            $('#hdnStateId').val($('#ddlState').val());
        }
        function setCityValue() {

            $('#hdnCityId').val('');
            $('#hdnCityId').val($('#ddlCity').val());
        }
        function setDafaultValues() {
            $('#ddlState').val($('#hdnStateId').val());
        }
        function setDafaulCitytValues() {
            $('#ddlCity').val($('#hdnCityId').val());
        }
        $(document).ready(function () {
            $('#ddlCountry').change();
            if ($('#hdnStateIdRead').val() != '') {

                if ($('#hdnStateNameReadDisable').val() != '') {
                    addItemToSelectIfNotFound('ddlState', $('#hdnStateIdRead').val(), $('#hdnStateNameReadDisable').val());
                }

                $('#ddlState').val($('#hdnStateIdRead').val());
                $('#hdnStateId').val($('#hdnStateIdRead').val());
            }

            $('#ddlState').change();
            if ($('#hdnCityIdRead').val() != '') {
                if ($('#hdnCityNameReadDisable').val() != '') {
                    addItemToSelectIfNotFound('ddlCity', $('#hdnCityIdRead').val(), $('#hdnCityNameReadDisable').val());
                }
                $('#ddlCity').val($('#hdnCityIdRead').val());
                $('#hdnCityId').val($('#hdnCityIdRead').val());
            }


            $('#UserName').keypress(function (e) {
                if (e.which === 32)
                    e.preventDefault();
            });
            function validateUser(obj) {

                var fl = 0;
                EditItems = "";

                if ($(obj).val().length == 0 || $(obj).val().trim() == '') {

                    $(obj).css('border-color', 'red');
                    $('#lblUserExists').html('');
                    $('#HdnUserExists').val('');
                }
                else {
                    fl = 1;
                    $(obj).css('border-color', '');
                    $(obj).next('.tooltip_outer').hide();
                }

                if (fl == 1) {

                    var type1 = 'User';
                    var strType = "POST";
                    var strContentType = "text/html; charset=utf-8";
                    var strData = "{}";
                    var strURL = '../Handlers/User.ashx?Type=' + type1 + '&Email=' + $('#Email').val() + "&UserName=" + $('#UserName').val();
                    var strCatch = false;
                    var strDataType = 'json';
                    var strAsync = false;
                    var objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
                    EditItems = objHandler.HandlerReturnedData;
                    $('#lblUserExists').html(EditItems);
                    $('#HdnUserExists').val(EditItems);
                    if (EditItems != "")
                        $('#UserName').val('');
                }
            }
            $('#UserName').change(function () {
                validateUser(this);
            });
            $('#Email').change(function () {
                var fl = 0;

                $('#lblEmailExists').html('');
                $('#HdnUserExists').val('');
                $(this).next('.tooltip_outer').hide();
                var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (filter.test($(this).val()) === false && $(this).val() != "") {

                    empty_count = 1;

                }
                else {
                    fl = 1;
                    $(this).next('.tooltip_outer').hide();
                    $('#lblEmailExists').html('');
                }

                if (fl == 1) {
                    EditItems = "";
                    var type1 = 'email';
                    var strType = "POST";
                    var strContentType = "text/html; charset=utf-8";
                    var strData = "{}";
                    var strURL = '../Handlers/User.ashx?Type=' + type1 + '&Email=' + $('#Email').val() + "&UserName=" + $('#UserName').val();
                    var strCatch = false;
                    var strDataType = 'json';
                    var strAsync = false;
                    var objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
                    EditItems = objHandler.HandlerReturnedData;
                    $('#lblEmailExists').html(EditItems);
                    $('#HdnEmailExists').val(EditItems);
                    if (EditItems != "")
                        $('#Email').val('');
                }
                else {
                    EditItems = "";
                    $('#lblEmailExists').html(EditItems);
                    $('#HdnEmailExists').val(EditItems);
                }
            });
        });


        function setValue(name) {
            try {

                $("#MainContent_RegisterUser_CreateUserStepContainer_imgUpload").attr('src', name);
                $('#hdnPhotoId').val(name);
            } catch (e) {

            }

        }
      
        
    </script>
    <script type="text/javascript">
        $(function () {
            // if (e.shiftKey || e.ctrlKey || e.altKey) {
            $('.CharValidation').keydown(function (e) {
                if (e.ctrlKey || e.altKey) {
                    e.preventDefault();
                } else {
                    var key = e.keyCode;
                    if (!((key == 9) || (key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                        e.preventDefault();
                    }
                }
            });


        });
</script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script language="javascript" type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");
    </script>


    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <div class="box tabular">
        <input id="hdnOldUserName" clientidmode="Static" runat="server" type="hidden" />
        <input id="hdnStateNameReadDisable" clientidmode="Static" runat="server" type="hidden" />
        <input id="hdnCityNameReadDisable" clientidmode="Static" runat="server" type="hidden" />
        <asp:CreateUserWizard ID="RegisterUser" runat="server" EnableViewState="true" OnCreatedUser="RegisterUser_CreatedUser"
            LoginCreatedUser="false" OnSendingMail="RegisterUser_SendingMail" MailDefinition-From="ng@myuberall.com"
            MailDefinition-Subject="Account Creation" MailDefinition-IsBodyHtml="false" AutoGeneratePassword="true">
            <LayoutTemplate>
                <asp:PlaceHolder ID="wizardStepPlaceholder" runat="server"></asp:PlaceHolder>
                <asp:PlaceHolder ID="navigationPlaceholder" runat="server"></asp:PlaceHolder>
            </LayoutTemplate>
            <WizardSteps>
                <asp:CreateUserWizardStep ID="RegisterUserWizardStep" runat="server">
                    <ContentTemplate>
                        <asp:HiddenField ID="HdnUserExists" ClientIDMode="Static" runat="server" />
                        <asp:HiddenField ID="HdnEmailExists" ClientIDMode="Static" runat="server" />
                        <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
                        <input id="hdnStateId" clientidmode="Static" runat="server" type="hidden" />
                        <input id="hdnStateIdRead" clientidmode="Static" runat="server" type="hidden" />
                        <input id="hdnCityId" clientidmode="Static" runat="server" type="hidden" />
                        <input id="hdnCityIdRead" clientidmode="Static" runat="server" type="hidden" />
                        <input id="hdnPhotoId" clientidmode="Static" runat="server" type="hidden" />
                        <h2>
                            <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
                        </h2>
                        <div class="box tabular">
                            <table>
                                <tr>
                                    <td width="48%">
                                    </td>
                                    <td width="52%">
                                        <a href="../PhotoUpload.aspx" onclick="NewWindow(this.href,'mywin','900','500','No','center');return false"
                                            onfocus="this.blur()">
                                            <asp:Image ID="imgUpload" runat="server" Width="150px" Height="150px" Style="padding-left: 16px;
                                                background-repeat: no-repeat;" ImageUrl="../Images/userdefault.jpg" /></a>
                                        <br />
                                        &nbsp;&nbsp; <a href="../PhotoUpload.aspx" onclick="NewWindow(this.href,'mywin','900','500','No','center');return false"
                                            onfocus="this.blur()">Click To Upload Photo</a>
                                    </td>
                                </tr>
                            </table>
                            <p id="contracttypebox" runat="server">
                                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName" for="time_entry_activity_id"><span class="required">*</span>User Name</asp:Label>
                                <asp:TextBox ID="UserName" runat="server" class="required emptycheck" size="32" onchange="checkUser(this);"
                                    ClientIDMode="Static" TabIndex="1" MaxLength="40" Style="width: 35%" AutoPostBack="false"></asp:TextBox>
                                <asp:Label ID="lblUserExists" runat="server" ForeColor="red" ClientIDMode="Static" />
                            </p>
                            <p>
                                <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email" for="time_entry_activity_id"><span class="required">*</span>E-mail</asp:Label>
                                <asp:TextBox ID="Email" runat="server" class="required email" TabIndex="2" ClientIDMode="Static"
                                    MaxLength="50" Style="width: 35%" AutoPostBack="false"></asp:TextBox>
                                <asp:Label ID="lblEmailExists" runat="server" ForeColor="red" ClientIDMode="Static" />
                            </p>
                            <p>
                                <label for="time_entry_activity_id">
                                    <span class="required">*</span>Title
                                </label>
                                <select id="ddlTitle" style="width: 35%" class="chzn-select required chzn-select"
                                    clientidmode="Static" runat="server" TabIndex="3">
                                    <option></option>
                                </select>
                            </p>
                            <p>
                                <label for="time_entry_activity_id">
                                    <span class="required">*</span>First Name
                                </label>
                                <asp:TextBox ID="txtFirstName" runat="server" Style="width: 35%" ClientIDMode="Static" CssClass="required CharValidation"   TabIndex="4" MaxLength="40"></asp:TextBox>
                               
                            </p>
                            <p>
                                <label for="time_entry_activity_id">
                                    Middle Name
                                </label>
                                <asp:TextBox ID="txtMiddleName" runat="server" Style="width: 35%" ClientIDMode="Static" TabIndex="5" CssClass="CharValidation" MaxLength="40"></asp:TextBox>
                            </p>
                            <p>
                                <label for="time_entry_activity_id">
                                    <span class="required">*</span>Last Name
                                </label>
                                <asp:TextBox ID="txtLastName" runat="server"  ClientIDMode="Static" Style="width: 35%" TabIndex="6"  CssClass=" required CharValidation" MaxLength="40"></asp:TextBox>
                            </p>
                            <p>
                                <label for="time_entry_activity_id">
                                    Address
                                </label>
                                <textarea id="txtAddress" placeholder="Address" runat="server" maxlength="200" clientidmode="static"
                                    tabindex="7" type="text" autocomplete="false" style="width: 35%" rows="10" class="myclass"></textarea>
                            </p>
                            <p>
                                <label for="time_entry_activity_id">
                                    Country
                                </label>
                                <select id="ddlCountry" style="width: 35%" class="chzn-select" clientidmode="Static" TabIndex="8"
                                    onchange="JQSelectBind('ddlCountry','ddlState', 'MstState');setStateValue();JQSelectBind('ddlState','ddlCity', 'MstCity');"
                                    runat="server">
                                    <option></option>
                                </select>
                            </p>
                            <p style="display:none;">
                                <label for="time_entry_activity_id">
                                    State
                                </label>
                                <select id="ddlState" style="width: 35%; display:none;" class="chzn-select" clientidmode="Static" TabIndex="9"
                                    onchange="JQSelectBind('ddlState','ddlCity', 'MstCity');setStateValue();setCityValue();"
                                    runat="server">
                                    <option></option>
                                </select>
                                
                            </p>
                            <p style="display:none;">
                                <label for="time_entry_activity_id">
                                    City
                                </label>
                                <select id="ddlCity" style="width: 35%; display:none;" class="chzn-select" clientidmode="Static" TabIndex="10"
                                    onchange="setCityValue();" runat="server">
                                    <option></option>
                                </select>                               
                            </p>
                            <p>
                                <label for="time_entry_activity_id">
                                    State
                                </label>
                                <asp:TextBox ID="txtStateName" runat="server" placeholder="State Name" MaxLength="500" Style="width: 35%" TabIndex="11"
                                    size="32" aria-autocomplete="list" aria-haspopup="true" autocomplete="off">
                                </asp:TextBox>
                            </p>
                            <p>
                                <label for="time_entry_activity_id">
                                    City
                                </label>
                                <asp:TextBox ID="txtCityName" placeholder="City Name" runat="server" MaxLength="500" Style="width: 35%" TabIndex="12"
                                    size="32" aria-autocomplete="list" aria-haspopup="true" autocomplete="off">
                                </asp:TextBox>
                            </p>
                            <p>
                                <label for="time_entry_activity_id">
                                    <span class="required">*</span>Zip code/Postcode
                                </label>
                                <asp:TextBox ID="txtPinCode" runat="server" class="required" MaxLength="8" Style="width: 35%" TabIndex="13"
                                    size="32" aria-autocomplete="list" aria-haspopup="true" autocomplete="off"></asp:TextBox>
                            </p>
                            <p>
                                <label for="time_entry_activity_id">
                                    Contact Number
                                </label>
                                <asp:TextBox ID="txtPhone" runat="server" MaxLength="20" TabIndex="14" Style="width: 35%"></asp:TextBox>
                            </p>
                            <p>
                                <label for="time_entry_activity_id">
                                   Secondary Contact Number
                                </label>
                                <asp:TextBox ID="txtMobile" runat="server" MaxLength="20" Style="width: 35%"
                                    TabIndex="15"></asp:TextBox>
                            </p>
                            <p>
                                <label for="time_entry_activity_id">
                                    <span class="required">*</span>Department
                                </label>
                                <select id="ddlDepartment" style="width: 35%" class="chzn-select required chzn-select"
                                    clientidmode="Static" runat="server" TabIndex="16">
                                    <option></option>
                                </select>
                            </p>
                            <p>
                                <label for="time_entry_activity_id">
                                    <span class="required">*</span>Role
                                </label>
                                <select id="ddlRole" style="width: 35%" class="chzn-select required chzn-select"
                                    clientidmode="Static" runat="server" TabIndex="17">
                                    <option></option>
                                </select>
                            </p>
                            <p>
                                <label for="time_entry_activity_id">
                                    <span class="required">*</span>Status
                                </label>
                                <input id="rdActive" name="ActiveInactive" runat="server" type="radio" class="required"
                                    checked="true" TabIndex="18"/>Active
                                <input id="rdInactive" TabIndex="19" name="ActiveInactive" runat="server" type="radio" class="required" />Inactive
                            </p>
                            <p>
                                <label for="time_entry_activity_id">
                                    Description</label>
                                <textarea id="txtDescription" placeholder="Description" runat="server" maxlength="200" TabIndex="20"
                                    clientidmode="static" type="text" autocomplete="false" style="width: 35%" rows="10"
                                    class="myclass"></textarea>
                                <em></em>
                            </p>
                            <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="Update" TabIndex="22"
                                class="btn_validate" ValidationGroup="Register" Visible="False" />
                            <asp:Button ID="Users_RegisterButton" runat="server" CommandName="MoveNext" Text="Save" TabIndex="21"
                                ValidationGroup="Register" class="btn_validate exists" OnClick="Users_RegisterButton_Click" />
                            <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back_Click" TabIndex="23"/>
                        </div>
                    </ContentTemplate>
                    <CustomNavigationTemplate>
                    </CustomNavigationTemplate>
                </asp:CreateUserWizardStep>
                <asp:CompleteWizardStep runat="server">
                </asp:CompleteWizardStep>
            </WizardSteps>
        </asp:CreateUserWizard>
    </div>
    <div id="rightlinks" style="display: none;">
        <uc1:UserManagementLinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());

    </script>
</asp:Content>
