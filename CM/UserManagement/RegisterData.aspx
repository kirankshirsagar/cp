﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/CM.master"
    AutoEventWireup="true" CodeFile="RegisterData.aspx.cs" Inherits="RegisterData" %>

<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridSelect.js" type="text/javascript"></script>
    <script src="../CommonScripts/GridNavigation.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");
    </script>
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <h2>
        User Details</h2>
    <asp:HiddenField ID="hdncheckflg" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnPrimeIds" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnUserName" ClientIDMode="Static" runat="server" />

    <asp:HiddenField ID="hdnEmailIdsWithUserName" ClientIDMode="Static" runat="server" />
     <input id="hdnUserRemainingCount" clientidmode="Static" runat="server" type="hidden" />
     <input id="hdnTotalUsersAllowed" clientidmode="Static" runat="server" type="hidden" />


    

    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <div style="margin: 0; padding: 0; display: inline">
        <div id="query_form_content" class="hide-when-print">
            <fieldset id="filters" class="collapsible">
                <legend onclick="toggleFieldset(this);">Filters</legend>
                <div style="">
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td>
                                    <table id="filters-table" width="100%" cellpadding="3" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td width="10%">
                                                    Keywords :
                                                </td>
                                                <td width="10%">
                                                    <input id="txtSearch" clientidmode="Static" runat="server" type="text" maxlength="50" />
                                                </td>
                                                <td width="30%">
                                                    <p>
                                                        <label for="time_entry_activity_id">
                                                            Department :
                                                        </label>
                                                        <select id="ddlDepartment" style="width: 70%" class="chzn-select" clientidmode="Static"
                                                            runat="server">
                                                            <option></option>
                                                        </select>
                                                    </p>
                                                </td>
                                                <td width="40%">
                                                    <p>
                                                        <label for="time_entry_activity_id">
                                                            Role :
                                                        </label>
                                                        <select id="ddlRole" style="width: 50%" class="chzn-select" clientidmode="Static"
                                                            runat="server">
                                                            <option></option>
                                                        </select></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </fieldset>
        </div>
        <p class="buttons hide-when-print">
            <asp:LinkButton ID="btnSearch" runat="server" OnClientClick="return searchClick();"
                CssClass="icon icon-checked" OnClick="btnSearch_Click">Filter</asp:LinkButton>
            <asp:LinkButton ID="btnShowAll" runat="server" OnClientClick="return resetddl();return resetClick();"
                CssClass="icon icon-reload" OnClick="btnSearch_Click">Reset Filter</asp:LinkButton>
            <asp:LinkButton ID="btnAddRecord" CssClass="icon icon-add" runat="server" OnClientClick="return remainingCount(); resetPrimeID(); "
                OnClick="btnAddRecord_Click">Add new User</asp:LinkButton>
            <asp:LinkButton ID="btnDelete" CssClass="icon icon-del" runat="server" OnClientClick="return GetSelectedItems('D');"
                OnClick="btnDelete_Click" Text="Add new country">Delete</asp:LinkButton>
            <asp:LinkButton ID="btnResetPassword" CssClass="icon icon-cancel" runat="server" OnClientClick="return GetSelectedItems('R');"
                OnClick="btnResetPassword_Click" Text="Add new country">Reset Password</asp:LinkButton>
            <asp:LinkButton ID="btnUnlockUser" CssClass="icon icon-unlock" runat="server" OnClientClick="return GetSelectedItems('U');"
                OnClick="btnUnlockUser_Click" Text="Add new country">Unlock</asp:LinkButton>


        </p>
        <div class="autoscroll">
            <table width="100%" class="list issues">
                <asp:Repeater ID="rptUser" runat="server">
                    <HeaderTemplate>
                        <table id="masterDataTable" class="masterTable list issues" width="100%">
                            <thead>
                                <tr>
                                    <th class="checkbox hide-when-print" style="text-align: left; padding-left: 2px;"
                                        width="1%">
                                        <input id="chkSelectAll" type="checkbox" title="Check all/Uncheck all" class="maincheckbox" />
                                    </th>
                                    <th class="checkbox hide-when-print">
                                        &nbsp;
                                    </th>
                                    <th width="0%" style="display: none">
                                        User Id
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnSortUserName" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">User Name</asp:LinkButton>
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnSortName" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Name</asp:LinkButton>
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnSortMoble" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Secondary Contact Number</asp:LinkButton>
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnSortEmailId" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Email</asp:LinkButton>
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnSortDepartment" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Department</asp:LinkButton>
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnSortRole" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Role</asp:LinkButton>
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnSortStatus" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Status</asp:LinkButton>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr id="issue-13874" class="hascontextmenu odd issue status-1 priority-3 child created-by-me">
                            <td class="checkbox hide-when-print">
                                <input type="checkbox" onchange="SetMainCheckbox(this);" class="mid-margin-left" />
                            </td>
                            <td style="text-align: left; padding-bottom: 2px">
                                <img id="imgLock" alt="img.." class="mws-tooltip-e" title="unused" border="0" src="../Images/link_break.png">
                            </td>
                            <td style="display: none">
                                <asp:Label ID="lblUserId" runat="server" ClientIDMode="Static" Text='<%#Eval("UserId") %>'></asp:Label>
                                <asp:Label ID="lblIsUsed" runat="server" ClientIDMode="Static" Text='<%#Eval("isUsed") %>'></asp:Label>
                                <asp:Label ID="lblUserName" runat="server" ClientIDMode="Static" Text='<%#Eval("UserName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:LinkButton ID="imgEdit" runat="server" OnClientClick="return setSelectedId(this);"
                                    OnClick="imgEdit_Click"><%#Eval("UserName")%></asp:LinkButton>
                            </td>
                            <td>
                                <asp:Label ID="lblName" runat="server" ClientIDMode="Static" Text='<%#Eval("FullName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblMobile" runat="server" ClientIDMode="Static" Text='<%#Eval("MobileNo") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblEmailId" runat="server" ClientIDMode="Static" Text='<%#Eval("Email") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblDepartment" runat="server" ClientIDMode="Static" Text='<%#Eval("DepartmentName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblRole" runat="server" ClientIDMode="Static" Text='<%#Eval("RoleName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Label1" runat="server" ClientIDMode="Static" Text='<%#Eval("isActive") %>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
            <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
        </div>
        <table style="display: none" width="100%">
            <tr>
                <td align="left" width="10%">
                </td>
                <td runat="server" id="actinacttd" style="padding-right: 60%" class="labelfont" align="left"
                    width="90%">
                    <input id="rdActive" name="ActiveInactive" runat="server" type="radio" />Active
                    <input id="rdInactive" name="ActiveInactive" runat="server" type="radio" />Inactive
                    <asp:Button ID="btnChangeStatus" runat="server" Text="Status" OnClientClick="return GetSelectedItems('S');"
                        OnClick="btnChangeStatus_Click" />
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">


        function remainingCount() {
            var cnt = parseInt($('#hdnUserRemainingCount').val());
            var TotalUsers = parseInt($('#hdnTotalUsersAllowed').val());
            
            if (cnt <= 0) {
                alert('You already have the maximum ' + TotalUsers + ' number of users allowed.');
                return false;
            }
            else {
                return true;
            }
        }

       


        function resetddl() {
            $('#hdncheckflg').val('');
            $('#txtSearch').val('');
            $('#ddlDepartment').val(0);
            $('#ddlRole').val(0);
        }
        function msgSaveSuccess() {
            alert('success...');
        }


        function GetSelectedItems(flg) {
            $('#hdnPrimeIds').val('');
            $('#hdnUsedNames').val('');

            var tableClass = 'masterTable';
            var deleteLabelId = 'lblUserId';
            var deleteLabelName = 'lblUserName';
            var objCheck = new CkeckBoxSelect(tableClass, deleteLabelId, deleteLabelName);
            var deletedIds = objCheck.DeletedIds;
            var usedNames = objCheck.UsedNames;

            if (deletedIds == '') {
                MessageMasterDiv('Please select record(s).');
                return false;
            }
            else if (usedNames != '' && flg == 'D') {
                MessageMasterDiv('Some records can not be deleted.' + usedNames, 1);
                return false;
            }

            if (flg == 'R') {
                GetEmailAndUserName();
            }

            if (flg == 'D') {
                if (DeleteConfrim() == true) {
                    $('#hdnPrimeIds').val(deletedIds);
                    $('#hdnUserName').val(usedNames);
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                $('#hdnPrimeIds').val(deletedIds);
                $('#hdnUserName').val(usedNames);
                return true;
            }
            $('#hdnPrimeIds').val(deletedIds);
            $('#hdnUserName').val(usedNames);

            return true;
        }


        $(document).ready(function () {
            $(document).on('keypress', function (e) {
                if (e.which === 13)
                    e.preventDefault();
            });
            defaultTableValueSetting('lblUserId', 'MstUser', 'UserId');
            LockUnLockImage('masterTable');

        });

        function setSelectedId(obj) {
            var pId = $(obj).closest('tr').find('span[id *= lblUserId]').text();

            if (pId == '' || pId == '0') {
                return false;
            }
            else {
                $('#hdnPrimeIds').val(pId);
            }
        }



        function GetEmailAndUserName() {
            var cnt = 0;
            var emailAndUserName = "";
            $("#masterDataTable").find('tbody').find('td').find(':checkbox').each(function () {
                if ($(this).is(":checked")) {
                    var emailId = ($(this).closest('tr').find('[id^="lblEmailId"]').html());
                    var userName = ($(this).closest('tr').find('[id^="lblUserName"]').html());

                    if (emailId != null && userName != null) {
                        if (cnt == 0) {
                            emailAndUserName = emailId + '#' + userName;
                        }
                        else {
                            emailAndUserName += ',' + emailId + '#' + userName;
                        }
                    }
                    else {
                        cnt--;
                    }
                    cnt++;
                }
            });
            $('#hdnEmailIdsWithUserName').val(emailAndUserName);
        }

    </script>
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
</asp:Content>
