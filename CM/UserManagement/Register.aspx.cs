﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;
using System.IO;
using MD = System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing;
using UserManagementBLL;
using System.Net.Mail;

using System.Net;
using System.Configuration;
using System.Text;
using System.Web.UI.HtmlControls;

public partial class Users_Register : System.Web.UI.Page
{
    IUsers objUsers;
    IState objState;
    ICountry objCountry;
    ICity objCity;
    IRole objRole;
    IDepartment objDepartment;
    ITitle objTitle;
  

    //protected override void OnInit(EventArgs e)
    //{
    //    base.OnInit(e);
    //    CreateObjects();
    //    if (!IsPostBack)
    //    {
    //        TitleBind();
    //        CountryBind();
    //        RoleBind();
    //        DepartmentBind();
    //    }
    //}

    protected void Page_Load(object sender, EventArgs e)
    {

        CreateObjects();

        if (!IsPostBack)
        {
            TitleBind();
            CountryBind();
            RoleBind();
            DepartmentBind();
        }



        if (!IsPostBack)
        {
            if (String.IsNullOrEmpty(Request.extValue("hdnPrimeIds")) != true)
            {
                HiddenField hdnPrimeId = (HiddenField)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("hdnPrimeId");
                hdnPrimeId.Value = Request.extValue("hdnPrimeIds");
                ReadData();
                Button Users_RegisterButton = (Button)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("Users_RegisterButton");
                Button btnUpdate = (Button)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("btnUpdate");
                btnUpdate.Visible = true;
                Users_RegisterButton.Visible = false;
                HtmlInputHidden hdnPhotoId = (HtmlInputHidden)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("hdnPhotoId");
                System.Web.UI.WebControls.Image imgUpload = (System.Web.UI.WebControls.Image)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("imgUpload");

                imgUpload.ImageUrl = hdnPhotoId.Value;
                System.Web.UI.WebControls.Label lblTitle = (System.Web.UI.WebControls.Label)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("lblTitle");
                lblTitle.Text = "Edit User";
                TextBox Email =(TextBox)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("Email");
                Email.Text = "";
            }
            else
            {
                HiddenField hdnPrimeId = (HiddenField)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("hdnPrimeId");
                hdnPrimeId.Value = "0";
                System.Web.UI.WebControls.Label lblTitle = (System.Web.UI.WebControls.Label)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("lblTitle");
                lblTitle.Text = "Create User";


            }
            if (String.IsNullOrEmpty(Request.extValue("hdnPrimeIds")) != true)
            {
                HiddenField hdnPrimeId = (HiddenField)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("hdnPrimeId");
            }
        }
       

    }

    protected void Back_Click(object sender, EventArgs e)
    {
        Response.Redirect("RegisterData.aspx");
    }
    private bool UserExists(string username)
    {
        //***************************************
        IUsers obj = FactoryUser.GetUsersDetail();
        obj.CreateDALObjForMembershipWork();
        //****************************************

        if (Membership.GetUser(username) != null) { return true; }

        return false;
    }

    protected void RegisterUser_CreatedUser(object sender, EventArgs e)
    {
        InsertUpdate();
    }

    protected void Users_RegisterButton_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");

        ClearData();
        Page.TraceWrite("Save Update button clicked event ends.");
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objUsers = FactoryUser.GetUsersDetail();
            objState = FactoryMaster.GetStateDetail();
            objCountry = FactoryMaster.GetCountryDetail();
            objCity = FactoryMaster.GetCityDetail();
            objDepartment = FactoryUser.GetDepartmentDetail();
            objRole = FactoryUser.GetRoleDetail();
            objTitle = FactoryMaster.GetTitleDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }
    protected void btnCreateAndContinue_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save and Add more button clicked event starts.");
        InsertUpdate();
        ClearData();
        Page.TraceWrite("Save and Add more button clicked event ends.");
    }


    void ReadData()
    {
        Page.TraceWrite("ReadData starts.");
        try
        {

            HiddenField hdnPrimeId = (HiddenField)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("hdnPrimeId");
            objUsers.UserId = int.Parse(hdnPrimeId.Value);
            List<Users> Users_Register = objUsers.ReadData();

            TextBox txtFirstName = (TextBox)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtFirstName");
            TextBox txtMiddleName = (TextBox)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtMiddleName");
            TextBox txtLastName = (TextBox)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtLastName");
            TextBox txtPhone = (TextBox)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtPhone");
            HtmlTextArea txtAddress = (HtmlTextArea)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtAddress");
            HtmlTextArea txtDescription = (HtmlTextArea)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtDescription");
            TextBox txtPinCode = (TextBox)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtPinCode");
            TextBox txtUserName = (TextBox)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("UserName");
           
            TextBox txtMobile = (TextBox)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtMobile");


            System.Web.UI.WebControls.Image imgUpload = (System.Web.UI.WebControls.Image)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("imgUpload");

            HtmlSelect ddlCountry = (HtmlSelect)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("ddlCountry");
            TextBox txtStateName = (TextBox)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtStateName");
            TextBox txtCityName = (TextBox)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtCityName");
            //HtmlSelect ddlState = (HtmlSelect)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("ddlState");
            //HtmlSelect ddlCity = (HtmlSelect)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("ddlCity");
            HtmlSelect ddlRole = (HtmlSelect)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("ddlRole");
            HtmlSelect ddlDepartment = (HtmlSelect)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("ddlDepartment");
            HtmlSelect ddlTitle = (HtmlSelect)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("ddlTitle");

            HtmlInputRadioButton rdactive = (HtmlInputRadioButton)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("rdActive");
            HtmlInputRadioButton rdinactive = (HtmlInputRadioButton)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("rdInactive");
            if (Users_Register[0].isActive == "Y")
            {

                rdactive.Checked = true;
                rdinactive.Checked = false;
            }
            else
            {

                rdinactive.Checked = true;
                rdactive.Checked = false;
            }
            txtFirstName.Text = Users_Register[0].FirstName;
            txtMiddleName.Text = Users_Register[0].MiddleName;
            txtLastName.Text = Users_Register[0].LastName;
            txtAddress.Value = Users_Register[0].Address;
            txtPhone.Text = Users_Register[0].Phone;
            txtPinCode.Text = Users_Register[0].Pincode;

            txtStateName.Text = Users_Register[0].StateName;
            txtCityName.Text = Users_Register[0].CityName;
            RegisterUser.UserName = Users_Register[0].UserName;
            hdnOldUserName.Value = Users_Register[0].UserName;
            txtUserName.Text = Users_Register[0].UserName;
            txtUserName.Enabled = false;
            RegisterUser.Email = Users_Register[0].Email;
            txtMobile.Text = Users_Register[0].MobileNo;
            txtDescription.Value = Users_Register[0].Description;
            hdnStateNameReadDisable.Value = Users_Register[0].StateName;
            hdnCityNameReadDisable.Value = Users_Register[0].CityName;

            HtmlInputHidden hdnPhotoId = (HtmlInputHidden)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("hdnPhotoId");
            if (Users_Register[0].ImageUrl != "")
            {
                hdnPhotoId.Value = Users_Register[0].ImageUrl;
            }
            else
                hdnPhotoId.Value = "../Images/userdefault.jpg";
            ddlCountry.extSelectedValues(Users_Register[0].CountryId.ToString());
            HtmlInputHidden hdnStateIdRead = (HtmlInputHidden)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("hdnStateIdRead");
            HtmlInputHidden hdnCityIdRead = (HtmlInputHidden)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("hdnCityIdRead");

            hdnStateIdRead.Value = Users_Register[0].StateId.ToString();
            hdnCityIdRead.Value = Users_Register[0].CityId.ToString();
            ddlRole.extSelectedValues(Users_Register[0].RoleId.ToString(), Users_Register[0].RoleName);
            ddlDepartment.extSelectedValues(Users_Register[0].DepartmentId.ToString(), Users_Register[0].DepartmentName);
            ddlTitle.extSelectedValues(Users_Register[0].TitleId.ToString(), Users_Register[0].TitleName);
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData ends.");
    }

    void TitleBind()
    {
        Page.TraceWrite("Title dropdown bind starts.");
        try
        {
            HtmlSelect ddlTitle = (HtmlSelect)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("ddlTitle");
            ddlTitle.extDataBind(objTitle.SelectData());
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Title dropdown bind ends.");
    }
    void CountryBind()
    {
        Page.TraceWrite("Country dropdown bind starts.");
        try
        {
            HtmlSelect ddlCountry = (HtmlSelect)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("ddlCountry");
            ddlCountry.extDataBind(objCountry.SelectData());
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Country dropdown bind ends.");
    }
    void CityBind()
    {
        Page.TraceWrite("City dropdown bind starts.");
        try
        {
            HtmlSelect ddlCity = (HtmlSelect)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("ddlCity");
            ddlCity.extDataBind(objCity.SelectData());
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("City dropdown bind ends.");
    }
    void StateBind()
    {
        Page.TraceWrite("State dropdown bind starts.");
        try
        {
            HtmlSelect ddlState = (HtmlSelect)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("ddlState");
            ddlState.extDataBind(objState.SelectData());
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("State dropdown bind ends.");
    }
    void RoleBind()
    {
        Page.TraceWrite("Role dropdown bind starts.");
        try
        {
            HtmlSelect ddlRole = (HtmlSelect)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("ddlRole");
            ddlRole.extDataBind(objRole.SelectData());
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Role dropdown bind ends.");
    }
    void DepartmentBind()
    {
        Page.TraceWrite("Department dropdown bind starts.");
        try
        {
            HtmlSelect ddlDepartment = (HtmlSelect)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("ddlDepartment");
            ddlDepartment.extDataBind(objDepartment.SelectData());
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Department dropdown bind ends.");
    }

    void InsertUpdate(int flg = 0)
    {
        Page.TraceWrite("Insert, Update starts.");
        string status = "";

        TextBox txtFirstName = (TextBox)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtFirstName");
        TextBox txtMiddleName = (TextBox)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtMiddleName");
        TextBox txtLastName = (TextBox)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtLastName");
        TextBox txtPhone = (TextBox)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtPhone");
        TextBox txtPinCode = (TextBox)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtPinCode");
        TextBox txtUserName = (TextBox)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("UserName");
        TextBox txtMobile = (TextBox)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtMobile");
        HtmlTextArea txtAddress = (HtmlTextArea)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtAddress");
        HtmlTextArea txtDescription = (HtmlTextArea)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtDescription");
        HtmlSelect ddlCountry1 = (HtmlSelect)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("ddlCountry");

        TextBox txtStateName = (TextBox)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtStateNAme");
        TextBox txtCityName = (TextBox)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtCityName");

        objUsers.FirstName = txtFirstName.Text;
        objUsers.MiddleName = txtMiddleName.Text;
        objUsers.LastName = txtLastName.Text;
        objUsers.Phone = txtPhone.Text;
        objUsers.Pincode = txtPinCode.Text;
        objUsers.UserName = txtUserName.Text.Trim() != string.Empty ? txtUserName.Text.Trim() : hdnOldUserName.Value;
        objUsers.MobileNo = txtMobile.Text;
        objUsers.Address = txtAddress.Value;
        objUsers.Description = txtDescription.Value;
        objUsers.CountryId = int.Parse(ddlCountry1.Value);
        HtmlInputHidden hdnPhotoId = (HtmlInputHidden)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("hdnPhotoId");
        if (hdnPhotoId.Value != "")
            objUsers.ImageUrl = hdnPhotoId.Value;
        else
            objUsers.ImageUrl = "";
        //HtmlInputHidden hdnStateId = (HtmlInputHidden)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("hdnStateId");
        //if (hdnStateId.Value != "")
        //    objUsers.StateId = int.Parse(hdnStateId.Value);
        //else
        //    objUsers.StateId = 0;
        //HtmlInputHidden hdnCityId = (HtmlInputHidden)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("hdnCityId");
        //if (hdnCityId.Value != "")
        //    objUsers.CityId = int.Parse(hdnCityId.Value);
        //else
        //    objUsers.CityId = 0;

        objUsers.StateName = txtStateName.Text.Trim();
        objUsers.CityName = txtCityName.Text.Trim();
        objUsers.Email = RegisterUser.Email;

        HtmlSelect ddlRole = (HtmlSelect)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("ddlRole");
      
        objUsers.RoleId = int.Parse(ddlRole.Value);
        HtmlSelect ddlDepartment = (HtmlSelect)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("ddlDepartment");
        objUsers.DepartmentId = int.Parse(ddlDepartment.Value);
        HtmlSelect ddlTitle = (HtmlSelect)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("ddlTitle");
        objUsers.TitleId = int.Parse(ddlTitle.Value);

        objUsers.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objUsers.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objUsers.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;

        HtmlInputRadioButton rdactive = (HtmlInputRadioButton)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("rdActive");
        HtmlInputRadioButton rdinactive = (HtmlInputRadioButton)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("rdInactive");

        if (rdactive.Checked == true)
        {
            objUsers.isActive = "Y";

        }
        else
        {
            objUsers.isActive = "N";
        }

        HiddenField hdnPrimeId = (HiddenField)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("hdnPrimeId");
        HiddenField HdnEmailExists = (HiddenField)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("HdnEmailExists");
        HiddenField HdnUserExists = (HiddenField)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("HdnUserExists");
        
        try
        {
            if (hdnPrimeId.Value == "0")
            {
                if (HdnEmailExists.Value == "" && HdnUserExists.Value == "")
                {
                    objUsers.UserId = 0;
                    status = objUsers.UpdateRecord();
                }
                else
                {
                    if (HdnEmailExists.Value != "")
                        Page.Message(status, HdnEmailExists.Value);
                    if (HdnUserExists.Value != "")
                        Page.Message(status, HdnEmailExists.Value);
                    if (HdnEmailExists.Value != "" && HdnUserExists.Value != "")
                        Page.Message(status, HdnEmailExists.Value + "," + HdnUserExists.Value);

                }
            }
            else
            {
               
                if (HdnEmailExists.Value == "" && HdnUserExists.Value == "")
                {
                    objUsers.UserId = int.Parse(hdnPrimeId.Value);
                    status = objUsers.UpdateRecord();
                    if (flg == 0 && status == "1")
                    {
                        Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";

                    }
                    Response.Redirect("RegisterData.aspx");
                }
                else
                {
                    if (HdnEmailExists.Value != "")
                        Page.Message(status, HdnEmailExists.Value);
                    if (HdnUserExists.Value != "")
                        Page.Message(status, HdnEmailExists.Value);
                    if (HdnEmailExists.Value != "" && HdnUserExists.Value != "")
                        Page.Message(status, HdnEmailExists.Value + "," + HdnUserExists.Value);

                }
            }
        }
        catch (Exception ex)
        {
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Insert, Update ends.");
        if (flg == 0 && status == "1")
        {
            Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
            
        }
        //ClearData(status);
    }

    void ClearData()
    {
        Page.TraceWrite("clearData starts.");
        //objState.StateId = 0;
        //hdnPrimeId.Value = "0";
        //txtFirstName.Value = "";
        //txtMiddleName.Value = "";
        //txtLastName.Value = "";
        //txtAddress.Value = "";
        //txtPhone.Value = "";
        //txtPinCode.Value = "";
        //txtUserName.Value = "";
        //txtEmailId.Value = "";
        //txtMobile.Value = "";
        //txtDescription.Value = "";
        Page.TraceWrite("clear Data ends.");
    }


    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string status = "";
         HiddenField HdnEmailExists = (HiddenField)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("HdnEmailExists");
         HiddenField HdnUserExists = (HiddenField)this.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("HdnUserExists");
         if (HdnEmailExists.Value == "" && HdnUserExists.Value == "")
         {
             InsertUpdate();
         }
         else
         {
             if (HdnEmailExists.Value != "")
             Page.Message(status, HdnEmailExists.Value);
             if (HdnUserExists.Value != "")
                 Page.Message(status, HdnEmailExists.Value);
             if (HdnEmailExists.Value != "" && HdnUserExists.Value != "")
                 Page.Message(status, HdnEmailExists.Value + "," + HdnUserExists.Value);

         }
    }


    protected void RegisterUser_SendingMail(object sender, MailMessageEventArgs e)
    {
         e.Cancel = true;

         System.Net.Mail.MailMessage objEmail = new System.Net.Mail.MailMessage();
         string mailServer = System.Configuration.ConfigurationSettings.AppSettings["mailserver"];
         string usernm = System.Configuration.ConfigurationSettings.AppSettings["UserName"];
         string Password = System.Configuration.ConfigurationSettings.AppSettings["Password"];
         Int32 Port = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["Port"]);
         string MailFrom = ConfigurationSettings.AppSettings["MailFrom"];
       
         string DisplayName = ConfigurationSettings.AppSettings["DisplayName"];
         SmtpClient objSC = new SmtpClient(mailServer, Port);
         NetworkCredential cre = new NetworkCredential(usernm, Password);
         objSC.UseDefaultCredentials = false;
         objSC.Credentials = cre;
         objEmail.From = new System.Net.Mail.MailAddress(MailFrom, DisplayName);
         objEmail.Subject = "ContractPod™ - New User";
         objEmail.To.Add(new System.Net.Mail.MailAddress(RegisterUser.Email));
         string recoveryLink = HttpContext.Current.Request.Url.AbsoluteUri.Remove(HttpContext.Current.Request.Url.AbsoluteUri.Length - HttpContext.Current.Request.Url.Segments.Last().Length);


         recoveryLink = recoveryLink.TrimStart('{').TrimEnd('}');

     

         recoveryLink = recoveryLink + "GeneratePassword.aspx" + "?UserId=" + RegisterUser.Email;
         objUsers.UserName = RegisterUser.UserName;
         string FullName = objUsers.GetUserFirstName();
         StringBuilder strBody = new StringBuilder();
         strBody.Append("<font face='Arial' size='2' color='#006666'>Hi ");
        strBody.Append(FullName);
         //strBody.Append(System.Text.RegularExpressions.Regex.Split(FullName, " ")[0]);
         //strBody.Append(",");         
         strBody.Append("<br><br> Welcome to ContractPod™! Your login details have been created successfully.  Your username is:<br><br>");
         strBody.Append("<b>" + RegisterUser.UserName + "</b> &nbsp;");
        strBody.Append("<br><br>Now all you need is a password to get started.  To set one up and access your account please click here:");
        strBody.Append("<br><a href='" + recoveryLink + "' moz-do-not-send='true'>" + recoveryLink + "</a>");
        strBody.Append("<br><br>Alternatively, you can copy and paste the link into your browser's address.<br>We hope you enjoy using ContractPod™!");

        strBody.Append("<br><br>The ContractPod™ team</font>");
        //strBody.Append("<br><img src='"+ Strings.domainURL() +"images/cmlogoemail.jpg'><br>");
        strBody.Append("<br><br><font face='Arial' size='1' color='#006666'>Need help? Please do not hesitate to contact our technical support team on 0800 699 0045 or at help@contractpod.com.");
        strBody.Append("<br>The information in this email is confidential and for use by the addressee(s) only. If you are not the intended recipient, please notify us immediately on 0800 699 0045 and delete the message from your computer. You may not copy or forward the e-mail, or use it or disclose its contents to any other person. We do not accept any liability or responsibility for changes made to this email after it was sent, or viruses transmitted through this e-mail or any attachment.</font>");
        
         string Body = strBody.ToString();
         objEmail.Body = Body;
         AlternateView htmlView = AlternateView.CreateAlternateViewFromString(Body, null, "text/html");
         objEmail.AlternateViews.Add(htmlView);
        try
        {
            objSC.Send(objEmail);
            LogPasswordActivity(RegisterUser.Email, RegisterUser.UserName);

        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception is:" + ex.ToString());
        }


        Response.Redirect("RegisterData.aspx");
    }


    private void LogPasswordActivity(string emailId, string userName)
    {
        objUsers.UserName = userName;
        objUsers.Email = emailId;
        objUsers.MailType = "New User";
        objUsers.UserPasswordActivityLog();
    }



}
