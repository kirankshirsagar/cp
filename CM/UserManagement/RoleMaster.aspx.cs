﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UserManagementBLL;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;


public partial class Masters_RoleMaster : System.Web.UI.Page
{

    IRole objRole;
    public string Add;
    public string View;
    public string Update;
    public string Delete;
    public static int RecordsPerPage = 50;
    public string isRoleUsed = "Y";

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();

        if (!IsPostBack)
        {

            if (Session[Declarations.User] != null)
            {
                Access.PageAccess(this, Session[Declarations.User].ToString(), "usermanagement_");
                ViewState[Declarations.Add] = Access.Add.Trim();
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
            }
            AccessVisibility();

            ReportingBind();
            Message();
            if (Request.QueryString["RoleId"] != null && String.IsNullOrEmpty(Request.extValue("hdnPrimeIds")) == true)
            {
                hdnPrimeId.Value = Request.QueryString["RoleId"].ToString();
                ReadData();
                lblTitle.Text = "Edit Role";
                if (isRoleUsed == "Y" || Delete == "N")
                {
                    btnDelete.Visible = false;
                }
                else
                {
                    btnDelete.Visible = true;
                }
                btnSave.Text = "Update";

                if (hdnPrimeId.Value == "1")
                {
                    rdActive.Checked = true;
                    rdActive.Disabled = true;
                    rdInactive.Disabled = true;
                }

            }

            else
            {
                rdActive.Checked = true;
                hdnPrimeId.Value = "0";
                lblTitle.Text = "Add Role";
                btnDelete.Visible = false;
                btnSave.Text = "Save";
            }
        }
    }



    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objRole = FactoryUser.GetRoleDetail();

        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        InsertUpdate();
        Page.TraceWrite("Save Update button clicked event ends.");
    }

    protected void SaveAndContinue_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save and Add more button clicked event starts.");
        InsertUpdate(1);
        Page.TraceWrite("Save and Add more button clicked event ends.");
        ReportingBind();
    }

    protected void Back_Click(object sender, EventArgs e)
    {
        Server.Transfer("RoleMasterData.aspx");
    }



    void ReadData()
    {
        Page.TraceWrite("ReadData starts.");
        try
        {
            objRole.RoleId = int.Parse(hdnPrimeId.Value);
            List<Role> Role = objRole.ReadData();
            txtRoleName.Value = Role[0].RoleName;
            txtDescription.Value = Role[0].Description;
            try
            {
                ddlReporting.extDataBind(objRole.SelectReportingTo());
                ddlReporting.Value = Role[0].ReportingId.ToString();
            }
            catch { }
            btnSaveAndContinue.Visible = false;
            isRoleUsed = objRole.IsRoleUsed();

            if (Role[0].isActive == "Y")
            {
                rdActive.Checked = true;
            }
            else
            {
                rdInactive.Checked = true;
            }



        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData ends.");
    }


    void ReportingBind()
    {
        Page.TraceWrite("Country dropdown bind starts.");
        try
        {
            ddlReporting.extDataBind(objRole.SelectData(1));
            ddlReporting.extSelectedValues("1");
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Country dropdown bind ends.");
    }


    void InsertUpdate(int flg = 0)
    {
        Page.TraceWrite("Insert, Update starts.");
        string status = "";

        objRole.RoleName = txtRoleName.Value.Trim();
        objRole.Description = txtDescription.Value.Trim();
        objRole.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objRole.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objRole.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
        objRole.isActive = rdActive.Checked == true ? "Y" : "N";
        objRole.ReportingId = int.Parse(string.IsNullOrEmpty(ddlReporting.Value) == true ? "0" : (ddlReporting.Value).ToString());

        try
        {
            if (hdnPrimeId.Value == "0")
            {
                objRole.RoleId = 0;
                status = objRole.InsertRecord();

            }
            else
            {
                objRole.RoleId = int.Parse(hdnPrimeId.Value);
                status = objRole.UpdateRecord();
            }

            Page.Message(status, hdnPrimeId.Value);
        }
        catch (Exception ex)
        {
            Page.Message("0", hdnPrimeId.Value);
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

        Page.TraceWrite("Insert, Update ends.");
        if (flg == 0 && status == "1")
        {
            Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
            Response.Redirect("RoleMasterData.aspx");
        }
        ClearData(status);

    }

    void ClearData(string status)
    {
        if (status == "1")
        {
            Page.TraceWrite("clearData starts.");
            objRole.RoleId = 0;
            hdnPrimeId.Value = "0";
            txtDescription.Value = "";
            txtRoleName.Value = "";
            ddlReporting.SelectedIndex = -1;
            Page.TraceWrite("clear Data ends.");
        }
    }

    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }


    void AccessVisibility()
    {
        setAccessValues();

        Page.TraceWrite("AccessVisibility starts.");
        if (Add == "N" && (hdnPrimeId.Value == "" || hdnPrimeId.Value == "0" || hdnPrimeId.Value == null))
        {
            Page.extDisableControls();
            btnBack.Enabled = true;
        }
        if (View == "N")
        {
            btnBack.Enabled = false;
        }
        Page.TraceWrite("AccessVisibility starts.");
    }



    protected void btnDelete_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Delete starts.");
        string status = "";
        objRole.RoleIds = hdnPrimeId.Value;
        try
        {
            status = objRole.DeleteRecord();
        }
        catch (Exception ex)
        {
            hdnPrimeId.Value = string.Empty;
            Page.JavaScriptClientScriptBlock("status", "PageGetMessage('DF')");
            Page.TraceWarn("Delete fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        if (status == "-1" || status == "")
        {
            hdnPrimeId.Value = string.Empty;
            Session[Declarations.Message] = "DF";
            Page.JavaScriptClientScriptBlock("status", "msgDeleteFail('DF')");
        }
        else
        {
            hdnPrimeId.Value = string.Empty;
            Session[Declarations.Message] = "DS";
            Page.JavaScriptClientScriptBlock("status", "PageGetMessage('DS')");
            Page.TraceWrite("Delete ends.");
        }
        ReadData();
        Response.Redirect("RoleMasterData.aspx");
    }
    void Message()
    {
        if (Session[Declarations.Message] != null)
        {
            var flg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }

    }
}










