﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using UserManagementBLL;
using System.Text;
using System.Net.Mail;
using System.Security.Cryptography;

public partial class Account_ResetPassword : System.Web.UI.Page
{

    private string userName = "";
    private string userID = "";
   


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            errorExplanation.Style.Add("display", "none");
            flash_notice.Style.Add("display", "none");
            try
            {
                if (Request.QueryString["UserId"] != null)
                {
                    userID = Request.QueryString["UserId"].ToString();

                    Users objusr = new Users();
                    objusr.Email=userID;
                    objusr.UserName = objusr.GetEmailId();
                    string struId = userID;// objusr.GetUserID();
                    //Session["userID"] = struId;

                    int failedPWds = 0;
                    failedPWds = int.Parse(objusr.getTotalLoginfailed().ToString());
                    if (isValidLink(objusr.UserName) == "N")
                    {
                        flash_notice.Style.Add("display", "none");
                        errorExplanation.Style.Add("display", "block");
                        errorExplanation.Attributes.Add("id", "flash_notice");
                        errorExplanation.Attributes.Add("class", "errorExplanation");
                        errorExplanation.InnerHtml = "This link has been expired.";
                        errorExplanation.Style.Add("fadeout", "slow");
                        divResetPassword.Visible = false;
                    }

                    else if (failedPWds >= 10)
                    {
                        objusr.GetUsersDetails();
                        objusr.UserIds = (objusr.UserId).ToString();
                        objusr.LockUser();
                        flash_notice.Style.Add("display", "none");
                        errorExplanation.Style.Add("display", "block");
                        errorExplanation.Attributes.Add("id", "flash_notice");
                        errorExplanation.Attributes.Add("class", "errorExplanation");
                        errorExplanation.InnerHtml = "Your login has been locked! Please contact your administrator.";
                        errorExplanation.Style.Add("fadeout", "slow");
                        divResetPassword.Visible = false;
                    }
                    else
                    {
                        divResetPassword.Visible = true;
                        hdnusername.Value = objusr.UserName;
                        if (objusr.UserName == "")
                        {
                            Response.Redirect("~/Login.aspx");
                        }
                    }
                   
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Login.aspx");
            }
        }
    }
   
    protected void btnChangePassword_Click(object sender, EventArgs e)
    {
        //***************************************
        IUsers obj = FactoryUser.GetUsersDetail();
        obj.CreateDALObjForMembershipWork();
        //****************************************

        if (hdnusername.Value != "")
        {
            MembershipUser user = Membership.GetUser(hdnusername.Value);
            string PUserID = user.ProviderUserKey.ToString();
            bool flag = Membership.ValidateUser(hdnusername.Value, txtNewPassword.Text);
            string oldpswd = user.ResetPassword();
            string newpass = txtNewPassword.Text;
            bool isupper = false;
            bool isLower = false;
            // bool isNumber = false;
            bool isSpecial = false;
            bool isQuotation = false;
            
            char[] val = newpass.ToCharArray();

            char[] spl = "!@#$%^&*(,).1234567890'\"".ToCharArray();

            for (int i = 0; i < val.Length; i++)
            {

                if (char.IsLower(val[i]) == true)
                {
                    isLower = true;
                }
                if (char.IsUpper(val[i]) == true)
                {
                    isupper = true;
                }

                for (int j = 0; j < spl.Length; j++)
                {
                    if (val[i] == spl[j])
                    {
                        isSpecial = true;
                    }
                }
                if (val[i] == '\'' || val[i] == '\"')
                {
                    isQuotation = true;
                }
            }
            string UserNamePart = hdnusername.Value.Substring(0, 4);// getting first 4 char of Username

            if (flag)
            {
                flash_notice.Style.Add("display", "none");
                errorExplanation.Style.Add("display", "block");
                errorExplanation.Attributes.Add("id", "flash_notice");
                errorExplanation.Attributes.Add("class", "errorExplanation");
                errorExplanation.InnerHtml = "The entered password is same as current password. Please enter different password.";
              
                return;

            }
            if (newpass.Length < 8 || isupper == false || isLower == false || isSpecial == false)
            {

                flash_notice.Style.Add("display", "none");
                errorExplanation.Style.Add("display", "block");
                errorExplanation.Attributes.Add("id", "flash_notice");
                errorExplanation.Attributes.Add("class", "errorExplanation");
                errorExplanation.InnerHtml = "At least 8 characters long and contain at least one of each of the following: </br> § Uppercase letter </br> § Lowercase letter </br> § Special character or Number </br>";

                return;
            
            }
            if (isQuotation)
            {
                errorExplanation.Style.Add("display", "block");
                errorExplanation.Style.Add("Color", "Red");
                errorExplanation.Attributes.Add("class", "error mws-form-message");
                errorExplanation.InnerHtml = "The characters \" (double quote) and \' (single quote) are not allowed in passwords.";
                return;
            }
            if (newpass.ToLower().Contains(UserNamePart.ToLower()))
            {
                errorExplanation.Style.Add("display", "block");
                errorExplanation.Style.Add("Color", "Red");
                errorExplanation.Attributes.Add("class", "error mws-form-message");
                errorExplanation.InnerHtml = "New password should not contain a part of user name.";
                return;
            }
            if (newpass.ToLower().Contains(hdnusername.Value.ToLower()))
            {
                flash_notice.Style.Add("display", "none");
                errorExplanation.Style.Add("display", "block");
                errorExplanation.Style.Add("Color", "Red");
                errorExplanation.Attributes.Add("class", "error mws-form-message");
                errorExplanation.InnerHtml = "New password should not contain user name.";
                return;
            }
            user.ChangePassword(oldpswd, newpass);
            Users objuser = new Users();
            objuser.UserName=hdnusername.Value;
            objuser.PasswordChangedSuccessfully();
            objuser.UpdateUserPasswordExpireStatusConfirm();
            LogPasswordActivity(hdnusername.Value);

            flash_notice.Style.Add("display", "block");
            flash_notice.Attributes.Add("class", "flash notice");
            flash_notice.InnerHtml = "Your password has been changed successfully";
            errorExplanation.Style.Add("display", "none");
            lnklogin.Visible = true;

            tblpwd.Visible = false;
            btnChangePassword.Visible = false;
            BtnLogout.Visible = false;
        }
    }

    protected void BtnLogout_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Login.aspx");
    }

    private void LogPasswordActivity(string userName)
    {
        IUsers objuser = FactoryUser.GetUsersDetail();
        objuser.UserName = userName;
        objuser.UserPasswordActivityLog(1);
    }

    private string isValidLink(string userName)
    {
        IUsers objuser = FactoryUser.GetUsersDetail();
        objuser.UserName = userName;
        return objuser.IsValidLink();
    }

}