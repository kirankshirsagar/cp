﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="RolePermissions.aspx.cs" Inherits="Access_RolePermissions" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="UserManagementLinks"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script language="javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");
    </script>
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <asp:HiddenField ID="hdnContractTypeNameVIEW" ClientIDMode="Static" runat="server" Value="N" />
    <asp:HiddenField ID="hdnReportsVIEW" ClientIDMode="Static" runat="server" Value="N" />
    <table style="width: 100%">
        <tr>
            <td style="width: 25%; padding-right: 10px; text-align: right">
                <label>
                    Role
                </label>
            </td>
            <td style="width: 1%">
                :
            </td>
            <td style="width: 64%; padding-left: 10px">
                <asp:DropDownList ID="ddlRole" Width="40%" AutoPostBack="true" CssClass="chzn-select required"
                    ClientIDMode="Static" runat="server" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td style="width: 10%">
            </td>
        </tr>
    </table>
    <div class="autoscroll">
        <table width="100%">
            <asp:Repeater ID="rptParent" runat="server" >
                <HeaderTemplate>
                    <table id="masterDataTable" class="masterTable list issues" width="100%" cellpadding="5">
                        <thead>
                            <th style="width: 20%">
                                Main Section
                            </th>
                            <th style="width: 63.6%">
                                Sub Section
                            </th>
                            <th style="width: 16.4%">
                            </th>
                        </thead>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="clickMe" style="background-color: ButtonFace">
                        <td valign="top">
                            <span class="hiddenInformationClass" style="display: none; color: Red; font-weight: bold">
                                P<%#Eval("ParentId")%></span><asp:Label ID="lblParentId" Visible="false" runat="server" Text='<%# Eval("ParentId") %>'></asp:Label>
                            <b>
                                <asp:Label ID="lblParentName" runat="server" Text='<%# Eval("ParentName") %>'></asp:Label></b>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <input id="chkParentAll" onclick="return parentRowClick(this);" runat="server" clientidmode="Static"
                                type="checkbox" />
                            All
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                        </td>
                        <td valign="top" colspan="2">
                            <div class="=toggleMe">
                                <asp:Repeater ID="rptChild" runat="server">
                                    <ItemTemplate>
                                        <table id="childTable" width="100%">
                                            <tr class="AllCheckBoxClass">
                                                <td style="width: 40%">
                                                    <span class="hiddenInformationClass" style="display: none; color: Purple; font-weight: bold">
                                                        <%#Eval("ChildId")%>
                                                    </span>
                                                    <asp:Label ID="lblParentId" Visible="false" runat="server" Text='<%# Eval("ParentId") %>'></asp:Label>
                                                    <asp:Label ID="lblChildId" Visible="false" runat="server" Text='<%# Eval("ChildId") %>'></asp:Label>
                                                    <asp:Label ID="lblChildName" runat="server" Text='<%# Eval("ChildName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hdnAddReadOnly" ClientIDMode="Static" runat="server" Value='<%# Eval("AddReadOnly") %>'>
                                                    </asp:HiddenField>
                                                    <asp:HiddenField ID="hdnUpdateReadOnly" ClientIDMode="Static" runat="server" Value='<%# Eval("UpdateReadOnly") %>'>
                                                    </asp:HiddenField>
                                                    <asp:HiddenField ID="hdnDeleteReadOnly" ClientIDMode="Static" runat="server" Value='<%# Eval("DeleteReadOnly") %>'>
                                                    </asp:HiddenField>
                                                    <asp:HiddenField ID="hdnViewReadOnly" ClientIDMode="Static" runat="server" Value='<%# Eval("ViewReadOnly") %>'>
                                                    </asp:HiddenField>
                                                    <asp:HiddenField ID="hdnAllReadOnly" ClientIDMode="Static" runat="server" Value='<%# Eval("AllReadOnly") %>'>
                                                    </asp:HiddenField>
                                                </td>
                                                <td style="width: 10%">
                                                    <span id="spanAdd" class="lefttds">
                                                        <input id="chkAdd" runat="server" checked='<%#Convert.ToBoolean(Eval("Add")) %>'
                                                            clientidmode="Static" type="checkbox" />
                                                        Add</span>
                                                </td>
                                                <td style="width: 10%">
                                                    <span id="spanUpdate" class="lefttds">
                                                        <input id="chkUpdate" runat="server" checked='<%#Convert.ToBoolean(Eval("Update")) %>'
                                                            clientidmode="Static" type="checkbox" />
                                                        Update</span>
                                                </td>
                                                <td style="width: 10%">
                                                    <span id="spanDelete" class="lefttds">
                                                        <input id="chkDelete" runat="server" checked='<%#Convert.ToBoolean(Eval("Delete")) %>'
                                                            clientidmode="Static" type="checkbox" />
                                                        Delete</span>
                                                </td>
                                                <td style="width: 10%">
                                                    <span id="spanView" class="lefttds">
                                                        <input id="chkView" class="clsView" runat="server" checked='<%#Convert.ToBoolean(Eval("View")) %>'
                                                            clientidmode="Static" type="checkbox" />
                                                        View</span>
                                                </td>
                                                <td style="width: 20%">
                                                    <span id="spanAll" class="lefttds">
                                                        <input id="ChkAllRow" onclick="selectAllColumns(this);" clientidmode="Static" type="checkbox" />
                                                        All </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 40%">
                                                </td>
                                                <td colspan="4">
                                                    <asp:Repeater ID="rptGrandChild" runat="server">
                                                        <ItemTemplate>
                                                            <table id="grandchildTable" width="100%">
                                                                <tr>
                                                                    <td style="width: 100%">
                                                                        <span class="hiddenInformationClass" style="display: none; color: Blue; font-weight: bold">
                                                                            <%#Eval("AccessId")%>
                                                                        </span>
                                                                        <asp:Label ID="lblAccessId" Visible="false" runat="server" Text='<%# Eval("AccessId") %>'></asp:Label>
                                                                        <asp:Label ID="lblChildId" Visible="false" runat="server" Text='<%# Eval("ChildId") %>'></asp:Label>
                                                                        <input id="chkisApplicable" runat="server" checked='<%#Convert.ToBoolean(Eval("isApplicable")) %>'
                                                                            clientidmode="Static" type="checkbox" />
                                                                        <asp:Label ID="lblControlName" runat="server" Text='<%# Eval("ControlName") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </td>
                                                <td style="width: 20%">
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </table>
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn_validate" OnClick="btnSave_Click" />
        <script type="text/javascript">

            function selectAllColumns(obj) {

                $(obj).closest('tr').find(':checkbox').each(function () {
                    if ($(obj).is(":checked")) {
                        $(this).prop('checked', true);
                        $(this).addClass('checked');
                        $(this).closest('tr').find(':checkbox[id^="chkView"]').removeAttr('onclick');
                    }
                    else {
                        $(this).prop('checked', false);
                        $(this).removeClass('checked');
                        $(this).closest('table').parents('tr').prev('tr').find(':checkbox[id^="chkParentAll"]').prop('checked', false);
                        if ($(this).closest('tr').next().find(':checkbox[id^="chkisApplicable"]:checked').length > 0) {
                            $(this).closest('tr').find(':checkbox[id^="chkView"]').prop('checked', true);
                            $(this).closest('tr').find(':checkbox[id^="chkView"]').attr('onclick', 'return false');
                        }
                    }
                });
            }

            var checkFlag = 0;

            $('.clickMe').click(function () {

                if (checkFlag == 0) {
                    $(this).next('tr').find('div').slideToggle(500);
                    return false;
                }
                else {
                    checkFlag = 0;
                }
            });


            function parentRowClick(obj) {

                if ($(obj).is(":checkbox") == true) {
                    if ($(obj).is(":checked")) {
                        $(obj).closest('tr').next('tr').find('div').find(':checkbox').prop('checked', true);
                        $(obj).closest('tr').next('tr').find('div').find(':checkbox').addClass('checked');
                        $(obj).closest('tr').next('tr').find('div').find(':checkbox[id^="chkView"]').removeAttr('onclick');
                    }
                    else {
                        $(obj).closest('tr').next('tr').find('div').find(':checkbox').prop('checked', false);
                        $(obj).closest('tr').next('tr').find('div').find(':checkbox').removeClass('checked');
                    }
                    checkFlag = 1;
                }
            }

            $('#chkAdd, #chkUpdate, #chkDelete,#chkisApplicable').click(function () {
                if ($(this).attr('id') == 'chkisApplicable') {
                    if ($(this).is(":checked")) {
                        $(this).closest('table').parent().parent().prev().find(':checkbox[id^="chkView"]').prop('checked', true);
                        $(this).closest('table').parent().parent().prev().find(':checkbox[id^="chkView"]').attr('onclick', 'return false');
                    }
                    else {
                        if ($(this).closest('table').parent().find(':checkbox[id^="chkisApplicable"]:checked').length == 0) {
                            $(this).closest('table').parent().parent().prev().find(':checkbox[id^="chkView"]').removeAttr('onclick');
                        }
                        else {
                            $(this).closest('table').parent().parent().prev().find(':checkbox[id^="chkView"]').attr('onclick', 'return false');
                        }
                        //$(this).closest('table').parent().parent().parents('tr').prev('tr').find(':checkbox[id^="chkParentAll"]').prop('checked', false);
                        $(this).closest('table').parents('tr').prev('tr').find(':checkbox[id^="chkParentAll"]').prop('checked', false);
                    }
                }
                else {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').find(':checkbox[id^="chkView"]').prop('checked', true);

                        //Added By Prashant for 18788
                        var StrSpId = '';
                        $(this).closest('tr').find("span.lefttds[style$='display: none;']").each(function (index) {
                            $(this).find(':checkbox').prop('checked', false);
                            if (StrSpId == '')
                                StrSpId = $(this).attr('id');
                            else
                                StrSpId += ',' + $(this).attr('id');
                        });

                        if (StrSpId.indexOf('spanAll') <= 0) {
                            if (StrSpId != '') {
                                var ChkCnt = 4 - StrSpId.split(',').length;
                                if ($(this).closest('tr').find("[type='checkbox']:checked").length == ChkCnt) {
                                    $(this).closest('tr').find(':checkbox[id^="ChkAllRow"]').prop('checked', true);
                                }
                            }
                            else {
                                if ($(this).closest('tr').find("[type='checkbox']:checked").length == 4) {
                                    $(this).closest('tr').find(':checkbox[id^="ChkAllRow"]').prop('checked', true);
                                }
                            }
                        }
                    }
                    else {
                        $(this).closest('tr').find(':checkbox[id^="ChkAllRow"]').prop('checked', false);
                        $(this).closest('table').parents('tr').prev('tr').find(':checkbox[id^="chkParentAll"]').prop('checked', false);
                    }
                }
            });

            $('.clsView').click(function () {
                if ($(this).is(":checked")) {
                }
                else {
                    if ($(this).closest('tr').next().find(':checkbox[id^="chkisApplicable"]:checked').length > 0) {
                        $(this).closest('tr').find(':checkbox[id^="chkView"]').prop('checked', true);
                        $(this).closest('tr').find(':checkbox[id^="chkView"]').attr('onclick', 'return false');
                    }

                    var isAddVisible = $(this).closest('tr').find('#spanAdd').is(":visible");
                    var isUpdateVisible = $(this).closest('tr').find('#spanUpdate').is(":visible");
                    var isDelVisible = $(this).closest('tr').find('#spanDelete').is(":visible");

                    var add = $(this).closest('tr').find(':checkbox[id^="chkAdd"]').is(":checked");
                    var update = $(this).closest('tr').find(':checkbox[id^="chkUpdate"]').is(":checked");
                    var del = $(this).closest('tr').find(':checkbox[id^="chkDelete"]').is(":checked");

                    if (isAddVisible == false && isUpdateVisible == false && isDelVisible == false) {
                        $(this).closest('table').parents('tr').prev('tr').find(':checkbox[id^="chkParentAll"]').prop('checked', false);
                    }

                    if (isAddVisible == true && isUpdateVisible == true && isDelVisible == true) {
                        if (add == false && update == false && del == false) {
                            $(this).closest('tr').find(':checkbox[id^="ChkAllRow"]').prop('checked', false);
                        }
                        else {
                            return false;
                        }
                    }
                    else {
                        if (((isAddVisible == false && isUpdateVisible == true && isDelVisible == true) && (update == false && del == false)) ||
                            ((isAddVisible == true && isUpdateVisible == false && isDelVisible == true) && (add == false && del == false)) ||
                            ((isAddVisible == true && isUpdateVisible == true && isDelVisible == false) && (add == false && update == false)) ||

                            ((isAddVisible == false && isUpdateVisible == false && isDelVisible == true) && (del == false)) ||
                            ((isAddVisible == true && isUpdateVisible == false && isDelVisible == false) && (add == false)) ||
                            ((isAddVisible == false && isUpdateVisible == true && isDelVisible == false) && (update == false)) ||

                            ((isAddVisible == false && isUpdateVisible == false && isDelVisible == false))) {

                            $(this).closest('tr').find(':checkbox[id^="ChkAllRow"]').prop('checked', false);
                        }
                        else {
                            return false;
                        }
                    }
                }
            });

            $(document).ready(function () {
                function rowStyle() {
                    try {
                        var tableClass = 'masterTable';
                        $("." + tableClass).find('tbody').find('tr').each(function (t) {

                            if (t % 2 === 0) {
                                $(this).closest('tr').addClass('even');
                            }
                            else {
                                $(this).closest('tr').addClass('odd');
                            }
                        });

                    } catch (e) {
                    }
                }

                //rowStyle();
            }); 
        </script>
    </div>
    <div id="rightlinks" style="display: none;">
        <uc1:UserManagementLinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
    <script type="text/javascript">

        $(window).load(function () {
            $('.AllCheckBoxClass').each(function () {
               
                var Cnt = 0;
                var spanAll = 0;
                var ContractTypeNameVIEW = $("#hdnContractTypeNameVIEW").val();
                var ReportsVIEW = $("#hdnReportsVIEW").val();

                var ID = $.trim($(this).find("td :eq(0)").text())
                var Val = ContractTypeNameVIEW;

                if ($(this).find('#hdnAddReadOnly').val() == 'Y') {
                    $(this).find('#spanAdd').hide();
                    $(this).find('#spanAdd :checkbox').prop('checked', false);
                    Cnt++;
                }
                if ($(this).find('#hdnUpdateReadOnly').val() == 'Y') {
                    $(this).find('#spanUpdate').hide();
                    $(this).find('#spanUpdate :checkbox').prop('checked', false);
                    Cnt++;
                }
                if ($(this).find('#hdnDeleteReadOnly').val() == 'Y') {
                    $(this).find('#spanDelete').hide();
                    $(this).find('#spanDelete :checkbox').prop('checked', false);
                    Cnt++;
                }
                if ($(this).find('#hdnViewReadOnly').val() == 'Y') {
                    $(this).find('#spanView').hide();

                    if ((ID == "603" && Val == "Y") || (ID == "601" && ReportsVIEW == "Y")) {
                        $(this).find('#spanView :checkbox').prop('checked', true);
                    }
                    else {
                        $(this).find('#spanView :checkbox').prop('checked', false);
                    }
                    Cnt++;
                }
                if ($(this).find('#hdnAllReadOnly').val() == 'Y') {
                    $(this).find('#spanAll').hide();
                    spanAll++;
                }

                if (spanAll == 0) {
                    if ($(this).find("[type='checkbox']:checked").length == 4 - Cnt)
                        $(this).find(':checkbox[id^="ChkAllRow"]').prop('checked', true);
                }
            });
        });

        function LockCheckBox() {
            $('#masterDataTable').find(':checkbox').attr("disabled", true);
        }
         
    </script>
</asp:Content>
