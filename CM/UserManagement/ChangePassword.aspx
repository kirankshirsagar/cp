﻿<%@ Page Title="Change Password" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="ChangePassword.aspx.cs" Inherits="Account_ChangePassword" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="usermanagementcntrl"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <style type="text/css">
        #result
        {
            margin-left: 5px;
        }
        
        #register .short
        {
            color: #FF0000;
        }
        
        #register .weak
        {
            color: #E66C2C;
        }
        
        #register .good
        {
            color: #2D98F3;
        }
        
        #register .strong
        {
            color: #006400;
        }
    </style>
    <script type="text/javascript">


        $('#NewPassword').live('keyup', function (evt) {
            if ($('#NewPassword').val().trim().length == 0) {
                $('#result').html('');
                $('#strength').hide();
            }
            else {
                $('#result').html(checkStrength($('#NewPassword').val()));
            }
        });

        function checkStrength(password) {

            var strength = 0;
            if (password.length < 8) {
                $('#result').removeClass();
                $('#result').addClass('short');
                $('#strength').hide();
                $('#strength').css('width', '200px');
                $('#strength').css('height', '20px');
                $('#strength').css('background-color', 'transparent');
                $('#strength').show();
                //                $('#strength').animate({ width: '200px', height: '20px' }, 'medium').css('backgroundColor', 'transparent');
                return 'Password Too short';
            }
            if (password.length > 7)
                strength += 1;
            if (password.match(/(^[A-Z])/) && password.match(/([0-9])/) && strength == 1)
                strength += 1;
            if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/) && strength == 2)
                strength += 1;
            if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/) && strength == 3)
                strength += 1;
            if (strength < 2) {
                $('#result').removeClass();
                $('#result').addClass('weak');

                $('#strength').hide();
                $('#strength').css('width', '50px');
                $('#strength').css('height', '20px');
                $('#strength').css('background-color', 'red');
                $('#strength').show();
                //                $('#strength').animate({ width: '50px', height: '20px' }, 'medium').css('backgroundColor', 'red');
                //                $('#strength').show();
                return 'Weak';
            } else if (strength == 2) {
                $('#result').removeClass();
                $('#result').addClass('good');
                $('#strength').hide();
                $('#strength').css('width', '120px');
                $('#strength').css('height', '20px');
                $('#strength').css('background-color', 'Orange');
                $('#strength').show();
                //                $('#strength').animate({ width: '100px', height: '20px' }, 'medium').css('backgroundColor', '#FFBF00');
                //                $('#strength').show();
                return 'Good';
            } else {
                $('#result').removeClass();
                $('#result').addClass('strong');
                $('#strength').hide();
                $('#strength').css('width', '180px');
                $('#strength').css('height', '20px');
                $('#strength').css('background-color', 'green');
                $('#strength').show();
                //                $('#strength').animate({ width: '150px', height: '20px' }, 'medium').css('backgroundColor', 'green');
                //                $('#strength').show();
                return 'Strong';
            }
        }


        function CheckUserName(ctl) {

            var hdnUserName = document.getElementById('hdnusername').value.toUpperCase();
            var mytextbox = document.getElementById('NewPassword').value.toUpperCase();
            var username = ctl.value;


            if (mytextbox != '') {

                if (mytextbox.indexOf(hdnUserName) >= 0 && hdnUserName.val() != '') {

                    document.getElementById('NewPassword').value = '';
                    alert('User name is not allowed in password.');
                    document.getElementById('NewPassword').focus();
                    return false;
                }
            }
            return true;
        }

    </script>
    <script type="text/javascript">
        function togglemessage() {
            $('#ChangePasswordDivMessage').slideToggle('slow');
        }
    </script>
    <script language="javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");
    </script>
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <h2>
        Change Password</h2>
    <div style="margin: 0; padding: 0; display: inline">
        <div id="query_form_content" class="hide-when-print">
            <table width="100%" border="0">
                <tr>
                    <td width="100%" align="right">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="100%">
                                    <br />
                                    <div class="mws-panel grid_8">
                                        <div id="Mydiv">
                                            <div class="mws-panel-body">
                                                <div id="ChangePasswordDivMessage" clientidmode="Static" style="display: none;" runat="server"
                                                    class="mws-form-message" onclick="return togglemessage(true);">
                                                </div>
                                                <asp:HiddenField ID="hdnusername" runat="server" ClientIDMode="Static" />
                                                <asp:ChangePassword ID="ChangeUserPassword" runat="server" CancelDestinationPageUrl="~/"
                                                    RenderOuterTable="false" SuccessPageUrl="ChangePasswordSuccess.aspx" MembershipProvider="AspNetSqlMembershipProvider"
                                                    ChangePasswordFailureText="">
                                                    <ChangePasswordTemplate>
                                                        <asp:ValidationSummary ID="ChangeUserPasswordValidationSummary" runat="server" CssClass="mws-form-message error"
                                                            ValidationGroup="ChangeUserPasswordValidationGroup" DisplayMode="List" />
                                                        <div>
                                                            <fieldset>
                                                                <table>
                                                                    <tr>
                                                                        <td style="width: 20%; text-align: right; padding-top: 2px">
                                                                            <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                                                                                ErrorMessage="Old Password is required." ToolTip="Old Password is required."
                                                                                ValidationGroup="ChangeUserPasswordValidationGroup" ForeColor="Red" Display="None">  </asp:RequiredFieldValidator>
                                                                            <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword"
                                                                                Font-Bold="false"><span class="required">*</span> <b>Old Password</b>
                                                                            </asp:Label>
                                                                        </td>
                                                                     
                                                                        <td style="width: 20%; padding-left: 3px;">
                                                                            <asp:TextBox ID="CurrentPassword" runat="server" CssClass="mws-textinput ValforTextArea Password"
                                                                                TextMode="Password" MaxLength="15"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 58%; padding-top: 10px;">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 20%; text-align: right; padding-top: 2px">
                                                                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="CurrentPassword"
                                                                                ControlToValidate="NewPassword" Display="None" ErrorMessage="Old password should not be equal to new password."
                                                                                ValidationGroup="ChangeUserPasswordValidationGroup" ForeColor="Red" Operator="NotEqual"> </asp:CompareValidator>
                                                                            <asp:RequiredFieldValidator Display="None" ID="NewPasswordRequired" runat="server"
                                                                                ControlToValidate="NewPassword" ErrorMessage="New Password is required." ToolTip="New Password is required."
                                                                                ValidationGroup="ChangeUserPasswordValidationGroup" ForeColor="Red">  </asp:RequiredFieldValidator>
                                                                            <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword"
                                                                                Font-Bold="false"><span class="required">*</span><b>New Password</b>
                                                                            </asp:Label>
                                                                        </td>
                                                                       
                                                                        <td style="padding-left: 3px;">
                                                                            <asp:TextBox ID="NewPassword" onBlur="return CheckUserName(this)" ClientIDMode="Static"
                                                                                runat="server" CssClass="mws-textinput ValforTextArea Password" TextMode="Password"
                                                                                MaxLength="15"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <div id="strength" style="margin-left: 10px; display: none" class="mws-panel-header">
                                                                                <center>
                                                                                    <b><span id="result" style="color: black"></span></b>
                                                                                </center>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 20%; text-align: right; padding-top: 2px">
                                                                            <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                                                                                Display="none" ErrorMessage="Confirm New Password is required." ToolTip="Confirm New Password is required."
                                                                                ValidationGroup="ChangeUserPasswordValidationGroup" ForeColor="Red">  </asp:RequiredFieldValidator>
                                                                            <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                                                                                ControlToValidate="ConfirmNewPassword" Display="none" ErrorMessage="The Confirm New Password must match the New Password entry."
                                                                                ValidationGroup="ChangeUserPasswordValidationGroup" ForeColor="Red"> </asp:CompareValidator>
                                                                            <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword"
                                                                                Font-Bold="false"><span class="required">*</span><b>Confirm&nbsp;New&nbsp;Password</b>
                                                                            </asp:Label>
                                                                        </td>
                                                                    
                                                                        <td style="padding-left: 3px;">
                                                                            <asp:TextBox ID="ConfirmNewPassword" runat="server" CssClass="mws-textinput ValforTextArea Password"
                                                                                ClientIDMode="Static" TextMode="Password" MaxLength="15"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                            <div class="mwschild-button-row">
                                                                <asp:Button ID="ChangePasswordPushButton" runat="server" CommandName="ChangePassword"
                                                                    ClientIDMode="Static" Text="Change Password" ValidationGroup="ChangeUserPasswordValidationGroup"
                                                                    OnClick="ChangePasswordPushButton_Click" CssClass="mws-button red" OnClientClick="return ValidatePage()" />
                                                                <asp:Button ID="btnCancel" runat="server" CssClass="mws-button red" Text="Cancel"
                                                                    CausesValidation="false" OnClientClick="HideValidationSummary();" OnClick="btnCancel_Click" />
                                                            </div>
                                                        </div>
                                                    </ChangePasswordTemplate>
                                                </asp:ChangePassword>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="rightlinks" style="display: none;">
        <uc1:usermanagementcntrl ID="usermanagementcntrl" runat="server" />
    </div>
    <script language="javascript" type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
    <script type="text/javascript">

        function HideValidationSummary() {

            for (sums = 0; sums < Page_ValidationSummaries.length; sums++) {
                summary = Page_ValidationSummaries[sums];
                summary.style.display = "none";
            }

            $('#NewPassword').val('');
            $('#ConfirmNewPassword').val('');
            for (sums = 0; sums < Page_ValidationSummaries.length; sums++) {
                summary = Page_ValidationSummaries[sums];
                summary.style.display = "none";
            }

            window.location.replace("RegisterData.aspx");


        }


        function ValidatePage() {
            if (Page_ClientValidate("ChangeUserPasswordValidationGroup")) { //validate using above validation controls group
                //validation return true section

                return true;
            }
            else {

                return false;
                //validation return false section
            }
        }



    </script>
</asp:Content>
