﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using CommonBLL;
using UserManagementBLL;

public partial class Account_ChangePassword : System.Web.UI.Page
{
    static string prevPage = String.Empty;


    public string View;
    public string Update;




    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            prevPage = Request.UrlReferrer.ToString();
            hdnusername.Value = Convert.ToString(Session[Declarations.User]);

            if (Session[Declarations.User] != null)
            {
                Access.PageAccess(this, Session[Declarations.User].ToString(), "usermanagement_");
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
            }
            AccessVisibility();

        }
    }



    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (Update == "N")
        {
            ChangeUserPassword.Enabled = false;
        }

        Page.TraceWrite("AccessVisibility starts.");
    }


    private void setAccessValues()
    {

        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }


    protected void ChangePasswordPushButton_Click(object sender, EventArgs e)
    {
        try
        {
            bool isupper = false;
            bool isLower = false;
            //  bool isNumber = false;
            bool isSpecial = false;
            bool isQuotation = false;


            char[] val = ChangeUserPassword.NewPassword.ToCharArray();

            char[] spl = "!@#$%^&*(,).1234567890'\"".ToCharArray();

            for (int i = 0; i < val.Length; i++)
            {

                if (char.IsLower(val[i]) == true)
                {
                    isLower = true;
                }
                if (char.IsUpper(val[i]) == true)
                {
                    isupper = true;
                }

                for (int j = 0; j < spl.Length; j++)
                {
                    if (val[i] == spl[j])
                    {
                        isSpecial = true;
                    }
                }
                if (val[i] == '\'' || val[i] == '\"')
                {
                    isQuotation = true;
                }
            }


            string username = User.Identity.Name;

            //***************************************
            IUsers obj = FactoryUser.GetUsersDetail();
            obj.CreateDALObjForMembershipWork();
            //****************************************
            MembershipUser user = Membership.GetUser(username);


            string oldpswd = ChangeUserPassword.CurrentPassword;//user.ResetPassword();

            string UserNamePart="";// = ChangeUserPassword.UserName.Substring(0, 4);// getting first 4 char of Username
            if (user.UserName.Length > 4)
                UserNamePart = user.UserName.Substring(0, 4);// getting first 4 char of Username
            else if (user.UserName.Length >= 4)
                UserNamePart = user.UserName.Substring(0, 3);

            if (Membership.ValidateUser(username, oldpswd) == true)
            {

                if (isQuotation)
                {
                    ChangePasswordDivMessage.Style.Add("display", "block");
                    ChangePasswordDivMessage.Style.Add("Color", "Red");
                    ChangePasswordDivMessage.Attributes.Add("class", "error mws-form-message");
                    ChangePasswordDivMessage.InnerHtml = "The characters \" (double quote) and \' (single quote) are not allowed in passwords.";
                    return;
                }
                if (ChangeUserPassword.NewPassword.Length < 8 || isupper == false || isLower == false || isSpecial == false || ChangeUserPassword.NewPassword.Contains(Convert.ToString(Session[Declarations.User])) == true)
                {
                    ChangePasswordDivMessage.Style.Add("display", "block");
                    ChangePasswordDivMessage.Style.Add("Color", "Red");
                    ChangePasswordDivMessage.Attributes.Add("class", "error mws-form-message");
                    ChangePasswordDivMessage.InnerHtml = "At least 8 characters long and contain atleast one of each of the following: </br> § Uppercase letter </br> § Lowercase letter </br> § Special character or number </br>";
                    return;
                }
                
                if (username == ChangeUserPassword.NewPassword)
                {
                    ChangePasswordDivMessage.Style.Add("display", "block");
                    ChangePasswordDivMessage.Style.Add("Color", "Red");
                    ChangePasswordDivMessage.Attributes.Add("class", "error mws-form-message");
                    ChangePasswordDivMessage.InnerHtml = "New password should not contain user name.";
                    return;
                }
                if (ChangeUserPassword.NewPassword.ToLower().Contains(UserNamePart.ToLower()))
                {
                    ChangePasswordDivMessage.Style.Add("display", "block");
                    ChangePasswordDivMessage.Style.Add("Color", "Red");
                    ChangePasswordDivMessage.Attributes.Add("class", "error mws-form-message");
                    ChangePasswordDivMessage.InnerHtml = "New password should not contain a part of user name.";
                    return;
                }
                Users Usr = new Users();
                string newpass = ChangeUserPassword.NewPassword;
                user.ChangePassword(oldpswd, newpass);
                HttpCookie myCookie = new HttpCookie("myCookie");
                Response.Cookies.Remove("myCookie");
                myCookie.Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies.Add(myCookie);
                Usr.UserName = Convert.ToString(Session[Declarations.User]);
                Usr.UpdateUserPasswordExpireStatus();
                ChangePasswordDivMessage.Style.Add("display", "block");
                ChangePasswordDivMessage.Style.Add("Color", "green");
                ChangePasswordDivMessage.Attributes.Add("class", "success mws-form-message");
                ChangePasswordDivMessage.InnerHtml = "Your password has been changed successfully.";

                //Response.Redirect("ChangePasswordSuccess.aspx", true);
            }
            else
            {
                ChangeUserPassword.ChangePasswordFailureText = "Invalid Old Password";
                ChangePasswordDivMessage.Style.Add("display", "block");
                ChangePasswordDivMessage.Style.Add("Color", "Red");
                ChangePasswordDivMessage.Attributes.Add("class", "error mws-form-message");
                ChangePasswordDivMessage.InnerHtml = "Invalid Old Password.";
            }
        }
        catch
        {


        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {

        Response.Redirect("RegisterData.aspx");

    }
}
