﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UserManagementBLL;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;


public partial class Masters_DepartmentMaster : System.Web.UI.Page
{

    IDepartment objDepartment;
    public string Add;
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
       

        if (!IsPostBack)
        {
            if (String.IsNullOrEmpty(Request.extValue("hdnPrimeIds")) != true)
            {
                hdnPrimeId.Value = Request.extValue("hdnPrimeIds");
                ReadData();
                lblTitle.Text = "Edit Department";
                btnSave.Text = "Update";
            }
            else
            {
                rdActive.Checked = true;
                hdnPrimeId.Value = "0";
                lblTitle.Text = "Add Department";
                btnSave.Text = "Save";
            }

            if (Session[Declarations.User] != null)
            {
                Access.PageAccess(this, Session[Declarations.User].ToString(), "usermanagement_");
                ViewState[Declarations.Add] = Access.Add.Trim();
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
            }
            AccessVisibility();

        }
    }

 

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objDepartment = FactoryUser.GetDepartmentDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        InsertUpdate();
        Page.TraceWrite("Save Update button clicked event ends.");
    }

    protected void SaveAndContinue_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save and Add more button clicked event starts.");
        InsertUpdate(1);
        Page.TraceWrite("Save and Add more button clicked event ends.");
    }

    protected void Back_Click(object sender, EventArgs e)
    {
        Server.Transfer("DepartmentMasterData.aspx");
    }



    void ReadData()
    {
        Page.TraceWrite("ReadData starts.");
        try
        {
            objDepartment.DepartmentId = int.Parse(hdnPrimeId.Value);
            List<Department> Department = objDepartment.ReadData();
            txtDepartmentName.Value = Department[0].DepartmentName;
            txtDescription.Value = Department[0].Description;
            btnSaveAndContinue.Visible = false;
            if (Department[0].isActive == "Y")
            {
                rdActive.Checked = true;
            }
            else
            {
                rdInactive.Checked = true;
            }

        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData ends.");
    }

    void InsertUpdate(int flg = 0)
    {
        Page.TraceWrite("Insert, Update starts.");
        string status = "";
        objDepartment.DepartmentName = txtDepartmentName.Value.Trim();
        objDepartment.Description = txtDescription.Value.Trim();
        objDepartment.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objDepartment.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objDepartment.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
        objDepartment.isActive = rdActive.Checked == true ? "Y" : "N";

        try
        {
            if (hdnPrimeId.Value == "0")
            {
                objDepartment.DepartmentId = 0;
                status = objDepartment.InsertRecord();
            }
            else
            {
                objDepartment.DepartmentId = int.Parse(hdnPrimeId.Value);
                status = objDepartment.UpdateRecord();
            }

            Page.Message(status, hdnPrimeId.Value);
        }
        catch (Exception ex)
        {
            Page.Message("0", hdnPrimeId.Value);
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Insert, Update ends.");
        if (flg == 0 && status == "1")
        {
            Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
            Server.Transfer("DepartmentMasterData.aspx");
        }
        ClearData(status);
    }

    void ClearData(string status)
    {
        if (status == "1")
        {
            Page.TraceWrite("clearData starts.");
            objDepartment.DepartmentId = 0;
            hdnPrimeId.Value = "0";
            txtDepartmentName.Value = "";
            txtDescription.Value = "";
            Page.TraceWrite("clear Data ends.");
        }
    }

    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (Add == "N" && (hdnPrimeId.Value == "" || hdnPrimeId.Value == "0" || hdnPrimeId.Value == null))
        {
            Page.extDisableControls();
            btnBack.Enabled = true;
        }
        if (View == "N")
        {
            btnBack.Enabled = false;
        }
        Page.TraceWrite("AccessVisibility starts.");
    }



}










