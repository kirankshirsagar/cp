﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MasterBLL;
using CommonBLL;
using System.Data;
using UserManagementBLL;

public partial class Access_RolePermissions : System.Web.UI.Page
{
    IRolePermissions objPermission;
    IRole objRole;
   
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            Access.PageAccess(this, Session[Declarations.User].ToString(), "usermanagement_");
           
            ViewState[Declarations.View] = Access.View.Trim();
            ViewState[Declarations.Delete] = Access.Delete.Trim();
            ViewState[Declarations.Update] = Access.Update.Trim();
        }
        AccessVisibility();
        if (!IsPostBack)
        {
            RoleBind();
            PermisionBind();           
        }
    } 


    void CreateObjects()
    {

        Page.TraceWrite("Page object create starts.");
        try
        {
            objPermission = FactoryUser.GetRolePermissionDetail();
            objRole = FactoryUser.GetRoleDetail();
           
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }


    void PermisionBind()
    {
        Page.TraceWrite("Permision repeater bind starts..");
        try
        {
            objPermission.RoleId = int.Parse(ddlRole.SelectedValue);
            objPermission.UsersId = Convert.ToInt32(Session[Declarations.User]);
            objPermission.ReadData();
            ParentBind();
            ChildBind();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("Permision repeater bind fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Permision repeater bind ends.");

    }


    public void ParentBind()
    {
        rptParent.DataSource = objPermission.ReadParent;
        rptParent.DataBind();
    }

    public void ChildBind()
    {
        List<RolePermissions> child = new List<RolePermissions>();
        child = objPermission.ReadChild;
        for (int i = 0; i <= rptParent.Items.Count - 1; i++)
        {
            Label id = (Label)rptParent.Items[i].FindControl("lblParentId");
            Repeater rptChild = (Repeater)rptParent.Items[i].FindControl("rptChild");
            var result = child.FindAll(x => x.ParentId == int.Parse(id.Text));           
            rptChild.DataSource = result;
            rptChild.DataBind();
            GrandChildBind(rptChild);
        }
    }


    public void GrandChildBind(Repeater rptChild)
    {
        List<RolePermissions> child = new List<RolePermissions>();
        child = objPermission.ReadGrandChild;
        for (int i = 0; i <= rptChild.Items.Count - 1; i++)
        {
            Label id = (Label)rptChild.Items[i].FindControl("lblChildId");
            Repeater rptGrandChild = (Repeater)rptChild.Items[i].FindControl("rptGrandChild");
            var result = child.FindAll(x => x.ChildId == int.Parse(id.Text));
            if (id.Text == "603") // ChildId 603 is set for 'Contract Type Name'
            {
                foreach (var ContractTypeName in result)
                {
                    if (ContractTypeName.isApplicable.ToUpper().Equals("TRUE"))
                    {
                        hdnContractTypeNameVIEW.Value = "Y";
                        break;
                    }
                }
            }
            if (id.Text == "601") // ChildId 603 is set for 'Contract Type Name'
            {
                foreach (var ReportName in result)
                {
                    if (ReportName.isApplicable.ToUpper().Equals("TRUE"))
                    {
                        hdnReportsVIEW.Value = "Y";
                        break;
                    }
                }
            }
            
            rptGrandChild.DataSource = result;
            rptGrandChild.DataBind();
        }
    }



    void RoleBind()
    {
        Page.TraceWrite("Role dropdown bind starts.");
        try
        {
            // filter select and superadmin from dropdown...
            if (Session[Declarations.UserRole].ToString().ToLower().Equals("super admin"))
            {
                ddlRole.extDataBind(objRole.SelectData().Where(x => x.Id > 1).ToList());
            }
            else
            {
                ddlRole.extDataBind(objRole.SelectData().Where(x => x.Id > 1 && x.Id != 99999).ToList());
            }
        }
        catch (Exception ex)
        {
            Page.TraceWarn("Role dropdown bind fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Role dropdown bind ends.");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save and Add more button clicked event starts.");
        Insert();
        Page.TraceWrite("Save and Add more button clicked event ends.");
    }


    void Insert()
    {
        Page.TraceWrite("Insert starts.");
        string status = "";
        try
        {
            objPermission.RoleId = int.Parse(ddlRole.SelectedValue);
            objPermission.RoleAccess = objPermission.GetAccessTable(rptParent);
            objPermission.RoleAccessDynamic = objPermission.GetAccessTableFeature(rptParent);
            status = objPermission.InsertRecord();
            Page.JavaScriptClientScriptBlock("status", "msgUpdateSuccess();");
        }
        catch (Exception ex)
        {
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            Page.JavaScriptClientScriptBlock("status", "msgUpdateFail();");
        }
        Page.TraceWrite("Insert ends.");
    }

    protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
    {
        PermisionBind();
    }


    private void setAccessValues()
    {
       
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }


    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (View == "N")
        {
            Page.extDisableControls();
            btnSave.Enabled = false;
        }
        else
        {
            btnSave.Enabled = true;
        }

        if (Update == "N")
        {
            btnSave.Enabled = false;
            Page.JavaScriptStartupScript("lock", "LockCheckBox();");
        }
        else
        {
            btnSave.Enabled = true;
        }

        Page.TraceWrite("AccessVisibility starts.");
    }   
}