﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;


public partial class Masters_RequestMaster : System.Web.UI.Page
{

    IRequest objRequest;
    public string Add;
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        
       
        if (!IsPostBack)
        {

            if (String.IsNullOrEmpty(Request.extValue("hdnPrimeIds")) != true)
            {
                hdnPrimeId.Value = Request.extValue("hdnPrimeIds");
               
                ReadData();
                lblTitle.Text = "Edit Request";
            }
            else
             {
                rdActive.Checked = true;
                hdnPrimeId.Value = "0";
                lblTitle.Text = "Add Request";
             }

            if (Session[Declarations.User] != null)
            {
                 Access.PageAccess(this, Session[Declarations.User].ToString(), "masters_");
                ViewState[Declarations.Add] = Access.Add.Trim();
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
            }
            AccessVisibility();
            
        }
    }


    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objRequest = FactoryMaster.GetRequestDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        InsertUpdate();
        Page.TraceWrite("Save Update button clicked event ends.");
    }

    protected void SaveAndContinue_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save and Add more button clicked event starts.");
        InsertUpdate(1);
        Page.TraceWrite("Save and Add more button clicked event ends.");
       
    }

    protected void Back_Click(object sender, EventArgs e)
    {
        Server.Transfer("RequestMasterData.aspx");
    }



    void ReadData()
    {
        Page.TraceWrite("ReadData starts.");
        try
        {
            objRequest.RequestId = int.Parse(hdnPrimeId.Value);
            List<Request> Request = objRequest.ReadData();
            txtRequestName.Value = Request[0].RequestName;
            txtRequestDescription.Value = Request[0].Description;
            btnSaveAndContinue.Visible = false;
            if (Request[0].isActive == "Y")
            {
                rdActive.Checked = true;
            }
            else
            {
                rdInactive.Checked = true;
            }

        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData ends.");
    }

    void InsertUpdate(int flg = 0)
    {
        Page.TraceWrite("Insert, Update starts.");
        string status = "";
        objRequest.RequestName = txtRequestName.Value.Trim();
        objRequest.isActive = rdActive.Checked == true ? "Y" : "N";
        objRequest.IpAddress = Session[Declarations.IP].ToString();
        objRequest.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objRequest.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objRequest.Description = txtRequestDescription.Value;
        try
        {
            if (hdnPrimeId.Value == "0")
            {
                objRequest.RequestId = 0;
                status = objRequest.InsertRecord();
                //Page.AlertMessage(
                //Page.AlertMessage(0, status);
            }
            else
            {
                objRequest.RequestId = int.Parse(hdnPrimeId.Value);
                status = objRequest.UpdateRecord();
                //Page.AlertMessage(1, status);
            }

            Page.Message(status, hdnPrimeId.Value);
        }
        catch (Exception ex)
        {
            Page.Message("0", hdnPrimeId.Value);
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Insert, Update ends.");
        if (flg == 0 && status == "1")
        {
            Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
            Server.Transfer("RequestMasterData.aspx");
        }
        ClearData(status);
    }

    void ClearData(string status)
    {
        if (status == "1")
        {
            Page.TraceWrite("clearData starts.");
            objRequest.RequestId = 0;
            hdnPrimeId.Value = "0";
            txtRequestName.Value = "";
            txtRequestDescription.Value = "";
            Page.TraceWrite("clear Data ends.");
        }
    }


    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }


    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (Add == "N" && (hdnPrimeId.Value == "" || hdnPrimeId.Value == "0" || hdnPrimeId.Value == null))
        {
            Page.extDisableControls();
            btnBack.Enabled = true;
        }
        if (View == "N")
        {
            btnBack.Enabled = false;
        }
        Page.TraceWrite("AccessVisibility starts.");
    }



}










