﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;



public partial class Masters_CityMasterData : System.Web.UI.Page
{
    // navigation//
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 10;
    // navigation//

    ICity objCity;
    public string Add;
    public string View;
    public string Update;
    public string Delete;


    protected void Page_Load(object sender, EventArgs e)
    {

        CreateObjects();
        deleteMessage();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            Access.PageAccess(this, Session[Declarations.User].ToString(), "masters_");
            ViewState[Declarations.Add] = Access.Add.Trim();
            ViewState[Declarations.View] = Access.View.Trim();
            ViewState[Declarations.Delete] = Access.Delete.Trim();
            ViewState[Declarations.Update] = Access.Update.Trim();
        }

        AccessVisibility();
        if (!IsPostBack || hdnSearch.Value.Trim() != string.Empty)
        {
            Session[Declarations.SortControl] = null;
            rdActive.Checked = true;
            ReadData(1, RecordsPerPage);
            Message();
        }
        else
        {
            SetNavigationButtonParameters();
        }
    }

    protected void btnSort_Click(object sender, EventArgs e)
    {
        hdnSearch.Value = "";
        LinkButton clickBtn = (LinkButton)sender;
        Session.Add(Declarations.SortControl, clickBtn);
        ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
    }

    void Message()
    {
        if (Session[Declarations.Message] != null)
        {
            var flg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }

    }

    // navigation//

    #region Navigation
    void SetNavigationButtonParameters()
    {
        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }


    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
            AccessVisibility();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

    }
    #endregion

    // navigation//

    void ReadData(int pageNo, int recordsPerPage)
    {

        if (View == "Y")
        {
            Page.TraceWrite("ReadData starts.");
            try
            {
                objCity.PageNo = pageNo;
                objCity.RecordsPerPage = recordsPerPage;
                objCity.Search = txtSearch.Value.Trim();
                LinkButton btnSort = null;
                if (Session[Declarations.SortControl] != null && Session[Declarations.SortControl] != string.Empty)
                {
                    btnSort = (LinkButton)Session[Declarations.SortControl];
                    objCity.Direction = btnSort.CssClass.ToLower() == "sort desc" ? 1 : 0;
                    objCity.SortColumn = btnSort.Text.Trim();
                }
                rptCityMaster.DataSource = objCity.ReadData();
                rptCityMaster.DataBind();
                if (btnSort != null)
                {
                    rptCityMaster.ClassChange(btnSort);
                }
                Page.TraceWrite("ReadData ends.");
                ViewState[Declarations.TotalRecord] = objCity.TotalRecords;
                PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
                PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
                PaginationButtons1.RecordsPerPage = RecordsPerPage;
            }
            catch (Exception ex)
            {
                Page.TraceWarn("ReadData fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objCity = FactoryMaster.GetCityDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Delete starts.");
        string status = "";
        objCity.CityIds = hdnPrimeIds.Value;
        try
        {
            status = objCity.DeleteRecord();
        }
        catch (Exception ex)
        {
            hdnPrimeIds.Value = string.Empty;
            Session[Declarations.DeleteMSG] = "PageGetMessage('DF')";
            Page.TraceWarn("Delete fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        hdnPrimeIds.Value = string.Empty;
        Page.TraceWrite("Delete ends.");
        Session[Declarations.DeleteMSG] = "PageGetMessage('DS')";
        Server.Transfer("CityMasterData.aspx");
    }


    void deleteMessage()
    {
        if (Session[Declarations.DeleteMSG] != null)
        {
            Page.JavaScriptClientScriptBlock("status", Session[Declarations.DeleteMSG].ToString());
            Session[Declarations.DeleteMSG] = null;
        }
    }

    void loadPagination()
    {
        UserControl_PaginationButtons ucSimpleControl = LoadControl("../UserControl/Requestdetaillinks.ascx") as UserControl_PaginationButtons;
        Placeholder1.Controls.Add(ucSimpleControl);

    }

    protected void btnChangeStatus_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Change Status starts.");
        string status = "";
        objCity.CityIds = hdnPrimeIds.Value;
        try
        {
            objCity.isActive = rdActive.Checked == true ? "Y" : "N";
            status = objCity.ChangeIsActive();
            ReadData(1, RecordsPerPage);
        }
        catch (Exception ex)
        {
            hdnPrimeIds.Value = string.Empty;
            Page.JavaScriptClientScriptBlock("status", "PageGetMessage('SF')");
            Page.TraceWarn("Change Status fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.JavaScriptClientScriptBlock("status", "PageGetMessage('SS')");
        Page.TraceWrite("Change Status ends.");
    }


    protected void imgEdit_Click(object sender, EventArgs e)
    {
        if (Update == "Y")
        {
            Server.Transfer("CityMaster.aspx");
        }
    }

    protected void btnAddRecord_Click(object sender, EventArgs e)
    {
        Server.Transfer("CityMaster.aspx");
    }

    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (View == "N")
        {
            Page.extDisableControls();
            btnChangeStatus.Enabled = false;

        }
        else
        {
            btnSearch.Enabled = true;
            txtSearch.Disabled = false;
        }

        if (Update == "N")
        {
            foreach (RepeaterItem item in rptCityMaster.Items)
            {
                LinkButton imgEdit = (LinkButton)item.FindControl("imgEdit");
                imgEdit.Enabled = false;
            }
            btnDelete.Enabled = false;
            btnChangeStatus.Enabled = false;
        }
        if (Add == "N")
        {
            Page.extDisableControls();
            btnChangeStatus.Enabled = false;
            btnAddRecord.Enabled = false;
        }
        else
        {
            btnAddRecord.Enabled = true;
        }

        if (Delete == "N")
        {
            btnDelete.Enabled = false;
            btnDelete.OnClientClick = null;
        }
        else if (Delete == "Y")
        {
            btnDelete.Enabled = true;
        }

        if (View == "Y")
        {
            txtSearch.Disabled = false;
            btnSearch.Enabled = true;
            btnShowAll.Enabled = true;
        }

        Page.TraceWrite("AccessVisibility starts.");
    }

}










