﻿<%@ Page Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true" CodeFile="CurrencyMaster.aspx.cs"
    Inherits="Masters_CurrencyMaster" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>



<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/mastervalidations.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
<script src="../JQueryValidations/mastervalidations.js" type="text/javascript"></script>
    <script type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");
    </script>
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <h2>
        <asp:Label ID="lblCurrency" runat="server" Text=""></asp:Label>
    </h2>
    <div class="box tabular">
        <p>
            <label for="time_entry_issue_id">Currency Name</label>
            <input id="txtCurrencyName" runat="server" type="text" class="required ui-autocomplete-input"
                style="width: 220px;" maxlength="50 " autocomplete="off" role="textbox" aria-autocomplete="list" readonly="readonly"
                aria-haspopup="true" />
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id">Currency Code</label>
            <input id="txtCurrencyCode" runat="server" type="text" class="required ui-autocomplete-input"
                style="width: 220px;" maxlength="50 " autocomplete="off" role="textbox" aria-autocomplete="list" readonly="readonly"
                aria-haspopup="true" />
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id">
                <span class="required">*</span>Conversion Rate</label>
            <input id="txtConversionRate" class="required Currency" runat="server" maxlength="11" clientidmode="static" type="text" autocomplete="false" />
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id">Is Base Currency</label>
            <input id="chkIsBaseCurrency" runat="server" type="checkbox" class="ui-autocomplete-input" style="width: 20px;margin-top: 6px;" />
            <em></em>
        </p>       
        <p>
         <label for="time_entry_issue_id">
                Status</label>           
            <input id="rdActive" name="ActiveInactive" runat="server" type="radio" class="required"/>Active
            <input id="rdInactive" name="ActiveInactive" runat="server" type="radio" class="required"/>Inactive           
            <em></em>
        </p>
    </div>
    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" class="btn_validate" />
    <asp:Button ID="btnSaveAndContinue" runat="server" Text="Save and Add more" OnClick="SaveAndContinue_Click"
        class="btn_validate" />
    <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back_Click" />
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">

        $("#sidebar").html($("#rightlinks").html());

       
    </script>
</asp:Content>
