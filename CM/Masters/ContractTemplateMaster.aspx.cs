﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;
using System.Web.Security;
//using WorkflowBLL;
using DocumentOperationsBLL;
using System.Collections;
using System.Net;


public partial class Masters_ContractTemplateMaster : System.Web.UI.Page
{


    IContractTemplate objContractTemplate;
    IContractType objContractType;
    IRequest objRequest;

    public string Add;
    public string View;
    public string Update;
    public string Delete;
    string ContractTempId = string.Empty;

    static string ContractTemplateIsExist = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        btnSaveAndContinue.Visible = false;
        if (!IsPostBack)
        {
            flash_error.Visible = false;
            RequestTypeBind();
            if (String.IsNullOrEmpty(Request.extValue("hdnPrimeIds")) != true)
            {
                hdnPrimeId.Value = Request.extValue("hdnPrimeIds");
                ReadData();

                lblTitle.Text = "Edit Contract Template";
            }
            else
            {
                hdnSetContractTypeId.Value = Request.extValue("hdnContractTypeId");
                lblcontracttypeName.Text = Request.extValue("txtContracttypeName");

                rdActive.Checked = true;
                FUContractTemplate.Style.Add("color", "");

                if (String.IsNullOrEmpty(Request.extValue("hdnNewTemplateOREditTemplate")) != true)
                {
                    hdnAddEditCheck.Value = Request.extValue("hdnNewTemplateOREditTemplate");

                }
                if (hdnAddEditCheck.Value == "0")
                {

                    lblTitle.Text = "Add Contract Template";
                    FileDownload.Visible = false;
                    lblfilename.Visible = false;
                }
                else
                {
                    ReadData();
                    lblTitle.Text = "Edit Contract Template";
                    btnSave.Text = "Update";
                    FUContractTemplate.Style.Add("color", "transparent");

                }

            }
            if (Session[Declarations.User] != null)
            {
                Access.PageAccess(this, Session[Declarations.User].ToString(), "masters_");
                ViewState[Declarations.Add] = Access.Add.Trim();
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
            }
            AccessVisibility();

        }

    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objContractTemplate = FactoryMaster.GetContractTemplateDetail();
            objContractType = FactoryMaster.GetContractTypeDetail();
            objRequest = FactoryMaster.GetRequestDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        if (btnSave.Text == "Update")
        {
            System.Threading.Thread.Sleep(300);
            InsertUpdate();
        }
        else
        {
            if (FUContractTemplate.HasFile)
            {
                System.Threading.Thread.Sleep(300);
                InsertUpdate();
           }
        }
        Page.TraceWrite("Save Update button clicked event ends.");
    }

    protected void SaveAndContinue_Click(object sender, EventArgs e)
    {

        Page.TraceWrite("Save and Add more button clicked event starts.");
        if (FUContractTemplate.HasFile)
        {
            System.Threading.Thread.Sleep(300);
            InsertUpdate(1);
        }
        Page.TraceWrite("Save and Add more button clicked event ends.");
       

    }

    protected void Back_Click(object sender, EventArgs e)
    {
        Server.Transfer("ContractTypeMaster.aspx");
    }

    protected void FileDowload_Click(object sender, EventArgs e)
    {
        Server.Transfer("ContractTypeMaster.aspx");
    }

    void RequestTypeBind()
    {
        Page.TraceWrite("Request type bind starts.");
        try
        {
            //ddlRequestType.extDataBind(objContractTemplate.SelectDataRequestType());

            //CheckBoxList chkList = new CheckBoxList();
            //CheckBox chk = new CheckBox();
            //chkList.ID = "ChkUser";
            chkList.AutoPostBack = false;
            chkList.RepeatColumns = 3;
            //chkList.TextAlign = TextAlign.Right;
            //chkList.TextAlign = TextAlign.Left;
            //chkList.RepeatDirection =RepeatDirection.Vertical ;
            chkList.DataSource = objContractTemplate.SelectDataRequestType();
            chkList.DataValueField = "Id";
            chkList.DataTextField = "Name";
            // chkList.TextAlign = TextAlign.Right;
            chkList.DataBind();

            chkList.Items.RemoveAt(0);

        }
        catch (Exception ex)
        {
            Page.TraceWarn("Request dropdown bind fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Request type bind ends.");
    }


    void ReadData()
    {
        Page.TraceWrite("ReadData starts.");
        try
        {
            objContractTemplate.ContractTemplateId = int.Parse(hdnAddEditCheck.Value);
            //**********Amol-7-7-14 for request id is reqired while edit case
            objContractTemplate.ContractTypeId = int.Parse(Request.extValue("hdnContractTypeId"));
            //**********************
            List<ContractTemplate> ContractTemplate = objContractTemplate.ReadData();
            ContractTemplateIsExist = objContractTemplate.IsExistInContractQuestionsAnswersTbl();
            if (ContractTemplateIsExist == "RecordExist")
            {
                btnSave.Visible = true;
                //flash_error.Visible = true;
                lblFileUpload.Visible = false;
                lblFileUploaded.Visible = true;
                FUContractTemplate.Visible = false;                                
            }
            txtContractTemplateName.Value = ContractTemplate[0].ContractTemplateName;
            lblfilename.Text = ContractTemplate[0].TemplateFileActualName;
            HF_UploadFileName.Value = ContractTemplate[0].TemplateFileName;
            lblfilename.Visible = false;
            //****************Amol 7-7-14 while edit case
            string RequestTypeIds = ContractTemplate[0].RequestTypeIds;
            if (RequestTypeIds != "")
            {
                string[] lines = RequestTypeIds.Split(',');
                for (int i = 0; i < lines.Length; i++)
                {
                    //chkList.SelectedValue = lines[i];
                    //if (lines[i] != "5")
                        chkList.Items.FindByValue(lines[i].ToString()).Selected = true;
                }
            }

            //******************************************


            string filename = "";
            string subPath = "../Uploads/" + Session["TenantDIR"] + "/Contract Template";
            filename = subPath + "/" + HF_UploadFileName.Value;

            FileDownload.HRef = filename;
            //FileDownload.Title = lblfilename.Text;
            //Response.AddHeader("Content-Disposition", "attachment; filename=" + lblfilename.Text);
            lnkDOwnload.Text = lblfilename.Text; ;
            lnkDOwnload.Visible = true;
            FileDownload.InnerText = lblfilename.Text;
            FileDownload.Visible = false;

            txtDescription.Value = ContractTemplate[0].Description;

            //ddlRequestType.extSelectedValues(ContractTemplate[0].RequestTypeId.ToString());

            btnSaveAndContinue.Visible = false;

            if (ContractTemplate[0].isActive == "Y")
            {
                rdActive.Checked = true;
            }
            else
            {
                rdInactive.Checked = true;
            }

        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData ends.");
    }

    #region Bharati 23052014 6PM
    private void UpdateFieldLibrary(string ContractTempId)
    {
        if (ContractTemplateIsExist != "RecordExist")
        {
            Credentials credentials = new Credentials();
            credentials.IP = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
            credentials.UserId = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            credentials.UpdateFieldLibrary(ViewState["FilePath"].ToString(), Convert.ToInt32(ContractTempId));
        }
    }
    #endregion

    void InsertUpdate(int flg = 0)
    {
        Page.TraceWrite("Insert, Update starts.");
        string status = "";
        string filename = "";
        string NewFileName = "";
        string FinalFileName = "";
        string ext = "";
        string subPath = "";
        bool isValidTemplate = false;
        if (btnSave.Text == "Create")
        {


            //isValidTemplate = CheckFileUploadFile();
            if (FUContractTemplate.HasFile)
            {
                subPath = @"~/Uploads/" + Session["TenantDIR"] + "/Contract Template";
                bool isExists = System.IO.Directory.Exists(Server.MapPath(subPath));
                if (!isExists)
                {
                    System.IO.Directory.CreateDirectory(Server.MapPath(subPath));
                }
                filename = System.IO.Path.GetFileName(FUContractTemplate.PostedFile.FileName);
                ext = Path.GetExtension(filename);
                if (filename.Contains('.'))
                {
                    NewFileName = filename.Substring(0, filename.LastIndexOf('.'));
                }

                FinalFileName = GetNewFileName(NewFileName);
                if (ext == ".doc" || ext == ".docx" || ext == ".DOC" || ext == ".DOCX")
                {
                    FUContractTemplate.PostedFile.SaveAs(Server.MapPath(subPath + "/" + FinalFileName + ext));
                    ViewState["FilePath"] = Server.MapPath(subPath + "/" + FinalFileName + ext);
                    try
                    {
                        Credentials credentials = new Credentials();// Added by Bharati
                        isValidTemplate = credentials.IsValidTemplate(ViewState["FilePath"].ToString()); // Added by Bharati
                    }
                    catch (NotSupportedException ex)
                    {
                        Page.JavaScriptClientScriptBlock("NotSupportedException", "MessageMasterDiv('Unable to find licence copy for Aspose.Word.', 1, 100)");
                        Page.TraceWrite("Aspose.lic file not found." + ex.Message);
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                    catch (Exception ex)
                    {
                        Page.TraceWrite("IsValidTemplate Check failed." + ex.Message);
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }
            }

        }
        else
        {
            //if (FUContractTemplate.HasFile)
            //{
            //    isValidTemplate = CheckFileUploadFile();
            //}
            if (FUContractTemplate.HasFile)
            {
                subPath = @"~/Uploads/" + Session["TenantDIR"] + "/Contract Template";
                bool isExists = System.IO.Directory.Exists(Server.MapPath(subPath));
                if (!isExists)
                {
                    System.IO.Directory.CreateDirectory(Server.MapPath(subPath));
                }
                filename = System.IO.Path.GetFileName(FUContractTemplate.PostedFile.FileName);
                ext = Path.GetExtension(filename);
                if (filename.Contains('.'))
                {
                    NewFileName = filename.Substring(0, filename.LastIndexOf('.'));
                }

                FinalFileName = GetNewFileName(NewFileName);
                if (ext == ".doc" || ext == ".docx" || ext == ".DOC" || ext == ".DOCX")
                {
                    FUContractTemplate.PostedFile.SaveAs(Server.MapPath(subPath + "/" + FinalFileName + ext));
                    ViewState["FilePath"] = Server.MapPath(subPath + "/" + FinalFileName + ext);
                    try
                    {
                        Credentials credentials = new Credentials();// Added by Bharati
                        isValidTemplate = credentials.IsValidTemplate(ViewState["FilePath"].ToString()); // Added by Bharati
                    }
                    catch (NotSupportedException ex)
                    {
                        Page.JavaScriptClientScriptBlock("NotSupportedException", "MessageMasterDiv('Unable to find licence copy for Aspose.Word.', 1, 100)");
                        Page.TraceWrite("Aspose.lic file not found." + ex.Message);
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                    catch (Exception ex)
                    {
                        Page.TraceWrite("IsValidTemplate Check failed." + ex.Message);
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }

            }
            else
            {
                subPath = @"~/Uploads/" + Session["TenantDIR"] + "/Contract Template";
                filename = FileDownload.InnerText;
                ext = Path.GetExtension(filename);
                if (filename.Contains('.'))
                {
                    NewFileName = filename.Substring(0, filename.LastIndexOf('.'));
                }

                FinalFileName = GetNewFileName(NewFileName);
                if (ext == ".doc" || ext == ".docx" || ext == ".DOC" || ext == ".DOCX")
                {
                    File.Copy(Path.Combine(Server.MapPath(subPath + "/" + HF_UploadFileName.Value)), Path.Combine(Server.MapPath(subPath + "/" + FinalFileName + ext)), true);
                    ViewState["FilePath"] = Server.MapPath(subPath + "/" + FinalFileName + ext);
                    try
                    {
                        Credentials credentials = new Credentials();// Added by Bharati
                        isValidTemplate = credentials.IsValidTemplate(ViewState["FilePath"].ToString()); // Added by Bharati
                    }
                    catch (NotSupportedException ex)
                    {
                        Page.JavaScriptClientScriptBlock("NotSupportedException", "MessageMasterDiv('Unable to find licence copy for Aspose.Word.', 1, 100)");
                        Page.TraceWrite("Aspose.lic file not found." + ex.Message);
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                    catch (Exception ex)
                    {
                        Page.TraceWrite("IsValidTemplate Check failed." + ex.Message);
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }

            }
        }
        if (isValidTemplate)
        {

            objContractTemplate.ContractTemplateName = txtContractTemplateName.Value.Trim();
            objContractTemplate.Description = txtDescription.Value;
            objContractTemplate.ContractTypeId = int.Parse(hdnSetContractTypeId.Value);
            //objContractTemplate.RequestTypeId = int.Parse(ddlRequestType.Value);

            //---Amol

            string AddIDs = string.Empty;
            string IDs = string.Empty;
            string FinalIDs = string.Empty;
            for (int i = 0; i < chkList.Items.Count; i++)
            {
                if (chkList.Items[i].Selected)
                {

                    IDs = chkList.Items[i].Value;
                    if (FinalIDs != "")
                    {
                        FinalIDs = FinalIDs + IDs + ",";
                    }
                    else
                    {
                        FinalIDs = IDs + ",";
                    }

                }
            }
            objContractTemplate.RequestTypeIds = FinalIDs;
            //------------End


            if (FUContractTemplate.HasFile)
            {
                objContractTemplate.TemplateFileActualName = filename;
                objContractTemplate.TemplateFileName = FinalFileName + ext;
            }
            else
            {
                objContractTemplate.TemplateFileActualName = FileDownload.InnerText;
                objContractTemplate.TemplateFileName = FinalFileName + ext;
            }

            objContractTemplate.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            objContractTemplate.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            objContractTemplate.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
            objContractTemplate.isActive = rdActive.Checked == true ? "Y" : "N";

            try
            {
                if (hdnPrimeId.Value == "0")
                {
                    objContractTemplate.ContractTemplateId = 0;
                    status = objContractTemplate.InsertRecord();
                    ArrayList ALParametersInsert = new ArrayList(status.Split(','));
                    status = ALParametersInsert[0].ToString();
                    string ContractTempId = string.Empty;
                    ContractTempId = ALParametersInsert[1].ToString();
                    Page.Message(status, hdnPrimeId.Value);
                }
                else
                {
                    objContractTemplate.ContractTemplateId = int.Parse(hdnAddEditCheck.Value);
                    status = objContractTemplate.UpdateRecord();
                    ArrayList ALParametersUpdate = new ArrayList(status.Split(','));
                    status = ALParametersUpdate[0].ToString();
                    //string ContractTempId = string.Empty;
                    ContractTempId = ALParametersUpdate[1].ToString();

                    //Addedd by Bharati 18June2014 3.04PM
                    if (ContractTempId != "")
                    {
                        UpdateFieldLibrary(ContractTempId);
                    }

                    Page.Message(status, hdnAddEditCheck.Value);
                    
                }

            }
            catch (Exception ex)
            {
                //if (ContractTempId == "")
                //{
                //    ClearDataForRefresh();
                //}
                //else
                //{
                Page.Message("0", hdnPrimeId.Value);
                Page.TraceWarn("Insert, Update fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //}
            }
            Page.TraceWrite("Insert, Update ends.");

            if (flg == 0 && status == "1")
            {
                Session[Declarations.Message] = (hdnAddEditCheck.Value == "0") ? "0" : "1";
                //Response.Redirect("ContractTypeMaster.aspx");
                Server.Transfer("ContractTypeMaster.aspx");

            }
            ClearData(status);
            //if (flg == 0 && status!="2")
            //{
            //    Back_Click(btnBack, null);
            //}

        }
        else
        {
            Page.JavaScriptClientScriptBlock("a", "MessageMasterDiv('Invalid Contract Template.', 1, 100)");
        }

    }
    private bool CheckFileUploadFile()
    {

        string filename = "";
        string NewFileName = "";
        string FinalFileName = "";
        string ext = "";
        string subPath = "";
        bool isValidTemplate = false;
        if (FUContractTemplate.HasFile)
        {
            subPath = @"~/Uploads/" + Session["TenantDIR"] + "/Contract Template";
            bool isExists = System.IO.Directory.Exists(Server.MapPath(subPath));
            if (!isExists)
            {
                System.IO.Directory.CreateDirectory(Server.MapPath(subPath));
            }
            filename = System.IO.Path.GetFileName(FUContractTemplate.PostedFile.FileName);
            ext = Path.GetExtension(filename);
            if (filename.Contains('.'))
            {
                NewFileName = filename.Substring(0, filename.LastIndexOf('.'));
            }

            FinalFileName = GetNewFileName(NewFileName);
            if (ext == ".doc" || ext == ".docx" || ext == ".DOC" || ext == ".DOCX")
            {
                FUContractTemplate.PostedFile.SaveAs(Server.MapPath(subPath + "/" + FinalFileName + ext));
                ViewState["FilePath"] = Server.MapPath(subPath + "/" + FinalFileName + ext);
                try
                {
                    Credentials credentials = new Credentials();// Added by Bharati
                    isValidTemplate = credentials.IsValidTemplate(ViewState["FilePath"].ToString()); // Added by Bharati
                }
                catch (NotSupportedException ex)
                {
                    Page.JavaScriptClientScriptBlock("NotSupportedException", "MessageMasterDiv('Unable to find licence copy for Aspose.Word.', 1, 100)");
                    Page.TraceWrite("Aspose.lic file not found." + ex.Message);
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
                catch (Exception ex)
                {
                    Page.TraceWrite("IsValidTemplate Check failed." + ex.Message);
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }

        }
        return isValidTemplate;
    }
    private string GetNewFileName(string NewFileName)
    {
        string Datetime = DateTime.Now.ToString("yyyyMMdd_hhmmss_fffff");
        string fname = "";
        fname = NewFileName + "_" + Datetime;
        return fname;
    }

    void ClearData(string status)
    {
        if (status == "1")
        {
            Page.TraceWrite("clearData starts.");
            hdnPrimeId.Value = "0";
            objContractTemplate.ContractTemplateId = 0;
            txtContractTemplateName.Value = "";

            FUContractTemplate.Attributes.Clear();
            txtDescription.Value = "";
            rdInactive.Checked = false;
            rdActive.Checked = true;
            RequestTypeBind();
            Page.TraceWrite("clear Data ends.");

        }

    }

    void ClearDataForRefresh()
    {
        txtContractTemplateName.Value = "";
        FUContractTemplate.Attributes.Clear();
        txtDescription.Value = "";
        rdInactive.Checked = false;
        rdActive.Checked = true;

    }

    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (Add == "N" && (hdnPrimeId.Value == "" || hdnPrimeId.Value == "0" || hdnPrimeId.Value == null))
        {
            Page.extDisableControls();
            btnBack.Enabled = true;
        }
        if (View == "N")
        {
            btnBack.Enabled = false;
        }
        Page.TraceWrite("AccessVisibility starts.");
    }



    protected void lnkDOwnload_Click(object sender, EventArgs e)
    {
        string filename = "";
        string subPath = @"~/Uploads/" + Session["TenantDIR"] + "/Contract Template";
        bool isExists = System.IO.Directory.Exists(Server.MapPath(subPath));
        if (!isExists)
        {
            Page.JavaScriptStartupScript("displayalertmessage", "alert('File not exist')");
        }
        filename = Server.MapPath(subPath) + "\\" + HF_UploadFileName.Value;

        //Response.AddHeader("Content-Disposition", "attachment; filename=" + lblfilename.Text);
        Response.AddHeader("Content-Disposition", string.Format("attachment; filename = \"{0}\"", System.IO.Path.GetFileName("" + lblfilename.Text + "")));
        Response.WriteFile(filename);
        Response.End();

    }
}










