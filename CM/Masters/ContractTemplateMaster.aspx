﻿<%@ Page Language="C#" MasterPageFile="~/CM.master" ClientIDMode="Static" AutoEventWireup="true"
    CodeFile="ContractTemplateMaster.aspx.cs" Inherits="Masters_ContractTemplateMaster" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Configuratorlinks"
    TagPrefix="uc1" %>
<%--<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>--%>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");
    </script>
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <%-- <h2>
        Contract Template</h2>--%>
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="HF_UploadFileName" ClientIDMode="Static" runat="server" />
    <input id="hdnSetContractTypeId" clientidmode="Static" runat="server" type="hidden" />
    <input id="hdnAddEditCheck" clientidmode="Static" runat="server" type="hidden" />
    <input id="hdnPrimeIds" clientidmode="Static" runat="server" type="hidden" />
    <input id="hdntypidtoredirect" clientidmode="Static" runat="server" type="hidden" />
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
    </h2>
    <div>
        <div runat="server" class="flash error" id="flash_error">
            Contract file is already generated for this template so you cannot edit this template.
        </div>
        <div>
            <table>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblContractType" runat="server" Text=""  for="time_entry_issue_id">Contract Type</asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblcontracttypeName" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
             
                <tr>
                    <td align="right" valign="top">
                        <asp:Label ID="lblContractTemplateName" runat="server" Text="" Font-Bold="True" for="time_entry_issue_id"><span class="required">*</span>Contract Template Name</asp:Label>
                    </td>
                    <td>
                        <input id="txtContractTemplateName" runat="server" type="text" class="required ui-autocomplete-input"
                            style="width: 220px" maxlength="450" autocomplete="off" role="textbox" aria-autocomplete="list"
                            aria-haspopup="true" />
                    </td>
                </tr>
              
                <tr>
                    <td align="right" valign="top">
                        <asp:Label ID="lblRequestType" runat="server" Text="" Font-Bold="True" for="time_entry_issue_id">Request Type</asp:Label>
                    </td>
                    <td>
                     
                        <asp:CheckBoxList ID="chkList" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                            AutoPostBack="True">
                        </asp:CheckBoxList>
                    </td>
                </tr>
               
                <tr>
                    <td align="right" valign="top">
                        <asp:Label ID="lblFileUpload" runat="server" Text="" for="time_entry_issue_id"><span class="required">*</span>File Upload</asp:Label>
                        <asp:Label ID="lblFileUploaded" runat="server" Text=""  Visible="false" for="time_entry_issue_id">File Uploaded</asp:Label>
                    </td>
                    <td>
                        <asp:FileUpload ID="FUContractTemplate" runat="server" Onchange="return FileUploadSelectFile();"
                            Width="180px" />
                        <asp:Label ID="lblfilename" runat="server" Visible="False" ClientIDMode="Static"></asp:Label>
                        <a href="#" runat="server" id="FileDownload">File Download</a>
                        <asp:LinkButton ID="lnkDOwnload" runat="server" OnClick="lnkDOwnload_Click" class="icon icon-attachment"></asp:LinkButton>
                    </td>
                </tr>
               
                <tr>
                    <td align="right" valign="top">
                        <asp:Label ID="lblDescription" runat="server" Font-Bold="True" Text="" for="time_entry_issue_id">Description</asp:Label>
                    </td>
                    <td>
                        <textarea id="txtDescription" placeholder="Contract Template description" runat="server"
                            maxlength="100" clientidmode="static" type="text" autocomplete="false" style="width: 50
                            0px;
                            height: 100px" rows="10" />
                    </td>
                </tr>
               
                <tr>
                    <td align="right">
                        <asp:Label ID="lblStatus" runat="server" Text="" Font-Bold="True" for="time_entry_issue_id">Status</asp:Label>
                    </td>
                    <td>
                        <input id="rdActive" name="ActiveInactive" runat="server" type="radio" class="required" />Active
                        <input id="rdInactive" name="ActiveInactive" runat="server" type="radio" class="required" />Inactive
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" class="btn_validate submit"
        OnClientClick="return FileUploadValidation(this);" />
    <asp:Button ID="btnSaveAndContinue" runat="server" Text="Save and Add more" OnClick="SaveAndContinue_Click"
        OnClientClick="return FileUploadValidation(this);" class="btn_validate submit" />
    <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back_Click" OnClientClick="return SetValueBackButtonClick(this); " />
    <div id="rightlinks" style="display: none;">
        <uc1:Configuratorlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $('.submit').live("click", function () {
            ShowProgress();
        });
    </script>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
    <script type="text/javascript">



        $("input").on("keypress", function (e) {
            if (e.which === 32 && !this.value.length)
                e.preventDefault();
        });


     

        function filevlidation() {
            alert('Please upload .doc file.');
            return true;
        }

        function DownloadFile() {

            $.ajax({
                'type': 'POST',
                'url': 'DownloadContractTemplate.ashx',
                'data': {
                    'HF_UploadFileName': $('#HF_UploadFileName').val(),
                    'lblfilename': $('#lblfilename').val()
                },
                'success': function (msg) {
                    alert('ok');
                }
            });
        }
        $().ready(function () {
            if ($("#lnkDOwnload").text() == "")
                $("#lnkDOwnload").hide();
            else
                $("#lnkDOwnload").show();
        });

        function FileUploadSelectFile() {
            $("#lnkDOwnload").hide();
            var id_value = document.getElementById("<%=FUContractTemplate.ClientID %>").value;
            if (id_value != '') {

                var valid_extensions = /(.doc|.docx)$/i;
                if (valid_extensions.test(id_value)) {
                    $("#FileDownload").hide();
                    $("#lnkDOwnload").hide();
                    document.getElementById("<%=FUContractTemplate.ClientID %>").setAttribute('style', 'color:null');
                    return true;
                }
                else {
                    alert('Please upload only .doc and .docx extension file.')
                    document.getElementById("FUContractTemplate").value = '';
                    $("#FileDownload").show();
                    $("#lnkDOwnload").show();
                    return false;
                }
            }
            else {
                document.getElementById("<%=FUContractTemplate.ClientID %>").setAttribute('style', 'color:transparent');
                $("#FileDownload").show();
                $("#lnkDOwnload").show();
                return true;
            }

        }

        function validate() {

            var array = ['doc', 'docx'];
            var fileuploadcontrol = document.getElementById("FUContractTemplate");
            var Extension = fileuploadcontrol.value.substring(xyz.value.lastIndexOf('.') + 1).toLowerCase();
            if (array.indexOf(Extension) <= -1) {
                alert("Please upload only .doc and .docx extension file.");
                return false;
            }
        }


        function FileUploadValidation(obj) {            
            var rval = false;
            var id_value = document.getElementById("<%=FUContractTemplate.ClientID %>").value;
            var FileName = $("#FUContractTemplate").next("#lnkDOwnload").text();
            debugger;
            if (id_value == '' && FileName=="") {
                alert('Please upload file.')
                Thread.Sleep(300);
                rval = false;
            }
            else {
                $('#hdntypidtoredirect').val($('#hdnSetContractTypeId').val());
                rval = true;
            }
            return rval;
        }





//        function FileUploadValidation(obj) {
//            var btn_value = $('#btnSave').val();
//            var btnSaveAndContinue_value = $('#btnSaveAndContinue').val();
//            if (btn_value == 'Create' || btnSaveAndContinue_value == 'Create and continue') {

//                var id_value = document.getElementById("<%=FUContractTemplate.ClientID %>").value;
//                if (id_value == '') {
//                    alert('Please upload file.')
//                    Thread.Sleep(300);
//                    return false;
//                }
//                $('#hdntypidtoredirect').val($('#hdnSetContractTypeId').val());
//                return true;
//            }
//            else {

//                $('#hdntypidtoredirect').val($('#hdnSetContractTypeId').val());
//                return true;
//            }
//        }






        function SetValueBackButtonClick(obj) {
           
            $('#hdntypidtoredirect').val($('#hdnSetContractTypeId').val());
            return true;
        }

        function timedRefresh(timeoutPeriod) {
            setTimeout("location.href='ContractTypeMaster.aspx?ID=" + $('#hdnSetContractTypeId').val() + "'", timeoutPeriod);
        }
        function refershPage() {
            alert('hI');
            timedRefresh(2000);

        }
    </script>
</asp:Content>
