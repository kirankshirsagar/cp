﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;



public partial class Masters_ContractTypeMaster : System.Web.UI.Page
{
    // navigation//
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 10;
    // navigation//
    IContractType objContract;
    IContractTemplate objContractTemplate;
    public string Add;
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
             
        if (!IsPostBack)
        {
            if (String.IsNullOrEmpty(Request.extValue("hdnPrimeIds")) != true)
            {
                hdnPrimeId.Value = Request.extValue("hdnPrimeIds");
                hdnContractTypeId.Value = Request.extValue("hdnPrimeIds");
                hdndelete.Value = hdnContractTypeId.Value;
                ReadData();
                lblTitle.Text = "Edit Contract Type";
                btnSave.Text = "Update";

            }
            else if (String.IsNullOrEmpty(Request.extValue("hdnPrimeId")) != true)
            {

                if (Request.extValue("hdnPrimeId") == "0")
                {
                    hdnPrimeId.Value = Request.extValue("hdntypidtoredirect");
                    hdnContractTypeId.Value = Request.extValue("hdntypidtoredirect");
                    ReadData();
                    lblTitle.Text = "Edit Contract Type";
                    btnSave.Text = "Update";
                }
            
                else
                {
                    hdnPrimeId.Value = Request.extValue("hdnPrimeId");
                    hdnContractTypeId.Value = Request.extValue("hdnPrimeId");
                    ReadData();
                    lblTitle.Text = "Edit Contract Type";
                    btnSave.Text = "Update";
                }

            }
            else if (String.IsNullOrEmpty(Request.extValue("hdnContractTypeId")) != true)
            {
                hdnPrimeId.Value = Request.extValue("hdnContractTypeId");
                hdnContractTypeId.Value = Request.extValue("hdnContractTypeId");
                ReadData();
                lblTitle.Text = "Edit Contract Type";
                btnSave.Text = "Update";
            }

            //else if (String.IsNullOrEmpty(Request.extValue("hdntypidtoredirect")) != true)
            //{
            //    hdnPrimeId.Value = Request.extValue("hdntypidtoredirect");
            //    hdnContractTypeId.Value = Request.extValue("hdntypidtoredirect");
            //    ReadData();
            //    lblTitle.Text = "Edit Contract Type";
            //    btnSave.Text = "Update";
            //}
                // replaced by  BHarati 07 Aug 2014
            else if (String.IsNullOrEmpty(Request.extValue("hdnSetContractTypeId")) != true)
            {
                hdnPrimeId.Value = Request.extValue("hdnSetContractTypeId");
                hdnContractTypeId.Value = Request.extValue("hdnSetContractTypeId");
                ReadData();
                lblTitle.Text = "Edit Contract Type";
                btnSave.Text = "Update";
            }
           else
            {
                rdActive.Checked = true;
                hdnPrimeId.Value = "0";
                lblTitle.Text = "Add Contract Type";
                PaginationButtons1.Visible = false;
                PanelContractTemplate.Visible = false;
            }

            if (btnSave.Text == "Update")
            {
                Session[Declarations.SortControl] = null;
                ReadData(1, RecordsPerPage);
                Message();
            }

            if (Session[Declarations.User] != null)
            {
                Access.PageAccess(this, Session[Declarations.User].ToString(), "masters_");
                ViewState[Declarations.Add] = Access.Add.Trim();
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
            }
            AccessVisibility();
        }
        else
        {
            SetNavigationButtonParameters();
        }
        if (hdnSearch.Value.Trim() != string.Empty)
        {
            Session[Declarations.SortControl] = null;
            rdActive.Checked = true;
            ReadData(1, RecordsPerPage);
            Message();
        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
            AccessVisibility();
        }
    }

    protected void btnSort_Click(object sender, EventArgs e)
    {
        hdnSearch.Value = "";
        LinkButton clickBtn = (LinkButton)sender;
        Session.Add(Declarations.SortControl, clickBtn);
        ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
    }
   
    void SetNavigationButtonParameters()
    {
        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }
    void Message()
    {
        if (Session[Declarations.Message] != null)
        {
            var flg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objContract = FactoryMaster.GetContractTypeDetail();
            objContractTemplate = FactoryMaster.GetContractTemplateDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        if (btnSave.Text == "Create")
        {
            InsertUpdate();
        }
        else
        {
            InsertUpdate(1);
            Server.Transfer("ContractTypeMasterData.aspx");
        }
        Page.TraceWrite("Save Update button clicked event ends.");
    }

    protected void SaveAndContinue_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save and Add more button clicked event starts.");
        InsertUpdate(1);
        Page.TraceWrite("Save and Add more button clicked event ends.");
    }

    protected void Back_Click(object sender, EventArgs e)
    {
        Server.Transfer("ContractTypeMasterData.aspx");
    }

    void ReadData()
    {
        Page.TraceWrite("ReadData starts.");
        try
        {
            objContract.ContractTypeId = int.Parse(hdnPrimeId.Value);
            List<ContractType> Contract = objContract.ReadDataRequestType();
            txtContracttypeName.Text = Contract[0].ContractTypeName;
            txtDescription.Value = Contract[0].Description;
            
            if (Contract[0].isActive == "Y")
            {
                rdActive.Checked = true;
            }
            else
            {
                rdInactive.Checked = true;
            }

        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData ends.");
    }
    void ReadData(int pageNo, int recordsPerPage)
    {
        Page.TraceWrite("ReadData starts.");
        try
        {
            objContractTemplate.PageNo = pageNo;
            objContractTemplate.RecordsPerPage = recordsPerPage;
            objContractTemplate.Search = txtSearch.Value.Trim();
          
            if (txtSearch.Value != string.Empty)
            {
                //objContractTemplate.ContractTypeId = 0;
                objContractTemplate.ContractTypeId = int.Parse(hdnContractTypeId.Value);
            }
            else
            {
                //objContractTemplate.ContractTypeId = int.Parse(hdnPrimeId.Value);
                objContractTemplate.ContractTypeId = int.Parse(hdnContractTypeId.Value);
            }
            LinkButton btnSort = null;
            if (Session[Declarations.SortControl] != null && Session[Declarations.SortControl] != string.Empty)
            {
                btnSort = (LinkButton)Session[Declarations.SortControl];
                objContractTemplate.Direction = btnSort.CssClass.ToLower() == "sort desc" ? 1 : 0;
                objContractTemplate.SortColumn = btnSort.Text.Trim();
            }
            rptContractTemplateMaster.DataSource = objContractTemplate.ReadData();
            rptContractTemplateMaster.DataBind();
            if (btnSort != null)
            {
                rptContractTemplateMaster.ClassChange(btnSort);
            }
            Page.TraceWrite("ReadData ends.");
            ViewState[Declarations.TotalRecord] = objContractTemplate.TotalRecords;
            PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
            PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
            PaginationButtons1.RecordsPerPage = RecordsPerPage;
           
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }


    }
    protected void imgEdit_Click(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Request.extValue("hdnPrimeId")) != true)
        {
            hdnNewTemplateOREditTemplate.Value = Request.extValue("hdnPrimeId");

        }
        
        Server.Transfer("ContractTemplateMaster.aspx");
    }
    protected void lnkDownload_Click(object sender, EventArgs e)
    {
        string filename = "";
        string subPath = @"~/Uploads/" + Session["TenantDIR"] + "/Contract Template";
        bool isExists = System.IO.Directory.Exists(Server.MapPath(subPath));
        if (!isExists)
        {
            Page.JavaScriptStartupScript("displayalertmessage", "alert('File not exist')");
        }
        filename = Server.MapPath(subPath) + "\\" + HF_UploadFileName.Value;

        //Response.AddHeader("Content-Disposition", "attachment; filename=" + HF_TemplateFileActualName.Value);
        Response.AddHeader("Content-Disposition", string.Format("attachment; filename = \"{0}\"", System.IO.Path.GetFileName("" + HF_TemplateFileActualName.Value + "")));
        Response.WriteFile(filename);
        Response.End();

        HF_UploadFileName.Value = string.Empty;
        HF_TemplateFileActualName.Value = string.Empty;
    }


    void InsertUpdate(int flg = 0)
    {
        Page.TraceWrite("Insert, Update starts.");
        string status = "";
        objContract.ContractTypeName = txtContracttypeName.Text.Trim();
        objContract.isActive = rdActive.Checked == true ? "Y" : "N";
        objContract.Description = txtDescription.Value;
        objContract.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objContract.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objContract.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;

        try
        {
            if (hdnPrimeId.Value == "0")
            {
                objContract.ContractTypeId = 0;
                status = objContract.InsertRecord();
            }
            else
            {
                objContract.ContractTypeId = int.Parse(hdnPrimeId.Value);
                status = objContract.UpdateRecord();

            }
            Page.Message(status, hdnPrimeId.Value);
        }
        catch (Exception ex)
        {
            Page.Message("0", hdnPrimeId.Value);
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Insert, Update ends.");

        if (flg == 0 && status == "1")
        {
            Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
            Response.Redirect("ContractTypeMasterData.aspx");
            //Server.Transfer("ContractTypeMasterData.aspx");
        }
        ClearData(status);
    }

    void ClearData(string status)
    {
        if (status == "1")
        {
            Page.TraceWrite("clearData starts.");
            objContract.ContractTypeId = 0;
            hdnPrimeId.Value = "0";
            txtContracttypeName.Text = "";
            txtDescription.Value = "";
            Page.TraceWrite("clear Data ends.");
            rdInactive.Checked = false;
            rdActive.Checked=true;
        }
    }


    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (Access.Add == "N" && (hdnPrimeId.Value == "" || hdnPrimeId.Value == "0" || hdnPrimeId.Value == null))
        {
            Page.extDisableControls();
            btnBack.Enabled = true;
        }
        if (Access.View == "N")
        {
            btnBack.Enabled = false;
        }
        Page.TraceWrite("AccessVisibility starts.");
    }
    protected void btnAddTemplate_Click(object sender, EventArgs e)
    {
        Server.Transfer("ContractTemplateMaster.aspx");
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Delete starts.");
        string status = "";
        objContractTemplate.ContractTemplateIds = hdnPrimeId.Value;
        try
        {
            status = objContractTemplate.DeleteRecord();
        }
        catch (Exception ex)
        {
            hdnPrimeId.Value = string.Empty;
            Page.JavaScriptClientScriptBlock("status", "PageGetMessage('DF')");
            Page.TraceWarn("Delete fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        hdnPrimeId.Value = string.Empty;
        //hdndelete.Value = hdnPrimeId.Value;
        Page.JavaScriptClientScriptBlock("status", "PageGetMessage('DS')");
        Page.TraceWrite("Delete ends.");
        ReadData(1, RecordsPerPage);
  
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {

    }

}










