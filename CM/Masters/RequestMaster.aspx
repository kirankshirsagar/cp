﻿<%@ Page Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true" CodeFile="RequestMaster.aspx.cs"
    Inherits="Masters_RequestMaster" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
<link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
     <script type="text/javascript">
         $("#setupLink").addClass("menulink");
         $("#tab2").addClass("selectedtab");
    </script>
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <input id="hdnStateId" clientidmode="Static" runat="server" type="hidden" />
    <input id="hdnStateIdRead" clientidmode="Static" runat="server" type="hidden" />
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
    </h2>
    <div class="box tabular">
        <p>
            <label for="time_entry_issue_id">
                <span class="required">*</span>Request Name</label>
            <input id="txtRequestName" runat="server" type="text" class="required ui-autocomplete-input"
                maxlength="50 " autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" />
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id">
                Description</label>
            <input id="txtRequestDescription" runat="server" type="text" class="ui-autocomplete-input"
                maxlength="100" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" />
            <em></em>
        </p>
        <p>
            <input id="rdActive" name="ActiveInactive" runat="server" type="radio" class="required" />Active
            <input id="rdInactive" name="ActiveInactive" runat="server" type="radio" class="required" />Inactive
            <em></em>
        </p>
    </div>
    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" class="btn_validate" />
    <asp:Button ID="btnSaveAndContinue" runat="server" Text="Save and Add more" OnClick="SaveAndContinue_Click"
        class="btn_validate" />
    <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back_Click" />
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script> 
</asp:Content>
