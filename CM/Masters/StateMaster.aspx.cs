﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;


public partial class Masters_StateMaster : System.Web.UI.Page
{

    IState objState;
    ICountry objCountry;
    public string Add;
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {
       
        CreateObjects();

        if (!IsPostBack)
        {
            
            CountryBind();
            if (String.IsNullOrEmpty(Request.extValue("hdnPrimeIds")) != true)
            {
                hdnPrimeId.Value = Request.extValue("hdnPrimeIds");
                ReadData();
                lblTitle.Text = "Edit State";
                btnSave.Text = "Update";
            }
            else
            {
                rdActive.Checked = true;
                hdnPrimeId.Value = "0";
                lblTitle.Text = "Add State";
               

            }
            if (Session[Declarations.User] != null)
            {
                 Access.PageAccess(this, Session[Declarations.User].ToString(), "masters_");
                ViewState[Declarations.Add] = Access.Add.Trim();
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
            }
            AccessVisibility();
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objState = FactoryMaster.GetStateDetail();
            objCountry = FactoryMaster.GetCountryDetail();

        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        InsertUpdate();
        Page.TraceWrite("Save Update button clicked event ends.");
    }

    protected void SaveAndContinue_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save and Add more button clicked event starts.");
        InsertUpdate(1);
        Page.TraceWrite("Save and Add more button clicked event ends.");
    }

    protected void Back_Click(object sender, EventArgs e)
    {
        Server.Transfer("StateMasterData.aspx");
    }

    void ReadData()
    {
        Page.TraceWrite("ReadData starts.");
        try
        {
            objState.StateId = int.Parse(hdnPrimeId.Value);
            List<State> State = objState.ReadData();
            txtStateName.Value = State[0].StateName;
            txtDescription.Value = State[0].Description;
            ddlCountry.extSelectedValues(State[0].CountryId.ToString(), State[0].CountryName);
            btnSaveAndContinue.Visible = false;

            if (State[0].isActive == "Y")
            {
                rdActive.Checked = true;
            }
            else
            {
                rdInactive.Checked = true;
            }

        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData ends.");
    }

    void CountryBind()
    {
        Page.TraceWrite("Country dropdown bind starts.");
        try
        {
            ddlCountry.extDataBind(objCountry.SelectData());
        }
        catch (Exception ex)
        {
            Page.TraceWarn("Country dropdown bind fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Country dropdown bind ends.");
    }

    void InsertUpdate(int flg=0)
    {
        Page.TraceWrite("Insert, Update starts.");
        string status = "";
        objState.StateName = txtStateName.Value.Trim();
        objState.Description = txtDescription.Value.Trim();
        objState.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objState.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objState.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
        objState.isActive = rdActive.Checked == true ? "Y" : "N";
        objState.CountryId = int.Parse(ddlCountry.Value);

        try
        {
            if (hdnPrimeId.Value == "0")
            {
                objState.StateId = 0;
                status = objState.InsertRecord();
            }
            else
            {
                objState.StateId = int.Parse(hdnPrimeId.Value);
                status = objState.UpdateRecord();
            }
            Page.Message(status, hdnPrimeId.Value);

        }
        catch (Exception ex)
        {
            Page.Message("0", hdnPrimeId.Value);
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Insert, Update ends.");
        if (flg == 0 && status == "1")
        {
            Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
            Response.Redirect("StateMasterData.aspx");
            //Server.Transfer("StateMasterData.aspx");
        }
        else if (flg == 1 && status == "1")
        {
            Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
        }
        //Response.Redirect("StateMaster.aspx");
        ClearData(status);
    }

    void ClearData(string status)
       {
            if (status == "1")
            {
                Page.TraceWrite("clearData starts.");
                objState.StateId = 0;
                hdnPrimeId.Value = "0";
                txtStateName.Value = "";
                txtDescription.Value = "";
                CountryBind();
                Page.TraceWrite("clear Data ends.");
                rdInactive.Checked = false;
                rdActive.Checked = true;
            }
       }

    private void setAccessValues()
       {
           if (ViewState[Declarations.Add] != null)
           {
               Add = ViewState[Declarations.Add].ToString();
           }
           if (ViewState[Declarations.Update] != null)
           {
               Update = ViewState[Declarations.Update].ToString();
           }
           if (ViewState[Declarations.Delete] != null)
           {
               Delete = ViewState[Declarations.Delete].ToString();
           }
           if (ViewState[Declarations.View] != null)
           {
               View = ViewState[Declarations.View].ToString();
           }

       }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (Add == "N" && (hdnPrimeId.Value == "" || hdnPrimeId.Value == "0" || hdnPrimeId.Value == null))
        {
            Page.extDisableControls();
            btnBack.Enabled = true;
        }
        if (View == "N")
        {
            btnBack.Enabled = false;
        }
        Page.TraceWrite("AccessVisibility starts.");
    }


}










