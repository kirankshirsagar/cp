﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/CM.master"
    AutoEventWireup="true" CodeFile="CityMasterData.aspx.cs" Inherits="Masters_CityMasterData" %>

<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridSelect.js" type="text/javascript"></script>
    <script src="../CommonScripts/GridNavigation.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");
    </script>
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <h2>
        City</h2>
    <input id="hdnPrimeIds" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
   

    <div style="margin: 0; padding: 0; display: inline">
        <div id="query_form_content" class="hide-when-print">
            <fieldset id="filters" class="collapsible">
                <legend onclick="toggleFieldset(this);">Filters</legend>
                <div style="">
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td>
                                    <table id="filters-table" width="100%" cellpadding="3" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td width="10%">
                                                    Keywords :
                                                </td>
                                                <td width="90%">
                                                    <input id="txtSearch" clientidmode="Static" runat="server" type="text" maxlength="50"
                                                        onkeydown="return (event.keyCode!=13);" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </fieldset>
        </div>
        <p class="buttons hide-when-print">
            <asp:LinkButton ID="btnSearch" runat="server" OnClientClick="return searchClick();"
                CssClass="icon icon-checked" OnClick="btnSearch_Click">Filter</asp:LinkButton>
            <asp:LinkButton ID="btnShowAll" runat="server" OnClientClick="return resetClick();"
                CssClass="icon icon-reload" OnClick="btnSearch_Click">Reset Filter</asp:LinkButton>
            <asp:LinkButton ID="btnAddRecord" CssClass="icon icon-add" runat="server" OnClientClick="resetPrimeID();" OnClick="btnAddRecord_Click">Add new City</asp:LinkButton>
            <asp:LinkButton ID="btnDelete" CssClass="icon icon-del" runat="server" OnClientClick="return GetSelectedItems('D');"
                OnClick="btnDelete_Click">Delete</asp:LinkButton>
        </p>
        <div class="autoscroll">
            <table width="100%">
                <asp:Repeater ID="rptCityMaster" runat="server">
                    <HeaderTemplate>
                        <table id="masterDataTable" class="masterTable list issues" width="100%">
                            <thead>
                                <tr>
                                    <th style="text-align: left; padding-left: 2px;" width="1%">
                                        <input id="chkSelectAll" type="checkbox" class="checkbox maincheckbox" onkeydown="return (event.keyCode!=13);" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </th>
                                    <th class="checkbox hide-when-print" width="3%">
                                        &nbsp;
                                    </th>
                                    <th width="0%" style="display: none;">
                                        City Id
                                    </th>
                                    <th width="20%">
                                        <asp:LinkButton ID="btnSortCityName" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">City Name</asp:LinkButton>
                                    </th>
                                    <th width="20%">
                                        <asp:LinkButton ID="btnSortStateName" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">State Name</asp:LinkButton>
                                    </th>
                                    <th width="20%">
                                        <asp:LinkButton ID="btnSortCountryName" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Country Name</asp:LinkButton>
                                    </th>
                                    <th width="25%">
                                        <asp:LinkButton ID="btnSortDescription" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Description</asp:LinkButton>
                                    </th>
                                    <th width="15%">
                                        <asp:LinkButton ID="btnSortStatus" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Status</asp:LinkButton>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: left; padding-bottom: 2px">
                                <input type="checkbox" onchange="SetMainCheckbox(this);" class="mid-margin-left"
                                    onkeydown="return (event.keyCode!=13);" />
                            </td>
                            <td style="text-align: left; padding-bottom: 2px">
                                <img id="imgLock" alt="img.." class="mws-tooltip-e" title="Used" border="0" />
                            </td>
                            <td style="display: none">
                                <asp:Label ID="lblCityId" runat="server" ClientIDMode="Static" Text='<%#Eval("CityId") %>'></asp:Label>
                                <asp:Label ID="lblIsUsed" runat="server" ClientIDMode="Static" Text='<%#Eval("isUsed") %>'></asp:Label>
                                <asp:Label ID="lblCityName" runat="server" ClientIDMode="Static" Text='<%#Eval("CityName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:LinkButton ID="imgEdit" runat="server" OnClientClick="return setSelectedId(this);"
                                    OnClick="imgEdit_Click"><%#Eval("CityName")%></asp:LinkButton>
                            </td>
                            <td>
                                <asp:Label ID="lblStateName" runat="server" ClientIDMode="Static" Text='<%#Eval("StateName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lclCountryName" runat="server" ClientIDMode="Static" Text='<%#Eval("CountryName") %>'></asp:Label>
                            </td>
                            <td style="word-break: break-all; word-wrap: break-word">
                                <asp:Label ID="lbldecsription" runat="server" ClientIDMode="Static" Text='<%#Eval("Description").ToString().Replace("\n", "<br>") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblStatus" runat="server" ClientIDMode="Static" Text='<%#Eval("isActive") %>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
            <asp:PlaceHolder runat="server" ID="Placeholder1">
                <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
            </asp:PlaceHolder>
        </div>
        <table style="display: none" width="100%">
            <tr>
                <td align="left" width="10%">
                </td>
                <td runat="server" id="actinacttd" style="padding-right: 60%" align="left" width="90%">
                    <input id="rdActive" name="ActiveInactive" runat="server" type="radio" />Active
                    <input id="rdInactive" name="ActiveInactive" runat="server" type="radio" />Inactive
                    <asp:Button ID="btnChangeStatus" runat="server" Text="Status" OnClientClick="return GetSelectedItems('S');"
                        OnClick="btnChangeStatus_Click" />
                </td>
            </tr>
        </table>
        <script type="text/javascript">

            function GetSelectedItems(flg) {
                $('#hdnPrimeIds').val('');
                $('#hdnUsedNames').val('');
                var tableClass = 'masterTable';
                var deleteLabelId = 'lblCityId';
                var deleteLabelName = 'lblCityName';
                var objCheck = new CkeckBoxSelect(tableClass, deleteLabelId, deleteLabelName);
                var deletedIds = objCheck.DeletedIds;
                var usedNames = objCheck.UsedNames;

                if (deletedIds == '') {
                    MessageMasterDiv('Please select record(s).');
                    return false;
                }
                else if (usedNames != '' && flg == 'D') {
                    MessageMasterDiv('Some records can not be deleted.' + usedNames, 1);
                    return false;
                }
                if (flg == 'D') {
                    if (DeleteConfrim() == true) {
                        $('#hdnPrimeIds').val(deletedIds);
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    $('#hdnPrimeIds').val(deletedIds);
                    return true;
                }
                $('#hdnPrimeIds').val(deletedIds);
                return true;
            }


            $(document).ready(function () {
                defaultTableValueSetting('lblCityId', 'MstCity', 'CityId');
                LockUnLockImage('masterTable');
            });


            function setSelectedId(obj) {
                var pId = $(obj).closest('tr').find('#lblCityId').text();
                if (pId == '' || pId == '0') {
                    return false;
                }
                else {
                    $('#hdnPrimeIds').val(pId);
                    return true;
                }
            }



        </script>
        <div id="rightlinks" style="display: none;">
            <uc1:Masterlinks ID="rightactions" runat="server" />
        </div>
        <script type="text/javascript">
            $("#sidebar").html($("#rightlinks").html());
        </script>
</asp:Content>
