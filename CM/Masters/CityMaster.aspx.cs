﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;
using System.Web.Security;


public partial class Masters_CityMaster : System.Web.UI.Page
{

    ICity objCity;
    ICountry objCountry;
    public string Add;
    public string View;
    public string Update;
    public string Delete;
    public string ddlsatevalue = "";


    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();

        if (!IsPostBack)
        {

            CountryBind();
            if (String.IsNullOrEmpty(Request.extValue("hdnPrimeIds")) != true)
            {
                hdnPrimeId.Value = Request.extValue("hdnPrimeIds");
                ReadData();
                lblTitle.Text = "Edit City";
                btnSave.Text = "Update";
            }
            else
            {
                rdActive.Checked = true;
                hdnPrimeId.Value = "0";
                lblTitle.Text = "Add City";
            }
            if (Session[Declarations.User] != null)
            {
                Access.PageAccess(this, Session[Declarations.User].ToString(), "masters_");
                ViewState[Declarations.Add] = Access.Add.Trim();
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
            }
        }
        AccessVisibility();

    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objCity = FactoryMaster.GetCityDetail();
            objCountry = FactoryMaster.GetCountryDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        InsertUpdate();
        Page.TraceWrite("Save Update button clicked event ends.");
    }

    protected void SaveAndContinue_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save and Add more button clicked event starts.");
        InsertUpdate(1);
        Page.TraceWrite("Save and Add more button clicked event ends.");
    }

    protected void Back_Click(object sender, EventArgs e)
    {
        Server.Transfer("CityMasterData.aspx");
    }

    void CountryBind()
    {
        Page.TraceWrite("Country dropdown bind starts.");
        try
        {
            ddlCountry.extDataBind(objCountry.SelectData());
        }
        catch (Exception ex)
        {
            Page.TraceWarn("Country dropdown bind fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Country dropdown bind ends.");
    }

    void ReadData()
    {
        Page.TraceWrite("ReadData starts.");
        try
        {
            objCity.CityId = int.Parse(hdnPrimeId.Value);
            List<City> City = objCity.ReadData();
            txtCityName.Value = City[0].CityName;
            txtDescription.Value = City[0].Description;
            ddlCountry.extSelectedValues(City[0].CountryId.ToString(),City[0].CountryName);
            hdnStateIdRead.Value = City[0].StateId.ToString();
            hdnStateNameReadDisable.Value = City[0].StateName.ToString();

            btnSaveAndContinue.Visible = false;
            if (City[0].isActive == "Y")
            {
                rdActive.Checked = true;
            }
            else
            {
                rdInactive.Checked = true;
            }

        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData ends.");
    }

    void InsertUpdate(int flg = 0)
    {
            Page.TraceWrite("Insert, Update starts.");
            string status = "";
            objCity.CityName = txtCityName.Value.Trim();
            objCity.Description = txtDescription.Value.Trim();
            objCity.StateId = int.Parse(hdnStateId.Value);
            objCity.CountryId = int.Parse(ddlCountry.Value);

            objCity.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            objCity.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            objCity.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
            objCity.isActive = rdActive.Checked == true ? "Y" : "N";

            try
            {
                if (hdnPrimeId.Value == "0")
                {
                    objCity.CityId = 0;
                    status = objCity.InsertRecord();
                }
                else
                {
                    objCity.CityId = int.Parse(hdnPrimeId.Value);
                    status = objCity.UpdateRecord();
                }
                Page.Message(status, hdnPrimeId.Value);

            }
            catch (Exception ex)
            {
                Page.Message("0", hdnPrimeId.Value);
                Page.TraceWarn("Insert, Update fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            Page.TraceWrite("Insert, Update ends.");
            if (flg == 0 && status == "1")
            {
                Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
                Response.Redirect("CityMasterData.aspx");
            }
            ClearData(status);
        //}
    }

    void ClearData(string status)
    {
        if (status == "1")
        {
            Page.TraceWrite("clearData starts.");
            hdnPrimeId.Value = "0";
            objCity.CityId = 0;
            txtCityName.Value = "";
            CountryBind();
            hdnStateId.Value = "";
            hdnStateIdRead.Value = "";
            Page.TraceWrite("clear Data ends.");
            txtDescription.Value = "";
            rdInactive.Checked = false;
            rdActive.Checked = true;
        }

    }

    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }

    void AccessVisibility()
    {
        setAccessValues();

        Page.TraceWrite("AccessVisibility starts.");
        if (Add == "N" && (hdnPrimeId.Value == "" || hdnPrimeId.Value == "0" || hdnPrimeId.Value == null))
        {
            Page.extDisableControls();
            btnBack.Enabled = true;
        }
        if (View == "N")
        {
            btnBack.Enabled = false;
        }
        Page.TraceWrite("AccessVisibility starts.");
    }


   

}










