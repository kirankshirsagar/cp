﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;



public partial class Masters_ContractTemplateMasterData : System.Web.UI.Page
{
    // navigation//
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 10;
    // navigation//

    IContractTemplate objContractTemplate;
    public string Add;
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {

        CreateObjects();
        deleteMessage();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            Access.PageAccess(this, Session[Declarations.User].ToString(), "masters_");
            ViewState[Declarations.Add] = Access.Add.Trim();
            ViewState[Declarations.View] = Access.View.Trim();
            ViewState[Declarations.Delete] = Access.Delete.Trim();
            ViewState[Declarations.Update] = Access.Update.Trim();
        }

        AccessVisibility();
        if (!IsPostBack || hdnSearch.Value.Trim() != string.Empty)
        {
            rdActive.Checked = true;
            ReadData(1, RecordsPerPage);
            Message();
        }
        else
        {
            SetNavigationButtonParameters();
        }

       


    }

    void Message()
    {
        if (Session[Declarations.Message] != null)
        {
            var flg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }

    }

    // navigation//

    #region Navigation
    void SetNavigationButtonParameters()
    {
        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }


    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
            AccessVisibility();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

    }
    #endregion

    // navigation//




    void ReadData(int pageNo, int recordsPerPage)
    {

        if (View == "Y")
        {
            Page.TraceWrite("ReadData starts.");
            try
            {
                objContractTemplate.PageNo = pageNo;
                objContractTemplate.RecordsPerPage = recordsPerPage;
                objContractTemplate.Search = txtSearch.Value.Trim();
                rptContractTemplateMaster.DataSource = objContractTemplate.ReadData();
                rptContractTemplateMaster.DataBind();
                Page.TraceWrite("ReadData ends.");
                ViewState[Declarations.TotalRecord] = objContractTemplate.TotalRecords;
                PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
                PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
                PaginationButtons1.RecordsPerPage = RecordsPerPage;
            }
            catch (Exception ex)
            {
                Page.TraceWarn("ReadData fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

    }


    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objContractTemplate = FactoryMaster.GetContractTemplateDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }



    protected void btnDelete_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Delete starts.");
        string status = "";
        objContractTemplate.ContractTemplateIds = hdnPrimeIds.Value;
        try
        {
            status = objContractTemplate.DeleteRecord();
        }
        catch (Exception ex)
        {
            hdnPrimeIds.Value = string.Empty;
            Session[Declarations.DeleteMSG] = "PageGetMessage('DF')";
            Page.TraceWarn("Delete fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        hdnPrimeIds.Value = string.Empty;
        Session[Declarations.DeleteMSG] = "PageGetMessage('DS')";
        Page.TraceWrite("Delete ends.");
        Server.Transfer("ContractTemplateMasterData.aspx"); 
    }


    void deleteMessage()
    {
        if (Session[Declarations.DeleteMSG] != null)
        {
            Page.JavaScriptClientScriptBlock("status", Session[Declarations.DeleteMSG].ToString());
            Session[Declarations.DeleteMSG] = null;
        }
    }


    protected void btnChangeStatus_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Change Status starts.");
        string status = "";
        objContractTemplate.ContractTemplateIds = hdnPrimeIds.Value;
        try
        {
            objContractTemplate.isActive = rdActive.Checked == true ? "Y" : "N";
            status = objContractTemplate.ChangeIsActive();
            ReadData(1, RecordsPerPage);
        }
        catch (Exception ex)
        {
            hdnPrimeIds.Value = string.Empty;
            Page.JavaScriptClientScriptBlock("status", "PageGetMessage('SF')");
            Page.TraceWarn("Change Status fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.JavaScriptClientScriptBlock("status", "PageGetMessage('SS')");
        Page.TraceWrite("Change Status ends.");
    }


    protected void imgEdit_Click(object sender, EventArgs e)
    {

        if (Update == "Y")
            Server.Transfer("ContractTemplateMaster.aspx");

    }

    protected void btnAddRecord_Click(object sender, EventArgs e)
    {
        Server.Transfer("ContractTemplateMaster.aspx");
    }


    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (View == "N")
        {
            Page.extDisableControls();
            btnChangeStatus.Enabled = false;
            
        }
        else
        {
         
            txtSearch.Disabled = false;
            btnSearch.Enabled = true;
            btnShowAll.Enabled = true;
           
        }

        if (Update == "N")
        {
            foreach (RepeaterItem item in rptContractTemplateMaster.Items)
            {
                LinkButton imgEdit = (LinkButton)item.FindControl("imgEdit");
                imgEdit.Enabled = false;
               
            }
            btnDelete.Enabled = false;
            btnChangeStatus.Enabled = false;
        }
        if (Add == "N")
        {
            Page.extDisableControls();
            btnChangeStatus.Enabled = false;
            btnAddRecord.Enabled = false;
        }
        else
        {
            btnAddRecord.Enabled = true;
        }

        if (Delete == "N")
        {
            btnDelete.Enabled = false;
            btnDelete.OnClientClick = null;
        }
        else if (Delete == "Y")
        {
            btnDelete.Enabled = true;
        }



      //  btnBack.Enabled = true;
        Page.TraceWrite("AccessVisibility starts.");
    }
    protected void lnkDownload_Click(object sender, EventArgs e)
    {
        string filename="";
        string subPath = @"~/Uploads/" + Session["TenantDIR"] + "/Contract Template";
        bool isExists = System.IO.Directory.Exists(Server.MapPath(subPath));
         if (!isExists)
            {
               Page.JavaScriptStartupScript("displayalertmessage", "alert('File not exist')");
            }
         filename = Server.MapPath(subPath) + "\\" + HF_UploadFileName.Value;
         //Response.TransmitFile(Server.MapPath(filename));
         //Response.End();


         Response.AddHeader("Content-Disposition", "attachment; filename=" + HF_TemplateFileActualName.Value);
         Response.WriteFile(filename);
         Response.End();

         HF_UploadFileName.Value = string.Empty;
          HF_TemplateFileActualName.Value = string.Empty;
    }

   
}










