﻿<%@ Page Language="C#" MasterPageFile="~/CM.master" ClientIDMode="Static" AutoEventWireup="true"
    CodeFile="CityMaster.aspx.cs" Inherits="Masters_CityMaster" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");
    </script>
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <input id="hdnStateId" clientidmode="Static" runat="server" type="hidden" />
    <input id="hdnStateIdRead" clientidmode="Static" runat="server" type="hidden" />
    <input id="hdnStateNameReadDisable" clientidmode="Static" runat="server" type="hidden" />

    <h2>
        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
    </h2>
    <div class="box tabular">
        <p>
            <label for="time_entry_issue_id">
                <span class="required">*</span>City name</label>
            <input id="txtCityName" runat="server" type="text" class="required ui-autocomplete-input  "
                style="width: 220px;" maxlength="50 " autocomplete="off" role="textbox" aria-autocomplete="list"
                aria-haspopup="true" />
            <em></em>
        </p>
     
        <p>
            <label for="time_entry_issue_id">
                <span class="required">*</span>Country</label>
            <select id="ddlCountry" clientidmode="Static" onchange="JQSelectBind('ddlCountry','ddlState', 'MstState');setStateValue();"
                style="width: 225px;" runat="server" class="chzn-select required chzn-select">
                <option></option>
            </select>
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id">
                <span class="required">*</span>State</label>
            <select id="ddlState" onchange="setStateValue();" clientidmode="Static" style="width: 225px;"
                class="chzn-select required chzn-select">
                <option></option>
            </select>
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id">
                Description</label>
          
            <textarea id="txtDescription" placeholder="City description" runat="server" maxlength="100"
                clientidmode="static" type="text" autocomplete="false" style="width: 220px; height: 100px"
                rows="10" />
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id">
                Status</label>
            <input id="rdActive" name="ActiveInactive" runat="server" type="radio" class="required" />Active
            <input id="rdInactive" name="ActiveInactive" runat="server" type="radio" class="required" />Inactive
            <em></em>
        </p>
    </div>
    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" class="btn_validate" />
    <asp:Button ID="btnSaveAndContinue" runat="server" Text="Save and Add more" OnClick="SaveAndContinue_Click"
         class="btn_validate" />
    <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back_Click" />
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
    <script type="text/javascript">

        function CheckLength(events) {
                   
            var textBox = document.getElementById("txtPincode");
            var x = textBox.value.length;

            if (x < 6) {

                alert('Zip code/Postcode  should be equal to six numbers.');

            }
            else {
               
                return true;
            }

        }


        $(function () {
            $('#txtPincode').keydown(function (e) {
                if (e.shiftKey || e.ctrlKey || e.altKey) {
                    e.preventDefault();
                } else {
                    var key = e.keyCode;
                    if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
                        e.preventDefault();
                    }
                }
            });
            $('#txtPincode').blur(function (e) {
                if ($(this).val().length < 6) {
                    $(this).after('<div class="tooltip_outer"  style="filter:alpha(opacity=50); opacity:0.9;"><div class="arrow-left"></div><div class="tool_tip"  style="filter:alpha(opacity=50); opacity:0.5;">Zip code/Postcode  should be equal to six numbers. </div></div>').show("slow");
                }
                else {
                    $(this).next('.tooltip_outer').hide();
                    $(this).css('border-color', '');
                }
            });
        });


      

        function setStateValue() {
            $('#hdnStateId').val('');
            $('#hdnStateId').val($('#ddlState').val());
        }

        function setDafaultValues() {
            $('#ddlState').val($('#hdnStateId').val());
        }

        $(document).ready(function () {
            $('#ddlCountry').change();
            if ($('#hdnStateIdRead').val() != '') {
                if ($('#hdnStateNameReadDisable').val() != '') {
                    addItemToSelectIfNotFound('ddlState', $('#hdnStateIdRead').val(), $('#hdnStateNameReadDisable').val());
                }

                $('#ddlState').val($('#hdnStateIdRead').val());
                $('#hdnStateId').val($('#hdnStateIdRead').val());
            }
        });


    </script>
</asp:Content>
