﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/CM.master"
    AutoEventWireup="true" CodeFile="ContractTemplateData.aspx.cs" Inherits="Masters_ContractTemplateMasterData" %>

<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<script runat="server">

   
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridSelect.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#tab2").addClass("selectedtab");
    </script>
     <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <h2>
        Contract Template</h2>
    <input id="hdnPrimeIds" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <asp:HiddenField ID="HF_UploadFileName" runat="server" ClientIDMode="Static"/>
    <asp:HiddenField ID="HF_TemplateFileActualName" runat="server" ClientIDMode="Static" />
    <div style="margin: 0; padding: 0; display: inline">
        <div id="query_form_content" class="hide-when-print">
            <fieldset id="filters" class="collapsible">
                <legend onclick="toggleFieldset(this);">Filters</legend>
                <div style="">
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td>
                                    <table id="filters-table" width="100%" cellpadding="3" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td width="10%">
                                                    Keywords :
                                                </td>
                                                <td width="90%">
                                                    <input id="txtSearch" clientidmode="Static" runat="server" type="text" maxlength="50" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </fieldset>
        </div>
        <p class="buttons hide-when-print">
            <asp:LinkButton ID="btnSearch" runat="server" OnClientClick="return searchClick();"
                CssClass="icon icon-checked" OnClick="btnSearch_Click">Filter</asp:LinkButton>
            <asp:LinkButton ID="btnShowAll" runat="server" OnClientClick="return resetClick();"
                CssClass="icon icon-reload" OnClick="btnSearch_Click">Reset Filter</asp:LinkButton>
            <asp:LinkButton ID="btnAddRecord" CssClass="icon icon-add" runat="server" OnClientClick="resetPrimeID();" OnClick="btnAddRecord_Click">Add new Contract Template</asp:LinkButton>
            <asp:LinkButton ID="btnDelete" CssClass="icon icon-del" runat="server" OnClientClick="return GetSelectedItems('D');"
                OnClick="btnDelete_Click" Text="Add new country">Delete</asp:LinkButton>
        </p>
        <div class="autoscroll">
            <table width="100%">
                <asp:Repeater ID="rptContractTemplateMaster" runat="server">
                    <HeaderTemplate>
                        <table id="masterDataTable" class="masterTable list issues" width="100%">
                            <thead>
                                <tr>
                                     <th style="text-align: left; padding-left: 2px;" width="1%">
                                        <input id="chkSelectAll" type="checkbox" class="checkbox maincheckbox" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </th>
                                    <th class="checkbox hide-when-print" width="3%">
                                        &nbsp;
                                    </th>
                                    <th width="0%" style="display: none;">
                                        Contract Template Id
                                    </th>
                                    <th width="10%">
                                        Contract Template Name
                                    </th>
                                    <th width="10%"  style="display: none;">
                                        TemplateFileName
                                    </th>
                                    <th width="20%">
                                        Template Actual File Name
                                    </th>
                                    <th width="10%">
                                        Contract Type Name
                                    </th>
                                    <th width="10%">
                                        Request Name
                                    </th>
                                    <th width="7%">
                                        Status
                                    </th>
                                    <th width="20%">
                                        Description
                                    </th>
                                    <th width="10%">
                                        Download File
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: left; padding-bottom: 2px">
                                <input type="checkbox" onchange="SetMainCheckbox(this);" class="mid-margin-left" />
                            </td>
                            <td style="text-align: left; padding-bottom: 2px">
                                <img id="imgLock" alt="img.." class="mws-tooltip-e" title="Used" border="0" />
                            </td>
                            <td style="display: none">
                                <asp:Label ID="lblContractTemplateId" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTemplateId") %>'></asp:Label>
                                <asp:Label ID="lblIsUsed" runat="server" ClientIDMode="Static" Text='<%#Eval("isUsed") %>'></asp:Label>
                                 <asp:Label ID="lblContractTemplateName" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTemplateName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:LinkButton ID="imgEdit" runat="server" OnClientClick="return setSelectedId(this);"
                                    OnClick="imgEdit_Click"><%#Eval("ContractTemplateName")%></asp:LinkButton>
                            </td>
                            <td style="display: none">
                                <asp:Label ID="lblTemplateFileName" ClientIDMode="Static" runat="server" Text='<%#Eval("TemplateFileName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblTemplateFileActualName" ClientIDMode="Static" runat="server" Text='<%#Eval("TemplateFileActualName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblContractTypeName" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTypeName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lclRequestName" runat="server" ClientIDMode="Static" Text='<%#Eval("RequestTypeName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblStatus" runat="server" ClientIDMode="Static" Text='<%#Eval("isActive") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbldecsription" runat="server" ClientIDMode="Static" Text='<%#Eval("Description") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkDownload" ClientIDMode="Static" runat="server" Text="Download"
                                    OnClientClick="FileDownload(this)" OnClick="lnkDownload_Click">Download</asp:LinkButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
            <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
        </div>
        <table style="display: none" width="100%">
            <tr>
                <td align="left" width="10%">
                </td>
                <td runat="server" id="actinacttd" style="padding-right: 60%" align="left" width="90%">
                    <input id="rdActive" name="ActiveInactive" runat="server" type="radio" />Active
                    <input id="rdInactive" name="ActiveInactive" runat="server" type="radio" />Inactive
                    <asp:Button ID="btnChangeStatus" runat="server" Text="Status" OnClientClick="return GetSelectedItems('S');"
                        OnClick="btnChangeStatus_Click" />
                </td>
            </tr>
        </table>
        <script type="text/javascript">




            //            $('#lnkDownload').click(function () {
            //              
            //                
            //                var TemplateFileName = $(this).closest('tr').find('#lblTemplateFileName').text();
            //                //alert(TemplateFileName);
            //                $("#HF_UploadFileName").val(TemplateFileName);
            //                return false;
            //            });

            function FileDownload(obj) {
                
                var TemplateFileName = $(obj).closest('tr').find('#lblTemplateFileName').text();
                //alert(TemplateFileName);
                $("#HF_UploadFileName").val(TemplateFileName);
                var TemplateFileActualName = $(obj).closest('tr').find('#lblTemplateFileActualName').text();
                $("#HF_TemplateFileActualName").val(TemplateFileActualName);
                return false;
            }

            function GetSelectedItems(flg) {
                $('#hdnPrimeIds').val('');
                $('#hdnUsedNames').val('');
                var tableClass = 'masterTable';
                var deleteLabelId = 'lblContractTemplateId';
                var deleteLabelName = 'lblContractTemplateName';
                var objCheck = new CkeckBoxSelect(tableClass, deleteLabelId, deleteLabelName);
                var deletedIds = objCheck.DeletedIds;
                var usedNames = objCheck.UsedNames;

                if (deletedIds == '') {
                    MessageMasterDiv('Please select record(s).');
                    return false;
                }
                else if (usedNames != '' && flg == 'D') {
                    MessageMasterDiv('Some records can not be deleted.' + usedNames, 1);
                    return false;
                }
                if (flg == 'D') {
                    if (DeleteConfrim() == true) {
                        $('#hdnPrimeIds').val(deletedIds);
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    $('#hdnPrimeIds').val(deletedIds);
                    return true;
                }
                $('#hdnPrimeIds').val(deletedIds);
                return true;
            }


            $(document).ready(function () {
                defaultTableValueSetting('lblContractTemplateId', 'MstContractTemplate', 'ContractTemplateId');
                LockUnLockImage('masterTable');
            });


            function setSelectedId(obj) {
                var pId = $(obj).closest('tr').find('#lblContractTemplateId').text();
                if (pId == '' || pId == '0') {
                    return false;
                }
                else {
                    $('#hdnPrimeIds').val(pId);
                    return true;
                }
            }


        </script>
        <div id="rightlinks" style="display: none;">
            <uc1:Masterlinks ID="rightactions" runat="server" />
        </div>
        <script type="text/javascript">
            $("#sidebar").html($("#rightlinks").html());
        </script>
</asp:Content>
