﻿<%@ Page Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true" CodeFile="ContractTypeMaster.aspx.cs"
    Inherits="Masters_ContractTypeMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%--<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>--%>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Configuratorlinks"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
    <script src="../CommonScripts/GridSelect.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");
    </script>
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <asp:HiddenField ID="hdndelete" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="HF_UploadFileName" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HF_TemplateFileActualName" runat="server" ClientIDMode="Static" />
    <input id="hdnNewTemplateOREditTemplate" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnContractTypeId" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
    </h2>

    <asp:ScriptManager ID="scriptmanager1" runat="server">
    </asp:ScriptManager>
    <div class="box tabular">
        <p>
            <label for="time_entry_issue_id">
                <span class="required">*</span>Contract Type Name</label>
            <%-- <input id="txtContracttypeName" runat="server" type="text" class="required ui-autocomplete-input "
                style="width: 220px" maxlength="49" autocomplete="off" role="textbox" aria-autocomplete="list"
                aria-haspopup="true" />--%>
            <asp:TextBox ID="txtContracttypeName" runat="server" type="text" class="required ui-autocomplete-input "
                Style="width: 220px" MaxLength="49" autocomplete="off" role="textbox" aria-autocomplete="list"
                aria-haspopup="true"></asp:TextBox>
            <cc1:FilteredTextBoxExtender ID="fteContractTemplateName" runat="server" InvalidChars="/\:*?<>&quot;|$^%_=,.~`{}"
                FilterMode="InvalidChars" TargetControlID="txtContracttypeName" />
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id">
                Description</label>
            <textarea id="txtDescription" placeholder="Contract Type description" runat="server"
                maxlength="100" clientidmode="static" type="text" autocomplete="false" style="width: 220px;
                height: 100px" rows="10" />
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id">
                Status</label>
            <input id="rdActive" name="ActiveInactive" runat="server" type="radio" class="required" />Active
            <input id="rdInactive" name="ActiveInactive" runat="server" type="radio" class="required" />Inactive
            <em></em>
        </p>
    </div>
    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" class="btn_validate" />
    <asp:Button ID="btnSaveAndContinue" runat="server" Text="Save and Add more" OnClick="SaveAndContinue_Click"
        class="btn_validate" Visible="false" />
    <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back_Click" />
    <asp:Panel ID="PanelContractTemplate" runat="server" Style="margin-left: 0px;">
        <br />
        <h3 style="margin-left: 30px;">
            Contract Template</h3>
        <div style="margin-left: 0px;">
            <div id="query_form_content" class="hide-when-print" style="margin-left: 30px;">
                <fieldset id="filters" class="collapsible" style="margin-left: 0px;">
                    <legend onclick="toggleFieldset(this);">Filters</legend>
                    <div style="margin-left: 0px;">
                        <table style="width: 100%; margin-left: 0px;">
                            <tbody>
                                <tr>
                                    <td>
                                        <table id="filters-table" width="100%" cellpadding="3" cellspacing="0" style="margin-left: 0px;">
                                            <tbody>
                                                <tr>
                                                    <td width="10%">
                                                        Keywords :
                                                    </td>
                                                    <td width="90%">
                                                        <input id="txtSearch" clientidmode="Static" runat="server" type="text" maxlength="50"
                                                            onkeydown="return (event.keyCode!=13);" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </fieldset>
            </div>
            <p class="buttons hide-when-print" style="margin-left: 30px;">
                <asp:LinkButton ID="btnSearch" runat="server" OnClientClick="return searchClick();"
                    CssClass="icon icon-checked" OnClick="btnSearch_Click">Filter</asp:LinkButton>
                <asp:LinkButton ID="btnShowAll" runat="server" OnClientClick="return resetClick();"
                    CssClass="icon icon-reload" OnClick="btnSearch_Click">Reset Filter</asp:LinkButton>
                <asp:LinkButton ID="btnAddRecord" CssClass="icon icon-add" runat="server" OnClick="btnAddTemplate_Click"
                    OnClientClick="return SetValue(this);">Add Template</asp:LinkButton>
                <asp:LinkButton ID="btnDelete" CssClass="icon icon-del" runat="server" OnClientClick="return GetSelectedItems('D');"
                    OnClick="btnDelete_Click" Text="Add new country">Delete</asp:LinkButton>
            </p>
            <div class="autoscroll" style="margin-left: 0px;">
                <table width="100%" class="list issues" style="margin-left: 0px;">
                    <asp:Repeater ID="rptContractTemplateMaster" runat="server">
                        <HeaderTemplate>
                            <table id="masterDataTable" class="masterTable list issues" width="100%" style="margin-left: 10px;">
                                <thead>
                                    <tr>
                                        <th style="text-align: left; padding-left: 2px;" width="1%">
                                            <input id="chkSelectAll" type="checkbox" class="checkbox maincheckbox" onkeydown="return (event.keyCode!=13);" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </th>
                                        <th class="checkbox hide-when-print" width="3%">
                                            &nbsp;
                                        </th>
                                        <th width="0%" style="display: none;">
                                            Contract Template Id
                                        </th>
                                        <th width="10%">
                                            <asp:LinkButton ID="btnSortContractTemplateName" OnClick="btnSort_Click" CssClass="sort desc"
                                                runat="server">Contract Template Name</asp:LinkButton>
                                        </th>
                                        <th width="10%" style="display: none;">
                                            TemplateFileName
                                        </th>
                                        <th width="20%">
                                            <asp:LinkButton ID="btnSortTemplateActualFileName" OnClick="btnSort_Click" CssClass="sort desc"
                                                runat="server">Template Actual File Name</asp:LinkButton>
                                        </th>
                                        <th width="10%">
                                            <asp:LinkButton ID="btnSortContractTypeName" OnClick="btnSort_Click" CssClass="sort desc"
                                                runat="server">Contract Type Name</asp:LinkButton>
                                        </th>
                                        <th width="10%">
                                            <asp:LinkButton ID="btnSortRequestTypeName" OnClick="btnSort_Click" CssClass="sort desc"
                                                runat="server">Request Type Name</asp:LinkButton>
                                        </th>
                                        <th width="6%">
                                            <asp:LinkButton ID="btnSortStatus" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Status</asp:LinkButton>
                                        </th>
                                        <%-- <th width="20%" style="word-break:break-all; word-wrap:break-word" >--%>
                                        <th width="15%">
                                            <asp:LinkButton ID="btnSortDescription" OnClick="btnSort_Click" CssClass="sort desc"
                                                runat="server">Description</asp:LinkButton>
                                        </th>
                                        <th width="10%">
                                            <asp:Label ID="lblDownloadFile" runat="server" Text="">Download File</asp:Label>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="text-align: left; padding-bottom: 2px">
                                    <input type="checkbox" onchange="SetMainCheckbox(this);" class="mid-margin-left"
                                        onkeydown="return (event.keyCode!=13);" />
                                </td>
                                <td style="text-align: left; padding-bottom: 2px">
                                    <img id="imgLock" alt="img.." class="mws-tooltip-e" title="Used" border="0" />
                                </td>
                                <td style="display: none">
                                    <asp:Label ID="lblContractTemplateId" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTemplateId") %>'></asp:Label>
                                    <asp:Label ID="lblIsUsed" runat="server" ClientIDMode="Static" Text='<%#Eval("isUsed") %>'></asp:Label>
                                    <asp:Label ID="lblContractTypeId" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTypeId") %>'></asp:Label>
                                    <asp:Label ID="lblContractTemplateName" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTemplateName") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:LinkButton ID="imgEdit" runat="server" OnClientClick="return setSelectedId(this);"
                                        OnClick="imgEdit_Click"><%#Eval("ContractTemplateName")%></asp:LinkButton>
                                </td>
                                <td style="display: none">
                                    <asp:Label ID="lblTemplateFileName" ClientIDMode="Static" runat="server" Text='<%#Eval("TemplateFileName") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblTemplateFileActualName" ClientIDMode="Static" runat="server" Text='<%#Eval("TemplateFileActualName") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblContractTypeName" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTypeName") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblRequestTypeName" runat="server" ClientIDMode="Static" Text='<%#Eval("RequestTypeName") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblStatus" runat="server" ClientIDMode="Static" Text='<%#Eval("isActive") %>'></asp:Label>
                                </td>
                                <td style="word-break: break-all; word-wrap: break-word">
                                    <asp:Label ID="lbldecsription" runat="server" ClientIDMode="Static" Text='<%#Eval("Description").ToString().Replace("\n", "<br>") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkDownload" ClientIDMode="Static" runat="server" Text="Download"
                                        OnClientClick="FileDownload(this)" OnClick="lnkDownload_Click">Download</asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </table>
                <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
            </div>
        </div>
    </asp:Panel>
    <script type="text/javascript">



        function FileDownload(obj) {

            var TemplateFileName = $(obj).closest('tr').find('#lblTemplateFileName').text();
            $("#HF_UploadFileName").val(TemplateFileName);
            var TemplateFileActualName = $(obj).closest('tr').find('#lblTemplateFileActualName').text();
            $("#HF_TemplateFileActualName").val(TemplateFileActualName);
            return false;
        }

        function SetValue(obj) {
            $('#hdnNewTemplateOREditTemplate').val('0');
            return true;
        }

        function GetSelectedItems(flg) {
            $('#hdnPrimeId').val('');
            $('#hdnUsedNames').val('');
            var tableClass = 'masterTable';
            var deleteLabelId = 'lblContractTemplateId';
            var deleteLabelName = 'lblContractTemplateName';
            var objCheck = new CkeckBoxSelect(tableClass, deleteLabelId, deleteLabelName);
            var deletedIds = objCheck.DeletedIds;
            var usedNames = objCheck.UsedNames;

            if (deletedIds == '') {
                MessageMasterDiv('Please select record(s).');
                return false;
            }
            else if (usedNames != '' && flg == 'D') {
                MessageMasterDiv('Some records can not be deleted.' + usedNames, 1);
                return false;
            }
            if (flg == 'D') {
                if (DeleteConfrim() == true) {
                    $('#hdnPrimeId').val(deletedIds);
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                $('#hdnPrimeId').val(deletedIds);
                return true;
            }
            $('#hdnPrimeId').val(deletedIds);
            return true;
        }


        $(document).ready(function () {
            defaultTableValueSetting('lblContractTemplateId', 'MstContractTemplate', 'ContractTemplateId');
            LockUnLockImage('masterTable');
        });


        function setSelectedId(obj) {
            var pId = $(obj).closest('tr').find('#lblContractTemplateId').text();
            var CId = $(obj).closest('tr').find('#lblContractTypeId').text();

            if (pId == '' || pId == '0') {
                return false;
            }
            else {
                $('#hdnPrimeId').val(pId);
                $('#hdnContractTypeId').val(CId);
                $('#hdnNewTemplateOREditTemplate').val(pId);
                return true;
            }
        }

        $(document).ready(function () {
            defaultTableValueSetting('lblContractTemplateId', 'MstContractTemplate', 'ContractTemplateId');
            LockUnLockImage('masterTable');
        });
    </script>
    <div id="rightlinks" style="display: none;">
        <uc1:Configuratorlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
</asp:Content>
