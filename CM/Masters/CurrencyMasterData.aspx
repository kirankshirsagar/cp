﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/CM.master"
    AutoEventWireup="true" CodeFile="CurrencyMasterData.aspx.cs" Inherits="Masters_CurrencyMasterData" %>

<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridSelect.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");
    </script>
     <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <h2>
        Currency</h2>
    <input id="hdnPrimeIds" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <div style="margin: 0; padding: 0; display: inline">
        <div id="query_form_content" class="hide-when-print">
            <fieldset id="filters" class="collapsible">
                <legend onclick="toggleFieldset(this);">Filters</legend>
                <div style="">
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td>
                                    <table id="filters-table" width="100%" cellpadding="3" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td width="10%">
                                                    Keywords :
                                                </td>
                                                <td width="90%">
                                                    <input id="txtSearch" clientidmode="Static" runat="server" type="text" maxlength="50" onkeydown = "return (event.keyCode!=13);" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </fieldset>
        </div>
        <p class="buttons hide-when-print">
            <asp:LinkButton ID="btnSearch" runat="server" OnClientClick="return searchClick();"
                CssClass="icon icon-checked" OnClick="btnSearch_Click">Filter</asp:LinkButton>
            <asp:LinkButton ID="btnShowAll" runat="server" OnClientClick="return resetClick();"
                CssClass="icon icon-reload" OnClick="btnSearch_Click">Reset Filter</asp:LinkButton>
            <asp:LinkButton ID="btnAddRecord" CssClass="icon icon-add" runat="server" OnClientClick="resetPrimeID();" OnClick="btnAddRecord_Click" style='display:none'>Add new Currency</asp:LinkButton>
            <asp:LinkButton ID="btnDelete" CssClass="icon icon-del" runat="server"  style='display:none' OnClientClick="return GetSelectedItems('D');"
                OnClick="btnDelete_Click" Text="Add new country">Delete</asp:LinkButton>
        </p>
        <div class="autoscroll">
            <table width="100%">
                <asp:Repeater ID="rptCurrencyMaster" runat="server">
                    <HeaderTemplate>
                        <table id="masterDataTable"  class="masterTable list issues"  width="100%">
                            <thead>
                                <tr>
                                     <th style="text-align: left; padding-left: 2px;display:none;" width="1%">
                                        <input id="chkSelectAll" type="checkbox" class="checkbox maincheckbox" onkeydown = "return (event.keyCode!=13);" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </th>
                                    <th class="checkbox hide-when-print" width="3%" >
                                        &nbsp;
                                    </th>
                                    <th width="0%" style="display: none;">
                                        Currency ID
                                    </th>
                                    <th width="30%" >
                                     <asp:LinkButton ID="btnSortCurrencyName" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Currency Name</asp:LinkButton>
                                       
                                    </th>
                                    <th width="10%" >
                                     <asp:LinkButton ID="LinkButton1" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Currency Code</asp:LinkButton>
                                       
                                    </th>
                                    <th width="15%" >
                                         <asp:LinkButton ID="btnSortCurrencyCode" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Conversion Rate</asp:LinkButton>
                                    </th>
                                    <th width="10%" >
                                        <asp:LinkButton ID="btnSortStatus" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Status</asp:LinkButton>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                             <td style="text-align: left; padding-bottom: 2px;display: none;">
                                <input type="checkbox" onchange="SetMainCheckbox(this);" class="mid-margin-left" onkeydown = "return (event.keyCode!=13);" />
                            </td>
                            <td style="text-align: left; padding-bottom: 2px">
                                <img id="imgLock" alt="img.." class="mws-tooltip-e" Title="Used" border="0" />
                            </td>
                            <td style="display: none">
                                <asp:Label ID="lblCurrencyId" runat="server" ClientIDMode="Static" Text='<%#Eval("CurrencyId") %>'></asp:Label>
                                <asp:Label ID="lblIsUsed" runat="server" ClientIDMode="Static" Text='<%#Eval("IsUsed") %>'></asp:Label>
                                <asp:Label ID="lblCurrencyName" runat="server" ClientIDMode="Static" Text='<%#Eval("CurrencyName") %>'></asp:Label>
                            </td>
                             <td>
                                <asp:LinkButton ID="imgEdit" runat="server" OnClientClick="return setSelectedId(this);"
                                    OnClick="imgEdit_Click"><%#Eval("CurrencyName")%></asp:LinkButton>
                            </td>
                            <td style="word-break:break-all; word-wrap:break-word" >
                                <asp:Label ID="lblCurrencyCode" runat="server" ClientIDMode="Static" Text='<%#Eval("CurrencyCode") %>'></asp:Label>
                            </td>
                             <td style="word-break:break-all; word-wrap:break-word" >
                                <asp:Label ID="lblConversionRate" runat="server" ClientIDMode="Static" Text='<%#Eval("ConversionRate") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblStatus" runat="server" ClientIDMode="Static" Text='<%#Eval("IsActive") %>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
            <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
        </div>
        <table style="display: none" width="100%">
            <tr>
                <td align="left" width="10%">
                </td>
                <td runat="server" id="actinacttd" style="padding-right: 60%" align="left" width="90%">
                    <input id="rdActive" name="ActiveInactive" runat="server" type="radio" />Active
                    <input id="rdInactive" name="ActiveInactive" runat="server" type="radio" />Inactive
                    <asp:Button ID="btnChangeStatus" runat="server" Text="Status" OnClientClick="return GetSelectedItems('S');"
                        OnClick="btnChangeStatus_Click" />
                </td>
            </tr>
        </table>
        <script type="text/javascript">
            function msgSaveSuccess() {
                alert('success...');
            }


            function GetSelectedItems(flg) {
                $('#hdnPrimeIds').val('');
                $('#hdnUsedNames').val('');
                var tableClass = 'masterTable';
                var deleteLabelId = 'lblCurrencyId';
                var deleteLabelName = 'lblCurrencyName';
                var objCheck = new CkeckBoxSelect(tableClass, deleteLabelId, deleteLabelName);
                var deletedIds = objCheck.DeletedIds;
                var usedNames = objCheck.UsedNames;

                if (deletedIds == '') {
                    MessageMasterDiv('Please select record(s).');
                    return false;
                }
                else if (usedNames != '' && flg == 'D') {
                    MessageMasterDiv('Some records can not be deleted.' + usedNames, 1);
                    return false;
                }
                if (flg == 'D') {
                    if (DeleteConfrim() == true) {
                        $('#hdnPrimeIds').val(deletedIds);
                        return true;
                    }
                    else {

                        return false;
                    }
                }
                else {
                    $('#hdnPrimeIds').val(deletedIds);
                    return true;
                }



                $('#hdnPrimeIds').val(deletedIds);
                //            $('#hdnUsedNames').val(usedNames);
                return true;
            }


            $(document).ready(function () {
                defaultTableValueSetting('lblCurrencyId', 'MstCurrency', 'CurrencyId');
                LockUnLockImage('masterTable');
            });


            function setSelectedId(obj) {
                var pId = $(obj).closest('tr').find('#lblCurrencyId').text();
                if (pId == '' || pId == '0') {
                    return false;
                }
                else {
                    $('#hdnPrimeIds').val(pId);
                    return true;
                }
            }
        </script>
        <div id="rightlinks" style="display: none;">
            <uc1:Masterlinks ID="rightactions" runat="server" />
        </div>
        <script type="text/javascript">
            $("#sidebar").html($("#rightlinks").html());
        </script>
</asp:Content>
