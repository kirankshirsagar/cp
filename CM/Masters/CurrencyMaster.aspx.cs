﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;


public partial class Masters_CurrencyMaster : System.Web.UI.Page
{
    ICurrency objCurrency;
    public string Add;
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();


        if (!IsPostBack)
        {
            if (String.IsNullOrEmpty(Request.extValue("hdnPrimeIds")) != true)
            {
                hdnPrimeId.Value = Request.extValue("hdnPrimeIds");
                ReadData();
                lblCurrency.Text = "Edit Currency";
                btnSave.Text = "Update";
            }
            else
            {
                rdActive.Checked = true;
                hdnPrimeId.Value = "0";
                lblCurrency.Text = "Add Currency";

            }

            if (Session[Declarations.User] != null)
            {
                Access.PageAccess(this, Session[Declarations.User].ToString(), "masters_");
                ViewState[Declarations.Add] = Access.Add.Trim();
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
            }
            AccessVisibility();
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objCurrency = FactoryMaster.GetCurrencyDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        InsertUpdate();
        Page.TraceWrite("Save Update button clicked event ends.");
    }

    protected void SaveAndContinue_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save and Add more button clicked event starts.");
        InsertUpdate(1);
        Page.TraceWrite("Save and Add more button clicked event ends.");
    }

    protected void Back_Click(object sender, EventArgs e)
    {
        Server.Transfer("CurrencyMasterData.aspx");
    }

    void ReadData()
    {
        Page.TraceWrite("ReadData starts.");
        try
        {
            objCurrency.CurrencyId = int.Parse(hdnPrimeId.Value);
            List<Currency> Currency = objCurrency.ReadData();
            txtCurrencyName.Value = Currency[0].CurrencyName;
            txtCurrencyCode.Value = Currency[0].CurrencyCode;
            txtConversionRate.Value = Currency[0].ConversionRate.ToString();
            chkIsBaseCurrency.Checked = Currency[0].IsBaseCurrency == "Y" ? true : false;

            btnSaveAndContinue.Visible = false;
            if (Currency[0].IsActive == "Y")
            {
                rdActive.Checked = true;
            }
            else
            {
                rdInactive.Checked = true;
            }

        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData ends.");
    }

    void InsertUpdate(int flg = 0)
    {
        Page.TraceWrite("Insert, Update starts.");
        string status = "";
        objCurrency.ConversionRate = Convert.ToDecimal(txtConversionRate.Value.Trim());
        objCurrency.IsBaseCurrency = chkIsBaseCurrency.Checked ? "Y" : "N";
        objCurrency.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objCurrency.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objCurrency.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
        objCurrency.IsActive = rdActive.Checked == true ? "Y" : "N";
        string json = GetCurrencyRates(objCurrency.IsBaseCurrency == "N" ? "EUR" : txtCurrencyCode.Value.Trim());
        DataTable CurrencyRateTable = ToDataTable(json);
        objCurrency.CurrencyRateTable = CurrencyRateTable;

        try
        {
            if (hdnPrimeId.Value == "0")
            {
                objCurrency.CurrencyId = 0;
                status = objCurrency.InsertRecord();
            }
            else
            {
                objCurrency.CurrencyId = int.Parse(hdnPrimeId.Value);
                status = objCurrency.UpdateRecord();
            }
            Page.Message(status, hdnPrimeId.Value);
        }
        catch (Exception ex)
        {
            Page.Message("0", hdnPrimeId.Value);
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Insert, Update ends.");
        if (flg == 0 && status == "1")
        {
            Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
            Response.Redirect("CurrencyMasterData.aspx");
        }
        ClearData(status);
    }

    void ClearData(string status)
    {
        if (status == "1")
        {
            Page.TraceWrite("clearData starts.");
            objCurrency.CurrencyId = 0;
            hdnPrimeId.Value = "0";
            txtCurrencyName.Value = "";
            Page.TraceWrite("clear Data ends.");
            rdInactive.Checked = false;
            rdActive.Checked = true;
        }
    }

    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (Add == "N" && (hdnPrimeId.Value == "" || hdnPrimeId.Value == "0" || hdnPrimeId.Value == null))
        {
            Page.extDisableControls();
            btnBack.Enabled = true;
        }
        if (View == "N")
        {
            btnBack.Enabled = false;
        }
        Page.TraceWrite("AccessVisibility starts.");
    }

    private readonly static string ConversionRateUrl = "http://api.fixer.io/latest?base={0}";
    public static string GetCurrencyRates(string BaseCurrency)
    {
        string formattedUri = String.Format(CultureInfo.InvariantCulture, ConversionRateUrl, BaseCurrency);
        HttpWebRequest webRequest = GetWebRequest(formattedUri);
        HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();
        string jsonResponse = string.Empty;
        using (StreamReader sr = new StreamReader(response.GetResponseStream()))
        {
            jsonResponse = sr.ReadToEnd();
        }
        return jsonResponse;
    }

    private static HttpWebRequest GetWebRequest(string formattedUri)
    {
        // Create the request’s URI.
        Uri serviceUri = new Uri(formattedUri, UriKind.Absolute);

        // Return the HttpWebRequest.
        return (HttpWebRequest)System.Net.WebRequest.Create(serviceUri);
    }

    private DataTable ToDataTable(string jsonString)
    {
        DataTable dt = new DataTable();
        string[] jsonStringArray = Regex.Split(jsonString.Replace("[", "").Replace("]", ""), "},{");
        List<string> ColumnsName = new List<string>();
        dt.Columns.Add("CurrencyCode");
        dt.Columns.Add("ConversionRate");

        foreach (string jSA in jsonStringArray)
        {
            string[] RowData = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",");

            foreach (string rw in RowData)
            {
                DataRow nr = dt.NewRow();
                try
                {
                    string rowData = rw;
                    if (rowData.Contains("rates"))
                    {
                        rowData = rowData.Replace("\"rates\":", "");
                    }
                    int idx = rowData.IndexOf(":");
                    string CurrencyCode = rowData.Substring(0, idx - 1).Replace("\"", "");
                    string ConversionRate = rowData.Substring(idx + 1).Replace("\"", "");
                    if (CurrencyCode.Equals("base"))
                    {
                        CurrencyCode = ConversionRate;
                        ConversionRate = "1.0000";
                    }
                    nr["CurrencyCode"] = CurrencyCode;
                    nr["ConversionRate"] = ConversionRate;
                    dt.Rows.Add(nr);
                }
                catch (Exception ex)
                {
                    continue;
                }
            }

        }
        dt.Rows.RemoveAt(1);
        return dt;
    }
}










