﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;


public partial class StageConfigurator : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (String.IsNullOrEmpty(Request.extValue("hdnStageId")) != true)
            {
                string[] str = Convert.ToString(Request.extValue("hdnLevelId")).Split('-');
                HdnLevelId.Value = str[1].ToString().Trim();
                lbllevel.Text = HdnLevelId.Value;
                HdnCotractType.Value = Request.extValue("hdnContractTypeName");
                //lblConType.Text = HdnCotractType.Value;
                HdnCotractTypeId.Value = Request.extValue("hdnContractTypeId");
                HdnStageId.Value = Request.extValue("hdnStageId");
                hdnTemplateId.Value = Request.extValue("hdnTemplateId");
                //lblTemplate.Text = Request.extValue("hdnTemplateName");
                HdnCotractTemplateType.Value = Request.extValue("hdnTemplateName");

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "tytytt", "SetDefaultOptions();", true);
                //lblTitle.Text = "Edit Stage";
            }
            else
            {
                string[] str = Convert.ToString(Request.extValue("hdnLevelId")).Split('-');
                HdnLevelId.Value = str[1].ToString().Trim();
                lbllevel.Text = HdnLevelId.Value;
                HdnCotractType.Value = Request.extValue("hdnContractTypeName");
                //lblConType.Text = HdnCotractType.Value;
                HdnCotractTypeId.Value = Request.extValue("hdnContractTypeId");
                hdnTemplateId.Value = Request.extValue("hdnTemplateId");
                //lblTemplate.Text = Request.extValue("hdnTemplateName");
                HdnCotractTemplateType.Value = Request.extValue("hdnTemplateName");
                HdnStageId.Value = "0";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tytytt", "SetDefaultOptions();", true);
                //lblTitle.Text = "Add Stage";
            }
            if (Session[Declarations.User] != null)
            {
                Access.PageAccess(this, Session[Declarations.User].ToString(), "configurators_");
                ViewState[Declarations.Add] = Access.Add.Trim();
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
                hdnAddedBy.Value = Session[Declarations.User].ToString();
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Server.Transfer("StageConfiguratorContractTypeData.aspx");
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Server.Transfer("StageConfiguratorContractTypeData.aspx");
    }

    protected void lnkStageBack_Click(object sender, EventArgs e)
    {
        Server.Transfer("StageConfiguratorContractTypeData.aspx");
    }

}










