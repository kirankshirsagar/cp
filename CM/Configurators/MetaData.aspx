﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="MetaData.aspx.cs" Inherits="Configurators_MetaData" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <link href="../JQueryValidations/dhtmlwindow.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/dhtmlwindow.js" type="text/javascript"></script>
    <link href="../JQueryValidations/modal.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/modal.js" type="text/javascript"></script>
    <%-- <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />--%>
    <script type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");
    </script>
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <input id="hdnData" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnFieldNameValue" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnFieldNameText" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnReadFields" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnContractTemplateId" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnedit" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnAdd" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnview" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdndelete" runat="server" clientidmode="Static" type="hidden" />
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text="MetaData Configurators"></asp:Label>
    </h2>
    <div class="box tabular">
    </div>
    <table width="100%">
        <tr align="right">
            <td>
                <a href="#" id="ancAddNew" onclick="opennewsletter(); return false">Add New</a>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblTitalField" for="time_entry_issue_id" runat="server" Text=""></asp:Label>
            </td>
        </tr>
    </table>
    <table id="tblData" class="list issues" width="80%">
        <tr>
            <th id="thFieldType" width="40%">
                <asp:Label ID="lblFieldType" ClientIDMode="Static" runat="server" Text="Field Type"></asp:Label>
            </th>
            <th id="thFieldName" width="40%">
                <asp:Label ID="lblFieldName" ClientIDMode="Static" runat="server" Text="Field Name"></asp:Label>
            </th>
            <th id="thbutton" width="2%">
            </th>
            <th id="thDelete" width="5%">
            </th>
            <th id="thUpDown" width="5%">
            </th>
            <th id="thFieldID" width="1%" style="display: none">
                <asp:Label ID="lblFieldId" ClientIDMode="Static" runat="server" Text="FieldID"></asp:Label>
            </th>
        </tr>
    </table>
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $('#txtFiledName').on('keypress', function (e) {
            var ingnore_key_codes = [34, 39];
            if ($.inArray(e.which, ingnore_key_codes) >= 0) {
                return false;
            }
        });

        $("#sidebar").html($("#rightlinks").html());

        $(document).ready(function () {
            $('#spanMasterField').hide();
        })

        function hideTable(flg) {
            if (flg == 1 && $('#tblData tbody').find('tr').length <= 1) {
                if ($('#hdnReadFields').val() != '') {
                    $('#tblData').show();
                    $('#btnSave').show();
                }
                else {
                    $('#tblData').hide();
                    $('#btnSave').hide();
                }
            }
            else {
                $('#tblData').show();
                $('#btnSave').show();
            }

        }

        $(document).ready(function () {
            $(".up,.down").live('click', function () {
                debugger;
                var $row = $(this).parents('tr:first');

                if ($(this).is(".up")) {
                    if ($row.index() > 1) {
                        $row.insertBefore($row.prev());
                        //$row.insertBefore($('#tblData tbody tr:first').next());
                    }
                } else {
                    $row.insertAfter($row.next());
                }
            });


            $('.clsFieldNameText').live('click', function () {
                var fieldType = $(this).closest('tr').find('span[class="clsFieldTypeValue"]').html();
                var fieldValue = $(this).closest('tr').find('span[class="clsFieldNameValue"]').html();
                var fieldText = $(this).closest('tr').find('a[class="clsFieldNameText"]').html();

                $('#ddlFieldType').val(fieldType);
                updateSettings('#ddlFieldType');
                $('#ddlFieldType').change();
                if (fieldType == 9) {
                    $('#ddlMasterField').val(fieldValue);
                    updateSettings('#ddlMasterField');
                    $('#txtFiledName').val(fieldText.split('_')[0]);
                }
                else {
                    $('#txtFiledName').val(fieldValue);
                }

                $('#hdnFieldNameText').val(fieldText);

                return false;
            });
        });


        function getData() {
            var data = '';
            var cnt = 0;
            var vFieldType = '';
            var vFieldName = '';

            $('#tblData tbody').find('tr').each(function () {
                if (cnt > 0) {
                    vFieldType = $(this).find('span[class="clsFieldTypeValue"]').html();
                    vFieldName = $(this).find('span[class="clsFieldNameValue"]').html() + '$' + $(this).find('a[class="clsFieldNameText"]').html();

                    if (cnt == 1) {
                        data = vFieldType + ',' + vFieldName;
                    }
                    else {
                        data = data + '#' + vFieldType + ',' + vFieldName;
                    }
                }
                cnt = cnt + 1;
            });

            $('#hdnData').val(data);

            if (data == '') {
                alert('Please add fields.');
                return false;
            }
            else {

                return true;
            }
        }

        hideTable(1);

        function readRow(fieldTypeText, fieldTypeValue, fieldNameText, fieldID, fieldNameValue) {

            var rowData;
            var butt = 'Buttonx';
            rowData = "<tr>"
            + "<td width='35%'><span class='clsFiledTypeText'>" + fieldTypeText + "</span><span style='display:none' class='clsFieldTypeValue'>" + fieldTypeValue + "</span> </td>"
            + "<td width='35%'><a href='#' class='clsFieldNameText'>" + fieldNameText + "</a> <span style='display:none' class='clsFieldNameValue'>" + fieldNameValue + "</span>  </td>";

            //            + "<td width='10%'><img src='../Images/edit.png' title='Edit' onclick='opennewsletter(" + fieldID + "," + fieldTypeValue + "," + fieldNameValue + ");'> <span style='color:#289aa1; font-size:14px;font-weight:bold;'>Edit</span></img><span style='display:none' class='clsFieldTypeValue'>" + fieldID + "</span> </td>"
            if ($('#hdnedit').val() == 'Y') {
                rowData += "<td width='10%'><a href='#' onclick='opennewsletter(" + fieldID + "," + fieldTypeValue + "," + fieldNameValue + ");' style='text-decoration: none !important;'><img src='../Images/edit.png' title='Edit'> <span style='color:#289aa1; font-size:14px;font-weight:bold;'>Edit</span></img></a><span style='display:none' class='clsFieldTypeValue'>" + fieldID + "</span> </td>";
            }
            if ($('#hdndelete').val() == 'Y') {
                rowData += "<td width='10%'><a href='#' class='btnDelete' onclick='openDeleteletter(" + fieldID + "," + fieldTypeValue + "," + fieldNameValue + ");' clientidmode='static' style='text-decoration: none !important;' >Delete</a></td>";
            }
            rowData += "<td width='10%' style='display:none;' >&nbsp;<a href='#'class='up'>Up</a>&nbsp;&nbsp;<a href='#' class='down'>Down</a></td>"

            + "<td width='1%'><span style='display:none' class='clsFiledTypeText12' >" + fieldID + "</span><span  style='display:none' class='clsFieldTypeValue'>" + fieldID + "</span> </td>"
             + "<td width='1%'><span style='display:none' class='clsFiledTypeText12' >" + fieldNameValue + "</span><span  id='lblIsUsed' style='display:none' class='clsFieldTypeValue'>" + fieldNameValue + "</span> </td>"
            "</tr>";
            $('#tblData tbody').append(rowData);
        }

        function readData() {
            debugger

            var fields = $('#hdnReadFields').val();
            var arr = fields.split(',');
            var i = 0;
            for (var i = 0; i < arr.length; i++) {
                readRow(arr[i].split('#')[0], arr[i].split('#')[2], arr[i].split('#')[1], arr[i].split('#')[3], arr[i].split('#')[4]);
            }
            if ($('#hdnReadFields').val() != '') {
                $('#tblData').show();

            }
            else {
                $('#tblData').hide();

            }


            // $('#tblData').show();
        }





        $(document).ready(function () {

            var DrpSelectText = '<%= Session["DrpSelectText"] %>';

            var MetaDataTypeIndex = DrpSelectText; //$("#ddlMetaDataType option:selected").index();

            if (MetaDataTypeIndex != "--Select link--") {
                if ($('#hdnAdd').val() == 'Y') {
                    $("#ancAddNew").show();
                }
                else {
                    $("#ancAddNew").hide();
                }
            }
            else {
                $("#ancAddNew").hide();
            }
        })

        $("#sidebar").html($("#rightlinks").html());

        function opennewsletter(FieldID, FieldTypeValue, Isused) {
            var IsFlag = 0;
            if (FieldID == "" || FieldID == undefined) {

                var KeyValueKeyObligations = <%= KO%>;
                var KeyValueRequestForm = <%= RF%>;
                var KeyValueImportantDates = <%= ID%>;

                var rowCount = $("#tblData tr").length;
                // var MetaDataTypeText = $("#ddlMetaDataType option:selected").text();
                var MetaDataTypeText = '<%= Session["DrpSelectText"] %>';

                if (MetaDataTypeText == "Key Obligations") {
                    if (rowCount - 1 >= KeyValueKeyObligations) {
                        //alert("You can't allow to add more than " + KeyValueKeyObligations + " KeyObligations.");
                        alert("You are not allowed to add more than " + KeyValueKeyObligations + " Key Obligations.");
                        return false;
                    }
                }
                else if (MetaDataTypeText == "Request Form") {
                    if (rowCount - 1 >= KeyValueRequestForm) {
                        //alert("You can't allow to add more than " + KeyValueRequestForm + " RequestForm.");
                        alert("You are not allowed to add more than " + KeyValueRequestForm + " Request Form.");
                        return false;
                    }
                }
                else if (MetaDataTypeText == "Important Dates") {
                    if (rowCount - 1 >= KeyValueImportantDates) {
                        //alert("You can't allow to add more than " + KeyValueImportantDates + " ImportantDates.");
                        alert("You are not allowed to add more than " + KeyValueImportantDates + " Important Dates.");
                        return false;
                    }
                }

            }
            //var MetaDataTypeIndex = $("#ddlMetaDataType option:selected").index();
            var MetaDataTypeSelectText = '<%= Session["DrpSelectText"] %>';
            if (MetaDataTypeSelectText == "Key Obligations") {
                MetaDataTypeIndex = 1;
            }
            else if (MetaDataTypeSelectText == "Request Form") {
                MetaDataTypeIndex = 2;
            }
            else if (MetaDataTypeSelectText == "Important Dates") {
                MetaDataTypeIndex = 3;
            }
            if (MetaDataTypeSelectText == "Important Dates") {
                IsFlag = Isused;                    // Added by kiran for edit
                Isused = 0;
            }
            if (Isused == '1') {
                MessageMasterDiv('This records can not be deleted or edited.', 1);
                return false;
            }
            else {
                emailwindow = dhtmlmodal.open('FieldDetails', 'iframe', '../Configurators/MetaDataConfigurators.aspx?MetaDataIndex=' + MetaDataTypeIndex + "&MetaDataFieldId=" + FieldID + "&FieldTypeValue=" + FieldTypeValue + "&IsFlag=" + IsFlag, '', 'width=1100px,scrollbars=no,height=550px,center=1,resize=0"');

                emailwindow.onclose = function () {

                    var theform = this.contentDoc.forms[0]

                    return true;
                }

            }
        }

        function openDeleteletter(fieldID, fieldTypeValue, Isused) {
            debugger;
            var IsFlag = 0;
            var MetaDataTypeSelectText = '<%= Session["DrpSelectText"] %>';
//            if (MetaDataTypeSelectText == "Important Dates") {
//                IsFlag = Isused;                    // Added by kiran for edit
//                Isused = 0;
//            }
            if (Isused == '1') {
                MessageMasterDiv('This records can not be deleted or edited.', 1);
                return false;
            }
            var userOP = confirm('Are you sure want to delete this record ?');
            if (userOP == true) {
                BindOtherFields(fieldID);
            }

        }

        function checkIsused() {
            debugger
            $("#tblData").find('tbody').find('td').each(function () {
                debugger

                var isused = $(this).find('#lblIsUsed').html();
                if (isused == 'Y') {
                    MessageMasterDiv('This records can not be deleted or edited.', 1);
                    return false;
                }


            });

        }

//        $('a.btnDelete').live('click', function () {
//            var userOP = confirm('Are you sure want to delete this record ?');
//            if (userOP == true) {
//                // checkIsused();
//                BindOtherFields($(this).closest("tr").find(".clsFiledTypeText12").html());
//            }
//        })

        function BindOtherFields(FieldID) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "MetaData.aspx/DeleteMetaData",
                data: "{ClientName:'" + FieldID + "'}",
                dataType: "json",
                async: false,
                success: function (output) {

                    location.reload();
                    //location.href = "MetaData.aspx";
                }
            });
            //            $('#tblData').innerHTML = "";
            //            readData();
        }

        $('#tblData td img').live('mouseover', function () {
            $(this).css('cursor', 'pointer');
        })
    </script>
</asp:Content>
