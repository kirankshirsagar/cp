﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;
using WorkflowBLL;



public partial class Configurators_ContractType : System.Web.UI.Page
{
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 10;
    IContractType objContract;
    IContractRequest objContractRequest;
    public string Add;
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {

        CreateObjects();

        if (Session[Declarations.User] != null && !IsPostBack)
        {
            Access.PageAccess(this, Session[Declarations.User].ToString(), "Configurators_");
            ViewState[Declarations.Add] = Access.Add.Trim();
            ViewState[Declarations.View] = Access.View.Trim();
            ViewState[Declarations.Delete] = Access.Delete.Trim();
            ViewState[Declarations.Update] = Access.Update.Trim();
        }
        AccessVisibility();
        if (!IsPostBack || hdnSearch.Value.Trim() != string.Empty)
        {
            ContractTypeBind();
            ReadData(1, RecordsPerPage);
            Message();
        }
        else
        {
            SetNavigationButtonParameters();
        }



    }

    void ContractTypeBind()
    {
        Page.TraceWrite("Contract type dropdown bind starts.");
        try
        {
            objContractRequest.UserID = int.Parse(Session[Declarations.User].ToString());
            ddlContractType.extDataBind(objContractRequest.SelectContractType());
            ddlContractType.Items.RemoveAt(0);
            ddlContractType.Items.Insert(0, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            Page.TraceWarn("Contract type dropdown bind fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Contract type dropdown bind ends.");
    }

    void Message()
    {
        if (Session[Declarations.Message] != null)
        {
            var flg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }

    }

    #region Navigation
    void SetNavigationButtonParameters()
    {
        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
            AccessVisibility();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        ReadData(1, RecordsPerPage);
    }
    #endregion

    void ReadData(int pageNo, int recordsPerPage)
    {

        if (View == "Y")
        {
            Page.TraceWrite("ReadData starts.");
            try
            {
                objContract.UserId = int.Parse(Session[Declarations.User].ToString());
                objContract.PageNo = pageNo;
                objContract.RecordsPerPage = recordsPerPage;
                objContract.Search = txtSearch.Value.Trim();
                objContract.ContractTypeId = Convert.ToInt32(hdnContractTypeID.Value);
                List<ContractType> activeTemplates = null;
                if (hdnContractTypeID.Value != "0")
                {

                    activeTemplates = objContract.ReadData().FindAll(x => x.isActive == "Active" &&
                    x.ContractTypeId == Convert.ToInt32((hdnContractTypeID.Value)));

                }
                else
                {
                    activeTemplates = objContract.ReadData().FindAll(x => x.isActive == "Active");
                }

                rptContractTypeMaster.DataSource = activeTemplates;
                rptContractTypeMaster.DataBind();
                Page.TraceWrite("ReadData ends.");
                ViewState[Declarations.TotalRecord] = objContract.TotalRecords;
                PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
                PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
                PaginationButtons1.RecordsPerPage = RecordsPerPage;
                ddlContractType.SelectedValue = hdnContractTypeID.Value;
                SetNavigationButtonParameters();
            }
            catch (Exception ex)
            {
                Page.TraceWarn("ReadData fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objContractRequest = FactoryWorkflow.GetContractRequestDetails();
            objContract = FactoryMaster.GetContractTypeDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void imgEdit_Click(object sender, EventArgs e)
    {
        Server.Transfer("StageConfiguratorData.aspx");
    }

    protected void imgEditContractType_Click(object sender, EventArgs e)
    {
        Server.Transfer("StageConfiguratorContractTypeData.aspx");
    }

    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (View == "N")
        {
            Page.extDisableControls();

        }
        else
        {
            btnSearch.Enabled = true;
            txtSearch.Disabled = false;
        }

        if (Update == "N")
        {
            foreach (RepeaterItem item in rptContractTypeMaster.Items)
            {
                LinkButton imgEdit = (LinkButton)item.FindControl("imgEdit");
                imgEdit.Enabled = false;

            }
        }
        Page.TraceWrite("AccessVisibility starts.");
    }

    protected void rptContractTypeMaster_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label ContractTypeID = (Label)e.Item.FindControl("lblContractId");
            Label lblConName = (Label)e.Item.FindControl("lblConName");
            LinkButton lnkContractTypeName = (LinkButton)e.Item.FindControl("lnkContractTypeName");
            if (ContractTypeID.Text == "0")
            {
                lblConName.Visible = false;
                lnkContractTypeName.Visible = true;
            }
        }
    }

}










