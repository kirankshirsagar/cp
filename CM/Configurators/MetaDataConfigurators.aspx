﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MetaDataConfigurators.aspx.cs"
    Inherits="Configurators_MetaDataConfigurators" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%--<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script src="../scripts/jquery-1.7.2.min.js" type="text/javascript"></script>
    <link href="../Styles/chosen.css" rel="stylesheet" type="text/css" />
    <script src="../scripts/chosen.jquery.js" type="text/javascript"></script>
    <script src="../CommonScripts/dropdownlist.js" type="text/javascript"></script>
    <script src="../Scripts/application.js" type="text/javascript"></script>
    <script src="../JQueryValidations/mastervalidations.js" type="text/javascript"></script>
    <link href="../Styles/application.css" media="all" rel="stylesheet" type="text/css" />
    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    <script type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");

        function ReloadParent() {
            window.parent.location.reload();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <%--   <uc2:Setuplinks ID="SetuplinksID" runat="server" />--%>
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <input id="hdnData" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnFieldNameValue" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnFieldNameText" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnReadFields" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnContractTemplateId" runat="server" clientidmode="Static" type="hidden" />
    <input type="hidden" runat="Server" id="hdnSignleSelectValues" />
    <input type="hidden" runat="Server" id="hdnSignleSelectValuesWithClauses" />
    <input type="hidden" runat="Server" id="hdnItems" />
     <input id="hdnEmailTemplateId" runat="server" clientidmode="Static" type="hidden" value="0" />
     <input id="hdnIsFlag" runat="server" clientidmode="Static" type="hidden" value="0" />
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text="Add MetaData Field"></asp:Label>
    </h2>
    <div class="box tabular">
        <p>
            <label for="time_entry_issue_id" style="margin-top: 0px;">
                MetaData Type</label>
            <asp:Label ID="lblMetaDataType" runat="server" Text="Label"></asp:Label>
        </p>
        <p>
            <label for="time_entry_issue_id" style="margin-top: 0px;">
                <span class="required">*</span>Field Type name</label>
            <select id="ddlFieldType" clientidmode="Static" class="chzn-select required chzn-select"
                runat="server" style="width: 225px;">
                <option></option>
            </select>
            <em></em>
        </p>
        <div id="divEmailTemplate" style="display: none;">
            <p>
                <label for="time_entry_issue_id" style="margin-top: 0px;">
                    Email Template name</label>
                <select id="ddlEmailTemplate" clientidmode="Static" class="chzn-select chzn-select"
                    runat="server" style="width: 225px;">
                    <option></option>
                </select>
                <em></em>
            </p>
        </div>
        <p>
            <span id="spanFieldName">
                <label for="time_entry_issue_id" style="margin-top: 0px;">
                    <span class="required">*</span>Field Name</label>
                <input id="txtFiledName" clientidmode="Static" runat="server" type="text" class="required Name "
                    maxlength="50" style="width: 220px;" />
            </span><em></em>
        </p>
        <table id="mainTableMetadata" class="mainTableMetadata" style="margin-left: 17%">
            <tr>
                <td>
                    <label for="time_entry_issue_id" style="margin-top: 0px;">
                        Option</label>
                </td>
                <td>
                    <asp:TextBox ID="txtOption" runat="server" ClientIDMode="Static" MaxLength="50" CssClass="ClauseFieldsPatterns"></asp:TextBox>
                    <input type="button" id="btnAddOption" value="Add" />
                </td>
            </tr>
        </table>
        <table id="mainTable" class="mainTableMetadata" style="margin-left: 17%">
            <tr class="SingleSelect">
                <td>
                    <table id="tblData1" width="70%">
                        <tr>
                            <th id="thFieldValue" width="25%">
                                <asp:Label ID="lblValue" ClientIDMode="Static" runat="server" Text=""></asp:Label>
                            </th>
                            <th id="thAttachClause" width="65%">
                                <asp:Label ID="lblAttachClause" ClientIDMode="Static" runat="server" Text=""></asp:Label>
                            </th>
                            <th id="thDelete1" width="5%">
                            </th>
                            <th id="thUpDown1" width="10%">
                            </th>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <table id="tblData" class="list issues" width="80%">
        <tr>
            <th id="thFieldType" width="40%">
                <asp:Label ID="lblFieldType" ClientIDMode="Static" runat="server" Text="Field Type"></asp:Label>
            </th>
            <th id="thFieldName" width="40%">
                <asp:Label ID="lblFieldName" ClientIDMode="Static" runat="server" Text="Field Name"></asp:Label>
            </th>
            <th id="thDelete" width="5%">
            </th>
            <th id="thUpDown" width="5%">
            </th>
            <th id="thFieldID" width="1%" style="display: none">
                <asp:Label ID="lblFieldId" ClientIDMode="Static" runat="server" Text="FieldID"></asp:Label>
            </th>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <asp:Button ID="btnSave" ClientIDMode="Static" runat="server" OnClientClick="return getData();"
                    Text="Save" OnClick="btnSave_Click" />
            </td>
            <td>
            </td>
        </tr>
    </table>
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $('#ddlEmailTemplate').change(function () {
            $('#hdnEmailTemplateId').val($('#ddlEmailTemplate').val());
            //alert($('#hdnEmailTemplateId').val());
        });

        $(document).ready(function () {
            if ($('#ddlFieldType').val() == '7') {
                if ($('#lblMetaDataType').text() == 'Important Dates') {
                    if ($('#hdnEmailTemplateId').val() != '0')
                        $('#ddlEmailTemplate').val($('#hdnEmailTemplateId').val());

                    $('#divEmailTemplate').css('display', 'block');
                } else {
                    $('#divEmailTemplate').css('display', 'none');
                    $('#ddlEmailTemplate').val(0);
                }
            } else {
                $('#divEmailTemplate').css('display', 'none');
                $('#ddlEmailTemplate').val(0);
            }

            if ($('#hdnIsFlag').val() != '0' && $('#hdnIsFlag').val()!='undefined') {
                $('#ddlFieldType').attr('disabled', 'disabled');
                $('#txtFiledName').attr('disabled', 'disabled');
            }
        });

    </script>
    <script type="text/javascript">

        $('#txtOption').on('keypress', function (e) {
            var regex = new RegExp("^[a-zA-Z0-9\\ \\s]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }

            e.preventDefault();
            return false;

        });


        $('#txtFiledName').on('keypress', function (e) {
            var regex = new RegExp("^[a-zA-Z0-9\\ \\s]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }

            e.preventDefault();
            return false;

        });

        $("#sidebar").html($("#rightlinks").html());

        $('#ddlFieldType').change(function () {

            $('#mainTable').find('table[id^="tblData1"] tbody tr:not(:first)').remove();
            var FieldTypeText = $("#ddlFieldType option:selected").text();
            if (FieldTypeText == "RadioButton" || FieldTypeText == "Checkbox" || FieldTypeText == "Dropdown") {
                $(".mainTableMetadata").show();
            }
            else {
                $(".mainTableMetadata").hide();
            }

            if ($('#ddlFieldType').val() == 9) {
                $('#spanMasterField').show().find('div[id^="ddlMaster"]').attr('style', 'width:225px');
            }
            else {
                $('#spanMasterField').hide();
                $('#spanFieldName').show();
                $('#txtFiledName').focus();
            }

            if ($('#ddlFieldType').val() == '7') {
                if ($('#lblMetaDataType').text() == 'Important Dates') {
                    $('#divEmailTemplate').css('display', 'block');
                } else {
                    $('#divEmailTemplate').css('display', 'none');
                    $('#ddlEmailTemplate').val(0);
                }
            } else {
                $('#divEmailTemplate').css('display', 'none');
                $('#ddlEmailTemplate').val(0);
            }

        });

        $(document).ready(function () {
            $('#spanMasterField').hide();
        })

        function hideTable(flg) {

            if (flg == 1 && $('#tblData tbody').find('tr').length <= 1) {
                if ($('#hdnReadFields').val() != '') {
                    $('#tblData').show();
                }
                else {
                    $('#tblData').hide();
                }
            }
            else {
                $('#tblData').show();
            }
        }

        $('#btnAddMoreFiled').click(function () {

            if ($('#ddlFieldType').val() == 0) {
                alert('Please select field type.');
                $('#ddlFieldType').focus();
                return false;
            }

            if ($('#txtFiledName').val() == '') {
                alert('Please enter value.');
                $('#txtFiledName').focus().val('');
                return false;
            }

            var rowCount = $("#tblData tr").length;

            var MetaDataTypeText = $("#ddlMetaDataType option:selected").text();

            var fieldNameValue = "";
            var fieldTypeValue = $('#ddlFieldType').val();
            var fieldTypeText = $("#ddlFieldType option:selected").text();
            var fieldNameText = '';

            if ($('#ddlFieldType').val() == 9) {
                fieldNameValue = $('#ddlMasterField').val();
                fieldNameText = $('#txtFiledName').val() + '_' + $("#ddlMasterField option:selected").text();
            }
            else {
                fieldNameValue = $('#txtFiledName').val();
                fieldNameText = fieldNameValue;
            }

            if ($('#txtFiledName').val() == '') {
                fieldTypeValue = '';
            }

            var rowData;

            rowData = "<tr>"
            + "<td width='40%'><span class='clsFiledTypeText'>" + fieldTypeText + "</span><span style='display:none' class='clsFieldTypeValue'>" + fieldTypeValue + "</span> </td>"
            + "<td width='40%'><a href='#' class='clsFieldNameText'>" + fieldNameText + "</a> <span style='display:none' class='clsFieldNameValue'>" + fieldNameValue + "</span>  </td>"
            + "<td width='10%'><a href='#' class='btnDelete' clientidmode='static'>Delete</a></td>"
            + "<td width='10%'>&nbsp;<a href='#'class='up'>Up</a>&nbsp;&nbsp;<a href='#' class='down'>Down</a></td>"
            "</tr>";


            if (fieldNameValue != '' && checkExists(fieldNameText) == true) {
                if ($('#hdnFieldNameText').val() != '') {
                    replaceTr(fieldTypeText, fieldTypeValue, fieldNameText, fieldNameValue)
                }
                else {
                    $('#tblData tbody').append(rowData);
                }
                $('#txtFiledName').val('').focus();
            }

            else if (fieldNameValue == '') {
                alert('Please enter value.');
                $('#txtFiledName').focus().val('');
                return false;
            }
            else {
                alert('This value already exists.');
                $('#txtFiledName').focus().val('');
                return false;
            }

            hideTable(0);
            $('#ddlFieldType').val('0');
            $('#ddlFieldType').trigger('liszt:updated');
        });

        function checkExists(val) {

            var rvalue = true;

            $('#tblData tbody').find('a[class="clsFieldNameText"]').each(function () {

                if ($('#hdnFieldNameText').val() != '' && $('#hdnFieldNameText').val() == val) {
                    rvalue = true;
                }
                else if (val.toLowerCase() == $(this).html().toLowerCase()) {
                    rvalue = false;
                }
            });
            return rvalue;
        }

        function checkExistsForOptions(val) {
            var rvalue = true;
            $('#mainTable').find('table[id^="tblData1"] tbody').find('input[class^="clsValue"]').each(function () {
                if (val.toLowerCase() == $(this).val().toLowerCase()) {
                    rvalue = false;
                }
            });
            return rvalue;
        }

        function replaceTr(fieldTypeText, fieldTypeValue, fieldNameText, fieldNameValue) {
            $('#tblData tbody').find('tr').each(function () {

                if ($(this).find('a[class="clsFieldNameText"]').html() == $('#hdnFieldNameText').val()) {
                    $(this).find('span[class="clsFiledTypeText"]').html(fieldTypeText);
                    $(this).find('span[class="clsFieldTypeValue"]').html(fieldTypeValue);
                    $(this).find('a[class="clsFieldNameText"]').html(fieldNameText);
                    $(this).find('span[class="clsFieldNameValue"]').html(fieldNameValue);
                }
            });
            $('#hdnFieldNameText').val('');
        }

        $('.btnDelete1').live('click', function () {
            var par = $(this).parent().parent();
            par.remove();
            hideTable(1);

        });

        $(document).ready(function () {
            $(".up1,.down1").live('click', function () {
                var $row = $(this).parents('tr:first');
                if ($(this).is(".up1")) {
                    if ($row.index() > 1) {
                        $row.insertBefore($row.prev());
                    }
                } else {
                    $row.insertAfter($row.next());
                }
            });

            $('.clsFieldNameText').live('click', function () {
                var fieldType = $(this).closest('tr').find('span[class="clsFieldTypeValue"]').html();
                var fieldValue = $(this).closest('tr').find('span[class="clsFieldNameValue"]').html();
                var fieldText = $(this).closest('tr').find('a[class="clsFieldNameText"]').html();

                $('#ddlFieldType').val(fieldType);
                updateSettings('#ddlFieldType');
                $('#ddlFieldType').change();
                if (fieldType == 9) {
                    $('#ddlMasterField').val(fieldValue);
                    updateSettings('#ddlMasterField');
                    $('#txtFiledName').val(fieldText.split('_')[0]);
                }
                else {
                    $('#txtFiledName').val(fieldValue);
                }

                $('#hdnFieldNameText').val(fieldText);

                return false;
            });
        });

        function getData() {
            if ($('#ddlFieldType').val() == 0) {
                alert('Please select field type.');
                $('#ddlFieldType').focus();
                return false;
            }

            if ($('#txtFiledName').val() == '') {
                alert('Please enter value.');
                $('#txtFiledName').focus().val('');
                return false;
            }

            getSignleSelectValuesWithClauses();

            var data = '';
            var cnt = 0;
            var vFieldType = '';
            var vFieldName = '';
            var vEmailTempId = '';

            vFieldType = $('#ddlFieldType').val();
            var FieldNameValue = $("#txtFiledName").val();
            var FieldNameText = $("#txtFiledName").val();
            vFieldName = FieldNameValue + '$' + FieldNameText;

            //vEmailTempId = $("#ddlEmailTemplate").val();

            data = vFieldType + ',' + vFieldName; //+','+vEmailTempId;

            $('#hdnData').val(data);
            $('#hdnEmailTemplateId').val($('#ddlEmailTemplate').val());

            if (data == '') {
                alert('Please add fields.');
                return false;
            }
            else {

                return true;
            }
        }

        function getSignleSelectValuesWithClauses() {

            var rvalueWithClause = '';
            var rvalue = '';
            var value = '';
            var clauses = '';
            $('#mainTable').find('table[id^="tblData1"] tbody').find('tr').each(function () {
                value = $(this).find('input[class^="clsValue"]').val();
                clauses = $(this).find('select').val();

                if (value != undefined && value != null) {
                    if (clauses == undefined || clauses == null) {
                        clauses = '';
                    }
                    if (rvalueWithClause == '') {
                        rvalueWithClause = value + '#' + clauses;
                        rvalue = value;
                    }
                    else {
                        rvalueWithClause = rvalueWithClause + '^' + value + '#' + clauses;
                        rvalue = rvalue + ',' + value;
                    }
                }
            });

            $('#hdnSignleSelectValuesWithClauses').val(rvalueWithClause);
            $('#hdnSignleSelectValues').val(rvalue);
        }
        hideTable(1);

        function readRow(fieldTypeText, fieldTypeValue, fieldNameText, fieldID, fieldNameValue) {

            var rowData;
            rowData = "<tr>"
            + "<td width='40%'><span class='clsFiledTypeText'>" + fieldTypeText + "</span><span style='display:none' class='clsFieldTypeValue'>" + fieldTypeValue + "</span> </td>"
            + "<td width='40%'><a href='#' class='clsFieldNameText'>" + fieldNameText + "</a> <span style='display:none' class='clsFieldNameValue'>" + fieldNameValue + "</span>  </td>"
            + "<td width='10%'><a href='#' class='btnDelete' clientidmode='static'>Delete</a></td>"
            + "<td width='10%'>&nbsp;<a href='#'class='up'>Up</a>&nbsp;&nbsp;<a href='#' class='down'>Down</a></td>"

             + "<td width='1%'><span style='display:none' class='clsFiledTypeText'>" + fieldID + "</span><span style='display:none' class='clsFieldTypeValue'>" + fieldID + "</span> </td>"

            "</tr>";
            $('#tblData tbody').append(rowData);
        }

        function readData() {
            var fields = $('#hdnReadFields').val();
            var arr = fields.split(',');
            var i = 0;
            for (var i = 0; i < arr.length; i++) {
                readRow(arr[i].split('#')[0], arr[i].split('#')[2], arr[i].split('#')[1], arr[i].split('#')[3]);
            }
            $('#tblData').show();
        }

        $('#btnAddOption').click(function () {

            var tblRowCount = $("#tblData1 tr").length;
            if (tblRowCount > 10) {
                alert("Not allow to add more than 10 options.");
            }
            else {
                var val = $('#txtOption').val();
                var rowData;

                rowData = "<tr>"
            + "<td width='25%'><input class='clsValue required' maxlength='50' type='text' value='" + val + "' /></td>"
            + "<td width='65%'> <select style='width:95%' class='clsClause'></select> </td>"
            + "<td width='5%'><a href='#' class='btnDelete1' clientidmode='static'>Delete</a></td>"
            + "<td width='10%'>&nbsp;<a href='#'class='up1'>Up</a>&nbsp;&nbsp;<a href='#' class='down1'>Down</a></td>"
                "</tr>";

                if (val != '' && checkExistsForOptions(val) == true) {
                    $('#mainTable').find('table[id^="tblData1"] tbody').append(rowData);
                    $('#txtOption').val('').focus();
                }
                else if (val == '') {
                    alert('Please enter value.');
                    $('#txtOption').focus().val('');
                    return false;
                }
                else {
                    alert('This value already exists.');
                    $('#txtOption').focus().val('');
                    return false;
                }
            }
        });

        $(window).load(function () {

            var hdnValue = $('#hdnItems').val();

            if (hdnValue == "" || hdnValue == "undefined") {
                $(".mainTableMetadata").hide();
            }
        });

        $(document).ready(function () {

            var hdnValue = $('#hdnItems').val();

            if (hdnValue != '' && hdnValue != 'undefined') {

                var SelectedValues = new Array();
                var SelectedValues = $('#hdnItems').val().split(',');

                for (i = 0; i <= SelectedValues.length; i++) {

                    var ind = $('#drpDropDownList').val();

                    var rowData;

                    rowData = "<tr>"
                + "<td width='25%'><input class='clsValue required' maxlength='50' type='text' value='" + SelectedValues[i] + "' /></td>"
                + "<td width='65%'> <select style='width:95%' class='clsClause'></select> </td>"
                + "<td width='5%'><a href='#' class='btnDelete1' clientidmode='static'>Delete</a></td>"
                + "<td width='10%'>&nbsp;<a href='#'class='up1'>Up</a>&nbsp;&nbsp;<a href='#' class='down1'>Down</a></td>"
                    "</tr>";


                    if (SelectedValues[i] != '' && SelectedValues[i] != undefined) {
                        $('#mainTable').find('table[id^="tblData1"] tbody').append(rowData);
                    }
                    //fillSelectInTable(ind);
                }
            }
            //$(".mainTableMetadata").show();

        });

        function fillSelectInTable(flg) {

            $('#mainTable').find('table[id^="tblData1"] tbody').find('select').each(function () {
                if ($(this).has('option').length <= 0) {
                    bindSelect(GlobalClauses, this);
                }
            });
        }

    </script>
    </form>
</body>
</html>
