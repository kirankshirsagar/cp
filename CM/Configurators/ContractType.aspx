﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/CM.master"
    AutoEventWireup="true" CodeFile="ContractType.aspx.cs" Inherits="Configurators_ContractType" %>
    <%@ Register Src="~/UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridSelect.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
   <script language="javascript" type="text/javascript">
       $("#setupLink").addClass("menulink");
       $("#setuptab").addClass("selectedtab");       

    </script>
   <uc2:setuplinks ID="SetuplinksID" runat="server" />
    <h2>
        Approval workflow configurator</h2>
    <input id="hdnPrimeIds" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnPrimeName" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnTemplateId" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnTemplateName" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnContractTypeID" runat="server" clientidmode="Static" type="hidden" value="0" />
        <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
        <input id="Hidden1" runat="server" clientidmode="Static" type="hidden" />
    <div style="margin: 0; padding: 0; display: inline">
        <div id="query_form_content" class="hide-when-print">
            <fieldset id="filters" class="collapsible">
                <legend onclick="toggleFieldset(this);">Filters</legend>
                <div style="">
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td>
                                    <table id="filters-table" width="100%" cellpadding="3" cellspacing="0">
                                        <tbody>

                                            <tr>
                                                <td width="10%">
                                                    Keywords
                                                </td>
                                                <td width="60%" align="left">

                                                <input id="txtSearch" runat="Server" clientidmode="Static" />

                                                </td>
                                            </tr>


                                            <tr>
                                                <td width="10%">
                                                    Contract Type
                                                </td>
                                                <td width="60%" align"left">
                                                    <asp:DropDownList ID="ddlContractType" runat="server"
                                                     class="chzn-select required chzn-select" style="width: 40%" 
                                                       onchange="setValue(this)">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </fieldset>
        </div>
        <p class="buttons hide-when-print">
            <asp:LinkButton ID="btnSearch" runat="server" OnClientClick="return searchClick();"
                CssClass="icon icon-checked" OnClick="btnSearch_Click">Filter</asp:LinkButton>
            <asp:LinkButton ID="btnShowAll" runat="server" OnClientClick="return resetClick();"
                CssClass="icon icon-reload" OnClick="btnSearch_Click">Reset Filter</asp:LinkButton>
                <%--<asp:LinkButton ID="lnkAllContracts" runat="server" ClientIDMode="Static" 
                                    OnClick="imgEditContractType_Click">All Contract Type</asp:LinkButton>--%>
        </p>
        <div class="autoscroll">
            <table width="100%">
                <asp:Repeater ID="rptContractTypeMaster" runat="server" OnItemDataBound="rptContractTypeMaster_ItemDataBound">
                    <HeaderTemplate>
                        <table id="masterDataTable" class="masterTable list issues" width="100%">
                            <thead>
                                <tr>
                                    <th width="0%" style="display: none;">
                                        Contract Type Id
                                    </th>
                                    <th width="30%">
                                        Contract Type Name
                                    </th>
                                    <th width="30%">
                                        Contract Template
                                    </th>
                                    <th width="30%">
                                        Stage Count
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="display: none">
                                <asp:Label ID="lblContractId" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTypeId") %>'></asp:Label>
                                <asp:Label ID="lblIsUsed" runat="server" ClientIDMode="Static" Text='<%#Eval("isUsed") %>'></asp:Label>
                                <asp:Label ID="lblContractName" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTypeName") %>'></asp:Label>
                                 <asp:Label ID="lblContractTemplateName" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTemplateName") %>'></asp:Label>
                                  <asp:Label ID="lblTemplateId" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTemplateId") %>'></asp:Label>
                            </td>
                            <td>
                              <asp:LinkButton ID="lnkContractTypeName" runat="server" Visible="false"
                                    OnClick="imgEditContractType_Click"><%#Eval("ContractTypeName")%></asp:LinkButton>
                                <asp:Label ID="lblConName" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTypeName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:LinkButton ID="imgEdit" runat="server" OnClientClick="return setSelectedId(this);"
                                    OnClick="imgEdit_Click"><%#Eval("ContractTemplateName")%></asp:LinkButton>
                            </td>
                            <td>
                                <asp:Label ID="lblStageCount" runat="server" ClientIDMode="Static" Text='<%#Eval("StageCount") %>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
            <p class="pagination">
            <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
            </p>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $('ddlContractType').closest('form').trigger('onchange');
                LockUnLockImage('masterTable');
            });

            function setValue(obj) {
              //  alert($(obj).val());
                $('#hdnContractTypeID').val($(obj).val());
             
            }

            function resetClick() {
                $('#hdnContractTypeID').val('0');
                $('#ddlContractType').val('0');
                $('#txtSearch').val('');
                return true;
            
            }

            function setSelectedId(obj) {
                debugger;
                var pId = $(obj).closest('tr').find('span[id *= lblContractId]').text();
                var pName = $(obj).closest('tr').find('span[id *= lblContractName]').text();
                var TempId = $(obj).closest('tr').find('span[id *= lblTemplateId]').text();
                var TempName = $(obj).closest('tr').find('span[id *= lblContractTemplateName]').text();
               
                if (pId == '' || pId == '0') {
                    return false;
                }
                else {
                    
                    $('#hdnPrimeIds').val(pId);
                    $('#hdnPrimeName').val(pName);
                    $('#hdnTemplateId').val(TempId);
                    $('#hdnTemplateName').val(TempName);
                }
            }
        </script>
        <div id="rightlinks" style="display: none;">
            <uc1:Masterlinks ID="rightactions" runat="server" />
        </div>
        <script type="text/javascript">
            $("#sidebar").html($("#rightlinks").html());
        </script>
</asp:Content>
