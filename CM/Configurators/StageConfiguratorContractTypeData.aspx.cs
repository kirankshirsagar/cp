﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;
using StageConfigueBLL;

public partial class Configurators_StageConfiguratorData : System.Web.UI.Page
{ 
    IStageConfigurator objStage;
    public string Add;
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {

        CreateObjects();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            Access.PageAccess(this, Session[Declarations.User].ToString(), "Configurators_");
            ViewState[Declarations.Add] = Access.Add.Trim();
            ViewState[Declarations.View] = Access.View.Trim();
            ViewState[Declarations.Delete] = Access.Delete.Trim();
            ViewState[Declarations.Update] = Access.Update.Trim();
        }
        setAccessValues();
      
        if (!IsPostBack || hdnSearch.Value.Trim()!=string.Empty)
        {
            hdnContractTypeId.Value = "0";
            hdnTemplateId.Value = "0";
            ReadData();
            Message();
        }
        AccessVisibility();
        
    }

    void Message()
    { 
         if (Session[Declarations.Message] != null)
        {
            var flg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }
       
    }
   
    void ReadData()
    {
        if (View == "Y")
        {
            Page.TraceWrite("ReadData starts.");
            try
            {
                //if (String.IsNullOrEmpty(Request.extValue("hdnPrimeIds")) != true)
                //{
                    //hdnContractTypeId.Value = Request.extValue("hdnPrimeIds");
                    //hdnTemplateId.Value = Request.extValue("hdnTemplateId");
                    objStage.ContractTypeId = int.Parse(hdnContractTypeId.Value);
                    objStage.ContractTemplateId = int.Parse(hdnTemplateId.Value);
                    objStage.ReadData();
                    rptLevel.DataSource = objStage.Levels;
                    rptLevel.DataBind();
                    StageBind();
                //}

                if (String.IsNullOrEmpty(Request.extValue("HdnCotractTypeId")) != true && String.IsNullOrEmpty(Request.extValue("hdnTemplateId")) != true)
                {
                    hdnContractTypeId.Value = Request.extValue("HdnCotractTypeId");
                    hdnTemplateId.Value = Request.extValue("hdnTemplateId");
                    objStage.ContractTypeId = int.Parse(hdnContractTypeId.Value);
                    objStage.ContractTemplateId = int.Parse(hdnTemplateId.Value);
                    objStage.ReadData();
                    rptLevel.DataSource = objStage.Levels;
                    rptLevel.DataBind();
                    StageBind();
                }
                //if (String.IsNullOrEmpty(Request.extValue("hdnPrimeName")) != true)
                //{
                //    lblContractType.Text = Request.extValue("hdnPrimeName");
                //    lblContractTemplate.Text = Request.extValue("hdnTemplateName");
                //    hdnContractTypeName.Value = lblContractType.Text;
                //    hdnTemplateName.Value = lblContractTemplate.Text;
                //}
                //if (String.IsNullOrEmpty(Request.extValue("HdnCotractType")) != true && String.IsNullOrEmpty(Request.extValue("HdnCotractTemplateType")) != true)
                //{
                //    lblContractType.Text = Request.extValue("HdnCotractType");
                //    lblContractTemplate.Text = Request.extValue("HdnCotractTemplateType");
                //    hdnContractTypeName.Value = lblContractType.Text;
                //    hdnTemplateName.Value = lblContractTemplate.Text;
                //}
                
                Page.TraceWrite("ReadData ends.");
            }
            catch (Exception ex)
            {
                Page.TraceWarn("ReadData fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
    }

    public void StageBind()
    {
        var stage = objStage.Stages;
        for (int i = 0; i <= rptLevel.Items.Count - 1; i++)
        {
            Label id = (Label)rptLevel.Items[i].FindControl("lblLevelStr");
            Repeater rptStages = (Repeater)rptLevel.Items[i].FindControl("rptStages");

            var result = stage.Where(x => x.LevelStr1 == id.Text
                                       || x.LevelStr2 == id.Text
                                       || x.LevelStr2 == id.Text).ToList();
            rptStages.DataSource = result;
            rptStages.DataBind();
            
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
          objStage = FactoryStageConfigurator.GetStageConfiguratorDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);  
         }
        Page.TraceWrite("Page object create ends.");
    }

    protected void lnkAddStage_Click(object sender, EventArgs e)
    {
        Server.Transfer("StageConfiguratorContractType.aspx");
    }

    protected void Back_Click(object sender, EventArgs e)
    {
        Server.Transfer("ContractType.aspx");
    }

    protected void lnkEditStage_Click(object sender, EventArgs e)
    {
        Server.Transfer("StageConfiguratorContractType.aspx");
    }

    protected void lnkDeleteStage_Click(object sender, EventArgs e)
    {
        objStage.StageId = int.Parse(hdnStageId.Value);
        objStage.DeleteRecord();
        objStage.StageId = 0;
        objStage.ContractTypeId = int.Parse(hdnContractTypeId.Value);
        objStage.ContractTemplateId = int.Parse(hdnTemplateId.Value);
        objStage.ReadData();
        rptLevel.DataSource = objStage.Levels;
        rptLevel.DataBind();
        StageBind();
        hdnStageId.Value = "";
       
    }

    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }

    void AccessVisibility()
    {
       
        Page.TraceWrite("AccessVisibility starts.");
      
        if (Update == "N" || Delete =="N")
        {
            foreach (RepeaterItem item in rptLevel.Items)
            {
                Repeater rptStages = (Repeater)item.FindControl("rptStages");
                foreach (RepeaterItem stageItem in rptStages.Items)
                {
                    if (Update == "N")
                    {
                        LinkButton lnkEditStage1 = (LinkButton)stageItem.FindControl("lnkEditStage1");
                        LinkButton lnkEditStage2 = (LinkButton)stageItem.FindControl("lnkEditStage2");
                        LinkButton lnkEditStage3 = (LinkButton)stageItem.FindControl("lnkEditStage3");
                        if (lnkEditStage1 != null)
                        {
                            lnkEditStage1.Enabled = false;
                            lnkEditStage1.Visible = false;
                        }
                        if (lnkEditStage2 != null)
                        {
                            lnkEditStage2.Visible = false;
                        }
                        if (lnkEditStage1 != null)
                        {
                            lnkEditStage3.Visible = false;
                        }
                    }

                    if (Delete == "N")
                    {
                        LinkButton lnkDeleteStage1 = (LinkButton)stageItem.FindControl("lnkDeleteStage1");
                        LinkButton lnkDeleteStage2 = (LinkButton)stageItem.FindControl("lnkDeleteStage2");
                        LinkButton lnkDeleteStage3 = (LinkButton)stageItem.FindControl("lnkDeleteStage3");
                        if (lnkDeleteStage1 != null)
                        {
                            lnkDeleteStage1.Enabled = false;
                            lnkDeleteStage1.Visible = false;
                        }
                        if (lnkDeleteStage2 != null)
                        {
                            lnkDeleteStage2.Visible = false;
                        }
                        if (lnkDeleteStage3 != null)
                        {
                            lnkDeleteStage3.Visible = false;
                        }
                    }
                }
            }
        }
        if (Add == "N")
        {
             foreach (RepeaterItem item in rptLevel.Items)
            {
                LinkButton lnkAddStage = (LinkButton)item.FindControl("lnkAddStage");
                if (lnkAddStage != null)
                {
                    lnkAddStage.Visible = false;
                    lnkAddStage.Enabled = false;
                }
            }

            Page.extDisableControls();
            btnBack.Enabled = true;
          
        }

        Page.TraceWrite("AccessVisibility starts.");
    }

}


    




 


