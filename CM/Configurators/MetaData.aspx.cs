﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BulkImportBLL;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;
using MetaDataConfiguratorsBLL;

public partial class Configurators_MetaData : System.Web.UI.Page
{
    IMetaDataConfigurators objCTFields;
    string ViewTemplateFiledData = string.Empty;

    public int RF { get; set; }
    public int KO { get; set; }
    public int ID { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        if (!IsPostBack)  
        {  
            Message();
            BindData();
            setAccessValues();

            RF = Declarations.RequestForm;
            KO = Declarations.KeyObligations;
            ID = Declarations.ImportantDates;
        }
        string a =Convert.ToString(Session["DrpData"]);
    }
    void Message()
    {
        if (Session[Declarations.Message] != null)
        {
            var flg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }
        if (Session["status"] != null)
        {
            Page.Message("2", "0");
            Page.TraceWarn("Insert, Update fails.");
            Session["status"] = null;
        }
    }

    private void setAccessValues()
    {
        string DrpSelectText = Convert.ToString(Session["DrpSelectText"]);
        #region 
        if (DrpSelectText == "Key Obligations")
        {
            Access.PageAccess("MetaData.aspx?Id=KO", Session[Declarations.User].ToString());
        }
        else if (DrpSelectText == "Request Form")
        {
            Access.PageAccess("MetaData.aspx?Id=RF", Session[Declarations.User].ToString());
        }
        else if (DrpSelectText == "Important Dates")
        {
            Access.PageAccess("MetaData.aspx?Id=ID", Session[Declarations.User].ToString());
        }
        ViewState[Declarations.Update] = Access.Update.Trim();
        ViewState[Declarations.Add] = Access.Add.Trim();
        ViewState[Declarations.View] = Access.View.Trim();
        ViewState[Declarations.Delete] = Access.Delete.Trim();
        hdnAdd.Value = ViewState[Declarations.Add].ToString();
        hdnedit.Value = ViewState[Declarations.Update].ToString();
        hdnview.Value = ViewState[Declarations.View].ToString();
        hdndelete.Value = ViewState[Declarations.Delete].ToString();
        //if (ViewState[Declarations.Update].ToString() == "N")
        //{
            
        //}

        #endregion
    }
    
    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objCTFields = FactoryMedaData.GetContractTemplateDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }
    void BindData()
    {
        string DrpSelectText=Convert.ToString(Session["DrpSelectText"]);
        if (DrpSelectText == "Key Obligations")
        {
            objCTFields.MetaDataType = "Key Obligations";
            ViewTemplateFiledData = objCTFields.GetContractTemplateFieldRecord();
            hdnReadFields.Value = ViewTemplateFiledData;
            lblTitalField.Text = "Key Obligations MetaData Fields";
        }
        else if (DrpSelectText == "Request Form")
        {
            objCTFields.MetaDataType = "Request Form";
            ViewTemplateFiledData = objCTFields.GetContractTemplateFieldRecord();
            hdnReadFields.Value = ViewTemplateFiledData;
            lblTitalField.Text = "Request Form MetaData Fields";
        }
        else if (DrpSelectText == "Important Dates")
        {
            objCTFields.MetaDataType = "Important Dates";
            ViewTemplateFiledData = objCTFields.GetContractTemplateFieldRecord();
            hdnReadFields.Value = ViewTemplateFiledData;
            lblTitalField.Text = "Important Dates MetaData Fields";
        }
        Page.ClientScript.RegisterStartupScript(this.GetType(), "readData", "readData()", true);
    }

    [System.Web.Services.WebMethod]
    public static string DeleteMetaData(string ClientName)
    {
        string DeleteMetaDataRecord = "";
        if (ClientName.Length > 0)
        {
            IMetaDataOption objMetaData;
            objMetaData = FactoryMetaDataOption.GetContractTemplateDetail();
            objMetaData.FieldID = Convert.ToInt16(ClientName);
            DeleteMetaDataRecord = objMetaData.DeleteRecord();

            if (DeleteMetaDataRecord != "-1")
                System.Web.HttpContext.Current.Session[Declarations.Message] = "DS";
            else
                System.Web.HttpContext.Current.Session[Declarations.Message] = "DF";
        }
        return DeleteMetaDataRecord;
    }
}