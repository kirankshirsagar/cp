﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/CM.master"
    AutoEventWireup="true" CodeFile="StageConfiguratorData.aspx.cs" Inherits="Configurators_StageConfiguratorData" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
    <%@ Register Src="~/UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridSelect.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");
    </script>
     <input id="hdnTemplateId" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnTemplateName" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnContractTypeId" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnContractTypeName" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnStageId" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnLevelId" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <uc2:setuplinks ID="SetuplinksID" runat="server" />
        <h2>
        Approval workflow configurator</h2>
          
        <label>Contract Type :</label>
        <asp:Label ID="lblContractType" ClientIDMode="Static" runat="server" Text=""></asp:Label>
        <label>Contract Template :</label>
        <asp:Label ID="lblContractTemplate" ClientIDMode="Static" runat="server" Text=""></asp:Label>
      <br><br>
    <asp:Repeater ID="rptLevel" runat="server">
        <HeaderTemplate>
            <table id="masterDataTable" cellpadding="2" class="masterTable configlist" width="100%">
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <th colspan="2" align="left">
                    <b>
                        <asp:Label ID="lblLevelStr" runat="server" ClientIDMode="Static" Text='<%#Eval("LevelStr") %>'></asp:Label></b>
                </th>
                <th align="right" style="text-align: right;">
                    <asp:LinkButton ID="lnkAddStage" OnClick="lnkAddStage_Click" OnClientClick="return setLevelId(this);"
                        CssClass="icon icon-add" ClientIDMode="Static" runat="server">Add Stage</asp:LinkButton>
                </th>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Repeater ID="rptStages" runat="server">
                        <ItemTemplate>
                            <table class="stageTable" width="100%">
                                <tr>
                                    <td style="width: 33.33%">
                                        <div class="splitcontentleft"  >
                                            <div class="issues box StageBox">
                                                <asp:HiddenField ID="hdnStageId1" ClientIDMode="Static" Value='<%# Eval("StageId1") %>'
                                                    runat="server" />
                                                <asp:HiddenField ID="hdnLevelStr1" ClientIDMode="Static" Value='<%# Eval("LevelStr1") %>'
                                                    runat="server" />
                                                <div id="divStageName1">
                                                    <h3>
                                                        <asp:Label ID="lblStageName1" ClientIDMode="Static" runat="server" Text='<%# Eval("StageName1") %>'></asp:Label>
                                                    </h3>
                                                </div>
                                                <div id="divConditions1">
                                                    <b>Conditions : </b>
                                                    <asp:Label ID="lblConditions1" ClientIDMode="Static" runat="server" Text='<%# Eval("Conditions1") %>'></asp:Label>
                                                </div>
                                                <div id="divAssignedTo1">
                                                    <b>Assigned To : </b>
                                                    <asp:Label ID="lblAssignedTo1" ClientIDMode="Static" runat="server" Text='<%# Eval("AssignedTo1") %>'></asp:Label>
                                                </div>
                                                <table class="tblEditDelete" width="100%">
                                                    <tr>
                                                        <td width="50%" align="left">
                                                            <div id="divEdit1">
                                                                <asp:LinkButton ID="lnkEditStage1" OnClick="lnkEditStage_Click" OnClientClick="return setSelectedId(this,0);"
                                                                    CssClass="icon icon-edit" ClientIDMode="Static" runat="server">Edit</asp:LinkButton>
                                                            </div>
                                                        </td>
                                                        <td width="50%" align="right">
                                                            <div id="divDelete1">
                                                                <asp:LinkButton ID="lnkDeleteStage1" ClientIDMode="Static" CssClass="icon icon-del"
                                                                    OnClick="lnkDeleteStage_Click" OnClientClick="return setSelectedId(this,1);"
                                                                    runat="server">Delete</asp:LinkButton>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="width: 33.33%">
                                        <div class="splitcontentleft">
                                            <div class="issues box StageBox">
                                                <asp:HiddenField ID="hdnStageId2" ClientIDMode="Static" Value='<%# Eval("StageId2") %>'
                                                    runat="server" />
                                                    <asp:HiddenField ID="hdnLevelStr2" ClientIDMode="Static" Value='<%# Eval("LevelStr2") %>'
                                                    runat="server" />
                                                <div id="divStageName2">
                                                    <h3>
                                                        <asp:Label ID="lblStageName2" ClientIDMode="Static" runat="server" Text='<%# Eval("StageName2") %>'></asp:Label>
                                                    </h3>
                                                </div>
                                                <div id="divConditions2">
                                                    <b>Conditions : </b>
                                                    <asp:Label ID="lblConditions2" ClientIDMode="Static" runat="server" Text='<%# Eval("Conditions2") %>'></asp:Label>
                                                </div>
                                                <div id="divAssignedTo2">
                                                    <b>Assigned To : </b>
                                                    <asp:Label ID="lblAssignedTo2" ClientIDMode="Static" runat="server" Text='<%# Eval("AssignedTo2") %>'></asp:Label>
                                                </div>
                                                <table class="tblEditDelete" width="100%">
                                                    <tr>
                                                        <td width="50%" align="left">
                                                            <div id="divEdit2">
                                                                <asp:LinkButton ID="lnkEditStage2" OnClick="lnkEditStage_Click" OnClientClick="return setSelectedId(this,0);"
                                                                    CssClass="icon icon-edit" ClientIDMode="Static" runat="server">Edit</asp:LinkButton>
                                                            </div>
                                                        </td>
                                                        <td width="50%" align="right">
                                                            <div id="divDelete2">
                                                                <asp:LinkButton ID="lnkDeleteStage2" ClientIDMode="Static" CssClass="icon icon-del"
                                                                    OnClick="lnkDeleteStage_Click" OnClientClick="return setSelectedId(this,1);"
                                                                    runat="server">Delete</asp:LinkButton>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="width: 33.33%">
                                        <div class="splitcontentleft">
                                            <div class="issues box StageBox">
                                                <asp:HiddenField ID="hdnStageId3" ClientIDMode="Static" Value='<%# Eval("StageId3") %>'
                                                    runat="server" />
                                                     <asp:HiddenField ID="hdnLevelStr3" ClientIDMode="Static" Value='<%# Eval("LevelStr3") %>'
                                                    runat="server" />
                                                <div id="divStageName3">
                                                    <h3>
                                                        <asp:Label ID="lblStageName3" ClientIDMode="Static" runat="server" Text='<%# Eval("StageName3") %>'></asp:Label>
                                                    </h3>
                                                </div>
                                                <div id="divConditions3">
                                                    <b>Conditions : </b>
                                                    <asp:Label ID="lblConditions3" ClientIDMode="Static" runat="server" Text='<%# Eval("Conditions3") %>'></asp:Label>
                                                </div>
                                                <div id="divAssignedTo3">
                                                    <b>Assigned To : </b>
                                                    <asp:Label ID="lblAssignedTo3" ClientIDMode="Static" runat="server" Text='<%# Eval("AssignedTo3") %>'></asp:Label>
                                                </div>
                                                <table class="tblEditDelete" width="100%">
                                                    <tr>
                                                        <td width="50%" align="left">
                                                            <div id="divEdit3">
                                                                <asp:LinkButton ID="lnkEditStage3" OnClick="lnkEditStage_Click" OnClientClick="return setSelectedId(this,0);"
                                                                    CssClass="icon icon-edit" ClientIDMode="Static" runat="server">Edit</asp:LinkButton>
                                                            </div>
                                                        </td>
                                                        <td width="50%" align="right">
                                                            <div id="divDelete3">
                                                                <asp:LinkButton ID="lnkDeleteStage3" ClientIDMode="Static" CssClass="icon icon-del"
                                                                    OnClick="lnkDeleteStage_Click" OnClientClick="return setSelectedId(this,1);"
                                                                    runat="server">Delete</asp:LinkButton>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody> </table>
        </FooterTemplate>
    </asp:Repeater>

      <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back_Click" />
    <script type="text/javascript">
        function LoadWindow() {
           // location.reload();
        
        }

        function setSelectedId(obj, flg) {
      
            $('#hdnStageId').val('');
            $('#hdnLevelId').val('');
      
            var pId = $(obj).closest('table').parent().find("input[id*=hdnStageId]").val();
            var levelId = $(obj).closest('table').parent().find("input[id*=hdnLevelStr]").val();
            var rValue = false;
            if (flg > 0) {
                if (DeleteConfrim() == true && pId != '' && pId != '0' && pId != undefined) {
                    $('#hdnStageId').val(pId);
                    rValue = true;
                   
                }
                else {
                    rValue = false;
                }
            }
            else {
                if (pId == '' || pId == '0' || pId == undefined) {
                    rValue = false;
                }
                else {
                    $('#hdnStageId').val(pId);
                    $('#hdnLevelId').val(levelId);
                    rValue = true;
                }
            }
   
            return rValue;
        }


        function setLevelId(obj) {
            var pId = $(obj).closest('tr').find("#lblLevelStr").text();
            if (pId == '' || pId == '0') {
                return false;
            }
            else {
                $('#hdnLevelId').val(pId);
                return true;
            }
       
        }




        $(document).ready(function () {

            $('.stageTable').find('.splitcontentleft').each(function (t) {

                if ($(this).find('#hdnStageId1').val() == '0') {
                    $(this).hide();
                }

                if ($(this).find('#hdnStageId2').val() == '0') {
                    $(this).hide();
                }

                if ($(this).find('#hdnStageId3').val() == '0') {
                    $(this).hide();

                }

            });

        });


    </script>
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
</asp:Content>
