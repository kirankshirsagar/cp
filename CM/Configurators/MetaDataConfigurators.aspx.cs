﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BulkImportBLL;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;
using MetaDataConfiguratorsBLL;

public partial class Configurators_MetaDataConfigurators : System.Web.UI.Page
{
    IMetaDataConfigurators objCTFields;
    IMetaDataOption objMetaDataOption;
    string ViewTemplateFiledData = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        int MetaDataTypeIndex = Convert.ToInt16(Request.QueryString["MetaDataIndex"]);

        if (MetaDataTypeIndex == 1)
        {
            lblMetaDataType.Text = "Key Obligations";
        }
        else if (MetaDataTypeIndex == 2)
        {
            lblMetaDataType.Text = "Request Form";
        }
        else if (MetaDataTypeIndex == 3)
        {
            hdnIsFlag.Value =Convert.ToString(Request.QueryString["IsFlag"]);
            lblMetaDataType.Text = "Important Dates";
        }
        CreateObjects();
        FieldTypeBind();

        if (!IsPostBack)
            if (Request.QueryString["FieldTypeValue"] != null && Request.QueryString["FieldTypeValue"] != "undefined")
            {
                string FieldTypeValue = Request.QueryString["FieldTypeValue"];
                ddlFieldType.Value = FieldTypeValue;

                ReadMetaDataOptionDetails();
            }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        InsertUpdate();
        Page.TraceWrite("Save Update button clicked event ends.");
    }
    void FieldTypeBind()
    {
        int MetaDataTypeIndex = Convert.ToInt16(Request.QueryString["MetaDataIndex"]);
        Page.TraceWrite("FieldType dropdown bind starts.");
        try
        {
            if (MetaDataTypeIndex == 1)
            {
                objCTFields.MetaDataId = 1;
            }
            else if (MetaDataTypeIndex == 2)
            {
                objCTFields.MetaDataId = 2;
            }
            else if (MetaDataTypeIndex == 3)
            {
                objCTFields.MetaDataId = 3;
                try
                {
                    objCTFields.FieldID = Convert.ToInt16(Request.QueryString["MetaDataFieldId"]);
                }
                catch { }
                ddlEmailTemplate.extDataBind(objCTFields.SelectEmailTemplateData());
            }
            ddlFieldType.extDataBind(objCTFields.SelectFieldTypeData());
        }
        catch (Exception ex)
        {
            Page.TraceWarn("FieldType dropdown bind fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("FieldType dropdown bind ends.");
    }
    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objCTFields = FactoryMedaData.GetContractTemplateDetail();
            objMetaDataOption = FactoryMetaDataOption.GetContractTemplateDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }
    void ClearData(string status)
    {
        if (status == "1")
        {
            Page.TraceWrite("clearData starts.");
            hdnPrimeId.Value = "0";
            txtFiledName.Value = "";
            FieldTypeBind();
            Page.TraceWrite("clear Data ends.");
        }
    }

    void InsertUpdate(int flg = 0)
    {
        Page.TraceWrite("Insert, Update starts.");

        int AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        string AddedOn = DateTime.Now.Date.ToShortDateString();
        string IPAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;

        string status = "";

        int MetaDataTypeIndex = Convert.ToInt16(Request.QueryString["MetaDataIndex"]);
        if (MetaDataTypeIndex == 1)
        {
            objCTFields.MetaDataType = "Key Obligations";
            objCTFields.GetDataTableFilled(hdnData.Value, "Key Obligations", AddedBy, AddedOn, IPAddress);
        }
        else if (MetaDataTypeIndex == 2)
        {
            objCTFields.MetaDataType = "Request Form";
            objCTFields.GetDataTableFilled(hdnData.Value, "Request Form", AddedBy, AddedOn, IPAddress);
        }
        else if (MetaDataTypeIndex == 3)
        {
            objCTFields.MetaDataType = "Important Dates";
            objCTFields.EmailTemplateId = int.Parse(hdnEmailTemplateId.Value);
            objCTFields.GetDataTableFilled(hdnData.Value, "Important Dates", AddedBy, AddedOn, IPAddress);
        }
        if (Request.QueryString["MetaDataFieldId"] != null && Request.QueryString["MetaDataFieldId"] != "undefined")
        {
            objCTFields.FieldID = Convert.ToInt16(Request.QueryString["MetaDataFieldId"]);
        }
        else
        {
        }
        objMetaDataOption.OptionTypeText = hdnSignleSelectValues.Value;
        try
        {
            if (hdnData.Value.Trim() != string.Empty)
            {
                status = objCTFields.InsertRecord();
                hdnData.Value = "";
                Page.Message(status, hdnPrimeId.Value);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tytytt", "ReloadParent();", true);
            }
            if (hdnSignleSelectValues.Value.Trim() != string.Empty)
            {
                objMetaDataOption.InsertRecord();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tytytt", "ReloadParent();", true);
            }
        }
        catch (Exception ex)
        {
            Page.Message("0", hdnPrimeId.Value);
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        if (status == "1")
        {
            Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "1" : "0";
        }
        else if (status == "2")
        {
            Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "1" : "1";
        }
        else if (status == "3")
        {
            Page.Message("2", "0");
            Page.TraceWarn("Insert, Update fails.");
            status = "2";
            Session["status"] = "2";
        }

        Page.TraceWrite("Insert, Update ends.");
        ClearData(status);
    }

    protected void ReadMetaDataOptionDetails()
    {
        objMetaDataOption.FieldID = Convert.ToInt16(Request.QueryString["MetaDataFieldId"]);
        objMetaDataOption.ReadMetaDataOptionDetails();
        List<MetaDataOption> lstClauseField = new List<MetaDataOption>();
        List<MetaDataOption> lstClauseDetails = new List<MetaDataOption>();
        List<MetaDataOption> lstClauseValues = new List<MetaDataOption>();
        lstClauseDetails = objMetaDataOption.ReadClauseFieldDetails;
        hdnItems.Value = lstClauseDetails[0].OptionTypeText;
        string FieldName = lstClauseDetails[0].FieldName;
        txtFiledName.Value = FieldName;
        hdnEmailTemplateId.Value = lstClauseDetails[0].EmailTemplateId.ToString();
    }
}