﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BulkImportBLL;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;
using ImportExcel;
using System.IO;



public partial class BulkImport_BulkContractUpload : System.Web.UI.Page
{

    IContractTemplate objContractTemplate;
    IImportData objImport;

    public string Add;
    public string View;
    public string Update;
    public string Delete;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //btnUpload.UseSubmitBehavior = false;   
        //FileUpload1.Attributes.Add("onchange", "document.getElementById('" + btnUpload.ClientID + "').click();");
       
        CreateObjects();

        if (!IsPostBack)
        {
            ContractTypeBind();
            hdnPrimeId.Value = "0";
            lblTitle.Text = "Upload File";

            if (Session[Declarations.User] != null)
            {
                Access.PageAccess(this, Session[Declarations.User].ToString(), "bulkimport_");
                ViewState[Declarations.Add] = Access.Add.Trim();
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
            }
            AccessVisibility();
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objContractTemplate = FactoryBulkImport.GetContractTemplateDetail();
            objImport = FactoryBulkImport.GetImportDataDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void Back_Click(object sender, EventArgs e)
    {
        Server.Transfer("BulkImportUploadData.aspx");
    }

    void ContractTypeBind()
    {
        Page.TraceWrite("ContractType dropdown bind starts.");
        try
        {
            if (Session[Declarations.UserRole] != null)
                objContractTemplate.UserRole = Convert.ToString(Session[Declarations.UserRole]);  
            ddlContractTemplate.extDataBind(objContractTemplate.SelectData());
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ContractType dropdown bind fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ContractType dropdown bind ends.");
    }

    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        //if (Add == "N" && (hdnPrimeId.Value == "" || hdnPrimeId.Value == "0" || hdnPrimeId.Value == null))
        //{
        //    Page.extDisableControls();
        //    btnBack.Enabled = true;
        //}
        if (View == "N")
        {
            btnBack.Enabled = false;
        }
        Page.TraceWrite("AccessVisibility starts.");
    }
        
    void UploadFile()
    {
        if (FileUpload1.HasFile)
        {
            try
            {
                FileInfo fInfo = new FileInfo(FileUpload1.FileName);
                string fileExtention = fInfo.Extension.ToLower();

                if (fileExtention == ".xlsx")
                {
                    if (FileUpload1.PostedFile.ContentLength < 502400)
                    {
                        string filename = Path.GetFileName(FileUpload1.FileName).Replace(" ","-").extTimeStamp();
                        var serverPath = Server.MapPath("../Uploads/" + Session["TenantDIR"] + "/BulkUpload/" + filename);
                        FileUpload1.SaveAs(serverPath);
                        Insert(filename, serverPath);
                    }
                    else
                        StatusLabel.Text = "Upload status: The file has to be less than 500 mb!";
                }
                else
                    StatusLabel.Text = "Upload status: Only xlsx files are accepted!";
            }
            catch (Exception ex)
            {
                StatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
                //StatusLabel.Text = "Upload status: The file could not be uploaded.";

            }
        }
    }
    
    void Insert(string fileName, string filePath)
    {
        Page.TraceWrite("Insert, Update starts.");
        string status = "";
        objImport.ContractTemplateId = int.Parse(ddlContractTemplate.Value);
        objImport.InputFileName = fileName;
        objImport.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objImport.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
        DataTable dt = ReadFileIntoDataTable.ConvertHTMLTablesToDataTable(filePath);
        objImport.BulkImportStaging = objImport.FillStagingTable(dt);
        Session["IsFirstCall"] = "1";
        try
        {
            if (objImport.BulkImportStaging.Rows.Count > 1)
            {
                status = objImport.InsertRecord();
            }
            else if (objImport.BulkImportStaging.Rows.Count == 1)
            {
                status = "4";
            }
            else
            {
                status = "3";
            }
            Page.Message(status, hdnPrimeId.Value);
        }
        catch (Exception ex)
        {
            Page.Message("0", hdnPrimeId.Value);
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        if (status == "1")
        {
            Session[Declarations.Message] = "1";
            Response.Redirect("BulkImportUploadData.aspx");
        }
        Page.TraceWrite("Insert, Update ends.");
        
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        UploadFile();
    }
}










