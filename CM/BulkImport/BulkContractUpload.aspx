﻿<%@ Page Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true" CodeFile="BulkContractUpload.aspx.cs"
    Inherits="BulkImport_BulkContractUpload" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
<script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    

    <script type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");
    </script>
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
    </h2>
    <div class="box tabular">
        <p>
            <label for="time_entry_issue_id" style="width: 200px; padding-right: 5px">
                <span class="required">*</span>Bulk Import Template name</label>
            <select id="ddlContractTemplate" clientidmode="Static" class="chzn-select required chzn-select" runat="server"
                style="width: 225px;">
                <option></option>
            </select>
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id" style="width: 200px; padding-right: 5px">
                <span class="required">*</span>Upload File</label>
            <asp:FileUpload ID="FileUpload1" ClientIDMode="Static" runat="server" class="btn_validate" />
            <br />
            <asp:Label runat="server" ClientIDMode="Static" ID="StatusLabel" Text="" ForeColor="Red" />
            <em></em>
        </p>
    </div>
    <br />
    <asp:Button ID="btnUpload" ClientIDMode="Static" runat="server" Text="Upload" OnClick="btnUpload_Click" CssClass="btn_validate" />
       <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back_Click" />
 


    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">

        $("#sidebar").html($("#rightlinks").html());

        $('#FileUpload1').click(function () {
            $('#StatusLabel').html('');
        });

        $('#btnUpload').click(function () {

            var fileName = $('#FileUpload1').val().replace(/^.*[\\\/]/, '');
            var arrlen = fileName.split('.').length;
            var fileType = fileName.split('.')[arrlen - 1];

            if (fileName == '' && $('#ddlContractTemplate').val() > 0) {
                MessageMasterDiv('Please select a valid file.', 1, 500);
                return false;
            }
            else if (fileType != 'xls' && fileType != 'xlsx' && $('#ddlContractTemplate').val() > 0) {
                MessageMasterDiv(' Only xls or xlsx files are accepted.', 1, 500);
                return false;
            }

            if ($('#ddlContractTemplate').val() > 0) {

                ShowProgress();
            }

        });


    </script>
</asp:Content>
