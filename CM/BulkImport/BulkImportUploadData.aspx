﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/CM.master"
    AutoEventWireup="true" CodeFile="BulkImportUploadData.aspx.cs" Inherits="BulkImport_BulkImportUploadData" %>

<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridSelect.js" type="text/javascript"></script>
    <script src="../CommonScripts/GridNavigation.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");
    </script>
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <h2>
        Bulk Import Upload</h2>
    <input id="hdnPrimeIds" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <input id="Hidden1" runat="server" clientidmode="Static" type="hidden" />
    <div style="margin: 0; padding: 0; display: inline">
        <div id="query_form_content" class="hide-when-print">
            <fieldset id="filters" class="collapsible">
                <legend onclick="toggleFieldset(this);">Filters</legend>
                <div style="">
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td>
                                    <table id="filters-table" width="100%" cellpadding="3" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td width="10%">
                                                    Keywords :
                                                </td>
                                                <td width="90%">
                                                    <input id="txtSearch" clientidmode="Static" runat="server" type="text" maxlength="50"
                                                        onkeydown="return (event.keyCode!=13);" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </fieldset>
        </div>
        <p class="buttons hide-when-print">
            <asp:LinkButton ID="btnSearch" runat="server" OnClientClick="return searchClick();"
                CssClass="icon icon-checked" OnClick="btnSearch_Click">Filter</asp:LinkButton>
            <asp:LinkButton ID="btnShowAll" runat="server" OnClientClick="return resetClick();"
                CssClass="icon icon-reload" OnClick="btnSearch_Click">Reset Filter</asp:LinkButton>
            <asp:LinkButton ID="btnAddRecord" CssClass="icon icon-add" runat="server" OnClientClick="resetPrimeID();"
                OnClick="btnAddRecord_Click">Upload new file</asp:LinkButton>
             <asp:LinkButton ID="btnManualAdd" CssClass="icon icon-add" runat="server" OnClientClick="resetPrimeID();"
                OnClick="btnManualAdd_Click">Manual Entry</asp:LinkButton>


        </p>
        <div class="autoscroll">
            <table width="100%">
                <asp:Repeater ID="rptContractTemplateMaster" runat="server">
                    <HeaderTemplate>
                        <table id="masterDataTable" class="masterTable list issues" width="100%">
                            <thead>
                                <tr>
                                    <th width="15%">
                                    <asp:LinkButton ID="btnUplodedOn" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Uploaded On</asp:LinkButton></th>
                                    <th width="20%">
                                        <asp:LinkButton ID="btnSortContractTemplateName" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Bulk Import Template</asp:LinkButton>
                                    </th>
                                    <th width="20%">
                                        <asp:LinkButton ID="btnSortContractType" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Contract Type</asp:LinkButton>
                                    </th>
                                   <%-- <th width="15%">
                                        <asp:LinkButton ID="btnFileName" OnClick="btnSort_Click" CssClass="sort desc" runat="server">File Name</asp:LinkButton>
                                    </th>--%>
                                    <th width="10%">
                                        <asp:LinkButton ID="btnTotalCount" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Total Count</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="btnValidCount" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Valid Count</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="btnInvalidCount" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Invalid Count</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="btnSortStatus" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Status</asp:LinkButton>
                                    </th>
                                    <th width="5%">
                                     <asp:LinkButton ID="btnErrorLog" OnClientClick="return false;" runat="server"> Error Log</asp:LinkButton>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="display: none">
                                <asp:Label ID="lblBulkImportId" runat="server" ClientIDMode="Static" Text='<%#Eval("BulkImportID") %>'></asp:Label>
                                <asp:Label ID="lblContractTemplateId" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTemplateId") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblUplodedOn" runat="server" ClientIDMode="Static" Text='<%#Eval("ModifiedOn") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblContractTemplateName" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTemplateName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblContractTypeName" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTypeName") %>'></asp:Label>
                            </td>
                          <%--  <td>
                                <asp:Label ID="lblFileName" runat="server" ClientIDMode="Static" Text='<%#Eval("FileName") %>'></asp:Label>
                            </td>--%>
                            <td>
                                <asp:Label ID="lblTotalCount" runat="server" ClientIDMode="Static" Text='<%#Eval("TotalCount") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblApprovedCount" runat="server" ClientIDMode="Static" Text='<%#Eval("ApprovedCount") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblRejectedCount" runat="server" ClientIDMode="Static" Text='<%#Eval("RejectedCount") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblStatus" runat="server" ClientIDMode="Static" Text='<%#Eval("Status") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkDownload" ClientIDMode="Static" runat="server" OnClick="lnkDownload_Click">Download</asp:LinkButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
            <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
        </div>
        
<asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back_Click" />
    </div>
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());


        $(document).ready(function () {

            $('#masterDataTable tbody').find('tr').each(function () {
            
                if ($(this).find('span[id^="lblStatus"]').html() == 'Imported' && $(this).find('span[id^="lblRejectedCount"]').html() !='0') {

                }
                else {
                    $(this).find('a[id^="lnkDownload"]').html('');
                }
            });
        });



        function callBulkImportProcessing() {
            var strType = "POST";
            var strContentType = "text/html; charset=utf-8";
            var strData = "{}";
            var strURL = '../Handlers/BulkImport.ashx';
            var strCatch = false;
            var strDataType = 'json';
            var strAsync = false;
            var objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
            var arr = objHandler.HandlerReturnedData;
        }

        $(window).load(function () {
            var a = $('#Hidden1').val(); 
            if (a == "1") {
                callBulkImportProcessing();
            }
        });


        $(document).ready(function () {
          
            RowColor('masterTable');
        });


    </script>
</asp:Content>
