﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BulkImportBLL;
using CommonBLL;
using System.IO;



public partial class BulkImport_BulkImportUploadData : System.Web.UI.Page
{
    // navigation//
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 10;
    // navigation//

    IUplodedData objUploaded;
    public string Add;
    public string View;
    public string Update;
    public string Delete;
    public bool DownloadErrorFile;
    public bool ManualEntry;
    DataGrid gvDetails = new DataGrid();


    protected void Page_Load(object sender, EventArgs e)
    {
       

        CreateObjects();
       
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            Access.PageAccess(this, Session[Declarations.User].ToString(), "bulkimport_");
            ViewState[Declarations.Add] = Access.Add.Trim();
            ViewState[Declarations.View] = Access.View.Trim();
            ViewState[Declarations.Delete] = Access.Delete.Trim();
            ViewState[Declarations.Update] = Access.Update.Trim();
            ViewState["DownloadErrorFile"] = Access.isCustomised(33);
            ViewState["ManualEntry"] = Access.isCustomised(34);
            
        }
        setAccessValues();
        if (!IsPostBack || hdnSearch.Value.Trim() != string.Empty)
        {
            Session[Declarations.SortControl] = null;

            if (Session["IsFirstCall"] != null)
            {
                Hidden1.Value = Session["IsFirstCall"].ToString();
                Session["IsFirstCall"] = "2";
            }
            ReadData(1, RecordsPerPage);
            Message();

        }
        else
        {
            SetNavigationButtonParameters();
        }
        AccessVisibility();
    }
    
    
    void Message()
    {
        if (Session[Declarations.Message] != null)
        {
            var flg = 0;
            //var Message = "File uploded successfully. It may take some time to upload the actual data.";
            var Message = "Upload Successful - Please refresh the webpage";
            Page.JavaScriptClientScriptBlock("msg", "PageWriteMessage('"+ Message +"','" + flg + "')");
        }

    }
    // navigation//

    #region Navigation
    void SetNavigationButtonParameters()
    {
        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }


    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
            AccessVisibility();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

    }
    #endregion

    // navigation//


    protected void Back_Click(object sender, EventArgs e)
    {
        //Server.Transfer("../Workflow/VaultData.aspx");
        Server.Transfer("BulkContractTemplateData.aspx");
    }


    protected void btnSort_Click(object sender, EventArgs e)
    {
        hdnSearch.Value = "";
        LinkButton clickBtn = (LinkButton)sender;
        Session.Add(Declarations.SortControl, clickBtn);
        ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
        AccessVisibility();
    }


    void ReadData(int pageNo, int recordsPerPage)
    {
        if (View == "Y")
        {
            Page.TraceWrite("ReadData starts.");
            try
            {
                objUploaded.PageNo = pageNo;
                objUploaded.RecordsPerPage = recordsPerPage;
                objUploaded.Search = txtSearch.Value.Trim();
                if (Session[Declarations.UserRole] != null)
                    objUploaded.UserRole = Convert.ToString(Session[Declarations.UserRole]);
                LinkButton btnSort = null;
                if (Session[Declarations.SortControl] != null && Session[Declarations.SortControl] != string.Empty)
                {
                    btnSort = (LinkButton)Session[Declarations.SortControl];
                    objUploaded.Direction = btnSort.CssClass.ToLower() == "sort desc" ? 1 : 0;
                    objUploaded.SortColumn = btnSort.Text.Trim();
                }
                rptContractTemplateMaster.DataSource = objUploaded.ReadData();
                rptContractTemplateMaster.DataBind();

                if (btnSort != null)
                {
                    rptContractTemplateMaster.ClassChange(btnSort);
                }
                Page.TraceWrite("ReadData ends.");
                ViewState[Declarations.TotalRecord] = objUploaded.TotalRecords;
                PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
                PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
                PaginationButtons1.RecordsPerPage = RecordsPerPage;
            }
            catch (Exception ex)
            {
                Page.TraceWarn("ReadData fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objUploaded = FactoryBulkImport.GetUplodedDataDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void lnkDownload_Click(object sender, EventArgs e)
    {
        //Label lblContractTemplateId = (Label)((LinkButton)sender).FindControl("lblContractTemplateId");
        Label lblStatus = (Label)((LinkButton)sender).FindControl("lblStatus");
        Label lblContractTemplateName = (Label)((LinkButton)sender).FindControl("lblContractTemplateName");
        Label lblContractTypeName = (Label)((LinkButton)sender).FindControl("lblContractTypeName");
        Label lblBulkImportId = (Label)((LinkButton)sender).FindControl("lblBulkImportId");

        if (lblStatus.Text.Trim() == "Pending")
        {
           
        }
        else if (lblStatus.Text.Trim() == "Imported")
        {
            IUplodedData obj = FactoryBulkImport.GetUplodedDataDetail();
            string fileName = lblContractTemplateName.Text + "_ErrorLog_" + lblBulkImportId.Text + ".xls";
            fileName = fileName.Replace(" ","-").extTimeStamp();
            obj.BulkImportID = int.Parse(lblBulkImportId.Text);
            //obj.GetDocument(fileName, Server.MapPath("../Uploads/" + Session["TenantDIR"] + "/BulkImport/"+ fileName), Response); 
            gvDetails.DataSource = obj.GetUplodedInvalidData();
            gvDetails.DataBind();
            ExportToExcell(fileName);
        }
    }

    void ExportToExcell(string excelFileName)
    {
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", excelFileName));
        Response.ContentType = "application/ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        gvDetails.AllowPaging = false;
        gvDetails.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }


    protected void btnAddRecord_Click(object sender, EventArgs e)
    {
        Server.Transfer("BulkContractUpload.aspx");
    }

    protected void btnManualAdd_Click(object sender, EventArgs e)
    {
        Server.Transfer("~/WorkFlow/AddManualContract.aspx");
    }

    
    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

        if (ViewState["DownloadErrorFile"] != null)
        {
            DownloadErrorFile = Convert.ToBoolean(ViewState["DownloadErrorFile"].ToString());
        }
        if (ViewState["ManualEntry"] != null)
        {
            ManualEntry = Convert.ToBoolean(ViewState["ManualEntry"].ToString());
        }


    }

    void AccessVisibility()
    {

        Page.TraceWrite("AccessVisibility starts.");
        if (View == "N")
        {
            Page.extDisableControls();


        }
        else
        {
            btnSearch.Enabled = true;
            txtSearch.Disabled = false;
        }

        if (Update == "N" || DownloadErrorFile == false)
        {
            foreach (RepeaterItem item in rptContractTemplateMaster.Items)
            {
                LinkButton lnkDownload = (LinkButton)item.FindControl("lnkDownload");
                if (lnkDownload != null && DownloadErrorFile == false)
                {
                    lnkDownload.Enabled = false;
                    lnkDownload.Visible = false;
                }
            }
        }

        if (Update == "N")
        {
            Page.extDisableControls();

            btnAddRecord.Enabled = false;
            btnAddRecord.Visible = false;
        }
        else
        {
            btnAddRecord.Enabled = true;
        }



        if (View == "Y")
        {
            txtSearch.Disabled = false;
            btnSearch.Enabled = true;
            btnShowAll.Enabled = true;
        }

        if (ManualEntry == false)
        {
            btnManualAdd.Enabled = false;
            btnManualAdd.Visible = false;
        }


        Page.TraceWrite("AccessVisibility starts.");
    }

}










