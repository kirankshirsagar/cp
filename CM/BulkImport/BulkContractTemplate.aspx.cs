﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BulkImportBLL;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;
using WorkflowBLL;


public partial class BulkImport_BulkContractTemplate : System.Web.UI.Page
{

    IContractTemplate objContractTemplate;
    MasterBLL.IContractType objContractType;
    public string Add;
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {
       
        CreateObjects();

        if (!IsPostBack)
        {
            
            ContractTypeBind();
            if (String.IsNullOrEmpty(Request.extValue("hdnPrimeIds")) != true)
            {
                hdnPrimeId.Value = Request.extValue("hdnPrimeIds");
                ReadData();
                lblTitle.Text = "Edit Bulk Import Template";
                btnSave.Text = "Update";
            }
            else
            {
                rdActive.Checked = true;
                hdnPrimeId.Value = "0";
                lblTitle.Text = "Add Bulk Import Template";
               

            }
            if (Session[Declarations.User] != null)
            {
                Access.PageAccess(this, Session[Declarations.User].ToString(), "bulkimport_");
                ViewState[Declarations.Add] = Access.Add.Trim();
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
            }
            AccessVisibility();
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objContractTemplate = FactoryBulkImport.GetContractTemplateDetail();
            objContractType = MasterBLL.FactoryMaster.GetContractTypeDetail();

        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        InsertUpdate();
        Page.TraceWrite("Save Update button clicked event ends.");
    }

    protected void SaveAndContinue_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save and Add more button clicked event starts.");
        InsertUpdate(1);
        Page.TraceWrite("Save and Add more button clicked event ends.");
    }

    protected void Back_Click(object sender, EventArgs e)
    {
        Server.Transfer("BulkContractTemplateData.aspx");
    }

    void ReadData()
    {
        Page.TraceWrite("ReadData starts.");
        try
        {
            objContractTemplate.ContractTemplateId = int.Parse(hdnPrimeId.Value);
            List<ContractTemplate> ContractTemplate = objContractTemplate.ReadData();
            txtContractTemplateName.Value = ContractTemplate[0].ContractTemplateName;
            txtDescription.Value = ContractTemplate[0].Description;
            ddlContractType.extSelectedValues(ContractTemplate[0].ContractTypeId.ToString(), ContractTemplate[0].ContractTypeName);
            btnSaveAndContinue.Visible = false;

            if (ContractTemplate[0].isActive == "Y")
            {
                rdActive.Checked = true;
            }
            else
            {
                rdInactive.Checked = true;
            }

        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData ends.");
    }

    void ContractTypeBind()
    {
        Page.TraceWrite("ContractType dropdown bind starts.");
        try
        {
            IContractRequest objcontractreq;
            objcontractreq = FactoryWorkflow.GetContractRequestDetails();
            objcontractreq.UserID = int.Parse(Session[Declarations.User].ToString());
            ddlContractType.extDataBind(objcontractreq.SelectContractType());
            
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ContractType dropdown bind fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ContractType dropdown bind ends.");
    }

    void InsertUpdate(int flg=0)
    {
        Page.TraceWrite("Insert, Update starts.");
        string status = "";
        string ContractTemplateId = "";
        string IsUsed = "";
        string ContractTemplateName = "";
        string ContractTypeName = "";

        objContractTemplate.ContractTemplateName = txtContractTemplateName.Value.Trim();
        objContractTemplate.Description = txtDescription.Value.Trim();
        objContractTemplate.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objContractTemplate.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objContractTemplate.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
        objContractTemplate.isActive = rdActive.Checked == true ? "Y" : "N";
        objContractTemplate.ContractTypeId = int.Parse(ddlContractType.Value);

        try
        {
            if (hdnPrimeId.Value == "0")
            {
                objContractTemplate.ContractTemplateId = 0;
                objContractTemplate.InsertRecord();
            }
            else
            {
                objContractTemplate.ContractTemplateId = int.Parse(hdnPrimeId.Value);
                objContractTemplate.UpdateRecord();
            }

            List<ContractTemplate> lst = objContractTemplate.ContractTemplateInformation;
            status = lst[0].Status.ToString();
            ContractTemplateId = lst[0].ContractTemplateId.ToString();
            IsUsed = lst[0].IsUsed.ToString();
            ContractTemplateName = lst[0].ContractTemplateName.ToString();
            ContractTypeName = lst[0].ContractTypeName.ToString();

            Page.Message(status, hdnPrimeId.Value);

        }
        catch (Exception ex)
        {
            Page.Message("0", hdnPrimeId.Value);
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Insert, Update ends.");
        if (flg == 0 && status == "1")
        {
            Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
            Response.Redirect("BulkContractTemplateData.aspx");
            //Server.Transfer("BulkContractTemplateData.aspx");
        }
        else if (flg == 1 && status == "1")
        {

            Server.Transfer("BulkContractTemplateFields.aspx?ContractTemplateId=" + ContractTemplateId
           + "&IsUsed=" + IsUsed + "&ContractTemplateName=" + ContractTemplateName +
              "&ContractTypeName=" + ContractTypeName
           );


            //Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
        }
        //Response.Redirect("ContractTemplateMaster.aspx");
        ClearData(status);
    }

    void ClearData(string status)
       {
            if (status == "1")
            {
                Page.TraceWrite("clearData starts.");
                objContractTemplate.ContractTemplateId = 0;
                hdnPrimeId.Value = "0";
                txtContractTemplateName.Value = "";
                txtDescription.Value = "";
                ContractTypeBind();
                Page.TraceWrite("clear Data ends.");
                rdInactive.Checked = false;
                rdActive.Checked = true;
            }
       }

    private void setAccessValues()
       {
           if (ViewState[Declarations.Add] != null)
           {
               Add = ViewState[Declarations.Add].ToString();
           }
           if (ViewState[Declarations.Update] != null)
           {
               Update = ViewState[Declarations.Update].ToString();
           }
           if (ViewState[Declarations.Delete] != null)
           {
               Delete = ViewState[Declarations.Delete].ToString();
           }
           if (ViewState[Declarations.View] != null)
           {
               View = ViewState[Declarations.View].ToString();
           }

       }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (Add == "N" && (hdnPrimeId.Value == "" || hdnPrimeId.Value == "0" || hdnPrimeId.Value == null))
        {
            Page.extDisableControls();
            btnBack.Enabled = true;
        }
        if (View == "N")
        {
            btnBack.Enabled = false;
        }
        Page.TraceWrite("AccessVisibility starts.");
    }


}










