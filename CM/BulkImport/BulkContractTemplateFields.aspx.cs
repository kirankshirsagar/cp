﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BulkImportBLL;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;


public partial class BulkImport_BulkContractTemplateFields : System.Web.UI.Page
{

    IContractTemplateField objCTFields;
    
    public string Add;
    public string View;
    public string Update;
    public string Delete;
   
    string IsUsed;
    string ContractTemplateName;
    string ViewTemplateFiledData=string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
       
        CreateObjects();
    
        if (!IsPostBack)
        {
            if (Request.QueryString["ContractTemplateId"] != null)
            {
                hdnContractTemplateId.Value = Request.QueryString["ContractTemplateId"].ToString();
                objCTFields.ContractTemplateId=Convert.ToInt32(hdnContractTemplateId.Value);
                ViewTemplateFiledData = objCTFields.GetContractTemplateFieldRecord();


                hdnReadFields.Value = ViewTemplateFiledData;
                if(hdnReadFields.Value !="")
                { btnSave.Style.Add("display", "block"); }
            }
            if (Request.QueryString["IsUsed"] != null)
            {
                IsUsed = Request.QueryString["IsUsed"].ToString();
            }
            if (Request.QueryString["ContractTemplateName"] != null)
            {
                ContractTemplateName = Request.QueryString["ContractTemplateName"].ToString();
                lblContractTemplate.Text = ContractTemplateName;
            }
            if (Request.QueryString["ContractTypeName"] != null)
            {
                lblContractType.Text = Request.QueryString["ContractTypeName"].ToString();
            }


            FieldTypeBind();
            MasterFieldBind();
            if (String.IsNullOrEmpty(Request.extValue("hdnPrimeIds")) != true)
            {
                hdnPrimeId.Value = Request.extValue("hdnPrimeIds");
                //ReadData();
                lblTitle.Text = "Edit Contract Template";
                btnSave.Text = "Update";
            }
            else
            {
                
                hdnPrimeId.Value = "0";
                lblTitle.Text = "Add Contract Template Fields";
               

            }
            if (Session[Declarations.User] != null)
            {
                Access.PageAccess(this, Session[Declarations.User].ToString(), "bulkimport_");
                ViewState[Declarations.Add] = Access.Add.Trim();
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
            }
            AccessVisibility();
           
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objCTFields = FactoryBulkImport.GetContractTemplateFieldDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        InsertUpdate();
        Page.TraceWrite("Save Update button clicked event ends.");
    }

  

    protected void Back_Click(object sender, EventArgs e)
    {
        Server.Transfer("BulkContractTemplateData.aspx");
    }

    /*
    void ReadData()
    {
        Page.TraceWrite("ReadData starts.");
        try
        {
            objCTFields.ContractTemplateId = int.Parse(hdnPrimeId.Value);
            List<ContractTemplate> ContractTemplate = objCTFields.ReadData();
            txtFiledName.Value = ContractTemplate[0].Description;
            ddlFieldType.extSelectedValues(ContractTemplate[0].ContractTypeId.ToString(), ContractTemplate[0].ContractTypeName);
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData ends.");
    }

    */

    

     void MasterFieldBind()
    {
        Page.TraceWrite("FieldType dropdown bind starts.");
        try
        {
            ddlMasterField.extDataBind(objCTFields.SelectMasterData(1));
        }
        catch (Exception ex)
        {
            Page.TraceWarn("FieldType dropdown bind fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("FieldType dropdown bind ends.");
    }

    void FieldTypeBind()
    {
        Page.TraceWrite("FieldType dropdown bind starts.");
        try
        {
            ddlFieldType.extDataBind(objCTFields.SelectFieldTypeData());
        }
        catch (Exception ex)
        {
            Page.TraceWarn("FieldType dropdown bind fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("FieldType dropdown bind ends.");
    }

    void InsertUpdate(int flg=0)
    {
        Page.TraceWrite("Insert, Update starts.");
        string status = "";
        objCTFields.ContractTemplateId = int.Parse(hdnContractTemplateId.Value);
        objCTFields.GetDataTableFilled(hdnData.Value);
        objCTFields.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objCTFields.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objCTFields.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;

        try
        {
            if (hdnData.Value.Trim() != string.Empty)
            {
                status = objCTFields.InsertRecord();
                hdnData.Value = "";
                Page.Message(status, hdnPrimeId.Value);
            }
        }
        catch (Exception ex)
        {
            Page.Message("0", hdnPrimeId.Value);
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Insert, Update ends.");
        if (flg == 0 && status == "1")
        {
            Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
            Response.Redirect("BulkContractTemplateData.aspx");
        }
        else if (flg == 1 && status == "1")
        {
            Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
        }
        ClearData(status);
    }

    void ClearData(string status)
       {
            if (status == "1")
            {
                Page.TraceWrite("clearData starts.");
                objCTFields.ContractTemplateId = 0;
                hdnPrimeId.Value = "0";
               
                txtFiledName.Value = "";
                FieldTypeBind();
                MasterFieldBind();
                Page.TraceWrite("clear Data ends.");
                
            }
       }

    private void setAccessValues()
       {
           if (ViewState[Declarations.Add] != null)
           {
               Add = ViewState[Declarations.Add].ToString();
           }
           if (ViewState[Declarations.Update] != null)
           {
               Update = ViewState[Declarations.Update].ToString();
           }
           if (ViewState[Declarations.Delete] != null)
           {
               Delete = ViewState[Declarations.Delete].ToString();
           }
           if (ViewState[Declarations.View] != null)
           {
               View = ViewState[Declarations.View].ToString();
           }

       }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (Add == "N" && (hdnPrimeId.Value == "" || hdnPrimeId.Value == "0" || hdnPrimeId.Value == null))
        {
            Page.extDisableControls();
            btnBack.Enabled = true;
        }
        if (View == "N")
        {
            btnBack.Enabled = false;
        }
        Page.TraceWrite("AccessVisibility starts.");
    }


}










