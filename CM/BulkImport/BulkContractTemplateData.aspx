﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/CM.master"
    AutoEventWireup="true" CodeFile="BulkContractTemplateData.aspx.cs" Inherits="BulkImport_ContractTemplateData" %>

<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridSelect.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");

    </script>
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <h2>
        Bulk Import Template</h2>
    <input id="hdnPrimeIds" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <div style="margin: 0; padding: 0; display: inline">
        <div id="query_form_content" class="hide-when-print">
            <fieldset id="filters" class="collapsible">
                <legend onclick="toggleFieldset(this);">Filters</legend>
                <div style="">
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td>
                                    <table id="filters-table" width="100%" cellpadding="3" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td width="10%">
                                                    Keywords :
                                                </td>
                                                <td width="30%">
                                                    <input id="txtSearch" clientidmode="Static" runat="server" type="text" maxlength="50"
                                                        onkeydown="return (event.keyCode!=13);" />
                                                </td>

                                                 <td width="60%" style="text-align:right;color:Red">
                                                   Note : Please save the downloaded template file (.xls)  in latest (.xlsx) format to work.
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </fieldset>
        </div>
        <p class="buttons hide-when-print">
            <asp:LinkButton ID="btnSearch" runat="server" OnClientClick="return searchClick();"
                CssClass="icon icon-checked" OnClick="btnSearch_Click">Filter</asp:LinkButton>
            <asp:LinkButton ID="btnShowAll" runat="server" OnClientClick="return resetClick();"
                CssClass="icon icon-reload" OnClick="btnSearch_Click">Reset Filter</asp:LinkButton>
            <asp:LinkButton ID="btnAddRecord" CssClass="icon icon-add" runat="server" OnClientClick="resetPrimeID();"
                OnClick="btnAddRecord_Click">Add new Bulk Import Template</asp:LinkButton>
            <asp:LinkButton ID="btnDelete" CssClass="icon icon-del" runat="server" OnClientClick="return GetSelectedItems('D');"
                OnClick="btnDelete_Click" Text="Add new ContractType">Delete</asp:LinkButton>

            <asp:LinkButton ID="btnBulkImportUpload" CssClass="icon icon-add" runat="server"
                OnClick="btnBulkImportUpload_Click">Bulk Import Upload</asp:LinkButton>
        </p>
        <div class="autoscroll">
            <table width="100%">
                <asp:Repeater ID="rptContractTemplateMaster" runat="server">
                    <HeaderTemplate>
                        <table id="masterDataTable" class="masterTable list issues" width="100%">
                            <thead>
                                <tr>
                                    <th style="text-align: left; padding-left: 2px;" width="1%">
                                        <input id="chkSelectAll" type="checkbox" title="Check all/Uncheck all" class="maincheckbox"
                                            onkeydown="return (event.keyCode!=13);" />
                                    </th>
                                    <th class="checkbox hide-when-print" width="3%">
                                        &nbsp;
                                    </th>
                                    <th width="0%" style="display: none;">
                                        Contract Template Id
                                    </th>
                                    <th width="25%">
                                        <asp:LinkButton ID="btnSortContractTemplateName" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Bulk Import Template</asp:LinkButton>
                                    </th>
                                    <th width="25%">
                                        <asp:LinkButton ID="btnSortContractType" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Contract Type</asp:LinkButton>
                                    </th>
                                    <th width="25%">
                                        <asp:LinkButton ID="btnSortDecription" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Description</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="btnSortStatus" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Status</asp:LinkButton>
                                    </th>
                                    <th width="11%">
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: left; padding-bottom: 2px">
                                <input type="checkbox" onchange="SetMainCheckbox(this);" class="mid-margin-left"
                                    onkeydown="return (event.keyCode!=13);" />
                            </td>
                            <td style="text-align: left; padding-bottom: 2px">
                                <img id="imgLock" alt="img.." class="mws-tooltip-e" title="Used" border="0" />
                            </td>
                            <td style="display: none">
                                <asp:Label ID="lblContractTemplateId" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTemplateId") %>'></asp:Label>
                                <asp:Label ID="lblIsUsed" runat="server" ClientIDMode="Static" Text='<%#Eval("isUsed") %>'></asp:Label>
                                <asp:Label ID="lblContractTemplateName" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTemplateName") %>'></asp:Label>
                                <asp:Label ID="lblisEditable" runat="server" ClientIDMode="Static" Text='<%#Eval("isEditable") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:LinkButton ID="imgEdit" runat="server" OnClientClick="return setSelectedId(this);"
                                    OnClick="imgEdit_Click"><%#Eval("ContractTemplateName")%></asp:LinkButton>
                            </td>
                            <td>
                                <asp:Label ID="lblContractTypeName" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTypeName") %>'></asp:Label>
                            </td>
                            <td style="word-break: break-all; word-wrap: break-word">
                                <asp:Label ID="lbldecsription" runat="server" ClientIDMode="Static" Text='<%#Eval("Description").ToString().Replace("\n", "<br>") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblStatus" runat="server" ClientIDMode="Static" Text='<%#Eval("isActive") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkEdit" CssClass="clsEditTemp" ClientIDMode="Static" runat="server" OnClick="lnkEdit_Click"
                                    ToolTip="Edit">Edit</asp:LinkButton>
                                &nbsp;&nbsp;
                                <asp:LinkButton ID="lnkDownload" ClientIDMode="Static" runat="server" OnClick="lnkDownload_Click">Download</asp:LinkButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
            <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
        </div>
        <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back_Click" style="display:none" />


        <table style="display: none" width="100%">
            <tr>
                <td align="left" width="10%">
                </td>
                <td runat="server" id="actinacttd" style="padding-right: 60%" class="labelfont" align="left"
                    width="90%">
                    <input id="rdActive" name="ActiveInactive" runat="server" type="radio" />Active
                    <input id="rdInactive" name="ActiveInactive" runat="server" type="radio" />Inactive
                    <asp:Button ID="btnChangeStatus" runat="server" Text="Status" OnClientClick="return GetSelectedItems('S');"
                        OnClick="btnChangeStatus_Click" />
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">

        $('.clsEditTemp').click(function () {
            
            if ($(this).closest('tr').find('#lblisEditable').text() == 'N') {
                alert('Can not modified this template fields.');
                return false;
            }
            else {
                return true;
            }
        });



        function GetSelectedItems(flg) {
            $('#hdnPrimeIds').val('');
            $('#hdnUsedNames').val('');
            var tableClass = 'masterTable';
            var deleteLabelId = 'lblContractTemplateId';
            var deleteLabelName = 'lblContractTemplateName';
            var objCheck = new CkeckBoxSelect(tableClass, deleteLabelId, deleteLabelName);
            var deletedIds = objCheck.DeletedIds;
            var usedNames = objCheck.UsedNames;

            if (deletedIds == '') {
                MessageMasterDiv('Please select record(s).');
                return false;
            }
            else if (usedNames != '' && flg == 'D') {
                MessageMasterDiv('Some records can not be deleted.' + usedNames, 1);
                return false;
            }
            if (flg == 'D') {
                if (DeleteConfrim() == true) {
                    $('#hdnPrimeIds').val(deletedIds);
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                $('#hdnPrimeIds').val(deletedIds);
                return true;
            }

            $('#hdnPrimeIds').val(deletedIds);
            //            $('#hdnUsedNames').val(usedNames);
            return true;
        }


        $(document).ready(function () {
            defaultTableValueSetting('lblContractTemplateId', 'MstContractTemplate', 'ContractTemplateId');
            LockUnLockImage('masterTable');
        });


        function setSelectedId(obj) {
            var pId = $(obj).closest('tr').find('#lblContractTemplateId').text();
            if (pId == '' || pId == '0') {
                return false;
            }
            else {
                $('#hdnPrimeIds').val(pId);
                return true;
            }
        }


    </script>
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());


        $(document).ready(function () {

            $('#masterDataTable tbody').find('tr').each(function () {


               // alert($(this).find('span[id^="lblContractTemplateId"]').html());
                if ($(this).find('span[id^="lblContractTemplateId"]').html() == "5030") 
                {
                   // $(this).find('a[id^="lnkEdit"]').hide();
                }

                if ($(this).find('span[id^="lblIsUsed"]').html() == 'Y') {

                }
                else {
                   // $(this).find('a[id^="lnkDownload"]').html('Add Fields');
                }
            });


        });
    
    
    </script>
</asp:Content>
