﻿<%@ Page Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true" CodeFile="BulkContractTemplateFields.aspx.cs"
    Inherits="BulkImport_BulkContractTemplateFields" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");
    </script>
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <input id="hdnData" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnFieldNameValue" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnFieldNameText" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnReadFields" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnContractTemplateId" runat="server" clientidmode="Static" type="hidden" />
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
    </h2>
    <div class="box tabular">
        <p>
            <label for="time_entry_issue_id" style="margin-top:0px;">
                Contract Type
            </label>
            <asp:Label ID="lblContractType" runat="server" Text=""></asp:Label>
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id" style="margin-top:0px;">
                Contract Template
            </label>
            <asp:Label ID="lblContractTemplate" runat="server" Text=""></asp:Label>
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id" style="margin-top:0px;">
                <span class="required">*</span>Field Type name</label>
            <select id="ddlFieldType" clientidmode="Static" class="chzn-select required chzn-select"
                runat="server" style="width: 225px;">
                <option></option>
            </select>
            <em></em>
        </p>
        <p>
            <span id="spanFieldName">
                <label for="time_entry_issue_id" style="margin-top:0px;">
                    <span class="required">*</span>Field Name</label>
                <input id="txtFiledName" clientidmode="Static" runat="server" type="text" class="required Name "
                    maxlength="50" style="width: 220px;" />
            </span><em></em>
        </p>
        <p>
            <span id="spanMasterField" style="width: 100%">
                <label for="time_entry_issue_id">
                    <span class="required">*</span>Select Master Field</label>
                <select id="ddlMasterField" clientidmode="Static" class="chzn-select required chzn-select"
                    runat="server" style="width: 225px;">
                    <option></option>
                </select>
            </span><em></em>
        </p>
        <input id="btnAddMoreFiled" clientidmode="Static" type="button" value="Add" />
       
    </div>
    <table id="tblData" class="list issues" width="80%">
        <tr>
            <%--<th width="10%">
            </th>--%>
            <th id="thFieldType" width="40%">
                <asp:Label ID="lblFieldType" ClientIDMode="Static" runat="server" Text="Field Type"></asp:Label>
            </th>
            <th id="thFieldName" width="40%">
                <asp:Label ID="lblFieldName" ClientIDMode="Static" runat="server" Text="Field Name"></asp:Label>
            </th>
            <th id="thDelete" width="5%">
            </th>
            <th id="thUpDown" width="5%">
            </th>
        </tr>
    </table>
    <table>
    <tr>
    <td>
    <asp:Button ID="btnSave" ClientIDMode="Static" runat="server" OnClientClick="return getData();"
        Text="Save" OnClick="btnSave_Click" />
    </td> 
    <td>   
         <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back_Click" />
         </td>
    </tr>
    </table>
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">


        $('#txtFiledName').on('keypress', function (e) {
            var ingnore_key_codes = [34, 39];
            if ($.inArray(e.which, ingnore_key_codes) >= 0) {
                return false;
            }
        });



        $("#sidebar").html($("#rightlinks").html());

        $('#ddlFieldType').change(function () {

            if ($('#ddlFieldType').val() == 9) {
                $('#spanMasterField').show().find('div[id^="ddlMaster"]').attr('style', 'width:225px');
                //$('#spanFieldName').hide();
            }
            else {
                $('#spanMasterField').hide();
                $('#spanFieldName').show();
                $('#txtFiledName').focus();
            }

        });

        $(document).ready(function () {
            $('#spanMasterField').hide();
        })



        function hideTable(flg) {

            if (flg == 1 && $('#tblData tbody').find('tr').length <= 1) {
                if ($('#hdnReadFields').val() != '') {
                    $('#tblData').show();
                    $('#btnSave').show();
                }
                else {
                    $('#tblData').hide();
                    $('#btnSave').hide();
                }
            }
            else {
                $('#tblData').show();
                $('#btnSave').show();
            }

        }


        $('#btnAddMoreFiled').click(function () {

            if ($('#ddlFieldType').val() == 0) {
                alert('Please select field type.');
                $('#ddlFieldType').focus();
                return false;

            }

            if ($('#txtFiledName').val() == '') {
                alert('Please enter value.');
                $('#txtFiledName').focus().val('');
                return false;
            }

            var fieldNameValue = "";
            var fieldTypeValue = $('#ddlFieldType').val();
            var fieldTypeText = $("#ddlFieldType option:selected").text();
            var fieldNameText = '';

            if ($('#ddlFieldType').val() == 9) {
                fieldNameValue = $('#ddlMasterField').val();
                fieldNameText = $('#txtFiledName').val() + '_' + $("#ddlMasterField option:selected").text();
            }
            else {
                fieldNameValue = $('#txtFiledName').val();
                fieldNameText = fieldNameValue;
            }

            if ($('#txtFiledName').val() == '') {
                fieldTypeValue = '';
            }


            var rowData;

            rowData = "<tr>"
            //            + "<td width='10%'></td>"
            + "<td width='40%'><span class='clsFiledTypeText'>" + fieldTypeText + "</span><span style='display:none' class='clsFieldTypeValue'>" + fieldTypeValue + "</span> </td>"
            + "<td width='40%'><a href='#' class='clsFieldNameText'>" + fieldNameText + "</a> <span style='display:none' class='clsFieldNameValue'>" + fieldNameValue + "</span>  </td>"
            + "<td width='10%'><a href='#' class='btnDelete' clientidmode='static'>Delete</a></td>"
            + "<td width='10%'>&nbsp;<a href='#'class='up'>Up</a>&nbsp;&nbsp;<a href='#' class='down'>Down</a></td>"
            "</tr>";


            //            if (fieldNameValue != '' && checkExists(fieldNameValue) == true) {
            if (fieldNameValue != '' && checkExists(fieldNameText) == true) {
                if ($('#hdnFieldNameText').val() != '') {
                    replaceTr(fieldTypeText, fieldTypeValue, fieldNameText, fieldNameValue)
                }
                else {
                    $('#tblData tbody').append(rowData);
                }
                $('#txtFiledName').val('').focus();
            }

            else if (fieldNameValue == '') {
                alert('Please enter value.');
                $('#txtFiledName').focus().val('');
                return false;
            }
            else {
                alert('This value already exists.');
                $('#txtFiledName').focus().val('');
                return false;
            }

            hideTable(0);            
            $('#ddlFieldType').val('0');
            $('#ddlFieldType').trigger('liszt:updated');
        });


        function checkExists(val) {
           
            var rvalue = true;

            $('#tblData tbody').find('a[class="clsFieldNameText"]').each(function () {

                if ($('#hdnFieldNameText').val() != '' && $('#hdnFieldNameText').val() == val) {
                    rvalue = true;
                }
                else if (val.toLowerCase() == $(this).html().toLowerCase()) {
                    rvalue = false;
                }
            });
            return rvalue;
        }


        function replaceTr(fieldTypeText, fieldTypeValue, fieldNameText, fieldNameValue) {
            $('#tblData tbody').find('tr').each(function () {

                if ($(this).find('a[class="clsFieldNameText"]').html() == $('#hdnFieldNameText').val()) {
                    $(this).find('span[class="clsFiledTypeText"]').html(fieldTypeText);
                    $(this).find('span[class="clsFieldTypeValue"]').html(fieldTypeValue);
                    $(this).find('a[class="clsFieldNameText"]').html(fieldNameText);
                    $(this).find('span[class="clsFieldNameValue"]').html(fieldNameValue);
                }
            });
            $('#hdnFieldNameText').val('');
        }


        $('.btnDelete').live('click', function () {
            var par = $(this).parent().parent();
            par.remove();
            hideTable(1);

        });


        $(document).ready(function () {
            $(".up,.down").live('click', function () {
                var $row = $(this).parents('tr:first');
                if ($(this).is(".up")) {
                    if ($row.index() > 1) {
                        $row.insertBefore($row.prev());
                    }
                } else {
                    $row.insertAfter($row.next());
                }
            });


            $('.clsFieldNameText').live('click', function () {


                var fieldType = $(this).closest('tr').find('span[class="clsFieldTypeValue"]').html();
                var fieldValue = $(this).closest('tr').find('span[class="clsFieldNameValue"]').html();
                var fieldText = $(this).closest('tr').find('a[class="clsFieldNameText"]').html();

                $('#ddlFieldType').val(fieldType);
                updateSettings('#ddlFieldType');
                $('#ddlFieldType').change();
                if (fieldType == 9) {
                    $('#ddlMasterField').val(fieldValue);
                    updateSettings('#ddlMasterField');
                    $('#txtFiledName').val(fieldText.split('_')[0]);
                }
                else {
                    $('#txtFiledName').val(fieldValue);
                }

                $('#hdnFieldNameText').val(fieldText);

                return false;
            });




        });


        function getData() {
            var data = '';
            var cnt = 0;
            var vFieldType = '';
            var vFieldName = '';

            $('#tblData tbody').find('tr').each(function () {

                if (cnt > 0) {
                    vFieldType = $(this).find('span[class="clsFieldTypeValue"]').html();
                    vFieldName = $(this).find('span[class="clsFieldNameValue"]').html() + '$' + $(this).find('a[class="clsFieldNameText"]').html();

                    if (cnt == 1) {
                        data = vFieldType + ',' + vFieldName;
                    }
                    else {
                        data = data + '#' + vFieldType + ',' + vFieldName;
                    }
                }

                cnt = cnt + 1;
            });
            $('#hdnData').val(data);

            if (data == '') {
                alert('Please add fields.');
                return false;
            }
            else {

                return true;
            }
        }





        hideTable(1);



        function readRow(fieldTypeText, fieldTypeValue, fieldNameText, fieldNameValue) {
            var rowData;
            rowData = "<tr>"
//            + "<td width='10%'></td>"
            + "<td width='40%'><span class='clsFiledTypeText'>" + fieldTypeText + "</span><span style='display:none' class='clsFieldTypeValue'>" + fieldTypeValue + "</span> </td>"
            + "<td width='40%'><a href='#' class='clsFieldNameText'>" + fieldNameText + "</a> <span style='display:none' class='clsFieldNameValue'>" + fieldNameValue + "</span>  </td>"
            + "<td width='10%'><a href='#' class='btnDelete' clientidmode='static'>Delete</a></td>"
            + "<td width='10%'>&nbsp;<a href='#'class='up'>Up</a>&nbsp;&nbsp;<a href='#' class='down'>Down</a></td>"
            "</tr>";
            $('#tblData tbody').append(rowData);
        }


        function readData() {
            var fields = $('#hdnReadFields').val();
            var arr = fields.split(',');
            var i = 0;
            for (var i = 0; i < arr.length; i++) {
                readRow(arr[i].split('#')[0], arr[i].split('#')[2], arr[i].split('#')[1], arr[i].split('#')[3]);
            }
            $('#tblData').show();
        }

        $(window).load(function () {
            if ($('#hdnReadFields').val() != '') {
                readData();
                ("#btnSave").show();
            }
        });
        



    </script>
</asp:Content>
