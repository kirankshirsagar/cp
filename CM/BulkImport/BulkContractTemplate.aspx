﻿<%@ Page Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true" CodeFile="BulkContractTemplate.aspx.cs"
    Inherits="BulkImport_BulkContractTemplate" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");
    </script>
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
    </h2>
    <div class="box tabular">
        <p>
            <label for="time_entry_issue_id" style="width: 200px; padding-right: 5px">
                <span class="required">*</span>Bulk Import Template name</label>
            <input id="txtContractTemplateName" runat="server" type="text" class="required Name "
                maxlength="50" style="width: 220px;" />
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id" style="width: 200px; padding-right: 5px">
                <span class="required">*</span>Contract Type name</label>
            <select id="ddlContractType" class="chzn-select required chzn-select" runat="server"
                style="width: 225px;">
                <option></option>
            </select>
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id" style="width: 200px; padding-right: 5px">
                Description</label>
            <textarea id="txtDescription" placeholder="Bulk Import Template description" runat="server"
                maxlength="100" clientidmode="static" type="text" autocomplete="false" style="width: 220px;
                height: 100px" rows="10" />
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id" style="width: 200px; padding-right: 5px">
                Status</label>
            <input id="rdActive" name="ActiveInactive" runat="server" type="radio" class="required" />Active
            <input id="rdInactive" name="ActiveInactive" runat="server" type="radio" class="required" />Inactive
            <em></em>
        </p>
    </div>
    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" class="btn_validate" />
    <asp:Button ID="btnSaveAndContinue" runat="server" Text="Save and Continue" OnClick="SaveAndContinue_Click"
        class="btn_validate" />
    <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back_Click" />
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">

        $("#sidebar").html($("#rightlinks").html());

      
    </script>
</asp:Content>
