﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="User_Login" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login </title>
    <meta name="keywords" content="issue,bug,tracker" />
    
    <script src="scripts/jquery-1.7.2.min.js" type="text/javascript"></script>
    <link href="Styles/chosen.css" rel="stylesheet" type="text/css" />
    <script src="../scripts/chosen.jquery.js" type="text/javascript"></script>
    <script src="../CommonScripts/dropdownlist.js" type="text/javascript"></script>
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/mastervalidations.js" type="text/javascript"></script>
    <script src="../JQueryValidations/DynamicEffects.js" type="text/javascript"></script>
    <script src="../Scripts/application.js" type="text/javascript"></script>
    <script src="../Scripts/select_list_move.js" type="text/javascript"></script>
    <script src="../Scripts/context_menu.js" type="text/javascript"></script>
    <link href="Styles/jquery-ui-1.8.21.css" media="all" rel="stylesheet" type="text/css"/>
    <link href="Styles/menu.css" media="all" rel="stylesheet" type="text/css"/>
    <link href="Styles/application.css" media="all" rel="stylesheet" type="text/css" />
    <link href="Styles/stylesheet.css" media="screen" rel="stylesheet" type="text/css"/>
    <link href="Styles/context_menu.css" media="screen" rel="stylesheet" type="text/css"/>
    <script src="../CommonScripts/Messages.js" type="text/javascript"></script>


    <script language="javascript" type="text/javascript">
        $(document).ready(function () {

            $("#LoginButton").css("margin-left", "0px");
            $("#tblLogin").css("margin-top", "88px");
            function ErrorDiv(msg) {

                $(div[id ^= "divMsg"]).addClass('flash notice').show();
                $(div[id ^= "divMsg"]).html(msg);
            }
        });
    </script>
</head>
<body style="color:transparent;background:url(images/Login_Background.jpg)  no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;">
    
  
    <%--<div id="wrapper">
        <div id="wrapper2">           
            <div id="main" class="nosidebar">
                <div id="sidebar">
                </div>
                <div id="content"  style="background-color:transparent;" >--%>
                    <div id="login-form">
                        <form accept-charset="UTF-8" action="#" method="post" runat="server">
                        <asp:HiddenField ID="hdnPassword" ClientIDMode="Static" runat="server" />
                        <div style="margin: 0; padding: 0; display: inline">
                            <input name="utf8" type="hidden" value="✓" /><input name="authenticity_token" type="hidden"
                                value="aGPayDGC4ESIcRD8fjUqLPGgj9l1t9XD+BE6iTs7Fgw=" /></div>
                        <input name="back_url" type="hidden" value="http://192.168.168.1:82/redmine/" />
                        <div id="errorExplanation" runat="server">
                        </div>
                        <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" CssClass="flash error"
                            DisplayMode="List" ValidationGroup="LoginUserValidationGroup" ForeColor="#880000" />
                                             
                            <asp:Login ID="LoginUser" runat="server" EnableViewState="false" RenderOuterTable="false"
                                DestinationPageUrl="~/WorkFlow/Home.aspx">
                                <LayoutTemplate>
                                    <table id="tblLogin" cellpadding="0px" cellspacing="0px" class="login-card">
                                        <tr>
                                            <td style="width:100%; height:130px; ">
                                                <asp:Image ID="imgLogo" runat="server" ImageUrl="~/Images/ContractPodai-Logo-Dark.png" ClientIDMode="Static" Width="276px" Height="57px" />
                                            </td>                                           
                                        </tr>
                                        <tr>                                           
                                            <td>
                                                <asp:TextBox ID="UserName" runat="server" MaxLength="50" CssClass="textEntry" ClientIDMode="Static"  placeholder="Username" ></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                                    CssClass="failureNotification" ErrorMessage="User name is required." ToolTip="User name is required."
                                                    ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="Password" MaxLength="50" runat="server" CssClass="passwordEntry" placeholder="Password"
                                                    TextMode="Password"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                                    CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required."
                                                    ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center;margin:0px;">
                                                <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Log In" ValidationGroup="LoginUserValidationGroup"
                                                    ClientIDMode="Static" OnClick="LoginButton_Click" />
                                                <span id="nothing" title="" class="failureNotification1" style="visibility:hidden;">*</span>
                                            </td>                                        
                                        </tr>
                                        <tr>                                          
                                            <td style="width:100%; height:25px;">
                                                <asp:CheckBox ID="RememberMe" runat="server" />
                                                <span for="autologin">
                                                    Remember me
                                                </span>
                                                 &nbsp;
                                                 <span style="color:Black;">•</span>
                                                &nbsp;
                                                <a href="UserManagement/passwordforgot.aspx">Forgot Password</a>
                                            </td>
                                        </tr>                                                
                                    </table>
                                </LayoutTemplate>
                            </asp:Login>                          
                         
                        <script type="text/javascript">
                            $('#UserName').focus();
                        </script>
                        </form>
                    </div>
                    <div style="clear: both;">
                    </div>
                <%--</div>--%>
            
            <div id="ajax-indicator" style="display: none;">
                <span>Loading...</span></div>
            <div id="ajax-modal" style="display: none;">
            </div>
            <div id="footer" style="display:none">
                <div class="bgl">
                    <div class="bgr">
                        Powered by <a href="http://www.uberall.in/" target="_blank">Uberall Solutions (I) Ltd.</a>
                        and <a href="http://www.newgalexy.com/" target="_blank">NewGalexy</a> © 2014-2015<br />
                        <a href="customer/index.htm">Customer Portal</a>
                    </div>
                </div>
            </div>
     <%--   </div>
    </div>--%>
    <script type="text/javascript">

        $('#DivLoginError').click(function () {
            hideShowDiv(0);
        });

        function hideShowDiv(flg) {
            if (flg == 0) {
                $('#DivLoginError').hide();
            }
            else {
                $('#DivLoginError').show();
            }
            return false;
        }

        $(window).load(function () {
        });

        $(document).ready(function () {

            $('#UserName').focus();
            $("#LoginButton").css("margin-left", "0px");
            $('#LoginButton').click(function () {

                var fl = 0;
                $("#errorExplanation").html('').attr('style', 'display:none;');
                $('#lblloginStatus').html('');

                var EditItems = "";
                var type1 = 'IsActive';
                var strType = "POST";
                var strContentType = "text/html; charset=utf-8";
                var strData = "{}";
                var strURL = 'Handlers/User.ashx?Type=' + type1 + '&Email=' + $('#UserName').val() + "&UserName=" + $('#UserName').val();
                var strCatch = false;
                var strDataType = 'json';
                var strAsync = false;
                var objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
                EditItems = objHandler.HandlerReturnedData;
                //$('#lblloginStatus').text(EditItems);
                if (EditItems != "" && EditItems != undefined)
                    $(div[id ^= "divMsg"]).html(EditItems);

            });
        });

      
    </script>

     
</body>
</html>
