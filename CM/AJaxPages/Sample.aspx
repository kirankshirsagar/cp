﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Sample.aspx.cs" Inherits="AJaxPages_Sample" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../scripts/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    <script src="../CommonScripts/dropdownlist.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Button ID="Button1" runat="server" Text="Button" onclick="Button1_Click" />
        <select id="ddlCountry" onchange="JQSelectBind('ddlCountry','ddlState', 'MstState'); 
                  JQSelectBind('ddlState','ddlCity', 'MstCity');" clientidmode="Static" runat="server">
            <option value="1">India</option>
            <option value="2">Pakistan</option>
        </select>
        <select id="ddlState" onchange="JQSelectBind('ddlState','ddlCity', 'MstCity');" clientidmode="Static"
            runat="server">
        </select>
        <select id="ddlCity" clientidmode="Static" runat="server">
        </select>
        <a id="a" runat="server">hi</a>

       


        <script type="text/javascript">

            $(document).ready(function () {
                $('#ddlCountry').change();
                $('#ddlState').val('1');
                $('#ddlState').change();
                $('#ddlCity').val('1');

            });
            

        </script>


    </div>
    </form>
</body>
</html>
