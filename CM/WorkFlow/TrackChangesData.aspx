﻿<%@ Page Language="C#" MasterPageFile="~/CM.master" ClientIDMode="Static" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="TrackChangesData.aspx.cs" Inherits="Negotiation_TrackChangesData" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>

    

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    <script src="../JQueryValidations/mastervalidations.js" type="text/javascript"></script>
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />    
    <link href="../Styles/Font.css" rel="stylesheet" type="text/css" />    
    <link href="../Styles/application.css" rel="stylesheet" type="text/css" />  

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">



    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>

    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>


    <script type="text/javascript">
        $("#requestLink").addClass("menulink");
        $("#requesttab").addClass("selectedtab");
    </script>

    

    <script type="text/javascript">




        $(document).ready(function () {


            var IsDocumentNotValid = $("#hdnHideActionButtons").val();
            if (IsDocumentNotValid == "Y") {
                $(".ActionColumn").hide();
                $("#btnSaveUp").hide();
                $("#btnSaveDown").hide();
               
                $("#dvMesssageForActions").html("Accept - Reject feature has disabled because comparing documents contain multiple sections.");
            }
            else {
                $("#dvMesssageForActions").html("Note : New document which will get generated after accepting and rejecting changes may not have same formatting (e.g. Font, Size) like original document.");        
            }


            var lSelectedTab = $("#hdnCurrentSelectedTab").val();
            if (lSelectedTab.toString() == "Terms") {

                $("#tab-Terms").addClass("selected")
                $("#tab-content-Terms").css("display", "block");

                $("#tab-Clauses").removeClass("selected")
                $("#tab-content-Clauses").css("display", "none");

            }
            else if (lSelectedTab.toString() == "Clauses") {

                $("#tab-Clauses").addClass("selected")
                $("#tab-content-Clauses").css("display", "block");

                $("#tab-Terms").removeClass("selected")
                $("#tab-content-Terms").css("display", "none");

            }
            else if (lSelectedTab.toString() == "Lines") {

                $("#tab-Lines").addClass("selected")
                $("#tab-content-Lines").css("display", "block");

                $("#tab-Terms").removeClass("selected")
                $("#tab-content-Terms").css("display", "none");

            }

            var lOldVersion = $("#hdnOldFileVersion").val();
            //document.getElementById('lblFSortOldVersion').innerText = "Version #" + lOldVersion;

            var lLatestVersion = $("#hdnLatestFileVersion").val();
            //document.getElementById('lblFSortNewVersion').innerText = "Version #" + lLatestVersion;
            //document.getElementById('lblCSortVersion').innerText = "Version #" + lLatestVersion;

            //document.getElementById('lblLSortVersion').innerText = "Version #" + lLatestVersion;




            var lhdnTermsRecordCount = $("#hdnTermsRecordCount").val();
            var lhdnClausesRecordCount = $("#hdnClausesRecordCount").val();
            var lhdnLinesRecordCount = $("#hdnLinesRecordCount").val();

            if (lhdnClausesRecordCount == "0" && lhdnTermsRecordCount == "0" && lhdnLinesRecordCount == "0") {
                $("#btnSaveUp").hide();
                $("#btnSaveDown").hide();
            }


        });
    </script>


    <script type="text/javascript">
        function setHiddenFieldValue(value) {
            $("#hdnCurrentSelectedTab").val(value);
        }
    </script>

    <script type="text/javascript">
        function scrollToAnchor(aid) {
            var aTag = $(aid);
            $('html,body').animate({ scrollTop: aTag.offset().top }, 'slow');
        }
    </script>


    <script type="text/javascript">

       
        function ValidationMessage() {

            $('.lRequired').each(function (i, lspan) {

                var isChecked = false;

                $(this).find("input:radio").each(function (index, chk) {
                    if ($(chk).attr("checked") == "checked") {
                        isChecked = true;
                        return false;
                    }
                });

                if (!isChecked) {

                    $("#dvMesssage").css("display", "block");
                    $("#dvMesssage").html("Please select all Accept-Reject decisions for all paragraph changes");

                    setTimeout(function () {
                        $('#dvMesssage').hide();
                    }, 3000);

                    evt.preventDefault();
                }
            });
        }


    </script>

    <script type="text/javascript">
        $('.submit').live("click", function () {
            ShowProgress();
        });        
    </script>

    <h2>
        Track Changes
    </h2>

    <input id="hdnHideActionButtons" runat="server" clientidmode="Static" type="hidden" value="N" />
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnCurrentSelectedTab" runat="server" clientidmode="Static" type="hidden"
        value="Terms" />
    <input id="hdnContractFileOldId" runat="server" clientidmode="Static" type="hidden"
        value="0" />
    <input id="hdnContractFileNewId" runat="server" clientidmode="Static" type="hidden"
        value="0" />
    <input id="hdnOldFileVersion" runat="server" clientidmode="Static" type="hidden"
        value="0" />
    <input id="hdnLatestFileVersion" runat="server" clientidmode="Static" type="hidden"
        value="0" />
    <input id="hdnOldFilePath" runat="server" clientidmode="Static" type="hidden" value="" />
    <input id="hdnLatestFilePath" runat="server" clientidmode="Static" type="hidden"
        value="" />
    <input id="hdnNextVersionFileName" runat="server" clientidmode="Static" type="hidden"
        value="" />
    <input id="hdnRequestID" runat="server" clientidmode="Static" type="hidden" value="0" />

    <input id="hdnTermsRecordCount" runat="server" clientidmode="Static" type="hidden" value="0" />
    <input id="hdnClausesRecordCount" runat="server" clientidmode="Static" type="hidden" value="0" />

    <input id="hdnLinesRecordCount" runat="server" clientidmode="Static" type="hidden" value="0" />


    <div id="dvMesssage" class="flash error" style="display:none">    
    </div>  
    <div id="dvMesssageForActions" class="flash" style=" padding: 4px 4px 4px 30px; margin-bottom: 12px;  font-size: 1.1em;  border: 2px solid;  background-color: #ffe3e3;  border-color: #dd0000;  color: #880000; ">    
    </div>  

    <div class="issue status-1 priority-3 child created-by-me details">
       <p>
            <asp:LinkButton ID="lnkBtnRequestFlow" OnClick="lnkBtnRequestFlow_Click" CssClass="issue status-1 priority-4 parent" runat="server">Request Number</asp:LinkButton>:
            <span id="MainContent_lblSubTitle">Track changes</span>
       </p>           
       <br />
        <table class="attributes">
            <tbody>
                <tr>
                    <th class="status" style="width:15%">
                        Contract Type
                    </th>
                    <td class="status" style="width:50%">
                        <asp:Label ID="lblContractType" runat="server"></asp:Label>
                    </td>
                    <th class="start-date" style="width:15% ;text-align:right" >
                        Contract ID :
                    </th>
                    <td class="start-date" style="width:20%">
                        <asp:Label ID="lblContractID" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <th class="category" style="width:15%">
                        <asp:Label ID="lblVersionNewDoc" runat="server">Latest Document :</asp:Label>
                    </th>
                    <td class="category" colspan="3" style="width:85%">
                        <asp:Label ID="lblNewVersionDocName" runat="server"> </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th class="category" style="width:15%">
                        <asp:Label ID="lblVersionPrevDoc" runat="server">Previous Document :</asp:Label>
                    </th>
                    <td class="category" colspan="3" style="width:85%">
                        <asp:Label ID="lblPrevVersionDocName" runat="server"> </asp:Label>
                    </td>
                </tr>
            </tbody>
        </table>
         <br />
    </div>

    <asp:Button ID="btnSaveUp" runat="server" Text="Save" class="submit btn_validate " OnClientClick="ValidationMessage()"
        OnClick="btnSave_Click" />
    <br /><br />
    <div class="tabs" style="display:none" >
        <ul>
            <%--<li><a href="#" class="selected" id="tab-Terms" onclick="showTab('Terms'); setHiddenFieldValue('Terms'); this.blur(); return false;">
                Terms</a></li>

            <li><a href="#" id="tab-Clauses" onclick="showTab('Clauses'); setHiddenFieldValue('Clauses');this.blur(); return false;">
                Clauses</a></li>--%>

                 <li><a href="#" id="A1" onclick="showTab('Lines'); setHiddenFieldValue('Lines');this.blur(); return false;">
                Paragraph</a></li>
        </ul>
    </div>
   <%-- <div class="tab-content" id="tab-content-Terms" style="display: block;">
        <div class="autoscroll">
            <table width="100%">
                <asp:Repeater ID="rptTrackFieldChanges" runat="server">
                    <HeaderTemplate>
                        <table id="tblTrackFieldChanges" class="masterTable list issues" width="100%">
                            <thead>
                                <tr>
                                    <th width="0%" style="display: none;">
                                        TrackChangesFieldID
                                    </th>
                                    <th width="0%" style="display: none;">
                                        ContractFileOldId
                                    </th>
                                    <th width="0%" style="display: none;">
                                        ContractFileNewId
                                    </th>
                                    <th width="0%" style="display: none;">
                                        FieldLibraryID
                                    </th>
                                    <th width="0%" style="display: none;">
                                        BookmarkName
                                    </th>
                                    <th width="10%">
                                        Action
                                    </th>
                                    <th width="20%">
                                        Field Name
                                    </th>
                                    <th width="25%">
                                        <asp:Label ID="lblFSortOldVersion" runat="server" ClientIDMode="Static" Text="Old Version"></asp:Label>
                                    </th>
                                    <th width="25%">
                                        <asp:Label ID="lblFSortNewVersion" runat="server" ClientIDMode="Static" Text="New Version"></asp:Label>
                                    </th>
                                    <th width="20%">
                                    </th>
                                    <tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="Display">
                            <td style="display: none">
                                <asp:Label ID="lblFTrackChangesFieldID" runat="server" ClientIDMode="Static" Text='<%#Eval("TrackChangesFieldID") %>'></asp:Label>
                            </td>
                            <td style="display: none">
                                <asp:Label ID="lblFContractFileOldId" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractFileOldId") %>'></asp:Label>
                            </td>
                            <td style="display: none">
                                <asp:Label ID="lblFContractFileNewId" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractFileNewId") %>'></asp:Label>
                            </td>
                            <td style="display: none">
                                <asp:Label ID="lblFFieldLibraryID" runat="server" ClientIDMode="Static" Text='<%#Eval("FieldLibraryID") %>'></asp:Label>
                            </td>
                            <td style="display: none">
                                <asp:Label ID="lblFFieldBookmarkName" runat="server" ClientIDMode="Static" Text='<%#Eval("BookmarkName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFFieldAction" runat="server" ClientIDMode="Static" Text='<%#Eval("Action") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFFieldName" runat="server" ClientIDMode="Static" Text='<%#Eval("FieldName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFFieldOldValue" runat="server" ClientIDMode="Static" Text='<%#Eval("OldFileValue") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFFieldNewValue" runat="server" ClientIDMode="Static" Text='<%#Eval("NewFileValue") %>'></asp:Label>
                                <a id='btnFModifyValue' title="Click for edit." class='icon icon-edit' href='#' onclick="ModifyFieldValue(this);scrollToAnchor(this);">
                                </a>

                                <input id='txtFFieldNewValue' runat='server' style="display: none; width: 100%" clientidmode='Static'
                                    type='Text' />
                                <input id="hdnFIsModifiedAgain" runat="server" clientidmode="Static" type="hidden"
                                    value="N" />
                            </td>
                            <td>
                                <span id="spanRdoTerms" class="radio required lRequired" style="color: Black;">
                                    <input id="rdoFAccept" runat="server" type="radio" />Accept
                                    <input id="rdoFReject" runat="server" type="radio" />Reject <em></em></span>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
            <asp:PlaceHolder runat="server" ID="Placeholder1">
                <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
            </asp:PlaceHolder>
        </div>
    </div>

    <div class="tab-content" id="tab-content-Clauses" style="display: block;">
        <div class="autoscroll">
            <table width="100%">
                <asp:Repeater ID="rptTrackClauseChanges" runat="server">
                    <HeaderTemplate>
                        <table id="tblTrackClauseChanges" class="masterTable list issues" width="100%">
                            <thead>
                                <tr>
                                    <th width="0%" style="display: none;">
                                        TrackChangesClauseID
                                    </th>
                                    <th width="0%" style="display: none;">
                                        ContractFileOldId
                                    </th>
                                    <th width="0%" style="display: none;">
                                        ContractFileNewId
                                    </th>
                                    <th width="0%" style="display: none;">
                                        BookmarkName
                                    </th>
                                    <th width="15%">
                                        Action
                                    </th>
                                    <th width="50%">
                                        Clause Name
                                    </th>
                                    <th width="15%">
                                        <asp:Label ID="lblCSortVersion" runat="server" ClientIDMode="Static" Text="Version"></asp:Label>
                                    </th>
                                    <th width="20%">
                                    </th>
                                    <tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="display: none">
                                <asp:Label ID="lblCTrackChangesClauseID" runat="server" ClientIDMode="Static" Text='<%#Eval("TrackChangesClauseID") %>'></asp:Label>
                            </td>
                            <td style="display: none">
                                <asp:Label ID="lblCContractFileOldId" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractFileOldId") %>'></asp:Label>
                                <input id="hdnClauseOldValue" runat="server" clientidmode="Static" type="hidden"
                                    value='<%#Eval("OldFileValue") %>' />
                            </td>
                            <td style="display: none">
                                <asp:Label ID="lblCContractFileNewId" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractFileNewId") %>'></asp:Label>
                                <input id="hdnClauseNewValueModified" runat="server" clientidmode="Static" type="hidden"
                                    value='<%#Eval("NewFileValue") %>' />
                            </td>
                            <td style="display: none">
                                <asp:Label ID="lblCClauseBookmarkName" runat="server" ClientIDMode="Static" Text='<%#Eval("BookmarkName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCAction" runat="server" ClientIDMode="Static" Text='<%#Eval("Action") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCClause" runat="server" ClientIDMode="Static" Text='<%#Eval("ClauseName") %>'></asp:Label>
                            </td>
                            <td>
                                <a href="#" class="downlnk" onclick="clauseShowChanges(this);scrollToAnchor(this)">View
                                    Changes
                                    <img src="../images/sort_desc.png" alt="" /></a> <a href="#" class="uplnk" style="display: none;"
                                        onclick="clauseHideChanges(this);scrollToAnchor(this)">Hide Changes
                                        <img src="../images/sort_asc.png" alt="" /></a>
                                <input id="hdnIsClauseChangesShown" runat="server" clientidmode="Static" type="hidden"
                                    value="N" />
                            </td>
                            <td>
                                <span id="spanRdoClause" class="radio required lRequired" style="color: Black;">
                                    <input id="rdoCAccept" runat="server" type="radio" />Accept
                                    <input id="rdoCReject" runat="server" type="radio" />Reject                                    
                                    <em></em></span>
                                    <input id="hdnCIsModifiedAgain" runat="server" clientidmode="Static" type="hidden"
                                        value="N" />
                            </td>
                        </tr>
                        <tr class="trClauseSecondRow">
                            <td class="tdClauseSecondRow" colspan="7">
                                <div class="divClauseSecondRow">
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
            <asp:PlaceHolder runat="server" ID="Placeholder2">
                <uc1:PaginationButtons ID="PaginationButtons2" runat="server" />
            </asp:PlaceHolder>
        </div>
    </div>--%>


   
   <div  id="tab-content-Lines" style="display: block;">
        <div class="autoscroll">
            <table width="100%">
                <asp:Repeater ID="rptTrackLineChanges" runat="server">
                    <HeaderTemplate>
                        <table id="tblTrackLineChanges" class="masterTable list issues" width="100%">
                            <thead>
                                <tr>

                                <th width="0%" style="display: none;">
                                        TrackChangesID
                                    </th>   

                                    <th width="0%" style="display: none;">
                                        Paragraph No.
                                    </th>                                    

                                    <th width="0%" style="display: none;">
                                        ContractFileOldId
                                    </th>
                                    <th width="0%" style="display: none;">
                                        ContractFileNewId
                                    </th>
                                    <th width="0%" style="display: none;">
                                        BookmarkName
                                    </th>
                                    <th width="10%">
                                        Change
                                    </th>
                                    <th width="50%">
                                        Paragraph
                                    </th>
                                    <th width="15%">
                                        <asp:Label ID="lblLSortVersion" runat="server" ClientIDMode="Static" Text="View/Hide Changes"></asp:Label>
                                    </th>
                                    <th width="20%" class="ActionColumn">
                                    Action
                                    </th>
                                    <tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="display: none">                            
                                <asp:Label ID="lblLTrackChangesID" runat="server"   ClientIDMode="Static" Text='<%#Eval("TrackChangesID") %>'></asp:Label>                              
                            </td>

                            <td style="display: none;">                            
                               <asp:Label ID="lblLTrackChangesLineNo" runat="server" ClientIDMode="Static" Text='<%#Eval("LineNo") %>'></asp:Label>
                            </td>

                            <td style="display: none">
                                <asp:Label ID="lblLContractFileOldId" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractFileOldId") %>'></asp:Label>
                                <input id="hdnLineOldValue" runat="server" clientidmode="Static" type="hidden"
                                    value='<%#Eval("OldFileValue") %>' />
                            </td>
                            <td style="display: none">
                                <asp:Label ID="lblLContractFileNewId" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractFileNewId") %>'></asp:Label>
                                <input id="hdnLineNewValueModified" runat="server" clientidmode="Static" type="hidden"
                                    value='<%#Eval("NewFileValueToEdit") %>' />
                            </td>
                            <td style="display: none">
                                <asp:Label ID="lblLLineBookmarkName" runat="server" ClientIDMode="Static" Text='<%#Eval("BookmarkName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblLAction" runat="server" ClientIDMode="Static" Text='<%#Eval("Action") %>'></asp:Label>
                                <input id="hdnStatus" runat="server" clientidmode="Static" type="hidden"
                                    value='<%#Eval("StatusFlag") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblLLine"  runat="server" ClientIDMode="Static" Text='<%#Eval("OriginalLineText") %>'></asp:Label>
                            </td>
                            <td>
                                <a href="#" class="downlnk" onclick="LineShowChanges(this);scrollToAnchor(this)">View
                                    Changes
                                    <img src="../images/sort_desc.png" alt="" /></a> <a href="#" class="uplnk" style="display: none;"
                                        onclick="LineHideChanges(this);scrollToAnchor(this)">Hide Changes
                                        <img src="../images/sort_asc.png" alt="" /></a>
                                <input id="hdnIsLineChangesShown" runat="server" clientidmode="Static" type="hidden"
                                    value="N" />
                            </td>

                            <td class="ActionColumn">
                                <span id="spanRdoLine" class="radio required lRequired" style="color: Black;">
                                    <input id="rdoCAccept" runat="server" type="radio" />Accept
                                    <input id="rdoCReject" runat="server" type="radio" />Reject                                    
                                    <em></em></span>
                                    <input id="hdnLIsModifiedAgain" runat="server" clientidmode="Static" type="hidden"
                                        value="N" />
                            </td>

                        </tr>
                        <tr class="trLineSecondRow" style="display:none">
                            <td class="tdLineSecondRow" colspan="7">
                                <div class="divLineSecondRow">
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
            <asp:PlaceHolder runat="server" ID="Placeholder3" >
                <uc1:PaginationButtons ID="PaginationButtons3" runat="server"  />
            </asp:PlaceHolder>
        </div>
    </div>
    <br />
    <asp:Button ID="btnSaveDown" runat="server" Text="Save" class="submit btn_validate" OnClientClick="ValidationMessage()" 
        OnClick="btnSave_Click" />
    <asp:Button ID="btnBack" runat="server" Text="Back" class="submit" OnClick="lnkBtnRequestFlow_Click" />

    <script type="text/javascript">

        function ModifyFieldValue(obj) {

            $(obj).closest('tr').find("#txtFFieldNewValue").val($(obj).closest('tr').find("#lblFFieldNewValue").text());
            $(obj).closest('tr').find("#txtFFieldNewValue").show();
            $(obj).closest('tr').find("#lblFFieldNewValue").hide();
            $(obj).closest('tr').find("#btnFModifyValue").hide();
            $(obj).closest('tr').find("#hdnFIsModifiedAgain").val('Y');

        }

        function ModifyClauseValue(obj) {

            $(obj).closest('tr').find("#txtClauseNewValue").val($(obj).closest('tr').find("#lblClauseNewValue").html().replace(/<br>/g, '\n').replace(/&nbsp;/g, "\t"));
            var lNewvalue = $(obj).closest('tr').find("#lblClauseNewValue").html().replace(/<br>/g, '\n');
            lNewvalue = lNewvalue.replace(/&nbsp;/g, "\t");
            lNewvalue = lNewvalue.replace(/<font color="red">/g, "");
            lNewvalue = lNewvalue.replace(/<\/font>/g, "");
            $(obj).closest('tr').find("#txtClauseNewValue").val(lNewvalue);
            $(obj).closest('tr').find("#txtClauseNewValue").show();
            $(obj).closest('tr').find("#lblClauseNewValue").hide();
            $(obj).closest('tr').find("#btnCModifyValue").hide();
            $('textarea').autogrow();
            $(obj).closest('.trClauseSecondRow').prev('tr').find('td').find("#hdnCIsModifiedAgain").val('Y');

        }





        function ModifyLineValue(obj) {

            $(obj).closest('tr').find("#txtLineNewValue").val($(obj).closest('tr').find("#lblLineNewValueToEdit").html().replace(/<br>/g, '\n').replace(/&nbsp;/g, "\t"));
            var lNewvalue = $(obj).closest('tr').find("#lblLineNewValueToEdit").html().replace(/<br>/g, '\n');
            lNewvalue = lNewvalue.replace(/&nbsp;/g, "\t");
            lNewvalue = lNewvalue.replace(/<font color="red">/g, "");
            lNewvalue = lNewvalue.replace(/<font color="green">/g, "");
            lNewvalue = lNewvalue.replace(/<font color="blue">/g, "");
            lNewvalue = lNewvalue.replace(/<\/font>/g, "");

            $(obj).closest('tr').find("#txtLineNewValue").closest('td').find('br').remove();

            $(obj).closest('tr').find("#txtLineNewValue").val(lNewvalue);
            $(obj).closest('tr').find("#txtLineNewValue").show();
            $(obj).closest('tr').find("#lblLineNewValueToEdit").hide();
            $(obj).closest('tr').find("#lblLineNewValue").hide();
            $(obj).closest('tr').find("#btnLModifyValue").hide();
            $('textarea').autogrow();
            $(obj).closest('.trLineSecondRow').prev('tr').find('td').find("#hdnLIsModifiedAgain").val('Y');

        }








        function fieldShowChanges(obj) {

            $(obj).closest('tr').next('tr').show();
            var lTrackChangesFieldID = $(obj).closest('tr').find('td').find('#lblFTrackChangesFieldID').html();

            $(obj).hide();
            $(obj).closest('td').find(".uplnk").show();

            var IsAlreadyShown = $(obj).closest('td').find("#hdnIsFieldChangesShown").val();

            if (IsAlreadyShown == 'N') {

                $.ajax({
                    type: "POST",
                    contentType: "text/html; charset=utf-8",
                    data: "{}",
                    url: '../Handlers/TrackDocumentChanges.ashx?ID=' + lTrackChangesFieldID.toString() + '&Entity=Term',
                    cache: false,
                    dataType: "html",
                    success: function (data) {
                        $(obj).closest('tr').next('tr').find('td').find('div').html(data);
                        $(obj).closest('td').find("#hdnIsFieldChangesShown").val('Y')

                    }
                });
            }
        }

        function fieldHideChanges(obj) {

            $(obj).closest('tr').next('tr').hide();
            $(obj).hide();
            $(obj).closest('td').find(".downlnk").show();
        }


        function clauseShowChanges(obj) {

            $(obj).closest('tr').next('tr').show();
            var lTrackChangesFieldID = $(obj).closest('tr').find('td').find('#lblCTrackChangesClauseID').html();

            $(obj).hide();
            $(obj).closest('td').find(".uplnk").show();

            var IsAlreadyShown = $(obj).closest('td').find("#hdnIsClauseChangesShown").val();

            if (IsAlreadyShown == 'N') {

                $.ajax({
                    type: "POST",
                    contentType: "text/html; charset=utf-8",
                    data: "{}",
                    url: '../Handlers/TrackDocumentChanges.ashx?ID=' + lTrackChangesFieldID.toString() + '&Entity=Clause',
                    cache: false,
                    dataType: "html",
                    success: function (data) {
                        $(obj).closest('tr').next('tr').find('td').find('div').html(data);
                        $(obj).closest('td').find("#hdnIsClauseChangesShown").val('Y');

                    }
                });
            }
        }

        function clauseHideChanges(obj) {

            $(obj).closest('tr').next('tr').hide();
            $(obj).hide();
            $(obj).closest('td').find(".downlnk").show();
        }







        function LineShowChanges(obj) {

            debugger;

           
            $(obj).closest('tr').next('tr').show();
            var lTrackChangesID = $(obj).closest('tr').find('td').find('#lblLTrackChangesID').html();

            $(obj).hide();
            $(obj).closest('td').find(".uplnk").show();

            var IsAlreadyShown = $(obj).closest('td').find("#hdnIsLineChangesShown").val();

            if (IsAlreadyShown == 'N') {

                $.ajax({
                    type: "POST",
                    contentType: "text/html; charset=utf-8",
                    data: "{}",
                    url: '../Handlers/TrackDocumentChanges.ashx?ID=' + lTrackChangesID.toString() + '&Entity=Line',
                    cache: false,
                    dataType: "html",
                    success: function (data) {
                        $(obj).closest('tr').next('tr').find('td').find('div').html(data);
                        $(obj).closest('td').find("#hdnIsLineChangesShown").val('Y');

                    }
                });
            }
        }

        function LineHideChanges(obj) {

            $(obj).closest('tr').next('tr').hide();
            $(obj).hide();
            $(obj).closest('td').find(".downlnk").show();
        }



        function SetNewClauseValueInParentTD(obj) {
            var lNewVal = $(obj).val();
            $(obj).closest('.trClauseSecondRow').prev('tr').find('td').find("#hdnClauseNewValueModified").val(lNewVal);
           // alert($(obj).closest('.trClauseSecondRow').prev('tr').find('td').find("#hdnClauseNewValueModified").val());
        }


        function SetNewLineValueInParentTD(obj) {
            var lNewVal = $(obj).val();
            $(obj).closest('.trLineSecondRow').prev('tr').find('td').find("#hdnLineNewValueModified").val(lNewVal);
            //alert($(obj).closest('.trLineSecondRow').prev('tr').find('td').find("#hdnLineNewValueModified").val());
        }


    </script>
</asp:Content>
