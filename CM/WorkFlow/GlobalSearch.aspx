﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/CM.master"
    AutoEventWireup="true" CodeFile="GlobalSearch.aspx.cs" Inherits="WorkFlow_GlobalSearch" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridNavigation.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#requesttab").addClass("selectedtab");
    </script>
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
    </h2>
    <input id="hdnRequestId" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnDomainURL" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnIsDetailAccess"  runat="server" clientidmode="Static" type="hidden" />


    <h3>
        <asp:Label ID="lblTotalRecords" runat="server" Text=""></asp:Label></h3>
    <dl id="search-results">
        <asp:Repeater ID="rptGlobalSearch" runat="server">
            <HeaderTemplate>
                <table id="masterDataTable" class="masterTable list issues" width="100%">
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <dt class="issue"><a class="requestClick" myval="<%#Eval("RequestId")%>" href='ReplaceLinkWith<%#Eval("Link") %>'>
                            <%#Eval("ClientName")%>
                        </a></dt>
                        <dd>
                            <span class="description">
                                <%#Eval("ContractType")%><br>
                                <%#Eval("AssignedTo")%><br>
                                <%#Eval("RequestDate")%>
                            </span>
                        </dd>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
        <div id="rightlinks" style="display: none;">
            <uc1:Masterlinks ID="rightactions" runat="server" />
        </div>
        
    </dl>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());

        $(window).load(function () {
            $('#masterDataTable').find('tr a').each(function () {
                var oldHref = $(this).attr('href');
                var newHref = oldHref.replace('ReplaceLinkWith', $('#hdnDomainURL').val());
                if ($('#hdnIsDetailAccess').val() == 'False') {
                    $(this).attr('href', '#');
                }
                else {
                    $(this).attr('href', newHref);
                }
            });
        });      

    </script>
</asp:Content>
