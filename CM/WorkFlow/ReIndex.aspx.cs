﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;
using DocumentOperationsBLL;
using WorkflowBLL;
using System.Data;
using System.Data.SqlClient;
using EmailBLL;
using System.Configuration;
using System.Web.Services;
using System.Web.Security;
using ClauseLibraryBLL;
//using System.IO;
using System.Xml;
using Delimon.Win32.IO;


public partial class Workflow_ReIndex : System.Web.UI.Page
{
    string flg = "";
    IContractType objContract;
    IActivity objActivity;
    IDocuSign objDocuSign;
    IClauseLibrary ObjDocumentType;
    static string filename = "";
    IContractRequest req;
    string isCompleted = "";
    string fileName = "";

    SqlDataAdapter da;

    protected void Page_Load(object sender, EventArgs e)
    {
        flg = Request.QueryString["MessDisplay"];
        Message();
        //Session["RequestId"];
        //Response.Write(Request.Url.Host);
        Response.Write(HttpContext.Current.Server.MapPath(@"~/Uploads/asics/IndexDirectory"));
    }

    void Message()
    {
        if (flg != null && flg != "")
        {
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }
    }

    //protected void IndexingDocument()
    //{
    //    string FolderPath = HttpContext.Current.Server.MapPath(@"..\Uploads\CM\ContractDocs\");
    //    DirectoryInfo d = new DirectoryInfo(FolderPath);//Assuming Test is your Folder

    //    FileInfo[] txtFiles = d.GetFiles("*.txt");
    //    foreach (FileInfo file in txtFiles)
    //    {
    //        getIndexingDocument(Convert.ToString(FolderPath), Convert.ToString(file.Name));

    //    }

    //    FileInfo[] docxFiles = d.GetFiles("*.docx");
    //    foreach (FileInfo file in docxFiles)
    //    {
    //        getIndexingDocument(Convert.ToString(FolderPath), Convert.ToString(file.Name));

    //    }
    //    FileInfo[] xlsxFiles = d.GetFiles("*.xlsx");
    //    foreach (FileInfo file in xlsxFiles)
    //    {
    //        getIndexingDocument(Convert.ToString(FolderPath), Convert.ToString(file.Name));

    //    }
    //    FileInfo[] pdfFiles = d.GetFiles("*.pdf");
    //    foreach (FileInfo file in pdfFiles)
    //    {
    //        getIndexingDocument(Convert.ToString(FolderPath), Convert.ToString(file.Name));

    //    }

    //   // FileInfo[] Files = d.GetFiles("*.txt");
    //    //string DocumentName = "";
    //    //string DocumentSectionName = "ContractDocs";
    //    //foreach (FileInfo file in Files)
    //    //{           
    //        //string IndexcingPath = "";
    //        ////str = str + ", " + file.Name;
    //        //IndexcingPath +=FolderPath+ file.Name;
    //        //DocumentName = file.Name;
    //        //objActivity = FactoryWorkflow.GetActivityDetails();
    //        //objActivity.DocumentName = DocumentName;
    //        //objActivity.DocumentSectionName = DocumentSectionName;
    //        //hdnRequestDate.Value = objActivity.GetDatabyDocumentName();
    //        //if (!string.IsNullOrEmpty(Convert.ToString(hdnRequestDate.Value)))
    //        //{
    //        //    string strUrl = "";
    //        //    strUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + IndexcingPath;
    //        //    AsyncIndexerProduction.GetInstance().QueueForIndexing(strUrl, 1, hdnRequestDate.Value);
    //        //}            
    //    //}

    //}
    protected void btnPrev_Click(object sender, EventArgs e)
    {
        //IndexingDocument();
    }

    protected void getIndexingDocument(string FolderPath, string fileName)
    {
        //string DocumentName = "";
        //string DocumentSectionName = "ContractDocs";
        //string IndexcingPath = "";
        ////str = str + ", " + file.Name;
        //IndexcingPath += FolderPath + fileName;
        //DocumentName = fileName;
        //objActivity = FactoryWorkflow.GetActivityDetails();
        //objActivity.DocumentName = DocumentName;
        //objActivity.DocumentSectionName = DocumentSectionName;
        //hdnRequestDate.Value = objActivity.GetDatabyDocumentName();
        //if (!string.IsNullOrEmpty(Convert.ToString(hdnRequestDate.Value)))
        //{
        //    string strUrl = "";
        //    strUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + IndexcingPath;
        //    AsyncIndexerProduction.GetInstance().QueueForIndexing(strUrl, 1, hdnRequestDate.Value);
        //}        
    }

    protected void btnIndexContractDocs_Click(object sender, EventArgs e)
    {
        try
        {
            lblMessage.Text = "Fetching data...";

            ReIndex();
            //objActivity = FactoryWorkflow.GetActivityDetails();
            //for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            //{
            //    objActivity.RequestId = Convert.ToInt32(ds.Tables[0].Rows[i]["RequestId"]);
            //    ds.Tables[0].Rows[i]["IsMigrated"] = "Y";
            //    ds.AcceptChanges();                
            //}
            //SqlCommandBuilder com = new SqlCommandBuilder(da);
            //da.Update(ds);
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    private DataSet GetFileDetails()
    {
        string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["CMSCentral_connString"].ToString();
        SqlConnection con = new SqlConnection(ConString);
        SqlCommand cmd = new SqlCommand("TenantFileList", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@Domain", txtDomainName.Text);

        da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        con.Open();
        da.Fill(ds);
        con.Close();

        return ds;
    }    

    private void CreateFolders(string DomainName)
    {
        string FileLocation = Server.MapPath(@"~/");

        System.IO.DirectoryInfo DomainDIR = null;
        if (!Directory.Exists(FileLocation + @"\Uploads\" + DomainName))
        {
            DomainDIR = System.IO.Directory.CreateDirectory(FileLocation + @"\Uploads\" + DomainName);
            string[] subDIR = {  
                                 "ActivitiesDocuments\\ScannedActivitiesDocuments",
                                 "BulkImport", 
                                 "BulkUpload",                                  
                                 "Contract Documents\\PDF\\ScannedPDF",
                                 "Contract Documents\\SignedDocuments\\ScannedSignedDocuments", 
                                 "Contract Template", 
                                 "ContractDocs\\ScannedContractDocs", 
                                 "CustomerPortal",
                                 "EmailAttachments",
                                 "Reports",
                                 "PDFExport",
                                // "IndexDirectory", //For Advanced search within documents
                                 "IndexDirectory\\Dictionaries" //For Advanced search Dictionaries
                                };
            for (int i = 0; i < subDIR.Length; i++)
            {
                if (!System.IO.Directory.Exists(DomainDIR.FullName + "\\" + subDIR[i]))
                {
                    System.IO.Directory.CreateDirectory(DomainDIR.FullName + "\\" + subDIR[i]);
                }
            }
        }
    }

    private void CopyFiles()
    {
        DataSet ds = GetFileDetails();
        DataTable dtTenantName, dtTenantFileDetails;
        dtTenantName = ds.Tables[0];
        dtTenantFileDetails = ds.Tables[1];
        //string PATH = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + Context.Request.Url.Segments[1] + "/";
        string PATH = Server.MapPath(@"~/");
        //string DetinationPath = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + Context.Request.Url.Segments[1] + "Uploads/";
        
        //creating folder structure Tenant-wise
        try
        {
            for (int i = 0; i < dtTenantName.Rows.Count; i++)
            {
                CreateFolders(dtTenantName.Rows[i]["URL"].ToString());
            }
        }
        catch (Exception ex)
        {
            Response.Write("<br/>" + ex.Message);
        }

        try
        {
            for (int i = 0; i < dtTenantFileDetails.Rows.Count; i++)
            {
                string SourcePath = PATH + dtTenantFileDetails.Rows[i]["SourcePath"];
                string DestinationPath = PATH + dtTenantFileDetails.Rows[i]["DestinationPath"];
                string FolderPath = PATH + dtTenantFileDetails.Rows[i]["FolderPath"].ToString();
                string IndexDirectory = PATH + "Uploads\\" + dtTenantFileDetails.Rows[i]["Domain"] + "\\IndexDirectory\\Dictionaries";
                string DictionaryFile = "DICT-EN-US-USEnglish_SE.dict";

                if (!Directory.Exists(FolderPath))
                {
                    Directory.CreateDirectory(FolderPath);
                }
                if (!Directory.Exists(IndexDirectory))
                {
                    Directory.CreateDirectory(IndexDirectory);
                }

                if (!File.Exists(IndexDirectory + "\\" + DictionaryFile))
                {
                    File.Copy(PATH +  DictionaryFile, IndexDirectory + "\\" + DictionaryFile, true);
                }

                try
                {
                    //if (!File.Exists(DestinationPath))
                    {
                        File.Copy(SourcePath, DestinationPath, true);
                    }
                }
                catch (Exception Error)
                {
                    Response.Write("<br/>" + dtTenantFileDetails.Rows[i]["SRNO"] + " : " + Error.Message);
                }
            }
            lblMessage.Text = "Files Copied Successfully";
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    private void ReIndex()
    {
        DataSet ds = GetFileDetails();
        //string PATH = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + Context.Request.Url.Segments[1] + "/";
        string PATH = Server.MapPath(@"~/");
        //string DetinationPath = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + Context.Request.Url.Segments[1] + "Uploads/";
        try
        {
            for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
            {
                string SourcePath = PATH + ds.Tables[1].Rows[i]["SourcePath"];
                string DestinationPath = PATH + ds.Tables[1].Rows[i]["DestinationPath"];
                string FolderPath = PATH + ds.Tables[1].Rows[i]["FolderPath"].ToString();
                string IndexDirectory = PATH + @"Uploads\" + ds.Tables[1].Rows[i]["Domain"] + @"\IndexDirectory\";
                DestinationPath = "https://app.contractpod.com/" + ds.Tables[1].Rows[i]["URL"];
                AsyncIndexerProduction.Tenant = ds.Tables[1].Rows[i]["Domain"].ToString();
                
                Response.Write("<br/>"+AsyncIndexerProduction.Tenant);
                if (ds.Tables[1].Rows[i]["ToBeIndexed"].ToString().Equals("Y"))
                {
                    /*Keyoti Uploaded Document Indexing Start*/
                    try
                    {
                        string SignatureDate = "-";
                        if (ds.Tables[1].Rows[i]["SignatureDate"] != DBNull.Value)
                            SignatureDate = Convert.ToDateTime(ds.Tables[1].Rows[i]["SignatureDate"]).ToShortDateString();
                        string ContractCustomInfo = ds.Tables[1].Rows[i]["RequestId"].ToString() + "#" + ds.Tables[1].Rows[i]["ContractId"].ToString() + "#" + Convert.ToDateTime(ds.Tables[1].Rows[i]["requestDate"]).ToShortDateString() + "#" + ds.Tables[1].Rows[i]["ContractTypeId"].ToString() + "#" + ds.Tables[1].Rows[i]["Customer_SupplierId"].ToString() + "#" + ds.Tables[1].Rows[i]["Requester_Assigner"].ToString() + "#" + ds.Tables[1].Rows[i]["Assign_User"].ToString() + "#" + ds.Tables[1].Rows[i]["Assigned_Dept"].ToString() + "#" + ds.Tables[1].Rows[i]["ContractValue"].ToString() + "#" + SignatureDate;

                        ViewState["ContractInfo"] = ContractCustomInfo;
                        string strUrl = "";

                        strUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + ds.Tables[1].Rows[i]["Url"];
                        File.AppendAllText(IndexDirectory + "strUrl.txt", "\n\nDestinationPath - " + DestinationPath + " : ContractInfo - " + ViewState["ContractInfo"].ToString());

                        //Response.Write("<br/>DestinationPath - " + DestinationPath + " : ContractInfo - " + ViewState["ContractInfo"].ToString());
                        //AsyncIndexerProduction.GetInstance().QueueForIndexing(DestinationPath, 1, ViewState["ContractInfo"].ToString());
                        AsyncIndexerProduction.GetInstance().QueueForIndexing(DestinationPath + "##@@##" + ViewState["ContractInfo"].ToString(), 1, ViewState["ContractInfo"].ToString());
                        //Response.Write(strUrl + " : " + ContractCustomInfo + "<br/>");
                       

                        /*Keyoti Uploaded Document Indexing End*/
                    }
                    catch (Exception Err)
                    {
                        Response.Write("<br/>" + ds.Tables[1].Rows[i]["SRNO"] + "<br/>" + Err.Message);
                    }
                }
               // ds.Tables[1].Rows[i]["IsMigrated"] = "Y";
            }
            //ds.Tables[1].Columns.Remove("DestinationPath");
            //ds.Tables[1].Columns.Remove("FolderPath");
            //ds.Tables[1].Columns.Remove("SourcePath");
            //ds.Tables[1].Columns.Remove("ToBeIndexed");
            //ds.AcceptChanges();
            //SqlCommandBuilder com = new SqlCommandBuilder(da);
            //int a = da.Update(ds);

            lblMessage.Text = "Indexing Completed Successfully";
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    private void DeleteIndex()
    {
        DataSet ds = GetFileDetails();
        string DestinationPath = "";
        AsyncIndexerProduction.GetInstance().QueueForIndexing(DestinationPath, 2);
    }
    protected void btnCopyFiles_Click(object sender, EventArgs e)
    {
        CopyFiles();
    }
    protected void btnStopIndexing_Click(object sender, EventArgs e)
    {
        AsyncIndexerProduction.GetInstance().QueueForIndexing("", 2, ViewState["ContractInfo"].ToString());
    }

    protected void btnCreateContractVersion_Click(object sender, EventArgs e)
    {
        string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["connString"].ToString();
        SqlConnection con = new SqlConnection(ConString);
        SqlCommand cmd = new SqlCommand("SELECT TOP 5 * FROM ContractRequest WHERE IsBulkImport='Y' ORDER BY RequestId ASC", con);
        cmd.CommandType = CommandType.StoredProcedure;       

        da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        con.Open();
        da.Fill(dt);
        con.Close();
        cmd = null;
       
        string[] Files = Directory.GetFiles(Server.MapPath("~/BulkUpload"));        

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string RequestId, ContractId;
            RequestId = dt.Rows[i]["RequestId"].ToString();
            ContractId = dt.Rows[i]["ContractId"].ToString();

            for (int file = 0; file < Files.Length; file++)
            {
                FileInfo f = new FileInfo(Files[file]);
                string filesize = Math.Round(Convert.ToDecimal(f.Length) / 1024, 2).ToString();
                con.Open();
                cmd = new SqlCommand("EXEC CreateVersion @RequestId=" + RequestId + " @ContractId=" + ContractId + " @FileSizeKB=" + filesize, con);
                string retval = cmd.ExecuteScalar().ToString();
                con.Close();
                f = null;
            }
        }
    }

    protected void btnContractDocument_Click(object sender, EventArgs e)
    {
        string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["connString"].ToString();
        SqlConnection con = new SqlConnection(ConString);
        SqlCommand cmd = new SqlCommand("SELECT TOP 5 * FROM ContractRequest WHERE IsBulkImport='Y'", con);
        cmd.CommandType = CommandType.StoredProcedure;

        da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        con.Open();
        da.Fill(dt);
        con.Close();


    }
}