﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="RequestFlowPdfExport.aspx.cs" Inherits="WorkFlow_RequestFlowPdfExport" %>

<%@ Register Src="../UserControl/requestnewlinks.ascx" TagName="requestnewlinks"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/activities.ascx" TagName="activitiescontrol" TagPrefix="uc2" %>
<%@ Register Src="~/UserControl/KeyFieldsPdf.ascx" TagName="KeyFieldscontrol" TagPrefix="uckey" %>
<%@ Register Src="~/UserControl/KeyObligationsPdf.ascx" TagName="KeyFieldscontrol"
    TagPrefix="ucobligkey" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../Styles/application.css" rel="stylesheet" type="text/css" />
    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    <script src="../scripts/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    <link href="../Styles/jquery-ui-1.8.19.custom.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
    <asp:HiddenField ID="hdnActivityMessageFlag" ClientIDMode="Static" Visible="false"
        runat="server" />
    <asp:HiddenField ID="isFileGenereated" ClientIDMode="Static" Visible="false" runat="server" />
    <script language="javascript" type="text/javascript">
        $("#requestLink").addClass("menulink");
        $("#requesttab").addClass("selectedtab");
    </script>
    <script type="text/javascript">
        function scrollToAnchor(aid) {
            var aTag = $("div[id='" + aid + "']");
            $('html,body').animate({ scrollTop: aTag.offset().top }, 'slow');
        }

        var Counter = 0;
        function CallMe(obj) {
            $(obj).closest('li').find('div[class="plupload_file_DocumentType"]').find('input').val($(obj).val());
            if ($(obj).val() != '0') {

                $(obj).closest('li').find('div:eq(4)').find('select').next('div[id="dvFont"]').remove();
            }
            else {
                $(obj).closest('li').find('div:eq(4)').find('select').after('<div id="dvFont" style="margin-left:205px;text-align:left"><font color="red">Please select document Type</font></div>');
            }
        }
        $(document).ready(function () {
            var selectedTab = '<%=Request.QueryString["selectedTab"] %>';
            if (selectedTab != "") {
                $("#" + selectedTab.split('_')[0]).trigger("click");
                scrollToAnchor("activity");

                if ($('#hdnActivityMessageFlag').val() != '') {
                    var msg = $('#hdnActivityMessageFlag').val();
                    SectionMessageDiv('msgActivity', msg, 0, 100);
                    $('#hdnActivityMessageFlag').val('');
                }

            }

            var Mode = '<%=Request.QueryString["Mode"] %>';
            var ScrollTo = '<%=Request.QueryString["ScrollTo"] %>';
            if (ScrollTo != "")
                scrollToAnchor(ScrollTo);

            var Section = '<%=Request.QueryString["Section"] %>';
            if (Section != "")
                showMessage(Section, Mode);

            var obj = $("#ParentDiv");
            obj.on('dragenter', function (e) {
                e.stopPropagation();
                e.preventDefault();
            });
            obj.on('dragover', function (e) {
                e.stopPropagation();
                e.preventDefault();
            });
            obj.on('drop', function (e) {
                e.preventDefault();
                var files = e.originalEvent.target.files || (e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files);
                handleFileUpload(files, obj);
            });

            $('#no').click(function () {
                $.unblockUI();
                return false;
            });
        });

        function CloseUploader() {
            $.unblockUI();
            $('a.onPage').css('margin-top', '32px');
            $('.OnPageul').css('margin-top', '-75px');
            return false;
        }

        function showMessage(section, Mode) {
            var DivId, msg, flg;
            var ContractFilesCount = parseInt($("#hdnContractFilesCount").val());
            switch (section) {
                case "Approvals":
                    DivId = "msgApprovals";
                    msg = "Approval status updated.";
                    break;
                case "QA":
                    DivId = "msgQuestionnaire";
                    msg = "Questionnaire updated.";
                    break;
                case "QADraft":
                    DivId = "msgQuestionnaire";
                    msg = "Draft Saved.";
                    break;
                case "ContractVersions":
                    DivId = "msgContractVersions";
                    if (Mode == "CheckOut")
                        msg = "Check-out done successfully.";
                    else if (Mode == "MailSent")
                        msg = "Mail sent successfully.";
                    else if (Mode == "CheckIn")
                        msg = "Check-in done successfully.";
                    else if (Mode == "Delete")
                        msg = "Contract file deleted successfully.";
                    else if (Mode == "DeleteDocusign") {

                        if ('<%=ViewState["IsFromDocuSign"] %>' != 'Y' && (('<%=ViewState["CurrentIsValidDocument"] %>' != 'Y' || '<%=ViewState["PreviousIsValidDocument"] %>' != 'Y') && ContractFilesCount > 1)) // In case contract document not generated through Contract Pod
                            msg = "Contract file deleted successfully.<br/>'Track Changes' is not available for this document.";
                    }
                    else if (Mode == "TrackChanges")
                        msg = "Contract file created successfully.";
                    else if (Mode == "DocuSign")
                        msg = "Document sent successfully for signature.";
                    else {
                        if ('<%=ViewState["IsFromDocuSign"] %>' != 'Y' && (('<%=ViewState["CurrentIsValidDocument"] %>' != 'Y' || '<%=ViewState["PreviousIsValidDocument"] %>' != 'Y') && ContractFilesCount > 1)) // In case contract document not generated through Contract Pod
                            msg = "Contract document uploaded successfully.<br/>'Track Changes' is not available for this document.";
                        else
                            msg = "Contract document uploaded successfully.";
                    }
                    break;
                case "Documents":
                    scrollToAnchor("tab-content-document");
                    DivId = "msgDocuments";
                    if (Mode == "Upload")
                        msg = "Document uploaded successfully.";
                    else if (Mode == "Delete")
                        msg = "Document deleted successfully.";
                    break;


                case "KeyField":
                    scrollToAnchor("tab-content-keyfields");
                    DivId = "msgkeyfields";
                    msg = "Important dates updated successfully.";
                    break;

                case "KeyObligation":
                    scrollToAnchor("tab-content-keyobligations");
                    DivId = "msgkeyobligations";
                    msg = "Key obligations updated successfully.";
                    break;

            }

            if (msg != "" && msg != undefined)
                SectionMessageDiv(DivId, msg, 0, 100);
        }
    
    </script>
    <table id="statusTable" runat="server">
    </table>
    <asp:HiddenField ID="hdnExportId" Value="" Visible="false" runat="server" />
    <asp:HiddenField ID="hdnMalAlreadySent" Value="" Visible="false" runat="server" />
    <div>
        <asp:LinkButton ID="btnPDF" runat="server" Style="background: #df6c34 none repeat scroll 0 0;
            border: medium none; color: #fff; cursor: pointer; font-size: 13px; height: 33px;
            padding: 5px; text-align: center; vertical-align: text-bottom; width: auto;"
            OnClick="btnPDF_Click">Export to pdf</asp:LinkButton>
    </div>
    <div id="Workflowexport" runat="server">
        <div class="contextual" style="font-size: small; font-weight: bold; color: #289aa1;
            font-weight: bold">
            Current Contract Status :
            <asp:Label ID="lblContractStatusName" runat="server" Text=""></asp:Label>
        </div>
        <%#Eval("OrigionalFileName") %>
        <span style="font-size: 22px; color: #de6528">
            <h3>
                <asp:Label ID="lblContractIDHeader" runat="server" Text="" ClientIDMode="Static"></asp:Label></h3>
            <asp:Label ID="lblRequestId" runat="server" Text="" ClientIDMode="Static" Style="display: none"></asp:Label></span>
        <select name="BB" style="display: none" id="ddlDocumentType" runat="server" onchange="SetContractTypeValue(this)">
        </select>
        <div style="border: none" class="issue status-1 priority-3 child created-by-me details">
            <br />
            <div id="Div1" class="links-rightaligned wiki editable" runat="server" clientidmode="Static">
                <div style="width: 50%; display: inline" id="divEditRequest" runat="server">
                </div>
                <div style="width: 50%; display: inline" id="divBackToParentRequest" runat="server">
                    <asp:LinkButton ID="lnkToMainPage" class="icon-back-to-previous-request" runat="server"
                        OnClick="lnkToMainPage_Click">Back To Main Request</asp:LinkButton>
                </div>
            </div>
            <asp:Label ID="lblShowExpiryDateMessage" CssClass="Notice" runat="server" Text=""
                Visible="true"></asp:Label>
            <asp:Button ID="btnPrev" runat="server" Text="<< Previous" Visible="false" OnClick="btnPrev_Click" />
            <asp:Button ID="btnNext" runat="server" Text="Next >>" Visible="false" OnClick="btnNext_Click" />
            <br />
            <div class="subject">
                <h3>
                    <span style="font-size: 20px; color: #de6528"><strong>Snapshot</strong></span>
                </h3>
            </div>
            <p class="author">
                <asp:Label ID="lblRequestDetails" runat="server" Style="word-wrap: break-word;" Text=""></asp:Label>
                <asp:HiddenField ID="hdnRequestDate" Visible="false" runat="server" Value="" />
            </p>
            <table style="vertical-align: top; width: 97%" border="0">
                <tbody>
                    <tr>
                        <th style="width: 10%; text-align: right" class="search-filter-label">
                            <strong style="font-size: 12px; color: #289aa1">Request Id :</strong>
                        </th>
                        <td style="width: 20%;">
                            <%= ViewState["RequestId"] %>
                        </td>
                        <th style="width: 10%; text-align: right" class="search-filter-label">
                            <strong style="font-size: 12px; color: #289aa1">Request Type :</strong>
                        </th>
                        <td style="width: 40%;">
                            <asp:Label ID="lblRequestType" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 10%; text-align: right" class="search-filter-label">
                            <strong style="font-size: 12px; color: #289aa1">Contract ID :</strong>
                        </th>
                        <td>
                            <asp:Label ID="lblContractId" runat="server" Text=""></asp:Label>
                        </td>
                        <th style="width: 10%; text-align: right" class="search-filter-label">
                            <strong style="font-size: 12px; color: #289aa1">Contract Type :</strong>
                        </th>
                        <td style="width: 20%;">
                            <asp:Label ID="lblContractType" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 10%; text-align: right" class="search-filter-label">
                            <strong style="font-size: 12px; color: #289aa1">Assign To :</strong>
                        </th>
                        <td>
                            <asp:Label ID="lblAssignedTo" runat="server" Text=""></asp:Label>
                        </td>
                        <th style="width: 10%; text-align: right" class="search-filter-label">
                            <strong style="font-size: 12px; color: #289aa1">Contract Value :</strong>
                        </th>
                        <td>
                            <asp:Label ID="lblContractValue" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr style="display: none">
                        <th style="width: 10%; text-align: right" class="search-filter-label">
                            Deadline Date :
                        </th>
                        <td style="width: 40%;">
                            <asp:Label ID="lblDeadLineDate" runat="server" Text=""></asp:Label>
                        </td>
                        <th style="width: 10%; text-align: right" class="search-filter-label">
                            Term of Contract :
                        </th>
                        <td style="width: 40%;">
                            <asp:Label ID="lblTermOfContract" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr runat="server" id="trEstimatedValue" clientidmode="Static">
                        <th style="width: 10%; text-align: right" class="search-filter-label">
                            <strong style="font-size: 12px; color: #289aa1">Estimated value :</strong>
                        </th>
                        <td>
                            <asp:Label ID="lblEstimatedValue" runat="server" Text=""></asp:Label>
                        </td>
                        <th>
                        </th>
                        <td>
                        </td>
                    </tr>
                    <tr runat="server" id="tr1" clientidmode="Static">
                        <th style="width: 10%; text-align: right" class="search-filter-label">
                            <strong style="font-size: 12px; color: #289aa1">Priority :</strong>
                        </th>
                        <td>
                            <asp:Label ID="lblPriority" runat="server" Text=""></asp:Label>
                        </td>
                        <th style="width: 10%; text-align: right" class="search-filter-label">
                            <strong style="font-size: 12px; color: #289aa1">Priority Reason :</strong>
                        </th>
                        <td>
                            <asp:Label ID="lblPriorityreason" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; width: 100%" colspan="5">
                            <hr style="color: Black;" />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left" colspan="4" class="data-container-title">
                            <strong style="font-size: 20px; color: #de6528">Details</strong>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 10%; text-align: right; vertical-align: top; padding-top: 3px;"
                            class="search-filter-label">
                            <strong style="font-size: 12px; color: #289aa1">Name :</strong>
                        </th>
                        <td style="vertical-align: top; padding-top: 3px;">
                            <asp:Label ID="lblClientNames" runat="server" Text=""></asp:Label>
                        </td>
                        <th style="width: 10%; text-align: right" class="search-filter-label">
                            <strong style="font-size: 12px; color: #289aa1">Email Id :</strong>
                        </th>
                        <td style="vertical-align: top;">
                            <asp:Label ID="lblClientEmailId" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 10%; text-align: right; vertical-align: top; padding-top: 3px;"
                            class="search-filter-label">
                            <strong style="font-size: 12px; color: #289aa1">Address :</strong>
                        </th>
                        <td style="width: 20%;">
                            <asp:Label ID="lblClientAddress" runat="server" Text=""></asp:Label>
                        </td>
                        <th style="width: 10%; text-align: right; vertical-align: top; padding-top: 3px;"
                            class="search-filter-label">
                            <strong style="font-size: 12px; color: #289aa1">Contact Number :</strong>
                        </th>
                        <td style="width: 40%; vertical-align: top; padding-top: 3px;">
                            <asp:Label ID="lblClientMobile" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <%-- ************** NEW CHANGES ADDED ON 7 NOVEMBER 2014*********************--%>
                    <tr runat="server" id="trContractHistoryHR" clientidmode="Static">
                        <td style="text-align: left; width: 100%" colspan="5">
                            <hr />
                        </td>
                    </tr>
                    <tr runat="server" id="trContractHistoryHeader" clientidmode="Static">
                        <td style="text-align: left" colspan="4" class="data-container-title">
                            <strong style="font-size: 12px; color: #289aa1">Contract History</strong>
                        </td>
                    </tr>
                    <tr runat="server" id="trContractHistoryData" clientidmode="Static">
                        <td style="text-align: left" colspan="4" class="data-container-title">
                            <table id="tblContractHistory" class="masterTable list issues" width="100%" style="background-color: transparent">
                                <asp:Repeater ID="rptContractHistory" runat="server">
                                    <HeaderTemplate>
                                        <%--<table id="tblContractHistory" class="masterTable list issues" width="100%">--%>
                                        <thead>
                                            <tr>
                                                <th width="20%" style="background-color: transparent">
                                                    <strong style="font-size: 12px; color: #289aa1">Request ID</strong>
                                                </th>
                                                <th width="25%" style="background-color: transparent">
                                                    <strong style="font-size: 12px; color: #289aa1">Request Type</strong>
                                                </th>
                                                <th width="25%" style="background-color: transparent">
                                                    <strong style="font-size: 12px; color: #289aa1">Contract Type</strong>
                                                </th>
                                                <th width="30%" style="background-color: transparent; display: none">
                                                    <strong style="font-size: 12px; color: #289aa1">Contract Status</strong>
                                                </th>
                                                <tr>
                                        </thead>
                                        <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr style="cursor: pointer">
                                            <td>
                                                <asp:Label ID="lblRequestID" runat="server" ClientIDMode="Static" Text='<%#Eval("RequestID") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblRequestType" Style="word-wrap: break-word;" runat="server" ClientIDMode="Static"
                                                    Text='<%#Eval("RequestType") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblContractType" Style="word-wrap: break-word;" runat="server" ClientIDMode="Static"
                                                    Text='<%#Eval("ContractType") %>'></asp:Label>
                                                <input id="hdnContractHistoryURL" runat="server" clientidmode="Static" type="hidden"
                                                    value='<%#Eval("URL") %>' />
                                            </td>
                                            <td style="display: none">
                                                <asp:Label ID="lblContractStatus" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractStatus") %>'></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                        <%--</table>--%>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </table>
                        </td>
                    </tr>
                    <%--*************************************************************************************************************--%>
                    <tr runat="server" id="divReqDescrHR" clientidmode="Static">
                        <td style="text-align: left; width: 100%" colspan="5">
                            <hr />
                        </td>
                    </tr>
                    <tr runat="server" id="divReqDescrTITLE" clientidmode="Static">
                        <td style="text-align: left" colspan="4" class="data-container-title">
                            <strong style="font-size: 12px; color: #289aa1">Request Description</strong>
                        </td>
                    </tr>
                    <tr runat="server" id="divReqDescr" clientidmode="Static">
                        <td style="vertical-align: top;" colspan="5">
                            <asp:Label ID="lblRequestDescription" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr runat="server" id="trLatestContractFileHR" clientidmode="Static">
                        <td style="text-align: left; width: 100%" colspan="5">
                            <hr />
                        </td>
                    </tr>
                    <tr runat="server" id="trLatestContractFileTITLE" clientidmode="Static">
                        <td style="text-align: left" colspan="5" class="data-container-title">
                            <strong style="font-size: 12px; color: #289aa1">Latest Contract File</strong>
                        </td>
                    </tr>
                    <tr runat="server" id="trLatestContractFileLINK" clientidmode="Static">
                        <td style="vertical-align: top; width: 90%" colspan="4">
                            <a id="linkLatestContractVersionFile" runat="server" class="icon icon-attachment">
                            </a><a title="MSWord file" id="aMSWordLatestFile" runat="server" clientidmode="Static">
                            </a><a title="PDF file" id="aPDFLatestFile" runat="server" clientidmode="Static"
                                target="_blank"></a>
                        </td>
                        <td style="text-align: right; width: 20%; display: none">
                            <a id="aViewAll" href="#" onclick="showTab('workflow');scrollToAnchor('DivContractVersions'); this.blur(); return false;"
                                runat="server">View All</a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br />
        </div>
        <br />
        <%--TAB WORKFLOW START--%>
        <div class="" id="tab-content-workflow" style="display: block;">
            <div id="update" style="">
                <span style="font-size: 20px; color: #de6528"><strong>Workflow</strong></span>
                <%--Contract Version--%>
                <div id="DivContractVersions" style="" runat="server" clientidmode="Static">
                    <h3 style="font-size: 20px; color: #de6528">
                        Contract versions</h3>
                    <div id="msgContractVersions" style="display: none;">
                    </div>
                    <div class="attachments">
                        <asp:HiddenField ID="hdnContractFilesCount" Value="" Visible="false" runat="server"
                            ClientIDMode="Static" />
                        <asp:Repeater ID="rptContractVersions" runat="server" ClientIDMode="Static" OnItemDataBound="rptContractVersions_ItemDataBound  "
                            OnItemCommand="rptContractVersions_ItemCommand">
                            <ItemTemplate>
                                <h4>
                                    <span class="journal-link" id="JournalLink" runat="server" clientidmode="Static">
                                    </span><a id="linkContractVersionFile" runat="server" class="icon icon-attachment">
                                        <%#Eval("FileName")%></a> <span class="size" style="font-weight: normal;">(<%#Eval("FileSizeKB")%>KB)
                                        </span><span id="spanDeleteContractVersion" runat="server" clientidmode="Static"><a
                                            class="delete" rel="nofollow" title="Delete file" isfromdocusign='<%#Eval("isFromDocuSign")%>'
                                            id="<%#Eval("ContractFileId") %>" style="cursor: pointer" onclick="return DeleteContractFile(this);">
                                        </a></span><a rel="nofollow" title="MSWord file" id="aMSWordFile" runat="server"
                                            clientidmode="Static" href='<%# "../Uploads/" + Session["TenantDIR"] + "/Contract Documents/"+Eval("FileName")+""+Eval("GUID") +".docx" %>'>
                                        </a><a rel="nofollow" title="PDF file" id="aPDFFile" runat="server" clientidmode="Static"
                                            target="_blank" href='<%# "../Uploads/" + Session["TenantDIR"] + "/Contract Documents/PDF/"+ Eval("FileName")  +""+Eval("GUID") +".pdf" %>'>
                                        </a>
                                </h4>
                                <span class="author">By
                                    <%#Eval("ModifiedByUserName")%>,
                                    <%#Eval("ModifiedOnFormattedDate")%>
                                </span>
                                <ul class="details">
                                    <%#Eval("Remark")%>
                                    <asp:Repeater ID="rptContractFilesActivity" runat="server">
                                        <ItemTemplate>
                                            <li>
                                                <%#Eval("ContractFileActivityDetails")%></li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                                <asp:HiddenField ID="hdnIsPDFFile" Visible="false" Value='<%#Eval("isPDFFile")%>'
                                    runat="server" />
                                <asp:HiddenField ID="hdnFileName" Visible="false" Value='<%#Eval("FileName")+""+Eval("GUID")%>'
                                    runat="server" />
                                <asp:HiddenField ID="hdnIsFromDocuSign" Visible="false" Value='<%#Eval("isFromDocuSign")%>'
                                    runat="server" />
                                <asp:HiddenField ID="hdnContractFileId" Visible="false" Value='<%#Eval("ContractFileId")%>'
                                    runat="server" />
                                <asp:HiddenField ID="hdnIsCheckOutDone" Visible="false" Value='<%#Eval("IsCheckOutDone")%>'
                                    runat="server" />
                                <asp:HiddenField ID="hdnIsValidDocument" Visible="false" Value='<%#Eval("IsValidDocument")%>'
                                    runat="server" />
                                <asp:HiddenField ID="hdnIsUploaded" Visible="false" Value='<%#Eval("isUploaded")%>'
                                    runat="server" />
                                <br />
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <script language="javascript" type="text/javascript">
                        function DeleteContractFile(obj) {
                            var isFromDocuSign = $(obj).attr('isFromDocuSign');
                            var RequestId = $('#hdnRequestId').val();
                            var ContractFilesCount = parseInt($("#hdnContractFilesCount").val());
                            var mode = (isFromDocuSign == "Y" && ContractFilesCount > 1) ? "DeleteDocusign" : "Delete";

                            var FileName = "", FilePath = "";

                            FilePath = $(obj).closest('h4').find('a[id^="aPDFFile"]').attr('href');
                            if ($(obj).closest('h4').find('a[id^="aMSWordFile"]').attr('href') != undefined) {
                                FilePath = FilePath + "," + $(obj).closest('h4').find('a[id^="aMSWordFile"]').attr('href');
                            }
                            //$(obj).closest('div').find('a[id^="aMSWordFile"]').each(function () {
                            $(obj).closest('div').find('a[id^="aPDFFile"]').each(function () {
                                FileName = $(this).attr('href');
                                return false;
                            });

                            var PdfFileCnt = $('.delete').closest('div').find('a[id^="aPDFFile"]').length;
                            var NVFileName = "";
                            if ($(obj).attr('isfromdocusign') == 'Y' && PdfFileCnt > 1) {
                                var cntr = 1;
                                $(obj).closest('div').find('a[id^="aPDFFile"]').each(function () {
                                    if (cntr > 2) {
                                        return false;
                                    }
                                    else {
                                        NVFileName = $(this).attr('href');
                                    }
                                    cntr++;
                                });
                            }

                            var OFileName1 = FileName.substr(FileName.lastIndexOf('/') + 1);
                            var OFileName2 = OFileName1.substr(0, OFileName1.indexOf('.'));

                            OFileName1 = OFileName1.replace("'", "\'");
                            OFileName2 = OFileName2.replace("'", "\'");

                            if (NVFileName != "") {
                                NVFileName = NVFileName.substr(NVFileName.lastIndexOf('/') + 1);
                                NVFileName = NVFileName.substr(0, NVFileName.indexOf('.'));
                            }
                            if (confirm('Do you want to delete this version ?')) {
                                $.ajax({
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    url: "RequestFlow.aspx/DeleteContractFile",
                                    async: false,
                                    data: "{ContractFileId:'" + $(obj).attr('id') + "',isFromDocuSign:'" + isFromDocuSign + "',RequestId:'" + RequestId + "',FileName:'" + escape(OFileName2) + "',NVFileName:'" + escape(NVFileName) + "',FilePath:'" + escape(FilePath) + "',ContractInfo:'" + $("#hdnRequestDate").val() + "'}",
                                    dataType: "json",
                                    success: function (output) {
                                        //if (output.d == "1")

                                        var ParentRequestID = '<%= ViewState["ParentRequestID"]%>';
                                        if (ParentRequestID != "" && RequestId != ParentRequestID)
                                            ParentRequestID = "&ParentRequestID=" + ParentRequestID;
                                        else
                                            ParentRequestID = "";
                                        location.href = "RequestFlow.aspx?Section=ContractVersions&Mode=" + mode + "&ScrollTo=DivContractVersions&RequestId=" + '<%= ViewState["RequestId"]%>' + ParentRequestID;
                                    },
                                    error: function (err) {
                                    }
                                });
                            }
                            return false;
                        }
                    </script>
                </div>
                <div style="clear: both;">
                </div>
                <%--Updated Answers--%>
                <div id="DivUpdatedQuestions" style="" runat="server" clientidmode="Static">
                    <h3 style="font-size: 20px; color: #de6528">
                        Contract Assembly</h3>
                    <p>
                        <asp:Label ID="lblContractTemplateName" runat="server" Text=""></asp:Label></p>
                    <br />
                    <div id="note-1">
                        <div id="msgQuestionnaire" style="display: none;">
                        </div>
                        <ul class="details ">
                            <asp:Repeater ID="rptQA" runat="server">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <strong><span style="font-size: 12px; color: #289aa1">
                                                    <%#Eval("Question") %></span> </strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <%#Eval("Answer").ToString().Replace("\n", "<br>&nbsp;")%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
                <div style="clear: both;">
                </div>
                <%--Approval--%>
                <div id="PendingApprovals" class="journal has-notes has-details" style="" runat="server"
                    clientidmode="Static">
                    <h3 id="appheading" style="">
                        Approvals</h3>
                    <div id="msgApprovals" style="display: none;">
                    </div>
                    <div id="ApprovalStages">
                        <asp:Repeater ID="rptPendingApprovals" runat="server" OnItemDataBound="rptPendingApprovals_ItemDataBound">
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnContractApprovalID" Visible="false" runat="server" Value='<%#Eval("ContractApprovalId") %>' />
                                <input id="hdnApproverLoginId" type="hidden" visible="false" runat="server" clientidmode="Static"
                                    value='<%#Eval("AssignTo") %>' />
                                <input id="hdnIsActive" type="hidden" visible="false" runat="server" clientidmode="Static"
                                    value='<%#Eval("IsActive") %>' />
                                <h4>
                                    <span id="spanStageName" runat="server" clientidmode="Static">
                                        <%#Eval("StageName") %></span> <a href='<%#Eval("linkToApproval") %>' title="Approval"
                                            id="aLinkToApproval" runat="server" clientidmode="Static">
                                            <%#Eval("StageName") %></a> <span class="journal-link" id="ApprovalJournalLink" runat="server"
                                                clientidmode="Static">
                                                <div class="wiki editable" id="journal-22716-notes">
                                                    <div>
                                                        <a href='<%#Eval("linkToApproval") %>' title="Edit Approval" id="aLinkToApproval_Second"
                                                            runat="server" style="font-weight: normal" clientidmode="Static">
                                                            <img src="../images/edit.png?1349001717" />
                                                            Edit </a>&nbsp;&nbsp;
                                                    </div>
                                                </div>
                                            </span>
                                </h4>
                                <ul class="details">
                                    <asp:Repeater ID="rptApprovalsActivity" runat="server">
                                        <ItemTemplate>
                                            <li>
                                                <%#Eval("ApprovalActivtiyDetails").ToString().Replace("\n", "<br/>&nbsp;&nbsp;")%>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                                <br />
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div id="PendingStagesForApproval">
                    <asp:Repeater ID="rptPendingStagesForApproval" runat="server">
                        <ItemTemplate>
                            <h4>
                                <span id="spanPendingStageName">
                                    <%#Eval("StageName") %></span>
                            </h4>
                            <ul class="details">
                                <li>
                                    <%#Eval("AssignedToUserName") %></li>
                            </ul>
                            <br />
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div style="clear: both;">
                </div>
                <%--Questionnair--%>
                <div class="box" id="DivQuestionnair" runat="server">
                    <h3 style="font-size: 20px; color: #de6528">
                        Contract Assembly</h3>
                    <fieldset runat="server" id="QuestionnairTemplate" clientidmode="Static">
                        <%#Eval("DocumentTypeName") %>
                        <legend>Templates</legend>
                        <br />
                        <table style="width: 100%; table-layout: fixed;">
                            <tr>
                                <th width="20%">
                                </th>
                                <th width="2%">
                                </th>
                                <th width="78%">
                                </th>
                            </tr>
                            <tr>
                                <td style="text-align: right;">
                                    <label for="issue_tracker_id">
                                        Contract Template</label>
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    <select name="select" id="ddlContractTemplate" style="width: 50%" clientidmode="Static"
                                        disabled="disabled" runat="server" class="chzn-select" onchange="setFileName(this); ddlContractTemplate_onclick();">
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <%#Eval("FullName") %>
                        <p>
                            <%#Eval("AddedDate") %>
                        </p>
                    </fieldset>
                </div>
                <%--Buttons--%>
                <br />
                &nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnDisplayQuestionnair" runat="server" OnClick="btnDisplayQuestionnair_Click"
                    ClientIDMode="Static" Text="Load Questionnaire" />
                <%--  <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="Back" />--%>
                <asp:HiddenField ID="hdnFileName" runat="server" Visible="false" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnRequestId" runat="server" Visible="false" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnSelectedFileId" runat="server" Visible="false" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnContractId" runat="server" Visible="false" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnContractFileName" runat="server" Visible="false" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnContractTypeId" runat="server" Visible="false" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnContractStatusId" runat="server" Visible="false" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnRequestTypeId" runat="server" Visible="false" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnParentRequestID" runat="server" Visible="false" ClientIDMode="Static" />
                <input id="hdnDocumentIDs" runat="server" clientidmode="Static" visible="false" type="hidden" />
                <input id="hdnUserID" runat="server" clientidmode="Static" visible="false" type="hidden" />
                <uckey:KeyFieldscontrol ID="KeyFieldcontrol1" runat="server" />
                <br />
                <ucobligkey:KeyFieldscontrol ID="KeyObligation" runat="server" />
                <br />
                <asp:UpdatePanel ID="upBack" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="Back" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="../UploadJs/js/plupload.full.min.js"></script>
    <script type="text/javascript" src="../UploadJs/js/jquery.plupload.queue/jquery.plupload.queue.js"></script>
    <script type="text/javascript" src="../scripts/jquery.blockUI.js"></script>
    <script language="javascript" type="text/javascript">
        function DeleteDocument(obj) {
            //var DocPath = $(obj).parent('span').prev().prev().find('#alinkWorkflowDocument').attr('href');
            var thisObj = $(obj).attr('id');

            var DocPath = "";
            DocPath = $($('#' + thisObj).parents('span')[1]).find('.icon').attr('href');
            if (typeof (DocPath) == 'undefined') {
                DocPath = $('#' + thisObj).prev().attr('href');
            }

            if (confirm('Do you want to delete the file ?')) {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "RequestFlow.aspx/DeleteDocument",
                    data: "{DocumentID:'" + $(obj).attr('id') + "',DocumentPath:'" + DocPath + "'}",
                    dataType: "json",
                    success: function (output) {
                        if (output.d == "1") {
                            $(obj).parent('span').parent('span').remove();
                            showMessage("Documents", "Delete");
                        }
                    },
                    error: function (err) {
                    }
                });
            }
            return false;
        }

        var DocumentTypeId = "";
        var upl;
        $(function () {
            upl = $("#uploader").pluploadQueue({

                runtimes: 'html5, html4',
                url: '../ClauseLiabrary/DocumentHandler.ashx',
                max_file_size: '80mb',
                max_file_count: 20, // user can add no more then 20 files at a time
                chunk_size: '80mb',
                unique_names: true,
                multiple_queues: true,
                multipart: true,
                multipart_params: {
                },
                resize: { width: 1024, height: 768, quality: 90 },
                rename: true,
                sortable: true,
                filters: [
               	{ title: "Image files", extensions: "jpg,gif,png,bmp,tiff,jpeg,TIF,blob" },
                { title: "Document files", extensions: "xls,xlsx,doc,docx,pdf,swf,ppt,pptx,txt,csv,odt,odp,odg,ods,mp3,mp4,wmv,rtf" },
                { title: "Zip files", extensions: "zip,avi" },
                { title: "Other", extensions: "msg,eml" }
                //             {title: "Document files", extensions: "doc,docx" },
		],
                // PreInit events, bound before any internal events
                preinit: {
                    Init: function (up, info) {
                        // log('[Init]', 'Info:', info, 'Features:', up.features);
                        $('#uploader').next('p').remove();
                        $('#uploader').next('p').remove();
                        $('#uploader').next('p').remove();
                        $('#uploader').next('p').remove();
                    },
                    PostInit: function (up) {
                    },
                    UploadFile: function (up, file) {
                        fname = file.name.replace(' & ', '').replace('&', '').replace('#', '');
                        var filesize = file.size;
                        if ($('.plupload_filelist').find('li:eq(' + Counter + ') div.plupload_file_DocumentType:eq(1)').html() != 'Done') {
                            DocumentTypeId = $('.plupload_filelist').find('li:eq(' + Counter + ') div.plupload_file_DocumentType:eq(1)').find('input').val();
                            Counter++;
                        }
                        else {
                        }
                        if (DocumentTypeId.indexOf("o_") == -1) {
                            //alert($('#hdnRequestDate').val());
                            var ContractInfo = $('#hdnRequestDate').val().split('#');

                            up.settings.url = '../ClauseLiabrary/DocumentHandler.ashx?filesize=' + filesize + '&filename=' + fname + '&DocumentTypeId=' + DocumentTypeId + '&UserID=' + $('#hdnUserID').val() + '&RequestID=' + $('#hdnRequestId').val();
                        }
                    }
                },
                init: {
                    Refresh: function (up) {
                    },
                    StateChanged: function (up) {
                    },
                    QueueChanged: function (up) {
                    },
                    UploadProgress: function (up, file) {
                    },

                    FilesAdded: function (up, files) {
                        //$('a.onPage').css('margin-top', '-75px');
                        // $('.OnPageul').css('margin-top', '-75px');
                        //  $('div.plupload_buttons').find('a.plupload_start').before($('a.onPage').detach());
                        $.blockUI({ message: $('#uploader'), css: { width: '600px'} });
                        $('.plupload_filelist').css('height', $('.plupload_filelist').height(up.files.length * 50));

                        plupload.each(files, function (file) {
                            //  log('  File:', file);
                        });
                    },

                    FilesRemoved: function (up, files) {
                        $('.plupload_filelist').css('height', $('.plupload_filelist').height(up.files.length * 50));
                        plupload.each(files, function (file) {
                            //  log('  File:', file);
                        });
                    },

                    FileUploaded: function (up, file, info) {
                        $.ajax({
                            async: false,
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "RequestFlow.aspx/BindDocumentDetails",
                            data: "{RequestId:'" + $("#hdnRequestId").val() + "'}",
                            dataType: "json",
                            success: function (output) {
                                $("#tab-content-document div.attachments").html('').append(output.d);
                                showMessage("Documents", "Upload");
                            },
                            error: function (err) {
                                //alert(err.responseText);
                            }
                        });
                    },
                    ChunkUploaded: function (up, file, info) {
                        // Called when a file chunk has finished uploading
                        if (info.response != "Chunk") {
                            chunkreponse = info.response;
                            // log('[FileUploaded] File:', file, "Info:", info);
                        }
                    },
                    Error: function (up, args) {
                        // Called when a error has occured
                        //  log('[error] ', args);
                    }
                },
                // Resize images on clientside if we can
                resize: { width: 320, height: 240, quality: 90 },
                flash_swf_url: '../../js/Moxie.swf',
                silverlight_xap_url: '../../js/Moxie.xap'
            });
        });
    </script>
    <script type="text/javascript">
        function setFileName(obj) {
            var fileName = $(obj).val().split('#')[1];
            $("#hdnSelectedFileId").val($(obj).val());
            $("#hdnFileName").val(fileName);
        }
        function setDafaultValues() {
            $('#ddlContractTemplate').val($('#hdnSelectedFileId').val());
        }

        function setUpdated(obj) {
            $(obj).attr("IsUpdated", "Y");
        }

        $(function () {
            $(".mws-datepicker").datepicker({ minDate: 0, maxDate: "+10M +10D", dateFormat: "dd-M-yy" });

            if ($('#ddlContractType').val() != "0" && $('#ddlContractType').val() != null && $('#ddlContractType').val() != undefined) {
                $('#ddlContractType').trigger("change");
                setDafaultValues();
            }
        });

        $().ready(function () {
            setDafaultValues();
            $('#ui-datepicker-div').hide();
            $("#actdiv").find(".chzn-container-single").css("width", "200px");
        });
        $(window).load(function () {
            $("#ui-datepicker-div").hide();

        });
    </script>
    <script type="text/javascript">
        $("#btnDisplayQuestionnair").hide();
        function ddlContractTemplate_onclick() {
            if ($("#ddlContractTemplate").val() == "0") {
                $("#btnDisplayQuestionnair").hide();
            }
            else {
                $("#btnDisplayQuestionnair").show();
            }
        }
        function ddlContractType_onclick() {
        }

        function setActivityMessageFlag(msg) {
            $('#hdnActivityMessageFlag').val(msg);
        }
    </script>
    <%--********************** ADDED BY SANTOSH ****************************--%>
    <script type="text/javascript">
        $("#tblContractHistory tbody tr td").click(function (index, TD) {
            location.href = "RequestFlow.aspx?Status=Workflow&RequestId=" + $(this).parent('tr').find('#lblRequestID').text() + "&ParentRequestID=" + '<%= ViewState["ParentRequestID"] %>';
        });             
    </script>
    <%--********************************************************************--%>
</asp:Content>
