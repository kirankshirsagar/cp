﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using CommonBLL;
using DocumentOperationsBLL;

public partial class WorkFlow_UploadContractFile : System.Web.UI.Page
{
   //private readonly static string Path = @"~/Uploads/Contract Files";
    public bool FinalSignedCopy;

    protected void Page_Load(object sender, EventArgs args)
    {
        //if (!IsPostBack)
        {

            ViewState["ContractTypeId"] = Request.QueryString["ContractTypeId"];
            ViewState["RequestId"] = Request.QueryString["RequestId"];
            ViewState["ContractId"] = Request.QueryString["ContractId"];
            ViewState["ContractTemplateId"] = Request.QueryString["ContractTemplateId"];
            ViewState["NextFileName"] = HttpUtility.UrlEncode(Request.QueryString["NextFileName"]);
            ViewState["IsApprovalDone"] = Request.QueryString["IsApprovalDone"];
            ViewState["ParentRequestID"] = "";
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["ParentRequestID"])))
            {
                ViewState["ParentRequestID"] = "&ParentRequestID=" + Request.QueryString["ParentRequestID"];
            }

            if (ViewState["IsApprovalDone"] == null)
            {
                chkIsFinalSignedDocument.Enabled = true;
            }
            else if (ViewState["IsApprovalDone"].ToString() != "Y")
            {
                chkIsFinalSignedDocument.Enabled = false;
            }
            ViewState["User"] = Session[Declarations.User];
            ViewState["IP"] = Session[Declarations.IP];

            if (ViewState["ContractId"].ToString() == "0")
            {
                lblTitle.Text = "Upload contract file";
                lblSubTitle.Text = "Upload contract file";
            }
            else
            {
                lblTitle.Text = "Upload file for contract " + ViewState["ContractId"].ToString();
                lblSubTitle.Text = "Upload file for contract " + ViewState["ContractId"].ToString();
            }

            ViewState["FinalSignedCopy"] = Access.isCustomised(73);
            if (ViewState["FinalSignedCopy"] != null)
            {
                FinalSignedCopy = Convert.ToBoolean(ViewState["FinalSignedCopy"].ToString());
            }
            pFinalSignedCopy.Visible = FinalSignedCopy;
        }
    }   

    protected void SaveContractFile()
    {
        //ContractProcedures CP = new ContractProcedures();
        //FileInfo file = new FileInfo(Server.MapPath(Path) + "//" + hdnFileName.Value);
        ////CP.ContractId = Convert.ToInt32(ViewState["ContractId"].ToString());
        //CP.RequestId = Convert.ToInt32(ViewState["RequestId"].ToString());
        //CP.FileName = hdnFileName.Value;
        //CP.FileSizeKB = Math.Round(Convert.ToDecimal(file.Length) / 1024, 2).ToString();
        //CP.ModifiedBy = Convert.ToInt32(Session[Declarations.User]);
        //CP.IpAddress = Session[Declarations.IP].ToString();
        //string result = CP.InsertRecord();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("RequestFlow.aspx?ScrollTo=DivContractVersions&RequestId=" + ViewState["RequestId"] + "&Status=Workflow" + ViewState["ParentRequestID"]);
    }
}