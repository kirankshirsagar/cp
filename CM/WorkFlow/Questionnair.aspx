﻿<%@ Page Title="Questionnaire" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"  ValidateRequest="false"
    CodeFile="Questionnair.aspx.cs" Inherits="WorkFlow_Questionnair" %>

<%@ Register Src="../UserControl/requestnewlinks.ascx" TagName="requestnewlinks"
    TagPrefix="uc2" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../JQueryValidations/jquery-ui-1.8.19.custom.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
<div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="Masterlinks1" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>

    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
    <link href="../JQueryValidations/jquery-ui-1.8.19.custom.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../scripts/jquery-ui-1.8.16.custom.css"/>    

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $('#ui-datepicker-div').hide();
            $("#requestLink").addClass("menulink");
            $("#requesttab").addClass("selectedtab");
            $('textarea').autogrow();           
        });
        $(window).load(function () {
            $("#ui-datepicker-div").hide();
            $(".chzn-container-multi").find('input:text').css("width", "150px");
            $('input[value="Select Some Options"]').css("width", "150px");
        });
    </script>
    <h2>
        Questionnaire for request #<%=ViewState["RequestId"].ToString() %></h2>
    <div>
        <div id="tblContainer">
            <p>
                <a href='RequestFlow.aspx?RequestId=<%=ViewState["RequestId"].ToString()+"&Status=Workflow " %>' class="issue status-1 priority-4 parent">Request #<%=ViewState["RequestId"].ToString() %></a>:
                Questionnaire for request #<%=ViewState["RequestId"].ToString() %>
           </p>
           <br />
           <asp:PlaceHolder ID="phQuestionnair" runat="server" ClientIDMode="Static"></asp:PlaceHolder> 
        </div>
        <asp:CheckBox ID="chkGenerateContract" runat="server"  Text="Generate Contract" ClientIDMode="Static" Checked="true"/>  
    </div> 
     <br />
        <asp:Button ID="btnSaveDraft" runat="server" onclick="btnSaveDraft_Click" Text="Save Draft" ClientIDMode="Static" Visible="true" class="submit"/>
        <asp:Button ID="btnGenerateContract" runat="server" onclick="btnGenerateContract_Click"  ClientIDMode="Static"  Text="Save & Generate Contract" class="submit btn_validate"  Visible="false"/>
        <asp:Button ID="btnBack" runat="server" onclick="btnBack_Click" Text="Back" />
   

    <script type="text/javascript">
        $('.submit').live("click", function () {               
            ShowProgress();
        });

        var ContractStatusId = '<%= ViewState["ContractStatusID"]%>';
        var RequestTypeId = '<%= ViewState["RequestTypeId"]%>';
        if (ContractStatusId <= 6)
            $('#btnSaveDraft').show();
        else
            $('#btnSaveDraft').hide();

        $('#chkGenerateContract').live("change", function () {
            if ($(this).prop('checked')) {
                $('#btnGenerateContract').val('Save & Generate Contract');
                $('#btnGenerateContract').show();
                $('#btnSaveDraft').hide();

                if (ContractStatusId <= 6)
                    $('#btnSaveDraft').show();
            }
            else {
                if (ContractStatusId <= 6) {
                    $('#btnSaveDraft').show();
                    $('#btnGenerateContract').hide();

                    if (RequestTypeId == 1) {
                        $('#btnGenerateContract').show();
                        $('#btnGenerateContract').val('Save');
                    }
                }
                else {
                    $('#btnSaveDraft').hide();
                    $('#btnGenerateContract').val('Save');
                    $('#btnGenerateContract').show();
                }
            }
        });
    </script>
   
    <script language="javascript"type="text/javascript"> 
        function setUpdated(obj) {
            $(obj).attr("IsUpdated", "Y");
        }

        $(function () {
            //$(".mws-datepicker").datepicker();
            //$(".mws-datepicker").datepicker("option", "dateFormat", "dd-MMM-yy");
            //$(".mws-datepicker").datepicker({ minDate: 0, maxDate: "+10M +10D", dateFormat: "dd-M-yy" });
            $(".datepicker").datepicker({ showOn: "both",
                buttonImage: "../Styles/css/icons/16/calendar_1.png",
                buttonImageOnly: true,
                prevText: 'Previous',
                //yearRange: 'c-50:c+50',
                yearRange: "-100:+10",
                changeMonth: true,
                dateFormat: 'dd-M-yy',
                buttonText: '',
                onClose: function (dateText, inst) {                    
                },
                changeYear: true
            }).attr('placeholder', 'dd-MMM-yyyy').blur(function () {
                //alert("2"); return RemoveErrorClass($(this));
            });
        });      

    </script>


    <%--Data Type Validation Script--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.number').live('keypress', function (evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode == 8) {
                    return true;
                }
                else if ((charCode < 48) || (charCode > 57)) {
                    return false;
                }
            });

            $('.int').live('keypress', function (evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode == 8) {
                    return true;
                }
                else if ((charCode < 48) || (charCode > 57)) {
                    return false;
                }
            });

            $('.int').live('paste', function (evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                $obj = $(this);
                setTimeout(function () {
                    myNum = $obj.val();

                    var pattern = /^[0-9]+$/;

                    if (!pattern.test(myNum)) {
                        myNum = myNum.replace(/[^0-9]/g, '');
                        $obj.val(myNum);
                        return false;
                    }
                }, 0);
            });

            $('.numbersOnly').live('keypress', function (evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode == 8) {
                    return true;
                }
                else if ((charCode != 46 || $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57)) {
                    return false;
                }
            });


            /*money  - old name comCurrency*/
            $('.money,.moneyZero').live('keypress', function (evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode == 8) {
                    return true;
                }
                else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    if (charCode != 46) {
                        return false;
                    }
                }

                var caretpos = $(this).caret().start;
                valuestart = $(this).val().substring(0, caretpos);
                valueend = $(this).val().substring(caretpos);
                myNum = valuestart + String.fromCharCode(charCode) + valueend;

                var count = 0;

                var pattern = /^\d{0,8}\.?\d{0,2}$/;

                if (!pattern.test(myNum)) {
                    count = 1;
                }
                if (count == 1) {
                    return false;
                }
                else {
                    if (myNum > 1000000000) {
                        return false;
                    }
                }
                return true;
            });


            $('.money').live('blur', function () {
                if ($(this).val() != "") {
                    var amt = parseFloat($(this).val());
                    if (amt <= 0) {
                        $(this).val('');
                    }
                }
            });


            $('.percentage').live('keypress', function (evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode

                if (charCode == 8) {
                    return true;
                }
                else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    if (charCode != 46) {
                        return false;
                    }
                }
                var count = 0;
                var caretpos = $(this).caret().start;
                valuestart = $(this).val().substring(0, caretpos);
                valueend = $(this).val().substring(caretpos);
                myNum = valuestart + String.fromCharCode(charCode) + valueend;


                var pattern = /^\d{0,3}\.?\d{0,2}$/;
                if (!pattern.test(myNum)) {
                    count = 1;

                }
                if (count == 1) {
                    return false;
                }
                else {
                    if (myNum > 100) {
                        return false;
                    }
                }
                return true;
            });


            $('.Landline').live('keypress', function (evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode

                if (charCode == 8) {
                    return true;
                }
                else if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;

                var caretpos = $(this).caret().start;
                valuestart = $(this).val().substring(0, caretpos);
                valueend = $(this).val().substring(caretpos);
                myNum = valuestart + String.fromCharCode(charCode) + valueend;

                if (myNum.length > 10) {
                    return false;
                }
                return true;
            });
            $('.Currency').live('keypress', function (evt) {
                debugger;
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode == 8) {
                    return true;
                }
                if (charCode == 13)
                    return false;

                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    if (charCode != 46) {
                        return false;
                    }
                }
                var count = 0;
                var myNum = this.value + String.fromCharCode(charCode);
                //  var pattern = /^\d{0,8}$/;
                var pattern = /^\d{0,18}\.?\d{0,2}$/;
                // var pattern = /^\d{0,8}\.?\d{0,0}$/;
                if (!pattern.test(myNum)) {
                    count = 1;
                }
                if (count == 1) {
                    return false;
                }
                else if (myNum > 999999999999999999) {
                    return false;
                }
                else
                    return true;
            });
        });
    </script>

    <%--Script for Conditional Questions [Added by Bharati]--%>

    <script type="text/javascript">
        var prevSelect;
        var currSelect;
        var PrevSelectedRows = "";
        var CurrSelectedRows = "";

        function ShowHideRow(ddl) {
           
            var associatedRows = "#MainContent_ASPOSE_tbl tbody tr.AttachedToField" + $(ddl).attr('flid');
            currSelect = ddl;
            CurrSelectedRows = "#MainContent_ASPOSE_tbl tbody tr.AttachedToValue" + $(ddl).val();
            $(associatedRows).hide();
            $(associatedRows).find('select.conditional, input:text.conditional, textarea.conditional').removeClass("required");

            if ($(ddl).val() != "0") {
                if ($(CurrSelectedRows).length > 0) {
                    $(associatedRows).hide();
                    $(associatedRows).find('select.conditional, input:text.conditional, textarea.conditional').removeClass("required");

                    if ($(PrevSelectedRows).length > 0 && currSelect == prevSelect) {
                        $(PrevSelectedRows).hide();
                        $(PrevSelectedRows).find('select.conditional, input:text.conditional, textarea.conditional').removeClass("required");
                    }
                    $(CurrSelectedRows).show();
                    $(CurrSelectedRows).find('select.conditional, input:text.conditional, textarea.conditional').addClass("required");
                    $(CurrSelectedRows).find("div.chzn-container").css("width", "300px");
                    PrevSelectedRows = CurrSelectedRows;
                    prevSelect = ddl;
                }
                else {
                    if (currSelect == prevSelect) {
                        $(PrevSelectedRows).hide();
                        $(PrevSelectedRows).find('select.conditional, input:text.conditional, textarea.conditional').removeClass("required");
                    }
                }
            }
        }

        function ResetTR() {
            $("#MainContent_ASPOSE_tbl tbody tr select").each(function (index, value) {
                $("#MainContent_ASPOSE_tbl tbody tr.AttachedToValue" + $(this).val()).show();
                $("#MainContent_ASPOSE_tbl tbody tr.AttachedToValue" + $(this).val()).find('select.conditional, input:text.conditional, textarea.conditional').addClass("required"); ;
                $("#MainContent_ASPOSE_tbl tbody tr.AttachedToValue" + $(this).val()).find("div.chzn-container").css("width", "300px");
            });
        }
        $(window).load(function (evt) {
           $("#MainContent_ASPOSE_tbl tbody tr.IsConditional").hide();
           $("#MainContent_ASPOSE_tbl tbody tr.IsConditional").find('select.conditional, input:text.conditional, textarea.conditional').removeClass("required"); ;
            ResetTR();
        });
    </script>
    

    <script src="../CommonScripts/dropdownlist.js" type="text/javascript"></script>

    <%--Script for binding dropdowns from master table [Added by Bharati]--%>
    <script type="text/javascript">
        var search = '';

        $(document).ready(function () {
            $('select').each(function () {
                var MasterTable = $(this).attr('master');
                if (MasterTable != undefined && MasterTable != '') {
                    BindMasters(MasterTable, $(this), '');
                    var selected = $(this).closest('tr').find("input[type='hidden']").val();
                    $(this).val(selected);
                }
            }); ;

        });

        $(window).load(function () {
            $('input[value="Select Some Options"]').css("width", "150px");

            $("select[master!='']").change(function () {                
                $(this).closest('tr').find("input[type='hidden']").val($(this).val());
            });

            $("select[master!='']").next('div').find('.chzn-search').bind('keyup', function (e) {
                var obj = $(this).closest('tr').find('select');
                delay(function () {
                    keyUpSearch(obj, e.keyCode);
                }, 1000);

            });
        });

        var delay = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();

        function keyUpSearch(obj, key) {
            if ($('.no-results').text() != '') {
                var search = $('.no-results').find('span').html();
                if (search.length > 2) {                    
                    var MasterTable = $(obj).attr('master');
                    if (search == undefined)
                        search = '';

                    if (key != 8) {
                        BindMasters(MasterTable, obj, search);
                    }                   
                }
            }
        }

        function BindMasters(MasterTable, obj, search) {
            //var MasterTable = "MstCity";       
            var strType = "POST";
            var strContentType = "text/html; charset=utf-8";
            var strData = "{}";
            var strCatch = false;
            var strDataType = 'json';
            var strAsync = false;
            var MasterList;
            var type = 'FillMaster';
            var index = 0;
            var parentId = 0;
            var strURL = '../Handlers/GetQuestionnaireDropDownHandler.ashx?Type=' + type + "&MasterName=" + MasterTable + "&parentId=" + parentId + '&Search=' + search + '&RequestId=' + '<%= ViewState["RequestId"].ToString() %>';
            objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
            MasterList = objHandler.HandlerReturnedData;

            if (MasterList.length > 0) {
                bindSelect(MasterList, '#' + $(obj).attr('id'));
                if (search != '') {
                    $(obj).closest('tr').find('.chzn-search').find('input').val(search);
                }
            }
        }

    </script>
</asp:Content>