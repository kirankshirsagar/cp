﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ClientAdd.aspx.cs" Inherits="WorkFlow_ClientAdd" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
<%--    <link href="../Styles/Font.css" rel="stylesheet" type="text/css" />--%>
    <script src="../scripts/jquery-1.11.0.min.js" type="text/javascript"></script>
    <link href="../Styles/chosen.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/chosen.css" rel="stylesheet" type="text/css" />
    <script src="../scripts/chosen.jquery.js" type="text/javascript"></script>
    <script src="../CommonScripts/dropdownlist.js" type="text/javascript"></script>
  <%--  <link href="../Styles/Font.css" rel="stylesheet" type="text/css" />--%>
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/mastervalidations.js" type="text/javascript"></script>
    <script src="../JQueryValidations/DynamicEffects.js" type="text/javascript"></script>

    <script type="text/javascript">
        function PassParametersToPatentPage(ClientID, Client, Address, CountryID, StateID, CityID, Pincode) {

            window.parent.GetParameters(ClientID, Client, Address, CountryID, StateID, CityID, Pincode);
            
           // window.close();
            //dhtmlmodal.close();
           // dhtmlwindow.close(this)
        }





        function drpval() {
            // var countryid = $("#ddlCountry").val();
            $("#hdnPrimeIds").val($("#ddlCountry").val());
            // var stateid = $("#ddlState").val();
            $("#hdnStateIds").val($("#ddlState").val());
            // var cityid = $("#ddlcity").val();
            $("#hdnCityIds").val($("#ddlcity").val());
            // alert(cityid);
            self.close();
        }

        function setvalues() {
        
            if ($('#ddlCountry').val() == 0) {
                $('#ddlState').trigger('change');
            }
        }

        $('#ddlCountry').change(function () {
            if ($('#ddlCountry').val() == 0) {
                
                $('#ddlState').empty();
                $('#ddlState').trigger("liszt:updated");
                $('#ddlcity').empty();
                $('#ddlcity').trigger("liszt:updated");
            }
        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <input id="hdnPrimeIds" runat="server" clientidmode="Static" type="hidden" />
        <input id="hdnStateIds" runat="server" clientidmode="Static" type="hidden" />
        <input id="hdnCityIds" runat="server" clientidmode="Static" type="hidden" />
        <table width="100%">
            <tr>
                <td align="center">
                    <asp:Label ID="lblTitle" runat="server" Text="Add Customer/Supplier Details" CssClass="heading" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <table align="center" width="100%">
                        <tr>
                            <td style="width: 25%; padding-right: 10px; text-align: right" class="labelfont">
                                Customer/Supplier Name
                            </td>
                            <td>
                                :
                            </td>
                            <td style="width: 64%; padding-left: 10px">
                                <input id="txtclientname" runat="server" maxlength="50" clientidmode="static" type="text"
                                    autocomplete="true" style="width: 40%;" class="required" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 25%; padding-right: 10px; text-align: right" class="labelfont">
                                Customer/Supplier Address
                            </td>
                            <td>
                                :
                            </td>
                            <td style="width: 64%; padding-left: 10px">
                                <%--   <input id="txtclientaddress" runat="server" maxlength="100" clientidmode="static"
                                    type="text" autocomplete="false" style="width: 40%;" cols="40" rows="5" class="required" />--%>
                         
                         <textarea id="txtclientaddress"  runat="server" maxlength="100" clientidmode="static"
                            type="text" autocomplete="false" style="width: 30%" class="required myclass" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 25%; padding-right: 10px; text-align: right" class="labelfont">
                                Country
                            </td>
                            <td>
                                :
                            </td>
                            <td style="width: 64%; padding-left: 10px">
                                <select id="ddlCountry" clientidmode="Static" onchange="JQSelectBind('ddlCountry','ddlState', 'MstState');setvalues();"
                                    runat="server" style="width: 40%" class="chzn-select required chzn-select">
                                    <option></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 25%; padding-right: 10px; text-align: right" class="labelfont">
                                State
                            </td>
                            <td>
                                :
                            </td>
                            <td style="width: 64%; padding-left: 10px">
                                <select id="ddlState" clientidmode="Static" style="width: 40%" onchange="JQSelectBind('ddlState','ddlcity', 'MstCity');"
                                    class="chzn-select required chzn-select">
                                    <option></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 25%; padding-right: 10px; text-align: right" class="labelfont">
                                City
                            </td>
                            <td>
                                :
                            </td>
                            <td style="width: 64%; padding-left: 10px">
                                <select id="ddlcity" clientidmode="Static" style="width: 40%" class="chzn-select required chzn-select">
                                    <option></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 25%; padding-right: 10px; text-align: right" class="labelfont">
                                Zip code/Postcode 
                            </td>
                            <td style="width: 1%" class="labelfont">
                                :
                            </td>
                            <td style="width: 64%; padding-left: 10px">
                                <input id="txtPincode" runat="server" clientidmode="static" type="text" maxlength="6"
                                    autocomplete="false" class="required Pincode" style="width: 40%;" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button runat="server" Text="Submit" ID="btnsubmit" OnClick="btnsubmit_Click"
                        OnClientClick="drpval();" class="btn_validate" />
                </td>
            </tr>
        </table>
    </div>
   
    </form>
    <script type="text/javascript">
        function expandTextarea(id) {
            var $element = $('.myclass').get(0);

            $element.addEventListener('keyup', function () {
                this.style.overflow = 'hidden';
                this.style.height = 0;
                this.style.height = this.scrollHeight + 'px';
            }, false);
        }

        expandTextarea('txtclientaddress');
    </script>
</body>
</html>
