﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="AvailableClientsMaster.aspx.cs" Inherits="WorkFlow_AvailableClientsMaster" %>

<%@ Register Src="~/UserControl/Requestdetaillinks.ascx" TagName="Requestlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <script type="text/javascript">

        $("#requestLink").addClass("menulink");
        $("#requesttab").addClass("selectedtab");

        function setStateValue() {
            $('#hdnStateId').val('');
            $('#hdnStateId').val($('#ddlState').val());
        }
        function setCityValue() {

            $('#hdnCityId').val('');
            $('#hdnCityId').val($('#ddlCity').val());
        }
        function setDafaultValues() {
            $('#ddlState').val($('#hdnStateId').val());
        }
        function setDafaulCitytValues() {
            $('#ddlCity').val($('#hdnCityId').val());
        }
        $(document).ready(function () {
            



            $('#ddlCountry').change();
            if ($('#hdnStateIdRead').val() != '') {
                $('#ddlState').val($('#hdnStateIdRead').val());
                $('#hdnStateId').val($('#hdnStateIdRead').val());
            }

            $('#ddlState').change();
            if ($('#hdnCityIdRead').val() != '') {
                $('#ddlCity').val($('#hdnCityIdRead').val());
                $('#hdnCityId').val($('#hdnCityIdRead').val());
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <input id="hdnStateId" clientidmode="Static" runat="server" type="hidden" />
    <input id="hdnStateIdRead" clientidmode="Static" runat="server" type="hidden" />
    <input id="hdnCityId" clientidmode="Static" runat="server" type="hidden" />
    <input id="hdnCityIdRead" clientidmode="Static" runat="server" type="hidden" />
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
    </h2>
    <div class="box tabular">
        <p>
            <label for="time_entry_issue_id">
                Customer/Supplier name<span class="required"> *</span></label>
            <input id="txtClientName" runat="server" type="text" class="required ui-autocomplete-input  "
                style="width: 220px;" maxlength="50 " autocomplete="off" role="textbox" aria-autocomplete="list"
                aria-haspopup="true" />
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id">
                Address<span class="required"> *</span></label>
            <textarea id="txtAddress" placeholder="Address" runat="server" maxlength="100" clientidmode="static"
                type="text" autocomplete="false" style="width: 60%" rows="10" class="required myclass"></textarea>
            <em></em>
        </p>
        <p>
            <label for="time_entry_activity_id">
                Country <span class="required"> *</span>
            </label>
            <select id="ddlCountry" style="width: 35%" class="chzn-select required chzn-select" clientidmode="Static"
                onchange="JQSelectBind('ddlCountry','ddlState', 'MstState');setStateValue();JQSelectBind('ddlState','ddlCity', 'MstCity');"
                runat="server">
                <option></option>
            </select>
        </p>
        <p>
            <label for="time_entry_activity_id">
                State <span class="required"> *</span>
            </label>
            <select id="ddlState" style="width: 35%" class="chzn-select required chzn-select" clientidmode="Static"
                onchange="JQSelectBind('ddlState','ddlCity', 'MstCity');setStateValue();setCityValue();"
                runat="server">
                <option></option>
            </select>
        </p>
        <p>
            <label for="time_entry_activity_id">
                City <span class="required"> *</span>
            </label>
            <select id="ddlCity" style="width: 35%" class="chzn-select required chzn-select" clientidmode="Static"
                onchange="setCityValue();" runat="server">
                <option></option>
            </select>
        </p>
        <p>
            <label for="time_entry_activity_id">
                Zip code/Postcode  <span class="required">*</span>
            </label>
            <asp:TextBox ID="txtPinCode" runat="server" class="required NumericOnly Pincode"
                MaxLength="6" Style="width: 35%" size="32" aria-autocomplete="list" aria-haspopup="true"
                autocomplete="off"></asp:TextBox>
        </p>
        <p>
            <label for="time_entry_activity_id">
                Mobile :<span class="required"> * </span>
            </label>
            <asp:TextBox ID="txtMobile" runat="server" class="required NumericOnly" MaxLength="10"
                Style="width: 35%" TabIndex="13"></asp:TextBox>
        </p>
        <p>
            <label for="time_entry_issue_id">
                Email ID<span class="required"> *</span></label>
            <input id="txtEmailID" runat="server" type="text" class="required email" style="width: 220px;"
                maxlength="50 " autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" />
            <em></em>
        </p>
    </div>
     <div id="rightlinks" style="display: none;">
       <uc1:Requestlinks ID="RequestLinksID" runat="server" />
    </div>
       
   
    <asp:Button ID="btnSave" runat="server" Text="Save" class="btn_validate" OnClick="btnSave_Click" />
    <asp:Button ID="btnSaveAndContinue" runat="server" Text="Save and Add more" class="btn_validate"
        OnClick="btnSaveAndContinue_Click" />
    <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnBack_Click" />
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
</asp:Content>
