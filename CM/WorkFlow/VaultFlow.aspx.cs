﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;
using DocumentOperationsBLL;
using WorkflowBLL;
using System.Data;
using System.Data.SqlClient;
using EmailBLL;
using System.Configuration;
using System.Web.Services;
using System.Web.Security;
using ClauseLibraryBLL;
using System.IO;


public partial class Workflow_VaultFlow : System.Web.UI.Page
{
    IContractType objContract;
    IDocuSign objDocuSign;
    IClauseLibrary ObjDocumentType;
    static string filename = "";
    IVault req;
    string isCompleted = "";
    #region Permission Access attributes
    public bool LoadQuestionnaireAccess;
    public bool DownloadContractAccess;
    public bool UploadFileAccess;
    public bool CheckOutAccess;
    public bool CheckOutSendMailAccess;
    public bool CheckInAccess;
    public bool TrackChangesAccess;
    public bool EditQuestionnaireAccess;
    public bool DeleteContractFileAccess;
    public static bool TabPDFDocumentAccess;

    public bool TabActivityViAccess;

    public string TabDocumentViewAccess;
    public bool TabDocumentUploadAccess;
    public static bool TabDocumentDownloadAccess;
    public static bool TabDocumentDeleteAccess;
    #endregion
    public string PageProperty { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
      //  PageProperty = "1";
        CreateObjects();
        ddlDocumentType.extDataBind(ObjDocumentType.SelectDocumentType());
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            setAccessValues();
            ContractTypeBind();
            ViewState["RequestId"] = Request.QueryString["RequestId"];
            hdnUserID.Value = Session[Declarations.User].ToString();
         
            BindRequestDetails();
     
            ShowHidePanels();

            BindDocuSignStatus();


            if ((!string.IsNullOrEmpty(ViewState["ContractId"].ToString()) && ViewState["ContractId"].ToString() != "0") || ViewState["IsAnswered"].ToString() == "Y" )
            {
                
                hdnContractId.Value = ViewState["ContractId"].ToString();
                if (ViewState["ContractId"].ToString() != "0")
                {
                    //linkUploadFileDefault.Visible = false;
                    divEditRequest.Visible = false;
                }

                if (!string.IsNullOrEmpty(hdnSelectedFileId.Value))
                {
                    BindUpdatedQuestionnair();
                    BindContractApproval();
                }
                BindContractVersions();
            }

            //For selected Tab
            if (Request.QueryString["Status"] != null)
            {
                if (Request.QueryString["Status"].ToString() != null)
                {
                    if (Request.QueryString["Status"].ToString() == "KeyField")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "viewkeyfield", "showTab('keyfields');", true);
                    }
                    else if (Request.QueryString["Status"].ToString() == "Workflow")
                    {
                        //  ScriptManager.RegisterStartupScript(this, this.GetType(), "viewworkflow", "showTab('workflow');", true);
                    }
                    else if (Request.QueryString["Status"].ToString() == "KeyObligation")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "viewkeyobligations", "showTab('keyobligations');", true);
                    }

                }
            }
        }
    }   

    #region Methods
    private void setAccessValues()
    {
        #region Workflow Tab Permission

        ViewState["LoadQuestionnaireAccess"] = Access.isCustomised(13);
        if (ViewState["LoadQuestionnaireAccess"] != null)
        {
            LoadQuestionnaireAccess = Convert.ToBoolean(ViewState["LoadQuestionnaireAccess"].ToString());
        }
        ViewState["DownloadContractAccess"] = Access.isCustomised(14);
        if (ViewState["DownloadContractAccess"] != null)
        {
            DownloadContractAccess = Convert.ToBoolean(ViewState["DownloadContractAccess"].ToString());
        }
        ViewState["UploadFileAccess"] = Access.isCustomised(15);
        if (ViewState["UploadFileAccess"] != null)
        {
            UploadFileAccess = Convert.ToBoolean(ViewState["UploadFileAccess"].ToString());
        }
        ViewState["CheckOutAccess"] = Access.isCustomised(16);
        if (ViewState["CheckOutAccess"] != null)
        {
            CheckOutAccess = Convert.ToBoolean(ViewState["CheckOutAccess"].ToString());
        }
        ViewState["CheckOutSendMailAccess"] = Access.isCustomised(17);
        if (ViewState["CheckOutSendMailAccess"] != null)
        {
            CheckOutSendMailAccess = Convert.ToBoolean(ViewState["CheckOutSendMailAccess"].ToString());
        }
        ViewState["CheckInAccess"] = Access.isCustomised(18);
        if (ViewState["CheckInAccess"] != null)
        {
            CheckInAccess = Convert.ToBoolean(ViewState["CheckInAccess"].ToString());
        }
        ViewState["TrackChangesAccess"] = Access.isCustomised(28);
        if (ViewState["TrackChangesAccess"] != null)
        {
            TrackChangesAccess = Convert.ToBoolean(ViewState["TrackChangesAccess"].ToString());
        }

        ViewState["EditQuestionnaireAccess"] = Access.isCustomised(19);
        if (ViewState["EditQuestionnaireAccess"] != null)
        {
            EditQuestionnaireAccess = Convert.ToBoolean(ViewState["EditQuestionnaireAccess"].ToString());
        }
        ViewState["DeleteContractFileAccess"] = Access.isCustomised(22);
        if (ViewState["DeleteContractFileAccess"] != null)
        {
            DeleteContractFileAccess = Convert.ToBoolean(ViewState["DeleteContractFileAccess"].ToString());
        }

        ViewState["TabPDFDocumentAccess"] = Access.isCustomised(31);
        if (ViewState["TabPDFDocumentAccess"] != null)
        {
            TabPDFDocumentAccess = Convert.ToBoolean(ViewState["TabPDFDocumentAccess"].ToString());
        }
        #endregion

        #region Activity Tab Permission
        Access.PageAccess("WorkflowActivityTab", Session[Declarations.User].ToString());
        ViewState[Declarations.View] = Access.View.Trim();
        if (ViewState[Declarations.View].ToString() == "N")
        {
            ulActivityTab.Visible = false;
        }
        #endregion

        #region Document Tab Permission
        Access.PageAccess("WorkflowDocumentTab", Session[Declarations.User].ToString());
        ViewState[Declarations.View] = Access.View.Trim();
        if (ViewState[Declarations.View].ToString() == "N")
        {
            ulDocumentTab.Visible = false;
        }

        ViewState["TabDocumentUploadAccess"] = Access.isCustomised(20);
        if (ViewState["TabDocumentUploadAccess"] != null)
        {
            TabDocumentUploadAccess = Convert.ToBoolean(ViewState["TabDocumentUploadAccess"].ToString());
        }

        ViewState["TabDocumentDownloadAccess"] = Access.isCustomised(21);
        if (ViewState["TabDocumentDownloadAccess"] != null)
        {
            TabDocumentDownloadAccess = Convert.ToBoolean(ViewState["TabDocumentDownloadAccess"].ToString());
        }       

        ViewState["TabDocumentDeleteAccess"] = Access.isCustomised(30);
        if (ViewState["TabDocumentDeleteAccess"] != null)
        {
            TabDocumentDeleteAccess = Convert.ToBoolean(ViewState["TabDocumentDeleteAccess"].ToString());
        }
        
        #endregion
        ParentDiv.Visible = TabDocumentUploadAccess;
        //btnDisplayQuestionnair.Visible = LoadQuestionnaireAccess;
        //aPDFLatestFile.Visible = TabPDFDocumentAccess;
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objContract = FactoryMaster.GetContractTypeDetail();
            ObjDocumentType = FactoryClause.GetContractTypesDetails();
            req = FactoryWorkflow.GetVaultDetails();
            objDocuSign = FactoryAssembly.DocusignDetails();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    void ContractTypeBind()
    {
        Page.TraceWrite("Contract Type dropdown bind starts.");
        try
        {
            //ddlContractType.extDataBind(objContract.SelectData());
        }
        catch (Exception ex)
        {
            Page.TraceWarn("Contract Type dropdown bind fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Contract Type dropdown bind ends.");
    }      

    protected void ShowHidePanels()
    {
        //PendingApprovals.Visible = false;
       // DivContractVersions.Visible = true;
        DivUpdatedQuestions.Visible = false;
        //Questionnair.Visible = false;
        //QuestionnairTemplate.Visible = true;
        //DivQuestionnair.Visible = true;

        trLatestContractFileHR.Visible = false;
        //trLatestContractFileTITLE.Visible = false;
        //trLatestContractFileLINK.Visible = false;
    }   
   
    protected void BindRequestDetails()
    {
        Page.TraceWrite("BindRequestDetails started.");
        try
        {
            lblContractIDHeader.Text = "Request #" + ViewState["RequestId"].ToString();
            hdnRequestId.Value = ViewState["RequestId"].ToString();
            req.RequestID = Convert.ToInt32(ViewState["RequestId"]);
            DataSet ds = req.ReadContractDetails();
            DataTable dtRequestDetails = ds.Tables[0];
            DataTable dtRequestDocDetails = ds.Tables[1];
            DataTable dtContractTemplate = ds.Tables[2];
            DataTable dtIsAnswered = ds.Tables[3];
            DataTable dtTemplate = ds.Tables[4];
            hdnContractFileName.Value = ds.Tables[5].Rows[0][0].ToString();
            ViewState["isApprovalDone"] = ds.Tables[6].Rows[0]["IsApprovalDone"].ToString();

            if (dtRequestDetails.Rows.Count > 0)
            {
                ViewState["ContractTypeId"] = dtRequestDetails.Rows[0]["ContractTypeId"].ToString();
                hdnContractTypeId.Value = ViewState["ContractTypeId"].ToString();
                lblRequestType.Text = dtRequestDetails.Rows[0]["RequestTypeName"].ToString();
                lblRequestDetails.Text = "Added by " + dtRequestDetails.Rows[0]["FullName"].ToString() + " on " + dtRequestDetails.Rows[0]["AddedDate"].ToString();
                lblContractType.Text = dtRequestDetails.Rows[0]["ContractTypeName"].ToString();
                lblDeadLineDate.Text = dtRequestDetails.Rows[0]["DeadlineDate"].ToString();
                lblContractId.Text = dtRequestDetails.Rows[0]["ContractId"].ToString();
                if (!string.IsNullOrEmpty(lblContractId.Text))
                lblContractIDHeader.Text = "Contract #" + lblContractId.Text;// +" <label style='font-size:14px;color:black;'>(Request #" + ViewState["RequestId"].ToString() + ")</label>";
                lblRequestId.Text = " (Request #" + ViewState["RequestId"].ToString() + ")";

                lblAssignedTo.Text = dtRequestDetails.Rows[0]["AssignedTo"].ToString();
                lblEstimatedValue.Text = dtRequestDetails.Rows[0]["EstimatedValue"].ToString();
                //lblIsApprovalRequired.Text = dtRequestDetails.Rows[0]["IsApprovalRequired"].ToString();
                lblClientNames.Text = dtRequestDetails.Rows[0]["ClientName"].ToString();
                lblClientAddress.Text = dtRequestDetails.Rows[0]["ClientAddress"].ToString();
                lblClientMobile.Text = dtRequestDetails.Rows[0]["ContactNumber"].ToString();
                lblClientEmailId.Text = dtRequestDetails.Rows[0]["EmailID"].ToString();
                lblRequestDescription.Text = dtRequestDetails.Rows[0]["RequestDescription"].ToString();
                lblRequestDescription.Text = lblRequestDescription.Text.Replace("\n", "<br>");
                lblContractStatusName.Text = dtRequestDetails.Rows[0]["StatusName"].ToString();
                if (dtRequestDetails.Rows[0]["RequestTypeId"].ToString() == "5")
                    trEstimatedValue.Visible = true;
                else
                    trEstimatedValue.Visible = false;
            }

            rptRequestDocuments.DataSource = dtRequestDocDetails;
            rptRequestDocuments.DataBind();

            if (dtContractTemplate.Rows.Count > 0)
            {
                //ddlContractTemplate.DataSource = dtContractTemplate;
                //ddlContractTemplate.DataTextField = dtContractTemplate.Columns[1].ColumnName;
                //ddlContractTemplate.DataValueField = dtContractTemplate.Columns[0].ColumnName;
                //ddlContractTemplate.DataBind();
                //ddlContractTemplate.SelectedIndex = 0;
            }

            ViewState["ContractId"] = string.IsNullOrEmpty(lblContractId.Text) ? "0" : lblContractId.Text;
            ViewState["IsAnswered"] = dtIsAnswered.Rows[0]["IsAnswered"].ToString();

            if (dtTemplate.Rows.Count > 0)
            {
                hdnSelectedFileId.Value = dtTemplate.Rows[0]["ContractTemplateId"].ToString() + "#" + dtTemplate.Rows[0]["TemplateFileName"];
            }
            //hdnFileName.Value = dtTemplate.Rows[0]["TemplateFileName"].ToString();

            Page.TraceWrite("BindRequestDetails finished.");
        }
        catch (Exception ex)
        {
            Page.TraceWarn("BindRequestDetails failed.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    protected void BindUpdatedQuestionnair()
    {
        List<Vault> ListQA = new List<Vault>();
        Vault QA = new Vault();
        QA.ContractId = Convert.ToInt32(ViewState["ContractId"].ToString());
        QA.ContractTemplateId = 0;// Convert.ToInt32(ViewState["SelectedFileId"].ToString().Split('#')[0]);
        QA.ContractRequestId = Convert.ToInt32(ViewState["RequestId"]);
        ListQA = QA.ContractQuestionairDetailsForVaultFlow();

        if (ListQA.Count > 0)
        {
            divReqDescr.Visible = false;
            divReqDescrHR.Visible = false;
            divReqDescrTITLE.Visible = false;           

            lblQuestionnaireUpdated.Text = "Questionnaire Updated by " + ListQA[0].ModifiedByUserName + " " + ListQA[0].ModifiedOnFormattedDate;
            lblContractTemplateName.Text = "Contract Template : " + ListQA[0].TemplateName;
            ListQA.RemoveAt(ListQA.Count - 1);
            rptQA.DataSource = ListQA;
            rptQA.DataBind();

            DivUpdatedQuestions.Visible = true;
        }
        //Questionnair.Visible = false;
        //QuestionnairTemplate.Visible = false;
        //DivQuestionnair.Visible = false;
    }

    protected void BindContractApproval()
    {
        List<ContractProcedures> ListCPParent = new List<ContractProcedures>();
        List<ContractProcedures> ListCPChild= new List<ContractProcedures>();
        ContractProcedures CP = new ContractProcedures();
        CP.ContractId = Convert.ToInt32(ViewState["ContractId"].ToString());
        CP.ContractTemplateId = Convert.ToInt32(hdnSelectedFileId.Value.Split('#')[0]);
        CP.RequestId = Convert.ToInt32(ViewState["RequestId"].ToString());
        CP.ModifiedBy = Convert.ToInt32(Session[Declarations.User]);

        CP.PendingForApprovals();

        ListCPParent = CP.StageDetails;
        ListCPChild = CP.StageApprovalActivity;

        if (ListCPParent.Count > 0)
        {
            //rptPendingApprovals.DataSource = ListCPParent;
            //rptPendingApprovals.DataBind();
            //PendingApprovals.Visible = true;
            if (Request.QueryString["SendEmail"] == "true")
            {
                SendEmailToApprover(ListCPParent);
            }
        }
       
        //for (int i = 0; i <= rptPendingApprovals.Items.Count - 1; i++)
        //{
        //    HiddenField hdnContractApprovalID = (HiddenField)rptPendingApprovals.Items[i].FindControl("hdnContractApprovalID");
        //    Repeater rptApprovalsActivity = (Repeater)rptPendingApprovals.Items[i].FindControl("rptApprovalsActivity");
        //    var result = ListCPChild.FindAll(x => x.ContractApprovalId == int.Parse(hdnContractApprovalID.Value));
        //    rptApprovalsActivity.DataSource = result;
        //    rptApprovalsActivity.DataBind();            
        //}
    }

   

    protected void BindContractVersions()
    {
        List<ContractProcedures> ListCP = new List<ContractProcedures>();
        ContractProcedures CP = new ContractProcedures();
        CP.ContractId = Convert.ToInt32(ViewState["ContractId"].ToString());
        CP.RequestId = Convert.ToInt32(ViewState["RequestId"]);
        //CP.ContractTemplateId = Convert.ToInt32(ViewState["SelectedFileId"].ToString().Split('#')[0]);

        CP.ContractVersions();

        List<ContractProcedures> ListCPParent = new List<ContractProcedures>();
        List<ContractProcedures> ListCPChild = new List<ContractProcedures>();
        ListCPParent=CP.ContractFiles;
        ListCPChild = CP.ContractFileActivity;




        //if (ListCPParent.Count > 0)
        //{
        //    rptContractVersions.DataSource = ListCPParent;
        //    rptContractVersions.DataBind();
        //    DivContractVersions.Visible = true;
        //}
        //for (int i = 0; i <= rptContractVersions.Items.Count - 1; i++)
        //{
        //    HiddenField hdnContractFileId = (HiddenField)rptContractVersions.Items[i].FindControl("hdnContractFileId");
        //    Repeater rptContractFilesActivity = (Repeater)rptContractVersions.Items[i].FindControl("rptContractFilesActivity");
        //    var result = ListCPChild.FindAll(x => x.ContractFileId == int.Parse(hdnContractFileId.Value));
        //    rptContractFilesActivity.DataSource = result;
        //    rptContractFilesActivity.DataBind();
        //}
    }




    protected void SendEmailToApprover(List<ContractProcedures> listCP)
    {
        SendMail sm = new SendMail();
        for (int i = 0; i < listCP.Count; i++)
        {
            if (listCP[i].IsActive == 'Y')
            {
                sm.Parameters.Clear();
                sm.MailTo = listCP[i].EmailId;
                sm.MailFrom = ConfigurationManager.AppSettings["MailFrom"];
                sm.Subject = listCP[i].MailSubject;
                sm.Body = listCP[i].MailBody;
                bool IsSend = sm.SendSimpleMail();

                if (IsSend)
                {
                    ContractProcedures CP = new ContractProcedures();
                    CP.ContractApprovalId = listCP[i].ContractApprovalId;
                    string retVal = CP.UpdateRecordContractApproval();
                }
            }
        }
    }

    [WebMethod]
    public static string DeleteDocument(string DocumentID)
    {
        IClauseLibrary ObjDocumentType;
        ObjDocumentType = FactoryClause.GetContractTypesDetails();
        ObjDocumentType.ContractRequestDocumentID = DocumentID;
        string IsDeleted = ObjDocumentType.DeleteDocumentFile();
        return IsDeleted;
    }

    //Added By Nilesh For Docusign Operations

    protected void BindDocuSignStatus()
    {

        /**Updated by nilesh to get envelope id for that request****/

        objDocuSign.RequestId = int.Parse(ViewState["RequestId"].ToString());
        string EnvelopeId = objDocuSign.ReadEnvelopeId();
        if (EnvelopeId.Trim().Length != 0)
        {
           
            hdnMalAlreadySent.Value = "Y";
            GetStatuses(EnvelopeId); //Get the status of Envelope or sent document
        }
    }



    [WebMethod]
    public static string DeleteContractFile(string ContractFileId,string isFromDocuSign,string RequestId,string FileName)
    {
        IContractProcedures ObjContractProcedures;
        ObjContractProcedures = FactoryAssembly.RequestDocumentsDetails();
        ObjContractProcedures.ContractFileId= Convert.ToInt32(ContractFileId);
        ObjContractProcedures.isFromDocuSign = isFromDocuSign;
        ObjContractProcedures.RequestId =Convert.ToInt32( RequestId);
        string retval = ObjContractProcedures.DeleteContractFile();

        int pos = FileName.LastIndexOf("_") + 1;
        int FileCount=Convert.ToInt32((FileName.Substring(pos, FileName.Length - pos)));
        string Ori = FileName.Substring(0, pos);

        string OrigionalFileName = Ori + (FileCount-1).ToString();
        string NewFileName = Ori + (FileCount).ToString();

        try
        {
            string WordFilePath = HttpContext.Current.Server.MapPath(@"..//Uploads//" + HttpContext.Current.Session["TenantDIR"] + "//Contract Documents//" + OrigionalFileName + ".docx");
            string NewWordFilePath = HttpContext.Current.Server.MapPath(@"..//Uploads//" + HttpContext.Current.Session["TenantDIR"] + "//Contract Documents//" + NewFileName + ".docx");

            string PdfFilePath = HttpContext.Current.Server.MapPath(@"..//Uploads//" + HttpContext.Current.Session["TenantDIR"] + "//Contract Documents//PDF//" + OrigionalFileName + ".pdf");
            string NewPdfFilePath = HttpContext.Current.Server.MapPath(@"..//Uploads//" + HttpContext.Current.Session["TenantDIR"] + "//Contract Documents//PDF//" + NewFileName + ".pdf");

        File.Copy(WordFilePath, NewWordFilePath, true);
        File.Copy(PdfFilePath, NewPdfFilePath, true);
        }
        catch (Exception ex)
        { 
        
        
        }

        return retval;
    }

  

    [WebMethod]
    public static string BindDocumentDetails(string RequestId)
    {
        IContractProcedures ObjContractProcedures;
        ObjContractProcedures = FactoryAssembly.RequestDocumentsDetails();
        ObjContractProcedures.RequestId = Convert.ToInt32(RequestId);
        List<ContractProcedures> ListRequestDocuments = ObjContractProcedures.RequestDocumentDetails();
        string doc = "", href = "", delete = "";

        for (int i = 0; i < ListRequestDocuments.Count; i++)
        {
            if (TabDocumentDownloadAccess)
                href = @"href='../Uploads/" +HttpContext.Current.Session["TenantDIR"] + "/ContractDocs/" + ListRequestDocuments[i].ActuallFileName + "'";
            if (TabDocumentDeleteAccess)
                delete = "<a href='' class='delete' rel='nofollow' title='Delete' id='" + ListRequestDocuments[i].ContractRequestDocumentId + "' onclick='return DeleteDocument(this);'>" +
                         "<img alt='Delete' src='../images/icon-del.jpg?1349001717'></a>";
            doc += "<span>" +
                       "<h4><asp:HiddenField ID='hdnDocumentName' runat='server' ClientIDMode='Static' Value='" + ListRequestDocuments[i].ActuallFileName + "' />" +
                       "<a " + href + " class='icon icon-attachment'>" + ListRequestDocuments[i].OrigionalFileName + "</a></h4><span class='size'></span>" +
                       "-<span id='spanDocumentDelete' runat='server' clientidmode='Static'>" + delete +
                       "</span><span class='author'>" + ListRequestDocuments[i].FullName + ", " + ListRequestDocuments[i].AddedDate + "</span>" +
                       "<br>" +
                       "<em>" + ListRequestDocuments[i].DocumentTypeName + "</em> " +
                       "<br>" +
                       "<br>  " +
                   "</span>";         
        }
        return doc;
    }

    #endregion

    #region Button events 
    protected void btnDisplayQuestionnair_Click(object sender, EventArgs e)
    {
        //if (ddlContractTemplate.Value != "0" || ViewState["SelectedFileId"].ToString() != "")
        //{
        //    Server.Transfer("~/Workflow/Questionnair.aspx", true);
        //}
    }

    protected void EditQuestionnair_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ViewState["ContractId"].ToString()))
        {
            //GetDynamicControls();
            Server.Transfer("~/Workflow/Questionnair.aspx", true);
        }
        //else
        //    Questionnair.Visible = true;
        //if (ViewState["ContractId"].ToString() == "0")
        //    btnSaveDraft.Visible = true;
        //else
        //    btnSaveDraft.Visible = false;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Server.Transfer("~/WorkFlow/VaultData.aspx", false);
    }

    protected void linkUploadFileDefault_Click(object sender, EventArgs e)
    {
        Response.Redirect("UploadContractFile.aspx?RequestId=" + ViewState["RequestId"] + "&ContractId=" + ViewState["ContractId"] + "&NextFileName=" + hdnContractFileName.Value + "&ContractTypeId=" + ViewState["ContractTypeId"] + "&ContractTemplateId=" + hdnSelectedFileId.Value.Split('#')[0]);
    }

    #endregion

    #region Repeater events
    protected void rptPendingApprovals_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {            
            HtmlInputHidden hdnApproverLoginId = (HtmlInputHidden)e.Item.FindControl("hdnApproverLoginId");
            HtmlInputHidden hdnIsActive = (HtmlInputHidden)e.Item.FindControl("hdnIsActive");
            HtmlAnchor aLinkToApproval = (HtmlAnchor)e.Item.FindControl("aLinkToApproval");
            HtmlAnchor aLinkToApproval_Second = (HtmlAnchor)e.Item.FindControl("aLinkToApproval_Second");
            HtmlGenericControl ApprovalJournalLink = (HtmlGenericControl)e.Item.FindControl("ApprovalJournalLink");

            HtmlGenericControl spanStageName = (HtmlGenericControl)e.Item.FindControl("spanStageName");
            aLinkToApproval.HRef = aLinkToApproval.HRef + "&StageName=" + spanStageName.InnerText;
            aLinkToApproval_Second.HRef = aLinkToApproval_Second.HRef + "&StageName=" + spanStageName.InnerText;

            if (hdnApproverLoginId.Value == Session[Declarations.User].ToString() && hdnIsActive.Value=="Y")
            {
                aLinkToApproval.Visible = true;
                ApprovalJournalLink.Visible = true;
                aLinkToApproval_Second.Visible = true;
                spanStageName.Visible = false;
            }
            else
            {
                aLinkToApproval.Visible = false;
                ApprovalJournalLink.Visible = false;
                aLinkToApproval_Second.Visible = false;
                spanStageName.Visible = true;
            }
        }
    }

    protected void rptContractVersions_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HtmlGenericControl JournalLink = (HtmlGenericControl)e.Item.FindControl("JournalLink");
            HtmlAnchor linkContractVersionFile = (HtmlAnchor)e.Item.FindControl("linkContractVersionFile");
            HtmlAnchor aMSWordFile = (HtmlAnchor)e.Item.FindControl("aMSWordFile");
            HtmlAnchor aPDFFile = (HtmlAnchor)e.Item.FindControl("aPDFFile");
            LinkButton linkCheckOut = (LinkButton)e.Item.FindControl("linkCheckOut");
            LinkButton linkCheckOutSendMail = (LinkButton)e.Item.FindControl("linkCheckOutSendMail");
            LinkButton linkUploadFile = (LinkButton)e.Item.FindControl("linkUploadFile");
            LinkButton linkTrackChanges = (LinkButton)e.Item.FindControl("linkTrackChanges");
            LinkButton linkCheckIn = (LinkButton)e.Item.FindControl("linkCheckIn");
            LinkButton linkElectronicSignature = (LinkButton)e.Item.FindControl("linkElectronicSignature");
            
            HiddenField hdnIsCheckOutDone = (HiddenField)e.Item.FindControl("hdnIsCheckOutDone");
            HiddenField hdnIsIsFromDocuSign = (HiddenField)e.Item.FindControl("hdnIsFromDocuSign");
            HiddenField hdnFileName = (HiddenField)e.Item.FindControl("hdnFileName");
            HtmlGenericControl spanDeleteContractVersion = (HtmlGenericControl)e.Item.FindControl("spanDeleteContractVersion");
            
            linkTrackChanges.Visible = false;

            aMSWordFile.Visible = DownloadContractAccess;
            aPDFFile.Visible = TabPDFDocumentAccess;

                if (e.Item.ItemIndex == 0)
                {
                    JournalLink.Visible = true;
                    spanDeleteContractVersion.Visible = false;

                     if (DownloadContractAccess)
                    {
                        //Updated by Nilesh to show Docusign generated file

                        if (hdnIsIsFromDocuSign.Value == "Y")
                        {
                            spanDeleteContractVersion.Visible = true;
                            linkCheckOut.Style.Add("display", "none");
                            linkCheckOutSendMail.Style.Add("display", "none");
                            linkUploadFile.Style.Add("display", "none");
                            linkTrackChanges.Style.Add("display", "none");
                            linkCheckIn.Style.Add("display", "none");
                            linkElectronicSignature.Style.Add("display", "none");
                            aMSWordFile.Visible = false;
                            aPDFFile.HRef = @"../Uploads/" + HttpContext.Current.Session["TenantDIR"] + "/Contract Documents/SignedDocuments/" + hdnFileName.Value + ".pdf";
                            //aPDFLatestFile.HRef = @"../Uploads/Contract Documents/SignedDocuments/" + hdnFileName.Value + ".pdf";
                            //aMSWordLatestFile.Visible = false;
                        }
                        else
                        {
                            //aMSWordLatestFile.Visible = true;
                            //aMSWordLatestFile.HRef = @"../Uploads/Contract Documents/" + hdnFileName.Value + ".docx";
                            //aPDFLatestFile.HRef = @"../Uploads/Contract Documents/PDF/" + hdnFileName.Value + ".pdf";
                        }
                    
                    }

                    //linkLatestContractVersionFile.InnerText = hdnFileName.Value;
                     //trLatestContractFileTITLE.Visible = true;
                     //trLatestContractFileLINK.Visible = true;

                    trLatestContractFileHR.Visible = true;
                  

                    if (ViewState["isApprovalDone"].ToString() == "Y")
                    {
                        if (hdnIsCheckOutDone.Value == "Y")
                        {
                           // aEditQuestionnair.Visible = false;
                            linkElectronicSignature.Visible = true;
                            //DivQuestionnair.Visible = false;
                            linkCheckOut.Visible = false;
                            linkCheckOutSendMail.Visible = false;
                            linkTrackChanges.Visible = false;

                            if (UploadFileAccess)
                                linkUploadFile.Visible = true;
                            else
                                linkUploadFile.Visible = false;

                            if (CheckInAccess)
                                linkCheckIn.Visible = true;
                            else
                                linkCheckIn.Visible = false;
                        }
                        else
                        {
                            linkCheckIn.Visible = false;

                            //if (EditQuestionnaireAccess)
                            //    aEditQuestionnair.Visible = true;
                            //else
                            //    aEditQuestionnair.Visible = false;

                            if (CheckOutAccess)
                                linkCheckOut.Visible = true;
                            else
                                linkCheckOut.Visible = false;

                            if (CheckOutSendMailAccess)
                                linkCheckOutSendMail.Visible = true;
                            else
                                linkCheckOutSendMail.Visible = false;

                            if (UploadFileAccess)
                                linkUploadFile.Visible = true;
                            else
                                linkUploadFile.Visible = false;

                            //if ((((List<ContractProcedures>)(rptContractVersions.DataSource))).Count > 1)
                            //{
                            //    if (TrackChangesAccess)
                            //        linkTrackChanges.Visible = true;
                            //    else
                            //        linkTrackChanges.Visible = false;
                            //}
                        }
                    }
                    else
                    {
                        linkCheckOut.Visible = false;
                        linkCheckOutSendMail.Visible = false;
                        linkCheckIn.Visible = false;
                        linkTrackChanges.Visible = false;

                        //if (EditQuestionnaireAccess)
                        //    aEditQuestionnair.Visible = true;
                        //else
                        //    aEditQuestionnair.Visible = false;

                        if (UploadFileAccess)
                            linkUploadFile.Visible = true;
                        else
                            linkUploadFile.Visible = false;
                    }
                }
                else
                {
                    JournalLink.Visible = false;
                    spanDeleteContractVersion.Visible = DeleteContractFileAccess;
                }
        }
    }
    
    protected void rptContractVersions_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        HiddenField hdnContractFileId = (HiddenField)e.Item.FindControl("hdnContractFileId");
        HiddenField hdnFileName = (HiddenField)e.Item.FindControl("hdnFileName");
        string ID = ((LinkButton)e.CommandSource).ID;
        if (ID == "linkCheckIn" || ID == "linkCheckOut")
        {
            IContractProcedures objContractProcedures;
            objContractProcedures = new ContractProcedures();
            objContractProcedures.ContractFileId = Convert.ToInt32(hdnContractFileId.Value);
            objContractProcedures.ContractFileActivityTypeId = ID == "linkCheckIn" ? 1 : 2;
            objContractProcedures.IsMailSent = 'N';
            objContractProcedures.ModifiedBy = Convert.ToInt32(Session[Declarations.User]);
            objContractProcedures.IpAddress = Session[Declarations.IP].ToString();
            string retVal = objContractProcedures.FileActivityInsertRecord();
            if (retVal == "1")
            {
                BindContractVersions();
                string Mode;
                if (ID == "linkCheckIn")
                    Mode = "CheckIn";
                else
                    Mode = "CheckOut";
                //Page.JavaScriptClientScriptBlock(Mode, "showMessage('ContractVersions','" + Mode + "')");
                Server.Transfer("VaultFlow.aspx?Status=Workflow&Section=ContractVersions&Mode=" + Mode + "&ScrollTo=DivContractVersions&RequestId=" + ViewState["RequestId"]);
            }
        }
        else if (ID == "linkCheckOutSendMail")
        {
            Response.Redirect("CheckOutAndSendEmail.aspx?RequestId=" + ViewState["RequestId"] + "&ContractId=" + ViewState["ContractId"] + "&FileName=" + hdnFileName.Value + "&ContractFileId=" + hdnContractFileId.Value);            
        }
        else if (ID == "linkUploadFile")
        {
            Response.Redirect("UploadContractFile.aspx?RequestId=" + ViewState["RequestId"] + "&ContractId=" + ViewState["ContractId"] + "&NextFileName=" + hdnContractFileName.Value + "&ContractTypeId=" + ViewState["ContractTypeId"] + "&ContractTemplateId=" + hdnSelectedFileId.Value.Split('#')[0]);
        }
        else if (ID == "linkTrackChanges")
        {
            Response.Redirect("TrackChanges.aspx?rid=" + ViewState["RequestId"]);
        }
        else if (ID == "linkElectronicSignature")
        {
            Response.Redirect("../DocuSign/SendDocument.aspx?RequestId=" + ViewState["RequestId"] + "&ContractId=" + ViewState["ContractId"] + "&FileName=" + hdnFileName.Value + "&ContractFileId=" + hdnContractFileId.Value+"&ClientName="+lblClientNames.Text+"&ClientEmail="+lblClientEmailId.Text+"&isMessageAlreadySent="+hdnMalAlreadySent.Value);
        }
    }
  
    protected void rptRequestDocuments_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdnDocumentName = (HiddenField)e.Item.FindControl("hdnDocumentName");
            HtmlAnchor alinkWorkflowDocument = (HtmlAnchor)e.Item.FindControl("alinkWorkflowDocument");
            HtmlGenericControl spanDocumentDelete = (HtmlGenericControl)e.Item.FindControl("spanDocumentDelete");            
            if (TabDocumentDownloadAccess)
                alinkWorkflowDocument.HRef = @"../Uploads/" + HttpContext.Current.Session["TenantDIR"] + "/ContractDocs/" + hdnDocumentName.Value;
            spanDocumentDelete.Visible = TabDocumentDeleteAccess;
        }
    }


    #endregion


    #region Document Signature Routines Added By Nilesh

    protected void GetStatuses(string EnvelopeId)
    {
        var client =  objDocuSign.CreateAPIProxy();
        var DD = EnvelopeId;
        try
        {
            // Get all the envelope IDs which we have created in this session
            var filter = new ClauseLibraryBLL.DocuSignAPI.EnvelopeStatusFilter
            {
                AccountId = System.Configuration.ConfigurationManager.AppSettings["DocuSignAPIAccountId"],
                EnvelopeIds = EnvelopeId.Trim().Split(',')
            };
            if (filter.EnvelopeIds.Length > 0)
            {
                // Request all the envelope statuses based on that filter
                ClauseLibraryBLL.DocuSignAPI.FilteredEnvelopeStatuses statuses = client.RequestStatusesEx(filter);

                CreateStatusTable(statuses);
            }
        }
        catch (Exception ex)
        {

        //    GoToErrorPage(ex.Message);

        }

    }


    protected void CreateStatusTable(ClauseLibraryBLL.DocuSignAPI.FilteredEnvelopeStatuses statuses)
    {
        foreach (ClauseLibraryBLL.DocuSignAPI.EnvelopeStatus status in statuses.EnvelopeStatuses)
        {
            var containerDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
            var info = new System.Web.UI.HtmlControls.HtmlGenericControl("p")
            {
                InnerHtml =
                    "<a href=\"javascript:toggle('" + status.EnvelopeID + "_Detail" +
                    "');\"><img src=\"images/plus.png\"></a> " + status.Subject + " (" + status.Status.ToString() +
                    ") - " + status.EnvelopeID
            };
            containerDiv.Controls.Add(info);
            System.Web.UI.HtmlControls.HtmlGenericControl envelopeDetail = CreateEnvelopeTable(status);
            envelopeDetail.Attributes[Keys.Class] = "detail";
            envelopeDetail.Attributes[Keys.Id] = status.EnvelopeID + "_Detail";
            containerDiv.Controls.Add(envelopeDetail);
            var tr = new System.Web.UI.HtmlControls.HtmlTableRow();
            var tc = new System.Web.UI.HtmlControls.HtmlTableCell();
            tc.Controls.Add(containerDiv);
            tr.Cells.Add(tc);
            //statusTable.Rows.Add(tr);
        }

    }

    protected System.Web.UI.HtmlControls.HtmlGenericControl CreateEnvelopeTable(ClauseLibraryBLL.DocuSignAPI.EnvelopeStatus status)
    {
        var envelopeDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("div");

        int recipIndex = 0;
        int CounterCompleted = 0;

        foreach (ClauseLibraryBLL.DocuSignAPI.RecipientStatus recipient in status.RecipientStatuses)
        {
            //Updated by Nilesh for Document Signature
            List<DocuSign> lstRecipeintStatus=new List<DocuSign>();
            string Status = "Sent";
            if (recipient.Status == ClauseLibraryBLL.DocuSignAPI.RecipientStatusCode.Completed)
            {
                Status = "Completed";
            }
            else if (recipient.Status == ClauseLibraryBLL.DocuSignAPI.RecipientStatusCode.Created)
            {
                Status = "Created";
            }
            else if (recipient.Status == ClauseLibraryBLL.DocuSignAPI.RecipientStatusCode.Declined)
            {
                Status = "Declined";
            }
            else if (recipient.Status == ClauseLibraryBLL.DocuSignAPI.RecipientStatusCode.Delivered)
            {
                Status = "Delivered";
            }
            else if (recipient.Status == ClauseLibraryBLL.DocuSignAPI.RecipientStatusCode.Sent)
            {
                Status = "Sent";
            }
            else if (recipient.Status == ClauseLibraryBLL.DocuSignAPI.RecipientStatusCode.Signed)
            {
                Status = "Signed";
            }

            lstRecipeintStatus.Add(new DocuSign() { Name = recipient.UserName, Email = recipient.Email, EnvelopeID = status.EnvelopeID, Status = Status });
            foreach (var item in lstRecipeintStatus)
            {
                objDocuSign.Name = item.Name;
                objDocuSign.Email = item.Email;
                objDocuSign.EnvelopeID = item.EnvelopeID;
                objDocuSign.DocStatus = item.Status;
                try
                {
                if (item.Status == "Completed")
                {
                    CounterCompleted++;

                }

                objDocuSign.UpdateDetails();
                }
                catch (Exception ex)
                {
                    //Handle FIle Download
                }
       
            }

            var info = new System.Web.UI.HtmlControls.HtmlGenericControl("p");

            String recipId = "Recipient_Detail_" + status.EnvelopeID + "_" + recipient.RoutingOrder + "_" + recipient.UserName + "_" + recipient.Email + "_" + recipIndex++;

            info.InnerHtml = " Recipient - " +
                recipient.UserName + ": " + recipient.Status.ToString();
            if (recipient.Status != ClauseLibraryBLL.DocuSignAPI.RecipientStatusCode.Completed && null != recipient.ClientUserId)
            {
                info.InnerHtml += " <input type=\"submit\" id=\"" + status.EnvelopeID + "\" value=\"Start Signing\" name=\"DocEnvelope+" + status.EnvelopeID + "&Email+" + recipient.Email + "&UserName+" +
                    recipient.UserName + "&CID+" + recipient.ClientUserId + "\">";
            }


            envelopeDiv.Controls.Add(info);
        }


        if (status.RecipientStatuses.Count()==CounterCompleted)
        {
                ClauseLibraryBLL.DocuSignAPI.APIServiceSoapClient client=  objDocuSign.CreateAPIProxy();
                try
                {
                    // Download all documents as one pdf
                    ClauseLibraryBLL.DocuSignAPI.EnvelopePDF pdf = client.RequestPDF(status.EnvelopeID);
                    string PathToStore = HttpContext.Current.Server.MapPath(@"..//Uploads//" + HttpContext.Current.Session["TenantDIR"] + "//Contract Documents//SignedDocuments//");
                    string FileName = PathToStore + hdnContractFileName.Value.ToString() + ".pdf";
                
                        System.IO.File.WriteAllBytes(FileName, pdf.PDFBytes);
                        //Insert New Generated file entry in Contract Files Table.

                        ContractProcedures CP = new ContractProcedures();
                        FileInfo file = new FileInfo(FileName);
                        CP.RequestId = Convert.ToInt32(ViewState["RequestId"]);
                        CP.FileName = hdnContractFileName.Value;
                        CP.FileSizeKB = Math.Round(Convert.ToDecimal(file.Length) / 1024, 2).ToString();
                        CP.ModifiedBy = Convert.ToInt32(Session[Declarations.User].ToString());
                        CP.IpAddress = Session[Declarations.IP].ToString();
                        CP.isFromDocuSign = "Y";
                        string result = CP.InsertRecord();
                        isCompleted = "Y";
                        hdnMalAlreadySent.Value = "Y";                      
                }
                catch (Exception ex)
                {
                    //Handle FIle Download
                }        
        }

        var documents = new System.Web.UI.HtmlControls.HtmlGenericControl("p")
        {
            InnerHtml =
                "<a href=\"javascript:toggle('" + status.EnvelopeID + "_Detail_Documents" +
                "');\"><img src=\"images/plus.png\"></a> Documents"
        };

        if (status.Status == ClauseLibraryBLL.DocuSignAPI.EnvelopeStatusCode.Completed)
        {
            documents.InnerHtml += " <input type=\"submit\" id=\"" + status.EnvelopeID + "\" value=\"Download\" name=\"" + status.EnvelopeID + "\";>";
 
        }
        envelopeDiv.Controls.Add(documents);
        if (null != status.DocumentStatuses)
        {
            var documentDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
            foreach (var info in status.DocumentStatuses.Select(document => new System.Web.UI.HtmlControls.HtmlGenericControl("p") { InnerHtml = document.Name }))
            {
                documentDiv.Controls.Add(info);
            }
            documentDiv.Attributes[Keys.Id_Lower] = status.EnvelopeID + "_Detail_Documents";
            documentDiv.Attributes[Keys.Class] = "detail";
            envelopeDiv.Controls.Add(documentDiv);
        }
        return envelopeDiv;
    }

#endregion

}

