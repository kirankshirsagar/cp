﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;
using WorkflowBLL;

public partial class Workflow_activities : System.Web.UI.Page
{
    string flg = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        flg = Request.QueryString["MessDisplay"];
        Message();
        //Session["RequestId"];
    }

    void Message()
    {
        if (flg != null && flg != "")
        {
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }
    }
}