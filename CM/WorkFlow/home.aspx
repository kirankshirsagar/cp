﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="home.aspx.cs" Inherits="Workflow_home" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#homenewLink").addClass("menulink");
            $("#hometab").addClass("selectedtab");
        });        

    </script>
    <style>
        .whiteheading
        {
            color: #ffffff;
            font-size: 15px;
        }
        
        .greyheading
        {
            color: #919191;
            font-size: 15px;
        }
    </style>
    <%--<a id="activityID" href="#">popoup</a>--%>
    <table width="96%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr valign="top">
            <td width="33%" style="padding-bottom: 10px;">
                <table id="myDashboardTable" width="96%" border="0" cellspacing="0" cellpadding="0">
                    <tr bgcolor="#814997" align="right">
                        <td height="120">
                            <a href="#">
                                <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                    <tr align="center">
                                        <td height="94">
                                            <img src="../images/metro-btn-icon-1.png">
                                        </td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td height="22">
                                            <span class="whiteheading">My Dashboard</span>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="33%" style="padding-bottom: 10px;">
                <table id="myClientsTable" width="96%" border="0" cellspacing="0" cellpadding="0">
                    <tr bgcolor="#153259" align="right">
                        <td height="120">
                            <a href="#">
                                <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                    <tr align="center">
                                        <td height="94">
                                            <img src="../images/metro-btn-icon-2.png">
                                        </td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td height="22">
                                            <span class="whiteheading">My Customers and Suppliers</span>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="33%" style="padding-bottom: 10px;">
                <table id="myReportsTable" width="96%" border="0" cellspacing="0" cellpadding="0">
                    <tr bgcolor="#289AA1" align="right">
                        <td height="120">
                            <a  id='rptReportaCC' style="cursor:pointer">
                                <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                    <tr align="center">
                                        <td height="94">
                                            <img src="../images/metro-btn-icon-3.png">
                                        </td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td height="22">
                                            <span class="whiteheading">My Reports</span>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr valign="top">
            <td width="33%" style="padding-bottom: 10px;">
                <table id="myAutoRenewalTable" width="96%" border="0" cellspacing="0" cellpadding="0">
                    <tr bgcolor="#3C8747" align="right">
                        <td height="120">
                            <a href="#">
                                <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                    <tr align="center">
                                        <td height="94">
                                            <img src="../images/metro-btn-icon-4.png">
                                        </td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td height="22">
                                            <span class="whiteheading">My Vault</span>
                                            <%--old -- Auto renewal--%>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="33%" style="padding-bottom: 10px;">
                <table id="myCalenderTable" width="96%" border="0" cellspacing="0" cellpadding="0">
                    <tr bgcolor="#DE6528" align="right">
                        <td height="120">
                            <a href="#">
                                <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                    <tr align="center">
                                        <td height="94">
                                            <img src="../images/metro-btn-icon-5.png">
                                        </td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td height="22">
                                            <span class="whiteheading">My Calendar</span>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="33%" style="padding-bottom: 10px;">
                <table id="myContractsTable" width="96%" border="0" cellspacing="0" cellpadding="0">
                    <tr bgcolor="#814997" align="right">
                        <td height="120">
                            <a href="#">
                                <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                    <tr align="center">
                                        <td height="94">
                                            <img src="../images/metro-btn-icon-6.png">
                                        </td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td height="22">
                                            <span class="whiteheading">My Contracts</span>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr valign="top">
            <td width="33%" style="padding-bottom: 10px;">
                <table id="myRenewContractTable" width="96%" border="0" cellspacing="0" cellpadding="0">
                    <tr bgcolor="#153259" align="right">
                        <td height="120">
                            <a href="#">
                                <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                    <tr align="center">
                                        <td height="94">
                                            <img src="../images/metro-btn-icon-7.png">
                                        </td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td height="22">
                                            <span class="whiteheading">Renew Contract</span>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="33%" style="padding-bottom: 10px;">
                <table id="myReviseContractTable" width="96%" border="0" cellspacing="0" cellpadding="0">
                    <tr bgcolor="#289AA1" align="right">
                        <td height="120">
                            <a href="#">
                                <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                    <tr align="center">
                                        <td height="94">
                                            <img src="../images/metro-btn-icon-8.png">
                                        </td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td height="22">
                                            <span class="whiteheading">Revise Contract</span>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="33%" style="padding-bottom: 10px;">
                <table id="myTerminateContractTable" width="96%" border="0" cellspacing="0" cellpadding="0">
                    <tr bgcolor="#3C8747" align="right">
                        <td height="120">
                            <a href="#">
                                <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                    <tr align="center">
                                        <td height="94">
                                            <img src="../images/metro-btn-icon-9.png">
                                        </td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td height="22">
                                            <span class="whiteheading">Terminate Contract</span>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr valign="top">
            <td width="33%" style="padding-bottom: 10px;">
                <table id="myCreateNewContractTable" width="96%" border="0" cellspacing="0" cellpadding="0">
                    <tr bgcolor="#DE6528" align="right">
                        <td height="120">
                            <a href="#">
                                <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                    <tr align="center">
                                        <td height="94">
                                            <img src="../images/metro-btn-icon-10.png">
                                        </td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td height="22">
                                            <span class="whiteheading">Generate Contract</span>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="33%" style="padding-bottom: 10px;">
                <table id="myVaultTable" width="96%" border="0" cellspacing="0" cellpadding="0">
                    <tr bgcolor="#814997" align="right">
                        <td height="120">
                            <a href="#">
                                <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                    <tr align="center">
                                        <td height="94">
                                            <img src="../images/metro-btn-icon-11.png">
                                        </td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td height="22">
                                            <span class="whiteheading">Add Contract Record</span>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="33%" style="padding-bottom: 10px;">
                <table id="myCreateNewReviewRequestTable" width="96%" border="0" cellspacing="0"
                    cellpadding="0">
                    <tr bgcolor="#153259" align="right">
                        <td height="120">
                            <a href="#">
                                <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                    <tr align="center">
                                        <td height="94">
                                            <img src="../images/metro-btn-icon-12.png">
                                        </td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td height="22">
                                            <span class="whiteheading">Contract Review Request</span>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
   <script type="text/javascript">

       $(window).load(function () {

           if ($('#hdnMyGlobalDashboardTable').val() == 'N') {
               setAccess('myDashboardTable');
           }
           else {
               setLink('myDashboardTable', '../Dashboard/Dasboard.aspx');
           }


           if ($('#hdnMyGlobalContractsTable').val() == 'N') {
               setAccess('myContractsTable');
           }
           else {

               setLink('myContractsTable', '../Workflow/ContractRequestData.aspx');
           }



           if ($('#hdnMyGlobalCreateNewContractTable').val() == 'N') {
               setAccess('myCreateNewContractTable');
           }
           else {
               setLink('myCreateNewContractTable', 'ContractRequest.aspx?ch=cnc');
           }

           if ($('#hdnMyGlobalCreateNewReviewRequestTable').val() == 'N') {
               setAccess('myCreateNewReviewRequestTable');
           } else {
               setLink('myCreateNewReviewRequestTable', 'ContractRequest.aspx?ch=nrr');
           }

           if ($('#hdnMyGlobalClientsTable').val() == 'N') {
               setAccess('myClientsTable');
           } else {
               setLink('myClientsTable', 'AvailableClientsData.aspx');
           }

           if ($('#hdnMyGlobalRenewContractTable').val() == 'N') {
               setAccess('myRenewContractTable');
           }
           else {
               setLink('myRenewContractTable', 'ContractRequest.aspx?ch=ren');
           }

           if ($('#hdnMyGlobalReviseContractTable').val() == 'N') {
               setAccess('myReviseContractTable');
           }
           else {
               setLink('myReviseContractTable', 'ContractRequest.aspx?ch=rev');
           }
           if ($('#hdnMyGlobalAddContractTable').val() == 'N') {
               setAccess('myVaultTable');
           }
           else {
               setLink('myVaultTable', '../BulkImport/BulkContractTemplateData.aspx');
           }
           if ($('#hdnMyGlobalTerminateContractTable').val() == 'N') {
               setAccess('myTerminateContractTable');
           }
           else {
               setLink('myTerminateContractTable', 'ContractRequest.aspx?ch=tc');
           }

           if ($('#hdnMyGlobalVaultTable').val() == 'N') {
               setAccess('myAutoRenewalTable');
           }
           else {
                //setLink('myAutoRenewalTable', '../Masters/ContractTypeMasterData.aspx');
              setLink('myAutoRenewalTable', $('#hdnMyGlobalVaultHref').val());
           }
           if ($('#hdnMyGlobalCalenderTable').val() == 'N') {
               setAccess('myCalenderTable');
           }
           else {
               setLink('myCalenderTable', '../Calendar/Calendar.aspx');
           }
           if ($('#hdnMyGlobalReportsTable').val() == 'N') {
               setAccess('myReportsTable');
           }
           else {
               $('#rptReportaCC').attr('onclick', 'GoToReport()');
           }

       });


       function setAccess(objTable) {
           try {

               //$('#' + objTable).closest('a').removeAttr('href');
               $('#' + objTable).find('td:eq(0) a').removeAttr('href');
               $('#' + objTable).find('tr').attr('bgcolor', '#cccccc');
               $('#' + objTable).find('img').attr('src', '../images/access_point_lock_open.png');
               $('#' + objTable).find('span').removeClass('whiteheading').addClass('greyheading')
           }
           catch (e) {

           }
       }


       function setLink(objTable, link) {
           try {
               $('#' + objTable).find('td:eq(0) a').attr('href', link);
           }
           catch (e) {

           }
       }




    </script>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
       
    </script>
    <div style="display: none">
        <asp:DropDownList ID="ddlReplist" ClientIDMode="Static" Width="100px" runat="server">
        </asp:DropDownList>
        <asp:Button ID="btnReportAction" runat="server" ClientIDMode="Static" OnClick="btnReportAction_Click" />
    </div>
    <script type="text/javascript" language="javascript">

        function GoToReport() {
            $("#btnReportAction").click();
        }
    </script>
</asp:Content>
