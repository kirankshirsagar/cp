﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="UploadContractFile.aspx.cs" Inherits="WorkFlow_UploadContractFile" %>

<%@ Register Src="../UserControl/requestnewlinks.ascx" TagName="requestnewlinks"
    TagPrefix="uc2" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%--<link href="../Styles/ThemeBlue.css" rel="stylesheet" type="text/css" />--%>
    <%--<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>--%>
    <%--<link rel="stylesheet" href="/resources/demos/style.css"/>--%>
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
    <%--<script src="http://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js" type="text/javascript"></script> --%>
    <script src="jquery-1.10.2.js" type="text/javascript"></script>
    <script src="jquery-ui.js" type="text/javascript"></script>
    <link href="jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/mastervalidations.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
    <style>
        .progressbar
        {
            width: 500px;
            height: 21px;
        }
        .progressbarlabel
        {
            width: 300px;
            height: 21px;
            position: absolute;
            text-align: center;
            font-size: small;
        }
        textarea
        {
            width: 474px;
            height: 76px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#requestLink").addClass("menulink");
            $("#requesttab").addClass("selectedtab");
            $('textarea').autogrow();
            //$("#chkIsFinalSignedDocument").css("margin-left", "1px");
        });    
    </script>
    <script type="text/javascript">
        function StopProgress() {
            $("div.loading").hide();
            $("div.modal").hide();
        }

        var isError = 0;
        $(document).ready(function () {
            $("#btnUpload").bind('click', function (evt) {
                var ShowProgressbar = false;
                if ($("#FileUpload1").next("div.tooltip_outer:visible").text() == "" && $("#txtRemark").next("div.tooltip_outer:visible").text() == "") {

                    var xhr = new XMLHttpRequest();
                    var data = new FormData();
                    var files = $("#FileUpload1").get(0).files;

                    if (files.length > 0) {
                        var ext = files[0].name.substr(files[0].name.lastIndexOf('.')).toUpperCase();

                        if (ext != ".DOC" && ext != ".DOCX" && ext != ".PDF") {
                            StopProgress();
                            MessageMasterDiv('Invalid Contract File.', 1, 100);
                        }
                        else {
                            ShowProgress();
                            for (var i = 0; i < files.length; i++) {
                                data.append(files[i].name, files[i]);
                            }
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    var progress = Math.round(evt.loaded * 100 / evt.total);
                                    $("#progressbar").progressbar("value", progress);
                                }
                                else {
                                    MessageMasterDiv('File size exceeding limit.', 1, 100);
                                }
                            }, false);

                            var IsAI = $("#chkAI").prop("checked");
                            var isFromDocuSign = $("#chkIsFinalSignedDocument").prop("checked");
                            var Remark = $("#txtRemark").val().replace(/\n/g, "<br/>");
                            var RequestId = '<%=ViewState["RequestId"].ToString() %>';
                            var NextFileName = '<%=ViewState["NextFileName"].ToString() %>';
                            var User = '<%=ViewState["User"].ToString() %>';
                            var IP = '<%=ViewState["IP"].ToString() %>';
                            var ContractTypeId = '<%=ViewState["ContractTypeId"].ToString() %>';
                            var TemplateId = '<%=ViewState["ContractTemplateId"].ToString() %>';
                            var IsApprovalDone = '<%=ViewState["IsApprovalDone"].ToString() %>';
                            var ParentRequestID = '<%=ViewState["ParentRequestID"].ToString() %>';

                            var TenantDIR = '<%=Session["TenantDIR"].ToString() %>';

                            var IsOCR = $("#chkIsOCR").prop("checked");

                            xhr.open("POST", "UploadHandler.ashx?RequestId=" + RequestId + "&NextFileName=" + escape(NextFileName) + "&Remark=" + Remark + "&isFromDocuSign=" + isFromDocuSign + "&LoginUserId=" + User + "&IPAddress=" + IP + "&ContractTypeId=" + ContractTypeId + "&ContractTemplateId=" + TemplateId + "&IsApprovalDone=" + IsApprovalDone + ParentRequestID + "&IsOCR=" + IsOCR + "&IsAI=" + IsAI, true);
                            xhr.send(data);
                            xhr.onload = function () {
                                if (xhr.responseText.toString().split('_')[0] == "1") {
                                    // MessageMasterDiv('File uploaded.', 0, 100);
                                    $("#hdnFileName").val(xhr.responseText.toString().split('_')[1]);
                                    ShowProgressbar = true;
                                    location.href = "RequestFlow.aspx?Section=ContractVersions&ScrollTo=DivContractVersions&SendEmail=true&Status=Workflow&RequestId=" + '<%= ViewState["RequestId"]%>' + ParentRequestID;
                                }
                                else {
                                    isError = 1;
                                    StopProgress();
                                    MessageMasterDiv('Invalid Contract File.', 1, 100);
                                }
                            }
                        }
                    }
                    else {
                        MessageMasterDiv('Please select file to upload.', 1, 100);
                    }
                    evt.preventDefault();
                }
            });
        });

        function setDefault() {
            //$("#progressbar").removeClass().addClass('progressbar');
            //$("#progressbar div.progressbarlabel").text("");
            //$("#progressbar div.ui-progressbar-value").hide();
        }

        var validFilesTypes = ["doc", "docx", "pdf"];
        function ValidateFile() {

            $('#FileUpload1').css('color', '');
            var file = document.getElementById("<%=FileUpload1.ClientID%>");

            var path = file.value;
            var ext = path.substring(path.lastIndexOf(".") + 1, path.length).toLowerCase();
            if (ext == "") {

                if ($("#hdnisocr").val() == "Y")
                    $("#chkIsOCR").attr('checked', true);
                else
                    $("#chkIsOCR").attr('checked', false);

                $("#chkIsOCR").attr('disabled', 'disabled');
                return false;
            }
            var isValidFile = false;
            for (var i = 0; i < validFilesTypes.length; i++) {
                if (ext == validFilesTypes[i]) {
                    isValidFile = true;
                    if (ext.toLowerCase() == "pdf")
                        $("#chkIsOCR").removeAttr('disabled');
                    else {
                        $("#chkIsOCR").attr('disabled', 'disabled');
                        $("#chkIsOCR").attr('checked', false);
                    }
                    break;
                }
            }

            if (!isValidFile) {

                $("#chkIsOCR").attr('checked', false);
                $("#chkIsOCR").attr('disabled', 'disabled');

                alert("Invalid File. Please upload a File with" +
         " extension:\n\n" + validFilesTypes.join(", "));
            }
            return isValidFile;
        }

        function AutoExtract(obj) {
            var IsFinalSigned = $("#chkIsFinalSignedDocument").prop("checked");
            if (IsFinalSigned) {
                $("#pAI").show();
                $("#pAI").children().show();
            }
            else {
                $("#pAI").hide();
                $("#pAI").children().hide();
                $("#chkAI").prop("checked", false);
            }
        }

    </script>
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label></h2>
    <div>
        <div>
            <p>
                <a href='RequestFlow.aspx?RequestId=<%=ViewState["RequestId"].ToString() %>&Status=Workflow<%=ViewState["ParentRequestID"].ToString() %>'
                    class="issue status-1 priority-4 parent">Request #<%=ViewState["RequestId"].ToString() %></a>:
                <asp:Label ID="lblSubTitle" runat="server" Text=""></asp:Label>
        </div>
    </div>
    <div id="uploadbox">
        <div class="box" id="Div13">
            <a name="uploadboxname"></a>
            <fieldset class="tabular" style="border: 0">
                <legend></legend>
                <p>
                    <label for="issue_tracker_id">
                        <span class="required">*</span> Upload file</label>
                    <asp:FileUpload ID="FileUpload1" runat="server" AllowMultiple="true" ClientIDMode="Static"
                        class="required" onclick="return setDefault();" Onchange="return ValidateFile();" />
                    <asp:HiddenField ID="hdnFileName" runat="server" ClientIDMode="Static" />
                </p>
                <p>
                    <label for="issue_tracker_id">
                        Remark<span class="required"> </span>
                    </label>
                    <textarea id="txtRemark" class="" runat="server" style="margin-left: 3px;" clientidmode="Static"
                        maxlength="1000"></textarea>
                </p>
                <p>
                    <label for="chkIsOCR">
                        OCR</label>
                    <input type="checkbox" runat="server" clientidmode="Static" id="chkIsOCR" style="margin-top: 8px"
                        disabled="disabled" />
                </p>
                <p id="pFinalSignedCopy" runat="server" clientidmode="Static">
                    <label for="issue_tracker_id">
                        Final signed copy
                    </label>
                    <asp:CheckBox ID="chkIsFinalSignedDocument" ClientIDMode="Static" runat="server" onchange="AutoExtract(this);"
                        Style="margin-left: 0px;" />
                </p>
                <p id="pAI" runat="server" clientidmode="Static" style="display: none;">
                    <label for="issue_tracker_id" style="display: none;">
                        AI
                    </label>
                    <asp:CheckBox ID="chkAI" ClientIDMode="Static" runat="server" Style="margin-left: 0px;
                        display: none;" />
                </p>
            </fieldset>
        </div>
        <%--<asp:Button ID="btnUploadFile" runat="server" Text="Upload file" class="btn_validate" onclick="btnUploadFile_Click" />--%>
        <table>
            <tr>
                <td>
                    <asp:Button ID="btnUpload" runat="server" Text="Upload" ClientIDMode="Static" class="btn_validate" />
                    <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnBack_Click" />
                </td>
                <td>
                    <div id="progressbar" class="progressbar">
                        <div id="progresslabel" class="progressbarlabel">
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
