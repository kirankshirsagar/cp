﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using WorkflowBLL;
using UserManagementBLL;
using CommonBLL;
using System.Web.UI.HtmlControls;
using System.Data;
using MetaDataConfiguratorsBLL;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class WorkFlow_KeyFieldsMaster : System.Web.UI.Page
{
    IKeyFields objkey;
    IUsers objuser;
    IActivity objActivity;
    string LinkStatus = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        if (!IsPostBack)
        {
            //int ContarctID = Convert.ToInt32(Request.QueryString["ContractId"].ToString());
            if (Request.QueryString["LinkStatus"].ToString() != null)
            {
                hdnLinkStatus.Value = Request.QueryString["LinkStatus"].ToString();
            }
            SetDefault();
            BindUser();
            BindValues();

            lblRequestIDheader.Text = HdnRequestID.Value;
            lblReqID.Text = HdnRequestID.Value;
            BindContractRequest();

        }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);
        if (Request.Params["ctl00$MainContent$btnBack"] == null)
        {
            BindContractRequest();
        }
    }

    public void CreateObjects()
    {


        // Page.TraceWrite("Page object create starts.");
        try
        {
            objkey = FactoryWorkflow.GetKeyFieldsDetails();
            objuser = FactoryUser.GetUsersDetail();
            objActivity = FactoryWorkflow.GetActivityDetails();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        //  Page.TraceWrite("Page object create ends.");
    }

    private void SetDefault()
    {
        System.Collections.Specialized.NameValueCollection RequestFlow = Request.Form;
        ViewState["RequestId"] = RequestFlow["ctl00$MainContent$hdnRequestId"];
        HdnRequestID.Value = Convert.ToString(ViewState["RequestId"]);
        ViewState["ContractTypeId"] = RequestFlow["ctl00$MainContent$hdnContractTypeId"];
        hdnContractypeId.Value = Convert.ToString(ViewState["ContractTypeId"]);
        ViewState["ParentRequestId"] = RequestFlow["ctl00$MainContent$hdnParentRequestID"];
    }

    public void BindUser()
    {
        try
        {
            if (Session[Declarations.User] != null)
            {
                objuser.UserId = int.Parse(Session[Declarations.User].ToString());
                objuser.DepartmentId = 0;
                /* Added by kiran on 29 Mar 2016 for show the user dependendent on contract type   */
                objuser.ContractTypeId = Convert.ToInt32(ViewState["ContractTypeId"]);
                ddlexp180Users1.extDataBind(objuser.SelectAllUsers());
                ddlexp180Users2.extDataBind(objuser.SelectAllUsers());

                ddlexp90Users1.extDataBind(objuser.SelectAllUsers());
                ddlexp90Users2.extDataBind(objuser.SelectAllUsers());

                ddlexp60Users1.extDataBind(objuser.SelectAllUsers());
                ddlexp60Users2.extDataBind(objuser.SelectAllUsers());

                ddlRenewal180User1.extDataBind(objuser.SelectAllUsers());
                ddlRenewal180User2.extDataBind(objuser.SelectAllUsers());

                ddlRenewal90User1.extDataBind(objuser.SelectAllUsers());
                ddlRenewal90User2.extDataBind(objuser.SelectAllUsers());

                ddlRenewal60User1.extDataBind(objuser.SelectAllUsers());
                ddlRenewal60User2.extDataBind(objuser.SelectAllUsers());

            }
        }
        catch { }
    }

    public void BindValues()
    {
        try
        {
            List<KeyFeilds> mylist;

            objkey.RequestId = int.Parse(HdnRequestID.Value);
            mylist = objkey.ReadData();
            ViewState["ContractID"] = mylist[0].ContractID;
            txteffectivedate.Value = (mylist[0].EffectiveDatestring).ToString();
            txtexpirationdate.Value = (mylist[0].ExpirationDatestring).ToString();
            txtrenewaldate.Value = (mylist[0].RenewalDatestring).ToString();

            hdnContractID.Value = mylist[0].ContractID.ToString();

            //--Expiration  //--Renewal
            //added by js on 13-1-2016

            DataTable dtEx = (DataTable)Session["ImportDateDataEx"];
            DataTable dtRen = (DataTable)Session["ImportDateDataRen"];
            DataTable dtE = new DataTable();

            IEnumerable<DataRow> rowsrpt = dtEx.AsEnumerable()
             .Where(r => r.Field<string>("ExAlerts") == "Y");

            if (rowsrpt.Count() > 0)
                dtE = rowsrpt.CopyToDataTable();

            if (dtE.Rows.Count > 0)
            {
                try
                {
                    ddlRemindExpiration1.Value = Convert.ToString(dtE.Rows[0]["ExAlertsDay"]);
                    ddlexp180Users1.Value = Convert.ToString(dtE.Rows[0]["ExUser1"]);
                    ddlexp180Users2.Value = Convert.ToString(dtE.Rows[0]["ExUser2"]);
                    chkexp180.Checked = true;

                    ddlRemindExpiration2.Value = Convert.ToString(dtE.Rows[1]["ExAlertsDay"]);
                    ddlexp90Users1.Value = Convert.ToString(dtE.Rows[1]["ExUser1"]);
                    ddlexp90Users2.Value = Convert.ToString(dtE.Rows[1]["ExUser2"]);
                    chkexp90.Checked = true;

                    ddlRemindExpiration3.Value = Convert.ToString(dtE.Rows[2]["ExAlertsDay"]);
                    ddlexp60Users1.Value = Convert.ToString(dtE.Rows[2]["ExUser1"]);
                    ddlexp60Users2.Value = Convert.ToString(dtE.Rows[2]["ExUser2"]);
                    chkexp60.Checked = true;

                }
                catch (Exception ex) { }
            }

            DataTable dtR = new DataTable();
            IEnumerable<DataRow> rowsrpt1 = dtRen.AsEnumerable()
            .Where(r => r.Field<string>("RenAlerts") == "Y");

            if (rowsrpt1.Count() > 0)
                dtR = rowsrpt1.CopyToDataTable();

            if (dtR.Rows.Count > 0)
            {
                try
                {
                    ddlRemindRenewal1.Value = Convert.ToString(dtR.Rows[0]["RenAlertsDay"]);
                    ddlRenewal180User1.Value = Convert.ToString(dtR.Rows[0]["RenUser1"]);
                    ddlRenewal180User2.Value = Convert.ToString(dtR.Rows[0]["RenUser2"]);
                    chkrem180.Checked = true;

                    ddlRemindRenewal2.Value = Convert.ToString(dtR.Rows[1]["RenAlertsDay"]);
                    ddlRenewal90User1.Value = Convert.ToString(dtR.Rows[1]["RenUser1"]);
                    ddlRenewal90User2.Value = Convert.ToString(dtR.Rows[1]["RenUser2"]);
                    chkrem90.Checked = true;

                    ddlRemindRenewal3.Value = Convert.ToString(dtR.Rows[2]["RenAlertsDay"]);
                    ddlRenewal60User1.Value = Convert.ToString(dtR.Rows[2]["RenUser1"]);
                    ddlRenewal60User2.Value = Convert.ToString(dtR.Rows[2]["RenUser2"]);
                    chkrem60.Checked = true;

                }
                catch (Exception ex) { }
            }
        }
        catch { }
    }

    private void BindContractRequest()
    {
        try
        {
            IMetaDataConfigurators objMetaDataConfig = FactoryMedaData.GetContractTemplateDetail();
            /////Control container = objMetaDataConfig.GenerateRequestForm("Important Dates", HdnRequestID.Value,"Edit");
            HtmlTable container = objMetaDataConfig.GenerateRequestForm("Important Dates", HdnRequestID.Value, "Edit");
            ////pnlnewadd.Controls.Add(container);

            int returnTableRow = container.Rows.Count;
            int cnt = ImportantDateTable.Rows.Count;

            for (int i = 0; i < returnTableRow; i++)
            {
                ImportantDateTable.Rows.Add(container.Rows[0]);
            }

            IKeyFields objusers = FactoryWorkflow.GetKeyFieldsDetails();
            objusers.RequestId = Convert.ToInt32(HdnRequestID.Value);

            int MetafieldId1 = 0; int MetafieldId2 = 0; int MetafieldId3 = 0;
            DataTable dt = objusers.GetMetaDataDateUser();

            string ReturnStr = Convert.ToString(Session["IDFieldId"]);
            Session.Remove("IDFieldId");
            string temp = "";
            ReturnStr.Split(',').Distinct().ToList().ForEach(k => temp += k + ",");
            ReturnStr = temp.Trim(',');
            string[] words = ReturnStr.Split(',');

            if (dt.Rows.Count > 0)
            {
                if (words.Count() > 0)
                {
                    try
                    {
                        MetafieldId1 = int.Parse(words[0]);
                    }
                    catch { } try
                    {
                        MetafieldId2 = int.Parse(words[1]);
                    }
                    catch { } try
                    {
                        MetafieldId3 = int.Parse(words[2]);
                    }
                    catch { }
                }
                //var xMetaData = (from r in dt.AsEnumerable()
                //                 select r["MetaDataFieldId"]).Distinct().ToList();
                //foreach (var v in xMetaData)
                //{
                //    if (MetafieldId1 == 0)
                //        MetafieldId1 = int.Parse(v.ToString());
                //    else if (MetafieldId2 == 0)
                //        MetafieldId2 = int.Parse(v.ToString());
                //    else if (MetafieldId3 == 0)
                //        MetafieldId3 = int.Parse(v.ToString());
                //}

                try
                {
                    DataTable dtCustR1 = new DataTable();
                    DataTable dtCustR2 = new DataTable();
                    DataTable dtCustR3 = new DataTable();


                    IEnumerable<DataRow> rowsrpt = dt.AsEnumerable()
                    .Where(r => r.Field<int>("MetaDataFieldId") == MetafieldId1);

                    if (rowsrpt.Count() > 0)
                        dtCustR1 = rowsrpt.CopyToDataTable();

                    IEnumerable<DataRow> rowsrptD = dt.AsEnumerable()
                          .Where(r => r.Field<int>("MetaDataFieldId") == MetafieldId2);

                    if (rowsrptD.Count() > 0)
                        dtCustR2 = rowsrptD.CopyToDataTable();

                    IEnumerable<DataRow> rowsrptK = dt.AsEnumerable()
                          .Where(r => r.Field<int>("MetaDataFieldId") == MetafieldId3);

                    if (rowsrptK.Count() > 0)
                        dtCustR3 = rowsrptK.CopyToDataTable();

                    if (dtCustR1.Rows.Count > 0)
                    {
                        try
                        {
                            hdnRemindDay10.Value = Convert.ToString(dtCustR1.Rows[0]["ReminderDays"]);
                            hdnRemindDay1User10.Value = Convert.ToString(dtCustR1.Rows[0]["ReminderUser1"]);
                            hdnRemindDay1User20.Value = Convert.ToString(dtCustR1.Rows[0]["ReminderUser2"]);

                            hdnRemindDay20.Value = Convert.ToString(dtCustR1.Rows[1]["ReminderDays"]);
                            hdnRemindDay2User10.Value = Convert.ToString(dtCustR1.Rows[1]["ReminderUser1"]);
                            hdnRemindDay2User20.Value = Convert.ToString(dtCustR1.Rows[1]["ReminderUser2"]);

                            hdnRemindDay30.Value = Convert.ToString(dtCustR1.Rows[2]["ReminderDays"]);
                            hdnRemindDay3User10.Value = Convert.ToString(dtCustR1.Rows[2]["ReminderUser1"]);
                            hdnRemindDay3User20.Value = Convert.ToString(dtCustR1.Rows[2]["ReminderUser2"]);
                        }
                        catch { }
                    }

                    if (dtCustR2.Rows.Count > 0)
                    {
                        try
                        {
                            hdnRemindDay11.Value = Convert.ToString(dtCustR2.Rows[0]["ReminderDays"]);
                            hdnRemindDay1User11.Value = Convert.ToString(dtCustR2.Rows[0]["ReminderUser1"]);
                            hdnRemindDay1User21.Value = Convert.ToString(dtCustR2.Rows[0]["ReminderUser2"]);

                            hdnRemindDay21.Value = Convert.ToString(dtCustR2.Rows[1]["ReminderDays"]);
                            hdnRemindDay2User11.Value = Convert.ToString(dtCustR2.Rows[1]["ReminderUser1"]);
                            hdnRemindDay2User21.Value = Convert.ToString(dtCustR2.Rows[1]["ReminderUser2"]);

                            hdnRemindDay31.Value = Convert.ToString(dtCustR2.Rows[2]["ReminderDays"]);
                            hdnRemindDay3User11.Value = Convert.ToString(dtCustR2.Rows[2]["ReminderUser1"]);
                            hdnRemindDay3User21.Value = Convert.ToString(dtCustR2.Rows[2]["ReminderUser2"]);
                        }
                        catch { }
                    }

                    if (dtCustR3.Rows.Count > 0)
                    {
                        try
                        {
                            hdnRemindDay12.Value = Convert.ToString(dtCustR3.Rows[0]["ReminderDays"]);
                            hdnRemindDay1User12.Value = Convert.ToString(dtCustR3.Rows[0]["ReminderUser1"]);
                            hdnRemindDay1User22.Value = Convert.ToString(dtCustR3.Rows[0]["ReminderUser2"]);

                            hdnRemindDay22.Value = Convert.ToString(dtCustR3.Rows[1]["ReminderDays"]);
                            hdnRemindDay2User12.Value = Convert.ToString(dtCustR3.Rows[1]["ReminderUser1"]);
                            hdnRemindDay2User22.Value = Convert.ToString(dtCustR3.Rows[1]["ReminderUser2"]);

                            hdnRemindDay32.Value = Convert.ToString(dtCustR3.Rows[2]["ReminderDays"]);
                            hdnRemindDay3User12.Value = Convert.ToString(dtCustR3.Rows[2]["ReminderUser1"]);
                            hdnRemindDay3User22.Value = Convert.ToString(dtCustR3.Rows[2]["ReminderUser2"]);
                        }
                        catch { }
                    }
                }
                catch (Exception ex)
                { throw ex; }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataTable SaveData(Control pnl)
    {
        try
        {
            Control ctrl = pnl;
            DataTable dt = new DataTable();
            dt.Columns.Add("MetaDataFieldId", Type.GetType("System.Int32"));
            dt.Columns.Add("FieldValue", Type.GetType("System.String"));
            string InputValue = "", ID = "", Master = "", FieldType = "";

            int FieldTypeId = 0;
            #region GetControlValue
            HtmlTable tbl = (HtmlTable)ctrl.FindControl("ImportantDateTable");

            #region fill answers
            foreach (HtmlTableRow row in tbl.Rows)
            {
                if (row.Attributes["class"] == "trdynamic trimportantDate")
                {
                    if (row.Style.Value == null || (row.Style.Value != null && !row.Style.Value.Contains("display:none")))//(row.Visible)
                    {
                        Control parentctrl = row.Cells[1];
                        //string Question = row.Cells[0].InnerText;
                        InputValue = "";
                        foreach (var c in parentctrl.Controls.OfType<HtmlInputText>())
                        {
                            InputValue = string.IsNullOrEmpty(c.Value) ? InputValue : c.Value;
                            ID = c.ID;
                            FieldTypeId = Convert.ToInt32(c.Attributes["FieldID"]);
                            FieldType = c.Attributes["FieldType"];
                            if (ViewState["FieldTypeId1"] == null)
                                ViewState["FieldTypeId1"] = FieldTypeId;
                            else if (ViewState["FieldTypeId2"] == null)
                                ViewState["FieldTypeId2"] = FieldTypeId;
                            else if (ViewState["FieldTypeId3"] == null)
                                ViewState["FieldTypeId3"] = FieldTypeId;

                            if (InputValue != null)
                            {
                                switch (FieldType.ToUpper())
                                {
                                    case "TEXT":
                                        dt.Rows.Add(FieldTypeId, InputValue);
                                        break;
                                    case "NUMBER":
                                        dt.Rows.Add(FieldTypeId, InputValue);
                                        break;
                                    case "DATE":
                                        if (string.IsNullOrEmpty(InputValue))
                                            dt.Rows.Add(FieldTypeId, InputValue);
                                        else
                                            dt.Rows.Add(FieldTypeId, InputValue);
                                        break;
                                }
                            }
                        }

                        foreach (var c in parentctrl.Controls.OfType<HtmlTextArea>())
                        {
                            InputValue = c.Value;
                            ID = c.ID;
                            FieldTypeId = Convert.ToInt32(c.Attributes["FieldID"]);
                            FieldType = c.Attributes["FieldType"];
                            dt.Rows.Add(FieldTypeId, InputValue);
                        }

                        foreach (var ddlList in parentctrl.Controls.OfType<HtmlSelect>())
                        {
                            ID = ddlList.ID;
                            FieldTypeId = Convert.ToInt32(ddlList.Attributes["FieldID"]);
                            Master = ddlList.Attributes["master"];
                            InputValue = "";
                            if (string.IsNullOrEmpty(Master))
                            {
                                foreach (ListItem objItem in ddlList.Items)
                                {
                                    if (objItem.Selected)
                                    {
                                        InputValue += objItem.Value + ",";
                                    }
                                }
                                dt.Rows.Add(FieldTypeId, InputValue);
                            }
                        }
                        foreach (var hdnField in parentctrl.Controls.OfType<HtmlInputHidden>())
                        {
                            ID = hdnField.ID;
                            Master = hdnField.Attributes["master"];
                            InputValue = hdnField.Value;
                            if (!string.IsNullOrEmpty(Master))
                                dt.Rows.Add(FieldTypeId, InputValue);
                        }

                        foreach (var cbList in parentctrl.Controls.OfType<CheckBoxList>())
                        {
                            ID = cbList.ID;
                            FieldTypeId = Convert.ToInt32(cbList.Attributes["FieldID"]);

                            InputValue = "";
                            foreach (ListItem objItem in cbList.Items)
                            {
                                if (objItem.Selected)
                                {
                                    InputValue += objItem.Value + ",";
                                }
                            }
                            dt.Rows.Add(FieldTypeId, InputValue.Trim(','));
                        }

                        foreach (var rbList in parentctrl.Controls.OfType<RadioButtonList>())
                        {
                            ID = rbList.ID;
                            FieldTypeId = Convert.ToInt32(rbList.Attributes["FieldID"]);

                            InputValue = rbList.SelectedValue;
                            dt.Rows.Add(FieldTypeId, InputValue);
                        }
                    }
                }
            }
            return dt;
            #endregion
            #endregion GetControlValue
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    [WebMethod]
    public static List<ListItem> LoadUsers(string ContractID)
    {
        try
        {
            if (ContractID == "")
                ContractID = "0";

            List<ListItem> us = new List<ListItem>();
            IKeyFields objusers = FactoryWorkflow.GetKeyFieldsDetails();
            /* Added by kiran on 29 Mar 2016 for show the user dependendent on contract type   */
            objusers.ContractTypeId = Convert.ToInt32(ContractID); ;
            DataTable dt = objusers.GetUsers();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                us.Add(new ListItem
                {
                    Value = Convert.ToString(dt.Rows[i]["UsersId"]),
                    Text = Convert.ToString(dt.Rows[i]["FullName"]),
                });
            }

            return us;
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        objkey.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        try
        {
            if (txteffectivedate.Value != "")
            {
                objkey.EffectiveDate = Convert.ToDateTime(txteffectivedate.Value);
            }

            if (txtexpirationdate.Value != "")
            {
                objkey.ExpirationDate = Convert.ToDateTime(txtexpirationdate.Value);
            }

            if (txtrenewaldate.Value != "")
            {
                objkey.RenewalDate = Convert.ToDateTime(txtrenewaldate.Value);
            }

            #region ddlRemindExpiration1
            if (Convert.ToInt32(ddlRemindExpiration1.Value) == 180)
            {
                objkey.ExpirationAlert180days = "Y";
                objkey.ExpirationAlert180daysUserID1 = Convert.ToInt32(ddlexp180Users1.Value); //js
                objkey.ExpirationAlert180daysUserID2 = Convert.ToInt32(ddlexp180Users2.Value); //js
            }
            else
            {
                objkey.ExpirationAlert180days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration1.Value) == 90)
            {
                objkey.ExpirationAlert90days = "Y";
                objkey.ExpirationAlert90daysUserID1 = Convert.ToInt32(ddlexp180Users1.Value); //js
                objkey.ExpirationAlert90daysUserID2 = Convert.ToInt32(ddlexp180Users2.Value); //js
            }
            else
            {
                objkey.ExpirationAlert90days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration1.Value) == 60)
            {
                objkey.ExpirationAlert60days = "Y";
                objkey.ExpirationAlert60daysUserID1 = Convert.ToInt32(ddlexp180Users1.Value); //js
                objkey.ExpirationAlert60daysUserID2 = Convert.ToInt32(ddlexp180Users2.Value); //js
            }
            else
            {
                objkey.ExpirationAlert60days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration1.Value) == 30)
            {
                objkey.ExpirationAlert30days = "Y";
                objkey.ExpirationAlert30daysUserID1 = Convert.ToInt32(ddlexp180Users1.Value); //js
                objkey.ExpirationAlert30daysUserID2 = Convert.ToInt32(ddlexp180Users2.Value); //js
            }
            else
            {
                objkey.ExpirationAlert30days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration1.Value) == 7)
            {
                objkey.ExpirationAlert7days = "Y";
                objkey.ExpirationAlert7daysUserID1 = Convert.ToInt32(ddlexp180Users1.Value); //js
                objkey.ExpirationAlert7daysUserID2 = Convert.ToInt32(ddlexp180Users2.Value); //js
            }
            else
            {
                objkey.ExpirationAlert7days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration1.Value) == 1)
            {
                objkey.ExpirationAlert24Hrs = "Y";
                objkey.ExpirationAlert24HrsUserID1 = Convert.ToInt32(ddlexp180Users1.Value); //js
                objkey.ExpirationAlert24HrsUserID2 = Convert.ToInt32(ddlexp180Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert24Hrs != "Y")
                    objkey.ExpirationAlert24Hrs = "N";
            }
            #endregion

            #region ddlRemindExpiration2
            if (Convert.ToInt32(ddlRemindExpiration2.Value) == 180)
            {
                objkey.ExpirationAlert180days = "Y";
                objkey.ExpirationAlert180daysUserID1 = Convert.ToInt32(ddlexp90Users1.Value); //js
                objkey.ExpirationAlert180daysUserID2 = Convert.ToInt32(ddlexp90Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert180days != "Y")
                    objkey.ExpirationAlert180days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration2.Value) == 90)
            {
                objkey.ExpirationAlert90days = "Y";
                objkey.ExpirationAlert90daysUserID1 = Convert.ToInt32(ddlexp90Users1.Value); //js
                objkey.ExpirationAlert90daysUserID2 = Convert.ToInt32(ddlexp90Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert90days != "Y")
                    objkey.ExpirationAlert90days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration2.Value) == 60)
            {
                objkey.ExpirationAlert60days = "Y";
                objkey.ExpirationAlert60daysUserID1 = Convert.ToInt32(ddlexp90Users1.Value); //js
                objkey.ExpirationAlert60daysUserID2 = Convert.ToInt32(ddlexp90Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert60days != "Y")
                    objkey.ExpirationAlert60days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration2.Value) == 30)
            {
                objkey.ExpirationAlert30days = "Y";
                objkey.ExpirationAlert30daysUserID1 = Convert.ToInt32(ddlexp90Users1.Value); //js
                objkey.ExpirationAlert30daysUserID2 = Convert.ToInt32(ddlexp90Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert30days != "Y")
                    objkey.ExpirationAlert30days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration2.Value) == 7)
            {
                objkey.ExpirationAlert7days = "Y";
                objkey.ExpirationAlert7daysUserID1 = Convert.ToInt32(ddlexp90Users1.Value); //js
                objkey.ExpirationAlert7daysUserID2 = Convert.ToInt32(ddlexp90Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert7days != "Y")
                    objkey.ExpirationAlert7days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration2.Value) == 1)
            {
                objkey.ExpirationAlert24Hrs = "Y";
                objkey.ExpirationAlert24HrsUserID1 = Convert.ToInt32(ddlexp90Users1.Value); //js
                objkey.ExpirationAlert24HrsUserID2 = Convert.ToInt32(ddlexp90Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert24Hrs != "Y")
                    objkey.ExpirationAlert24Hrs = "N";
            }
            #endregion

            #region ddlRemindExpiration3
            if (Convert.ToInt32(ddlRemindExpiration3.Value) == 180)
            {
                objkey.ExpirationAlert180days = "Y";
                objkey.ExpirationAlert180daysUserID1 = Convert.ToInt32(ddlexp60Users1.Value); //js
                objkey.ExpirationAlert180daysUserID2 = Convert.ToInt32(ddlexp60Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert180days != "Y")
                    objkey.ExpirationAlert180days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration3.Value) == 90)
            {
                objkey.ExpirationAlert90days = "Y";
                objkey.ExpirationAlert90daysUserID1 = Convert.ToInt32(ddlexp60Users1.Value); //js
                objkey.ExpirationAlert90daysUserID2 = Convert.ToInt32(ddlexp60Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert90days != "Y")
                    objkey.ExpirationAlert90days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration3.Value) == 60)
            {
                objkey.ExpirationAlert60days = "Y";
                objkey.ExpirationAlert60daysUserID1 = Convert.ToInt32(ddlexp60Users1.Value); //js
                objkey.ExpirationAlert60daysUserID2 = Convert.ToInt32(ddlexp60Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert60days != "Y")
                    objkey.ExpirationAlert60days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration3.Value) == 30)
            {
                objkey.ExpirationAlert30days = "Y";
                objkey.ExpirationAlert30daysUserID1 = Convert.ToInt32(ddlexp60Users1.Value); //js
                objkey.ExpirationAlert30daysUserID2 = Convert.ToInt32(ddlexp60Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert30days != "Y")
                    objkey.ExpirationAlert30days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration3.Value) == 7)
            {
                objkey.ExpirationAlert7days = "Y";
                objkey.ExpirationAlert7daysUserID1 = Convert.ToInt32(ddlexp60Users1.Value); //js
                objkey.ExpirationAlert7daysUserID2 = Convert.ToInt32(ddlexp60Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert7days != "Y")
                    objkey.ExpirationAlert7days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration3.Value) == 1)
            {
                objkey.ExpirationAlert24Hrs = "Y";
                objkey.ExpirationAlert24HrsUserID1 = Convert.ToInt32(ddlexp60Users1.Value); //js
                objkey.ExpirationAlert24HrsUserID2 = Convert.ToInt32(ddlexp60Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert24Hrs != "Y")
                    objkey.ExpirationAlert24Hrs = "N";
            }
            #endregion

            #region ddlRemindRenewal1
            if (Convert.ToInt32(ddlRemindRenewal1.Value) == 180)
            {
                objkey.RenewalAlert180days = "Y";
                objkey.RenewalAlert180daysUserID1 = Convert.ToInt32(ddlRenewal180User1.Value); //js
                objkey.RenewalAlert180daysUserID2 = Convert.ToInt32(ddlRenewal180User2.Value); //js
            }
            else
            {
                objkey.RenewalAlert180days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal1.Value) == 90)
            {
                objkey.RenewalAlert90days = "Y";
                objkey.RenewalAlert90daysUserID1 = Convert.ToInt32(ddlRenewal180User1.Value); //js
                objkey.RenewalAlert90daysUserID2 = Convert.ToInt32(ddlRenewal180User2.Value); //js
            }
            else
            {
                objkey.RenewalAlert90days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal1.Value) == 60)
            {
                objkey.RenewalAlert60days = "Y";
                objkey.RenewalAlert60daysUserID1 = Convert.ToInt32(ddlRenewal180User1.Value); //js
                objkey.RenewalAlert60daysUserID2 = Convert.ToInt32(ddlRenewal180User2.Value); //js
            }
            else
            {
                objkey.RenewalAlert60days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal1.Value) == 30)
            {
                objkey.RenewalAlert30days = "Y";
                objkey.RenewalAlert30daysUserID1 = Convert.ToInt32(ddlRenewal180User1.Value); //js
                objkey.RenewalAlert30daysUserID2 = Convert.ToInt32(ddlRenewal180User2.Value); //js
            }
            else
            {
                objkey.RenewalAlert30days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal1.Value) == 7)
            {
                objkey.RenewalAlert7days = "Y";
                objkey.RenewalAlert7daysUserID1 = Convert.ToInt32(ddlRenewal180User1.Value); //js
                objkey.RenewalAlert7daysUserID2 = Convert.ToInt32(ddlRenewal180User2.Value); //js
            }
            else
            {
                objkey.RenewalAlert7days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal1.Value) == 1)
            {
                objkey.RenewalAlert24Hrs = "Y";
                objkey.RenewalAlert24HrsUserID1 = Convert.ToInt32(ddlRenewal180User1.Value); //js
                objkey.RenewalAlert24HrsUserID2 = Convert.ToInt32(ddlRenewal180User2.Value); //js
            }
            else
            {
                objkey.RenewalAlert24Hrs = "N";
            }
            #endregion

            #region ddlRemindRenewal2
            if (Convert.ToInt32(ddlRemindRenewal2.Value) == 180)
            {
                objkey.RenewalAlert180days = "Y";
                objkey.RenewalAlert180daysUserID1 = Convert.ToInt32(ddlRenewal90User1.Value); //js
                objkey.RenewalAlert180daysUserID2 = Convert.ToInt32(ddlRenewal90User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert180days != "Y")
                    objkey.RenewalAlert180days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal2.Value) == 90)
            {
                objkey.RenewalAlert90days = "Y";
                objkey.RenewalAlert90daysUserID1 = Convert.ToInt32(ddlRenewal90User1.Value); //js
                objkey.RenewalAlert90daysUserID2 = Convert.ToInt32(ddlRenewal90User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert90days != "Y")
                    objkey.RenewalAlert90days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal2.Value) == 60)
            {
                objkey.RenewalAlert60days = "Y";
                objkey.RenewalAlert60daysUserID1 = Convert.ToInt32(ddlRenewal90User1.Value); //js
                objkey.RenewalAlert60daysUserID2 = Convert.ToInt32(ddlRenewal90User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert60days != "Y")
                    objkey.RenewalAlert60days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal2.Value) == 30)
            {
                objkey.RenewalAlert30days = "Y";
                objkey.RenewalAlert30daysUserID1 = Convert.ToInt32(ddlRenewal90User1.Value); //js
                objkey.RenewalAlert30daysUserID2 = Convert.ToInt32(ddlRenewal90User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert30days != "Y")
                    objkey.RenewalAlert30days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal2.Value) == 7)
            {
                objkey.RenewalAlert7days = "Y";
                objkey.RenewalAlert7daysUserID1 = Convert.ToInt32(ddlRenewal90User1.Value); //js
                objkey.RenewalAlert7daysUserID2 = Convert.ToInt32(ddlRenewal90User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert7days != "Y")
                    objkey.RenewalAlert7days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal2.Value) == 1)
            {
                objkey.RenewalAlert24Hrs = "Y";
                objkey.RenewalAlert24HrsUserID1 = Convert.ToInt32(ddlRenewal90User1.Value); //js
                objkey.RenewalAlert24HrsUserID2 = Convert.ToInt32(ddlRenewal90User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert24Hrs != "Y")
                    objkey.RenewalAlert24Hrs = "N";
            }
            #endregion

            #region ddlRemindRenewal3
            if (Convert.ToInt32(ddlRemindRenewal3.Value) == 180)
            {
                objkey.RenewalAlert180days = "Y";
                objkey.RenewalAlert180daysUserID1 = Convert.ToInt32(ddlRenewal60User1.Value); //js
                objkey.RenewalAlert180daysUserID2 = Convert.ToInt32(ddlRenewal60User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert180days != "Y")
                    objkey.RenewalAlert180days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal3.Value) == 90)
            {
                objkey.RenewalAlert90days = "Y";
                objkey.RenewalAlert90daysUserID1 = Convert.ToInt32(ddlRenewal60User1.Value); //js
                objkey.RenewalAlert90daysUserID2 = Convert.ToInt32(ddlRenewal60User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert90days != "Y")
                    objkey.RenewalAlert90days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal3.Value) == 60)
            {
                objkey.RenewalAlert60days = "Y";
                objkey.RenewalAlert60daysUserID1 = Convert.ToInt32(ddlRenewal60User1.Value); //js
                objkey.RenewalAlert60daysUserID2 = Convert.ToInt32(ddlRenewal60User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert60days != "Y")
                    objkey.RenewalAlert60days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal3.Value) == 30)
            {
                objkey.RenewalAlert30days = "Y";
                objkey.RenewalAlert30daysUserID1 = Convert.ToInt32(ddlRenewal60User1.Value); //js
                objkey.RenewalAlert30daysUserID2 = Convert.ToInt32(ddlRenewal60User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert30days != "Y")
                    objkey.RenewalAlert30days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal3.Value) == 7)
            {
                objkey.RenewalAlert7days = "Y";
                objkey.RenewalAlert7daysUserID1 = Convert.ToInt32(ddlRenewal60User1.Value); //js
                objkey.RenewalAlert7daysUserID2 = Convert.ToInt32(ddlRenewal60User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert7days != "Y")
                    objkey.RenewalAlert7days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal3.Value) == 1)
            {
                objkey.RenewalAlert24Hrs = "Y";
                objkey.RenewalAlert24HrsUserID1 = Convert.ToInt32(ddlRenewal60User1.Value); //js
                objkey.RenewalAlert24HrsUserID2 = Convert.ToInt32(ddlRenewal60User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert24Hrs != "Y")
                    objkey.RenewalAlert24Hrs = "N";
            }
            #endregion

            DataTable dt = SaveData(ImportantDateTable);
            objkey.dtMatadataFieldValues = dt;

            objkey.Param = 1;
            objkey.RequestId = int.Parse(HdnRequestID.Value);
            objkey.InsertRecord();
            SaveUserInActivityTable();//For add details in Activity table
            SendEmail(3); //Metadata changes to key obligation
            ////SendEmail(13);
            SendScheduledEmail(objkey.RequestId);

            if (hdnLinkStatus.Value == "Volts")
            {
                if (ViewState["ParentRequestId"].ToString() == HdnRequestID.Value)
                    Response.Redirect("~/WorkFlow/VaultFlow.aspx?ScrollTo=msgkeyfields&RequestId=" + HdnRequestID.Value + "&Status=KeyField ", false);
                else
                    Response.Redirect("~/WorkFlow/VaultFlow.aspx?ScrollTo=msgkeyfields&RequestId=" + HdnRequestID.Value + "&Status=KeyField&ParentRequestID=" + ViewState["ParentRequestId"], false);
            }
            else
            {
                if (ViewState["ParentRequestId"].ToString() == HdnRequestID.Value)
                    Response.Redirect("~/WorkFlow/RequestFlow.aspx?ScrollTo=msgkeyfields&RequestId=" + HdnRequestID.Value + "&Status=KeyField ", false);
                else
                    Response.Redirect("~/WorkFlow/RequestFlow.aspx?ScrollTo=msgkeyfields&RequestId=" + HdnRequestID.Value + "&Status=KeyField&ParentRequestID=" + ViewState["ParentRequestId"], false);
            }
        }
        catch (Exception ex) { }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {

        if (hdnLinkStatus.Value == "Volts")
        {
            if (Convert.ToString(ViewState["ParentRequestId"]).ToString() == HdnRequestID.Value)
                Response.Redirect("~/WorkFlow/VaultFlow.aspx?ScrollTo=msgkeyfields&RequestId=" + HdnRequestID.Value + "&Status=KeyField ", false);
            else
                Response.Redirect("~/WorkFlow/VaultFlow.aspx?ScrollTo=msgkeyfields&RequestId=" + HdnRequestID.Value + "&Status=KeyField&ParentRequestID=" + ViewState["ParentRequestId"], false);
        }
        else
        {
            if (Convert.ToString(ViewState["ParentRequestId"]).ToString() == HdnRequestID.Value)
                Response.Redirect("~/WorkFlow/RequestFlow.aspx?ScrollTo=msgkeyfields&RequestId=" + HdnRequestID.Value + "&Status=KeyField ", false);
            else
                Response.Redirect("~/WorkFlow/RequestFlow.aspx?ScrollTo=msgkeyfields&RequestId=" + HdnRequestID.Value + "&Status=KeyField&ParentRequestID=" + ViewState["ParentRequestId"], false);
        }
    }

    protected void btnChangeUser_Click(object sender, EventArgs e)
    {
        switch (hdnUserStatus.Value)
        {
            case "Exp180User":
                ddlexp180Users1.Value = "0";
                ddlexp180Users2.Value = "0";
                break;

            case "Exp90User":
                ddlexp90Users1.Value = "0";
                ddlexp90Users2.Value = "0";
                break;

            case "Exp60User":
                ddlexp60Users1.Value = "0";
                ddlexp60Users2.Value = "0";
                break;

            case "Rem180User":
                ddlRenewal180User1.Value = "0";
                ddlRenewal180User2.Value = "0";
                break;

            case "Rem90User":
                ddlRenewal90User1.Value = "0";
                ddlRenewal90User2.Value = "0";
                break;

            case "Rem60User":
                ddlRenewal60User1.Value = "0";
                ddlRenewal60User2.Value = "0";
                break;

            default:
                break;

        }

    }

    public void SaveUserInActivityTable()
    {
        try
        {
            objkey.RequestId = Convert.ToInt32(HdnRequestID.Value);
            string expdate = "";
            string renualdate = "";
            string Reminddate1 = "";
            string Reminddate2 = "";
            string Reminddate3 = "";
            try
            {
                expdate = Convert.ToDateTime(txtexpirationdate.Value).ToString("dd-MMM-yyyy");
            }
            catch { }
            try
            {
                renualdate = Convert.ToDateTime(txtrenewaldate.Value).ToString("dd-MMM-yyyy");
            }
            catch { }
            try
            {
                Reminddate1 = Convert.ToDateTime(hdnReminddate1.Value).ToString("dd-MMM-yyyy");
            }
            catch { }
            try
            {
                Reminddate2 = Convert.ToDateTime(hdnReminddate2.Value).ToString("dd-MMM-yyyy");
            }
            catch { }
            try
            {
                Reminddate3 = Convert.ToDateTime(hdnReminddate3.Value).ToString("dd-MMM-yyyy");
            }
            catch { }


            DateTime exp30day; DateTime exp7day; DateTime exp1day;
            DateTime Ren30day; DateTime Ren7day; DateTime Ren1day;
            DateTime exp180day; DateTime exp90day; DateTime exp60day;
            DateTime Ren180day; DateTime Ren90day; DateTime Ren60day;


            //return;

            //For delete
            objkey.Param = 4;
            objkey.RequestId = Convert.ToInt32(HdnRequestID.Value);
            objkey.InsertRecord();

            //End


            objkey.Param = 3;
            objkey.ActivityStatusId = 1;
            objkey.ActivityTypeId = 2;
            objkey.IsFromOther = "KeyField";

            objkey.IsForCustomer = "Y";
            objkey.IsFromCalendar = "Y";

            objkey.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            objkey.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            objkey.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
            #region Expiration Activity
            //For Expiration 
            if (expdate != "")
            {
                objkey.ActivityText = "Contract expiration on-" + expdate;
                objkey.ActivityDate = Convert.ToDateTime(expdate);
                exp180day = Convert.ToDateTime(expdate).AddDays(-180); //js
                exp90day = Convert.ToDateTime(expdate).AddDays(-90);
                exp60day = Convert.ToDateTime(expdate).AddDays(-60);
                exp30day = Convert.ToDateTime(expdate).AddDays(-30);
                exp7day = Convert.ToDateTime(expdate).AddDays(-7);
                exp1day = Convert.ToDateTime(expdate).AddDays(-1);
                //----------------------------------------------------------
                if (ddlexp180Users1.Value != "0")
                {
                    if (chkexp180.Checked == true)
                    {
                        objkey.ReminderDate = (exp180day);
                        objkey.AssignToId = Convert.ToInt32(ddlexp180Users1.Value);
                        objkey.InsertRecord();
                    }
                }
                if (ddlexp180Users2.Value != "0")
                {
                    if (chkexp180.Checked == true)
                    {
                        objkey.ReminderDate = (exp180day);
                        objkey.AssignToId = Convert.ToInt32(ddlexp180Users2.Value);
                        objkey.InsertRecord();
                    }
                }
                if (ddlexp90Users1.Value != "0")
                {
                    if (chkexp90.Checked == true)
                    {
                        objkey.ReminderDate = (exp90day);
                        objkey.AssignToId = Convert.ToInt32(ddlexp90Users1.Value);
                        objkey.InsertRecord();
                    }
                }
                if (ddlexp90Users2.Value != "0")
                {
                    if (chkexp90.Checked == true)
                    {
                        objkey.ReminderDate = (exp90day);
                        objkey.AssignToId = Convert.ToInt32(ddlexp90Users2.Value);
                        objkey.InsertRecord();
                    }
                }
                if (ddlexp60Users1.Value != "0")
                {
                    if (chkexp60.Checked == true)
                    {
                        objkey.ReminderDate = (exp60day);
                        objkey.AssignToId = Convert.ToInt32(ddlexp60Users1.Value);
                        objkey.InsertRecord();
                    }
                }
                if (ddlexp60Users2.Value != "0")
                {
                    if (chkexp60.Checked == true)
                    {
                        objkey.ReminderDate = (exp60day);
                        objkey.AssignToId = Convert.ToInt32(ddlexp60Users2.Value);
                        objkey.InsertRecord();
                    }
                }
            }
            #endregion

            #region Renewal Activity
            //For renewal 
            if (renualdate != "")
            {
                objkey.ActivityText = "Renewal date of contract is-" + renualdate;
                objkey.ActivityDate = Convert.ToDateTime(renualdate);

                Ren180day = Convert.ToDateTime(renualdate).AddDays(-180);
                Ren90day = Convert.ToDateTime(renualdate).AddDays(-90);
                Ren60day = Convert.ToDateTime(renualdate).AddDays(-60);
                Ren30day = Convert.ToDateTime(renualdate).AddDays(-30);
                Ren7day = Convert.ToDateTime(renualdate).AddDays(-7);
                Ren1day = Convert.ToDateTime(renualdate).AddDays(-1);

                //--------------------------------
                if (ddlRenewal180User1.Value != "0")
                {
                    if (chkrem180.Checked == true)
                    {
                        objkey.ReminderDate = (Ren180day);
                        objkey.AssignToId = Convert.ToInt32(ddlRenewal180User1.Value);
                        objkey.InsertRecord();
                    }
                }
                if (ddlRenewal180User2.Value != "0")
                {
                    if (chkrem180.Checked == true)
                    {
                        objkey.ReminderDate = (Ren180day);
                        objkey.AssignToId = Convert.ToInt32(ddlRenewal180User2.Value);
                        objkey.InsertRecord();
                    }
                }
                if (ddlRenewal90User1.Value != "0")
                {
                    if (chkrem90.Checked == true)
                    {
                        objkey.ReminderDate = (Ren90day);
                        objkey.AssignToId = Convert.ToInt32(ddlRenewal90User1.Value);
                        objkey.InsertRecord();
                    }
                }
                if (ddlRenewal90User2.Value != "0")
                {
                    if (chkrem90.Checked == true)
                    {
                        objkey.ReminderDate = (Ren90day);
                        objkey.AssignToId = Convert.ToInt32(ddlRenewal90User2.Value);
                        objkey.InsertRecord();
                    }
                }
                if (ddlRenewal60User1.Value != "0")
                {
                    if (chkrem60.Checked == true)
                    {
                        objkey.ReminderDate = (Ren60day);
                        objkey.AssignToId = Convert.ToInt32(ddlRenewal60User1.Value);
                        objkey.InsertRecord();
                    }
                }
                if (ddlRenewal60User2.Value != "0")
                {
                    if (chkrem60.Checked == true)
                    {
                        objkey.ReminderDate = (Ren60day);
                        objkey.AssignToId = Convert.ToInt32(ddlRenewal60User2.Value);
                        objkey.InsertRecord();
                    }
                }
            }
            #endregion

            /// Delete for Import Date Custom Reminder
            int ContarctID = Convert.ToInt32(ViewState["ContractID"]);
            objkey.ContractID = ContarctID;
            objkey.Param = 6;
            objkey.RequestId = Convert.ToInt32(HdnRequestID.Value);
            //objkey.MetaDataFieldId = Convert.ToInt32(ViewState["FieldTypeId1"]);
            objkey.InsertRecord();



            #region Custom Reminder date 1
            if (Reminddate1 != "")
            {

                objkey.ActivityText = hdnReminderName1.Value + " reminder on-" + Reminddate1;
                objkey.ActivityDate = Convert.ToDateTime(Reminddate1);
                exp180day = Convert.ToDateTime(Reminddate1).AddDays(-180); //js
                exp90day = Convert.ToDateTime(Reminddate1).AddDays(-90);
                exp60day = Convert.ToDateTime(Reminddate1).AddDays(-60);
                exp30day = Convert.ToDateTime(Reminddate1).AddDays(-30);
                exp7day = Convert.ToDateTime(Reminddate1).AddDays(-7);
                exp1day = Convert.ToDateTime(Reminddate1).AddDays(-1);
                //--------------------------
                objkey.Param = 3;
                if (hdnRemindDay10.Value != "0")
                {
                    if (hdnRemindDay10.Value == "180")
                        objkey.ReminderDate = (exp180day);
                    else if (hdnRemindDay10.Value == "90")
                        objkey.ReminderDate = (exp90day);
                    else if (hdnRemindDay10.Value == "60")
                        objkey.ReminderDate = (exp60day);
                    else if (hdnRemindDay10.Value == "30")
                        objkey.ReminderDate = (exp30day);
                    else if (hdnRemindDay10.Value == "7")
                        objkey.ReminderDate = (exp7day);
                    else if (hdnRemindDay10.Value == "1")
                        objkey.ReminderDate = (exp1day);

                    if (hdnRemindDay1User10.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay1User10.Value);
                        objkey.InsertRecord();
                    }
                    if (hdnRemindDay1User20.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay1User20.Value);
                        objkey.InsertRecord();
                    }
                }

                if (hdnRemindDay20.Value != "0")
                {
                    if (hdnRemindDay20.Value == "180")
                        objkey.ReminderDate = (exp180day);
                    else if (hdnRemindDay20.Value == "90")
                        objkey.ReminderDate = (exp90day);
                    else if (hdnRemindDay20.Value == "60")
                        objkey.ReminderDate = (exp60day);
                    else if (hdnRemindDay20.Value == "30")
                        objkey.ReminderDate = (exp30day);
                    else if (hdnRemindDay20.Value == "7")
                        objkey.ReminderDate = (exp7day);
                    else if (hdnRemindDay20.Value == "1")
                        objkey.ReminderDate = (exp1day);

                    if (hdnRemindDay2User10.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay2User10.Value);
                        objkey.InsertRecord();
                    }
                    if (hdnRemindDay2User20.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay2User20.Value);
                        objkey.InsertRecord();
                    }
                }

                if (hdnRemindDay30.Value != "0")
                {
                    if (hdnRemindDay30.Value == "180")
                        objkey.ReminderDate = (exp180day);
                    else if (hdnRemindDay30.Value == "90")
                        objkey.ReminderDate = (exp90day);
                    else if (hdnRemindDay30.Value == "60")
                        objkey.ReminderDate = (exp60day);
                    else if (hdnRemindDay30.Value == "30")
                        objkey.ReminderDate = (exp30day);
                    else if (hdnRemindDay30.Value == "7")
                        objkey.ReminderDate = (exp7day);
                    else if (hdnRemindDay30.Value == "1")
                        objkey.ReminderDate = (exp1day);

                    if (hdnRemindDay3User10.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay3User10.Value);
                        objkey.InsertRecord();
                    }
                    if (hdnRemindDay3User20.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay3User20.Value);
                        objkey.InsertRecord();
                    }
                }

                //int ContarctID = Convert.ToInt32(ViewState["ContractID"]);
                //objkey.ContractID = ContarctID;
                //objkey.Param = 6;
                //objkey.RequestId = Convert.ToInt32(HdnRequestID.Value);
                objkey.MetaDataFieldId = Convert.ToInt32(ViewState["FieldTypeId1"]);
                //objkey.InsertRecord();


                objkey.Param = 5;
                objkey.RequestId = int.Parse(HdnRequestID.Value);
                if (hdnRemindDay10.Value != "0")
                {
                    objkey.ReminderDays = Convert.ToInt32(hdnRemindDay10.Value);
                    objkey.ReminderUser1 = Convert.ToInt32(hdnRemindDay1User10.Value);
                    objkey.ReminderUser2 = Convert.ToInt32(hdnRemindDay1User20.Value);
                    objkey.InsertRecord();
                }
                if (hdnRemindDay20.Value != "0")
                {
                    objkey.ReminderDays = Convert.ToInt32(hdnRemindDay20.Value);
                    objkey.ReminderUser1 = Convert.ToInt32(hdnRemindDay2User10.Value);
                    objkey.ReminderUser2 = Convert.ToInt32(hdnRemindDay2User20.Value);
                    objkey.InsertRecord();
                }
                if (hdnRemindDay30.Value != "0")
                {
                    objkey.ReminderDays = Convert.ToInt32(hdnRemindDay30.Value);
                    objkey.ReminderUser1 = Convert.ToInt32(hdnRemindDay3User10.Value);
                    objkey.ReminderUser2 = Convert.ToInt32(hdnRemindDay3User20.Value);
                    objkey.InsertRecord();
                }

            }

            #endregion

            #region Custom Reminder date 2
            if (Reminddate2 != "")
            {
                objkey.ActivityText = hdnReminderName2.Value + " reminder on-" + Reminddate2;
                objkey.ActivityDate = Convert.ToDateTime(Reminddate2);
                exp180day = Convert.ToDateTime(Reminddate2).AddDays(-180); //js
                exp90day = Convert.ToDateTime(Reminddate2).AddDays(-90);
                exp60day = Convert.ToDateTime(Reminddate2).AddDays(-60);
                exp30day = Convert.ToDateTime(Reminddate2).AddDays(-30);
                exp7day = Convert.ToDateTime(Reminddate2).AddDays(-7);
                exp1day = Convert.ToDateTime(Reminddate2).AddDays(-1);
                objkey.Param = 3;
                if (hdnRemindDay11.Value != "0")
                {
                    if (hdnRemindDay11.Value == "180")
                        objkey.ReminderDate = (exp180day);
                    else if (hdnRemindDay11.Value == "90")
                        objkey.ReminderDate = (exp90day);
                    else if (hdnRemindDay11.Value == "60")
                        objkey.ReminderDate = (exp60day);
                    else if (hdnRemindDay11.Value == "30")
                        objkey.ReminderDate = (exp30day);
                    else if (hdnRemindDay11.Value == "7")
                        objkey.ReminderDate = (exp7day);
                    else if (hdnRemindDay11.Value == "1")
                        objkey.ReminderDate = (exp1day);

                    if (hdnRemindDay1User11.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay1User11.Value);
                        objkey.InsertRecord();
                    }
                    if (hdnRemindDay1User21.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay1User21.Value);
                        objkey.InsertRecord();
                    }
                }

                if (hdnRemindDay21.Value != "0")
                {
                    if (hdnRemindDay21.Value == "180")
                        objkey.ReminderDate = (exp180day);
                    else if (hdnRemindDay21.Value == "90")
                        objkey.ReminderDate = (exp90day);
                    else if (hdnRemindDay21.Value == "60")
                        objkey.ReminderDate = (exp60day);
                    else if (hdnRemindDay21.Value == "30")
                        objkey.ReminderDate = (exp30day);
                    else if (hdnRemindDay21.Value == "7")
                        objkey.ReminderDate = (exp7day);
                    else if (hdnRemindDay21.Value == "1")
                        objkey.ReminderDate = (exp1day);

                    if (hdnRemindDay2User11.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay2User11.Value);
                        objkey.InsertRecord();
                    }
                    if (hdnRemindDay2User21.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay2User21.Value);
                        objkey.InsertRecord();
                    }
                }

                if (hdnRemindDay31.Value != "0")
                {
                    if (hdnRemindDay31.Value == "180")
                        objkey.ReminderDate = (exp180day);
                    else if (hdnRemindDay31.Value == "90")
                        objkey.ReminderDate = (exp90day);
                    else if (hdnRemindDay31.Value == "60")
                        objkey.ReminderDate = (exp60day);
                    else if (hdnRemindDay31.Value == "30")
                        objkey.ReminderDate = (exp30day);
                    else if (hdnRemindDay31.Value == "7")
                        objkey.ReminderDate = (exp7day);
                    else if (hdnRemindDay31.Value == "1")
                        objkey.ReminderDate = (exp1day);

                    if (hdnRemindDay3User11.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay3User11.Value);
                        objkey.InsertRecord();
                    }
                    if (hdnRemindDay3User21.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay3User21.Value);
                        objkey.InsertRecord();
                    }
                }

                //int ContarctID = Convert.ToInt32(ViewState["ContractID"]);
                //objkey.ContractID = ContarctID;
                //objkey.Param = 6;
                //objkey.RequestId = Convert.ToInt32(HdnRequestID.Value);
                objkey.MetaDataFieldId = Convert.ToInt32(ViewState["FieldTypeId2"]);
                //objkey.InsertRecord();

                objkey.Param = 5;
                objkey.RequestId = int.Parse(HdnRequestID.Value);
                if (hdnRemindDay11.Value != "0")
                {
                    objkey.ReminderDays = Convert.ToInt32(hdnRemindDay11.Value);
                    objkey.ReminderUser1 = Convert.ToInt32(hdnRemindDay1User11.Value);
                    objkey.ReminderUser2 = Convert.ToInt32(hdnRemindDay1User21.Value);
                    objkey.InsertRecord();
                }
                if (hdnRemindDay21.Value != "0")
                {
                    objkey.ReminderDays = Convert.ToInt32(hdnRemindDay21.Value);
                    objkey.ReminderUser1 = Convert.ToInt32(hdnRemindDay2User11.Value);
                    objkey.ReminderUser2 = Convert.ToInt32(hdnRemindDay2User21.Value);
                    objkey.InsertRecord();
                }
                if (hdnRemindDay31.Value != "0")
                {
                    objkey.ReminderDays = Convert.ToInt32(hdnRemindDay31.Value);
                    objkey.ReminderUser1 = Convert.ToInt32(hdnRemindDay3User11.Value);
                    objkey.ReminderUser2 = Convert.ToInt32(hdnRemindDay3User21.Value);
                    objkey.InsertRecord();
                }

            }
            #endregion

            #region Custom Reminder date 3
            if (Reminddate3 != "")
            {
                objkey.ActivityText = hdnReminderName3.Value + " reminder on-" + Reminddate3;
                objkey.ActivityDate = Convert.ToDateTime(Reminddate3);
                exp180day = Convert.ToDateTime(Reminddate3).AddDays(-180); //js
                exp90day = Convert.ToDateTime(Reminddate3).AddDays(-90);
                exp60day = Convert.ToDateTime(Reminddate3).AddDays(-60);
                exp30day = Convert.ToDateTime(Reminddate3).AddDays(-30);
                exp7day = Convert.ToDateTime(Reminddate3).AddDays(-7);
                exp1day = Convert.ToDateTime(Reminddate3).AddDays(-1);
                objkey.Param = 3;
                if (hdnRemindDay12.Value != "0")
                {
                    if (hdnRemindDay12.Value == "180")
                        objkey.ReminderDate = (exp180day);
                    else if (hdnRemindDay12.Value == "90")
                        objkey.ReminderDate = (exp90day);
                    else if (hdnRemindDay12.Value == "60")
                        objkey.ReminderDate = (exp60day);
                    else if (hdnRemindDay12.Value == "30")
                        objkey.ReminderDate = (exp30day);
                    else if (hdnRemindDay12.Value == "7")
                        objkey.ReminderDate = (exp7day);
                    else if (hdnRemindDay12.Value == "1")
                        objkey.ReminderDate = (exp1day);

                    if (hdnRemindDay1User12.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay1User12.Value);
                        objkey.InsertRecord();
                    }
                    if (hdnRemindDay1User22.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay1User22.Value);
                        objkey.InsertRecord();
                    }
                }

                if (hdnRemindDay22.Value != "0")
                {
                    if (hdnRemindDay22.Value == "180")
                        objkey.ReminderDate = (exp180day);
                    else if (hdnRemindDay22.Value == "90")
                        objkey.ReminderDate = (exp90day);
                    else if (hdnRemindDay22.Value == "60")
                        objkey.ReminderDate = (exp60day);
                    else if (hdnRemindDay22.Value == "30")
                        objkey.ReminderDate = (exp30day);
                    else if (hdnRemindDay22.Value == "7")
                        objkey.ReminderDate = (exp7day);
                    else if (hdnRemindDay22.Value == "1")
                        objkey.ReminderDate = (exp1day);

                    if (hdnRemindDay2User12.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay2User12.Value);
                        objkey.InsertRecord();
                    }
                    if (hdnRemindDay2User22.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay2User22.Value);
                        objkey.InsertRecord();
                    }
                }

                if (hdnRemindDay32.Value != "0")
                {
                    if (hdnRemindDay32.Value == "180")
                        objkey.ReminderDate = (exp180day);
                    else if (hdnRemindDay32.Value == "90")
                        objkey.ReminderDate = (exp90day);
                    else if (hdnRemindDay32.Value == "60")
                        objkey.ReminderDate = (exp60day);
                    else if (hdnRemindDay32.Value == "30")
                        objkey.ReminderDate = (exp30day);
                    else if (hdnRemindDay32.Value == "7")
                        objkey.ReminderDate = (exp7day);
                    else if (hdnRemindDay32.Value == "1")
                        objkey.ReminderDate = (exp1day);

                    if (hdnRemindDay3User12.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay3User12.Value);
                        objkey.InsertRecord();
                    }
                    if (hdnRemindDay3User22.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay3User22.Value);
                        objkey.InsertRecord();
                    }
                }

                //int ContarctID = Convert.ToInt32(ViewState["ContractID"]);
                //objkey.ContractID = ContarctID;
                //objkey.Param = 6;
                //objkey.RequestId = Convert.ToInt32(HdnRequestID.Value);
                //objkey.MetaDataFieldId = Convert.ToInt32(ViewState["FieldTypeId3"]);
                //objkey.InsertRecord();

                objkey.Param = 5;
                objkey.MetaDataFieldId = Convert.ToInt32(ViewState["FieldTypeId3"]);
                objkey.RequestId = int.Parse(HdnRequestID.Value);
                if (hdnRemindDay12.Value != "0")
                {
                    objkey.ReminderDays = Convert.ToInt32(hdnRemindDay12.Value);
                    objkey.ReminderUser1 = Convert.ToInt32(hdnRemindDay1User12.Value);
                    objkey.ReminderUser2 = Convert.ToInt32(hdnRemindDay1User22.Value);
                    objkey.InsertRecord();
                }
                if (hdnRemindDay22.Value != "0")
                {
                    objkey.ReminderDays = Convert.ToInt32(hdnRemindDay22.Value);
                    objkey.ReminderUser1 = Convert.ToInt32(hdnRemindDay2User12.Value);
                    objkey.ReminderUser2 = Convert.ToInt32(hdnRemindDay2User22.Value);
                    objkey.InsertRecord();
                }
                if (hdnRemindDay32.Value != "0")
                {
                    objkey.ReminderDays = Convert.ToInt32(hdnRemindDay32.Value);
                    objkey.ReminderUser1 = Convert.ToInt32(hdnRemindDay3User12.Value);
                    objkey.ReminderUser2 = Convert.ToInt32(hdnRemindDay3User22.Value);
                    objkey.InsertRecord();
                }
            }
            #endregion

        }
        catch { }
    }

    void SendEmail(int EmailTriggerId)
    {
        try
        {
            Page.TraceWrite("send emails.");
            EmailTemplateBLL.IEmailTrigger objEmail = EmailTemplateBLL.FactoryEmailTemplate.GetEmailTriggerDetail();
            objEmail.RequestId = int.Parse(HdnRequestID.Value);
            objEmail.ContractTypeId = 99999;
            objEmail.ContractTemplateId = 99999;
            objEmail.EmailTriggerId = EmailTriggerId; // hardcoded for Metadata changed
            if (Session[Declarations.User] != null)
            {
                objEmail.userId = int.Parse(Session[Declarations.User].ToString());
                objEmail.BulkEmail();
            }
        }
        catch (Exception)
        {
            Page.TraceWarn("send emails fail.");
        }
    }

    void SendScheduledEmail(int RequestId)
    {
        try
        {
            Page.TraceWrite("send scheduled emails.");
            EmailTemplateBLL.IEmailTrigger objEmail = EmailTemplateBLL.FactoryEmailTemplate.GetEmailTriggerDetail();
            objEmail.RequestId = int.Parse(HdnRequestID.Value);
            //            objEmail.AlertDate = DateTime.Now;
            if (Session[Declarations.User] != null)
            {
                objEmail.userId = int.Parse(Session[Declarations.User].ToString());
                objEmail.BulkEmailScheduler();
            }
        }
        catch (Exception)
        {
            Page.TraceWarn("send emails fail.");
        }
    }

}



