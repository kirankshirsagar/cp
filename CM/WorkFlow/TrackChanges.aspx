﻿<%@ Page Language="C#" MasterPageFile="~/CM.master" ClientIDMode="Static" AutoEventWireup="true"
    CodeFile="TrackChanges.aspx.cs" Inherits="Negotiation_TrackChanges" %>

         

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
     <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
     <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
     <link href="../Styles/Font.css" rel="stylesheet" type="text/css" />   

      <link href="../Styles/application.css" rel="stylesheet" type="text/css" />
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">

        $("#requestLink").addClass("menulink");
        $("#requesttab").addClass("selectedtab");

        $('form').live("submit", function () {
            ShowProgress();
        });

    </script>
    <h2>
        Track Changes
    </h2>
   
    <input id="hdnRequestID" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnContractFileOldId" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnContractFileNewId" runat="server" clientidmode="Static" type="hidden" />
  
   <asp:Button ID="btnSubmit" runat="server" Text="Load Customers" style="display:none"
    OnClick="btnSubmit_Click" />
    

</asp:Content>
