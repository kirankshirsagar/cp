﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/CM.master"
    AutoEventWireup="true" CodeFile="SearchResults.aspx.cs" Inherits="SearchResults" Async="true" %>

<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register TagPrefix="searchengine" Assembly="Keyoti4.SearchEngine.Web, Version=2015.6.15.213, Culture=neutral, PublicKeyToken=58d9fd2e9ec4dc0e"
    Namespace="Keyoti.SearchEngine.Web" %>
<%@ Register TagPrefix="searchengine1" Assembly="Keyoti4.SearchEnginePro.Web, Version=2015.6.15.213, Culture=neutral, PublicKeyToken=58d9fd2e9ec4dc0e"
    Namespace="Keyoti.SearchEngine.Web" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridNavigation.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".ui-datepicker").hide();
            $('#ui-datepicker-div').removeAttr('id');

        });     
    </script>
    <link href="../Styles/application.css" rel="stylesheet" type="text/css" />
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
    <link href="../JQueryValidations/jquery-ui-1.8.19.custom.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../scripts/jquery-ui-1.8.16.custom.css" />
    <asp:HiddenField ID="hdnDownloadContractAccess" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnTabPDFDocumentAccess" runat="server" ClientIDMode="Static" />
    <h2>Advanced Search</h2>
    <br />
    <div>
        <br />
        <searchengine:SearchBox ID="SearchBox1" runat="server" ResultPageURL="~/WorkFlow/searchresults.aspx"
            AutoCompleteQuery="true" AutoCompleteQueryShowNumberOfResults="false" RememberQuery="false"
            AutoCompleteQueryRunSearchOnSelect="false" WatermarkText="Please use at least one filter from below list to retrieve search result(s) in efficient manner." CssClass=""
            Height="100px">

            <SearchButton onclick="__doPostBack('SearchBox1','');" BorderWidth="" BorderColor=""
                ForeColor="" BackColor="" Height="" Width="" CssClass="KeyotisearchBtn" type="button"
                Value="Search" ID="ctl05"></SearchButton>
            <QueryTextBox BorderWidth="" BorderColor="" ForeColor="" BackColor="" Height="" Width="50%"
                CssClass="" Font-Size="20px" type="text" ID="ctl04"></QueryTextBox>
        </searchengine:SearchBox>
        <p>
            <label for="time_entry_issue_id">
                <searchengine1:SearchSuggestions ID="Searchsuggestions2" runat="server" Language="EnglishUS"
                    SpellingSuggestionSource="PresetDictionaryAndOptionalSearchLexicon" CustomDictionaryPath='<%# MapPath("DICT-EN-US-USEnglish_SE.dict") %>'>
                </searchengine1:SearchSuggestions>
            </label>
        </p>
        <br />
        <br />
        <fieldset id="filters1" class="collapsible">
            <legend onclick="toggleFieldset(this);">Filters</legend>
            <div style="">
                <table style="width: 100%">
                    <tbody>
                        <tr>
                            <td>
                                <table id="filters-table" width="100%" cellpadding="3" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <td style="width: 10%">
                                            </td>
                                            <td style="width: 1%">
                                            </td>
                                            <td style="width: 30%">
                                            </td>
                                            <td style="width: 10%">
                                            </td>
                                            <td style="width: 1%">
                                            </td>
                                            <td style="width: 16%">
                                            </td>
                                            <td style="width: 11%">
                                            </td>
                                            <td style="width: 1%">
                                            </td>
                                            <td style="width: 15%">
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label for="time_entry_issue_id">
                                                    Search In</label>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:RadioButtonList ID="rdoSearchInOption" runat="server" RepeatDirection="Horizontal"
                                                    Width="100%" AutoPostBack="false">
                                                    <asp:ListItem Text="Metadata" Value="Metadata" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="Document" Value="Document"></asp:ListItem>
                                                    <asp:ListItem Text="Both" Value="Both"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="time_entry_issue_id">
                                                    Search For</label>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:RadioButtonList ID="rdoSignOption" runat="server" RepeatDirection="Horizontal"
                                                    Width="100%" AutoPostBack="false">
                                                    <asp:ListItem Text="Pre Signature" Value="N" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="Post Signature" Value="Y"></asp:ListItem>
                                                    <asp:ListItem Text="All" Value="all"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="time_entry_issue_id">
                                                    Request Date</label>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                From
                                                <input id="txtFromDate" class="datepicker FROMDATE" runat="server" clientidmode="static" readonly="readonly"
                                                    type="text" autocomplete="false" style="width: 30%;" onchange="ValidateFilterDate(this);" />
                                                &nbsp;&nbsp;To
                                                <input id="txtToDate" class="datepicker TODATE" runat="server" clientidmode="static" readonly="readonly"
                                                    type="text" autocomplete="false" style="width: 30%;" onchange="chkToDate(this);" />
                                            </td>
                                            <td>
                                                <label for="time_entry_issue_id">
                                                    Contract Type</label>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <select id="ddlContracttype" clientidmode="Static" runat="server" style="width: 90%"
                                                    class="chzn-select">
                                                </select>
                                            </td>
                                            <td>
                                                <label for="time_entry_issue_id">
                                                    Assigned Dept</label>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <select id="ddlassigndept" clientidmode="Static" runat="server" style="width: 90%"
                                                    class="chzn-select">
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="time_entry_issue_id">
                                                    Signature Date</label>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                From
                                                <input id="txtSignatureFromDate" class="datepicker FROMDATE" runat="server" clientidmode="static"
                                                    readonly="readonly" type="text" autocomplete="false" style="width: 30%;"  onchange="ValidateFilterDate(this);" />
                                                &nbsp;&nbsp;To
                                                <input id="txtSignatureToDate" class="datepicker TODATE" runat="server" clientidmode="static"
                                                    readonly="readonly" type="text" autocomplete="false" style="width: 30%;" onchange="chkToDate(this);" />
                                            </td>
                                            <td>
                                                <label for="time_entry_issue_id">
                                                    Customer/Supplier/Others</label>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <select id="ddlCustomerSupplier" clientidmode="Static" runat="server" style="width: 90%"
                                                    class="chzn-select chzn-select">
                                                </select>
                                            </td>
                                            <td>
                                                <label for="time_entry_issue_id">
                                                    Request Assigner</label>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <select id="ddlRequestAssigner" clientidmode="Static" runat="server" style="width: 90%"
                                                    class="chzn-select chzn-select">
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="time_entry_issue_id">
                                                    Date of Agreement</label>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                From
                                                <input id="txtEffectiveFromDate" class="datepicker" runat="server" clientidmode="static"
                                                    readonly="readonly" type="text" autocomplete="false" style="width: 30%;" onchange="ValidateFilterDate(this);" />
                                                &nbsp;&nbsp;To
                                                <input id="txtEffectiveToDate" class="datepicker" runat="server" clientidmode="static"
                                                    readonly="readonly" type="text" autocomplete="false" style="width: 30%;" onchange="chkToDate(this);" />
                                            </td>
                                            <td>
                                                <label for="time_entry_issue_id">
                                                    Contract Status</label>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <select id="ddlContractStatus" clientidmode="Static" runat="server" style="width: 90%"
                                                    class="chzn-select chzn-select">
                                                    <option value="0">--------Select-------</option>
                                                    <option value="1">Contract Generation Request submitted</option>
                                                    <option value="2">New Contract Review request submitted</option>
                                                    <option value="3">Contract Renewal Request submitted</option>
                                                    <option value="4">Contract Revision request submitted</option>
                                                    <option value="5">Contract termination request submitted</option>
                                                    <option value="6">Request in progress</option>
                                                    <option value="7">Awaiting approval</option>
                                                    <option value="8">Awaiting signatures</option>
                                                    <option value="9">Active Contract</option>
                                                    <option value="10">Expired Contract</option>
                                                    <option value="11">Terminated Contract</option>
                                                    <option value="12">Negotiation Stage</option>
                                                </select>
                                            </td>
                                            <td>
                                                <label for="time_entry_issue_id">
                                                    Assigned User</label>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <select id="ddlAssignedUser" clientidmode="Static" runat="server" style="width: 90%"
                                                    class="chzn-select chzn-select">
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="time_entry_issue_id">
                                                    Expiry Date</label>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                From
                                                <input id="txtExpiryFromDate" class="datepicker" runat="server" maxlength="100" clientidmode="static"
                                                    readonly="readonly" type="text" autocomplete="false" style="width: 30%;" onchange="ValidateFilterDate(this);" />
                                                &nbsp;&nbsp;To
                                                <input id="txtExpiryToDate" class="datepicker" runat="server" maxlength="100" clientidmode="static"
                                                    readonly="readonly" type="text" autocomplete="false" style="width: 30%;" onchange="chkToDate(this);" />
                                            </td>
                                            <td>
                                                <label for="time_entry_issue_id">
                                                    Contract ID</label>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <input id="txtContractID" class="int" runat="server" maxlength="10" clientidmode="static"
                                                    type="text" autocomplete="false" style="width: 85%;" />
                                            </td>
                                            <td>
                                                <label for="time_entry_issue_id">
                                                    Contract Value</label>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <input id="txtContractValue" class="Currency" runat="server" maxlength="11" clientidmode="static"
                                                    type="text" autocomplete="false" style="width: 85%;" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p class="buttons hide-when-print" style="margin-left: 0px;">
                    <asp:LinkButton ID="btnReset" ClientIDMode="Static" runat="server" OnClientClick="return resetClick();"
                        CssClass="icon icon-reload" OnClick="btnReset_Click">Reset Filter</asp:LinkButton>
                </p>
            </div>
        </fieldset>
    </div>
    <br />
    <div style="display: none">
        <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" Width="1009px">
            <Columns>
                <asp:BoundField DataField="RequestId" HeaderText="Request Id" ReadOnly="True" />
                <asp:BoundField DataField="ContractId" HeaderText="Contract Id" ReadOnly="True" />
                <asp:BoundField DataField="RequestDate" HeaderText="Request Date" ReadOnly="True" />
                <asp:BoundField DataField="ContractType" HeaderText="Contract Type" ReadOnly="True" />
                <asp:BoundField DataField="ContractStatus" HeaderText="Contract Status" ReadOnly="True" />
                <asp:BoundField DataField="ClientName" HeaderText="Customer/Supplier/Others" ReadOnly="True" />
                <asp:BoundField DataField="AssignedTo" HeaderText="Assigned User" ReadOnly="True" />
                <asp:BoundField DataField="SignatureDate" HeaderText="Signature Date" ReadOnly="True" />
                <asp:BoundField DataField="FoundIn" HeaderText="Found In" ReadOnly="True" />
            </Columns>
        </asp:GridView>
    </div>
    <div id="tblMetadataTitle" runat="server">
        <table style="table-layout: fixed; width: 100%;">
            <tr>
                <td width="80%">
                    <h2>
                        Metadata Search Results</h2>
                </td>
                <td width="20%;" style="text-align: right">
                    <asp:LinkButton ID="btnMetaDataExportToExcel" ClientIDMode="Static" CssClass="icon icon-library areadOnly"
                        runat="server" OnClick="btnMetaDataExportToExcel_Click" Enabled="false">Export to Excel</asp:LinkButton>
                    <asp:LinkButton ID="btnMetaDataExportToPDF" ClientIDMode="Static" CssClass="icon icon-masters areadOnly"
                        runat="server" OnClick="btnMetaDataExportToPDF_Click" Enabled="false">Export to PDF</asp:LinkButton>
                </td>
            </tr>
        </table>
        <div class="autoscroll">
            <table width="100%">
                <asp:Repeater ID="rptMetaDataSearch" runat="server">
                    <HeaderTemplate>
                        <table id="masterDataTable" class="reportTable masterTable list issues" width="100%">
                            <thead>
                                <tr>
                                    <th style="width: 6%;">
                                        <asp:LinkButton ID="btnSortRequestId" OnClick="btnSort_Click" CssClass="sort asc"
                                            runat="server">
                                    Request ID
                                        </asp:LinkButton>
                                    </th>
                                    <th style="width: 6%;">
                                        <asp:LinkButton ID="btnSortContractId" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">
                                    Contract ID
                                        </asp:LinkButton>
                                    </th>
                                    <th style="width: 6%;">
                                        <asp:LinkButton ID="btnSortRequestDate" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">
                                    Request Date
                                        </asp:LinkButton>
                                    </th>
                                    <th style="width: 6%;">
                                        <asp:LinkButton ID="btnSortCountractType" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">
                                    Contract Type
                                        </asp:LinkButton>
                                    </th>
                                    <th style="width: 6%;">
                                        <asp:LinkButton ID="btnSortContractStatus" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">
                                    Contract Status
                                        </asp:LinkButton>
                                    </th>
                                    <th style="width: 6%;">
                                        <asp:LinkButton ID="btnSortCustomerSupplier" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">
                                    Customer / Supplier / Others
                                        </asp:LinkButton>
                                    </th>
                                    <th style="width: 6%;">
                                        <asp:LinkButton ID="btnSortAssignedUser" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">
                                    Assigned User
                                        </asp:LinkButton>
                                    </th>
                                    <th style="width: 6%;">
                                        <asp:LinkButton ID="btnSortSignaureDate" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">
                                    Signature Date
                                        </asp:LinkButton>
                                    </th>
                                    <th style="width: 36%;">
                                        Found In
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <%--<asp:HyperLink ID="abtnRequestID" runat="server" Text='<%#Eval("RequestId") %>'>
                        </asp:HyperLink>--%>
                                <asp:LinkButton ID="abtnRequestID" OnClientClick="window.document.forms[0].target='_blank';"
                                    OnClick="abtnRequestID_Click" runat="server" Text='<%#Eval("RequestId") %>'>                                  
                                </asp:LinkButton>
                            </td>
                            <td>
                                <%#Eval("ContractId") %>
                            </td>
                            <td>
                                <%#Eval("RequestDate") %>
                            </td>
                            <td>
                                <%#Eval("ContractType") %>
                            </td>
                            <td>
                                <%#Eval("ContractStatus") %>
                            </td>
                            <td>
                                <%#Eval("ClientName") %>
                            </td>
                            <td>
                                <%#Eval("AssignedTo") %>
                            </td>
                            <td>
                                <%#Eval("SignatureDate") %>
                            </td>
                            <td>
                                <input id="foundin_source_<%#Eval("RequestId") %>" type="hidden" class="found-in" value='<%# ((string)Eval("FoundIn")).Replace("'", "") %>' />
                                <span id="foundin_<%#Eval("RequestId") %>"></span>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        <%--<span>Per Page:</span>
                        <asp:LinkButton ID="lnkbtnMetaData10Records" runat="server" Text="10" OnClick="lnkbtn10Records_Click"></asp:LinkButton>,&nbsp
                        <asp:LinkButton ID="lnkbtnMetaData50Records" runat="server" Text="50" OnClick="lnkbtn50Records_Click"></asp:LinkButton>,&nbsp
                        <asp:LinkButton ID="lnkbtnMetaData100Records" runat="server" Text="100" OnClick="lnkbtn100Records_Click"></asp:LinkButton>--%>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
        

            <input type="hidden" id="searchterm" value='<%=SearchTerm%>' />
            <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.found-in').each(function (index) {
                var cacheString = $('.found-in')[index];
                debugger;

                cacheString = cacheString.value;
                cacheString = cacheString.replace("'", "");
                cacheString = cacheString.replace(/[\\]/g, '\\\\')
                     .replace(/[\/]/g, '\\/')
                     .replace(/[\b]/g, '\\b')
                     .replace(/[\f]/g, '\\f')
                     .replace(/[\n]/g, '\\n')
                     .replace(/[\r]/g, '\\r')
                     .replace(/[\t]/g, '\\t');


                console.log(cacheString);
                cacheString = JSON.parse(cacheString);
                var foundIn = "";
                var lookFor = "<%=SearchTerm%>";
                lookFor = lookFor.toLowerCase();
                if (cacheString) {
                    if ((cacheString.RequestId.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.ContractId.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.RequestDescription.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.ContractingPartyName.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.ContractTerm.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.ContactNumber.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.ContractValue.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.ClientName.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.AssignedToUser.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.Pincode.toLowerCase().indexOf(lookFor) >= 0)) {
                        foundIn += "RequestForm, "
                    }
                    if ((cacheString.LiabilityCap.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.Indemnity.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.Insurance.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.Strictliability.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.TerminationDescription.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.Others.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.AssignmentNovation.toLowerCase().indexOf(lookFor) >= 0) ||

                    //AI - phase 2 fields
                         (cacheString.IndirectConsequentialLosses.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.Warranties.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.EntireAgreementClause.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.ThirdPartyGuaranteeRequired.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.ForceMajeure.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.SetOffRights.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.GoverningLaw.toLowerCase().indexOf(lookFor) >= 0)) {

                        foundIn += "Key Obligation, "
                    }
                    if ((cacheString.ContractTypeName.toLowerCase().indexOf(lookFor) >= 0)) {
                        foundIn += "ContractType, "
                    }

                    if ((cacheString.ActivityText.toLowerCase().indexOf(lookFor) >= 0)) {
                        foundIn += "Activity, "
                    }
                    if ((cacheString.Addresses.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.CityName.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.StateName.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.CountryName.toLowerCase().indexOf(lookFor) >= 0)) {
                        foundIn += "Customer/Supplier/Others, "
                    }
                    if ((cacheString.Question.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.TextValue.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.NumericValue.toLowerCase().indexOf(lookFor) >= 0) ||
                         (cacheString.DecimalValue.toLowerCase().indexOf(lookFor) >= 0)) {
                        foundIn += "Questionnaire, "
                    }
                }
                if (foundIn.length > 2)
                    foundIn = foundIn.substring(0, foundIn.length - 2);
                var ele = $("#foundin_" + cacheString.RequestId);
                ele.text(foundIn);
            });
        });
    </script>
    <br />
    <div id="tblDocumentTitle" runat="server" style="display: none">
        <table style="table-layout: fixed; width: 100%;">
            <tr>
                <td width="80%">
                    <h2>Documents Search Results</h2>
                </td>
                <td width="20%;" style="text-align: right">
                    <asp:LinkButton ID="btnDocSearchExportToExcel" ClientIDMode="Static" CssClass="icon icon-library areadOnly"
                        runat="server" OnClick="btnDocExportToExcel_Click" Enabled="true">Export to Excel</asp:LinkButton>
                    <asp:LinkButton ID="btnDocSearchExportToPDF" ClientIDMode="Static" CssClass="icon icon-masters areadOnly"
                        runat="server" OnClick="btnDocExportToPDF_Click" Enabled="true">Export to PDF</asp:LinkButton>
                </td>
            </tr>
        </table>
        <searchengine:SearchResult ID="SearchResult1" runat="server" Width="100%" HighlightQueryWordsInSummary="True"
            ResultPreviewerEnabled="True" CssClass="autoscroll" NumberOfResultsPerPage="10" OnPageXClicked="SearchResult_pageIndexChanged">
            <HeaderTemplate>
                <table class="reportTable list issues" style="width: 100%">
                    <thead>
                        <tr>
                            <th style="width: 6%;">
                                Request ID
                            </th>
                            <th style="width: 6%;">
                                Contract ID
                            </th>
                            <th style="width: 6%;">
                                Request Date
                            </th>
                            <th style="width: 6%;">
                                Contract Type
                            </th>
                            <th style="width: 6%;">
                                Contract Status
                            </th>
                            <th style="width: 6%;">
                                Customer / Supplier / Others
                            </th>
                            <th style="width: 6%;">
                                Assigned User
                            </th>
                            <th style="width: 6%;">
                                Signature Date
                            </th>
                            <th style="width: 36%;">
                                Found In
                            </th>
                        </tr>
                    </thead>
                    <br />
                    <%-- <searchengine1:SearchSuggestions ID="SearchSuggestions1" Language="EnglishUS" runat="server"
                SpellingSuggestionSource="PresetDictionaryAndOptionalSearchLexicon" CustomDictionaryPath='<%# MapPath("DICT-EN-US-USEnglish_SE.dict") %>'>
                <SuggestedLink Font-Italic="True">
                        suggestedExpression
                </SuggestedLink>
            </searchengine1:SearchSuggestions>--%>
            </HeaderTemplate>
            <ErrorMessageTemplate>
                <div class='SEError nodata' style='font-family: sans-serif, Verdana, Arial, Helvetica;
                    font-size: 9pt; padding: 4px;'>
                    There was an error in the search expression;<p>
                        <%#Container.Message%></p>
                    <p>
                        <%#Container.ExceptionMessage%></p>
                </div>
            </ErrorMessageTemplate>
            <NoResultsTemplate>
                <br />
                <div id="divError" runat="server" class='SENoResults nodata' style='display: none;'>
                    Sorry, no results were found for the query &quot;<%# Container.QueryExpression %>&quot;.
                    <br />
                    <%# Container.IgnoredWordsMessage %>
                </div>
                <p id="pError" runat="server" class="nodata" style="display: none;">
                </p>
            </NoResultsTemplate>
            <NoQueryTemplate>
                <div class='SENoResults  nodata' style='font-family: sans-serif, Verdana, Arial, Helvetica;
                    font-size: 9pt; padding: 4px;'>
                    No data to display.
                    <br />
                </div>
            </NoQueryTemplate>
            <ResultItemTemplate>
                <tbody>
                    <tr>
                        <td>
                            <asp:HyperLink ID="abtnRequestID" runat="server" Text="-" Target="_blank">
                            </asp:HyperLink>
                        </td>
                        <td>
                            <asp:Label ID="lblContractId" runat="server" Text="">
                            </asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblRequestDate" runat="server" Text="">
                            </asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblContractType" runat="server" Text="">
                            </asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblContractStatus" runat="server" Text="">
                            </asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblCustomerSupplier" runat="server" Text="">
                            </asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblAssignedUser" runat="server" Text="">
                            </asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblSignatureDate" runat="server" Text="">
                            </asp:Label>
                        </td>
                        <td>
                            <asp:HyperLink ID="aDocTitle" runat="server" Target="_blank" ClientIDMode="Static"
                                NavigateUrl="javascript:void(0);" onclick="assignDocTilte(this);">
                            </asp:HyperLink>
                            <asp:Label ID="lblDocTitleUrl" runat="server" Text='<%# Container.Uri %>' Style="display: none;"></asp:Label>
                            <span id="spandocshow" class='sew_previewResultWrapper' runat="server">...<img alt="Click to preview the document text"
                                onclick="fnTextLoad(this);sew_toggleResultPreview(this, '<%# Container.UriAsStored %>', '<%# Container.ResultPreview_Expander_ClosedUrl %>', '<%# Container.ResultPreview_Expander_OpenedUrl %>')"
                                src="<%# Container.ResultPreview_Expander_ClosedUrl %>" /><span class='sew_previewResultContent'>Loading
                                    document...</span></span>
                        </td>
                    </tr>
                </tbody>
            </ResultItemTemplate>
            <FooterTemplate>
                <table border="0" cellpadding="0" cellspacing="3" style="font-weight: bold">
                    <tr>
                        <div class='pagination'>
                        </div>
                        <td style='font-family: sans-serif, Verdana, Arial, Helvetica; font-size: 9pt;'>
                            Page
                            <%# Container.PageLinksBlock %>:
                        </td>
                        <td>
                            <asp:Label ID="lbltotal" CssClass="pagination" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center; width: 70%">
                            <span>Per Page:</span>
                            <asp:LinkButton ID="lnkbtn10Records" runat="server" Text="10" OnClick="lnkbtn10Records_Click"></asp:LinkButton>,&nbsp
                            <asp:LinkButton ID="lnkbtn50Records" runat="server" Text="50" OnClick="lnkbtn50Records_Click"></asp:LinkButton>,&nbsp
                            <asp:LinkButton ID="lnkbtn100Records" runat="server" Text="100" OnClick="lnkbtn100Records_Click"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </FooterTemplate>
        </searchengine:SearchResult>
    </div>
   
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());

        $(window).load(function () {
            $(".ui-datepicker").hide(); //for removing vertical scrollbar

            $('#masterDataTable').find('tr a').each(function () {
                var oldHref = $(this).attr('href');
                var newHref = oldHref.replace('ReplaceLinkWith', $('#hdnDomainURL').val());
                if ($('#hdnIsDetailAccess').val() == 'False') {
                    $(this).attr('href', '#');
                }
                else {
                    $(this).attr('href', newHref);
                }
            });

            //            if ($('#hdnMyGlobalContractsTable').val() == 'N') {
            //                $('.ac_input').attr('disabled', 'disabled');
            //                $('.KeyotisearchBtn').attr('disabled', 'disabled');
            //            }

            if ($('#hdnAdvanceSearchTable').val() == 'N') {
                $('.ac_input').attr('disabled', 'disabled');
                $('.KeyotisearchBtn').attr('disabled', 'disabled');
            }
        });

        function assignDocTilte(obj) {
            var attr = $(obj).attr('assignedto');
            if (attr > 0) {
                $(obj).attr('href', 'javascript:void(0);');
                $(obj).addClass("areadOnly");
            }
            else {
                var lblhref = $(obj).next().html();

                if ($("#hdnTabPDFDocumentAccess").val() == 'True') {
                    if (lblhref.indexOf('.pdf') > 0) {
                        $(obj).attr('href', lblhref);
                    }
                }

                if ($("#hdnDownloadContractAccess").val() == 'True') {
                    if (lblhref.indexOf('.docx') > 0) {
                        $(obj).attr('href', lblhref);
                    }
                }
                if (lblhref.indexOf('.docx') < 0 && lblhref.indexOf('.pdf') < 0) {
                    $(obj).attr('href', lblhref);
                }
            }
        }

        if ($('#hdnMyGlobalContractsTable').val() == 'N') {
            $('#divDocSearch').show();
        }

        var doctitle = "";
        var index = 0;
        var extIndex = 0;
        function fnTextLoad(obj) {
            //doctitle = $(obj).parent().prev().prev().html();
            doctitle = $(obj).parent().prev().html().toLowerCase();
            if (doctitle != "") {
                index = doctitle.lastIndexOf("/");
                extIndex = doctitle.lastIndexOf(".");
            }
            if (index > 0) {
                doctitle = doctitle.substring(index + 1, extIndex);
            }
        }

        function sew_OnResultPreviewTextLoaded(url, text, button, closedImgURL, openedImgURL) {

            if (doctitle == '' || text.toLowerCase().indexOf(doctitle) <= 0) {
                return text;
            } else
                return text.substring(0, text.toLowerCase().indexOf(doctitle) - 1);
        }

        $(function () {
            $(".datepicker").datepicker({ showOn: "both",
                buttonImage: "../Styles/css/icons/16/calendar_1.png",
                buttonImageOnly: true,
                prevText: 'Previous',
                //yearRange: 'c-50:c+50',
                yearRange: "-100:+10",
                changeMonth: true,
                dateFormat: 'dd-M-yy',
                buttonText: '',
                onClose: function (dateText, inst) {
                },
                changeYear: true
            }).attr('placeholder', 'dd-MMM-yyyy').blur(function () {
            });
        });

        function ValidateFilterDate(obj) {
            if ($(obj).val() != "") {
                if ($(obj).next().next().val() == "") {
                    $(obj).next().next().val($(obj).val());
                }
            }
            if ($(obj).next().next().val() != "") {
                CompareDate(obj);
            }
        }

        function chkToDate(obj) {
            if ($(obj).prev().prev().val() == "") {
                $(obj).prev().prev().val($(obj).val());
            }

            if ($(obj).prev().prev().val() != "") {
                CompareDate($(obj).prev().prev());
            }
        }
        function CompareDate(obj) {
            //Note: 00 is month i.e. January
            var dateOne = new Date($(obj).val()); //Year, Month, Date
            var dateTwo = new Date($(obj).next().next().val()); //Year, Month, Date
            var DateSender = "Request Date";
            if ($(obj).attr("id").indexOf("Signature") > 0) {
                DateSender = "Signature Date";
            }
            if ($(obj).attr("id").indexOf("Effective") > 0) {
                DateSender = "Effective Date";
            }
            if ($(obj).attr("id").indexOf("Expiry") > 0) {
                DateSender = "Expiry Date";
            }

            if (dateOne > dateTwo) {
                alert(DateSender + " : To Date is greater than From Date.");
                $(obj).next().next().val($(obj).val());
            }
        }

        $('.KeyotisearchBtn').live("click", function () {
            ShowProgress();
        });
    </script>
</asp:Content>
