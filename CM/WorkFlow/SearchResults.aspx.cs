using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Linq;
using MasterBLL;
using CommonBLL;
using WorkflowBLL;
using System.Text.RegularExpressions;
using UserManagementBLL;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public partial class SearchResults : System.Web.UI.Page
{
    // navigation//
    public static int RecordsPerPage = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["RecordsPerPage"]);
    public static int VisibleButtonNumbers = 50;
    // navigation//
    bool TabPDFDocumentAccess;
    bool DownloadContractAccess;
    public string Add;
    public string View;
    public string Update;
    public string Delete;

    public String SearchTerm { get;  set; }
    IDocumentSearch objDocumentSearch;
    List<int> myList = new List<int>();
    static List<DocumentSearch> mySearchList = new List<DocumentSearch>();

    DataSet ds;
    static DataTable dtExport = new DataTable();
    string title = "";
    string filePath = "";
    string customdata = "";

    string strContactInfo = "";
    int RequestId = 0;
    int AssignToId = 0;
    int RowCnt = 0;
    string[] splitSessionVal;

    static string strEventArg = "";

    protected void Page_Init(object sender, System.EventArgs e)
    {
        //string tenant = "CM";
        if (Session[Declarations.User] != null)
        {
            string PhysPath = Server.MapPath("~");
            PhysPath = PhysPath + @"\Uploads" + @"\" + HttpContext.Current.Session["TenantDIR"].ToString();
            string indexDirPath = PhysPath + @"\IndexDirectory";
            SearchResult1.IndexDirectory = indexDirPath;
            SearchBox1.AutoCompleteQueryIndexDirectory = indexDirPath;
            SearchResult1.Configuration.LemmaSearchEnabled = false;
            SearchResult1.Configuration.ImpliedLogicOperator = Keyoti.SearchEngine.Search.ImpliedLogicOperator.Or;
            SearchResult1.LicenseKey = System.Configuration.ConfigurationManager.AppSettings["KeyotiLicenseKey"];

            if (SearchResult1.QueryExpression == null)
            {
                Session["DocRecordPerPage"] = 50;
                Session["SearchIn"] = "metadata";
                Session["SearchFor"] = "N";
                Session["ResetSearchBox"] = null;
                Session["RequestDate"] = null;
                Session["SignatureDate"] = null;
                Session["EffectiveDate"] = null;
                Session["ExpiryDate"] = null;
                Session["ContractTypeClientContStatus"] = null;
                Session["ContractID&Value"] = null;
                Session["deptRequesterAssignTo"] = null;
            }
        }
    }

    protected void Page_Load(object sender, System.EventArgs e)
    {
        string searchTerm = Request.QueryString["QueryExpr"];
        if(searchTerm!= null &&  searchTerm != string.Empty)
        {
            SearchTerm = searchTerm.Replace("\"", ""); 
        }
        
        if (Session[Declarations.User] != null)
        {
            if (!IsPostBack)
            {
                Access.PageAccess(this, Session[Declarations.User].ToString(), "WorkFlow_");
                ViewState[Declarations.Add] = Access.Add.Trim();
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();

                objDocumentSearch = FactoryWorkflow.GetDocumentSearchDetails();
                SetAccess();
                FillDropDown();

                if (!string.IsNullOrEmpty(strContactInfo))
                {
                    System.Threading.Thread.Sleep(0);
                }
                Session[Declarations.SortControl] = null;
                if (Session["DocRecordPerPage"] == null)
                {
                    SearchResult1.NumberOfResultsPerPage = 5;
                    Session["DocRecordPerPage"] = 5;
                }
                else
                {
                    SearchResult1.NumberOfResultsPerPage = Convert.ToInt16(Session["DocRecordPerPage"]);
                }
                if (Convert.ToString(Session["ResetSearchBox"]).ToLower() == Convert.ToString("Reset").ToLower())
                {
                    SearchBox1.Text = "";
                    SearchResult1.QueryExpression = null;
                    Session["ResetSearchBox"] = null;
                }
            }
            SetSearchGrid(sender);
            //var runningTask = System.Threading.Tasks.Task.Factory.StartNew(() => SetSearchGrid(sender));
            SearchBox1.Text = SearchResult1.QueryExpression;
            SetNavigationButtonParameters();
            setControlVal();
        }
    }
    private void SetAccess()
    {
        ViewState["DownloadContractAccess"] = Access.isCustomised(14);
        if (ViewState["DownloadContractAccess"] != null)
        {
            DownloadContractAccess = Convert.ToBoolean(ViewState["DownloadContractAccess"].ToString());
            hdnDownloadContractAccess.Value = DownloadContractAccess.ToString();
        }

        ViewState["TabPDFDocumentAccess"] = Access.isCustomised(31);
        if (ViewState["TabPDFDocumentAccess"] != null)
        {
            TabPDFDocumentAccess = Convert.ToBoolean(ViewState["TabPDFDocumentAccess"].ToString());
            hdnTabPDFDocumentAccess.Value = TabPDFDocumentAccess.ToString();
        }
    }

    private void FillDropDown()
    {
        if (ddlContracttype.Items.Count == 0)
        {
            IContractType objcontracttype;
            objcontracttype = FactoryMaster.GetContractTypeDetail();
            ddlContracttype.extDataBind(objcontracttype.SelectData());
        }

        if (ddlCustomerSupplier.Items.Count == 0)
        {
            IClient Client;
            Client = FactoryWorkflow.GetClientDetails();
            ddlCustomerSupplier.extDataBind(Client.SelectClientData());
        }

        if (ddlassigndept.Items.Count == 0)
        {
            IContractRequest objcontractreq;
            objcontractreq = FactoryWorkflow.GetContractRequestDetails();
            ddlassigndept.extDataBind(objcontractreq.SelectDeptData());
        }

        if (ddlRequestAssigner.Items.Count == 0 || ddlAssignedUser.Items.Count == 0)
        {
            IUsers objuser;
            objuser = FactoryUser.GetUsersDetail();

            if (ddlRequestAssigner.Items.Count == 0)
            {
                ddlRequestAssigner.extDataBind(objuser.SelectData());
            }

            if (ddlAssignedUser.Items.Count == 0)
            {
                ddlAssignedUser.extDataBind(objuser.SelectData());
            }
        }

        if (ddlCustomerSupplier.Items.Count == 0)
        {
            IAvailableClient objAvailableClient;
            objAvailableClient = FactoryWorkflow.GetAvailableClientDetails();
        }
    }
    private void SetSearchGrid(object sender)
    {
        if (SearchResult1.QueryExpression != null && !strEventArg.Contains("abtnrequestid")
                && (strEventArg.Contains("paginationbuttons1")
                    || strEventArg.Contains("ctl00$maincontent$searchresult1")
                    || strEventArg.Contains("rptmetadatasearch")
                    || strEventArg.Contains("docsearchexport")
                    || strEventArg.Contains("0records")
                    || strEventArg.Contains("searchbox1")
                    || Session["SearchIn"] != null
                    || Session["SearchFor"] != null

                ))
        {
            string SearchIn = Session["SearchIn"].ToString().ToLower();

            if (SearchIn == "metadata")
            {
                BindMetaDataSearchResult(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
                SetNavigationButtonParameters();
                tblMetadataTitle.Style.Add("display", "");
                tblDocumentTitle.Style.Add("display", "none");
            }
            else if (SearchIn == "document")
            {
                Keyoti.SearchEngine.Web.ResultsPageClickEventArgs ke = new Keyoti.SearchEngine.Web.ResultsPageClickEventArgs(1);
                SearchResult_pageIndexChanged(sender, ke);
                tblMetadataTitle.Style.Add("display", "none");
                tblDocumentTitle.Style.Add("display", "");
            }
            else if (SearchIn == "both")
            {
                BindMetaDataSearchResult(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
                SetNavigationButtonParameters();
                Keyoti.SearchEngine.Web.ResultsPageClickEventArgs ke = new Keyoti.SearchEngine.Web.ResultsPageClickEventArgs(1);
                SearchResult_pageIndexChanged(sender, ke);
                tblMetadataTitle.Style.Add("display", "");
                tblDocumentTitle.Style.Add("display", "");
            }
            setControlVal();
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        //base.VerifyRenderingInServerForm(control);
    }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    private void InitializeComponent()
    {

    }
    #endregion

    protected void SearchResult1_ItemCreated(object sender, Keyoti.SearchEngine.Web.SearchResultItemEventArgs e)
    {
        if (e.Item is Keyoti.SearchEngine.Web.NoResults)
        {
            if (SearchResult1.QueryExpression == "")
            {
                HtmlGenericControl pError = (HtmlGenericControl)e.Item.FindControl("pError");
                pError.InnerHtml = "Please enter characters for search.";
                pError.Attributes.Remove("style");
            }
            else
            {
                HtmlGenericControl divError = (HtmlGenericControl)e.Item.FindControl("divError");
                divError.Attributes.Remove("style");
            }
            setControlVal();
            btnDocSearchExportToExcel.Enabled = false;
            btnDocSearchExportToPDF.Enabled = false;
        }

        if (e.Item is Keyoti.SearchEngine.Web.Footer)
        {
            Label lbltotal = (Label)e.Item.FindControl("lbltotal");
            int NumberOfResults = SearchResult1.NumberOfResults;

            //if (SearchResult1.ResultsPage * Convert.ToInt16(Session["DocRecordPerPage"]) < objDocumentSearch.TotalRecords)
            //    lbltotal.Text = "Showing " + ((SearchResult1.ResultsPage - 1) * Convert.ToInt16(Session["DocRecordPerPage"]) + 1) + " To " + SearchResult1.ResultsPage * Convert.ToInt16(Session["DocRecordPerPage"]) + " Of " + objDocumentSearch.TotalRecords.ToString() + " entries";
            //else
            //    lbltotal.Text = "Showing " + ((SearchResult1.ResultsPage - 1) * Convert.ToInt16(Session["DocRecordPerPage"]) + 1) + " To " + objDocumentSearch.TotalRecords.ToString() + " Of " + objDocumentSearch.TotalRecords.ToString() + " entries";

            if (SearchResult1.ResultsPage * Convert.ToInt16(Session["DocRecordPerPage"]) < NumberOfResults)
                lbltotal.Text = "Showing " + ((SearchResult1.ResultsPage - 1) * Convert.ToInt16(Session["DocRecordPerPage"]) + 1) + " To " + SearchResult1.ResultsPage * Convert.ToInt16(Session["DocRecordPerPage"]) + " Of " + NumberOfResults + " entries";
            else
                lbltotal.Text = "Showing " + ((SearchResult1.ResultsPage - 1) * Convert.ToInt16(Session["DocRecordPerPage"]) + 1) + " To " + NumberOfResults + " Of " + NumberOfResults + " entries";

            setControlVal();
        }

        if (e.Item is Keyoti.SearchEngine.Web.ResultItem)
        {
            Label lblContractId = (Label)e.Item.FindControl("lblContractId");
            HyperLink aDocTitle = (HyperLink)e.Item.FindControl("aDocTitle");
            Label lblRequestDate = (Label)e.Item.FindControl("lblRequestDate");
            Label lblContractType = (Label)e.Item.FindControl("lblContractType");
            Label lblContractStatus = (Label)e.Item.FindControl("lblContractStatus");
            Label lblCustomerSupplier = (Label)e.Item.FindControl("lblCustomerSupplier");
            Label lblAssignedUser = (Label)e.Item.FindControl("lblAssignedUser");
            Label lblSignatureDate = (Label)e.Item.FindControl("lblSignatureDate");
            HyperLink abtnRequestID = (HyperLink)e.Item.FindControl("abtnRequestID");

            //Label lblUriString = (Label)e.Item.FindControl("lblUriString");
            //List<DocumentSearch> myDoclist = new List<DocumentSearch>();

            title = (e.Data as Keyoti.SearchEngine.Search.ResultItem).DocumentRecord.Title;
            filePath = (e.Data as Keyoti.SearchEngine.Search.ResultItem).UriString;
            customdata = (e.Data as Keyoti.SearchEngine.Search.ResultItem).CustomData;

            RequestId = 0;
            DataRow dtContractDtls = null;
            DataRow dtContractActualFileName = null;
            if (customdata != "")
            {
                if (customdata.Contains("#"))
                {
                    string[] ContractInfo = customdata.Split('#');
                    RequestId = Convert.ToInt32(ContractInfo[0]);

                    dtContractDtls = (from ContractDtls in ds.Tables[0].AsEnumerable()
                                      where ContractDtls.Field<Int64>("RequestId") == Convert.ToInt64(RequestId)
                                      select ContractDtls).FirstOrDefault();
                }
            }

            if (title != "")
            {
                dtContractActualFileName = (from ContractActualFileName in ds.Tables[1].AsEnumerable()
                                            where ContractActualFileName.Field<string>("ActuallFileName") == title
                                            select ContractActualFileName).FirstOrDefault();
            }

            if (title.LastIndexOf(".") > 0 && dtContractDtls != null)
            {
                myList.Add(RequestId);
                //Session["IsSearch"] = "Yes";

                HtmlGenericControl spandocshow = (HtmlGenericControl)e.Item.FindControl("spandocshow");
                abtnRequestID.Text = dtContractDtls.ItemArray[0].ToString();
                abtnRequestID.Attributes.Add("RequestId", dtContractDtls.ItemArray[0].ToString());
                lblContractId.Text = dtContractDtls.ItemArray[1] == DBNull.Value ? "-" : dtContractDtls.ItemArray[1].ToString();
                //string FileName = title.Length > 50 ? title.Substring(0, 50) : title.ToString();
                string[] FileNameText = title.Split('_');
                string Excluding = FileNameText[FileNameText.GetUpperBound(0)];
                string Extn = Excluding.Split('.')[1];
                int index = title.IndexOf("_" + Excluding);
                string FileName = index > -1 ? title.Remove(index) + "." + Extn : title;

                if (dtContractActualFileName != null)
                    aDocTitle.Text = dtContractActualFileName.ItemArray[1].ToString();
                else
                    aDocTitle.Text = FileName;

                if (aDocTitle.Text == "")
                {
                    aDocTitle.Attributes.Add("class", "empty");
                }
                lblRequestDate.Text = Convert.ToDateTime(dtContractDtls.ItemArray[8]).ToString("dd-MMM-yyyy");
                lblContractType.Text = dtContractDtls.ItemArray[2].ToString();

                lblContractStatus.Text = dtContractDtls.ItemArray[19].ToString();
                lblCustomerSupplier.Text = dtContractDtls.ItemArray[3].ToString();
                lblAssignedUser.Text = dtContractDtls.ItemArray[6].ToString();

                if (dtContractDtls.ItemArray[25] != DBNull.Value)
                {
                    if (title.ToString().ToLower().Contains(dtContractDtls.ItemArray[25].ToString().ToLower() + "_"))
                    {
                        if (dtContractDtls.ItemArray[23] != DBNull.Value)
                        {
                            lblSignatureDate.Text = Convert.ToDateTime(dtContractDtls.ItemArray[23]).ToString("dd-MMM-yyyy");
                        }
                        else if (dtContractDtls.ItemArray[24] != DBNull.Value)
                        {
                            lblSignatureDate.Text = Convert.ToDateTime(dtContractDtls.ItemArray[24]).ToString("dd-MMM-yyyy");
                        }
                    }
                }

                //lblSignatureDate.Text = dtContractDtls.ItemArray[23] && dtContractDtls.ItemArray[24] == DBNull.Value ? dtContractDtls.ItemArray[23] == DBNull.Value ? Convert.ToDateTime(dtContractDtls.ItemArray[24]).ToShortDateString() : Convert.ToDateTime(dtContractDtls.ItemArray[23]).ToShortDateString() : Convert.ToDateTime(dtContractDtls.ItemArray[24]).ToShortDateString();

                abtnRequestID.NavigateUrl = "RequestFlow.aspx?Sender=DocSearch&Status=Workflow&RequestId=" + abtnRequestID.Text;
                aDocTitle.Attributes.Add("assignedto", "0");
                if (hdnDownloadContractAccess.Value == "False" && title.Contains(".docx"))
                {
                    aDocTitle.Attributes.Add("class", "areadOnly");
                    spandocshow.Attributes.Add("style", "display:none");
                }
                if (hdnTabPDFDocumentAccess.Value == "False" && title.Contains(".pdf"))
                {
                    aDocTitle.Attributes.Add("class", "areadOnly");
                    spandocshow.Attributes.Add("style", "display:none");
                }
            }
            else if (title.LastIndexOf(".") > 0)
            {
                string[] FileNameText = title.Split('_');
                string Excluding = FileNameText[FileNameText.GetUpperBound(0)];
                string Extn = Excluding.Split('.')[1];
                int index = title.IndexOf("_" + Excluding);
                string FileName = index > -1 ? title.Remove(index) + "." + Extn : title;

                aDocTitle.Text = FileName;
            }
            else
            {
                if (aDocTitle.Text == "")
                {
                    aDocTitle.Attributes.Add("class", "empty");
                }
            }
        }
        Session["DocSearchList"] = myList.Distinct().ToList();
        objDocumentSearch.TotalRecords = myList.Count;
    }

    protected void SearchResult_pageIndexChanged(object sender, Keyoti.SearchEngine.Web.ResultsPageClickEventArgs e)
    {
        if (e.PageNo > 0)
        {
            if (SearchResult1.QueryExpression != null)
            {
                System.Text.StringBuilder queryB = new System.Text.StringBuilder(SearchResult1.QueryExpression);
                SearchResult1.QueryExpression = queryB.ToString();
                BindDocSearch();
                if (Session["SearchIn"].ToString().ToLower() == "document")
                {
                    tblMetadataTitle.Style.Add("display", "none");
                    tblDocumentTitle.Style.Add("display", "block");
                }
            }
        }
    }

    void CentralEventDispatcher_Action(object sender, Keyoti.SearchEngine.Events.ActionEventArgs e)
    {
        if (e.ActionData.Name == Keyoti.SearchEngine.Events.ActionName.ResultItemsFinalized)
        {
            Keyoti.SearchEngine.Utils.ResultItemList results = e.ActionData.Data as Keyoti.SearchEngine.Utils.ResultItemList;
            strContactInfo = "";
            ds = new DataSet();
            assignValuetoPropoerty();

            ds = objDocumentSearch.ReadFilterData();

            //dtExport = null;
            dtExport.Clear();
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < results.Count; i++)
                {
                    strContactInfo = results[i].CustomData == null ? "" : results[i].CustomData;
                    RequestId = 0;
                    AssignToId = 0;

                    if (i == 0 && dtExport.Columns.Count == 0)
                    {
                        //dtExport = new DataTable();
                        dtExport.Columns.Add("RequestID");
                        dtExport.Columns.Add("ContractID");
                        dtExport.Columns.Add("RequestDate");
                        dtExport.Columns.Add("ContractType");
                        dtExport.Columns.Add("ContractStatus");
                        dtExport.Columns.Add("ClientName");
                        dtExport.Columns.Add("AssignedTo");
                        dtExport.Columns.Add("SignatureDate");
                        dtExport.Columns.Add("FoundIn");
                    }

                    if (strContactInfo != "")
                    {
                        string StringToBeReplacedWith = results[i].UriString.Substring(0, results[i].UriString.IndexOf("uploads"));
                        string StringToBeReplace = Request.PhysicalApplicationPath.Replace("\\", "/");
                        string FileActualPath = results[i].UriString.Replace(StringToBeReplacedWith, StringToBeReplace).Replace("%20", " ").Replace("%5b", "[").Replace("%5d", "]");

                        if (strContactInfo.Contains("#"))
                        {
                            string[] ContractInfo = strContactInfo.Split('#');
                            RequestId = Convert.ToInt32(ContractInfo[0]);       // Not used in Search Filter
                            //RowCnt = ds.Tables[0].Select("RequestId =" + RequestId).Length;

                            var dataRow = (from ContractDtls in ds.Tables[0].AsEnumerable()
                                           where ContractDtls.Field<Int64>("RequestId") == RequestId
                                           select ContractDtls).FirstOrDefault();

                            if (dataRow == null)
                            {
                                results.RemoveAt(i);
                                i--;
                            }
                            else if (!File.Exists(FileActualPath))
                            {
                                results.RemoveAt(i);
                                i--;
                            }
                            else if (txtSignatureFromDate.Value != "" && results[i].Title.ToString().ToLower().Contains((dataRow.ItemArray[25] + "_").ToString().ToLower()) == false)
                            {
                                results.RemoveAt(i);
                                i--;
                            }
                            else
                            {
                                string SignatureDate = "";
                                SignatureDate = dataRow.ItemArray[23] != DBNull.Value ? Convert.ToDateTime(dataRow.ItemArray[23]).ToString("dd-MMM-yyyy") : (dataRow.ItemArray[24] != DBNull.Value ? Convert.ToDateTime(dataRow.ItemArray[24]).ToString("dd-MMM-yyyy") : "");

                                DataRow dtContractActualFileName = null;
                                string FileName = "";

                                if (results[i].Title != "")
                                {
                                    dtContractActualFileName = (from ContractActualFileName in ds.Tables[1].AsEnumerable()
                                                                where ContractActualFileName.Field<string>("ActuallFileName") == results[i].Title
                                                                select ContractActualFileName).FirstOrDefault();


                                    string[] FileNameText = results[i].Title.Split('_');
                                    string Excluding = FileNameText[FileNameText.GetUpperBound(0)];
                                    string Extn = Excluding.Split('.')[1];
                                    int index = results[i].Title.IndexOf("_" + Excluding);
                                    FileName = index > -1 ? results[i].Title.Remove(index) + "." + Extn : results[i].Title;

                                    if (dtContractActualFileName != null)
                                        FileName = dtContractActualFileName.ItemArray[1].ToString();
                                }

                                dtExport.Rows.Add(dataRow.ItemArray[0].ToString(), dataRow.ItemArray[1].ToString(), Convert.ToDateTime(dataRow.ItemArray[8]).ToString("dd-MMM-yyyy"), dataRow.ItemArray[2].ToString(), dataRow.ItemArray[19].ToString(), dataRow.ItemArray[3].ToString(), dataRow.ItemArray[6].ToString(), SignatureDate, FileName);  //results[i].Title
                            }
                        }
                        else
                        {
                            //AssignToId = Convert.ToInt32(strContactInfo);
                            //RowCnt = ds.Tables[1].Select("ActivityFileUpload =" + results[i].Title.ToString()).Length;

                            var dataRow = (from ContractDtls in ds.Tables[2].AsEnumerable()
                                           where ContractDtls.Field<string>("ActivityFileUpload").Trim() == results[i].Title.ToString().Trim()
                                           select ContractDtls).FirstOrDefault();


                            if (dataRow == null)
                            {
                                results.RemoveAt(i);
                                i--;
                            }
                            else if (txtSignatureFromDate.Value != "")
                            {
                                if (results[i].Title.ToString().ToLower().Contains((dataRow.ItemArray[25] + "_").ToString().ToLower()) == false)
                                {
                                    results.RemoveAt(i);
                                    i--;
                                }
                            }
                            else if (!File.Exists(FileActualPath))
                            {
                                results.RemoveAt(i);
                                i--;
                            }
                            else
                            {
                                DataRow dtContractActualFileName = null;
                                string FileName = "";

                                if (results[i].Title != "")
                                {
                                    dtContractActualFileName = (from ContractActualFileName in ds.Tables[1].AsEnumerable()
                                                                where ContractActualFileName.Field<string>("ActuallFileName") == results[i].Title
                                                                select ContractActualFileName).FirstOrDefault();


                                    string[] FileNameText = results[i].Title.Split('_');
                                    string Excluding = FileNameText[FileNameText.GetUpperBound(0)];
                                    string Extn = Excluding.Split('.')[1];
                                    int index = results[i].Title.IndexOf("_" + Excluding);
                                    FileName = index > -1 ? results[i].Title.Remove(index) + "." + Extn : results[i].Title;

                                    if (dtContractActualFileName != null)
                                        FileName = dtContractActualFileName.ItemArray[1].ToString();
                                }

                                dtExport.Rows.Add("-", "-", "-", "-", "-", "-", "-", "-", FileName);
                            }
                        }
                    }
                    else
                    {
                        results.RemoveAt(i);  // remove if customData is null for index document.
                        i--;
                    }
                }
            }
            else
            {
                results.RemoveRange(0, results.Count);
            }
            objDocumentSearch.TotalRecords = results.Count;
        }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (Request.Params["ctl00$MainContent$btnSort"] != null)
        {
            Session["SortOrder"] = Request.Params["ctl00$MainContent$btnSort"];
        }

        if (Request.Params["ctl00$MainContent$rdoSearchInOption"] != null)
        {
            Session["SearchIn"] = Request.Params["ctl00$MainContent$rdoSearchInOption"];
        }

        if (Request.Params["ctl00$MainContent$rdoSignOption"] != null)
        {
            Session["SearchFor"] = Request.Params["ctl00$MainContent$rdoSignOption"];
        }

        if (Request.Params["ctl00$MainContent$txtFromDate"] != null || Request.Params["ctl00$MainContent$txtToDate"] != null)
        {
            Session["RequestDate"] = Request.Params["ctl00$MainContent$txtFromDate"] + "##" + Request.Params["ctl00$MainContent$txtToDate"];
        }

        if (Request.Params["ctl00$MainContent$txtSignatureFromDate"] != null || Request.Params["ctl00$MainContent$txtSignatureToDate"] != null)
        {
            Session["SignatureDate"] = Request.Params["ctl00$MainContent$txtSignatureFromDate"] + "##" + Request.Params["ctl00$MainContent$txtSignatureToDate"];
        }

        if (Request.Params["ctl00$MainContent$txtEffectiveFromDate"] != null || Request.Params["ctl00$MainContent$txtEffectiveToDate"] != null)
        {
            Session["EffectiveDate"] = Request.Params["ctl00$MainContent$txtEffectiveFromDate"] + "##" + Request.Params["ctl00$MainContent$txtEffectiveToDate"];
        }

        if (Request.Params["ctl00$MainContent$txtExpiryFromDate"] != null || Request.Params["ctl00$MainContent$txtExpiryToDate"] != null)
        {
            Session["ExpiryDate"] = Request.Params["ctl00$MainContent$txtExpiryFromDate"] + "##" + Request.Params["ctl00$MainContent$txtExpiryToDate"];
        }

        if (Request.Params["ctl00$MainContent$ddlContractType"] != null || Request.Params["ctl00$MainContent$ddlCustomerSupplier"] != null || Request.Params["ctl00$MainContent$ddlContractStatus"] != null)
        {
            Session["ContractTypeClientContStatus"] = Request.Params["ctl00$MainContent$ddlContractType"] + "##" + Request.Params["ctl00$MainContent$ddlCustomerSupplier"] + "##" + Request.Params["ctl00$MainContent$ddlContractStatus"];
        }

        if (Request.Params["ctl00$MainContent$txtContractID"] != null || Request.Params["ctl00$MainContent$txtContractValue"] != null)
        {
            Session["ContractID&Value"] = Request.Params["ctl00$MainContent$txtContractID"] + "##" + Request.Params["ctl00$MainContent$txtContractValue"];
        }

        if (Request.Params["ctl00$MainContent$ddlassigndept"] != null || Request.Params["ctl00$MainContent$ddlRequestAssigner"] != null || Request.Params["ctl00$MainContent$ddlAssignedUser"] != null)
        {
            Session["deptRequesterAssignTo"] = Request.Params["ctl00$MainContent$ddlassigndept"] + "##" + Request.Params["ctl00$MainContent$ddlRequestAssigner"] + "##" + Request.Params["ctl00$MainContent$ddlAssignedUser"];
        }

        strEventArg = Page.Request.Params.Get("__EVENTTARGET").ToLower();
    }

    protected void lnkbtn10Records_Click(object sender, EventArgs e)
    {
        Session["DocRecordPerPage"] = "10";
        Response.Redirect(HttpContext.Current.Request.Url.ToString());
    }

    protected void lnkbtn50Records_Click(object sender, EventArgs e)
    {
        Session["DocRecordPerPage"] = "50";
        Response.Redirect(HttpContext.Current.Request.Url.ToString());
    }

    protected void lnkbtn100Records_Click(object sender, EventArgs e)
    {
        Session["DocRecordPerPage"] = "100";
        Response.Redirect(HttpContext.Current.Request.Url.ToString());
    }

    protected void btnSort_Click(object sender, EventArgs e)
    {
        //hdnSearch.Value = "";
        LinkButton clickBtn = (LinkButton)sender;
        Session.Add(Declarations.SortControl, clickBtn);
        BindMetaDataSearchResult(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
    }

    protected void abtnRequestID_Click(object sender, EventArgs e)
    {
        LinkButton clickBtn = (LinkButton)sender;
        Response.Redirect("RequestFlow.aspx?Sender=MetaSearch&Status=Workflow&RequestId=" + clickBtn.Text);
    }

    protected void setControlVal()
    {
        if (Session["SearchIn"] != null)
        {
            rdoSearchInOption.SelectedValue = Session["SearchIn"].ToString();
        }
        if (Session["SearchFor"] != null)
        {
            rdoSignOption.SelectedValue = Session["SearchFor"].ToString();
        }

        if (Session["RequestDate"] != null)
        {
            splitSessionVal = null;
            splitSessionVal = Session["RequestDate"].ToString().Split(new string[] { "##" }, StringSplitOptions.None);
            txtFromDate.Value = splitSessionVal[0];
            txtToDate.Value = splitSessionVal[1];
        }

        if (Session["SignatureDate"] != null)
        {
            splitSessionVal = null;
            splitSessionVal = Session["SignatureDate"].ToString().Split(new string[] { "##" }, StringSplitOptions.None);
            txtSignatureFromDate.Value = splitSessionVal[0];
            txtSignatureToDate.Value = splitSessionVal[1];
        }

        if (Session["EffectiveDate"] != null)
        {
            splitSessionVal = null;
            splitSessionVal = Session["EffectiveDate"].ToString().Split(new string[] { "##" }, StringSplitOptions.None);
            txtEffectiveFromDate.Value = splitSessionVal[0];
            txtEffectiveToDate.Value = splitSessionVal[1];
        }

        if (Session["ExpiryDate"] != null)
        {
            splitSessionVal = null;
            splitSessionVal = Session["ExpiryDate"].ToString().Split(new string[] { "##" }, StringSplitOptions.None);
            txtExpiryFromDate.Value = splitSessionVal[0];
            txtExpiryToDate.Value = splitSessionVal[1];
        }

        if (Session["ContractTypeClientContStatus"] != null)
        {
            splitSessionVal = null;
            splitSessionVal = Session["ContractTypeClientContStatus"].ToString().Split(new string[] { "##" }, StringSplitOptions.None);
            ddlContracttype.Value = splitSessionVal[0];
            ddlCustomerSupplier.Value = splitSessionVal[1];
            ddlContractStatus.Value = splitSessionVal[2];
        }

        if (Session["ContractID&Value"] != null)
        {
            splitSessionVal = null;
            splitSessionVal = Session["ContractID&Value"].ToString().Split(new string[] { "##" }, StringSplitOptions.None);
            txtContractID.Value = splitSessionVal[0];
            txtContractValue.Value = splitSessionVal[1];
        }

        if (Session["deptRequesterAssignTo"] != null)
        {
            splitSessionVal = null;
            splitSessionVal = Session["deptRequesterAssignTo"].ToString().Split(new string[] { "##" }, StringSplitOptions.None);
            ddlassigndept.Value = splitSessionVal[0];
            ddlRequestAssigner.Value = splitSessionVal[1];
            ddlAssignedUser.Value = splitSessionVal[2];
        }
    }

    void BindMetaDataSearchResult(int pageNo, int recordsPerPage)
    {
        //if (SearchResult1.QueryExpression != "")
        //{
            List<DocumentSearch> SearchList = new List<DocumentSearch>();
            if (!rdoSearchInOption.SelectedValue.ToUpper().Equals("DOCUMENT"))
            {
                Session["IsSearch"] = "Yes";

                objDocumentSearch = FactoryWorkflow.GetDocumentSearchDetails();
                objDocumentSearch.UserId = Convert.ToInt32(Session[Declarations.User]);

                assignValuetoPropoerty();

                objDocumentSearch.PageNo = pageNo == 0 ? 1 : pageNo;
                objDocumentSearch.RecordsPerPage = recordsPerPage == 0 ? 50 : recordsPerPage;
                objDocumentSearch.Search = SearchResult1.QueryExpression;

                LinkButton btnSort = null;
                if (Session[Declarations.SortControl] != null && Session[Declarations.SortControl] != string.Empty)
                {
                    btnSort = (LinkButton)Session[Declarations.SortControl];
                    objDocumentSearch.Direction = btnSort.CssClass.ToLower() == "sort desc" ? 1 : 0;
                    objDocumentSearch.SortColumn = btnSort.Text.Trim();
                }
                mySearchList.Clear();
                SearchList = objDocumentSearch.MetaDataSearch();
                mySearchList = SearchList;
                rptMetaDataSearch.DataSource = SearchList;
                rptMetaDataSearch.DataBind();
                if (btnSort != null)
                {
                    rptMetaDataSearch.ClassChange(btnSort);
                    Session["SortOrder"] = btnSort.CssClass;
                }
                Page.TraceWrite("ReadData ends.");
                ViewState[Declarations.TotalRecord] = objDocumentSearch.TotalRecords;
                PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
                PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;

                if (SearchList.Count > 0)
                {
                    btnMetaDataExportToExcel.Enabled = true;
                    btnMetaDataExportToPDF.Enabled = true;
                }
                else
                {
                    btnMetaDataExportToExcel.Enabled = false;
                    btnMetaDataExportToPDF.Enabled = false;
                }
                rptMetaDataSearch.Visible = true;
            }
            else
            {
                rptMetaDataSearch.Visible = false;
            }

            Session["MetaSearchList"] = SearchList.Select(l => l.RequestId).ToList();
        //}

    }

    #region Export
    protected void btnMetaDataExportToExcel_Click(object sender, EventArgs e)
    {
        DataTable dt = GetDataTable();
        if (dt.Rows.Count > 0)
        {
            string fileName = "Metadata_Search_Result" + ".xls";
            gvDetails.Caption = "<div align=center><font size=4 ><b>MetaData Search Result</b></font></div>";
            gvDetails.DataSource = dt.extReplaceExcelQuotes();
            gvDetails.DataBind();
            ExportToExcell(fileName);
        }
    }
    protected void btnMetaDataExportToPDF_Click(object sender, EventArgs e)
    {
        DataTable dt = GetDataTable();
        if (dt.Rows.Count > 0)
        {
            string fileName = "Metadata Search Result.pdf";
            gvDetails.Caption = "<div align=center><font size=4 ><b>Metadata Search Result</b></font></div>";
            gvDetails.DataSource = dt.extReplaceExcelQuotes();
            gvDetails.DataBind();
            ExportToPdf(fileName, "Metadata Search Result");
        }
    }

    protected void btnDocExportToExcel_Click(object sender, EventArgs e)
    {
        if (dtExport.Rows.Count > 0)
        {
            string fileName = "Document_Search_Result" + ".xls";
            gvDetails.Caption = "<div align=center><font size=4 ><b>Document Search Result</b></font></div>";
            gvDetails.DataSource = dtExport.extReplaceExcelQuotes();
            gvDetails.DataBind();
            ExportToExcell(fileName);
        }
    }
    protected void btnDocExportToPDF_Click(object sender, EventArgs e)
    {
        if (dtExport.Rows.Count > 0)
        {
            string fileName = "Document Search Result.pdf";
            gvDetails.Caption = "<div align=center><font size=4 ><b>Document Search Result</b></font></div>";
            gvDetails.DataSource = dtExport.extReplaceQuotes();
            gvDetails.DataBind();
            ExportToPdf(fileName, "Document Search Result");
        }
    }

    void ExportToPdf(string pdfFileName, string reportTitle)
    {
        Response.ContentType = "application/pdf";
        string strAttachmnet = "attachment;filename=" + pdfFileName;
        Response.AddHeader("content-disposition", strAttachmnet);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter s_w = new StringWriter();
        HtmlTextWriter h_w = new HtmlTextWriter(s_w);
        gvDetails.AllowPaging = false;
        gvDetails.RenderControl(h_w);

        string strOldString = "scope=\"col\"";
        string strReplace = strOldString + " style=\"font-weight:bold; font-size:12px\" ";
        string strOut = s_w.ToString().Replace(strOldString, strReplace);
        strOut = "<div align=center><font size=4 ><b>" + reportTitle + "</b></font></div><br>" + strOut;

        StringReader sr = new StringReader(strOut);
        Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        pdfDoc.Open();
        htmlparser.Parse(sr);
        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
    }
    void ExportToExcell(string excelFileName)
    {
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", excelFileName));
        Response.ContentType = "application/ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        gvDetails.AllowPaging = false;
        //Change the Header Row back to white color
        gvDetails.HeaderRow.Style.Add("background-color", "#E1E1E1");
        //Applying stlye to gridview header cells
        for (int i = 0; i < gvDetails.HeaderRow.Cells.Count; i++)
        {
            gvDetails.HeaderRow.Cells[i].Style.Add("background-color", "#E1E1E1");
            gvDetails.HeaderRow.Cells[i].Style.Add("font-weight", "bold");

        }
        int j = 1;
        //Set alternate row color
        foreach (GridViewRow gvrow in gvDetails.Rows)
        {
            gvrow.BackColor = System.Drawing.Color.White;
            if (j <= gvDetails.Rows.Count)
            {
                if (j % 2 != 0)
                {
                    for (int k = 0; k < gvrow.Cells.Count; k++)
                    {
                        gvrow.Cells[k].Style.Add("background-color", "#EFF3FB");
                    }
                }
            }
            j++;
        }
        gvDetails.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }
    DataTable GetDataTable()
    {
        List<DocumentSearch> SearchList = new List<DocumentSearch>();
        objDocumentSearch = FactoryWorkflow.GetDocumentSearchDetails();

        assignValuetoPropoerty();
        objDocumentSearch.UserId = Convert.ToInt32(Session[Declarations.User]);
        objDocumentSearch.Search = SearchResult1.QueryExpression;
        objDocumentSearch.PageNo = 1;
        objDocumentSearch.RecordsPerPage = 1;
        objDocumentSearch.GetAll = "Y";
        LinkButton btnSort = null;
        if (Session[Declarations.SortControl] != null && Session[Declarations.SortControl] != string.Empty)
        {
            btnSort = (LinkButton)Session[Declarations.SortControl];
            objDocumentSearch.Direction = btnSort.CssClass.ToLower() == "sort desc" ? 1 : 0;
            objDocumentSearch.SortColumn = btnSort.Text.Trim();
        }
        SearchList = objDocumentSearch.MetaDataSearch();

        for (int i = 0; i < SearchList.Count; i++)
        {
            var cacheString = SearchList[i].FoundIn;

            cacheString = cacheString.Replace("'", "");
            cacheString = cacheString.Replace("\\", "\\\\")
                 .Replace("/", "\\/")
                 .Replace("\b", "\\b")
                 .Replace("\f", "\\f")
                 .Replace("\n", "\\n")
                 .Replace("\r", "\\r")
                 .Replace("\t", "\\t");

            var foundIn = "";
            string lookFor = SearchResult1.QueryExpression;
            lookFor = lookFor.ToLower();
            try
            {
                dynamic data = JObject.Parse(cacheString);

                if (data != null)
                {
                    if ((Convert.ToString(data.RequestId.Value).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.ContractId.Value).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.RequestDescription.Value).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.ContractingPartyName.Value).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.ContractTerm.Value).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.ContactNumber.Value).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.ContractValue.Value).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.ClientName.Value).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.AssignedToUser.Value).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.Pincode.Value).ToLower().IndexOf(lookFor) >= 0))
                    {
                        foundIn += "RequestForm";
                    }
                    if ((Convert.ToString(data.LiabilityCap).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.Indemnity).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.Insurance).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.Strictliability).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.TerminationDescription).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.Others).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.AssignmentNovation).ToLower().IndexOf(lookFor) >= 0) ||

                    //AI - phase 2 fields
                         (Convert.ToString(data.IndirectConsequentialLosses).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.Warranties).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.EntireAgreementClause).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.ThirdPartyGuaranteeRequired).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.ForceMajeure).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.SetOffRights).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.GoverningLaw).ToLower().IndexOf(lookFor) >= 0))
                    {

                        if (!string.IsNullOrEmpty(foundIn))
                        {
                            foundIn += ", ";
                        }
                        foundIn += "Key Obligation";
                    }
                    if ((Convert.ToString(data.ContractTypeName).ToLower().IndexOf(lookFor) >= 0))
                    {
                        if (!string.IsNullOrEmpty(foundIn))
                        {
                            foundIn += ", ";
                        }
                        foundIn += "ContractType";
                    }

                    if ((Convert.ToString(data.ActivityText).ToLower().IndexOf(lookFor) >= 0))
                    {
                        if (!string.IsNullOrEmpty(foundIn))
                        {
                            foundIn += ", ";
                        }
                        foundIn += "Activity";
                    }
                    if ((Convert.ToString(data.Addresses).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.CityName).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.StateName).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.CountryName).ToLower().IndexOf(lookFor) >= 0))
                    {
                        if (!string.IsNullOrEmpty(foundIn))
                        {
                            foundIn += ", ";
                        }
                        foundIn += "Customer/Supplier/Others";
                    }
                    if ((Convert.ToString(data.Question).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.TextValue).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.NumericValue).ToLower().IndexOf(lookFor) >= 0) ||
                         (Convert.ToString(data.DecimalValue).ToLower().IndexOf(lookFor) >= 0))
                    {
                        if (!string.IsNullOrEmpty(foundIn))
                        {
                            foundIn += ", ";
                        }
                        foundIn += "Questionnaire";
                    }
                }
            }
            catch (Exception ex)
            {
                foundIn += "";
            }

            SearchList[i].FoundIn = foundIn;
        }
        return SearchList.ToDataTable();
    }

    #endregion

    #region Navigation
    void SetNavigationButtonParameters()
    {
        PaginationButtons1.VisibleButtonNumbers = 10;
        //PaginationButtons1.RecordsPerPage = 50;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            BindMetaDataSearchResult(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
            SetNavigationButtonParameters();
        }
    }

    #endregion

    private void BindDocSearch()
    {
        if (SearchResult1.QueryExpression != null)
        {
            Regex RgxUrl = new Regex("[^A-Za-z0-9 ]");
            bool blnisSpecialChar = RgxUrl.IsMatch(SearchResult1.QueryExpression);
            string license = System.Configuration.ConfigurationManager.AppSettings["KeyotiSeachKey"];
            Keyoti.SearchEngine.Search.SearchAgent searchAgent = new Keyoti.SearchEngine.Search.SearchAgent(license, SearchResult1.Configuration);
            searchAgent.Configuration.CentralEventDispatcher.Action += new Keyoti.SearchEngine.Events.ActionEventHandler(CentralEventDispatcher_Action);
            SearchResult1.ItemCreated += new Keyoti.SearchEngine.Web.SearchResult.SearchResultItemEventHandler(SearchResult1_ItemCreated);

            SearchResult1.FilterLoadLevel = Keyoti.SearchEngine.Search.FilterLoadLevel.Everything;
            myList.Clear();
        }
    }

    private void assignValuetoPropoerty()
    {
        objDocumentSearch = FactoryWorkflow.GetDocumentSearchDetails();
        objDocumentSearch.UserId = Convert.ToInt32(Session[Declarations.User]);      

        if (Session["SearchFor"] != "" && Session["SearchFor"] != null)
        {
            if (Session["SearchFor"].ToString().ToLower() != "all")
                objDocumentSearch.SearchFor = Session["SearchFor"].ToString();
        }
        else
        {
            if (Session["SearchFor"].ToString().ToLower() != "all")
                objDocumentSearch.SearchFor = rdoSignOption.SelectedValue;
        }

        if (Session["RequestDate"] != "" && Session["RequestDate"] != null)
        {
            splitSessionVal = null;
            splitSessionVal = Session["RequestDate"].ToString().Split(new string[] { "##" }, StringSplitOptions.None);
            if (splitSessionVal[0] != "")
                objDocumentSearch.RequestFromDate = Convert.ToDateTime(splitSessionVal[0]);
            if (splitSessionVal[1] != "")
                objDocumentSearch.RequestToDate = Convert.ToDateTime(splitSessionVal[1]);
        }
        else
        {
            if (txtFromDate.Value != "")
                objDocumentSearch.RequestFromDate = Convert.ToDateTime(txtFromDate.Value);
            if (txtToDate.Value != "")
                objDocumentSearch.RequestToDate = Convert.ToDateTime(txtToDate.Value);
        }

        if (Session["SignatureDate"] != "" && Session["SignatureDate"] != null)
        {
            splitSessionVal = null;
            splitSessionVal = Session["SignatureDate"].ToString().Split(new string[] { "##" }, StringSplitOptions.None);
            if (splitSessionVal[0] != "")
                objDocumentSearch.SignatureFromDate = Convert.ToDateTime(splitSessionVal[0]);
            if (splitSessionVal[1] != "")
                objDocumentSearch.SignatureToDate = Convert.ToDateTime(splitSessionVal[1]);
        }
        else
        {
            if (txtSignatureFromDate.Value != "")
                objDocumentSearch.SignatureFromDate = Convert.ToDateTime(txtSignatureFromDate.Value);
            if (txtSignatureToDate.Value != "")
                objDocumentSearch.SignatureToDate = Convert.ToDateTime(txtSignatureToDate.Value);
        }

        if (Session["EffectiveDate"] != "" && Session["EffectiveDate"] != null)
        {
            splitSessionVal = null;
            splitSessionVal = Session["EffectiveDate"].ToString().Split(new string[] { "##" }, StringSplitOptions.None);
            if (splitSessionVal[0] != "")
                objDocumentSearch.EffectiveFromDate = Convert.ToDateTime(splitSessionVal[0]);
            if (splitSessionVal[1] != "")
                objDocumentSearch.EffectiveToDate = Convert.ToDateTime(splitSessionVal[1]);
        }
        else
        {
            if (txtEffectiveFromDate.Value != "")
                objDocumentSearch.EffectiveFromDate = Convert.ToDateTime(txtEffectiveFromDate.Value);
            if (txtEffectiveToDate.Value != "")
                objDocumentSearch.EffectiveToDate = Convert.ToDateTime(txtEffectiveToDate.Value);
        }

        if (Session["ExpiryDate"] != "" && Session["ExpiryDate"] != null)
        {
            splitSessionVal = null;
            splitSessionVal = Session["ExpiryDate"].ToString().Split(new string[] { "##" }, StringSplitOptions.None);
            if (splitSessionVal[0] != "")
                objDocumentSearch.ExpiryFromDate = Convert.ToDateTime(splitSessionVal[0]);
            if (splitSessionVal[1] != "")
                objDocumentSearch.ExpiryToDate = Convert.ToDateTime(splitSessionVal[1]);
        }
        else
        {
            if (txtExpiryFromDate.Value != "")
                objDocumentSearch.ExpiryFromDate = Convert.ToDateTime(txtExpiryFromDate.Value);
            if (txtExpiryToDate.Value != "")
                objDocumentSearch.ExpiryToDate = Convert.ToDateTime(txtExpiryToDate.Value);
        }

        if (Session["ContractTypeClientContStatus"] != "" && Session["ContractTypeClientContStatus"] != null)
        {
            splitSessionVal = null;
            splitSessionVal = Session["ContractTypeClientContStatus"].ToString().Split(new string[] { "##" }, StringSplitOptions.None);
            if (splitSessionVal[0] != "")
                objDocumentSearch.ContractTypeID = Convert.ToInt32(splitSessionVal[0]);
            if (splitSessionVal[1] != "")
                objDocumentSearch.ClientID = Convert.ToInt32(splitSessionVal[1]);
            if (splitSessionVal[2] != "")
                objDocumentSearch.ContractStatus = splitSessionVal[2];
        }
        else
        {
            objDocumentSearch.ContractTypeID = Convert.ToInt32(ddlContracttype.Value);//Convert.ToInt32(splitSessionVal[0]);
            objDocumentSearch.ClientID = Convert.ToInt32(ddlCustomerSupplier.Value);
            objDocumentSearch.ContractStatus = ddlContractStatus.Value;
        }

        if (Session["ContractID&Value"] != "" && Session["ContractID&Value"] != null)
        {
            splitSessionVal = null;
            splitSessionVal = Session["ContractID&Value"].ToString().Split(new string[] { "##" }, StringSplitOptions.None);
            if (splitSessionVal[0] != "")
                objDocumentSearch.ContractID = Convert.ToInt32(splitSessionVal[0]);
            if (splitSessionVal[1] != "")
                objDocumentSearch.ContractValue = Convert.ToDecimal(splitSessionVal[1]);
        }
        else
        {
            if (txtContractID.Value != "")
                objDocumentSearch.ContractID = Convert.ToInt32(txtContractID.Value);
            if (txtContractValue.Value != "")
                objDocumentSearch.ContractValue = Convert.ToDecimal(txtContractValue.Value);
        }

        if (Session["deptRequesterAssignTo"] != "" && Session["deptRequesterAssignTo"] != null)
        {
            splitSessionVal = null;
            splitSessionVal = Session["deptRequesterAssignTo"].ToString().Split(new string[] { "##" }, StringSplitOptions.None);
            if (splitSessionVal[0] != "")
                objDocumentSearch.DepartmentId = Convert.ToInt32(splitSessionVal[0]);
            if (splitSessionVal[1] != "")
            {
                objDocumentSearch.RequestUserID = Convert.ToInt32(splitSessionVal[1]);
                objDocumentSearch.RequesterUserId = Convert.ToInt32(splitSessionVal[1]);
            }
            if (splitSessionVal[2] != "")
            {
                objDocumentSearch.AssignToId = Convert.ToInt32(splitSessionVal[2]);
                objDocumentSearch.AssignToUserId = Convert.ToInt32(splitSessionVal[2]);
            }
        }
        else
        {
            objDocumentSearch.DepartmentId = Convert.ToInt32(ddlassigndept.Value);
            objDocumentSearch.RequestUserID = Convert.ToInt32(ddlRequestAssigner.Value);
            objDocumentSearch.RequesterUserId = Convert.ToInt32(ddlRequestAssigner.Value);

            objDocumentSearch.AssignToId = Convert.ToInt32(ddlAssignedUser.Value);
            objDocumentSearch.AssignToUserId = Convert.ToInt32(ddlAssignedUser.Value);
        }
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        Session["RequestDate"] = null;
        Session["SignatureDate"] = null;
        Session["EffectiveDate"] = null;
        Session["ExpiryDate"] = null;
        Session["ContractTypeClientContStatus"] = null;
        Session["ContractID&Value"] = null;
        Session["deptRequesterAssignTo"] = null;
        Session["SearchIn"] = null;
        SearchResult1.QueryExpression = null;
        SearchBox1.Text = "";
        Session["ResetSearchBox"] = "Reset";
        Session["SearchFor"] = null;
        Response.Redirect(HttpContext.Current.Request.Url.ToString());
    }
}
