﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;
using WorkflowBLL;
using MasterBLL;
using System.Web.UI.HtmlControls;

public partial class WorkFlow_AutoRenewalDataData : System.Web.UI.Page
{
    // navigation//
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 10;
    // navigation//
    
    string RequestType = string.Empty;
    public string Add;
    public string View;
    public string Update;
    public string Delete;
    public bool RequestTypeClickAccess;

    
    IAutoRenewal objAutoRenewal;
    IContractTemplate objContractTemplate;



    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();

        if (Session[Declarations.User] != null && !IsPostBack)
        {
            Access.PageAccess(this, Session[Declarations.User].ToString(), "WorkFlow_");
            ViewState[Declarations.Add] = Access.Add.Trim();
            ViewState[Declarations.View] = Access.View.Trim();
            ViewState[Declarations.Delete] = Access.Delete.Trim();
            ViewState[Declarations.Update] = Access.Update.Trim();
            ViewState["RequestTypeClickAccess"] = Access.isCustomised(12);
        }

        AccessVisibility();


        ddlContractType.extDataBind(objContractTemplate.SelectDataRequestType());
     
        if (Request.QueryString["ch"] != null)
        {
            RequestType = Request.QueryString["ch"].ToString(); 
        }
        if (RequestType == string.Empty)
        {
            RequestType = "cnc";
        }

        if ((!IsPostBack || hdnSearch.Value.Trim() != string.Empty) && Session[Declarations.User] != null)
        {
            ReadData(1, RecordsPerPage, Convert.ToInt32(Session[Declarations.User].ToString()), RequestType);
            Message();
        }
        else
        {
            SetNavigationButtonParameters();
        }

    }


    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

        if (ViewState["RequestTypeClickAccess"] != null)
        {
            RequestTypeClickAccess = Convert.ToBoolean(ViewState["RequestTypeClickAccess"].ToString());
        }

    }


    void Message()
    {
        if (Session[Declarations.Message] != null)
        {
            var flg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }

    }

    // navigation//

    #region Navigation
    void SetNavigationButtonParameters()
    {
        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }


    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage, Convert.ToInt32(Session[Declarations.User].ToString()), RequestType);
            AccessVisibility();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        objAutoRenewal.RequestTypeID =0;
        rptContractRequestDetails.DataSource = objAutoRenewal.ReadData();
        rptContractRequestDetails.DataBind();
    }
    #endregion

    // navigation//




    void ReadData(int pageNo, int recordsPerPage, int UserId, string RequestType)
    {

        if (View == "Y")
        {
            Page.TraceWrite("ReadData starts.");
            try
            {
                if (txtContractId.Value.Trim().Length > 0)
                    objAutoRenewal.ContractID = Convert.ToInt32(txtContractId.Value);

                objAutoRenewal.UserID = UserId;
                objAutoRenewal.PageNo = pageNo;
                objAutoRenewal.RequestType = RequestType;
                objAutoRenewal.RecordsPerPage = recordsPerPage;
                objAutoRenewal.ClientName = TxtClientName.Value.Trim();
                if (hdnContractTypeId.Value == "")
                {
                    hdnContractTypeId.Value = "0";
                }
                objAutoRenewal.RequestTypeID = Convert.ToInt32(hdnContractTypeId.Value);
                rptContractRequestDetails.DataSource = objAutoRenewal.ReadData();
                rptContractRequestDetails.DataBind();
                Page.TraceWrite("ReadData ends.");
                ViewState[Declarations.TotalRecord] = objAutoRenewal.TotalRecords;
                PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
                PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
                PaginationButtons1.RecordsPerPage = RecordsPerPage;
            }
            catch (Exception ex)
            {
                Page.TraceWarn("ReadData fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

           
                ddlContractType.Value = hdnContractTypeId.Value;
          
        }

    }

  

      protected void rptContractRequestDetails_ItemDataBound(Object Sender, RepeaterItemEventArgs e) {

          if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) {
             Label ContractID= (Label)e.Item.FindControl("lblContractID");
             if (ContractID.Text== "0") {
                 ContractID.Text = "";
             }
          }


          HtmlTableCell headCheckBoxId = (HtmlTableCell)e.Item.FindControl("headCheckBoxId");
          HtmlTableCell rowCheckBoxId = (HtmlTableCell)e.Item.FindControl("rowCheckBoxId");

          HtmlTableCell headUsedImageId = (HtmlTableCell)e.Item.FindControl("headUsedImageId");
          HtmlTableCell rowUsedImageId = (HtmlTableCell)e.Item.FindControl("rowUsedImageId");

          HtmlTableCell headEditLinkId = (HtmlTableCell)e.Item.FindControl("headEditLinkId");
          HtmlTableCell rowEditLinkId = (HtmlTableCell)e.Item.FindControl("rowEditLinkId");

          //LinkButton imgEdit = (LinkButton)e.Item.FindControl("imgEdit");
          //Label lblRequestTypeNameLink = (Label)e.Item.FindControl("lblRequestTypeNameLink");

          if (headCheckBoxId != null && Delete =="N")
          {
              headCheckBoxId.Visible = false;
          }

          if (headEditLinkId != null && Update == "N")
          {
              headEditLinkId.Visible = false;
          }

          if (rowCheckBoxId != null && Delete == "N")
          {
              rowCheckBoxId.Visible = false;
          }

          if (rowEditLinkId != null && Update == "N")
          {
              rowEditLinkId.Visible = false;
          }

          if (rowUsedImageId != null && Delete == "N")
          {
              rowUsedImageId.Visible = false;
          }
          if (headUsedImageId != null && Delete == "N")
          {
              headUsedImageId.Visible = false;
          }

          //if (imgEdit != null)
          //{
          //    if (RequestTypeClickAccess == false)
          //    {
          //        imgEdit.Visible = false;
          //        lblRequestTypeNameLink.Visible = true;
          //    }
          //    else
          //    {
          //        imgEdit.Visible = true;
          //        lblRequestTypeNameLink.Visible = false;
          //    }
          //}


       }    



    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objAutoRenewal = FactoryWorkflow.GetAutoRenewalDetails();
            objContractTemplate = FactoryMaster.GetContractTemplateDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }



    protected void btnDelete_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Delete starts.");
        string status = "";
        objAutoRenewal.RequestIDs = hdnRequestId.Value;
        try
        {
            status = objAutoRenewal.DeleteRecord();
        }
        catch (Exception ex)
        {
            hdnPrimeIds.Value = string.Empty;
            Page.JavaScriptClientScriptBlock("status", "PageGetMessage('DF')");
            Page.TraceWarn("Delete fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        hdnPrimeIds.Value = string.Empty;
        Page.JavaScriptClientScriptBlock("status", "PageGetMessage('DS')");
        Page.TraceWrite("Delete ends.");
        ReadData(1, RecordsPerPage, Convert.ToInt32(Session[Declarations.User].ToString()), RequestType);


    }


    protected void btnChangeStatus_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Change Status starts.");
        string status = "";
       
        try
        {
           // objAutoRenewal.isActive = rdActive.Checked == true ? "Y" : "N";
            //status = objAutoRenewal.ChangeIsActive();
            ReadData(1, RecordsPerPage, Convert.ToInt32(Session[Declarations.User].ToString()), RequestType);
        }
        catch (Exception ex)
        {
            hdnPrimeIds.Value = string.Empty;
            Page.JavaScriptClientScriptBlock("status", "PageGetMessage('SF')");
            Page.TraceWarn("Change Status fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.JavaScriptClientScriptBlock("status", "PageGetMessage('SS')");
        Page.TraceWrite("Change Status ends.");
    }


    protected void imgEdit_Click(object sender, EventArgs e)
    {
     
        Server.Transfer("RequestFlow.aspx?RequestId=" + hdnRequestId.Value + "&RequestNo=" + hdnRequestNo.Value);
    }
    protected void linkEdit_Click(object sender, EventArgs e)
    {
        string isAutoRenewal = "isAutoRenewal";
        Server.Transfer("ContractRequest.aspx?RequestId=" + hdnRequestId.Value + "&RequestNo=" + hdnRequestNo.Value + "&isAutoRenewal=" + isAutoRenewal);
    }
 

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (View == "N")
        {
            Page.extDisableControls();
            btnChangeStatus.Enabled = false;
            
        }
        else
        {
            btnSearch.Enabled = true;
            txtContractId.Disabled = false;
        }

        if (Update == "N")
        {
            foreach (RepeaterItem item in rptContractRequestDetails.Items)
            {
                ImageButton imgEdit = (ImageButton)item.FindControl("imgEdit");
                imgEdit.Enabled = false;
                imgEdit.Visible = false;
            }
            btnDelete.Enabled = false;
           
            btnChangeStatus.Enabled = false;
        }
        if (Add == "N")
        {
            Page.extDisableControls();
            btnChangeStatus.Enabled = false;
            btnSearch.Enabled = false;
        }
        else
        {
            btnSearch.Enabled = true;
        }

        if (Delete == "N")
        {
            btnDelete.Enabled = false;
            btnDelete.Visible = false;
        }

       
        Page.TraceWrite("AccessVisibility starts.");
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtContractId.Value = "";
        TxtClientName.Value = "";
    }


    
}










