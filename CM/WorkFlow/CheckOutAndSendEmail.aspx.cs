﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EmailBLL;
using System.Configuration;
using DocumentOperationsBLL;
using CommonBLL;

public partial class WorkFlow_CheckOutAndSendEmail : System.Web.UI.Page
{
    static string PATH = ""; //"~/Uploads/" + HttpContext.Current.Session["TenantDIR"] + "/Contract Documents/";
    public static bool TabPDFDocumentAccess;

    protected void Page_Load(object sender, EventArgs e)
    {
        PATH = "~/Uploads/" + Session["TenantDIR"] + "/Contract Documents/";

        ViewState["RequestId"] = Request.QueryString["RequestId"];
        ViewState["ContractId"] = Request.QueryString["ContractId"];
        ViewState["FilePath"] = HttpUtility.UrlEncode(Request.QueryString["FileName"]);        
        ViewState["ContractFileId"] = Request.QueryString["ContractFileId"];
        ViewState["IsPDFFile"] = Request.QueryString["IsPDFFile"];
        ViewState["TabPDFDocumentAccess"] = Access.isCustomised(19);
        ViewState["ParentRequestID"] = "";
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["ParentRequestID"])))
        {
            ViewState["ParentRequestID"] = "&ParentRequestID=" + Request.QueryString["ParentRequestID"];
        }

        string[] hdnFILEPATHWITHGUID = System.Text.RegularExpressions.Regex.Split(ViewState["FilePath"].ToString(), "_");
        int len = hdnFILEPATHWITHGUID.Length;
        string GUID = hdnFILEPATHWITHGUID[len - 1];
        ViewState["FileName"] = ViewState["FilePath"].ToString().Replace("_" + GUID, "");

        aFileName.InnerHtml = HttpUtility.UrlDecode(ViewState["FileName"].ToString());

        if (ViewState["TabPDFDocumentAccess"] != null)
        {
            TabPDFDocumentAccess = Convert.ToBoolean(ViewState["TabPDFDocumentAccess"].ToString());
        }
        
        if (TabPDFDocumentAccess)
        {
            aPDFFile.HRef = PATH + "PDF//" + ViewState["FilePath"] + ".pdf";
            spanPDFFile.Visible = true;
        }
        else
            spanPDFFile.Visible = false;

        if (ViewState["IsPDFFile"].ToString() == "Y")
        {
            spanMSWordFile.Visible = false;
        }
        else
            aMSWordFile.HRef = PATH + ViewState["FilePath"] + ".docx";
    }

    protected void btnSendMail_Click(object sender, EventArgs e)
    {
        SaveAndSendMail();
    }
    
    protected void SaveAndSendMail()
    {
        //Page.JavaScriptClientScriptBlock("Progress", "ShowProgress();");
        System.Threading.Thread.Sleep(1000);
        SendMail sm = new SendMail();
        sm.Parameters.Clear();
        sm.MailTo = txtMailTo.Value;
        sm.MailCc = txtMailCC.Value;// "bgujarathi@myuberall.com"; 
        sm.MailFrom = ConfigurationManager.AppSettings["MailFrom"];
        sm.Subject = txtMailSubject.Value.Trim();
        sm.Body = txtMailBody.Value.Trim().Replace("\n", "<br/>");
        if (chkWordFile.Checked)
            sm.Attachment.Add(Server.MapPath(PATH) + HttpUtility.UrlDecode(ViewState["FilePath"].ToString()) + ".docx");
        if (TabPDFDocumentAccess && chkPDFFile.Checked)
            sm.Attachment.Add(Server.MapPath(PATH + "PDF//") + HttpUtility.UrlDecode(ViewState["FilePath"].ToString()) + ".pdf");

        bool IsSend = sm.SendSimpleMail();

        IContractProcedures objContractProcedures;
        objContractProcedures = new ContractProcedures();
        objContractProcedures.ContractFileId = Convert.ToInt32(ViewState["ContractFileId"]);
        objContractProcedures.ContractFileActivityTypeId = 4; //1=Check-in   2=Check-out   3=Upload   4=Send Mail
        objContractProcedures.IsMailSent = IsSend ? 'Y' : 'N';
        objContractProcedures.ModifiedBy = Convert.ToInt32(Session[Declarations.User]);
        objContractProcedures.IpAddress = Session[Declarations.IP].ToString();
        string retVal = objContractProcedures.FileActivityInsertRecord();

        if (IsSend)
        {
            Response.Redirect("RequestFlow.aspx?Status=Workflow&Section=ContractVersions&Mode=MailSent&ScrollTo=DivContractVersions&RequestId=" + ViewState["RequestId"] + ViewState["ParentRequestID"]);
        }
        else
        {
            Page.JavaScriptClientScriptBlock("Unable", "MessageMasterDiv('Unable to send mail', 1 100)");
        }        
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("RequestFlow.aspx?Status=Workflow&ScrollTo=DivContractVersions&RequestId=" + ViewState["RequestId"] + ViewState["ParentRequestID"]);
    }
}