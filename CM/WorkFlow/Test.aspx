﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="Test.aspx.cs" Inherits="WorkFlow_Test" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <title>Under Construction</title>
    <script type="text/javascript">
        $(document).ready(function () {
            var user_profile = { "language": "en", "entities": [{ "type": "EntireAgreement", "text": "EntireAgreement", "count": 3 }, { "type": "LiabilityCap", "text": "Liability", "count": 3 }, { "type": "ContractingParty", "text": "ASOS", "count": 1 }, { "type": "TerminationforConvenience", "text": "convenience", "count": 1 }, { "type": "StrictLiability", "text": "ensure", "count": 2 }, { "type": "Indemnity", "text": "indemnify", "count": 2}], "warnings": ["truncated-oversized-text-content"] };          
            flagContractingParty = "N";

            flagContractType = "N";

            flagTermnationforMaterialBreach = "N";

            flagTerminationforConvenience = "N";

            flagThirdPartyGuarantee = "N";

            flagTerminationforChangeofControl = "N";

            flagInsolvency = "N";

            flagStrictLiability = "N";

            flagEntireAgreement = "N";



            $.each(user_profile.entities, function (index, value) {                

                if (value.type === "ContractingParty") {

                    document.write("ContractingParty :-");

                    document.write(value.text);

                    document.write("<br>")

                    flagContractingParty = "Y";

                }

                else if (value.type === "ContractType") {

                    document.write("ContractType :-");

                    document.write(value.text);

                    document.write("Y")

                    document.write("<br>")

                    flagContractType = "Y";

                }

                else if (value.type === "TermnationforMaterialBreach") {

                    document.write("TermnationforMaterialBreach :-");

                    document.write("Y");

                    document.write("<br>")

                    flagTermnationforMaterialBreach = "Y";

                }

                else if (value.type === "TerminationforConvenience") {

                    document.write("TerminationforConvenience :-");

                    document.write("Y");

                    document.write("<br>")

                    flagTerminationforConvenience = "Y";

                }

                else if (value.type === "ThirdPartyGuarantee") {

                    document.write("ThirdPartyGuarantee :-");

                    document.write("Y");

                    document.write("<br>")

                    flagThirdPartyGuarantee = "Y";

                }

                else if (value.type === "TerminationforChangeofControl") {

                    document.write("TerminationforChangeofControl :-");

                    document.write("Y");

                    document.write("<br>")

                    flagTerminationforChangeofControl = "Y";

                }

                else if (value.type === "Insolvency") {

                    document.write("Insolvency :-");

                    document.write("Y");

                    document.write("<br>")

                    flagInsolvency = "Y";

                }

                else if (value.type === "StrictLiability") {

                    document.write("StrictLiability :-");

                    document.write("Y");

                    document.write("<br>")

                    flagStrictLiability = "Y";

                }

                else if (value.type === "EntireAgreement") {

                    document.write("EntireAgreement :-");

                    document.write("Y");

                    document.write("<br>")

                    flagEntireAgreement = "Y";

                }





            });

            if (flagContractType === "N") {

                document.write("Contracttype - Others");

                document.write("<br>")

            }

            if (flagEntireAgreement === "N") {

                document.write("EntireAgreement - N")

                document.write("<br>")

            }

            if (flagStrictLiability === "N") {

                document.write("StrictLiability - N");

                document.write("<br>")

            }

            if (flagInsolvency == "N") {

                document.write("Insolvency - N");

                document.write("<br>")

            }

            if (flagTerminationforChangeofControl === "N") {

                document.write("TerminationforChangeofControl - N");

                document.write("<br>")

            }

            if (flagThirdPartyGuarantee === "N") {

                document.write("ThirdPartyGuarantee - N");

                document.write("<br>")

            }

            if (flagTerminationforConvenience === "N") {

                document.write("TerminationforConvenience - N");

                document.write("<br>")

            }

            if (flagTermnationforMaterialBreach === "N") {

                document.write("TermnationforMaterialBreach - N");

                document.write("<br>")

            }

            if (flagContractingParty === "N") {

                document.write("ContractingParty - N");

                document.write("<br>")

            }

        });
    </script>
</asp:Content>
