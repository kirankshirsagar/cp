﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using WorkflowBLL;
using CommonBLL;
using MetaDataConfiguratorsBLL;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class WorkFlow_KeyObligationMaster : System.Web.UI.Page
{
    IKeyFields objkey;
    IActivity objActivity;

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        if (!IsPostBack)
        {
            if (Request.QueryString["LinkStatus"].ToString() != null)
            {
                hdnLinkStatus.Value = Request.QueryString["LinkStatus"].ToString();
            }
            SetDefault();
            BindValues();
            lblRequestIDheader.Text = HdnRequestID.Value;
            lblReqID.Text = HdnRequestID.Value;
            BindContractRequest();
        }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);
        if (Request.Params["ctl00$MainContent$btnBack"] == null)
        {
            BindContractRequest();
        }
    }

    private void BindContractRequest()
    {
        try
        {
            IMetaDataConfigurators objMetaDataConfig = FactoryMedaData.GetContractTemplateDetail();
            HtmlTable container = objMetaDataConfig.GenerateRequestForm("Key Obligations", HdnRequestID.Value, "Edit");
            int returnTableRow = container.Rows.Count;
            int cnt = keyObligationTable.Rows.Count;

            for (int i = 0; i < returnTableRow; i++)
            {
                keyObligationTable.Rows.Add(container.Rows[0]);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataTable SaveData(Control pnl)
    {
        Control ctrl = pnl;
        DataTable dt = new DataTable();
        dt.Columns.Add("MetaDataFieldId", Type.GetType("System.Int32"));
        dt.Columns.Add("FieldValue", Type.GetType("System.String"));
        string InputValue = "", ID = "", Master = "", FieldType = "";

        int FieldTypeId = 0;
        #region GetControlValue
        HtmlTable tbl = (HtmlTable)ctrl.FindControl("keyObligationTable");

        #region fill answers
        foreach (HtmlTableRow row in tbl.Rows)
        {
            // string cls = row.Attributes["Class"];
            if (row.Attributes["class"] == "trdynamic")
            {
                if (row.Style.Value == null || (row.Style.Value != null && !row.Style.Value.Contains("display:none")))
                {
                    Control parentctrl = row.Cells[1];
                    InputValue = "";
                    foreach (var c in parentctrl.Controls.OfType<HtmlInputText>())
                    {
                        InputValue = string.IsNullOrEmpty(c.Value) ? InputValue : c.Value;
                        ID = c.ID;
                        FieldTypeId = Convert.ToInt32(c.Attributes["FieldID"]);
                        FieldType = c.Attributes["FieldType"];
                        if (InputValue != null)
                        {
                            switch (FieldType.ToUpper())
                            {
                                case "TEXT":
                                    dt.Rows.Add(FieldTypeId, InputValue);
                                    break;
                                case "NUMBER":
                                    dt.Rows.Add(FieldTypeId, InputValue);
                                    break;
                                case "DATE":
                                    if (string.IsNullOrEmpty(InputValue))
                                        dt.Rows.Add(FieldTypeId, InputValue);
                                    else
                                        dt.Rows.Add(FieldTypeId, InputValue);
                                    break;
                            }
                        }
                    }

                    foreach (var c in parentctrl.Controls.OfType<HtmlTextArea>())
                    {
                        InputValue = c.Value;
                        ID = c.ID;
                        FieldTypeId = Convert.ToInt32(c.Attributes["FieldID"]);
                        FieldType = c.Attributes["FieldType"];
                        dt.Rows.Add(FieldTypeId, InputValue);
                    }

                    foreach (var ddlList in parentctrl.Controls.OfType<HtmlSelect>())
                    {
                        ID = ddlList.ID;
                        FieldTypeId = Convert.ToInt32(ddlList.Attributes["FieldID"]);
                        Master = ddlList.Attributes["master"];
                        InputValue = "";
                        if (string.IsNullOrEmpty(Master))
                        {
                            foreach (ListItem objItem in ddlList.Items)
                            {
                                if (objItem.Selected)
                                {
                                    InputValue += objItem.Value + ",";
                                }
                            }
                            dt.Rows.Add(FieldTypeId, InputValue);
                        }
                    }
                    foreach (var hdnField in parentctrl.Controls.OfType<HtmlInputHidden>())
                    {
                        ID = hdnField.ID;
                        Master = hdnField.Attributes["master"];
                        InputValue = hdnField.Value;
                        if (!string.IsNullOrEmpty(Master))
                            dt.Rows.Add(FieldTypeId, InputValue);
                    }

                    foreach (var cbList in parentctrl.Controls.OfType<CheckBoxList>())
                    {
                        ID = cbList.ID;
                        FieldTypeId = Convert.ToInt32(cbList.Attributes["FieldID"]);

                        InputValue = "";
                        foreach (ListItem objItem in cbList.Items)
                        {
                            if (objItem.Selected)
                            {
                                InputValue += objItem.Value + ",";
                            }
                        }
                        dt.Rows.Add(FieldTypeId, InputValue.Trim(','));
                    }

                    foreach (var rbList in parentctrl.Controls.OfType<RadioButtonList>())
                    {
                        ID = rbList.ID;
                        FieldTypeId = Convert.ToInt32(rbList.Attributes["FieldID"]);

                        InputValue = rbList.SelectedValue;
                        dt.Rows.Add(FieldTypeId, InputValue);
                    }
                }
            }
        }
        return dt;
        #endregion
        #endregion GetControlValue
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            objkey.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            objkey.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
            objkey.LiabilityCap = txtLiabilityCap.Value;
            objkey.Indemnity = txtIndemnity.Value;
            objkey.Insurance = txtInsurance.Value;
            objkey.AssignmentNovation = txtNovation.Value;
            objkey.Others = txtOthers.Value;
            objkey.TerminationDescription = txtTerminationdesc.Value;
            objkey.Strictliability = txtStrictliability.Value;

            if (chkIsTerminationConvenience.Checked == true)
            {
                objkey.IsTerminationConvenience = "Y";
            }
            else { objkey.IsTerminationConvenience = "N"; }
            if (chkIsTerminationmaterial.Checked == true)
            {
                objkey.IsTerminationmaterial = "Y";
            }
            else { objkey.IsTerminationmaterial = "N"; }
            if (chkIsTerminationinsolvency.Checked == true)
            {
                objkey.IsTerminationinsolvency = "Y";
            }
            else { objkey.IsTerminationinsolvency = "N"; }
            if (chkIsTerminationinChangeofcontrol.Checked == true)
            {
                objkey.IsTerminationinChangeofcontrol = "Y";
            }
            else { objkey.IsTerminationinChangeofcontrol = "N"; }


            if (chkIsTerminationothercauses.Checked == true)
            {
                objkey.IsTerminationothercauses = "Y";
            }
            else { objkey.IsTerminationothercauses = "N"; }


            if (rdIsChangeofControlYes.Checked == true)
            {
                objkey.IsChangeofControl = "Y";
            }
            else if (rdIsChangeofControlNo.Checked == true) { objkey.IsChangeofControl = "N"; }

            if (rdIsStrictliabilityYes.Checked == true)
            {
                objkey.IsStrictliability = "Y";
            }
            else if (rdIsStrictliabilityNo.Checked == true) { objkey.IsStrictliability = "N"; }

            if (rdIsGuaranteeRequiredYes.Checked == true)
            {
                objkey.IsGuaranteeRequired = "Y";
            }
            else if (rdIsGuaranteeRequiredNo.Checked == true) { objkey.IsGuaranteeRequired = "N"; }

            if (rdIsAgreementClauseYes.Checked == true)
            {
                objkey.IsAgreementClause = "Y";
            }
            else if (rdIsAgreementClauseNo.Checked == true) { objkey.IsAgreementClause = "N"; }


            if (rdIndirectConsequentialLossesYes.Checked == true)
            {
                objkey.IsIndirectConsequentialLosses = "Y";
            }
            else if (rdIndirectConsequentialLossesNo.Checked == true) { objkey.IsIndirectConsequentialLosses = "N"; }

            if (rdIndemnityYes.Checked == true)
            {
                objkey.IsIndemnity = "Y";
            }
            else if (rdIndemnityNo.Checked == true) { objkey.IsIndemnity = "N"; }

            if (rdWarrantiesYes.Checked == true)
            {
                objkey.IsWarranties = "Y";
            }
            else if (rdWarrantiesNo.Checked == true) { objkey.IsWarranties = "N"; }

            if (rdForceMajeureYes.Checked == true)
            {
                objkey.IsForceMajeure = "Y";
            }
            else if (rdForceMajeureNo.Checked == true) { objkey.IsForceMajeure = "N"; }
            objkey.ForceMajeure = txtForceMajeure.Value;

            if (rdSetOffRightsYes.Checked == true)
            {
                objkey.IsSetOffRights = "Y";
            }
            else if (rdSetOffRightsNo.Checked == true) { objkey.IsSetOffRights = "N"; }
            objkey.SetOffRights = txtSetOffRights.Value;

            if (rdGoverningLawYes.Checked == true)
            {
                objkey.IsGoverningLaw = "Y";
            }
            else if (rdGoverningLawNo.Checked == true) { objkey.IsGoverningLaw = "N"; }
            objkey.GoverningLaw = txtGoverningLaw.Value;

            objkey.IndirectConsequentialLosses = txtIndirectConsequentialLosses.Value;// hdnIndirectConsequentialLosses.Value;
            objkey.Warranties = hdnWarranties.Value;
            objkey.EntireAgreementClause = hdnAgreementClause.Value;
            objkey.ThirdPartyGuaranteeRequired = hdnGuaranteeRequired.Value;

            DataTable dt = SaveData(pnlnewadd);
            objkey.dtMatadataFieldValues = dt;
            objkey.Param = 2;
            objkey.RequestId = int.Parse(HdnRequestID.Value);
            objkey.InsertRecord();
            SendEmail();
            if (hdnLinkStatus.Value == "Volts")
            {
                if (ViewState["ParentRequestId"].ToString() == HdnRequestID.Value)
                    Response.Redirect("~/WorkFlow/VaultFlow.aspx?ScrollTo=msgkeyobligations&RequestId=" + HdnRequestID.Value + "&Status=KeyObligation ", false);
                else
                    Response.Redirect("~/WorkFlow/VaultFlow.aspx?ScrollTo=msgkeyobligations&RequestId=" + HdnRequestID.Value + "&Status=KeyObligation&ParentRequestID=" + ViewState["ParentRequestId"], false);
            }
            else
            {
                if (ViewState["ParentRequestId"].ToString() == HdnRequestID.Value)
                    Response.Redirect("~/WorkFlow/RequestFlow.aspx?ScrollTo=msgkeyobligations&RequestId=" + HdnRequestID.Value + "&Status=KeyObligation ", false);
                else
                    Response.Redirect("~/WorkFlow/RequestFlow.aspx?ScrollTo=msgkeyobligations&RequestId=" + HdnRequestID.Value + "&Status=KeyObligation&ParentRequestID=" + ViewState["ParentRequestId"], false);
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (hdnLinkStatus.Value == "Volts")
        {
            if (ViewState["ParentRequestId"].ToString() == HdnRequestID.Value)
                Response.Redirect("~/WorkFlow/VaultFlow.aspx?ScrollTo=msgkeyobligations&RequestId=" + HdnRequestID.Value + "&Status=KeyObligation ", false);
            else
                Response.Redirect("~/WorkFlow/VaultFlow.aspx?ScrollTo=msgkeyobligations&RequestId=" + HdnRequestID.Value + "&Status=KeyObligation&ParentRequestID=" + ViewState["ParentRequestId"], false);
        }
        else
        {
            if (ViewState["ParentRequestId"].ToString() == HdnRequestID.Value)
                Response.Redirect("~/WorkFlow/RequestFlow.aspx?ScrollTo=msgkeyobligations&RequestId=" + HdnRequestID.Value + "&Status=KeyObligation ", false);
            else
                Response.Redirect("~/WorkFlow/RequestFlow.aspx?ScrollTo=msgkeyobligations&RequestId=" + HdnRequestID.Value + "&Status=KeyObligation&ParentRequestID=" + ViewState["ParentRequestId"], false);
        }
    }

    private void SetDefault()
    {
        System.Collections.Specialized.NameValueCollection RequestFlow = Request.Form;
        ViewState["RequestId"] = RequestFlow["ctl00$MainContent$hdnRequestId"];
        HdnRequestID.Value = ViewState["RequestId"].ToString();
        ViewState["ContractTypeId"] = RequestFlow["ctl00$MainContent$hdnContractTypeId"];
        ViewState["ParentRequestId"] = RequestFlow["ctl00$MainContent$hdnParentRequestID"];
    }

    public void CreateObjects()
    {
        objkey = FactoryWorkflow.GetKeyFieldsDetails();
        objActivity = FactoryWorkflow.GetActivityDetails();
    }

    public void BindValues()
    {
        try
        {
            List<KeyFeilds> mylist;
            objkey.RequestId = int.Parse(HdnRequestID.Value);
            mylist = objkey.ReadData();
            txtLiabilityCap.Value = mylist[0].LiabilityCap;
            txtIndemnity.Value = mylist[0].Indemnity;
            txtInsurance.Value = mylist[0].Insurance;
            txtNovation.Value = mylist[0].AssignmentNovation;
            txtOthers.Value = mylist[0].Others;
            txtTerminationdesc.Value = mylist[0].TerminationDescription;
            txtStrictliability.Value = mylist[0].Strictliability;
            if (mylist[0].IsChangeofControl == "Y")
            {
                rdIsChangeofControlYes.Checked = true;
            }
            else
                if (mylist[0].IsChangeofControl == "N")
                { rdIsChangeofControlNo.Checked = true; }


            if (mylist[0].IsAgreementClause == "Y")
            {
                rdIsAgreementClauseYes.Checked = true;
                aAgreementClause.Visible = true;
            }
            else if (mylist[0].IsAgreementClause == "N") { rdIsAgreementClauseNo.Checked = true; }

            if (mylist[0].IsStrictliability == "Y")
            {
                rdIsStrictliabilityYes.Checked = true;
            }
            else if (mylist[0].IsStrictliability == "N") { rdIsStrictliabilityNo.Checked = true; }

            if (mylist[0].IsGuaranteeRequired == "Y")
            {
                rdIsGuaranteeRequiredYes.Checked = true;
                aGuaranteeRequired.Visible = true;
            }
            else if (mylist[0].IsGuaranteeRequired == "N") { rdIsGuaranteeRequiredNo.Checked = true; }

            if (mylist[0].IsTerminationConvenience == "Y")
            {
                chkIsTerminationConvenience.Checked = true;
            }

            if (mylist[0].IsTerminationmaterial == "Y")
            {
                chkIsTerminationmaterial.Checked = true;
            }

            if (mylist[0].IsTerminationinsolvency == "Y")
            {
                chkIsTerminationinsolvency.Checked = true;
            }

            if (mylist[0].IsTerminationinChangeofcontrol == "Y")
            {
                chkIsTerminationinChangeofcontrol.Checked = true;
            }

            if (mylist[0].IsTerminationothercauses == "Y")
            {
                chkIsTerminationothercauses.Checked = true;
            }

            txtForceMajeure.Value = mylist[0].ForceMajeure;
            txtSetOffRights.Value = mylist[0].SetOffRights;
            txtGoverningLaw.Value = mylist[0].GoverningLaw;

            if (mylist[0].IsIndirectConsequentialLosses == "Y")
            {
                rdIndirectConsequentialLossesYes.Checked = true;
                aIndirectConsequentialLosses.Visible = true;
            }
            if (mylist[0].IsIndirectConsequentialLosses == "N")
                rdIndirectConsequentialLossesNo.Checked = true;

            if (mylist[0].IsIndemnity == "Y")
            {
                rdIndemnityYes.Checked = true;
                aIndemnity.Visible = true;
            }
            if (mylist[0].IsIndemnity == "N")
                rdIndemnityNo.Checked = true;

            if (mylist[0].IsWarranties == "Y")
            {
                rdWarrantiesYes.Checked = true;
                aWarranties.Visible = true;
            }
            if (mylist[0].IsWarranties == "N")
                rdWarrantiesNo.Checked = true;

            if (mylist[0].IsForceMajeure == "Y")
                rdForceMajeureYes.Checked = true;
            if (mylist[0].IsForceMajeure == "N")
                rdForceMajeureNo.Checked = true;

            if (mylist[0].IsSetOffRights == "Y")
                rdSetOffRightsYes.Checked = true;
            if (mylist[0].IsSetOffRights == "N")
                rdSetOffRightsNo.Checked = true;

            if (mylist[0].IsGoverningLaw == "Y")
                rdGoverningLawYes.Checked = true;
            if (mylist[0].IsGoverningLaw == "N")
                rdGoverningLawNo.Checked = true;

            txtIndirectConsequentialLosses.Value = mylist[0].IndirectConsequentialLosses;
            hdnIndirectConsequentialLosses.Value = mylist[0].IndirectConsequentialLosses;
            hdnIndemnity.Value = mylist[0].Indemnity;
            hdnWarranties.Value = mylist[0].Warranties;
            hdnAgreementClause.Value = mylist[0].EntireAgreementClause;
            hdnGuaranteeRequired.Value = mylist[0].ThirdPartyGuaranteeRequired;
            //txtGoverningLaw.Value = mylist[0].GoverningLaw;

            if (mylist[0].IndirectConsequentialLosses == "")
                aIndirectConsequentialLosses.Visible = false;
            if (mylist[0].Indemnity == "")
                aIndemnity.Visible = false;
            if (mylist[0].Warranties == "")
                aWarranties.Visible = false;
            if (mylist[0].EntireAgreementClause == "")
                aAgreementClause.Visible = false;
            if (mylist[0].ThirdPartyGuaranteeRequired == "")
                aGuaranteeRequired.Visible = false;
        }
        catch { }
    }

    void SendEmail()
    {
        try
        {
            Page.TraceWrite("send emails.");
            EmailTemplateBLL.IEmailTrigger objEmail = EmailTemplateBLL.FactoryEmailTemplate.GetEmailTriggerDetail();
            objEmail.RequestId = int.Parse(HdnRequestID.Value);
            objEmail.ContractTypeId = 99999;
            objEmail.ContractTemplateId = 99999;
            objEmail.EmailTriggerId = 3; // hardcoded for Metadata changed
            if (Session[Declarations.User] != null)
            {
                objEmail.userId = int.Parse(Session[Declarations.User].ToString());
                objEmail.BulkEmail();
            }
        }
        catch (Exception)
        {
            Page.TraceWarn("send emails fail.");
        }
    }
}