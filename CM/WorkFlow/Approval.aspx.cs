﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocumentOperationsBLL;
using CommonBLL;
using WorkflowBLL;
using WorkflowBLL;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using EmailBLL;

public partial class WorkFlow_Approval : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            setDefault();
            BindApprovalDetails();
        }
    }

    protected void btnSaveApproval_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("SaveApproval started");
        try
        {
            ContractProcedures CP = new ContractProcedures();
            CP.RequestId = Convert.ToInt32(ViewState["RequestId"]);
            CP.ContractId = Convert.ToInt32(ViewState["ContractId"]);
            CP.ContractApprovalId = Convert.ToInt32(ViewState["ContractApprovalId"]);
            CP.StageId = Convert.ToInt32(ViewState["StageId"]);
            CP.IsApproved = rdoApprove.Checked ? 'Y' : 'N';
            CP.Remark = txtRemark.Value;
            CP.ModifiedBy = Convert.ToInt32(Session[Declarations.User]);
            CP.IpAddress = Session[Declarations.IP].ToString();
            string result = CP.InsertRecordContractApproval();
            SendEmail();
            Page.Message("1", "0");
            Response.Redirect("RequestFlow.aspx?SendEmail=true&Status=Workflow&Section=Approvals&ScrollTo=PendingApprovals&RequestId=" + ViewState["RequestId"]);
            Page.TraceWrite("SaveApproval succeed");
        }
        catch (Exception ex)
        {
            Page.TraceWarn("SaveApproval failed." + ex.Message);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    protected void setDefault()
    {
        ViewState["RequestId"] = Request.QueryString["RequestId"];
        ViewState["ContractId"] = Request.QueryString["ContractId"];
        ViewState["StageId"] = Request.QueryString["StageId"];
        ViewState["ContractApprovalId"] = Request.QueryString["ContractApprovalId"];
        ViewState["StageName"] = Request.QueryString["StageName"];
        if (!string.IsNullOrEmpty(Request.QueryString["ContractId"]) && (Request.QueryString["ContractId"])!="0")
            ViewState["ContractIdMsg"] = "for contract " + ViewState["ContractId"];
    }

    protected void BindApprovalDetails()
    {
        Page.TraceWrite("BindApprovalDetails started");
        ContractProcedures CP = new ContractProcedures();
        List<ContractProcedures> listCP = new List<ContractProcedures>();
        //CP.RequestId = Convert.ToInt32(ViewState["RequestId"]);
        //CP.ContractId = Convert.ToInt32(ViewState["ContractId"]);
        CP.ContractApprovalId = Convert.ToInt32(ViewState["ContractApprovalId"]);
        CP.StageId = Convert.ToInt32(ViewState["StageId"]);
        try
        {
            listCP = CP.ApprovalDetails();
            if (listCP.Count > 0)
            {
                lblStageConditions.Text = listCP[0].Condition;
                txtRemark.Value = listCP[0].Remark;
                if (listCP[0].IsApproved == 'Y')
                {
                    rdoApprove.Checked = true;
                    rdoDisapprove.Checked = false;
                }
                else if (listCP[0].IsApproved == 'N')
                {
                    rdoApprove.Checked = false;
                    rdoDisapprove.Checked = true;
                }
            }
            Page.TraceWrite("BindApprovalDetails succeed");
        }
        catch (Exception ex)
        {
            Page.TraceWarn("BindApprovalDetails failed." + ex.Message);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("RequestFlow.aspx?Status=Workflow&ScrollTo=PendingApprovals&RequestId=" + ViewState["RequestId"]);
    }

    void SendEmail()
    {
        try
        {
            Page.TraceWrite("send emails.");
            EmailTemplateBLL.IEmailTrigger objEmail = EmailTemplateBLL.FactoryEmailTemplate.GetEmailTriggerDetail();
            objEmail.RequestId = Convert.ToInt32(ViewState["RequestId"]); ;
            objEmail.ContractTypeId = Convert.ToInt32(ViewState["ContractId"]);
            objEmail.ContractTemplateId = 0;
            objEmail.EmailTriggerId = 11; // hardcoded for Approval/Rejection
            objEmail.userId = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            objEmail.StageId = Convert.ToInt32(ViewState["StageId"]);
            objEmail.BulkEmail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("send emails fails.");
        }
    }
}