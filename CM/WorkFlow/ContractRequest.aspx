﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="ContractRequest.aspx.cs" Inherits="Workflow_ContractRequest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .tdAlign
        {
            text-align: right;
            width: 30%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
    <link href="../JQueryValidations/jquery-ui-1.8.19.custom.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    <script src="../JQueryValidations/autocomplete.js" type="text/javascript"></script>
    <link href="../JQueryValidations/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="../scripts/jquery.ui.core.min.js"></script>
    <script src="../scripts/jquery.ui.widget.min.js"></script>
    <script src="../scripts/jquery.ui.position.min.js"></script>
    <script src="../scripts/jquery.ui.autocomplete.min.js"></script>
    <link rel="stylesheet" href="../scripts/jquery-ui-1.8.16.custom.css" />
    <link rel="stylesheet" href="../modalfiles/modal.css" type="text/css" />
    <script type="text/javascript" src="../modalfiles/modal.js"></script>
    <script type="text/javascript" src="../scripts/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="../scripts/jquery.blockUI.js"></script>
    <script src="../JQueryValidations/jquery.caret.1.02.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../windowfiles/dhtmlwindow.css" type="text/css" />
    <link rel="stylesheet" href="../scripts/jquery-ui-timepicker-addon.css" type="text/css" />
    <script type="text/javascript" src="../windowfiles/dhtmlwindow.js"></script>
    <script type="text/javascript">
        $("#requestLink").addClass("menulink");
        $("#requesttab").addClass("selectedtab");
    </script>
    <script type="text/javascript">
      $('.tele').live('keypress', function (evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode == 8) {
                    return true;
                }
                else if (((charCode < 48) || (charCode > 57)) && !(charCode == 41 || charCode == 40 || charCode == 38 || charCode == 43)) {
                    return false;
                }
            });
        function ClearItem() {
            $('#ddlassignto').find('option').remove();
            $('#ddlassignto').trigger('liszt:updated');
        }

        function HideShowSC(obj,obj1) {
            $('#hdnSelectedDepartment').val($(obj1).val());
            $('#hdnSelectedUser').val($(obj).val());
            $('#ddlassignto option').each(function () {
                if ($(this).val() == $('#hdnUserID').val()) {
                    $(this).text('Me');
                    $('#ddlassignto').trigger('liszt:updated');
                    return;
                }
            });
            $('#hdnDeptId').val($('#ddlassigndept').val());
        }

        //******Functions Added By Nilesh*********//
        function LoadEditWindowNew(ClientId, PrimaryAddress) {
            BindAddressEdit(ClientId);
            BindContactsEdit(ClientId);
            BindEmailEdit(ClientId);
            SetPrimaryAddress(PrimaryAddress)
        }

        function getfield(ContractID)
        {            
             $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "ContractRequest.aspx/getfield",
                    data: "{ContractID:'" + ContractID + "'}",
                    dataType: "json",
                    async: false,
                    success: function (output) {
                        var MetaDataTable=$(output.d);
                        var trdynamic= $(MetaDataTable).find("tbody tr.trdynamic");
                        $("#MainContent_MetaDataTable tbody tr.trdynamic").remove();
                        $("#MainContent_MetaDataTable tbody:eq(0)").append(trdynamic);
                    }
                    });
        }

        function SelectPrimaryAddressForContract(ContractID)
        {
           $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ContractRequest.aspx/LoadContractIDAddress",
                data: "{ContractID:'" + ContractID + "'}",
                dataType: "json",
                async: false,
                success: function (output) {
                   $('ul#ulAddress').find('li').each(function () {
                    try {
                var AddressToComapre = output.d.split('--Contact 1--')[1].split('--Contact 2--')[0].split('~')
                if ($(this).attr('id') == AddressToComapre[1]) {
                    $(this).find('input').attr('checked', 'true');
                    $('#txtclientaddress').removeClass('required');
                    $('#ddlCountry').removeClass('required');
                    $('#hdnAdddressFromList').val(AddressToComapre[1]);
                    $('#hdnIsNewAddress').val('N');
                }
                }catch (e) { }
            });

              $('ul#ulContactNumber').find('li').each(function () {
              try{
                var AddressToComapre = output.d.split('--Contact 2--')[1].split('--Contact 3--')[0].split('~')
                if ($(this).attr('id') == AddressToComapre[1]) {
                    $(this).find('input').attr('checked', 'true');
                    $('#txtContactNumbers').removeClass('required');
                    $('#hdnContactNumberFromList').val(AddressToComapre[1]);
                    $('#hdnisNewContact').val('N');
                }
                }catch (e) { }
            });

            $('ul#ulEmail').find('li').each(function () {
                try{
                var AddressToComapre = output.d.split('--Contact 3--')[1].split('--Contact 4--')[0].split('~')
                if ($(this).attr('id') == AddressToComapre[1]) {
                    $(this).find('input').attr('checked', 'true');
                    $('#txtEmailIds').removeClass('required');
                    $('#hdnEmailIDFromList').val(AddressToComapre[1]);
                    $('#hdnisNewEmail').val('N');
                }
                }catch (e) { }
            });
                }
                });
        }

        function SetPrimaryAddress(PrimaryAddress) {
            $('ul#ulAddress').find('li').each(function () {
                var AddressToComapre = PrimaryAddress.split('--Contact 1--')[1].split('--Contact 2--')[0].split('~')
                if ($(this).attr('id') == AddressToComapre[1]) {
                    $(this).find('input').attr('checked', 'true');
                    //alert($('#txtclientaddress').attr('class'));
                    $('#txtclientaddress').removeClass('required');
                    $('#ddlCountry').removeClass('required');
                    $('#hdnAdddressFromList').val(AddressToComapre[1]);
                    $('#hdnIsNewAddress').val('N');
                    addressFieldDisable();
                }
            });
         
            $('ul#ulContactNumber').find('li').each(function () {
                var AddressToComapre = PrimaryAddress.split('--Contact 2--')[1].split('--Contact 3--')[0].split('~')
                if ($(this).attr('id') == AddressToComapre[1]) {
                    $(this).find('input').attr('checked', 'true');
                    $('#txtContactNumbers').removeClass('required');
                    $('#hdnContactNumberFromList').val(AddressToComapre[1]);
                    $('#hdnisNewContact').val('N');
                     $('#txtContactNumbers').attr('disabled', 'disabled');
                }
            });

            $('ul#ulEmail').find('li').each(function () {
                var AddressToComapre = PrimaryAddress.split('--Contact 3--')[1].split('--Contact 4--')[0].split('~')
                if ($(this).attr('id') == AddressToComapre[1]) {
                    $(this).find('input').attr('checked', 'true');
                    $('#txtEmailIds').removeClass('required');
                    $('#hdnEmailIDFromList').val(AddressToComapre[1]);
                    $('#hdnisNewEmail').val('N');
                    $('#txtEmailIds').attr('disabled', 'disabled');
                } 
            });
             $('#txtclientaddress').val('');
             $('#txtPincode').val('');
        }

         function addressFieldDisable(){
                $('#txtclientaddress').attr('disabled', 'disabled');
                $('#ddlCountry').removeClass('required');
                $('#ddlCountry').attr('disabled', 'disabled');
                $('#ddlCountry').val('0').trigger("liszt:updated");
                $('#ddlCountry').prev('span').css('display','None');
                $('#ddlState').attr('disabled', 'disabled');
                $('#ddlcity').attr('disabled', 'disabled');
                $('#txtPincode').attr('disabled', 'disabled');
                $('#txtclientaddressStreet2').attr('disabled', 'disabled');
        }

        function BindMataFields(ClientName)
        {
        var MataDataArray=new Array();
        $.ajax({
            type:"POST",
            contentType:"application/json; charset=utf-8",
            url:"ContractRequest.aspx/ReadMetaDataFieldsDetails",
            data:"{ClientName:'"+ClientName+"'}",
            dataType:"json",
            async:false,
            success:function (output)
            {
                if(output.d!="")
                {
                var res=$.parseJSON(output.d);
                    for(var i = 0;i< res.length;i++)                    {
                   
                    var controlID=res[i].FieldID;
                    var value=res[i].FieldValue;
                
                    if (controlID!='undefined') {                         
                           if(controlID.indexOf('ddl') > -1){
                             $('#'+controlID).val(value);
                           $('#'+controlID).trigger("liszt:updated");
                           }
                           else if(controlID.indexOf('rdo')> -1 || controlID.indexOf('chk') > -1)
                           {                                                 
                            var rad=document.getElementById(controlID);
                            var radio = rad.getElementsByTagName("input");
                                for (var j=0;j<radio.length;j++){
                                       if (radio[j].value==value)
                                       {
                                        radio[j].checked=true;
                                       }
                                    }
                           }
                           else
                             $('#'+controlID).val(value);
                     }
                    }
                }
            }
        });
        }

         function BindOtherFields(ClientName)
         {
          var AddressStringArray = new Array();
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ContractRequest.aspx/LoadOtherFields",
                data: "{ClientName:'" + ClientName + "'}",
                dataType: "json",
                async: false,
                success: function (output) {
                   
                if(output.d!="")
                {
                
                var res = $.parseJSON(output.d);
                for(var i = 0;i< res.length;i++)
                {
                    var ContractTypeId = res[i].ContractTypeID;
                    var ContractingTypeId = res[i].ContractingpartyId;
                    var ContractTerm =  res[i].ContractTerm;
                    var ContractValue =  res[i].ContractValue;
                    var isCustomer =  res[i].isCustomer;
                    var DeadlineDate =  res[i].DeadlineDate;
                    var CurrencyID =  res[i].CurrencyID;
                    var PriorityID=res[i].Priority;
                    var PriorityReason=res[i].PriorityReason;
                }
                if (PriorityID != '') 
                {
                    $('#ddlPrority').val(PriorityID);
                    $('#ddlPrority').trigger("liszt:updated");
                    if(PriorityID=="1")
                    {
                    $('#txtpriorityremark').val(PriorityReason);
                    $('#txtpriorityremark').show();
                    }
                }
                if(CurrencyID!='' && CurrencyID != undefined){
                   $('#ddlOriginalCurrency').val(CurrencyID);
                    $('#ddlOriginalCurrency').trigger("liszt:updated");
                 }
                if (ContractTypeId != '' && ContractTypeId != undefined) 
                {
                    $('#ddlContracttype').val(ContractTypeId);
                    $('#ddlContracttype').trigger("liszt:updated");
               }
                if (ContractingTypeId != '' && ContractingTypeId != undefined) 
                {
                    $('#ddlContractingParty').val(ContractingTypeId);
                    $('#ddlContractingParty').trigger("liszt:updated");
                }
                $('#txtContractTerm').val(ContractTerm);
                $('#txtContractValue').val(ContractValue);
               
               if(isCustomer=='Y')
               {
                $('#rdoCustomer').attr('checked','checked')
                SetCustomerLable('rdoCustomer');
               }
               else if(isCustomer=='O')
               {
                $('#rdoOthers').attr('checked','checked')
                SetCustomerLable('rdoOthers');
               }
               else
               {
                $('#rdoSupplier').attr('checked','checked');
                SetCustomerLable('rdoSupplier');
               }

               if(DeadlineDate != ' '){
               $('#txtdeadlinedate').val(DeadlineDate);
              }
             }
                }
                });
         }
         
         function BindIsCustomer(ClientId) { 
             $.ajax({
                 type: "POST",
                 contentType: "application/json; charset=utf-8",
                 url: "ContractRequest.aspx/LoadIsCustomer",
                 data: "{ClientId:'" + ClientId + "'}",
                 dataType: "json",
                 async: false,
                 success: function (output) {
                     var IsCustomer = output.d;
                    
                        if(IsCustomer=='Y')
                       {
                        $('#rdoCustomer').attr('checked','checked')
                        SetCustomerLable('rdoCustomer');
                       }
                       else if(IsCustomer=='O')
                       {
                        $('#rdoOthers').attr('checked','checked')
                        SetCustomerLable('rdoOthers');
                       }
                       else
                       {
                        $('#rdoSupplier').attr('checked','checked');
                        SetCustomerLable('rdoSupplier');
                       }
                 }
             });
        }

        function BindAddress(ClientId) {
            var AddressStringArray = new Array();
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ContractRequest.aspx/LoadAddress",
                data: "{ClientId:'" + ClientId + "'}",
                dataType: "json",
                async: false,
                success: function (output) {
                if(output.d!=''){
                    var AddressString = output.d.split('##AddressStart##')[1].split('##AddressEnd##')[0];
                    AddressStringArray = AddressString.split('####');
                    $('#hdnisNewCLient').val('N');
                    $('#txtclientaddress').prev('input').remove();
                    $('#txtContactNumbers').prev('input').remove();
                    $('#txtEmailIds').prev('input').remove();
                    $('input[id="rdoAdddress"]').remove();
                    $('input[id="rdoContact"]').remove();
                    $('input[id="rdoEmail"]').remove();

                    $('#txtclientaddress').prev('ul').remove();
                    $('#txtContactNumbers').prev('ul').remove();
                    $('#txtEmailIds').prev('ul').remove();
                    $('#txtclientaddress').before('<ul class="List" style="list-style-type: none;padding-left:0px;margin-top:0px" id="ulAddress"></ul>');
                    $('#txtContactNumbers').before('<ul class="List" style="list-style-type: none;padding-left:0px;margin-top:0px" id="ulContactNumber"></ul> ');
                    $('#txtEmailIds').before('<ul class="List" style="list-style-type: none;padding-left:0px;margin-top:0px" id="ulEmail"></ul> ');
                    
                    var AddCnt = AddressStringArray.length -  1;
                    for (var i = 0; i < AddressStringArray.length; i++) {
                       
//                       if(AddressStringArray[0].split('~')[1].lastIndexOf(",") > 0){
//                       var countryName = AddressStringArray[0].split('~')[1].substring(AddressStringArray[0].split('~')[1].lastIndexOf(",") + 1);
//                        }
//                        else{
//                        var countryName = AddressStringArray[0].split('~')[1].substring(AddressStringArray[0].split('~')[1].lastIndexOf(" ") + 1);
//                       }
                        if(AddressStringArray[AddCnt].split('~')[1].indexOf(",") > 0){
                            var countryId = AddressStringArray[AddCnt].split('~')[1].substring(0, AddressStringArray[AddCnt].split('~')[1].indexOf(","));
                        }

                       $('#hdnclientID').val(AddressStringArray[i].split('~')[0]);
                        if(AddressStringArray[i].split('~')[1]!='')
                        {
                        $('#txtclientaddress').prev('ul').append('<li id="' + AddressStringArray[i].split('~')[2] + '"><input type="radio" class="radio_label" runat="Server" name="Address" onchange="SetValuess(this)" ><span>' + AddressStringArray[i].split('~')[1].substring(AddressStringArray[i].split('~')[1].indexOf(",") + 1) + '</span></input></li><br>');
                        }
                    }

                    $('#txtclientaddress').before(' <input type="radio" runat="Server" id="rdoAdddress"   name="Address" style="margin-top:-3px" onchange="SetValuesRdo(this)"></input>');
                    $('#ddlCountry').next('div').css('margin-left', '0px');
                    $('#ddlState').next('div').css('margin-left', '0px');
                    $('#ddlcity').next('div').css('margin-left', '0px');
                     
                    //$("#ddlCountry option:selected").text(countryName);
                    $('#ddlCountry').val(countryId);
                    $('#ddlCountry').trigger("liszt:updated");
                    }
                } 
            });
        }

        function BindContacts(ClientId) {
            var EmailStringArray = new Array();
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ContractRequest.aspx/LoadContacts",
                data: "{ClientId:'" + ClientId + "'}",
                dataType: "json",
                async: false,
                success: function (output) {
                if(output.d!=''){
                    var ContactsString = output.d.split('##ContactsStart##')[1].split('##ContactsEnd##')[0];
                    ContactsString = ContactsString.split('####');
                    for (var i = 0; i < ContactsString.length; i++) {
                      if( ContactsString[i].split('~')[1]!='')
                        {
                        $('#txtContactNumbers').prev('ul').append('<li id="' + ContactsString[i].split('~')[2] + '"><input type="radio" class="radio_label" runat="Server" name="Contact" onchange="SetValuess(this)" ><span>' + ContactsString[i].split('~')[1] + '</span></input></li><br>');
                        }
                    }
                    $('#txtContactNumbers').before(' <input type="radio" runat="Server" name="Contact"  style="margin-top:-3px" id="rdoContact" onchange="SetValuesRdo(this)"></input>');
                    }
                } 
            });
        }

        function BindEmail(ClientId) {
            var EmailStringArray = new Array();
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ContractRequest.aspx/LoadEmail",
                data: "{ClientId:'" + ClientId + "'}",
                dataType: "json",
                async: false,
                success: function (output) {
                if(output.d!=''){
                    var EmailString = output.d.split('##EMailAddressStart##')[1].split('##EMailAddressEnd##')[0];
                    EmailString = EmailString.split('####');
                    for (var i = 0; i < EmailString.length; i++) {
                          if( EmailString[i].split('~')[1]!='')
                        {
                        $('#txtEmailIds').prev('ul').append('<li id="' + EmailString[i].split('~')[2] + '"><input type="radio" class="radio_label" runat="Server" name="email" onchange="SetValuess(this)" ><span>' + EmailString[i].split('~')[1] + '</span></input></li><br>');
                        }
                    }

                    $('#txtEmailIds').before(' <input type="radio" runat="Server"  name="email"   style="margin-top:-3px" id="rdoEmail" onchange="SetValuesRdo(this)"></input>');
                } 
                }
            });
        }

        function BindAddressEdit(ClientId) {
            var AddressStringArray = new Array();
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ContractRequest.aspx/LoadAddress",
                data: "{ClientId:'" + ClientId + "'}",
                dataType: "json",
                async: false,
                success: function (output) {
                if(output.d!=''){
                    var AddressString = output.d.split('##AddressStart##')[1].split('##AddressEnd##')[0];
                    AddressStringArray = AddressString.split('####');
                    $('#hdnisNewCLient').val('N');
                    $('#txtclientname').prev('br').prev('span').html('');
                    $('#txtclientname').prev('br').remove();
                    $('#txtclientaddress').prev('input').remove();
                    $('#txtContactNumbers').prev('input').remove();
                    $('#txtEmailIds').prev('input').remove();
                    $('input[id="rdoAdddress"]').remove();
                    $('input[id="rdoContact"]').remove();
                    $('input[id="rdoEmail"]').remove();
                    $('#txtclientaddress').prev('ul').remove();
                    $('#txtContactNumbers').prev('ul').remove();
                    $('#txtEmailIds').prev('ul').remove();
                    $('#txtclientaddress').before('<ul class="List" style="list-style-type: none;padding-left:0px;margin-top:0px" id="ulAddress"></ul>');
                    $('#txtContactNumbers').before('<ul class="List" style="list-style-type: none;padding-left:0px;margin-top:0px" id="ulContactNumber"></ul> ');
                    $('#txtEmailIds').before('<ul class="List" style="list-style-type: none;padding-left:0px;margin-top:0px" id="ulEmail"></ul> ');
                     
                    var AddCnt = AddressStringArray.length - 1;
                    for (var i = 0; i < AddressStringArray.length; i++) 
                    {
                    if(AddressStringArray[AddCnt].split('~')[1].indexOf(",") > 0){
                       var countryId = AddressStringArray[AddCnt].split('~')[1].substring(0, AddressStringArray[AddCnt].split('~')[1].indexOf(","));
                    }
                    
                    $('#hdnclientID').val(AddressStringArray[i].split('~')[0]);
                    if( AddressStringArray[i].split('~')[1]!='')
                    {
                        $('#txtclientaddress').prev('ul').append('<li id="' + AddressStringArray[i].split('~')[2] + '"><input type="radio" class="radio_label" runat="Server" name="Address" onchange="SetValuess(this)" ><span>' + AddressStringArray[i].split('~')[1].substring(AddressStringArray[i].split('~')[1].indexOf(",") + 1) + '</span></input></li><br>');
                    }
                    }
                    //  $('#txtclientaddress').before(' <input type="radio" runat="Server"   name="Address" style="margin-top:-3px" onchange="SetValuesRdo(this)"></input>');
                    $('#txtclientaddress').before(' <input type="radio" runat="Server" id="EA" name="Address" style="margin-top:-3px" onchange="SetValuesRdoEdit(this)"></input>');
                    $('#ddlCountry').next('div').css('margin-left', '0px');
                    $('#ddlState').next('div').css('margin-left', '0px');
                    $('#ddlcity').next('div').css('margin-left', '0px');
                    //$("#ddlCountry option:selected").text(countryName);
                    $('#ddlCountry').val(countryId);
                    $('#ddlCountry').trigger("liszt:updated");
                    }
                } 
            });
        }
         
        function BindContactsEdit(ClientId) {
            var EmailStringArray = new Array();
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ContractRequest.aspx/LoadContacts",
                data: "{ClientId:'" + ClientId + "'}",
                dataType: "json",
                async: false,
                success: function (output) {
                if(output.d!=''){
                    var ContactsString = output.d.split('##ContactsStart##')[1].split('##ContactsEnd##')[0];
                    ContactsString = ContactsString.split('####');

                    for (var i = 0; i < ContactsString.length; i++) {
                     if( ContactsString[i].split('~')[1]!='')
                    {
                        $('#txtContactNumbers').prev('ul').append('<li id="' + ContactsString[i].split('~')[2] + '"><input type="radio" class="radio_label" runat="Server" name="Contact" onchange="SetValuess(this)" ><span>' + ContactsString[i].split('~')[1] + '</span></input></li><br>');
                    }
                    }
                     for (var i = 0; i < ContactsString.length; i++) {
                      
                     if( ContactsString[i].split('~')[1]!='')
                    {
                    $('#txtContactNumbers').before(' <input type="radio" runat="Server" name="Contact"  style="margin-top:-3px" id="EC" onchange="SetValuesRdoEdit(this)"></input>');
                    if(i>=0)
                    { break; }
                    }
                    }
                    }
                } 
            });
        }

        function BindEmailEdit(ClientId) {
            var EmailStringArray = new Array();
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ContractRequest.aspx/LoadEmail",
                data: "{ClientId:'" + ClientId + "'}",
                dataType: "json",
                async: false,
                success: function (output) {
                if(output.d!=''){
                    var EmailString = output.d.split('##EMailAddressStart##')[1].split('##EMailAddressEnd##')[0];
                    EmailString = EmailString.split('####');
                    
                    for (var i = 0; i < EmailString.length; i++) {
                       if( EmailString[i].split('~')[1]!='')
                    {
                     if($('#hdnNotification').val()== "Y")
                        //  $('#hdnclientID').val(EmailString[i].split('~')[0]);
                        $('#txtEmailIds').prev('ul').append('<li id="' + EmailString[i].split('~')[2] + '"><input type="radio" disabled="disabled"  class="radio_label" runat="Server" name="email" onchange="SetValuess(this)" ><span>' + EmailString[i].split('~')[1] + '</span></input></li><br>');
                        else
                        $('#txtEmailIds').prev('ul').append('<li id="' + EmailString[i].split('~')[2] + '"><input type="radio" class="radio_label" runat="Server" name="email" onchange="SetValuess(this)" ><span>' + EmailString[i].split('~')[1] + '</span></input></li><br>');
                    }
                    }
                   
                   for (var i = 0; i < EmailString.length; i++) {
                    
                   if( EmailString[i].split('~')[1]!='')
                   {
                     if($('#hdnNotification').val()== "Y")
                     $('#txtEmailIds').before(' <input type="radio" runat="Server"  name="email" disabled="disabled"  style="margin-top:-3px" id="pd" onchange="SetValuesRdoEdit(this)"></input>');
                     else
                    $('#txtEmailIds').before(' <input type="radio" runat="Server"  name="email"   style="margin-top:-3px" id="EE" onchange="SetValuesRdoEdit(this)"></input>');
                     if(i>=0)
                    { break; }
                    }
                    }
                    }
                } 
            });
        }
        //*** Functions Ended********************//

        $(document).ready(function () {
            //$('#txtcontractdescription').autoGrow();
            //  $('.onPage').attr('id','uploader_browse');
             var src = <%= ClientData%> ;
           // var src = [{ "value": 0, "label": "--------Select--------" }, { "value": 1, "label": "Sai Infotech" }, { "value": 2, "label": "Ganesh Resource Agency" }, { "value": 3, "label": "AirIndia Resource Agency" }, { "value": 4, "label": "AirDeccan Resource Agency" }, { "value": 5, "label": "AirIndia Resource Agency" }, { "value": 6, "label": "Kingfisher Resource Agency" }, { "value": 7, "label": "After deleted fields from clause" }, { "value": 8, "label": "After added variable in clause" }, { "value": 9, "label": "Track Changes Limited" }, { "value": 10, "label": "Bharti Texttiles" }, { "value": 11, "label": "Vinayak Docusign Testing" }, { "value": 12, "label": "Passport Agency Limited" }, { "value": 13, "label": "Bharti Texttiles" }, { "value": 14, "label": "Nilesh Infotech" }, { "value": 15, "label": "Nilesh Infotech Ltd" }, { "value": 16, "label": "Sadanand Infotech" }, { "value": 17, "label": "Sancheti Hospital" }, { "value": 18, "label": "Bharti Texttiles" }, { "value": 19, "label": "Nilesh test" }, { "value": 20, "label": "Bharti Texttiles" }, { "value": 21, "label": "Tata Consultancy Services" }, { "value": 22, "label": "Vinayak Check Out" }, { "value": 23, "label": "Blue Dart India Ltd." }, { "value": 24, "label": "Mphasis" }, { "value": 25, "label": "Ramjane Band party" }, { "value": 26, "label": "Baba Electronics" }, { "value": 27, "label": "Best Template" }, { "value": 28, "label": "Sample Request" }, { "value": 32, "label": "Demo Service Limited" }, { "value": 33, "label": "Final Demo Service Limited" }, { "value": 36, "label": "a" }, { "value": 37, "label": "aaaa" }, { "value": 38, "label": "Testing new cases" }, { "value": 39, "label": "Signature Details" }, { "value": 40, "label": "Last Signature Details" }, { "value": 41, "label": "ssss1" }, { "value": 42, "label": "SSS Infocity" }, { "value": 43, "label": "Bharti Airtel (I)" }, { "value": 44, "label": "Bharti Airtel (I)" }, { "value": 45, "label": "test" }, { "value": 46, "label": "Bharti Airtel (I)" }, { "value": 47, "label": "Bharti Airtel (I)" }, { "value": 48, "label": "Survik" }, { "value": 49, "label": "Contract Party" }, { "value": 50, "label": "Sadanand Infocity" }, { "value": 51, "label": "Decay solutions1" }, { "value": 52, "label": "Suhas Infocity" }, { "value": 53, "label": "Vin Infocity" }, { "value": 1052, "label": "Sai Infotech1" }, { "value": 1053, "label": "Vin Infocity1" }, { "value": 1055, "label": "Vin Infocity11" }, { "value": 1056, "label": "TATA Motors" }, { "value": 1057, "label": "Reliance B" }, { "value": 1058, "label": "Reliance B" }, { "value": 1059, "label": "Reliance B" }, { "value": 1060, "label": "Reliance B" }, { "value": 1061, "label": "trial" }, { "value": 1062, "label": "abcd" }, { "value": 1063, "label": "pqr" }, { "value": 1064, "label": "bcbcvbcvb" }, { "value": 1065, "label": "Albert Rao" }, { "value": 1066, "label": "Surjit Singh" }, { "value": 1067, "label": "Nilesh A" }, { "value": 1068, "label": "NileshC" }, { "value": 1069, "label": "Nilesh M A" }, { "value": 1070, "label": "Nilesh Services" }, { "value": 1071, "label": "Nilesh Infocity" }, { "value": 1072, "label": "N M" }, { "value": 1073, "label": "N I" }, { "value": 1074, "label": "Bajaj" }, { "value": 1075, "label": "NileshC" }, { "value": 1076, "label": "Bajaj" }, { "value": 1077, "label": "Nil Info" }, { "value": 1078, "label": "Surendra Infosystem" }, { "value": 1079, "label": "Sheetal Agro Farms" }, { "value": 1080, "label": "test1" }, { "value": 1081, "label": "Force Motor Estate" }, { "value": 1082, "label": "Bajaj\u0027s" }, { "value": 1083, "label": "Bajaj" }, { "value": 1084, "label": "Airtel" }, { "value": 1085, "label": "testValvalueation1" }, { "value": 1086, "label": "TestAgain" }, { "value": 1087, "label": "Ok" }, { "value": 1088, "label": "V Info" }, { "value": 1089, "label": "test123" }, { "value": 1090, "label": "dtsdsdg" }, { "value": 2090, "label": "Bharti Airtel" }, { "value": 3090, "label": "Bharti Airtel" }, { "value": 3091, "label": "Bharti Airtel supplier" }, { "value": 4090, "label": "New Custromer" }, { "value": 4091, "label": "Santosh INC ltd" }, { "value": 4092, "label": "Assembler Inc" }, { "value": 4093, "label": "Bharti Texttiles (100303 )"}];
         
            $(".name").autocomplete({
                source: src,
                select: function (event, ui) {
          
                var ContractID="";
                    var CustomerName=ui.item.label.split('(');
                    var n = ui.item.label.lastIndexOf('(');
                    var result = ui.item.label.substring(n + 1).replace(')','');
                    var RequestType=$('#hdnrequesttype').val();
                    try
                    {
                    if(parseInt(result).toString().trim()!=NaN.toString().trim()  && RequestType!='cnc')
                    {
                        $('#txtContractID').val(result.trim());
                        ContractID=$('#txtContractID').val();
                        $('#txtclientname').val(ui.item.label.substr(0,ui.item.label.lastIndexOf('(')));
                    }
                    else
                    {  
                        $('#txtContractID').val('');
                        ContractID=$('#txtContractID').val();
                        $('#txtclientname').val(ui.item.label);
                    }
                    }
                    catch(err)
                    {
                        ContractID="";
                    }
                   // alert(ui.item.label.substr(0,ui.item.label.lastIndexOf('(')));
                   debugger;
                    BindIsCustomer(ui.item.value);
                    BindAddress(ui.item.value);
                    BindEmail(ui.item.value);
                    BindContacts(ui.item.value);
                    BindOtherFields(ui.item.label);
                    $("#Text1").val(ui.item.label);
                       BindMataFields(ui.item.label);
                    if(ContractID!="")
                    {
                    SelectPrimaryAddressForContract(ContractID);
                    }
                    return false;
                }
            });

            $('textarea').autogrow();
            var obj = $("#ParentDiv");
            obj.on('dragenter', function (e) {
                e.stopPropagation();
                e.preventDefault();
                // $(this).css('border', '2px solid #0B85A1');
            });
            obj.on('dragover', function (e) {
                e.stopPropagation();
                e.preventDefault();
            });
            obj.on('drop', function (e) {

                e.preventDefault();
                var files = e.originalEvent.target.files || (e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files);
                handleFileUpload(files, obj);
            });

            $('#test').click(function () {
            });

            $('#yes').click(function () {
                $.blockUI({ message: "<h1>Remote call in progress...</h1>" });
                $.ajax({
                    url: 'wait.php',
                    cache: false,
                    complete: function () {
                        $.unblockUI();
                    }
                });
            });

            $('#no').click(function () {
                $.unblockUI();
                return false;
            });
        });

        function CloseUploader() {
            $.unblockUI();
            $('a.onPage').css('margin-top', '32px');
            $('.OnPageul').css('margin-top', '-75px');
            return false;
        }
        $(window).load(function () {
            $('#ddlassignto option').each(function () {
                if ($(this).val() == $('#hdnUserID').val()) {
                    $(this).text('Me');
                    return;
                }
            });

            $('#ddlassignto').trigger("liszt:updated");
            $('#ddlCountry').next('div').css('margin-top', '7px');
            $('#ddlState').next('div').css('margin-top', '7px');
            $('#ddlcity').next('div').css('margin-top', '7px');
            $('#hdnSelectedDepartment').val($('#ddlassigndept').val());
            $('#hdnSelectedUser').val( $('#ddlassignto').val());
        });

        function HiseShowCustomerPara(obj) 
        {
            if ($(obj).val() > 0) {
                $('.CustomerOrSupplier').show();
            }
            else {
                $('.CustomerOrSupplier').hide();
            }
            $('.CSName').text('');

            if ($("#rdoCustomer").prop('checked') == true) {

                $('.CSName').html('<span class="required">*</span> Customer name')
            }
            else if ($("#rdoOthers").prop('checked') == true) {
                $('.CSName').html('<span class="required">*</span> Others name')
            }
            else {
                $('.CSName').html('<span class="required">*</span> Supplier name')
            }
        }
    </script>
    <script type="text/javascript">
        function popup() {
            var agreewin = dhtmlmodal.open("agreebox", "iframe", "CLientAdd.aspx", "Add new Customer/Supplier", "width=590px,height=450px,center=1,resize=1,scrolling=0", "recal")
            agreewin.onclose = function () { //Define custom code to run when window is closed
                var theform = this.contentDoc.getElementById("eula") //Access form with id="eula" inside iframe
                var yesbox = theform.eulabox[0] //Access the first radio button within form
                var nobox = theform.eulabox[1] //Access the second radio button within form
                if (yesbox.checked == true)
                    alert("You agreed to the terms")
                else if (nobox.checked == true)
                    alert("You didn't agree to the terms")
                return true //Allow closing of window in both cases
            }
        }
    </script>
    <input id="hdnNotification" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdncid" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnPrimeId" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnPrimeIds" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnRequestId" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnStateIds" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnCityIds" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnclientID" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnStateIdRead" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnCityIdRead" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdndate" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnrequesttype" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnDocumentIDs" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnUserID" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnAdddressFromList" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnContactNumberFromList" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnEmailIDFromList" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnSelectedClientID" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnIsNewAddress" runat="server" clientidmode="Static" type="hidden" value="Y" />
    <input id="hdnisNewContact" runat="server" clientidmode="Static" type="hidden" value="Y" />
    <input id="hdnisNewEmail" runat="server" clientidmode="Static" type="hidden" value="Y" />
    <input id="hdnisNewCLient" runat="server" clientidmode="Static" type="hidden" value="Y" />
    <input id="hdnClientNameHidden" runat="server" clientidmode="Static" type="hidden"
        value="Y" />
    <input id="hdnAssinedToUser" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnAddRequesterID" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnAddDeptID" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnAddAssignID" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnDeptId" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnSelectedUser" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnSelectedDepartment" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnCityName" runat="server" clientidmode="Static" type="hidden" value="" />
    <input id="hdnDocumentName" runat="server" clientidmode="Static" type="hidden" />
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text="">Add New Request</asp:Label>
    </h2>
    <div id="dvMesssage" class="flash error" style="display: none">
    </div>
    <select style="display: none" id="ddlDocumentType" runat="server" onchange="SetContractTypeValue(this)">
    </select>
    <table id="MetaDataTable" runat="server" width="100%">
        <tr id="ParaSearchBy" runat="server">
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Search by</label>
            </td>
            <td>
                <input class="name" id="Text1" runat="server" placeholder="Search Customer/Supplier/Others name"
                    maxlength="50" clientidmode="static" type="text" style="width: 30%;" />&nbsp;
                <span id="loading"></span><em></em>
            </td>
        </tr>
        <tr id="ContractIDPara" runat="Server" style="display: none">
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    <span class="required">*</span> Contract ID</label>
            </td>
            <td>
                <input id="txtContractID" runat="server" class="mobile" maxlength="10" clientidmode="static"
                    type="text" style="width: 30%;" />&nbsp; <span id="Span1"></span><em></em>
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    <span class="required">*</span> Contract Type</label>
            </td>
            <td>
                <asp:UpdatePanel ID="pnlcontract" runat="server" style="margin-left: 0px">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlContracttype" EventName="SelectedIndexChanged" />
                    </Triggers>
                    <ContentTemplate>
                        <asp:DropDownList ID="ddlContracttype" runat="server" ClientIDMode="Static" Style="width: 31%"
                            AutoPostBack="true" CssClass="chzn-select required chzn-select AF" OnSelectedIndexChanged="ddlContracttype_SelectedIndexChanged">
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <em></em>
                <%--<select id="ddlContracttype" clientidmode="Static" runat="server" style="width: 31%"
                    class="chzn-select required chzn-select">
                    <option></option>
                </select>
                <em></em>--%>
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Contracting party</label>
            </td>
            <td>
                <select id="ddlContractingParty" clientidmode="Static" onchange="HiseShowCustomerPara(this);"
                    runat="server" style="width: 31%;" class="chzn-select chzn-select AF ">
                    <option></option>
                </select>
                <em></em>
            </td>
        </tr>
        <tr clientidmode="Static" class="CustomerOrSupplier" runat="Server" id="CustomerOrSupplierId">
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    <span class="required">*</span>Customer or Supplier or Others</label>
            </td>
            <td>
                <asp:RadioButton ID="rdoCustomer" onclick="javascript:SetCustomerLable(this.id)"
                    Checked="true" GroupName="radiobutton1" ClientIDMode="Static" runat="Server" />Customer
                <asp:RadioButton ID="rdoSupplier" onclick="javascript:SetCustomerLable(this.id)"
                    GroupName="radiobutton1" ClientIDMode="Static" runat="Server" />Supplier
                <asp:RadioButton ID="rdoOthers" onclick="javascript:SetCustomerLable(this.id)" GroupName="radiobutton1"
                    ClientIDMode="Static" runat="Server" />Others <em></em>
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id" class="CSName">
                    <span class="required">*</span> Customer</label>
            </td>
            <td>
                <asp:TextBox ID="txtclientname" onkeyup="AddNewClient();" runat="server" MaxLength="50"
                    ClientIDMode="static" type="text" Style="width: 30%;" class="required WithoughtSemicolon">
                </asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="fteclientname" runat="server" InvalidChars="/\:*?<>&quot;|$^%_=,.~`{}"
                    FilterMode="InvalidChars" TargetControlID="txtclientname" />
                <em></em>
            </td>
        </tr>
        <tr>
            <td class="tdAlign" style="vertical-align: top">
                <label for="time_entry_issue_id">
                    Address</label>
            </td>
            <td>
                <asp:TextBox runat="server" placeholder="Street1" MaxLength="100" ClientIDMode="Static"
                    ID="txtclientaddress" class="myclass" Style="width: 30%;"></asp:TextBox>
                <br />
                <asp:TextBox runat="server" placeholder="Street2" MaxLength="100" ClientIDMode="Static"
                    ID="txtclientaddressStreet2" class="myclass" Style="width: 30%;"></asp:TextBox>
                <br />
                <input id="ddlcity" class="" runat="server" placeholder="City Name" clientidmode="static"
                    type="text" maxlength="500" autocomplete="false" style="width: 30%; margin-top: 5px" />
                <br />
                <input id="ddlState" class="" runat="server" placeholder="State Name" clientidmode="static"
                    type="text" maxlength="500" autocomplete="false" style="width: 30%; margin-top: 5px" />
                <br />
                <input id="txtPincode" runat="server" placeholder="Zip code/Post code" clientidmode="static"
                    type="text" maxlength="8" autocomplete="false" style="width: 30%; margin-top: 5px" />
                <br />
                <span class="required" style="margin-left: -10px">*</span>
                <select id="ddlCountry" clientidmode="Static" onchange="SetDDlValues();" runat="server"
                    style="width: 31%;" class="chzn-select required chzn-select AF ">
                    <option></option>
                </select>
                <em></em>
            </td>
        </tr>
        <tr>
            <td class="tdAlign" style="vertical-align: top">
                <label for="time_entry_issue_id">
                    Contact Number
                </label>
            </td>
            <td>
                <input id="txtContactNumbers" runat="server" maxlength="20" clientidmode="static"
                    class="PhoneNumberTxtNumSplChar PhoneNumberTxtNumSplCharPaste" type="text" style="width: 30%;" />
                <em></em>
            </td>
        </tr>
        <tr>
            <td class="tdAlign" style="vertical-align: top">
                <label for="time_entry_issue_id">
                    Email Id</label>
            </td>
            <td>
                <input id="txtEmailIds" runat="server" maxlength="50" clientidmode="static" type="text"
                    style="width: 30%;" class="email" />
                &nbsp; &nbsp; <span runat="server" id="trNotification">
                    <label for="time_entry_issue_id" clientidmode="Static" runat="server" id="lblNotifyCustomer"
                        visible="false">
                        Send Notification</label>
                    <select id="ddlNotifyCustomer" clientidmode="Static" runat="server" style="width: 8%;"
                        visible="false" class="chzn-select required chzn-select">
                        <option value="Y" selected="true">Yes</option>
                        <option value="N">No</option>
                    </select>
                </span>
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    <span class="required">*</span> Requester Name</label>
            </td>
            <td>
                <select id="ddlrequestername" clientidmode="Static" runat="server" style="width: 31%"
                    class="chzn-select required chzn-select">
                    <option></option>
                </select>
                <em></em>
            </td>
        </tr>
        <tr>
            <td class="tdAlign" style="vertical-align: top">
                <label for="time_entry_issue_id">
                    Request Description</label>
            </td>
            <td>
                <textarea id="txtcontractdescription" placeholder="Request description" runat="server"
                    clientidmode="static" type="text" autocomplete="false" style="width: 60%" rows="10"
                    class="myclass" />
                <em></em>
            </td>
        </tr>
        <tr style="display: none;">
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Deadline Date</label>
            </td>
            <td>
                <input id="txtdeadlinedate" runat="server" maxlength="100" clientidmode="static"
                    readonly="readonly" type="text" autocomplete="false" style="width: 20%;" />
                <em></em>
            </td>
        </tr>
        <tr style="display: none;">
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Term of contract</label>
            </td>
            <td>
                <input id="txtContractTerm" runat="server" maxlength="100" clientidmode="static"
                    type="text" autocomplete="false" style="width: 20%;" />
                <em></em>
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    <span class="required" id="SpanOriginalcurrency">*</span> Original Currency</label>
            </td>
            <td>
                <asp:DropDownList ID="ddlOriginalCurrency" ClientIDMode="Static" runat="server" Style="width: 31%;"
                    class="chzn-select chzn-select AF required" onchange="Checkthisvalue();">
                </asp:DropDownList>
                <em></em>
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    <span class="required" id="SpanTotalValue">*</span> Contract value</label>
            </td>
            <td>
                <input id="txtContractValue" class="Currency required" runat="server" maxlength="15"
                    clientidmode="static" type="text" autocomplete="false" style="width: 20%;" />
                <em></em>
            </td>
        </tr>
        <tr id="EV" style="display: none" runat="Server">
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Estimated value</label>
            </td>
            <td>
                <input id="txtEstimatedValue" runat="server" clientidmode="static" type="text" maxlength="8"
                    autocomplete="false" class="Pincode" style="width: 15%;" />
                <em></em>
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id" style="text-align: right">
                    Attach relevant Document</label>
            </td>
            <td>
                <div id="ParentDiv" style="margin-top: 25px; height: 60px; width: 60%; background-color: #F8F4CE;
                    color: #865476; border: #4D2B2F dashed 1px">
                    <table width="100%">
                        <tr>
                            <td valign="top" align="center">
                                <h3 class="plupload_droptext" style="font-size: 30px; color: #cccccc;">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Drag files here.</h3>
                                <ul id="uploader_filelist" class="plupload_filelist OnPageul" style="font-size: 30px;
                                    color: #cccccc; margin-top: -75px; position: relative; height: 100px;top: 0px;left: 0px;"">
                                    <li class="plupload_droptext" style="height: 55px; vertical-align: top"></li>
                                </ul>
                            </td>
                            <td align="right" valign="top" id="tdAdd">
                                <a href="#" class="plupload_button plupload_add onPage" id="uploader_browse" style="z-index: 1;
                                    margin-top: 32px">Add Files</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <div class="attachments">
                    <asp:Repeater ID="rptRequestDocuments" runat="server">
                        <ItemTemplate>
                            <p>
                                <a href='../Uploads/<%# Session["TenantDIR"] %>/ContractDocs/<%#Eval("ActuallFileName") %>'
                                    class="icon icon-attachment" target="_blank">
                                    <%#Eval("OrigionalFileName") %>
                                </a>-
                                <%#Eval("DocumentTypeName") %>
                                <span class="size"></span><a id='<%#Eval("ContractRequestDocumentId") %>' href=''
                                    class="delete" onclick="return DeleteDocument(this)" data-confirm="Are you sure?"
                                    data-method="delete" rel="nofollow" title="Delete">
                                    <img alt="Delete" src="../images/icon-del.jpg"></a> <span class="author">
                                        <%#Eval("FullName") %>,
                                        <%#Eval("AddedDate") %></span>
                            </p>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div id="uploader" style="display: none; width: 600px;">
                </div>
                <div id="FileName" style="display: none; margin-left: 180px;">
                </div>
            </td>
        </tr>
        <tr id="ApprovalP" clientidmode="Static" runat="Server" style="display: none">
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Approval required<span class="required"> </span>
                </label>
            </td>
            <td>
                <input type="radio" runat="Server" id="rdoApprovalRequiredYes" name="radiobutton"
                    checked="true" />
                Yes
                <input type="radio" runat="Server" id="rdoApprovalRequiredNo" name="radiobutton" />
                No
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="margin-left: 0px">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlassigndept" EventName="SelectedIndexChanged" />
                    </Triggers>
                    <ContentTemplate>
                        <table width="100%">
                            <tr>
                                <td class="tdAlign" style="width: 31%">
                                    <label for="time_entry_issue_id">
                                        <span class="required">*</span> Assign To Department</label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlassigndept" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlassigndept_SelectedIndexChanged"
                                        ClientIDMode="Static" onchange="HideShowSC($(ddlassignto),this);" Style="width: 31%"
                                        class="chzn-select required">
                                    </asp:DropDownList>
                                    <em></em>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdAlign">
                                    <label for="time_entry_issue_id">
                                        <span class="required">*</span> Assign To</label>
                                </td>
                                <td>
                                    <select id="ddlassignto" onchange="HideShowSC(this,$(ddlassigndept));" clientidmode="Static"
                                        runat="server" style="width: 31%" class="chzn-select required">
                                        <option></option>
                                    </select>
                                    <em></em>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdAlign">
                                    <label for="time_entry_issue_id">
                                        <span class="required">*</span> Priority</label>
                                </td>
                                <td>
                                    <select id="ddlPrority" clientidmode="Static" onchange="HideShowPReason();" runat="server"
                                        style="width: 31%" class="chzn-select required">
                                        <option></option>
                                    </select>
                                    <%--  <em></em>--%>
                                    <label id="lblPriorityreason" clientidmode="Static" runat="server" for="time_entry_issue_id">
                                        Priority Reason</label>
                                    <input id="txtpriorityremark" runat="server" clientidmode="Static" style="width: 30%;"
                                        maxlength="100" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <div class="box tabularr">
        <p>
            <asp:PlaceHolder ID="pnlnewadd" runat="server"></asp:PlaceHolder>
        </p>
    </div>
    <div id="msgmsg" style="display: none; width: 95%" clientidmode="Static" runat="server">
    </div>
    <div id="msgEdit" style="display: none; width: 95%" clientidmode="Static" runat="server">
    </div>
    <asp:Button runat="server" Text="Save" ID="btnsubmit" OnClientClick="return ValidateContractID()"
        OnClick="btnsubmit_Click" class="btn_validate" />
    <asp:Button runat="server" Text="Save & continue" ID="Button1" OnClientClick="return ValidateContractID()"
        OnClick="btnsubmit_Click" class="btn_validate" />
    <%-- <asp:Button ID="btnSaveAndContinue" runat="server" Text="Save and continue" 
        class="btn_validate" Visible="false" />--%>
    <label id="lblProcessingLink" visible="false" runat="Server">
        <b>Processing....</b></label>
    <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnBack_Click" />
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $('.Currency').on('keypress', function (evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode == 8) {
                return true;
            }
            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                if (charCode != 46) {
                    return false;
                }
            }
            var count = 0;
            var myNum = this.value + String.fromCharCode(charCode);
            //  var pattern = /^\d{0,8}$/;
            var pattern = /^\d{0,18}\.?\d{0,2}$/;
            // var pattern = /^\d{0,8}\.?\d{0,0}$/;
            if (!pattern.test(myNum)) {
                count = 1;
            }
            if (count == 1) {
                return false;
            }

            else if (myNum > 99999999) {
                return false;
            }
            else
                return true;
        });
        function SetCustomerLable(id) {
            if (id == "rdoCustomer") {

                $('.CSName').html('<span class="required">*</span> Customer name')
            }
            else if (id == "rdoOthers") {
                $('.CSName').html('<span class="required">*</span> Others name')
            }
            else {
                $('.CSName').html('<span class="required">*</span> Supplier name')
            }

        }
        function SetrdoLable(textid) {
            if (id == "rdoCustomer") {
                $('.CSName').html('<span class="required">*</span> Customer name')
            }
            else if (id == "rdoOthers") {
                $('.CSName').html('<span class="required">*</span> Others name')
            }
            else {
                $('.CSName').html('<span class="required">*</span> Supplier name')
            }

        }

        var ClientNameArrayEdit = new Array();
        var ClientNameArrayEdit1 = new Array();
        var ClientNameArrayEdit2 = new Array();
        var ClientInfoContactNumber = new Array();
        var ClientInfoAddress = new Array();
        var ClientInfoEmail = new Array();
        var isCLientAddInitiated = 'N';
        function AddNewClient() {
            if ($('#txtclientname').val() == "") {
                $('#msgmsg').html('');
                $('#msgEdit').html('');
                $('#txtclientname').addClass('required');
                $('#hdnisNewCLient').val('Y');
                $('#hdnIsNewAddress').val('Y');
                $('#hdnisNewContact').val('Y');
                $('#hdnisNewEmail').val('Y');
                $('#hdnclientID').val('0');
                //  $('#txtContractID').val('');
                $('#ddlCountry').next('div').css('margin-left', '0px');
                $('#ddlState').next('div').css('margin-left', '0px');
                $('#ddlcity').next('div').css('margin-left', '0px');
                LoadEditWindow('Add');
            }
        }

        var DocumentTypeId = "";
        var IsOCR = false;
        var upl;
        upl = $(function () {
            upl = $("#uploader").pluploadQueue({
                // General settings
                runtimes: 'html5, html4',
                url: '../ClauseLiabrary/DocumentHandler.ashx',
                max_file_size: '80mb',
                max_file_count: 20, // user can add no more then 20 files at a time
                chunk_size: '80mb',
                unique_names: true,
                multiple_queues: true,
                multipart: true,
                multipart_params: {
                },
                resize: { width: 1024, height: 768, quality: 90 },
                // Rename files by clicking on their titles
                rename: true,
                // Sort files
                sortable: true,
                // Specify what files to browse for
                filters: [
               	{ title: "Image files", extensions: "jpg,gif,png,bmp,tiff,jpeg,TIF,blob" },
                { title: "Document files", extensions: "xls,xlsx,doc,docx,pdf,swf,ppt,pptx,txt,csv,odt,odp,odg,ods,mp3,mp4,wmv,msg" },
                { title: "Zip files", extensions: "zip,avi" }
                //             {title: "Document files", extensions: "doc,docx" },
		],
                // PreInit events, bound before any internal events
                preinit: {
                    Init: function (up, info) {
                        // log('[Init]', 'Info:', info, 'Features:', up.features);
                        $('#uploader').next('p').remove();
                        $('#uploader').next('p').remove();
                        $('#uploader').next('p').remove();
                        $('#uploader').next('p').remove();
                    },
                    PostInit: function (up) {
                        //   $('select').chosen();
                    },
                    UploadFile: function (up, file) {
                        fname = file.name.replace(' & ', '').replace('&', '').replace('#', '');
                        var filesize = file.size;
                        //   var DocumentTypeId = "";
                        if ($('.plupload_filelist').find('li:eq(' + Counter + ') div.plupload_file_DocumentType:eq(1)').html() != 'Done') {

                            DocumentTypeId = $('.plupload_filelist').find('li:eq(' + Counter + ') div.plupload_file_DocumentType:eq(1)').find('input').val();
                            IsOCR = $('.plupload_filelist').find('li:eq(' + Counter + ') div.plupload_file_IsOCR:eq(0)').find('input[type=checkbox]').prop('checked');
                            Counter++;
                        }
                        else {
                        }
                        if (DocumentTypeId.indexOf("o_") == -1) {
                            var date = new Date();
                            var dateString = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear().toString().substr(2, 2); dateString = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear().toString().substr(2, 2);
                            up.settings.url = '../ClauseLiabrary/DocumentHandler.ashx?filesize=' + filesize + '&filename=' + fname + '&DocumentTypeId=' + DocumentTypeId + '&UserID=' + $('#hdnUserID').val() + '&RequestDate=' + dateString + '&ContractType=' + $('#ddlContracttype').val() + '&IsOCR=' + IsOCR;
                        }
                    }
                },
                // Post init events, bound after the internal events
                init: {
                    Refresh: function (up) {
                        // Called when upload shim is moved
                        //log('[Refresh]');
                    },

                    StateChanged: function (up) {
                    },
                    QueueChanged: function (up) {
                        // Called when the files in queue are changed by adding/removing files
                        //log('[QueueChanged]');
                    },
                    UploadProgress: function (up, file) {
                        // Called while a file is being uploaded
                        // log('[UploadProgress]', 'File:', file, "Total:", up.total);
                    },
                    FilesAdded: function (up, files) {
                        debugger;
                        if (up.files.length > files.length) {
                            up.splice();
                        }
                        $.blockUI({ message: $('#uploader'), css: { width: '600px'} });

                        $('.plupload_filelist').css('height', $('.plupload_filelist').height(up.files.length * 50));

                        plupload.each(files, function (file) {
                            //  log('  File:', file);
                        });
                    },

                    FilesRemoved: function (up, files) {
                        $('.plupload_filelist').css('height', $('.plupload_filelist').height(up.files.length * 50));

                        plupload.each(files, function (file) {
                            //  log('  File:', file);
                        });
                    },
                    FileUploaded: function (up, file, info) {
                        // Called when a file has finished uploading
                        $('#FileName').show();
                        $('#FileName').find('.icon').attr('target', '_blank');
                        var FileArray = new Array();
                        FileArray = info.response.split('#');
                        // try{
                        var arr = "";
                        $('#hdnDocumentIDs').val($('#hdnDocumentIDs').val() + ',' + FileArray[1]);
                        $('#hdnDocumentName').val($('#hdnDocumentName').val() + ',' + FileArray[2]);

                        if (FileArray[0] != "Chunk") {
                            arr = FileArray[0].split(/=/);
                        }
                        else if (chunkreponse != "") {
                            arr = chunkreponse.split(/=/);
                        }
                        if (arr != "") {
                            chunkreponse = "";
                            // log('[FileUploaded] File:', file, "Info:", info);
                        }
                    },
                    ChunkUploaded: function (up, file, info) {
                        // Called when a file chunk has finished uploading
                        if (info.response != "Chunk") {
                            chunkreponse = info.response;
                            // log('[FileUploaded] File:', file, "Info:", info);
                        }
                    },
                    Error: function (up, args) {
                        // Called when a error has occured
                        // log('[error] ', args);
                    }
                },
                // Resize images on clientside if we can
                resize: { width: 320, height: 240, quality: 90 },

                flash_swf_url: '../../js/Moxie.swf',
                silverlight_xap_url: '../../js/Moxie.xap'
            });
        });

        function SetAssingdToValue(AssingedToValue) {
            if (AssingedToValue != '' || AssingedToValue != null || AssingedToValue != 'undefined' || AssingedToValue != undefined) {
                addItemToSelectIfNotFound('ddlassignto', AssingedToValue, $('#hdnStateNameReadDisable').val());
            }
            $('#ddlassignto').val(AssingedToValue);
        }

        function handleFileUpload(files, obj) {
            debugger;
            for (var i = 0; i < files.length; i++) {
                var fd = new FormData();
                fd.append('file', files[i]);
                $.blockUI({ message: $('#uploader'), css: { width: '600px'} });
            }
        }

        var Counter = 0;
        function CallMe(obj) {debugger;
            $(obj).closest('li').find('div[class="plupload_file_DocumentType"]').find('input').val($(obj).val());
            if ($(obj).val() != '0') {
                $(obj).closest('li').find('div:eq(4)').find('select').next('div[id="dvFont"]').remove();
            }
            else {
                $(obj).closest('li').find('div:eq(4)').find('select').after('<div id="dvFont" style="margin-left:205px;text-align:left"><font color="red">Please select document Type</font></div>');
            }
        }

        function LoadEditWindow(str) {debugger;
            ShowLoadEditControls();
            if (str == "Add") {
                $(document).find('input:radio').each(function () {
                    if ($(this).attr('name') != 'ctl00$MainContent$radiobutton' && $(this).attr('name') != 'ctl00$MainContent$radiobutton1') {
                        $(this).remove();
                    }
                });

                $(document).find('ul[class="List"]').remove();
            }
        }

        function ShowLoadEditControls() {
        }

        function GetParameters(ClientID, Client, Address, CountryID, StateID, CityID, Pincode) {
            $('#<%=txtclientaddress.ClientID %>').val(Address.replace('<br>', '\n').replace('<br>', '\n').replace('<br>', '\n').replace('<br>', '\n').replace('<br>', '\r\n').replace('<br>', '\r\n').replace('<br>', '\r\n').replace('<br>', '\r\n').replace('<br>', '\r\n'));
            $("#hdnPrimeIds").val(CountryID);
            $("#hdnStateIds").val(StateID);
            $("#hdnCityIds").val(CityID);
            $('#txtPincode').val(Pincode);
            $('#ddlCountry').val(CountryID);
            $('#ddlCountry').trigger("liszt:updated");
            JQSelectBind('ddlCountry', 'ddlState', 'MstState');
            $('#ddlState').val(StateID);
            $('#ddlState').trigger("liszt:updated");
            JQSelectBind('ddlState', 'ddlcity', 'MstCity');
            $('#ddlcity').val(CityID);
            $('#ddlcity').trigger("liszt:updated");
            $('#hdnclientID').val(ClientID);
            $('#txtclientaddress').css('height', $('#txtclientaddress').prop('scrollHeight') + 'px');
        }

        function SetDDlValues() {
            $("#hdnPrimeIds").val($('#ddlCountry').val());
        }

        function HiseShowCustomerPara(obj) {
            $('.CSName').text('');

            if ($("#rdoCustomer").prop('checked') == true) {

                $('.CSName').html('<span class="required">*</span> Customer name')
            }
            else if ($("#rdoOthers").prop('checked') == true) {
                $('.CSName').html('<span class="required">*</span> Others name')
            }
            else {
                $('.CSName').html('<span class="required">*</span> Supplier name')
            }
        }

        $(document).ready(function () {
            if (txtContractValue == "" && ddlOriginalCurrency == "0") { }
            var txtContractValue = $('#txtContractValue').val();
            var ddlOriginalCurrency = $('#ddlOriginalCurrency').val();
            if (txtContractValue != "" && ddlOriginalCurrency != "undefined") {
                $('#SpanOriginalcurrency').show();
            }
            else {
                $('#SpanOriginalcurrency').hide();
            }
            if ((ddlOriginalCurrency != "undefined" && txtContractValue != "")) {
                $('#SpanTotalValue').show();
            }
            else {
                $('#SpanTotalValue').hide();
            }

            $('#ApprovalP').css('display', 'none');

            $('.WithoughtSemicolon').on('keypress', function (evt) {

                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode == 59) {
                    return false;
                }
            });

            if ($("#rdoCustomer").prop('checked') == true) {
                $('.CSName').html('<span class="required">*</span> Customer name')
            }
            else if ($("#rdoOthers").prop('checked') == true) {
                $('.CSName').html('<span class="required">*</span> Others name')
            }
            else {
                $('.CSName').html('<span class="required">*</span> Supplier name')
            }

            try {
                $("#txtdeadlinedate").datepicker({
                    defaultDate: "+1d",
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1,
                    dateFormat: 'dd-MM-yy'
                }).attr('placeholder', 'dd-MMM-yyyy').blur(function () {
                    //alert("2"); return RemoveErrorClass($(this));
                });
            }
            catch (e) {
                alert(e);
            }

            $(".txtDate").datepicker({ showOn: "both",
                buttonImage: "../Styles/css/icons/16/calendar_1.png",
                buttonImageOnly: true,
                prevText: 'Previous',
                //yearRange: 'c-50:c+50',
                //                minDate: 0,
                //                maxDate: "+10Y +10M +10D",
                //yearRange: "-100:+10",
                changeMonth: true,
                dateFormat: 'dd-M-yy',
                buttonText: '',
                onClose: function (dateText, inst) {
                },
                changeYear: true
            }).attr('placeholder', 'dd-MMM-yyyy').blur(function () {
                //alert("2"); return RemoveErrorClass($(this));
            });

        });
        function setvalues() {
            $('#ddlState').trigger('change');
            $('#ddlState option:contains("Select")').text('--Select State--');
            $('#ddlState').trigger("liszt:updated");
        }

        function setvaluesState() {
            $('#ddlcity').trigger('change');
            $('#ddlcity option:contains("Select")').text('--Select City--');
            $('#ddlcity').trigger("liszt:updated");
        }
        $('.chzn-select').change(function () {
            if ($(this).val() != '0') {
                $(this).next('div').next('.tooltip_outer').hide();
                $(this).next('div').css('border-color', '');
            }
            else {
                $(this).next('div').next('.tooltip_outer').show();
                $(this).next('div').css('border-color', '');
            }
        });

        $('#ddlCountry').change(function () {
        });

        function SetValuesRdoEdit(obj) {
            // alert($(obj).attr('id').indexOf('EA'));
            if ($(obj).attr('id').indexOf('EA') > -1) {
                //alert($(obj).next('span').html());
                $('#hdnAdddressFromList').val('');
                $('#hdnIsNewAddress').val('Y');
                $('#txtclientaddress').removeAttr('disabled');
                $('#ddlCountry').removeAttr('disabled');
                $('#ddlState').removeAttr('disabled');
                $('#ddlcity').removeAttr('disabled');
                $('#txtPincode').removeAttr('disabled');
                $('#ddlCountry').removeAttr('disabled');
                $('#ddlCountry').addClass('required').trigger("liszt:updated");
                $('#ddlCountry').prev('span').css('display', 'initial');
                $('#txtclientaddressStreet2').removeAttr('disabled');
            }

            if ($(obj).attr('id').indexOf('EC') > -1) {
                // alert($(obj).next('span').html());
                $('#hdnContactNumberFromList').val('');
                $('#hdnisNewContact').val('Y');
                $('#txtContactNumbers').addClass('required')
                $('#txtContactNumbers').removeAttr('disabled', 'disabled');
            }

            if ($(obj).attr('id').indexOf('EE') > -1) {
                //alert($(obj).next('span').html());
                $('#hdnEmailIDFromList').val('');
                $('#hdnisNewEmail').val('Y');
                $('#txtEmailIds').addClass('required')
                $('#txtEmailIds').removeAttr('disabled', 'disabled');
            }
        }
        function SetWaterMark() {
            $('#Text1').attr('placeholder', 'Contract ID or Customer/Supplier /Others Name');
        }

        function SetValuesRdo(obj) {
            if ($(obj).attr('id').indexOf('rdoAdddress') > -1) {
                //alert($(obj).next('span').html());
                $('#hdnAdddressFromList').val('');
                $('#hdnIsNewAddress').val('Y');
                $('#txtclientaddress').removeAttr('disabled');
                $('#ddlCountry').removeAttr('disabled');
                $('#ddlState').removeAttr('disabled');
                $('#ddlcity').removeAttr('disabled');
                $('#txtPincode').removeAttr('disabled');
                $('#ddlCountry').addClass('required');
            }

            if ($(obj).attr('id').indexOf('rdoContact') > -1) {
                // alert($(obj).next('span').html());
                $('#hdnContactNumberFromList').val('');
                $('#hdnisNewContact').val('Y');
                $('#txtContactNumbers').addClass('required')
                $('#txtContactNumbers').removeAttr('disabled', 'disabled');
            }

            if ($(obj).attr('id').indexOf('rdoEmail') > -1) {
                //alert($(obj).next('span').html());
                $('#hdnEmailIDFromList').val('');
                $('#hdnisNewEmail').val('Y');
                $('#txtEmailIds').addClass('required')
                $('#txtEmailIds').removeAttr('disabled', 'disabled');
            }
        }

        function DeleteDocument(obj) {
            if (confirm('Do you want to delete the file ?')) {
                //var DocPath = $(obj).prev().attr('href');
                var DocPath = $(obj).parent().children('a:eq(0)').attr('href');
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "ContractRequest.aspx/DeleteFile",
                    data: "{DocumentID:'" + $(obj).attr('id') + "',DocumentPath:'" + DocPath + "'}",
                    dataType: "json",
                    success: function (output) {
                        $(obj).parent('p').remove();
                    }
                });
            }
            return false;
        }

        function JavascriptFunctionName() {
            $(".chzn-select").chosen();
            HideShowPReason();
        }

        function Checkthisvalue() {
            var txtContractValue = $('#txtContractValue').val();
            var ddlOriginalCurrency = $('#ddlOriginalCurrency').val();
            if (ddlOriginalCurrency == "0") {
                $('.tool_tip').hide();
                $('#SpanTotalValue').hide();
                $('#txtContractValue').css('border-color', '');
                $('#txtContractValue').addClass('required', '');
            }
            if (txtContractValue != "" || txtContractValue != "0" || txtContractValue != "0.00") {
                $('.tool_tip').hide();
                $('#SpanOriginalcurrency').hide();
                $('#ddlOriginalCurrency').css('border-color', '');
                $('#txtContractValue').css('border-color', '');
                $('#ddlOriginalCurrency').addClass('required', '');
            }
        }

        function ValidateContractID() {
            var txtContractValue = $('#txtContractValue').val();
            var ddlOriginalCurrency = $('#ddlOriginalCurrency').val();
            if ((txtContractValue == "0" || txtContractValue == "" || txtContractValue == "0.00") && (ddlOriginalCurrency == "0")) {
                $('#txtContractValue').removeClass('required', 'required');
                $('#ddlOriginalCurrency').removeClass('required', 'required');
                return true;
            }
            else {
                if (txtContractValue == "0" || txtContractValue == "" || txtContractValue == "0.00") {
                    $('#txtContractValue').val('');
                    $('#SpanTotalValue').show();
                    $(".tool_tip").show();
                    $('#txtContractValue').css('border-color', 'red');
                    $('#txtContractValue').addClass('required', 'required');
                    return false;
                }
                if (ddlOriginalCurrency == "0") {
                    $('#ddlOriginalCurrency').val('');
                    $('#SpanOriginalcurrency').show();
                    $(".tool_tip").show();
                    $('#ddlOriginalCurrency').css('border-color', 'red');
                    $('#ddlOriginalCurrency').addClass('required', 'required');
                    return false;
                }
            }

            SetDDlValues();
            var lContractID = $('#txtContractID').val();
            var isValid = 0;

            $.ajax({
                type: "POST",
                async: false,
                contentType: "application/json; charset=utf-8",
                url: "ContractRequest.aspx/ValidateContractID",
                data: "{ContractID:'" + lContractID + "'}",
                dataType: "json",
                success: function (output) {
                    isValid = output.d;
                }
            });

            var lRequestType = $('#hdnrequesttype').val();
            if (lRequestType != "cnc" && lRequestType != "nrr") {
                if (isValid.split('#')[0] == "0") {
                    $("#dvMesssage").css("display", "block");
                    if (isValid.split('#')[1] != undefined) {
                        $("#dvMesssage").html("Please enter valid Contract ID . Status of this contract is " + isValid.split('#')[1]);
                        scrollToAnchor('dvMesssage');
                    }
                    else {
                        $("#dvMesssage").html("Please enter valid ContractID");
                        scrollToAnchor('dvMesssage');
                    }

                    setTimeout(function () {
                        $('#dvMesssage').hide();
                    }, 10000);

                    return false;
                }
                else {

                    return true;
                }
            }
            else
                return true;
        }
        //***************************************
        function scrollToAnchor(aid) {
            var aTag = $("div[id='" + aid + "']");
            $('html,body').animate({ scrollTop: aTag.offset().top }, 'slow');
        }

        function SetValuess(obj) {
            if ($(obj).attr('name').indexOf('$Address') > -1) {
                //alert($(obj).parent().attr('id'));
                $('#hdnAdddressFromList').val($(obj).parent().attr('id'));
                $('#hdnIsNewAddress').val('N');
                $('#txtclientaddress').attr('disabled', 'disabled');
                $('#ddlCountry').removeClass('required');
                $('#ddlCountry').attr('disabled', 'disabled');
                $('#ddlCountry').val('0').trigger("liszt:updated");
                $('#ddlCountry').prev('span').css('display', 'None');
                $('#ddlState').attr('disabled', 'disabled');
                $('#ddlcity').attr('disabled', 'disabled');
                $('#txtPincode').attr('disabled', 'disabled');
                $('#txtclientaddressStreet2').attr('disabled', 'disabled');
                $('#txtclientaddressStreet2').val('');
                $('#ddlState').val('');
                $('#ddlcity').val('');
                $('#txtPincode').val('');
                $('#txtclientaddress').val('');
            }

            if ($(obj).attr('name').indexOf('$Contact') > -1) {
                // alert($(obj).next('span').html());
                $('#hdnContactNumberFromList').val($(obj).parent().attr('id'));
                $('#hdnisNewContact').val('N');
                $('#txtContactNumbers').removeClass('required')
                $('#txtContactNumbers').attr('disabled', 'disabled');
                $('#txtContactNumbers').val('');
            }

            if ($(obj).attr('name').indexOf('$email') > -1) {
                //alert($(obj).next('span').html());
                $('#hdnEmailIDFromList').val($(obj).parent().attr('id'));
                $('#hdnisNewEmail').val('N');
                $('#txtEmailIds').removeClass('required')
                $('#txtEmailIds').attr('disabled', 'disabled');
                $('#txtEmailIds').val('');
            }
        }
        function ClearData() {
            $('#txtContactNumbers').val('');
            $('#txtEmailIds').val('');
            $('#ddlrequestername').val('0');
            $('#txtcontractdescription').val('');
            $('#ddlassigndept').val('0');
            $('#ddlassignto').val('0');
            $('#Text1').val('');
            $("#txtdeadlinedate").val('');
            $('#txtEstimatedValue').val('');
        }

        function ClearAddress() {
            $('#txtclientaddress').val('')
            $('#ddlCountry').val('0');
            $('#ddlState').val('');
            $('#ddlcity').val('');
            $('#txtPincode').val('');
            $('#txtclientaddress').removeAttr('disabled');
            $('#ddlCountry').removeAttr('disabled');
            $('#ddlState').removeAttr('disabled');
            $('#ddlcity').removeAttr('disabled');
            $('#txtPincode').removeAttr('disabled');
            $('#ddlCountry').addClass('required');
            $('#txtContactNumbers').addClass('required');
            $('#txtEmailIds').addClass('required');
        }

        $('#msgmsg').html('');

        $(document).ready(function () {
            $.noConflict();
            $('#ddlrequestername').removeClass('required');
            $('#txtcontractdescription').css('height', $('#txtcontractdescription').prop('scrollHeight') + 'px');
            $('#txtclientaddress').css('height', $('#txtclientaddress').prop('scrollHeight') + 'px');
            $(".hello").remove();
            $('#ui-datepicker-div').hide();
            $('#sidebar').find("a[id=" + $('#hdnrequesttype').val() + "]").css('font-weight', 'bold');
        });
    </script>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
        function HideShowPReason() {
            $("#txtpriorityremark").hide();
            $("#lblPriorityreason").hide();
            var ID = $("#ddlPrority").val();
            if (ID == "1") {
                $("#lblPriorityreason").show();
                $("#txtpriorityremark").show();
            }
        }
    </script>
    <%--    Document Upload Starts Here Files--%>
    <link rel="stylesheet" href="../UploadJs/js/jquery.plupload.queue/css/jquery.plupload.queue.css"
        type="text/css" media="screen" />
    <script src="RequestJS/jquery.min.js" type="text/javascript"></script>
    <script src="RequestJS/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../UploadJs/js/plupload.full.min.js"></script>
    <script type="text/javascript" src="../UploadJs/js/jquery.plupload.queue/jquery.plupload.queue.js"></script>
</asp:Content>
