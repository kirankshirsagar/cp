﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using WorkflowBLL;
using CommonBLL;
using System.Data;
using MasterBLL;


public partial class WorkFlow_AvailableMultiClientsMaster : System.Web.UI.Page
{
    IAvailableClient obj;
    ICountry objCountry;
    IState objState;
    ICity objCity;

    public string Add;
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();


        if (!IsPostBack)
        {
           
            if (String.IsNullOrEmpty(Request.extValue("hdnPrimeIds")) != true)
            {
                hdnPrimeId.Value = Request.extValue("hdnPrimeIds");
            }
            errorExplanation.Attributes.Remove("class");
            errorExplanation.Style.Add("display", "none");
            CountryBind();
            AddAdressDetail();
            AddEmailDetail();
            AddMobileDetail();
            if (String.IsNullOrEmpty(Request.extValue("hdnPrimeIds")) != true)
            {
                hdnPrimeId.Value = Request.extValue("hdnPrimeIds");
                hdnIsUsedStatus.Value = Request.QueryString["Isused"].ToString();
                               
                EditClientDetails();
                lblTitle.Text = "Edit Customer/Supplier/Others Details";
                btnSave.Text = "Update";
                btnSaveAndContinue.Visible = false;
                if (hdnIsUsedStatus.Value == "Y")
                {
                    rdoSupplier.Enabled = false;
                    rdoCustomer.Enabled = false;
                    rdoOthers.Enabled = false;
                }
            }
            else
            {
                hdnPrimeId.Value = "0";
                lblTitle.Text = "Add Customer/Supplier/Others Details";
                btnSaveAndContinue.Visible = true;
                rdoCustomer.Checked = true;
            }

            if (Session[Declarations.User] != null)
            {
                Access.PageAccess(this, Session[Declarations.User].ToString(), "WorkFlow_");
                ViewState[Declarations.Add] = Access.Add.Trim();
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
            }
            AccessVisibility();

        }

    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            obj = FactoryWorkflow.GetAvailableClientDetails();
            objCountry = FactoryMaster.GetCountryDetail();
            objState = FactoryMaster.GetStateDetail();
            objCity = FactoryMaster.GetCityDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    void CountryBind()
    {
        Page.TraceWrite("Country dropdown bind starts.");
        try
        {
            ddlCountry.extDataBind(objCountry.SelectData());
    
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Country dropdown bind ends.");
    }

    public string CheckValidation()
    {
        string status = string.Empty;
        try
        {

            DataTable dtAddress = (DataTable)ViewState["Address"];
            DataTable dtEmail = (DataTable)ViewState["Email"];
            DataTable dtMobile = (DataTable)ViewState["Mobile"];
            if (dtAddress.Rows.Count <= 0)
            {
                lblError.Text = "Please add atleast one address details";
                lblError.ForeColor = System.Drawing.Color.Red;
                status = "1";
            }
           
            else if (dtMobile.Rows.Count <= 0)
            {
                lblMobileError.Text = "Please add atleast one Contact no";
                lblMobileError.ForeColor = System.Drawing.Color.Red;
                status = "1";
            }
            else if (dtEmail.Rows.Count <= 0)
            {
                lblemailError.Text = "<br/>Please add atleast one Email ID";
                lblemailError.ForeColor = System.Drawing.Color.Red;
                status = "1";
            }
            else { status = "0"; }
        }
        catch { }
        return status;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
           
            //lblTitle.Text = "";
            string Status = CheckValidation();
            if (Status == "0")
            {
               
                if (hdnPrimeId.Value == "0")
                {
                    SaveData("Save");
                }
           
                else
                {
                    Page.TraceWrite("Save Update button clicked event ends.");
                    UpdateData();
                    
                }
            }

        }
        catch { }
    }

    public void SaveData(string status)
    {
        try
        {
            Page.TraceWrite("Save Update button clicked event starts.");

            DataTable dtAddress = (DataTable)ViewState["Address"];
            DataTable dtEmail = (DataTable)ViewState["Email"];
            DataTable dtMobile = (DataTable)ViewState["Mobile"];

            string Address = string.Empty,Street2 = string.Empty, PinCode = string.Empty, Mobileno = string.Empty, EmailID = string.Empty,StateName=string.Empty,CityName=string.Empty;
            int CountryID = 0, CityID = 0, StateID = 0;
            //For Inserty Client Name
            obj.ClientName = txtClientName.Text.Trim();
            if (rdoCustomer.Checked == true)
            {
                obj.isCustomer = "Y";
            }
            if (rdoSupplier.Checked == true)
            { 
                obj.isCustomer = "N"; 
            }
            if (rdoOthers.Checked == true)
            {
                obj.isCustomer = "O";
            }
            obj.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            obj.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            obj.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
            obj.Param = 1;
            string ClientID = obj.InsertRecord();
            obj.ClientID = Convert.ToInt32(ClientID);

            if (ClientID == "0")//For duplicate check
            {
               // lblTitle.Text = "Client is already exist.";
               errorExplanation.Attributes.Add("class", "flash error");
               errorExplanation.Style.Add("display", "block");
               errorExplanation.InnerHtml = "Client is already exist.";
            }
            else
            {
                errorExplanation.Attributes.Remove("class");
                errorExplanation.Style.Add("display", "none");
                //-----For Address Details
               // lblTitle.Text = "";
                if (dtAddress.Rows.Count > 0)
                {
                    for (int i = 0; i < dtAddress.Rows.Count; i++)
                    {
                        Address = dtAddress.Rows[i]["Address"].ToString();
                        Street2 = dtAddress.Rows[i]["Street"].ToString();
                        CountryID = Convert.ToInt32(dtAddress.Rows[i]["CountryID"]);
                        StateName = Convert.ToString(dtAddress.Rows[i]["StateName"]);
                        CityName = Convert.ToString(dtAddress.Rows[i]["CityName"]);
                        PinCode = dtAddress.Rows[i]["PinCode"].ToString();

                        obj.Address = Address.Trim();
                        obj.Street = Street2.Trim();
                        obj.Pincode = PinCode.Trim();
                        obj.CountryID = CountryID;
                        obj.StateName = StateName;
                        obj.CityName = CityName;
                        obj.Param = 2;
                        obj.InsertRecord();

                    }
                }


                //For Mobile Details

                if (dtMobile.Rows.Count > 0)
                {
                    for (int i = 0; i < dtMobile.Rows.Count; i++)
                    {
                        Mobileno = dtMobile.Rows[i]["ContactNo"].ToString();
                        obj.ContactNo = dtMobile.Rows[i]["ContactNo"].ToString();
                        obj.Param = 3;
                        obj.InsertRecord();
                    }
                }

                //For Mobile Details

                if (dtEmail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEmail.Rows.Count; i++)
                    {
                        EmailID = dtEmail.Rows[i]["EmailID"].ToString();
                        obj.EmailID = dtEmail.Rows[i]["EmailID"].ToString();
                        obj.Param = 4;
                        obj.InsertRecord();
                    }
                }
                if (status == "Save")
                {
                    Page.TraceWrite("Save button clicked event ends.");
                    Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
                    Response.Redirect("AvailableClientsData.aspx");
                }
                else
                {
                    ClearData("1");
                    AddAdressDetail();
                    AddEmailDetail();
                    AddMobileDetail();
                    Page.Message("1", hdnPrimeId.Value);
                }
            }//End 

        }
        catch { }
    }

    public void UpdateData()
    {
        try
        {
            Page.TraceWrite("Save Update button clicked event starts.");

            DataTable dtAddress = (DataTable)ViewState["Address"];
            DataTable dtEmail = (DataTable)ViewState["Email"];
            DataTable dtMobile = (DataTable)ViewState["Mobile"];


            string Address = string.Empty, Street2 = string.Empty, PinCode = string.Empty, Mobileno = string.Empty, EmailID = string.Empty,StateName=string.Empty,CityName=string.Empty;
            int CountryID = 0, CityID = 0, StateID = 0;
            //For Inserty Client Name
            obj.ClientID = Convert.ToInt32(hdnPrimeId.Value);
            obj.ClientName = txtClientName.Text.Trim();
            if (rdoCustomer.Checked == true)
            {                
                obj.isCustomer = "Y";
            }
            if (rdoSupplier.Checked == true)
            { 
                obj.isCustomer = "N"; 
            }
            if (rdoOthers.Checked == true)
            {
                obj.isCustomer = "O";
            }
            obj.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            obj.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            obj.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
            obj.Param = 6;

            string Status = obj.UpdateRecord();


            if (Status == "1")
            {
                errorExplanation.Attributes.Remove("class");
                errorExplanation.Style.Add("display", "none");
                //For delete all child record as per client wise
                obj.Param = 7;
                obj.UpdateRecord();
                //-----For Address Details

                if (dtAddress.Rows.Count > 0)
                {
                    for (int i = 0; i < dtAddress.Rows.Count; i++)
                    {
                        Address = dtAddress.Rows[i]["Address"].ToString();
                        Street2 = dtAddress.Rows[i]["Street"].ToString();
                        CountryID = Convert.ToInt32(dtAddress.Rows[i]["CountryID"]);
                        StateName = Convert.ToString(dtAddress.Rows[i]["StateName"]);
                        CityName= Convert.ToString(dtAddress.Rows[i]["CityName"]);
                        PinCode = dtAddress.Rows[i]["PinCode"].ToString();

                        obj.Address = Address.Trim();
                        obj.Street = Street2.Trim();
                        obj.Pincode = PinCode.Trim();
                        obj.CountryID = CountryID;
                        obj.StateName = StateName;
                        obj.CityName = CityName;
                        obj.Param = 2;
                        obj.InsertRecord();

                    }
                }
                //For Mobile Details

                if (dtMobile.Rows.Count > 0)
                {
                    for (int i = 0; i < dtMobile.Rows.Count; i++)
                    {
                        Mobileno = dtMobile.Rows[i]["ContactNo"].ToString();
                        obj.ContactNo = dtMobile.Rows[i]["ContactNo"].ToString();
                        obj.Param = 3;
                        obj.InsertRecord();
                    }
                }
                //For Mobile Details

                if (dtEmail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEmail.Rows.Count; i++)
                    {
                        EmailID = dtEmail.Rows[i]["EmailID"].ToString();
                        obj.EmailID = dtEmail.Rows[i]["EmailID"].ToString();
                        obj.Param = 4;
                        obj.InsertRecord();
                    }
                }

                Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
                Response.Redirect("AvailableClientsData.aspx");
            }
            else
            {
               // lblTitle.Text = "Client Name is already exist.";
                errorExplanation.Attributes.Add("class", "flash error");
                errorExplanation.Style.Add("display", "block");
                errorExplanation.InnerHtml = "Client Name is already exist.";
            }

        }
        catch { }
    }

    public void EditClientDetails()
    {
        obj.ClientID = Convert.ToInt32(hdnPrimeId.Value);
        obj.Param = 5;
        obj.ReadEditData();

        List<AvailableClient> Client = new List<AvailableClient>();
        Client = obj.ReadClient;
        txtClientName.Text = Client[0].ClientName;
        if (Client[0].isCustomer=="Y")
        {
            rdoCustomer.Checked = true;
        }
        if (Client[0].isCustomer == "N")
        {
            rdoSupplier.Checked = true;
        }
        if (Client[0].isCustomer == "O")
        {
            rdoOthers.Checked = true;
        }

    }

    private void AddAdressDetail()
    {
        try
        {
            DataTable dtAddr = new DataTable();
            DataColumn Address = new DataColumn();
            Address.DataType = System.Type.GetType("System.String");
            Address.ColumnName = "Address";
            dtAddr.Columns.Add(Address);

            DataColumn Street = new DataColumn();
            Street.DataType = System.Type.GetType("System.String");
            Street.ColumnName = "Street";
            dtAddr.Columns.Add(Street);

            DataColumn CountryName = new DataColumn();
            CountryName.DataType = System.Type.GetType("System.String");
            CountryName.ColumnName = "CountryName";
            dtAddr.Columns.Add(CountryName);

            DataColumn StateName = new DataColumn();
            StateName.DataType = System.Type.GetType("System.String");
            StateName.ColumnName = "StateName";
            dtAddr.Columns.Add(StateName);

            DataColumn CityName = new DataColumn();
            CityName.DataType = System.Type.GetType("System.String");
            CityName.ColumnName = "CityName";
            dtAddr.Columns.Add(CityName);

            DataColumn Pincode = new DataColumn();
            Pincode.DataType = System.Type.GetType("System.String");
            Pincode.ColumnName = "Pincode";
            dtAddr.Columns.Add(Pincode);

            DataColumn CountryID = new DataColumn();
            CountryID.DataType = System.Type.GetType("System.String");
            CountryID.ColumnName = "CountryID";
            dtAddr.Columns.Add(CountryID);

            DataColumn StateID = new DataColumn();
            StateID.DataType = System.Type.GetType("System.String");
            StateID.ColumnName = "StateID";
            dtAddr.Columns.Add(StateID);

            DataColumn CityID = new DataColumn();
            CityID.DataType = System.Type.GetType("System.String");
            CityID.ColumnName = "CityID";
            dtAddr.Columns.Add(CityID);

            DataColumn IsUsed = new DataColumn();
            IsUsed.DataType = System.Type.GetType("System.String");
            IsUsed.ColumnName = "IsUsed";
            dtAddr.Columns.Add(IsUsed);

            DataRow rwResource = dtAddr.NewRow();

            if (hdnPrimeId.Value == "0" || hdnPrimeId.Value == "")
            {
                dtlAddress.DataSource = dtAddr;
                dtlAddress.DataBind();
            }
            else
            {
                obj.ClientID = Convert.ToInt32(hdnPrimeId.Value);
                obj.Param = 5;
                obj.ReadEditData();
                List<AvailableClient> Addresslist = new List<AvailableClient>();
                Addresslist = obj.ReadAddress;
                foreach (var item in Addresslist)
                {
                    dtAddr.Rows.Add(item.Address, item.Street, item.CountryName, item.StateName, item.CityName, item.Pincode, item.CountryID, item.StateID, item.CityID, item.IsUsed);
                }
                if (dtAddr.Rows.Count > 0)
                {
                    dtlAddress.DataSource = dtAddr;
                    dtlAddress.DataBind();
                }
            }
            ViewState["Address"] = dtAddr;
        }
        catch (Exception ex)
        {

        }
    }

    private void AddEmailDetail()
    {
        try
        {
            DataTable dtAddr = new DataTable();

            DataColumn EmailID = new DataColumn();
            EmailID.DataType = System.Type.GetType("System.String");
            EmailID.ColumnName = "EmailID";
            dtAddr.Columns.Add(EmailID);
            DataColumn IsUsed = new DataColumn();
            IsUsed.DataType = System.Type.GetType("System.String");
            IsUsed.ColumnName = "IsUsed";
            dtAddr.Columns.Add(IsUsed);

            DataRow rwResource = dtAddr.NewRow();

            if (hdnPrimeId.Value == "0" || hdnPrimeId.Value == "")
            {
                dtlEmail.DataSource = dtAddr;
                dtlEmail.DataBind();
            }
            else
            {
                obj.ClientID = Convert.ToInt32(hdnPrimeId.Value);
                obj.Param = 5;
                obj.ReadEditData();
                List<AvailableClient> Emaillist = new List<AvailableClient>();
                Emaillist = obj.ReadEmailID;
                foreach (var item in Emaillist)
                {
                    dtAddr.Rows.Add(item.EmailID, item.IsUsed);
                }
                if (dtAddr.Rows.Count > 0)
                {
                    dtlEmail.DataSource = dtAddr;
                    dtlEmail.DataBind();
                }
            }
            ViewState["Email"] = dtAddr;

        }
        catch (Exception ex)
        {

        }
    }

    private void AddMobileDetail()
    {
        try
        {
            DataTable dtAddr = new DataTable();
            DataColumn ContactNo = new DataColumn();
            ContactNo.DataType = System.Type.GetType("System.String");
            ContactNo.ColumnName = "ContactNo";
            dtAddr.Columns.Add(ContactNo);
            DataColumn IsUsed = new DataColumn();
            IsUsed.DataType = System.Type.GetType("System.String");
            IsUsed.ColumnName = "IsUsed";
            dtAddr.Columns.Add(IsUsed);

            DataRow rwResource = dtAddr.NewRow();

            if (hdnPrimeId.Value == "0" || hdnPrimeId.Value == "")
            {
                dtlMobile.DataSource = dtAddr;
                dtlMobile.DataBind();
            }
            else
            {
                obj.ClientID = Convert.ToInt32(hdnPrimeId.Value);
                obj.Param = 5;
                obj.ReadEditData();
                List<AvailableClient> Contactlist = new List<AvailableClient>();
                Contactlist = obj.ReadContact;
                foreach (var item in Contactlist)
                {
                    dtAddr.Rows.Add(item.ContactNo, item.IsUsed);
                }
                if (dtAddr.Rows.Count > 0)
                {
                    dtlMobile.DataSource = dtAddr;
                    dtlMobile.DataBind();
                }
            }
            ViewState["Mobile"] = dtAddr;
        }
        catch (Exception ex)
        {

        }
    }

    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }
    }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (Add == "N" && (hdnPrimeId.Value == "" || hdnPrimeId.Value == "0" || hdnPrimeId.Value == null))
        {
            Page.extDisableControls();
            btnBack.Enabled = true;
        }
        if (View == "N")
        {
            btnBack.Enabled = false;
        }
        Page.TraceWrite("AccessVisibility starts.");
    }

    void ClearData(string status)
    {
        if (status == "1")
        {
            Page.TraceWrite("clearData starts.");
            obj.ClientID = 0;
            txtClientName.Text = "";
            Page.TraceWrite("clear Data ends.");
            txtAddress.Text  = "";
            txtStreet.Text = "";
            txtEmailID.Value = "";
            txtMobile.Text = "";
            txtPinCode.Text = "";
            ddlCountry.SelectedIndex = -1;
        }
    }

    protected void btnSaveAndContinue_Click(object sender, EventArgs e)
    {
        try
        {
            string Status = CheckValidation();
           // lblTitle.Text = "";
            if (Status == "0")
            {
                Page.TraceWrite("Save and Add button clicked event starts.");
                SaveData("SaveAndContinuee");
                
                Page.TraceWrite("Save and Add button clicked event ends.");
            }

        }
        catch { }
    }

    #region Adress Details
    protected void btnAddress_Click(object sender, EventArgs e)
    {
        try
        {
            string Address = string.Empty;
            DataTable dtaddr = new DataTable();
            dtaddr = (DataTable)ViewState["Address"];
            DataRow reResource = dtaddr.NewRow();
            DataTable dtResource;

            //For Update Address
            if (hdnAddrStatus.Value == "UpdateAddress")
            {
                for (int i = 0; i < dtaddr.Rows.Count; i++)
                {
                    if (dtaddr.Rows[i]["Address"].ToString() == hdnAddredetails.Value)
                    {
                        dtaddr.Rows[i].Delete();
                        dtaddr.AcceptChanges();
                        btnAddress.Text = "Add Address";
                    }
                }
            }
            //End

            if (dtaddr.Rows.Count <= 0)
            {
                if (txtAddress.Text != "")
                {
                    Address = txtAddress.Text.ToString();
                    reResource["Address"] = txtAddress.Text.ToString();
                    reResource["Street"] = txtStreet.Text.ToString();
                    reResource["CountryName"] = hdnCountryName.Value;
                    reResource["StateName"] = ddlState.Text;
                    reResource["CityName"] = ddlCity.Text;
                    reResource["Pincode"] = txtPinCode.Text.ToString();
                    reResource["CountryID"] = ddlCountry.Value.ToString();
                    //reResource["StateID"] = hdnStateId.Value;
                    //reResource["CityID"] = hdnCityId.Value;

                    for (int i = 0; i < dtaddr.Rows.Count; i++)
                    {
                        if (Address == dtaddr.Rows[i]["Address"].ToString())
                        {
                            lblError.Text = "Address already added.";
                            lblError.ForeColor = System.Drawing.Color.Red;
                            return;
                        }
                    }
                    lblError.Text = "";
                    dtaddr.Rows.Add(reResource);
                }
            }
            else
            {
                //if (txtResourceRate.Text.Trim().Length > 0)
                if (txtAddress.Text != "")
                {
                    Address = txtAddress.Text.ToString();
                    reResource["Address"] = txtAddress.Text.ToString();
                    reResource["Street"] = txtStreet.Text.ToString();
                    reResource["CountryName"] = hdnCountryName.Value;
                    reResource["StateName"] = ddlState.Text;
                    reResource["CityName"] = ddlCity.Text;
                    reResource["Pincode"] = txtPinCode.Text.ToString();
                    reResource["CountryID"] = ddlCountry.Value.ToString();
                    //reResource["StateID"] = hdnStateId.Value;
                    //reResource["CityID"] = hdnCityId.Value;

                    if (dtaddr.Rows.Count <= 0)
                    {
                        lblError.Text = "Add atleast one Address.";
                        lblError.ForeColor = System.Drawing.Color.Red;
                        return;
                    }

                    for (int i = 0; i < dtaddr.Rows.Count; i++)
                    {
                        if (Address == dtaddr.Rows[i]["Address"].ToString())
                        {
                            lblError.Text = "Address already added.";
                            lblError.ForeColor = System.Drawing.Color.Red;
                            return;
                        }
                    }
                    lblError.Text = "";
                    dtaddr.Rows.Add(reResource);
                }
            }

            dtlAddress.DataSource = dtaddr;
            dtlAddress.DataBind();
            ViewState["Address"] = dtaddr;
            lblError.Text = "";
            txtPinCode.Text = "";
            txtAddress.Text = "";
            txtStreet.Text = "";
            ddlState.Text = "";
            ddlCity.Text = "";
            ddlCountry.SelectedIndex = 0;
            
        }
        catch (Exception ex)
        {

        }

    }
    protected void dtlAddress_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        //Label lblCityID = (Label)e.Item.FindControl("lblCityID");
        //Label lblStateID = (Label)e.Item.FindControl("lblStateID");
        Label lblCountryID = (Label)e.Item.FindControl("lblCountryID");

        //lblCityID.Visible = false;
        lblCountryID.Visible = false;
        //lblStateID.Visible = false;
    }
    protected void dtlAddress_CancelCommand(object source, DataListCommandEventArgs e)
    {

    }
    protected void dtlAddress_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        try
        {
            DataTable dtAddr = new DataTable();
            dtAddr = (DataTable)ViewState["Address"];
            dtAddr.Rows.RemoveAt(e.Item.ItemIndex);

            dtlAddress.DataSource = dtAddr;
            dtlAddress.DataBind();
        }
        catch (Exception ex)
        {

        }
    }
    protected void dtlAddress_EditCommand(object source, DataListCommandEventArgs e)
    {
        Label lblAddress = (Label)e.Item.FindControl("lblAddress");
        Label lblStreet = (Label)e.Item.FindControl("lblStreet");
        Label lblCountryID = (Label)e.Item.FindControl("lblCountryID");
        Label lblStateName = (Label)e.Item.FindControl("lblStateName");
        Label lblCityName = (Label)e.Item.FindControl("lblCityName");
        Label lblPincode = (Label)e.Item.FindControl("lblPincode");
        Label lblState = (Label)e.Item.FindControl("lblState");

        txtAddress.Text = lblAddress.Text;
        txtStreet.Text = lblStreet.Text;
        ddlCountry.Value = lblCountryID.Text;
        ddlCity.Text = lblCityName.Text;
        ddlState.Text = lblStateName.Text;
       
        txtPinCode.Text = lblPincode.Text;
        btnAddress.Text = "Update Address No";
        hdnAddrStatus.Value = "UpdateAddress";
        hdnAddredetails.Value = txtAddress.Text;
        hdnStreetdetails.Value = txtStreet.Text;
        //hdnStateIdRead.Value = lblStateID.Text;
        //hdnCityIdRead.Value = lblCityID.Text;

        ScriptManager.RegisterStartupScript(this, this.GetType(), "test", "GetSelectValue();", true);

        
    }
    protected void dtlAddress_UpdateCommand(object source, DataListCommandEventArgs e)
    {

    }

    #endregion

    #region Bind EmailID
    protected void btnAddEmail_Click(object sender, EventArgs e)
    {
        try
        {
            string Email = string.Empty;
            DataTable dtaddr = new DataTable();
            dtaddr = (DataTable)ViewState["Email"];
            DataRow reResource = dtaddr.NewRow();
            DataTable dtResource;

            //For Update Address
            if (hdnEmailStatus.Value == "UpdateEmail")
            {
                for (int i = 0; i < dtaddr.Rows.Count; i++)
                {
                    if (dtaddr.Rows[i]["EmailID"].ToString() == hdnEmailDetail.Value)
                    {
                        dtaddr.Rows[i].Delete();
                        dtaddr.AcceptChanges();
                        btnAddEmail.Text = "Add Email ID";
                    }
                }
            }
            //End

            if (dtaddr.Rows.Count <= 0)
            {

                if (txtEmailID.Value != "")
                {
                    Email = txtEmailID.Value;
                    reResource["EmailID"] = txtEmailID.Value;

                    for (int i = 0; i < dtaddr.Rows.Count; i++)
                    {
                        if (Email == dtaddr.Rows[i]["EmailID"].ToString())
                        {
                            lblemailError.Text = "Email ID already added.";
                            lblemailError.ForeColor = System.Drawing.Color.Red;
                            return;
                        }
                    }
                    lblError.Text = "";
                    dtaddr.Rows.Add(reResource);
                }
            }
            else
            {
                if (txtEmailID.Value != "")
                {
                    Email = txtEmailID.Value;
                    reResource["EmailID"] = txtEmailID.Value;

                    if (dtaddr.Rows.Count <= 0)
                    {
                        lblemailError.Text = "Add atleast one Email ID.";
                        lblemailError.ForeColor = System.Drawing.Color.Red;
                        return;
                    }

                    for (int i = 0; i < dtaddr.Rows.Count; i++)
                    {
                        if (Email == dtaddr.Rows[i]["EmailID"].ToString())
                        {
                            lblemailError.Text = "EmailID already added.";
                            lblemailError.ForeColor = System.Drawing.Color.Red;
                            return;
                        }
                    }
                    lblemailError.Text = "";
                    dtaddr.Rows.Add(reResource);
                }
            }

            dtlEmail.DataSource = dtaddr;
            dtlEmail.DataBind();
            ViewState["Email"] = dtaddr;
            lblemailError.Text = "";
            txtEmailID.Value = "";

        }
        catch (Exception ex)
        {

        }

    }
    protected void dtlEmail_CancelCommand(object source, DataListCommandEventArgs e)
    {

    }
    protected void dtlEmail_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        try
        {
            DataTable dtAddr = new DataTable();
            dtAddr = (DataTable)ViewState["Email"];
            dtAddr.Rows.RemoveAt(e.Item.ItemIndex);

            dtlEmail.DataSource = dtAddr;
            dtlEmail.DataBind();
        }
        catch (Exception ex)
        {

        }
    }
    protected void dtlEmail_EditCommand(object source, DataListCommandEventArgs e)
    {
        Label lblEmail = (Label)e.Item.FindControl("lblEmail");
        txtEmailID.Value = lblEmail.Text;
        btnAddEmail.Text = "Update Email ID";
        hdnEmailStatus.Value = "UpdateEmail";
        hdnEmailDetail.Value = lblEmail.Text;

    }
    protected void dtlEmail_ItemDataBound(object sender, DataListItemEventArgs e)
    {

    }
    protected void dtlEmail_UpdateCommand(object source, DataListCommandEventArgs e)
    {

    }
    #endregion

    #region Bind Mobile
    protected void btnAddMobile_Click(object sender, EventArgs e)
    {
        try
        {
            string Mobile = string.Empty;
            DataTable dtaddr = new DataTable();
            dtaddr = (DataTable)ViewState["Mobile"];
            DataRow reResource = dtaddr.NewRow();
            DataTable dtResource;

            //For Update MobileNo
            if (hdnContactStatus.Value == "UpdateContact")
            {
                for (int i = 0; i < dtaddr.Rows.Count; i++)
                {
                    if (dtaddr.Rows[i]["ContactNo"].ToString() == hdnContactNo.Value)
                    {
                        dtaddr.Rows[i].Delete();
                        dtaddr.AcceptChanges();
                        btnAddMobile.Text = "Add Contact No";
                    }
                }
            }
            //End


            if (dtaddr.Rows.Count <= 0)
            {

                if (txtMobile.Text != "")
                {
                    Mobile = txtMobile.Text.ToString();
                    reResource["ContactNo"] = txtMobile.Text;

                    for (int i = 0; i < dtaddr.Rows.Count; i++)
                    {
                        if (Mobile == dtaddr.Rows[i]["ContactNo"].ToString())
                        {
                            lblMobileError.Text = "Contact No already added.";
                            lblMobileError.ForeColor = System.Drawing.Color.Red;
                            return;
                        }
                    }
                    lblError.Text = "";
                    dtaddr.Rows.Add(reResource);
                }
            }
            else
            {
                if (txtMobile.Text != "")
                {
                    Mobile = txtMobile.Text.ToString();
                    reResource["ContactNo"] = txtMobile.Text;

                    if (dtaddr.Rows.Count <= 0)
                    {
                        lblMobileError.Text = "Add atleast one Contact No.";
                        lblMobileError.ForeColor = System.Drawing.Color.Red;
                        return;
                    }

                    for (int i = 0; i < dtaddr.Rows.Count; i++)
                    {
                        if (Mobile == dtaddr.Rows[i]["ContactNo"].ToString())
                        {
                            lblMobileError.Text = "Contact No already added.";
                            lblMobileError.ForeColor = System.Drawing.Color.Red;
                            return;
                        }
                    }
                    lblMobileError.Text = "";
                    dtaddr.Rows.Add(reResource);
                }
            }

            dtlMobile.DataSource = dtaddr;
            dtlMobile.DataBind();
            ViewState["Mobile"] = dtaddr;
            lblMobileError.Text = "";
            txtMobile.Text = "";
        }
        catch (Exception ex)
        {

        }
    }
    protected void dtlMobile_CancelCommand(object source, DataListCommandEventArgs e)
    {

    }
    protected void dtlMobile_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        try
        {
            DataTable dtAddr = new DataTable();
            dtAddr = (DataTable)ViewState["Mobile"];
            dtAddr.Rows.RemoveAt(e.Item.ItemIndex);

            dtlMobile.DataSource = dtAddr;
            dtlMobile.DataBind();
        }
        catch (Exception ex)
        {

        }
    }
    protected void dtlMobile_EditCommand(object source, DataListCommandEventArgs e)
    {
       
        Label lblContact = (Label)e.Item.FindControl("lblContact");
        txtMobile.Text = lblContact.Text;
        btnAddMobile.Text = "Update Contact No";
        hdnContactStatus.Value = "UpdateContact";
        hdnContactNo.Value = lblContact.Text; ;
       // UpdatePanel2.Update();
       

    }
    protected void dtlMobile_ItemDataBound(object sender, DataListItemEventArgs e)
    {

    }
    protected void dtlMobile_UpdateCommand(object source, DataListCommandEventArgs e)
    {

    }
    #endregion

    protected void Button1_Click(object sender, EventArgs e)
    {

    }
}