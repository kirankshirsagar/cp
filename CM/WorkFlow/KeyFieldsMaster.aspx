﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="KeyFieldsMaster.aspx.cs" Inherits="WorkFlow_KeyFieldsMaster" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery-ui-1.8.21.css" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../scripts/jquery-ui-1.8.16.custom.css" />
    <link rel="stylesheet" href="../modalfiles/modal.css" type="text/css" />
    <script src="../JQueryValidations/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../scripts/jquery-ui-timepicker-addon.js"></script>
    <link rel="stylesheet" href="../scripts/jquery-ui-timepicker-addon.css" type="text/css" />
    <style type="text/css">
        .tdAlign
        {
            text-align: right;
            width: 30%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            try {
                $("#txteffectivedate").datepicker({
                    showOn: "both",
                    buttonImage: "../Styles/css/icons/16/calendar_1.png",
                    buttonImageOnly: true,
                    defaultDate: "+1d",
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1,
                    dateFormat: 'dd-M-yy'
                }).attr('placeholder', 'dd-Mmm-yyyy');
                $("#txtexpirationdate").datepicker({
                    showOn: "both",
                    buttonImage: "../Styles/css/icons/16/calendar_1.png",
                    buttonImageOnly: true,
                    defaultDate: "+1d",
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1,
                    dateFormat: 'dd-M-yy'
                }).attr('placeholder', 'dd-Mmm-yyyy');
                $("#txtrenewaldate").datepicker({
                    showOn: "both",
                    buttonImage: "../Styles/css/icons/16/calendar_1.png",
                    buttonImageOnly: true,
                    defaultDate: "+1d",
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1,
                    dateFormat: 'dd-M-yy'
                }).attr('placeholder', 'dd-Mmm-yyyy');

                $(".datepicker").datepicker({
                    showOn: "both",
                    buttonImage: "../Styles/css/icons/16/calendar_1.png",
                    buttonImageOnly: true,
                    defaultDate: "+1d",
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1,
                    dateFormat: 'dd-M-yy'
                }).attr('placeholder', 'dd-Mmm-yyyy');
            }
            catch (e) {
                alert(e);
            }

        });
     
    </script>
    <script type="text/javascript">
        $("#requesttab").addClass("selectedtab");
    </script>
    <h2>
        Important Dates for request #<asp:Label ID="lblRequestIDheader" runat="server"></asp:Label>
    </h2>
    <div>
        <div id="tblContainer">
            <p>
                <a href='RequestFlow.aspx?RequestId=<%=ViewState["RequestId"].ToString() %>&Status=KeyField'
                    class="issue status-1 priority-4 parent">Request #<%=ViewState["RequestId"].ToString() %></a>:
                Important Dates for request #<asp:Label ID="lblReqID" runat="server"></asp:Label>
            </p>
        </div>
    </div>
    <div class="box" style="padding-left: 60px">
        <table id="ImportantDateTable" runat="server" width="100%" style="table-layout: fixed">
            <thead>
                <tr>
                    <th width="20%">
                    </th>
                    <th width="80%">
                    </th>
                </tr>
            </thead>
            <tr>
                <td class="tdAlign">
                    <label for="time_entry_issue_id">
                        Date of Agreement
                    </label>
                </td>
                <td>
                    &nbsp;<input id="txteffectivedate" runat="server" maxlength="100" clientidmode="static"
                        readonly="readonly" type="text" autocomplete="false" style="width: 20%;" class="datepicker" />
                </td>
            </tr>
            <tr>
                <td class="tdAlign">
                    <label for="time_entry_issue_id">
                        Expiration date of contract
                    </label>
                </td>
                <td>
                    &nbsp;<input id="txtexpirationdate" runat="server" maxlength="100" clientidmode="static"
                        readonly="readonly" type="text" autocomplete="false" style="width: 20%;" class="datepicker" />
                    &nbsp;<asp:RequiredFieldValidator ID="reqexpir" runat="server" Display="Dynamic" ControlToValidate="txtexpirationdate"
                        Font-Italic="false" ErrorMessage="This field is required." ValidationGroup="Save"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <fieldset>
                        <legend style="color: Black; font-weight: bold">Set reminder for expiration date</legend>
                        <table width="100%" style="table-layout: fixed" border="0" cellpadding="0" cellspacing="0"
                            class="reminder">
                            <thead>
                                <tr>
                                    <th width="2%">
                                    </th>
                                    <th width="22%">
                                    </th>
                                    <th width="69%">
                                    </th>
                                </tr>
                            </thead>
                            <tr>
                                <td style="display: block;">
                                    <input type="checkbox" id="chkexp180" runat="server" clientidmode="Static" onchange="CheckDateStatus('Expiration','ddlRemindExpiration1','ddlexp180Users1','ddlexp180Users2','chkexp180');" />
                                </td>
                                <td>
                                    <select id="ddlRemindExpiration1" onchange="ddlDateStatus('Expiration','ddlRemindExpiration1','ddlexp180Users1','ddlexp180Users2','chkexp180');"
                                        clientidmode="Static" runat="server" style="width: 210px" class="cssExpire1 chzn-select chzn-select">
                                        <option value="0">-----No Reminder------</option>
                                        <option value="180">Remind one eighty days before</option>
                                        <option value="90">Remind ninety days before</option>
                                        <option value="60">Remind sixty days before</option>
                                        <option value="30">Remind thirty days before</option>
                                        <option value="7">Remind seven days before</option>
                                        <option value="1">Remind one day before</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="ddlexp180Users1" onchange="SetUserValue('ddlexp180Users1','ddlRemindExpiration1','ddlexp180Users2');"
                                        clientidmode="Static" runat="server" style="width: 203px" class="chzn-select required chzn-select">
                                        <option></option>
                                    </select>
                                    <select id="ddlexp180Users2" onchange="SetUserValue('ddlexp180Users2','ddlRemindExpiration1','ddlexp180Users1');"
                                        clientidmode="Static" runat="server" style="width: 203px" class="chzn-select required chzn-select">
                                        <option></option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td style="display: block;">
                                    <input type="checkbox" id="chkexp90" runat="server" clientidmode="Static" onclick="CheckDateStatus('Expiration','ddlRemindExpiration2','ddlexp90Users1','ddlexp90Users2','chkexp90');" />
                                </td>
                                <td>
                                    <select id="ddlRemindExpiration2" onchange="ddlDateStatus('Expiration','ddlRemindExpiration2','ddlexp90Users1','ddlexp90Users2','chkexp90');"
                                        clientidmode="Static" runat="server" style="width: 210px" class="cssExpire2 chzn-select chzn-select">
                                        <option value="0">-----No Reminder------</option>
                                        <option value="180">Remind one eighty days before</option>
                                        <option value="90">Remind ninety days before</option>
                                        <option value="60">Remind sixty days before</option>
                                        <option value="30">Remind thirty days before</option>
                                        <option value="7">Remind seven days before</option>
                                        <option value="1">Remind one day before</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="ddlexp90Users1" onchange="SetUserValue('ddlexp90Users1','ddlRemindExpiration2','ddlexp90Users2');"
                                        clientidmode="Static" runat="server" style="width: 203px" class="chzn-select required chzn-select">
                                        <option></option>
                                    </select>
                                    <select id="ddlexp90Users2" onchange="SetUserValue('ddlexp90Users2','ddlRemindExpiration2','ddlexp90Users1');"
                                        clientidmode="Static" runat="server" style="width: 203px" class="chzn-select required chzn-select">
                                        <option></option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td style="display: block;">
                                    <input type="checkbox" id="chkexp60" runat="server" clientidmode="Static" onclick="CheckDateStatus('Expiration','ddlRemindExpiration3','ddlexp60Users1','ddlexp60Users2','chkexp60');" />
                                </td>
                                <td>
                                    <select id="ddlRemindExpiration3" onchange="ddlDateStatus('Expiration','ddlRemindExpiration3','ddlexp60Users1','ddlexp60Users2','chkexp60');"
                                        clientidmode="Static" runat="server" style="width: 210px" class="cssExpire3 chzn-select chzn-select">
                                        <option value="0">-----No Reminder------</option>
                                        <option value="180">Remind one eighty days before</option>
                                        <option value="90">Remind ninety days before</option>
                                        <option value="60">Remind sixty days before</option>
                                        <option value="30">Remind thirty days before</option>
                                        <option value="7">Remind seven days before</option>
                                        <option value="1">Remind one day before</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="ddlexp60Users1" onchange="SetUserValue('ddlexp60Users1','ddlRemindExpiration3','ddlexp60Users2');"
                                        clientidmode="Static" runat="server" style="width: 203px" class="chzn-select required chzn-select">
                                        <option></option>
                                    </select>
                                    <select id="ddlexp60Users2" onchange="SetUserValue('ddlexp60Users2','ddlRemindExpiration3','ddlexp60Users1');"
                                        clientidmode="Static" runat="server" style="width: 203px" class="chzn-select required chzn-select">
                                        <option></option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td class="tdAlign">
                    <label for="time_entry_issue_id">
                        Renewal notification date
                    </label>
                </td>
                <td>
                    &nbsp;<input id="txtrenewaldate" runat="server" maxlength="100" clientidmode="static" readonly="readonly"
                        type="text" autocomplete="false" style="width: 20%;" class="datepicker" />
                    &nbsp;<asp:RequiredFieldValidator ID="reqrenewal" runat="server" Display="Dynamic" ControlToValidate="txtrenewaldate"
                        Font-Italic="false" ErrorMessage="This field is required." ValidationGroup="Save"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <fieldset>
                        <legend style="color: Black; font-weight: bold">Set reminder for renewal date</legend>
                        <table width="100%" style="table-layout: fixed" border="0" cellpadding="0" cellspacing="0"
                            class="reminder">
                            <thead>
                                <tr>
                                    <th width="2%">
                                    </th>
                                    <th width="22%">
                                    </th>
                                    <th width="69%">
                                    </th>
                                </tr>
                            </thead>
                            <tr>
                                <td style="display: block;">
                                    <input type="checkbox" id="chkrem180" runat="server" clientidmode="Static" onclick="CheckDateStatus('Renewal','ddlRemindRenewal1','ddlRenewal180User1','ddlRenewal180User2','chkrem180');" />
                                </td>
                                <td>
                                    <select id="ddlRemindRenewal1" onchange="ddlDateStatus('Renewal','ddlRemindRenewal1','ddlRenewal180User1','ddlRenewal180User2','chkrem180');"
                                        clientidmode="Static" runat="server" style="width: 210px" class="cssRemind1 chzn-select chzn-select">
                                        <option value="0">-----No Reminder------</option>
                                        <option value="180">Remind one eighty days before</option>
                                        <option value="90">Remind ninety days before</option>
                                        <option value="60">Remind sixty days before</option>
                                        <option value="30">Remind thirty days before</option>
                                        <option value="7">Remind seven days before</option>
                                        <option value="1">Remind one day before</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="ddlRenewal180User1" onchange="SetUserValue('ddlRenewal180User1','ddlRemindRenewal1','ddlRenewal180User2');"
                                        clientidmode="Static" runat="server" style="width: 203px" class="chzn-select required chzn-select">
                                        <option></option>
                                    </select>&nbsp;<select id="ddlRenewal180User2" onchange="return SetUserValue('ddlRenewal180User2','ddlRemindRenewal1','ddlRenewal180User1')"
                                        clientidmode="Static" runat="server" style="width: 203px" class="chzn-select required chzn-select">
                                        <option></option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td style="display: block;">
                                    <input type="checkbox" id="chkrem90" runat="server" clientidmode="Static" onclick="CheckDateStatus('Renewal','ddlRemindRenewal2','ddlRenewal90User1','ddlRenewal90User2','chkrem90');" />
                                </td>
                                <td>
                                    <select id="ddlRemindRenewal2" onchange="ddlDateStatus('Renewal','ddlRemindRenewal2','ddlRenewal90User1','ddlRenewal90User2','chkrem90');"
                                        clientidmode="Static" runat="server" style="width: 210px" class="cssRemind2 chzn-select chzn-select">
                                        <option value="0">-----No Reminder------</option>
                                        <option value="180">Remind one eighty days before</option>
                                        <option value="90">Remind ninety days before</option>
                                        <option value="60">Remind sixty days before</option>
                                        <option value="30">Remind thirty days before</option>
                                        <option value="7">Remind seven days before</option>
                                        <option value="1">Remind one day before</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="ddlRenewal90User1" onchange="SetUserValue('ddlRenewal90User1','ddlRemindRenewal2','ddlRenewal90User2');"
                                        clientidmode="Static" runat="server" style="width: 203px" class="chzn-select required chzn-select">
                                        <option></option>
                                    </select>&nbsp;<select id="ddlRenewal90User2" onchange="return SetUserValue('ddlRenewal90User2','ddlRemindRenewal2','ddlRenewal90User1')"
                                        clientidmode="Static" runat="server" style="width: 203px" class="chzn-select required chzn-select">
                                        <option></option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td style="display: block;">
                                    <input type="checkbox" id="chkrem60" runat="server" clientidmode="Static" onclick="CheckDateStatus('Renewal','ddlRemindRenewal3','ddlRenewal60User1','ddlRenewal60User2','chkrem60');" />
                                </td>
                                <td>
                                    <select id="ddlRemindRenewal3" onchange="ddlDateStatus('Renewal','ddlRemindRenewal3','ddlRenewal60User1','ddlRenewal60User2','chkrem60');"
                                        clientidmode="Static" runat="server" style="width: 210px" class="cssRemind3 chzn-select chzn-select">
                                        <option value="0">-----No Reminder------</option>
                                        <option value="180">Remind one eighty days before</option>
                                        <option value="90">Remind ninety days before</option>
                                        <option value="60">Remind sixty days before</option>
                                        <option value="30">Remind thirty days before</option>
                                        <option value="7">Remind seven days before</option>
                                        <option value="1">Remind one day before</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="ddlRenewal60User1" onchange="SetUserValue('ddlRenewal60User1','ddlRemindRenewal3','ddlRenewal60User2');"
                                        clientidmode="Static" runat="server" style="width: 203px" class="chzn-select required chzn-select">
                                        <option></option>
                                    </select>&nbsp;<select id="ddlRenewal60User2" onchange="SetUserValue('ddlRenewal60User2','ddlRemindRenewal3','ddlRenewal60User1');"
                                        clientidmode="Static" runat="server" style="width: 203px" class="chzn-select required chzn-select">
                                        <option></option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
        </table>
        <asp:PlaceHolder ID="pnlnewadd" runat="server"></asp:PlaceHolder>
        <br />
        <asp:LinkButton ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" ForeColor="White"
            Style="text-decoration: none" CssClass="linkbutton" ClientIDMode="Static" />
        <asp:LinkButton ID="btnBack" runat="server" OnClick="btnBack_Click" Text="Back" ForeColor="White"
            Style="text-decoration: none" CssClass="linkbutton" />
        <%-- Custom Remind Date   --%>
        <input id="hdnReminddate1" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" />
        <input id="hdnReminddate2" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" />
        <input id="hdnReminddate3" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" />
        <%-- Custom Remind Days   --%>
        <input id="hdnRemindDay10" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay20" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay30" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay11" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay21" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay31" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay12" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay22" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay32" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <%-- Custom Remind date user1   --%>
        <input id="hdnRemindDay1User10" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay1User20" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay1User11" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay1User21" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay1User12" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay1User22" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <%-- Custom Remind date user2   --%>
        <input id="hdnRemindDay2User10" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay2User20" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay2User11" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay2User21" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay2User12" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay2User22" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <%-- Custom Remind date user3   --%>
        <input id="hdnRemindDay3User10" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay3User20" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay3User11" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay3User21" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay3User12" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
        <input id="hdnRemindDay3User22" runat="server" clientidmode="static" readonly="readonly"
            type="hidden" value="0" />
    </div>
    <style>
        .validation
        {
            background: White;
            color: red;
            height: 24px;
            line-height: 24px;
            font-size: 15px;
            font-style: normal;
            padding: 0px;
            margin: 0px; /* float: left;
            border: solid 1px silver;*/
            z-index: 99;
            filter: alpha(opacity=50);
            opacity: 0.5;
        }
    </style>
    <script type="text/javascript" language="javascript">

        ///////////   Custom date 1         /////////////
        $(document).on('click', '#chkcust1800', function () {
            if ($("#chkcust1800").prop("checked") == false) {
                $("#ddlcustRemindRenewal10").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal180User10").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal180User20").val('0').trigger("liszt:updated");
            }
            debugger;
            var j = 0; var r = 0;
            $("table tbody tr.trdynamic").each(function (index, value) {
                $tr = $(value);
                if ($tr.find('.datepicker').val() != undefined) {
                    j++;
                    if ($tr.find('.datepicker').val() == '') {
                        if (r == 0) {
                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                            alert('Please insert ' + lbl + '.');
                            $("#chkcust1800").prop("checked", false);
                            return false;
                        }
                    }
                    r = j;
                }
            });
        });
        $(document).on('click', '#chkcust900', function () {
            if ($("#chkcust900").prop("checked") == false) {
                $("#ddlcustRemindRenewal20").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal90User10").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal90User20").val('0').trigger("liszt:updated");
            }
            var j = 0; var r = 0;
            $("table tbody tr.trdynamic").each(function (index, value) {
                $tr = $(value);
                if ($tr.find('.datepicker').val() != undefined) {
                    j++;
                    if ($tr.find('.datepicker').val() == '') {
                        if (r == 0) {
                            //alert("Please insert meta data date.");
                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                            alert('Please insert ' + lbl + '.');
                            $("#chkcust900").prop("checked", false);
                            return false;
                        }
                    }
                    r = j;
                }
            });
        });
        $(document).on('click', '#chkcust600', function () {
            if ($("#chkcust600").prop("checked") == false) {
                $("#ddlcustRemindRenewal30").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal60User10").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal60User20").val('0').trigger("liszt:updated");
            }
            var j = 0; var r = 0;
            $("table tbody tr.trdynamic").each(function (index, value) {
                $tr = $(value);
                if ($tr.find('.datepicker').val() != undefined) {
                    j++;
                    if ($tr.find('.datepicker').val() == '') {
                        if (r == 0) {
                            //alert("Please insert meta data date.");
                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                            alert('Please insert ' + lbl + '.');
                            $("#chkcust600").prop("checked", false);
                            return false;
                        }
                    }
                    r = j;
                }
            });
        });

        ///////////   Custom date 2         /////////////
        $(document).on('click', '#chkcust1801', function () {
            if ($("#chkcust1801").prop("checked") == false) {
                $("#ddlcustRemindRenewal11").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal180User11").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal180User21").val('0').trigger("liszt:updated");
            }
            var j = 0; var r = 0;
            $("table tbody tr.trdynamic").each(function (index, value) {
                $tr = $(value);
                if ($tr.find('.datepicker').val() != undefined) {
                    j++;
                    if ($tr.find('.datepicker').val() == '') {
                        //                        if (j == 1 && r == 0) {
                        //                            //alert("Please insert meta data date.");
                        //                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                        //                            alert('Please insert ' + lbl + '.');
                        //                            $("#chkcust1801").prop("checked", false);
                        //                            return false;
                        //                        }
                        if (r == 1) {
                            //alert("Please insert meta data date.");
                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                            alert('Please insert ' + lbl + '.');
                            $("#chkcust1801").prop("checked", false);
                            return false;
                        }
                    }
                    r = j;
                }
            });
        });

        $(document).on('click', '#chkcust901', function () {
            if ($("#chkcust901").prop("checked") == false) {
                $("#ddlcustRemindRenewal21").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal90User11").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal90User21").val('0').trigger("liszt:updated");
            }
            var j = 0; var r = 0;
            $("table tbody tr.trdynamic").each(function (index, value) {
                $tr = $(value);
                if ($tr.find('.datepicker').val() != undefined) {
                    j++;
                    if ($tr.find('.datepicker').val() == '') {
                        //                        if (j == 1 && r == 0) {
                        //                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                        //                            alert('Please insert ' + lbl + '.');
                        //                            $("#chkcust901").prop("checked", false);
                        //                            return false;
                        //                        }
                        if (r == 1) {
                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                            alert('Please insert ' + lbl + '.');
                            $("#chkcust901").prop("checked", false);
                            return false;
                        }
                    }
                    r = j;
                }
            });
        });

        $(document).on('click', '#chkcust601', function () {
            if ($("#chkcust601").prop("checked") == false) {
                $("#ddlcustRemindRenewal31").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal60User11").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal60User21").val('0').trigger("liszt:updated");
            }
            var j = 0; var r = 0;
            $("table tbody tr.trdynamic").each(function (index, value) {
                $tr = $(value);
                if ($tr.find('.datepicker').val() != undefined) {
                    j++;
                    if ($tr.find('.datepicker').val() == '') {
                        //                        if (j == 1 && r == 0) {
                        //                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                        //                            alert('Please insert ' + lbl + '.');
                        //                            $("#chkcust601").prop("checked", false);
                        //                            return false;
                        //                        }
                        if (r == 1) {
                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                            alert('Please insert ' + lbl + '.');
                            $("#chkcust601").prop("checked", false);
                            return false;
                        }
                    }
                    r = j;
                }
            });
        });

        ///////////   Custom date 3         /////////////
        $(document).on('click', '#chkcust1802', function () {
            if ($("#chkcust1802").prop("checked") == false) {
                $("#ddlcustRemindRenewal12").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal180User12").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal180User22").val('0').trigger("liszt:updated");
            }
            var j = 0; var r = 0;
            $("table tbody tr.trdynamic").each(function (index, value) {
                $tr = $(value);
                if ($tr.find('.datepicker').val() != undefined) {
                    j++;
                    if ($tr.find('.datepicker').val() == '') {
                        //                        if (j == 1 && r == 0) {
                        //                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                        //                            alert('Please insert ' + lbl + '.');
                        //                            $("#chkcust1802").prop("checked", false);
                        //                            return false;
                        //                        } else
                        //                            if (j == 2 && r == 1) {
                        //                                var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                        //                                alert('Please insert ' + lbl + '.');
                        //                                $("#chkcust1802").prop("checked", false);
                        //                                return false;
                        //                            }
                        if (r == 2) {
                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                            alert('Please insert ' + lbl + '.');
                            $("#chkcust1802").prop("checked", false);
                            return false;
                        }
                    }
                    r = j;
                }
            });
        });
        $(document).on('click', '#chkcust902', function () {
            if ($("#chkcust902").prop("checked") == false) {
                $("#ddlcustRemindRenewal22").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal90User12").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal90User22").val('0').trigger("liszt:updated");
            }
            var j = 0; var r = 0;
            $("table tbody tr.trdynamic").each(function (index, value) {
                $tr = $(value);
                if ($tr.find('.datepicker').val() != undefined) {
                    j++;
                    if ($tr.find('.datepicker').val() == '') {
                        //                        if (j == 1 && r == 0) {
                        //                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                        //                            alert('Please insert ' + lbl + '.');
                        //                            $("#chkcust902").prop("checked", false);
                        //                            return false;
                        //                        } else
                        //                            if (j == 2 && r == 1) {
                        //                                var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                        //                                alert('Please insert ' + lbl + '.');
                        //                                $("#chkcust902").prop("checked", false);
                        //                                return false;
                        //                            }
                        if (r == 2) {
                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                            alert('Please insert ' + lbl + '.');
                            $("#chkcust902").prop("checked", false);
                            return false;
                        }
                    }
                    r = j;
                }
            });
        });
        $(document).on('click', '#chkcust602', function () {
            if ($("#chkcust602").prop("checked") == false) {
                $("#ddlcustRemindRenewal32").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal60User12").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal60User22").val('0').trigger("liszt:updated");
            }
            var j = 0; var r = 0;
            $("table tbody tr.trdynamic").each(function (index, value) {
                $tr = $(value);
                if ($tr.find('.datepicker').val() != undefined) {
                    j++;
                    if ($tr.find('.datepicker').val() == '') {
                        //                        if (j == 1 && r == 0) {
                        //                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                        //                            alert('Please insert ' + lbl + '.');
                        //                            $("#chkcust902").prop("checked", false);
                        //                            return false;
                        //                        } else
                        //                            if (j == 2 && r == 1) {
                        //                                var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                        //                                alert('Please insert ' + lbl + '.');
                        //                                $("#chkcust902").prop("checked", false);
                        //                                return false;
                        //                            }
                        if (r == 2) {
                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                            alert('Please insert ' + lbl + '.');
                            $("#chkcust602").prop("checked", false);
                            return false;
                        }
                    }
                    r = j;
                }
            });
        });

        $(document).ready(function () {


            var k = 0;
            $("table tbody tr.trdynamic").each(function (index, value) {
                $tr = $(value);
                if ($tr.find('.datepicker').val() != undefined) {
                    var CustomReminder = '<tr class="trdynamic trimportantDate"> <td colspan="2"> <fieldset> <legend style="color: Black; font-weight: bold">Set reminder for <label id="lblRemind' + k + '" style="color: Black; font-weight: bold"></label></legend><table width="100%" style="table-layout: fixed" border="0" cellpadding="0" cellspacing="0" class="reminder"> <thead> <tr> <th width="2%"> </th><th width="22%"></th><th width="69%"></th></tr></thead><tr><td style="display: block;"><input type="checkbox" id="chkcust180' + k + '" clientidmode="Static" onclick="CheckDateStatus("Custom","ddlcustRenewal1","ddlcustRenewal180User1","ddlcustRenewal180User2","chkcust180");" /></td><td><select id="ddlcustRemindRenewal1' + k + '" onchange="ddlDateStatus("Custom","ddlcustRemindRenewal1","ddlcustRenewal180User1","ddlcustRenewal180User2","chkcust180");" clientidmode="Static" style="width: 210px" class="chzn-select chzn-select"><option value="0">-----No Reminder------</option><option value="180">Remind one eighty days before</option><option value="90">Remind ninety days before</option><option value="60">Remind sixty days before</option><option value="30">Remind thirty days before</option><option value="7">Remind seven days before</option> <option value="1">Remind one day before</option></select></td> <td><%-- User 1--%><select id="ddlcustRenewal180User1' + k + '" onchange="SetUserValue("ddlcustRenewal180User1","ddlcustRemindRenewal1","ddlcustRenewal180User2");" clientidmode="Static" style="width: 203px" class="chzn-select required chzn-select"><option></option></select>&nbsp;<%-- User 2--%><select id="ddlcustRenewal180User2' + k + '" onchange="return SetUserValue("ddlcustRenewal180User2","ddlcustRemindRenewal1","ddlcustRenewal180User1")" clientidmode="Static" style="width: 203px" class="chzn-select required chzn-select"><option></option></select></td> </tr> <%-- tr end--%>  <tr> <td style="display: block;">      <input type="checkbox" id="chkcust90' + k + '" clientidmode="Static" onclick="CheckDateStatus("Custom","ddlcustRemindRenewal2","ddlcustRenewal90User1","ddlcustRenewal90User2","chkcust90");" /> </td><td><select id="ddlcustRemindRenewal2' + k + '" onchange="ddlDateStatus("Custom","ddlcustRemindRenewal2","ddlcustRenewal90User1","ddlcustRenewal90User2","chkcust90");"  clientidmode="Static" style="width: 210px" class="chzn-select chzn-select"><option value="0">-----No Reminder------</option><option value="180">Remind one eighty days before</option><option value="90">Remind ninety days before</option><option value="60">Remind sixty days before</option><option value="30">Remind thirty days before</option>    <option value="7">Remind seven days before</option><option value="1">Remind one day before</option></select></td><td><%-- User 1--%><select id="ddlcustRenewal90User1' + k + '" onchange="SetUserValue("ddlcustRenewal90User1","ddlcustRemindRenewal2","ddlcustRenewal90User2");" clientidmode="Static" style="width: 203px" class="chzn-select required chzn-select">                 <option></option> </select>&nbsp;<%-- User 2--%><select id="ddlcustRenewal90User2' + k + '" onchange="return SetUserValue("ddlcustRenewal90User2","ddlcustRemindRenewal2","ddlcustRenewal90User1")" clientidmode="Static" style="width: 203px" class="chzn-select required chzn-select"><option></option></select></td></tr><%-- tr end--%>  <tr>                                <td style="display: block;"> <input type="checkbox" id="chkcust60' + k + '" clientidmode="Static" onclick="CheckDateStatus("Custom","ddlcustRemindRenewal3","ddlcustRenewal60User1","ddlcustRenewal60User2","chkcust60");" /> </td><td><select id="ddlcustRemindRenewal3' + k + '" onchange="ddlDateStatus("Custom","ddlcustRemindRenewal3","ddlcustRenewal60User1","ddlcustRenewal60User2","chkcust60");" clientidmode="Static" style="width: 210px" class="chzn-select chzn-select"> <option value="0">-----No Reminder------</option> <option value="180">Remind one eighty days before</option><option value="90">Remind ninety days before</option><option value="60">Remind sixty days before</option><option value="30">Remind thirty days before</option><option value="7">Remind seven days before</option>           <option value="1">Remind one day before</option> </select> </td> <td><%-- User 1--%> <select id="ddlcustRenewal60User1' + k + '" onchange="SetUserValue("ddlcustRenewal60User1","ddlcustRemindRenewal3","ddlcustRenewal60User2");" clientidmode="Static" style="width: 203px" class="chzn-select required chzn-select">            <option></option></select>&nbsp;<%-- User 2--%><select id="ddlcustRenewal60User2' + k + '" onchange="SetUserValue("ddlcustRenewal60User2","ddlcustRemindRenewal3","ddlcustRenewal60User1");"           clientidmode="Static" style="width: 203px" class="chzn-select required chzn-select"><option></option></select></td>  </tr>   </table></fieldset></td></tr>     <tr><td><br /></td></tr>';

                    $(CustomReminder).insertAfter($tr);
                    //return false;
                    k++;
                }
            });


            $.ajax({
                type: "POST",
                async: false,
                contentType: "application/json; charset=utf-8",
                url: "KeyFieldsMaster.aspx/LoadUsers",
                data: "{ContractID:'" + $('#hdnContractypeId').val() + "'}",
                dataType: "json",
                success: function (output) {
                    while (k >= 0) {
                        $('#ddlcustRenewal180User1' + k + '').empty().append('<option selected="selected" value="0">-----Select-----</option>');
                        $('#ddlcustRenewal180User2' + k + '').empty().append('<option selected="selected" value="0">-----Select-----</option>');
                        $('#ddlcustRenewal90User1' + k + '').empty().append('<option selected="selected" value="0">-----Select-----</option>');
                        $('#ddlcustRenewal90User2' + k + '').empty().append('<option selected="selected" value="0">-----Select-----</option>');
                        $('#ddlcustRenewal60User1' + k + '').empty().append('<option selected="selected" value="0">-----Select-----</option>');
                        $('#ddlcustRenewal60User2' + k + '').empty().append('<option selected="selected" value="0">-----Select-----</option>');
                        k--;
                    }
                    k = 0;
                    try {
                        while (k <= 2) {
                            $.each(output.d, function () {
                                $('#ddlcustRenewal180User1' + k + '').append($("<option></option>").val(this['Value']).html(this['Text']));
                                $('#ddlcustRenewal180User2' + k + '').append($("<option></option>").val(this['Value']).html(this['Text']));
                                $('#ddlcustRenewal90User1' + k + '').append($("<option></option>").val(this['Value']).html(this['Text']));
                                $('#ddlcustRenewal90User2' + k + '').append($("<option></option>").val(this['Value']).html(this['Text']));
                                $('#ddlcustRenewal60User1' + k + '').append($("<option></option>").val(this['Value']).html(this['Text']));
                                $('#ddlcustRenewal60User2' + k + '').append($("<option></option>").val(this['Value']).html(this['Text']));
                            });
                            k++;
                        }
                    } catch (Error) { }
                }
            });

            $("table tbody tr.trimportantDate").each(function (index, value) {
                $tr = $(value);
                if ($tr.find('label[for=time_entry_issue_id]').text() != undefined && $tr.find('label[for=time_entry_issue_id]').text() != '' && $tr.find('.datepicker').val() != undefined) {
                    if ($('#lblRemind0').text() == '') {
                        $('#lblRemind0').text($tr.find('label[for=time_entry_issue_id]').text());
                        $('#hdnReminderName1').val($tr.find('label[for=time_entry_issue_id]').text());
                    }
                    else if ($('#lblRemind1').text() == '') {
                        $('#lblRemind1').text($tr.find('label[for=time_entry_issue_id]').text());
                        $('#hdnReminderName2').val($tr.find('label[for=time_entry_issue_id]').text());
                    }
                    else if ($('#lblRemind2').text() == '') {
                        $('#lblRemind2').text($tr.find('label[for=time_entry_issue_id]').text());
                        $('#hdnReminderName3').val($tr.find('label[for=time_entry_issue_id]').text());
                    }
                }
            });


            if ($('#hdnRemindDay10').val() != "0") {
                $('#chkcust1800').prop("checked", true);
                $('#ddlcustRemindRenewal10').val($('#hdnRemindDay10').val());
                $('#ddlcustRenewal180User10').val($('#hdnRemindDay1User10').val());
                $('#ddlcustRenewal180User20').val($('#hdnRemindDay1User20').val());
            }
            if ($('#hdnRemindDay20').val() != "0") {
                $('#chkcust900').prop("checked", true);
                $('#ddlcustRemindRenewal20').val($('#hdnRemindDay20').val());
                $('#ddlcustRenewal90User10').val($('#hdnRemindDay2User10').val());
                $('#ddlcustRenewal90User20').val($('#hdnRemindDay2User20').val());
            }
            if ($('#hdnRemindDay30').val() != "0") {
                $('#chkcust600').prop("checked", true);
                $('#ddlcustRemindRenewal30').val($('#hdnRemindDay30').val());
                $('#ddlcustRenewal60User10').val($('#hdnRemindDay3User10').val());
                $('#ddlcustRenewal60User20').val($('#hdnRemindDay3User20').val());
            }

            if ($('#hdnRemindDay11').val() != "0") {
                $('#chkcust1801').prop("checked", true);
                $('#ddlcustRemindRenewal11').val($('#hdnRemindDay11').val());
                $('#ddlcustRenewal180User11').val($('#hdnRemindDay1User11').val());
                $('#ddlcustRenewal180User21').val($('#hdnRemindDay1User21').val());
            }
            if ($('#hdnRemindDay21').val() != "0") {
                $('#chkcust901').prop("checked", true);
                $('#ddlcustRemindRenewal21').val($('#hdnRemindDay21').val());
                $('#ddlcustRenewal90User11').val($('#hdnRemindDay2User11').val());
                $('#ddlcustRenewal90User21').val($('#hdnRemindDay2User21').val());
            }
            if ($('#hdnRemindDay31').val() != "0") {
                $('#chkcust601').prop("checked", true);
                $('#ddlcustRemindRenewal31').val($('#hdnRemindDay31').val());
                $('#ddlcustRenewal60User11').val($('#hdnRemindDay3User11').val());
                $('#ddlcustRenewal60User21').val($('#hdnRemindDay3User21').val());
            }

            if ($('#hdnRemindDay12').val() != "0") {
                $('#chkcust1802').prop("checked", true);
                $('#ddlcustRemindRenewal12').val($('#hdnRemindDay12').val());
                $('#ddlcustRenewal180User12').val($('#hdnRemindDay1User12').val());
                $('#ddlcustRenewal180User22').val($('#hdnRemindDay1User22').val());
            }
            if ($('#hdnRemindDay22').val() != "0") {
                $('#chkcust902').prop("checked", true);
                $('#ddlcustRemindRenewal22').val($('#hdnRemindDay22').val());
                $('#ddlcustRenewal90User12').val($('#hdnRemindDay2User12').val());
                $('#ddlcustRenewal90User22').val($('#hdnRemindDay2User22').val());
            }
            if ($('#hdnRemindDay32').val() != "0") {
                $('#chkcust602').prop("checked", true);
                $('#ddlcustRemindRenewal32').val($('#hdnRemindDay32').val());
                $('#ddlcustRenewal60User12').val($('#hdnRemindDay3User12').val());
                $('#ddlcustRenewal60User22').val($('#hdnRemindDay3User22').val());
            }
        });


        function SetUserValue(ctl1, ddl, ctl2) {
            var UserValue1 = $("#" + ctl1 + " ").val();
            var UserValue2 = $("#" + ctl2 + " ").val();
            if (UserValue1 == "0" && UserValue2 == "0") {
                $("#" + ddl + " ").val("0").trigger("liszt:updated");
            }

            if (UserValue1 != "0" || UserValue2 != "0") {
                $("#" + ctl2 + " ").closest('tr').find('div.validation').remove();
            }
            if ((UserValue1 != UserValue2) && $("#" + ddl + " ").val() != "0") {
                $("#" + ctl2 + " ").closest('tr').find('div.validation').remove();
            }
        }

        $("#btnSave").click(function (evt) {
            var flag = true; //$tr.find('div.validation').remove();
            var MultipleRem = '<div class="validation" style="margin-left: 2px;">Please select different reminder.</div>';

            $("table.reminder tbody tr").each(function (index, value) {

                $tr = $(value);
                $tr.find('div.validation').remove();
                var selectUser = '<div class="validation">Please select atleast one user.</div>';
                var selectUserAnother = '<div class="validation">Both user names should not be same.</div>';
                var selectReminder = '<div class="validation">Please select reminder.</div>';
                var MultipleReminder = '<div class="validation" style="margin-left: 2px;">Please select different reminder.</div>';
                var chk = $tr.find('input:checkbox').prop("checked");
                var SelectVal = $tr.find('select:eq(0)').val();
                var val1 = $tr.find('select:eq(1)').val();
                var val2 = $tr.find('select:eq(2)').val();
                var chkID = $tr.find('input:checkbox').attr('id');

                var ExVal0 = $tr.find('select:eq(0)').val();

                //var ctlType = chkID.indexOf('exp') > -1 ? 'Expiration' : 'Renewal';
                var ctlType = chkID.indexOf('exp') > -1 ? 'Expiration' : 'Renewal';
                if (chkID.indexOf('exp') > -1)
                    ctlType = 'Expiration'; // : 'Renewal';
                else if (chkID.indexOf('rem') > -1)
                    ctlType = 'Renewal';
                else
                    ctlType = 'Custom';

                if (!chk && (val1 != "0" && val1 != "0")) {
                    $tr.find('input:checkbox').attr("checked", "checked");
                    chk = $tr.find('input:checkbox').prop("checked");
                }
                if (chk && (val1 != "0" && val1 != "0") && ctlType == "Expiration") {
                    if ($("#txtexpirationdate").val() == "") {
                        alert("Please insert expiration date.");
                        flag = false;
                        return false;
                    }
                }
                if (chk && (val1 != "0" && val1 != "0") && ctlType == "Renewal") {
                    if ($("#txtrenewaldate").val() == "") {
                        alert("Please insert renewal date.");
                        flag = false;
                        return false;
                    }
                }
                if (chk && SelectVal == "0") {
                    $(selectReminder).insertAfter($tr.find('select:eq(0)').next('div.chzn-container-single'));
                    flag = false;
                } else
                    if (chk && (val1 == "0" && val2 == "0")) {
                        $(selectUser).insertAfter($tr.find('select:eq(2)').next('div.chzn-container-single'));
                        flag = false;
                    }
                    else if (val1 == val2 && (val1 != "0" && val1 != "0")) {
                        $(selectUserAnother).insertAfter($tr.find('select:eq(2)').next('div.chzn-container-single'));
                        flag = false;
                    }
            });
            debugger;
            var cssflag = true;
            if (($('.cssExpire2').val() != "0" || $('.cssExpire1').val() != "0") && cssflag == true) {
                if ($('.cssExpire1').val() == $('.cssExpire2').val()) {
                    $(MultipleRem).insertAfter($('.cssExpire1').next('div.chzn-container-single'));
                    $(MultipleRem).insertAfter($('.cssExpire2').next('div.chzn-container-single'));
                    flag = false; cssflag = false; //return false;
                } else if (($('.cssExpire1').val() == $('.cssExpire3').val()) && $('.cssExpire1').val() != 0) {
                    $(MultipleRem).insertAfter($('.cssExpire1').next('div.chzn-container-single'));
                    $(MultipleRem).insertAfter($('.cssExpire3').next('div.chzn-container-single'));
                    flag = false; cssflag = false; //return false;
                }
            }
            if (($('.cssExpire2').val() != "0" || $('.cssExpire3').val() != "0") && cssflag == true) {
//                if ($('.cssExpire1').val() == $('.cssExpire3').val()) {
//                    $(MultipleRem).insertAfter($('.cssExpire1').next('div.chzn-container-single'));
//                    $(MultipleRem).insertAfter($('.cssExpire3').next('div.chzn-container-single'));
//                    flag = false; cssflag = false; //return false;
//                } else
                    if ($('.cssExpire2').val() == $('.cssExpire3').val()) {
                        $(MultipleRem).insertAfter($('.cssExpire3').next('div.chzn-container-single'));
                        $(MultipleRem).insertAfter($('.cssExpire2').next('div.chzn-container-single'));
                        flag = false; cssflag = false; //return false;
                    }
            }
            if (($('.cssExpire3').val() != "0" || $('.cssExpire1').val() != "0") && cssflag == true) {
                //                if ($('.cssExpire1').val() == $('.cssExpire2').val()) {
                //                    $(MultipleRem).insertAfter($('.cssExpire1').next('div.chzn-container-single'));
                //                    $(MultipleRem).insertAfter($('.cssExpire2').next('div.chzn-container-single'));
                //                    flag = false; cssflag = false; //return false;
                //                } else 
                if (($('.cssExpire1').val() == $('.cssExpire3').val()) && $('.cssExpire1').val() != 0) {
                    $(MultipleRem).insertAfter($('.cssExpire1').next('div.chzn-container-single'));
                    $(MultipleRem).insertAfter($('.cssExpire3').next('div.chzn-container-single'));
                    flag = false; cssflag = false; //return false;
                }
            }
            cssflag = true;
            if (($('.cssRemind1').val() != "0" || $('.cssRemind2').val() != "0") && cssflag == true) {
                if ($('.cssRemind1').val() == $('.cssRemind2').val()) {
                    $(MultipleRem).insertAfter($('.cssRemind1').next('div.chzn-container-single'));
                    $(MultipleRem).insertAfter($('.cssRemind2').next('div.chzn-container-single'));
                    flag = false; cssflag = false; //return false;
                } else if (($('.cssRemind1').val() == $('.cssRemind3').val()) && $('.cssRemind1').val() != 0) {
                    $(MultipleRem).insertAfter($('.cssRemind1').next('div.chzn-container-single'));
                    $(MultipleRem).insertAfter($('.cssRemind3').next('div.chzn-container-single'));
                    flag = false; cssflag = false; //return false;
                }
            }

            if (($('.cssRemind1').val() != "0" || $('.cssRemind3').val() != "0") && cssflag == true) {
                //                if ($('.cssRemind1').val() == $('.cssRemind2').val()) {
                //                    $(MultipleRem).insertAfter($('.cssRemind1').next('div.chzn-container-single'));
                //                    $(MultipleRem).insertAfter($('.cssRemind2').next('div.chzn-container-single'));
                //                    flag = false; cssflag = false; //return false;
                //                } else 
                if (($('.cssRemind1').val() == $('.cssRemind3').val()) && $('.cssRemind1').val() != 0) {
                    $(MultipleRem).insertAfter($('.cssRemind1').next('div.chzn-container-single'));
                    $(MultipleRem).insertAfter($('.cssRemind3').next('div.chzn-container-single'));
                    flag = false; cssflag = false; //return false;
                }
            }

            if (($('.cssRemind2').val() != "0" || $('.cssRemind3').val() != "0") && cssflag == true) {
                if (($('.cssRemind1').val() == $('.cssRemind3').val()) && $('.cssRemind1').val() != 0) {
                    $(MultipleRem).insertAfter($('.cssRemind1').next('div.chzn-container-single'));
                    $(MultipleRem).insertAfter($('.cssRemind3').next('div.chzn-container-single'));
                    flag = false; cssflag = false; //return false;
                } else
                    if ($('.cssRemind2').val() == $('.cssRemind3').val()) {
                        $(MultipleRem).insertAfter($('.cssRemind2').next('div.chzn-container-single'));
                        $(MultipleRem).insertAfter($('.cssRemind3').next('div.chzn-container-single'));
                        flag = false; cssflag = false; //return false;
                    }
            }
           
            var cssCustflag = true;
            try {
                $('#hdnReminddate1').val(''); $('#hdnReminddate2').val(''); $('#hdnReminddate3').val('');
                $('#hdnRemindDay10').val('0'); $('#hdnRemindDay20').val('0'); $('#hdnRemindDay30').val('0');
                $('#hdnRemindDay11').val('0'); $('#hdnRemindDay21').val('0'); $('#hdnRemindDay31').val('0');
                $('#hdnRemindDay12').val('0'); $('#hdnRemindDay22').val('0'); $('#hdnRemindDay32').val('0');
            } catch (err) { }
            var kk = "0";
            var MultipleReminder = '<div class="validation" style="margin-left: 2px;">Please select different reminder.</div>';
            $("table tbody tr.trimportantDate").each(function (index, value) {
                $tr = $(value);
                $tr1 = $(index);

                var chk = $tr.find('input:checkbox').prop("checked");
                var RemindVal0 = $tr.find('select:eq(0)').val();
                var RemindVal1 = $tr.find('select:eq(3)').val();
                var RemindVal2 = $tr.find('select:eq(6)').val();
                var val1 = $tr.find('select:eq(1)').val();
                var val2 = $tr.find('select:eq(2)').val();
                var chkID = $tr.find('input:checkbox').attr('id');
                cssCustflag = true;
                if (RemindVal0 != 0 && RemindVal0 != undefined && cssCustflag == true) {
                    if (RemindVal0 == RemindVal1) {
                        $(MultipleReminder).insertAfter($tr.find('select:eq(0)').next('div.chzn-container-single'));
                        $(MultipleReminder).insertAfter($tr.find('select:eq(3)').next('div.chzn-container-single'));
                        flag = false; cssCustflag = false; //return false;
                    }
                    if (RemindVal0 == RemindVal2) {
                        $(MultipleReminder).insertAfter($tr.find('select:eq(0)').next('div.chzn-container-single'));
                        $(MultipleReminder).insertAfter($tr.find('select:eq(6)').next('div.chzn-container-single'));
                        flag = false; cssCustflag = false; //return false;
                    }
                }

                if (RemindVal1 != 0 && RemindVal1 != undefined && cssCustflag == true) {
                    if (RemindVal1 == RemindVal2) {
                        $(MultipleReminder).insertAfter($tr.find('select:eq(3)').next('div.chzn-container-single'));
                        $(MultipleReminder).insertAfter($tr.find('select:eq(6)').next('div.chzn-container-single'));
                        flag = false; cssCustflag = false; //return false;
                    }
                    if (RemindVal1 == RemindVal0) {
                        $(MultipleReminder).insertAfter($tr.find('select:eq(0)').next('div.chzn-container-single'));
                        $(MultipleReminder).insertAfter($tr.find('select:eq(3)').next('div.chzn-container-single'));
                        flag = false; cssCustflag = false; //return false;
                    }
                }

                if (RemindVal2 != 0 && RemindVal2 != undefined && cssCustflag == true) {
                    if (RemindVal1 == RemindVal2) {
                        $(MultipleReminder).insertAfter($tr.find('select:eq(3)').next('div.chzn-container-single'));
                        $(MultipleReminder).insertAfter($tr.find('select:eq(6)').next('div.chzn-container-single'));
                        flag = false; cssCustflag = false; //return false;
                    }
                    if (RemindVal2 == RemindVal0) {
                        $(MultipleReminder).insertAfter($tr.find('select:eq(0)').next('div.chzn-container-single'));
                        $(MultipleReminder).insertAfter($tr.find('select:eq(6)').next('div.chzn-container-single'));
                        flag = false; cssCustflag = false; //return false;
                    }
                }


                if ($tr.find('.datepicker').val() != undefined) {
                    if ($tr.find('.datepicker').val() != '' && kk == 0)
                        $('#hdnReminddate1').val($tr.find('.datepicker').val());

                    if ($tr.find('.datepicker').val() != '' && kk == 1)
                        $('#hdnReminddate2').val($tr.find('.datepicker').val());

                    if ($tr.find('.datepicker').val() != '' && kk == 2)
                        $('#hdnReminddate3').val($tr.find('.datepicker').val());

                    kk++;
                }

            });
            kk = 0;

            //            alert($('#hdnReminddate1').val()); alert($('#hdnReminddate2').val()); alert($('#hdnReminddate3').val());
            //return false;

            k = 0;
            while (k <= 2) {
                $("table tbody tr.trimportantDate").each(function (index, value) {
                    $tr = $(value);
                    //if ($tr.find('.datepicker').val() != undefined) {
                    //alert($tr.find('.datepicker').val());        //$tr.find('input:checkbox').prop("checked");
                    if ($tr.find('#chkcust180' + k + '').prop("checked")) {

                        //if ($('#hdnRemindDay10').val() == '0' && k == 0) {
                        if (k == 0) {
                            $('#hdnRemindDay10').val($tr.find('#ddlcustRemindRenewal1' + k + '').val());
                        }

                        //if ($('#hdnRemindDay11').val() == '0' && k == 1) {
                        if (k == 1) {
                            $('#hdnRemindDay11').val($tr.find('#ddlcustRemindRenewal1' + k + '').val());
                        }

                        //if ($('#hdnRemindDay12').val() == '0' && k == 2) {
                        if (k == 2) {
                            $('#hdnRemindDay12').val($tr.find('#ddlcustRemindRenewal1' + k + '').val());
                        }

                        /* User record for first date reminder  */
                        //if ($('#hdnRemindDay1User10').val() == '0' && k == 0)
                        if (k == 0)
                            $('#hdnRemindDay1User10').val($tr.find('#ddlcustRenewal180User1' + k + '').val());

                        //if ($('#hdnRemindDay1User20').val() == '0' && k == 0)
                        if (k == 0)
                            $('#hdnRemindDay1User20').val($tr.find('#ddlcustRenewal180User2' + k + '').val());

                        /* User record for second date reminder  */
                        //if ($('#hdnRemindDay1User11').val() == '0' && k == 1)
                        if (k == 1)
                            $('#hdnRemindDay1User11').val($tr.find('#ddlcustRenewal180User1' + k + '').val());

                        //if ($('#hdnRemindDay1User21').val() == '0' && k == 1)
                        if (k == 1)
                            $('#hdnRemindDay1User21').val($tr.find('#ddlcustRenewal180User2' + k + '').val());

                        /* User record for third date reminder  */
                        //if ($('#hdnRemindDay1User12').val() == '0' && k == 2)
                        if (k == 2)
                            $('#hdnRemindDay1User12').val($tr.find('#ddlcustRenewal180User1' + k + '').val());

                        //if ($('#hdnRemindDay1User22').val() == '0' && k == 2)
                        if (k == 2)
                            $('#hdnRemindDay1User22').val($tr.find('#ddlcustRenewal180User2' + k + '').val());

                    }
                    if ($tr.find('#chkcust90' + k + '').prop("checked")) {

                        //if ($('#hdnRemindDay20').val() == '0' && k == 0) {
                        if (k == 0) {
                            $('#hdnRemindDay20').val($tr.find('#ddlcustRemindRenewal2' + k + '').val());
                        }

                        //if ($('#hdnRemindDay21').val() == '0' && k == 1) {
                        if (k == 1) {
                            $('#hdnRemindDay21').val($tr.find('#ddlcustRemindRenewal2' + k + '').val());
                        }

                        //if ($('#hdnRemindDay22').val() == '0' && k == 2) {
                        if (k == 2) {
                            $('#hdnRemindDay22').val($tr.find('#ddlcustRemindRenewal2' + k + '').val());
                        }

                        /* User record for first date reminder  */
                        //if ($('#hdnRemindDay2User10').val() == '0' && k == 0)
                        if (k == 0)
                            $('#hdnRemindDay2User10').val($tr.find('#ddlcustRenewal90User1' + k + '').val());

                        //if ($('#hdnRemindDay2User20').val() == '0' && k == 0)
                        if (k == 0)
                            $('#hdnRemindDay2User20').val($tr.find('#ddlcustRenewal90User2' + k + '').val());

                        /* User record for second date reminder  */
                        //if ($('#hdnRemindDay2User11').val() == '0' && k == 1)
                        if (k == 1)
                            $('#hdnRemindDay2User11').val($tr.find('#ddlcustRenewal90User1' + k + '').val());

                        //if ($('#hdnRemindDay2User21').val() == '0' && k == 1)
                        if (k == 1)
                            $('#hdnRemindDay2User21').val($tr.find('#ddlcustRenewal90User2' + k + '').val());

                        /* User record for third date reminder  */
                        //if ($('#hdnRemindDay2User12').val() == '0' && k == 2)
                        if (k == 2)
                            $('#hdnRemindDay2User12').val($tr.find('#ddlcustRenewal90User1' + k + '').val());

                        //if ($('#hdnRemindDay2User22').val() == '0' && k == 2)
                        if (k == 2)
                            $('#hdnRemindDay2User22').val($tr.find('#ddlcustRenewal90User2' + k + '').val());



                    }
                    if ($tr.find('#chkcust60' + k + '').prop("checked")) {

                        //if ($('#hdnRemindDay30').val() == '0' && k == 0) {
                        if (k == 0) {
                            $('#hdnRemindDay30').val($tr.find('#ddlcustRemindRenewal3' + k + '').val());
                        }

                        //if ($('#hdnRemindDay31').val() == '0' && k == 1) {
                        if (k == 1) {
                            $('#hdnRemindDay31').val($tr.find('#ddlcustRemindRenewal3' + k + '').val());
                        }

                        //if ($('#hdnRemindDay32').val() == '0' && k == 2) {
                        if (k == 2) {
                            $('#hdnRemindDay32').val($tr.find('#ddlcustRemindRenewal3' + k + '').val());
                        }

                        /* User record for first date reminder  */
                        //if ($('#hdnRemindDay3User10').val() == '0' && k == 0)
                        if (k == 0)
                            $('#hdnRemindDay3User10').val($tr.find('#ddlcustRenewal60User1' + k + '').val());

                        //if ($('#hdnRemindDay3User20').val() == '0' && k == 0)
                        if (k == 0)
                            $('#hdnRemindDay3User20').val($tr.find('#ddlcustRenewal60User2' + k + '').val());

                        /* User record for second date reminder  */
                        //if ($('#hdnRemindDay3User11').val() == '0' && k == 1)
                        if (k == 1)
                            $('#hdnRemindDay3User11').val($tr.find('#ddlcustRenewal60User1' + k + '').val());

                        //if ($('#hdnRemindDay3User21').val() == '0' && k == 1)
                        if (k == 1)
                            $('#hdnRemindDay3User21').val($tr.find('#ddlcustRenewal60User2' + k + '').val());

                        /* User record for third date reminder  */
                        //if ($('#hdnRemindDay3User12').val() == '0' && k == 2)
                        if (k == 2)
                            $('#hdnRemindDay3User12').val($tr.find('#ddlcustRenewal60User1' + k + '').val());

                        //if ($('#hdnRemindDay3User22').val() == '0' && k == 2)
                        if (k == 2)
                            $('#hdnRemindDay3User22').val($tr.find('#ddlcustRenewal60User2' + k + '').val());


                    }
                    // }
                });
                k++;
            }
            //            alert($('#hdnRemindDay1User10').val());
            //return false;
            return flag;
        });

        function AvoidReadonly(ctl) {

            if (ctl == 1) {
                if ($("#chkIsTerminationConvenience").is(':checked'))
                    $('#txtTerminationConvenience').prop('readonly', false);
                else
                    $('#txtTerminationConvenience').prop('readonly', true);
            }

            if (ctl == 2) {
                if ($("#chkIsTerminationmaterial").is(':checked'))
                    $('#txtTerminationmaterial').prop('readonly', false);
                else
                    $('#txtTerminationmaterial').prop('readonly', true);
            }

            if (ctl == 3) {
                if ($("#chkIsTerminationinsolvency").is(':checked'))
                    $('#txtTerminationinsolvency').prop('readonly', false);
                else
                    $('#txtTerminationinsolvency').prop('readonly', true);
            }

            if (ctl == 4) {
                if ($("#chkIsTerminationinChangeofcontrol").is(':checked'))
                    $('#txtTerminationinChangeofcontrol').prop('readonly', false);
                else
                    $('#txtTerminationinChangeofcontrol').prop('readonly', true);
            }
        }

        function CheckDateStatus(ctl, ctl2, CtlU1, CtlU2, CtlChk) {
            var flag = true;
            var ddlValue = $("#" + ctl2 + " ").val();
            //alert(ddlValue);
            if (ctl == "Renewal") {
                if ($("#txtrenewaldate").val() == "") {
                    alert('Please insert renewal date ');
                    $('#ddlRemindRenewal1').val("0").trigger("liszt:updated");
                    $('#ddlRemindRenewal2').val("0").trigger("liszt:updated");
                    $('#ddlRemindRenewal3').val("0").trigger("liszt:updated");
                    flag = false;
                }
            }
            else if (ctl == "Expiration") {
                if ($("#txtexpirationdate").val() == "") {
                    alert('Please insert expiration date ');
                    $('#ddlRemindExpiration3').val("0").trigger("liszt:updated");
                    $('#ddlRemindExpiration2').val("0").trigger("liszt:updated");
                    $('#ddlRemindExpiration1').val("0").trigger("liszt:updated");
                    flag = false;
                }
            }

            if ($("#" + CtlChk + " ").prop("checked") == false) {
                $("#" + ctl2 + " ").val('0').trigger("liszt:updated");
                $("#" + CtlU1 + " ").val('0').trigger("liszt:updated");
                $("#" + CtlU2 + " ").val('0').trigger("liszt:updated");
            }
            return flag;
        }

        function ddlDateStatus(ctl, ctl2, CtlU1, CtlU2, CtlChk) {
            debugger;
            var flag = true;
            var ddlValue = $("#" + ctl2 + " ").val();
            //alert(ddlValue)
            if (ddlValue == '0') {
                $("#" + CtlU1 + " ").val('0').trigger("liszt:updated");
                $("#" + CtlU2 + " ").val('0').trigger("liszt:updated");
                $("#" + CtlChk + " ").prop('checked', false);
            }
            return flag;
        }
        
  
    </script>
    <asp:HiddenField ID="HdnRequestID" runat="server" ClientIDMode="Static" Value="0" />
    <asp:HiddenField ID="hdnContractypeId" runat="server" ClientIDMode="Static" Value="0" />
    <asp:HiddenField ID="hdnContractID" runat="server" />
    <asp:HiddenField ID="hdnUserStatus" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnLinkStatus" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnReminderName1" runat="server" ClientIDMode="Static" Value="" />
    <asp:HiddenField ID="hdnReminderName2" runat="server" ClientIDMode="Static" Value="" />
    <asp:HiddenField ID="hdnReminderName3" runat="server" ClientIDMode="Static" Value="" />
    <div style="display: none">
        <asp:Button ID="btnChangeUser" runat="server" ClientIDMode="Static" OnClick="btnChangeUser_Click" />
    </div>
</asp:Content>
