﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true" CodeFile="CheckOutAndSendEmail.aspx.cs" Inherits="WorkFlow_CheckOutAndSendEmail" %>
<%@ Register Src="../UserControl/requestnewlinks.ascx" TagName="requestnewlinks"
    TagPrefix="uc2" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
 <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/mastervalidations.js" type="text/javascript"></script>
    <link href="../Styles/application.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
     
            $("#requestLink").addClass("menulink");
            $("#requesttab").addClass("selectedtab");
            $('textarea').autogrow();
        });
</script>
 <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>

    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
    <h2>Send email for contract <%=ViewState["ContractId"].ToString()%></h2>
    <div>    
        <p>
            <a href='RequestFlow.aspx?Status=Workflow&RequestId=<%=ViewState["RequestId"].ToString() %>' class="issue status-1 priority-4 parent">Request #<%=ViewState["RequestId"].ToString() %></a>:
            Send email for contract <%=ViewState["ContractId"].ToString()%>
        </p>
    
</div>   
     <div id="Emailbox" style="margin-left:0px">
                <div class="box" id="Div12">
                    <a name="Emailboxname"></a>
                    <label for="issue_tracker_id" style="color:Red;">Note : Use comma(,) to separate multiple Email Ids.</label>
                    <fieldset class="tabular" style="border:0">
                        <legend></legend> 
                       
                        <p>
                            <label for="issue_tracker_id"><span class="required">*</span> To Email IDs : </label>
                            <input id="txtMailTo"  type="text" value="" class="EmailList required" clientidmode="Static" runat="server" style="width:500px;"/>
                        </p>
                        <p>
                            <label for="issue_tracker_id">Cc Email IDs : </label>
                            <input id="txtMailCC" type="text" value="" class="EmailList" clientidmode="Static" runat="server" style="width:500px;"/>
                        </p>
                        <p>
                            <label for="issue_tracker_id"><span class="required">*</span> Subject : </label>
                            <input id="txtMailSubject" type="text" value="" class="required" clientidmode="Static" runat="server" style="width:500px;"/>
                        </p>
                        <p>
                            <label for="issue_tracker_id"><span class="required">*</span> Body Text : </label>
                            <textarea id="txtMailBody" class="required" clientidmode="Static" 
                                runat="server"></textarea>
                        </p>
                        <p>
                            <label for="issue_tracker_id">Attachment : 
                            </label>
                            <a id="aFileName" runat="server" class="icon icon-attachment" style="text-decoration:none"  >
                               
                            </a>
                            <span id="spanMSWordFile" runat="server" clientidmode="Static">
                                <asp:CheckBox ID="chkWordFile" ClientIDMode="Static" runat="server" />
                                <a title="MSWord file" id="aMSWordFile" runat="server" clientidmode="Static">
                                    <img alt="MSWord file" src="../images/icon-word.jpg" />
                                </a>
                            </span>&nbsp;
                            <span id="spanPDFFile" runat="server" clientidmode="Static">
                                <asp:CheckBox ID="chkPDFFile" ClientIDMode="Static" runat="server" />
                                <a title="PDF file" id="aPDFFile" runat="server" clientidmode="Static" target="_blank" >
                                    <img alt="PDF file" src="../images/icon-pdf.jpg" />
                                </a>
                            </span>
                        </p>
                    </fieldset>
                </div>
            <asp:Button ID="btnSendMail" runat="server" Text="Send Mail" class="btn_validate"  OnClick="btnSendMail_Click" ClientIDMode="Static" />
            <asp:Button ID="btnBack" runat="server" Text="Back" onclick="btnBack_Click" />
            </div>
    
    <script type="text/javascript">
        $(window).load(function () {
            $("#btnSendMail").click(function () {
                if ($(".tool_tip:visible, .tooltip_outer:visible").length == 0) {
                    ShowProgress();
                    return true;
                }
                else {
                    return false;
                }
            });
        });

      
    </script>
</asp:Content>

