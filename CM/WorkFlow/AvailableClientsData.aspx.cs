﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WorkflowBLL;
using CommonBLL;

public partial class WorkFlow_AvailableClientsData : System.Web.UI.Page
{
    // navigation//
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 10;
    // navigation//

    IAvailableClient objAvailableClient;
    public string Add;
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        Session["MasterSeelcted"] = null;
        Session["ChlidSeelcted"] = null;

        if (Session[Declarations.User] != null && !IsPostBack)
        {

            Access.PageAccess(this, Session[Declarations.User].ToString(), "WorkFlow_");
            ViewState[Declarations.Add] = Access.Add.Trim();
            ViewState[Declarations.View] = Access.View.Trim();
            ViewState[Declarations.Delete] = Access.Delete.Trim();
            ViewState[Declarations.Update] = Access.Update.Trim();
        }

        setAccessValues();
        if (!IsPostBack || hdnSearch.Value.Trim() != string.Empty)
        {
            Session[Declarations.SortControl] = null;

            ReadData(1, RecordsPerPage);
            Message();
        }
        else
        {
            SetNavigationButtonParameters();
        }
        AccessVisibility();
    }
    protected void btnSort_Click(object sender, EventArgs e)
    {
        hdnSearch.Value = "";
        LinkButton clickBtn = (LinkButton)sender;
        Session.Add(Declarations.SortControl, clickBtn);
        ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
    }

    void Message()
    {
        if (Session[Declarations.Message] != null)
        {
            var flg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }

    }
    #region Navigation
    void SetNavigationButtonParameters()
    {
        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
            AccessVisibility();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

    }
    #endregion
    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objAvailableClient = FactoryWorkflow.GetAvailableClientDetails();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Delete starts.");
        string status = "";
        objAvailableClient.ClientIDs = hdnPrimeIds.Value;
        try
        {
            status = objAvailableClient.DeleteRecord();
        }
        catch (Exception ex)
        {
            hdnPrimeIds.Value = string.Empty;
            Page.JavaScriptClientScriptBlock("status", "PageGetMessage('DF')");
            Page.TraceWarn("Delete fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        hdnPrimeIds.Value = string.Empty;
        Page.JavaScriptClientScriptBlock("status", "PageGetMessage('DS')");
        Page.TraceWrite("Delete ends.");
        Server.Transfer("AvailableClientsData.aspx");

    }

    void ReadData(int pageNo, int recordsPerPage)
    {
        if (View == "Y")
        {
            Page.TraceWrite("ReadData starts.");
            try
            {
                objAvailableClient.PageNo = pageNo;
                objAvailableClient.RecordsPerPage = recordsPerPage;
                objAvailableClient.Search = txtSearch.Value.Trim();
                LinkButton btnSort = null;
                if (Session[Declarations.SortControl] != null && Session[Declarations.SortControl] != string.Empty)
                {
                    btnSort = (LinkButton)Session[Declarations.SortControl];
                    objAvailableClient.Direction = btnSort.CssClass.ToLower() == "sort desc" ? 1 : 0;
                    objAvailableClient.SortColumn = btnSort.Text.Trim();
                }
                rptClientsMaster.DataSource = objAvailableClient.ReadData();
                rptClientsMaster.DataBind();
                if (btnSort != null)
                {
                    rptClientsMaster.ClassChange(btnSort);
                }
                Page.TraceWrite("ReadData ends.");
                ViewState[Declarations.TotalRecord] = objAvailableClient.TotalRecords;
                PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
                PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
                PaginationButtons1.RecordsPerPage = RecordsPerPage;
            }
            catch (Exception ex)
            {
                Page.TraceWarn("ReadData fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

    }

    void loadPagination()
    {
        UserControl_PaginationButtons ucSimpleControl = LoadControl("../UserControl/Requestdetaillinks.ascx") as UserControl_PaginationButtons;
        Placeholder1.Controls.Add(ucSimpleControl);

    }

    protected void btnAddRecord_Click(object sender, EventArgs e)
    {
        Server.Transfer("AvailableMultiClientsMaster.aspx");
    }
    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }
    void AccessVisibility()
    {
       
        Page.TraceWrite("AccessVisibility starts.");
        if (View == "N")
        {
            Page.extDisableControls();
        }
        else
        {
            btnSearch.Enabled = true;
            txtSearch.Disabled = false;
        }

        if (Update == "N")
        {
            foreach (RepeaterItem item in rptClientsMaster.Items)
            {
                LinkButton imgEdit = (LinkButton)item.FindControl("imgEdit");
                imgEdit.Enabled = false;
               
            }
            btnDelete.Enabled = false;
  
        }
        if (Add == "N")
        {
            Page.extDisableControls();
            btnAddRecord.Enabled = false;
            btnAddRecord.Visible = false;
        }
        else
        {
            btnAddRecord.Enabled = true;
        }

        if (Delete == "N")
        {
            btnDelete.Enabled = false;
            btnDelete.OnClientClick = null;
            btnDelete.Visible = false;
        }
        else if (Delete == "Y")
        {
            btnDelete.Enabled = true;
        }

        if (View == "Y")
        {
            txtSearch.Disabled = false;
            btnSearch.Enabled = true;
            btnShowAll.Enabled = true;
        }


        Page.TraceWrite("AccessVisibility starts.");
    }
    protected void rptClientsMaster_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Update")
        {
            Label lblIsUsed = (Label)e.Item.FindControl("lblIsUsed");
            Server.Transfer("AvailableMultiClientsMaster.aspx?Isused=" + lblIsUsed.Text + "");
        }
    }
}