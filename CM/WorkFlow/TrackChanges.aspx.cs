﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;
//using WorkflowBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;
using System.Web.Security;
using UserManagementBLL;
using System.Web.Services;
using ClauseLibraryBLL;
using DiffPlex;
using Aspose.Words;
using System.Text;
using DocumentOperationsBLL;

public partial class Negotiation_TrackChanges : System.Web.UI.Page
{
    ITrackDocumentChanges objTrackDocChanges;
    string lDIR = "";// @"~/Uploads/" +HttpContext.Current.Session["TenantDIR"] + "/Contract Documents/";

    string FileExtnsn = ".docx";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        lDIR = @"~/Uploads/" + HttpContext.Current.Session["TenantDIR"] + "/Contract Documents/";

            if (!IsPostBack)
            {
                string lRequestID = Request.QueryString["rid"].ToString();
                hdnRequestID.Value = lRequestID;
                string script = "$(document).ready(function () { $('[id*=btnSubmit]').click(); });";
                ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);
            }      
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objTrackDocChanges = FactoryAssembly.GetTrackDocumentChangesObject();           
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    

    public void CompareDataInWordDoc(string RequestID)
    {
        Page.TraceWrite("CompareDataInWordDoc starts.");
        try
        {
            objTrackDocChanges.RequestID = Convert.ToInt32(RequestID);


            string lUserID = Session[Declarations.User].ToString(); ;

            string lIpAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (lIpAddress == "" || lIpAddress == null)
                lIpAddress = Request.ServerVariables["REMOTE_ADDR"];


            DataTable dtDocumentDetails = objTrackDocChanges.GetContractFileDetails();

            if (dtDocumentDetails.Rows.Count == 2)
            {

                lDIR = @"~/Uploads/" + Session["TenantDIR"] + "/Contract Documents/";
                //string lOldFileName = Server.MapPath(lDIR) + dtDocumentDetails.Rows[1]["FileName"].ToString()+FileExtnsn;
                string lOldFileName = Server.MapPath(lDIR) + dtDocumentDetails.Rows[1]["FilePath"].ToString() + FileExtnsn;

                string lNewFileName = Server.MapPath(lDIR) + dtDocumentDetails.Rows[0]["FilePath"].ToString() + FileExtnsn;                

                string lContractFileOldId = dtDocumentDetails.Rows[1]["ContractFileId"].ToString();
                string lContractFileNewId = dtDocumentDetails.Rows[0]["ContractFileId"].ToString();

                hdnContractFileOldId.Value = lContractFileOldId;
                hdnContractFileNewId.Value = lContractFileNewId;
              

                objTrackDocChanges.ContractFileOldId = Convert.ToInt32(lContractFileOldId);
                objTrackDocChanges.ContractFileNewId = Convert.ToInt32(lContractFileNewId);             


                if (!objTrackDocChanges.IsTrackedChangesAlreadySaved())
                {

                    DataSet dsFieldAndClauseDetails = objTrackDocChanges.GetFieldLibraryAndClausesDetails();
                    DataTable dtFieldsDet = dsFieldAndClauseDetails.Tables[0];
                    DataTable dtClauseFieldsDet = dsFieldAndClauseDetails.Tables[1];
                    DataTable dtClauseDet = dsFieldAndClauseDetails.Tables[2];

                    Aspose.Words.Document docOld1 = new Aspose.Words.Document(lOldFileName);
                    SaveDummyFileWithouHeaderFooter(docOld1, false);
                    Aspose.Words.Document docNew1 = new Aspose.Words.Document(lNewFileName);
                    SaveDummyFileWithouHeaderFooter(docNew1, true);

                    objTrackDocChanges.dtTrackLineChanges = new DataTable();
                    objTrackDocChanges.dtTrackLineChanges = objTrackDocChanges.DefineTrackChangesTable().Clone();

                    objTrackDocChanges.dtTrackClauseChanges = new DataTable();
                    objTrackDocChanges.dtTrackClauseChanges = objTrackDocChanges.DefineTrackChangesTable().Clone();

                    objTrackDocChanges.dtTrackFieldChanges = new DataTable();
                    objTrackDocChanges.dtTrackFieldChanges = objTrackDocChanges.DefineTrackChangesTable().Clone();

                  

                    #region Tracking Field Changes Logic
                   
                    //for (int i = 0; i < dtFieldsDet.Rows.Count; i++)
                    //{

                    //    string lFieldOldValue = "";
                    //    string lFieldNewValue = "";
                    //    string lModificationStatus = "";


                    //    for (int j = 0; j < docOld.Range.Bookmarks.Count; j++)
                    //    {
                    //        string bName = docOld.Range.Bookmarks[j].Name;
                    //        string lFieldName = bName.Split('_')[1];
                    //        if (lFieldName.Equals(Convert.ToString(dtFieldsDet.Rows[i]["FieldName"]).Trim()))
                    //        {
                    //            lFieldOldValue = docOld.Range.Bookmarks[bName].Text.Trim();
                    //            if (docNew.Range.Bookmarks[bName] != null)
                    //            {
                    //                lFieldNewValue = docNew.Range.Bookmarks[bName].Text;
                    //            }
                    //            else
                    //            {
                    //                lFieldNewValue = "";
                    //            }


                    //            var sdField = new DiffPlex.DiffBuilder.SideBySideDiffBuilder(d);
                    //            var sdFieldResult = sdField.BuildDiffModel(lFieldOldValue.ToString(), lFieldNewValue);

                    //            lModificationStatus = "U";
                    //            if (sdFieldResult.OldText.Lines[0].Type.ToString().Trim().Equals("Modified"))
                    //            {
                    //                lModificationStatus = "M";
                    //                lFieldNewValue = "<font color='red'>" + lFieldNewValue + "</font>";
                    //            }
                    //            else if (sdFieldResult.OldText.Lines[0].Type.ToString().Trim().Equals("Deleted"))
                    //            {
                    //                lModificationStatus = "D";
                    //                lFieldNewValue = "<font color='red'>" + lFieldNewValue + "</font>";
                    //            }

                    //            if (!lModificationStatus.Equals("U"))
                    //            {
                    //                DataRow drTrackFields = objTrackDocChanges.dtTrackFieldChanges.NewRow();
                    //                drTrackFields["ContractFileOldId"] = lContractFileOldId;
                    //                drTrackFields["ContractFileNewId"] = lContractFileNewId;
                    //                drTrackFields["FieldLibraryID"] = Convert.ToString(dtFieldsDet.Rows[i]["FieldLibraryID"]);
                    //                drTrackFields["OldFileFieldValue"] = GetFormatedString(lFieldOldValue);
                    //                drTrackFields["NewFileFieldValue"] = GetFormatedString(lFieldNewValue);
                    //                drTrackFields["ModificationStatus"] = lModificationStatus;
                    //                drTrackFields["BookmarkName"] = bName;
                    //                drTrackFields["AddedBy"] = lUserID;
                    //                drTrackFields["IpAddress"] = lIpAddress;

                    //                objTrackDocChanges.dtTrackFieldChanges.Rows.Add(drTrackFields);
                    //            }
                    //        }
                    //    }
                    //} 
                    #endregion
                    #region Tracking ClauseField Changes Logic
                    //for (int i = 0; i < dtClauseFieldsDet.Rows.Count; i++)
                    //{

                    //    string lFieldOldValue = "";
                    //    string lFieldNewValue = "";
                    //    string lModificationStatus = "";

                    //    for (int j = 0; j < docOld.Range.Bookmarks.Count; j++)
                    //    {
                    //        string bName = docOld.Range.Bookmarks[j].Name;

                    //        int lCount = bName.Count(f => f == '_');

                    //        string lFieldID = "";
                    //        string lCombinedName = "";

                    //        if (lCount > 2)
                    //        {
                    //            string lTemp = bName.Split('_')[1];
                    //            string lFieldName = bName.Split('_')[2];

                    //            lFieldID = bName.Split('_')[3];
                    //            lCombinedName = lFieldName + lFieldID;

                    //            if (lCombinedName.Equals(Convert.ToString(dtClauseFieldsDet.Rows[i]["FieldName"]).Trim()))
                    //            {
                    //                lFieldOldValue = docOld.Range.Bookmarks[bName].Text.Trim();

                    //                if (docNew.Range.Bookmarks[bName] != null)
                    //                {
                    //                    lFieldNewValue = docNew.Range.Bookmarks[bName].Text;
                    //                }
                    //                else
                    //                {
                    //                    lFieldNewValue = "";
                    //                }

                    //                var sdField = new DiffPlex.DiffBuilder.SideBySideDiffBuilder(d);
                    //                var sdFieldResult = sdField.BuildDiffModel(lFieldOldValue.ToString(), lFieldNewValue);

                    //                lModificationStatus = "U";
                    //                if (sdFieldResult.OldText.Lines[0].Type.ToString().Trim().Equals("Modified"))
                    //                {
                    //                    lModificationStatus = "M";
                    //                    lFieldNewValue = "<font color='red'>" + lFieldNewValue + "</font>";
                    //                }
                    //                else if (sdFieldResult.OldText.Lines[0].Type.ToString().Trim().Equals("Deleted"))
                    //                {
                    //                    lModificationStatus = "D";
                    //                    lFieldNewValue = "<font color='red'>" + lFieldNewValue + "</font>";
                    //                }



                    //                if (!lModificationStatus.Equals("U"))
                    //                {
                    //                    DataRow drTrackClauseFields = objTrackDocChanges.dtTrackFieldChanges.NewRow();
                    //                    drTrackClauseFields["ContractFileOldId"] = lContractFileOldId;
                    //                    drTrackClauseFields["ContractFileNewId"] = lContractFileNewId;
                    //                    drTrackClauseFields["FieldLibraryID"] = Convert.ToString(dtClauseFieldsDet.Rows[i]["FieldLibraryID"]);
                    //                    drTrackClauseFields["ClauseID"] = Convert.ToString(dtClauseFieldsDet.Rows[i]["ClauseID"]);
                    //                    drTrackClauseFields["OldFileFieldValue"] = GetFormatedString(lFieldOldValue);
                    //                    drTrackClauseFields["NewFileFieldValue"] = GetFormatedString(lFieldNewValue);
                    //                    drTrackClauseFields["ModificationStatus"] = lModificationStatus;
                    //                    drTrackClauseFields["BookmarkName"] = bName;
                    //                    drTrackClauseFields["AddedBy"] = lUserID;
                    //                    drTrackClauseFields["IpAddress"] = lIpAddress;
                    //                    objTrackDocChanges.dtTrackFieldChanges.Rows.Add(drTrackClauseFields);
                    //                }

                    //            }
                    //        }
                    //    }
                    //} 
                    #endregion
                    #region Tracking Clause Changes logic

                    //for (int Rowcnt = 0; Rowcnt < dtClauseDet.Rows.Count; Rowcnt++)
                    //{
                    //    string lClauseID = dtClauseDet.Rows[Rowcnt]["ClauseID"].ToString();
                    //    string lClauseBookmarkName = "_Clause_" + lClauseID;

                    //    Aspose.Words.Bookmark bmOldClause = docOld.Range.Bookmarks[lClauseBookmarkName];
                    //    string ClauseOldText = bmOldClause.Text;

                    //    Aspose.Words.Bookmark bmNewClause = docNew.Range.Bookmarks[lClauseBookmarkName];
                    //    string ClauseNewText = bmNewClause.Text;

                    //    string lModificationStatus = "U";
                    //    if (ClauseOldText.Trim().Length > 0 && ClauseNewText.Trim().Length <= 0)
                    //        lModificationStatus = "D";

                    //    var sdClause = new DiffPlex.DiffBuilder.SideBySideDiffBuilder(d);
                    //    var sdClauseResult = sdClause.BuildDiffModel(ClauseOldText.ToString(), ClauseNewText);


                    //    //for (int i = 0; i < sdClauseResult.OldText.Lines.Count; i++)
                    //    //{
                    //    //    lModificationStatus = "M";

                    //    //     if (sdClauseResult.OldText.Lines[i].SubPieces.Count > 0)
                    //    //    {
                    //    //        for (int j = 0; j < sdClauseResult.OldText.Lines[i].SubPieces.Count; j++)
                    //    //        {
                    //    //            if (sdClauseResult.OldText.Lines[i].SubPieces[j].Type == DiffPlex.DiffBuilder.Model.ChangeType.Deleted)
                    //    //            {
                    //    //                lInnerHTML += "<font style='BACKGROUND-COLOR: red' color='black'><strike>" + sdClauseResult.OldText.Lines[i].SubPieces[j].Text + "</strike></font>";
                    //    //            }
                    //    //            else
                    //    //                if (sdClauseResult.OldText.Lines[i].SubPieces[j].Type == DiffPlex.DiffBuilder.Model.ChangeType.Inserted)
                    //    //                {
                    //    //                    lInnerHTML += "<font style='BACKGROUND-COLOR: yellow' color='blue'>" + sdClauseResult.OldText.Lines[i].SubPieces[j].Text + "</font>";
                    //    //                }
                    //    //                else
                    //    //                    if (sdClauseResult.OldText.Lines[i].SubPieces[j].Type == DiffPlex.DiffBuilder.Model.ChangeType.Modified)
                    //    //                    {
                    //    //                        lInnerHTML += "<font style='BACKGROUND-COLOR: yellow' color='yellow'>" + sdClauseResult.OldText.Lines[i].SubPieces[j].Text + "</font>";
                    //    //                    }
                    //    //                    else
                    //    //                        lInnerHTML += sdClauseResult.OldText.Lines[i].SubPieces[j].Text;
                    //    //        }
                    //    //    }
                    //    //    else
                    //    //    {
                    //    //        lInnerHTML += sdClauseResult.OldText.Lines[i].Text;
                    //    //    }
                    //    //}




                    //    string lNewClauseText = "";

                    //    for (int i = 0; i < sdClauseResult.NewText.Lines.Count; i++)
                    //    {
                    //        if (sdClauseResult.NewText.Lines[i].SubPieces.Count > 0)
                    //        {
                    //            for (int j = 0; j < sdClauseResult.NewText.Lines[i].SubPieces.Count; j++)
                    //            {
                    //                if (sdClauseResult.NewText.Lines[i].SubPieces[j].Type == DiffPlex.DiffBuilder.Model.ChangeType.Deleted)
                    //                {
                    //                    lNewClauseText += "<font  color='red'><strike>" + sdClauseResult.NewText.Lines[i].SubPieces[j].Text + "</strike></font>";
                    //                    lModificationStatus = "M";
                    //                }
                    //                else
                    //                    if (sdClauseResult.NewText.Lines[i].SubPieces[j].Type == DiffPlex.DiffBuilder.Model.ChangeType.Inserted)
                    //                    {
                    //                        lNewClauseText += "<font  color='red'>" + sdClauseResult.NewText.Lines[i].SubPieces[j].Text + "</font>";
                    //                        lModificationStatus = "M";
                    //                    }
                    //                    else
                    //                        if (sdClauseResult.NewText.Lines[i].SubPieces[j].Type == DiffPlex.DiffBuilder.Model.ChangeType.Modified)
                    //                        {
                    //                            lNewClauseText += "<font color='red'>" + sdClauseResult.NewText.Lines[i].SubPieces[j].Text + "</font>";
                    //                            lModificationStatus = "M";
                    //                        }
                    //                        else
                    //                        {
                    //                            lNewClauseText += sdClauseResult.NewText.Lines[i].SubPieces[j].Text;
                    //                        }
                    //            }
                    //        }
                    //        else
                    //        {
                    //            lNewClauseText += sdClauseResult.NewText.Lines[i].Text;
                    //        }
                    //        lNewClauseText = lNewClauseText + "<br>";
                    //    }

                    //    if (!lModificationStatus.Equals("U"))
                    //    {
                    //        DataRow drTrackClauseChanges = objTrackDocChanges.dtTrackClauseChanges.NewRow();
                    //        drTrackClauseChanges["ContractFileOldId"] = lContractFileOldId;
                    //        drTrackClauseChanges["ContractFileNewId"] = lContractFileNewId;
                    //        drTrackClauseChanges["ClauseID"] = Convert.ToString(dtClauseDet.Rows[Rowcnt]["ClauseID"]);
                    //        drTrackClauseChanges["OldFileClauseValue"] = GetFormatedString(ClauseOldText);
                    //        drTrackClauseChanges["NewFileClauseValue"] = GetFormatedString(lNewClauseText);
                    //        drTrackClauseChanges["ModificationStatus"] = lModificationStatus;
                    //        drTrackClauseChanges["BookmarkName"] = lClauseBookmarkName;
                    //        drTrackClauseChanges["AddedBy"] = lUserID;
                    //        drTrackClauseChanges["IpAddress"] = lIpAddress;
                    //        objTrackDocChanges.dtTrackClauseChanges.Rows.Add(drTrackClauseChanges);
                    //    }
                    //} 
                    #endregion


                    #region Tracking Line By Line Changes Logic

                    Aspose.Words.Document docOld = new Aspose.Words.Document(Server.MapPath(lDIR) + "USILTrackChangesOld.docx");
                    Aspose.Words.Document docNew = new Aspose.Words.Document(Server.MapPath(lDIR) + "USILTrackChangesNew.docx");  

                    DocumentBuilder builderOld = new DocumentBuilder(docOld);
                    DocumentBuilder builderNew = new DocumentBuilder(docNew);

                    string lOldFileText = docOld.ToString(SaveFormat.Text);
                    string lNewFileText = docNew.ToString(SaveFormat.Text);                  

                    var d = new Differ();                   

                    var diffLine = new DiffPlex.DiffBuilder.SideBySideDiffBuilder(d);
                    var diffLineResult = diffLine.BuildDiffModel(lOldFileText.ToString(), lNewFileText);                   


                    for (int i = 0; i < diffLineResult.OldText.Lines.Count; i++)
                    {
                        StringBuilder lOriginalLineText = new StringBuilder();
                        StringBuilder lOldFileLineText = new StringBuilder();
                        StringBuilder lNewFileLineText = new StringBuilder();
                        StringBuilder lNewFileValueToEdit = new StringBuilder();


                        int lLineNumber = i+1;
                        string lModificationFlag = "U";

                        lOldFileLineText.Append(diffLineResult.OldText.Lines[i].Text);

                        if (((Convert.ToString(diffLineResult.OldText.Lines[i].Text) != null) && (Convert.ToString(diffLineResult.OldText.Lines[i].Text).Trim().Length > 0))
                          || ((Convert.ToString(diffLineResult.NewText.Lines[i].Text) != null) && (Convert.ToString(diffLineResult.NewText.Lines[i].Text).Trim().Length > 0)))
                        {
                            switch (diffLineResult.NewText.Lines[i].Type)
                            {
                                case DiffPlex.DiffBuilder.Model.ChangeType.Modified:
                                    {
                                        lModificationFlag = "M";

                                        for (int j = 0; j < diffLineResult.NewText.Lines[i].SubPieces.Count; j++)
                                        {
                                            if (diffLineResult.NewText.Lines[i].SubPieces[j].Type == DiffPlex.DiffBuilder.Model.ChangeType.Deleted)
                                            {
                                                lNewFileLineText.Append("<font color='red'><strike>" + diffLineResult.NewText.Lines[i].SubPieces[j].Text + "</strike></font>");
                                            }
                                            else
                                                if (diffLineResult.NewText.Lines[i].SubPieces[j].Type == DiffPlex.DiffBuilder.Model.ChangeType.Inserted)
                                                {
                                                    lNewFileLineText.Append("<font color='red'><strike>" + diffLineResult.OldText.Lines[i].SubPieces[j].Text + "</strike></font>");
                                                    lNewFileLineText.Append("<font color='blue'>" + diffLineResult.NewText.Lines[i].SubPieces[j].Text + "</font>");

                                                    lNewFileValueToEdit.Append("<font color='blue'>" + diffLineResult.NewText.Lines[i].SubPieces[j].Text + "</font>");

                                                }
                                                else
                                                    if (diffLineResult.NewText.Lines[i].SubPieces[j].Type == DiffPlex.DiffBuilder.Model.ChangeType.Modified)
                                                    {
                                                        lNewFileLineText.Append("<font color='yellow'>" + diffLineResult.NewText.Lines[i].SubPieces[j].Text + "</font>");

                                                        lNewFileValueToEdit.Append("<font color='yellow'>" + diffLineResult.NewText.Lines[i].SubPieces[j].Text + "</font>");
                                                    }
                                                    else
                                                        if (diffLineResult.NewText.Lines[i].SubPieces[j].Type == DiffPlex.DiffBuilder.Model.ChangeType.Imaginary)
                                                        {
                                                            lNewFileLineText.Append("<font color='red'><strike>" + diffLineResult.OldText.Lines[i].SubPieces[j].Text + "</strike></font>");
                                                        }
                                                        else
                                                        {
                                                            lNewFileLineText.Append(diffLineResult.NewText.Lines[i].SubPieces[j].Text);

                                                            lNewFileValueToEdit.Append(diffLineResult.NewText.Lines[i].SubPieces[j].Text);
                                                        }

                                        }
                                        lOriginalLineText.Append(diffLineResult.OldText.Lines[i].Text);
                                        break;
                                    }

                                case DiffPlex.DiffBuilder.Model.ChangeType.Deleted:
                                    {
                                        lNewFileLineText.Append("<font color='red'><strike>" + diffLineResult.NewText.Lines[i].Text + "</strike></font>");
                                        lOriginalLineText.Append(diffLineResult.OldText.Lines[i].Text);

                                        break;
                                    }

                                case DiffPlex.DiffBuilder.Model.ChangeType.Inserted:
                                    {
                                        lModificationFlag = "I";
                                        lNewFileLineText.Append("<font color='red'><strike>" + diffLineResult.OldText.Lines[i].Text + "</strike></font>");
                                        lNewFileLineText.Append("<font color='blue'>" + diffLineResult.NewText.Lines[i].Text + "</font>");
                                        lOriginalLineText.Append(diffLineResult.NewText.Lines[i].Text);
                                        lNewFileValueToEdit.Append("<font color='blue'>" + diffLineResult.NewText.Lines[i].Text + "</font>");

                                        break;
                                    }

                                case DiffPlex.DiffBuilder.Model.ChangeType.Imaginary:
                                    {
                                        lModificationFlag = "D";
                                        lOriginalLineText.Append(diffLineResult.OldText.Lines[i].Text);
                                        break;
                                    }

                                default:
                                    {
                                        lNewFileLineText.Append(diffLineResult.NewText.Lines[i].Text);
                                        lOriginalLineText.Append(diffLineResult.OldText.Lines[i].Text);


                                        lNewFileValueToEdit.Append(diffLineResult.NewText.Lines[i].Text);
                                        break;
                                    }
                            }

                            if (!lModificationFlag.Equals("U"))
                            {
                                DataRow drTrackLineChanges = objTrackDocChanges.dtTrackLineChanges.NewRow();
                                drTrackLineChanges["ContractFileOldId"] = lContractFileOldId;
                                drTrackLineChanges["ContractFileNewId"] = lContractFileNewId;
                                drTrackLineChanges["LineNo"] = Convert.ToString(lLineNumber);

                                string lText = lOriginalLineText.ToString();
                                if (lText.Length > 150)
                                    lText = lText.Substring(0, 150) + ".....";


                                drTrackLineChanges["OriginalLineText"] = GetFormatedString(lText.ToString());

                                drTrackLineChanges["OldFileLineValue"] = GetFormatedString(lOldFileLineText.ToString());
                                drTrackLineChanges["NewFileLineValue"] = GetFormatedString(lNewFileLineText.ToString());

                                drTrackLineChanges["NewFileValueToEdit"] = GetFormatedString(lNewFileValueToEdit.ToString());

                                drTrackLineChanges["ModificationStatus"] = lModificationFlag;
                                drTrackLineChanges["BookmarkName"] = "";
                                drTrackLineChanges["AddedBy"] = lUserID;
                                drTrackLineChanges["IpAddress"] = lIpAddress;
                                objTrackDocChanges.dtTrackLineChanges.Rows.Add(drTrackLineChanges);
                            }
                        }
                    }
                    #endregion

                    objTrackDocChanges.InsertRecord();
                }
            }
            Page.TraceWrite("CompareDataInWordDoc ends.");
        }
        catch (Exception ex)
        {
            Page.TraceWarn("Compare documents fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }         
    }



    public DataTable CompareFilesLineByLineAndReturnDatatoSaveInDB()
    {
        DataTable dt = new DataTable();
        try
        {
            return dt;
        }
        catch (Exception ex)
        {
            
            throw;
        }
    
    }



    public string GetFormatedString(string lText)
    {
        try
        {
            lText = lText.Replace("\r", "<br>");
            lText = lText.Replace("\t", "&nbsp;");
            return lText;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }



    

    void Message()
    {
        if (Session[Declarations.Message] != null)
        {
            var flg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }

    }



    protected void btnSubmit_Click(object sender, EventArgs e)
    {
            
        System.Threading.Thread.Sleep(500);
        string lRequestID = Request.QueryString["rid"].ToString();
        CompareDataInWordDoc(lRequestID);
        string lUrl = "~/WorkFlow/TrackChangesData.aspx?RequestID=" + Convert.ToString(hdnRequestID.Value);
        Server.Transfer(lUrl);
    }


    public void SaveDummyFileWithouHeaderFooter(Document doc, bool IsLatestFileVersion)
    {
        try
        {
            foreach (Section section in doc)
            {
                // Up to three different lHeaders are possible in a section (for first, even and odd pages).
                // We check and delete all of them.
                HeaderFooter lHeader;

                lHeader = section.HeadersFooters[HeaderFooterType.HeaderFirst];
                if (lHeader != null)
                    lHeader.Remove();

                // Primary lHeader is the lHeader used for odd pages.
                lHeader = section.HeadersFooters[HeaderFooterType.HeaderPrimary];
                if (lHeader != null)
                    lHeader.Remove();

                lHeader = section.HeadersFooters[HeaderFooterType.HeaderEven];
                if (lHeader != null)
                    lHeader.Remove();


                // Up to three different footers are possible in a section (for first, even and odd pages).
                // We check and delete all of them.
                HeaderFooter footer;

                footer = section.HeadersFooters[HeaderFooterType.FooterFirst];
                if (footer != null)
                    footer.Remove();

                // Primary footer is the footer used for odd pages.
                footer = section.HeadersFooters[HeaderFooterType.FooterPrimary];
                if (footer != null)
                    footer.Remove();

                footer = section.HeadersFooters[HeaderFooterType.FooterEven];
                if (footer != null)
                    footer.Remove();
            }
            if (IsLatestFileVersion)
                doc.Save(Server.MapPath(lDIR) + "USILTrackChangesNew.docx");
            else
                doc.Save(Server.MapPath(lDIR) + "USILTrackChangesOLD.docx");
        }
        catch (Exception ex)
        {

            throw ex;
        }

    }

}