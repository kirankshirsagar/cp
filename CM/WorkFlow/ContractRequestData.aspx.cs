﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;
using WorkflowBLL;
using MasterBLL;
using System.Web.UI.HtmlControls;

public partial class Masters_ContractRequestData : System.Web.UI.Page
{
    // navigation//
    public static int RecordsPerPage = int.Parse(System.Configuration.ConfigurationManager.AppSettings["RecordsPerPage"]);
    public static int VisibleButtonNumbers = 10;
    // navigation//
    
    string RequestType = string.Empty;
    public string Add;
    public string View;
    public string Update;
    public string Delete;
    public bool RequestTypeClickAccess;
    IContractRequest objContractRequest;
    IContractTemplate objContractTemplate;
    string DashboardRequestType = "";
    string Tenure = "";

    delegate void DelBulkQueueFordelIndex(string strUrl,List<ContractRequest> mylist, int opt);

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        Session["MasterSeelcted"] = null;
        Session["ChlidSeelcted"] = null;

        string strargs = Page.Request.Params.Get("__EVENTTARGET");
        if (strargs != null)
        {
            strargs = strargs.ToLower();
        }

        if (Session[Declarations.User] != null && !IsPostBack)
        {
            DashboardRequestType = Request.QueryString["DashboardRequestType"] == null ? "" : Request.QueryString["DashboardRequestType"];
            Tenure = Request.QueryString["Tenure"] == null ? "" : Request.QueryString["Tenure"];
            Access.PageAccess(this, Session[Declarations.User].ToString(), "WorkFlow_");
            ViewState[Declarations.Add] = Access.Add.Trim();
            ViewState[Declarations.View] = Access.View.Trim();
            ViewState[Declarations.Delete] = Access.Delete.Trim();
            ViewState[Declarations.Update] = Access.Update.Trim();
            ViewState["RequestTypeClickAccess"] = Access.isCustomised(12);

            Access.PageAccess("ContractRelationAdd.aspx", Session[Declarations.User].ToString());
            ViewState["ViewAccessContractRelationTree"] = Access.View.Trim();
        }

        AccessVisibility();

        ddlContractType.extDataBind(objContractTemplate.SelectDataRequestType());

        if (Request.QueryString["ch"] != null)
        {
            RequestType = Request.QueryString["ch"].ToString();
        }
        if (RequestType == string.Empty)
        {
            RequestType = "cnc";
        }

        if ((!IsPostBack || hdnSearch.Value.Trim() != string.Empty))
        {
            Session[Declarations.SortControl] = null;
            if (Session[Declarations.User] != null)
            {
                ReadData(1, RecordsPerPage, Convert.ToInt32(Session[Declarations.User].ToString()), RequestType, DashboardRequestType, Tenure);
                Message();
                //SetNavigationButtonParameters();
            }
        }
        else
        {
            if (Session[Declarations.User] != null && !strargs.Contains("btnshowall"))
            {
                ReadData(1, RecordsPerPage, Convert.ToInt32(Session[Declarations.User].ToString()), "", DashboardRequestType, Tenure);
                SetNavigationButtonParameters();
            }
        }
    }

    protected void btnSort_Click(object sender, EventArgs e)
    {
        hdnSearch.Value = "";
        LinkButton clickBtn = (LinkButton)sender;
        Session.Add(Declarations.SortControl, clickBtn);
        ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage, Convert.ToInt32(Session[Declarations.User].ToString()), RequestType, DashboardRequestType, Tenure);
    }

    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

        if (ViewState["RequestTypeClickAccess"] != null)
        {
            RequestTypeClickAccess = Convert.ToBoolean(ViewState["RequestTypeClickAccess"].ToString());
        }
    }

    void Message()
    {
        if (Session[Declarations.Message] != null)
        {
            var flg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }
    }

    // navigation//

    #region Navigation
    void SetNavigationButtonParameters()
    {
        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }



    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage, Convert.ToInt32(Session[Declarations.User].ToString()), RequestType, DashboardRequestType, Tenure);
            AccessVisibility();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString["DashboardRequestType"].Length == 0)
            {
                Response.Redirect("../Workflow/ContractRequestData.aspx");
            }
            else
            {
                if (Session[Declarations.User] != null)
                {
                    ReadData(1, PaginationButtons1.RecordsPerPage, Convert.ToInt32(Session[Declarations.User].ToString()), "", DashboardRequestType, Tenure);
                    SetNavigationButtonParameters();
                }
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("../Workflow/ContractRequestData.aspx");
        
        }

    }
    #endregion

    // navigation//

    void ReadData(int pageNo, int recordsPerPage, int UserId, string RequestType, string DashboardRequestType,string Tenure)
    {
        DashboardRequestType = Request.QueryString["DashboardRequestType"] == null ? "" : Request.QueryString["DashboardRequestType"];
        Tenure = Request.QueryString["Tenure"] == null ? "" : Request.QueryString["Tenure"];
        if (RequestType == "")
        {
            txtContractId.Value = "";
        }

        if (View == "Y")
        {
            Page.TraceWrite("ReadData starts.");
            try
            {
                string ContractTypeName = "", ContractStatusName = "";
                ContractTypeName = Request.QueryString["CType"];
                ContractStatusName = Request.QueryString["CStatus"];

                if (txtContractId.Value.Trim().Length > 0)
                    objContractRequest.ContractID = Convert.ToInt32(txtContractId.Value);
                objContractRequest.UserID = UserId;
                objContractRequest.PageNo = pageNo;
                objContractRequest.RequestType = RequestType;
                objContractRequest.RecordsPerPage = recordsPerPage;
                objContractRequest.ClientName = TxtClientName.Value.Trim();
                objContractRequest.ContractStatus = ContractStatusName;
                objContractRequest.ContractTypeName = ContractTypeName;

                if (DashboardRequestType.Length > 0)
                {
                    objContractRequest.DashboardRequestType = DashboardRequestType;
                    objContractRequest.Tenure = Tenure;
                }
                if (hdnContractTypeId.Value == "")
                {
                    hdnContractTypeId.Value = "0";
                }
                objContractRequest.RequestTypeID = Convert.ToInt32(hdnContractTypeId.Value);
                LinkButton btnSort = null;
                if (Session[Declarations.SortControl] != null && Session[Declarations.SortControl] != string.Empty)
                {
                    btnSort = (LinkButton)Session[Declarations.SortControl];
                    objContractRequest.Direction = btnSort.CssClass.ToLower() == "sort desc" ? 1 : 0;
                    objContractRequest.SortColumn = btnSort.Text.Trim();
                }
                rptContractRequestDetails.DataSource = objContractRequest.ReadDataMyContracts(); //objContractRequest.ReadData();
                rptContractRequestDetails.DataBind();
                if (btnSort != null)
                {
                    rptContractRequestDetails.ClassChange(btnSort);
                }
                Page.TraceWrite("ReadData ends.");
                ViewState[Declarations.TotalRecord] = objContractRequest.TotalRecords;
                PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
                PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
                PaginationButtons1.RecordsPerPage = RecordsPerPage;
                PaginationButtons1.AddpagingButton();
            }
            catch (Exception ex)
            {
                Page.TraceWarn("ReadData fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            ddlContractType.Value = hdnContractTypeId.Value;
        }
    }

    protected void rptContractRequestDetails_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label ContractID = (Label)e.Item.FindControl("lblContractID");          
            HtmlTableCell TdDisable = (HtmlTableCell)e.Item.FindControl("rowEditLinkId");

            Label IsDocumentGenerated = (Label)e.Item.FindControl("lblIsDocumentGenerated");
            Label IsBulkImport = (Label)e.Item.FindControl("lblIsBulkImport");

            Label lblRequestNo = (Label)e.Item.FindControl("lblRequestNo");
            HtmlAnchor aEditRequest = (HtmlAnchor)e.Item.FindControl("aEditRequest");
            if (Update == "N")
            {
                lblRequestNo.Visible = true;
                aEditRequest.Visible = false;
            }
            else
            {
                lblRequestNo.Visible = false;
                //if (ContractID.Text == "0")

                if (IsDocumentGenerated.Text.Equals("N") && IsBulkImport.Text.Trim().Equals("N"))
                {
                    aEditRequest.Visible = true;
                }
                else
                {
                    //aEditRequest.Visible = false;
                    //lblRequestNo.Visible = true;
                }
            }

            if (ContractID.Text == "0")
            {
                ContractID.Text = "";
            }
            else
            {
                TdDisable.InnerText = "";
            }
        }
        HtmlTableCell headCheckBoxId = (HtmlTableCell)e.Item.FindControl("headCheckBoxId");
        HtmlTableCell rowCheckBoxId = (HtmlTableCell)e.Item.FindControl("rowCheckBoxId");

        HtmlTableCell headUsedImageId = (HtmlTableCell)e.Item.FindControl("headUsedImageId");
        HtmlTableCell rowUsedImageId = (HtmlTableCell)e.Item.FindControl("rowUsedImageId");

       // HtmlTableCell headEditLinkId = (HtmlTableCell)e.Item.FindControl("headEditLinkId");
        //HtmlTableCell rowEditLinkId = (HtmlTableCell)e.Item.FindControl("rowEditLinkId");        

        LinkButton imgEdit = (LinkButton)e.Item.FindControl("imgEdit");
        Label lblRequestTypeNameLink = (Label)e.Item.FindControl("lblRequestTypeNameLink");

        if (headCheckBoxId != null && Delete == "N")
        {
            headCheckBoxId.Visible = false;
        }

        //if (headEditLinkId != null && Update == "N")
        //{
        //    headEditLinkId.Visible = false;
        //}
        //if (rowEditLinkId != null && Update == "N")
        //{
        //    rowEditLinkId.Visible = false;
        //}        

        if (rowCheckBoxId != null && Delete == "N")
        {
            rowCheckBoxId.Visible = false;
        }

       

        if (rowUsedImageId != null && Delete == "N")
        {
            rowUsedImageId.Visible = false;
        }
        if (headUsedImageId != null && Delete == "N")
        {
            headUsedImageId.Visible = false;
        }

        //if (imgEdit != null)
        //{
        //    if (RequestTypeClickAccess == false)
        //    {
        //        imgEdit.Visible = false;
                
        //    }
        //    else
        //    {
        //        imgEdit.Visible = true;
                
        //    }
        //}
    } 

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objContractRequest = FactoryWorkflow.GetContractRequestDetails();
            objContractTemplate = FactoryMaster.GetContractTemplateDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Delete starts.");
        string status = "";
        objContractRequest.RequestIDs = hdnRequestId.Value;
        objContractRequest.UserID = Convert.ToInt32(Session[Declarations.User].ToString());
        objContractRequest.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;

        List<ContractRequest> myList = new List<ContractRequest>();
        try
        {
            myList = objContractRequest.ContractVersionsFile();
            status = objContractRequest.DeleteRecord();
        }
        catch (Exception ex)
        {
            hdnPrimeIds.Value = string.Empty;
            Page.JavaScriptClientScriptBlock("status", "PageGetMessage('DF')");
            Page.TraceWarn("Delete fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        hdnPrimeIds.Value = string.Empty;
        Page.JavaScriptClientScriptBlock("status", "PageGetMessage('DS')");
        Page.TraceWrite("Delete ends.");
        RecordsPerPage = Convert.ToInt32(hdnRecordsPerPage1.Value == "" ? "50" : hdnRecordsPerPage1.Value);
        ReadData(1, RecordsPerPage, Convert.ToInt32(Session[Declarations.User].ToString()), RequestType, DashboardRequestType, Tenure);
         
            /*Keyoti Uploaded Document Indexing Start.*/
            string strUrl = "";
            strUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + HttpContext.Current.Session["TenantDIR"] + "/Uploads/" + HttpContext.Current.Session["TenantDIR"];
            //AsyncIndexer.GetInstance().BulkQueueForIndexing(myList, 2);
            DelBulkQueueFordelIndex objdel = new DelBulkQueueFordelIndex(AsyncIndexer.GetInstance().BulkQueueForIndexing);
            IAsyncResult ar = objdel.BeginInvoke(strUrl, myList, 2, null, null);
            /*Keyoti Uploaded Document Indexing End.*/
    }

    protected void btnChangeStatus_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Change Status starts.");
        string status = "";
       
        try
        {
            ReadData(1, RecordsPerPage, Convert.ToInt32(Session[Declarations.User].ToString()), RequestType, DashboardRequestType, Tenure);
        }
        catch (Exception ex)
        {
            hdnPrimeIds.Value = string.Empty;
            Page.JavaScriptClientScriptBlock("status", "PageGetMessage('SF')");
            Page.TraceWarn("Change Status fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.JavaScriptClientScriptBlock("status", "PageGetMessage('SS')");
        Page.TraceWrite("Change Status ends.");
    }

    protected void imgEdit_Click(object sender, EventArgs e)
    {
        Server.Transfer("RequestFlow.aspx?RequestId=" + hdnRequestId.Value + "&RequestNo=" + hdnRequestNo.Value + "&Status=Workflow ");
    }
    protected void linkEdit_Click(object sender, EventArgs e)
    {
       
        Server.Transfer("ContractRequest.aspx?RequestId=" + hdnRequestId.Value + "&RequestNo=" + hdnRequestNo.Value);
    }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (View == "N")
        {
            Page.extDisableControls();
            btnChangeStatus.Enabled = false;
        }
        else
        {
            btnSearch.Enabled = true;
            txtContractId.Disabled = false;
        }

        if (Update == "N")
        {
            //foreach (RepeaterItem item in rptContractRequestDetails.Items)
            //{
            //    LinkButton imgEdit = (LinkButton)item.FindControl("imgEdit");
            //    imgEdit.Enabled = false;
            //    imgEdit.Visible = false;
            //}
           // btnDelete.Enabled = false;
            btnChangeStatus.Enabled = false;
        }
        if (Add == "N")
        {
            //Page.extDisableControls();
            btnChangeStatus.Enabled = false;
            btnSearch.Enabled = false;
        }
        else
        {
            btnSearch.Enabled = true;
        }

        if (Delete == "N")
        {
            btnDelete.Enabled = false;
            btnDelete.Visible = false;
        }

        if (View == "Y")
        {
            txtContractId.Disabled = false;
            TxtClientName.Disabled = false;
            ddlContractType.Disabled = false;
            btnSearch.Enabled = true;
            btnShowAll.Enabled = true;
        }       
        Page.TraceWrite("AccessVisibility starts.");
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtContractId.Value = "";
        TxtClientName.Value = "";
    }
}










