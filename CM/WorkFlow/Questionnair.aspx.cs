﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;
using DocumentOperationsBLL;
using WorkflowBLL;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using EmailBLL;
using System.Text.RegularExpressions;

public partial class WorkFlow_Questionnair : System.Web.UI.Page
{
    IContractType objContract;
    static string filename = "";
    IContractRequest req;
    private readonly static string Path = @"~/Uploads/" + HttpContext.Current.Session["TenantDIR"] + "/Contract Documents";    

    protected void Page_Load(object sender, EventArgs e)
    {
        //abc();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            SetDefault();
            GetDynamicControls();
            ViewState["IsBulkImport"] = Request.QueryString["IsBulkImport"];
            ViewState["ParentRequestID"] = Request.QueryString["ParentRequestID"];
            //ViewState["RequestId"] = Request.QueryString["RequestId"];
             
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ParentRequestID"])))
            {
                ViewState["ParentRequestIDPARAM"] = "&ParentRequestID=" + ViewState["ParentRequestID"];
            }
            chkGenerateContract.Checked = true;
        }
        
    }   

    #region Button events

    protected void btnSaveDraft_Click(object sender, EventArgs e)
    {
        Credentials credentials = new Credentials();
        credentials.RequestId = Convert.ToInt32(ViewState["RequestId"]);
        credentials.UserId = int.Parse(Session[Declarations.User].ToString());
        credentials.IP = Session[Declarations.IP].ToString();
        bool isContractGenrate;
        if (chkGenerateContract.Checked)
            isContractGenrate = true;
        else
            isContractGenrate = false;
        if (credentials.SaveAnswers(phQuestionnair, true, isContractGenrate))
        {
            //Page.JavaScriptClientScriptBlock("SaveDraft", "MessageMasterDiv('Draft Saved.', 0, 100)");
            Response.Redirect("~/Workflow/RequestFlow.aspx?Section=QADraft&ScrollTo=DivUpdatedQuestions&RequestId=" + ViewState["RequestId"] + "&Status=Workflow" + ViewState["ParentRequestIDPARAM"], true);
        }
        else
        {

        }
    }

    protected void btnGenerateContract_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Generate Contract started");
        System.Threading.Thread.Sleep(0);
        Credentials credentials = new Credentials();        
        bool isExists = System.IO.Directory.Exists(Server.MapPath(Path));
        if (!isExists)
        {
            System.IO.Directory.CreateDirectory(Server.MapPath(Path));
        }
        try
        {
            string Template = ViewState["FilePath"] + "\\" + filename;
            Aspose.Words.Document doc = new Aspose.Words.Document();
            credentials.RequestId = Convert.ToInt32(ViewState["RequestId"]);
            credentials.UserId = int.Parse(Session[Declarations.User].ToString());
            credentials.IP = Session[Declarations.IP].ToString();
            bool isContractGenrate;
            if (chkGenerateContract.Checked)
                isContractGenrate = true;
            else
                isContractGenrate = false;

            if (credentials.SaveAnswers(phQuestionnair, false, isContractGenrate))
            {
                if (chkGenerateContract.Checked)
                {
                    if (ViewState["IsBulkImport"].ToString() == "Y")
                    {
                        doc = credentials.CreateWordDoc(Template, Convert.ToInt32(ViewState["TemplateId"]));
                    }
                    else
                    {
                        doc = credentials.GetWordDoc(Template, Convert.ToInt32(ViewState["TemplateId"]));
                    }
                    doc.Variables.Add("ContractDocumentVersion", ViewState["ContractFileName"].ToString());
                    doc.Save(Server.MapPath(Path) + "//" + ViewState["ContractFileName"] + ".docx", Aspose.Words.SaveFormat.Docx);
                    doc.Save(Server.MapPath(Path) + "//PDF//" + ViewState["ContractFileName"] + ".pdf", Aspose.Words.SaveFormat.Pdf);
                    SaveContractFile();


                    /*Keyoti Uploaded Document Indexing Start*/
                    string strUrl = "";
                    IActivity objActivity;
                    objActivity = FactoryWorkflow.GetActivityDetails();
                    objActivity.RequestId = Convert.ToInt32(ViewState["RequestId"]);
                    ViewState["ContractInfo"] = objActivity.ContractDetailsByRequestId();

                    strUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + Context.Request.Url.Segments[1] + "Uploads/" + Context.Request.Url.Segments[1].Replace("/", "") + "/Contract Documents/" + ViewState["ContractFileName"] + ".docx";
                    AsyncIndexer.GetInstance().QueueForIndexing(strUrl, 1, ViewState["ContractInfo"].ToString());
                    strUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + Context.Request.Url.Segments[1] + "Uploads/" + Context.Request.Url.Segments[1].Replace("/", "") + "/Contract Documents/PDF/" + ViewState["ContractFileName"] + ".pdf";
                    AsyncIndexer.GetInstance().QueueForIndexing(strUrl, 1, ViewState["ContractInfo"].ToString());
                    /*Keyoti Uploaded Document Indexing End*/
                }
                SendEmail();
                Response.Redirect("~/Workflow/RequestFlow.aspx?Section=QA&ScrollTo=DivUpdatedQuestions&SendEmail=true&RequestId=" + ViewState["RequestId"] + "&Status=Workflow " + ViewState["ParentRequestIDPARAM"], true);
            }
            
            phQuestionnair.Controls.Clear();
            Page.TraceWrite("Generate Contract succeed");
        }
        catch (IOException ex)
        {
            //phQuestionnair.Controls.Add(new Label() { Text="File not found"});
            Page.JavaScriptClientScriptBlock("FileNotFound", "MessageMasterDiv('Template not found.', 1, 100)");
            Page.TraceWrite("Generate Contract failed." + ex.Message);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        catch (Exception ex)
        {
            Page.TraceWrite("Generate Contract failed." + ex.Message);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }        
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/WorkFlow/RequestFlow.aspx?RequestId=" + ViewState["RequestId"] + "&Status=Workflow " + ViewState["ParentRequestIDPARAM"], false);
    }

    #endregion


    #region Methods

    private void SetDefault()
    {
        System.Collections.Specialized.NameValueCollection RequestFlow = Request.Form;
        ViewState["RequestId"] = RequestFlow["ctl00$MainContent$hdnRequestId"];
        ViewState["ContractTypeId"] = RequestFlow["ctl00$MainContent$hdnContractTypeId"];
        if (!string.IsNullOrEmpty(RequestFlow["ctl00$MainContent$hdnSelectedFileId"]))
        {
            ViewState["FileName"] = RequestFlow["ctl00$MainContent$hdnSelectedFileId"].Split('#')[1];
            ViewState["TemplateId"] = RequestFlow["ctl00$MainContent$hdnSelectedFileId"].Split('#')[0];
        }
        ViewState["ContractId"] = RequestFlow["ctl00$MainContent$hdnContractId"];
        ViewState["ContractStatusID"] = RequestFlow["ctl00$MainContent$hdnContractStatusID"];
        ViewState["RequestTypeId"] = RequestFlow["ctl00$MainContent$hdnRequestTypeId"];
        ViewState["ContractFileName"] = HttpUtility.UrlDecode(RequestFlow["ctl00$MainContent$hdnContractFileName"]);
        ViewState["GUID"] = Guid.NewGuid().ToString();
        ViewState["ContractFileName"] = ViewState["ContractFileName"] + "_" + ViewState["GUID"];

        //if (string.IsNullOrEmpty(ViewState["ContractId"].ToString()) || ViewState["ContractId"].ToString() == "0")
        if (string.IsNullOrEmpty(ViewState["ContractStatusID"].ToString()) || Convert.ToInt32(ViewState["ContractStatusID"]) <= 6)
            btnSaveDraft.Visible = true;
        //else
        //btnSaveDraft.Visible = false;
    }

    protected void GetDynamicControls()
    {
        Page.TraceWrite("Dynamic control generation started");

        phQuestionnair.Controls.Clear();

        string Path = @"~/Uploads/" + Session["TenantDIR"] + "/Contract Template";
        bool isExists = System.IO.Directory.Exists(Server.MapPath(Path));
        if (!isExists)
        {
            Console.WriteLine("No file found.");
        }
        else
        {
            try
            {
                if (!string.IsNullOrEmpty(ViewState["FileName"].ToString()))
                {
                    filename = ViewState["FileName"].ToString();
                    //ViewState["SelectedFileId"] = hdnSelectedFileId.Value;
                }
                string TemplateId = ViewState["TemplateId"].ToString();//ViewState["SelectedFileId"] == null ? "0" : ViewState["SelectedFileId"].ToString().Split('#')[0];
                ViewState["FilePath"] = Server.MapPath(Path);// +"//" + hdnFileName.Value;
                Credentials credentials = new Credentials();
                credentials.IP = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
                credentials.UserId = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
                credentials.ContractId = ViewState["ContractId"].ToString() == "" ? 0 : Convert.ToInt32(ViewState["ContractId"].ToString());
                credentials.RequestId = Convert.ToInt32(ViewState["RequestId"]);
                phQuestionnair.Controls.Clear();
                Control container = credentials.GenerateQuetionnair(TemplateId);// FileRead.ReadMsWord(ViewState["filePath"].ToString());        
                phQuestionnair.Controls.Add(container);

                btnGenerateContract.Visible = true;
            }
            catch (System.Xml.XmlException ex)
            {
                Page.JavaScriptClientScriptBlock("QuestionnaireNotFound", "MessageMasterDiv('No questionnaire found for this request.', 1, 100)");
                btnGenerateContract.Visible = false;
                btnSaveDraft.Visible = false;
                chkGenerateContract.Visible = false;

                Page.TraceWrite("Dynamic control generation failed. " + ex.Message);
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            catch (NotSupportedException ex)
            {
                Page.JavaScriptClientScriptBlock("NotSupportedException", "MessageMasterDiv('Unable to find licence copy for Aspose.Word.', 1, 100)");
                Page.TraceWrite("Aspose.lic file not found. " + ex.Message);
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            catch (Exception ex)
            {
                Page.JavaScriptClientScriptBlock("QuestionnaireNotFound", "MessageMasterDiv('No questionnaire found for this request.', 1, 100)");
                Page.TraceWrite("Dynamic control generation failed. " + ex.Message);
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        Page.TraceWrite("Dynamic control generation succeed");
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);
        if (Request.Params["ctl00$MainContent$btnBack"] == null)
        {
            GetDynamicControls();
        }
    }

    protected void SaveContractFile()
    {
        ContractProcedures CP = new ContractProcedures();
        FileInfo file = new FileInfo(Server.MapPath(Path) + "//" + ViewState["ContractFileName"] + ".docx");
        //CP.ContractId = Convert.ToInt32(ViewState["ContractId"].ToString());
        CP.RequestId = Convert.ToInt32(ViewState["RequestId"].ToString());
        CP.FileName = ViewState["ContractFileName"].ToString();
        CP.FileSizeKB = Math.Round(Convert.ToDecimal(file.Length) / 1024, 2).ToString();
        CP.ModifiedBy = Convert.ToInt32(Session[Declarations.User]);
        CP.IpAddress = Session[Declarations.IP].ToString();
        CP.IsValidDocument = 'Y';
        CP.GUID = ViewState["GUID"].ToString();
        string result = CP.InsertRecord();
    }

    void SendEmail()
    {
        try
        {
            Page.TraceWrite("send emails.");
            EmailTemplateBLL.IEmailTrigger objEmail = EmailTemplateBLL.FactoryEmailTemplate.GetEmailTriggerDetail();
            objEmail.RequestId = Convert.ToInt32(ViewState["RequestId"]);
            //objEmail.ContractTypeId = Convert.ToInt32(ViewState["ContractTypeId"]);
            //objEmail.ContractTemplateId = 0;// Convert.ToInt32(ViewState["TemplateId"]); // not required 
            objEmail.EmailTriggerId = 2; // hardcoded for Amendment made to contract
            objEmail.userId = int.Parse(Session[Declarations.User].ToString());
            //objEmail.AssignedToUserId = int.Parse(ddlassignto.Value);
            objEmail.BulkEmail();

            // sk
            //SendContractGeneratedEmailToClient(objEmail.RequestId);

        }
        catch (Exception)
        {
            Page.TraceWarn("send emails fail.");
        }
    }

    void SendContractGeneratedEmailToClient(int RequestId)
    {
        IActivity objAct = FactoryWorkflow.GetActivityDetails();
        objAct.RequestId = RequestId;
        List<Activity> lst = new List<Activity>();
        lst = objAct.GetUserNameAndPassword();
        string userEmailId = lst[0].EmailID;
        string password = lst[0].Password;
        string customerName = lst[0].UserName;

        string contractGeneratedDate = lst[0].AddedOnDate;
        int ContractId = lst[0].ContractID;
        if (ContractId > 0)
        {
            try
            {
                Page.TraceWrite("Contract Generated send emails.");
                SendMail sm = new SendMail();
                sm.MailFrom = System.Configuration.ConfigurationManager.AppSettings["MailFrom"];
                sm.Subject = "Contract " + ContractId.ToString() + " - Available for Review";

                string body = "<font face='Arial' size='2' color='#006666'>Hello <br><br>";
                body = body + "Contract ID: " + ContractId.ToString() + "<br><br>";
                body = body + "A contract has been generated for your attention.  You can view the contract and its activities by <br>";
                body = body + "following the link below:<br><br>";
                body = body + "<a href='" + Strings.domainURL() + "customerportal/login.aspx'>" + Strings.domainURL() + "customerportal/login.aspx</a><br><br>";
                body = body + "Your login details are:<br><br>";
                body = body + "Username: " + userEmailId + "<br>";
                body = body + "Password: " + password + "<br><br>";
                body = body + "Happy contracting!";
                body = body + "<br>The ContractPod™ team</font>";

                body = body + "<br><br><font face='Arial' size='1' color='#006666'>Need help? Please do not hesitate to contact our technical support team on 0800 699 0045 or at help@contractpod.com.";
                body = body + "<br>The information in this email is confidential and for use by the addressee(s) only. If you are not the intended recipient, please notify us immediately on 0800 699 0045 and delete the message from your computer. You may not copy or forward the e-mail, or use it or disclose its contents to any other person. We do not accept any liability or responsibility for changes made to this email after it was sent, or viruses transmitted through this e-mail or any attachment.</font>";

                sm.Body = body;
                sm.MailTo = userEmailId;
                sm.RequestId = RequestId;
                sm.Mode = "Contract Generated";
                sm.SendSimpleMail();
            }
            catch (Exception)
            {
                Page.TraceWarn("Contract Generated fail.");
            }
        }
    }

    /*
    void SendContractGeneratedEmailToClient(int RequestId)
    {
        IActivity objAct = FactoryWorkflow.GetActivityDetails();
        objAct.RequestId = RequestId;
        List<Activity> lst = new List<Activity>();
        lst = objAct.GetUserNameAndPassword();
        string userEmailId = lst[0].EmailID;
        string password = lst[0].Password;
        string customerName = lst[0].UserName;

        string contractGeneratedDate = lst[0].AddedOnDate;
        int ContractId = lst[0].ContractID;
        if (ContractId > 0)
        {
            try
            {
                Page.TraceWrite("Contract Generated send emails.");
                SendMail sm = new SendMail();
                sm.MailFrom = System.Configuration.ConfigurationManager.AppSettings["MailFrom"];
                sm.Subject = "Your Contract ID " + ContractId.ToString() + " generated.";
                string body = "Dear " + customerName + ",<br><br>You contract has been generated on " + contractGeneratedDate + ". You can view all your contract related activities on following URL<br><br>";
                body = body + "URL : <a href='" + Strings.domainURL() + "customerportal/login.aspx'>" + Strings.domainURL() + "customerportal/login.aspx</a><br><br>";
                body = body + "User name : " + userEmailId + "<br>";
                body = body + "Password : " + password + "<br><br>";
                body = body + "Thanks,<br><br>";
                body = body + "Contract Management Team";
                sm.Body = body;
                sm.MailTo = userEmailId;
                sm.RequestId = RequestId;
                sm.Mode = "Contract Generated";
                sm.SendSimpleMail();
            }
            catch (Exception)
            {
                Page.TraceWarn("Contract Generated fail.");
            }
        }
    }
    */
    #endregion


}