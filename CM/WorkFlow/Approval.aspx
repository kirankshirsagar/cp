﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true" CodeFile="Approval.aspx.cs" Inherits="WorkFlow_Approval" %>

<%@ Register Src="../UserControl/requestnewlinks.ascx" TagName="requestnewlinks"
    TagPrefix="uc1" %>  
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Font.css" rel="stylesheet" type="text/css" />
    <script src="../scripts/CommonValidations.js" type="text/javascript"></script>
    <script src="../scripts/jquery-1.7.2.min.js" type="text/javascript"></script>       
    <script src="../scripts/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    <link href="../Styles/jquery-ui-1.8.19.custom.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="Masterlinks1" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>

 <style>
     textarea { resize: none;}
 </style>
<script type="text/javascript">
    $(document).ready(function () {
        $("#requestLink").addClass("menulink");
        $("#requesttab").addClass("selectedtab");
        $('textarea').autogrow();
    });        
    </script>
    <h2>
        <%=ViewState["StageName"].ToString()%> Approval <%=ViewState["ContractIdMsg"]%> </h2>
          <div>
            <div>
                <p>
                    <a href='RequestFlow.aspx?Status=Workflow&RequestId=<%=ViewState["RequestId"].ToString() %>' class="issue status-1 priority-4 parent">Request #<%=ViewState["RequestId"]%> </a>:
                    <%=ViewState["StageName"].ToString()%> Approval  <%=ViewState["ContractIdMsg"]%></p>
            </div>
        </div>   
    <div id="Approvals">
                <div class="box" id="Div8">
                    <a name="Approvalsname"></a>
                    <fieldset class="tabular">
                        <legend>Conditions</legend>
                        <ul class="details">
                            <%--<li><strong>Contract Date is greater than 15-Aug-2013 AND Business City is Pune </strong><br>
                                17-Sep-2014</li>--%>
                            <li>
                                <asp:Label ID="lblStageConditions" runat="server"></asp:Label>
                            </li>
                        </ul>
                    </fieldset>
                    <fieldset class="tabular">
                        <legend>Approval</legend>
                        <p>
                            <label for="time_entry_activity_id">
                                <span class="required">* </span>Approval</label>
                         <span class="radio required" style="color:Black;">                         
                            <input id="rdoApprove" runat="server" type="radio" />Approve
                            <input id="rdoDisapprove"  runat="server" type="radio" />Disapprove
                            <em></em>
                        </span>
                        </p>
                        <p>
                            <label for="time_entry_issue_id">
                                <span class="required">* </span>Remark
                             </label>                             
                             <textarea id="txtRemark" rows="2" cols="20" class="ui-autocomplete-input required" runat="server" style="width:500px;" ></textarea>
                            <em></em>
                        </p>
                    </fieldset>
                </div>
                <%--<input name="commit" type="button" value="Save" onclick="location.href='requestflow.aspx#newapproval'" />--%>        
        <asp:Button ID="btnSaveApproval" runat="server" Text="Save" 
                    onclick="btnSaveApproval_Click" class="btn_validate"/>
        <asp:Button ID="btnBack" runat="server" Text="Back" onclick="btnBack_Click" />
            </div>
  
    <script type="text/javascript">
        $('textarea').each(function (evt) {
            var $element = $(this).get(0);
            $element.addEventListener('keyup', function () {
                
                this.style.overflow = 'hidden';                    
                this.style.height = 0;
                this.style.height = this.scrollHeight + 'px';
            }, false);
        });
    </script>
</asp:Content>

