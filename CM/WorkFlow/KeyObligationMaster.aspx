﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="KeyObligationMaster.aspx.cs" Inherits="WorkFlow_KeyObligationMaster" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    <%-- <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />--%>
    <link href="../Styles/jquery-ui-1.8.21.css" media="all" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../scripts/jquery-ui-1.8.16.custom.css" />
    <link rel="stylesheet" href="../modalfiles/modal.css" type="text/css" />
    <script src="../JQueryValidations/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../scripts/jquery-ui-timepicker-addon.js"></script>
    <link rel="stylesheet" href="../scripts/jquery-ui-timepicker-addon.css" type="text/css" />
    <style type="text/css">
        .tdAlign
        {
            text-align: right;
            width: 30%;
        }
        .tooltip
        {
            margin-left: 2px !important;
        }
    </style>
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.10/clipboard.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"
        rel="stylesheet" />--%>
    <%-- <script src="../scripts/tooltip/jquery.min.js" type="text/javascript"></script> --%>
    <%--<script src="../scripts/tooltip/clipboard.min.js" type="text/javascript"></script>--%>
    <%--<script src="../scripts/tooltip/jquery.min.js" type="text/javascript"></script>--%>
    <script src="../scripts/tooltip/bootstrap.min.js" type="text/javascript"></script>
    <link href="../scripts/tooltip/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script> 
        $(function () {
            $('[data-toggle="popover"]').popover({ trigger: 'click' })
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $('.CustomCurrency').on('keypress', function (evt) {

                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode == 20) {
                    return true;
                }
                if (charCode == 13)
                    return false;

                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    if (charCode != 46) {
                        return false;
                    }
                }
                var count = 0;
                var myNum = this.value + String.fromCharCode(charCode);
            });

            $('.submit').live("click", function () {
                var GCount = 0;
                var OCount = 0;
                $('.KeyG').each(function () {
                    // alert($(this).val().length);
                    if ($(this).val().length > 1000) {
                        alert($(this).prev('label').html().trim() + ' should be of less than 1000 characters.')
                        $(this).focus();
                        GCount++;
                        return false;
                    }


                });


                $('.KeyO').each(function () {
                    if ($(this).val().length > 2500) {
                        alert($(this).prev('label').html().trim() + ' should be of less than 2500 characters.')
                        $(this).focus();
                        OCount++;
                        return false;
                    }

                });


                if (GCount == 0 && OCount == 0) {
                    ShowProgress();
                }
                else {


                    return false;
                }
            });


            try {
                $("#txteffectivedate").datepicker({
                    defaultDate: "+1d",
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1,
                    dateFormat: 'dd-MM-yy'
                });
                $("#txtexpirationdate").datepicker({
                    defaultDate: "+1d",
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1,
                    dateFormat: 'dd-MM-yy'
                });
                $("#txtrenewaldate").datepicker({
                    defaultDate: "+1d",
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1,
                    dateFormat: 'dd-MM-yy'
                });

            }
            catch (e) {
                alert(e);
            }

        });
    </script>
    <script type="text/javascript">
        $("#requesttab").addClass("selectedtab");
    </script>
    <h2>
        Key Obligations for request #<asp:Label ID="lblRequestIDheader" runat="server"></asp:Label>
    </h2>
    <div>
        <div id="tblContainer">
            <p>
                <a href='RequestFlow.aspx?RequestId=<%=ViewState["RequestId"].ToString() %>&Status=KeyObligation'
                    class="issue status-1 priority-4 parent">Request #<%=ViewState["RequestId"].ToString() %></a>:
                Key Obligations for request #<asp:Label ID="lblReqID" runat="server"></asp:Label>
            </p>
        </div>
    </div>
    <table id="keyObligationTable" runat="server" width="100%">
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Liability Cap
                </label>
            </td>
            <td>
                <textarea id="txtLiabilityCap" class="KeyG" runat="server" maxlength="1000" clientidmode="static"
                    type="text" autocomplete="false" style="width: 280px; height: 40px;" rows="10"
                    onclick="return txtLiabilityCap_onclick()" />
                <em></em>
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Liability for Indirect/Consequential Losses
                </label>
            </td>
            <td>
                <span style="display:none">
                    <input id="rdIndirectConsequentialLossesYes" name="IsIndirectConsequentialLosses"
                        runat="server" type="radio" class="required" />Yes
                    <input id="rdIndirectConsequentialLossesNo" name="IsIndirectConsequentialLosses"
                        runat="server" type="radio" class="required" />No <a href="#" class="clsToolTip"
                            onclick="a1Onclick(this)" data-toggle="popover" data-placement="right" data-container="body"
                            role="button" clientidmode="Static" id="aIndirectConsequentialLosses" runat="server"
                            visible="false" data-clipboard-text="1">
                            <div class="information">
                                !</div>
                        </a>
                        </span>
                <textarea id="txtIndirectConsequentialLosses" class="KeyG" runat="server" maxlength="1000"
                    clientidmode="static" type="text" autocomplete="false" style="width: 280px; height: 40px"
                    rows="10" />
                <input id="hdnIndirectConsequentialLosses" runat="server" type="hidden" clientidmode="Static"
                    value="" />
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Indemnity
                </label>
            </td>
            <td>
                <input id="rdIndemnityYes" name="IsIndemnity" runat="server" type="radio" class="required" />Yes
                <input id="rdIndemnityNo" name="IsIndemnity" runat="server" type="radio" class="required" />No
                <a href="#" class="clsToolTip" data-clipboard-text="1" id="aIndemnity" runat="server"
                    onclick="a1Onclick(this)" data-toggle="popover" data-placement="right" data-container="body"
                    role="button" clientidmode="Static" visible="false">
                    <div class="information"><span>!</span></div>
                </a>
                <%--<a href="#" class="clsToolTip" data-clipboard-text="1" id="aIndemnity" runat="server"
                    onclick="aOnclick(this)" data-toggle="tooltip" data-placement="right" clientidmode="Static"
                    visible="false">
                    <div class="information">
                        !</div>
                </a>--%>
                <input id="hdnIndemnity" runat="server" type="hidden" clientidmode="Static" value="" />
            </td>
        </tr>
        <tr style="display: none;">
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Indemnity
                </label>
            </td>
            <td>
                <textarea id="txtIndemnity" class="KeyG" runat="server" maxlength="1000" clientidmode="static"
                    type="text" autocomplete="false" style="width: 280px; height: 40px" rows="10" />
                <em></em>
            </td>
        </tr>
        <tr style="display: none;">
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Warranties
                </label>
            </td>
            <td>
                <input id="rdWarrantiesYes" name="IsWarranties" runat="server" type="radio" class="required" />Yes
                <input id="rdWarrantiesNo" name="IsWarranties" runat="server" type="radio" class="required" />No
                <a href="#" class="clsToolTip" data-clipboard-text="1" id="aWarranties" runat="server"
                    onclick="a1Onclick(this)" data-toggle="popover" data-placement="right" data-container="body"
                    role="button" clientidmode="Static" visible="false">
                    <div class="information">
                        !</div>
                </a>
                <input id="hdnWarranties" runat="server" type="hidden" clientidmode="Static" value="" />
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Change of Control
                </label>
            </td>
            <td>
                <input id="rdIsChangeofControlYes" name="IsChangeofControl" runat="server" type="radio"
                    class="required" />Yes
                <input id="rdIsChangeofControlNo" name="IsChangeofControl" runat="server" type="radio"
                    class="required" />No
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Entire agreement clause
                </label>
            </td>
            <td>
                <input id="rdIsAgreementClauseYes" name="IsAgreementClause" runat="server" type="radio"
                    class="required" />Yes
                <input id="rdIsAgreementClauseNo" name="IsAgreementClause" runat="server" type="radio"
                    class="required" />No <a href="#" class="clsToolTip" data-clipboard-text="1" id="aAgreementClause"
                        onclick="a1Onclick(this)" data-toggle="popover" data-placement="right" data-container="body"
                        role="button" runat="server" visible="false">
                        <div class="information">
                            !</div>
                    </a>
                <input id="hdnAgreementClause" runat="server" type="hidden" clientidmode="Static"
                    value="" />
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Strict liability
                </label>
            </td>
            <td>
                <input id="rdIsStrictliabilityYes" name="IsStrictliability" runat="server" type="radio"
                    class="required" />Yes
                <input id="rdIsStrictliabilityNo" name="IsStrictliability" runat="server" type="radio"
                    class="required" />No
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Strict liability description
                </label>
            </td>
            <td>
                <textarea id="txtStrictliability" runat="server" maxlength="1000" clientidmode="static"
                    type="text" autocomplete="false" style="width: 280px; height: 40px" rows="10" />
                <em></em>
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Third party guarantee required
                </label>
            </td>
            <td>
                <input id="rdIsGuaranteeRequiredYes" name="IsGuaranteeRequired" runat="server" type="radio"
                    class="required" />Yes
                <input id="rdIsGuaranteeRequiredNo" name="IsGuaranteeRequired" runat="server" type="radio"
                    class="required" />No <a href="#" class="clsToolTip" clientidmode="Static" data-clipboard-text="1"
                        onclick="a1Onclick(this)" data-toggle="popover" data-placement="right" data-container="body"
                        role="button" id="aGuaranteeRequired" runat="server" visible="false">
                        <div class="information">
                            !</div>
                    </a>
                <input id="hdnGuaranteeRequired" runat="server" type="hidden" clientidmode="Static"
                    value="" />
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Insurance
                </label>
            </td>
            <td>
                <textarea id="txtInsurance" class="KeyG" runat="server" maxlength="1000" clientidmode="static"
                    type="text" autocomplete="false" style="width: 280px; height: 40px" rows="10" />
                <em></em>
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Termination
                </label>
            </td>
            <td>
                <textarea id="txtTerminationdesc" class="KeyG" runat="server" maxlength="1000" clientidmode="static"
                    type="text" autocomplete="false" style="width: 280px; height: 40px" rows="10" />
                <em></em>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <input type="checkbox" id="chkIsTerminationConvenience" clientidmode="Static" onclick="AvoidReadonly(1);"
                    runat="server" />Termination for convenience<br />
                <input type="checkbox" id="chkIsTerminationmaterial" clientidmode="Static" onclick="AvoidReadonly(2);"
                    runat="server" />Termination for breach<br />
                <input type="checkbox" id="chkIsTerminationinsolvency" clientidmode="Static" onclick="AvoidReadonly(3);"
                    runat="server" />Termination for insolvency<br />
                <input type="checkbox" id="chkIsTerminationinChangeofcontrol" clientidmode="Static"
                    runat="server" onclick="AvoidReadonly(4);" />Termination for change of control<br />
                <input type="checkbox" id="chkIsTerminationothercauses" clientidmode="Static" runat="server"
                    onclick="AvoidReadonly(4);"  style="display:none"  /><%--Termination for other reasons--%>
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Assignment/Novation/Subcontract</label>
            </td>
            <td>
                <textarea id="txtNovation" class="KeyG" runat="server" maxlength="1000" clientidmode="static"
                    type="text" autocomplete="false" style="width: 280px; height: 40px" rows="10" />
                <em></em>
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Force Majeure
                </label>
            </td>
            <td>
                <input id="rdForceMajeureYes" name="IsForceMajeure" runat="server" type="radio" class="required" />Yes
                <input id="rdForceMajeureNo" name="IsForceMajeure" runat="server" type="radio" class="required" />No
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Force Majeure Description
                </label>
            </td>
            <td>
                <textarea id="txtForceMajeure" class="KeyG" runat="server" maxlength="1000" clientidmode="static"
                    type="text" autocomplete="false" style="width: 280px; height: 40px" rows="10" />
                <em></em>
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Set off rights
                </label>
            </td>
            <td>
                <input id="rdSetOffRightsYes" name="IsSetOffRights" runat="server" type="radio" class="required" />Yes
                <input id="rdSetOffRightsNo" name="IsSetOffRights" runat="server" type="radio" class="required" />No
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Set off rights Description
                </label>
            </td>
            <td>
                <textarea id="txtSetOffRights" class="KeyG" runat="server" maxlength="1000" clientidmode="static"
                    type="text" autocomplete="false" style="width: 280px; height: 40px" rows="10" />
                <em></em>
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Governing Law / Applicable Law
                </label>
            </td>
            <td>
                <input id="rdGoverningLawYes" name="IsGoverningLaw" runat="server" type="radio" class="required" />Yes
                <input id="rdGoverningLawNo" name="IsGoverningLaw" runat="server" type="radio" class="required" />No
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Governing Law / Applicable Law Description
                </label>
            </td>
            <td>
                <textarea id="txtGoverningLaw" class="KeyG" runat="server" maxlength="1000" clientidmode="static"
                    type="text" autocomplete="false" style="width: 280px; height: 40px" rows="10" />
                <em></em>
            </td>
        </tr>
        <tr>
            <td class="tdAlign">
                <label for="time_entry_issue_id">
                    Others</label>
            </td>
            <td>
                <textarea id="txtOthers" class="KeyO" runat="server" maxlength="2500" clientidmode="static"
                    type="text" autocomplete="false" style="width: 280px; height: 40px" rows="10" />
                <em></em>
            </td>
        </tr>
    </table>
    <div class="box tabularr" style="padding-left: 60px; height: 158px;">
        <p>
            <asp:PlaceHolder ID="pnlnewadd" runat="server"></asp:PlaceHolder>
        </p>
        <br />
        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" class="submit" />
        <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="Back" />
    </div>
    <script type="text/javascript" language="javascript">
        function AvoidReadonly(ctl) {
            if (ctl == 1) {
                if ($("#chkIsTerminationConvenience").is(':checked'))
                    $('#txtTerminationConvenience').prop('readonly', false);
                else
                    $('#txtTerminationConvenience').prop('readonly', true);
            }

            if (ctl == 2) {
                if ($("#chkIsTerminationmaterial").is(':checked'))
                    $('#txtTerminationmaterial').prop('readonly', false);
                else
                    $('#txtTerminationmaterial').prop('readonly', true);
            }

            if (ctl == 3) {
                if ($("#chkIsTerminationinsolvency").is(':checked'))
                    $('#txtTerminationinsolvency').prop('readonly', false);
                else
                    $('#txtTerminationinsolvency').prop('readonly', true);
            }

            if (ctl == 4) {
                if ($("#chkIsTerminationinChangeofcontrol").is(':checked'))
                    $('#txtTerminationinChangeofcontrol').prop('readonly', false);
                else
                    $('#txtTerminationinChangeofcontrol').prop('readonly', true);
            }
        }

        function CheckDateStatus(ctl) {
            if (ctl == "Renewal") {
                if ($("#txtrenewaldate").val() == "") {
                    alert('Please insert renewal date ');
                    $('#chkrem30').prop('checked', false);
                    $('#chkrem7').prop('checked', false);
                    $('#chkrem24').prop('checked', false);
                }

            }
            else {
                if ($("#txtexpirationdate").val() == "") {
                    alert('Please insert expiration date ');
                    $('#chkexp30').prop('checked', false);
                    $('#chkexp7').prop('checked', false);
                    $('#chkexp24').prop('checked', false);
                }
            }
        }

        function txtLiabilityCap_onclick() {
        }
    </script>
    <asp:HiddenField ID="HdnRequestID" runat="server" />
    <asp:HiddenField ID="hdnLinkStatus" runat="server" ClientIDMode="Static" />
    <input type="hidden" id="hdnvale" runat="server" clientidmode="Static" value="adasd" />
    <script type="text/javascript">
        var aId = '', clickId = '';
        function a1Onclick(obj) {
            clickId = '1';
            var data = '';
            if ($(obj).next('input').val() != undefined) {
                data = $(obj).next('input').val();
            }
            else {
                data = $(obj).next().next('input').val();
            }

            if (aId == '' || $(obj).attr('id') != aId) {
                aId = $(obj).attr('id');
                removepopoverp();
            }
            var t = false;
            $("div").each(function () {
                if ($(this).hasClass('popover fade right in')) {
                    t = true;
                }
            });

            if (t == true) {
                $(obj).attr('data-content', data).popover('show');
            }
            else
                $(obj).attr('data-content', data).popover('hide');
        }

        function removepopoverp() {
            $("div").each(function () {
                if ($(this).hasClass('popover fade right in')) {
                    $(this).remove();
                }
            });
        }
        $('#wrapper').click(function (e) {
            if (clickId != '1') {
                removepopoverp();
            }
            clickId = '';
        });
    </script>
    <script type="text/javascript">
        //        //        var timeoutId = 0, aId = '';
        //        //        $('.clsToolTip').tooltip({
        //        //            trigger: 'click',
        //        //            placement: 'right'
        //        //        });
        //        function setTooltip(btn, message) {
        //            //btn.tooltip('hide').attr('data-original-title', message).tooltip('show');
        //            //btn.attr('data-original-title', message).tooltip('show');
        //            btn.attr('data-original-title', message).tooltip('toggle');
        //        }
        //        function hideTooltip(btn) {
        //            //alert(timeoutId)
        //            //timeoutId = 0;
        //            timeoutId = setTimeout(function () {
        //                btn.tooltip('hide');
        //            }, 5000);
        //            //alert(timeoutId)
        //        }
        //        // Clipboard
        //        //        var clipboard = new Clipboard('.clsToolTip');
        //        //        clipboard.on('success', function (e) {
        //        //            var btn = $(e.trigger);
        //        //            
        //        ////            if (btn.next('div').hasClass('tooltip fade right in') == false && (aId == '' || btn.attr('id') == aId)) {
        //        ////                aId = btn.attr('id');
        //        ////                clearTimeout(timeoutId);
        //        ////                btn.tooltip('hide');
        //        ////                //alert(btn.next('div').hasClass('tooltip fade right in'))
        //        ////            }
        //        //            //alert(btn.next('div').hasClass('tooltip fade right in'))
        //        //            removeTooltip(btn);
        //        //            var data = '';
        //        //            if (btn.next('input').val() != undefined) {
        //        //                data = btn.next('input').val();
        //        //            }
        //        //            else {
        //        //                data = btn.next().next('input').val();
        //        //            }
        //        //            //
        //        //            //            alert(timeoutId)
        //        //            debugger;
        //        //            //alert(btn.tooltip('hide').is(":hidden"));
        //        //            //alert(btn.next('div').hasClass('tooltip fade right in'));
        //        //            //            if (btn.next('div').hasClass('tooltip fade right in') == true) {

        //        //            //            }

        //        //            //            alert(btn.attr('id'));
        //        //            //            alert(aId);
        //        //            //if (aId == '' || btn.attr('id') != aId) {
        //        //            //aId = btn.attr('id');
        //        //            setTooltip(btn, data);
        //        ////            hideTooltip(btn);
        //        ////            e.clearSelection();
        //        //            //            } else {
        //        //            //                clearTimeout(timeoutId);
        //        //            //                //btn.tooltip('hide');
        //        //            //                setTooltip(btn, data);
        //        //            //            }
        //        //            //} else {
        //        //            //btn.tooltip('hide');
        //        //            //}
        //        //            
        //        //        });

        //        function removeTooltip(btn) {
        //            $("div").each(function () {
        //                if ($(this).hasClass('tooltip fade right in')) {
        //                    $(this).removeClass('in');
        //                }
        //            });
        //        }

        //        function aOnclick(obj) {
        //            var data = '';
        //            if ($(obj).next('input').val() != undefined) {
        //                data = $(obj).next('input').val();
        //            }
        //            else {
        //                data = $(obj).next().next('input').val();
        //            }
        //            //$(obj).attr('data-original-title', data).tooltip('show');
        //            $(obj).attr('data-original-title', data).tooltip('toggle');
        //            //$(obj).attr('data-original-title', data).popover('toggle');
        //        }
    </script>
</asp:Content>
