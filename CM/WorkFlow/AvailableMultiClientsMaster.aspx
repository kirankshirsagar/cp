﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="AvailableMultiClientsMaster.aspx.cs" Inherits="WorkFlow_AvailableMultiClientsMaster"
    ValidateRequest="false" %>

<%@ Register Src="~/UserControl/Requestdetaillinks.ascx" TagName="Requestlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
    .validation
        {
            background: White;
            color: red;
            padding-left: 5px;
            padding-right: 5px;
            height: 24px;
            line-height: 24px;
            font-size: 15px;
            font-style: normal;
            margin-top: 8px;
           /* float: left;
            border: solid 1px silver;*/
          z-index: 99;
          filter:alpha(opacity=50); opacity:0.5;
        }
      </style>
    <style type="text/css">
        .style1
        {
            height: 244px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <input id="hdnStateId" clientidmode="Static" runat="server" type="hidden" />
    <input id="hdnStateIdRead" clientidmode="Static" runat="server" type="hidden" />
    <input id="hdnCityId" clientidmode="Static" runat="server" type="hidden" />
    <input id="hdnCityIdRead" clientidmode="Static" runat="server" type="hidden" />
    <asp:HiddenField ID="hdnStateName" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnCityName" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnCountryName" runat="server" ClientIDMode="Static" />
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
    </h2>
    <asp:ScriptManager ID="scrpmgr1" runat="server">
    </asp:ScriptManager>
   <div style="padding-left:120px">
    <div class="box tabular">
     <div id="errorExplanation" runat="server">
                        </div>
        <table id="Table1" width="70%" runat="server" align="center" style="padding-left:120px">
            <tr>
                <td >
                    <label for="time_entry_issue_id">
                        <span class="required">*</span>Customer or Supplier or Others</label>
                    <asp:RadioButton ID="rdoCustomer" GroupName="radiobutton1" ClientIDMode="Static" runat="Server" />Customer
                    <asp:RadioButton ID="rdoSupplier"  GroupName="radiobutton1" ClientIDMode="Static" runat="Server" />Supplier 
                    <asp:RadioButton ID="rdoOthers"  GroupName="radiobutton1" ClientIDMode="Static" runat="Server" />Others 
                    <em>
                    </em>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="time_entry_issue_id">
                        <span class="required">*</span>Customer/Supplier/Others Name</label>
                        <asp:TextBox ID="txtClientName" runat="server" style="width: 220px;" maxlength="50" ClientIDMode="Static" CssClass="txtCharNumberPaste txtCharNumber"></asp:TextBox>
                    <br />
                    <em>
                     &nbsp;&nbsp;   <asp:RequiredFieldValidator ID="reqclient" runat="server" Display="Dynamic" ControlToValidate="txtClientName"  CssClass="validation"
                     ClientIDMode="Static" ErrorMessage="This field is required." Font-Italic="false" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </em>
                </td>
            </tr>
        </table>
        <fieldset>
            <legend style="color: Black;">Address Details</legend>
            <table id="Table3" width="70%" runat="server" align="center" style="padding-left:120px">
                <tr>
                    <td>
                        <asp:HiddenField ID="hdnAddrStatus" runat="server" />
                        <asp:HiddenField ID="hdnAddredetails" runat="server" />
                        <asp:HiddenField ID="hdnStreetdetails" runat="server" />
                        <table id="Table2" width="100%" runat="server" align="center">
                            <tr>
                                <td>
                                    <label for="time_entry_issue_id">
                                        <span class="required">*</span>Street1</label>
                                        <asp:TextBox ID="txtAddress" runat="server" class="required myclass" MaxLength="50"
                                       Style="width: 220px" size="32" aria-autocomplete="list" aria-haspopup="true"
                                        autocomplete="off"></asp:TextBox>
                                        <br />
                                    <em>
                                        <asp:RequiredFieldValidator ID="reqAddr" runat="server" Display="Dynamic" ControlToValidate="txtAddress" CssClass="validation" 
                                            ErrorMessage="This field is required." Font-Italic="false" ValidationGroup="AddrSave"></asp:RequiredFieldValidator>
                                    </em>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <label for="time_entry_issue_id"> Street2</label>
                                        <asp:TextBox ID="txtStreet" runat="server" class="required myclass" MaxLength="50"
                                        Style="width: 220px" size="32" aria-autocomplete="list" aria-haspopup="true"
                                        autocomplete="off"></asp:TextBox>
                                                                     
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="time_entry_activity_id">
                                        <span class="required">*</span>Country
                                    </label>
                                    <select id="ddlCountry" style="width: 220px" class="chzn-select required chzn-select"
                                        clientidmode="Static" onchange="setStateValue()"
                                        runat="server">
                                        <option></option>
                                    </select>
                                    <br />
                                    <em>
                                        <asp:CustomValidator ID="CustomValidator3" runat="server" ClientValidationFunction="ValidateCountry"
                                            CssClass="failureNotification validation" ErrorMessage="Please select any option." 
                                            Font-Italic="false" Display="Dynamic" ValidationGroup="AddrSave"></asp:CustomValidator>
                                    </em>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="time_entry_activity_id">
                                        <span class="required">*</span>State
                                    </label>
                                      <asp:TextBox ID="ddlState" runat="server" class="required myclass" MaxLength="50"
                                       Style="width: 220px" size="32" aria-autocomplete="list" aria-haspopup="true"
                                        autocomplete="off"></asp:TextBox>
                                    <br />
                                    <em>
                                        <asp:RequiredFieldValidator ID="reqState" runat="server" Display="Dynamic" ControlToValidate="ddlState"  CssClass="validation"
                                            Font-Italic="false" ErrorMessage="This field is required."
                                            ValidationGroup="AddrSave"></asp:RequiredFieldValidator>
                                    </em>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="time_entry_activity_id">
                                        <span class="required">*</span>City
                                    </label>
                                       <asp:TextBox ID="ddlCity" runat="server" class="required myclass" MaxLength="25"
                                       Style="width: 220px" size="32" aria-autocomplete="list" aria-haspopup="true"
                                        autocomplete="off"></asp:TextBox>
                                    <br />
                                    <em>
                                        <asp:RequiredFieldValidator ID="reqCity" runat="server" Display="Dynamic" ControlToValidate="ddlCity"  CssClass="validation"
                                            Font-Italic="false" ErrorMessage="This field is required."
                                            ValidationGroup="AddrSave"></asp:RequiredFieldValidator>
                                    </em>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="time_entry_activity_id">
                                        <span class="required">*</span>Zip code/Postcode 
                                    </label>
                                    <asp:TextBox ID="txtPinCode" runat="server" class="required myclass"
                                        MaxLength="8" Style="width: 220px" size="32" aria-autocomplete="list" aria-haspopup="true"
                                        autocomplete="off"></asp:TextBox>
                                    <asp:LinkButton ID="btnAddress" runat="server" Text="Add Address" ValidationGroup="AddrSave"
                                         CssClass="lnkAdd" OnClick="btnAddress_Click" />
                                        <br />
                                    <em>
                                        <asp:RequiredFieldValidator ID="reqpincode" runat="server" Display="Dynamic" ControlToValidate="txtPinCode"  CssClass="validation"
                                            Font-Italic="false" ErrorMessage="This field is required." ValidationGroup="AddrSave"></asp:RequiredFieldValidator>
                                    </em>
                                </td>
                            </tr>
                            <tr id="trAddress" runat="server">
                                <td style="font-family: Verdana; font-size: 8pt" valign="top">
                                    <asp:DataList ID="dtlAddress" runat="server" RepeatDirection="Vertical" Style="margin-top: 4px"
                                        GridLines="Both" Width="400px" DataKeyField="Address" OnCancelCommand="dtlAddress_CancelCommand"
                                        OnEditCommand="dtlAddress_EditCommand" OnUpdateCommand="dtlAddress_UpdateCommand"
                                        OnDeleteCommand="dtlAddress_DeleteCommand" OnItemDataBound="dtlAddress_ItemDataBound">
                                        <ItemTemplate>
                                            <table width="100%" cellpadding="2" cellspacing="2">
                                                <tr>
                                                    <td align="left" style="width: 20%" valign="top">
                                                        <asp:Label ID="lblAddress" runat="server" Text='<%#Eval("Address") %>'></asp:Label>
                                                    </td>
                                                     <td align="left" style="width: 20%" valign="top">
                                                        <asp:Label ID="lblStreet" runat="server" Text='<%#Eval("Street") %>'></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%" valign="top">
                                                        <asp:Label ID="lblCountry" runat="server" Text='<%#Eval("CountryName") %>'></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%" valign="top">
                                                        <asp:Label ID="lblStateName" runat="server" Text='<%#Eval("StateName") %>'></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 20%" valign="top">
                                                        <asp:Label ID="lblCityName" runat="server" Text='<%#Eval("CityName") %>'></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 10%" valign="top">
                                                        <asp:Label ID="lblPincode" runat="server" Text='<%#Eval("Pincode") %>'></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 2%" valign="top">
                                                        <asp:Label ID="lblCountryID" runat="server" Text='<%#Eval("CountryID") %>'></asp:Label>
                                                    </td>
                                            
                                                    <td style="width: 3%" valign="top">
                                                        <div>
                                                            <asp:ImageButton ImageUrl="../Images/edit.png" runat="Server" ID="ImageButton2" ToolTip="Edit"
                                                               OnClientClick="javascript:return GetEditedItems(this);" Text="Edit" CommandName="Edit" CausesValidation="false" />
                                                        </div>
                                                    </td>
                                                    <td style="width: 2%" valign="top">
                                                        <div>
                                                           <%-- <asp:ImageButton ImageUrl="../Images/icon-del.jpg" runat="Server" ID="ImgbtnDelete"
                                                                ToolTip="Delete" Text="Delete" CommandName="Delete" CausesValidation="false"
                                                                OnClientClick='javascript:return confirm("Do you want to delete?");' />--%>
                                                                <asp:LinkButton runat="Server" ID="ImgbtnDelete" CssClass="icon icon-del" ToolTip="Delete"
                                                                    CommandName="Delete" OnClientClick='javascript:return GetdeletedItems(this);' />
                                                                <input type="hidden" id="hdnIsUsed" runat="server" clientidmode="Static" value='<%#Eval("IsUsed") %>' />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:DataList>
                                    <asp:Label ID="lblError" runat="server" CssClass="validation"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </fieldset>
        <fieldset>
            <legend style="color: Black; border-color: Blue">Contact Details</legend>
            <table id="Table33" width="70%" runat="server" align="center" style="padding-left:120px">
                <tr>
                    <td>
                        <label for="time_entry_activity_id">
                            <span class="required">* </span>Contact Number
                        </label>
                        <asp:TextBox ID="txtMobile" runat="server" class="required myclass PhoneNumberTxtNumSplChar PhoneNumberTxtNumSplCharPaste" MaxLength="20"
                            Style="width: 220px" TabIndex="13"></asp:TextBox>
                        <asp:LinkButton ID="btnAddMobile" runat="server" Text="Add Contact Number " ValidationGroup="ContactSave"
                             CssClass="lnkAdd" OnClick="btnAddMobile_Click" />
                            <br />
                        <em>
                            <asp:RequiredFieldValidator ID="reqContact" runat="server" Display="Dynamic" ControlToValidate="txtMobile"  CssClass="validation"
                           Font-Italic="false"  ErrorMessage="This field is required." ValidationGroup="ContactSave"></asp:RequiredFieldValidator>
                        </em>
                    </td>
                </tr>
                <tr>
                    <td style="font-family: Verdana; font-size: 8pt" colspan="2" valign="top">
                        <asp:HiddenField ID="hdnContactStatus" runat="server" />
                        <asp:HiddenField ID="hdnContactNo" runat="server" />
                        <asp:DataList ID="dtlMobile" runat="server" RepeatDirection="Vertical" Style="margin-top: 4px"
                            GridLines="Both" Width="400px" DataKeyField="ContactNo" OnCancelCommand="dtlMobile_CancelCommand"
                            OnEditCommand="dtlMobile_EditCommand" OnUpdateCommand="dtlMobile_UpdateCommand"
                            OnDeleteCommand="dtlMobile_DeleteCommand" OnItemDataBound="dtlMobile_ItemDataBound">
                            <ItemTemplate>
                                <table width="100%" cellpadding="2" cellspacing="2">
                                    <tr>
                                        <td align="left" style="width: 90%" valign="top">
                                            <asp:Label ID="lblContact" runat="server" Text='<%#Eval("ContactNo") %>'></asp:Label>
                                        </td>
                                        <td style="width: 8%; text-align: right" valign="top">
                                            <div>
                                                <asp:ImageButton ImageUrl="../Images/edit.png" runat="Server" ID="ImageButton2" ToolTip="Edit"
                                                    OnClientClick="javascript:return GetEditedItems(this);" Text="Edit" CommandName="Edit" CausesValidation="false" />
                                            </div>
                                        </td>
                                        <td style="width: 2%" valign="top">
                                            <div>
                                              <%--  <asp:ImageButton ImageUrl="../Images/icon-del.jpg" runat="Server" ID="ImgbtnDelete"
                                                    ToolTip="Delete" Text="Delete" CommandName="Delete" CausesValidation="false"
                                                    OnClientClick='javascript:return confirm("Do you want to delete?");' />--%>
                                                    <asp:LinkButton runat="Server" ID="ImgbtnDelete" CssClass="icon icon-del" ToolTip="Delete"
                                                        CommandName="Delete" OnClientClick='javascript:return GetdeletedItems(this);' />
                                                    <input type="hidden" id="hdnIsUsed" runat="server" clientidmode="Static" value='<%#Eval("IsUsed") %>' />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:Label ID="lblMobileError" runat="server" CssClass="validation"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HiddenField ID="hdnEmailDetail" runat="server" />
                        <asp:HiddenField ID="hdnIsUsedStatus" runat="server" />
                        <asp:HiddenField ID="hdnEmailStatus" runat="server" />
                        <label for="time_entry_issue_id">
                            <span class="required">*</span>Email ID</label>
                        <input id="txtEmailID" runat="server" type="text" class="required email" style="width: 220px;"
                            maxlength="50 " autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" />
                        <asp:LinkButton ID="btnAddEmail" runat="server" Text="Add Email ID" ValidationGroup="EmailSave"
                             CssClass="lnkAdd" OnClick="btnAddEmail_Click" />
                        <asp:Label ID="lblemailError" runat="server" CssClass="validation"></asp:Label>
						<br />	
                        <em>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                Font-Italic="false" ControlToValidate="txtEmailID" ErrorMessage="This field is required."  CssClass="validation"
                                ValidationGroup="EmailSave"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" CssClass="failureNotification validation" 
                                ControlToValidate="txtEmailID" runat="server" ErrorMessage="Invalid Email." Text="Invalid Email"
                                Font-Italic="false" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                ValidationGroup="EmailSave"></asp:RegularExpressionValidator>
                        </em>
                    </td>
                </tr>
                <tr>
                    <td style="font-family: Verdana; font-size: 8pt" colspan="2" valign="top">
                        <asp:DataList ID="dtlEmail" runat="server" RepeatDirection="Vertical" Style="margin-top: 4px"
                            GridLines="Both" Width="400px" DataKeyField="EmailID" OnCancelCommand="dtlEmail_CancelCommand"
                            OnEditCommand="dtlEmail_EditCommand" OnUpdateCommand="dtlEmail_UpdateCommand"
                            OnDeleteCommand="dtlEmail_DeleteCommand" OnItemDataBound="dtlEmail_ItemDataBound">
                            <ItemTemplate>
                                <table width="100%" cellpadding="2" cellspacing="2">
                                    <tr>
                                        <td align="left" style="width: 90%" valign="top">
                                            <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("EmailID") %>'></asp:Label>
                                        </td>
                                        <td style="width: 8%; text-align: right" valign="top">
                                            <div>
                                                <asp:ImageButton ImageUrl="../Images/edit.png" runat="Server" ID="ImageButton2" ToolTip="Edit"
                                                    OnClientClick="javascript:return GetEditedItems(this);" Text="Edit" CommandName="Edit" CausesValidation="false" />
                                            </div>
                                        </td>
                                        <td style="width: 2%" valign="top">
                                            <div>
                                                <%--<asp:ImageButton ImageUrl="../Images/icon-del.jpg" runat="Server" ID="ImgbtnDelete"
                                                    ToolTip="Delete" Text="Delete" CommandName="Delete" CausesValidation="false"
                                                    OnClientClick='javascript:return confirm("Do you want to delete?");' />--%>
                                                     <asp:LinkButton runat="Server" ID="ImgbtnDelete" CssClass="icon icon-del" ToolTip="Delete"
                                                        CommandName="Delete" OnClientClick='javascript:return GetdeletedItems(this);' />
                                                    <input type="hidden" id="hdnIsUsed" runat="server" clientidmode="Static" value='<%#Eval("IsUsed") %>' />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>                        
                    </td>
                </tr>
            </table>
        </fieldset>
        <br />
        <table id="Table322" runat="server" >
            <tr>
            <td>
              <asp:LinkButton ID="btnSave" runat="server"  Text="Save" OnClick="btnSave_Click" ValidationGroup="Save"
                        ForeColor="White" Style="text-decoration: none" ClientIDMode="Static" CssClass="linkbutton" />
                  
                    <asp:LinkButton ID="btnSaveAndContinue" runat="server" Text="Save and Add more" ValidationGroup="Save"
                        ForeColor="White" Style="text-decoration: none" CssClass="linkbutton" OnClick="btnSaveAndContinue_Click" />
                    <asp:LinkButton ID="btnBack" runat="server" Text="Back" OnClientClick="return CallCancel(true)"
                        ForeColor="White" Style="text-decoration: none" CssClass="linkbutton" CausesValidation="true" />
                </td>
                <td>
                </td>
            </tr>
        </table>
       </div>
  </div>
    <div id="rightlinks" style="display: none;">
        <uc1:Requestlinks ID="RequestLinksID" runat="server" />
    </div>
    <script type="text/javascript">
        $("#requestLink").addClass("menulink");
        $("#requesttab").addClass("selectedtab");
        $("#sidebar").html($("#rightlinks").html());
        function GetdeletedItems(obj) {
            if ($(obj).next().val() == 'Y') {
                MessageMasterDiv('Records can not be deleted.', 1);
                return false;
            } else {
                return confirm("Do you want to delete?");
            }
        }
        function GetEditedItems(obj) {
            //alert($(obj).closest('td').next('td').find('input').val());
            if ($(obj).closest('td').next('td').find('input').val() == 'Y') {
                MessageMasterDiv('Records can not be edited.', 1);
                return false;
            } else {
                return true;
            }
        }
        function CallCancel() {
            location.href = 'AvailableClientsData.aspx';
            return false;
        }
    </script>
    <script type="text/javascript">
            function setStateValue() {
                $('#hdnStateId').val('');
                $('#hdnStateId').val($('#ddlState').val());
                var State = $("#ddlState option:selected").text();
                $('#hdnStateName').val(State);
                var Country = $("#ddlCountry option:selected").text();
                $('#hdnCountryName').val(Country);
                $('#hdnCityName').val(City);
                var City = $("#ddlCity option:selected").text();
                if (Country.indexOf('Select') > -1)
                    $("#MainContent_CustomValidator3").show();
                else
                    $("#MainContent_CustomValidator3").hide();
            }

            function setCityValue() {
                $('#hdnCityId').val('');
                $('#hdnCityId').val($('#ddlCity').val());
                var City = $("#ddlCity option:selected").text();
                $('#hdnCityName').val(City);
            }
            function setDafaultValues() {
                $('#ddlState').val($('#hdnStateId').val());
            }
            function setDafaulCitytValues() {
                $('#ddlCity').val($('#hdnCityId').val());
            }
            $(document).ready(function () {
            });

            function ValidateCountry(source, args) {
                var country = $('#ddlCountry').val();
                if (country == '0' || country == '') {
                    args.IsValid = false;
                }
                else {
                    args.IsValid = true;
                }
            }

            function GetSelectValue() {
                $('#ddlCountry').change();
                if ($('#hdnStateIdRead').val() != '') {
                    $('#ddlState').val($('#hdnStateIdRead').val());
                    $('#hdnStateId').val($('#hdnStateIdRead').val());
                }
                $('#ddlState').change();
                if ($('#hdnCityIdRead').val() != '') {
                    $('#ddlCity').val($('#hdnCityIdRead').val());
                    $('#hdnCityId').val($('#hdnCityIdRead').val());
                }
            }

            function HideShowValidation() {
                if ($("#txtClientName").val() == "") {
                    $("#reqclient").show();
                    return false;
                }
                else {
                    $("#reqclient").hide();
                }
            }
    </script>
</asp:Content>
