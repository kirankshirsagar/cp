﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true" CodeFile="AvailableClientsData.aspx.cs" Inherits="WorkFlow_AvailableClientsData" %>
<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/Requestdetaillinks.ascx" TagName="Requestlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
 <script src="../CommonScripts/GridSelect.js" type="text/javascript"></script>

    <script src="../CommonScripts/GridNavigation.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
 <script type="text/javascript">
     $("#requestLink").addClass("menulink");
     $("#requesttab").addClass("selectedtab");   
  </script>
   
    <h2>
        My Customers/Suppliers/Others</h2>
       
         <input id="hdnPrimeIds" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <div style="margin: 0; padding: 0; display: inline">
        <div id="query_form_content" class="hide-when-print">
            <fieldset id="filters" class="collapsible">
                <legend onclick="toggleFieldset(this);">Filters</legend>
                <div style="">
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td>
                                    <table id="filters-table" width="100%" cellpadding="3" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td width="10%">
                                                    Keywords :
                                                </td>
                                                <td width="90%">
                                                    <input id="txtSearch" clientidmode="Static" runat="server" type="text" maxlength="50" onkeydown = "return (event.keyCode!=13);" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </fieldset>
        </div>
        <p class="buttons hide-when-print">
            <asp:LinkButton ID="btnSearch" runat="server" OnClientClick="return searchClick();"
                CssClass="icon icon-checked" OnClick="btnSearch_Click">Filter</asp:LinkButton>
            <asp:LinkButton ID="btnShowAll" runat="server" OnClientClick="return resetClick();"
                CssClass="icon icon-reload" OnClick="btnSearch_Click">Reset Filter</asp:LinkButton>
            <asp:LinkButton ID="btnAddRecord" CssClass="icon icon-add" runat="server" OnClientClick="resetPrimeID();" OnClick="btnAddRecord_Click">Add new Customer/Supplier/Others</asp:LinkButton>
            <asp:LinkButton ID="btnDelete" CssClass="icon icon-del" runat="server" OnClientClick="return GetSelectedItems('D');"
                OnClick="btnDelete_Click">Delete</asp:LinkButton>
        </p>
           <div class="autoscroll">
            <table width="100%">
                <asp:Repeater ID="rptClientsMaster" runat="server" 
                    onitemcommand="rptClientsMaster_ItemCommand">
                    <HeaderTemplate>
                        <table id="masterDataTable" class="masterTable list issues" width="100%">
                            <thead>
                                <tr>
                                    <th style="text-align: left; padding-left: 2px;" width="1%">
                                        <input id="chkSelectAll" type="checkbox" class="checkbox maincheckbox" onkeydown = "return (event.keyCode!=13);" />
                                    </th>
                                    <th class="checkbox hide-when-print" width="3%">
                                        &nbsp;
                                    </th>
                                    <th width="0%" style="display: none;">
                                        Customer/Supplier /Others Id
                                    </th>
                                    <th width="20%">
                                      <asp:LinkButton ID="btnSortClientName" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Customer/Supplier /Others Name</asp:LinkButton>
                                        
                                    </th> 
                                    <th width="30%">
                                      <asp:LinkButton ID="btnSortAddress" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Address</asp:LinkButton>
                                    </th>
                                    <th width="15%">
                                     <asp:LinkButton ID="btnSortContactNo" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Contact Number(s)</asp:LinkButton>
                                        
                                    </th>
                                    <th width="15%">
                                         <asp:LinkButton ID="btnSortEmailIDs" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Email ID(s)</asp:LinkButton>
                                    </th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: left; padding-bottom: 2px">
                                <input type="checkbox" onchange="SetMainCheckbox(this);" class="mid-margin-left" onkeydown = "return (event.keyCode!=13);" />
                            </td>
                            <td style="text-align: left; padding-bottom: 2px">
                                <img id="imgLock" alt="img.." class="mws-tooltip-e" title="Used" border="0" />
                              
                            </td>
                            <td style="display: none">
                                <asp:Label ID="lblClientId" runat="server" ClientIDMode="Static" Text='<%#Eval("ClientId") %>'></asp:Label>
                                <asp:Label ID="lblIsUsed" runat="server" ClientIDMode="Static" Text='<%#Eval("isUsed") %>'></asp:Label>
                                <asp:Label ID="lblClientName" runat="server" ClientIDMode="Static" Text='<%#Eval("ClientName") %>'></asp:Label>
                            </td>
                            <td>
                               <asp:LinkButton ID="imgEdit" runat="server" style="display:block;" OnClientClick="return setSelectedId(this);"
                                    CommandName="Update"><%#Eval("ClientName")%></asp:LinkButton>
                                 
                            </td> 
                            <td>
                                <asp:Label ID="lblAddress" runat="server" ClientIDMode="Static" Text='<%#Eval("Address") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lclContactNo" runat="server" ClientIDMode="Static" Text='<%#Eval("ContactNo") %>'></asp:Label>
                            </td>
                             <td>
                                <asp:Label ID="lblEmailIDs" runat="server" ClientIDMode="Static" Text='<%#Eval("EmailID") %>'></asp:Label>
                            </td>
                            
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
            <asp:PlaceHolder runat="server" ID="Placeholder1">
                <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
            </asp:PlaceHolder>
            <script type="text/javascript">


                function GetSelectedItems(flg) {
                    $('#hdnPrimeIds').val('');
                    $('#hdnUsedNames').val('');
                    var tableClass = 'masterTable';
                    var deleteLabelId = 'lblClientId';
                    var deleteLabelName = 'lblClientName';
                    var objCheck = new CkeckBoxSelect(tableClass, deleteLabelId, deleteLabelName);
                    var deletedIds = objCheck.DeletedIds;
                    var usedNames = objCheck.UsedNames;

                    if (deletedIds == '') {
                        MessageMasterDiv('Please select record(s).');
                        return false;
                    }
                    else if (usedNames != '' && flg == 'D') {
                        MessageMasterDiv('Some records can not be deleted.' + usedNames, 1);
                        return false;
                    }
                    if (flg == 'D') {
                        if (DeleteConfrim() == true) {
                            $('#hdnPrimeIds').val(deletedIds);
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    else {
                        $('#hdnPrimeIds').val(deletedIds);
                        return true;
                    }

                    $('#hdnPrimeIds').val(deletedIds);
                    //            $('#hdnUsedNames').val(usedNames);
                    return true;
                }

             $(document).ready(function () {
                 defaultTableValueSetting('lblClientId', 'Client', 'ClientId');
                 LockUnLockImage('masterTable');
             });

        function setSelectedId(obj) {
            var pId = $(obj).closest('tr').find('#lblClientId').text();
            if (pId == '' || pId == '0') {
                
                return false;
            }
            else {
                $('#hdnPrimeIds').val(pId);
               
                return true;
            }
           
        }
         </script>
        </div>
      <div id="rightlinks" style="display: none;">
       <uc1:Requestlinks ID="RequestLinksID" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
</asp:Content>

