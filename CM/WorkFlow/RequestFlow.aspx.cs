﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;
using DocumentOperationsBLL;
using WorkflowBLL;
using System.Data;
using System.Data.SqlClient;
using EmailBLL;
using System.Configuration;
using System.Web.Services;
using System.Web.Security;
using ClauseLibraryBLL;
using System.IO;
using System.Xml;
using System.Threading;


public partial class Workflow_RequestFlow : System.Web.UI.Page
{
    IContractType objContract;
    IActivity objActivity;
    IDocuSign objDocuSign;
    IClauseLibrary ObjDocumentType;
    static string filename = "";
    IContractRequest req;
    string isCompleted = "";
    string fileName = "";
    #region Permission Access attributes
    public bool RequestEdit;
    public bool LoadQuestionnaireAccess;
    public bool DownloadContractAccess;
    public bool UploadFileAccess;
    public bool CheckOutAccess;
    public bool CheckOutSendMailAccess;
    public bool CheckInAccess;
    public bool TrackChangesAccess;
    public bool EditQuestionnaireAccess;
    public bool ElectronicSignatureAccess;
    public bool DeleteContractFileAccess;
    public static bool TabPDFDocumentAccess;

    public bool TabActivityViAccess;

    public string TabDocumentViewAccess;
    public bool TabDocumentUploadAccess;
    public static bool TabDocumentDownloadAccess;
    public static bool TabDocumentDeleteAccess;

    public string Update;
    #endregion
    public string ContractTypeMessage { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        ddlDocumentType.extDataBind(ObjDocumentType.SelectDocumentType());
        //ExecuteEmailUpload();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            setAccessValues();
            ContractTypeBind();
            ViewState["RequestId"] = Request.QueryString["RequestId"];

            if (Request.QueryString["Sender"] != null)
            {
                if (Request.QueryString["Sender"].Equals("DocSearch") || Request.QueryString["Sender"].Equals("MetaSearch"))
                {
                    ShowHideButtons();
                }
            }

            ViewState["IsRequestedFromVault"] = Request.QueryString["IsRequestedFromVault"];
            hdnUserID.Value = Session[Declarations.User].ToString();

            BindRequestDetails();
            ShowHidePanels();
            BindDocuSignStatus();

            if (!string.IsNullOrEmpty(Request.QueryString["ParentRequestID"]) && Convert.ToString(Request.QueryString["ParentRequestID"]) != "0")
            {
                ViewState["ParentRequestID"] = Request.QueryString["ParentRequestID"];
                ViewState["ParentRequestIDPARAM"] = "&ParentRequestID=" + ViewState["ParentRequestID"];
                if (ViewState["ParentRequestID"].ToString().Equals(ViewState["RequestId"].ToString()))
                    divBackToParentRequest.Visible = false;
            }
            else
            {
                divBackToParentRequest.Visible = false;
                ViewState["ParentRequestID"] = ViewState["RequestId"];
            }
            hdnParentRequestID.Value = ViewState["ParentRequestID"].ToString();

            if (Convert.ToString(ViewState["isContractTypeExits"]) == "N")
            {
                //lblShowExpiryDateMessage.Text = "You do not have access to records of this contract type. Please contact your administrator.";
                lblShowExpiryDateMessage.Text = Convert.ToString(ViewState["CTMessage"]);
                lblShowExpiryDateMessage.Visible = true;
                DivQuestionnair.Visible = false;
                setAccessValuesForContractType();
                btnPDF.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "CallHideDiv", "CallHideDiv();", true);
                return;
            }

            int FlagApproval = 0;
            if ((!string.IsNullOrEmpty(ViewState["ContractId"].ToString()) && ViewState["ContractId"].ToString() != "0") || ViewState["IsAnswered"].ToString() == "Y")
            {
                divReqDescr.Visible = false;
                divReqDescrHR.Visible = false;
                divReqDescrTITLE.Visible = false;
                hdnContractId.Value = ViewState["ContractId"].ToString();
                if (ViewState["ContractId"].ToString() != "0")
                {
                    linkUploadFileDefault.Visible = false;
                    //divEditRequest.Visible = false;
                    DivQuestionnair.Visible = false;
                }

                if (!string.IsNullOrEmpty(hdnSelectedFileId.Value))
                {
                    BindUpdatedQuestionnair();
                    if (ViewState["ContractId"].ToString() != "0")
                    {
                        BindContractApproval();
                        FlagApproval = 1;
                    }
                }
                BindContractVersions();

                #region Unlock Request Form
                ///Uncomment following TO LOCK Request FORM
                //if ((ViewState["IsDocumentGenerated"].ToString() == "Y" || ViewState["IsBulkImport"].ToString() == "Y") || (hdnRequestTypeId.Value.ToString() == "1" && ViewState["ContractId"].ToString() != "0"))
                //{
                //    divEditRequest.Visible = false;
                //}
                #endregion
            }

            //if (FlagApproval == 0 && Convert.ToString(ViewState["IsDocumentGenerated"]) != "Y")
            if (FlagApproval == 0)
                BindFieldApproval();

            //For selected Tab
            if (Request.QueryString["Status"] != null)
            {
                if (Request.QueryString["Status"].ToString() != null)
                {
                    if (Request.QueryString["Status"].ToString() == "KeyField")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "viewkeyfield", "showTab('keyfields');", true);
                    }
                    else if (Request.QueryString["Status"].ToString() == "Workflow")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "viewworkflow", "showTab('workflow');", true);
                    }
                    else if (Request.QueryString["Status"].ToString() == "KeyObligation")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "viewkeyobligations", "showTab('keyobligations');", true);
                    }
                }
            }
        }
    }

    #region Methods
    private void setAccessValues()
    {
        Access.PageAccess("ContractRequestData.aspx", Session[Declarations.User].ToString());
        ViewState[Declarations.Update] = Access.Update.Trim();

        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }

        if (Update == "N")
        {
            divEditRequest.Visible = false;
        }

        #region Workflow Tab Permission

        Access.PageAccess("ContractRequestData.aspx", Session[Declarations.User].ToString());
        ViewState[Declarations.Update] = Access.Update.Trim();
        if (ViewState[Declarations.Update] != null)
        {
            RequestEdit = ViewState[Declarations.Update].ToString() == "Y" ? true : false;
        }

        ViewState["LoadQuestionnaireAccess"] = Access.isCustomised(13);
        if (ViewState["LoadQuestionnaireAccess"] != null)
        {
            LoadQuestionnaireAccess = Convert.ToBoolean(ViewState["LoadQuestionnaireAccess"].ToString());
        }
        ViewState["DownloadContractAccess"] = Access.isCustomised(14);
        if (ViewState["DownloadContractAccess"] != null)
        {
            DownloadContractAccess = Convert.ToBoolean(ViewState["DownloadContractAccess"].ToString());
        }
        ViewState["UploadFileAccess"] = Access.isCustomised(15);
        if (ViewState["UploadFileAccess"] != null)
        {
            UploadFileAccess = Convert.ToBoolean(ViewState["UploadFileAccess"].ToString());
        }
        ViewState["CheckOutAccess"] = Access.isCustomised(16);
        if (ViewState["CheckOutAccess"] != null)
        {
            CheckOutAccess = Convert.ToBoolean(ViewState["CheckOutAccess"].ToString());
        }
        ViewState["CheckOutSendMailAccess"] = Access.isCustomised(17);
        if (ViewState["CheckOutSendMailAccess"] != null)
        {
            CheckOutSendMailAccess = Convert.ToBoolean(ViewState["CheckOutSendMailAccess"].ToString());
        }
        ViewState["CheckInAccess"] = Access.isCustomised(18);
        if (ViewState["CheckInAccess"] != null)
        {
            CheckInAccess = Convert.ToBoolean(ViewState["CheckInAccess"].ToString());
        }
        ViewState["TrackChangesAccess"] = Access.isCustomised(28);
        if (ViewState["TrackChangesAccess"] != null)
        {
            TrackChangesAccess = Convert.ToBoolean(ViewState["TrackChangesAccess"].ToString());
        }

        ViewState["EditQuestionnaireAccess"] = Access.isCustomised(19);
        if (ViewState["EditQuestionnaireAccess"] != null)
        {
            EditQuestionnaireAccess = Convert.ToBoolean(ViewState["EditQuestionnaireAccess"].ToString());
        }
        ViewState["DeleteContractFileAccess"] = Access.isCustomised(22);
        if (ViewState["DeleteContractFileAccess"] != null)
        {
            DeleteContractFileAccess = Convert.ToBoolean(ViewState["DeleteContractFileAccess"].ToString());
        }
        ViewState["ElectronicSignatureAccess"] = Access.isCustomised(74);
        if (ViewState["ElectronicSignatureAccess"] != null)
        {
            ElectronicSignatureAccess = Convert.ToBoolean(ViewState["ElectronicSignatureAccess"].ToString());
        }
        ViewState["TabPDFDocumentAccess"] = Access.isCustomised(31);
        if (ViewState["TabPDFDocumentAccess"] != null)
        {
            TabPDFDocumentAccess = Convert.ToBoolean(ViewState["TabPDFDocumentAccess"].ToString());
        }
        #endregion

        #region Activity Tab Permission
        Access.PageAccess("WorkflowActivityTab", Session[Declarations.User].ToString());
        ViewState[Declarations.View] = Access.View.Trim();
        if (ViewState[Declarations.View].ToString() == "N")
        {
            ulActivityTab.Visible = false;
        }
        #endregion

        #region Document Tab Permission
        Access.PageAccess("WorkflowDocumentTab", Session[Declarations.User].ToString());
        ViewState[Declarations.View] = Access.View.Trim();
        if (ViewState[Declarations.View].ToString() == "N")
        {
            ulDocumentTab.Visible = false;
        }

        ViewState["TabDocumentUploadAccess"] = Access.isCustomised(20);
        if (ViewState["TabDocumentUploadAccess"] != null)
        {
            TabDocumentUploadAccess = Convert.ToBoolean(ViewState["TabDocumentUploadAccess"].ToString());
        }

        ViewState["TabDocumentDownloadAccess"] = Access.isCustomised(21);
        if (ViewState["TabDocumentDownloadAccess"] != null)
        {
            TabDocumentDownloadAccess = Convert.ToBoolean(ViewState["TabDocumentDownloadAccess"].ToString());
        }

        ViewState["TabDocumentDeleteAccess"] = Access.isCustomised(30);
        if (ViewState["TabDocumentDeleteAccess"] != null)
        {
            TabDocumentDeleteAccess = Convert.ToBoolean(ViewState["TabDocumentDeleteAccess"].ToString());
        }

        #endregion
        ParentDiv.Visible = TabDocumentUploadAccess;
        btnDisplayQuestionnair.Visible = LoadQuestionnaireAccess;
        aPDFLatestFile.Visible = TabPDFDocumentAccess;
        linkUploadFileDefault.Visible = UploadFileAccess;
        //Added by Dilip 17-12-2014
        #region KeyField Tab Permission
        Access.PageAccess("WorkflowImportantDatesTab", Session[Declarations.User].ToString());
        ViewState[Declarations.View] = Access.View.Trim();
        if (ViewState[Declarations.View].ToString() == "N")//Important Dates  
        {
            ulKeyFields.Visible = false;
        }

        Access.PageAccess("WorkflowKeyObligationsTab", Session[Declarations.User].ToString());
        ViewState[Declarations.View] = Access.View.Trim();
        if (ViewState[Declarations.View].ToString() == "N")//Important Dates  
        {
            ulKeyObligations.Visible = false;
        }
        #endregion
    }

    private void setAccessValuesForContractType()
    {
        Access.PageAccess("ContractTypes.aspx", Session[Declarations.User].ToString());
        ViewState[Declarations.Update] = Access.Update.Trim();

        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }

        if (Update == "N")
        {
            divEditRequest.Visible = false;
        }

        #region Workflow Tab Permission

        Access.PageAccess("ContractTypes.aspx", Session[Declarations.User].ToString());
        ViewState[Declarations.Update] = Access.Update.Trim();
        if (ViewState[Declarations.Update] != null)
        {
            RequestEdit = ViewState[Declarations.Update].ToString() == "Y" ? true : false;
        }        
        #endregion
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objContract = FactoryMaster.GetContractTypeDetail();
            ObjDocumentType = FactoryClause.GetContractTypesDetails();
            req = FactoryWorkflow.GetContractRequestDetails();
            objDocuSign = FactoryAssembly.DocusignDetails();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    void ContractTypeBind()
    {
        Page.TraceWrite("Contract Type dropdown bind starts.");
        try
        {
            //ddlContractType.extDataBind(objContract.SelectData());
        }
        catch (Exception ex)
        {
            Page.TraceWarn("Contract Type dropdown bind fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Contract Type dropdown bind ends.");
    }

    protected void ShowHidePanels()
    {
        PendingApprovals.Visible = false;
        DivContractVersions.Visible = true;
        DivUpdatedQuestions.Visible = false;
        //Questionnair.Visible = false;
        QuestionnairTemplate.Visible = true;
        DivQuestionnair.Visible = true;

        trLatestContractFileHR.Visible = false;
        trLatestContractFileTITLE.Visible = false;
        trLatestContractFileLINK.Visible = false;
    }

    protected void BindRequestDetails()
    {
        Page.TraceWrite("BindRequestDetails started.");
        try
        {
            lblContractIDHeader.Text = "Request #" + ViewState["RequestId"].ToString();
            hdnRequestId.Value = ViewState["RequestId"].ToString();
            req.UserID = Convert.ToInt32(Session[Declarations.User]);
            req.RequestID = Convert.ToInt32(ViewState["RequestId"]);
            DataSet ds = req.ReadContractDetails();
            DataTable dtRequestDetails = ds.Tables[0];
            DataTable dtRequestDocDetails = ds.Tables[1];
            DataTable dtContractTemplate = ds.Tables[2];
            DataTable dtIsAnswered = ds.Tables[3];
            DataTable dtTemplate = ds.Tables[4];

            ViewState["IsBulkImport"] = ds.Tables[0].Rows[0]["IsBulkImport"].ToString();
            hdnContractFileName.Value = HttpUtility.UrlEncode(ds.Tables[5].Rows[0][0].ToString());
            ViewState["isApprovalDone"] = ds.Tables[6].Rows[0]["IsApprovalDone"].ToString();
            ViewState["IsSignatureStatusSent"] = ds.Tables[7].Rows[0]["IsSignatureStatusSent"].ToString();
            ViewState["IsFirstCheckOutDone"] = ds.Tables[9].Rows[0]["IsFirstCheckOutDone"].ToString();
            ViewState["ShowExpiryDateMessage"] = ds.Tables[10].Rows[0]["ShowExpiryDateMessage"].ToString();
            ViewState["ShowExpiryDateMessageForRenewal"] = ds.Tables[11].Rows[0]["ShowExpiryDateMessageForRenewal"].ToString();
            Session["DocuSignBrandId"] = ds.Tables[12].Rows[0]["DocuSignBrandId"].ToString();
            Session["DocuSignUsername"] = ds.Tables[12].Rows[0]["DocuSignUsername"].ToString();
            Session["DocuSignPassword"] = ds.Tables[12].Rows[0]["DocuSignPassword"].ToString();
            ViewState["isContractTypeExits"] = ds.Tables[14].Rows[0]["isContractTypeExits"].ToString();
            ViewState["CTMessage"] = ds.Tables[14].Rows[0]["CTMessage"].ToString();

            lblPriority.Text = ds.Tables[0].Rows[0]["Priority"].ToString();
            if (lblPriority.Text == "Yes" || lblPriority.Text == "Y")
            {
                lblPriorityreason.Text = ds.Tables[0].Rows[0]["PriorityReason"].ToString();
            }

            if (ViewState["ShowExpiryDateMessage"].ToString() == "Y")
            {
                lblShowExpiryDateMessage.Visible = true;
                lblShowExpiryDateMessage.Text = ds.Tables[10].Rows[0]["MessageText"].ToString(); ;
            }
            else
            {
                lblShowExpiryDateMessage.Visible = false;
                lblShowExpiryDateMessage.Text = "";
            }

            if (ViewState["ShowExpiryDateMessageForRenewal"].ToString() == "Y")
            {
                lblShowExpiryDateMessage.Visible = true;
                lblShowExpiryDateMessage.Text = ds.Tables[11].Rows[0]["MessageText"].ToString(); ;
            }
            else
            {
                lblShowExpiryDateMessage.Visible = false;
                lblShowExpiryDateMessage.Text = "";
            }

            //************** 7 nov 14*********************
            DataTable dtContractHistory = ds.Tables[8];
            if (dtContractHistory.Rows.Count > 0)
            {
                rptContractHistory.DataSource = dtContractHistory;
                rptContractHistory.DataBind();
            }
            else
            {
                trContractHistoryHR.Visible = false;
                trContractHistoryHeader.Visible = false;
                trContractHistoryData.Visible = false;
            }
            //*******************************************

            if (dtRequestDetails.Rows.Count > 0)
            {
                ViewState["ContractTypeId"] = dtRequestDetails.Rows[0]["ContractTypeId"].ToString();
                hdnContractTypeId.Value = ViewState["ContractTypeId"].ToString();
                lblRequestType.Text = dtRequestDetails.Rows[0]["RequestTypeName"].ToString();
                lblRequestDetails.Text = "Added by " + dtRequestDetails.Rows[0]["FullName"].ToString() + " on " + dtRequestDetails.Rows[0]["AddedDate"].ToString();
                lblContractType.Text = dtRequestDetails.Rows[0]["ContractTypeName"].ToString();
                lblDeadLineDate.Text = dtRequestDetails.Rows[0]["DeadlineDate"].ToString();
                //lblContractValue.Text = dtRequestDetails.Rows[0]["ContractValue"].ToString();
                lblContractValue.Text = dtRequestDetails.Rows[0]["ContractValuewithcurrency"].ToString();
                lblTermOfContract.Text = dtRequestDetails.Rows[0]["ContractTerm"].ToString();
                lblContractId.Text = dtRequestDetails.Rows[0]["ContractId"].ToString();
                if (!string.IsNullOrEmpty(lblContractId.Text))
                    lblContractIDHeader.Text = "Contract #" + lblContractId.Text;// +" <label style='font-size:14px;color:black;'>(Request #" + ViewState["RequestId"].ToString() + ")</label>";
                lblRequestId.Text = " (Request #" + ViewState["RequestId"].ToString() + ")";

                lblAssignedTo.Text = dtRequestDetails.Rows[0]["AssignedTo"].ToString();
                lblEstimatedValue.Text = dtRequestDetails.Rows[0]["EstimatedValue"].ToString();
                //lblIsApprovalRequired.Text = dtRequestDetails.Rows[0]["IsApprovalRequired"].ToString();
                lblClientNames.Text = dtRequestDetails.Rows[0]["ClientName"].ToString();
                lblClientAddress.Text = dtRequestDetails.Rows[0]["ClientAddress"].ToString();
                lblClientMobile.Text = dtRequestDetails.Rows[0]["ContactNumber"].ToString();
                lblClientEmailId.Text = dtRequestDetails.Rows[0]["EmailID"].ToString();
                lblRequestDescription.Text = dtRequestDetails.Rows[0]["RequestDescription"].ToString();
                lblRequestDescription.Text = lblRequestDescription.Text.Replace("\n", "<br>");
                lblContractStatusName.Text = dtRequestDetails.Rows[0]["StatusName"].ToString();
                hdnContractStatusId.Value = dtRequestDetails.Rows[0]["ContractStatusID"].ToString();
                hdnRequestTypeId.Value = dtRequestDetails.Rows[0]["RequestTypeId"].ToString();

                if (dtRequestDetails.Rows[0]["RequestTypeId"].ToString() == "5")
                    trEstimatedValue.Visible = true;
                else
                    trEstimatedValue.Visible = false;

                objActivity = FactoryWorkflow.GetActivityDetails();
                objActivity.RequestId = Convert.ToInt32(ViewState["RequestId"]);
                hdnRequestDate.Value = objActivity.ContractDetailsByRequestId();
                activitiescontrolid.ContractInfo = hdnRequestDate.Value;
                activitiescontrolid.ContractTypeId = Convert.ToInt32(ViewState["ContractTypeId"]);
            }

            rptRequestDocuments.DataSource = dtRequestDocDetails;
            rptRequestDocuments.DataBind();

            if (dtContractTemplate.Rows.Count > 0)
            {
                ddlContractTemplate.DataSource = dtContractTemplate;
                ddlContractTemplate.DataTextField = dtContractTemplate.Columns[1].ColumnName;
                ddlContractTemplate.DataValueField = dtContractTemplate.Columns[0].ColumnName;
                ddlContractTemplate.DataBind();
                ddlContractTemplate.SelectedIndex = 0;
            }

            ViewState["ContractId"] = string.IsNullOrEmpty(lblContractId.Text) ? "0" : lblContractId.Text;
            ViewState["IsAnswered"] = dtIsAnswered.Rows[0]["IsAnswered"].ToString();

            if (dtTemplate.Rows.Count > 0)
            {
                hdnSelectedFileId.Value = dtTemplate.Rows[0]["ContractTemplateId"].ToString() + "#" + dtTemplate.Rows[0]["TemplateFileName"];
            }
            //hdnFileName.Value = dtTemplate.Rows[0]["TemplateFileName"].ToString();
            if (ViewState["ContractId"] != null && ViewState["ContractId"].ToString() != "0")
            {
                btnViewRelations.Visible = true;
                DataTable dtRelation = ds.Tables[15];
                if (dtRelation.Rows.Count > 0)
                {
                    if (dtRelation.Rows[0]["View"].ToString() == "N")
                        btnViewRelations.Visible = false;
                }
                else { btnViewRelations.Visible = false; }
            }
            Page.TraceWrite("BindRequestDetails finished.");
        }
        catch (Exception ex)
        {
            Page.TraceWarn("BindRequestDetails failed.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    protected void BindUpdatedQuestionnair()
    {
        List<QuestionAnswer> ListQA = new List<QuestionAnswer>();
        QuestionAnswer QA = new QuestionAnswer();
        QA.ContractId = Convert.ToInt32(ViewState["ContractId"].ToString());
        QA.ContractTemplateId = 0;// Convert.ToInt32(ViewState["SelectedFileId"].ToString().Split('#')[0]);
        QA.ContractRequestId = Convert.ToInt32(ViewState["RequestId"]);
        ListQA = QA.ContractUpdatedQuestionniarDetails();

        if (ListQA.Count > 0)
        {
            divReqDescr.Visible = false;
            divReqDescrHR.Visible = false;
            divReqDescrTITLE.Visible = false;

            lblQuestionnaireUpdated.Text = "Questionnaire Updated by " + ListQA[0].ModifiedByUserName + " " + ListQA[0].ModifiedOnFormattedDate;
            lblContractTemplateName.Text = "Contract Template : " + ListQA[0].TemplateName;
            ListQA.RemoveAt(ListQA.Count - 1);
            rptQA.DataSource = ListQA;
            rptQA.DataBind();

            DivUpdatedQuestions.Visible = true;
        }
        //Questionnair.Visible = false;
        QuestionnairTemplate.Visible = false;
        DivQuestionnair.Visible = false;
    }

    protected void BindContractApproval()
    {
        List<ContractProcedures> ListCPParent = new List<ContractProcedures>();
        List<ContractProcedures> ListCPChild = new List<ContractProcedures>();
        List<ContractProcedures> ListPendingStagesForApproval = new List<ContractProcedures>();

        ContractProcedures CP = new ContractProcedures();
        CP.ContractId = Convert.ToInt32(ViewState["ContractId"].ToString());
        CP.ContractTemplateId = Convert.ToInt32(hdnSelectedFileId.Value.Split('#')[0]);
        CP.RequestId = Convert.ToInt32(ViewState["RequestId"].ToString());
        CP.ModifiedBy = Convert.ToInt32(Session[Declarations.User]);
        CP.Approvaluri = Session[Declarations.URL] + "/WorkFlow/RequestFlow.aspx?Status=Workflow&RequestId=" + Convert.ToString(ViewState["RequestId"]);

        CP.PendingForApprovals();

        ListCPParent = CP.StageDetails;
        ListCPChild = CP.StageApprovalActivity;
        ListPendingStagesForApproval = CP.StagesPendingForApproval;

        if (ListCPParent.Count > 0)
        {
            rptPendingApprovals.DataSource = ListCPParent;
            rptPendingApprovals.DataBind();
            PendingApprovals.Visible = true;
            if (Request.QueryString["SendEmail"] == "true")
            {
                SendEmailToApprover(ListCPParent);
            }
        }

        for (int i = 0; i <= rptPendingApprovals.Items.Count - 1; i++)
        {
            HiddenField hdnContractApprovalID = (HiddenField)rptPendingApprovals.Items[i].FindControl("hdnContractApprovalID");
            Repeater rptApprovalsActivity = (Repeater)rptPendingApprovals.Items[i].FindControl("rptApprovalsActivity");
            var result = ListCPChild.FindAll(x => x.ContractApprovalId == int.Parse(hdnContractApprovalID.Value));
            rptApprovalsActivity.DataSource = result;
            rptApprovalsActivity.DataBind();
        }

        if (ListPendingStagesForApproval.Count > 0)
        {
            rptPendingStagesForApproval.DataSource = ListPendingStagesForApproval;
            rptPendingStagesForApproval.DataBind();
        }
    }

    protected void BindFieldApproval()
    {
        List<ContractProcedures> ListCPParent = new List<ContractProcedures>();
        List<ContractProcedures> ListCPChild = new List<ContractProcedures>();
        List<ContractProcedures> ListPendingStagesForApproval = new List<ContractProcedures>();

        ContractProcedures CP = new ContractProcedures();
        if (!string.IsNullOrEmpty(ViewState["ContractId"].ToString()) && ViewState["ContractId"].ToString() != "0")
            CP.ContractId = Convert.ToInt32(ViewState["ContractId"]);
        else
            CP.ContractId = Convert.ToInt32(0);

        CP.ContractTemplateId = Convert.ToInt32(0);
        CP.RequestId = Convert.ToInt32(ViewState["RequestId"].ToString());
        CP.ModifiedBy = Convert.ToInt32(Session[Declarations.User]);
        CP.Approvaluri = Session[Declarations.URL] + "/WorkFlow/RequestFlow.aspx?Status=Workflow&RequestId=" + Convert.ToString(ViewState["RequestId"]);

        CP.PendingForApprovals();

        ListCPParent = CP.StageDetails;
        ListCPChild = CP.StageApprovalActivity;
        ListPendingStagesForApproval = CP.StagesPendingForApproval;

        if (ListCPParent.Count > 0)
        {
            rptPendingApprovals.DataSource = ListCPParent;
            rptPendingApprovals.DataBind();
            PendingApprovals.Visible = true;
            //if (Request.QueryString["SendEmail"] == "true")
            //{
            SendEmailToApprover(ListCPParent);
            //}
        }

        for (int i = 0; i <= rptPendingApprovals.Items.Count - 1; i++)
        {
            HiddenField hdnContractApprovalID = (HiddenField)rptPendingApprovals.Items[i].FindControl("hdnContractApprovalID");
            Repeater rptApprovalsActivity = (Repeater)rptPendingApprovals.Items[i].FindControl("rptApprovalsActivity");
            var result = ListCPChild.FindAll(x => x.ContractApprovalId == int.Parse(hdnContractApprovalID.Value));
            rptApprovalsActivity.DataSource = result;
            rptApprovalsActivity.DataBind();
        }

        if (ListPendingStagesForApproval.Count > 0)
        {
            rptPendingStagesForApproval.DataSource = ListPendingStagesForApproval;
            rptPendingStagesForApproval.DataBind();
        }
    }

    protected void BindContractVersions()
    {
        List<ContractProcedures> ListCP = new List<ContractProcedures>();
        ContractProcedures CP = new ContractProcedures();
        CP.ContractId = Convert.ToInt32(ViewState["ContractId"].ToString());
        CP.RequestId = Convert.ToInt32(ViewState["RequestId"]);
        CP.ContractVersions();

        List<ContractProcedures> ListCPParent = new List<ContractProcedures>();
        List<ContractProcedures> ListCPChild = new List<ContractProcedures>();
        ListCPParent = CP.ContractFiles;
        ListCPChild = CP.ContractFileActivity;

        if (ListCPParent.Count > 0)
        {
            if (ListCPParent.Count > 1)
                ViewState["PreviousIsValidDocument"] = "N";
            ViewState["CurrentIsValidDocument"] = "N";
            hdnContractFilesCount.Value = ListCPParent.Count.ToString();
            rptContractVersions.DataSource = ListCPParent;
            rptContractVersions.DataBind();
            DivContractVersions.Visible = true;
            ViewState["IsDocumentGenerated"] = "Y";
        }
        else
        {
            ViewState["IsDocumentGenerated"] = "N";
            linkUploadFileDefault.Visible = UploadFileAccess;
            if (string.IsNullOrEmpty(hdnSelectedFileId.Value))
            {
                DivQuestionnair.Visible = true;
            }
        }
        for (int i = 0; i <= rptContractVersions.Items.Count - 1; i++)
        {
            HiddenField hdnContractFileId = (HiddenField)rptContractVersions.Items[i].FindControl("hdnContractFileId");
            Repeater rptContractFilesActivity = (Repeater)rptContractVersions.Items[i].FindControl("rptContractFilesActivity");
            var result = ListCPChild.FindAll(x => x.ContractFileId == int.Parse(hdnContractFileId.Value));
            rptContractFilesActivity.DataSource = result;
            rptContractFilesActivity.DataBind();
        }
    }

    protected void SendEmailToApprover(List<ContractProcedures> listCP)
    {
        SendMail sm = new SendMail();
        for (int i = 0; i < listCP.Count; i++)
        {
            if (listCP[i].IsActive == 'Y' && listCP[i].IsMailSent == 'N')
            {
                sm.Parameters.Clear();
                sm.MailTo = listCP[i].EmailId;
                sm.MailFrom = ConfigurationManager.AppSettings["MailFrom"];
                sm.Subject = listCP[i].MailSubject;
                sm.Body = listCP[i].MailBody;
                bool IsSend = sm.SendSimpleMail();

                if (IsSend)
                {
                    ContractProcedures CP = new ContractProcedures();
                    CP.ContractApprovalId = listCP[i].ContractApprovalId;
                    string retVal = CP.UpdateRecordContractApproval();
                }
            }
        }
    }

    protected void SendContractGeneratedEmailToClient(int RequestId)
    {
        IActivity objAct = FactoryWorkflow.GetActivityDetails();
        objAct.RequestId = RequestId;
        List<Activity> lst = new List<Activity>();
        lst = objAct.GetUserNameAndPassword();
        string userEmailId = lst[0].EmailID;
        string password = lst[0].Password;
        string customerName = lst[0].UserName;

        string contractGeneratedDate = lst[0].AddedOnDate;
        int ContractId = lst[0].ContractID;
        if (ContractId > 0)
        {
            try
            {
                Page.TraceWrite("Contract Generated send emails.");
                SendMail sm = new SendMail();
                sm.MailFrom = System.Configuration.ConfigurationManager.AppSettings["MailFrom"];
                sm.Subject = "Contract " + ContractId.ToString() + " - Available for Review";

                string body = "<font face='Arial' size='2' color='#006666'>Hello <br><br>";
                body = body + "Contract ID: " + ContractId.ToString() + "<br><br>";
                body = body + "A contract has been generated for your attention. You can view the contract and its activities by <br>";
                body = body + "following the link below:<br><br>";
                body = body + "<a href='" + Strings.domainURL() + "customerportal/login.aspx'>" + Strings.domainURL() + "customerportal/login.aspx</a><br><br>";
                body = body + "Your login details are:<br><br>";
                body = body + "Username: " + userEmailId + "<br>";
                body = body + "Password: " + password + "<br><br>";
                body = body + "Happy contracting!";
                body = body + "<br>The ContractPod™ team</font>";

                body = body + "<br><br><font face='Arial' size='1' color='#006666'>Need help? Please do not hesitate to contact our technical support team on 0800 699 0045 or at help@contractpod.com.";
                body = body + "<br>The information in this email is confidential and for use by the addressee(s) only. If you are not the intended recipient, please notify us immediately on 0800 699 0045 and delete the message from your computer. You may not copy or forward the e-mail, or use it or disclose its contents to any other person. We do not accept any liability or responsibility for changes made to this email after it was sent, or viruses transmitted through this e-mail or any attachment.</font>";

                sm.Body = body;
                sm.MailTo = userEmailId;
                sm.RequestId = RequestId;
                sm.Mode = "Contract Generated";
                sm.SendSimpleMail();
            }
            catch (Exception)
            {
                Page.TraceWarn("Contract Generated fail.");
            }
        }
    }

    protected void BindDocuSignStatus()
    {
        /**Updated by nilesh to get envelope id for that request****/
        objDocuSign.RequestId = int.Parse(ViewState["RequestId"].ToString());
        string EnvelopeId = objDocuSign.ReadEnvelopeId();
        if (EnvelopeId.Trim().Length != 0)
        {
            hdnMalAlreadySent.Value = "Y";
            GetStatuses(EnvelopeId); //Get the status of Envelope or sent document
            BindRequestDetails();
        }
    }

    void SendSignatureCompletitionEmail()
    {
        try
        {
            Page.TraceWrite("Send Signature Completition Email.");
            EmailTemplateBLL.IEmailTrigger objEmail = EmailTemplateBLL.FactoryEmailTemplate.GetEmailTriggerDetail();
            objEmail.RequestId = int.Parse(hdnRequestId.Value);
            objEmail.ContractTypeId = 99999;
            objEmail.ContractTemplateId = 0;
            objEmail.EmailTriggerId = 8;
            objEmail.userId = Convert.ToInt32(Session[Declarations.User]);
            objEmail.BulkEmail();
        }
        catch (Exception)
        {
            Page.TraceWarn("Send Signature Completition Email.");
        }
    }

    [WebMethod]
    public static string DeleteContractFile(string ContractFileId, string isFromDocuSign, string RequestId, string FileName, string NVFileName, string FilePath, string ContractInfo, string isOCR)
    {
        //FileName = FileName.Replace("%27", "'").Replace("%3B", ";");
        //NVFileName = NVFileName.Replace("%27", "'").Replace("%3B", ";");
        FilePath = HttpUtility.UrlDecode(FilePath);
        FileName = HttpUtility.UrlDecode(FileName);
        NVFileName = HttpUtility.UrlDecode(NVFileName);
        IContractProcedures ObjContractProcedures;
        ObjContractProcedures = FactoryAssembly.RequestDocumentsDetails();
        ObjContractProcedures.ContractFileId = Convert.ToInt32(ContractFileId);
        ObjContractProcedures.isFromDocuSign = isFromDocuSign;

        ObjContractProcedures.RequestId = Convert.ToInt32(RequestId);
        string[] FIleNameComp = FileName.Split('_');
        string GUID = Guid.NewGuid().ToString();
        string retval;
        string strUrlwordFile = "";
        string strUrlPdfFile = "";

        int FileCount = 0;
        string OrigionalFileName = "";
        string NewFileName = "";
        string isNew = "";

        try
        {
            FileCount = Convert.ToInt32(FIleNameComp[4]);
            OrigionalFileName = NVFileName;
            NewFileName = FIleNameComp[0] + "_" + FIleNameComp[1] + "_" + FIleNameComp[2] + "_" + FIleNameComp[3] + "_" + FileCount.ToString() + "_" + GUID;
        }
        catch (Exception ex)
        {
            isNew = "Y";
        }

        try
        {
            if (isFromDocuSign == "Y" && isNew == "")
            {
                string WordFilePath = HttpContext.Current.Server.MapPath(@"..//Uploads//" + HttpContext.Current.Session["TenantDIR"] + "//Contract Documents//" + OrigionalFileName + ".docx");
                string NewWordFilePath = HttpContext.Current.Server.MapPath(@"..//Uploads//" + HttpContext.Current.Session["TenantDIR"] + "//Contract Documents//" + NewFileName + ".docx");

                string PdfFilePath = HttpContext.Current.Server.MapPath(@"..//Uploads//" + HttpContext.Current.Session["TenantDIR"] + "//Contract Documents//PDF//" + OrigionalFileName + ".pdf");
                string NewPdfFilePath = HttpContext.Current.Server.MapPath(@"..//Uploads//" + HttpContext.Current.Session["TenantDIR"] + "//Contract Documents//PDF//" + NewFileName + ".pdf");

                if (string.IsNullOrEmpty(NVFileName) == false)
                {
                    if (File.Exists(WordFilePath))
                    {
                        File.Copy(WordFilePath, NewWordFilePath, true);

                        strUrlwordFile = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + HttpContext.Current.Request.Url.Segments[1] + "Uploads/" + HttpContext.Current.Session["TenantDIR"] + "/Contract Documents/" + NewFileName + ".docx";
                        AsyncIndexer.GetInstance().QueueForIndexing(strUrlwordFile, 1, ContractInfo);
                    }
                    File.Copy(PdfFilePath, NewPdfFilePath, true);

                    strUrlPdfFile = "";
                    strUrlPdfFile = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + HttpContext.Current.Request.Url.Segments[1] + "Uploads/" + HttpContext.Current.Session["TenantDIR"] + "/Contract Documents/PDF/" + NewFileName + ".pdf";
                    AsyncIndexer.GetInstance().QueueForIndexing(strUrlPdfFile, 1, ContractInfo);
                }
                ObjContractProcedures.GUID = GUID;
            }
            ObjContractProcedures.IpAddress = HttpContext.Current.Session[Declarations.IP] != null ? HttpContext.Current.Session[Declarations.IP].ToString() : null;
            ObjContractProcedures.AddedBy = HttpContext.Current.Session[Declarations.User] != null ? int.Parse(HttpContext.Current.Session[Declarations.User].ToString()) : 0;

            retval = ObjContractProcedures.DeleteContractFile();

            /*Keyoti Uploaded Document Indexing Start.*/
            string strUrl = "";
            string[] Files = FilePath.Split(',');

            foreach (var file in Files)
            {
                string FileVitPath = "";
                FileVitPath = file;
                strUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + HttpContext.Current.Session["TenantDIR"] + FileVitPath.Replace("../Uploads", "/Uploads");
                AsyncIndexer.GetInstance().QueueForIndexing(strUrl, 2, ContractInfo);
            }

            /*Keyoti Uploaded Document Indexing End.*/

            DeleteFileFromPhysicalLocation.DeleteFile(FilePath);

            if (isOCR.ToUpper() == "Y" && isFromDocuSign == "Y")
            {
                DeleteFileFromPhysicalLocation.DeleteFile(FilePath.Replace("SignedDocuments", "SignedDocuments/ScannedSignedDocuments"));
            }
            else if (isOCR.ToUpper() == "Y")
            {
                DeleteFileFromPhysicalLocation.DeleteFile(FilePath.Replace("PDF", "PDF/ScannedPDF"));
            }

        }
        catch (Exception ex)
        {
            retval = "";
        }

        return retval;
    }

    [WebMethod]
    public static string BindDocumentDetails(string RequestId)
    {
        IContractProcedures ObjContractProcedures;
        ObjContractProcedures = FactoryAssembly.RequestDocumentsDetails();
        ObjContractProcedures.RequestId = Convert.ToInt32(RequestId);
        List<ContractProcedures> ListRequestDocuments = ObjContractProcedures.RequestDocumentDetails();
        string doc = "", href = "", delete = "";

        for (int i = 0; i < ListRequestDocuments.Count; i++)
        {
            if (TabDocumentDownloadAccess)
                href = @"href='../Uploads/" + HttpContext.Current.Session["TenantDIR"] + "/ContractDocs/" + ListRequestDocuments[i].ActuallFileName + "'";
            if (TabDocumentDeleteAccess)
                delete = "<a href='' class='delete' rel='nofollow' title='Delete' id='" + ListRequestDocuments[i].ContractRequestDocumentId + "' onclick='return DeleteDocument(this);'>" +
                         "<img alt='Delete' src='../images/icon-del.jpg?1349001717'></a>";
            doc += "<span>" +
                       "<h4 ><asp:HiddenField ID='hdnDocumentName' runat='server' ClientIDMode='Static' Value='" + ListRequestDocuments[i].ActuallFileName + "' />" +
                        "<em>" + ListRequestDocuments[i].DocumentTypeName + "</em> " + "<br>" + "</h4>" +
                       "<a " + href + " class='icon icon-attachment' style='color: #de6528' >" + ListRequestDocuments[i].OrigionalFileName + "</a><span class='size'></span><br>" +
                       "-<span id='spanDocumentDelete' runat='server' clientidmode='Static'>" + delete +
                       "</span><span class='author'>" + ListRequestDocuments[i].FullName + ", " + ListRequestDocuments[i].AddedDate + "</span>" +

                       "<br>" +
                       "<br>  " +
                   "</span>";
        }
        return doc;
    }

    [WebMethod]
    public static string DeleteDocument(string DocumentID, string DocumentPath)
    {
        IClauseLibrary ObjDocumentType;
        ObjDocumentType = FactoryClause.GetContractTypesDetails();
        ObjDocumentType.ContractRequestDocumentID = DocumentID;
        ObjDocumentType.IP = HttpContext.Current.Session[Declarations.IP] != null ? HttpContext.Current.Session[Declarations.IP].ToString() : null;
        ObjDocumentType.Addedby = HttpContext.Current.Session[Declarations.User] != null ? int.Parse(HttpContext.Current.Session[Declarations.User].ToString()) : 0;
        string IsDeleted = ObjDocumentType.DeleteDocumentFile();

        if (IsDeleted == "1")
        {
            /*Keyoti Uploaded Document Indexing Start.*/
            string strUrl = "";
            strUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + HttpContext.Current.Session["TenantDIR"] + DocumentPath.Replace("../Uploads", "/Uploads");
            AsyncIndexer.GetInstance().QueueForIndexing(strUrl, 2, "");
            /*Keyoti Uploaded Document Indexing End.*/

            DeleteFileFromPhysicalLocation.DeleteFile(DocumentPath);
        }
        return IsDeleted;
    }

    protected void ExecuteEmailUpload()
    {
        //EmailUploadSchedulerService.Service1Client eu = new EmailUploadSchedulerService.Service1Client();
        //string retval = eu.ReadMail("pop.practiceleague.com", "democontracts@practiceleague.com", "is$aeH$9");
    }

    protected void NavigateSearch(int val)
    {
        try
        {
            string SENDER = Request.QueryString["Sender"];
            string RID = ViewState["RequestId"].ToString();
            List<int> Searchist = SENDER.Equals("MetaSearch") ? (List<int>)Session["MetaSearchList"] : (List<int>)Session["DocSearchList"];
            int INDEXM = Searchist.FindIndex(a => a.ToString() == RID.ToString());
            RID = Searchist[INDEXM + val].ToString();

            Response.Redirect("RequestFlow.aspx?Sender=" + SENDER + "&Status=Workflow&RequestId=" + RID);
        }
        catch (Exception ex)
        {

        }

    }

    protected void ShowHideButtons(int index = 0)
    {
        string RID = ViewState["RequestId"].ToString();
        string SENDER = Request.QueryString["Sender"];

        List<int> Searchist = SENDER.Equals("MetaSearch") ? (List<int>)Session["MetaSearchList"] : (List<int>)Session["DocSearchList"];
        int INDEX = Searchist.FindIndex(a => a.ToString() == RID.ToString());
        if (Searchist.Count > 1 && (SENDER.Equals("DocSearch") || SENDER.Equals("MetaSearch")))
        {
            if ((INDEX + 1) == Searchist.Count)
            {
                btnNext.Visible = false;
                btnPrev.Visible = true;
            }
            else if (INDEX == 0)
            {
                btnNext.Visible = true;
                btnPrev.Visible = false;
            }
            else if (INDEX < Searchist.Count)
            {
                btnNext.Visible = true;
                btnPrev.Visible = true;
            }
        }
        else
        {
            btnNext.Visible = false;
            btnPrev.Visible = false;
        }
    }

    #endregion

    #region Button events
    protected void btnDisplayQuestionnair_Click(object sender, EventArgs e)
    {
        if (ddlContractTemplate.Value != "0" || ViewState["SelectedFileId"].ToString() != "")
        {
            Server.Transfer("~/Workflow/Questionnair.aspx?IsBulkImport=" + ViewState["IsBulkImport"] + ViewState["ParentRequestIDPARAM"] + "&RequestDate=" + hdnRequestDate.Value, true);
        }
    }

    protected void EditQuestionnair_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ViewState["ContractId"].ToString()))
        {
            //GetDynamicControls();
            Server.Transfer("~/Workflow/Questionnair.aspx?IsBulkImport=" + ViewState["IsBulkImport"] + ViewState["ParentRequestIDPARAM"] + "&RequestId=" + ViewState["RequestId"].ToString(), true);

        }
        //else
        //    Questionnair.Visible = true;
        //if (ViewState["ContractId"].ToString() == "0")
        //    btnSaveDraft.Visible = true;
        //else
        //    btnSaveDraft.Visible = false;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //if (Request.QueryString["Sender"] != null)
        //{
        //    if (Request.QueryString["Sender"].Contains("Search"))
        //    {
        //        Response.Redirect("~/WorkFlow/SearchResults.aspx", false);
        //    }
        //}
        //else 
        if (ViewState["IsRequestedFromVault"] != null)
        {
            Response.Redirect("~/WorkFlow/VaultData.aspx", false);
        }
        else
        {
            Response.Redirect("~/WorkFlow/ContractRequestData.aspx", false);
        }
    }

    protected void linkUploadFileDefault_Click(object sender, EventArgs e)
    {
        Response.Redirect("UploadContractFile.aspx?RequestId=" + ViewState["RequestId"] + "&ContractId=" + ViewState["ContractId"] + "&NextFileName=" + hdnContractFileName.Value + "&IsApprovalDone=" + ViewState["isApprovalDone"].ToString() + "&ContractTypeId=" + ViewState["ContractTypeId"] + "&ContractTemplateId=" + hdnSelectedFileId.Value.Split('#')[0]);
    }

    protected void lnkToMainPage_Click(object sender, EventArgs e)
    {
        Response.Redirect("RequestFlow.aspx?Status=Workflow&RequestId=" + ViewState["ParentRequestID"]);
    }

    protected void btnPrev_Click(object sender, EventArgs e)
    {
        NavigateSearch(-1);
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        NavigateSearch(1);
    }

    protected void btnPDF_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["RequestId"])) && !string.IsNullOrEmpty(Request.QueryString["ParentRequestID"]))
        {
            Response.Redirect("~/WorkFlow/RequestFlowPdfExport.aspx?Status=Workflow&RequestId=" + Request.QueryString["RequestId"] + "&ParentRequestID=" + Request.QueryString["ParentRequestID"], false);
        }
        else if (string.IsNullOrEmpty(Convert.ToString(Request.QueryString["RequestId"])) && !string.IsNullOrEmpty(Request.QueryString["ParentRequestID"]))
            Response.Redirect("~/WorkFlow/RequestFlowPdfExport.aspx?Status=Workflow&ParentRequestID=" + Request.QueryString["ParentRequestID"], false);
        else
            Response.Redirect("RequestFlowPdfExport.aspx?Status=Workflow&RequestId=" + hdnRequestId.Value.ToString() + "");
    }
    #endregion

    #region Repeater events

    protected void rptPendingApprovals_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HtmlInputHidden hdnApproverLoginId = (HtmlInputHidden)e.Item.FindControl("hdnApproverLoginId");
            HtmlInputHidden hdnIsActive = (HtmlInputHidden)e.Item.FindControl("hdnIsActive");
            HtmlAnchor aLinkToApproval = (HtmlAnchor)e.Item.FindControl("aLinkToApproval");
            HtmlAnchor aLinkToApproval_Second = (HtmlAnchor)e.Item.FindControl("aLinkToApproval_Second");
            HtmlGenericControl ApprovalJournalLink = (HtmlGenericControl)e.Item.FindControl("ApprovalJournalLink");

            HtmlGenericControl spanStageName = (HtmlGenericControl)e.Item.FindControl("spanStageName");
            aLinkToApproval.HRef = aLinkToApproval.HRef + "&StageName=" + spanStageName.InnerText;
            aLinkToApproval_Second.HRef = aLinkToApproval_Second.HRef + "&StageName=" + spanStageName.InnerText;

            if (hdnApproverLoginId.Value == Session[Declarations.User].ToString() && hdnIsActive.Value == "Y")
            {
                aLinkToApproval.Visible = true;
                ApprovalJournalLink.Visible = true;
                aLinkToApproval_Second.Visible = true;
                spanStageName.Visible = false;
            }
            else
            {
                aLinkToApproval.Visible = false;
                ApprovalJournalLink.Visible = false;
                aLinkToApproval_Second.Visible = false;
                spanStageName.Visible = true;
            }
        }
    }

    protected void rptContractVersions_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HtmlGenericControl JournalLink = (HtmlGenericControl)e.Item.FindControl("JournalLink");
            HtmlAnchor linkContractVersionFile = (HtmlAnchor)e.Item.FindControl("linkContractVersionFile");
            HtmlAnchor aMSWordFile = (HtmlAnchor)e.Item.FindControl("aMSWordFile");
            HtmlAnchor aPDFFile = (HtmlAnchor)e.Item.FindControl("aPDFFile");
            LinkButton linkCheckOut = (LinkButton)e.Item.FindControl("linkCheckOut");
            LinkButton linkCheckOutSendMail = (LinkButton)e.Item.FindControl("linkCheckOutSendMail");
            LinkButton linkUploadFile = (LinkButton)e.Item.FindControl("linkUploadFile");
            LinkButton linkTrackChanges = (LinkButton)e.Item.FindControl("linkTrackChanges");
            LinkButton linkCheckIn = (LinkButton)e.Item.FindControl("linkCheckIn");
            LinkButton linkElectronicSignature = (LinkButton)e.Item.FindControl("linkElectronicSignature");

            HiddenField hdnIsCheckOutDone = (HiddenField)e.Item.FindControl("hdnIsCheckOutDone");
            HiddenField hdnIsIsFromDocuSign = (HiddenField)e.Item.FindControl("hdnIsFromDocuSign");
            HiddenField hdnFileName = (HiddenField)e.Item.FindControl("hdnFileName");
            HiddenField hdnIsPDFFile = (HiddenField)e.Item.FindControl("hdnIsPDFFile");
            HiddenField hdnIsValidDocument = (HiddenField)e.Item.FindControl("hdnIsValidDocument");
            HiddenField hdnIsUploaded = (HiddenField)e.Item.FindControl("hdnIsUploaded");

            if (e.Item.ItemIndex == 0)
                ViewState["IsFromDocuSign"] = hdnIsIsFromDocuSign.Value.Trim() == "Y" ? "Y" : "N";


            if ((((List<ContractProcedures>)(rptContractVersions.DataSource))).Count > 1 && e.Item.ItemIndex == 0)
                ViewState["CurrentIsValidDocument"] = (((List<ContractProcedures>)(rptContractVersions.DataSource)))[0].IsValidDocument.ToString();
            if ((((List<ContractProcedures>)(rptContractVersions.DataSource))).Count > 1 && e.Item.ItemIndex == 1)
                ViewState["PreviousIsValidDocument"] = hdnIsValidDocument.Value;


            string[] hdnFILEPATHWITHGUID = System.Text.RegularExpressions.Regex.Split(hdnFileName.Value, "_");
            int len = hdnFILEPATHWITHGUID.Length;
            string GUID = hdnFILEPATHWITHGUID[len - 1];

            HtmlGenericControl spanDeleteContractVersion = (HtmlGenericControl)e.Item.FindControl("spanDeleteContractVersion");

            linkTrackChanges.Visible = false;

            aMSWordFile.Visible = DownloadContractAccess;
            aPDFFile.Visible = TabPDFDocumentAccess;
            //aMSWordLatestFile.Visible = DownloadContractAccess;
            fileName = HttpUtility.UrlEncode(aPDFFile.HRef.Substring(aPDFFile.HRef.ToString().LastIndexOf("/") + 1));
            aPDFFile.HRef = aPDFFile.HRef.Substring(0, aPDFFile.HRef.ToString().LastIndexOf("/") + 1) + fileName;

            fileName = HttpUtility.UrlEncode(aMSWordFile.HRef.Substring(aMSWordFile.HRef.ToString().LastIndexOf("/") + 1));
            aMSWordFile.HRef = aMSWordFile.HRef.Substring(0, aMSWordFile.HRef.ToString().LastIndexOf("/") + 1) + fileName;


            if (e.Item.ItemIndex == 0)
            {
                JournalLink.Visible = true;
                //spanDeleteContractVersion.Visible = true;
                spanDeleteContractVersion.Visible = DeleteContractFileAccess;

                linkLatestContractVersionFile.InnerText = hdnFileName.Value.Replace("_" + GUID, "");
                trLatestContractFileHR.Visible = true;
                trLatestContractFileTITLE.Visible = true;
                trLatestContractFileLINK.Visible = true;

                if (hdnIsPDFFile.Value == "Y")
                {
                    aMSWordFile.Visible = false;
                }

                if (ViewState["isApprovalDone"].ToString() == "Y")
                {
                    if (hdnIsCheckOutDone.Value == "Y")
                    {
                        aEditQuestionnair.Visible = false;
                        linkElectronicSignature.Visible = ElectronicSignatureAccess;
                        DivQuestionnair.Visible = false;
                        linkCheckOut.Visible = false;
                        linkCheckOutSendMail.Visible = false;
                        linkTrackChanges.Visible = false;
                    }
                    else
                    {
                        linkCheckIn.Visible = false;

                        if (EditQuestionnaireAccess)
                            aEditQuestionnair.Visible = true;
                        else
                            aEditQuestionnair.Visible = false;

                        if (CheckOutAccess)
                            linkCheckOut.Visible = true;
                        else
                            linkCheckOut.Visible = false;

                        if (CheckOutSendMailAccess)
                            linkCheckOutSendMail.Visible = true;
                        else
                            linkCheckOutSendMail.Visible = false;

                        if (UploadFileAccess)
                            linkUploadFile.Visible = true;
                        else
                            linkUploadFile.Visible = false;


                        if ((((List<ContractProcedures>)(rptContractVersions.DataSource))).Count > 1)
                        {
                            if (TrackChangesAccess)
                            {
                                if (ViewState["CurrentIsValidDocument"].ToString() == "Y" && (((List<ContractProcedures>)(rptContractVersions.DataSource)))[1].IsValidDocument.ToString() == "Y")
                                    linkTrackChanges.Visible = true;
                            }
                            else
                                linkTrackChanges.Visible = false;
                        }
                        //(((List<ContractProcedures>)(rptContractVersions.DataSource)))[
                    }
                    if (ViewState["IsSignatureStatusSent"].ToString() == "Y")
                    {
                        linkUploadFile.Visible = false;
                        //linkCheckIn.Visible = false;
                        if (hdnIsCheckOutDone.Value == "Y")
                            linkCheckIn.Visible = true;
                        linkElectronicSignature.Visible = false;
                        aEditQuestionnair.Visible = false;
                    }
                    else
                    {
                        if (UploadFileAccess)
                            linkUploadFile.Visible = true;
                        else
                            linkUploadFile.Visible = false;


                        if (CheckInAccess && hdnIsCheckOutDone.Value == "Y")
                            linkCheckIn.Visible = true;
                        else
                            linkCheckIn.Visible = false;

                        if (EditQuestionnaireAccess)
                            aEditQuestionnair.Visible = true;
                        else
                            aEditQuestionnair.Visible = false;
                    }
                }
                else
                {
                    linkCheckOut.Visible = false;
                    linkCheckOutSendMail.Visible = false;
                    linkCheckIn.Visible = false;
                    linkTrackChanges.Visible = false;

                    if (EditQuestionnaireAccess)
                        aEditQuestionnair.Visible = true;
                    else
                        aEditQuestionnair.Visible = false;

                    if (UploadFileAccess)
                        linkUploadFile.Visible = true;
                    else
                        linkUploadFile.Visible = false;
                }

                if (DownloadContractAccess || TabPDFDocumentAccess)
                {
                    //Updated by Nilesh to show Docusign generated file

                    if (hdnIsIsFromDocuSign.Value == "Y")
                    {
                        aEditQuestionnair.Visible = false;
                        //spanDeleteContractVersion.Visible = true;
                        spanDeleteContractVersion.Visible = DeleteContractFileAccess;
                        JournalLink.Visible = false;

                        aMSWordFile.Visible = false;
                        aPDFFile.HRef = @"../Uploads/" + Session["TenantDIR"] + "/Contract Documents/SignedDocuments/" + HttpUtility.UrlEncode(hdnFileName.Value) + ".pdf";

                        // aPDFLatestFile.InnerText = @"../Uploads/Contract Documents/SignedDocuments/" + hdnFileName.Value + ".pdf";
                        //aPDFLatestFile.HRef = @"../Uploads/Contract Documents/SignedDocuments/" + hdnFILEPATHWITHGUID.Value + ".pdf";

                        //aPDFLatestFile.InnerText = aPDFFile.InnerText.Replace("_" + GUID, "");
                        aPDFLatestFile.HRef = aPDFFile.HRef;

                        aMSWordLatestFile.Visible = false;
                    }
                    else
                    {
                        if (hdnIsPDFFile.Value == "Y")
                        {
                            aMSWordLatestFile.Visible = false;
                        }
                        else
                        {
                            aMSWordLatestFile.Visible = DownloadContractAccess;

                            //aMSWordLatestFile.InnerText =  hdnFileName.Value.Replace("_" + GUID, "");
                            aMSWordLatestFile.HRef = @"../Uploads/" + Session["TenantDIR"] + "/Contract Documents/" + HttpUtility.UrlEncode(hdnFileName.Value) + ".docx";

                        }
                        aPDFLatestFile.HRef = @"../Uploads/" + Session["TenantDIR"] + "/Contract Documents/PDF/" + HttpUtility.UrlEncode(hdnFileName.Value) + ".pdf";
                        //aPDFLatestFile.InnerText = hdnFileName.Value.Replace("_" + GUID, "");

                        if (EditQuestionnaireAccess && ViewState["IsSignatureStatusSent"].ToString() == "N")
                            aEditQuestionnair.Visible = true;
                        else
                            aEditQuestionnair.Visible = false;
                    }
                }
            }
            else
            {
                JournalLink.Visible = false;
                spanDeleteContractVersion.Visible = DeleteContractFileAccess;
                if (hdnIsPDFFile.Value == "Y" && hdnIsUploaded.Value == "Y")
                {
                    aMSWordFile.Visible = false;
                }
            }
        }
    }

    protected void rptContractVersions_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        HiddenField hdnContractFileId = (HiddenField)e.Item.FindControl("hdnContractFileId");
        HiddenField hdnFileName = (HiddenField)e.Item.FindControl("hdnFileName");
        HiddenField hdnIsCheckOutDone = (HiddenField)e.Item.FindControl("hdnIsCheckOutDone");
        HiddenField hdnIsPDFFile = (HiddenField)e.Item.FindControl("hdnIsPDFFile");

        string ID = ((LinkButton)e.CommandSource).ID;
        if (ID == "linkCheckIn" || ID == "linkCheckOut")
        {
            IContractProcedures objContractProcedures;
            objContractProcedures = new ContractProcedures();
            objContractProcedures.ContractFileId = Convert.ToInt32(hdnContractFileId.Value);
            objContractProcedures.ContractFileActivityTypeId = ID == "linkCheckIn" ? 1 : 2;
            objContractProcedures.IsMailSent = 'N';
            objContractProcedures.ModifiedBy = Convert.ToInt32(Session[Declarations.User]);
            objContractProcedures.IpAddress = Session[Declarations.IP].ToString();
            string retVal = objContractProcedures.FileActivityInsertRecord();
            if (retVal == "1")
            {
                BindContractVersions();
                string Mode;
                if (ID == "linkCheckIn")
                    Mode = "CheckIn";
                else
                {
                    Mode = "CheckOut";
                    string IsFirstCheckOutDone = ViewState["IsFirstCheckOutDone"].ToString();
                    if (IsFirstCheckOutDone == "N")
                    {
                        try
                        {
                            SendContractGeneratedEmailToClient(Convert.ToInt32(ViewState["RequestId"]));
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
                //Page.JavaScriptClientScriptBlock(Mode, "showMessage('ContractVersions','" + Mode + "')");
                Response.Redirect("RequestFlow.aspx?Status=Workflow&Section=ContractVersions&Mode=" + Mode + "&ScrollTo=DivContractVersions&RequestId=" + ViewState["RequestId"] + ViewState["ParentRequestIDPARAM"]);
            }
        }
        else if (ID == "linkCheckOutSendMail")
        {
            Response.Redirect("CheckOutAndSendEmail.aspx?RequestId=" + ViewState["RequestId"] + "&ContractId=" + ViewState["ContractId"] + "&FileName=" + HttpUtility.UrlEncode(hdnFileName.Value) + "&ContractFileId=" + hdnContractFileId.Value + "&IsPDFFile=" + hdnIsPDFFile.Value + ViewState["ParentRequestIDPARAM"]);
        }
        else if (ID == "linkUploadFile")
        {
            Response.Redirect("UploadContractFile.aspx?RequestId=" + ViewState["RequestId"] + "&ContractId=" + ViewState["ContractId"] + "&NextFileName=" + hdnContractFileName.Value + "&ContractTypeId=" + ViewState["ContractTypeId"] + "&ContractTemplateId=" + hdnSelectedFileId.Value.Split('#')[0] + "&IsApprovalDone=" + ViewState["isApprovalDone"].ToString() + "&IsCheckOutDone=" + hdnIsCheckOutDone.Value + ViewState["ParentRequestIDPARAM"]);
        }
        else if (ID == "linkTrackChanges")
        {
            Response.Redirect("TrackChanges.aspx?rid=" + ViewState["RequestId"] + ViewState["ParentRequestIDPARAM"]);
        }
        else if (ID == "linkElectronicSignature")
        {
            Response.Redirect("../DocuSign/SendDocument.aspx?RequestId=" + ViewState["RequestId"] + "&ContractId=" + ViewState["ContractId"] + "&FileName=" + HttpUtility.UrlEncode(hdnFileName.Value) + "&ContractFileId=" + hdnContractFileId.Value + "&ClientName=" + HttpUtility.UrlEncode(lblClientNames.Text) + "&ClientEmail=" + lblClientEmailId.Text + "&isMessageAlreadySent=" + hdnMalAlreadySent.Value + "&isPDF=" + hdnIsPDFFile.Value + ViewState["ParentRequestIDPARAM"]);
        }
    }

    protected void rptRequestDocuments_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdnDocumentName = (HiddenField)e.Item.FindControl("hdnDocumentName");
            HtmlAnchor alinkWorkflowDocument = (HtmlAnchor)e.Item.FindControl("alinkWorkflowDocument");
            HtmlGenericControl spanDocumentDelete = (HtmlGenericControl)e.Item.FindControl("spanDocumentDelete");
            if (TabDocumentDownloadAccess)
                alinkWorkflowDocument.HRef = @"../Uploads/" + Session["TenantDIR"] + "/ContractDocs/" + hdnDocumentName.Value;
            spanDocumentDelete.Visible = TabDocumentDeleteAccess;
        }
    }

    #endregion

    #region Document Signature Routines Added By Nilesh

    protected void GetStatuses(string EnvelopeId)
    {
        var DD = EnvelopeId;
        try
        {
            XmlDataDocument xmldoc = new XmlDataDocument();
            XmlNodeList xmlnode;
            int i = 0;

            //string url = @"https://app.contractpod.com/DocusignService/EnvelopeCerts/" + EnvelopeId + ".xml";
            string url = ConfigurationManager.AppSettings["DocuSignServiceUrl"] + EnvelopeId + ".xml";

            //FileStream fs = new FileStream(HttpContext.Current.Server.MapPath(@"..//EnvelopeCerts//" + EnvelopeId + ".xml"), FileMode.Open, FileAccess.Read);
            xmldoc.Load(url);
            xmlnode = xmldoc.GetElementsByTagName("RecipientStatus");
            List<DocuSign> lstRecipeintStatus = new List<DocuSign>();
            int CompletedCount = 0;

            for (i = 0; i <= xmlnode.Count - 1; i++)
            {
                string Type = xmlnode[i].ChildNodes.Item(0).InnerText.Trim();
                string EMail = xmlnode[i].ChildNodes.Item(1).InnerText.Trim();
                string UserName = xmlnode[i].ChildNodes.Item(2).InnerText.Trim();
                string RoutingOrder = xmlnode[i].ChildNodes.Item(3).InnerText.Trim();
                string SentDate = xmlnode[i].ChildNodes.Item(4).InnerText.Trim();
                string Status = xmlnode[i]["Status"].InnerText.Trim();
                if (Status == "Completed")
                {
                    CompletedCount++;
                }

                lstRecipeintStatus.Add(new DocuSign() { Name = UserName, Email = EMail, EnvelopeID = EnvelopeId, Status = Status });
            }


            foreach (var item in lstRecipeintStatus)
            {
                objDocuSign.Name = item.Name;
                objDocuSign.Email = item.Email;
                objDocuSign.EnvelopeID = item.EnvelopeID;
                // objDocuSign.DocStatus = item.Status;
                if (item.Status != "Completed")
                {
                    objDocuSign.DocStatus = "Sent";
                }
                else
                {
                    objDocuSign.DocStatus = item.Status;
                }
                objDocuSign.UpdateDetails();
            }

            if (CompletedCount == xmlnode.Count)
            {
                try
                {
                    xmlnode = xmldoc.GetElementsByTagName("DocumentPDF");
                    string PDFBytes = "";
                    for (i = 0; i <= xmlnode.Count - 1; i++)
                    {
                        PDFBytes = xmlnode[i].ChildNodes.Item(1).InnerText.Trim();
                    }
                    string PathToStore = HttpContext.Current.Server.MapPath(@"..//Uploads//" + Session["TenantDIR"] + "//Contract Documents//SignedDocuments//");
                    string GUID = Guid.NewGuid().ToString();
                    //string ActualFileName = hdnContractFileName.Value.ToString() + "_" + GUID + ".pdf";
                    string FileName = PathToStore + hdnContractFileName.Value.ToString().Replace("%27", "'") + "_" + GUID + ".pdf";
                    byte[] decodedFromBase64 = Convert.FromBase64String(PDFBytes);
                    System.IO.File.WriteAllBytes(FileName, decodedFromBase64);
                    //Insert New Generated file entry in Contract Files Table.

                    ContractProcedures CP = new ContractProcedures();
                    FileInfo file = new FileInfo(FileName);
                    CP.RequestId = Convert.ToInt32(ViewState["RequestId"]);
                    CP.FileName = hdnContractFileName.Value;
                    CP.FileSizeKB = Math.Round(Convert.ToDecimal(file.Length) / 1024, 2).ToString();
                    CP.ModifiedBy = Convert.ToInt32(Session[Declarations.User].ToString());
                    CP.IpAddress = Session[Declarations.IP].ToString();
                    CP.isFromDocuSign = "Y";
                    CP.GUID = GUID;
                    string result = CP.InsertRecord();
                    isCompleted = "Y";
                    hdnMalAlreadySent.Value = "Y";

                    //if (!result.ToLower().Equals("2"))
                    {
                        string strUrlPdfFile = "";
                        strUrlPdfFile = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + HttpContext.Current.Request.Url.Segments[1] + "Uploads/" + HttpContext.Current.Session["TenantDIR"] + "/Contract Documents/SignedDocuments/" + hdnContractFileName.Value.ToString() + "_" + GUID + ".pdf";
                        AsyncIndexer.GetInstance().QueueForIndexing(strUrlPdfFile, 1, hdnRequestDate.Value);

                        SendSignatureCompletitionEmail();
                    }
                   // ArtificialIntelligence.ReadAndExtract(file.FullName, false, true, Convert.ToInt32(ViewState["RequestId"]));
                }
                catch (Exception ex)
                {

                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    #endregion



    protected void btnViewRelations_Click(object sender, EventArgs e)
    {
        //if (ViewState["ContractId"] != null && ViewState["ContractId"].ToString() != "")
        //{
        //    hdnPrimeId.Value = ViewState["ContractId"].ToString();
        //}
        Response.Redirect("ContractRelations.aspx?RequestId=" + ViewState["RequestId"] + "&ContractId=" + ViewState["ContractId"]);
        //Server.Transfer("ContractRelations.aspx",true);
    }
}

