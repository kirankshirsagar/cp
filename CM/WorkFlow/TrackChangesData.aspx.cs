﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;
//using WorkflowBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;
using System.Web.Security;
using UserManagementBLL;
using System.Web.Services;
using ClauseLibraryBLL;
using DiffPlex;
using Aspose.Words;
using System.Web.UI.HtmlControls;
using DocumentOperationsBLL;
using System.IO;
public partial class Negotiation_TrackChangesData : System.Web.UI.Page
{

    ITrackDocumentChanges objTrackDocChanges;

    // navigation//
    public static int RecordsPerPage = 2000;
    public static int VisibleButtonNumbers = 10;
    // navigation//

    public string Add;
    public string View;
    public string Update;
    public string Delete;


    private readonly static string Path = @"~/Uploads/" +HttpContext.Current.Session["TenantDIR"] + "/Contract Documents";
    private readonly static string PathForPDF = @"~/Uploads/" + HttpContext.Current.Session["TenantDIR"] + "/Contract Documents/PDF";
    private readonly static string FileExtnsn = ".docx";

    private static string GUID = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();

        Aspose.Words.License license = new Aspose.Words.License();
        license.SetLicense("Aspose.Words.lic");

        if (!IsPostBack)
        {
            ViewState["ParentRequestID"] = "";
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["ParentRequestID"])))
            {
                ViewState["ParentRequestID"] = "&ParentRequestID=" + Request.QueryString["ParentRequestID"];
            }
            GUID =  Guid.NewGuid().ToString();

            string lRequestID = Request.QueryString["RequestID"].ToString();
            lnkBtnRequestFlow.Text = "Request #" + lRequestID;        

            hdnRequestID.Value = lRequestID;

            BindControls(lRequestID);
            hdnCurrentSelectedTab.Value = "Lines";

        }

        
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            Access.PageAccess(this, Session[Declarations.User].ToString(), "WorkFlow_");
            ViewState[Declarations.Add] = Access.Add.Trim();
            ViewState[Declarations.View] = Access.View.Trim();
            ViewState[Declarations.Delete] = Access.Delete.Trim();
            ViewState[Declarations.Update] = Access.Update.Trim();
        }

        
        if (!IsPostBack || hdnSearch.Value.Trim() != string.Empty)
        {
            Session[Declarations.SortControl] = null;

            BindTrackLineChangesRepeater(1, RecordsPerPage);

            //BindTrackFieldChangesRepeater(1, RecordsPerPage);
            //BindTrackClauseChangesRepeater(1, RecordsPerPage);

            Message();
        }
        else
        {
            SetNavigationButtonParameters();
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
           objTrackDocChanges = FactoryAssembly.GetTrackDocumentChangesObject();           
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }


    public void BindControls(string RequestID)
    {
        Page.TraceWrite("BindControls starts.");
        try
        {
            objTrackDocChanges.RequestID = Convert.ToInt32(RequestID);
             DataTable dtDocumentDetails = objTrackDocChanges.GetContractFileDetails();

             if (dtDocumentDetails.Rows.Count == 2)
             {

                 string lOldFileName = Server.MapPath(Path) + "\\" + dtDocumentDetails.Rows[1]["FileName"].ToString() + FileExtnsn;
                 string lNewFileName = Server.MapPath(Path) + "\\" + dtDocumentDetails.Rows[0]["FileName"].ToString() + FileExtnsn;

                 string lOldFilePath = Server.MapPath(Path) + "\\" + dtDocumentDetails.Rows[1]["FilePath"].ToString() + FileExtnsn;
                 string lLatestFilePath = Server.MapPath(Path) + "\\" + dtDocumentDetails.Rows[0]["FilePath"].ToString() + FileExtnsn;

                 string lContractFileOldId = dtDocumentDetails.Rows[1]["ContractFileId"].ToString();
                 string lContractFileNewId = dtDocumentDetails.Rows[0]["ContractFileId"].ToString();

                 hdnOldFilePath.Value = lOldFilePath;
                 hdnLatestFilePath.Value = lLatestFilePath;

                 hdnNextVersionFileName.Value = dtDocumentDetails.Rows[0]["NextVersionFileName"].ToString() + "_" + GUID;

                 hdnContractFileOldId.Value = lContractFileOldId;
                 hdnContractFileNewId.Value = lContractFileNewId;

                 hdnOldFileVersion.Value = dtDocumentDetails.Rows[1]["Version"].ToString();
                 hdnLatestFileVersion.Value = dtDocumentDetails.Rows[0]["Version"].ToString();

                 objTrackDocChanges.ContractFileOldId = Convert.ToInt32(lContractFileOldId);
                 objTrackDocChanges.ContractFileNewId = Convert.ToInt32(lContractFileNewId);


                 lblContractID.Text = dtDocumentDetails.Rows[0]["ContractID"].ToString();
                 lblContractType.Text = dtDocumentDetails.Rows[0]["ContractTypeName"].ToString();
                 lblVersionNewDoc.Text = "Version " + dtDocumentDetails.Rows[0]["Version"].ToString() + " Document";
                 lblNewVersionDocName.Text = dtDocumentDetails.Rows[0]["FileName"].ToString();
                 lblVersionPrevDoc.Text = "Version " + dtDocumentDetails.Rows[1]["Version"].ToString() + " Document";
                 lblPrevVersionDocName.Text = dtDocumentDetails.Rows[1]["FileName"].ToString();





                 //************** CHECK DOCUMENT VALID OR NOT *******************
                              

                 Aspose.Words.Document docOld = new Aspose.Words.Document(lOldFilePath);
                 Aspose.Words.Document docNew = new Aspose.Words.Document(lLatestFilePath);

                 hdnHideActionButtons.Value = "N";
                 if (docOld.Sections.Count > 1 || docNew.Sections.Count > 1)
                     hdnHideActionButtons.Value = "Y";

                 //for (int i = 0; i < docOld.Sections.Count; i++)
                 //{
                 //    if (docOld.Sections[i].Body.Tables.Count > 1)
                 //    {
                 //        hdnHideActionButtons.Value = "Y";
                 //        break;
                 //    }
                 //}
                 //for (int i = 0; i < docNew.Sections.Count; i++)
                 //{
                 //    if (docNew.Sections[i].Body.Tables.Count > 1)
                 //    {
                 //        hdnHideActionButtons.Value = "Y";
                 //        break;
                 //    }
                 //}
             }
        }
        catch (Exception ex)
        {
            Page.TraceWarn("BindControls fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("BindControls ends.");
    }

  


    protected void btnSortField_Click(object sender, EventArgs e)
    {
        //hdnCurrentSelectedTab.Value = "Terms";
        //hdnSearch.Value = "";
        //LinkButton clickBtn = (LinkButton)sender;
        //Session.Add(Declarations.SortControl, clickBtn);
        //BindTrackFieldChangesRepeater(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
    }

    protected void btnSortClause_Click(object sender, EventArgs e)
    {
        //hdnCurrentSelectedTab.Value = "Clauses";
        //hdnSearch.Value = "";
        //LinkButton clickBtn = (LinkButton)sender;
        //Session.Add(Declarations.SortControl, clickBtn);
        //BindTrackClauseChangesRepeater(PaginationButtons2.PageNumber, PaginationButtons2.RecordsPerPage);
    }

    protected void btnSortLine_Click(object sender, EventArgs e)
    {
        hdnCurrentSelectedTab.Value = "Lines";
        hdnSearch.Value = "";
        LinkButton clickBtn = (LinkButton)sender;
        Session.Add(Declarations.SortControl, clickBtn);
        BindTrackLineChangesRepeater(PaginationButtons3.PageNumber, PaginationButtons3.RecordsPerPage);
    }

    void Message()
    {
        if (Session[Declarations.Message] != null)
        {
            var flg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }
    }

    // navigation//
    #region Navigation
    void SetNavigationButtonParameters()
    {
        //PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        //PaginationButtons1.RecordsPerPage = RecordsPerPage;
        //PaginationButtons1.TotalRecord = ViewState["FieldTotalRecord"] != null ? (long)ViewState["FieldTotalRecord"] : 0;

        //PaginationButtons2.VisibleButtonNumbers = VisibleButtonNumbers;
        //PaginationButtons2.RecordsPerPage = RecordsPerPage;
        //PaginationButtons2.TotalRecord = ViewState["ClauseTotalRecord"] != null ? (long)ViewState["ClauseTotalRecord"] : 0;

        PaginationButtons3.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons3.RecordsPerPage = RecordsPerPage;
        PaginationButtons3.TotalRecord = ViewState["LineTotalRecord"] != null ? (long)ViewState["LineTotalRecord"] : 0;
    }
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        //if (PaginationButtons1.PageNumber > 0)
        //{
        //    BindTrackFieldChangesRepeater(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
        //   
        //}
        //if (PaginationButtons2.PageNumber > 0)
        //{
        //    BindTrackClauseChangesRepeater(PaginationButtons2.PageNumber, PaginationButtons2.RecordsPerPage);
        //   
        //}

        if (PaginationButtons3.PageNumber > 0)
        {
            BindTrackLineChangesRepeater(PaginationButtons3.PageNumber, PaginationButtons3.RecordsPerPage);
           
        }
        
        
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {

    }
    #endregion



    // navigation//
    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }
    


    /********************** Commented Terms and Clause Binding feature *********************
    void BindTrackFieldChangesRepeater(int pageNo, int recordsPerPage)
    {
        //if (View == "Y")
        //{
        Page.TraceWrite("BindTrackFieldChangesRepeater starts.");
            try
            {
                objTrackDocChanges.ContractFileOldId = Convert.ToInt32(hdnContractFileOldId.Value);
                objTrackDocChanges.ContractFileNewId = Convert.ToInt32(hdnContractFileNewId.Value);

                objTrackDocChanges.PageNo = pageNo;
                objTrackDocChanges.RecordsPerPage = recordsPerPage;
                objTrackDocChanges.Search ="";
                LinkButton btnSort = null;
                if (Session[Declarations.SortControl] != null && Session[Declarations.SortControl] != string.Empty)
                {
                    btnSort = (LinkButton)Session[Declarations.SortControl];
                    objTrackDocChanges.Direction = btnSort.CssClass.ToLower() == "sort desc" ? 1 : 0;
                    objTrackDocChanges.SortColumn = btnSort.Text.Trim();
                }

                rptTrackFieldChanges.DataSource = objTrackDocChanges.GetTrackChangesTermsDetails();
                rptTrackFieldChanges.DataBind();
                if (btnSort != null)
                {
                    rptTrackFieldChanges.ClassChange(btnSort);
                }
                Page.TraceWrite("BindTrackFieldChangesRepeater ends.");               

                ViewState["FieldTotalRecord"] = objTrackDocChanges.TotalRecords;

                hdnTermsRecordCount.Value = objTrackDocChanges.TotalRecords.ToString();

                PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
                PaginationButtons1.TotalRecord = ViewState["FieldTotalRecord"] != null ? (long)ViewState["FieldTotalRecord"] : 0;
                PaginationButtons1.RecordsPerPage = RecordsPerPage;
            }
            catch (Exception ex)
            {
                Page.TraceWarn("BindTrackFieldChangesRepeater fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        //}

    }
    void BindTrackClauseChangesRepeater(int pageNo, int recordsPerPage)
    {

        //if (View == "Y")
        //{
        Page.TraceWrite("BindTrackClauseChangesRepeater starts.");
        try
        {
            objTrackDocChanges.ContractFileOldId = Convert.ToInt32(hdnContractFileOldId.Value);
            objTrackDocChanges.ContractFileNewId = Convert.ToInt32(hdnContractFileNewId.Value);

            objTrackDocChanges.PageNo = pageNo;
            objTrackDocChanges.RecordsPerPage = recordsPerPage;
            objTrackDocChanges.Search = "";
            LinkButton btnSort = null;
            if (Session[Declarations.SortControl] != null && Session[Declarations.SortControl] != string.Empty)
            {
                btnSort = (LinkButton)Session[Declarations.SortControl];
                objTrackDocChanges.Direction = btnSort.CssClass.ToLower() == "sort desc" ? 1 : 0;
                objTrackDocChanges.SortColumn = btnSort.Text.Trim();
            }
            rptTrackClauseChanges.DataSource = objTrackDocChanges.GetTrackChangesClauseDetails();
            rptTrackClauseChanges.DataBind();
            if (btnSort != null)
            {
                rptTrackClauseChanges.ClassChange(btnSort);
            }
            Page.TraceWrite("BindTrackClauseChangesRepeater ends.");
            ViewState["ClauseTotalRecord"] = objTrackDocChanges.TotalRecords;

            hdnClausesRecordCount.Value = objTrackDocChanges.TotalRecords.ToString();
            
            PaginationButtons2.VisibleButtonNumbers = VisibleButtonNumbers;
            PaginationButtons2.TotalRecord = ViewState["ClauseTotalRecord"] != null ? (long)ViewState["ClauseTotalRecord"] : 0;
            PaginationButtons2.RecordsPerPage = RecordsPerPage;
        }
        catch (Exception ex)
        {
            Page.TraceWarn("BindTrackClauseChangesRepeater fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        //}

    }
    ***********************************************************************************************/


    void BindTrackLineChangesRepeater(int pageNo, int recordsPerPage)
    {

        //if (View == "Y")
        //{
        Page.TraceWrite("BindTrackLineChangesRepeater starts.");
        try
        {
            objTrackDocChanges.ContractFileOldId = Convert.ToInt32(hdnContractFileOldId.Value);
            objTrackDocChanges.ContractFileNewId = Convert.ToInt32(hdnContractFileNewId.Value);

            objTrackDocChanges.PageNo = pageNo;
            objTrackDocChanges.RecordsPerPage = recordsPerPage;
            objTrackDocChanges.Search = "";
            LinkButton btnSort = null;
            if (Session[Declarations.SortControl] != null && Convert.ToString(Session[Declarations.SortControl]).Equals(string.Empty))
            {
                btnSort = (LinkButton)Session[Declarations.SortControl];
                objTrackDocChanges.Direction = btnSort.CssClass.ToLower() == "sort desc" ? 1 : 0;
                objTrackDocChanges.SortColumn = btnSort.Text.Trim();
            }
            rptTrackLineChanges.DataSource = objTrackDocChanges.GetTrackChangesLineDetails();
            rptTrackLineChanges.DataBind();
            if (btnSort != null)
            {
                rptTrackLineChanges.ClassChange(btnSort);
            }
            Page.TraceWrite("BindTrackLineChangesRepeater ends.");
            ViewState["LineTotalRecord"] = objTrackDocChanges.TotalRecords;

            hdnLinesRecordCount.Value = objTrackDocChanges.TotalRecords.ToString();

            PaginationButtons3.VisibleButtonNumbers = VisibleButtonNumbers;
            PaginationButtons3.TotalRecord = ViewState["LineTotalRecord"] != null ? (long)ViewState["LineTotalRecord"] : 0;
            PaginationButtons3.RecordsPerPage = RecordsPerPage;
        }
        catch (Exception ex)
        {
            Page.TraceWarn("BindTrackLineChangesRepeater fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        //}

    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        GenerateDocument();
    }





    private void GenerateDocument()
    {
        Page.TraceWrite("GenerateDocument starts.");
        System.Threading.Thread.Sleep(1000);

        Aspose.Words.License license = new Aspose.Words.License();
        license.SetLicense("Aspose.Words.lic");
       
        bool isExists = System.IO.Directory.Exists(Server.MapPath(Path));
        if (!isExists)
        {
            System.IO.Directory.CreateDirectory(Server.MapPath(Path));
        }
       
        try
        {
            string lOldFilePath = Convert.ToString(hdnOldFilePath.Value);
            string lNewFilePath = Convert.ToString(hdnLatestFilePath.Value);

            string lNextVersionFileName = Convert.ToString(hdnNextVersionFileName.Value);

            Aspose.Words.Document docOld = new Aspose.Words.Document(lOldFilePath);
            Aspose.Words.Document ldocNewTemp = new Aspose.Words.Document(lNewFilePath);           

            #region Accept Reject Terms and Clauses Changes
            /**********************************************************************************************************
            foreach (RepeaterItem ri in rptTrackFieldChanges.Items)
            {
                Label llblFTrackChangesFieldID = ri.FindControl("lblFTrackChangesFieldID") as Label;
                Label llblFFieldBookmarkName = ri.FindControl("lblFFieldBookmarkName") as Label;
                Label llblFFieldOldValue = ri.FindControl("lblFFieldOldValue") as Label;
                Label llblFFieldNewValue = ri.FindControl("lblFFieldNewValue") as Label;
                HtmlInputText ltxtFFieldNewValue = ri.FindControl("txtFFieldNewValue") as HtmlInputText;
                HtmlInputHidden lhdnFIsModifiedAgain = ri.FindControl("hdnFIsModifiedAgain") as HtmlInputHidden;

                HtmlInputRadioButton rdoFAccept = ri.FindControl("rdoFAccept") as HtmlInputRadioButton;
                HtmlInputRadioButton rdoFReject = ri.FindControl("rdoFReject") as HtmlInputRadioButton;

                bool IsAccepted = rdoFAccept.Checked;

                string IsModifiedAgain=Convert.ToString(lhdnFIsModifiedAgain.Value);

                string lNewValue = llblFFieldNewValue.Text.Replace("<font color='red'>", "").Replace("</font>", "").Replace("<br>", Environment.NewLine);
                string lOldValue = llblFFieldOldValue.Text.Replace("<font color='red'>", "").Replace("</font>", "").Replace("<br>", Environment.NewLine);

                if (IsModifiedAgain.Equals("Y"))
                {
                    lNewValue= ltxtFFieldNewValue.Value;
                }

                if (IsAccepted == true)
                {
                    if (docNew.Range.Bookmarks[llblFFieldBookmarkName.Text.Trim()] != null)
                    {
                        Bookmark lbookmark = docNew.Range.Bookmarks[llblFFieldBookmarkName.Text.Trim()];
                        lbookmark.Text = lNewValue;
                    }
                }
                else
                {
                    if (docNew.Range.Bookmarks[llblFFieldBookmarkName.Text.Trim()] != null)
                    {
                        Bookmark lbookmark = docNew.Range.Bookmarks[llblFFieldBookmarkName.Text.Trim()];
                        lbookmark.Text = lOldValue;
                    }
                }
            }          
           

            for (int i = 0; i < rptTrackClauseChanges.Items.Count; i++)
            {
                RepeaterItem ri = rptTrackClauseChanges.Items[i];

                Label lblCTrackChangesClauseID = ri.FindControl("lblCTrackChangesClauseID") as Label;
                Label lblCClauseBookmarkName = ri.FindControl("lblCClauseBookmarkName") as Label;
               
                HtmlInputHidden lhdnCIsModifiedAgain = ri.FindControl("hdnCIsModifiedAgain") as HtmlInputHidden;
                HtmlInputHidden hdnClauseNewValueModified = ri.FindControl("hdnClauseNewValueModified") as HtmlInputHidden;

                HtmlInputHidden hdnClauseOldValue = ri.FindControl("hdnClauseOldValue") as HtmlInputHidden;
                

                HtmlInputRadioButton rdoCAccept = ri.FindControl("rdoCAccept") as HtmlInputRadioButton;
                HtmlInputRadioButton rdoCReject = ri.FindControl("rdoCReject") as HtmlInputRadioButton;
                bool IsAccepted = rdoCAccept.Checked;
                string IsModifiedAgain = Convert.ToString(lhdnCIsModifiedAgain.Value);


                string lNewValue = hdnClauseNewValueModified.Value.Replace("<font color='red'>", "").Replace("</font>", "").Replace("<br>", Environment.NewLine);
                lNewValue = lNewValue.Replace("\n", Environment.NewLine);

                string lOldValue = hdnClauseOldValue.Value.Replace("<font color='red'>", "").Replace("</font>", "").Replace("<br>", Environment.NewLine);
                lOldValue = lOldValue.Replace("\n", Environment.NewLine);

                if (IsAccepted == true)
                {                   
                    if (docNew.Range.Bookmarks[lblCClauseBookmarkName.Text.Trim()] != null)
                    {
                        Bookmark lbookmark = docNew.Range.Bookmarks[lblCClauseBookmarkName.Text.Trim()];
                        lbookmark.Text = lNewValue;
                    }
                }
                else
                {
                    if (docNew.Range.Bookmarks[lblCClauseBookmarkName.Text.Trim()] != null)
                   
                    {
                        Bookmark lbookmark = docNew.Range.Bookmarks[lblCClauseBookmarkName.Text.Trim()];
                        lbookmark.Text = lOldValue;
                    } 
                   
                }               
            }
            **********************************************************************************************/
            #endregion

            string lOldFileText = docOld.ToString(SaveFormat.Text);
            int lOldDocParaCnt = lOldFileText.Where(c => c == '\n').Count();

            string lNewFileText = ldocNewTemp.ToString(SaveFormat.Text);
            int lNewDocParaCnt = lNewFileText.Where(c => c == '\n').Count();

            DocumentBuilder lbuilderTemp = new DocumentBuilder(ldocNewTemp);
            for (int i = 0; i < lOldDocParaCnt-lNewDocParaCnt; i++)
            {
                lbuilderTemp.MoveToDocumentEnd();
                lbuilderTemp.Writeln(Environment.NewLine);                
            }
            ldocNewTemp.Save(lNewFilePath);

            Aspose.Words.Document docNew = new Aspose.Words.Document(lNewFilePath);

            string NewFileText = docNew.ToString(SaveFormat.Text);
            int NewDocParaCnt = NewFileText.Where(c => c == '\n').Count();


            //******************* 3 November 2014******************************
            //int cntr = docNew.Sections.Count;
            //RemoveSectionBreaks(docNew);
            //RemovePageBreaks(docNew);
            //*************************************************


            DocumentBuilder builder = new DocumentBuilder(docNew);
            DocumentBuilder builderOld = new DocumentBuilder(docOld);


            bool flgLineAddedAtEnd = false;
            int lDeletedParagraphSkiped = 0;
            for (int i = 0; i < rptTrackLineChanges.Items.Count; i++)
            {
                RepeaterItem ri = rptTrackLineChanges.Items[i];

                Label lblLTrackChangesLineID = ri.FindControl("lblLTrackChangesLineID") as Label;
                Label lblLLineBookmarkName = ri.FindControl("lblLLineBookmarkName") as Label;
                Label lblLTrackChangesLineNo = ri.FindControl("lblLTrackChangesLineNo") as Label;

                HtmlInputHidden hdnStatus = ri.FindControl("hdnStatus") as HtmlInputHidden;
                

                HtmlInputHidden lhdnLIsModifiedAgain = ri.FindControl("hdnLIsModifiedAgain") as HtmlInputHidden;
                HtmlInputHidden hdnLineNewValueModified = ri.FindControl("hdnLineNewValueModified") as HtmlInputHidden;

                HtmlInputHidden hdnLineOldValue = ri.FindControl("hdnLineOldValue") as HtmlInputHidden;

                HtmlInputRadioButton rdoCAccept = ri.FindControl("rdoCAccept") as HtmlInputRadioButton;
                HtmlInputRadioButton rdoCReject = ri.FindControl("rdoCReject") as HtmlInputRadioButton;


                bool IsAccepted = rdoCAccept.Checked;

                string lStatus = Convert.ToString(hdnStatus.Value);

                //******** 3 Nov 2014
                int lParagraphNumber = Convert.ToInt32(lblLTrackChangesLineNo.Text) - 1-lDeletedParagraphSkiped;

                string IsModifiedAgain = Convert.ToString(lhdnLIsModifiedAgain.Value);

                string lNewValue = hdnLineNewValueModified.Value.Replace("<font color='red'>", "").Replace("</font>", "").Replace("<br>", Environment.NewLine);
                lNewValue = lNewValue.Replace("\n", Environment.NewLine).Replace("<font color='blue'>", "").Replace("<font color='green'>", "").Replace("<strike>", "").Replace("</strike>", "");
                lNewValue = lNewValue.Replace("&nbsp;","\t");


                string lOldValue = hdnLineOldValue.Value.Replace("<font color='red'>", "").Replace("</font>", "").Replace("<br>", Environment.NewLine);
                lOldValue = lOldValue.Replace("\n", Environment.NewLine).Replace("<font color='blue'>", "").Replace("<font color='green'>", "").Replace("<strike>", "").Replace("</strike>", "");
                lOldValue = lOldValue.Replace("&nbsp;", "\t");


                if (lParagraphNumber <= NewDocParaCnt)
                {         


                    if (lStatus.Equals("D"))
                    {

                        if (lStatus.Equals("D"))
                        {
                            if (IsAccepted == true && lNewValue.Trim().Length <= 0)
                            {
                                lDeletedParagraphSkiped++;
                                continue;
                            }
                        }

                        if (IsAccepted == true)
                        {
                            builder.MoveToParagraph(lParagraphNumber, 0);
                            builder.Writeln(lNewValue);
                        }
                        else
                        {
                            builder.MoveToParagraph(lParagraphNumber, 0);
                            builder.Writeln(lOldValue);
                        }
                       
                    }
                    else
                    {
                        builder.MoveToParagraph(lParagraphNumber, 0);
                        Paragraph curParagraph = builder.CurrentParagraph;
                        curParagraph.RemoveAllChildren();                         
                   
                                    

                        if (IsAccepted == true)
                        {
                            Run run = new Run(docNew);
                            run.Text = lNewValue;
                            curParagraph.AppendChild(run);

                        }
                        else
                        {                        

                            Run run = new Run(docNew);
                            run.Text = lOldValue;
                            curParagraph.AppendChild(run);

                        }
                    }
                }
                else
                {
                    if (lStatus.Equals("D"))
                    {
                        if (IsAccepted == true && lNewValue.Trim().Length <= 0)
                        {
                            continue;
                        }
                    }

                    if (flgLineAddedAtEnd == false)
                    {
                        builder.MoveToDocumentEnd();
                        builder.Writeln(Environment.NewLine);
                        flgLineAddedAtEnd = true;
                    }

                    if (IsAccepted == true)
                    {
                        builder.MoveToDocumentEnd();
                        builder.Writeln(lNewValue);
                    }
                    else
                    {
                        builder.MoveToDocumentEnd();
                        builder.Writeln(lOldValue);
                    }
                }
            }            

            docNew.Save(Server.MapPath(Path) + "//" + lNextVersionFileName+FileExtnsn,SaveFormat.Docx);
            docNew.Save(Server.MapPath(PathForPDF) + "//" + lNextVersionFileName +".pdf", SaveFormat.Pdf);
            //CopyHeaderFooterFromOldDocument();
            SaveContractFile();
            Response.Redirect("RequestFlow.aspx?Status=Workflow&Section=ContractVersions&Mode=TrackChanges&ScrollTo=DivContractVersions&RequestId=" + hdnRequestID.Value + ViewState["ParentRequestID"], false);

           
        }
        catch (Exception ex)
        {
            Page.TraceWarn("GenerateDocument fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

        Page.TraceWrite("GenerateDocument Ends.");
    }




    //************************ 3 NOVEMBER 2014 *******************************
    private static void RemovePageBreaks(Document doc)
    {
        // Retrieve all paragraphs in the document.
        NodeCollection paragraphs = doc.GetChildNodes(NodeType.Paragraph, true);

        // Iterate through all paragraphs
        foreach (Paragraph para in paragraphs)
        {
            // If the paragraph has a page break before set then clear it.
            if (para.ParagraphFormat.PageBreakBefore)
                para.ParagraphFormat.PageBreakBefore = false;

            // Check all runs in the paragraph for page breaks and remove them.
            foreach (Run run in para.Runs)
            {
                if (run.Text.Contains(ControlChar.PageBreak))
                    run.Text = run.Text.Replace(ControlChar.PageBreak, string.Empty);
            }

        }

    }


    private static void RemoveSectionBreaks(Document doc)
    {
        // Loop through all sections starting from the section that precedes the last one 
        // and moving to the first section.
        for (int i = doc.Sections.Count - 2; i >= 0; i--)
        {
            // Copy the content of the current section to the beginning of the last section.
            doc.LastSection.PrependContent(doc.Sections[i]);
            // Remove the copied section.
            doc.Sections[i].Remove();
        }
    }






    public void CopyHeaderFooterFromOldDocument()
    {
        try
        {

            //source document which contains header and footer.
            Document originalDoc = new Document(Server.MapPath(Path) + "//" + "New-Custromer_Do-not-use_Create-New-Contract_Version_3.docx");
            //Create new document for the input template which has to be branded.
            Document existingDoc = new Document(Server.MapPath(Path) + "//" + "New-Custromer_Do-not-use_Create-New-Contract_Version_4.docx");
            //Final output file.
            String targetFileName = Server.MapPath(Path)+"//santmax.docx";
            DocumentBuilder builder = new DocumentBuilder(existingDoc);        



            NodeImporter importer = new NodeImporter(originalDoc, existingDoc, ImportFormatMode.KeepSourceFormatting);
            //int sectionIndex = 0;
            SectionCollection originalDocSections = originalDoc.Sections;
            Section originalDocSection = null;

            for (int sectionIndex = 0; sectionIndex < originalDocSections.Count; sectionIndex++)
            {

                originalDocSection = originalDocSections[sectionIndex];

                //create section in the new document
                Section newSection = null;

                if (existingDoc.Sections[sectionIndex] == null)
                {

                    //Import SectionStart
                    builder.MoveToDocumentEnd();
                    SectionStart sectionStart = originalDocSection.PageSetup.SectionStart;;

                    switch (sectionStart)
                    {

                        case SectionStart.Continuous:

                            builder.InsertBreak(BreakType.SectionBreakContinuous);
                            break;

                        case SectionStart.EvenPage:

                            builder.InsertBreak(BreakType.SectionBreakEvenPage);

                            break;

                        case SectionStart.NewColumn:

                            builder.InsertBreak(BreakType.SectionBreakNewColumn);

                            break;

                        case SectionStart.NewPage:

                            builder.InsertBreak(BreakType.SectionBreakNewPage);

                            break;

                        case SectionStart.OddPage:

                            builder.InsertBreak(BreakType.SectionBreakOddPage);

                            break;

                    }

                    newSection = builder.CurrentSection;

                }

                else
                {

                    newSection = existingDoc.Sections[sectionIndex];

                }

                //Remove Headers/Footers from new section

                newSection.HeadersFooters.Clear();



                HeaderFooterCollection headersFooters = originalDocSection.HeadersFooters;

                HeaderFooter headerFooter = null;

                // //import headers and footers

                for (int headersFootersIndex = 0; headersFootersIndex < headersFooters.Count; headersFootersIndex++)
                {

                    headerFooter = headersFooters[headersFootersIndex];

                    newSection.HeadersFooters.Add(importer.ImportNode(headerFooter, true));

                }

                newSection.PageSetup.DifferentFirstPageHeaderFooter=originalDocSection.PageSetup.DifferentFirstPageHeaderFooter;

            }

            existingDoc.Save(targetFileName, SaveFormat.Docx);
        }
        catch (Exception)
        {

            throw;
        }

    }






    //*********************************************************************************






















    protected void SaveContractFile()
    {
        ContractProcedures CP = new ContractProcedures();
        FileInfo file = new FileInfo(Server.MapPath(Path) + "//" + Convert.ToString(hdnNextVersionFileName.Value)+FileExtnsn);
       
        CP.RequestId = Convert.ToInt32(hdnRequestID.Value);
        CP.FileName = hdnNextVersionFileName.Value;
        CP.FileSizeKB = Math.Round(Convert.ToDecimal(file.Length) / 1024, 2).ToString();
        CP.ModifiedBy = Convert.ToInt32(Session[Declarations.User]);
        CP.IpAddress = Session[Declarations.IP].ToString();
        CP.IsValidDocument = 'Y';
        CP.GUID = GUID;
        string result = CP.InsertRecord();
    }
    protected void lnkBtnRequestFlow_Click(object sender, EventArgs e)
    {
        Response.Redirect("RequestFlow.aspx?Status=Workflow&ScrollTo=DivContractVersions&RequestId=" + hdnRequestID.Value + ViewState["ParentRequestID"], false);
    }


   
}