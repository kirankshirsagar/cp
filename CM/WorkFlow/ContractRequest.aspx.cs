﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;
using WorkflowBLL;
using System.Data;
using UserManagementBLL;
using System.Web.Services;
using ClauseLibraryBLL;
using System.Web.Script.Serialization;
using System.Web.UI.HtmlControls;
using MetaDataConfiguratorsBLL;
using System.Configuration;
using System.Text;
using System.IO;

public partial class Workflow_ContractRequest : System.Web.UI.Page
{
    ICountry objCountry;
    IContractType objcontracttype;
    IUsers objuser;
    IContractRequest objcontractreq;
    IClauseLibrary ObjDocumentType;
    static string XMLstring = "";
    static List<ContractRequest> list;
    static DataTable dt = new DataTable();
    static DataTable dts = new DataTable();
    static DataSet ds = new DataSet();
    string autoRenewalCheck = "";
    public string ClientData { get; set; }
    List<SelectControlFields> _data = new List<SelectControlFields>();
    public string Add;
    public string View;
    public string Update;
    public string Delete;
    public string SendNotification;
    public string RequestType = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        #region

        autoRenewalCheck = "";

        if (Request.QueryString["ch"] != null)
        {
            RequestType = Request.QueryString["ch"].ToString();
        }

        if (Request.QueryString["isAutoRenewal"] != null)
        {
            autoRenewalCheck = "1";
        }

        if (RequestType == string.Empty)
        {
            RequestType = "cnc";
            ApprovalP.Style.Add("display", "none");
        }

        if (RequestType != "cnc" && RequestType != "nrr")
        {
            ApprovalP.Style.Add("display", "none");
            ContractIDPara.Style.Add("display", "");
            txtContractID.Attributes.Add("class", "required mobile");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "tytyttrrrr", "SetWaterMark();", true);
        }
        else
        {
            ApprovalP.Style.Add("display", "none");
            ContractIDPara.Style.Add("display", "none");
            txtContractID.Attributes.Add("class", "mobile");
        }
        if (RequestType == "nrr")
        {
            EV.Style.Add("display", "");
        }
        else
        {
            EV.Style.Add("display", "none");
        }
        hdnrequesttype.Value = RequestType;
        #endregion
        dt.Rows.Clear();
        CreateObjects();
        Session["MasterSeelcted"] = null;
        Session["ChlidSeelcted"] = null;
        XMLstring = "";
        msgEdit.InnerHtml = "";
        ddlDocumentType.extDataBind(ObjDocumentType.SelectDocumentType());       

        if (Request.QueryString["ParentRequestId"] != null)
        {
            ViewState["ParentRequestID"] = Request.QueryString["ParentRequestId"];
        }
        if (!IsPostBack)
        {
            if (Session[Declarations.User] != null && !IsPostBack)
            {
                Access.PageAccess(this, Session[Declarations.User].ToString(), "WorkFlow_");
                ViewState[Declarations.Add] = Access.isCustomised(4);
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
                hdnUserID.Value = Session[Declarations.User].ToString();

                ViewState["RedirectedFrom"] = Request.QueryString["RedirectedFrom"];

            }

            AccessVisibility();
            BindDropDowns();

            if (String.IsNullOrEmpty(Request.extValue("hdnRequestId")) != true)
            {
                hdnRequestId.Value = Request.extValue("hdnRequestId");
                BindDropDowns();
                ReadData();
                hdnClientNameHidden.Value = txtclientname.Text;
            }
            //sk
            else if (String.IsNullOrEmpty(Request.QueryString["RequestId"]) != true)
            {
                hdnRequestId.Value = Request.QueryString["RequestId"].ToString();
                BindDropDowns();
                ReadData();
                hdnClientNameHidden.Value = txtclientname.Text;
                if (RequestType == "cnc" || RequestType == "nrr")
                {
                    lblNotifyCustomer.Visible = true;
                    ddlNotifyCustomer.Visible = true;
                }
            }
            else
            {
                hdnRequestId.Value = "0";
                if (RequestType == "cnc")
                {
                    lblTitle.Text = "Add Contract Request";
                    lblNotifyCustomer.Visible = true;
                    ddlNotifyCustomer.Visible = true;
                }
                if (RequestType == "rev")
                {
                    lblTitle.Text = "Revise existing contract";
                }
                if (RequestType == "ren")
                {
                    lblTitle.Text = "Renew existing contract";
                }
                if (RequestType == "ar")
                {
                    lblTitle.Text = "Auto renewal";
                }
                if (RequestType == "nrr")
                {
                    lblTitle.Text = "New Review Request (Client Contract)";
                    lblNotifyCustomer.Visible = true;
                    ddlNotifyCustomer.Visible = true;
                }
                if (RequestType == "tc")
                {
                    lblTitle.Text = "Terminate contract";
                }
                if (RequestType == "dc")
                {
                    lblTitle.Text = "Delete contract";
                }
            }
            BindContractRequest(hdnRequestId.Value);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "abmsg", " HideShowPReason();", true);
        }

        if (lblTitle.Text.IndexOf("Edit Contract Request") > -1 || autoRenewalCheck == "1")
        {
            if (lblTitle.Text.IndexOf("Revise Existing Contract") > -1)
            {
                RequestType = "rev";
                lblNotifyCustomer.Visible = false;
                ddlNotifyCustomer.Visible = false;

            }
            if (lblTitle.Text.IndexOf("Renew Existing Contract") > -1)
            {
                RequestType = "ren";
                lblNotifyCustomer.Visible = false;
                ddlNotifyCustomer.Visible = false;
            }
            if (lblTitle.Text.IndexOf("Auto Renewal") > -1)
            {
                RequestType = "ar";
            }

            if (lblTitle.Text.IndexOf("Terminate Contract") > -1)
            {
                RequestType = "tc";
                lblNotifyCustomer.Visible = false;
                ddlNotifyCustomer.Visible = false;
            }
            if (lblTitle.Text.IndexOf("New Review Request") > -1)
            {
                RequestType = "nrr";
            }

            if (lblTitle.Text.IndexOf("Add Contract Request") > -1)
            {
                RequestType = "cnc";
            }
        }

        if (RequestType == "cnc" || RequestType == "nrr")
        {
            _data = objcontractreq.SelectClientData();
        }
        else
        {
            objcontractreq.UserID = int.Parse(Session[Declarations.User].ToString());
            _data = objcontractreq.SelectClientDataWithContract();
        }
        JavaScriptSerializer jss = new JavaScriptSerializer();
        ClientData = jss.Serialize(_data); //this make your list in jSON format like [88,99,10]
        ClientData = ClientData.Replace("Id", "value").Replace("Name", "label");

        if (lblTitle.Text.IndexOf("Edit Contract Request") > -1 || autoRenewalCheck == "1")
        {
            ds.Tables.Clear();
            dt.Rows.Clear();
            string PrimaryAddress = GetContactDetails(hdnRequestId.Value);
            
            //******************************** CHANGED ***************************************
            if (lblTitle.Text.IndexOf("Revise Existing Contract") > -1)
            {
                RequestType = "rev";
                txtContractID.Disabled = true;
                ParaSearchBy.Visible = false;
            }
            if (lblTitle.Text.IndexOf("Renew Existing Contract") > -1)
            {
                RequestType = "ren";
                txtContractID.Disabled = true;
                ParaSearchBy.Visible = false;
            }
            if (lblTitle.Text.IndexOf("Auto Renewal") > -1)
            {
                RequestType = "ar";
            }

            if (lblTitle.Text.IndexOf("Terminate Contract") > -1)
            {
                RequestType = "tc";
                txtContractID.Disabled = true;
                ParaSearchBy.Visible = false;
            }

            if (lblTitle.Text.IndexOf("Revise Existing Contract") > -1 ||
                lblTitle.Text.IndexOf("Renew Existing Contract") > -1 ||
                lblTitle.Text.IndexOf("Auto Renewal") > -1 ||
                lblTitle.Text.IndexOf("Terminate contract") > -1)
            {
            }
            else
            {
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "tytytt", "LoadEditWindow('Edit');", true);
            Button1.Visible = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "tytyttrrr", "LoadEditWindowNew('" + hdnclientID.Value + "','" + PrimaryAddress + "');", true);
            //*************************************************************************************************************
        }
        else
        {
            try
            {
                ddlassignto.extSelectedValues(hdnUserID.Value);
            }
            catch (Exception ex)
            {
            }
            hdnAssinedToUser.Value = hdnUserID.Value;
        }
        if (Session[Declarations.User] != null)
        {
            NotificationVisibility();
        }
    }

    #region for Requestform

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);
        if (Request.Params["ctl00$MainContent$btnBack"] == null)
        {
            BindContractRequest(hdnRequestId.Value);
        }
    }
    private void BindContractRequest(string RequestId)
    {

        IMetaDataConfigurators objMetaDataConfig = FactoryMedaData.GetContractTemplateDetail();
        HtmlTable container = objMetaDataConfig.GenerateRequestForm("Request Form", RequestId, "edit");

        int returnTableRow = container.Rows.Count;
        int cnt = MetaDataTable.Rows.Count;

        for (int i = 0; i < returnTableRow; i++)
        {
            MetaDataTable.Rows.Add(container.Rows[0]);
        }
    }

    public DataTable SaveData(Control pnl)
    {
        Control ctrl = pnl;
        DataTable dt = new DataTable();
        dt.Columns.Add("MetaDataFieldId", Type.GetType("System.Int32"));
        dt.Columns.Add("FieldValue", Type.GetType("System.String"));
        string InputValue = "", ID = "", Master = "", FieldType = "";

        int FieldTypeId = 0;
        #region GetControlValue
        HtmlTable tbl = (HtmlTable)ctrl.FindControl("MetaDataTable");

        #region fill answers
        foreach (HtmlTableRow row in tbl.Rows)
        {
            if (row.Attributes["class"] == "trdynamic")
            {
                if (row.Style.Value == null || (row.Style.Value != null && !row.Style.Value.Contains("display:none")))//(row.Visible)
                {
                    Control parentctrl = row.Cells[1];
                    //string Question = row.Cells[0].InnerText;
                    InputValue = "";
                    foreach (var c in parentctrl.Controls.OfType<HtmlInputText>())
                    {
                        InputValue = string.IsNullOrEmpty(Request.Form[c.ID]) ? c.Value : Request.Form[c.ID].ToString();// c.Value;
                        ID = c.ID;
                        FieldTypeId = Convert.ToInt32(c.Attributes["FieldID"]);
                        FieldType = c.Attributes["FieldType"];

                        switch (FieldType.ToUpper())
                        {
                            case "TEXT":
                                dt.Rows.Add(FieldTypeId, InputValue);
                                break;
                            case "NUMBER":
                                dt.Rows.Add(FieldTypeId, InputValue);
                                break;
                            case "DATE":
                                if (string.IsNullOrEmpty(InputValue))
                                    dt.Rows.Add(FieldTypeId, InputValue);
                                else
                                    dt.Rows.Add(FieldTypeId, InputValue);
                                break;
                        }
                    }

                    foreach (var c in parentctrl.Controls.OfType<HtmlTextArea>())
                    {
                        InputValue = Request.Form[c.ID].ToString();//c.Value;
                        ID = c.ID;
                        FieldTypeId = Convert.ToInt32(c.Attributes["FieldID"]);
                        FieldType = c.Attributes["FieldType"];
                        dt.Rows.Add(FieldTypeId, InputValue);
                    }

                    foreach (var ddlList in parentctrl.Controls.OfType<HtmlSelect>())
                    {
                        ID = ddlList.ID;
                        FieldTypeId = Convert.ToInt32(ddlList.Attributes["FieldID"]);
                        Master = ddlList.Attributes["master"];
                        InputValue = "";
                        if (string.IsNullOrEmpty(Master))
                        {
                            foreach (ListItem objItem in ddlList.Items)
                            {
                                // if (objItem.Selected)
                                // {
                                InputValue = string.IsNullOrEmpty(Request.Form[ID]) ? ddlList.Value : Request.Form[ID].ToString();// c.Value;
                                //  InputValue += Request.Form[ID];
                                //  }
                            }
                            dt.Rows.Add(FieldTypeId, InputValue);
                        }
                    }
                    foreach (var hdnField in parentctrl.Controls.OfType<HtmlInputHidden>())
                    {
                        ID = hdnField.ID;
                        Master = hdnField.Attributes["master"];
                        InputValue = hdnField.Value;
                        if (!string.IsNullOrEmpty(Master))
                            dt.Rows.Add(FieldTypeId, InputValue);
                    }

                    foreach (var cbList in parentctrl.Controls.OfType<CheckBoxList>())
                    {
                        ID = cbList.ID;
                        FieldTypeId = Convert.ToInt32(cbList.Attributes["FieldID"]);

                        //for(Control a in cbList.Controls.OfType<HtmlInputCheckBox>())
                        //{
                        //}
                        // Control parentctrl1 = (cbList AS HtmlTable).Cells[1];

                        //foreach (HtmlTableRow row1 in cbList.Rows)
                        //{
                        //    if (row1.Attributes["fieldtype"] == "Checkbox")
                        //    {
                        //        if (row1.Style.Value == null || (row1.Style.Value != null && !row1.Style.Value.Contains("display:none")))//(row.Visible)
                        //        {
                        //            Control parentctrl1 = row1.Cells[1];
                        //          var chk=parentctrl1.Controls.OfType<HtmlInputCheckBox>();
                        //            //if(chk)
                        //        }
                        //    }
                        //}
                        InputValue = "";

                        foreach (ListItem objItem in cbList.Items)
                        {
                            if (objItem.Selected)
                            {
                                InputValue += objItem.Value + ",";
                            }
                        }

                        // CheckBoxList chkbx = (CheckBoxList)Form.FindControl(ID);
                        ////for (int j = 0; j < cbList.Items.Count; j++)
                        ////{
                        ////   var abc= (Request.Form[ID+"_"+j.ToString().to
                        ////}
                        dt.Rows.Add(FieldTypeId, InputValue.Trim(','));
                    }

                    foreach (var rbList in parentctrl.Controls.OfType<RadioButtonList>())
                    {
                        ID = rbList.ID;
                        FieldTypeId = Convert.ToInt32(rbList.Attributes["FieldID"]);

                        InputValue = rbList.SelectedValue;
                        dt.Rows.Add(FieldTypeId, InputValue);
                    }
                }
            }
        }
        return dt;
        #endregion
        #endregion GetControlValue
    }
    #endregion

    [WebMethod]
    public static string DeleteFile(string DocumentID, string DocumentPath)
    {
        IClauseLibrary ObjDocumentType;
        ObjDocumentType = FactoryClause.GetContractTypesDetails();
        ObjDocumentType.ContractRequestDocumentID = DocumentID;
        ObjDocumentType.IP = HttpContext.Current.Session[Declarations.IP] != null ? HttpContext.Current.Session[Declarations.IP].ToString() : null;
        ObjDocumentType.Addedby = HttpContext.Current.Session[Declarations.User] != null ? int.Parse(HttpContext.Current.Session[Declarations.User].ToString()) : 0;
        string IsDeleted = ObjDocumentType.DeleteDocumentFile();

        if (IsDeleted == "1")
        {
            /*Keyoti Uploaded Document Indexing Start.*/
            string strUrl = "";
            strUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + HttpContext.Current.Session["TenantDIR"] + DocumentPath.Replace("../Uploads", "/Uploads");
            AsyncIndexer.GetInstance().QueueForIndexing(strUrl, 2);
            /*Keyoti Uploaded Document Indexing End.*/

            DeleteFileFromPhysicalLocation.DeleteFile(DocumentPath);
        }


        return IsDeleted;
    }

    [WebMethod]
    public static string LoadClientList(string input)
    {
        string json = GetClient(input);
        XMLstring = json;
        return json;
    }

    protected string GetContactDetails(string RequestId)
    {
        IContractRequest objContractRequest;
        objContractRequest = FactoryWorkflow.GetContractRequestDetails();
        objContractRequest.RequestID = Convert.ToInt32(RequestId);
        string ClientAddressDetails = objContractRequest.ReadPrimaryAddress();
        return ClientAddressDetails;
    }

    [WebMethod]
    public static string LoadAddress(string ClientId)
    {
        IContractRequest objContractRequest;
        objContractRequest = FactoryWorkflow.GetContractRequestDetails();
        objContractRequest.ClientID = Convert.ToInt32(ClientId);
        string ClientAddressDetails = objContractRequest.ReadAddressDetails();
        return ClientAddressDetails;
    }

    [WebMethod]
    public static string LoadEmail(string ClientId)
    {
        IContractRequest objContractRequest;
        objContractRequest = FactoryWorkflow.GetContractRequestDetails();
        objContractRequest.ClientID = Convert.ToInt32(ClientId);
        string ClientAddressDetails = objContractRequest.ReadEmailDetails();
        return ClientAddressDetails;

    }

    [WebMethod]
    public static string LoadContractIDAddress(string ContractID)
    {
        IContractRequest objContractRequest;
        objContractRequest = FactoryWorkflow.GetContractRequestDetails();
        objContractRequest.ContractID = Convert.ToInt32(ContractID);
        string ClientAddressDetails = objContractRequest.ContractIDAddressDetails();
        return ClientAddressDetails;

    }

    [WebMethod]
    public static string LoadContacts(string ClientId)
    {
        IContractRequest objContractRequest;
        objContractRequest = FactoryWorkflow.GetContractRequestDetails();
        objContractRequest.ClientID = Convert.ToInt32(ClientId);
        string ClientAddressDetails = objContractRequest.ReadContactDetails();
        return ClientAddressDetails;

    }

    [WebMethod]
    public static string LoadOtherFields(string ClientName)
    {
        string ClientAddressDetails = "";
        if (ClientName.LastIndexOf("(") > -1)
        {

            string FinalString;
            int Pos1 = ClientName.LastIndexOf("(") + 1;
            int Pos2 = ClientName.LastIndexOf(")");
            FinalString = ClientName.Substring(Pos1, Pos2 - Pos1);

            IContractRequest objContractRequest;
            objContractRequest = FactoryWorkflow.GetContractRequestDetails();
            objContractRequest.ClientID = Convert.ToInt32(FinalString.Trim());
            ClientAddressDetails = objContractRequest.ReadOtherDetails();


        }
        return ClientAddressDetails;
    }

    [WebMethod]
    public static string ReadMetaDataFieldsDetails(string ClientName)
    {
        string MetaData = "";
        if (ClientName.LastIndexOf("(") > -1)
        {
            string FinalString;
            int Pos1 = ClientName.LastIndexOf("(") + 1;
            int Pos2 = ClientName.LastIndexOf(")");
            FinalString = ClientName.Substring(Pos1, Pos2 - Pos1);

            IContractRequest objContractRequest;
            objContractRequest = FactoryWorkflow.GetContractRequestDetails();
            objContractRequest.ContractID = Convert.ToInt32(FinalString.Trim());
            MetaData = objContractRequest.ReadMetaDataFieldsDetails();


        }
        return MetaData;
    }
    [WebMethod]
    public static string getfield(string ContractID)
    {
        IMetaDataConfigurators objMetaDataConfig = FactoryMedaData.GetContractTemplateDetail();
        HtmlTable container = objMetaDataConfig.GenerateRequestForm("Request Form", ContractID, "CONTRACTID");
        StringBuilder sb = new StringBuilder();
        int returnTableRow = container.Rows.Count;
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter tw = new HtmlTextWriter(sw))
            {
                container.RenderControl(tw);
            }
        }
        return sb.ToString();

    }

    protected void ddlassigndept_SelectedIndexChanged(object sender, EventArgs e)
    {

        objuser.DepartmentId = Convert.ToInt32(ddlassigndept.SelectedValue);
        objuser.ContractTypeId = Convert.ToInt32(ddlContracttype.SelectedValue);
        ddlassignto.extDataBind(objuser.SelectData());
        hdnSelectedDepartment.Value = ddlassigndept.SelectedValue;
        ddlassignto.Focus();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "abc", "JavascriptFunctionName();", true);


    }
    protected void ddlContracttype_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlassigndept.SelectedIndex != 0)
            {
                string val = ddlassignto.Value;
                objuser.DepartmentId = Convert.ToInt32(ddlassigndept.SelectedValue);
                objuser.ContractTypeId = Convert.ToInt32(ddlContracttype.SelectedValue);
                ddlassignto.extDataBind(objuser.SelectData());
                hdnSelectedDepartment.Value = ddlassigndept.SelectedValue;
                ddlassignto.Value = val;
                ddlassignto.Focus();
                ddlassignto.SelectedIndex = 0;
                ddlassigndept.SelectedIndex = 0;
                // ddlContracttype.Attributes.Remove("class");


            }
            ddlContracttype.CssClass = "chzn-select chzn-select AF";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "abcd", "JavascriptFunctionName();", true);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void setAccessValues()
    {
        if (Access.isCustomised(13) == true)
        {
            Button1.Visible = true;
        }
        else
        {
            Button1.Visible = false;
        }
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }

    public static string GetClient(string input)
    {
        DataView dv = new DataView(dt);
        ds.Clear();
        ds.Reset();

        dt.DefaultView.RowFilter = "ClientName like '%" + input.TrimEnd().Replace("'", "''") + "%'";
        ds.Tables.Add(dt.DefaultView.ToTable());
        return ds.GetXml();
    }

    public void PopulateList()
    {
        if (RequestType == "cnc" || RequestType == "nrr")
        {
            objcontractreq.RequestTypeID = 1;

        }
        else
        {
            objcontractreq.RequestTypeID = 0;

        }

        list = objcontractreq.ReadClientData();
        try
        {
            dt.Columns.Add("Address");
            dt.Columns.Add("ClientName");
        }
        catch (Exception ex)
        {
            ds.Clear();
            ds.Reset();

        }
        try
        {

            foreach (var listItem in Workflow_ContractRequest.list)
            {
                dt.Rows.Add(listItem.Address, listItem.ClientName);

            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objCountry = FactoryMaster.GetCountryDetail();
            objcontracttype = FactoryMaster.GetContractTypeDetail();
            objuser = FactoryUser.GetUsersDetail();
            objcontractreq = FactoryWorkflow.GetContractRequest2Details();
            ObjDocumentType = FactoryClause.GetContractTypesDetails();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    #region Populating  Dropdown
    void BindDropDowns()
    {
        Page.TraceWrite("Dropdown bind starts.");

        try
        {
            ddlCountry.extDataBind(objCountry.SelectData());
            if (hdnRequestId.Value == "")
            {
                objcontracttype.ContractTypeId = 0;
                objcontractreq.RequestID = 0;
            }
            else
            {
                objcontracttype.ContractTypeId = int.Parse(hdnRequestId.Value);
                objcontractreq.RequestID = int.Parse(hdnRequestId.Value);
            }
            objcontractreq.UserID = int.Parse(Session[Declarations.User].ToString());
            ddlContracttype.extDataBind(objcontractreq.SelectContractType());
            ddlassigndept.extDataBind(objcontractreq.SelectDeptData());



            ddlContractingParty.extDataBind(objcontractreq.SelectContractingPartyData());

            //ddlrequestername.extDataBind(objcontractreq.SelectOtherData());
            if (Session[Declarations.User] != null)
            {
                objuser.UserId = int.Parse(Session[Declarations.User].ToString());
                objuser.DepartmentId = 0;
                ddlrequestername.extDataBind(objuser.SelectSubordinateUsers());
                ddlrequestername.Value = Session[Declarations.User].ToString();
                ddlrequestername.Items.FindByValue(ddlrequestername.Value).Text = "Me";
            }


            ddlCountry.Items.FindByValue("0").Text = "--Select Country--";

            ddlassigndept.SelectedValue = Session[Declarations.Department].ToString();
            objuser.DepartmentId = Convert.ToInt32(ddlassigndept.SelectedValue);
            ddlassignto.extDataBind(objuser.SelectData());

            ddlOriginalCurrency.DataSource = FillMasterData("MstCurrency"); //table name
            ddlOriginalCurrency.DataTextField = "Name";
            ddlOriginalCurrency.DataValueField = "Id";
            ddlOriginalCurrency.DataBind();

            ddlPrority.DataSource = FillMasterData("mstPriority"); //table name
            ddlPrority.DataTextField = "Name";
            ddlPrority.DataValueField = "Id";
            ddlPrority.DataBind();


        }
        catch (Exception ex)
        {
            Page.TraceWarn("dropdown bind fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("dropdown bind ends.");
    }

    private List<SelectControlFields> FillMasterData(string TableName)
    {
        try
        {
            ICommonMaster obj = FactoryMaster.GetCommonMasterDetail();
            obj.TableName = TableName;
            List<SelectControlFields> myList = obj.SelectData();
            return myList;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    void ReadData()
    {
        Page.TraceWrite("ReadData starts.");
        try
        {
            objcontractreq.RequestID = int.Parse(hdnRequestId.Value);
            List<ContractRequest> ContractRequest = objcontractreq.ReadData();

            ddlContracttype.Enabled = ContractRequest[0].IsContractTypeEditable;           
            ddlContracttype.SelectedValue = ContractRequest[0].ContractTypeID.ToString();
            ddlContractingParty.Value = ContractRequest[0].ContractingpartyId.ToString();
            txtclientname.Text = ContractRequest[0].ClientName;
            hdnclientID.Value = ContractRequest[0].ClientID.ToString();
            txtclientaddress.Text = ContractRequest[0].Address;
            hdnPrimeIds.Value = ContractRequest[0].CountryID.ToString();
            txtEstimatedValue.Value = ContractRequest[0].EstimatedValue.ToString();
            txtPincode.Value = ContractRequest[0].Pincode;
            ddlOriginalCurrency.SelectedValue = ContractRequest[0].CurrencyID.ToString();
            //txtTotalValue.Value = ContractRequest[0].dTotalValue.ToString();
            ddlNotifyCustomer.Value = ContractRequest[0].IsCustomerPortalEnable;
            hdnNotification.Value = ContractRequest[0].IsNotified;
            ddlCountry.Value = ContractRequest[0].CountryID.ToString();

            if (ContractRequest[0].IsNotified == "Y")
            {
                txtEmailIds.Disabled = true;
                ddlNotifyCustomer.Disabled = true;
            }
            if (ContractRequest[0].ContractID == 0)
            {
                txtContractID.Value = "";
            }
            else
            {
                txtContractID.Value = ContractRequest[0].ContractID.ToString();
            }
            if (ContractRequest[0].isCustomer.ToString() == "Y")
            {
                rdoCustomer.Checked = true;
            }
            if (ContractRequest[0].isCustomer.ToString() == "N")
            {
                rdoSupplier.Checked = true;
            }
            if (ContractRequest[0].isCustomer.ToString() == "O")
            {
                rdoOthers.Checked = true;
            }
            if (ContractRequest[0].ContractingpartyId.ToString() == "0")
            {
                //CustomerOrSupplierId.Style.Add("display", "block");
            }
            else
            {
                //CustomerOrSupplierId.Style.Add("display", "block");
            }
            ddlrequestername.Value = ContractRequest[0].RequestUserID.ToString();
            txtcontractdescription.Value = ContractRequest[0].ContractDescription;
            hdndate.Value = ContractRequest[0].DeadlineDate.ToString();
            ddlassigndept.SelectedValue = ContractRequest[0].DepartmentId.ToString();
            hdnDeptId.Value = ContractRequest[0].DepartmentId.ToString();
            objuser.DepartmentId = Convert.ToInt32(ddlassigndept.SelectedValue);
            ddlassignto.extDataBind(objuser.SelectData());

            ddlassignto.extSelectedValues(ContractRequest[0].AssignToId.ToString(), ContractRequest[0].AssignerUserName.ToString());
            hdnAssinedToUser.Value = ContractRequest[0].AssignToId.ToString();
            txtdeadlinedate.Value = ContractRequest[0].DeadlineDate.ToString();
            txtContractTerm.Value = ContractRequest[0].ContractTerm.ToString();
            txtContractValue.Value = Convert.ToString(ContractRequest[0].ContractValue);
            objcontractreq.RequestID = Convert.ToInt32(objcontractreq.RequestID);

           
            switch (ContractRequest[0].Priority.ToString())
            {
                case "Y/N":
                    ddlPrority.Value = "0";
                    break;
                case "Y":
                    ddlPrority.Value = "1";
                    break;
                case "N":
                    ddlPrority.Value = "2";
                    break;
                case "Yes":
                    ddlPrority.Value = "1";
                    break;
                case "No":
                    ddlPrority.Value = "2";
                    break;
            }
            if (ddlPrority.Value == "1")
            {
                lblPriorityreason.Visible = true;
                txtpriorityremark.Value = ContractRequest[0].PriorityReason.ToString();
            }
            else
            {
                lblPriorityreason.Visible = false;
                txtpriorityremark.Value = "";
            }

            hdnSelectedUser.Value = ContractRequest[0].AssignToId.ToString();
            hdnSelectedDepartment.Value = ContractRequest[0].DepartmentId.ToString();

            DataSet ds = objcontractreq.ReadContractDetails();
            DataTable dtRequestDocDetails = ds.Tables[1];

            rptRequestDocuments.DataSource = dtRequestDocDetails;
            rptRequestDocuments.DataBind();


            string IsApprovalR = ContractRequest[0].isApprovalRequried.ToString();
            int RequestTypeID = ContractRequest[0].RequestTypeID;
            if (RequestTypeID != 1)
            {
                ApprovalP.Style.Add("display", "none");
                ContractIDPara.Style.Add("display", "");
                txtContractID.Attributes.Add("class", "required mobile");
            }
            else
            {
                txtContractID.Attributes.Add("class", "mobile");
            }
            SetEditLabel(RequestTypeID);

            if (RequestTypeID == 5)
            {
                EV.Style.Add("display", "");
            }

            if (IsApprovalR != "")
            {
                if (IsApprovalR == "Y")
                {
                    rdoApprovalRequiredYes.Checked = true;
                }
                else
                {
                    rdoApprovalRequiredNo.Checked = true;
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "tytyttrrrr", "SetWaterMark();", true);
            }

            DisableControls();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData ends.");
    }

    protected void DisableControls()
    {
        if (lblTitle.Text.IndexOf("Revise Existing Contract") > -1 || lblTitle.Text.IndexOf("Renew Existing Contract") > -1 || lblTitle.Text.IndexOf("Auto Renewal") > -1 || lblTitle.Text.IndexOf("Terminate Contract") > -1)
        {
            ddlContracttype.Enabled = true;
        }
    }

    protected void SetEditLabel(int RequestID)
    {
        lblTitle.Text = "Edit Contract Request";

        if (autoRenewalCheck == "1")
        {
            lblTitle.Text = "Auto renewal";
        }
        else if (RequestID == 3)
        {
            lblTitle.Text = lblTitle.Text + " ( Renew Existing Contract )";
        }

        else if (RequestID == 1)
        {

            lblTitle.Text = lblTitle.Text + " ( New contract )";
        }

        else if (RequestID == 2)
        {
            lblTitle.Text = lblTitle.Text + " ( Revise Existing Contract )";
        }

        else if (RequestID == 5)
        {
            lblTitle.Text = lblTitle.Text + " ( New Review Request (Client Contract) )";
            ApprovalP.Style.Add("display", "none");
            ContractIDPara.Style.Add("display", "none");
            txtContractID.Attributes.Add("class", "mobile");
            EV.Style.Add("display", "block");
        }
        else if (RequestID == 6)
        {
            lblTitle.Text = lblTitle.Text + " ( Terminate Contract )";
        }
        else if (RequestID == 7)
        {
            lblTitle.Text = lblTitle.Text + " ( Delete Contract )";
        }
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        Button clickedButton = (Button)sender;
        string ButtonText = clickedButton.Text;
        Page.TraceWrite("Save Update button clicked event starts.");
        lblProcessingLink.Visible = true;
        btnsubmit.Visible = false;
        Button1.Visible = false;
        btnBack.Visible = false;
        InsertUpdate(ButtonText, 0);

        Page.TraceWrite("Save Update button clicked event ends.");
    }

    void InsertUpdate(string ButtonText, int flg = 0)
    {
        Page.TraceWrite("Insert, Update starts.");
        string status = "";
        string val = "0";
        objcontractreq.ContractTypeID = int.Parse(ddlContracttype.SelectedValue);
        if (hdnclientID.Value != "")
            objcontractreq.ClientID = int.Parse(hdnclientID.Value);
        else
            objcontractreq.ClientID = 0;

        objcontractreq.ClientName = txtclientname.Text;
        //if (txtTotalValue.Value != "")
        //    objcontractreq.dTotalValue = Convert.ToDecimal(txtTotalValue.Value);
        objcontractreq.CurrencyID = Convert.ToInt32(ddlOriginalCurrency.SelectedValue);

        if (!string.IsNullOrEmpty(Convert.ToString(hdnPrimeIds.Value)))
            objcontractreq.CountryID = int.Parse(hdnPrimeIds.Value);
        //if (txtclientaddress.Text != "")
        //{
            objcontractreq.Address = txtclientaddress.Text;
       // }
      //  else { objcontractreq.Address = null; }
        if (hdnIsNewAddress.Value != "N")
        {
            
            objcontractreq.Street1 = txtclientaddressStreet2.Text;
            //if (!string.IsNullOrEmpty(Convert.ToString(hdnPrimeIds.Value)))
            //    objcontractreq.CountryID = int.Parse(hdnPrimeIds.Value);
            objcontractreq.StateName = ddlState.Value;
            objcontractreq.CityName = ddlcity.Value;
            objcontractreq.Pincode = txtPincode.Value;
        }
        objcontractreq.RequestUserID = int.Parse(ddlrequestername.Value);
        objcontractreq.ContractDescription = txtcontractdescription.Value;
        if (txtdeadlinedate.Value != "")
        {
            objcontractreq.DeadlineDate = Convert.ToString(txtdeadlinedate.Value);
        }

        objcontractreq.AssignToId = int.Parse(hdnSelectedUser.Value);
        objcontractreq.DepartmentId = int.Parse(ddlassigndept.SelectedValue);
        objcontractreq.RequestType = RequestType;
        objcontractreq.ContactNumber = txtContactNumbers.Value;
        objcontractreq.EmailID = txtEmailIds.Value;
        objcontractreq.EstimatedValue = txtEstimatedValue.Value;
        if (txtContractID.Value != "")
        {
            objcontractreq.ContractID = Convert.ToInt32(txtContractID.Value);
        }
        if (hdnIsNewAddress.Value == "N")
        {
            string[] isPrimary = new string[2];
            isPrimary = hdnAdddressFromList.Value.Split('#');
            objcontractreq.AddressID = Convert.ToInt32(isPrimary[0]);
        }
        else
        {
            objcontractreq.CountryID =Convert.ToInt32(ddlCountry.Value);
        }

        if (hdnisNewContact.Value == "N")
        {
            string[] isPrimaryContact = new string[2];
            isPrimaryContact = hdnContactNumberFromList.Value.Split('#');
            objcontractreq.ContactDetailID = Convert.ToInt32(isPrimaryContact[0]);
        }
        if (hdnisNewEmail.Value == "N")
        {
            string[] isPrimaryEMail = new string[2];
            isPrimaryEMail = hdnEmailIDFromList.Value.Split('#');
            objcontractreq.EmailContactID = Convert.ToInt32(isPrimaryEMail[0]);
        }
        objcontractreq.isNewAddress = hdnIsNewAddress.Value;
        objcontractreq.isNewContactNumber = hdnisNewContact.Value;
        objcontractreq.isNewEmailID = hdnisNewEmail.Value;
        objcontractreq.DocumentIDs = Convert.ToString(hdnDocumentIDs.Value);
        if (rdoApprovalRequiredYes.Checked == true)
        {
            objcontractreq.isApprovalRequried = "Y";
        }
        else
        {
            objcontractreq.isApprovalRequried = "N";
        }
        objcontractreq.ContractTerm = txtContractTerm.Value;
        if (txtContractValue.Value.Trim() != "")
        {
            objcontractreq.ContractValue = Convert.ToDecimal(txtContractValue.Value);
        }
        objcontractreq.ContractingpartyId = Convert.ToInt32(ddlContractingParty.Value);


        //objcontractreq.isCustomer = rdoCustomer.Checked == true ? "Y" : "N";
        if (rdoCustomer.Checked == true)
            objcontractreq.isCustomer = "Y";
        if (rdoSupplier.Checked == true)
            objcontractreq.isCustomer = "N";
        if (rdoOthers.Checked == true)
            objcontractreq.isCustomer = "O";

        objcontractreq.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objcontractreq.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objcontractreq.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
        if (SendNotification == "N")
            objcontractreq.IsCustomerPortalEnable = "N";
        else
            objcontractreq.IsCustomerPortalEnable = ddlNotifyCustomer.Value;

        //Added by dk
        if (ddlPrority.Value == "1")
        {
            objcontractreq.PriorityReason = txtpriorityremark.Value;
        }
        else { objcontractreq.PriorityReason = ""; }
        switch (ddlPrority.Value)
        {
            case "0":
                objcontractreq.Priority = "Y/N";
                break;
            case "1":
                objcontractreq.Priority = "Y";
                break;
            case "2":
                objcontractreq.Priority = "N";
                break;
        }

        DataTable dt = SaveData(pnlnewadd);
        objcontractreq.dtMatadataFieldValues = dt;
        
        try
        {
            if (hdnclientID.Value != "")
            {
                if (hdnRequestId.Value == "" || hdnRequestId.Value == "0" || hdnRequestId.Value == null || autoRenewalCheck == "1")
                {
                    objcontractreq.RequestID = 0;                    
                    status = objcontractreq.InsertRecord();                    
                    if (status != "-1" && status != "0")
                    {
                        SendEmail();
                    }
                }
                else
                {
                    objcontractreq.RequestID = int.Parse(hdnRequestId.Value);
                    status = objcontractreq.UpdateRecord();                    
                    SendEmail();
                }
            }
            else
            {
                status = objcontractreq.InsertRecord();
                if (status != "-1" && status != "0")
                {
                    SendEmail();
                }
            }
            if (objcontractreq is IContractRequest2)
            {
                if (hdnRequestId.Value == "" || hdnRequestId.Value == "0" || hdnRequestId.Value == null)
                    objcontractreq.RequestID = int.Parse(status);
                else
                    objcontractreq.RequestID = int.Parse(hdnRequestId.Value);
                ((IContractRequest2)objcontractreq).BuildCacheString();
            }
            Page.Message(status, val);
            msgEdit.InnerHtml = "";

            if (hdnDocumentName.Value != "")
            {
                string strUrl = "";
                string ContractInfo = "";

                IActivity objActivity;
                objActivity = FactoryWorkflow.GetActivityDetails();
                objActivity.RequestId = Convert.ToInt32(status);
                ContractInfo = objActivity.ContractDetailsByRequestId();

                string[] fileNames = hdnDocumentName.Value.Split(',');

                foreach (var fileName in fileNames)
                {
                    if (fileName != "")
                    {
                        strUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + HttpContext.Current.Request.Url.Segments[1] + fileName.Replace(" ", "").Replace("../Uploads", "Uploads");
                        AsyncIndexer.GetInstance().QueueForIndexing(strUrl, 1, ContractInfo);
                    }
                }
            }
        }

        catch (Exception ex)
        {
            Page.Message("0", val);
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Insert, Update ends.");
        if (flg == 0 && status != "-1" && status != "0")
        {
            Session[Declarations.Message] = (val == "0") ? "0" : "1";
            if (ButtonText != "Save & continue")
            {
                if (Convert.ToString(ViewState["RedirectedFrom"]) == "snapshot")
                // Server.Transfer("RequestFlow.aspx?Status=Workflow&RequestId=" + status);
                {
                    if (ViewState["ParentRequestID"] != null)
                    {
                        Server.Transfer("RequestFlow.aspx?Status=Workflow&RequestId=" + hdnRequestId.Value + "&ParentRequestID=" + ViewState["ParentRequestID"].ToString());
                    }
                    else
                        Server.Transfer("RequestFlow.aspx?Status=Workflow&RequestId=" + hdnRequestId.Value);
                }
                else
                    Server.Transfer("ContractRequestData.aspx");
            }
            else
            {
                Server.Transfer("RequestFlow.aspx?Status=Workflow&RequestId=" + status);
            }
        }
        ClearData(status);
        BindDropDowns();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "tytyttr", "JQSelectBind('ddlassigndept','ddlassignto', 'vwUsersWithDepartMent');", true);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "tytyttrrr", "SetAssingdToValue('" + hdnUserID.Value + "');", true);
        hdnAssinedToUser.Value = hdnUserID.Value;
    }

    void SendEmail()
    {
        try
        {
            Page.TraceWrite("send emails.");
            EmailTemplateBLL.IEmailTrigger objEmail = EmailTemplateBLL.FactoryEmailTemplate.GetEmailTriggerDetail();
            objEmail.RequestId = objcontractreq.RequestID;
            objEmail.ContractTypeId = objcontractreq.ContractTypeID;
            objEmail.ContractTemplateId = 0;
            objEmail.EmailTriggerId = 1;
            objEmail.userId = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            objEmail.AssignedToUserId = objcontractreq.AssignToId;
            objEmail.BulkEmail();
            objcontractreq.RequestID = 0;
        }
        catch (Exception)
        {
            Page.TraceWarn("send emails fails.");
        }
    }


    protected void btnBack_Click(object sender, EventArgs e)
    {
        msgEdit.InnerHtml = "";
        if (Convert.ToString(ViewState["RedirectedFrom"]) == "snapshot")
        // Server.Transfer("RequestFlow.aspx?Status=Workflow&RequestId=" + hdnRequestId.Value);
        {
            if (ViewState["ParentRequestID"] != null)
            {
                Server.Transfer("RequestFlow.aspx?Status=Workflow&RequestId=" + hdnRequestId.Value + "&ParentRequestID=" + ViewState["ParentRequestID"].ToString());
            }
            else
                Server.Transfer("RequestFlow.aspx?Status=Workflow&RequestId=" + hdnRequestId.Value);
        }
        else
            Server.Transfer("ContractRequestData.aspx");
    }

    void ClearData(string status)
    {
        if (status == "1")
        {
            Page.TraceWrite("clearData starts.");
            Text1.Value = "";
            ddlCountry.SelectedIndex = 0;
            ddlContracttype.SelectedIndex = 0;
            ddlassigndept.SelectedIndex = 0;
            txtclientname.Text = "";
            txtclientaddress.Text = "";
            txtPincode.Value = "";
            txtcontractdescription.Value = "";
            txtdeadlinedate.Value = "";
            hdnPrimeIds.Value = "";
            hdnRequestId.Value = "";
            hdnStateIds.Value = "";
            hdnCityIds.Value = "";
            hdnclientID.Value = "";
            hdnStateIdRead.Value = "";
            hdnCityIdRead.Value = "";
            hdndate.Value = "";
            txtContactNumbers.Value = "";
            txtEmailIds.Value = "";
            txtclientaddress.Text = "";
            txtContractID.Value = "";
            rdoApprovalRequiredNo.Checked = false;
            rdoApprovalRequiredYes.Checked = true;
            txtContractValue.Value = "";
            txtContractTerm.Value = "";
            rdoCustomer.Checked = true;
            rdoSupplier.Checked = false;
            rdoOthers.Checked = false;
            ddlContractingParty.SelectedIndex = 0;
            CustomerOrSupplierId.Visible = false;
            txtclientaddressStreet2.Text = "";
            Page.TraceWrite("clear Data ends.");
        }
    }

    void NotificationVisibility()
    {
        Access.PageAccess("CustomerPortal", Session[Declarations.User].ToString());

        if (Access.View != "Y")
        {
            trNotification.Style.Add("display", "none");
            SendNotification = "N";

            // ddlNotifyCustomer.Items.FindByText("No").Selected = true;
        }
        else
            trNotification.Style.Add("display", "");
    }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (Add == "N" && (hdnRequestId.Value == "" || hdnRequestId.Value == "0" || hdnRequestId.Value == null))
        {
            Page.extDisableControls();
        }
        if (View == "N")
        {
            btnBack.Enabled = false;
            btnsubmit.Enabled = false;
            btnBack.Visible = false;
            btnsubmit.Visible = false;
        }
        if (Update == "N")
        {
            btnsubmit.Visible = false;
        }
        Page.TraceWrite("AccessVisibility starts.");
    }

    [WebMethod]
    public static string ValidateContractID(string ContractID)
    {
        try
        {
            string Counter = string.Empty;
            if (ContractID != "")
            {
                IContractRequest ObjCR = FactoryWorkflow.GetContractRequestDetails();
                ObjCR.ContractID = Convert.ToInt32(ContractID);
                Counter = ObjCR.ValidateContract();
            }
            return Counter;
        }
        catch (Exception)
        {
            throw;
        }
    }

    [WebMethod]
    public static string LoadIsCustomer(string ClientId)
    {
        IContractRequest objContractRequest;
        objContractRequest = FactoryWorkflow.GetContractRequestDetails();
        objContractRequest.ClientID = Convert.ToInt32(ClientId);
        string IsCustomer = objContractRequest.ReadIsCustomerDetails();
        return IsCustomer;
    }
}