﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using WorkflowBLL;

public partial class WorkFlow_ContractRelationAdd : System.Web.UI.Page
{
    IContractRelations objContractRelations;
    public string Add;
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        Session["MasterSeelcted"] = null;
        Session["ChlidSeelcted"] = null;
//        System.Threading.Thread.Sleep(0);
        if (Session[Declarations.User] != null && !IsPostBack)
        {

            Access.PageAccess(this, Session[Declarations.User].ToString(), "WorkFlow_");
            ViewState[Declarations.Add] = Access.Add.Trim();
            ViewState[Declarations.View] = Access.View.Trim();
            ViewState[Declarations.Delete] = Access.Delete.Trim();
            ViewState[Declarations.Update] = Access.Update.Trim();
        }

        if (!IsPostBack)
        {
            if (String.IsNullOrEmpty(Request.extValue("hdnContractId")) != true)
            {
                hdnContractId.Value = Request.extValue("hdnContractId");
            }
            else
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ContractId"]))
                    hdnContractId.Value = Request.QueryString["ContractId"];
                else
                    hdnContractId.Value = "0";
            }
            ViewState["RequestId"] = Request.QueryString["RequestId"];
            hdnRelationshipId.Value = "0";
            ReadData();
        }
        AccessVisibility();
    }

    void CreateObjects()
    {
        Page.TraceWrite("ContractRelations: Page object create starts.");
        try
        {
            objContractRelations = FactoryWorkflow.GetContractRelations();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ContractRelations: create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ContractRelations: Page object create ends.");
    }

    void ReadData()
    {
        Page.TraceWrite("ReadData starts.");
        try
        {
            objContractRelations.ContractID = int.Parse(hdnContractId.Value);
            objContractRelations.Flag = 0;
            ddlContracts.extDataBind(objContractRelations.ContractRelationsAll());
            ddlContractParentOf.extDataBind(objContractRelations.ParentOf());
            ddlContractChildOf.extDataBind(objContractRelations.ChildOf());

            List<ContractRelations> ContractRelationsDetails = objContractRelations.ReadData();
            ddlContracts.Value = hdnContractId.Value;
            if (ContractRelationsDetails != null && ContractRelationsDetails.Count > 0)
            {
                objContractRelations.Flag = 1;
                ddlContractParentOf.extDataBind(objContractRelations.ParentOf());
                ddlContractChildOf.extDataBind(objContractRelations.ChildOf());
                ddlContractChildOf.Value = ContractRelationsDetails[0].ChildOfContractId.ToString();
                ddlContractParentOf.Value = ContractRelationsDetails[0].ParentOfContractId.ToString();
                ddlContractParentOf.Disabled = ContractRelationsDetails[0].IsParentOfDisabled;
                if (ContractRelationsDetails[0].IsParentOfDisabled == true)
                    ddlContractParentOf.Value = "1";

                if (ContractRelationsDetails.Count == 1)
                {
                    hdnRelationshipId.Value = ContractRelationsDetails[0].ContractRelationsId.ToString();
                }
                else
                {
                    hdnRelationshipId.Value = ContractRelationsDetails[1].ContractRelationsId.ToString();
                }
                txtDescription.Value = ContractRelationsDetails[0].RelationsDescription.ToString();
                hdnIsUsed.Value = ContractRelationsDetails[0].IsUsed;
            }
            if (!string.IsNullOrEmpty(hdnRelationshipId.Value) && hdnRelationshipId.Value!="0")
            {
                lblTitle.Text = "Edit Contract Relation for " + "Contract #" + hdnContractId.Value;
                btnSave.Text = "Update";
            }
            else
            {
                lblTitle.Text = "Add Contract Relation for " + "Contract #" + hdnContractId.Value;
            }
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData ends.");
    }

    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }

    void AccessVisibility()
    {
        setAccessValues();

        Page.TraceWrite("AccessVisibility starts.");
         if ( Add == "N" && (hdnContractId.Value == "" || hdnContractId.Value == "0" || hdnContractId.Value == null))
        {
            Page.extDisableControls();
            btnBack.Enabled = true;
            btnSave.Visible = false;
        }
        if (View == "N")
        {
            Page.extDisableControls();
            btnBack.Enabled = true;
        }
        if (Delete == "N")
        {
            btnDelete.Enabled = false;
            btnDelete.OnClientClick = null;
            btnDelete.Visible = false;
        }
        else if (Delete == "Y")
        {
            btnDelete.Enabled = true;
        }

        if (Add == "N" && Update == "N")
        {
            //Page.extDisableControls();
            if (!string.IsNullOrEmpty(hdnRelationshipId.Value))
            {
                btnSave.Visible = false;
            }
        }
        else
        {
            btnSave.Visible = true; ;
        }

        Page.TraceWrite("AccessVisibility starts.");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("ContractRelations: Save Update button clicked event starts.");
        InsertUpdate();
        Page.TraceWrite("Save Update button clicked event ends.");

    }

    protected void InsertUpdate()
    {
        string status = "";
        try
        {
            objContractRelations.ContractID = int.Parse(hdnContractId.Value);
            objContractRelations.ParentOfContractId = int.Parse(ddlContractParentOf.Value);
            objContractRelations.ChildOfContractId = int.Parse(ddlContractChildOf.Value);
            objContractRelations.RelationsDescription = txtDescription.Value;

            objContractRelations.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            objContractRelations.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            objContractRelations.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;

            if (hdnRelationshipId.Value == "0")
            {
                objContractRelations.ContractRelationsId = 0;
                status = objContractRelations.InsertRecord();
            }
            else
            {
                objContractRelations.ContractRelationsId = int.Parse(hdnRelationshipId.Value);
                status = objContractRelations.UpdateRecord();
            }
            Page.Message(status, hdnRelationshipId.Value);
        }
        catch (Exception ex)
        {
            Page.Message("0", hdnRelationshipId.Value);
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Insert, Update ends.");
        if (status == "1")
        {
            Session[Declarations.Message] = (hdnRelationshipId.Value == "0") ? "0" : "1";
            Response.Redirect("ContractRelations.aspx?RequestId=" + ViewState["RequestId"] + "&ContractId=" + hdnContractId.Value);
        }
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Delete starts.");
        objContractRelations.ContractID = int.Parse(hdnContractId.Value);
        objContractRelations.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objContractRelations.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
        //objContractRelations.ContractRelationsIds = int.Parse(hdnContractId.Value);
        string status = objContractRelations.DeleteRecord();
        Page.TraceWrite("Delete End.");
        if (status != "")
        {
            //Session[Declarations.Message] = (hdnRelationshipId.Value == "0") ? "0" : "1";
            Page.JavaScriptClientScriptBlock("status", "PageGetMessage('DS')");
            Response.Redirect("ContractRelations.aspx?RequestId=" + ViewState["RequestId"] + "&ContractId=" + hdnContractId.Value + "&IsDelete=DS");
        }
        Page.TraceWrite("Delete End.");
    }

    protected void Back_Click(object sender, EventArgs e)
    {
        //Server.Transfer("ContractRelations.aspx");
        Response.Redirect("ContractRelations.aspx?RequestId=" + ViewState["RequestId"] + "&ContractId=" + hdnContractId.Value);
    }

    //private void setAccessValues()
    //{
    //    if (ViewState[Declarations.Add] != null)
    //    {
    //        Add = ViewState[Declarations.Add].ToString();
    //    }
    //    if (ViewState[Declarations.Update] != null)
    //    {
    //        Update = ViewState[Declarations.Update].ToString();
    //    }
    //    if (ViewState[Declarations.Delete] != null)
    //    {
    //        Delete = ViewState[Declarations.Delete].ToString();
    //    }
    //    if (ViewState[Declarations.View] != null)
    //    {
    //        View = ViewState[Declarations.View].ToString();
    //    }

    //}

    //void AccessVisibility()
    //{

    //    Page.TraceWrite("AccessVisibility starts.");
    //    if (View == "N")
    //    {
    //        Page.extDisableControls();
    //    }
    //    else
    //    {
    //        //btnSearch.Enabled = true;
    //        //txtSearch.Disabled = false;
    //    }

    //    if (Update == "N")
    //    {
    //        Page.extDisableControls();
    //        //btnAddRecord.Enabled = false;
    //        //btnAddRecord.Visible = false;

    //    }
    //    if (Add == "N")
    //    {
    //        Page.extDisableControls();
    //        //btnAddRecord.Enabled = false;
    //        //btnAddRecord.Visible = false;
    //    }
    //    else
    //    {
    //        //btnAddRecord.Enabled = true;
    //    }

    //    if (Delete == "N")
    //    {
    //        btnDelete.Enabled = false;
    //        btnDelete.OnClientClick = null;
    //        btnDelete.Visible = false;
    //    }
    //    else if (Delete == "Y")
    //    {
    //        btnDelete.Enabled = true;
    //    }

    //    if (View == "Y")
    //    {
    //        //txtSearch.Disabled = false;
    //        //btnSearch.Enabled = true;
    //        //btnShowAll.Enabled = true;
    //    }


    //    Page.TraceWrite("AccessVisibility starts.");
    //}

}