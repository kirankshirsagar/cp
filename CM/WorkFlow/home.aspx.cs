﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UserManagementBLL;
using CommonBLL;
using System.Text.RegularExpressions;


public partial class Workflow_home : System.Web.UI.Page
{
    IAccessLink obj = FactoryUser.GetAccessDetail();
    //IAccessHome obj;
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["MasterSeelcted"] = null;
        Session["ChlidSeelcted"] = null;

        if (!IsPostBack)
        {
            if (Session["ReturnUrl"] != null)
            {
                string url = Convert.ToString(Session[Declarations.URL]);
                string DIR = Convert.ToString(Session["TenantDIR"]);
                string ReturnUrl = Regex.Replace(Convert.ToString(Session["ReturnUrl"]), "/" + DIR, "", RegexOptions.IgnoreCase);
                Session["ReturnUrl"] = null;
                Response.Redirect(url + ReturnUrl);
            }
            if (Session[Declarations.User] != null)
            {
                obj.UserId = int.Parse(Session[Declarations.User].ToString());
                obj.Flag = "Reports";
                ddlReplist.extDataBind(obj.SelectData(), "");
            }

        }
    }

    /*
    void CreateObjects()
    {
        obj = FactoryUser.GetAccessHomeDetail();
    }

    void ReadData()
    {
        obj.UserId = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;

        if (obj.UserId > 0)
        {
            List<AccessHome> lst = obj.ReadData();
            hdnmyAutoRenewalTable.Value = lst[0].AutoRenewal;
            hdnmyCalenderTable.Value = lst[0].Calender;
            hdnmyClientsTable.Value = lst[0].MyClients;
            hdnmyContractsTable.Value = lst[0].MyContracts;
            hdnmyCreateNewContractTable.Value = lst[0].CreateNewContract;
            hdnmyCreateNewReviewRequestTable.Value = lst[0].NewReviewRequest;
            hdnmyDashboardTable.Value = lst[0].MyDashBoard;
            hdnmyRenewContractTable.Value = lst[0].RenewContract;
            hdnmyReportsTable.Value = lst[0].MyReports;
            hdnmyReviseContractTable.Value = lst[0].ReviseContract;
            hdnmyTerminateContractTable.Value = lst[0].TerminateContract;
            hdnmyVaultTable.Value = lst[0].MyVault;
            hdnmyCalenderTable.Value = lst[0].Calender;
        }
    }
    */

    #region Added by Dilip
    protected void btnReportAction_Click(object sender, EventArgs e)
    {
        string Reportlink = string.Empty;
        string Path = string.Empty;
        foreach (ListItem item in ddlReplist.Items)
        {
            Reportlink = ddlReplist.Items[1].Value;
            break;
        }
        Path = "../Reports/" + Reportlink;
        Response.Redirect(Path,true);
    }

    #endregion

}