﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WorkflowBLL;
using CommonBLL;



public partial class WorkFlow_GlobalSearch : System.Web.UI.Page
{
    // navigation//
    public static int RecordsPerPage = 2000;
    public static int VisibleButtonNumbers =0;
    // navigation//

    IGlobalSearch objGlobal;
    public string Add;
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        //if (Session[Declarations.User] != null && !IsPostBack)
        //{
        //    Access.PageAccess(this, Session[Declarations.User].ToString(), "workflow_");
        //    ViewState[Declarations.Add] = Access.Add.Trim();
        //    ViewState[Declarations.View] = Access.View.Trim();
        //    ViewState[Declarations.Delete] = Access.Delete.Trim();
        //    ViewState[Declarations.Update] = Access.Update.Trim();
        //}

        if (!IsPostBack || hdnSearch.Value.Trim() != string.Empty)
        {


           hdnIsDetailAccess.Value = Access.isCustomised(12).ToString();
         
            hdnDomainURL.Value = Strings.domainURL();
            ViewState["GlobalSearch"] = Request.QueryString["GlobalSearch"];
            if (Session[Declarations.User] != null)
            {
              
                ReadData(1, RecordsPerPage);
            }
          Message();
        }
        else
        {
            //SetNavigationButtonParameters();
        }
    }



    void Message()
    {
        if (Session[Declarations.Message] != null)
        {
            var flg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }

    }

    // navigation//

    //#region Navigation
    //void SetNavigationButtonParameters()
    //{
    //    PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
    //    PaginationButtons1.RecordsPerPage = RecordsPerPage;
    //    PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    //}


    //protected void Page_LoadComplete(object sender, EventArgs e)
    //{
    //    if (PaginationButtons1.PageNumber > 0)
    //    {
    //        ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
    //    }
    //}


    //#endregion

    // navigation//


    void ReadData(int pageNo, int recordsPerPage)
    {
        if (Request.QueryString["GlobalSearch"] != null)
        {
            Page.TraceWrite("ReadData starts.");
            try
            {
                objGlobal.PageNo = pageNo;
                objGlobal.RecordsPerPage = recordsPerPage;
                objGlobal.Search = ViewState["GlobalSearch"].ToString();
                lblTitle.Text = "searched... " +"'"+ objGlobal.Search+"'";
                objGlobal.UserId = int.Parse(Session[Declarations.User].ToString());
                rptGlobalSearch.DataSource = objGlobal.ReadData();
                rptGlobalSearch.DataBind();
                Page.TraceWrite("ReadData ends.");
                ViewState[Declarations.TotalRecord] = objGlobal.TotalRecords;
               // PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
              //  PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
              //  PaginationButtons1.RecordsPerPage = RecordsPerPage;
                lblTotalRecords.Text = ViewState[Declarations.TotalRecord] != null ? "Results (" + ViewState[Declarations.TotalRecord].ToString() + ")" : "";
            }
            catch (Exception ex)
            {
                Page.TraceWarn("ReadData fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objGlobal = FactoryWorkflow.GetGlobalSearchDetails();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }



    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }
    }


}










