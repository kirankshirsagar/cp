﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;
using WorkflowBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;
using System.Web.Security;
using System.IO;
using System.Web.UI.HtmlControls;

public partial class WorkFlow_ClientAdd : System.Web.UI.Page
{
    ICountry objCountry;
    IClient objclient;
    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        if (!IsPostBack)
        {
            BindDropDowns();
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objCountry = FactoryMaster.GetCountryDetail();
            objclient = FactoryWorkflow.GetClientDetails();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }
    //Populating  Dropdown
    #region
    void BindDropDowns()
    {
        Page.TraceWrite("dropdown bind starts.");
        try
        {
            ddlCountry.extDataBind(objCountry.SelectData());
        }
        catch (Exception ex)
        {
            Page.TraceWarn("dropdown bind fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("dropdown bind ends.");
    }
    #endregion

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        InsertUpdate(0);
        Page.TraceWrite("Save Update button clicked event ends.");

        //string Client = txtclientname.Value.ToString().Trim();  
        //HtmlGenericControl all = new HtmlGenericControl();
        //all.InnerHtml = "<script language='javascript'>PassParametersToPatentPage('" + Client + "'); </script>";
        //Page.Controls.Add(all);
    }

    void InsertUpdate(int flg = 0)
    {
        string s = "";
        Page.TraceWrite("Insert, Update starts.");
        string status = "";
        objclient.ClientName = txtclientname.Value;
        objclient.Address = txtclientaddress.Value;
        objclient.CountryID = int.Parse(hdnPrimeIds.Value);
        objclient.StateID = int.Parse(hdnStateIds.Value);
        objclient.CityID = int.Parse(hdnCityIds.Value);
        objclient.Pincode = txtPincode.Value;
        objclient.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objclient.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objclient.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
        string val = "0";
        try
        {
            status = objclient.InsertRecord();

            if (status != "" && status != null)
            {
                s = "1";
                objclient.ClientID = int.Parse(status);
            }
            else
                s = "2";
            Page.Message(s, val);
            // Page.Message(status, hdnPrimeId.Value);
        }
        catch (Exception ex)
        {
            Page.Message("0", val);
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Insert, Update ends.");
        if (flg == 0 && s == "1")
        {
            Session[Declarations.Message] = (val == "0") ? "0" : "1";
            //Server.Transfer("CountryMasterData.aspx");
            funcpass();

        }
        ClearData(s);


    }

    void funcpass()
    {
        HtmlGenericControl all = new HtmlGenericControl();
        all.InnerHtml = "<script language='javascript'>PassParametersToPatentPage(" + objclient.ClientID + ",'" + objclient.ClientName + "','" + objclient.Address.Replace("\r\n","<br>") + "'," + objclient.CountryID + "," + objclient.StateID + "," + objclient.CityID + ",'" + objclient.Pincode + "'); </script>";
        Page.Controls.Add(all);
    }
    void ClearData(string status)
    {
        if (status == "1")
        {
            Page.TraceWrite("clearData starts.");
            objclient.CountryID = 0;
            objclient.CityID = 0;
            objclient.StateID = 0;
            txtclientname.Value = "";
            txtclientaddress.Value= "";
            txtPincode.Value = "";
            Page.TraceWrite("clear Data ends.");
        }
    }
}