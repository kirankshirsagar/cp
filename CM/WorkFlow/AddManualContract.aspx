﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CM.master" CodeFile="AddManualContract.aspx.cs"
    Inherits="WorkFlow_AddManualContract" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../JQueryValidations/jquery-ui-1.8.19.custom.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
    <link href="../JQueryValidations/jquery-ui-1.8.19.custom.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../scripts/jquery-ui-1.8.16.custom.css" />
    <link href="../Styles/jquery-ui-1.8.21.css" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../modalfiles/modal.css" type="text/css" />
    <script type="text/javascript" src="../scripts/jquery-ui-timepicker-addon.js"></script>
    <link rel="stylesheet" href="../scripts/jquery-ui-timepicker-addon.css" type="text/css" />
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="Masterlinks" runat="server" />
    </div>
    <style>
        .validation
        {
            background: White;
            color: red;
            height: 24px;
            line-height: 24px;
            font-size: 15px;
            font-style: normal;
            padding: 0px;
            margin: 0px; /* float: left;
            border: solid 1px silver;*/
            z-index: 99;
            filter: alpha(opacity=50);
            opacity: 0.5;
        }
    </style>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
    <script language="javascript" type="text/javascript">
        function SetCustomerLable(id) {
            if (id == "rdoCustomer") {
                $('.CSName').html('<span class="required">*</span> Customer name')
            }
            else if (id == "rdoOthers") {
                $('.CSName').html('<span class="required">*</span> Others name')
            }
            else {
                $('.CSName').html('<span class="required">*</span> Supplier name')
            }
        }
        $(document).ready(function () {
            $('.CustomCurrency').on('keypress', function (evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode == 20) {
                    return true;
                }
                if (charCode == 13)
                    return false;

                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    if (charCode != 46) {
                        return false;
                    }
                }
                var count = 0;
                var myNum = this.value + String.fromCharCode(charCode);

            });
            $('#ui-datepicker-div').hide();
            $("#requestLink").addClass("menulink");
            $("#requesttab").addClass("selectedtab");
            $('textarea').autogrow();

            $('.trdynamic').find("label").css('float', 'none');
            $('.trdynamic').find('input[type="radio"]').css('margin-left', '15px');
            $('.trdynamic').find('input[type="radio"]').parent().find('label').css('margin-left', '5px');

            $('.trdynamic').find('input[type="checkbox"]').parent().find('label').css('margin-left', '5px');
            $('.trdynamic').find('input[type="checkbox"]').css('margin-left', '15px');

        });
        $(window).load(function () {
            $("#ui-datepicker-div").hide();
        });
    </script>
    <div>
        <h2>
            Manual Contract</h2>
        <div style="margin-left: 280px">
            <label for="time_entry_issue_id">
                Contract template :
            </label>
            &nbsp;
            <asp:DropDownList ID="ddlContractTemplate" Style="width: 40%" ClientIDMode="Static"
                runat="server" class="chzn-select" AutoPostBack="true" onchange="ShowKeyObligations(this);"
                OnSelectedIndexChanged="ddlContractTemplate_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
        <br>
        <div id="tblContainer">
            <asp:PlaceHolder ID="phQuestionnair" runat="server" ClientIDMode="Static"></asp:PlaceHolder>
            <div class="box tabularr" runat="server" id="dvKeyObligations" style="margin-left: 345px;
                display: none">
                <fieldset style="width: 700px; border-style: none; margin-left: -150px">
                    <legend style="color: none; font-weight: bold; border-style: none; color: orange">Request
                        Details</legend>
                    <p>
                        <label for="time_entry_issue_id">
                            Contracting party</label>
                        <select id="ddlContractingParty" clientidmode="Static" runat="server" style="width: 30%;"
                            class="chzn-select chzn-select AF ">
                            <option></option>
                        </select>
                        <br>
                        <em></em>
                    </p>
                    <p clientidmode="Static" class="CustomerOrSupplier" runat="Server" id="CustomerOrSupplierId"
                        style="display: block">
                        <label for="time_entry_issue_id">
                            <span class="required">*</span>Customer or Supplier or Others</label>
                        <asp:RadioButton ID="rdoCustomer" onclick="javascript:SetCustomerLable(this.id)"
                            Checked="true" GroupName="radiobutton1" ClientIDMode="Static" runat="Server" />Customer
                        <asp:RadioButton ID="rdoSupplier" onclick="javascript:SetCustomerLable(this.id)"
                            GroupName="radiobutton1" ClientIDMode="Static" runat="Server" />Supplier
                        <asp:RadioButton ID="rdoOthers" onclick="javascript:SetCustomerLable(this.id)" GroupName="radiobutton1"
                            ClientIDMode="Static" runat="Server" />Others <em></em>
                        <br>
                    </p>
                    <p>
                        <label for="time_entry_issue_id" class="CSName">
                            <span class="required">*</span> Customer</label>
                        <input id="txtclientname" onkeyup="AddNewClient();" runat="server" maxlength="50"
                            clientidmode="static" type="text" style="width: 30%;" class="required WithoughtSemicolon" />
                        <br>
                        <em></em>
                    </p>
                    <p>
                        <label for="time_entry_issue_id">
                            Address</label>
                        <asp:TextBox runat="server" placeholder="Street" MaxLength="50" ClientIDMode="Static"
                            ID="txtclientaddress" class="myclass" Style="width: 30%;"></asp:TextBox>
                        <br>
                        <asp:TextBox runat="server" placeholder="Street" MaxLength="50" ClientIDMode="Static"
                            ID="txtclientaddressStreet2" class="myclass" Style="width: 30%;"></asp:TextBox>
                    </p>
                    <p>
                        <input id="ddlcity" class="" runat="server" placeholder="City Name" clientidmode="static"
                            type="text" maxlength="25" autocomplete="false" style="width: 30%; margin-top: 5px" />
                        <br>
                        <input id="ddlState" class="" runat="server" placeholder="State Name" clientidmode="static"
                            type="text" maxlength="50" autocomplete="false" style="width: 40%; margin-top: 5px;
                            imprtant" />
                        <br>
                        <input id="txtPincode" runat="server" placeholder="Zip code/Post code" clientidmode="static"
                            type="text" maxlength="8" autocomplete="false" style="width: 30%; margin-top: 5px" />
                    </p>
                    <p>
                        <label for="time_entry_issue_id">
                            <span class="required">*</span> Country</label>
                        <select id="ddlCountry" clientidmode="Static" runat="server" style="width: 30%;"
                            class="chzn-select required chzn-select AF ">
                            <option></option>
                        </select>
                    </p>
                    <p>
                        <label for="time_entry_issue_id">
                            <span></span>Contact Number
                        </label>
                        <input id="txtContactNumbers" runat="server" maxlength="20" clientidmode="static" class="PhoneNumberTxtNumSplChar PhoneNumberTxtNumSplCharPaste"
                            type="text" style="width: 30%;" />
                        <em></em>
                    </p>
                    <p>
                        <label for="time_entry_issue_id">
                            <span></span>Email Id
                        </label>
                        <input id="txtEmailId" runat="server" maxlength="50" clientidmode="static" type="text"
                            style="width: 30%;" class="email" />
                        &nbsp; <span runat="server" id="trNotification">
                            <label for="time_entry_issue_id" style="text-align: left; margin-left: 170px; margin-top: -22px;">
                                Send Notification</label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <select id="ddlNotifyCustomer" clientidmode="Static" runat="server" style="width: 11%;
                                margin-left: 50px; float: right;" class="chzn-select required chzn-select">
                                <option value="Y" selected="true">Yes</option>
                                <option value="N">No</option>
                            </select>
                            <em></em></span>
                    </p>
                    <p style="display: none;">
                        <label for="time_entry_issue_id">
                            Term of contract</label>
                        <input id="txtContractTerm" runat="server" maxlength="100" clientidmode="static"
                            type="text" autocomplete="false" style="width: 20%;" />
                        <em></em>
                    </p>
                    <p>
                        <label for="time_entry_issue_id">
                            <span class="required" id="SpanOriginalcurrency">*</span>Original Currency</label>
                        <asp:DropDownList ID="ddlOriginalCurrency" ClientIDMode="Static" runat="server" Style="width: 31%;"
                            class="chzn-select chzn-select AF " onchange="Checkthisvalue();">
                        </asp:DropDownList>
                        <em></em>
                        <p>
                            <label for="time_entry_issue_id">
                                <span class="required" id="SpanTotalValue">*</span>Contract value</label>
                            <input id="txtContractValue" class="Currency" runat="server" maxlength="15" clientidmode="static"
                                type="text" autocomplete="false" style="width: 20%;" onchange="Checkthisvalue();" />
                            <em></em>
                        </p>
                        <p>

                         <label for="time_entry_issue_id">
                           <span class="required" id="Span3">* </span>Priority</label>
                       
                         <select id="ddlPrority" clientidmode="Static" onchange="HideShowPReason();" runat="server"
                                style="width: 31%" class="chzn-select required">
                                <option></option>
                            </select>

                        &nbsp; <span runat="server" id="Span2">
                            <label  id="lblPriorityreason" clientidmode="Static" runat="server" for="time_entry_issue_id" style="text-align: left; margin-left: 170px; margin-top: -22px;">
                                Priority Reason</label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input id="txtpriorityremark" runat="server" clientidmode="Static" style="width: 30%;"  maxlength="100"/>
                            <em></em></span>

                       
                        </p>
                        <%-- Custom field area for Request Form added by kiran, 18 Mar 2016 --%>
                        <div id="divrequestForm" style="display: block; margin-right: 300px;">
                            <table id="MetaDataTable" runat="server" width="140%" clientidmode="Static">
                            </table>
                        </div>
                        <br />
                        <legend style="color: none; font-weight: bold; border-style: none; color: orange">Important
                            Dates</legend>
                        <p>
                            <label for="time_entry_issue_id">
                                Effective date of contract
                            </label>
                            <input id="txteffectivedate" runat="server" maxlength="100" clientidmode="static"
                                readonly="readonly" type="text" autocomplete="false" style="width: 20%;" class="datepicker" />
                        </p>
                        <p>
                            <label for="time_entry_issue_id">
                                Expiration date of contract
                            </label>
                            <input id="txtexpirationdate" runat="server" maxlength="100" clientidmode="static"
                                readonly="readonly" type="text" autocomplete="false" style="width: 20%;" class="datepicker" />
                            <asp:RequiredFieldValidator ID="reqexpir" runat="server" Display="Dynamic" ControlToValidate="txtexpirationdate"
                                Font-Italic="false" ErrorMessage="This field is required." ValidationGroup="Save"></asp:RequiredFieldValidator>
                        </p>
                        <div style="margin-left: 30px">
                            <fieldset>
                                <legend style="color: Black; font-weight: bold">Set reminder for expiration date</legend>
                                <table width="104%" style="table-layout: fixed" border="0" cellpadding="0" cellspacing="0"
                                    class="reminder">
                                    <thead>
                                        <tr>
                                            <th width="2%">
                                            </th>
                                            <th width="32%">
                                            </th>
                                            <th width="69%">
                                            </th>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td style="display: block;">
                                            <input type="checkbox" id="chkexp180" runat="server" clientidmode="Static" onchange="CheckDateStatus('Expiration','ddlRemindExpiration1','ddlexp180Users1','ddlexp180Users2','chkexp180');" />
                                        </td>
                                        <td>
                                            <select id="ddlRemindExpiration1" onchange="ddlDateStatus('Expiration','ddlRemindExpiration1','ddlexp180Users1','ddlexp180Users2','chkexp180');"
                                                clientidmode="Static" runat="server" style="width: 207px" class="cssExpire1 chzn-select chzn-select">
                                                <option value="0">-----No Reminder------</option>
                                                <option value="180">Remind one eighty days before</option>
                                                <option value="90">Remind ninety days before</option>
                                                <option value="60">Remind sixty days before</option>
                                                <option value="30">Remind thirty days before</option>
                                                <option value="7">Remind seven days before</option>
                                                <option value="1">Remind one day before</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select id="ddlexp180Users1" onchange="SetUserValue('ddlexp180Users1','ddlRemindExpiration1','ddlexp180Users2');"
                                                clientidmode="Static" runat="server" style="width: 150px" class="chzn-select chzn-select">
                                                <option></option>
                                            </select>
                                            <select id="ddlexp180Users2" onchange="SetUserValue('ddlexp180Users2','ddlRemindExpiration1','ddlexp180Users1');"
                                                clientidmode="Static" runat="server" style="width: 150px" class="chzn-select chzn-select">
                                                <option></option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="display: block;">
                                            <input type="checkbox" id="chkexp90" runat="server" clientidmode="Static" onclick="CheckDateStatus('Expiration','ddlRemindExpiration2','ddlexp90Users1','ddlexp90Users2','chkexp90');" />
                                        </td>
                                        <td>
                                            <select id="ddlRemindExpiration2" onchange="ddlDateStatus('Expiration','ddlRemindExpiration2','ddlexp90Users1','ddlexp90Users2','chkexp90');"
                                                clientidmode="Static" runat="server" style="width: 207px" class="cssExpire2 chzn-select chzn-select">
                                                <option value="0">-----No Reminder------</option>
                                                <option value="180">Remind one eighty days before</option>
                                                <option value="90">Remind ninety days before</option>
                                                <option value="60">Remind sixty days before</option>
                                                <option value="30">Remind thirty days before</option>
                                                <option value="7">Remind seven days before</option>
                                                <option value="1">Remind one day before</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select id="ddlexp90Users1" onchange="SetUserValue('ddlexp90Users1','ddlRemindExpiration2','ddlexp90Users2');"
                                                clientidmode="Static" runat="server" style="width: 150px" class="chzn-select chzn-select">
                                                <option></option>
                                            </select>
                                            <select id="ddlexp90Users2" onchange="SetUserValue('ddlexp90Users2','ddlRemindExpiration2','ddlexp90Users1');"
                                                clientidmode="Static" runat="server" style="width: 150px" class="chzn-select chzn-select">
                                                <option></option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="display: block;">
                                            <input type="checkbox" id="chkexp60" runat="server" clientidmode="Static" onclick="CheckDateStatus('Expiration','ddlRemindExpiration3','ddlexp60Users1','ddlexp60Users2','chkexp60');" />
                                        </td>
                                        <td>
                                            <select id="ddlRemindExpiration3" onchange="ddlDateStatus('Expiration','ddlRemindExpiration3','ddlexp60Users1','ddlexp60Users2','chkexp60');"
                                                clientidmode="Static" runat="server" style="width: 207px" class="cssExpire3 chzn-select chzn-select">
                                                <option value="0">-----No Reminder------</option>
                                                <option value="180">Remind one eighty days before</option>
                                                <option value="90">Remind ninety days before</option>
                                                <option value="60">Remind sixty days before</option>
                                                <option value="30">Remind thirty days before</option>
                                                <option value="7">Remind seven days before</option>
                                                <option value="1">Remind one day before</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select id="ddlexp60Users1" onchange="SetUserValue('ddlexp60Users1','ddlRemindExpiration3','ddlexp60Users2');"
                                                clientidmode="Static" runat="server" style="width: 150px" class="chzn-select chzn-select">
                                                <option></option>
                                            </select>
                                            <select id="ddlexp60Users2" onchange="SetUserValue('ddlexp60Users2','ddlRemindExpiration3','ddlexp60Users1');"
                                                clientidmode="Static" runat="server" style="width: 150px" class="chzn-select chzn-select">
                                                <option></option>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                    </p>
            </div>
            <p>
                <label for="time_entry_issue_id">
                    Renewal notification date
                </label>
                <input id="txtrenewaldate" runat="server" maxlength="100" clientidmode="static" readonly="readonly"
                    type="text" autocomplete="false" style="width: 20%;" class="datepicker" />
                <asp:RequiredFieldValidator ID="reqrenewal" runat="server" Display="Dynamic" ControlToValidate="txtrenewaldate"
                    Font-Italic="false" ErrorMessage="This field is required." ValidationGroup="Save"></asp:RequiredFieldValidator>
            </p>
            <div style="margin-left: 30px">
                <fieldset>
                    <legend style="color: Black; font-weight: bold">Set reminder for renewal date</legend>
                    <table width="104%" style="table-layout: fixed" border="0" cellpadding="0" cellspacing="0"
                        class="reminder">
                        <thead>
                            <tr>
                                <th width="2%">
                                </th>
                                <th width="32%">
                                </th>
                                <th width="69%">
                                </th>
                            </tr>
                        </thead>
                        <tr>
                            <td style="display: block;">
                                <input type="checkbox" id="chkrem180" runat="server" clientidmode="Static" onclick="CheckDateStatus('Renewal','ddlRemindRenewal1','ddlRenewal180User1','ddlRenewal180User2','chkrem180');" />
                            </td>
                            <td>
                                <select id="ddlRemindRenewal1" onchange="ddlDateStatus('Renewal','ddlRemindRenewal1','ddlRenewal180User1','ddlRenewal180User2','chkrem180');"
                                    clientidmode="Static" runat="server" style="width: 207px" class="cssRemind1 chzn-select chzn-select">
                                    <option value="0">-----No Reminder------</option>
                                    <option value="180">Remind one eighty days before</option>
                                    <option value="90">Remind ninety days before</option>
                                    <option value="60">Remind sixty days before</option>
                                    <option value="30">Remind thirty days before</option>
                                    <option value="7">Remind seven days before</option>
                                    <option value="1">Remind one day before</option>
                                </select>
                            </td>
                            <td>
                                <select id="ddlRenewal180User1" onchange="SetUserValue('ddlRenewal180User1','ddlRemindRenewal1','ddlRenewal180User2');"
                                    clientidmode="Static" runat="server" style="width: 150px" class="chzn-select chzn-select">
                                    <option></option>
                                </select>&nbsp;<select id="ddlRenewal180User2" onchange="return SetUserValue('ddlRenewal180User2','ddlRemindRenewal1','ddlRenewal180User1')"
                                    clientidmode="Static" runat="server" style="width: 150px" class="chzn-select chzn-select">
                                    <option></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td style="display: block;">
                                <input type="checkbox" id="chkrem90" runat="server" clientidmode="Static" onclick="CheckDateStatus('Renewal','ddlRemindRenewal2','ddlRenewal90User1','ddlRenewal90User2','chkrem90');" />
                            </td>
                            <td>
                                <select id="ddlRemindRenewal2" onchange="ddlDateStatus('Renewal','ddlRemindRenewal2','ddlRenewal90User1','ddlRenewal90User2','chkrem90');"
                                    clientidmode="Static" runat="server" style="width: 207px" class="cssRemind2 chzn-select chzn-select">
                                    <option value="0">-----No Reminder------</option>
                                    <option value="180">Remind one eighty days before</option>
                                    <option value="90">Remind ninety days before</option>
                                    <option value="60">Remind sixty days before</option>
                                    <option value="30">Remind thirty days before</option>
                                    <option value="7">Remind seven days before</option>
                                    <option value="1">Remind one day before</option>
                                </select>
                            </td>
                            <td>
                                <select id="ddlRenewal90User1" onchange="SetUserValue('ddlRenewal90User1','ddlRemindRenewal2','ddlRenewal90User2');"
                                    clientidmode="Static" runat="server" style="width: 150px" class="chzn-select chzn-select">
                                    <option></option>
                                </select>&nbsp;<select id="ddlRenewal90User2" onchange="return SetUserValue('ddlRenewal90User2','ddlRemindRenewal2','ddlRenewal90User1')"
                                    clientidmode="Static" runat="server" style="width: 150px" class="chzn-select chzn-select">
                                    <option></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td style="display: block;">
                                <input type="checkbox" id="chkrem60" runat="server" clientidmode="Static" onclick="CheckDateStatus('Renewal','ddlRemindRenewal3','ddlRenewal60User1','ddlRenewal60User2','chkrem60');" />
                            </td>
                            <td>
                                <select id="ddlRemindRenewal3" onchange="ddlDateStatus('Renewal','ddlRemindRenewal3','ddlRenewal60User1','ddlRenewal60User2','chkrem60');"
                                    clientidmode="Static" runat="server" style="width: 207px" class="cssRemind3 chzn-select chzn-select">
                                    <option value="0">-----No Reminder------</option>
                                    <option value="180">Remind one eighty days before</option>
                                    <option value="90">Remind ninety days before</option>
                                    <option value="60">Remind sixty days before</option>
                                    <option value="30">Remind thirty days before</option>
                                    <option value="7">Remind seven days before</option>
                                    <option value="1">Remind one day before</option>
                                </select>
                            </td>
                            <td>
                                <select id="ddlRenewal60User1" onchange="SetUserValue('ddlRenewal60User1','ddlRemindRenewal3','ddlRenewal60User2');"
                                    clientidmode="Static" runat="server" style="width: 150px" class="chzn-select chzn-select">
                                    <option></option>
                                </select>&nbsp;<select id="ddlRenewal60User2" onchange="SetUserValue('ddlRenewal60User2','ddlRemindRenewal3','ddlRenewal60User1');"
                                    clientidmode="Static" runat="server" style="width: 150px" class="chzn-select chzn-select">
                                    <option></option>
                                </select>
                            </td>
                        </tr>
                        <%-- Custom Remind Date   --%>
                        <input id="hdnReminddate1" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" />
                        <input id="hdnReminddate2" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" />
                        <input id="hdnReminddate3" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" />
                        <%-- Custom Remind Days   --%>
                        <input id="hdnRemindDay10" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay20" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay30" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay11" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay21" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay31" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay12" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay22" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay32" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <%-- Custom Remind date user1   --%>
                        <input id="hdnRemindDay1User10" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay1User20" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay1User11" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay1User21" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay1User12" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay1User22" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <%-- Custom Remind date user2   --%>
                        <input id="hdnRemindDay2User10" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay2User20" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay2User11" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay2User21" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay2User12" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay2User22" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <%-- Custom Remind date user3   --%>
                        <input id="hdnRemindDay3User10" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay3User20" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay3User11" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay3User21" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay3User12" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                        <input id="hdnRemindDay3User22" runat="server" clientidmode="static" readonly="readonly"
                            type="hidden" value="0" />
                    </table>
                </fieldset>
                </p>
            </div>
            <%-- Custom field area for Important Date added by kiran, 18 Mar 2016 --%>
            <div id="divImportantDate" style="display: block;">
                <table id="ImportantDateTable" runat="server" width="90%" clientidmode="Static" style="table-layout: fixed">
                </table>
            </div>
            </div>
            <div class=" box tabularr boxKyObligation" runat="server" id="boxKyObligation" style="margin-left: 345px;
                display: none">
                <fieldset style="width: 700px; border-style: none; margin-left: -150px">
                    <legend style="color: Orange; font-weight: bold">Key field obligations</legend>
                    <p>
                        <label for="time_entry_issue_id">
                            Liability Cap
                        </label>
                        <textarea id="txtLiabilityCap" class="KeyG" runat="server" maxlength="1000" clientidmode="static"
                            type="text" autocomplete="false" style="width: 280px; height: 40px" rows="10" />
                        <em></em>
                    </p>
                    <p>
                        <label for="time_entry_issue_id">
                            Indemnity
                        </label>
                        <textarea id="txtIndemnity" class="KeyG" runat="server" maxlength="1000" clientidmode="static"
                            type="text" autocomplete="false" style="width: 280px; height: 40px" rows="10" />
                        <em></em>
                    </p>
                    <p>
                        <label for="time_entry_issue_id">
                            Change of Control
                        </label>
                        <input id="rdIsChangeofControlYes" name="IsChangeofControl" runat="server" type="radio"
                            class="required" />Yes
                        <input id="rdIsChangeofControlNo" name="IsChangeofControl" runat="server" type="radio"
                            class="required" />No
                    </p>
                    <p>
                        <label for="time_entry_issue_id">
                            Entire agreement clause
                        </label>
                        <input id="rdIsAgreementClauseYes" name="IsAgreementClause" runat="server" type="radio"
                            class="required" />Yes
                        <input id="rdIsAgreementClauseNo" name="IsAgreementClause" runat="server" type="radio"
                            class="required" />No
                    </p>
                    <p>
                        <label for="time_entry_issue_id">
                            Strict liability
                        </label>
                        <input id="rdIsStrictliabilityYes" name="IsStrictliability" runat="server" type="radio"
                            class="required" />Yes
                        <input id="rdIsStrictliabilityNo" name="IsStrictliability" runat="server" type="radio"
                            class="required" />No
                    </p>
                    <p>
                        <label for="time_entry_issue_id">
                            Strict liability description
                        </label>
                        <textarea id="txtStrictliability" class="KeyG" runat="server" maxlength="1000" clientidmode="static"
                            type="text" autocomplete="false" style="width: 280px; height: 40px" rows="10" />
                        <em></em>
                    </p>
                    <p>
                        <label for="time_entry_issue_id">
                            Third party guarantee required
                        </label>
                        <input id="rdIsGuaranteeRequiredYes" name="IsGuaranteeRequired" runat="server" type="radio"
                            class="required" />Yes
                        <input id="rdIsGuaranteeRequiredNo" name="IsGuaranteeRequired" runat="server" type="radio"
                            class="required" />No
                    </p>
                    <p>
                        <label for="time_entry_issue_id">
                            Insurance
                        </label>
                        <textarea id="txtInsurance" class="KeyG" runat="server" maxlength="1000" clientidmode="static"
                            type="text" autocomplete="false" style="width: 280px; height: 40px" rows="10" />
                        <em></em>
                    </p>
                    <p>
                        <label for="time_entry_issue_id">
                            Termination
                        </label>
                        <textarea id="txtTerminationdesc" class="KeyG" runat="server" maxlength="1000" clientidmode="static"
                            type="text" autocomplete="false" style="width: 280px; height: 40px" rows="10" />
                        <em></em>
                    </p>
                    <p>
                        <input type="checkbox" id="chkIsTerminationConvenience" clientidmode="Static" onclick="AvoidReadonly(1);"
                            runat="server" />Termination Convenience<br />
                        <input type="checkbox" id="chkIsTerminationmaterial" clientidmode="Static" onclick="AvoidReadonly(2);"
                            runat="server" />Termination Material<br />
                        <input type="checkbox" id="chkIsTerminationinsolvency" clientidmode="Static" onclick="AvoidReadonly(3);"
                            runat="server" />Termination Insolvency<br />
                        <input type="checkbox" id="chkIsTerminationinChangeofcontrol" clientidmode="Static"
                            runat="server" onclick="AvoidReadonly(4);" />Termination Change of control<br />
                        <input type="checkbox" id="chkIsTerminationothercauses" clientidmode="Static" runat="server"
                            onclick="AvoidReadonly(4);" />
                        Termination of other causes
                    </p>
                    <p>
                        <label for="time_entry_issue_id">
                            Assignment/Novation/<br>
                            Subcontract</label>
                        <textarea id="txtNovation" class="KeyG" runat="server" maxlength="1000" clientidmode="static"
                            type="text" autocomplete="false" style="width: 280px; height: 40px" rows="10" />
                        <em></em>
                    </p>
                    <p>
                        <label for="time_entry_issue_id">
                            Others</label>
                        <textarea id="OthersText" class="KeyO" runat="server" maxlength="2500" clientidmode="static"
                            autocomplete="false" style="width: 280px; height: 40px" rows="10" />
                        <em></em>
                    </p>
                    <%-- Custom field area for key obligation added by kiran, 18 Mar 2016 --%>
                    <div id="divkeyObligation" style="display: block; margin-right: 320px;">
                        <table id="keyObligationTable" runat="server" width="150%" clientidmode="Static">
                        </table>
                    </div>
                </fieldset>
                <br />
            </div>
        </div>
    </div>
    <br />
    <asp:Button ID="btnSaveDraft" runat="server" OnClick="btnSaveDraft_Click" Text="Save Draft"
        Visible="false" class="submit" />
    <asp:Button ID="btnGenerateContract" runat="server" OnClick="btnGenerateContract_Click"
        class="submit btn_validate" Text="Save" Visible="false" ClientIDMode="Static" />
    <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="Back" />
    <asp:HiddenField ID="HdnRequestID" runat="server" />
    <asp:HiddenField ID="hdnContractID" runat="server" />
    <asp:HiddenField ID="hdnUserStatus" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnContractypeId" runat="server" ClientIDMode="Static" Value="0" />
    <asp:HiddenField ID="hdnLinkStatus" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnReminderName1" runat="server" ClientIDMode="Static" Value="" />
    <asp:HiddenField ID="hdnReminderName2" runat="server" ClientIDMode="Static" Value="" />
    <asp:HiddenField ID="hdnReminderName3" runat="server" ClientIDMode="Static" Value="" />
    <div style="display: none">
        <asp:Button ID="btnChangeUser" runat="server" ClientIDMode="Static" OnClick="btnChangeUser_Click" />
    </div>
    <script type="text/javascript" language="javascript">
        function ValidateContractID() {
            var txtContractValue = $('#txtContractValue').val();
            var ddlOriginalCurrency = $('#ddlOriginalCurrency').val();
            if ((txtContractValue == "0" || txtContractValue == "" || txtContractValue == "0.00") && (ddlOriginalCurrency == "0")) {
                $('#txtContractValue').removeClass('required', 'required');
                $('#ddlOriginalCurrency').removeClass('required', 'required');
                $('div').find('[class="tooltip_outer ddlOriginalCurrency"]').find(".tool_tip").hide();
                $('#txtContractValue').next('[class="tooltip_outer"]').find(".tool_tip").hide();
                $('#txtContractValue').css('border-color', '');
                $('#ddlOriginalCurrency').style('border-color', '');
                return true;
            }
            else {
                if (txtContractValue == "0" || txtContractValue == "" || txtContractValue == "0.00") {
                    $('#txtContractValue').val('');
                    $('#SpanTotalValue').show();
                    $(".tool_tip").show();
                    $('#txtContractValue').css('border-color', 'red');
                    $('#txtContractValue').addClass('required', 'required');

                    return false;
                }
                if (ddlOriginalCurrency == "0") {

                    $('#ddlOriginalCurrency').val('');
                    $('#SpanOriginalcurrency').show();
                    $(".tool_tip").show();
                    $('#ddlOriginalCurrency').css('border-color', 'red');
                    $('#ddlOriginalCurrency').addClass('required', 'required');
                    return false;
                }
            }
        }

        function Checkthisvalue() {

            var txtContractValue = $('#txtContractValue').val();
            var ddlOriginalCurrency = $('#ddlOriginalCurrency').val();

            if (ddlOriginalCurrency != "0") {

                $('#txtContractValue').addClass('required', 'required');
            }
            if (txtContractValue != "" || txtContractValue != "0" || txtContractValue != "0.00") {

                $('#ddlOriginalCurrency').addClass('required', 'required');
            }
        }

        function AvoidReadonly(ctl) {
            if (ctl == 1) {
                if ($("#chkIsTerminationConvenience").is(':checked'))
                    $('#txtTerminationConvenience').prop('readonly', false);
                else
                    $('#txtTerminationConvenience').prop('readonly', true);
            }

            if (ctl == 2) {
                if ($("#chkIsTerminationmaterial").is(':checked'))
                    $('#txtTerminationmaterial').prop('readonly', false);
                else
                    $('#txtTerminationmaterial').prop('readonly', true);
            }

            if (ctl == 3) {
                if ($("#chkIsTerminationinsolvency").is(':checked'))
                    $('#txtTerminationinsolvency').prop('readonly', false);
                else
                    $('#txtTerminationinsolvency').prop('readonly', true);
            }

            if (ctl == 4) {
                if ($("#chkIsTerminationinChangeofcontrol").is(':checked'))
                    $('#txtTerminationinChangeofcontrol').prop('readonly', false);
                else
                    $('#txtTerminationinChangeofcontrol').prop('readonly', true);
            }
        }

        function ddlDateStatus(ctl, ctl2, CtlU1, CtlU2, CtlChk) {
            var flag = true;
            var ddlValue = $("#" + ctl2 + " ").val();
            if (ddlValue == '0') {
                $("#" + CtlU1 + " ").val('0').trigger("liszt:updated");
                $("#" + CtlU2 + " ").val('0').trigger("liszt:updated");
                $("#" + CtlChk + " ").prop('checked', false);
            }
            return flag;
        }

        function CheckDateStatus(ctl, ctl2, CtlU1, CtlU2, CtlChk) {
            var flag = true;
            var ddlValue = $("#" + ctl2 + " ").val();
            if (ctl == "Renewal") {
                if ($("#txtrenewaldate").val() == "") {
                    alert('Please insert renewal date ');
                    $('#ddlRemindRenewal1').val("0").trigger("liszt:updated");
                    $('#ddlRemindRenewal2').val("0").trigger("liszt:updated");
                    $('#ddlRemindRenewal3').val("0").trigger("liszt:updated");
                    $("#" + CtlChk + " ").prop("checked", false);
                    flag = false;
                }
            }
            else if (ctl == "Expiration") {
                if ($("#txtexpirationdate").val() == "") {
                    alert('Please insert expiration date ');
                    $('#ddlRemindExpiration3').val("0").trigger("liszt:updated");
                    $('#ddlRemindExpiration2').val("0").trigger("liszt:updated");
                    $('#ddlRemindExpiration1').val("0").trigger("liszt:updated");
                    $("#" + CtlChk + " ").prop("checked", false);
                    flag = false;
                }
            }

            if ($("#" + CtlChk + " ").prop("checked") == false) {
                $("#" + ctl2 + " ").val('0').trigger("liszt:updated");
                $("#" + CtlU1 + " ").val('0').trigger("liszt:updated");
                $("#" + CtlU2 + " ").val('0').trigger("liszt:updated");
            }

            return flag;
        }

    </script>
    <script type="text/javascript">
        $('.submit').live("click", function () {
            var GCount = 0;
            var OCount = 0;

            $('.KeyG').each(function () {
                if ($(this).val().length > 1000) {
                    alert($(this).prev('label').html().trim() + ' should be of less than 1000 characters.')
                    $(this).focus();
                    GCount++;
                    return false;
                }
            });

            $('.KeyO').each(function () {
                if ($(this).val().length > 2500) {
                    alert($(this).prev('label').html().trim() + ' should be of less than 2500 characters.')
                    $(this).focus();
                    OCount++;
                    return false;
                }
            });


            if (GCount == 0 && OCount == 0) {
                ShowProgress();
            }
            else {
                return false;
            }
        });

        function ReloadMe() {
            setTimeout(function () {
                location.href = '../Workflow/AddManualContract.aspx';
            }
             , 3000);
        }

        function ShowKeyObligations(obj) {
            if ($(obj).val() != '0') {
                //                $('.box').show();
                //                $('.boxKyObligation').show();
            }
            else {

                $('.box').hide();
                $('.boxKyObligation').hide();
            }
        }

        $(document).ready(function () {
            if (txtContractValue == "" && ddlOriginalCurrency == "0") { }
            var txtContractValue = $('#txtContractValue').val();
            var ddlOriginalCurrency = $('#ddlOriginalCurrency').val();
            if (txtContractValue != "" && ddlOriginalCurrency != "undefined") {
                $('#SpanOriginalcurrency').show();
            }
            else {
                $('#SpanOriginalcurrency').hide();
            }
            if ((ddlOriginalCurrency != "undefined" && txtContractValue != "")) {
                $('#SpanTotalValue').show();
            }
            else {
                $('#SpanTotalValue').hide();
            }

            try {
                $("#txteffectivedate").datepicker({
                    showOn: "both",
                    buttonImage: "../Styles/css/icons/16/calendar_1.png",
                    buttonImageOnly: true,
                    defaultDate: "+1d",
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1,
                    dateFormat: 'dd-M-yy'
                }).attr('placeholder', 'dd-Mmm-yyyy');
                $("#txtexpirationdate").datepicker({
                    showOn: "both",
                    buttonImage: "../Styles/css/icons/16/calendar_1.png",
                    buttonImageOnly: true,
                    defaultDate: "+1d",
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1,
                    dateFormat: 'dd-M-yy'
                }).attr('placeholder', 'dd-Mmm-yyyy');
                $("#txtrenewaldate").datepicker({
                    showOn: "both",
                    buttonImage: "../Styles/css/icons/16/calendar_1.png",
                    buttonImageOnly: true,
                    defaultDate: "+1d",
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1,
                    dateFormat: 'dd-M-yy'
                }).attr('placeholder', 'dd-Mmm-yyyy');

                $(".datepicker").datepicker({
                    showOn: "both",
                    buttonImage: "../Styles/css/icons/16/calendar_1.png",
                    buttonImageOnly: true,
                    defaultDate: "+1d",
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1,
                    dateFormat: 'dd-M-yy'
                }).attr('placeholder', 'dd-Mmm-yyyy');
            }
            catch (e) {
                //alert(e);
            }

        });
    </script>
    <script type="text/javascript" language="javascript">
        function AvoidReadonly(ctl) {
            if (ctl == 1) {
                if ($("#chkIsTerminationConvenience").is(':checked'))
                    $('#txtTerminationConvenience').prop('readonly', false);
                else
                    $('#txtTerminationConvenience').prop('readonly', true);
            }

            if (ctl == 2) {
                if ($("#chkIsTerminationmaterial").is(':checked'))
                    $('#txtTerminationmaterial').prop('readonly', false);
                else
                    $('#txtTerminationmaterial').prop('readonly', true);
            }

            if (ctl == 3) {
                if ($("#chkIsTerminationinsolvency").is(':checked'))
                    $('#txtTerminationinsolvency').prop('readonly', false);
                else
                    $('#txtTerminationinsolvency').prop('readonly', true);
            }

            if (ctl == 4) {
                if ($("#chkIsTerminationinChangeofcontrol").is(':checked'))
                    $('#txtTerminationinChangeofcontrol').prop('readonly', false);
                else
                    $('#txtTerminationinChangeofcontrol').prop('readonly', true);
            }
        }

        function SetUserValue(ctl1, ddl, ctl2) {
            var UserValue1 = $("#" + ctl1 + " ").val();
            var UserValue2 = $("#" + ctl2 + " ").val();
            if (UserValue1 == "0" && UserValue2 == "0") {
                $("#" + ddl + " ").val("0").trigger("liszt:updated");
            }

            if (UserValue1 != "0" || UserValue2 != "0") {
                $("#" + ctl2 + " ").closest('tr').find('div.validation').remove();
            }
            if ((UserValue1 != UserValue2) && $("#" + ddl + " ").val() != "0") {
                $("#" + ctl2 + " ").closest('tr').find('div.validation').remove();
            }
        }
    </script>
    <script language="javascript" type="text/javascript">
        function setUpdated(obj) {
            $(obj).attr("IsUpdated", "Y");
        }
        $(function () {
            $(".datepicker").datepicker({ showOn: "both",
                buttonImage: "../Styles/css/icons/16/calendar_1.png",
                buttonImageOnly: true,
                prevText: 'Previous',
                yearRange: "-100:+10",
                changeMonth: true,
                dateFormat: 'dd-M-yy',
                buttonText: '',
                onClose: function (dateText, inst) {
                },
                changeYear: true
            }).attr('placeholder', 'dd-MMM-yyyy').blur(function () {
            });
        });      

    </script>
    <script type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#tab2").addClass("selectedtab");
    </script>
    <%--Data Type Validation Script--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.number').live('keypress', function (evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode == 8) {
                    return true;
                }
                else if ((charCode < 48) || (charCode > 57)) {
                    return false;
                }
            });

            $('.int').live('keypress', function (evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode == 8) {
                    return true;
                }
                else if ((charCode < 48) || (charCode > 57)) {
                    return false;
                }
            });

            $('.int').live('paste', function (evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                $obj = $(this);
                setTimeout(function () {
                    myNum = $obj.val();
                    var pattern = /^[0-9]+$/;
                    if (!pattern.test(myNum)) {
                        myNum = myNum.replace(/[^0-9]/g, '');
                        $obj.val(myNum);
                        return false;
                    }
                }, 0);
            });

            $('.numbersOnly').live('keypress', function (evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode == 8) {
                    return true;
                }
                else if ((charCode != 46 || $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57)) {
                    return false;
                }
            });

            /*money  - old name comCurrency*/
            $('.money,.moneyZero').live('keypress', function (evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode == 8) {
                    return true;
                }
                else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    if (charCode != 46) {
                        return false;
                    }
                }

                var caretpos = $(this).caret().start;
                valuestart = $(this).val().substring(0, caretpos);
                valueend = $(this).val().substring(caretpos);
                myNum = valuestart + String.fromCharCode(charCode) + valueend;

                var count = 0;
                var pattern = /^\d{0,8}\.?\d{0,2}$/;
                if (!pattern.test(myNum)) {
                    count = 1;
                }
                if (count == 1) {
                    return false;
                }
                else {
                    if (myNum > 1000000000) {
                        return false;
                    }
                }
                return true;
            });


            $('.money').live('blur', function () {
                if ($(this).val() != "") {
                    var amt = parseFloat($(this).val());
                    if (amt <= 0) {
                        $(this).val('');
                    }
                }
            });

            $('.percentage').live('keypress', function (evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode

                if (charCode == 8) {
                    return true;
                }
                else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    if (charCode != 46) {
                        return false;
                    }
                }
                var count = 0;
                var caretpos = $(this).caret().start;
                valuestart = $(this).val().substring(0, caretpos);
                valueend = $(this).val().substring(caretpos);
                myNum = valuestart + String.fromCharCode(charCode) + valueend;

                var pattern = /^\d{0,3}\.?\d{0,2}$/;
                if (!pattern.test(myNum)) {
                    count = 1;
                }
                if (count == 1) {
                    return false;
                }
                else {
                    if (myNum > 100) {
                        return false;
                    }
                }
                return true;
            });


            $('.Landline').live('keypress', function (evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode == 8) {
                    return true;
                }
                else if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;

                var caretpos = $(this).caret().start;
                valuestart = $(this).val().substring(0, caretpos);
                valueend = $(this).val().substring(caretpos);
                myNum = valuestart + String.fromCharCode(charCode) + valueend;

                if (myNum.length > 10) {
                    return false;
                }
                return true;
            });
            $('.Currency').live('keypress', function (evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode == 8) {
                    return true;
                }
                if (charCode == 13)
                    return false;

                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    if (charCode != 46) {
                        return false;
                    }
                }
                var count = 0;
                var myNum = this.value + String.fromCharCode(charCode);
                //  var pattern = /^\d{0,8}$/;
                var pattern = /^\d{0,8}\.?\d{0,2}$/;
                // var pattern = /^\d{0,8}\.?\d{0,0}$/;
                if (!pattern.test(myNum)) {
                    count = 1;
                }
                if (count == 1) {
                    return false;
                }
                else if (myNum > 99999999) {
                    return false;
                }
                else
                    return true;
            });
        });
    </script>
    <%--Script for Conditional Questions [Added by Bharati]--%>
    <script src="../CommonScripts/dropdownlist.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).on('click', '#chkcust1800', function () {
            if ($("#chkcust1800").prop("checked") == false) {
                $("#ddlcustRemindRenewal10").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal180User10").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal180User20").val('0').trigger("liszt:updated");
            }

            var j = 0; var r = 0;
            $("table tbody tr.trimportantDate").each(function (index, value) {
                $tr = $(value);
                if ($tr.find('.datepicker').val() != undefined) {
                    j++;
                    if ($tr.find('.datepicker').val() == '') {
                        if (r == 0) {
                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                            alert('Please insert ' + lbl + '.');
                            $("#chkcust1800").prop("checked", false);
                            return false;
                        }
                    }
                    r = j;
                }
            });
        });
        $(document).on('click', '#chkcust900', function () {
            if ($("#chkcust900").prop("checked") == false) {
                $("#ddlcustRemindRenewal11").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal90User11").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal90User21").val('0').trigger("liszt:updated");
            }
            var j = 0; var r = 0;
            $("table tbody tr.trimportantDate").each(function (index, value) {
                $tr = $(value);
                if ($tr.find('.datepicker').val() != undefined) {
                    j++;
                    if ($tr.find('.datepicker').val() == '') {
                        if (r == 0) {
                            //alert("Please insert meta data date.");
                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                            alert('Please insert ' + lbl + '.');
                            $("#chkcust900").prop("checked", false);
                            return false;
                        }
                    }
                    r = j;
                }
            });
        });
        $(document).on('click', '#chkcust600', function () {
            if ($("#chkcust600").prop("checked") == false) {
                $("#ddlcustRemindRenewal12").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal90User12").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal90User22").val('0').trigger("liszt:updated");
            }
            var j = 0; var r = 0;
            $("table tbody tr.trimportantDate").each(function (index, value) {
                $tr = $(value);
                if ($tr.find('.datepicker').val() != undefined) {
                    j++;
                    if ($tr.find('.datepicker').val() == '') {
                        if (r == 0) {
                            //alert("Please insert meta data date.");
                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                            alert('Please insert ' + lbl + '.');
                            $("#chkcust600").prop("checked", false);
                            return false;
                        }
                    }
                    r = j;
                }
            });
        });

        ///////////   Custom date 2         /////////////
        $(document).on('click', '#chkcust1801', function () {

            if ($("#chkcust1801").prop("checked") == false) {
                $("#ddlcustRemindRenewal11").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal180User11").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal180User21").val('0').trigger("liszt:updated");
            }
            var j = 0; var r = 0;
            $("table tbody tr.trimportantDate").each(function (index, value) {

                $tr = $(value);
                if ($tr.find('.datepicker').val() != undefined) {
                    j++;
                    if ($tr.find('.datepicker').val() == '') {
                        if (r == 1) {
                            //alert("Please insert meta data date.");
                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                            alert('Please insert ' + lbl + '.');
                            $("#chkcust1801").prop("checked", false);
                            return false;
                        }
                    }
                    r = j;
                }
            });
        });

        $(document).on('click', '#chkcust901', function () {
            if ($("#chkcust901").prop("checked") == false) {
                $("#ddlcustRemindRenewal21").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal90User11").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal90User21").val('0').trigger("liszt:updated");
            }
            var j = 0; var r = 0;
            $("table tbody tr.trimportantDate").each(function (index, value) {
                $tr = $(value);
                if ($tr.find('.datepicker').val() != undefined) {
                    j++;
                    if ($tr.find('.datepicker').val() == '') {
                        if (r == 1) {
                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                            alert('Please insert ' + lbl + '.');
                            $("#chkcust901").prop("checked", false);
                            return false;
                        }
                    }
                    r = j;
                }
            });
        });

        $(document).on('click', '#chkcust601', function () {
            if ($("#chkcust601").prop("checked") == false) {
                $("#ddlcustRemindRenewal31").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal60User11").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal60User21").val('0').trigger("liszt:updated");
            }
            var j = 0; var r = 0;
            $("table tbody tr.trimportantDate").each(function (index, value) {
                $tr = $(value);
                if ($tr.find('.datepicker').val() != undefined) {
                    j++;
                    if ($tr.find('.datepicker').val() == '') {
                        if (r == 1) {
                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                            alert('Please insert ' + lbl + '.');
                            $("#chkcust601").prop("checked", false);
                            return false;
                        }
                    }
                    r = j;
                }
            });
        });

        ///////////   Custom date 3         /////////////
        $(document).on('click', '#chkcust1802', function () {
            if ($("#chkcust1802").prop("checked") == false) {
                $("#ddlcustRemindRenewal12").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal180User12").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal180User22").val('0').trigger("liszt:updated");
            }
            var j = 0; var r = 0;
            $("table tbody tr.trimportantDate").each(function (index, value) {
                $tr = $(value);
                if ($tr.find('.datepicker').val() != undefined) {
                    j++;
                    if ($tr.find('.datepicker').val() == '') {
                        if (r == 2) {
                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                            alert('Please insert ' + lbl + '.');
                            $("#chkcust1802").prop("checked", false);
                            return false;
                        }
                    }
                    r = j;
                }
            });
        });
        $(document).on('click', '#chkcust902', function () {
            if ($("#chkcust902").prop("checked") == false) {
                $("#ddlcustRemindRenewal22").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal90User12").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal90User22").val('0').trigger("liszt:updated");
            }
            var j = 0; var r = 0;
            $("table tbody tr.trimportantDate").each(function (index, value) {
                $tr = $(value);
                if ($tr.find('.datepicker').val() != undefined) {
                    j++;
                    if ($tr.find('.datepicker').val() == '') {
                        if (r == 2) {
                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                            alert('Please insert ' + lbl + '.');
                            $("#chkcust902").prop("checked", false);
                            return false;
                        }
                    }
                    r = j;
                }
            });
        });
        $(document).on('click', '#chkcust602', function () {
            if ($("#chkcust602").prop("checked") == false) {
                $("#ddlcustRemindRenewal32").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal60User12").val('0').trigger("liszt:updated");
                $("#ddlcustRenewal60User22").val('0').trigger("liszt:updated");
            }
            var j = 0; var r = 0;
            $("table tbody tr.trimportantDate").each(function (index, value) {
                $tr = $(value);
                if ($tr.find('.datepicker').val() != undefined) {
                    j++;
                    if ($tr.find('.datepicker').val() == '') {
                        if (r == 2) {
                            var lbl = $tr.find('label[for=time_entry_issue_id]').text();
                            alert('Please insert ' + lbl + '.');
                            $("#chkcust602").prop("checked", false);
                            return false;
                        }
                    }
                    r = j;
                }
            });
        });
        $(document).ready(function () {

            var k = 0;
            $("table tbody tr.trimportantDate ").each(function (index, value) {
                $tr = $(value);
                if ($tr.find('.datepicker').val() != undefined) {
                    var CustomReminder = '<tr class="trdynamic trimportantDate"> <td colspan="2"> <fieldset> <legend style="color: Black; font-weight: bold">Set reminder for <label id="lblRemind' + k + '" style="color: Black; font-weight: bold;font-weight: bold;            float: none; text-align: right;margin-left: 0px; width: 0px;"></label></legend><table width="150%" style="table-layout: fixed" border="0" cellpadding="0" cellspacing="0" class="reminder"> <thead> <tr> <th width="2%"> </th><th width="22%"></th><th width="69%"></th></tr></thead><tr><td style="display: block;"><input type="checkbox" id="chkcust180' + k + '" clientidmode="Static" onclick="CheckDateStatus("Custom","ddlcustRenewal1","ddlcustRenewal180User1","ddlcustRenewal180User2","chkcust180");" /></td><td><select id="ddlcustRemindRenewal1' + k + '" onchange="ddlDateStatus("Custom","ddlcustRemindRenewal1","ddlcustRenewal180User1","ddlcustRenewal180User2","chkcust180");" clientidmode="Static" style="width: 207px" class="chzn-select chzn-select"><option value="0">-----No Reminder------</option><option value="180">Remind one eighty days before</option><option value="90">Remind ninety days before</option><option value="60">Remind sixty days before</option><option value="30">Remind thirty days before</option><option value="7">Remind seven days before</option> <option value="1">Remind one day before</option></select></td> <td><%-- User 1--%><select id="ddlcustRenewal180User1' + k + '" onchange="SetUserValue("ddlcustRenewal180User1","ddlcustRemindRenewal1","ddlcustRenewal180User2");" clientidmode="Static" style="width: 150px" class="chzn-select chzn-select"><option></option></select>&nbsp;<%-- User 2--%><select id="ddlcustRenewal180User2' + k + '" onchange="return SetUserValue("ddlcustRenewal180User2","ddlcustRemindRenewal1","ddlcustRenewal180User1")" clientidmode="Static" style="width: 150px" class="chzn-select chzn-select"><option></option></select></td> </tr> <%-- tr end--%>  <tr> <td style="display: block;">      <input type="checkbox" id="chkcust90' + k + '" clientidmode="Static" onclick="CheckDateStatus("Custom","ddlcustRemindRenewal2","ddlcustRenewal90User1","ddlcustRenewal90User2","chkcust90");" /> </td><td><select id="ddlcustRemindRenewal2' + k + '" onchange="ddlDateStatus("Custom","ddlcustRemindRenewal2","ddlcustRenewal90User1","ddlcustRenewal90User2","chkcust90");"  clientidmode="Static" style="width: 207px" class="chzn-select chzn-select"><option value="0">-----No Reminder------</option><option value="180">Remind one eighty days before</option><option value="90">Remind ninety days before</option><option value="60">Remind sixty days before</option><option value="30">Remind thirty days before</option>    <option value="7">Remind seven days before</option><option value="1">Remind one day before</option></select></td><td><%-- User 1--%><select id="ddlcustRenewal90User1' + k + '" onchange="SetUserValue("ddlcustRenewal90User1","ddlcustRemindRenewal2","ddlcustRenewal90User2");" clientidmode="Static" style="width: 150px" class="chzn-select chzn-select">                 <option></option> </select>&nbsp;<%-- User 2--%><select id="ddlcustRenewal90User2' + k + '" onchange="return SetUserValue("ddlcustRenewal90User2","ddlcustRemindRenewal2","ddlcustRenewal90User1")" clientidmode="Static" style="width: 150px" class="chzn-select chzn-select"><option></option></select></td></tr><%-- tr end--%>  <tr>                                <td style="display: block;"> <input type="checkbox" id="chkcust60' + k + '" clientidmode="Static" onclick="CheckDateStatus("Custom","ddlcustRemindRenewal3","ddlcustRenewal60User1","ddlcustRenewal60User2","chkcust60");" /> </td><td><select id="ddlcustRemindRenewal3' + k + '" onchange="ddlDateStatus("Custom","ddlcustRemindRenewal3","ddlcustRenewal60User1","ddlcustRenewal60User2","chkcust60");" clientidmode="Static" style="width: 207px" class="chzn-select chzn-select"> <option value="0">-----No Reminder------</option> <option value="180">Remind one eighty days before</option><option value="90">Remind ninety days before</option><option value="60">Remind sixty days before</option><option value="30">Remind thirty days before</option><option value="7">Remind seven days before</option>           <option value="1">Remind one day before</option> </select> </td> <td><%-- User 1--%> <select id="ddlcustRenewal60User1' + k + '" onchange="SetUserValue("ddlcustRenewal60User1","ddlcustRemindRenewal3","ddlcustRenewal60User2");" clientidmode="Static" style="width: 150px" class="chzn-select chzn-select">            <option></option></select>&nbsp;<%-- User 2--%><select id="ddlcustRenewal60User2' + k + '" onchange="SetUserValue("ddlcustRenewal60User2","ddlcustRemindRenewal3","ddlcustRenewal60User1");"           clientidmode="Static" style="width: 150px" class="chzn-select chzn-select"><option></option></select></td>  </tr>   </table></fieldset></td></tr>';

                    $(CustomReminder).insertAfter($tr);
                    //return false;
                    k++;
                }
            });


            $.ajax({
                type: "POST",
                async: false,
                contentType: "application/json; charset=utf-8",
                url: "KeyFieldsMaster.aspx/LoadUsers",
                data: "{ContractID:'" + $('#hdnContractypeId').val() + "'}",
                dataType: "json",
                success: function (output) {
                    while (k >= 0) {
                        $('#ddlcustRenewal180User1' + k + '').empty().append('<option selected="selected" value="0">-----Select-----</option>');
                        $('#ddlcustRenewal180User2' + k + '').empty().append('<option selected="selected" value="0">-----Select-----</option>');
                        $('#ddlcustRenewal90User1' + k + '').empty().append('<option selected="selected" value="0">-----Select-----</option>');
                        $('#ddlcustRenewal90User2' + k + '').empty().append('<option selected="selected" value="0">-----Select-----</option>');
                        $('#ddlcustRenewal60User1' + k + '').empty().append('<option selected="selected" value="0">-----Select-----</option>');
                        $('#ddlcustRenewal60User2' + k + '').empty().append('<option selected="selected" value="0">-----Select-----</option>');
                        k--;
                    }
                    k = 0;
                    try {
                        while (k <= 2) {
                            $.each(output.d, function () {
                                $('#ddlcustRenewal180User1' + k + '').append($("<option></option>").val(this['Value']).html(this['Text']));
                                $('#ddlcustRenewal180User2' + k + '').append($("<option></option>").val(this['Value']).html(this['Text']));
                                $('#ddlcustRenewal90User1' + k + '').append($("<option></option>").val(this['Value']).html(this['Text']));
                                $('#ddlcustRenewal90User2' + k + '').append($("<option></option>").val(this['Value']).html(this['Text']));
                                $('#ddlcustRenewal60User1' + k + '').append($("<option></option>").val(this['Value']).html(this['Text']));
                                $('#ddlcustRenewal60User2' + k + '').append($("<option></option>").val(this['Value']).html(this['Text']));
                            });
                            k++;
                        }
                    } catch (Error) { }
                }
            });

            $("table tbody tr.trimportantDate").each(function (index, value) {

                $tr = $(value);
                if ($tr.find('label[for=time_entry_issue_id]').text() != undefined && $tr.find('label[for=time_entry_issue_id]').text() != '') {
                    if ($('#lblRemind0').text() == '') {
                        $('#lblRemind0').text($tr.find('label[for=time_entry_issue_id]').text());
                        $('#hdnReminderName1').val($tr.find('label[for=time_entry_issue_id]').text());
                    }
                    else if ($('#lblRemind1').text() == '') {
                        $('#lblRemind1').text($tr.find('label[for=time_entry_issue_id]').text());
                        $('#hdnReminderName2').val($tr.find('label[for=time_entry_issue_id]').text());
                    }
                    else if ($('#lblRemind2').text() == '') {
                        $('#lblRemind2').text($tr.find('label[for=time_entry_issue_id]').text());
                        $('#hdnReminderName3').val($tr.find('label[for=time_entry_issue_id]').text());
                    }
                }
            });


            if ($('#hdnRemindDay10').val() != "0") {
                $('#chkcust1800').prop("checked", true);
                $('#ddlcustRemindRenewal10').val($('#hdnRemindDay10').val());
                $('#ddlcustRenewal180User10').val($('#hdnRemindDay1User10').val());
                $('#ddlcustRenewal180User20').val($('#hdnRemindDay1User20').val());
            }
            if ($('#hdnRemindDay20').val() != "0") {
                $('#chkcust900').prop("checked", true);
                $('#ddlcustRemindRenewal20').val($('#hdnRemindDay20').val());
                $('#ddlcustRenewal90User10').val($('#hdnRemindDay2User10').val());
                $('#ddlcustRenewal90User20').val($('#hdnRemindDay2User20').val());
            }
            if ($('#hdnRemindDay30').val() != "0") {
                $('#chkcust600').prop("checked", true);
                $('#ddlcustRemindRenewal30').val($('#hdnRemindDay30').val());
                $('#ddlcustRenewal60User10').val($('#hdnRemindDay3User10').val());
                $('#ddlcustRenewal60User20').val($('#hdnRemindDay3User20').val());
            }

            if ($('#hdnRemindDay11').val() != "0") {
                $('#chkcust1801').prop("checked", true);
                $('#ddlcustRemindRenewal11').val($('#hdnRemindDay11').val());
                $('#ddlcustRenewal180User11').val($('#hdnRemindDay1User11').val());
                $('#ddlcustRenewal180User21').val($('#hdnRemindDay1User21').val());
            }
            if ($('#hdnRemindDay21').val() != "0") {
                $('#chkcust901').prop("checked", true);
                $('#ddlcustRemindRenewal21').val($('#hdnRemindDay21').val());
                $('#ddlcustRenewal90User11').val($('#hdnRemindDay2User11').val());
                $('#ddlcustRenewal90User21').val($('#hdnRemindDay2User21').val());
            }
            if ($('#hdnRemindDay31').val() != "0") {
                $('#chkcust601').prop("checked", true);
                $('#ddlcustRemindRenewal31').val($('#hdnRemindDay31').val());
                $('#ddlcustRenewal60User11').val($('#hdnRemindDay3User11').val());
                $('#ddlcustRenewal60User21').val($('#hdnRemindDay3User21').val());
            }

            if ($('#hdnRemindDay12').val() != "0") {
                $('#chkcust1802').prop("checked", true);
                $('#ddlcustRemindRenewal12').val($('#hdnRemindDay12').val());
                $('#ddlcustRenewal180User12').val($('#hdnRemindDay1User12').val());
                $('#ddlcustRenewal180User22').val($('#hdnRemindDay1User22').val());
            }
            if ($('#hdnRemindDay22').val() != "0") {
                $('#chkcust902').prop("checked", true);
                $('#ddlcustRemindRenewal22').val($('#hdnRemindDay22').val());
                $('#ddlcustRenewal90User12').val($('#hdnRemindDay2User12').val());
                $('#ddlcustRenewal90User22').val($('#hdnRemindDay2User22').val());
            }
            if ($('#hdnRemindDay32').val() != "0") {
                $('#chkcust602').prop("checked", true);
                $('#ddlcustRemindRenewal32').val($('#hdnRemindDay32').val());
                $('#ddlcustRenewal60User12').val($('#hdnRemindDay3User12').val());
                $('#ddlcustRenewal60User22').val($('#hdnRemindDay3User22').val());
            }
        });

        $("#btnGenerateContract").click(function (evt) {
            var flag = true;
            $("table.reminder tbody tr").each(function (index, value) {
                $tr = $(value);
                $tr.find('div.validation').remove();
                var selectUser = '<div class="validation">Please select atleast one user.</div>';
                var selectUserAnother = '<div class="validation">Both user names should not be same.</div>';
                var selectReminder = '<div class="validation">Please select reminder.</div>';
                var MultipleReminder = '<div class="validation" style="margin-left: 2px;">Please select different reminder.</div>';
                var chk = $tr.find('input:checkbox').prop("checked");
                var SelectVal = $tr.find('select:eq(0)').val();
                var val1 = $tr.find('select:eq(1)').val();
                var val2 = $tr.find('select:eq(2)').val();
                var chkID = $tr.find('input:checkbox').attr('id');
                var ExVal0 = $tr.find('select:eq(0)').val();

                //var ctlType = chkID.indexOf('exp') > -1 ? 'Expiration' : 'Renewal';
                var ctlType = chkID.indexOf('exp') > -1 ? 'Expiration' : 'Renewal';
                if (chkID.indexOf('exp') > -1)
                    ctlType = 'Expiration'; // : 'Renewal';
                else if (chkID.indexOf('rem') > -1)
                    ctlType = 'Renewal';
                else
                    ctlType = 'Custom';

                if (!chk && (val1 != "0" && val1 != "0")) {
                    $tr.find('input:checkbox').attr("checked", "checked");
                    chk = $tr.find('input:checkbox').prop("checked");
                }
                if (chk && (val1 != "0" && val1 != "0") && ctlType == "Expiration") {
                    if ($("#txtexpirationdate").val() == "") {
                        alert("Please insert expiration date.");
                        flag = false;
                        return false;
                    }
                }
                if (chk && (val1 != "0" && val1 != "0") && ctlType == "Renewal") {
                    if ($("#txtrenewaldate").val() == "") {
                        alert("Please insert renewal date.");
                        flag = false;
                        return false;
                    }
                }
                if (chk && SelectVal == "0") {
                    $(selectReminder).insertAfter($tr.find('select:eq(0)').next('div.chzn-container-single'));
                    flag = false;
                } else
                    if (chk && (val1 == "0" && val2 == "0")) {
                        $(selectUser).insertAfter($tr.find('select:eq(2)').next('div.chzn-container-single'));
                        flag = false;
                    }
                    else if (val1 == val2 && (val1 != "0" && val1 != "0")) {
                        $(selectUserAnother).insertAfter($tr.find('select:eq(2)').next('div.chzn-container-single'));
                        flag = false;
                    }
            });

            var MultipleRem = '<div class="validation" style="margin-left: 2px;">Please select different reminder.</div>';

            var cssflag = true;
            if (($('.cssExpire2').val() != "0" || $('.cssExpire1').val() != "0") && cssflag == true) {
                if ($('.cssExpire1').val() == $('.cssExpire2').val()) {
                    $(MultipleRem).insertAfter($('.cssExpire1').next('div.chzn-container-single'));
                    $(MultipleRem).insertAfter($('.cssExpire2').next('div.chzn-container-single'));
                    flag = false; cssflag = false; //return false;
                } else if (($('.cssExpire1').val() == $('.cssExpire3').val()) && $('.cssExpire1').val() != 0) {
                    $(MultipleRem).insertAfter($('.cssExpire1').next('div.chzn-container-single'));
                    $(MultipleRem).insertAfter($('.cssExpire3').next('div.chzn-container-single'));
                    flag = false; cssflag = false; //return false;
                }
            }
            if (($('.cssExpire2').val() != "0" || $('.cssExpire3').val() != "0") && cssflag == true) {
                if ($('.cssExpire1').val() == $('.cssExpire3').val()) {
                    $(MultipleRem).insertAfter($('.cssExpire1').next('div.chzn-container-single'));
                    $(MultipleRem).insertAfter($('.cssExpire3').next('div.chzn-container-single'));
                    flag = false; cssflag = false; //return false;
                } else
                    if ($('.cssExpire2').val() == $('.cssExpire3').val()) {
                        $(MultipleRem).insertAfter($('.cssExpire3').next('div.chzn-container-single'));
                        $(MultipleRem).insertAfter($('.cssExpire2').next('div.chzn-container-single'));
                        flag = false; cssflag = false; //return false;
                    }
            }
            if (($('.cssExpire3').val() != "0" || $('.cssExpire1').val() != "0") && cssflag == true) {
                if (($('.cssExpire1').val() == $('.cssExpire3').val()) && $('.cssExpire1').val() != 0) {
                    $(MultipleRem).insertAfter($('.cssExpire1').next('div.chzn-container-single'));
                    $(MultipleRem).insertAfter($('.cssExpire3').next('div.chzn-container-single'));
                    flag = false; cssflag = false; //return false;
                }
            }
            cssflag = true;
            if (($('.cssRemind1').val() != "0" || $('.cssRemind2').val() != "0") && cssflag == true) {
                if ($('.cssRemind1').val() == $('.cssRemind2').val()) {
                    $(MultipleRem).insertAfter($('.cssRemind1').next('div.chzn-container-single'));
                    $(MultipleRem).insertAfter($('.cssRemind2').next('div.chzn-container-single'));
                    flag = false; cssflag = false; //return false;
                } else if (($('.cssRemind1').val() == $('.cssRemind3').val()) && $('.cssRemind1').val() != 0) {
                    $(MultipleRem).insertAfter($('.cssRemind1').next('div.chzn-container-single'));
                    $(MultipleRem).insertAfter($('.cssRemind3').next('div.chzn-container-single'));
                    flag = false; cssflag = false; //return false;
                }
            }

            if (($('.cssRemind1').val() != "0" || $('.cssRemind3').val() != "0") && cssflag == true) {
                if (($('.cssRemind1').val() == $('.cssRemind3').val()) && $('.cssRemind1').val() != 0) {
                    $(MultipleRem).insertAfter($('.cssRemind1').next('div.chzn-container-single'));
                    $(MultipleRem).insertAfter($('.cssRemind3').next('div.chzn-container-single'));
                    flag = false; cssflag = false; //return false;
                }
            }

            if (($('.cssRemind2').val() != "0" || $('.cssRemind3').val() != "0") && cssflag == true) {
                if (($('.cssRemind1').val() == $('.cssRemind3').val()) && $('.cssRemind1').val() != 0) {
                    $(MultipleRem).insertAfter($('.cssRemind1').next('div.chzn-container-single'));
                    $(MultipleRem).insertAfter($('.cssRemind3').next('div.chzn-container-single'));
                    flag = false; cssflag = false; //return false;
                } else
                    if ($('.cssRemind2').val() == $('.cssRemind3').val()) {
                        $(MultipleRem).insertAfter($('.cssRemind2').next('div.chzn-container-single'));
                        $(MultipleRem).insertAfter($('.cssRemind3').next('div.chzn-container-single'));
                        flag = false; cssflag = false; //return false;
                    }
            }

            $('#hdnReminddate1').val(''); $('#hdnReminddate2').val(''); $('#hdnReminddate3').val('');
            $('#hdnRemindDay10').val('0'); $('#hdnRemindDay20').val('0'); $('#hdnRemindDay30').val('0');
            $('#hdnRemindDay11').val('0'); $('#hdnRemindDay21').val('0'); $('#hdnRemindDay31').val('0');
            $('#hdnRemindDay12').val('0'); $('#hdnRemindDay22').val('0'); $('#hdnRemindDay32').val('0');

            var cssCustflag = true;
            var kk = "0";
            var MultipleReminder = '<div class="validation" style="margin-left: -4px;">Please select different reminder.</div>';
            $("table tbody tr.trimportantDate").each(function (index, value) {
                $tr = $(value);
                $tr1 = $(index);

                var chk = $tr.find('input:checkbox').prop("checked");
                var RemindVal0 = $tr.find('select:eq(0)').val();
                var RemindVal1 = $tr.find('select:eq(3)').val();
                var RemindVal2 = $tr.find('select:eq(6)').val();
                var val1 = $tr.find('select:eq(1)').val();
                var val2 = $tr.find('select:eq(2)').val();
                var chkID = $tr.find('input:checkbox').attr('id');
                cssCustflag = true;
                if (RemindVal0 != 0 && RemindVal0 != undefined && cssCustflag == true) {
                    if (RemindVal0 == RemindVal1) {
                        $(MultipleReminder).insertAfter($tr.find('select:eq(0)').next('div.chzn-container-single'));
                        $(MultipleReminder).insertAfter($tr.find('select:eq(3)').next('div.chzn-container-single'));
                        flag = false; cssCustflag = false;
                    }
                    if (RemindVal0 == RemindVal2) {
                        $(MultipleReminder).insertAfter($tr.find('select:eq(0)').next('div.chzn-container-single'));
                        $(MultipleReminder).insertAfter($tr.find('select:eq(6)').next('div.chzn-container-single'));
                        flag = false; cssCustflag = false;
                    }
                }

                if (RemindVal1 != 0 && RemindVal1 != undefined && cssCustflag == true) {
                    if (RemindVal1 == RemindVal2) {
                        $(MultipleReminder).insertAfter($tr.find('select:eq(3)').next('div.chzn-container-single'));
                        $(MultipleReminder).insertAfter($tr.find('select:eq(6)').next('div.chzn-container-single'));
                        flag = false; cssCustflag = false;
                    }
                    if (RemindVal1 == RemindVal0) {
                        $(MultipleReminder).insertAfter($tr.find('select:eq(0)').next('div.chzn-container-single'));
                        $(MultipleReminder).insertAfter($tr.find('select:eq(3)').next('div.chzn-container-single'));
                        flag = false; cssCustflag = false;
                    }
                }

                if (RemindVal2 != 0 && RemindVal2 != undefined && cssCustflag == true) {
                    if (RemindVal1 == RemindVal2) {
                        $(MultipleReminder).insertAfter($tr.find('select:eq(3)').next('div.chzn-container-single'));
                        $(MultipleReminder).insertAfter($tr.find('select:eq(6)').next('div.chzn-container-single'));
                        flag = false; cssCustflag = false;
                    }
                    if (RemindVal2 == RemindVal0) {
                        $(MultipleReminder).insertAfter($tr.find('select:eq(0)').next('div.chzn-container-single'));
                        $(MultipleReminder).insertAfter($tr.find('select:eq(6)').next('div.chzn-container-single'));
                        flag = false; cssCustflag = false;
                    }
                }

                if ($tr.find('.datepicker').val() != undefined) {
                    if ($tr.find('.datepicker').val() != '' && kk == 0)
                        $('#hdnReminddate1').val($tr.find('.datepicker').val());

                    if ($tr.find('.datepicker').val() != '' && kk == 1)
                        $('#hdnReminddate2').val($tr.find('.datepicker').val());

                    if ($tr.find('.datepicker').val() != '' && kk == 2)
                        $('#hdnReminddate3').val($tr.find('.datepicker').val());

                    kk++;
                }
            });
            kk = 0;

            k = 0;
            while (k <= 2) {
                $("table tbody tr.trimportantDate").each(function (index, value) {
                    $tr = $(value);
                    if ($tr.find('#chkcust180' + k + '').prop("checked")) {

                        if (k == 0) {
                            $('#hdnRemindDay10').val($tr.find('#ddlcustRemindRenewal1' + k + '').val());
                        }

                        if (k == 1) {
                            $('#hdnRemindDay11').val($tr.find('#ddlcustRemindRenewal1' + k + '').val());
                        }

                        if (k == 2) {
                            $('#hdnRemindDay12').val($tr.find('#ddlcustRemindRenewal1' + k + '').val());
                        }

                        /* User record for first date reminder  */
                        if (k == 0)
                            $('#hdnRemindDay1User10').val($tr.find('#ddlcustRenewal180User1' + k + '').val());

                        if (k == 0)
                            $('#hdnRemindDay1User20').val($tr.find('#ddlcustRenewal180User2' + k + '').val());

                        /* User record for second date reminder  */
                        if (k == 1)
                            $('#hdnRemindDay1User11').val($tr.find('#ddlcustRenewal180User1' + k + '').val());

                        if (k == 1)
                            $('#hdnRemindDay1User21').val($tr.find('#ddlcustRenewal180User2' + k + '').val());

                        /* User record for third date reminder  */
                        if (k == 2)
                            $('#hdnRemindDay1User12').val($tr.find('#ddlcustRenewal180User1' + k + '').val());

                        if (k == 2)
                            $('#hdnRemindDay1User22').val($tr.find('#ddlcustRenewal180User2' + k + '').val());

                    }
                    if ($tr.find('#chkcust90' + k + '').prop("checked")) {

                        if (k == 0) {
                            $('#hdnRemindDay20').val($tr.find('#ddlcustRemindRenewal2' + k + '').val());
                        }

                        if (k == 1) {
                            $('#hdnRemindDay21').val($tr.find('#ddlcustRemindRenewal2' + k + '').val());
                        }

                        if (k == 2) {
                            $('#hdnRemindDay22').val($tr.find('#ddlcustRemindRenewal2' + k + '').val());
                        }

                        /* User record for first date reminder  */
                        if (k == 0)
                            $('#hdnRemindDay2User10').val($tr.find('#ddlcustRenewal90User1' + k + '').val());

                        if (k == 0)
                            $('#hdnRemindDay2User20').val($tr.find('#ddlcustRenewal90User2' + k + '').val());

                        /* User record for second date reminder  */
                        if (k == 1)
                            $('#hdnRemindDay2User11').val($tr.find('#ddlcustRenewal90User1' + k + '').val());

                        if (k == 1)
                            $('#hdnRemindDay2User21').val($tr.find('#ddlcustRenewal90User2' + k + '').val());

                        /* User record for third date reminder  */
                        if (k == 2)
                            $('#hdnRemindDay2User12').val($tr.find('#ddlcustRenewal90User1' + k + '').val());

                        if (k == 2)
                            $('#hdnRemindDay2User22').val($tr.find('#ddlcustRenewal90User2' + k + '').val());



                    }
                    if ($tr.find('#chkcust60' + k + '').prop("checked")) {

                        if (k == 0) {
                            $('#hdnRemindDay30').val($tr.find('#ddlcustRemindRenewal3' + k + '').val());
                        }

                        if (k == 1) {
                            $('#hdnRemindDay31').val($tr.find('#ddlcustRemindRenewal3' + k + '').val());
                        }

                        if (k == 2) {
                            $('#hdnRemindDay32').val($tr.find('#ddlcustRemindRenewal3' + k + '').val());
                        }

                        /* User record for first date reminder  */
                        if (k == 0)
                            $('#hdnRemindDay3User10').val($tr.find('#ddlcustRenewal60User1' + k + '').val());

                        if (k == 0)
                            $('#hdnRemindDay3User20').val($tr.find('#ddlcustRenewal60User2' + k + '').val());

                        /* User record for second date reminder  */
                        if (k == 1)
                            $('#hdnRemindDay3User11').val($tr.find('#ddlcustRenewal60User1' + k + '').val());

                        if (k == 1)
                            $('#hdnRemindDay3User21').val($tr.find('#ddlcustRenewal60User2' + k + '').val());

                        /* User record for third date reminder  */
                        if (k == 2)
                            $('#hdnRemindDay3User12').val($tr.find('#ddlcustRenewal60User1' + k + '').val());

                        if (k == 2)
                            $('#hdnRemindDay3User22').val($tr.find('#ddlcustRenewal60User2' + k + '').val());

                    }
                });
                k++;
            }
            return flag;
        });

        //        });  
    </script>
    <%--Script for binding dropdowns from master table [Added by Bharati]--%>
    <script type="text/javascript">
        var search = '';
        $(document).ready(function () {
            $('select').each(function () {
                var MasterTable = $(this).attr('master');
                if (MasterTable != undefined && MasterTable != '') {
                    BindMasters(MasterTable, $(this), '');
                    var selected = $(this).closest('tr').find("input[type='hidden']").val();
                    $(this).val(selected);
                }
            });

            $('#ImportantDateTable').css('width', '101%');

        });

        $(window).load(function () {
            $('input[value="Select Some Options"]').css("width", "150px");
            $("select[master!='']").next('div').find('.chzn-search').bind('keyup', function (e) {
                var obj = $(this).closest('tr').find('select');
                delay(function () {
                    keyUpSearch(obj, e.keyCode);
                }, 1000);

            });
        });
        var delay = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();

        function keyUpSearch(obj, key) {
            if ($('.no-results').text() != '') {
                var search = $('.no-results').find('span').html();
                if (search.length > 2) {
                    var MasterTable = $(obj).attr('master');
                    if (search == undefined)
                        search = '';

                    if (key != 8) {
                        BindMasters(MasterTable, obj, search);
                    }
                }
            }
        }

        function BindMasters(MasterTable, obj, search) {
            //var MasterTable = "MstCity";       
            var strType = "POST";
            var strContentType = "text/html; charset=utf-8";
            var strData = "{}";
            var strCatch = false;
            var strDataType = 'json';
            var strAsync = false;
            var MasterList;
            var type = 'FillMaster';
            var index = 0;
            var parentId = 0;
            var strURL = '../Handlers/GetQuestionnaireDropDownHandler.ashx?Type=' + type + "&MasterName=" + MasterTable + "&parentId=" + parentId + '&Search=' + search + '&RequestId=0';
            objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
            MasterList = objHandler.HandlerReturnedData;

            if (MasterList.length > 0) {
                bindSelect(MasterList, '#' + $(obj).attr('id'));
                if (search != '') {
                    $(obj).closest('tr').find('.chzn-search').find('input').val(search);
                }
            }
        }

        function HideShowPReason() {
            $("#txtpriorityremark").hide();
            $("#lblPriorityreason").hide();
            var ID = $("#ddlPrority").val();
            if (ID == "1") {
                $("#lblPriorityreason").show();
                $("#txtpriorityremark").show();
            }
        }
    </script>
</asp:Content>
