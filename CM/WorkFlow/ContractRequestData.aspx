﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/CM.master"
    AutoEventWireup="true" CodeFile="ContractRequestData.aspx.cs" Inherits="Masters_ContractRequestData" %>

<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridSelect.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hdnRequestId" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnRequestNo" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <input id="hdnPrimeIds" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnContractTypeId" runat="server" clientidmode="Static" type="hidden" />
    <script type="text/javascript">
        function ChangeContractType(obj) {
            $('#hdnContractTypeId').val($(obj).val());
        }
        $("#requestLink").addClass("menulink");
        $("#requesttab").addClass("selectedtab");
      
    </script>
    <%--<h2>Contract Request</h2>--%>
    <h2>
        My Contracts</h2>
    <div style="margin: 0; padding: 0; display: inline">
        <div id="query_form_content" class="hide-when-print">
            <fieldset id="filters" class="collapsible">
                <legend onclick="toggleFieldset(this);">Filters</legend>
                <div style="">
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td>
                                    <table id="filters-table" width="100%" cellpadding="3" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td width="10%">
                                                    Contract ID :
                                                </td>
                                                <td align="left">
                                                    <input id="txtContractId" class="Pincode" clientidmode="Static" runat="server" type="text"
                                                        maxlength="50" />
                                                </td>
                                                <td width="20%">
                                                    Customer/Supplier/Others Name :
                                                </td>
                                                <td align="left">
                                                    <input id="TxtClientName" clientidmode="Static" runat="server" type="text" maxlength="50" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="10%" style="padding-top: 6px">
                                                    Request Type :
                                                </td>
                                                <td align="left" style="padding-top: 6px" colspan="3">
                                                    <select id="ddlContractType" clientidmode="Static" onchange="ChangeContractType(this)"
                                                        runat="server">
                                                        <option></option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </fieldset>
        </div>
        <p class="buttons hide-when-print">
            <asp:LinkButton ID="btnSearch" runat="server" OnClientClick="return searchClick();"
                CssClass="icon icon-checked">Filter</asp:LinkButton>
            <asp:LinkButton ID="btnShowAll" ClientIDMode="Static" runat="server" OnClientClick="return resetClick();"
                CssClass="icon icon-reload" OnClick="btnSearch_Click">Reset Filter</asp:LinkButton>
            <asp:LinkButton ID="btnDelete" CssClass="icon icon-del" runat="server" OnClientClick="return GetSelectedItems('D');"
                OnClick="btnDelete_Click" Text="Add new country">Delete</asp:LinkButton>
        </p>
        <div class="autoscroll">
            <table width="100%">
                <asp:Repeater ID="rptContractRequestDetails" runat="server" OnItemDataBound="rptContractRequestDetails_ItemDataBound">
                    <HeaderTemplate>
                        <table id="masterDataTable" class="masterTable list issues" width="100%">
                            <thead>
                                <tr>
                                    <th style="text-align: left; padding-left: 2px;" width="1%" runat="server" clientidmode="Static"
                                        id="headCheckBoxId">
                                        <input id="chkSelectAll" type="checkbox" title="Check all/Uncheck all" class="maincheckbox" />
                                    </th>
                                    <%-- <th class="checkbox hide-when-print" width="5%" runat="server" clientidmode="Static" id="headUsedImageId">
                                        &nbsp;
                                    </th>--%>
                                    <th width="3%" style="display: none;">
                                    </th>
                                    <th runat="server" clientidmode="Static" id="headEditLinkId" style="display: none">
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="btnSortRequestID" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Request ID</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="LinkButton6" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Contract ID</asp:LinkButton>
                                    </th>
                                    <th width="12%">
                                        <asp:LinkButton ID="btnSortRequestType" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Request Type</asp:LinkButton>
                                    </th>
                                    <th width="25%">
                                        <asp:LinkButton ID="LinkButton1" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Contract Type</asp:LinkButton>
                                    </th>
                                    <th width="10%" style="display: none;">
                                        Request summary
                                    </th>
                                    <th width="18%">
                                        <asp:LinkButton ID="LinkButton2" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Customer/Supplier/Others name</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="LinkButton3" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Requester name</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="LinkButton4" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Contract Value</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="LinkButton5" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Assigned User</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="LinkButton7" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Status</asp:LinkButton>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: left; padding-bottom: 2px" runat="server" clientidmode="Static"
                                id="rowCheckBoxId" class="tdCheckBox">
                                <input type="checkbox" onchange="SetMainCheckbox(this);" class="mid-margin-left" />
                            </td>
                            <%-- <td style="text-align: left; padding-bottom: 2px" runat="server" clientidmode="Static" id="rowUsedImageId">
                                <img id="imgLock" alt="img.." class="mws-tooltip-e" title="Used" border="0" />
                            </td>--%>
                            <td style="display: none;">
                                <asp:Label ID="lblRequestId" runat="server" ClientIDMode="Static" Text='<%#Eval("RequestId") %>'></asp:Label>
                                <%--<asp:Label ID="lblIsUsed" runat="server" ClientIDMode="Static" Text='N'></asp:Label>--%>
                                <asp:Label ID="lblIsUsed" runat="server" ClientIDMode="Static" Text='<%#Eval("IsUsed") %>'></asp:Label>
                                <asp:Label ID="lblRequestTypeName" runat="server" ClientIDMode="Static" Text='<%#Eval("RequestTypeName") %>'></asp:Label>
                                <asp:Label ID="lblContractIds" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractID") %>'></asp:Label>
                            </td>
                            <td runat="server" clientidmode="Static" id="rowEditLinkId" style="display: none">
                                <asp:LinkButton ID="linkEdit" runat="server" OnClientClick="return setSelectedId(this);"
                                    OnClick="linkEdit_Click">Edit</asp:LinkButton>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblRequestNo" runat="server" ClientIDMode="Static" Text='<%#Eval("RequestId") %>'></asp:Label>
                                <a id="aEditRequest" clientidmode="Static" href='<%# "ContractRequest.aspx?RequestId="+Eval("RequestId") %>'
                                    title="Edit Request" runat="server" style="font-weight: normal;">
                                    <%#Eval("RequestId") %><span><img alt="Edit Request" src="../images/edit.png?1349001717" /></span>                                    
                                </a>
                               
                                <asp:Label ID="lblIsDocumentGenerated" runat="server" Visible="false" ClientIDMode="Static"
                                    Text='<%#Eval("IsDocumentGenerated") %>'></asp:Label>
                                <asp:Label ID="lblIsBulkImport" runat="server" Visible="false" ClientIDMode="Static"
                                    Text='<%#Eval("IsBulkImport") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblContractID" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractID") %>'></asp:Label>
                                <a id="a1" clientidmode="Static" href='<% # "ContractRelations.aspx?RequestId="+Eval("RequestId") +"&ContractId="+ Eval("ContractID")%>'
                                     visible='<%#Eval("IsTreeLinkVisible") %>' runat="server" style="font-weight: normal;">
                                    <span><img src="../images/chart_organisation.png" /></span>
                                </a>
                            </td>
                            <td>
                                <asp:LinkButton ID="imgEdit" runat="server" OnClientClick="return setSelectedId(this);"
                                    OnClick="imgEdit_Click" Style="display: none;"><%#Eval("RequestTypeName")%></asp:LinkButton>
                                <asp:Label ID="lblRequestTypeNameLink" Visible="true" runat="server" ClientIDMode="Static"
                                    Text='<%#Eval("RequestTypeName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblContractTypeName" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTypeName") %>'></asp:Label>
                            </td>
                            <td style="display: none;">
                                <asp:Label ID="lblRequestDescription" runat="server" ClientIDMode="Static" Text='<%#Eval("Description") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblClientName" runat="server" ClientIDMode="Static" Text='<%#Eval("ClientName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFullNameRequester" runat="server" ClientIDMode="Static" Text='<%#Eval("RequesterUserName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblDeadlineDate" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractValuewithcurrency") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFullNameAssigneduser" runat="server" ClientIDMode="Static" Text='<%#Eval("AssignerUserName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblContractStatus" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractStatus") %>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
            <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
        </div>
        <table style="display: none" width="100%">
            <tr>
                <td align="left" width="10%">
                </td>
                <td runat="server" id="actinacttd" style="padding-right: 60%" class="labelfont" align="left"
                    width="90%">
                    <input id="rdActive" name="ActiveInactive" runat="server" type="radio" />Active
                    <input id="rdInactive" name="ActiveInactive" runat="server" type="radio" />Inactive
                    <asp:Button ID="btnChangeStatus" runat="server" Text="Status" OnClientClick="return GetSelectedItems('S');"
                        OnClick="btnChangeStatus_Click" />
                </td>
            </tr>
        </table>
        <input id="hdnRecordsPerPage1" runat="server" clientidmode="Static" type="hidden" />
    </div>
    <script type="text/javascript">
        function resetClick() {
            $('#hdnContractTypeId').val('0');
            $('#hdnSearch').val('');
            $('#TxtClientName').val('');
            $('#txtContractId').val('');
            return true;
        }

        function searchClientnameClick() {

            var rValue = true;

            try {
                var search = $('#TxtClientName').val();
                if (search.length < 3) {
                    alert('Miminum 3 characters must be entered.');
                    rValue = false;
                }
                $('#hdnSearch').val('1');
                paginationDefault();
            } catch (e) {
                $('#hdnSearch').val('');
                rValue = false;
            }
            return rValue;
        }
        
        function GetSelectedItems(flg) {
            $("#hdnRecordsPerPage1").val($("#hdnRecordsPerPage").val());
            $('#hdnPrimeIds').val('');
            $('#hdnUsedNames').val('');
            var tableClass = 'masterTable';
            var deleteLabelId = 'lblRequestId';
            //var deleteLabelName = 'lblRequestTypeName';
            var deleteLabelName = 'lblContractIds'; 
            var objCheck = new CkeckBoxSelect(tableClass, deleteLabelId, deleteLabelName);
            var deletedIds = objCheck.DeletedIds;
            var usedNames = objCheck.UsedNames;

            if (deletedIds == '') {
                MessageMasterDiv('Please select record(s).');
                return false;
            }
            else if (usedNames != '' && flg == 'D') {
                MessageMasterDiv('Some records can not be deleted. Contract Id - ' + usedNames, 1);
                return false;
            }
            if (flg == 'D') {

                if (DeleteConfrim() == true) {
                    $('#hdnRequestId').val(deletedIds);
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                $('#hdnRequestId').val(deletedIds);
                return true;
            }
            $('#hdnRequestId').val(deletedIds);

            return true;
        }



        $(document).ready(function () {
            defaultTableValueSetting('lblRequestId', 'ContractRequest', 'RequestId');
            // LockUnLockImage('masterTable');
            //            var uri = window.location.toString();
            //            if (uri.indexOf("?") > 0) {
            //                var clean_uri = uri.substring(0, uri.indexOf("?"));
            //                window.history.replaceState({}, document.title, clean_uri);
            //                window.history.clear();
            //            }

        });




        function setSelectedId(obj) {
            var pId = $(obj).closest('tr').find('#lblRequestId').text();
            var RNo = $(obj).closest('tr').find('#lblRequestNo').text();
            if (pId == '' || pId == '0') {
                return false;
            }
            else {
                $('#hdnRequestId').val(pId);
                $('#hdnRequestNo').val(RNo);

                return true;
            }
        }


    </script>
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
    <%--Added by Bharati 05Aug2014--%>
    <script type="text/javascript">
        var ContractAssemblyAccess = '<%= ViewState["RequestTypeClickAccess"] %>';

        $(window).load(function () {
            $("#masterDataTable tbody tr").css("cursor", "pointer");
            if (ContractAssemblyAccess == 'True') {
                $("#masterDataTable tbody tr").css("cursor", "pointer");
            }
            else
                $("#masterDataTable tbody tr").css("cursor", "default");
        });
          
        $("#masterDataTable tbody tr td:not(.tdCheckBox)").click(function (index, TD) {
            if (ContractAssemblyAccess == 'True') {
                location.href = "RequestFlow.aspx?Status=Workflow&RequestId=" + $(this).parent('tr').find('#lblRequestId').text();
            }

        });
    </script>
    <%--Changes by Bharati completed here--%>
</asp:Content>
