﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="ContractRelationAdd.aspx.cs" Inherits="WorkFlow_ContractRelationAdd" %>

<%@ Register Assembly="Keyoti4.SearchEnginePro.Web" Namespace="Keyoti.SearchEngine.Web"
    TagPrefix="SearchEngine" %>
<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridSelect.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $("#requestLink").addClass("menulink");
        $("#requesttab").addClass("selectedtab");
        //document.body.style.overflow = 'hidden';
    </script>
    <h2><asp:Label ID="lblTitle" runat="server" Text=""></asp:Label></h2>
    <h3>
        <asp:Label ID="lblContractIDHeader" runat="server" Text="" ClientIDMode="Static"></asp:Label>
    </h3>

    <asp:HiddenField ID="hdnContractId" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnRelationshipId" ClientIDMode="Static" runat="server" />

    <div id="dvMesssage" class="flash error" style="display: none">
    </div>
    <div>
        <table style="width: 100%; table-layout: fixed">
            <tr>
                <th style="width: 30%">
                </th>
                <th style="width: 70%">
                </th>
            </tr>
            <tr>
                <td class="tdAlign" style="vertical-align: top; padding-top: 5px; text-align: right">
                    <label for="time_entry_issue_id">
                        <span class="required">*</span>Contract Id
                    </label>
                </td>
                <td>
                    <select id="ddlContracts" clientidmode="Static" onchange="SetDDlValues();" runat="server"
                        disabled="true" style="width: 31%;" class="chzn-select required chzn-select AF ">
                        <option></option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="tdAlign" style="vertical-align: top; padding-top: 5px; text-align: right">
                    <label for="time_entry_issue_id">
                        <span class="required">*</span>Parent of
                    </label>
                </td>
                <td>
                    <select id="ddlContractParentOf" clientidmode="Static" onchange="SetDDlValues();"
                        runat="server" style="width: 31%;" class="chzn-select required chzn-select AF ">
                        <option></option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="tdAlign" style="vertical-align: top; padding-top: 5px; text-align: right">
                    <label for="time_entry_issue_id">
                        <span class="required">*</span>Child of
                    </label>
                </td>
                <td>
                    <select id="ddlContractChildOf" clientidmode="Static" onchange="SetDDlValues();"
                        runat="server" style="width: 31%;" class="chzn-select required chzn-select AF">
                        <option></option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="tdAlign" style="vertical-align: top; padding-top: 5px; text-align: right">
                    <label for="time_entry_issue_id">
                        Description
                    </label>
                </td>
                <td>
                    <textarea id="txtDescription" cols="20" rows="2" runat="server" clientidmode="Static" maxlength="100" class="TxtNumSplCharValidation" ></textarea>
                </td>
            </tr>
        </table>
    </div>
    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" class="btn_validate" />
    <%-- <asp:LinkButton ID="btnDelete" CssClass="icon icon-del" runat="server" OnClientClick="return GetSelectedItems();"
                OnClick="btnDelete_Click" ></asp:LinkButton>--%>
     <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_Click"  OnClientClick="return GetSelectedItems();" />
    <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back_Click" />
    <input id="hdnIsUsed" runat="server" clientidmode="Static" type="hidden" value="N" />
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
        function GetSelectedItems() {
            if ($('#hdnIsUsed').val() == 'Y') {

                MessageMasterDiv('Records can not be deleted.', 1);
                return false;
            } else {
                return confirm("Do you want to delete?");
            }

        }

       
    </script>
</asp:Content>
