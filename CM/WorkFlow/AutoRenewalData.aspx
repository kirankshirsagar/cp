﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/CM.master"
    AutoEventWireup="true" CodeFile="AutoRenewalData.aspx.cs" Inherits="WorkFlow_AutoRenewalDataData" %>

<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridSelect.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hdnRequestId" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnRequestNo" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <input id="hdnPrimeIds" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnContractTypeId" runat="server" clientidmode="Static" type="hidden" />
    <script type="text/javascript">
        function ChangeContractType(obj) {
            $('#hdnContractTypeId').val($(obj).val());
        }
        $("#requestLink").addClass("menulink");
        $("#requesttab").addClass("selectedtab");
    </script>
    <h2>
        Auto Renewal</h2>
    <div style="margin: 0; padding: 0; display: inline">
        <div id="query_form_content" class="hide-when-print">
            <fieldset id="filters" class="collapsible">
                <legend onclick="toggleFieldset(this);">Filters</legend>
                <div style="">
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td>
                                    <table id="filters-table" width="100%" cellpadding="3" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td width="10%">
                                                    Contract ID :
                                                </td>
                                                <td align="left">
                                                    <input id="txtContractId" class="Pincode" clientidmode="Static" runat="server" type="text"
                                                        maxlength="50" onkeydown = "return (event.keyCode!=13);" />
                                                </td>
                                                <td width="10%">
                                                    Customer/Supplier Name :
                                                </td>
                                                <td align="left">
                                                    <input id="TxtClientName" clientidmode="Static" runat="server" type="text" maxlength="50" onkeydown = "return (event.keyCode!=13);" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="10%" style="padding-top: 6px">
                                                    Request Type :
                                                </td>
                                                <td align="left" style="padding-top: 6px" colspan="3">
                                                    <select id="ddlContractType" clientidmode="Static" onchange="ChangeContractType(this)"
                                                        runat="server">
                                                        <option></option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </fieldset>
        </div>
        <p class="buttons hide-when-print">
            <asp:LinkButton ID="btnSearch" runat="server" OnClientClick="return searchClick();"
                CssClass="icon icon-checked">Filter</asp:LinkButton>
            <asp:LinkButton ID="btnShowAll" runat="server" OnClientClick="return resetClick();"
                CssClass="icon icon-reload" OnClick="btnSearch_Click">Reset Filter</asp:LinkButton>
            <%--       <asp:LinkButton ID="btnAddRecord" CssClass="icon icon-add" runat="server" OnClick="btnAddRecord_Click">Add new request</asp:LinkButton>--%>
            <asp:LinkButton ID="btnDelete" CssClass="icon icon-del" runat="server" OnClientClick="return GetSelectedItems('D');"
                OnClick="btnDelete_Click" Text="Add new country">Delete</asp:LinkButton>
        </p>
        <div class="autoscroll">
            <table width="100%">
                <asp:Repeater ID="rptContractRequestDetails" runat="server" OnItemDataBound="rptContractRequestDetails_ItemDataBound">
                    <HeaderTemplate>
                        <table id="masterDataTable" class="masterTable list issues" width="100%">
                            <thead>
                                <tr>
                                    <th style="text-align: left; padding-left: 2px;" width="1%" runat="server" clientidmode="Static"
                                        id="headCheckBoxId">
                                        <input id="chkSelectAll" type="checkbox" title="Check all/Uncheck all" class="maincheckbox" />
                                    </th>
                                    <th class="checkbox hide-when-print" width="5%" runat="server" clientidmode="Static" id="headUsedImageId">
                                        &nbsp;
                                    </th>
                                    <th width="3%" style="display: none;">
                                        Request Id
                                    </th>
                                    <th runat="server" clientidmode="Static" id="headEditLinkId">
                                    </th>
                                    <th width="10%">
                                        Request Id
                                    </th>
                                    <th width="10%">
                                        Contract ID
                                    </th>
                                    <th width="12%">
                                        Request Type
                                    </th>
                                    <th width="25%">
                                        Contract Type
                                    </th>
                                    <th width="10%" style="display: none;">
                                        Request summary
                                    </th>
                                    <th width="18%">
                                        Customer/Supplier name
                                    </th>
                                    <th width="10%">
                                        Requester name
                                    </th>
                                    <th width="10%">
                                        Deadline date
                                    </th>
                                    <th width="10%">
                                        Assigned User
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: left; padding-bottom: 2px" runat="server" clientidmode="Static"
                                id="rowCheckBoxId">
                                <input type="checkbox" onchange="SetMainCheckbox(this);" class="mid-margin-left" />
                            </td>
                            <td style="text-align: left; padding-bottom: 2px" runat="server" clientidmode="Static" id="rowUsedImageId">
                                <img id="imgLock" alt="img.." class="mws-tooltip-e" title="Used" border="0" />
                            </td>
                            <td style="display: none;">
                                <asp:Label ID="lblRequestId" runat="server" ClientIDMode="Static" Text='<%#Eval("RequestId") %>'></asp:Label>
                                <asp:Label ID="lblIsUsed" runat="server" ClientIDMode="Static" Text='<%#Eval("isUsed") %>'></asp:Label>
                                <asp:Label ID="lblRequestTypeName" runat="server" ClientIDMode="Static" Text='<%#Eval("RequestTypeName") %>'></asp:Label>
                            </td>
                            <td runat="server" clientidmode="Static" id="rowEditLinkId">
                                <asp:LinkButton ID="linkEdit" runat="server" OnClientClick="return setSelectedId(this);"
                                    OnClick="linkEdit_Click">Renewal</asp:LinkButton>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblRequestNo" runat="server" ClientIDMode="Static" Text='<%#Eval("RequestId") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblContractID" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractID") %>'></asp:Label>
                            </td>
                            <td>
                               <%-- <asp:LinkButton ID="imgEdit" runat="server" OnClientClick="return setSelectedId(this);"
                                    OnClick="imgEdit_Click"><%#Eval("RequestTypeName")%></asp:LinkButton>--%>
                                 <asp:Label ID="lblRequestTypeNameLink" Visible="true" runat="server" ClientIDMode="Static" Text='<%#Eval("RequestTypeName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblContractTypeName" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTypeName") %>'></asp:Label>
                            </td>
                            <td style="display: none;">
                                <asp:Label ID="lblRequestDescription" runat="server" ClientIDMode="Static" Text='<%#Eval("Description") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblClientName" runat="server" ClientIDMode="Static" Text='<%#Eval("ClientName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFullNameRequester" runat="server" ClientIDMode="Static" Text='<%#Eval("RequesterUserName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblDeadlineDate" runat="server" ClientIDMode="Static" Text='<%#Eval("DeadlineDateStr") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFullNameAssigneduser" runat="server" ClientIDMode="Static" Text='<%#Eval("AssignerUserName") %>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
            <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
        </div>
        <table style="display: none" width="100%">
            <tr>
                <td align="left" width="10%">
                </td>
                <td runat="server" id="actinacttd" style="padding-right: 60%" class="labelfont" align="left"
                    width="90%">
                    <input id="rdActive" name="ActiveInactive" runat="server" type="radio" />Active
                    <input id="rdInactive" name="ActiveInactive" runat="server" type="radio" />Inactive
                    <asp:Button ID="btnChangeStatus" runat="server" Text="Status" OnClientClick="return GetSelectedItems('S');"
                        OnClick="btnChangeStatus_Click" />
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">

        function resetClick() 
        {
            $('#hdnContractTypeId').val('0');
            $('#txtContractId').val('');
            $('#TxtClientName').val('');
            return true;
        }

        function searchClientnameClick() {

            var rValue = true;

            try {
                var search = $('#TxtClientName').val();
                if (search.length < 3) {
                    alert('Miminum 3 characters must be entered.');
                    rValue = false;
                }
                $('#hdnSearch').val('1');
                paginationDefault();
            } catch (e) {
                $('#hdnSearch').val('');
                rValue = false;
            }
            return rValue;
        }




        function GetSelectedItems(flg) {
            $('#hdnPrimeIds').val('');
            $('#hdnUsedNames').val('');
            var tableClass = 'masterTable';
            var deleteLabelId = 'lblRequestId';
            var deleteLabelName = 'lblRequestTypeName';
            var objCheck = new CkeckBoxSelect(tableClass, deleteLabelId, deleteLabelName);
            var deletedIds = objCheck.DeletedIds;
            var usedNames = objCheck.UsedNames;

            if (deletedIds == '') {
                MessageMasterDiv('Please select record(s).');
                return false;
            }
            else if (usedNames != '' && flg == 'D') {
                MessageMasterDiv('Some records can not be deleted.' + usedNames, 1);
                return false;
            }
            if (flg == 'D') {
                if (DeleteConfrim() == true) {
                    $('#hdnRequestId').val(deletedIds);
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                $('#hdnRequestId').val(deletedIds);
                return true;
            }
            $('#hdnRequestId').val(deletedIds);
            return true;
        }



        $(document).ready(function () {
            //            alert($('#ddlContractType').val());
            defaultTableValueSetting('lblRequestId', 'ContractRequest', 'RequestId');
            LockUnLockImage('masterTable');
        });




        function setSelectedId(obj) {
            var pId = $(obj).closest('tr').find('#lblRequestId').text();
            var RNo = $(obj).closest('tr').find('#lblRequestNo').text();
            if (pId == '' || pId == '0') {
                return false;
            }
            else {
                $('#hdnRequestId').val(pId);
                $('#hdnRequestNo').val(RNo);

                return true;
            }
        }


    </script>
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script language="javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
</asp:Content>
