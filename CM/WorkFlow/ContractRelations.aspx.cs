﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using WorkflowBLL;
using System.Web.Script.Serialization;

public partial class WorkFlow_ContractRelations : System.Web.UI.Page
{
    IContractRelations objContractRelations;
    public string Add;
    public string View;
    public string Update;
    public string Delete;
    public string CRData = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CreateObjects();
            if (Session[Declarations.User] != null && !IsPostBack)
            {
                Access.PageAccess(this, Session[Declarations.User].ToString(), "WorkFlow_");
                ViewState[Declarations.Add] = Access.Add.Trim();
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
            }
            ViewState["RequestId"] = Request.QueryString["RequestId"];
            hdnContractId.Value = Request.QueryString["ContractId"];
            lblContractIDHeader.Text = "Contract #" + hdnContractId.Value;
            if (ViewState[Declarations.Update].ToString() == "N" && ViewState[Declarations.Delete].ToString() == "N")
            {
                hdnHasEditDeleteAccess.Value = "N";
            }
            else
            {
                hdnHasEditDeleteAccess.Value = "Y";
            }
            Message();
            ReadData();
            AccessVisibility();
        }
    }

    protected void btnAddRecord_Click(object sender, EventArgs e)
    {
        Server.Transfer("ContractRelationAdd.aspx");
    }

    void CreateObjects()
    {
        Page.TraceWrite("ContractRelations: Page object create starts.");
        try
        {
            objContractRelations = FactoryWorkflow.GetContractRelations();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ContractRelations: create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ContractRelations: Page object create ends.");
    }

    void ReadData()
    {
        Page.TraceWrite("ReadData starts.");
        try
        {
            objContractRelations.ContractID = int.Parse(hdnContractId.Value);
            objContractRelations.URL = Session[Declarations.URL].ToString();
            //objContractRelations.For = "Graph";
            List<ContractRelations> ContractRelationsDetails = objContractRelations.ReadDataForGraph();

            JavaScriptSerializer jss = new JavaScriptSerializer();
            CRData = jss.Serialize(ContractRelationsDetails);
            if (ContractRelationsDetails.Count > 0)
                ViewState["Updaterelation"] = "Update relation";
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData ends.");
    }

    protected void Back_Click(object sender, EventArgs e)
    {
       Response.Redirect("RequestFlow.aspx?Status=Workflow&RequestId=" + ViewState["RequestId"]);
    }   
    

    void Message()
    {
        if (Session[Declarations.Message] != null)
        {
            var flg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            if (!String.IsNullOrEmpty(Request.QueryString["IsDelete"]))
            {
                Page.JavaScriptClientScriptBlock("status", "PageGetMessage('DS')");
            }
            else
            { Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')"); }
        }
    }

    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (View == "N")
        {
            Page.extDisableControls();
            btnBack.Enabled = true;
        }
        if (Update == "N")
        {
            btnEditRecord.Visible = false;
        }
        else
        {
            if (Convert.ToString(ViewState["Updaterelation"])!="")
                btnEditRecord.Visible = true;
            else
                btnAddRecord.Visible = false;
        }
        if (Add == "N")
        {
            btnAddRecord.Visible = false;
        }
        else
        {
            if (Convert.ToString(ViewState["Updaterelation"]) == "")
                btnAddRecord.Visible = true;
            else
                btnAddRecord.Visible = false;
        }
        if (Delete == "Y")
        {
            if (Convert.ToString(ViewState["Updaterelation"]) != "")
                btnEditRecord.Visible = true;
            else
                if (Add == "N")
                {
                    btnAddRecord.Visible = false;
                }

        }

        Page.TraceWrite("AccessVisibility starts.");
    }
}