﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WorkflowBLL;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;
using MasterBLL;

public partial class WorkFlow_AvailableClientsMaster : System.Web.UI.Page
{
    IAvailableClient obj;
    ICountry objCountry;
    public string Add;
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
      

        if (!IsPostBack)
        {
              CountryBind();
            if (String.IsNullOrEmpty(Request.extValue("hdnPrimeIds")) != true)
            {
                hdnPrimeId.Value = Request.extValue("hdnPrimeIds");
                ReadData();
                lblTitle.Text = "Edit Customer/Supplier Details";
                btnSave.Text = "Update";
                btnSaveAndContinue.Visible = false;
            }
            else
            {
                hdnPrimeId.Value = "0";
                lblTitle.Text = "Add Customer/Supplier Details";
                btnSaveAndContinue.Visible = true;
            }

            if (Session[Declarations.User] != null)
            {
                Access.PageAccess(this, Session[Declarations.User].ToString(), "WorkFlow_");
                ViewState[Declarations.Add] = Access.Add.Trim();
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
            }
            AccessVisibility();
            
        }
     
    }
    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            obj = FactoryWorkflow.GetAvailableClientDetails();
            objCountry = FactoryMaster.GetCountryDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }
    void CountryBind()
    {
        Page.TraceWrite("Country dropdown bind starts.");
        try
        {
            ddlCountry.extDataBind(objCountry.SelectData());
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Country dropdown bind ends.");
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        InsertUpdate();
        Page.TraceWrite("Save Update button clicked event ends.");
    }
    

    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }
    }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (Add == "N" && (hdnPrimeId.Value == "" || hdnPrimeId.Value == "0" || hdnPrimeId.Value == null))
        {
            Page.extDisableControls();
            btnBack.Enabled = true;
        }
        if (View == "N")
        {
            btnBack.Enabled = false;
        }
        Page.TraceWrite("AccessVisibility starts.");
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Server.Transfer("AvailableClientsData.aspx");
    }
    void ReadData()
    {
        Page.TraceWrite("ReadData starts.");
        try
        {
            obj.ClientID = int.Parse(hdnPrimeId.Value);
            List<AvailableClient> Clients = obj.ReadData();
            txtClientName.Value = Clients[0].ClientName;
            txtAddress.Value = Clients[0].Address;
            txtPinCode.Text = Clients[0].Pincode;
            txtMobile.Text = Clients[0].ContactNo;
            txtEmailID.Value = Clients[0].EmailID;
            hdnStateIdRead.Value = Clients[0].StateID.ToString();
            hdnCityIdRead.Value = Clients[0].CityID.ToString();
          //  ddlCountry.extSelectedValues(Clients[0].CountryID.ToString());
            ddlCountry.Value = Clients[0].CountryID.ToString();
            ddlState.extSelectedValues(Clients[0].StateID.ToString());
            ddlCity.extSelectedValues(Clients[0].CityID.ToString());
            btnSaveAndContinue.Visible = false;
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData ends.");
    }

    void InsertUpdate(int flg = 0)
    {
        Page.TraceWrite("Insert, Update starts.");
        string status = "";
        obj.ClientName= txtClientName.Value.Trim();
        obj.Address = txtAddress.Value.Trim();
        obj.Pincode = txtPinCode.Text.Trim();
        obj.CountryID = int.Parse(ddlCountry.Value);
        obj.StateID = int.Parse(hdnStateId.Value);
        obj.CityID = int.Parse(hdnCityId.Value);
        obj.ContactNo = txtMobile.Text;
        obj.EmailID = txtEmailID.Value;
        obj.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        obj.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        obj.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
      
        try
        {
            if (hdnPrimeId.Value == "0")
            {
                obj.ClientID = 0;
                status = obj.InsertRecord();
            }
            else
            {
                obj.ClientID = int.Parse(hdnPrimeId.Value);
                status = obj.UpdateRecord();
            }
            Page.Message(status, hdnPrimeId.Value);
        }
        catch (Exception ex)
        {
            Page.Message("0", hdnPrimeId.Value);
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Insert, Update ends.");
        if (flg == 0 && status == "1")
        {
            Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
            Response.Redirect("AvailableClientsData.aspx");
        }
        ClearData(status);
        // ScriptManager.RegisterStartupScript(this, GetType(), "refresh", "window.setTimeout('window.location.reload(true);',2000);", true);
    }

    void ClearData(string status)
    {
        if (status == "1")
        {
            Page.TraceWrite("clearData starts.");
            obj.ClientID = 0;
            txtClientName.Value = "";
            Page.TraceWrite("clear Data ends.");
            txtAddress.Value = "";
            txtEmailID.Value = "";
            txtMobile.Text = "";
            txtPinCode.Text = "";
            ddlCountry.SelectedIndex = -1;
        }
    }
    protected void btnSaveAndContinue_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save and Add more button clicked event starts.");
        InsertUpdate(1);
        Page.TraceWrite("Save and Add more button clicked event ends.");
    }
}