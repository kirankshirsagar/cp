﻿<%@ WebHandler Language="C#" Class="UploadHandler" %>

using System;
using System.Web;
using DocumentOperationsBLL;
using Elmah;
using System.IO;
using Aspose.Words;
using WorkflowBLL;
using System.Collections.Generic;
using EmailBLL;
using CommonBLL;

using Keyoti.SearchEngine;
using Keyoti.SearchEngine.Index;
using Keyoti.SearchEngine.DataAccess.IndexableSourceRecords;
using Keyoti.SearchEngine.Search;
using System.Web.SessionState;

public class UploadHandler : IHttpHandler, IRequiresSessionState
{

    string ret = "0";
    string NewFileName = "", FinalFileName = "", GUID = "";
    string DIR = "";
    string ext = "";
    string FilePath = "";
    bool isValidDocument = false;
    string RequestId, NextFileName, LoginUserId, IPAddress, ContractTypeId, ContractTemplateId, Remark, isFromDocuSign, IsApprovalDone, IsOCR, IsAI;
    public void ProcessRequest(HttpContext context)
    {
        HttpFileCollection files = context.Request.Files;
        SetParameters(context);

        foreach (string key in files)
        {
            Credentials credentials = new Credentials();
            HttpPostedFile file = files[key];
            string fileName = file.FileName.Replace("&", "and");

            ext = Path.GetExtension(fileName);
            if (fileName.Contains("."))
            {
                NewFileName = fileName.Substring(0, fileName.LastIndexOf('.'));
            }

            GUID = Guid.NewGuid().ToString();
           // FinalFileName = HttpUtility.UrlDecode(NewFileName) + "_" + DateTime.Now.ToString("dd/MM/yyyy").Replace("/", "") + "_" + NextFileName.Substring(NextFileName.LastIndexOf("Version")) + "_" + GUID;
            FinalFileName = HttpUtility.UrlDecode(NextFileName) + "_" + GUID;

            if (ext == ".doc" || ext == ".docx" || ext == ".DOC" || ext == ".DOCX")
            {
                FilePath = context.Server.MapPath(DIR + FinalFileName);
                file.SaveAs(FilePath + ".doc");

                Aspose.Words.Document doc = new Aspose.Words.Document(FilePath + ".doc");
                doc.Save(FilePath + ".docx", Aspose.Words.SaveFormat.Docx);
                File.Delete(FilePath + ".doc");
                isValidDocument = credentials.IsValidContractDocument(FilePath + ".docx");

                if (isFromDocuSign == "true")
                    doc.Save(context.Server.MapPath(DIR + "SignedDocuments//" + FinalFileName + ".pdf"), Aspose.Words.SaveFormat.Pdf);
                else
                    doc.Save(context.Server.MapPath(DIR + "PDF//" + FinalFileName + ".pdf"), Aspose.Words.SaveFormat.Pdf);
            }
            else if (ext.ToUpper() == ".PDF")
            {
                FilePath = context.Server.MapPath(DIR + FinalFileName);
         
                if (IsOCR.ToLower() == "false")
                {
                    if (isFromDocuSign == "true")
                        file.SaveAs(context.Server.MapPath(DIR + "SignedDocuments//" + FinalFileName + ".pdf"));
                    else
                        file.SaveAs(context.Server.MapPath(DIR + "PDF//" + FinalFileName + ".pdf"));
                }
                else if (IsOCR.ToLower() == "true")
                {
                    var DirectoryName = "";
                    var ParentDirectory = "";
                    bool isExists;
                    if (isFromDocuSign == "true")
                    {
                        isExists = System.IO.Directory.Exists(context.Server.MapPath(DIR + "SignedDocuments/ScannedSignedDocuments"));
                        if (!isExists)
                        {
                            System.IO.Directory.CreateDirectory(context.Server.MapPath(DIR + "SignedDocuments/ScannedSignedDocuments"));
                        }

                        DirectoryName = "/SignedDocuments/ScannedSignedDocuments";
                        ParentDirectory = "/SignedDocuments";
                        file.SaveAs(context.Server.MapPath(DIR + "SignedDocuments//ScannedSignedDocuments//" + FinalFileName + ".pdf"));
                    }
                    else
                    {
                        isExists = System.IO.Directory.Exists(context.Server.MapPath(DIR + "PDF/ScannedPDF"));
                        if (!isExists)
                        {
                            System.IO.Directory.CreateDirectory(context.Server.MapPath(DIR + "PDF/ScannedPDF"));
                        }

                        DirectoryName = "/PDF/ScannedPDF";
                        ParentDirectory = "/PDF";
                        file.SaveAs(context.Server.MapPath(DIR + "PDF//ScannedPDF//" + FinalFileName + ".pdf"));
                    }
                    AsyncIndexer.GetInstance().ConvertOCRFile(FinalFileName, ext, "contract", DirectoryName, ParentDirectory);
                }                
                isValidDocument = true;
            }

            //if (isValidDocument)
            {
                ret = "1_" + FinalFileName;
                System.Threading.Thread.Sleep(1000);
                SaveContractFile(context);
                if (ext == ".doc" || ext == ".docx" || ext == ".DOC" || ext == ".DOCX")
                {
                    if (isFromDocuSign == "true")
                        File.Delete(FilePath + ".docx");
                }
                SendEmail();
            }
            
            string strUrl = "";

            IActivity objActivity;
            objActivity = FactoryWorkflow.GetActivityDetails();
            objActivity.RequestId = Convert.ToInt32(RequestId);
            string ContractInfo = objActivity.ContractDetailsByRequestId();
            
            if (ext == ".doc" || ext == ".docx" || ext == ".DOC" || ext == ".DOCX")
            {
                strUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path).ToLower().Replace("workflow/uploadhandler.ashx", DIR.Replace("~/", "")) + FinalFileName + ".docx";
                AsyncIndexer.GetInstance().QueueForIndexing(strUrl, 1, ContractInfo);
            }

            if (isFromDocuSign == "true")
            {
                strUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path).ToLower().Replace("workflow/uploadhandler.ashx", DIR.Replace("~/", "")) + "SignedDocuments/" + FinalFileName + ".pdf";
            }
            else
            {
                strUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path).ToLower().Replace("workflow/uploadhandler.ashx", DIR.Replace("~/", "")) + "PDF/" + FinalFileName + ".pdf";
            }
            AsyncIndexer.GetInstance().QueueForIndexing(strUrl, 1, ContractInfo);

            /*Keyoti Uploaded Document Indexing End.*/
        }
        context.Response.ContentType = "text/plain";
        context.Response.Write(ret);
    }

    private void SetParameters(HttpContext context)
    {
        RequestId = context.Request.QueryString["RequestId"];
        NextFileName = context.Request.QueryString["NextFileName"];
        LoginUserId = context.Request.QueryString["LoginUserId"];
        IPAddress = context.Request.QueryString["IPAddress"];
        //ContractTypeId = IPAddress = context.Request.QueryString["ContractTypeID"];
        ContractTypeId = context.Request.QueryString["ContractTypeID"];
        ContractTemplateId = context.Request.QueryString["ContractTemplateId"];
        Remark = context.Request.QueryString["Remark"];
        isFromDocuSign = context.Request.QueryString["isFromDocuSign"];
        IsApprovalDone = context.Request.QueryString["IsApprovalDone"];
        DIR = @"~/Uploads/" + context.Request.Url.Segments[1].Replace("/", "") + "/Contract Documents/";
        IsOCR = context.Request.QueryString["IsOCR"];
        IsAI = context.Request.QueryString["IsAI"];
    }

    private string GetNewFileName(string NewFileName)
    {
        string Datetime = DateTime.Now.ToString("yyyyMMdd_hhmmss_fffff");
        string fname = "";
        fname = NewFileName + "_" + Datetime;
        return fname;
    }
    protected void SaveContractFile(HttpContext context)
    {
        ContractProcedures CP = new ContractProcedures();
        string FILE = "";
        if (ext.ToUpper() == ".PDF")
        {
            CP.isPDFFile = 'Y';
            if (isFromDocuSign == "true")
                FILE = context.Server.MapPath(DIR + "SignedDocuments//" + FinalFileName + ".pdf");
            else
                if (IsOCR.ToLower() == "false")
                {
                    FILE = context.Server.MapPath(DIR + "PDF//" + FinalFileName + ".pdf");                    
                }
                else
                {
                    FILE = context.Server.MapPath(DIR + "PDF//ScannedPDF//" + FinalFileName + ".pdf");                   
                }
        }
        else
            FILE = FilePath + ".docx";

        string IsPDFUpload = "NO";
        FileInfo file = new FileInfo(FILE);      
        
        CP.RequestId = Convert.ToInt32(RequestId);
        CP.FileName = FinalFileName;
        CP.FileSizeKB = Math.Round(Convert.ToDecimal(file.Length) / 1024, 2).ToString();
        CP.ModifiedBy = Convert.ToInt32(LoginUserId);
        CP.IpAddress = IPAddress;
        CP.isUploaded = 'Y';
        CP.IsApprovalDone = IsApprovalDone == "Y" ? 'Y' : 'N';
        if (isFromDocuSign == "true")
            CP.isFromDocuSign = "Y";
        CP.Remark = Remark;
        CP.IsValidDocument = (isValidDocument) ? 'Y' : 'N';
        CP.GUID = GUID;
        CP.IsOCR = IsOCR == "true" ? "Y" : "N";
        CP.IsFromAI = IsAI == "true" ? 'Y' : 'N';
        
        string result = CP.InsertRecord();

        IsPDFUpload = CP.isPDFFile == 'Y' ? "YES" : "NO";
        
        if (IsAI.Equals("true"))
        {
            if (IsPDFUpload.Equals("YES"))
            {
                PDFtoWORD wrd = new PDFtoWORD();
                FILE = wrd.Convert(FILE);
            }
            ArtificialIntelligence.ReadAndExtract(FILE, false, true, Convert.ToInt32(RequestId), "NO");
        }
    }

    void SendEmail()
    {
        try
        {
            //Page.TraceWrite("send emails.");
            EmailTemplateBLL.IEmailTrigger objEmail = EmailTemplateBLL.FactoryEmailTemplate.GetEmailTriggerDetail();
            objEmail.RequestId = Convert.ToInt32(RequestId);
            objEmail.ContractTypeId = 99999;
            objEmail.ContractTemplateId = 99999;
            objEmail.EmailTriggerId = 2; // hardcoded for Amendment made to contract
            objEmail.userId = int.Parse(LoginUserId);
            objEmail.BulkEmail();

            // sk
            if (isFromDocuSign == "true")
            {
                SendContractGeneratedEmailToClient(objEmail.RequestId);
            }
        }
        catch (Exception)
        {
            //Page.TraceWarn("send emails fail.");
        }
    }


    void SendContractGeneratedEmailToClient(int RequestId)
    {
        IActivity objAct = FactoryWorkflow.GetActivityDetails();
        objAct.RequestId = RequestId;
        List<Activity> lst = new List<Activity>();
        lst = objAct.GetUserNameAndPassword();
        string userEmailId = lst[0].EmailID;
        string password = lst[0].Password;
        string customerName = lst[0].UserName;

        string contractGeneratedDate = lst[0].AddedOnDate;
        int ContractId = lst[0].ContractID;
        if (ContractId > 0)
        {
            try
            {
                SendMail sm = new SendMail();
                sm.MailFrom = System.Configuration.ConfigurationManager.AppSettings["MailFrom"];
                sm.Subject = "Contract " + ContractId.ToString() + " - Available for Review";

                string body = "<font face='Arial' size='2' color='#006666'>Hello <br><br>";
                body = body + "Contract ID: " + ContractId.ToString() + "<br><br>";
                body = body + "A contract has been generated for your attention.  You can view the contract and its activities by <br>";
                body = body + "following the link below:<br><br>";
                body = body + "<a href='" + Strings.domainURL() + "customerportal/login.aspx'>" + Strings.domainURL() + "customerportal/login.aspx</a><br><br>";
                body = body + "Your login details are:<br><br>";
                body = body + "Username: " + userEmailId + "<br>";
                body = body + "Password: " + password + "<br><br>";
                body = body + "Happy contracting!";
                body = body + "<br>The ContractPod™ team</font>";

                body = body + "<br><br><font face='Arial' size='1' color='#006666'>Need help? Please do not hesitate to contact our technical support team on 0800 699 0045 or at help@contractpod.com.";
                body = body + "<br>The information in this email is confidential and for use by the addressee(s) only. If you are not the intended recipient, please notify us immediately on 0800 699 0045 and delete the message from your computer. You may not copy or forward the e-mail, or use it or disclose its contents to any other person. We do not accept any liability or responsibility for changes made to this email after it was sent, or viruses transmitted through this e-mail or any attachment.</font>";

                sm.Body = body;
                sm.MailTo = userEmailId;
                sm.RequestId = RequestId;
                sm.Mode = "Contract Generated";
                sm.SendSimpleMail();
            }
            catch (Exception)
            {

            }
        }
    }


    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}