﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;
using DocumentOperationsBLL;
using WorkflowBLL;
using System.Data;
using UserManagementBLL;
using MetaDataConfiguratorsBLL;
using System.Web.UI.HtmlControls;
using System.Web.Services;

public partial class WorkFlow_AddManualContract : System.Web.UI.Page
{
    IKeyFields objkey;
    IUsers objuser;
    IActivity objActivity;

    static string filename = "";
    IContractRequest objcontractreq;
    public string SendNotification;
    //// public readonly static string Path = @"~/Uploads/" +Convert.ToString(HttpContext.Current.Session["TenantDIR"]) + "/Contract Documents";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            objkey = FactoryWorkflow.GetKeyFieldsDetails();
            objuser = FactoryUser.GetUsersDetail();
            objActivity = FactoryWorkflow.GetActivityDetails();
            BindDropDowns();
            BindUser();
            SetDefault();
            BindValues();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "abmsg", " HideShowPReason();", true);
        }
        GetDynamicControls(ddlContractTemplate.SelectedValue);
        if (Session[Declarations.User] != null)
        {
            NotificationVisibility();
        }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);
        if (Request.Params["ctl00$MainContent$btnBack"] == null)
        {
            BindContractRequest("0");
            BindImportantDateRequest();
            BindKeyObligationRequest();
        }
    }

    private void BindContractRequest(string RequestId)
    {

        IMetaDataConfigurators objMetaDataConfig = FactoryMedaData.GetContractTemplateDetail();
        HtmlTable container = objMetaDataConfig.GenerateRequestForm("Request Form", RequestId, "edit");

        int returnTableRow = container.Rows.Count;
        int cnt = MetaDataTable.Rows.Count;

        for (int i = 0; i < returnTableRow; i++)
        {
            MetaDataTable.Rows.Add(container.Rows[0]);
        }

    }

    private void BindImportantDateRequest()
    {
        //try
        //{
        //    IMetaDataConfigurators objMetaDataConfig = FactoryMedaData.GetContractTemplateDetail();
        //    /////Control container = objMetaDataConfig.GenerateRequestForm("Important Dates", HdnRequestID.Value,"Edit");
        //    HtmlTable container = objMetaDataConfig.GenerateRequestForm("Important Dates", HdnRequestID.Value, "Edit");
        //    ////pnlnewadd.Controls.Add(container);

        //    int returnTableRow = container.Rows.Count;
        //    int cnt = ImportantDateTable.Rows.Count;

        //    for (int i = 0; i < returnTableRow; i++)
        //    {
        //        ImportantDateTable.Rows.Add(container.Rows[0]);
        //    }
        //}
        //catch (Exception ex)
        //{
        //    throw ex;
        //}
        try
        {
            IMetaDataConfigurators objMetaDataConfig = FactoryMedaData.GetContractTemplateDetail();
            /////Control container = objMetaDataConfig.GenerateRequestForm("Important Dates", HdnRequestID.Value,"Edit");
            HtmlTable container = objMetaDataConfig.GenerateRequestForm("Important Dates", HdnRequestID.Value, "Edit");
            ////pnlnewadd.Controls.Add(container);

            int returnTableRow = container.Rows.Count;
            int cnt = ImportantDateTable.Rows.Count;

            for (int i = 0; i < returnTableRow; i++)
            {
                ImportantDateTable.Rows.Add(container.Rows[0]);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindKeyObligationRequest()
    {
        try
        {
            IMetaDataConfigurators objMetaDataConfig = FactoryMedaData.GetContractTemplateDetail();
            HtmlTable container = objMetaDataConfig.GenerateRequestForm("Key Obligations", HdnRequestID.Value, "Edit");

            int returnTableRow = container.Rows.Count;
            int cnt = keyObligationTable.Rows.Count;
            for (int i = 0; i < returnTableRow; i++)
            {
                keyObligationTable.Rows.Add(container.Rows[0]);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void BindValues()
    {
        try
        {
            List<KeyFeilds> mylist;

            objkey.RequestId = int.Parse(HdnRequestID.Value);
            mylist = objkey.ReadData();
            txteffectivedate.Value = (mylist[0].EffectiveDatestring).ToString();
            txtexpirationdate.Value = (mylist[0].ExpirationDatestring).ToString();
            txtrenewaldate.Value = (mylist[0].RenewalDatestring).ToString();

            hdnContractID.Value = mylist[0].ContractID.ToString();

            //--Expiration  //--Renewal
            //added by js on 13-1-2016

            DataTable dtEx = (DataTable)Session["ImportDateDataEx"];
            DataTable dtRen = (DataTable)Session["ImportDateDataRen"];

            IEnumerable<DataRow> rowsrpt = dtEx.AsEnumerable()
             .Where(r => r.Field<string>("ExAlerts") == "Y");

            DataTable dtE = rowsrpt.CopyToDataTable();

            if (dtE.Rows.Count > 0)
            {
                try
                {
                    ddlRemindExpiration1.Value = Convert.ToString(dtE.Rows[0]["ExAlertsDay"]);
                    ddlexp180Users1.Value = Convert.ToString(dtE.Rows[0]["ExUser1"]);
                    ddlexp180Users2.Value = Convert.ToString(dtE.Rows[0]["ExUser2"]);
                    chkexp180.Checked = true;

                    ddlRemindExpiration2.Value = Convert.ToString(dtE.Rows[1]["ExAlertsDay"]);
                    ddlexp90Users1.Value = Convert.ToString(dtE.Rows[1]["ExUser1"]);
                    ddlexp90Users2.Value = Convert.ToString(dtE.Rows[1]["ExUser2"]);
                    chkexp90.Checked = true;

                    ddlRemindExpiration3.Value = Convert.ToString(dtE.Rows[2]["ExAlertsDay"]);
                    ddlexp60Users1.Value = Convert.ToString(dtE.Rows[2]["ExUser1"]);
                    ddlexp60Users2.Value = Convert.ToString(dtE.Rows[2]["ExUser2"]);
                    chkexp60.Checked = true;

                }
                catch (Exception ex) { }
            }

            IEnumerable<DataRow> rowsrpt1 = dtRen.AsEnumerable()
            .Where(r => r.Field<string>("RenAlerts") == "Y");

            DataTable dtR = rowsrpt1.CopyToDataTable();
            if (dtR.Rows.Count > 0)
            {
                try
                {
                    ddlRemindRenewal1.Value = Convert.ToString(dtR.Rows[0]["RenAlertsDay"]);
                    ddlRenewal180User1.Value = Convert.ToString(dtR.Rows[0]["RenUser1"]);
                    ddlRenewal180User2.Value = Convert.ToString(dtR.Rows[0]["RenUser2"]);
                    chkrem180.Checked = true;

                    ddlRemindRenewal2.Value = Convert.ToString(dtR.Rows[1]["RenAlertsDay"]);
                    ddlRenewal90User1.Value = Convert.ToString(dtR.Rows[1]["RenUser1"]);
                    ddlRenewal90User2.Value = Convert.ToString(dtR.Rows[1]["RenUser2"]);
                    chkrem90.Checked = true;

                    ddlRemindRenewal3.Value = Convert.ToString(dtR.Rows[2]["RenAlertsDay"]);
                    ddlRenewal60User1.Value = Convert.ToString(dtR.Rows[2]["RenUser1"]);
                    ddlRenewal60User2.Value = Convert.ToString(dtR.Rows[2]["RenUser2"]);
                    chkrem60.Checked = true;

                }
                catch (Exception ex) { }
            }

        }
        catch { }
    }

    public void BindUser()
    {
        try
        {
            if (Session[Declarations.User] != null)
            {
                objuser = new Users();
                objuser.UserId = int.Parse(Session[Declarations.User].ToString());
                objuser.ContractTypeId = Convert.ToInt32(ViewState["ContractTypeId"]);
                objuser.DepartmentId = 0;
                ddlexp180Users1.extDataBind(objuser.SelectAllUsers());
                ddlexp180Users2.extDataBind(objuser.SelectAllUsers());

                ddlexp90Users1.extDataBind(objuser.SelectAllUsers());
                ddlexp90Users2.extDataBind(objuser.SelectAllUsers());

                ddlexp60Users1.extDataBind(objuser.SelectAllUsers());
                ddlexp60Users2.extDataBind(objuser.SelectAllUsers());

                //ddlexp30Users1.extDataBind(objuser.SelectAllUsers());
                //ddlexp30Users2.extDataBind(objuser.SelectAllUsers());

                //ddlexp7Users1.extDataBind(objuser.SelectAllUsers());
                //ddlexp7Users2.extDataBind(objuser.SelectAllUsers());

                //ddlexp1Users1.extDataBind(objuser.SelectAllUsers());
                //ddlexp1Users2.extDataBind(objuser.SelectAllUsers());

                ddlRenewal180User1.extDataBind(objuser.SelectAllUsers());
                ddlRenewal180User2.extDataBind(objuser.SelectAllUsers());

                ddlRenewal90User1.extDataBind(objuser.SelectAllUsers());
                ddlRenewal90User2.extDataBind(objuser.SelectAllUsers());

                ddlRenewal60User1.extDataBind(objuser.SelectAllUsers());
                ddlRenewal60User2.extDataBind(objuser.SelectAllUsers());

                //ddlRenewal30User1.extDataBind(objuser.SelectAllUsers());
                //ddlRenewal30User2.extDataBind(objuser.SelectAllUsers());

                //ddlRenewal7User1.extDataBind(objuser.SelectAllUsers());
                //ddlRenewal7User2.extDataBind(objuser.SelectAllUsers());

                //ddlRenewal1User1.extDataBind(objuser.SelectAllUsers());
                //ddlRenewal1User2.extDataBind(objuser.SelectAllUsers());
            }
        }
        catch { }
    }

    protected void BindDropDowns()
    {

        IContractTemplate objtemplate = new ContractTemplate();
        IContractRequest objRequest = new ContractRequest();
        ICountry objCountry = new Country();
        objtemplate.UsersId = Convert.ToInt32(Session[Declarations.User]);
        ddlContractTemplate.extDataBind(objtemplate.SelectBulkImportData());
        ddlContractTemplate.DataBind();
        ddlContractingParty.extDataBind(objRequest.SelectContractingPartyData());
        ddlCountry.extDataBind(objCountry.SelectData());


        ddlOriginalCurrency.DataSource = FillMasterData("MstCurrency"); //table name
        ddlOriginalCurrency.DataTextField = "Name";
        ddlOriginalCurrency.DataValueField = "Id";
        ddlOriginalCurrency.DataBind();

        ddlPrority.DataSource = FillMasterData("mstPriority"); //table name
        ddlPrority.DataTextField = "Name";
        ddlPrority.DataValueField = "Id";
        ddlPrority.DataBind();

    }

    protected void CreateObjects()
    {


    }

    private void SetDefault()
    {
        System.Collections.Specialized.NameValueCollection RequestFlow = Request.Form;
        ViewState["RequestId"] = RequestFlow["ctl00$MainContent$hdnRequestId"] == null ? "0" : RequestFlow["ctl00$MainContent$hdnRequestId"];
        //ViewState["ContractTypeId"] = RequestFlow["ctl00$MainContent$hdnContractTypeId"];
        //if (!string.IsNullOrEmpty(RequestFlow["ctl00$MainContent$hdnSelectedFileId"]))
        //{
        //    ViewState["FileName"] = RequestFlow["ctl00$MainContent$hdnSelectedFileId"].Split('#')[1];
        //    ViewState["TemplateId"] = RequestFlow["ctl00$MainContent$hdnSelectedFileId"].Split('#')[0];
        //}
        //ViewState["ContractId"] = RequestFlow["ctl00$MainContent$hdnContractId"];
        //ViewState["ContractFileName"] = RequestFlow["ctl00$MainContent$hdnContractFileName"];

        //if (string.IsNullOrEmpty(ViewState["ContractId"].ToString()) || ViewState["ContractId"].ToString() == "0")
        //    btnSaveDraft.Visible = true;
        //else
        //    btnSaveDraft.Visible = false;
    }

    protected void GetDynamicControls(string value)
    {
        Page.TraceWrite("Dynamic control generation started");

        phQuestionnair.Controls.Clear();

        string Path = @"~/Uploads/" + Session["TenantDIR"] + "/Contract Template";
        bool isExists = System.IO.Directory.Exists(Server.MapPath(Path));
        if (!isExists)
        {
            Console.WriteLine("No file found.");
        }
        else
        {
            try
            {
                string TemplateId = value;//ViewState["SelectedFileId"] == null ? "0" : ViewState["SelectedFileId"].ToString().Split('#')[0];
                //ViewState["FilePath"] = Server.MapPath(Path);// +"//" + hdnFileName.Value;
                Credentials credentials = new Credentials();
                credentials.IP = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
                credentials.UserId = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
                phQuestionnair.Controls.Clear();
                Control container = credentials.GenerateQuetionnair(TemplateId);// FileRead.ReadMsWord(ViewState["filePath"].ToString());        
                phQuestionnair.Controls.Add(container);

                btnGenerateContract.Visible = true;
            }
            catch (Exception ex)
            {
                Page.TraceWrite("Dynamic control generation failed. " + ex.Message);
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        Page.TraceWrite("Dynamic control generation succeed");
    }

    protected void SaveContractFile()
    {

    }

    protected void btnChangeUser_Click(object sender, EventArgs e)
    {
        //switch (hdnUserStatus.Value)
        //{
        //case "Exp30User":
        //    ddlexp30Users1.Value = "0";
        //    ddlexp30Users2.Value = "0";
        //    break;

        //case "Exp7User":
        //    ddlexp7Users1.Value = "0";
        //    ddlexp7Users2.Value = "0";
        //    break;

        //case "Exp24User":
        //    ddlexp1Users1.Value = "0";
        //    ddlexp1Users2.Value = "0";
        //    break;

        //case "Rem30User":
        //    ddlRenewal30User1.Value = "0";
        //    ddlRenewal30User2.Value = "0";
        //    break;

        //case "Rem7User":
        //    ddlRenewal7User1.Value = "0";
        //    ddlRenewal7User2.Value = "0";
        //    break;

        //case "Rem24User":
        //    ddlRenewal1User1.Value = "0";
        //    ddlRenewal1User2.Value = "0";
        //    break;

        //default:
        //    break;

        //}

    }

    public void SaveUserInActivityTable()
    {
        try
        {
            objkey.RequestId = Convert.ToInt32(HdnRequestID.Value);
            string expdate = "";
            string renualdate = "";
            string Reminddate1 = "";
            string Reminddate2 = "";
            string Reminddate3 = "";
            try
            {
                expdate = Convert.ToDateTime(txtexpirationdate.Value).ToString("dd-MMM-yyyy");
            }
            catch { }
            try
            {
                renualdate = Convert.ToDateTime(txtrenewaldate.Value).ToString("dd-MMM-yyyy");
            }
            catch { }
            try
            {
                Reminddate1 = Convert.ToDateTime(hdnReminddate1.Value).ToString("dd-MMM-yyyy");
            }
            catch { }
            try
            {
                Reminddate2 = Convert.ToDateTime(hdnReminddate2.Value).ToString("dd-MMM-yyyy");
            }
            catch { }
            try
            {
                Reminddate3 = Convert.ToDateTime(hdnReminddate3.Value).ToString("dd-MMM-yyyy");
            }
            catch { }


            DateTime exp30day; DateTime exp7day; DateTime exp1day;
            DateTime Ren30day; DateTime Ren7day; DateTime Ren1day;
            DateTime exp180day; DateTime exp90day; DateTime exp60day;
            DateTime Ren180day; DateTime Ren90day; DateTime Ren60day;


            //return;

            //For delete
            objkey.Param = 4;
            objkey.RequestId = Convert.ToInt32(HdnRequestID.Value);
            objkey.InsertRecord();

            //End


            objkey.Param = 3;
            objkey.ActivityStatusId = 1;
            objkey.ActivityTypeId = 2;
            objkey.IsFromOther = "KeyField";

            objkey.IsForCustomer = "Y";
            objkey.IsFromCalendar = "Y";

            objkey.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            objkey.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            objkey.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
            #region Expiration Activity
            //For Expiration 
            if (expdate != "")
            {
                objkey.ActivityText = "Contract expiration on-" + expdate;
                objkey.ActivityDate = Convert.ToDateTime(expdate);
                exp180day = Convert.ToDateTime(expdate).AddDays(-180); //js
                exp90day = Convert.ToDateTime(expdate).AddDays(-90);
                exp60day = Convert.ToDateTime(expdate).AddDays(-60);
                exp30day = Convert.ToDateTime(expdate).AddDays(-30);
                exp7day = Convert.ToDateTime(expdate).AddDays(-7);
                exp1day = Convert.ToDateTime(expdate).AddDays(-1);
                //--------------------------
                if (ddlexp180Users1.Value != "0")
                {
                    if (chkexp180.Checked == true)
                    {
                        objkey.ReminderDate = (exp180day);
                        objkey.AssignToId = Convert.ToInt32(ddlexp180Users1.Value);
                        objkey.InsertRecord();
                    }
                }
                if (ddlexp180Users2.Value != "0")
                {
                    if (chkexp180.Checked == true)
                    {
                        objkey.ReminderDate = (exp180day);
                        objkey.AssignToId = Convert.ToInt32(ddlexp180Users2.Value);
                        objkey.InsertRecord();
                    }
                }
                if (ddlexp90Users1.Value != "0")
                {
                    if (chkexp90.Checked == true)
                    {
                        objkey.ReminderDate = (exp90day);
                        objkey.AssignToId = Convert.ToInt32(ddlexp90Users1.Value);
                        objkey.InsertRecord();
                    }
                }
                if (ddlexp90Users2.Value != "0")
                {
                    if (chkexp90.Checked == true)
                    {
                        objkey.ReminderDate = (exp90day);
                        objkey.AssignToId = Convert.ToInt32(ddlexp90Users2.Value);
                        objkey.InsertRecord();
                    }
                }
                if (ddlexp60Users1.Value != "0")
                {
                    if (chkexp60.Checked == true)
                    {
                        objkey.ReminderDate = (exp60day);
                        objkey.AssignToId = Convert.ToInt32(ddlexp60Users1.Value);
                        objkey.InsertRecord();
                    }
                }
                if (ddlexp60Users2.Value != "0")
                {
                    if (chkexp60.Checked == true)
                    {
                        objkey.ReminderDate = (exp60day);
                        objkey.AssignToId = Convert.ToInt32(ddlexp60Users2.Value);
                        objkey.InsertRecord();
                    }
                }

                ////--------------------------
                //if (ddlexp30Users1.Value != "0")
                //{
                //    if (chkexp30.Checked == true)
                //    {
                //        objkey.ReminderDate = (exp30day);
                //        objkey.AssignToId = Convert.ToInt32(ddlexp30Users1.Value);
                //        objkey.InsertRecord();
                //    }
                //}
                //if (ddlexp30Users2.Value != "0")
                //{
                //    if (chkexp30.Checked == true)
                //    {
                //        objkey.ReminderDate = (exp30day);
                //        objkey.AssignToId = Convert.ToInt32(ddlexp30Users2.Value);
                //        objkey.InsertRecord();
                //    }
                //}
                //if (ddlexp7Users1.Value != "0")
                //{
                //    if (chkexp7.Checked == true)
                //    {
                //        objkey.ReminderDate = (exp7day);
                //        objkey.AssignToId = Convert.ToInt32(ddlexp7Users1.Value);
                //        objkey.InsertRecord();
                //    }
                //}
                //if (ddlexp7Users2.Value != "0")
                //{
                //    if (chkexp7.Checked == true)
                //    {
                //        objkey.ReminderDate = (exp7day);
                //        objkey.AssignToId = Convert.ToInt32(ddlexp7Users2.Value);
                //        objkey.InsertRecord();
                //    }
                //}
                //if (ddlexp1Users1.Value != "0")
                //{
                //    if (chkexp24.Checked == true)
                //    {
                //        objkey.ReminderDate = (exp1day);
                //        objkey.AssignToId = Convert.ToInt32(ddlexp1Users1.Value);
                //        objkey.InsertRecord();
                //    }
                //}
                //if (ddlexp1Users2.Value != "0")
                //{
                //    if (chkexp24.Checked == true)
                //    {
                //        objkey.ReminderDate = (exp1day);
                //        objkey.AssignToId = Convert.ToInt32(ddlexp1Users2.Value);
                //        objkey.InsertRecord();
                //    }
                //}

            }
            #endregion

            #region Renewal Activity
            //For renewal 
            if (renualdate != "")
            {
                objkey.ActivityText = "Renewal date of contract is-" + renualdate;
                objkey.ActivityDate = Convert.ToDateTime(renualdate);

                Ren180day = Convert.ToDateTime(renualdate).AddDays(-180);
                Ren90day = Convert.ToDateTime(renualdate).AddDays(-90);
                Ren60day = Convert.ToDateTime(renualdate).AddDays(-60);
                Ren30day = Convert.ToDateTime(renualdate).AddDays(-30);
                Ren7day = Convert.ToDateTime(renualdate).AddDays(-7);
                Ren1day = Convert.ToDateTime(renualdate).AddDays(-1);

                //--------------------------------
                if (ddlRenewal180User1.Value != "0")
                {
                    if (chkrem180.Checked == true)
                    {
                        objkey.ReminderDate = (Ren180day);
                        objkey.AssignToId = Convert.ToInt32(ddlRenewal180User1.Value);
                        objkey.InsertRecord();
                    }
                }
                if (ddlRenewal180User2.Value != "0")
                {
                    if (chkrem180.Checked == true)
                    {
                        objkey.ReminderDate = (Ren180day);
                        objkey.AssignToId = Convert.ToInt32(ddlRenewal180User2.Value);
                        objkey.InsertRecord();
                    }
                }
                if (ddlRenewal90User1.Value != "0")
                {
                    if (chkrem90.Checked == true)
                    {
                        objkey.ReminderDate = (Ren90day);
                        objkey.AssignToId = Convert.ToInt32(ddlRenewal90User1.Value);
                        objkey.InsertRecord();
                    }
                }
                if (ddlRenewal90User2.Value != "0")
                {
                    if (chkrem90.Checked == true)
                    {
                        objkey.ReminderDate = (Ren90day);
                        objkey.AssignToId = Convert.ToInt32(ddlRenewal90User2.Value);
                        objkey.InsertRecord();
                    }
                }
                if (ddlRenewal60User1.Value != "0")
                {
                    if (chkrem60.Checked == true)
                    {
                        objkey.ReminderDate = (Ren30day);
                        objkey.AssignToId = Convert.ToInt32(ddlRenewal60User1.Value);
                        objkey.InsertRecord();
                    }
                }
                if (ddlRenewal60User2.Value != "0")
                {
                    if (chkrem60.Checked == true)
                    {
                        objkey.ReminderDate = (Ren60day);
                        objkey.AssignToId = Convert.ToInt32(ddlRenewal60User2.Value);
                        objkey.InsertRecord();
                    }
                }
                ////--------------------------------
                //if (ddlRenewal30User1.Value != "0")
                //{
                //    if (chkrem30.Checked == true)
                //    {
                //        objkey.ReminderDate = (Ren30day);
                //        objkey.AssignToId = Convert.ToInt32(ddlRenewal30User1.Value);
                //        objkey.InsertRecord();
                //    }
                //}
                //if (ddlRenewal30User2.Value != "0")
                //{
                //    if (chkrem30.Checked == true)
                //    {
                //        objkey.ReminderDate = (Ren30day);
                //        objkey.AssignToId = Convert.ToInt32(ddlRenewal30User2.Value);
                //        objkey.InsertRecord();
                //    }
                //}
                //if (ddlRenewal7User1.Value != "0")
                //{
                //    if (chkrem7.Checked == true)
                //    {
                //        objkey.ReminderDate = (Ren7day);
                //        objkey.AssignToId = Convert.ToInt32(ddlRenewal7User1.Value);
                //        objkey.InsertRecord();
                //    }
                //}
                //if (ddlRenewal7User2.Value != "0")
                //{
                //    if (chkrem7.Checked == true)
                //    {
                //        objkey.ReminderDate = (Ren7day);
                //        objkey.AssignToId = Convert.ToInt32(ddlRenewal7User2.Value);
                //        objkey.InsertRecord();
                //    }
                //}
                //if (ddlRenewal1User1.Value != "0")
                //{
                //    if (chkrem24.Checked == true)
                //    {
                //        objkey.ReminderDate = (Ren1day);
                //        objkey.AssignToId = Convert.ToInt32(ddlRenewal1User1.Value);
                //        objkey.InsertRecord();
                //    }
                //}
                //if (ddlRenewal1User2.Value != "0")
                //{
                //    if (chkrem24.Checked == true)
                //    {
                //        objkey.ReminderDate = (Ren1day);
                //        objkey.AssignToId = Convert.ToInt32(ddlRenewal1User2.Value);
                //        objkey.InsertRecord();
                //    }
                //}
            }
            #endregion

            /// Delete for Import Date Custom Reminder
            int ContarctID = Convert.ToInt32(ViewState["ContractID"]);
            objkey.ContractID = ContarctID;
            objkey.Param = 6;
            objkey.RequestId = Convert.ToInt32(HdnRequestID.Value);
            //objkey.MetaDataFieldId = Convert.ToInt32(ViewState["FieldTypeId1"]);
            objkey.InsertRecord();

            #region Custom Reminder date 1
            if (Reminddate1 != "")
            {

                objkey.ActivityText = hdnReminderName1.Value + " reminder on-" + Reminddate1;
                objkey.ActivityDate = Convert.ToDateTime(Reminddate1);
                exp180day = Convert.ToDateTime(Reminddate1).AddDays(-180); //js
                exp90day = Convert.ToDateTime(Reminddate1).AddDays(-90);
                exp60day = Convert.ToDateTime(Reminddate1).AddDays(-60);
                exp30day = Convert.ToDateTime(Reminddate1).AddDays(-30);
                exp7day = Convert.ToDateTime(Reminddate1).AddDays(-7);
                exp1day = Convert.ToDateTime(Reminddate1).AddDays(-1);
                //--------------------------
                objkey.Param = 3;
                if (hdnRemindDay10.Value != "0")
                {
                    if (hdnRemindDay10.Value == "180")
                        objkey.ReminderDate = (exp180day);
                    else if (hdnRemindDay10.Value == "90")
                        objkey.ReminderDate = (exp90day);
                    else if (hdnRemindDay10.Value == "60")
                        objkey.ReminderDate = (exp60day);
                    else if (hdnRemindDay10.Value == "30")
                        objkey.ReminderDate = (exp30day);
                    else if (hdnRemindDay10.Value == "7")
                        objkey.ReminderDate = (exp7day);
                    else if (hdnRemindDay10.Value == "1")
                        objkey.ReminderDate = (exp1day);

                    if (hdnRemindDay1User10.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay1User10.Value);
                        objkey.InsertRecord();
                    }
                    if (hdnRemindDay1User20.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay1User20.Value);
                        objkey.InsertRecord();
                    }
                }

                if (hdnRemindDay20.Value != "0")
                {
                    if (hdnRemindDay20.Value == "180")
                        objkey.ReminderDate = (exp180day);
                    else if (hdnRemindDay20.Value == "90")
                        objkey.ReminderDate = (exp90day);
                    else if (hdnRemindDay20.Value == "60")
                        objkey.ReminderDate = (exp60day);
                    else if (hdnRemindDay20.Value == "30")
                        objkey.ReminderDate = (exp30day);
                    else if (hdnRemindDay20.Value == "7")
                        objkey.ReminderDate = (exp7day);
                    else if (hdnRemindDay20.Value == "1")
                        objkey.ReminderDate = (exp1day);

                    if (hdnRemindDay2User10.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay2User10.Value);
                        objkey.InsertRecord();
                    }
                    if (hdnRemindDay2User20.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay2User20.Value);
                        objkey.InsertRecord();
                    }
                }

                if (hdnRemindDay30.Value != "0")
                {
                    if (hdnRemindDay30.Value == "180")
                        objkey.ReminderDate = (exp180day);
                    else if (hdnRemindDay30.Value == "90")
                        objkey.ReminderDate = (exp90day);
                    else if (hdnRemindDay30.Value == "60")
                        objkey.ReminderDate = (exp60day);
                    else if (hdnRemindDay30.Value == "30")
                        objkey.ReminderDate = (exp30day);
                    else if (hdnRemindDay30.Value == "7")
                        objkey.ReminderDate = (exp7day);
                    else if (hdnRemindDay30.Value == "1")
                        objkey.ReminderDate = (exp1day);

                    if (hdnRemindDay3User10.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay3User10.Value);
                        objkey.InsertRecord();
                    }
                    if (hdnRemindDay3User20.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay3User20.Value);
                        objkey.InsertRecord();
                    }
                }

                //int ContarctID = Convert.ToInt32(ViewState["ContractID"]);
                //objkey.ContractID = ContarctID;
                //objkey.Param = 6;
                //objkey.RequestId = Convert.ToInt32(HdnRequestID.Value);
                objkey.MetaDataFieldId = Convert.ToInt32(ViewState["FieldTypeId1"]);
                //objkey.InsertRecord();


                objkey.Param = 5;
                objkey.RequestId = int.Parse(HdnRequestID.Value);
                if (hdnRemindDay10.Value != "0")
                {
                    objkey.ReminderDays = Convert.ToInt32(hdnRemindDay10.Value);
                    objkey.ReminderUser1 = Convert.ToInt32(hdnRemindDay1User10.Value);
                    objkey.ReminderUser2 = Convert.ToInt32(hdnRemindDay1User20.Value);
                    objkey.InsertRecord();
                }
                if (hdnRemindDay20.Value != "0")
                {
                    objkey.ReminderDays = Convert.ToInt32(hdnRemindDay20.Value);
                    objkey.ReminderUser1 = Convert.ToInt32(hdnRemindDay2User10.Value);
                    objkey.ReminderUser2 = Convert.ToInt32(hdnRemindDay2User20.Value);
                    objkey.InsertRecord();
                }
                if (hdnRemindDay30.Value != "0")
                {
                    objkey.ReminderDays = Convert.ToInt32(hdnRemindDay30.Value);
                    objkey.ReminderUser1 = Convert.ToInt32(hdnRemindDay3User10.Value);
                    objkey.ReminderUser2 = Convert.ToInt32(hdnRemindDay3User20.Value);
                    objkey.InsertRecord();
                }

            }

            #endregion

            #region Custom Reminder date 2
            if (Reminddate2 != "")
            {
                objkey.ActivityText = hdnReminderName2.Value + " reminder on-" + Reminddate2;
                objkey.ActivityDate = Convert.ToDateTime(Reminddate2);
                exp180day = Convert.ToDateTime(Reminddate2).AddDays(-180); //js
                exp90day = Convert.ToDateTime(Reminddate2).AddDays(-90);
                exp60day = Convert.ToDateTime(Reminddate2).AddDays(-60);
                exp30day = Convert.ToDateTime(Reminddate2).AddDays(-30);
                exp7day = Convert.ToDateTime(Reminddate2).AddDays(-7);
                exp1day = Convert.ToDateTime(Reminddate2).AddDays(-1);
                objkey.Param = 3;
                if (hdnRemindDay11.Value != "0")
                {
                    if (hdnRemindDay11.Value == "180")
                        objkey.ReminderDate = (exp180day);
                    else if (hdnRemindDay11.Value == "90")
                        objkey.ReminderDate = (exp90day);
                    else if (hdnRemindDay11.Value == "60")
                        objkey.ReminderDate = (exp60day);
                    else if (hdnRemindDay11.Value == "30")
                        objkey.ReminderDate = (exp30day);
                    else if (hdnRemindDay11.Value == "7")
                        objkey.ReminderDate = (exp7day);
                    else if (hdnRemindDay11.Value == "1")
                        objkey.ReminderDate = (exp1day);

                    if (hdnRemindDay1User11.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay1User11.Value);
                        objkey.InsertRecord();
                    }
                    if (hdnRemindDay1User21.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay1User21.Value);
                        objkey.InsertRecord();
                    }
                }

                if (hdnRemindDay21.Value != "0")
                {
                    if (hdnRemindDay21.Value == "180")
                        objkey.ReminderDate = (exp180day);
                    else if (hdnRemindDay21.Value == "90")
                        objkey.ReminderDate = (exp90day);
                    else if (hdnRemindDay21.Value == "60")
                        objkey.ReminderDate = (exp60day);
                    else if (hdnRemindDay21.Value == "30")
                        objkey.ReminderDate = (exp30day);
                    else if (hdnRemindDay21.Value == "7")
                        objkey.ReminderDate = (exp7day);
                    else if (hdnRemindDay21.Value == "1")
                        objkey.ReminderDate = (exp1day);

                    if (hdnRemindDay2User11.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay2User11.Value);
                        objkey.InsertRecord();
                    }
                    if (hdnRemindDay2User21.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay2User21.Value);
                        objkey.InsertRecord();
                    }
                }

                if (hdnRemindDay31.Value != "0")
                {
                    if (hdnRemindDay31.Value == "180")
                        objkey.ReminderDate = (exp180day);
                    else if (hdnRemindDay31.Value == "90")
                        objkey.ReminderDate = (exp90day);
                    else if (hdnRemindDay31.Value == "60")
                        objkey.ReminderDate = (exp60day);
                    else if (hdnRemindDay31.Value == "30")
                        objkey.ReminderDate = (exp30day);
                    else if (hdnRemindDay31.Value == "7")
                        objkey.ReminderDate = (exp7day);
                    else if (hdnRemindDay31.Value == "1")
                        objkey.ReminderDate = (exp1day);

                    if (hdnRemindDay3User11.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay3User11.Value);
                        objkey.InsertRecord();
                    }
                    if (hdnRemindDay3User21.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay3User21.Value);
                        objkey.InsertRecord();
                    }
                }

                //int ContarctID = Convert.ToInt32(ViewState["ContractID"]);
                //objkey.ContractID = ContarctID;
                //objkey.Param = 6;
                //objkey.RequestId = Convert.ToInt32(HdnRequestID.Value);
                objkey.MetaDataFieldId = Convert.ToInt32(ViewState["FieldTypeId2"]);
                //objkey.InsertRecord();

                objkey.Param = 5;
                objkey.RequestId = int.Parse(HdnRequestID.Value);
                if (hdnRemindDay11.Value != "0")
                {
                    objkey.ReminderDays = Convert.ToInt32(hdnRemindDay11.Value);
                    objkey.ReminderUser1 = Convert.ToInt32(hdnRemindDay1User11.Value);
                    objkey.ReminderUser2 = Convert.ToInt32(hdnRemindDay1User21.Value);
                    objkey.InsertRecord();
                }
                if (hdnRemindDay21.Value != "0")
                {
                    objkey.ReminderDays = Convert.ToInt32(hdnRemindDay21.Value);
                    objkey.ReminderUser1 = Convert.ToInt32(hdnRemindDay2User11.Value);
                    objkey.ReminderUser2 = Convert.ToInt32(hdnRemindDay2User21.Value);
                    objkey.InsertRecord();
                }
                if (hdnRemindDay31.Value != "0")
                {
                    objkey.ReminderDays = Convert.ToInt32(hdnRemindDay31.Value);
                    objkey.ReminderUser1 = Convert.ToInt32(hdnRemindDay3User11.Value);
                    objkey.ReminderUser2 = Convert.ToInt32(hdnRemindDay3User21.Value);
                    objkey.InsertRecord();
                }

            }
            #endregion

            #region Custom Reminder date 3
            if (Reminddate3 != "")
            {
                objkey.ActivityText = hdnReminderName3.Value + " reminder on-" + Reminddate3;
                objkey.ActivityDate = Convert.ToDateTime(Reminddate3);
                exp180day = Convert.ToDateTime(Reminddate3).AddDays(-180); //js
                exp90day = Convert.ToDateTime(Reminddate3).AddDays(-90);
                exp60day = Convert.ToDateTime(Reminddate3).AddDays(-60);
                exp30day = Convert.ToDateTime(Reminddate3).AddDays(-30);
                exp7day = Convert.ToDateTime(Reminddate3).AddDays(-7);
                exp1day = Convert.ToDateTime(Reminddate3).AddDays(-1);
                objkey.Param = 3;
                if (hdnRemindDay12.Value != "0")
                {
                    if (hdnRemindDay12.Value == "180")
                        objkey.ReminderDate = (exp180day);
                    else if (hdnRemindDay12.Value == "90")
                        objkey.ReminderDate = (exp90day);
                    else if (hdnRemindDay12.Value == "60")
                        objkey.ReminderDate = (exp60day);
                    else if (hdnRemindDay12.Value == "30")
                        objkey.ReminderDate = (exp30day);
                    else if (hdnRemindDay12.Value == "7")
                        objkey.ReminderDate = (exp7day);
                    else if (hdnRemindDay12.Value == "1")
                        objkey.ReminderDate = (exp1day);

                    if (hdnRemindDay1User12.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay1User12.Value);
                        objkey.InsertRecord();
                    }
                    if (hdnRemindDay1User22.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay1User22.Value);
                        objkey.InsertRecord();
                    }
                }

                if (hdnRemindDay22.Value != "0")
                {
                    if (hdnRemindDay22.Value == "180")
                        objkey.ReminderDate = (exp180day);
                    else if (hdnRemindDay22.Value == "90")
                        objkey.ReminderDate = (exp90day);
                    else if (hdnRemindDay22.Value == "60")
                        objkey.ReminderDate = (exp60day);
                    else if (hdnRemindDay22.Value == "30")
                        objkey.ReminderDate = (exp30day);
                    else if (hdnRemindDay22.Value == "7")
                        objkey.ReminderDate = (exp7day);
                    else if (hdnRemindDay22.Value == "1")
                        objkey.ReminderDate = (exp1day);

                    if (hdnRemindDay2User12.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay2User12.Value);
                        objkey.InsertRecord();
                    }
                    if (hdnRemindDay2User22.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay2User22.Value);
                        objkey.InsertRecord();
                    }
                }

                if (hdnRemindDay32.Value != "0")
                {
                    if (hdnRemindDay32.Value == "180")
                        objkey.ReminderDate = (exp180day);
                    else if (hdnRemindDay32.Value == "90")
                        objkey.ReminderDate = (exp90day);
                    else if (hdnRemindDay32.Value == "60")
                        objkey.ReminderDate = (exp60day);
                    else if (hdnRemindDay32.Value == "30")
                        objkey.ReminderDate = (exp30day);
                    else if (hdnRemindDay32.Value == "7")
                        objkey.ReminderDate = (exp7day);
                    else if (hdnRemindDay32.Value == "1")
                        objkey.ReminderDate = (exp1day);

                    if (hdnRemindDay3User12.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay3User12.Value);
                        objkey.InsertRecord();
                    }
                    if (hdnRemindDay3User22.Value != "0")
                    {
                        objkey.AssignToId = Convert.ToInt32(hdnRemindDay3User22.Value);
                        objkey.InsertRecord();
                    }
                }

                //int ContarctID = Convert.ToInt32(ViewState["ContractID"]);
                //objkey.ContractID = ContarctID;
                //objkey.Param = 6;
                //objkey.RequestId = Convert.ToInt32(HdnRequestID.Value);
                //objkey.MetaDataFieldId = Convert.ToInt32(ViewState["FieldTypeId3"]);
                //objkey.InsertRecord();

                objkey.Param = 5;
                objkey.MetaDataFieldId = Convert.ToInt32(ViewState["FieldTypeId3"]);
                objkey.RequestId = int.Parse(HdnRequestID.Value);
                if (hdnRemindDay12.Value != "0")
                {
                    objkey.ReminderDays = Convert.ToInt32(hdnRemindDay12.Value);
                    objkey.ReminderUser1 = Convert.ToInt32(hdnRemindDay1User12.Value);
                    objkey.ReminderUser2 = Convert.ToInt32(hdnRemindDay1User22.Value);
                    objkey.InsertRecord();
                }
                if (hdnRemindDay22.Value != "0")
                {
                    objkey.ReminderDays = Convert.ToInt32(hdnRemindDay22.Value);
                    objkey.ReminderUser1 = Convert.ToInt32(hdnRemindDay2User12.Value);
                    objkey.ReminderUser2 = Convert.ToInt32(hdnRemindDay2User22.Value);
                    objkey.InsertRecord();
                }
                if (hdnRemindDay32.Value != "0")
                {
                    objkey.ReminderDays = Convert.ToInt32(hdnRemindDay32.Value);
                    objkey.ReminderUser1 = Convert.ToInt32(hdnRemindDay3User12.Value);
                    objkey.ReminderUser2 = Convert.ToInt32(hdnRemindDay3User22.Value);
                    objkey.InsertRecord();
                }
            }
            #endregion

        }
        catch { }
    }

    private List<SelectControlFields> FillMasterData(string TableName)
    {
        try
        {
            ICommonMaster obj = FactoryMaster.GetCommonMasterDetail();
            obj.TableName = TableName;
            List<SelectControlFields> myList = obj.SelectData();
            return myList;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnSaveDraft_Click(object sender, EventArgs e)
    {



    }

    protected void btnGenerateContract_Click(object sender, EventArgs e)
    {
        Credentials credentials = new Credentials();
        // credentials.RequestId = Convert.ToInt32(ViewState["RequestId"]);
        credentials.UserId = int.Parse(Session[Declarations.User].ToString());
        credentials.IP = Session[Declarations.IP].ToString();

        string BulkImportId = credentials.SaveAnswersFromBulkImport(phQuestionnair, true, ddlContractTemplate.SelectedValue);
        if (BulkImportId != "")
        {
            SaveKeyFields(BulkImportId);
            SaveKeyObligationMaster(BulkImportId);
            SaveRequestDetails(BulkImportId);
            //SaveKeyObligations(BulkImportId);
            Page.JavaScriptClientScriptBlock("SaveDraft", "MessageMasterDiv('Manual contract saved successfully.', 0, 100)");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "tytyttrrr", "ReloadMe();", true);
            // Page.JavaScriptClientScriptBlock("SaveDraft", "ReloadMe()");
            //System.Threading.Thread.Sleep(5000);
            // Response.Redirect("../Workflow/AddManualContract.aspx");

            SendEmail(objkey.RequestId); //Metadata changes to key obligation
            SendScheduledEmail(objkey.RequestId);
        }
        else
        { }
    }

    public DataTable SaveData(Control pnl, string matatbl)
    {
        Control ctrl = pnl;
        DataTable dt = new DataTable();
        dt.Columns.Add("MetaDataFieldId", Type.GetType("System.Int32"));
        dt.Columns.Add("FieldValue", Type.GetType("System.String"));
        string InputValue = "", ID = "", Master = "", FieldType = "";

        int FieldTypeId = 0;
        #region GetControlValue
        HtmlTable tbl = (HtmlTable)ctrl.FindControl(matatbl);

        string tableTRclass = "trdynamic";
        if (matatbl == "ImportantDateTable")
            tableTRclass = "trdynamic trimportantDate";

        #region fill answers
        foreach (HtmlTableRow row in tbl.Rows)
        {
            if (row.Attributes["class"] == tableTRclass)
            {
                if (row.Style.Value == null || (row.Style.Value != null && !row.Style.Value.Contains("display:none")))//(row.Visible)
                {
                    Control parentctrl = row.Cells[1];
                    //string Question = row.Cells[0].InnerText;
                    InputValue = "";
                    foreach (var c in parentctrl.Controls.OfType<HtmlInputText>())
                    {
                        InputValue = string.IsNullOrEmpty(Request.Form[c.ID]) ? c.Value : Request.Form[c.ID].ToString();// c.Value;
                        ID = c.ID;
                        FieldTypeId = Convert.ToInt32(c.Attributes["FieldID"]);
                        FieldType = c.Attributes["FieldType"];
                        if (ViewState["FieldTypeId1"] == null)
                            ViewState["FieldTypeId1"] = FieldTypeId;
                        else if (ViewState["FieldTypeId2"] == null)
                            ViewState["FieldTypeId2"] = FieldTypeId;
                        else if (ViewState["FieldTypeId3"] == null)
                            ViewState["FieldTypeId3"] = FieldTypeId;

                        switch (FieldType.ToUpper())
                        {
                            case "TEXT":
                                dt.Rows.Add(FieldTypeId, InputValue);
                                break;
                            case "NUMBER":
                                dt.Rows.Add(FieldTypeId, InputValue);
                                break;
                            case "DATE":
                                if (string.IsNullOrEmpty(InputValue))
                                    dt.Rows.Add(FieldTypeId, InputValue);
                                else
                                    dt.Rows.Add(FieldTypeId, InputValue);
                                break;
                        }
                    }

                    foreach (var c in parentctrl.Controls.OfType<HtmlTextArea>())
                    {
                        InputValue = Request.Form[c.ID].ToString();//c.Value;
                        ID = c.ID;
                        FieldTypeId = Convert.ToInt32(c.Attributes["FieldID"]);
                        FieldType = c.Attributes["FieldType"];
                        dt.Rows.Add(FieldTypeId, InputValue);
                    }

                    foreach (var ddlList in parentctrl.Controls.OfType<HtmlSelect>())
                    {
                        ID = ddlList.ID;
                        FieldTypeId = Convert.ToInt32(ddlList.Attributes["FieldID"]);
                        Master = ddlList.Attributes["master"];
                        InputValue = "";
                        if (string.IsNullOrEmpty(Master))
                        {
                            foreach (ListItem objItem in ddlList.Items)
                            {
                                if (objItem.Selected)
                                {
                                    //  InputValue = string.IsNullOrEmpty(Request.Form[c.ID]) ? c.Value : Request.Form[c.ID].ToString();// c.Value;
                                    InputValue += objItem.Value + ",";
                                }
                            }
                            dt.Rows.Add(FieldTypeId, InputValue);
                        }
                    }
                    foreach (var hdnField in parentctrl.Controls.OfType<HtmlInputHidden>())
                    {
                        ID = hdnField.ID;
                        Master = hdnField.Attributes["master"];
                        InputValue = hdnField.Value;
                        if (!string.IsNullOrEmpty(Master))
                            dt.Rows.Add(FieldTypeId, InputValue);
                    }

                    foreach (var cbList in parentctrl.Controls.OfType<CheckBoxList>())
                    {
                        ID = cbList.ID;
                        FieldTypeId = Convert.ToInt32(cbList.Attributes["FieldID"]);

                        InputValue = "";
                        foreach (ListItem objItem in cbList.Items)
                        {
                            if (objItem.Selected)
                            {
                                InputValue += objItem.Value + ",";
                            }
                        }
                        dt.Rows.Add(FieldTypeId, InputValue.Trim(','));
                    }

                    foreach (var rbList in parentctrl.Controls.OfType<RadioButtonList>())
                    {
                        ID = rbList.ID;
                        FieldTypeId = Convert.ToInt32(rbList.Attributes["FieldID"]);

                        InputValue = rbList.SelectedValue;
                        dt.Rows.Add(FieldTypeId, InputValue);
                    }
                }
            }
        }
        return dt;
        #endregion
        #endregion GetControlValue
    }

    protected void SaveRequestDetails(string BulkImportId)
    {
        IContractRequest objContractRequest = new ContractRequest();

        DataTable dt = SaveData(phQuestionnair, "MetaDataTable");
        // return;

        objContractRequest.BulkImportId = BulkImportId;
        //objContractRequest.isCustomer = rdoCustomer.Checked == true ? "Y" : "N";
        if (rdoCustomer.Checked == true)
            objContractRequest.isCustomer = "Y";
        if (rdoSupplier.Checked == true)
            objContractRequest.isCustomer = "N";
        if (rdoOthers.Checked == true)
            objContractRequest.isCustomer = "O";
        //   objContractRequest.IsCustomerPortalEnable = ddlNotifyCustomer.Value;
        if (SendNotification == "N")
            objContractRequest.IsCustomerPortalEnable = "N";
        else
            objContractRequest.IsCustomerPortalEnable = ddlNotifyCustomer.Value;

        objContractRequest.ContractingpartyId = Convert.ToInt32(ddlContractingParty.extGetSelectedValues());
        objContractRequest.ClientName = txtclientname.Value;
        objContractRequest.Address = txtclientaddress.Text;
        objContractRequest.Street1 = txtclientaddressStreet2.Text;
        objContractRequest.ContactNumber = txtContactNumbers.Value;
        objContractRequest.ContractTerm = txtContractTerm.Value;
        objContractRequest.ContractValue = Convert.ToDecimal(txtContractValue.Value == "" ? "0" : txtContractValue.Value);
        objContractRequest.CountryID = Convert.ToInt32(ddlCountry.extGetSelectedValues());
        objContractRequest.AddedBy = int.Parse(Session[Declarations.User].ToString());
        objContractRequest.Others = OthersText.Value;
        objContractRequest.EmailID = txtEmailId.Value;
        objContractRequest.StateName = ddlState.Value;
        objContractRequest.CityName = ddlcity.Value;
        objContractRequest.Pincode = txtPincode.Value;
        objContractRequest.dtMatadataFieldValues = dt;
        objContractRequest.CurrencyID = Convert.ToInt32(ddlOriginalCurrency.SelectedValue);

        //Added by dk
        if (ddlPrority.Value == "1")
        {
            lblPriorityreason.Visible = true;
            objContractRequest.PriorityReason = txtpriorityremark.Value;
        }
        else
        {
            lblPriorityreason.Visible = false;
            objContractRequest.PriorityReason = "";
        }
        switch (ddlPrority.Value)
        {
            case "0":
                objContractRequest.Priority = "Y/N";
                break;
            case "1":
                objContractRequest.Priority = "Y";
                break;
            case "2":
                objContractRequest.Priority = "N";
                break;
        }

        // END


        objContractRequest.InsertManualEntryDetails();

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Server.Transfer("~/BulkImport/BulkImportUploadData.aspx");
    }

    protected void SaveKeyObligationMaster(string RequestId)
    {
        try
        {
            DataTable dt = SaveData(phQuestionnair, "keyObligationTable");
            objkey.LiabilityCap = txtLiabilityCap.Value;
            objkey.Indemnity = txtIndemnity.Value;
            objkey.Insurance = txtInsurance.Value;
            objkey.AssignmentNovation = txtNovation.Value;
            objkey.TerminationDescription = txtTerminationdesc.Value;
            objkey.Strictliability = txtStrictliability.Value;
            objkey.dtMatadataFieldValues = dt;
            if (chkIsTerminationConvenience.Checked == true)
            {
                objkey.IsTerminationConvenience = "Y";
            }
            else { objkey.IsTerminationConvenience = "N"; }
            if (chkIsTerminationmaterial.Checked == true)
            {
                objkey.IsTerminationmaterial = "Y";
            }
            else { objkey.IsTerminationmaterial = "N"; }
            if (chkIsTerminationinsolvency.Checked == true)
            {
                objkey.IsTerminationinsolvency = "Y";
            }
            else { objkey.IsTerminationinsolvency = "N"; }
            if (chkIsTerminationinChangeofcontrol.Checked == true)
            {
                objkey.IsTerminationinChangeofcontrol = "Y";
            }
            else { objkey.IsTerminationinChangeofcontrol = "N"; }


            if (rdIsChangeofControlYes.Checked == true)
            {
                objkey.IsChangeofControl = "Y";
            }
            else { objkey.IsChangeofControl = "N"; }

            if (rdIsStrictliabilityYes.Checked == true)
            {
                objkey.IsStrictliability = "Y";
            }
            else { objkey.IsStrictliability = "N"; }

            if (rdIsGuaranteeRequiredYes.Checked == true)
            {
                objkey.IsGuaranteeRequired = "Y";
            }
            else { objkey.IsGuaranteeRequired = "N"; }

            if (rdIsAgreementClauseYes.Checked == true)
            {
                objkey.IsAgreementClause = "Y";
            }
            else { objkey.IsAgreementClause = "N"; }

            objkey.Param = 2;
            objkey.AddedBy = int.Parse(Session[Declarations.User].ToString());
            objkey.RequestId = int.Parse(RequestId);
            objkey.IsManualEntry = "Y";
            objkey.InsertRecord();
        }
        catch (Exception ex)
        {

        }
    }

    protected void SaveKeyFields(string RequestId)
    {
        objkey = FactoryWorkflow.GetKeyFieldsDetails();
        DataTable dt = SaveData(phQuestionnair, "ImportantDateTable");
        try
        {
            if (txteffectivedate.Value != "")
            {
                objkey.EffectiveDate = Convert.ToDateTime(txteffectivedate.Value);
            }

            if (txtexpirationdate.Value != "")
            {
                objkey.ExpirationDate = Convert.ToDateTime(txtexpirationdate.Value);
            }

            if (txtrenewaldate.Value != "")
            {
                objkey.RenewalDate = Convert.ToDateTime(txtrenewaldate.Value);
            }

            #region ddlRemindExpiration1
            if (Convert.ToInt32(ddlRemindExpiration1.Value) == 180)
            {
                objkey.ExpirationAlert180days = "Y";
                objkey.ExpirationAlert180daysUserID1 = Convert.ToInt32(ddlexp180Users1.Value); //js
                objkey.ExpirationAlert180daysUserID2 = Convert.ToInt32(ddlexp180Users2.Value); //js
            }
            else
            {
                objkey.ExpirationAlert180days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration1.Value) == 90)
            {
                objkey.ExpirationAlert90days = "Y";
                objkey.ExpirationAlert90daysUserID1 = Convert.ToInt32(ddlexp180Users1.Value); //js
                objkey.ExpirationAlert90daysUserID2 = Convert.ToInt32(ddlexp180Users2.Value); //js
            }
            else
            {
                objkey.ExpirationAlert90days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration1.Value) == 60)
            {
                objkey.ExpirationAlert60days = "Y";
                objkey.ExpirationAlert60daysUserID1 = Convert.ToInt32(ddlexp180Users1.Value); //js
                objkey.ExpirationAlert60daysUserID2 = Convert.ToInt32(ddlexp180Users2.Value); //js
            }
            else
            {
                objkey.ExpirationAlert60days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration1.Value) == 30)
            {
                objkey.ExpirationAlert30days = "Y";
                objkey.ExpirationAlert30daysUserID1 = Convert.ToInt32(ddlexp180Users1.Value); //js
                objkey.ExpirationAlert30daysUserID2 = Convert.ToInt32(ddlexp180Users2.Value); //js
            }
            else
            {
                objkey.ExpirationAlert30days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration1.Value) == 7)
            {
                objkey.ExpirationAlert7days = "Y";
                objkey.ExpirationAlert7daysUserID1 = Convert.ToInt32(ddlexp180Users1.Value); //js
                objkey.ExpirationAlert7daysUserID2 = Convert.ToInt32(ddlexp180Users2.Value); //js
            }
            else
            {
                objkey.ExpirationAlert7days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration1.Value) == 1)
            {
                objkey.ExpirationAlert24Hrs = "Y";
                objkey.ExpirationAlert24HrsUserID1 = Convert.ToInt32(ddlexp180Users1.Value); //js
                objkey.ExpirationAlert24HrsUserID2 = Convert.ToInt32(ddlexp180Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert24Hrs != "Y")
                    objkey.ExpirationAlert24Hrs = "N";
            }
            #endregion

            #region ddlRemindExpiration2
            if (Convert.ToInt32(ddlRemindExpiration2.Value) == 180)
            {
                objkey.ExpirationAlert180days = "Y";
                objkey.ExpirationAlert180daysUserID1 = Convert.ToInt32(ddlexp90Users1.Value); //js
                objkey.ExpirationAlert180daysUserID2 = Convert.ToInt32(ddlexp90Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert180days != "Y")
                    objkey.ExpirationAlert180days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration2.Value) == 90)
            {
                objkey.ExpirationAlert90days = "Y";
                objkey.ExpirationAlert90daysUserID1 = Convert.ToInt32(ddlexp90Users1.Value); //js
                objkey.ExpirationAlert90daysUserID2 = Convert.ToInt32(ddlexp90Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert90days != "Y")
                    objkey.ExpirationAlert90days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration2.Value) == 60)
            {
                objkey.ExpirationAlert60days = "Y";
                objkey.ExpirationAlert60daysUserID1 = Convert.ToInt32(ddlexp90Users1.Value); //js
                objkey.ExpirationAlert60daysUserID2 = Convert.ToInt32(ddlexp90Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert60days != "Y")
                    objkey.ExpirationAlert60days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration2.Value) == 30)
            {
                objkey.ExpirationAlert30days = "Y";
                objkey.ExpirationAlert30daysUserID1 = Convert.ToInt32(ddlexp90Users1.Value); //js
                objkey.ExpirationAlert30daysUserID2 = Convert.ToInt32(ddlexp90Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert30days != "Y")
                    objkey.ExpirationAlert30days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration2.Value) == 7)
            {
                objkey.ExpirationAlert7days = "Y";
                objkey.ExpirationAlert7daysUserID1 = Convert.ToInt32(ddlexp90Users1.Value); //js
                objkey.ExpirationAlert7daysUserID2 = Convert.ToInt32(ddlexp90Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert7days != "Y")
                    objkey.ExpirationAlert7days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration2.Value) == 1)
            {
                objkey.ExpirationAlert24Hrs = "Y";
                objkey.ExpirationAlert24HrsUserID1 = Convert.ToInt32(ddlexp90Users1.Value); //js
                objkey.ExpirationAlert24HrsUserID2 = Convert.ToInt32(ddlexp90Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert24Hrs != "Y")
                    objkey.ExpirationAlert24Hrs = "N";
            }
            #endregion

            #region ddlRemindExpiration3
            if (Convert.ToInt32(ddlRemindExpiration3.Value) == 180)
            {
                objkey.ExpirationAlert180days = "Y";
                objkey.ExpirationAlert180daysUserID1 = Convert.ToInt32(ddlexp60Users1.Value); //js
                objkey.ExpirationAlert180daysUserID2 = Convert.ToInt32(ddlexp60Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert180days != "Y")
                    objkey.ExpirationAlert180days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration3.Value) == 90)
            {
                objkey.ExpirationAlert90days = "Y";
                objkey.ExpirationAlert90daysUserID1 = Convert.ToInt32(ddlexp60Users1.Value); //js
                objkey.ExpirationAlert90daysUserID2 = Convert.ToInt32(ddlexp60Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert90days != "Y")
                    objkey.ExpirationAlert90days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration3.Value) == 60)
            {
                objkey.ExpirationAlert60days = "Y";
                objkey.ExpirationAlert60daysUserID1 = Convert.ToInt32(ddlexp60Users1.Value); //js
                objkey.ExpirationAlert60daysUserID2 = Convert.ToInt32(ddlexp60Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert60days != "Y")
                    objkey.ExpirationAlert60days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration3.Value) == 30)
            {
                objkey.ExpirationAlert30days = "Y";
                objkey.ExpirationAlert30daysUserID1 = Convert.ToInt32(ddlexp60Users1.Value); //js
                objkey.ExpirationAlert30daysUserID2 = Convert.ToInt32(ddlexp60Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert30days != "Y")
                    objkey.ExpirationAlert30days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration3.Value) == 7)
            {
                objkey.ExpirationAlert7days = "Y";
                objkey.ExpirationAlert7daysUserID1 = Convert.ToInt32(ddlexp60Users1.Value); //js
                objkey.ExpirationAlert7daysUserID2 = Convert.ToInt32(ddlexp60Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert7days != "Y")
                    objkey.ExpirationAlert7days = "N";
            }
            if (Convert.ToInt32(ddlRemindExpiration3.Value) == 1)
            {
                objkey.ExpirationAlert24Hrs = "Y";
                objkey.ExpirationAlert24HrsUserID1 = Convert.ToInt32(ddlexp60Users1.Value); //js
                objkey.ExpirationAlert24HrsUserID2 = Convert.ToInt32(ddlexp60Users2.Value); //js
            }
            else
            {
                if (objkey.ExpirationAlert24Hrs != "Y")
                    objkey.ExpirationAlert24Hrs = "N";
            }
            #endregion

            #region ddlRemindRenewal1
            if (Convert.ToInt32(ddlRemindRenewal1.Value) == 180)
            {
                objkey.RenewalAlert180days = "Y";
                objkey.RenewalAlert180daysUserID1 = Convert.ToInt32(ddlRenewal180User1.Value); //js
                objkey.RenewalAlert180daysUserID2 = Convert.ToInt32(ddlRenewal180User2.Value); //js
            }
            else
            {
                objkey.RenewalAlert180days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal1.Value) == 90)
            {
                objkey.RenewalAlert90days = "Y";
                objkey.RenewalAlert90daysUserID1 = Convert.ToInt32(ddlRenewal180User1.Value); //js
                objkey.RenewalAlert90daysUserID2 = Convert.ToInt32(ddlRenewal180User2.Value); //js
            }
            else
            {
                objkey.RenewalAlert90days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal1.Value) == 60)
            {
                objkey.RenewalAlert60days = "Y";
                objkey.RenewalAlert60daysUserID1 = Convert.ToInt32(ddlRenewal180User1.Value); //js
                objkey.RenewalAlert60daysUserID2 = Convert.ToInt32(ddlRenewal180User2.Value); //js
            }
            else
            {
                objkey.RenewalAlert60days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal1.Value) == 30)
            {
                objkey.RenewalAlert30days = "Y";
                objkey.RenewalAlert30daysUserID1 = Convert.ToInt32(ddlRenewal180User1.Value); //js
                objkey.RenewalAlert30daysUserID2 = Convert.ToInt32(ddlRenewal180User2.Value); //js
            }
            else
            {
                objkey.RenewalAlert30days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal1.Value) == 7)
            {
                objkey.RenewalAlert7days = "Y";
                objkey.RenewalAlert7daysUserID1 = Convert.ToInt32(ddlRenewal180User1.Value); //js
                objkey.RenewalAlert7daysUserID2 = Convert.ToInt32(ddlRenewal180User2.Value); //js
            }
            else
            {
                objkey.RenewalAlert7days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal1.Value) == 1)
            {
                objkey.RenewalAlert24Hrs = "Y";
                objkey.RenewalAlert24HrsUserID1 = Convert.ToInt32(ddlRenewal180User1.Value); //js
                objkey.RenewalAlert24HrsUserID2 = Convert.ToInt32(ddlRenewal180User2.Value); //js
            }
            else
            {
                objkey.RenewalAlert24Hrs = "N";
            }
            #endregion

            #region ddlRemindRenewal2
            if (Convert.ToInt32(ddlRemindRenewal2.Value) == 180)
            {
                objkey.RenewalAlert180days = "Y";
                objkey.RenewalAlert180daysUserID1 = Convert.ToInt32(ddlRenewal90User1.Value); //js
                objkey.RenewalAlert180daysUserID2 = Convert.ToInt32(ddlRenewal90User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert180days != "Y")
                    objkey.RenewalAlert180days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal2.Value) == 90)
            {
                objkey.RenewalAlert90days = "Y";
                objkey.RenewalAlert90daysUserID1 = Convert.ToInt32(ddlRenewal90User1.Value); //js
                objkey.RenewalAlert90daysUserID2 = Convert.ToInt32(ddlRenewal90User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert90days != "Y")
                    objkey.RenewalAlert90days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal2.Value) == 60)
            {
                objkey.RenewalAlert60days = "Y";
                objkey.RenewalAlert60daysUserID1 = Convert.ToInt32(ddlRenewal90User1.Value); //js
                objkey.RenewalAlert60daysUserID2 = Convert.ToInt32(ddlRenewal90User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert60days != "Y")
                    objkey.RenewalAlert60days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal2.Value) == 30)
            {
                objkey.RenewalAlert30days = "Y";
                objkey.RenewalAlert30daysUserID1 = Convert.ToInt32(ddlRenewal90User1.Value); //js
                objkey.RenewalAlert30daysUserID2 = Convert.ToInt32(ddlRenewal90User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert30days != "Y")
                    objkey.RenewalAlert30days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal2.Value) == 7)
            {
                objkey.RenewalAlert7days = "Y";
                objkey.RenewalAlert7daysUserID1 = Convert.ToInt32(ddlRenewal90User1.Value); //js
                objkey.RenewalAlert7daysUserID2 = Convert.ToInt32(ddlRenewal90User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert7days != "Y")
                    objkey.RenewalAlert7days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal2.Value) == 1)
            {
                objkey.RenewalAlert24Hrs = "Y";
                objkey.RenewalAlert24HrsUserID1 = Convert.ToInt32(ddlRenewal90User1.Value); //js
                objkey.RenewalAlert24HrsUserID2 = Convert.ToInt32(ddlRenewal90User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert24Hrs != "Y")
                    objkey.RenewalAlert24Hrs = "N";
            }
            #endregion

            #region ddlRemindRenewal3
            if (Convert.ToInt32(ddlRemindRenewal3.Value) == 180)
            {
                objkey.RenewalAlert180days = "Y";
                objkey.RenewalAlert180daysUserID1 = Convert.ToInt32(ddlRenewal60User1.Value); //js
                objkey.RenewalAlert180daysUserID2 = Convert.ToInt32(ddlRenewal60User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert180days != "Y")
                    objkey.RenewalAlert180days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal3.Value) == 90)
            {
                objkey.RenewalAlert90days = "Y";
                objkey.RenewalAlert90daysUserID1 = Convert.ToInt32(ddlRenewal60User1.Value); //js
                objkey.RenewalAlert90daysUserID2 = Convert.ToInt32(ddlRenewal60User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert90days != "Y")
                    objkey.RenewalAlert90days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal3.Value) == 60)
            {
                objkey.RenewalAlert60days = "Y";
                objkey.RenewalAlert60daysUserID1 = Convert.ToInt32(ddlRenewal60User1.Value); //js
                objkey.RenewalAlert60daysUserID2 = Convert.ToInt32(ddlRenewal60User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert60days != "Y")
                    objkey.RenewalAlert60days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal3.Value) == 30)
            {
                objkey.RenewalAlert30days = "Y";
                objkey.RenewalAlert30daysUserID1 = Convert.ToInt32(ddlRenewal60User1.Value); //js
                objkey.RenewalAlert30daysUserID2 = Convert.ToInt32(ddlRenewal60User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert30days != "Y")
                    objkey.RenewalAlert30days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal3.Value) == 7)
            {
                objkey.RenewalAlert7days = "Y";
                objkey.RenewalAlert7daysUserID1 = Convert.ToInt32(ddlRenewal60User1.Value); //js
                objkey.RenewalAlert7daysUserID2 = Convert.ToInt32(ddlRenewal60User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert7days != "Y")
                    objkey.RenewalAlert7days = "N";
            }
            if (Convert.ToInt32(ddlRemindRenewal3.Value) == 1)
            {
                objkey.RenewalAlert24Hrs = "Y";
                objkey.RenewalAlert24HrsUserID1 = Convert.ToInt32(ddlRenewal60User1.Value); //js
                objkey.RenewalAlert24HrsUserID2 = Convert.ToInt32(ddlRenewal60User2.Value); //js
            }
            else
            {
                if (objkey.RenewalAlert24Hrs != "Y")
                    objkey.RenewalAlert24Hrs = "N";
            }
            #endregion

            objkey.dtMatadataFieldValues = dt;
            objkey.Param = 1;
            HdnRequestID.Value = Convert.ToString(RequestId);
            objkey.RequestId = int.Parse(HdnRequestID.Value);
            objkey.AddedBy = int.Parse(Session[Declarations.User].ToString());
            objkey.IsManualEntry = "Y";
            objkey.InsertRecord();
            SaveUserInActivityTable();//For add details in Activity table
        }
        catch (Exception ex)
        {
        }
    }

    [WebMethod]
    public static List<ListItem> LoadUsers(string ContractID)
    {
        try
        {
            List<ListItem> us = new List<ListItem>();
            IKeyFields objusers = FactoryWorkflow.GetKeyFieldsDetails();
            objusers.ContractTypeId = Convert.ToInt32(ContractID); ;
            DataTable dt = objusers.GetUsers();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                us.Add(new ListItem
                {
                    Value = Convert.ToString(dt.Rows[i]["UsersId"]),
                    Text = Convert.ToString(dt.Rows[i]["FullName"]),
                });
            }

            return us;
        }
        catch (Exception)
        {
            throw;
        }
    }

    void SendEmail()
    {

    }

    protected void ddlContractTemplate_SelectedIndexChanged(object sender, EventArgs e)
    {
        btnGenerateContract.Visible = true;
        SetDefault();
        GetDynamicControls(ddlContractTemplate.SelectedValue);
        //vivek code start 
        objcontractreq = new ContractRequest();
        objcontractreq.ContractTemplateId = Convert.ToInt32(ddlContractTemplate.SelectedValue);
        DataTable dt = new DataTable();
        dt = objcontractreq.GetContractTypeIDByTemplateID();
        if (dt.Rows.Count > 0)
        {
            ViewState["ContractTypeId"] = dt.Rows[0]["ContractTypeId"];
            hdnContractypeId.Value = Convert.ToString(ViewState["ContractTypeId"]);
        }
        BindUser();
        //vivek code end

        if (ddlContractTemplate.SelectedValue != "0")
        {
            dvKeyObligations.Style.Add("display", "block");
            boxKyObligation.Style.Add("display", "block");
        }
        else
        {
            dvKeyObligations.Style.Add("display", "none");
            boxKyObligation.Style.Add("display", "none");
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "abmsg", " HideShowPReason();", true);

    }

    void SendEmail(int RequestId)
    {
        try
        {
            Page.TraceWrite("send emails.");
            EmailTemplateBLL.IEmailTrigger objEmail = EmailTemplateBLL.FactoryEmailTemplate.GetEmailTriggerDetail();
            objEmail.RequestId = RequestId;
            objEmail.ContractTypeId = 99999;
            objEmail.ContractTemplateId = 99999;
            if (Session[Declarations.User] != null)
            {
                objEmail.userId = int.Parse(Session[Declarations.User].ToString());
                objEmail.EmailTriggerId = 3;
                objEmail.BulkEmail();
            }
        }
        catch (Exception)
        {
            Page.TraceWarn("send emails fail.");
        }
    }

    void SendScheduledEmail(int RequestId)
    {
        try
        {
            Page.TraceWrite("send scheduled emails.");
            EmailTemplateBLL.IEmailTrigger objEmail = EmailTemplateBLL.FactoryEmailTemplate.GetEmailTriggerDetail();
            objEmail.RequestId = RequestId;
            //            objEmail.AlertDate = DateTime.Now;
            if (Session[Declarations.User] != null)
            {
                objEmail.userId = int.Parse(Session[Declarations.User].ToString());
                objEmail.BulkEmailScheduler();
            }
        }
        catch (Exception)
        {
            Page.TraceWarn("send emails fail.");
        }
    }

    void NotificationVisibility()
    {
        Access.PageAccess("CustomerPortal", Session[Declarations.User].ToString());
        if (Access.View != "Y")
        {
            trNotification.Style.Add("display", "none");
            SendNotification = "N";
            // ddlNotifyCustomer.Items.FindByText("No").Selected = true;
        }
        else
            trNotification.Style.Add("display", "");
    }
}