﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="VaultFlow.aspx.cs" Inherits="Workflow_VaultFlow" %>

<%@ Register Src="../UserControl/requestnewlinks.ascx" TagName="requestnewlinks"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/activities.ascx" TagName="activitiescontrol" TagPrefix="uc2" %>
<%@ Register Src="~/UserControl/KeyFields.ascx" TagName="KeyFieldscontrol" TagPrefix="uckey" %>
<%@ Register Src="~/UserControl/KeyObligations.ascx" TagName="KeyFieldscontrol" TagPrefix="ucobligkey" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc5" %>



<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Font.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../UploadJs/js/jquery.plupload.queue/css/jquery.plupload.queue.css"
        type="text/css" media="screen" />
    <script src="../scripts/CommonValidations.js" type="text/javascript"></script>
    <script src="../scripts/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    <link href="../Styles/jquery-ui-1.8.19.custom.css" rel="stylesheet" type="text/css" />

    <link href="../Styles/application.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/stylesheet.css" rel="stylesheet" type="text/css" />


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server" ClientIDMode="Static">


    <div id="rightlinks" style="display: none;">
        <uc5:Masterlinks ID="rightactions" runat="server" />
    </div>
     <script type="text/javascript">
         $("#sidebar").html($("#rightlinks").html());
    </script>

    <asp:HiddenField ID="hdnActivityMessageFlag" ClientIDMode="Static" runat="server" />
     <asp:HiddenField ID="isFileGenereated" ClientIDMode="Static" runat="server" />
    <script language="javascript" type="text/javascript">
        $("#requestLink").addClass("menulink");
        $("#requesttab").addClass("selectedtab");
    </script>
    <script type="text/javascript">
        $('#ulWorkflowTab,#ulActivityTab,#ulDocumentTab,#ulKeyFields,#ulKeyObligations').live('click', function () {
            if ($(this).attr('id') == 'ulWorkflowTab') {
                $('#ddlContractTemplate').addClass('required');
            }
            else {
                $('#ddlContractTemplate').removeClass('required');
            }
        });

        function scrollToAnchor(aid) {
            var aTag = $("div[id='" + aid + "']");
            $('html,body').animate({ scrollTop: aTag.offset().top }, 'slow');
        }

        var Counter = 0;
        function CallMe(obj) {
            $(obj).closest('li').find('div[class="plupload_file_DocumentType"]').find('input').val($(obj).val());
            if ($(obj).val() != '0') {

                $(obj).closest('li').find('div:eq(4)').find('select').next('div[id="dvFont"]').remove();
            }
            else {
                $(obj).closest('li').find('div:eq(4)').find('select').after('<div id="dvFont" style="margin-left:205px;text-align:left"><font color="red">Please select document Type</font></div>');
            }
        }
        $(document).ready(function () {
            var selectedTab = '<%=Request.QueryString["selectedTab"] %>';
            if (selectedTab != "") {
                $("#" + selectedTab.split('_')[0]).trigger("click");
                scrollToAnchor("activity");

                if ($('#hdnActivityMessageFlag').val() != '') {
                    var msg = $('#hdnActivityMessageFlag').val();
                    SectionMessageDiv('msgActivity', msg, 0, 100);
                    $('#hdnActivityMessageFlag').val('');
                }

            }

            var Mode = '<%=Request.QueryString["Mode"] %>';
            var ScrollTo = '<%=Request.QueryString["ScrollTo"] %>';
            if (ScrollTo != "")
                scrollToAnchor(ScrollTo);

            var Section = '<%=Request.QueryString["Section"] %>';
            if (Section != "")
                showMessage(Section, Mode);

            var obj = $("#ParentDiv");
            obj.on('dragenter', function (e) {
                e.stopPropagation();
                e.preventDefault();
            });
            obj.on('dragover', function (e) {
                e.stopPropagation();
                e.preventDefault();
            });
            obj.on('drop', function (e) {
                e.preventDefault();
                var files = e.originalEvent.target.files || (e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files);
                handleFileUpload(files, obj);
            });

            $('#no').click(function () {
                $.unblockUI();
                return false;
            });
        });

        function CloseUploader() {
            $.unblockUI();
            $('a.onPage').css('margin-top', '32px');
            $('.OnPageul').css('margin-top', '-75px');
            return false;
        }

        function showMessage(section, Mode) {
            var DivId, msg, flg;
            switch (section) {
                case "Approvals":
                    DivId = "msgApprovals";
                    msg = "Approval status updated.";
                    break;
                case "QA":
                    DivId = "msgQuestionnaire";
                    msg = "Questionnaire updated.";
                    break;
                case "ContractVersions":
                    DivId = "msgContractVersions";
                    if (Mode == "CheckOut")
                        msg = "Check-out done successfully.";
                    else if (Mode == "CheckIn")
                        msg = "Check-in done successfully.";
                    else if (Mode == "Delete")
                        msg = "Contract file deleted successfully.";
                    else if (Mode == "TrackChanges")
                        msg = "Contract file created successfully.";
                    else if (Mode == "DocuSign")
                        msg = "Document sent successfully for signature.";
                    else
                        msg = "Contract document uploaded successfully.";
                    break;
                case "Documents":
                    scrollToAnchor("tab-content-document");
                    DivId = "msgDocuments";
                    if (Mode == "Upload")
                        msg = "Document uploaded successfully.";
                    else if (Mode == "Delete")
                        msg = "Document deleted successfully.";
                    break;


                case "KeyField":
                    scrollToAnchor("tab-content-keyfields");
                    DivId = "msgkeyfields";
                    msg = "Important dates uploaded successfully.";
                    break;

                case "KeyObligation":
                    scrollToAnchor("tab-content-keyobligations");
                    DivId = "msgkeyobligations";
                    msg = "Key Obligation uploaded successfully.";
                    break;

            }
            SectionMessageDiv(DivId, msg, 0, 100);
        }
    
    </script>

       <table id="statusTable" runat="server">
         
        </table>
        <asp:HiddenField ID="hdnMalAlreadySent" Value="" runat="server" />

    <div class="contextual" style="font-size: small; font-weight: bold">
        Status :
        <asp:Label ID="lblContractStatusName" runat="server" Text=""></asp:Label>
    </div>
    <%#Eval("OrigionalFileName") %>
    <span><h3><asp:Label ID="lblContractIDHeader" runat="server" Text="" ClientIDMode="Static"></asp:Label></h3><asp:Label ID="lblRequestId" runat="server" Text="" ClientIDMode="Static" style="display:none"></asp:Label></span>
    <select name="BB" style="display: none" id="ddlDocumentType" runat="server" onchange="SetContractTypeValue(this)">
    </select>
    <%--TAB REQUEST DETAILS START--%>
    <div class="issue status-1 priority-3 child created-by-me details">
        <div class="links-rightaligned wiki editable" style="display:none" id="divEditRequest" runat="server" clientidmode="Static">
            <div>
                <a href='<%="ContractRequest.aspx?RequestId="+ ViewState["RequestId"] %>' title="Edit Request">
                    <img alt="Edit" src="../images/edit.png?1349001717" /> Edit
                </a>
            </div>
            &nbsp;&nbsp;
        </div>
        <div class="subject">
            <h3>
                Snapshot                
            </h3>
        </div>
        <p class="author">
            <asp:Label ID="lblRequestDetails" runat="server" Style="word-wrap: break-word;" Text=""></asp:Label>
        </p>
         <table style="vertical-align: top; width: 100%" border="0">
            <tbody>
                <tr>
                    <th style="width: 10%; text-align: right" class="search-filter-label">
                        Request Id :
                    </th>
                    <td style="width: 20%;">
                        <%= ViewState["RequestId"] %>
                    </td>
                    <th style="width: 10%; text-align: right" class="search-filter-label">
                        Request Type :
                    </th>
                    <td style="width: 40%;">
                        <asp:Label ID="lblRequestType" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <th style="width: 10%; text-align: right"class="search-filter-label">
                        Contract ID :
                    </th>
                    <td>
                       <asp:Label ID="lblContractId" runat="server" Text=""></asp:Label>
                    </td>
                   <th style="width: 10%; text-align: right"class="search-filter-label">
                        Contract Type :
                    </th>
                    <td style="width: 20%;">
                        <asp:Label ID="lblContractType" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                     <th style="width: 10%; text-align: right"class="search-filter-label">
                        Assign To :
                    </th>
                    <td>
                        <asp:Label ID="lblAssignedTo" runat="server" Text=""></asp:Label>
                    </td>
                    <th style="width: 10%; text-align: right" class="search-filter-label">
                        Deadline Date :
                    </th>
                    <td style="width: 40%;">
                        <asp:Label ID="lblDeadLineDate" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                
                <tr runat="server" id="trEstimatedValue" clientidmode="Static" class="search-filter-label">
                    <th style="width: 10%; text-align: right">
                        Estimated value :
                    </th>
                    <td>
                        <asp:Label ID="lblEstimatedValue" runat="server" Text=""></asp:Label>
                    </td>
                    <th>
                    </th>
                    <td>
                    </td>
                </tr>

                <tr style="display:none">
                    <td style="text-align: left; width: 100%" colspan="5">
                        <hr />
                    </td>
                </tr>

                <tr style="display:none">
                    <td style="text-align: left" colspan="4">
                        <strong>Customer/Supplier Details</strong>
                    </td>
                </tr>
                <tr style="display:none">
                    <th style="width: 10%; text-align: right; vertical-align: top;">
                        Name :
                    </th>
                    <td style="vertical-align: top;">
                        <asp:Label ID="lblClientNames" runat="server" Text=""></asp:Label>
                    </td>
                    <th style="width: 10%; text-align: right">
                        Email Id :
                    </th>
                    <td style="vertical-align: top;">
                        <asp:Label ID="lblClientEmailId" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr style="display:none" runat="server" id="tr1" clientidmode="Static">
                    <th style="width: 10%; text-align: right; vertical-align: top;">
                        Address :
                    </th>
                    <td style="vertical-align: top;">
                        <asp:Label ID="lblClientAddress" runat="server" Text=""></asp:Label>
                    </td>
                    <th style="width: 10%; text-align: right; vertical-align: top;">
                        Contact Number :
                    </th>
                    <td style="vertical-align: top;">
                        <asp:Label ID="lblClientMobile" runat="server" Text=""></asp:Label>
                    </td>
                </tr>


                <tr runat="server" id="divReqDescrHR" clientidmode="Static">
                    <td style="text-align: left; width: 100%" colspan="5">
                        <hr />
                    </td>
                </tr>
                <tr runat="server" id="divReqDescrTITLE" clientidmode="Static">
                    <td style="text-align: left" colspan="4">
                        <strong>Request Description</strong>
                    </td>
                </tr>
                <tr runat="server" id="divReqDescr" clientidmode="Static">
                    <td style="vertical-align: top;" colspan="5">
                        <asp:Label ID="lblRequestDescription" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr runat="server" id="trLatestContractFileHR" clientidmode="Static">
                    <td style="text-align: left; width: 100%" colspan="5">
                        <hr />
                    </td>
                </tr>


         <%--  ---------- COMMENTED FOR VAULTFLOW
         
              <tr runat="server" style="display:none" id="trLatestContractFileTITLE" clientidmode="Static">
                    <td style="text-align: left" colspan="5">
                        <strong>Latest Contract File</strong>
                    </td>
                </tr>
                <tr  style="display:none" runat="server" id="trLatestContractFileLINK" clientidmode="Static">
                    <td style="vertical-align: top;width:90%" colspan="4">
                        <a id="linkLatestContractVersionFile" runat="server" class="icon icon-attachment">
                        </a><a title="MSWord file" id="aMSWordLatestFile" runat="server" clientidmode="Static">
                            <img alt="MSWord file" src="../images/icon-word.jpg" />
                        </a><a title="PDF file" id="aPDFLatestFile" runat="server" clientidmode="Static"
                            target="_blank">
                            <img alt="PDF file" src="../images/icon-pdf.jpg" />
                        </a>
                    </td>
                    <td style="text-align: right;width:10%">
                        <a id="A1" href="#" onclick="showTab('workflow');scrollToAnchor('DivContractVersions'); this.blur(); return false;"
                            runat="server">View All</a>
                    </td>
                </tr>--%>



            </tbody>
        </table>
        <br />
    </div>
    <%--TAB REQUEST DETAILS END--%>

     <br />
    <div class="tabsDivContainer">
    <div class="tabs">
     <br /><br />
     <center>
        <ul>
            <li id="ulWorkflowTab" runat="server" clientidmode="Static" class="selected"><a href="#" class="selected"
                id="tab-workflow" onclick="showTab('workflow'); this.blur(); return false;">Workflow</a></li>
            <li id="ulActivityTab" runat="server" clientidmode="Static"><a href="#" id="tab-activity"
                onclick="showTab('activity'); this.blur(); return false;">Activity &amp; Notes</a></li>
            <li id="ulDocumentTab" runat="server" clientidmode="Static"><a href="#" id="tab-document"
                onclick="showTab('document'); this.blur();return false;">Documents</a></li>
            <li id="ulKeyFields" runat="server" clientidmode="Static"><a href="#" id="tab-keyfields"
                onclick="showTab('keyfields'); this.blur();return false;">Important Dates</a></li>
            <li id="ulKeyObligations" runat="server" clientidmode="Static"><a href="#" id="tab-keyobligations"
                onclick="showTab('keyobligations'); this.blur();return false;">Key Obligations</a></li>
        </ul>
        </center>
    </div>

    <%--TAB WORKFLOW START--%>
    <div class="tab-content" id="tab-content-workflow" style="display: block;">
        <div id="update" style="">
            <%--Contract Version--%>
         <%--   <div id="DivContractVersions" style="display: none;" runat="server" clientidmode="Static">
                <h3>
                    Contract versions</h3>
                <div id="msgContractVersions" style="display: none;">
                </div>
                <span class="journal-link" id="JournalLinkDefault" runat="server" clientidmode="Static">
                    <asp:LinkButton ID="linkUploadFileDefault" runat="server" ClientIDMode="Static" class="icon icon-upload"
                        Style="font-weight: normal;" OnClick="linkUploadFileDefault_Click">Upload</asp:LinkButton>&nbsp;&nbsp;
                </span>
                <div class="attachments">
                    <asp:Repeater ID="rptContractVersions" runat="server" ClientIDMode="Static" OnItemDataBound="rptContractVersions_ItemDataBound  "
                        OnItemCommand="rptContractVersions_ItemCommand">
                        <ItemTemplate>
                            <h4>
                                <span class="journal-link" id="JournalLink" runat="server" clientidmode="Static">
                                    <asp:LinkButton ID="linkCheckOut" runat="server" ClientIDMode="Static" class="icon icon-checkout"
                                        Style="font-weight: normal;">Check-out</asp:LinkButton>&nbsp;&nbsp;
                                    <asp:LinkButton ID="linkCheckOutSendMail" runat="server" ClientIDMode="Static" class="icon icon-checkout"
                                        Style="font-weight: normal;">Check-out &amp; Send Email</asp:LinkButton>&nbsp;&nbsp;
                                        <asp:LinkButton ID="linkElectronicSignature" Visible="false" runat="server" ClientIDMode="Static" class="icon icon-checkout"
                                        Style="font-weight: normal;">Electronic Signature</asp:LinkButton>&nbsp;&nbsp;
                                    <asp:LinkButton ID="linkTrackChanges" runat="server" ClientIDMode="Static" class="icon icon-compare"
                                        Style="font-weight: normal;">Track Changes</asp:LinkButton>&nbsp;&nbsp;
                                    <asp:LinkButton ID="linkUploadFile" runat="server" ClientIDMode="Static" class="icon icon-upload"
                                        Style="font-weight: normal;">Upload</asp:LinkButton>&nbsp;&nbsp;
                                     <asp:LinkButton ID="linkCheckIn" class="icon icon-checkin" runat="server" Style="font-weight: normal;">Check-in</asp:LinkButton>
                                </span><a id="linkContractVersionFile" runat="server" class="icon icon-attachment">
                                    <%#Eval("FileName")%></a> <span class="size" style="font-weight: normal;">(<%#Eval("FileSizeKB")%>KB)</span> <span id="spanDeleteContractVersion" runat="server" clientidmode="Static">
                                            <a class="delete" rel="nofollow" title="Delete file" isFromDocuSign='<%#Eval("isFromDocuSign")%>'  id="<%#Eval("ContractFileId") %>"
                                                style="cursor: pointer" onclick="return DeleteContractFile(this);">
                                                <img alt="Delete" src="../images/icon-del.jpg?1349001717" /></a> </span>
                                <a rel="nofollow" title="MSWord file" id="aMSWordFile" runat="server" clientidmode="Static"
                                    href='<%# "../Uploads/Contract Documents/"+Eval("FileName") +".docx" %>'>
                                    <img alt="MSWord file" src="../images/icon-word.jpg" />
                                </a><a rel="nofollow" title="PDF file" id="aPDFFile" runat="server" clientidmode="Static"
                                    target="_blank" href='<%# "../Uploads/Contract Documents/PDF/"+Eval("FileName") +".pdf" %>'>
                                    <img alt="PDF file" src="../images/icon-pdf.jpg" />
                                </a>
                            </h4>
                            <span class="author">By
                                <%#Eval("ModifiedByUserName")%>,
                                <%#Eval("ModifiedOnFormattedDate")%></span>
                            <ul class="details">
                                <asp:Repeater ID="rptContractFilesActivity" runat="server">
                                    <ItemTemplate>
                                        <li>
                                            <%#Eval("ContractFileActivityDetails")%></li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                            <asp:HiddenField ID="hdnFileName" Value='<%#Eval("FileName")%>' runat="server" />
                            <asp:HiddenField ID="hdnIsFromDocuSign" Value='<%#Eval("isFromDocuSign")%>' runat="server" />
                            <asp:HiddenField ID="hdnContractFileId" Value='<%#Eval("ContractFileId")%>' runat="server" />
                            <asp:HiddenField ID="hdnIsCheckOutDone" Value='<%#Eval("IsCheckOutDone")%>' runat="server" />
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <script language="javascript" type="text/javascript">
                    function DeleteContractFile(obj) {

                        var isFromDocuSign = $(obj).attr('isFromDocuSign');
                        var RequestId = $('#hdnRequestId').val();
                        var FileName = $(obj).closest('div').find('a#linkContractVersionFile').html().trim();
       

                        if (confirm('Do you want to delete this version ?')) {
                            $.ajax({
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                               url: "VaultFlow.aspx/DeleteContractFile",
                               data: "{ContractFileId:'" + $(obj).attr('id') + "',isFromDocuSign:'" + isFromDocuSign + "',RequestId:'" + RequestId + "',FileName:'" + FileName + "'}",
                                dataType: "json",
                                success: function (output) {
                                    //if (output.d == "1")
                                    location.href = "VaultFlow.aspx?Section=ContractVersions&Mode=Delete&ScrollTo=DivContractVersions&RequestId=" + '<%= ViewState["RequestId"]%>';
                                },
                                error: function (err) {
                                }
                            });
                        }
                        return false;
                    }
                </script>
            </div>--%>


            <div style="clear: both;">
            </div>
            <%--Updated Answers--%>
            <div id="DivUpdatedQuestions" style="" runat="server" clientidmode="Static">
                <h3>Contract Assembly</h3>               
                <p>
                    <asp:Label ID="lblContractTemplateName" runat="server" Text=""></asp:Label>
                </p>
                 <br />
                <div id="note-1">
                        
                    <h4>
                    <%--<span class="journal-link">--%>
                       <%-- &nbsp;<div class="wiki editable" id="journal-22716-notes" style="display: none;">
                                <div>
                                    <a id="aEditQuestionnair" href="#" title="Update Questions/Answers" runat="server"
                                        onserverclick="EditQuestionnair_Click" style="font-weight: normal;">
                                        <img alt="Edit" src="../images/edit.png?1349001717" />
                                        Edit </a>&nbsp;&nbsp;
                                </div>
                            </div>--%>
                        <%--</span><a href="/redmine/issues/9529#note-1" class="journal-link"></a>--%>
                        <asp:Label ID="lblQuestionnaireUpdated" runat="server" Text=""></asp:Label>
                    </h4>
                    <div id="msgQuestionnaire" style="display: none;">
                    </div>
                    <ul class="details ">
                    <asp:Repeater ID="rptQA" runat="server">                   
                        <ItemTemplate>                            
                                <li style="list-style-type:none;"><span class="question text-tab"><%#Eval("Question") %></span>
                                <br>
                                    <p class="answer"><%#Eval("Answer").ToString().Replace("\n", "<br>&nbsp;")%></p>
                                 </li>
                                <br>
                           
                        </ItemTemplate>
                    </asp:Repeater></ul>
                </div>
            </div>
            <div style="clear: both;">
            </div>
            <%--Approval--%>
         <%--   <div id="PendingApprovals" class="journal has-notes has-details" style="" runat="server"
                clientidmode="Static">
                <h3 id="appheading" style="">
                    Approvals</h3>
                <div id="msgApprovals" style="display: none;">
                </div>
                <div>
                    <asp:Repeater ID="rptPendingApprovals" runat="server" OnItemDataBound="rptPendingApprovals_ItemDataBound">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdnContractApprovalID" runat="server" Value='<%#Eval("ContractApprovalId") %>' />
                            <input id="hdnApproverLoginId" type="hidden" runat="server" clientidmode="Static"
                                value='<%#Eval("AssignTo") %>' />
                            <input id="hdnIsActive" type="hidden" runat="server" clientidmode="Static" value='<%#Eval("IsActive") %>' />
                            <h4>
                                <span id="spanStageName" runat="server" clientidmode="Static">
                                    <%#Eval("StageName") %></span> <a href='<%#Eval("linkToApproval") %>' title="Approval"
                                        id="aLinkToApproval" runat="server" clientidmode="Static">
                                        <%#Eval("StageName") %></a> <span class="journal-link" id="ApprovalJournalLink" runat="server"
                                            clientidmode="Static">
                                            <div class="wiki editable" id="journal-22716-notes">
                                                <div>
                                                    <a href='<%#Eval("linkToApproval") %>' title="Approval" id="aLinkToApproval_Second"
                                                        runat="server" style="font-weight: normal" clientidmode="Static">
                                                        <img src="../images/edit.png?1349001717" />
                                                        Edit </a>&nbsp;&nbsp;
                                                </div>
                                            </div>
                                        </span>
                            </h4>
                            <ul class="details">
                                <asp:Repeater ID="rptApprovalsActivity" runat="server">
                                    <ItemTemplate>
                                        <li>
                                            <%#Eval("ApprovalActivtiyDetails")%></li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>--%>
            <div style="clear: both;">
            </div>
            <%--Questionnair--%>
         <%--   <div class="box" id="DivQuestionnair" runat="server">
                <h3>
                    Contract Assembly</h3>
                <fieldset runat="server" id="QuestionnairTemplate" clientidmode="Static">
                    <%#Eval("DocumentTypeName") %>
                    <legend>Templates</legend>
                    <br />
                    <table style="width: 100%; table-layout: fixed;">
                        <tr>
                            <th width="20%">
                            </th>
                            <th width="2%">
                            </th>
                            <th width="78%">
                            </th>
                        </tr>
                        <tr>
                            <td style="text-align: right;">
                                <label for="issue_tracker_id">
                                    Contract Template</label>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <select name="select" id="ddlContractTemplate" style="width: 50%" clientidmode="Static"
                                    runat="server" class="chzn-select" onchange="setFileName(this); ddlContractTemplate_onclick();">
                                </select>
                            </td>
                        </tr>
                    </table>
                    <%#Eval("FullName") %>
                    <p>
                        <%#Eval("AddedDate") %>
                    </p>
                </fieldset>
            </div>--%>
            <%--Buttons--%>
            <%--<asp:Button ID="btnDisplayQuestionnair" runat="server" OnClick="btnDisplayQuestionnair_Click"
                ClientIDMode="Static" Text="Load Questionnaire" />--%>
            <br />
             &nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="Back" />
            <asp:HiddenField ID="hdnFileName" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="hdnRequestId" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="hdnSelectedFileId" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="hdnContractId" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="hdnContractFileName" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="hdnContractTypeId" runat="server" ClientIDMode="Static" />
            <input id="hdnDocumentIDs" runat="server" clientidmode="Static" type="hidden" />
            <input id="hdnUserID" runat="server" clientidmode="Static" type="hidden" />
            <br />
        </div>
    </div>
    <%--TAB WORKFLOW END--%>

    <%--TAB ACTIVITY START--%>
    <div class="tab-content" id="tab-content-activity" style="display: none;">
        <div id="msgActivity" style="display: none;">
        </div>
        <asp:PlaceHolder ID="PlaceHolder1" runat="server">
            <uc2:activitiescontrol ID="activitiescontrolid" runat="server" UserControlProperty = 'Volts' />
        </asp:PlaceHolder>
    </div>
    <%--TAB ACTIVITY END--%>

    <%--TAB DOCUMENT START--%>
    <div class="tab-content" id="tab-content-document" style="display: none;">
    <br/><br/>
        <div id="msgDocuments" style="display: none;">
        </div><br/><br/><br/><br/>
        <div id="ParentDiv" runat="server" clientidmode="Static" style="margin-left: 180px;
            height: 60px; width: 60%; background-color: #F8F4CE; color: #865476; border: #4D2B2F dashed 1px">
            <table width="100%">
                <tr>
                     <td valign="top" align="center">
                        <ul id="uploader_filelist" class="plupload_filelist OnPageul" style="font-size: 30px;
                            color: #cccccc; margin-top: -65px">
                            <li class="plupload_droptext" style="height: 100px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Drag
                                files here.</li></ul>
                    </td>
                    <td align="right" valign="top" id="tdAdd">
                        <a href="#" class="plupload_button plupload_add onPage" id="uploader_browse" style="z-index: 1;
                            margin-top: 32px">Add Files</a>
                    </td>
                </tr>
            </table>
        </div>
        <div id="uploader" style="display: none; width: 600px;">
        </div>
        <div id="FileName" style="display: none; margin-left: 180px;">
        </div>
        <div class="attachments">
            <asp:Repeater ID="rptRequestDocuments" runat="server" ClientIDMode="Static" OnItemDataBound="rptRequestDocuments_ItemDataBound">
                <ItemTemplate>
                    <span>
                        <h4>
                            <asp:HiddenField ID="hdnDocumentName" runat="server" ClientIDMode="Static" Value='<%#Eval("ActuallFileName") %>' />
                            <a id="alinkWorkflowDocument" runat="server" clientidmode="Static" class="icon icon-attachment">
                                <%#Eval("OrigionalFileName") %>
                            </a>
                        </h4>
                        <span class="size"></span>- <span id="spanDocumentDelete" runat="server" clientidmode="Static">
                            <a href='' class="delete" rel="nofollow" title="Delete" id="<%#Eval("ContractRequestDocumentId") %>"
                                onclick="return DeleteDocument(this);">
                                <img alt="Delete" src="../images/icon-del.jpg?1349001717" /></a></span> <span class="author">
                                    <%#Eval("FullName") %>,
                                    <%#Eval("AddedDate") %></span>
                        <br />
                        <em>
                            <%#Eval("DocumentTypeName") %></em>
                        <br />
                        <br />
                    </span>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <asp:Button ID="btnDocumentBack" runat="server" OnClick="btnBack_Click" Text="Back" />
        <script type="text/javascript" src="http://code.jquery.com/ui/1.10.0/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../UploadJs/js/plupload.full.min.js"></script>
        <script type="text/javascript" src="../UploadJs/js/jquery.plupload.queue/jquery.plupload.queue.js"></script>
        <script type="text/javascript" src="../scripts/jquery.blockUI.js"></script>
        <script language="javascript" type="text/javascript">
            function DeleteDocument(obj) {
                if (confirm('Do you want to delete the file ?')) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "VaultFlow.aspx/DeleteDocument",
                        data: "{DocumentID:'" + $(obj).attr('id') + "'}",
                        dataType: "json",
                        success: function (output) {
                            if (output.d == "1") {
                                $(obj).parent('span').parent('span').remove();
                                showMessage("Documents", "Delete");
                            }
                        },
                        error: function (err) {
                        }
                    });
                }
                return false;
            }

            var DocumentTypeId = "";
            var upl;
            $(function () {
                upl = $("#uploader").pluploadQueue({

                    runtimes: 'html5, html4',
                    url: '../ClauseLiabrary/DocumentHandler.ashx',
                    max_file_size: '80mb',
                    max_file_count: 20, // user can add no more then 20 files at a time
                    chunk_size: '80mb',
                    unique_names: true,
                    multiple_queues: true,
                    multipart: true,
                    multipart_params: {
                    },
                    resize: { width: 1024, height: 768, quality: 90 },
                    rename: true,
                    sortable: true,
                    filters: [
               	{ title: "Image files", extensions: "jpg,gif,png,bmp,tiff,jpeg,TIF,blob" },
                { title: "Document files", extensions: "xls,xlsx,doc,docx,pdf,swf,ppt,pptx,txt,csv,odt,odp,odg,ods,mp3,mp4,wmv" },
                { title: "Zip files", extensions: "zip,avi" }
                    //             {title: "Document files", extensions: "doc,docx" },
		],
                    // PreInit events, bound before any internal events
                    preinit: {
                        Init: function (up, info) {
                            // log('[Init]', 'Info:', info, 'Features:', up.features);
                            $('#uploader').next('p').remove();
                            $('#uploader').next('p').remove();
                            $('#uploader').next('p').remove();
                            $('#uploader').next('p').remove();
                        },
                        PostInit: function (up) {
                        },
                        UploadFile: function (up, file) {
                            fname = file.name.replace(' & ', '').replace('&', '').replace('#', '');
                            var filesize = file.size;
                            if ($('.plupload_filelist').find('li:eq(' + Counter + ') div.plupload_file_DocumentType:eq(1)').html() != 'Done') {
                                DocumentTypeId = $('.plupload_filelist').find('li:eq(' + Counter + ') div.plupload_file_DocumentType:eq(1)').find('input').val();
                                Counter++;
                            }
                            else {
                            }
                            if (DocumentTypeId.indexOf("o_") == -1) {
                                up.settings.url = '../ClauseLiabrary/DocumentHandler.ashx?filesize=' + filesize + '&filename=' + fname + '&DocumentTypeId=' + DocumentTypeId + '&UserID=' + $('#hdnUserID').val() + '&RequestID=' + $('#hdnRequestId').val();
                            }
                        }
                    },
                    init: {
                        Refresh: function (up) {
                        },
                        StateChanged: function (up) {
                        },
                        QueueChanged: function (up) {
                        },
                        UploadProgress: function (up, file) {
                        },

                        FilesAdded: function (up, files) {
                            //$('a.onPage').css('margin-top', '-75px');
                            // $('.OnPageul').css('margin-top', '-75px');
                            //  $('div.plupload_buttons').find('a.plupload_start').before($('a.onPage').detach());
                            $.blockUI({ message: $('#uploader'), css: { width: '600px'} });
                            $('.plupload_filelist').css('height', $('.plupload_filelist').height(up.files.length * 50));

                            plupload.each(files, function (file) {
                                //  log('  File:', file);
                            });
                        },

                        FilesRemoved: function (up, files) {
                            $('.plupload_filelist').css('height', $('.plupload_filelist').height(up.files.length * 50));
                            plupload.each(files, function (file) {
                                //  log('  File:', file);
                            });
                        },

                        FileUploaded: function (up, file, info) {
                            $.ajax({
                                async: false,
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                url: "VaultFlow.aspx/BindDocumentDetails",
                                data: "{RequestId:'" + $("#hdnRequestId").val() + "'}",
                                dataType: "json",
                                success: function (output) {
                                    $("#tab-content-document div.attachments").html('').append(output.d);
                                    showMessage("Documents", "Upload");
                                },
                                error: function (err) {
                                    //alert(err.responseText);
                                }
                            });
                        },
                        ChunkUploaded: function (up, file, info) {
                            // Called when a file chunk has finished uploading
                            if (info.response != "Chunk") {
                                chunkreponse = info.response;
                                // log('[FileUploaded] File:', file, "Info:", info);
                            }
                        },
                        Error: function (up, args) {
                            // Called when a error has occured
                            //  log('[error] ', args);
                        }
                    },
                    // Resize images on clientside if we can
                    resize: { width: 320, height: 240, quality: 90 },
                    flash_swf_url: '../../js/Moxie.swf',
                    silverlight_xap_url: '../../js/Moxie.xap'
                });
            });
        </script>
    </div>
    <%--TAB DOCUMENT END--%>

    <%--TAB KeyField START--%>
    <div class="tab-content" id="tab-content-keyfields" style="display: none;">
        <div id="msgkeyfields" style="display: none;">
        </div>
        <asp:PlaceHolder ID="PlaceHolder2" runat="server">
           <%-- <uckey:KeyFieldscontrol ID="KeyFieldcontrol1" Linkstatus="VaultFlow" runat="server" />--%>
               <uckey:KeyFieldscontrol ID="KeyFieldcontrol1" runat="server" UserControlProperty = 'Volts' />
        </asp:PlaceHolder>
    </div>
    <%--TAB KeyField END--%>

    <%--TAB KeyObligations START--%>
    <div class="tab-content" id="tab-content-keyobligations" style="display: none;">
        <div id="msgkeyobligations" style="display: none;">
        </div>
        <asp:PlaceHolder ID="PlaceHolder3" runat="server">
            <ucobligkey:KeyFieldscontrol ID="KeyObligation" Linkstatus="VaultFlow" runat="server" UserControlProperty = 'Volts' />
        </asp:PlaceHolder>
    </div>
    <%--TAB KeyObligations END--%>

    <br /><br />
    </div>
    <script type="text/javascript">
        function setFileName(obj) {
            var fileName = $(obj).val().split('#')[1];
            $("#hdnSelectedFileId").val($(obj).val());
            $("#hdnFileName").val(fileName);
        }
        function setDafaultValues() {
            $('#ddlContractTemplate').val($('#hdnSelectedFileId').val());
        }

        function setUpdated(obj) {
            $(obj).attr("IsUpdated", "Y");
        }

        $(function () {
            $(".mws-datepicker").datepicker({ minDate: 0, maxDate: "+10M +10D", dateFormat: "dd-M-yy" });

            if ($('#ddlContractType').val() != "0" && $('#ddlContractType').val() != null && $('#ddlContractType').val() != undefined) {
                $('#ddlContractType').trigger("change");
                setDafaultValues();
            }
        });

        $().ready(function () {
            setDafaultValues();
            $('#ui-datepicker-div').hide();
        });
        $(window).load(function () {
            $("#ui-datepicker-div").hide();
        });
    </script>
    <script type="text/javascript">
        $("#btnDisplayQuestionnair").hide();
        function ddlContractTemplate_onclick() {
            if ($("#ddlContractTemplate").val() == "0") {
                $("#btnDisplayQuestionnair").hide();
            }
            else {
                $("#btnDisplayQuestionnair").show();
            }
        }
        function ddlContractType_onclick() {
        }

        function setActivityMessageFlag(msg) {
            $('#hdnActivityMessageFlag').val(msg);
        }



    </script>
</asp:Content>
