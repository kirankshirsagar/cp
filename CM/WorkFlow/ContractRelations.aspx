﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="ContractRelations.aspx.cs" Inherits="WorkFlow_ContractRelations" %>

<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridSelect.js" type="text/javascript"></script>

    <script src="../JQueryValidations/mastervalidations.js" type="text/javascript"></script>
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <script type="text/javascript">
        $("#requestLink").addClass("menulink");
        $("#requesttab").addClass("selectedtab");

        $('.node.nodeExample1 a img').live("click", function () {
            ShowProgress();
        });
    </script>
    <h2>
        Contracts Relationship for    
        <asp:Label ID="lblContractIDHeader" runat="server" Text="" ClientIDMode="Static"></asp:Label>
    </h2>
    <input id="hdnHasEditDeleteAccess" runat="server" clientidmode="Static" type="hidden" value="Y" />
    <input id="hdnContractId" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnContractRelationData" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdd" runat="server" clientidmode="Static" type="hidden" />
    <p class="buttons hide-when-print">
        <asp:LinkButton ID="btnAddRecord" CssClass="icon icon-add" runat="server" OnClientClick="resetPrimeID();"
            OnClick="btnAddRecord_Click">Add new relation</asp:LinkButton>
             <asp:LinkButton ID="btnEditRecord" CssClass="icon icon-add" runat="server" OnClientClick="resetPrimeID();" Visible="false"
            OnClick="btnAddRecord_Click">Update relation</asp:LinkButton>
    </p>
    
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>

    <div class="chart" id="custom-colored" >
        
    </div>
    <script src="../JS/OrgChart/raphael.js" type="text/javascript"></script>
    <script src="../JS/OrgChart/Treant.js" type="text/javascript"></script>
    <script type="text/javascript">    

    var ArrayInstanceExtensions = {
    remove: function(index){
        var picked_element = this[index];
        this.splice(index,1);
        return picked_element;
    } 
};
$.extend(Array.prototype, ArrayInstanceExtensions);

    var other=null;
    var config = {
        container: "#custom-colored",
        nodeAlign: "BOTTOM",        
        connectors: {
            type: 'step'
        },
        node: {
            HTMLclass: 'nodeExample1'
        }
    }

    chart_config = [
        config
    ];

    var CRdata=<%= CRData%> ;
    var data = [];
    var CurrentObj,parentObj, EditURL,currentContractId=<%=  Request.QueryString["ContractId"]%>;
    //var ParentIds = $.map(CRdata, function (element, index) { return element.ContractID });
    //EditURL='<%= Session["URL"] %>';

    if (CRdata != undefined) {
        //debugger;
        for (var i = 0; i < CRdata.length; i++) {
            var obj;
            if (CRdata[i].ChildContractId == 1) {
                data.push(
        obj = {RequestId: CRdata[i].RequestId, contractId: CRdata[i].ParentContractId, HTMLclass: 'rootClass', text: {id:1, name: CRdata[i].PIDClient, title: CRdata[i].RelationsDescription},link: { href: CRdata[i].PIDClientHref  },image: "../images/edit.png", editLink: '<%= Session["URL"] %>',showEditLink:$("#hdnHasEditDeleteAccess").val()}
            );
                chart_config.push(obj);
            }
            else {
                var cls="";
                parentObj = jQuery.grep(chart_config, function (a) {
                    if (a.contractId == CRdata[i].ParentContractId) {
                        return a;
                    }
                });
                if (parentObj != undefined && parentObj != null) {
                    cls= CRdata[i].HtmlClass != "" ? CRdata[i].HtmlClass : parentObj[0].HTMLclass;
                     
                    data.push(
        obj = {RequestId: CRdata[i].RequestId, contractId: CRdata[i].ChildContractId, parent: parentObj[0], childrenDropLevel: CRdata[i].ChildrenDropLevel, HTMLclass: cls, text: { name: CRdata[i].CIDClient, title: CRdata[i].RelationsDescription},link: { href: CRdata[i].CIDClientHref  },image: "../images/edit.png", editLink: '<%= Session["URL"] %>',showEditLink:$("#hdnHasEditDeleteAccess").val() }
            );
                    chart_config.push(obj);
                }
            }
        }
    }
    CurrentObj = jQuery.grep(chart_config, function (a) {
                if (a.contractId ==currentContractId) {                    
                    a.IsCurrent="Y";
                }
            });
       var tree =  new Treant(chart_config);

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            setTimeout(
                 function () {
                     var MaxHeight = 0;
                     $('.nodeExample1').each(function () {
                         var offset = $(this).position();
                         if (MaxHeight < offset.top)
                             MaxHeight = offset.top;
                     });
                     
                     $('.chart').css('height', MaxHeight+100);
                 }, 100);
        });
    </script>
    <asp:Button ID="btnBack" runat="server" Text="Back" ClientIDMode="Static" OnClick="Back_Click" />    
</asp:Content>
