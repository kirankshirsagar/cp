﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReportBLL;
using CommonBLL;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using UserManagementBLL;
using WorkflowBLL;
using MasterBLL;

public partial class Reports_CustomReportCreate : System.Web.UI.Page
{
    //public bool View;
    ICustomReports objReport;
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 50;
    List<string> fn;

    protected void Page_Load(object sender, EventArgs e)
    {

        //hdnReportFlag.Value = "76";
        CreateObjects();
        //if (Session[Declarations.User] != null && !IsPostBack)
        //{
        //    ViewState[Declarations.View] = Convert.ToBoolean(Access.isCustomised(int.Parse(hdnReportFlag.Value)));
        //}
        setAccessValues();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            FillDropDown();
            LoadData();
            if ((Session[Declarations.CustomReportId]) != null)
            {
                ViewState[Declarations.CustomReportId] = Convert.ToString(Session[Declarations.CustomReportId]);
                Session[Declarations.CustomReportId] = null;
                LoadCustomFields();
            }
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objReport = FactoryReports.CustomReportDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    private void setAccessValues()
    {
        //if (ViewState[Declarations.View] != null)
        //{
        //    View = Convert.ToBoolean(ViewState[Declarations.View].ToString());
        //}
    }

    public void LoadData()
    {
        //DataTable dtFixField = new DataTable();
        DataTable dtrpt = new DataTable();
        DataTable dtrptD = new DataTable();
        DataTable dtrptK = new DataTable();
        DataTable dt = objReport.GetReports();

        IEnumerable<DataRow> rowsrpt = dt.AsEnumerable()
              .Where(r => r.Field<string>("FieldType") == "RF");

        dtrpt = rowsrpt.CopyToDataTable();

        IEnumerable<DataRow> rowsrptD = dt.AsEnumerable()
              .Where(r => r.Field<string>("FieldType") == "ID");

        dtrptD = rowsrptD.CopyToDataTable();

        IEnumerable<DataRow> rowsrptK = dt.AsEnumerable()
              .Where(r => r.Field<string>("FieldType") == "KO");

        dtrptK = rowsrptK.CopyToDataTable();

        if (dtrpt.Rows.Count > 0)
        {
            chkReqestField.DataSource = dtrpt;
            chkReqestField.DataTextField = "Name";
            //chkReqestField.DataValueField = "Value";
            chkReqestField.DataValueField = "Identitys";
            chkReqestField.DataBind();
        }

        if (dtrptD.Rows.Count > 0)
        {
            chkImportantField.DataSource = dtrptD;
            chkImportantField.DataTextField = "Name";
            //chkImportantField.DataValueField = "Value";
            chkImportantField.DataValueField = "Identitys";
            chkImportantField.DataBind();
        }

        if (dtrptK.Rows.Count > 0)
        {
            chkKeyObliField.DataSource = dtrptK;
            chkKeyObliField.DataTextField = "Name";
            //chkKeyObliField.DataValueField = "Value";
            chkKeyObliField.DataValueField = "Identitys";
            chkKeyObliField.DataBind();
        }

        objReport.UserID = int.Parse(Session[Declarations.User].ToString());
        DataTable dtCT = objReport.GetCustomContractType();
        chkContractType.DataSource = dtCT;
        chkContractType.DataTextField = "ContractTypeName";
        chkContractType.DataValueField = "ContractTypeId";
        chkContractType.DataBind();
    }

    private void FillDropDown()
    {
        if (ddlContractingParty.Items.Count == 0)
        {
            //   ddlContracttype.extDataBind(objcontracttype.SelectData());
            IContractRequest objcontractreq;
            objcontractreq = FactoryWorkflow.GetContractRequestDetails();
            objcontractreq.UserID = int.Parse(Session[Declarations.User].ToString());
            ddlContractingParty.extDataBind(objcontractreq.SelectContractingPartyData());
        }


        if (ddlAssignedDept.Items.Count == 0)
        {
            IContractRequest objcontractreq;
            objcontractreq = FactoryWorkflow.GetContractRequestDetails();
            ddlAssignedDept.extDataBind(objcontractreq.SelectDeptData());
        }

        if (ddlAssignUser.Items.Count == 0 || ddlAssignUser.Items.Count == 0)
        {
            IUsers objuser;
            objuser = FactoryUser.GetUsersDetail();

            if (ddlAssignUser.Items.Count == 0)
            {
                ddlAssignUser.extDataBind(objuser.SelectData());
            }

            if (ddlAssignUser.Items.Count == 0)
            {
                ddlAssignUser.extDataBind(objuser.SelectData());
            }
        }

        if (ddlAssignedDept.Items.Count == 0)
        {
            IAvailableClient objAvailableClient;
            objAvailableClient = FactoryWorkflow.GetAvailableClientDetails();
        }


        ddlTerritary.DataSource = FillMasterData("MstTerritory");
        ddlTerritary.DataTextField = "Name";
        ddlTerritary.DataValueField = "Id";
        ddlTerritary.DataBind();

        //ddlSeason.DataSource = FillMasterData("MstSeason");
        //ddlSeason.DataTextField = "Name";
        //ddlSeason.DataValueField = "Name";
        //ddlSeason.DataBind();

        DataTable dt = objReport.GetRecipentMailIdData();
        if (dt.Rows.Count > 0)
        {
            ddlUser.DataSource = dt;
            ddlUser.DataTextField = "FullName";
            ddlUser.DataValueField = "Email";
            ddlUser.DataBind();
            ddlUser.Multiple = true;
        }

        ddlIsCustomer.extDataBind(objReport.GetCustomerSupplierName());
        ddlIsCustomer.Items.Insert(0, new ListItem("------Select------", "0", true));
        //ddlIsCustomer.SelectedIndex = 0;
        //ddlIsCustomer.Items.Add(new ListItem("------Select------", "0", true));
        //ddlIsCustomer.Value = "0";
        //ddlIsCustomer.Items.Insert(0, "------Select------");
        //DataTable dtName = objReport.GetCustomerSupplierName();
        //if (dtName.Rows.Count > 0)
        //{
        //    ddlIsCustomer.DataSource = dtName;
        //    ddlIsCustomer.DataTextField = "ClientName";
        //    ddlIsCustomer.DataValueField = "ClientId";
        //    ddlIsCustomer.DataBind();
        //    ddlIsCustomer.Multiple = true;
        //}

    }

    protected void LoadCustomFields()
    {
        ICustomReportsNew objReport = FactoryReports.CustomReportDetails();
        objReport.PageNo = 1;
        objReport.RecordsPerPage = 100;
        objReport.CustomReportId = Convert.ToInt32(ViewState[Declarations.CustomReportId]);
        DataSet ds = objReport.GetDSCustomiesRecords();
        if (ds.Tables[0].Rows.Count > 0)
        {
            txtCustomReportName.Text = Convert.ToString(ds.Tables[0].Rows[0]["CustomReportName"]);
            txtDescription.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["CustomDescription"]);

            if (Convert.ToString(ds.Tables[0].Rows[0]["ReportFixedFields"]) != string.Empty)
            {
                string str = Convert.ToString(ds.Tables[0].Rows[0]["ReportFixedFields"]);
                string[] values = str.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < values.Length; i++)
                {
                    for (int j = 0; j < chkReqestField.Items.Count; j++)
                    {
                        if (chkReqestField.Items[j].Text.Trim() == values[i].Trim())
                        {
                            chkReqestField.Items[j].Selected = true;
                        }
                    }
                    for (int j = 0; j < chkImportantField.Items.Count; j++)
                    {
                        if (chkImportantField.Items[j].Text.Trim() == values[i].Trim())
                        {
                            chkImportantField.Items[j].Selected = true;
                        }
                    }
                    for (int j = 0; j < chkKeyObliField.Items.Count; j++)
                    {
                        if (chkKeyObliField.Items[j].Text.Trim() == values[i].Trim())
                        {
                            chkKeyObliField.Items[j].Selected = true;
                        }
                    }
                }
            }

            if (Convert.ToString(ds.Tables[0].Rows[0]["RequestFormReportFields"]) != string.Empty)
            {
                string str = Convert.ToString(ds.Tables[0].Rows[0]["RequestFormReportFields"]);
                string[] values = str.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < values.Length; i++)
                {
                    for (int j = 0; j < chkReqestField.Items.Count; j++)
                    {
                        if (chkReqestField.Items[j].Text.Trim() == values[i].Trim())
                        {
                            chkReqestField.Items[j].Selected = true;
                        }
                    }
                }
            }

            if (Convert.ToString(ds.Tables[0].Rows[0]["ImportantDatesReportFields"]) != string.Empty)
            {
                string str = Convert.ToString(ds.Tables[0].Rows[0]["ImportantDatesReportFields"]);
                string[] values = str.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < values.Length; i++)
                {
                    for (int j = 0; j < chkImportantField.Items.Count; j++)
                    {
                        if (chkImportantField.Items[j].Text.Trim() == values[i].Trim())
                        {
                            chkImportantField.Items[j].Selected = true;
                        }
                    }
                }
            }

            if (Convert.ToString(ds.Tables[0].Rows[0]["KeyObligationReportFields"]) != string.Empty)
            {
                string str = Convert.ToString(ds.Tables[0].Rows[0]["KeyObligationReportFields"]);
                string[] values = str.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < values.Length; i++)
                {
                    for (int j = 0; j < chkKeyObliField.Items.Count; j++)
                    {
                        if (chkKeyObliField.Items[j].Text.Trim() == values[i].Trim())
                        {
                            chkKeyObliField.Items[j].Selected = true;
                        }
                    }
                }
            }

            if (Convert.ToString(ds.Tables[0].Rows[0]["ReportWhereClauseFields"]) != string.Empty)
            {
                string str = Convert.ToString(ds.Tables[0].Rows[0]["ReportWhereClauseFields"]);
                string[] values = str.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < values.Length; i++)
                {
                    for (int j = 0; j < chkContractType.Items.Count; j++)
                    {
                        if (chkContractType.Items[j].Value.Trim() == values[i].Trim())
                        {
                            chkContractType.Items[j].Selected = true;
                        }
                    }
                }
            }
        }

        #region WhereCondition Data Except Contract Type
        if (ds.Tables[1].Rows.Count > 0)
        {
            if (Convert.ToString(ds.Tables[1].Rows[0]["EffectiveDate"]) != "")
            {
                txtEffectiveFromDate.Value = Convert.ToString(ds.Tables[1].Rows[0]["EffectiveDate"]);
                txtEffectiveToDate.Value = Convert.ToString(ds.Tables[1].Rows[0]["EffectiveToDate"]);
            }
            if (Convert.ToString(ds.Tables[1].Rows[0]["ExpiryDate"]) != "")
            {
                txtExpiryFromDate.Value = Convert.ToString(ds.Tables[1].Rows[0]["ExpiryDate"]);
                txtExpiryToDate.Value = Convert.ToString(ds.Tables[1].Rows[0]["ExpiryToDate"]);
            }
            if (Convert.ToString(ds.Tables[1].Rows[0]["RenewalDate"]) != "")
            {
                txtRenewalFromDate.Value = Convert.ToString(ds.Tables[1].Rows[0]["RenewalDate"]);
                txtRenewalToDate.Value = Convert.ToString(ds.Tables[1].Rows[0]["RenewalToDate"]);
            }
            if (Convert.ToString(ds.Tables[1].Rows[0]["SignatureDate"]) != "")
            {
                txtFromSignatureDate.Value = Convert.ToString(ds.Tables[1].Rows[0]["SignatureDate"]);
                txtToSignatureDate.Value = Convert.ToString(ds.Tables[1].Rows[0]["SignatureToDate"]);
            }
            if (Convert.ToString(ds.Tables[1].Rows[0]["RequestDate"]) != "")
            {
                txtRequestFromDate.Value = Convert.ToString(ds.Tables[1].Rows[0]["RequestDate"]);
                txtRequestToDate.Value = Convert.ToString(ds.Tables[1].Rows[0]["RequestToDate"]);
            }

            if (Convert.ToString(ds.Tables[1].Rows[0]["AnnualValue"]) != "")
            {
                //string[] values = str.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                txtFromContractValue.Value = Convert.ToString(ds.Tables[1].Rows[0]["AnnualValue"]);
                txtToContractValue.Value = Convert.ToString(ds.Tables[1].Rows[0]["AnnualValueTo"]);
            }
            if (Convert.ToString(ds.Tables[1].Rows[0]["ContractingPartyId"]) != "")
            {
                ddlContractingParty.Value = Convert.ToString(ds.Tables[1].Rows[0]["ContractingPartyId"]);
            }
            if (Convert.ToString(ds.Tables[1].Rows[0]["TerritoryId"]) != "")
            {
                ddlTerritary.Value = Convert.ToString(ds.Tables[1].Rows[0]["TerritoryId"]);
            }
            if (Convert.ToString(ds.Tables[1].Rows[0]["ContractStatusId"]) != "")
            {
                ddlContractStatus.Value = Convert.ToString(ds.Tables[1].Rows[0]["ContractStatusId"]);
            }
            if (Convert.ToString(ds.Tables[1].Rows[0]["AssignedDepartmentId"]) != "")
            {
                ddlAssignedDept.Value = Convert.ToString(ds.Tables[1].Rows[0]["AssignedDepartmentId"]);
            }
            if (Convert.ToString(ds.Tables[1].Rows[0]["AssignedUserId"]) != "")
            {
                ddlAssignUser.Value = Convert.ToString(ds.Tables[1].Rows[0]["AssignedUserId"]);
            }

            if (Convert.ToString(ds.Tables[1].Rows[0]["ClientId"]) != "")
            {
                ddlIsCustomer.Value = Convert.ToString(ds.Tables[1].Rows[0]["ClientId"]);
            }
            if (Convert.ToString(ds.Tables[1].Rows[0]["ChangeOfControl"]) != "")
            {
                ddlChangeOfControl.Value = Convert.ToString(ds.Tables[1].Rows[0]["ChangeOfControl"]);
            }
            if (Convert.ToString(ds.Tables[1].Rows[0]["EntireAgreementClause"]) != "")
            {
                ddlAgreementClause.Value = Convert.ToString(ds.Tables[1].Rows[0]["EntireAgreementClause"]);
            }
            if (Convert.ToString(ds.Tables[1].Rows[0]["Strictliability"]) != "")
            {
                ddlStrictLiability.Value = Convert.ToString(ds.Tables[1].Rows[0]["Strictliability"]);
            }
            if (Convert.ToString(ds.Tables[1].Rows[0]["ThirdPartyGuaranteeRequired"]) != "")
            {
                ddlThirdPartyGuarantee.Value = Convert.ToString(ds.Tables[1].Rows[0]["ThirdPartyGuaranteeRequired"]);
            }
            if (Convert.ToString(ds.Tables[1].Rows[0]["PriorityContract"]) != "")
            {
                ddlPriority.Value = Convert.ToString(ds.Tables[1].Rows[0]["PriorityContract"]);
            }
        }
        #endregion

        int Param = 0;

        DataTable dtSchedule = objReport.GetScheduleRecord();
        if (dtSchedule.Rows.Count > 0)
        {
            List<string> Chkdata = new List<string>();
            for (int i = 0; i < dtSchedule.Rows.Count; i++)
            {
                try
                {
                    ddlUser.extSelectedValues(Convert.ToString(dtSchedule.Rows[i]["RecipiantMailId"]).TrimStart(','));
                }
                catch (Exception ex)
                {
                    Param = 1;
                }

                txtMailId.Text = Convert.ToString(dtSchedule.Rows[i]["MailId"]);
                TimeSpan ts = TimeSpan.Parse(Convert.ToString(dtSchedule.Rows[i]["ScheduleTime"]));
                if (Convert.ToString(dtSchedule.Rows[i]["ScheduleType"]) == "Daily")
                {
                    chkDailys.Checked = true;
                    txtDailyTime.Text = Convert.ToString(ts.ToString(@"hh\:mm"));
                    Chkdata.Add("Daily");
                }
                else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleType"]) == "Weekly")
                {
                    Chkdata.Add("Weekly");
                    chkWeeklys.Checked = true;
                    txtWeeklyTime.Text = Convert.ToString(ts.ToString(@"hh\:mm"));
                    ddlWeeklyDay.SelectedValue = Convert.ToString(dtSchedule.Rows[i]["ScheduleWeekday"]);
                }
                else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleType"]) == "Monthly")
                {
                    Chkdata.Add("Monthly");
                    chkMonthlys.Checked = true;
                    txtMonthlyTime.Text = Convert.ToString(ts.ToString(@"hh\:mm"));
                    ddlMontlyDay.SelectedValue = Convert.ToString(dtSchedule.Rows[i]["ScheduleDay"]);
                    if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "1")
                        txtMonthlyMonth.Text = "January";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "2")
                        txtMonthlyMonth.Text = "February";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "3")
                        txtMonthlyMonth.Text = "March";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "4")
                        txtMonthlyMonth.Text = "April";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "5")
                        txtMonthlyMonth.Text = "May";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "6")
                        txtMonthlyMonth.Text = "June";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "7")
                        txtMonthlyMonth.Text = "July";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "8")
                        txtMonthlyMonth.Text = "August";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "9")
                        txtMonthlyMonth.Text = "September";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "10")
                        txtMonthlyMonth.Text = "October";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "11")
                        txtMonthlyMonth.Text = "November";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "12")
                        txtMonthlyMonth.Text = "December";

                }
                else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleType"]) == "Yearly")
                {
                    Chkdata.Add("Yearly");
                    chkYearlys.Checked = true;
                    txtYearlyTime.Text = Convert.ToString(ts.ToString(@"hh\:mm"));
                    ddlYearlyDay.SelectedValue = Convert.ToString(dtSchedule.Rows[i]["ScheduleDay"]);
                    if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "1")
                        txtYearlyMonth.Text = "January";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "2")
                        txtYearlyMonth.Text = "February";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "3")
                        txtYearlyMonth.Text = "March";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "4")
                        txtYearlyMonth.Text = "April";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "5")
                        txtYearlyMonth.Text = "May";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "6")
                        txtYearlyMonth.Text = "June";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "7")
                        txtYearlyMonth.Text = "July";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "8")
                        txtYearlyMonth.Text = "August";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "9")
                        txtYearlyMonth.Text = "September";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "10")
                        txtYearlyMonth.Text = "October";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "11")
                        txtYearlyMonth.Text = "November";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "12")
                        txtYearlyMonth.Text = "December";

                    txtYearlyYear.Text = Convert.ToString(dtSchedule.Rows[i]["ScheduleYear"]);
                }

            }
            if (Param == 1)
            {
                string script = "window.onload = function() { alert('Some recipients were removed as their email ids have changed.'); };";
                ClientScript.RegisterStartupScript(this.GetType(), "UpdateTime", script, true);

            }
            ViewState["Chkdata"] = Chkdata;
        }


    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        lblmessage.Text = "";
        objReport.IsCustomReport = Convert.ToString("Y");
        if (txtCustomReportName.Text.Trim() != string.Empty)
        {
            if (Convert.ToString(ViewState[Declarations.CustomReportId]) == "")
            {
                objReport.CustomReportName = txtCustomReportName.Text.Trim();
                StringBuilder sbFixedField = new StringBuilder();
                StringBuilder sbrpt = new StringBuilder();
                StringBuilder sbrptD = new StringBuilder();
                StringBuilder sbrptK = new StringBuilder();
                foreach (ListItem li in chkReqestField.Items)
                {
                    if (li.Selected)
                    {
                        if (Convert.ToString(li.Value) == "rpt")
                        {
                            sbrpt.Append(Convert.ToString(li.Text) + ",");
                        }
                        else if (Convert.ToString(li.Value) == "cR")
                        {
                            sbFixedField.Append(Convert.ToString(li.Text) + ",");
                        }
                    }
                }

                foreach (ListItem li in chkImportantField.Items)
                {
                    if (li.Selected)
                    {
                        if (Convert.ToString(li.Value) == "rptD")
                        {
                            sbrptD.Append(Convert.ToString(li.Text) + ",");
                        }
                        else if (Convert.ToString(li.Value) == "cR")
                        {
                            sbFixedField.Append(Convert.ToString(li.Text) + ",");
                        }
                    }
                }

                foreach (ListItem li in chkKeyObliField.Items)
                {
                    if (li.Selected)
                    {
                        if (Convert.ToString(li.Value) == "rptK")
                        {
                            sbrptK.Append(Convert.ToString(li.Text) + ",");
                        }
                        else if (Convert.ToString(li.Value) == "cR")
                        {
                            sbFixedField.Append(Convert.ToString(li.Text) + ",");
                        }
                    }
                }

                StringBuilder sbchkType = new StringBuilder();
                foreach (ListItem li in chkContractType.Items)
                {
                    if (li.Selected)
                    {
                        sbchkType.Append(Convert.ToString(li.Value) + ",");
                    }
                }
                if (sbFixedField.ToString().TrimEnd(',') == string.Empty && sbrpt.ToString().TrimEnd(',') == string.Empty && sbrptD.ToString().TrimEnd(',') == string.Empty && sbrptK.ToString().TrimEnd(',') == string.Empty)
                {
                    lblmessage.Text = "Please select field.";
                    return;
                }

                objReport.ReportFixedFields = sbFixedField.ToString().TrimEnd(',');
                objReport.RequestFormReportFields = sbrpt.ToString().TrimEnd(',');
                objReport.ImportantDatesReportFields = sbrptD.ToString().TrimEnd(',');
                objReport.KeyObligationReportFields = sbrptK.ToString().TrimEnd(',');
                objReport.ReportWhereClauseFields = sbchkType.ToString().TrimEnd(','); ;

                objReport.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
                objReport.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : "";
                objReport.CustomDescription = txtDescription.InnerText.Trim();

                #region WhereCondition Data Except Contract Type
                if (txtEffectiveFromDate.Value != "" && txtEffectiveToDate.Value != "")
                {
                    objReport.EffectiveDate = txtEffectiveFromDate.Value.ToString().Trim();
                    objReport.EffectiveToDate = txtEffectiveToDate.Value.ToString().Trim();
                }
                if (txtExpiryFromDate.Value != "" && txtExpiryToDate.Value != "")
                {
                    objReport.ExpiryDate = txtExpiryFromDate.Value.ToString().Trim();
                    objReport.ExpiryToDate = txtExpiryToDate.Value.ToString().Trim();
                }
                if (txtRenewalFromDate.Value != "" && txtRenewalToDate.Value != "")
                {
                    objReport.RenewalDate = txtRenewalFromDate.Value.ToString().Trim();
                    objReport.RenewalToDate = txtRenewalToDate.Value.ToString().Trim();
                }
                if (txtRequestFromDate.Value != "" && txtRequestToDate.Value != "")
                {
                    objReport.RequestDate = txtRequestFromDate.Value.ToString().Trim();
                    objReport.RequestToDate = txtRequestToDate.Value.ToString().Trim();
                }

                if (txtFromSignatureDate.Value != "" && txtToSignatureDate.Value != "")
                {
                    objReport.SignatureDate = txtFromSignatureDate.Value.ToString().Trim();
                    objReport.SignatureToDate = txtToSignatureDate.Value.ToString().Trim();
                }

                if (txtFromContractValue.Value != "" && txtToContractValue.Value != "")
                {
                    objReport.AnnualValue = txtFromContractValue.Value.ToString().Trim();
                    objReport.AnnualValueTo = txtToContractValue.Value.ToString().Trim();
                }

                if (ddlIsCustomer.SelectedIndex != 0)
                    objReport.ClientId = Convert.ToInt32(ddlIsCustomer.Value);
                if (ddlChangeOfControl.SelectedIndex != 0)
                    objReport.ChangeOfControl = ddlChangeOfControl.Value.ToString().Trim();
                if (ddlAgreementClause.SelectedIndex != 0)
                    objReport.EntireAgreementClause = ddlAgreementClause.Value.ToString().Trim();
                if (ddlStrictLiability.SelectedIndex != 0)
                    objReport.Strictliability = ddlStrictLiability.Value.ToString().Trim();
                if (ddlThirdPartyGuarantee.SelectedIndex != 0)
                    objReport.ThirdPartyGuaranteeRequired = ddlThirdPartyGuarantee.Value.ToString().Trim();
                /* Added by kk on 18 July 2016  */
                if (ddlPriority.SelectedIndex != 0)
                    objReport.Priority = ddlPriority.Value.ToString().Trim();



                objReport.TerritoryId = Convert.ToInt32(ddlTerritary.Value);
                objReport.ContractingPartyId = Convert.ToInt32(ddlContractingParty.Value);

                objReport.ContractStatusId = Convert.ToInt32(ddlContractStatus.Value);
                objReport.AssignedDepartmentId = Convert.ToInt32(ddlAssignedDept.Value);
                objReport.AssignedUserId = Convert.ToInt32(ddlAssignUser.Value);

                //if (ddlParentCompanyGuarantee.SelectedIndex != 0)
                //    objReport.Parentcompanyguarantee = ddlParentCompanyGuarantee.Value;
                ////if (ddlBankguarantee.SelectedIndex != 0)
                ////    objReport.Bankguarantee = ddlBankguarantee.Value;
                ////if (ddlOriginalsignature.SelectedIndex != 0)
                ////    objReport.Originalsignature = ddlOriginalsignature.Value;

                #endregion


                string status = objReport.InsertRecord();

                objReport.CustomReportId = Convert.ToInt32(status);
                objReport.MailId = txtMailId.Text.Trim();

                if (txtMailId.Text.Trim() != "")
                    objReport.RecipiantMailId = "," + hdnUsers.Value;
                else
                    objReport.RecipiantMailId = hdnUsers.Value;

                if (chkDailys.Checked == true)
                {
                    objReport.Flag = 4;
                    objReport.ScheduleTime = txtDailyTime.Text.Trim();
                    objReport.ScheduleType = "Daily";
                    status = objReport.InsertRecord();
                }
                if (chkWeeklys.Checked == true)
                {
                    objReport.Flag = 4;
                    objReport.ScheduleTime = txtWeeklyTime.Text.Trim();
                    objReport.ScheduleWeek = ddlWeeklyDay.SelectedValue;
                    objReport.ScheduleType = "Weekly";
                    status = objReport.InsertRecord();
                }
                if (chkMonthlys.Checked == true)
                {
                    objReport.ScheduleWeek = "";
                    objReport.Flag = 4;
                    objReport.ScheduleTime = txtMonthlyTime.Text.Trim();
                    if (txtMonthlyMonth.Text.Trim() == "January")
                        objReport.ScheduleMonth = "1";
                    else if (txtMonthlyMonth.Text.Trim() == "February")
                        objReport.ScheduleMonth = "2";
                    else if (txtMonthlyMonth.Text.Trim() == "March")
                        objReport.ScheduleMonth = "3";
                    else if (txtMonthlyMonth.Text.Trim() == "April")
                        objReport.ScheduleMonth = "4";
                    else if (txtMonthlyMonth.Text.Trim() == "May")
                        objReport.ScheduleMonth = "5";
                    else if (txtMonthlyMonth.Text.Trim() == "June")
                        objReport.ScheduleMonth = "6";
                    else if (txtMonthlyMonth.Text.Trim() == "July")
                        objReport.ScheduleMonth = "7";
                    else if (txtMonthlyMonth.Text.Trim() == "August")
                        objReport.ScheduleMonth = "8";
                    else if (txtMonthlyMonth.Text.Trim() == "September")
                        objReport.ScheduleMonth = "9";
                    else if (txtMonthlyMonth.Text.Trim() == "October")
                        objReport.ScheduleMonth = "10";
                    else if (txtMonthlyMonth.Text.Trim() == "November")
                        objReport.ScheduleMonth = "11";
                    else if (txtMonthlyMonth.Text.Trim() == "December")
                        objReport.ScheduleMonth = "12";

                    objReport.ScheduleDay = ddlMontlyDay.SelectedValue;
                    objReport.ScheduleType = "Monthly";
                    status = objReport.InsertRecord();
                }

                if (chkYearlys.Checked == true)
                {
                    objReport.ScheduleWeek = "";
                    objReport.ScheduleDay = ddlYearlyDay.SelectedValue;
                    objReport.Flag = 4;
                    objReport.ScheduleTime = txtYearlyTime.Text.Trim();
                    if (txtYearlyMonth.Text.Trim() == "January")
                        objReport.ScheduleMonth = "1";
                    else if (txtYearlyMonth.Text.Trim() == "February")
                        objReport.ScheduleMonth = "2";
                    else if (txtYearlyMonth.Text.Trim() == "March")
                        objReport.ScheduleMonth = "3";
                    else if (txtYearlyMonth.Text.Trim() == "April")
                        objReport.ScheduleMonth = "4";
                    else if (txtYearlyMonth.Text.Trim() == "May")
                        objReport.ScheduleMonth = "5";
                    else if (txtYearlyMonth.Text.Trim() == "June")
                        objReport.ScheduleMonth = "6";
                    else if (txtYearlyMonth.Text.Trim() == "July")
                        objReport.ScheduleMonth = "7";
                    else if (txtYearlyMonth.Text.Trim() == "August")
                        objReport.ScheduleMonth = "8";
                    else if (txtYearlyMonth.Text.Trim() == "September")
                        objReport.ScheduleMonth = "9";
                    else if (txtYearlyMonth.Text.Trim() == "October")
                        objReport.ScheduleMonth = "10";
                    else if (txtYearlyMonth.Text.Trim() == "November")
                        objReport.ScheduleMonth = "11";
                    else if (txtYearlyMonth.Text.Trim() == "December")
                        objReport.ScheduleMonth = "12";

                    objReport.ScheduleYear = txtYearlyYear.Text.Trim();
                    objReport.ScheduleType = "Yearly";
                    status = objReport.InsertRecord();
                }

                if (status != "0")
                {
                    txtCustomReportName.Text = "";
                    txtDescription.InnerText = "";
                    ViewState[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
                }

                if (Convert.ToString(Session[Declarations.UserRole]) != "Super Admin")
                {
                    //GetAccessId();
                    UpdateAccessIds();
                    //IRolePermissions objPermission = FactoryUser.GetRolePermissionDetail();
                    //objPermission.RoleId = int.Parse(ViewState["RoleIds"].ToString());
                    //objPermission.RoleAccess = GetAccessTable();
                    //objPermission.RoleAccessDynamic = GetAccessTableFeature();
                    //status = objPermission.InsertRecord();

                }
                //Server.Transfer("CustomReports.aspx");
                Response.Redirect("CustomReports.aspx");
            }
            else
            {
                #region Edit Custom Report
                objReport.CustomReportName = txtCustomReportName.Text.Trim();
                StringBuilder sbFixedField = new StringBuilder();
                StringBuilder sbrpt = new StringBuilder();
                StringBuilder sbrptD = new StringBuilder();
                StringBuilder sbrptK = new StringBuilder();

                foreach (ListItem li in chkReqestField.Items)
                {
                    if (li.Selected)
                    {
                        if (Convert.ToString(li.Value) == "rpt")
                        {
                            sbrpt.Append(Convert.ToString(li.Text) + ",");
                        }
                        else if (Convert.ToString(li.Value) == "cR")
                        {
                            sbFixedField.Append(Convert.ToString(li.Text) + ",");
                        }
                    }
                }

                foreach (ListItem li in chkImportantField.Items)
                {
                    if (li.Selected)
                    {
                        if (Convert.ToString(li.Value) == "rptD")
                        {
                            sbrptD.Append(Convert.ToString(li.Text) + ",");
                        }
                        else if (Convert.ToString(li.Value) == "cR")
                        {
                            sbFixedField.Append(Convert.ToString(li.Text) + ",");
                        }
                    }
                }

                foreach (ListItem li in chkKeyObliField.Items)
                {
                    if (li.Selected)
                    {
                        if (Convert.ToString(li.Value) == "rptK")
                        {
                            sbrptK.Append(Convert.ToString(li.Text) + ",");
                        }
                        else if (Convert.ToString(li.Value) == "cR")
                        {
                            sbFixedField.Append(Convert.ToString(li.Text) + ",");
                        }
                    }
                }

                StringBuilder sbchkType = new StringBuilder();
                foreach (ListItem li in chkContractType.Items)
                {
                    if (li.Selected)
                    {
                        sbchkType.Append(Convert.ToString(li.Value) + ",");
                    }
                }
                if (sbFixedField.ToString().TrimEnd(',') == string.Empty && sbrpt.ToString().TrimEnd(',') == string.Empty && sbrptD.ToString().TrimEnd(',') == string.Empty && sbrptK.ToString().TrimEnd(',') == string.Empty)
                {
                    lblmessage.Text = "Please select field.";
                    return;
                }

                objReport.ReportFixedFields = sbFixedField.ToString().TrimEnd(',');
                objReport.RequestFormReportFields = sbrpt.ToString().TrimEnd(',');
                objReport.ImportantDatesReportFields = sbrptD.ToString().TrimEnd(',');
                objReport.KeyObligationReportFields = sbrptK.ToString().TrimEnd(',');
                objReport.ReportWhereClauseFields = sbchkType.ToString().TrimEnd(',');
                objReport.CustomReportId = Convert.ToInt32(ViewState[Declarations.CustomReportId]);

                objReport.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
                objReport.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : "";
                objReport.CustomDescription = txtDescription.InnerText.Trim();

                #region WhereCondition Data Except Contract Type
                if (txtEffectiveFromDate.Value != "" && txtEffectiveToDate.Value != "")
                {
                    objReport.EffectiveDate = txtEffectiveFromDate.Value.ToString().Trim();
                    objReport.EffectiveToDate = txtEffectiveToDate.Value.ToString().Trim();
                }
                if (txtExpiryFromDate.Value != "" && txtExpiryToDate.Value != "")
                {
                    objReport.ExpiryDate = txtExpiryFromDate.Value.ToString().Trim();
                    objReport.ExpiryToDate = txtExpiryToDate.Value.ToString().Trim();
                }
                if (txtRenewalFromDate.Value != "" && txtRenewalToDate.Value != "")
                {
                    objReport.RenewalDate = txtRenewalFromDate.Value.ToString().Trim();
                    objReport.RenewalToDate = txtRenewalToDate.Value.ToString().Trim();
                }
                if (txtRequestFromDate.Value != "" && txtRequestToDate.Value != "")
                {
                    objReport.RequestDate = txtRequestFromDate.Value.ToString().Trim();
                    objReport.RequestToDate = txtRequestToDate.Value.ToString().Trim();
                }

                if (txtFromSignatureDate.Value != "" && txtToSignatureDate.Value != "")
                {
                    objReport.SignatureDate = txtFromSignatureDate.Value.ToString().Trim();
                    objReport.SignatureToDate = txtToSignatureDate.Value.ToString().Trim();
                }

                if (txtFromContractValue.Value != "" && txtToContractValue.Value != "")
                {
                    objReport.AnnualValue = txtFromContractValue.Value.ToString().Trim();
                    objReport.AnnualValueTo = txtToContractValue.Value.ToString().Trim();
                }

                if (ddlIsCustomer.SelectedIndex != 0)
                    objReport.ClientId = Convert.ToInt32(ddlIsCustomer.Value);
                if (ddlChangeOfControl.SelectedIndex != 0)
                    objReport.ChangeOfControl = ddlChangeOfControl.Value.ToString().Trim();
                if (ddlAgreementClause.SelectedIndex != 0)
                    objReport.EntireAgreementClause = ddlAgreementClause.Value.ToString().Trim();
                if (ddlStrictLiability.SelectedIndex != 0)
                    objReport.Strictliability = ddlStrictLiability.Value.ToString().Trim();
                if (ddlThirdPartyGuarantee.SelectedIndex != 0)
                    objReport.ThirdPartyGuaranteeRequired = ddlThirdPartyGuarantee.Value.ToString().Trim();
                /* Added by kk on 18 July 2016  */
                if (ddlPriority.SelectedIndex != 0)
                    objReport.Priority = ddlPriority.Value.ToString().Trim();

                objReport.TerritoryId = Convert.ToInt32(ddlTerritary.Value);
                objReport.ContractingPartyId = Convert.ToInt32(ddlContractingParty.Value);

                objReport.ContractStatusId = Convert.ToInt32(ddlContractStatus.Value);
                objReport.AssignedDepartmentId = Convert.ToInt32(ddlAssignedDept.Value);
                objReport.AssignedUserId = Convert.ToInt32(ddlAssignUser.Value);

                #endregion

                string status = objReport.UpdateRecord();
                if (status != "0")
                {
                    ViewState[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
                }

                objReport.CustomReportId = Convert.ToInt32(ViewState[Declarations.CustomReportId]);
                objReport.MailId = txtMailId.Text.Trim();

                if (txtMailId.Text.Trim() != "")
                    objReport.RecipiantMailId = "," + hdnUsers.Value;
                else
                    objReport.RecipiantMailId = hdnUsers.Value;


                #region Schedule
                List<string> mylist = (List<string>)ViewState["Chkdata"];
                if (chkDailys.Checked == true)
                {
                    objReport.Flag = 4;
                    objReport.ScheduleTime = txtDailyTime.Text.Trim();
                    objReport.ScheduleType = "Daily";
                    status = objReport.InsertRecord();
                    if (mylist != null)
                    {
                        mylist.Remove("Daily");
                    }
                }
                if (chkWeeklys.Checked == true)
                {
                    objReport.Flag = 4;
                    objReport.ScheduleTime = txtWeeklyTime.Text.Trim();
                    objReport.ScheduleWeek = ddlWeeklyDay.SelectedValue;
                    objReport.ScheduleType = "Weekly";
                    status = objReport.InsertRecord();
                    if (mylist != null)
                    {
                        mylist.Remove("Weekly");
                    }
                }
                if (chkMonthlys.Checked == true)
                {
                    objReport.ScheduleWeek = "";
                    objReport.Flag = 4;
                    objReport.ScheduleTime = txtMonthlyTime.Text.Trim();
                    if (txtMonthlyMonth.Text.Trim() == "January")
                        objReport.ScheduleMonth = "1";
                    else if (txtMonthlyMonth.Text.Trim() == "February")
                        objReport.ScheduleMonth = "2";
                    else if (txtMonthlyMonth.Text.Trim() == "March")
                        objReport.ScheduleMonth = "3";
                    else if (txtMonthlyMonth.Text.Trim() == "April")
                        objReport.ScheduleMonth = "4";
                    else if (txtMonthlyMonth.Text.Trim() == "May")
                        objReport.ScheduleMonth = "5";
                    else if (txtMonthlyMonth.Text.Trim() == "June")
                        objReport.ScheduleMonth = "6";
                    else if (txtMonthlyMonth.Text.Trim() == "July")
                        objReport.ScheduleMonth = "7";
                    else if (txtMonthlyMonth.Text.Trim() == "August")
                        objReport.ScheduleMonth = "8";
                    else if (txtMonthlyMonth.Text.Trim() == "September")
                        objReport.ScheduleMonth = "9";
                    else if (txtMonthlyMonth.Text.Trim() == "October")
                        objReport.ScheduleMonth = "10";
                    else if (txtMonthlyMonth.Text.Trim() == "November")
                        objReport.ScheduleMonth = "11";
                    else if (txtMonthlyMonth.Text.Trim() == "December")
                        objReport.ScheduleMonth = "12";

                    objReport.ScheduleDay = ddlMontlyDay.SelectedValue;
                    objReport.ScheduleType = "Monthly";
                    status = objReport.InsertRecord();
                    if (mylist != null)
                    {
                        mylist.Remove("Monthly");
                    }
                }

                if (chkYearlys.Checked == true)
                {
                    objReport.ScheduleWeek = "";
                    objReport.ScheduleDay = ddlYearlyDay.SelectedValue;
                    objReport.Flag = 4;
                    objReport.ScheduleTime = txtYearlyTime.Text.Trim();
                    if (txtYearlyMonth.Text.Trim() == "January")
                        objReport.ScheduleMonth = "1";
                    else if (txtYearlyMonth.Text.Trim() == "February")
                        objReport.ScheduleMonth = "2";
                    else if (txtYearlyMonth.Text.Trim() == "March")
                        objReport.ScheduleMonth = "3";
                    else if (txtYearlyMonth.Text.Trim() == "April")
                        objReport.ScheduleMonth = "4";
                    else if (txtYearlyMonth.Text.Trim() == "May")
                        objReport.ScheduleMonth = "5";
                    else if (txtYearlyMonth.Text.Trim() == "June")
                        objReport.ScheduleMonth = "6";
                    else if (txtYearlyMonth.Text.Trim() == "July")
                        objReport.ScheduleMonth = "7";
                    else if (txtYearlyMonth.Text.Trim() == "August")
                        objReport.ScheduleMonth = "8";
                    else if (txtYearlyMonth.Text.Trim() == "September")
                        objReport.ScheduleMonth = "9";
                    else if (txtYearlyMonth.Text.Trim() == "October")
                        objReport.ScheduleMonth = "10";
                    else if (txtYearlyMonth.Text.Trim() == "November")
                        objReport.ScheduleMonth = "11";
                    else if (txtYearlyMonth.Text.Trim() == "December")
                        objReport.ScheduleMonth = "12";

                    objReport.ScheduleYear = txtYearlyYear.Text.Trim();
                    objReport.ScheduleType = "Yearly";
                    status = objReport.InsertRecord();
                    if (mylist != null)
                    {
                        mylist.Remove("Yearly");
                    }
                }

                if (mylist != null)
                {
                    if (mylist.Count > 0)
                    {
                        for (int j = 0; j < mylist.Count; j++)
                        {
                            objReport.Flag = 6;
                            objReport.ScheduleType = Convert.ToString(mylist[j]);
                            status = objReport.InsertRecord();
                        }
                    }
                }
                #endregion

                Page.Message("1", hdnPrimeId.Value);
                Response.Redirect("CustomReports.aspx");

                #endregion
            }
        }


    }

    public void UpdateAccessIds()
    {
        objReport.UserID = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objReport.Flag = 3;
        DataTable dtAccessId = objReport.GetReportAccessId();
    }

    public DataTable GetAccessTable()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("ChildId", typeof(int));
        dt.Columns.Add("Add", typeof(string));
        dt.Columns.Add("Update", typeof(string));
        dt.Columns.Add("Delete", typeof(string));
        dt.Columns.Add("View", typeof(string));
        dt.Rows.Add
         (
            int.Parse("601"),
            "N",
             "N",
             "N",
             "Y"
          );

        return dt;
    }

    public DataTable GetAccessTableFeature()
    {
        objReport.UserID = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objReport.Flag = 1;
        DataTable dtAccessId = objReport.GetReportAccessId();
        DataTable dt = new DataTable();
        dt.Columns.Add("AccessId", typeof(int));

        dt.Rows.Add
                         (
                            int.Parse(dtAccessId.Rows[0]["AccessId"].ToString())
                          );

        return dt;
    }

    public void GetAccessId()
    {
        objReport.UserID = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objReport.Flag = 2;
        DataTable dtAccessId = objReport.GetReportAccessId();
        ViewState["RoleIds"] = int.Parse(dtAccessId.Rows[0]["RoleId"].ToString());
      
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Session[Declarations.CustomReportId] = null;
        Server.Transfer("CustomReports.aspx");
    }

    private List<SelectControlFields> FillMasterData(string TableName)
    {
        try
        {
            ICommonMaster obj = FactoryMaster.GetCommonMasterDetail();
            obj.TableName = TableName;
            List<SelectControlFields> myList = obj.SelectData();
            return myList;

        }
        catch (Exception ex)
        {
            Response.Write("TableName :" + TableName + "<br/>" + ex.Message);
            return null;
        }
    }

    [System.Web.Services.WebMethod]
    public static string GetReportName(string name)
    {

        //***********************************
        ICustomReports obj = FactoryReports.CustomReportDetail();
        //obj.CreateDALObjForMembershipWork();
        obj.Customer_Name = name.Trim();
        if (name != "")
        {
            int cnt = obj.GetReportName();
            if (cnt > 0)
                return "Report Name is already exists.";
            else
                return "";
        }
        else
            return "";

    }

}