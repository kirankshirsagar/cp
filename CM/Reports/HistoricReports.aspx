﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/CM.master"
    AutoEventWireup="true" CodeFile="HistoricReports.aspx.cs" Inherits="Reports_HistoricReports" %>

<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/reportrightactions.ascx" TagName="rptExport" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridNavigation.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script src="../JQueryValidations/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    <script src="../CommonScripts/datePickerReports.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../scripts/jquery-ui-1.8.16.custom.css" />
    <script type="text/javascript">
        $("#reportLink").addClass("menulink");
        $("#reportTab").addClass("selectedtab");

        
    </script>
    <%--<uc1:rptExport ID="rptExport" runat="server" />--%>
    <div class="contextual">
    
     <input id="hdnGlobalReportFlag" runat="server" clientidmode="Static" type="hidden" />

     <input id="hdnGlobalReportLink" runat="server" clientidmode="Static" type="hidden" />

       <asp:LinkButton ID="lnkPredefineSheduleReport" ClientIDMode="Static" CssClass="icon icon-library areadOnly"
        Style="cursor: pointer" runat="server" OnClick="lnkPredefineSheduleReport_Click">Schedule this report</asp:LinkButton>
    <asp:LinkButton ID="btnExportToExcel" ClientIDMode="Static" CssClass="icon icon-library"
        runat="server" onclick="btnExportToExcel_Click">Export to Excel</asp:LinkButton>

    <asp:LinkButton ID="btnExportToPDF" ClientIDMode="Static" CssClass="icon icon-masters"
        runat="server" onclick="btnExportToPDF_Click">Export to PDF</asp:LinkButton>
    
        <div style="display:none">
        <asp:GridView ID="gvDetails" runat="server">
        </asp:GridView>
        </div>

    <asp:DropDownList ID="ddlReport" ClientIDMode="Static" Width="350px" 
         CssClass="chzn-select required chzn-select" runat="server" AutoPostBack="true" 
         OnSelectedIndexChanged="ddlReport_SelectedIndexChanged">
         
    </asp:DropDownList>

    <script type="text/javascript">
        $('#ddlReport').change(function () {
            $('#hdnGlobalReportLink').val($('#ddlReport').val());
        });
    </script>

</div>
      <h2> My Reports </h2>
    <h2 style="font-size: medium">
    <asp:Label ID="lblHeader"  runat="server" >Request History</asp:Label>
       </h2>
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnReportFlag" runat="server" clientidmode="Static" type="hidden" />
    <div style="margin: 0; padding: 0; display: inline">
        <div id="reportFilter" style="display: none">
            <div id="query_form_content1" class="hide-when-print">
                <fieldset id="filters1" class="collapsible">
                    <legend onclick="toggleFieldset(this);">Filters</legend>
                    <div style="">
                        <table style="width: 100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <table id="filters-table" width="100%" cellpadding="3" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td width="10%">
                                                        Keywords
                                                    </td>
                                                    <td width="1%">
                                                        :
                                                    </td>
                                                    <td width="20%">
                                                        <input id="txtSearch" clientidmode="Static" runat="server" type="text" maxlength="50"
                                                            onkeydown="return (event.keyCode!=13);" />
                                                    </td>
                                                    <td width="10%" align="right">
                                                        From Date
                                                    </td>
                                                    <td width="1%">
                                                        :
                                                    </td>
                                                    <td width="15%" align="left">
                                                        <input id="txtFromDate" class="datepicker InputBlock" clientidmode="Static" runat="server"
                                                            style="width: 90px" type="text" onkeydown="return (event.keyCode!=13);" />
                                                    </td>
                                                    <td width="10%" align="right">
                                                        To Date
                                                    </td>
                                                    <td width="1%">
                                                        :
                                                    </td>
                                                    <td width="32%" align="left">
                                                        <input id="txtToDate" class="datepicker InputBlock" clientidmode="Static" runat="server"
                                                            style="width: 90px" type="text" onkeydown="return (event.keyCode!=13);" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="10%">
                                                        Contract Type
                                                    </td>
                                                    <td width="1%">
                                                        :
                                                    </td>
                                                    <td colspan="7" width="89%">
                                                        <p>
                                                            <br />
                                                            <select id="ddlContracttype" clientidmode="Static" runat="server" style="width: 30%"
                                                                class="chzn-select required chzn-select" onchange="GetContractTypeID();">
                                                                <option></option>
                                                            </select>
                                                            <em></em>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </fieldset>
            </div>
            <p class="buttons hide-when-print">
                <asp:LinkButton ID="btnSearch1" runat="server" OnClientClick="return searchClick();"
                    CssClass="icon icon-checked" OnClick="btnSearch_Click">Filter</asp:LinkButton>
                <asp:LinkButton ID="btnShowAll" runat="server" OnClientClick="return resetClick();"
                    CssClass="icon icon-reload" OnClick="btnSearch_Click">Reset Filter</asp:LinkButton>
            </p>
        </div>
        <div>
            <p>
                <b>Time Period For : </b>
                <asp:LinkButton ID="btnAll" runat="server" OnClientClick="SetMonth(0);"
                    CssClass="icon icon-checked" OnClick="btnSearch_Click" >All Months</asp:LinkButton>&nbsp;&nbsp;
                <asp:LinkButton ID="btnSearch" runat="server" OnClientClick="SetMonth(1);" 
                    CssClass="icon icon-checked" OnClick="btnSearch_Click">Last Month</asp:LinkButton>&nbsp;&nbsp;
                <asp:LinkButton ID="btnThreeMonth" runat="server" OnClientClick="SetMonth(3);"
                    CssClass="icon icon-checked" OnClick="btnSearch_Click" >Last 3 Months</asp:LinkButton>&nbsp;&nbsp;
                <asp:LinkButton ID="btnSixMonth" runat="server" OnClientClick="SetMonth(6);"
                    CssClass="icon icon-checked" OnClick="btnSearch_Click" >Last 6 Months</asp:LinkButton>&nbsp;&nbsp;
                <asp:LinkButton ID="btnTweleMonth" runat="server" OnClientClick="SetMonth(12);"
                    CssClass="icon icon-checked" OnClick="btnSearch_Click" >Last 12 Months</asp:LinkButton>
                    
                <%-- <asp:RadioButton ID="rdoonemonth" ClientIDMode="Static"
                    Style="cursor: pointer" runat="Server" Onclick="SetDate(1)"  /><b>1 Month</b>
                <asp:RadioButton ID="rdothreemonth" ClientIDMode="Static"
                    Style="cursor: pointer" runat="Server" Onclick="SetDate(3)"  /><b>3 Month</b>
                <asp:RadioButton ID="rdosixmonth"  ClientIDMode="Static"
                    Style="cursor: pointer" runat="Server" Onclick="SetDate(6)"  /><b>6 Month</b>
                <asp:RadioButton ID="rdotwelemonth" ClientIDMode="Static"
                    Style="cursor: pointer" runat="Server" Onclick="SetDate(12)" /><b>12 Month</b>--%>
            </p>
        </div>
        <br />
        <div class="autoscroll">
            <table width="100%">
                <asp:Repeater ID="rptReports" runat="server">
                    <HeaderTemplate>
                        <table id="masterDataTable" class="reportTable list issues" width="100%">
                            <thead>
                                <tr>
                                    <th width="5%">
                                        <asp:LinkButton ID="btnContractID" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Contract ID</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="btnRequestType" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Request Type</asp:LinkButton>
                                    </th>
                                    <th width="5%">
                                        <asp:LinkButton ID="btngenerationDate" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Request Generation Date</asp:LinkButton>
                                    </th>
                                    <th width="5%">
                                        <asp:LinkButton ID="btnContractType" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Contract Type</asp:LinkButton>
                                    </th>
                                    <th width="5%">
                                        <asp:LinkButton ID="btnEffectiveDate" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Effective Date</asp:LinkButton>
                                    </th>
                                    <th width="5%">
                                        <asp:LinkButton ID="btnExpiryDate" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Expiration Date</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="btnRequestorName" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Requester Name</asp:LinkButton>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <%#Eval("Contract_ID")%>
                            </td>
                            <td>
                                <%#Eval("Request_Type")%>
                            </td>
                            <td>
                                <%#Eval("Request_generation_Date")%>
                            </td>
                            <td>
                                <%#Eval("Contract_Type")%>
                            </td>
                            <td>
                                <%#Eval("Effective_Date")%>
                            </td>
                            <td>
                                <%#Eval("Expiration_Date")%>
                            </td>
                            <td>
                                <%#Eval("Requestor_Name")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
            <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
        </div>
    </div>
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <asp:HiddenField ID="hdnContractTypeID" runat="server" Value="0" ClientIDMode="Static" />
      <asp:HiddenField ID="hdnMonth" runat="server" Value="0" ClientIDMode="Static" />
       <script type="text/javascript">
           $(document).ready(function () {
               //alert($('#hdnGlobalReportFlag').val());
               $.ajax({
                   type: "POST",
                   url: "../Reports/PredefinedScheduleReport.aspx/GetReportIDs",
                   data: "{ReportId:'" + $('#hdnGlobalReportFlag').val() + "'}",
                   contentType: "application/json; charset=utf-8",
                   dataType: "json",
                   async: false,
                   success: function (output) {
                       if (output.d != "") {
                           $('#lnkPredefineSheduleReport').text('Scheduled report');
                       }
                   }
               });
           });
    </script>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
        function GetContractTypeID() {
            var ID = $("#ddlContracttype").val();
            $("#hdnContractTypeID").val(ID);
        }

        function SetDate(ctl) {
            var date = new Date();
            var Frommonth = date.getMonth();
            var FromDay = date.getDay();
            var FromDate = (date.getDay() + "-" + GetMonthName(Frommonth) + "-" + date.getFullYear());
            date.setMonth(date.getMonth() + ctl);
            var Monthname = date.getMonth();
            var YearName = date.getFullYear();
            var DayName = date.getDay();
            if (DayName == "0") {
                DayName = FromDay;
            }
            var ToDate = (DayName + "-" + GetMonthName(Monthname) + "-" + YearName);
            $("#txtFromDate").val(FromDate);
            $("#txtToDate").val(ToDate);
            $("#btnSearch").click();
        }

        function SetMonth(ctl) {
            $("#hdnMonth").val(ctl);
            $("#hdnSearch").val("1");
            
        }

        function GetMonthName(Month) {
            var Name = "";
            switch (Month) {

                case 1:
                    Name = "Jan";
                    break;

                case 2:
                    Name = "Feb";
                    break;
                case 3:
                    Name = "Mar";
                    break;
                case 4:
                    Name = "Apr";
                    break;

                case 5:
                    Name = "May";
                    break;

                case 6:
                    Name = "Jun";
                    break;

                case 7:
                    Name = "Jul";
                    break;

                case 8:
                    Name = "Aug";
                    break;

                case 9:
                    Name = "Sep";
                    break;

                case 10:
                    Name = "Oct";
                    break;

                case 11:
                    Name = "Nov";
                    break;

                case 12:
                    Name = "Dec";
                    break;
            }

            return Name;
        } 
        
    </script>
</asp:Content>
