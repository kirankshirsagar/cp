﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/CM.master"
    AutoEventWireup="true" CodeFile="CustomerKeyobligationsContract.aspx.cs" Inherits="Reports_CustomerKeyobligationsContract" %>

<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/reportrightactions.ascx" TagName="rptExport" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridNavigation.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script src="../JQueryValidations/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    <script src="../CommonScripts/datePickerReports.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../scripts/jquery-ui-1.8.16.custom.css" />
    <script type="text/javascript">
        $("#reportLink").addClass("menulink");
        $("#reportTab").addClass("selectedtab");

    </script>
    <uc1:rptExport ID="rptExport" runat="server" />
      <h2> My Reports </h2>
    <h2 style="font-size: medium">
        Customer Contracts - Key Obligations Analysis</h2>
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnReportFlag" runat="server" clientidmode="Static" type="hidden" />
    <div style="margin: 0; padding: 0; display: inline">
    <div id="reportFilter" style="display: none">
            <div id="query_form_content" class="hide-when-print">
                <fieldset id="filters" class="collapsible">
                    <legend onclick="toggleFieldset(this);">Filters</legend>
                   <div style="">
                        <table style="width: 100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <table id="filters-table" width="100%" cellpadding="3" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td width="10%">
                                                        Keywords
                                                    </td>
                                                    <td width="1%">
                                                        :
                                                    </td>
                                                    <td width="20%">
                                                        <input id="txtSearch" clientidmode="Static" runat="server" type="text" maxlength="50"
                                                            onkeydown="return (event.keyCode!=13);" />
                                                    </td>
                                                    <td width="10%" align="right">
                                                        From Date
                                                    </td>
                                                    <td width="1%">
                                                        :
                                                    </td>
                                                    <td width="15%" align="left">
                                                        <input id="txtFromDate" class="datepicker InputBlock" clientidmode="Static" runat="server"
                                                            style="width: 90px" type="text" onkeydown="return (event.keyCode!=13);" />
                                                    </td>
                                                    <td width="10%" align="right">
                                                        To Date
                                                    </td>
                                                    <td width="1%">
                                                        :
                                                    </td>
                                                    <td width="32%" align="left">
                                                        <input id="txtToDate" class="datepicker InputBlock" clientidmode="Static" runat="server"
                                                            style="width: 90px" type="text" onkeydown="return (event.keyCode!=13);" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="10%">
                                                        Contract Type
                                                    </td>
                                                    <td width="1%">
                                                        :
                                                    </td>
                                                    <td colspan="7" width="89%">
                                                        <p>
                                                            <br />
                                                            <select id="ddlContracttype" clientidmode="Static" runat="server" style="width: 30%"
                                                                class="chzn-select required chzn-select" onchange="GetContractTypeID();">
                                                                <option></option>
                                                            </select>
                                                            <em></em>
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </fieldset>
            </div>
            <p class="buttons hide-when-print">
                <asp:LinkButton ID="btnSearch" runat="server" OnClientClick="return searchClick();"
                    CssClass="icon icon-checked" OnClick="btnSearch_Click">Filter</asp:LinkButton>
                <asp:LinkButton ID="btnShowAll" runat="server" OnClientClick="return resetClick();"
                    CssClass="icon icon-reload" OnClick="btnSearch_Click">Reset Filter</asp:LinkButton>
            </p>
        </div>
        <div class="autoscroll">
            <table width="100%">
                <asp:Repeater ID="rptReports" runat="server">
                
                    <HeaderTemplate>
                        <table id="masterDataTable" class="reportTable list issues" width="100%">
                            <thead>
                                <tr>
                                    <th width="5%">
                                        <asp:LinkButton ID="btnContractID" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Contract ID</asp:LinkButton>
                                    </th>
                                    <th width="5%">
                                        <asp:LinkButton ID="btnContractType" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Contract Type</asp:LinkButton>
                                    </th>
                                    <th width="5%">
                                        <asp:LinkButton ID="btnEffectiveDate" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Date of Agreement</asp:LinkButton>
                                    </th>
                                    <th width="5%">
                                        <asp:LinkButton ID="btnExpiryDate" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Expiration Date</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="btnCustomerName" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Customer Name</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="btnRequestorName" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Requester Name</asp:LinkButton>
                                    </th>
                                    <th width="5%">
                                        <asp:LinkButton ID="btnLiabilityCap" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Liability Cap</asp:LinkButton>
                                    </th>
                                    <th width="5%">
                                        <asp:LinkButton ID="btnIndemnity" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Indemnity</asp:LinkButton>
                                    </th>
                                    <th width="5%">
                                        <asp:LinkButton ID="btnChangeofControl" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Change Of Control</asp:LinkButton>
                                    </th>
                                    <th width="5%">
                                        <asp:LinkButton ID="btnEntireagreement" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Entire Agreement</asp:LinkButton>
                                    </th>
                                    <th width="5%">
                                        <asp:LinkButton ID="btnStrictliability" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Strict Liability</asp:LinkButton>
                                    </th>
                                    <th width="5%">
                                        <asp:LinkButton ID="btnThirdpartyguarantee" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Third Party Guarantee</asp:LinkButton>
                                    </th>
                                    <th width="5%">
                                        <asp:LinkButton ID="btnInsurance" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Insurance</asp:LinkButton>
                                    </th>
                                    <th width="5%">
                                        <asp:LinkButton ID="btnTermination" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Termination</asp:LinkButton>
                                    </th>
                                    <th width="5%">
                                        <asp:LinkButton ID="btnNovation" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Assignment/Novation/Subcontracting</asp:LinkButton>
                                    </th>

                                    <th width="10%">
                                        <asp:LinkButton ID="btnOthers" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Others</asp:LinkButton>
                                    </th>

                                    <th width="10%">
                                        <asp:LinkButton ID="LinkButton10" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Is Indirect Consequential Losses</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="LinkButton1" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Indirect Consequential Losses</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="LinkButton2" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Is Warranties</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="LinkButton3" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Warranties</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="LinkButton4" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Is Force Majeure</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="LinkButton5" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Force Majeure Description</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="LinkButton6" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Is Set Off Rights</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="LinkButton7" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Set Off Rights Description</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="LinkButton8" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Is Governing Law / Applicable Law</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="LinkButton9" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Governing Law / Applicable Law Description</asp:LinkButton>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <%#Eval("Contract_ID")%>
                            </td>
                            <td>
                                <%#Eval("Contract_Type")%>
                            </td>
                            <td>
                                <%#Eval("Effective_Date")%>
                            </td>
                            <td>
                                <%#Eval("Expiry_Date")%>
                            </td>
                            <td>
                                <%#Eval("Customer_Name")%>
                            </td>
                            <td>
                                <%#Eval("Requestor_Name")%>
                            </td>
                            <td>
                            <%#Eval("Liability_Cap")%>
                            </td>
                            <td>
                                <%#Eval("Indemnity")%>
                            </td>
                            <td>
                                <%#Eval("Change_of_Control")%>
                            </td>
                            <td>
                                <%#Eval("Entire_agreement")%>
                            </td>
                            <td>
                                <%#Eval("Strict_liability")%>
                            </td>
                            <td>
                                <%#Eval("Third_party_guarantee")%>
                            </td>
                            <td>
                                <%#Eval("Insurance")%>
                            </td>
                            <td>
                                <%#Eval("Termination")%>
                            </td>
                            <td>
                                <%#Eval("Assignment_Novation_Subcontracting")%>
                            </td>
                            <td>
                                <%#Eval("Others")%>
                            </td>

                            <td>
                                <%#Eval("IsIndirectConsequentialLosses")%>
                            </td>
                            <td>
                                <%#Eval("IndirectConsequentialLosses")%>
                            </td>
                            <td>
                                <%#Eval("IsWarranties")%>
                            </td>
                            <td>
                                <%#Eval("Warranties")%>
                            </td>                           
                            <td>
                                <%#Eval("IsForceMajeure")%>
                            </td>
                            <td>
                                <%#Eval("ForceMajeure")%>
                            </td>
                            <td>
                                <%#Eval("IsSetOffRights")%>
                            </td>
                            <td>
                                <%#Eval("SetOffRights")%>
                            </td>
                            <td>
                                <%#Eval("IsGoverningLaw")%>
                            </td>
                            <td>
                                <%#Eval("GoverningLaw")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
            <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
        </div>
    </div>
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
   <asp:HiddenField ID="hdnContractTypeID" runat="server" Value="0" ClientIDMode="Static" />
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());

        function GetContractTypeID() {

            var ID = $("#ddlContracttype").val();
            $("#hdnContractTypeID").val(ID);
        }
         </script>
</asp:Content>
