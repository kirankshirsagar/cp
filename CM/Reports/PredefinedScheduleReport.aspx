﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="PredefinedScheduleReport.aspx.cs" Inherits="Reports_PredefinedScheduleReport" %>

<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/reportrightactions.ascx" TagName="rptExport" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridNavigation.js" type="text/javascript"></script>
    <script src="../JQueryValidations/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    <script src="../CommonScripts/datePickerReports.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../scripts/jquery-ui-1.8.16.custom.css" />
    <script src="../CommonScripts/Timepicker.js" type="text/javascript"></script>
    <link href="../CalendarCSS/jquery.timepicker.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $("#reportLink").addClass("menulink");
        $("#reportTab").addClass("selectedtab");
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#lnkPredefineSheduleReport').css('display', 'none');
            $('#ddlReport').removeClass('required');
            $('#btnExportToExcel').css('display', 'none');
            $('#btnExportToPDF').css('display', 'none');

            $('.ckAllCheck').click(function () {
                if ($('#chkAllSelect').prop('checked') == true) {
                    $('.chkChoice').find(':checkbox').each(function () {
                        $(this).prop('checked', true);
                    });
                }
                else {
                    $('.chkChoice').find(':checkbox').each(function () {
                        $(this).prop('checked', false);
                    });
                }
            });
        });
    </script>
    <style type="text/css">
        .chkChoice
        {
            margin-top: 5px;
        }
        .chkChoice input
        {
            margin-left: 15px;
        }
        .chkChoice td
        {
            padding: 2px 0px 2px 0px;
        }
        .chkChoice label
        {
            position: relative;
            margin-left: 5px;
            margin-top: 15px;
        }
        
        fieldset.collapsible legend
        {
            padding-left: 2px;
            background: none;
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnReportFlag" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnColumnName" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnColumnOrder" runat="server" clientidmode="Static" type="hidden" />
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <uc1:rptExport ID="rptExport" runat="server" />
    <h2>
        My Reports
    </h2>
    <h2 style="font-size: medium">
    </h2>
    <div style="margin: 0; padding: 0; display: inline">
        <div id="reportFilter">
            <div id="query_form_content" class="hide-when-print">
                <%--<fieldset id="filters" class="collapsible">--%>
                <div style="">
                    <div>
                        <label for="time_entry_issue_id">
                            Report Name :</label>
                        <label id="lblReportName" runat="server" clientidmode="Static" style="color: Black;
                            margin-left: 0px; width: 30%;">
                        </label>
                        <em></em>
                    </div>
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="UpAll" runat="server">
                                        <ContentTemplate>
                                            <br />
                                            <fieldset>
                                                <legend style="color: Black;">Schedule </legend>
                                                <table width="100%">
                                                    <tr>
                                                        <td width="18%">
                                                            <asp:CheckBox ID="chkDailys" runat="server" Text="&nbsp;Daily" CssClass="ShecduleCheck"
                                                                Style="margin-bottom: 5px;" ClientIDMode="Static" />
                                                        </td>
                                                        <td width="10%">
                                                            <asp:TextBox ID="txtDailyTime" runat="server" ClientIDMode="Static" placeholder="Time"
                                                                MaxLength="5" Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td width="12%">
                                                        </td>
                                                        <td width="10%">
                                                        </td>
                                                        <td width="15%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="chkWeeklys" runat="server" Text="&nbsp;Weekly" CssClass="ShecduleCheck"
                                                                Style="margin-bottom: 5px;" ClientIDMode="Static" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtWeeklyTime" runat="server" ClientIDMode="Static" placeholder="Time"
                                                                Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlWeeklyDay" runat="server" ClientIDMode="Static" CssClass="chzn-select"
                                                                Width="70%">
                                                                <asp:ListItem Value="0">--Select Week--</asp:ListItem>
                                                                <asp:ListItem Value="1">Sunday</asp:ListItem>
                                                                <asp:ListItem Value="2">Monday</asp:ListItem>
                                                                <asp:ListItem Value="3">Tuesday</asp:ListItem>
                                                                <asp:ListItem Value="4">Wednesday</asp:ListItem>
                                                                <asp:ListItem Value="5">Thursday</asp:ListItem>
                                                                <asp:ListItem Value="6">Friday</asp:ListItem>
                                                                <asp:ListItem Value="7">Saturday</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <em></em>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="chkMonthlys" runat="server" Text="&nbsp;Monthly" CssClass="ShecduleCheck"
                                                                Style="margin-bottom: 5px;" ClientIDMode="Static" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtMonthlyTime" runat="server" ClientIDMode="Static" placeholder="Time"
                                                                MaxLength="5" Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlMontlyDay" runat="server" ClientIDMode="Static" CssClass="chzn-select"
                                                                Width="64%">
                                                                <asp:ListItem Value="0">--Select Day--</asp:ListItem>
                                                                <asp:ListItem Value="1">1</asp:ListItem>
                                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                                <asp:ListItem Value="3">3</asp:ListItem>
                                                                <asp:ListItem Value="4">4</asp:ListItem>
                                                                <asp:ListItem Value="5">5</asp:ListItem>
                                                                <asp:ListItem Value="6">6</asp:ListItem>
                                                                <asp:ListItem Value="7">7</asp:ListItem>
                                                                <asp:ListItem Value="8">8</asp:ListItem>
                                                                <asp:ListItem Value="9">9</asp:ListItem>
                                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                                <asp:ListItem Value="11">11</asp:ListItem>
                                                                <asp:ListItem Value="12">12</asp:ListItem>
                                                                <asp:ListItem Value="13">13</asp:ListItem>
                                                                <asp:ListItem Value="14">14</asp:ListItem>
                                                                <asp:ListItem Value="15">15</asp:ListItem>
                                                                <asp:ListItem Value="16">16</asp:ListItem>
                                                                <asp:ListItem Value="17">17</asp:ListItem>
                                                                <asp:ListItem Value="18">18</asp:ListItem>
                                                                <asp:ListItem Value="19">19</asp:ListItem>
                                                                <asp:ListItem Value="20">20</asp:ListItem>
                                                                <asp:ListItem Value="21">21</asp:ListItem>
                                                                <asp:ListItem Value="22">22</asp:ListItem>
                                                                <asp:ListItem Value="23">23</asp:ListItem>
                                                                <asp:ListItem Value="24">24</asp:ListItem>
                                                                <asp:ListItem Value="25">25</asp:ListItem>
                                                                <asp:ListItem Value="26">26</asp:ListItem>
                                                                <asp:ListItem Value="27">27</asp:ListItem>
                                                                <asp:ListItem Value="28">28</asp:ListItem>
                                                                <asp:ListItem Value="29">29</asp:ListItem>
                                                                <asp:ListItem Value="30">30</asp:ListItem>
                                                                <asp:ListItem Value="31">31</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <em></em>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtMonthlyMonth" runat="server" ClientIDMode="Static" placeholder="Month"
                                                                Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="chkYearlys" runat="server" Text="&nbsp;Yearly" CssClass="ShecduleCheck"
                                                                Style="margin-bottom: 5px;" ClientIDMode="Static" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtYearlyTime" runat="server" ClientIDMode="Static" placeholder="Time"
                                                                MaxLength="5" Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlYearlyDay" runat="server" ClientIDMode="Static" CssClass="chzn-select"
                                                                Width="64%">
                                                                <asp:ListItem Value="0">--Select Day--</asp:ListItem>
                                                                <asp:ListItem Value="1">1</asp:ListItem>
                                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                                <asp:ListItem Value="3">3</asp:ListItem>
                                                                <asp:ListItem Value="4">4</asp:ListItem>
                                                                <asp:ListItem Value="5">5</asp:ListItem>
                                                                <asp:ListItem Value="6">6</asp:ListItem>
                                                                <asp:ListItem Value="7">7</asp:ListItem>
                                                                <asp:ListItem Value="8">8</asp:ListItem>
                                                                <asp:ListItem Value="9">9</asp:ListItem>
                                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                                <asp:ListItem Value="11">11</asp:ListItem>
                                                                <asp:ListItem Value="12">12</asp:ListItem>
                                                                <asp:ListItem Value="13">13</asp:ListItem>
                                                                <asp:ListItem Value="14">14</asp:ListItem>
                                                                <asp:ListItem Value="15">15</asp:ListItem>
                                                                <asp:ListItem Value="16">16</asp:ListItem>
                                                                <asp:ListItem Value="17">17</asp:ListItem>
                                                                <asp:ListItem Value="18">18</asp:ListItem>
                                                                <asp:ListItem Value="19">19</asp:ListItem>
                                                                <asp:ListItem Value="20">20</asp:ListItem>
                                                                <asp:ListItem Value="21">21</asp:ListItem>
                                                                <asp:ListItem Value="22">22</asp:ListItem>
                                                                <asp:ListItem Value="23">23</asp:ListItem>
                                                                <asp:ListItem Value="24">24</asp:ListItem>
                                                                <asp:ListItem Value="25">25</asp:ListItem>
                                                                <asp:ListItem Value="26">26</asp:ListItem>
                                                                <asp:ListItem Value="27">27</asp:ListItem>
                                                                <asp:ListItem Value="28">28</asp:ListItem>
                                                                <asp:ListItem Value="29">29</asp:ListItem>
                                                                <asp:ListItem Value="30">30</asp:ListItem>
                                                                <asp:ListItem Value="31">31</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <em></em>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtYearlyMonth" runat="server" ClientIDMode="Static" placeholder="Month"
                                                                data-calendar="false" CssClass="monthly-picker" Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtYearlyYear" runat="server" ClientIDMode="Static" placeholder="Year"
                                                                MaxLength="4" onblur="yearValidation(this.value,event)" onkeypress="yearValidation(this.value,event)"
                                                                Width="100px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <br />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: initial;">
                                                            <label for="time_entry_issue_id" style="margin-left: 15px;">
                                                                <%--<span class="required">*</span>--%>
                                                                Select E-mail Recipient
                                                            </label>
                                                        </td>
                                                        <td colspan="4">
                                                            <select id="ddlUser" clientidmode="Static" runat="server" style="width: 100%" onchange="Userchange(this);"
                                                                class="chzn-select">
                                                                <option></option>
                                                            </select>
                                                            <em></em>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <br />
                                                        </td>
                                                    </tr>
                                                    <tr style="margin-top: -20px;">
                                                        <td>
                                                            <label for="time_entry_issue_id" style="margin-left: 15px;">
                                                                Additional E-mail Recipient
                                                            </label>
                                                        </td>
                                                        <td colspan="4">
                                                            <asp:TextBox ID="txtMailId" runat="server" class="EmailList" ClientIDMode="Static"
                                                                TextMode="MultiLine" placeholder="Mail Id" Width="298px" Height="50px"></asp:TextBox>
                                                            <em></em>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br />
                                            </fieldset>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <%-- </fieldset>--%>
            </div>
            <p class="buttons hide-when-print" style="margin-left: 90px;">
                <asp:Label ID="lblmessage" runat="server" Text="" ClientIDMode="Static" ForeColor="Red"
                    Style="filter: alpha(opacity=50); opacity: 0.5;"></asp:Label>
                <br />
                <asp:Button runat="server" Text="Save" ID="btnsubmit" OnClientClick="return ValidateContractID()"
                    OnClick="btnSubmit_Click" class="btn_validate" />
                <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnClear_Click" />
                <div id="dvMesssage" class="flash error" style="display: none">
                </div>
            </p>
        </div>
    </div>
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <asp:HiddenField ID="hdnContractTypeID" runat="server" Value="0" ClientIDMode="Static" />
    <input id="hdnUsers" runat="server" clientidmode="Static" type="hidden" />
    <style type="text/css">
        .chkChoice
        {
            margin-top: 5px;
        }
        .chkChoice input
        {
            margin-left: 15px;
        }
        .chkChoice td
        {
            padding: 2px 0px 2px 0px;
        }
        .chkChoice label
        {
            position: relative;
            margin-left: 5px;
            margin-top: 15px;
        }
        .ShecduleCheck
        {
            margin-left: 15px;
        }
        
        fieldset.collapsible legend
        {
            padding-left: 2px;
            background: none;
            cursor: pointer;
        }
        #txtYearlyMonth .ui-datepicker-calendar
        {
            display: none;
        }
        .ui-datepicker-calendar
        {
            display: none;
        }
        .ui-datepicker-year
        {
            display: none;
        }
        .tabular p
        {
            padding-left: 65px;
        }
        .hide-calendar .ui-datepicker-calendar
        {
            display: none;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            if ($('#ddlEmail3').val() != '')
                $('#tdmail3').css('visibility', 'visible').trigger("liszt:updated");
            if ($('#ddlEmail4').val() != '')
                $('#tdmail4').css('visibility', 'visible').trigger("liszt:updated");
            if ($('#ddlEmail5').val() != '')
                $('#tdmail5').css('visibility', 'visible').trigger("liszt:updated");
            if ($('#ddlEmail6').val() != '')
                $('#tdmail6').css('visibility', 'visible').trigger("liszt:updated");
            if ($('#ddlEmail7').val() != '')
                $('#tdmail7').css('visibility', 'visible').trigger("liszt:updated");
            if ($('#ddlEmail8').val() != '')
                $('#tdmail8').css('visibility', 'visible').trigger("liszt:updated");
            if ($('#ddlEmail9').val() != '')
                $('#tdmail9').css('visibility', 'visible').trigger("liszt:updated");
            if ($('#ddlEmail10').val() != '')
                $('#tdmail10').css('visibility', 'visible').trigger("liszt:updated");
        });
        function AddMores() {
            if ($('#tdmail3').css('visibility') == 'hidden') {
                $('#tdmail3').css('visibility', 'visible').trigger("liszt:updated");
            } else
                if ($('#tdmail4').css('visibility') == 'hidden') {
                    $('#tdmail4').css('visibility', 'visible').trigger("liszt:updated");
                }
                else
                    if ($('#tdmail5').css('visibility') == 'hidden') {
                        $('#tdmail5').css('visibility', 'visible').trigger("liszt:updated");
                    }
                    else
                        if ($('#tdmail6').css('visibility') == 'hidden') {
                            $('#tdmail6').css('visibility', 'visible').trigger("liszt:updated");
                        }
                        else
                            if ($('#tdmail7').css('visibility') == 'hidden') {
                                $('#tdmail7').css('visibility', 'visible').trigger("liszt:updated");
                            }
                            else
                                if ($('#tdmail8').css('visibility') == 'hidden') {
                                    $('#tdmail8').css('visibility', 'visible').trigger("liszt:updated");
                                }
                                else
                                    if ($('#tdmail9').css('visibility') == 'hidden') {
                                        $('#tdmail9').css('visibility', 'visible').trigger("liszt:updated");
                                    } else
                                        if ($('#tdmail10').css('visibility') == 'hidden') {
                                            $('#tdmail10').css('visibility', 'visible').trigger("liszt:updated");
                                        }
        }
        
    </script>
    <script type="text/javascript">
        //                $(function () {
        //                    $(".datepicker").datepicker({ showOn: "both",
        //                        buttonImage: "../Styles/css/icons/16/calendar_1.png",
        //                        buttonImageOnly: true,
        //                        prevText: 'Previous',
        //                        //yearRange: 'c-50:c+50',
        //                        yearRange: "-100:+10",
        //                        changeMonth: true,
        //                        dateFormat: 'dd-M-yy',
        //                        buttonText: '',
        //                        onClose: function (dateText, inst) {
        //                        },
        //                        beforeShow: function (el, dp) { $('#ui-datepicker-div').toggleClass('hide-calendar', $(el).is('[data-calendar="false"]')); },
        //                        changeYear: true
        //                    }).attr('placeholder', 'dd-MMM-yyyy').blur(function () {
        //                    });
        //                });

        function resetClick() {
            $("#txtEffectiveFromDate").val(''); $("#txtEffectiveToDate").val('');
            $("#txtExpiryFromDate").val(''); $("#txtExpiryToDate").val('');
            $("#txtRenewalFromDate").val(''); $("#txtRenewalToDate").val('');
            $("#txtSignatureFromDate").val(''); $("#txtSignatureToDate").val('');
            $("#txtRequestFromDate").val(''); $("#txtRequestToDate").val('');
            $("#txtFromAnnualValue").val(''); $("#txtToAnnualValue").val('');
            $("#txtFromTotalValue").val(''); $("#txtToTotalValue").val('');

            $("#ddlContractingParty").val('0').trigger("liszt:updated"); $("#ddlTerritary").val('0').trigger("liszt:updated");
            $("#ddlThirdParty").val('0').trigger("liszt:updated"); $("#ddlContractStatus").val('0').trigger("liszt:updated");
            $("#ddlCompititions").val('0').trigger("liszt:updated"); $("#ddlAssignedDept").val('0').trigger("liszt:updated");
            $("#ddlCycle").val('0').trigger("liszt:updated"); $("#ddlAssignUser").val('0').trigger("liszt:updated");
            $("#ddlSeason").val('0').trigger("liszt:updated"); $("#ddlDivision").val('0').trigger("liszt:updated");
            $("#ddlQuarter").val('0').trigger("liszt:updated"); $("#ddlLegalunit").val('0').trigger("liszt:updated");
            $("#ddlRevenueExpense").val('0').trigger("liszt:updated"); $("#ddlLawyer").val('0').trigger("liszt:updated");
            $("#ddlPhysicallocation").val('0').trigger("liszt:updated");
        }
        function ValidateFilterDate(obj) {
            if ($(obj).val() != "") {
                if ($(obj).next().next().val() == "") {
                    $(obj).next().next().val($(obj).val());
                }
            }
            if ($(obj).next().next().val() != "") {
                CompareDate(obj);
            }
        }

        function chkToDate(obj) {
            if ($(obj).prev().prev().val() == "") {
                $(obj).prev().prev().val($(obj).val());
            }

            if ($(obj).prev().prev().val() != "") {
                CompareDate($(obj).prev().prev());
            }
        }
        function CompareDate(obj) {
            //Note: 00 is month i.e. January
            var dateOne = new Date($(obj).val()); //Year, Month, Date
            var dateTwo = new Date($(obj).next().next().val()); //Year, Month, Date
            var DateSender = "Request Date";
            if ($(obj).attr("id").indexOf("Signature") > 0) {
                DateSender = "Signature Date";
            }
            if ($(obj).attr("id").indexOf("Effective") > 0) {
                DateSender = "Effective Date";
            }
            if ($(obj).attr("id").indexOf("Expiry") > 0) {
                DateSender = "Expiry Date";
            }

            if (dateOne > dateTwo) {
                alert(DateSender + " : To Date is greater than From Date.");
                $(obj).next().next().val($(obj).val());
            }
        }
    </script>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
        function GetContractTypeID() {
            var ID = $("#ddlContracttype").val();
            $("#hdnContractTypeID").val(ID);
        }
        function RecipentMessage() {
            alert('Some recipients were removed as their email ids have changed.');
        }
    </script>
    <script type="text/javascript">
        function yearValidation(year, ev) {
            var text = /^[0-9]+$/;
            if (ev.type == "blur" || year.length == 4 && ev.keyCode != 8 && ev.keyCode != 46) {
                if (year != 0) {
                    if ((year != "") && (!text.test(year))) {

                        alert("Please Enter Numeric Values Only");
                        return false;
                    }

                    if (year.length != 4) {
                        alert("Year is not proper. Please check");
                        return false;
                    }
                    var current_year = new Date().getFullYear();
                    var current_year = current_year + 100;
                    if ((year < 2015)) {
                        alert("Year should be in range 2015 to upcoming year");
                        return false;
                    }
                    return true;
                }
            }
        }
        var Usercnt = 0;
        function Userchange(obj) {
            if ($(obj).val() != '') {
                Usercnt++;
            }
            //            if (Usercnt > 3) {
            //                debugger;
            //                alert('You can not select maximum 10 recipents.');
            //                alert($(obj).find('option:selected').last().val());
            //                $(obj).find('option:selected').last().remove();
            //                $(obj).trigger("liszt:updated");
            //                return false;
            //            }

        }

        function ValidateContractID() {
            var check = 0;
            if ($("#chkDailys").is(":checked")) {
                check = 1;
            } else if ($("#chkWeeklys").is(":checked")) {
                check = 1;
            } else if ($("#chkMonthlys").is(":checked")) {
                check = 1;
            } else if ($("#chkYearlys").is(":checked")) {
                check = 1;
            }
            if (check == 0) {
                $('#ddlUser').removeClass('required');
            } else {
                $("#ddlUser").addClass("required");
            }

            if ($('#txtFromAnnualValue').val() != '') {
                if ($('#txtToAnnualValue').val() == '') {
                    alert('Please check annual value.');
                    return false
                }
            }
            if ($('#txtToAnnualValue').val() != '') {
                if ($('#txtFromAnnualValue').val() == '') {
                    alert('Please check annual value.');
                    return false
                }
            }

            if ($('#txtFromAnnualValue').val() != '' && $('#txtToAnnualValue').val() != '') {
                var FromAnn = parseInt($('#txtFromAnnualValue').val());
                var ToAnn = parseInt($('#txtToAnnualValue').val());
                if (FromAnn >= ToAnn) {
                    alert('Please check annual value.');
                    return false
                }
            }

            if ($('#txtFromTotalValue').val() != '') {
                if ($('#txtToTotalValue').val() == '') {
                    alert('Please check total value.');
                    return false
                }
            }
            if ($('#txtToTotalValue').val() != '') {
                if ($('#txtFromTotalValue').val() == '') {
                    alert('Please check total value.');
                    return false
                }
            }

            if ($('#txtFromTotalValue').val() != '' && $('#txtToTotalValue').val() != '') {
                var FromAnn = parseInt($('#txtFromTotalValue').val());
                var ToAnn = parseInt($('#txtToTotalValue').val());
                if (FromAnn >= ToAnn) {
                    alert('Please check total value.');
                    return false
                }
            }

            $('#hdnUsers').val(getCSVFromArray($('#ddlUser').val()));
            var array = $('#hdnUsers').val().split(",");
            if (array.length > 10) {
                alert('You can not select maximum 10 email recipents.');
                return false;
            }

            //return false;
        }
        $(document).ready(function () {
            $(".chzn-select").chosen({
                max_selected_options: 10
            });
            if ($("#chkMonthlys").is(":checked") == false) {
                $("#txtMonth").attr('disabled', 'disabled').trigger("liszt:updated");
                $("#txtMonthlyTime").attr('disabled', 'disabled').trigger("liszt:updated");
                $("#txtMonthlyMonth").attr('disabled', 'disabled').trigger("liszt:updated");
                $("#ddlMontlyDay").attr('disabled', 'disabled').trigger("liszt:updated");

                $("#ddlMontlyDay").val('0').change();
                $('#ddlMontlyDay').removeClass('required');
                $('#txtMonthlyTime').removeClass('required');
                $('#txtMonthlyMonth').removeClass('required');
                $('#txtMonthlyTime').val('');
                $('#txtMonthlyMonth').val('');

            }
            if ($("#chkDailys").is(":checked") == false) {
                $("#txtDailyTime").attr('disabled', 'disabled').trigger("liszt:updated");

                $('#txtDailyTime').removeClass('required');
                $('#txtDailyTime').val('');
            }
            if ($("#chkWeeklys").is(":checked") == false) {
                $("#txtWeeklyTime").attr('disabled', 'disabled').trigger("liszt:updated");
                $("#ddlWeeklyDay").attr('disabled', 'disabled').trigger("liszt:updated");

                $("#ddlWeeklyDay").val('0').change();
                $('#ddlWeeklyDay').removeClass('required');
                $('#txtWeeklyTime').removeClass('required');
                $('#txtWeeklyTime').val('');
            }
            if ($("#chkYearlys").is(":checked") == false) {
                $("#txtYearlyTime").attr('disabled', 'disabled').trigger("liszt:updated");
                $("#txtYearlyMonth").attr('disabled', 'disabled').trigger("liszt:updated");
                $("#txtYearlyYear").attr('disabled', 'disabled').trigger("liszt:updated");
                $("#ddlYearlyDay").attr('disabled', 'disabled').trigger("liszt:updated");

                $("#ddlYearlyDay").val('0').change();
                $('#ddlYearlyDay').removeClass('required');
                $('#txtYearlyTime').removeClass('required');
                $('#txtYearlyMonth').removeClass('required');
                $('#txtYearlyYear').removeClass('required');
                $('#txtYearlyTime').val('');
                $('#txtYearlyMonth').val('');
                $('#txtYearlyYear').val('');
            }

            $("#chkDailys").click(function () {
                if ($(this).is(":checked")) {
                    $("#txtDailyTime").removeAttr('disabled').trigger("liszt:updated");
                    $("#txtDailyTime").addClass("required");
                } else {
                    $("#txtDailyTime").attr('disabled', 'disabled').trigger("liszt:updated");
                    $('#txtDailyTime').removeClass('required');
                    $('#txtDailyTime').val('');
                }
            });

            $("#chkWeeklys").click(function () {
                if ($(this).is(":checked")) {
                    $("#ddlWeeklyDay").removeAttr('disabled').trigger("liszt:updated");
                    $("#txtWeeklyTime").removeAttr('disabled').trigger("liszt:updated");
                    $("#ddlWeeklyDay").addClass("required");
                    $("#txtWeeklyTime").addClass("required");
                } else {
                    $("#ddlWeeklyDay").val('0').change();
                    $("#ddlWeeklyDay").attr('disabled', 'disabled').trigger("liszt:updated");
                    $("#txtWeeklyTime").attr('disabled', 'disabled').trigger("liszt:updated");
                    $('#ddlWeeklyDay').removeClass('required');
                    $('#txtWeeklyTime').removeClass('required');
                    $('#txtWeeklyTime').val('');
                }
            });

            $("#chkMonthlys").click(function () {
                if ($(this).is(":checked")) {
                    $("#ddlMontlyDay").removeAttr('disabled').trigger("liszt:updated");
                    $("#txtMonthlyTime").removeAttr('disabled').trigger("liszt:updated");
                    $("#txtMonthlyMonth").removeAttr('disabled').trigger("liszt:updated");
                    $("#ddlMontlyDay").addClass("required");
                    $("#txtMonthlyTime").addClass("required");
                    $("#txtMonthlyMonth").addClass("required");
                } else {
                    $("#ddlMontlyDay").val('0').change();
                    $("#ddlMontlyDay").attr('disabled', 'disabled').trigger("liszt:updated");
                    $("#txtMonthlyTime").attr('disabled', 'disabled').trigger("liszt:updated");
                    $("#txtMonthlyMonth").attr('disabled', 'disabled').trigger("liszt:updated");
                    $('#ddlMontlyDay').removeClass('required');
                    $('#txtMonthlyTime').removeClass('required');
                    $('#txtMonthlyMonth').removeClass('required');
                    $('#txtMonthlyTime').val('');
                    $('#txtMonthlyMonth').val('');
                }
            });

            $("#chkYearlys").click(function () {
                if ($(this).is(":checked")) {
                    $("#ddlYearlyDay").removeAttr('disabled').trigger("liszt:updated");
                    $("#txtYearlyTime").removeAttr('disabled').trigger("liszt:updated");
                    $("#txtYearlyMonth").removeAttr('disabled').trigger("liszt:updated");
                    $("#txtYearlyYear").removeAttr('disabled').trigger("liszt:updated");
                    $("#txtYearlyTime").addClass("required");
                    $("#txtYearlyMonth").addClass("required");
                    $("#txtYearlyYear").addClass("required");
                    $("#ddlYearlyDay").addClass("required");
                } else {
                    $("#ddlYearlyDay").val('0').change();
                    $("#ddlYearlyDay").attr('disabled', 'disabled').trigger("liszt:updated");
                    $("#txtYearlyTime").attr('disabled', 'disabled').trigger("liszt:updated");
                    $("#txtYearlyMonth").attr('disabled', 'disabled').trigger("liszt:updated");
                    $("#txtYearlyYear").attr('disabled', 'disabled').trigger("liszt:updated");
                    $('#ddlYearlyDay').removeClass('required');
                    $('#txtYearlyTime').removeClass('required');
                    $('#txtYearlyMonth').removeClass('required');
                    $('#txtYearlyYear').removeClass('required');
                    $('#txtYearlyTime').val('');
                    $('#txtYearlyMonth').val('');
                    $('#txtYearlyYear').val('');
                }
            });

            try {
                $("#txtTime").timepicker({
                    timeFormat: 'HH:mm',
                    interval: 15 // 15 minutes
                });
                $("#txtDailyTime").timepicker({
                    timeFormat: 'HH:mm',
                    interval: 15 // 15 minutes
                });
                $("#txtWeeklyTime").timepicker({
                    timeFormat: 'HH:mm',
                    interval: 15 // 15 minutes
                });
                $("#txtMonthlyTime").timepicker({
                    timeFormat: 'HH:mm',
                    interval: 15 // 15 minutes
                });
                $("#txtYearlyTime").timepicker({
                    timeFormat: 'HH:mm',
                    interval: 15 // 15 minutes
                });

                //                $('#txtYearlyMonth').datepicker({
                //                    changeMonth: true,
                //                    showButtonPanel: true,
                //                    dateFormat: 'MM',
                //                    beforeShow: function() { $('#txtYearlyMonth').html('.ui-datepicker-calendar{display:none;}'); },
                //                    onClose: function (dateText, inst) {
                //                        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                //                    }
                //                    //,
                //                    //beforeShow: function () { $(this).datepicker('ui-datepicker-calendar').css('display', 'none'); }
                //                });

                $('.monthly-picker').datepicker({
                    changeMonth: true,
                    showButtonPanel: true,
                    dateFormat: 'MM',
                    beforeShow: function (el, dp) { $('#ui-datepicker-div').toggleClass('hide-calendar', $(el).is('[data-calendar="false"]')); },
                    onClose: function (dateText, inst) {
                        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                    }
                });

                //                $('#txtEffectiveFromDate').datepicker({
                //                    changeMonth: true,
                //                    showButtonPanel: true,
                //                    dateFormat: 'dd-M-yy',
                //                    beforeShow: function (el, dp) { $('#ui-datepicker-div').toggleClass('hide-calendar', $(el).is('[data-calendar="true"]')); }
                //                }).attr('placeholder', 'dd-MMM-yyyy').blur(function () {
                //                });

                $('#txtMonthlyMonth').datepicker({
                    changeMonth: true,
                    showButtonPanel: true,
                    dateFormat: 'MM',
                    onClose: function (dateText, inst) {
                        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                    }
                });

            }
            catch (e) {
                alert(e);
            }

        });
    </script>
</asp:Content>
