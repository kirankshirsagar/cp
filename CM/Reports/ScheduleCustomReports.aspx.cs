﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReportBLL;
using CommonBLL;
using System.Data;
using System.Data.SqlClient;
using System.Text;

public partial class Reports_ScheduleCustomReports : System.Web.UI.Page
{
    public bool View;
    ICustomReports objReport;

    // navigation//
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 50;
    // navigation//
    List<string> fn;
    protected void Page_Load(object sender, EventArgs e)
    {
        hdnReportFlag.Value = "76";
        CreateObjects();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            ViewState[Declarations.View] = Convert.ToBoolean(Access.isCustomised(int.Parse(hdnReportFlag.Value)));
        }
        setAccessValues();
        if (!IsPostBack)
        {
            LoadData();
            if ((Session[Declarations.CustomReportId]) != null)
            {
                ViewState[Declarations.CustomReportId] = Convert.ToString(Session[Declarations.CustomReportId]);
                Session[Declarations.CustomReportId] = null;
                LoadCustomFields();
            }
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objReport = FactoryReports.CustomReportDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    private void setAccessValues()
    {
        if (ViewState[Declarations.View] != null)
        {
            View = Convert.ToBoolean(ViewState[Declarations.View].ToString());
        }
    }

    public void LoadData()
    {
        DataTable dtFixField = new DataTable();
        DataTable dtrpt = new DataTable();
        DataTable dtrptD = new DataTable();
        DataTable dtrptK = new DataTable();
        DataTable dt = objReport.GetReports();

        IEnumerable<DataRow> rows = dt.AsEnumerable()
              .Where(r => r.Field<string>("value") == "Cr");

        dtFixField = rows.CopyToDataTable();

        IEnumerable<DataRow> rowsrpt = dt.AsEnumerable()
              .Where(r => r.Field<string>("value") == "rpt");

        dtrpt = rowsrpt.CopyToDataTable();

        IEnumerable<DataRow> rowsrptD = dt.AsEnumerable()
              .Where(r => r.Field<string>("value") == "rptD");

        dtrptD = rowsrptD.CopyToDataTable();

        IEnumerable<DataRow> rowsrptK = dt.AsEnumerable()
              .Where(r => r.Field<string>("value") == "rptK");

        dtrptK = rowsrptK.CopyToDataTable();

        chkFixFields.DataSource = dtFixField;
        chkFixFields.DataTextField = "Name";
        chkFixFields.DataValueField = "Value";
        chkFixFields.DataBind();

        if (dtrpt.Rows.Count > 0)
        {
            chkReqestField.DataSource = dtrpt;
            chkReqestField.DataTextField = "Name";
            chkReqestField.DataValueField = "Value";
            chkReqestField.DataBind();
        }

        if (dtrptD.Rows.Count > 0)
        {
            chkImportantField.DataSource = dtrptD;
            chkImportantField.DataTextField = "Name";
            chkImportantField.DataValueField = "Value";
            chkImportantField.DataBind();
        }

        if (dtrptK.Rows.Count > 0)
        {
            chkKeyObliField.DataSource = dtrptK;
            chkKeyObliField.DataTextField = "Name";
            chkKeyObliField.DataValueField = "Value";
            chkKeyObliField.DataBind();
        }

        DataTable dtCT = objReport.GetContractType();
        chkContractType.DataSource = dtCT;
        chkContractType.DataTextField = "ContractTypeName";
        chkContractType.DataValueField = "ContractTypeId";
        chkContractType.DataBind();

        //chkAllSelect.Checked = true;
        if (chkAllSelect.Checked)
        {
            for (int i = 0; i < chkFixFields.Items.Count; i++)
            {
                chkFixFields.Items[i].Selected = true;
            }

            for (int i = 0; i < chkContractType.Items.Count; i++)
            {
                chkContractType.Items[i].Selected = true;
            }
            //DefaultCustomLoad();
        }

    }

    protected void LoadCustomFields()
    {
        ICustomReportsNew objReport = FactoryReports.CustomReportDetails();
        objReport.PageNo = 1;
        objReport.RecordsPerPage = 100;
        objReport.CustomReportId = Convert.ToInt32(ViewState[Declarations.CustomReportId]);
        DataTable dt = objReport.GetCustomiesRecord();
        if (dt.Rows.Count > 0)
        {
            txtCustomReportName.Text = Convert.ToString(dt.Rows[0]["CustomReportName"]);
            txtDescription.InnerText = Convert.ToString(dt.Rows[0]["CustomDescription"]);

            if (Convert.ToString(dt.Rows[0]["ReportFixedFields"]) != string.Empty)
            {
                string str = Convert.ToString(dt.Rows[0]["ReportFixedFields"]);
                string[] values = str.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < values.Length; i++)
                {
                    for (int j = 0; j < chkFixFields.Items.Count; j++)
                    {
                        if (chkFixFields.Items[j].Text.Trim() == values[i].Trim())
                        {
                            chkFixFields.Items[j].Selected = true;
                        }
                    }
                }
            }

            if (Convert.ToString(dt.Rows[0]["RequestFormReportFields"]) != string.Empty)
            {
                string str = Convert.ToString(dt.Rows[0]["RequestFormReportFields"]);
                string[] values = str.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < values.Length; i++)
                {
                    for (int j = 0; j < chkReqestField.Items.Count; j++)
                    {
                        if (chkReqestField.Items[j].Text.Trim() == values[i].Trim())
                        {
                            chkReqestField.Items[j].Selected = true;
                        }
                    }
                }
            }

            if (Convert.ToString(dt.Rows[0]["ImportantDatesReportFields"]) != string.Empty)
            {
                string str = Convert.ToString(dt.Rows[0]["ImportantDatesReportFields"]);
                string[] values = str.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < values.Length; i++)
                {
                    for (int j = 0; j < chkImportantField.Items.Count; j++)
                    {
                        if (chkImportantField.Items[j].Text.Trim() == values[i].Trim())
                        {
                            chkImportantField.Items[j].Selected = true;
                        }
                    }
                }
            }

            if (Convert.ToString(dt.Rows[0]["KeyObligationReportFields"]) != string.Empty)
            {
                string str = Convert.ToString(dt.Rows[0]["KeyObligationReportFields"]);
                string[] values = str.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < values.Length; i++)
                {
                    for (int j = 0; j < chkKeyObliField.Items.Count; j++)
                    {
                        if (chkKeyObliField.Items[j].Text.Trim() == values[i].Trim())
                        {
                            chkKeyObliField.Items[j].Selected = true;
                        }
                    }
                }
            }

            if (Convert.ToString(dt.Rows[0]["ReportWhereClauseFields"]) != string.Empty)
            {
                string str = Convert.ToString(dt.Rows[0]["ReportWhereClauseFields"]);
                string[] values = str.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < values.Length; i++)
                {
                    for (int j = 0; j < chkContractType.Items.Count; j++)
                    {
                        if (chkContractType.Items[j].Value.Trim() == values[i].Trim())
                        {
                            chkContractType.Items[j].Selected = true;
                        }
                    }
                }
            }
        }

        DataTable dtSchedule = objReport.GetScheduleRecord();
        if (dtSchedule.Rows.Count > 0)
        {
            List<string> Chkdata = new List<string>();
            for (int i = 0; i < dtSchedule.Rows.Count; i++)
            {
                txtMailId.Text = Convert.ToString(dtSchedule.Rows[i]["MailId"]);
                TimeSpan ts = TimeSpan.Parse(Convert.ToString(dtSchedule.Rows[i]["ScheduleTime"]));
                if (Convert.ToString(dtSchedule.Rows[i]["ScheduleType"]) == "Daily")
                {
                    chkDailys.Checked = true;
                    txtDailyTime.Text = Convert.ToString(ts.ToString(@"hh\:mm"));
                    Chkdata.Add("Daily");
                }
                else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleType"]) == "Weekly")
                {
                    Chkdata.Add("Weekly");
                    chkWeeklys.Checked = true;
                    txtWeeklyTime.Text = Convert.ToString(ts.ToString(@"hh\:mm"));
                    ddlWeeklyDay.SelectedValue = Convert.ToString(dtSchedule.Rows[i]["ScheduleWeekday"]);
                }
                else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleType"]) == "Monthly")
                {
                    Chkdata.Add("Monthly");
                    chkMonthlys.Checked = true;
                    txtMonthlyTime.Text = Convert.ToString(ts.ToString(@"hh\:mm"));
                    ddlMontlyDay.SelectedValue = Convert.ToString(dtSchedule.Rows[i]["ScheduleDay"]);
                    if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "1")
                        txtMonthlyMonth.Text = "January";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "2")
                        txtMonthlyMonth.Text = "February";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "3")
                        txtMonthlyMonth.Text = "March";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "4")
                        txtMonthlyMonth.Text = "April";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "5")
                        txtMonthlyMonth.Text = "May";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "6")
                        txtMonthlyMonth.Text = "June";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "7")
                        txtMonthlyMonth.Text = "July";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "8")
                        txtMonthlyMonth.Text = "August";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "9")
                        txtMonthlyMonth.Text = "September";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "10")
                        txtMonthlyMonth.Text = "October";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "11")
                        txtMonthlyMonth.Text = "November";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "12")
                        txtMonthlyMonth.Text = "December";

                }
                else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleType"]) == "Yearly")
                {
                    Chkdata.Add("Yearly");
                    chkYearlys.Checked = true;
                    txtYearlyTime.Text = Convert.ToString(ts.ToString(@"hh\:mm"));
                    ddlYearlyDay.SelectedValue = Convert.ToString(dtSchedule.Rows[i]["ScheduleDay"]);
                    if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "1")
                        txtYearlyMonth.Text = "January";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "2")
                        txtYearlyMonth.Text = "February";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "3")
                        txtYearlyMonth.Text = "March";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "4")
                        txtYearlyMonth.Text = "April";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "5")
                        txtYearlyMonth.Text = "May";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "6")
                        txtYearlyMonth.Text = "June";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "7")
                        txtYearlyMonth.Text = "July";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "8")
                        txtYearlyMonth.Text = "August";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "9")
                        txtYearlyMonth.Text = "September";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "10")
                        txtYearlyMonth.Text = "October";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "11")
                        txtYearlyMonth.Text = "November";
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "12")
                        txtYearlyMonth.Text = "December";

                    txtYearlyYear.Text = Convert.ToString(dtSchedule.Rows[i]["ScheduleYear"]);
                }

            }
            ViewState["Chkdata"] = Chkdata;
        }

    }

    protected void chkAllSelect_CheckedChanged(object sender, EventArgs e)
    {
        if (chkAllSelect.Checked)
        {
            for (int i = 0; i < chkFixFields.Items.Count; i++)
            {
                chkFixFields.Items[i].Selected = true;
            }
            for (int i = 0; i < chkReqestField.Items.Count; i++)
            {
                chkReqestField.Items[i].Selected = true;
            }
            for (int i = 0; i < chkImportantField.Items.Count; i++)
            {
                chkImportantField.Items[i].Selected = true;
            }
            for (int i = 0; i < chkKeyObliField.Items.Count; i++)
            {
                chkKeyObliField.Items[i].Selected = true;
            }
            for (int i = 0; i < chkContractType.Items.Count; i++)
            {
                chkContractType.Items[i].Selected = true;
            }
        }
        else
        {
            for (int i = 0; i < chkFixFields.Items.Count; i++)
            {
                chkFixFields.Items[i].Selected = false;
            }
            for (int i = 0; i < chkReqestField.Items.Count; i++)
            {
                chkReqestField.Items[i].Selected = false;
            }
            for (int i = 0; i < chkImportantField.Items.Count; i++)
            {
                chkImportantField.Items[i].Selected = false;
            }
            for (int i = 0; i < chkKeyObliField.Items.Count; i++)
            {
                chkKeyObliField.Items[i].Selected = false;
            }
            for (int i = 0; i < chkContractType.Items.Count; i++)
            {
                chkContractType.Items[i].Selected = false;
            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        lblmessage.Text = "";
        if (ViewState[Declarations.CustomReportId] != null)
        {
            if (txtCustomReportName.Text.Trim() != string.Empty)
            {
                objReport.CustomReportName = txtCustomReportName.Text.Trim();
                StringBuilder sbFixedField = new StringBuilder();
                StringBuilder sbrpt = new StringBuilder();
                StringBuilder sbrptD = new StringBuilder();
                StringBuilder sbrptK = new StringBuilder();
                foreach (ListItem li in chkFixFields.Items)
                {
                    if (li.Selected)
                    {
                        if (Convert.ToString(li.Value) == "Cr")
                        {
                            sbFixedField.Append(Convert.ToString(li.Text) + ",");
                        }
                    }
                }

                foreach (ListItem li in chkReqestField.Items)
                {
                    if (li.Selected)
                    {
                        if (Convert.ToString(li.Value) == "rpt")
                        {
                            sbrpt.Append(Convert.ToString(li.Text) + ",");
                        }
                    }
                }

                foreach (ListItem li in chkImportantField.Items)
                {
                    if (li.Selected)
                    {
                        if (Convert.ToString(li.Value) == "rptD")
                        {
                            sbrptD.Append(Convert.ToString(li.Text) + ",");
                        }
                    }
                }

                foreach (ListItem li in chkKeyObliField.Items)
                {
                    if (li.Selected)
                    {
                        if (Convert.ToString(li.Value) == "rptK")
                        {
                            sbrptK.Append(Convert.ToString(li.Text) + ",");
                        }
                    }
                }

                StringBuilder sbchkType = new StringBuilder();
                foreach (ListItem li in chkContractType.Items)
                {
                    if (li.Selected)
                    {
                        sbchkType.Append(Convert.ToString(li.Value) + ",");
                    }
                }
                if (sbFixedField.ToString().TrimEnd(',') == string.Empty)
                {
                    lblmessage.Text = "Please select field.";
                    return;
                }

                objReport.ReportFixedFields = sbFixedField.ToString().TrimEnd(',');
                objReport.RequestFormReportFields = sbrpt.ToString().TrimEnd(',');
                objReport.ImportantDatesReportFields = sbrptD.ToString().TrimEnd(',');
                objReport.KeyObligationReportFields = sbrptK.ToString().TrimEnd(',');
                objReport.ReportWhereClauseFields = sbchkType.ToString().TrimEnd(',');
                objReport.CustomReportId = Convert.ToInt32(ViewState[Declarations.CustomReportId]);

                objReport.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
                objReport.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
                objReport.CustomDescription = txtDescription.InnerText.Trim();

                string status = objReport.UpdateRecord();

                objReport.CustomReportId = Convert.ToInt32(ViewState[Declarations.CustomReportId]);
                objReport.MailId = txtMailId.Text.Trim();

                List<string> mylist = (List<string>)ViewState["Chkdata"];

                if (chkDailys.Checked == true)
                {
                    objReport.Flag = 4;
                    objReport.ScheduleTime = txtDailyTime.Text.Trim();
                    objReport.ScheduleType = "Daily";
                    status = objReport.InsertRecord();
                    if (mylist != null)
                    {
                        mylist.Remove("Daily");
                    }
                }
                if (chkWeeklys.Checked == true)
                {
                    objReport.Flag = 4;
                    objReport.ScheduleTime = txtWeeklyTime.Text.Trim();
                    objReport.ScheduleWeek = ddlWeeklyDay.SelectedValue;
                    objReport.ScheduleType = "Weekly";
                    status = objReport.InsertRecord();
                    if (mylist != null)
                    {
                        mylist.Remove("Weekly");
                    }
                }
                if (chkMonthlys.Checked == true)
                {
                    objReport.ScheduleWeek = "";
                    objReport.Flag = 4;
                    objReport.ScheduleTime = txtMonthlyTime.Text.Trim();
                    if (txtMonthlyMonth.Text.Trim() == "January")
                        objReport.ScheduleMonth = "1";
                    else if (txtMonthlyMonth.Text.Trim() == "February")
                        objReport.ScheduleMonth = "2";
                    else if (txtMonthlyMonth.Text.Trim() == "March")
                        objReport.ScheduleMonth = "3";
                    else if (txtMonthlyMonth.Text.Trim() == "April")
                        objReport.ScheduleMonth = "4";
                    else if (txtMonthlyMonth.Text.Trim() == "May")
                        objReport.ScheduleMonth = "5";
                    else if (txtMonthlyMonth.Text.Trim() == "June")
                        objReport.ScheduleMonth = "6";
                    else if (txtMonthlyMonth.Text.Trim() == "July")
                        objReport.ScheduleMonth = "7";
                    else if (txtMonthlyMonth.Text.Trim() == "August")
                        objReport.ScheduleMonth = "8";
                    else if (txtMonthlyMonth.Text.Trim() == "September")
                        objReport.ScheduleMonth = "9";
                    else if (txtMonthlyMonth.Text.Trim() == "October")
                        objReport.ScheduleMonth = "10";
                    else if (txtMonthlyMonth.Text.Trim() == "November")
                        objReport.ScheduleMonth = "11";
                    else if (txtMonthlyMonth.Text.Trim() == "December")
                        objReport.ScheduleMonth = "12";

                    objReport.ScheduleDay = ddlMontlyDay.SelectedValue;
                    objReport.ScheduleType = "Monthly";
                    status = objReport.InsertRecord();
                    if (mylist != null)
                    {
                        mylist.Remove("Monthly");
                    }
                }

                if (chkYearlys.Checked == true)
                {
                    objReport.ScheduleWeek = "";
                    objReport.ScheduleDay = ddlYearlyDay.SelectedValue;
                    objReport.Flag = 4;
                    objReport.ScheduleTime = txtYearlyTime.Text.Trim();
                    if (txtYearlyMonth.Text.Trim() == "January")
                        objReport.ScheduleMonth = "1";
                    else if (txtYearlyMonth.Text.Trim() == "February")
                        objReport.ScheduleMonth = "2";
                    else if (txtYearlyMonth.Text.Trim() == "March")
                        objReport.ScheduleMonth = "3";
                    else if (txtYearlyMonth.Text.Trim() == "April")
                        objReport.ScheduleMonth = "4";
                    else if (txtYearlyMonth.Text.Trim() == "May")
                        objReport.ScheduleMonth = "5";
                    else if (txtYearlyMonth.Text.Trim() == "June")
                        objReport.ScheduleMonth = "6";
                    else if (txtYearlyMonth.Text.Trim() == "July")
                        objReport.ScheduleMonth = "7";
                    else if (txtYearlyMonth.Text.Trim() == "August")
                        objReport.ScheduleMonth = "8";
                    else if (txtYearlyMonth.Text.Trim() == "September")
                        objReport.ScheduleMonth = "9";
                    else if (txtYearlyMonth.Text.Trim() == "October")
                        objReport.ScheduleMonth = "10";
                    else if (txtYearlyMonth.Text.Trim() == "November")
                        objReport.ScheduleMonth = "11";
                    else if (txtYearlyMonth.Text.Trim() == "December")
                        objReport.ScheduleMonth = "12";

                    objReport.ScheduleYear = txtYearlyYear.Text.Trim();
                    objReport.ScheduleType = "Yearly";
                    status = objReport.InsertRecord();
                    if (mylist != null)
                    {
                        mylist.Remove("Yearly");
                    }
                }

                if (mylist != null)
                {
                    if (mylist.Count > 0)
                    {
                        for (int j = 0; j < mylist.Count; j++)
                        {
                            objReport.Flag = 6;
                            objReport.ScheduleType = Convert.ToString(mylist[j]);
                            status = objReport.InsertRecord();
                        }
                    }
                }

                if (status != "0")
                {
                    txtCustomReportName.Text = "";
                    txtDescription.InnerText = "";
                    Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
                }

                Page.Message("1", hdnPrimeId.Value);
                ViewState[Declarations.CustomReportId] = null;
                chkAllSelect.Checked = false;
                Server.Transfer("CustomReports.aspx");
            }
            for (int i = 0; i < chkFixFields.Items.Count; i++)
            {
                chkFixFields.Items[i].Selected = false;
            }
            for (int i = 0; i < chkReqestField.Items.Count; i++)
            {
                chkReqestField.Items[i].Selected = false;
            }
            for (int i = 0; i < chkImportantField.Items.Count; i++)
            {
                chkImportantField.Items[i].Selected = false;
            }
            for (int i = 0; i < chkKeyObliField.Items.Count; i++)
            {
                chkKeyObliField.Items[i].Selected = false;
            }
            for (int i = 0; i < chkContractType.Items.Count; i++)
            {
                chkContractType.Items[i].Selected = false;
            }
        }
        else
        {
            lblmessage.Text = "Please select report.";
            return;
        }

    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Server.Transfer("CustomReports.aspx");
    }

}