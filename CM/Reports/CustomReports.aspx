﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="CustomReports.aspx.cs" Inherits="CustomReports" %>

<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/reportrightactions.ascx" TagName="rptExport" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridNavigation.js" type="text/javascript"></script>
    <script src="../JQueryValidations/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    <script src="../CommonScripts/datePickerReports.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../scripts/jquery-ui-1.8.16.custom.css" />
    <script type="text/javascript">
        $("#reportLink").addClass("menulink");
        $("#reportTab").addClass("selectedtab");
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#lnkPredefineSheduleReport').css('display', 'none');
            $('#ddlReport').removeClass('required');
        });
    </script>
    <style type="text/css">
        a.btnDelete
        {
            background-image: url(../images/icon-del.jpg);
            background-position: 0% 50%;
            background-repeat: no-repeat;
            padding-left: 20px;
            padding-top: 2px;
            padding-bottom: 3px;
            color: #289aa1;
            font-size: 14px;
            margin-top: 5px;
            font-weight: bold;
        }
        a.btnEdit
        {
            background-image: url(../Images/edit.png);
            background-position: 0% 50%;
            background-repeat: no-repeat;
            padding-left: 20px;
            padding-top: 2px;
            padding-bottom: 3px;
            color: #289aa1;
            font-size: 14px;
            margin-top: 5px;
            font-weight: bold;
        }
        a.btnSchedule
        {
            background-image: url(../images/calendar.png);
            background-position: 0% 50%;
            background-repeat: no-repeat;
            padding-left: 20px;
            padding-top: 2px;
            padding-bottom: 3px;
            color: #289aa1;
            font-size: 14px;
            margin-top: 5px;
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnReportFlag" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnColumnName" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnColumnOrder" runat="server" clientidmode="Static" type="hidden" />
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <input type="hidden" clientidmode="Static" id="hdnisAdd" runat="Server" />
    <input type="hidden" clientidmode="Static" id="hdnIsUpdate" runat="Server" />
    <input type="hidden" clientidmode="Static" id="hdnIsDelete" runat="Server" />
    <uc1:rptExport ID="rptExport" runat="server" />
    <h2>
        My Reports
    </h2>
    <h2 style="font-size: medium">
        Custom Report</h2>
    <div style="margin: 0; padding: 0; display: inline">
        <p class="buttons hide-when-print">
            <asp:LinkButton ID="btnAddRecord" CssClass="icon icon-add" runat="server" OnClientClick="resetPrimeID();"
                OnClick="btnAddRecord_Click">Add new Custom Report</asp:LinkButton>
        </p>
        <div class="autoscroll">
            <table width="100%">
                <asp:Repeater ID="rptCustom" runat="server" OnItemCommand="rptCustom_ItemCommand">
                    <HeaderTemplate>
                        <table id="masterDataTable" class="reportTable list issues" width="100%">
                            <thead>
                                <tr>
                                    <th width="20%">
                                        <asp:LinkButton ID="btnCustomerName" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Report Name</asp:LinkButton>
                                    </th>
                                    <th width="60%">
                                        <asp:LinkButton ID="lnkDescription" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Description</asp:LinkButton>
                                    </th>
                                    <th width="20%" style="display: none;">
                                        <asp:LinkButton ID="btnRequestorName" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Fixed Field</asp:LinkButton>
                                    </th>
                                    <th width="15%" style="display: none;">
                                        <asp:LinkButton ID="btnContractID" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Request Form Field</asp:LinkButton>
                                    </th>
                                    <th width="15%" style="display: none;">
                                        <asp:LinkButton ID="btnEffectiveDate" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Important Form Field</asp:LinkButton>
                                    </th>
                                    <th width="15%" style="display: none;">
                                        <asp:LinkButton ID="btnExpiryDate" OnClick="btnSort_Click" CssClass="sort desc" runat="server">key Obligation Field</asp:LinkButton>
                                    </th>
                                    <th width="20%" style="display: none;">
                                        <asp:LinkButton ID="LinkButton42" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Contract Type</asp:LinkButton>
                                    </th>
                                    <th width="10%" style="display: none;">
                                    </th>
                                    <th width="10%">
                                    </th>
                                    <th width="10%">
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:HiddenField ID="hdnCustomReportId" runat="server" Value='<%#Eval("CustomReportId")%>'
                            ClientIDMode="Static" Visible="false" />
                        <asp:HiddenField ID="hdnAll_Field" runat="server" Value='<%#Eval("All_Field")%>'
                            ClientIDMode="Static" Visible="false" />
                        <tr>
                            <td>
                                <asp:Label ID="lblReportName" runat="server" Text='<%#Eval("CustomReportName")%>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblDescription" runat="server" Text='<%#Eval("CustomDescription")%>'></asp:Label>
                            </td>
                            <td style="display: none;">
                                <asp:Label ID="lblFixedField" runat="server" Text='<%#Eval("ReportFixedFields")%>'></asp:Label>
                            </td>
                            <td style="display: none;">
                                <asp:Label ID="lblRequestForm" runat="server" Text='<%#Eval("RequestFormReportFields")%>'></asp:Label>
                            </td>
                            <td style="display: none;">
                                <asp:Label ID="lblImportantDate" runat="server" Text='<%#Eval("ImportantDatesReportFields")%>'></asp:Label>
                            </td>
                            <td style="display: none;">
                                <asp:Label ID="lblKeyObligation" runat="server" Text='<%#Eval("KeyObligationReportFields")%>'></asp:Label>
                            </td>
                            <td style="display: none;">
                                <asp:Label ID="lblContractType" runat="server" Text='<%#Eval("Contract_Type")%>'></asp:Label>
                            </td>
                            <td style="display: none;">
                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" ToolTip="Edit" CssClass="btnEdit"
                                    CommandName="Edit_Record"></asp:LinkButton>
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkSchedule" runat="server" Text="" CssClass="btnEdit" ToolTip=""
                                    CommandName="Schedule_Record"></asp:LinkButton>
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkDelete" runat="server" Text="" CssClass="btnDelete" ToolTip=""
                                    OnClientClick="return confirm('Are you sure you want to delete?');" CommandName="Delete_Record"></asp:LinkButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
            <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
        </div>
    </div>
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <asp:HiddenField ID="hdnContractTypeID" runat="server" Value="0" ClientIDMode="Static" />
    <script type="text/javascript">
        $(document).ready(function () {
            if ($('#hdnisAdd').val() == "N") {
                $('a.icon-add').attr('onclick', 'return false').addClass('areadOnly');
                $('a.icon-add').css('display', 'none');
            }
            if ($('#hdnIsUpdate').val() == "N") {
                $('a.btnEdit').attr('onclick', 'return false').addClass('areadOnly');
                $('a.btnEdit').css('display', 'none');
            }
            if ($('#hdnIsDelete').val() == "N") {
                $('a.btnDelete').attr('onclick', 'return false').addClass('areadOnly');
                $('a.btnDelete').css('display', 'none');
            }
        });
    </script>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
        function GetContractTypeID() {
            var ID = $("#ddlContracttype").val();
            $("#hdnContractTypeID").val(ID);
        }
    </script>
</asp:Content>
