﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReportBLL;
using CommonBLL;
using System.Data;
using System.IO;
using ImportExcel;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;//This name space present in "itextsharp-dll-core"
using UserManagementBLL;



public partial class Reports_HistoricReports : System.Web.UI.Page
{
    // navigation//
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 50;
    // navigation//

    IHistoricReports objReport;
    public bool View;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IAccessLink obj = FactoryUser.GetAccessDetail();
            if (Session[Declarations.User] != null && Session[Declarations.User] != string.Empty)
            {
                obj.UserId = int.Parse(Session[Declarations.User].ToString());
            }
            obj.Flag = "Reports";
            ddlReport.extDataBind(obj.SelectData(), "");

        }

        ddlReport_SelectedIndexChanged(this.ddlReport, EventArgs.Empty);

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        hdnReportFlag.Value = "69";
        CreateObjects();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            ViewState[Declarations.View] = Convert.ToBoolean(Access.isCustomised(int.Parse(hdnReportFlag.Value)));
        }
        SetMonthlink();
        setAccessValues();
        if (!IsPostBack || hdnSearch.Value.Trim() != string.Empty)
        {
            //Bind Contract Type
            MasterBLL.IContractType objType = MasterBLL.FactoryMaster.GetContractTypeDetail();
            ddlContracttype.extDataBind(objType.SelectData());
            //End
            Session[Declarations.SortControl] = null;
            ReadData(1, RecordsPerPage);
            Session["ReportAccessId"] = "69";

        }
        else
        {
            SetNavigationButtonParameters();

        }

    }

    public void SetMonthlink()
    {
        try
        {
            switch (hdnMonth.Value)
            {
                case "0":
                    btnSearch.Style.Add("text-decoration", "none");
                    btnThreeMonth.Style.Add("text-decoration", "none");
                    btnSixMonth.Style.Add("text-decoration", "none");
                    btnTweleMonth.Style.Add("text-decoration", "none");
                    btnAll.Style.Add("text-decoration", "underline");

                    btnSearch.Style.Add("font-weight", "false");
                    btnThreeMonth.Style.Add("font-weight", "false");
                    btnSixMonth.Style.Add("font-weight", "false");
                    btnTweleMonth.Style.Add("font-weight", "false");
                    btnAll.Style.Add("font-weight", "Bold");
                    lblHeader.Text = "Request History";
                    break;
                case "1":
                    btnSearch.Style.Add("text-decoration", "underline");
                    btnThreeMonth.Style.Add("text-decoration", "none");
                    btnSixMonth.Style.Add("text-decoration", "none");
                    btnThreeMonth.Style.Add("text-decoration", "none");
                    btnAll.Style.Add("text-decoration", "none");

                    btnSearch.Style.Add("font-weight", "Bold");
                    btnThreeMonth.Style.Add("font-weight", "false");
                    btnSixMonth.Style.Add("font-weight", "false");
                    btnTweleMonth.Style.Add("font-weight", "false");
                    btnAll.Style.Add("font-weight", "false");
                    lblHeader.Text = "Request History-Last Month";
                    break;

                case "3":
                    btnSearch.Style.Add("text-decoration", "none");
                    btnThreeMonth.Style.Add("text-decoration", "underline");
                    btnSixMonth.Style.Add("text-decoration", "none");
                    btnTweleMonth.Style.Add("text-decoration", "none");
                    btnAll.Style.Add("text-decoration", "none");

                    btnSearch.Style.Add("font-weight", "false");
                    btnThreeMonth.Style.Add("font-weight", "Bold");
                    btnSixMonth.Style.Add("font-weight", "false");
                    btnTweleMonth.Style.Add("font-weight", "false");
                    btnAll.Style.Add("font-weight", "false");
                    lblHeader.Text = "Request History-Last 3 Months";
                    break;

                case "6":
                    btnSearch.Style.Add("text-decoration", "none");
                    btnThreeMonth.Style.Add("text-decoration", "none");
                    btnSixMonth.Style.Add("text-decoration", "underline");
                    btnTweleMonth.Style.Add("text-decoration", "none");
                    btnAll.Style.Add("text-decoration", "none");

                    btnSearch.Style.Add("font-weight", "false");
                    btnThreeMonth.Style.Add("font-weight", "false");
                    btnSixMonth.Style.Add("font-weight", "Bold");
                    btnTweleMonth.Style.Add("font-weight", "false");
                    btnAll.Style.Add("font-weight", "false");
                    lblHeader.Text = "Request History-Last 6 Months";
                    break;

                case "12":
                    btnSearch.Style.Add("text-decoration", "none");
                    btnThreeMonth.Style.Add("text-decoration", "none");
                    btnSixMonth.Style.Add("text-decoration", "none");
                    btnTweleMonth.Style.Add("text-decoration", "underline");
                    btnAll.Style.Add("text-decoration", "none");

                    btnSearch.Style.Add("font-weight", "false");
                    btnThreeMonth.Style.Add("font-weight", "false");
                    btnSixMonth.Style.Add("font-weight", "false");
                    btnTweleMonth.Style.Add("font-weight", "Bold");
                    btnAll.Style.Add("font-weightd", "false");
                    lblHeader.Text = "Request History-Last 12 Months";
                    break;
            }
        }
        catch { }

    }
    protected void btnSort_Click(object sender, EventArgs e)
    {
        hdnSearch.Value = "";
        LinkButton clickBtn = (LinkButton)sender;
        Session.Add(Declarations.SortControl, clickBtn);
        ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
    }

    // navigation//

    #region Navigation
    void SetNavigationButtonParameters()
    {
        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }


    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);

        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        SetMonthlink();
    }
    #endregion

    // navigation//

    void ReadData(int pageNo, int recordsPerPage)
    {
        if (View == true)
        {
            Page.TraceWrite("ReadData starts.");
            try
            {
                if (hdnMonth.Value != "0")
                {
                    // string FromDate = DateTime.Now.ToString();
                    //  string TomDate = DateTime.Now.AddMonths(-(Convert.ToInt32(hdnMonth.Value))).ToString();
                    // objReport.FromDate = Convert.ToDateTime(FromDate);
                    // objReport.ToDate = Convert.ToDateTime(TomDate);
                    objReport.Flag = Convert.ToInt32(hdnMonth.Value);
                }
                if (txtFromDate.Value.Trim() != "")
                {
                    objReport.FromDate = Convert.ToDateTime(txtFromDate.Value);
                }
                if (txtToDate.Value.Trim() != "")
                {
                    objReport.ToDate = Convert.ToDateTime(txtToDate.Value);
                }

                if (hdnContractTypeID.Value != "0")
                {
                    objReport.ContractTypeID = Convert.ToInt32(hdnContractTypeID.Value);
                    ddlContracttype.Value = hdnContractTypeID.Value;
                }
                objReport.PageNo = pageNo;
                objReport.RecordsPerPage = recordsPerPage;
                objReport.Search = txtSearch.Value.Trim();
                LinkButton btnSort = null;
                if (Session[Declarations.SortControl] != null && Session[Declarations.SortControl] != string.Empty)
                {
                    btnSort = (LinkButton)Session[Declarations.SortControl];
                    objReport.Direction = btnSort.CssClass.ToLower() == "sort desc" ? 1 : 0;
                    objReport.SortColumn = btnSort.Text.Trim();
                }
                objReport.UserID = int.Parse(Session[Declarations.User].ToString());
                rptReports.DataSource = objReport.GetReport();
                rptReports.DataBind();
                if (btnSort != null)
                {
                    rptReports.ClassChange(btnSort);
                }
                Page.TraceWrite("ReadData ends.");
                ViewState[Declarations.TotalRecord] = objReport.TotalRecords;
                PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
                PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
                PaginationButtons1.RecordsPerPage = RecordsPerPage;
            }
            catch (Exception ex)
            {
                Page.TraceWarn("ReadData fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objReport = FactoryReports.HistoricReports();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }
    private void setAccessValues()
    {
        if (ViewState[Declarations.View] != null)
        {
            View = Convert.ToBoolean(ViewState[Declarations.View].ToString());
        }
    }
    public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
    {

    }

    protected void rdoonemonth_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void rdothreemonth_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void rdosixmonth_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void rdotwelemonth_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void chkOneMonth_CheckedChanged(object sender, EventArgs e)
    {

    }

    #region Export Functionality

    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        if (hdnGlobalReportFlag.Value != "")
        {
            if (hdnMonth.Value != "0")
            {
                objReport.Flag = Convert.ToInt32(hdnMonth.Value);
            }
            DataTable dt = objReport.GetFullReport();
            if (dt.Rows.Count > 0)
            {
                string Name =lblHeader.Text;
                string fileName = Name.Replace(" ", "-").extTimeStamp() + ".xls";
                gvDetails.Caption = "<div align=center><font size=4 ><b>" + Name + "</b></font></div>";
                gvDetails.DataSource = dt;
                gvDetails.DataBind();
                ExportToExcell(fileName);
            }
        }
    }
    protected void btnExportToPDF_Click(object sender, EventArgs e)
    {

        if (hdnMonth.Value != "0")
        {
            objReport.Flag = Convert.ToInt32(hdnMonth.Value);
        }
        DataTable dt = objReport.GetFullReport();
        if (dt.Rows.Count > 0)
        {
            string Name = lblHeader.Text;
            string fileName = Name.Replace(" ", "-").extTimeStamp() + ".pdf";
            gvDetails.Caption = "<div align=center><font size=4 ><b>" + fileName + "</b></font></div>";
            gvDetails.DataSource = dt;
            gvDetails.DataBind();
            ExportToPdf(fileName, Name);
        }

    }

    void ExportToPdf(string pdfFileName, string reportTitle)
    {
        Response.ContentType = "application/pdf";
        string strAttachmnet = "attachment;filename=" + pdfFileName;
        Response.AddHeader("content-disposition", strAttachmnet);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter s_w = new StringWriter();
        HtmlTextWriter h_w = new HtmlTextWriter(s_w);
        gvDetails.AllowPaging = false;

        gvDetails.RenderControl(h_w);

        string strOldString = "scope=\"col\"";
        string strReplace = strOldString + " style=\"font-weight:bold; font-size:large\" ";
        string strOut = s_w.ToString().Replace(strOldString, strReplace);
        strOut = "<div align=center><font size=4 ><b>" + reportTitle + "</b></font></div><br>" + strOut;

        StringReader sr = new StringReader(strOut);
        Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        pdfDoc.Open();
        htmlparser.Parse(sr);
        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
    }

    void ExportToExcell(string excelFileName)
    {
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", excelFileName));
        Response.ContentType = "application/ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        gvDetails.AllowPaging = false;
        //Change the Header Row back to white color
        gvDetails.HeaderRow.Style.Add("background-color", "#E1E1E1");
        //Applying stlye to gridview header cells
        for (int i = 0; i < gvDetails.HeaderRow.Cells.Count; i++)
        {
            gvDetails.HeaderRow.Cells[i].Style.Add("background-color", "#E1E1E1");
            gvDetails.HeaderRow.Cells[i].Style.Add("font-weight", "bold");

        }
        int j = 1;
        //Set alternate row color
        foreach (GridViewRow gvrow in gvDetails.Rows)
        {
            gvrow.BackColor = System.Drawing.Color.White;
            if (j <= gvDetails.Rows.Count)
            {
                if (j % 2 != 0)
                {
                    for (int k = 0; k < gvrow.Cells.Count; k++)
                    {
                        gvrow.Cells[k].Style.Add("background-color", "#EFF3FB");
                    }
                }
            }
            j++;
        }
        gvDetails.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    protected void ddlReport_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (hdnGlobalReportLink.Value != "")
        {
            Server.Transfer(hdnGlobalReportLink.Value);
        }
    }

    #endregion

    protected void lnkPredefineSheduleReport_Click(object sender, EventArgs e)
    {
        Server.Transfer("PredefinedScheduleReport.aspx");
    }


}










