﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="ScheduleCustomReports.aspx.cs" Inherits="Reports_ScheduleCustomReports" %>

<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/reportrightactions.ascx" TagName="rptExport" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridNavigation.js" type="text/javascript"></script>
    <script src="../JQueryValidations/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    <script src="../CommonScripts/datePickerReports.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../scripts/jquery-ui-1.8.16.custom.css" />
    <script src="../CommonScripts/Timepicker.js" type="text/javascript"></script>
    <link href="../CalendarCSS/jquery.timepicker.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $("#reportLink").addClass("menulink");
        $("#reportTab").addClass("selectedtab");
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#ddlReport').removeClass('required');
            $('#btnExportToExcel').css('display', 'none');
            $('#btnExportToPDF').css('display', 'none');

            $('.ckAllCheck').click(function () {
                if ($('#chkAllSelect').prop('checked') == true) {
                    $('.chkChoice').find(':checkbox').each(function () {
                        $(this).prop('checked', true);
                    });
                }
                else {
                    $('.chkChoice').find(':checkbox').each(function () {
                        $(this).prop('checked', false);
                    });
                }
            });  // end of ckAllcheck

            try {
                $("#txtTime").timepicker({
                    timeFormat: 'HH:mm',
                    interval: 15 // 15 minutes
                });

                $('#txtMonth').datepicker({
                    changeMonth: true,
                    showButtonPanel: true,
                    dateFormat: 'MM',
                    onClose: function (dateText, inst) {
                        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                    }
                });
            }
            catch (e) {
                alert(e);
            }

        });
    </script>
    <style type="text/css">
        .chkChoice
        {
            margin-top: 5px;
        }
        .chkChoice input
        {
            margin-left: 15px;
        }
        .chkChoice td
        {
            padding: 2px 0px 2px 0px;
        }
        .chkChoice label
        {
            position: relative;
            margin-left: 5px;
            margin-top: 15px;
        }
        .ShecduleCheck
        {
            margin-left: 15px;
        }
        
        fieldset.collapsible legend
        {
            padding-left: 2px;
            background: none;
            cursor: pointer;
        }
        .ui-datepicker-calendar
        {
            display: none;
        }
        .ui-datepicker-year
        {
            display: none;
        }
        .tabular p
        {
            padding-left: 65px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <link href="Styles/chosen.css" rel="stylesheet" type="text/css" />
    <script src="../scripts/chosen.jquery.js" type="text/javascript"></script>
    <script src="../CommonScripts/dropdownlist.js" type="text/javascript"></script>
    <link href="Styles/application.css" media="all" rel="stylesheet" type="text/css" />
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnReportFlag" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnColumnName" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnColumnOrder" runat="server" clientidmode="Static" type="hidden" />
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <uc1:rptExport ID="rptExport" runat="server" />
    <h2>
        My Reports
    </h2>
    <h2 style="font-size: medium">
        Custom Report</h2>
    <div style="margin: 0; padding: 0; display: inline">
        <div id="reportFilter">
            <div id="query_form_content" class="hide-when-print">
                <fieldset id="filters" class="collapsible">
                    <div style="">
                        <div class="box tabular">
                            <p>
                                <label for="time_entry_issue_id">
                                    <span class="required">*</span>Custom Report Name :</label>
                                <asp:TextBox ID="txtCustomReportName" runat="server" ClientIDMode="Static" placeholder="Custom Report Name"
                                    class="required"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="fteclientname" runat="server" InvalidChars="/\:*?<>&quot;|$^%_=,.~`{}"
                                    FilterMode="InvalidChars" TargetControlID="txtCustomReportName" />
                                <em></em>
                            </p>
                            <p>
                                <label for="time_entry_issue_id">
                                    Description :</label>
                                <textarea id="txtDescription" placeholder="Custom description" runat="server" maxlength="100"
                                    clientidmode="static" type="text" autocomplete="false" style="width: 220px; height: 100px"
                                    rows="10" />
                                <em></em>
                            </p>
                        </div>
                        <table style="width: 100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="UpAll" runat="server">
                                            <ContentTemplate>
                                                <%--<asp:CheckBox ID="chkAllSelect" runat="server" AutoPostBack="true" Text="&nbsp;Select All"
                                                    CssClass="ckAllCheck" OnCheckedChanged="chkAllSelect_CheckedChanged" />--%>
                                                <asp:CheckBox ID="chkAllSelect" runat="server" Text="&nbsp;Select All" CssClass="ckAllCheck"
                                                    Style="margin-bottom: 5px;" ClientIDMode="Static" />
                                                <br />
                                                <br />
                                                <fieldset>
                                                    <legend style="color: Black;">System Fields</legend>
                                                    <asp:CheckBoxList ID="chkFixFields" runat="server" RepeatColumns="4" Width="1000px"
                                                        CssClass="chkChoice">
                                                    </asp:CheckBoxList>
                                                    <br />
                                                </fieldset>
                                                <br />
                                                <fieldset>
                                                    <legend style="color: Black;">Request Form Custom Fields</legend>
                                                    <asp:CheckBoxList ID="chkReqestField" runat="server" RepeatColumns="4" Width="1000px"
                                                        CssClass="chkChoice">
                                                    </asp:CheckBoxList>
                                                    <br />
                                                </fieldset>
                                                <br />
                                                <fieldset>
                                                    <legend style="color: Black;">Important Date Custom Fields</legend>
                                                    <asp:CheckBoxList ID="chkImportantField" runat="server" RepeatColumns="4" Width="1000px"
                                                        CssClass="chkChoice">
                                                    </asp:CheckBoxList>
                                                    <br />
                                                </fieldset>
                                                <br />
                                                <fieldset>
                                                    <legend style="color: Black;">Key Obligation Custom Fields</legend>
                                                    <asp:CheckBoxList ID="chkKeyObliField" runat="server" RepeatColumns="4" Width="1000px"
                                                        CssClass="chkChoice">
                                                    </asp:CheckBoxList>
                                                    <br />
                                                </fieldset>
                                                <br />
                                                <fieldset>
                                                    <legend style="color: Black;">Contract Type</legend>
                                                    <asp:CheckBoxList ID="chkContractType" runat="server" RepeatColumns="2" Width="800px"
                                                        CssClass="chkChoice">
                                                    </asp:CheckBoxList>
                                                    <br />
                                                </fieldset>
                                                <br />
                                                <div style="display:none;">
                                                <fieldset>
                                                    <legend style="color: Black;">Schedule </legend>
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="15%">
                                                                <asp:CheckBox ID="chkDaily" runat="server" Text="&nbsp;Daily" CssClass="ShecduleCheck"
                                                                    Style="margin-bottom: 5px;" ClientIDMode="Static" />
                                                            </td>
                                                            <td width="15%">
                                                                <asp:CheckBox ID="chkWeekly" runat="server" Text="&nbsp;Weekly" CssClass="ShecduleCheck"
                                                                    Style="margin-bottom: 5px;" ClientIDMode="Static" />
                                                            </td>
                                                            <td width="15%">
                                                                <asp:CheckBox ID="chkMonthly" runat="server" Text="&nbsp;Monthly" CssClass="ShecduleCheck"
                                                                    Style="margin-bottom: 5px;" ClientIDMode="Static" />
                                                            </td>
                                                            <td width="15%">
                                                                <asp:CheckBox ID="chkYearly" runat="server" Text="&nbsp;Yearly" CssClass="ShecduleCheck"
                                                                    Style="margin-bottom: 5px;" ClientIDMode="Static" />
                                                            </td>
                                                            <td width="20%">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <div class="box tabular">
                                                        <p>
                                                            <label for="time_entry_issue_id">
                                                                <span class="required">*</span>Time :</label>
                                                            <asp:TextBox ID="txtTime" runat="server" ClientIDMode="Static" placeholder="Time" MaxLength="5"
                                                                Width="100px"></asp:TextBox>
                                                            <em></em>
                                                        </p>
                                                        <p>
                                                            <label for="time_entry_issue_id">
                                                                Day :</label>
                                                            <asp:DropDownList ID="ddlDate" runat="server" ClientIDMode="Static" CssClass="chzn-select"
                                                                Width="18%">
                                                                <asp:ListItem Value="0">--Select Day--</asp:ListItem>
                                                                <asp:ListItem Value="1">1</asp:ListItem>
                                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                                <asp:ListItem Value="3">3</asp:ListItem>
                                                                <asp:ListItem Value="4">4</asp:ListItem>
                                                                <asp:ListItem Value="5">5</asp:ListItem>
                                                                <asp:ListItem Value="6">6</asp:ListItem>
                                                                <asp:ListItem Value="7">7</asp:ListItem>
                                                                <asp:ListItem Value="8">8</asp:ListItem>
                                                                <asp:ListItem Value="9">9</asp:ListItem>
                                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                                <asp:ListItem Value="11">11</asp:ListItem>
                                                                <asp:ListItem Value="12">12</asp:ListItem>
                                                                <asp:ListItem Value="13">13</asp:ListItem>
                                                                <asp:ListItem Value="14">14</asp:ListItem>
                                                                <asp:ListItem Value="15">15</asp:ListItem>
                                                                <asp:ListItem Value="16">16</asp:ListItem>
                                                                <asp:ListItem Value="17">17</asp:ListItem>
                                                                <asp:ListItem Value="18">18</asp:ListItem>
                                                                <asp:ListItem Value="19">19</asp:ListItem>
                                                                <asp:ListItem Value="20">20</asp:ListItem>
                                                                <asp:ListItem Value="21">21</asp:ListItem>
                                                                <asp:ListItem Value="22">22</asp:ListItem>
                                                                <asp:ListItem Value="23">23</asp:ListItem>
                                                                <asp:ListItem Value="24">24</asp:ListItem>
                                                                <asp:ListItem Value="25">25</asp:ListItem>
                                                                <asp:ListItem Value="26">26</asp:ListItem>
                                                                <asp:ListItem Value="27">27</asp:ListItem>
                                                                <asp:ListItem Value="28">28</asp:ListItem>
                                                                <asp:ListItem Value="29">29</asp:ListItem>
                                                                <asp:ListItem Value="30">30</asp:ListItem>
                                                                <asp:ListItem Value="31">31</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <em></em>
                                                        </p>
                                                        <p>
                                                            <label for="time_entry_issue_id">
                                                                Week Day :</label>
                                                            <asp:DropDownList ID="ddlWeekday" runat="server" ClientIDMode="Static" CssClass="chzn-select"
                                                                Width="18%">
                                                                <asp:ListItem Value="0">--Select Week--</asp:ListItem>
                                                                <asp:ListItem Value="1">Sunday</asp:ListItem>
                                                                <asp:ListItem Value="2">Monday</asp:ListItem>
                                                                <asp:ListItem Value="3">Tuesday</asp:ListItem>
                                                                <asp:ListItem Value="4">Wednesday</asp:ListItem>
                                                                <asp:ListItem Value="5">Thursday</asp:ListItem>
                                                                <asp:ListItem Value="6">Friday</asp:ListItem>
                                                                <asp:ListItem Value="7">Saturday</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <em></em>
                                                        </p>
                                                        <p>
                                                            <label for="time_entry_issue_id">
                                                                Month :</label>
                                                            <asp:TextBox ID="txtMonth" runat="server" ClientIDMode="Static" placeholder="Month"
                                                                Width="100px"></asp:TextBox>
                                                            <em></em>
                                                        </p>
                                                        <p>
                                                            <label for="time_entry_issue_id">
                                                                Year :</label>
                                                            <asp:TextBox ID="txtYear" runat="server" ClientIDMode="Static" placeholder="Year"
                                                                MaxLength="4" onblur="yearValidation(this.value,event)" onkeypress="yearValidation(this.value,event)"
                                                                Width="100px"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="filTxt" runat="server" TargetControlID="txtYear"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </cc1:FilteredTextBoxExtender>
                                                            <em></em>
                                                        </p>
                                                    </div>
                                                    <br />
                                                </fieldset>
                                                </div>
                                                <br />
                                                <fieldset>
                                                    <legend style="color: Black;">Schedule </legend>
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="10%">
                                                                <asp:CheckBox ID="chkDailys" runat="server" Text="&nbsp;Daily" CssClass="ShecduleCheck"
                                                                    Style="margin-bottom: 5px;" ClientIDMode="Static" />
                                                            </td>
                                                            <td width="10%">
                                                                <asp:TextBox ID="txtDailyTime" runat="server" ClientIDMode="Static" placeholder="Time" MaxLength="5"
                                                                    Width="100px"></asp:TextBox>
                                                            </td>
                                                            <td width="10%">
                                                            </td>
                                                            <td width="10%">
                                                            </td>
                                                            <td width="15%">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="chkWeeklys" runat="server" Text="&nbsp;Weekly" CssClass="ShecduleCheck"
                                                                    Style="margin-bottom: 5px;" ClientIDMode="Static" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtWeeklyTime" runat="server" ClientIDMode="Static" placeholder="Time"
                                                                    Width="100px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlWeeklyDay" runat="server" ClientIDMode="Static" CssClass="chzn-select"
                                                                    Width="70%">
                                                                    <asp:ListItem Value="0">--Select Week--</asp:ListItem>
                                                                    <asp:ListItem Value="1">Sunday</asp:ListItem>
                                                                    <asp:ListItem Value="2">Monday</asp:ListItem>
                                                                    <asp:ListItem Value="3">Tuesday</asp:ListItem>
                                                                    <asp:ListItem Value="4">Wednesday</asp:ListItem>
                                                                    <asp:ListItem Value="5">Thursday</asp:ListItem>
                                                                    <asp:ListItem Value="6">Friday</asp:ListItem>
                                                                    <asp:ListItem Value="7">Saturday</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <em></em>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="chkMonthlys" runat="server" Text="&nbsp;Monthly" CssClass="ShecduleCheck"
                                                                    Style="margin-bottom: 5px;" ClientIDMode="Static" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtMonthlyTime" runat="server" ClientIDMode="Static" placeholder="Time" MaxLength="5"
                                                                    Width="100px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlMontlyDay" runat="server" ClientIDMode="Static" CssClass="chzn-select"
                                                                    Width="64%">
                                                                    <asp:ListItem Value="0">--Select Day--</asp:ListItem>
                                                                    <asp:ListItem Value="1">1</asp:ListItem>
                                                                    <asp:ListItem Value="2">2</asp:ListItem>
                                                                    <asp:ListItem Value="3">3</asp:ListItem>
                                                                    <asp:ListItem Value="4">4</asp:ListItem>
                                                                    <asp:ListItem Value="5">5</asp:ListItem>
                                                                    <asp:ListItem Value="6">6</asp:ListItem>
                                                                    <asp:ListItem Value="7">7</asp:ListItem>
                                                                    <asp:ListItem Value="8">8</asp:ListItem>
                                                                    <asp:ListItem Value="9">9</asp:ListItem>
                                                                    <asp:ListItem Value="10">10</asp:ListItem>
                                                                    <asp:ListItem Value="11">11</asp:ListItem>
                                                                    <asp:ListItem Value="12">12</asp:ListItem>
                                                                    <asp:ListItem Value="13">13</asp:ListItem>
                                                                    <asp:ListItem Value="14">14</asp:ListItem>
                                                                    <asp:ListItem Value="15">15</asp:ListItem>
                                                                    <asp:ListItem Value="16">16</asp:ListItem>
                                                                    <asp:ListItem Value="17">17</asp:ListItem>
                                                                    <asp:ListItem Value="18">18</asp:ListItem>
                                                                    <asp:ListItem Value="19">19</asp:ListItem>
                                                                    <asp:ListItem Value="20">20</asp:ListItem>
                                                                    <asp:ListItem Value="21">21</asp:ListItem>
                                                                    <asp:ListItem Value="22">22</asp:ListItem>
                                                                    <asp:ListItem Value="23">23</asp:ListItem>
                                                                    <asp:ListItem Value="24">24</asp:ListItem>
                                                                    <asp:ListItem Value="25">25</asp:ListItem>
                                                                    <asp:ListItem Value="26">26</asp:ListItem>
                                                                    <asp:ListItem Value="27">27</asp:ListItem>
                                                                    <asp:ListItem Value="28">28</asp:ListItem>
                                                                    <asp:ListItem Value="29">29</asp:ListItem>
                                                                    <asp:ListItem Value="30">30</asp:ListItem>
                                                                    <asp:ListItem Value="31">31</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <em></em>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtMonthlyMonth" runat="server" ClientIDMode="Static" placeholder="Month"
                                                                    Width="100px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="chkYearlys" runat="server" Text="&nbsp;Yearly" CssClass="ShecduleCheck"
                                                                    Style="margin-bottom: 5px;" ClientIDMode="Static" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtYearlyTime" runat="server" ClientIDMode="Static" placeholder="Time" MaxLength="5"
                                                                    Width="100px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlYearlyDay" runat="server" ClientIDMode="Static" CssClass="chzn-select"
                                                                    Width="64%">
                                                                    <asp:ListItem Value="0">--Select Day--</asp:ListItem>
                                                                    <asp:ListItem Value="1">1</asp:ListItem>
                                                                    <asp:ListItem Value="2">2</asp:ListItem>
                                                                    <asp:ListItem Value="3">3</asp:ListItem>
                                                                    <asp:ListItem Value="4">4</asp:ListItem>
                                                                    <asp:ListItem Value="5">5</asp:ListItem>
                                                                    <asp:ListItem Value="6">6</asp:ListItem>
                                                                    <asp:ListItem Value="7">7</asp:ListItem>
                                                                    <asp:ListItem Value="8">8</asp:ListItem>
                                                                    <asp:ListItem Value="9">9</asp:ListItem>
                                                                    <asp:ListItem Value="10">10</asp:ListItem>
                                                                    <asp:ListItem Value="11">11</asp:ListItem>
                                                                    <asp:ListItem Value="12">12</asp:ListItem>
                                                                    <asp:ListItem Value="13">13</asp:ListItem>
                                                                    <asp:ListItem Value="14">14</asp:ListItem>
                                                                    <asp:ListItem Value="15">15</asp:ListItem>
                                                                    <asp:ListItem Value="16">16</asp:ListItem>
                                                                    <asp:ListItem Value="17">17</asp:ListItem>
                                                                    <asp:ListItem Value="18">18</asp:ListItem>
                                                                    <asp:ListItem Value="19">19</asp:ListItem>
                                                                    <asp:ListItem Value="20">20</asp:ListItem>
                                                                    <asp:ListItem Value="21">21</asp:ListItem>
                                                                    <asp:ListItem Value="22">22</asp:ListItem>
                                                                    <asp:ListItem Value="23">23</asp:ListItem>
                                                                    <asp:ListItem Value="24">24</asp:ListItem>
                                                                    <asp:ListItem Value="25">25</asp:ListItem>
                                                                    <asp:ListItem Value="26">26</asp:ListItem>
                                                                    <asp:ListItem Value="27">27</asp:ListItem>
                                                                    <asp:ListItem Value="28">28</asp:ListItem>
                                                                    <asp:ListItem Value="29">29</asp:ListItem>
                                                                    <asp:ListItem Value="30">30</asp:ListItem>
                                                                    <asp:ListItem Value="31">31</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <em></em>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtYearlyMonth" runat="server" ClientIDMode="Static" placeholder="Month"
                                                                    Width="100px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtYearlyYear" runat="server" ClientIDMode="Static" placeholder="Year"
                                                                    MaxLength="4" onblur="yearValidation(this.value,event)" onkeypress="yearValidation(this.value,event)"
                                                                    Width="100px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label for="time_entry_issue_id" style="margin-left: 15px;">
                                                                    <%--<span class="required">*</span>--%> E-mail </label>
                                                            </td>
                                                            <td colspan="4">
                                                                <asp:TextBox ID="txtMailId" runat="server" class="EmailList" ClientIDMode="Static" TextMode="MultiLine"
                                                                    placeholder="Mail Id" Width="298px" Height="50px"></asp:TextBox>
                                                                <em></em>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                </fieldset>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </fieldset>
            </div>
            <p class="buttons hide-when-print" style="margin-left: 90px;">
                <asp:Label ID="lblmessage" runat="server" Text="" ClientIDMode="Static" ForeColor="Red"
                    Style="filter: alpha(opacity=50); opacity: 0.5;"></asp:Label>
                <br />
                <asp:Button runat="server" Text="Save" ID="btnsubmit" OnClientClick="return ValidateContractID()"
                    OnClick="btnSubmit_Click" class="btn_validate" />
                <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnClear_Click" />
                <div id="dvMesssage" class="flash error" style="display: none">
                </div>
            </p>
        </div>
    </div>
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <asp:HiddenField ID="hdnContractTypeID" runat="server" Value="0" ClientIDMode="Static" />
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            // re-bind your jQuery events here   divdate
        });

        $(document).ready(function () {
            if ($("#chkMonthly").is(":checked") == false && $("#chkYearly").is(":checked") == false) {
                $("#ddlDate").attr('disabled', 'disabled').trigger("liszt:updated");
                $("#txtMonth").attr('disabled', 'disabled').trigger("liszt:updated");
                $("#txtYear").attr('disabled', 'disabled').trigger("liszt:updated");
                $("#txtMonth").val('');
            } else {
                if ($("#chkMonthly").is(":checked") == false) {
                    $("#ddlDate").attr('disabled', 'disabled').trigger("liszt:updated");
                    $("#txtMonth").attr('disabled', 'disabled').trigger("liszt:updated");
                    $("#txtMonth").val('');
                }
                if ($("#chkYearly").is(":checked") == true) {
                    $("#ddlDate").removeAttr('disabled').trigger("liszt:updated");
                    $("#txtMonth").removeAttr('disabled').trigger("liszt:updated");
                }
            }
            if ($("#chkWeekly").is(":checked") == false) {
                $("#ddlWeekday").val('0').change();
                $("#ddlWeekday").attr('disabled', 'disabled').trigger("liszt:updated");
            }

            $("#chkWeekly").click(function () {
                if ($(this).is(":checked")) {
                    $("#ddlWeekday").removeAttr('disabled').trigger("liszt:updated");
                } else {
                    $("#ddlWeekday").val('0').change();
                    $("#ddlWeekday").attr('disabled', 'disabled').trigger("liszt:updated");
                }
            });

            $("#chkMonthly").click(function () {
                if ($(this).is(":checked")) {
                    $("#ddlDate").removeAttr('disabled').trigger("liszt:updated");
                    $("#txtMonth").removeAttr('disabled').trigger("liszt:updated");
                } else {
                    if ($("#chkYearly").is(":checked") == false) {
                        $("#ddlDate").val('0').change();
                        $("#txtMonth").val('');
                        $("#ddlDate").attr('disabled', 'disabled').trigger("liszt:updated");
                        $("#txtMonth").attr('disabled', 'disabled').trigger("liszt:updated");
                    }

                }
            });

            $("#chkYearly").click(function () {
                if ($(this).is(":checked")) {
                    $("#ddlDate").removeAttr('disabled').trigger("liszt:updated");
                    $("#txtMonth").removeAttr('disabled').trigger("liszt:updated");
                    $("#txtYear").removeAttr('disabled').trigger("liszt:updated");
                } else {
                    if ($("#chkMonthly").is(":checked") == true) {
                        $("#ddlDate").removeAttr('disabled').trigger("liszt:updated");
                        $("#txtMonth").removeAttr('disabled').trigger("liszt:updated");
                    }
                    $("#txtYear").attr('disabled', 'disabled').trigger("liszt:updated");
                    $("#txtYear").val('');
                }
            });

        });

    </script>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
        function GetContractTypeID() {
            var ID = $("#ddlContracttype").val();
            $("#hdnContractTypeID").val(ID);
        }
        function yearValidation(year, ev) {
            var text = /^[0-9]+$/;
            if (ev.type == "blur" || year.length == 4 && ev.keyCode != 8 && ev.keyCode != 46) {
                if (year != 0) {
                    if ((year != "") && (!text.test(year))) {

                        alert("Please Enter Numeric Values Only");
                        return false;
                    }

                    if (year.length != 4) {
                        alert("Year is not proper. Please check");
                        return false;
                    }
                    var current_year = new Date().getFullYear();
                    var current_year = current_year + 100;
                    if ((year < 2015)) {
                        alert("Year should be in range 2015 to upcoming year");
                        return false;
                    }
                    return true;
                }
            }
        }
    </script>
    <script type="text/javascript">
        function ValidateContractID() {
            var check = 0;
            if ($("#chkDailys").is(":checked")) {
                check = 1;
            } else if ($("#chkWeeklys").is(":checked")) {
                check = 1;
            } else if ($("#chkMonthlys").is(":checked")) {
                check = 1;
            } else if ($("#chkYearlys").is(":checked")) {
                check = 1;
            }
            if (check == 0) {
                $('#txtMailId').removeClass('required');
                //alert('Please select schedule time.');
                //return false;
            } else {
                $("#txtMailId").addClass("required");
            }
        }
        $(document).ready(function () {
            if ($("#chkMonthlys").is(":checked") == false) {
                $("#txtMonth").attr('disabled', 'disabled').trigger("liszt:updated");
                $("#txtMonthlyTime").attr('disabled', 'disabled').trigger("liszt:updated");
                $("#txtMonthlyMonth").attr('disabled', 'disabled').trigger("liszt:updated");
                $("#ddlMontlyDay").attr('disabled', 'disabled').trigger("liszt:updated");
            }
            if ($("#chkDailys").is(":checked") == false) {
                $("#txtDailyTime").attr('disabled', 'disabled').trigger("liszt:updated");
            }
            if ($("#chkWeeklys").is(":checked") == false) {
                $("#txtWeeklyTime").attr('disabled', 'disabled').trigger("liszt:updated");
                $("#ddlWeeklyDay").attr('disabled', 'disabled').trigger("liszt:updated");
            }
            if ($("#chkYearlys").is(":checked") == false) {
                $("#txtYearlyTime").attr('disabled', 'disabled').trigger("liszt:updated");
                $("#txtYearlyMonth").attr('disabled', 'disabled').trigger("liszt:updated");
                $("#txtYearlyYear").attr('disabled', 'disabled').trigger("liszt:updated");
                $("#ddlYearlyDay").attr('disabled', 'disabled').trigger("liszt:updated");
            }

            $("#chkDailys").click(function () {
                if ($(this).is(":checked")) {
                    $("#txtDailyTime").removeAttr('disabled').trigger("liszt:updated");
                    $("#txtDailyTime").addClass("required");
                } else {
                    $("#txtDailyTime").attr('disabled', 'disabled').trigger("liszt:updated");
                    $('#txtDailyTime').removeClass('required');
                    $('#txtDailyTime').val('');
                }
            });

            $("#chkWeeklys").click(function () {
                if ($(this).is(":checked")) {
                    $("#ddlWeeklyDay").removeAttr('disabled').trigger("liszt:updated");
                    $("#txtWeeklyTime").removeAttr('disabled').trigger("liszt:updated");
                    $("#ddlWeeklyDay").addClass("required");
                    $("#txtWeeklyTime").addClass("required");
                } else {
                    $("#ddlWeeklyDay").val('0').change();
                    $("#ddlWeeklyDay").attr('disabled', 'disabled').trigger("liszt:updated");
                    $("#txtWeeklyTime").attr('disabled', 'disabled').trigger("liszt:updated");
                    $('#ddlWeeklyDay').removeClass('required');
                    $('#txtWeeklyTime').removeClass('required');
                    $('#txtWeeklyTime').val('');
                }
            });

            $("#chkMonthlys").click(function () {
                if ($(this).is(":checked")) {
                    $("#ddlMontlyDay").removeAttr('disabled').trigger("liszt:updated");
                    $("#txtMonthlyTime").removeAttr('disabled').trigger("liszt:updated");
                    $("#txtMonthlyMonth").removeAttr('disabled').trigger("liszt:updated");
                    $("#ddlMontlyDay").addClass("required");
                    $("#txtMonthlyTime").addClass("required");
                    $("#txtMonthlyMonth").addClass("required");
                } else {
                    $("#ddlMontlyDay").val('0').change();
                    $("#ddlMontlyDay").attr('disabled', 'disabled').trigger("liszt:updated");
                    $("#txtMonthlyTime").attr('disabled', 'disabled').trigger("liszt:updated");
                    $("#txtMonthlyMonth").attr('disabled', 'disabled').trigger("liszt:updated");
                    $('#ddlMontlyDay').removeClass('required');
                    $('#txtMonthlyTime').removeClass('required');
                    $('#txtMonthlyMonth').removeClass('required');
                    $('#txtMonthlyTime').val('');
                    $('#txtMonthlyMonth').val('');
                }
            });

            $("#chkYearlys").click(function () {
                if ($(this).is(":checked")) {
                    $("#ddlYearlyDay").removeAttr('disabled').trigger("liszt:updated");
                    $("#txtYearlyTime").removeAttr('disabled').trigger("liszt:updated");
                    $("#txtYearlyMonth").removeAttr('disabled').trigger("liszt:updated");
                    $("#txtYearlyYear").removeAttr('disabled').trigger("liszt:updated");
                    $("#txtYearlyTime").addClass("required");
                    $("#txtYearlyMonth").addClass("required");
                    $("#txtYearlyYear").addClass("required");
                    $("#ddlYearlyDay").addClass("required");
                } else {
                    $("#ddlYearlyDay").val('0').change();
                    $("#ddlYearlyDay").attr('disabled', 'disabled').trigger("liszt:updated");
                    $("#txtYearlyTime").attr('disabled', 'disabled').trigger("liszt:updated");
                    $("#txtYearlyMonth").attr('disabled', 'disabled').trigger("liszt:updated");
                    $("#txtYearlyYear").attr('disabled', 'disabled').trigger("liszt:updated");
                    $('#ddlYearlyDay').removeClass('required');
                    $('#txtYearlyTime').removeClass('required');
                    $('#txtYearlyMonth').removeClass('required');
                    $('#txtYearlyYear').removeClass('required');
                    $('#txtYearlyTime').val('');
                    $('#txtYearlyMonth').val('');
                    $('#txtYearlyYear').val('');
                }
            });

            try {
                $("#txtTime").timepicker({
                    timeFormat: 'HH:mm',
                    interval: 15 // 15 minutes
                });
                $("#txtDailyTime").timepicker({
                    timeFormat: 'HH:mm',
                    interval: 15 // 15 minutes
                });
                $("#txtWeeklyTime").timepicker({
                    timeFormat: 'HH:mm',
                    interval: 15 // 15 minutes
                });
                $("#txtMonthlyTime").timepicker({
                    timeFormat: 'HH:mm',
                    interval: 15 // 15 minutes
                });
                $("#txtYearlyTime").timepicker({
                    timeFormat: 'HH:mm',
                    interval: 15 // 15 minutes
                });

                $('#txtYearlyMonth').datepicker({
                    changeMonth: true,
                    showButtonPanel: true,
                    dateFormat: 'MM',
                    onClose: function (dateText, inst) {
                        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                    }
                });
                $('#txtMonthlyMonth').datepicker({
                    changeMonth: true,
                    showButtonPanel: true,
                    dateFormat: 'MM',
                    onClose: function (dateText, inst) {
                        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                    }
                });

            }
            catch (e) {
                alert(e);
            }

        });
    </script>
</asp:Content>
