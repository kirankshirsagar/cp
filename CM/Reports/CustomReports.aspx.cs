﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReportBLL;
using CommonBLL;
using System.Data;
using System.Text;

public partial class CustomReports : System.Web.UI.Page
{
    ICustomReports objReport;
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 50;
    List<string> fn;
    public string Add;
    public string View;
    public string Update;
    public string Delete;
    protected void Page_Load(object sender, EventArgs e)
    {
        //hdnReportFlag.Value = "76";
        CreateObjects();
        Session["MasterSeelcted"] = null;
        Session["ChlidSeelcted"] = null;
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            Access.PageAccess("CustomReports.aspx", Session[Declarations.User].ToString());
            ViewState[Declarations.Add] = Access.Add.Trim();
            ViewState[Declarations.View] = Access.View.Trim();
            ViewState[Declarations.Delete] = Access.Delete.Trim();
            ViewState[Declarations.Update] = Access.Update.Trim();

            Session[Declarations.SortControl] = null;
            LoadCustomData(1, RecordsPerPage);
        }
        else
        {
            SetNavigationButtonParameters();
        }
        AccessVisibility();
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objReport = FactoryReports.CustomReportDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    public void LoadCustomData(int pageNo, int recordsPerPage)
    {
        objReport.PageNo = pageNo;
        objReport.RecordsPerPage = recordsPerPage;
        LinkButton btnSort = null;
        if (Session[Declarations.SortControl] != null && Session[Declarations.SortControl] != string.Empty)
        {
            btnSort = (LinkButton)Session[Declarations.SortControl];
            objReport.Direction = btnSort.CssClass.ToLower() == "sort desc" ? 1 : 0;
            objReport.SortColumn = btnSort.Text.Trim();
        }
        objReport.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        DataTable dt = objReport.GetCustomData();
        rptCustom.DataSource = dt;
        rptCustom.DataBind();
        if (btnSort != null)
        {
            rptCustom.ClassChange(btnSort);
        }
        ViewState[Declarations.TotalRecord] = objReport.TotalRecords;
        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.AddpagingButton();
    }

    #region Navigation
    void SetNavigationButtonParameters()
    {
        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            //ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage, Convert.ToString(ViewState["selectQuery"]));
            LoadCustomData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);

        }
    }

    #endregion

    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }
    }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (View == "N")
        {
            Page.extDisableControls();
        }
        if (Update == "N")
        {
            hdnIsUpdate.Value = "N";
        }
        if (Add == "N")
        {
            hdnisAdd.Value = "N";
        }
        if (Delete == "N")
        {
            hdnIsDelete.Value = "N";
        }
        else if (Delete == "Y")
        {
            hdnIsDelete.Value = "Y";
        }
        Page.TraceWrite("AccessVisibility starts.");
    }

    protected void btnSort_Click(object sender, EventArgs e)
    {
        hdnSearch.Value = "";
        LinkButton clickBtn = (LinkButton)sender;
        Session.Add(Declarations.SortControl, clickBtn);
        LoadCustomData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
        //ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage, Convert.ToString(ViewState["selectQuery"]));
    }

    void ReadData(int pageNo, int recordsPerPage, string Selectquery)
    {
        Page.TraceWrite("ReadData starts.");
        try
        {
            ICustomReports objReport = FactoryReports.CustomReportDetail();
            objReport.PageNo = pageNo;
            objReport.RecordsPerPage = recordsPerPage;
            objReport.SelectQuery = Selectquery;
            LinkButton btnSort = null;
            if (Session[Declarations.SortControl] != null && Session[Declarations.SortControl] != string.Empty)
            {
                btnSort = (LinkButton)Session[Declarations.SortControl];
                objReport.Direction = hdnColumnOrder.Value.ToLower() == "sort desc" ? 1 : 0;
                objReport.SortColumn = Convert.ToString(hdnColumnName.Value).Trim();
            }

            //IMasterReports objReport1 = FactoryReports.MasterReportDetail();
            //rptReports.DataSource = objReport1.GetReport();
            //rptReports.DataBind();

            DataTable dt = objReport.GetCustomRecord();
            fn = new List<string>();
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                if (dt.Columns[i].ColumnName != "Maxcount" && dt.Columns[i].ColumnName != "N" && dt.Columns[i].ColumnName != "RequestId"
                    && dt.Columns[i].ColumnName != "RequestId_ID" && dt.Columns[i].ColumnName != "RequestId_KO" && dt.Columns[i].ColumnName != "N1"
                    && dt.Columns[i].ColumnName != "N2")
                {
                    fn.Add(dt.Columns[i].ColumnName);
                }
            }

            //Repeater1.DataSource = dt;
            //Repeater1.DataBind();
            Page.TraceWrite("ReadData ends.");
            ViewState[Declarations.TotalRecord] = objReport.TotalRecords;
            PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
            PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
            PaginationButtons1.RecordsPerPage = RecordsPerPage;
            PaginationButtons1.AddpagingButton();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }

    public List<string> fieldName()
    {
        //ICustomReports objReport = FactoryReports.CustomReportDetail();
        //objReport.SelectQuery = Convert.ToString(ViewState["selectQuery"]);
        //DataTable dt = objReport.GetCustomRecord();
        ////DataTable dt = objReport.GetReports();
        //List<string> fn = new List<string>();
        //for (int i = 0; i < dt.Columns.Count; i++)
        //{
        //    if (dt.Columns[i].ColumnName != "Maxcount" && dt.Columns[i].ColumnName != "N" && dt.Columns[i].ColumnName != "RequestId"
        //        && dt.Columns[i].ColumnName != "RequestId_ID" && dt.Columns[i].ColumnName != "RequestId_KO" && dt.Columns[i].ColumnName != "N1"
        //        && dt.Columns[i].ColumnName != "N2")
        //    {
        //        fn.Add(dt.Columns[i].ColumnName);
        //    }
        //}
        return fn;
    }

    protected void Repeater1_databinding(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {
            if (e.Item.FindControl("literalHeader") != null)
            {
                StringBuilder sb = new StringBuilder();
                Literal li = e.Item.FindControl("literalHeader") as Literal;

                fieldName().ForEach(delegate(string fn)
                {
                    if (hdnColumnName.Value != fn.ToString())
                    {
                        sb.Append("<th width=\"10%\"> <a id=\"btnCustomerName\" class=\"sort desc\" href=\"javascript:__doPostBack('ctl00$MainContent$rptReports$ctl00$btnCustomerName','')\"  >"
                            + fn.ToString() + "</a></th>");
                    }
                    else
                    {
                        if (hdnColumnOrder.Value == "sort asc")
                            sb.Append("<th width=\"10%\"> <a id=\"btnCustomerName\" class=\"sort desc\" href=\"javascript:__doPostBack('ctl00$MainContent$rptReports$ctl00$btnCustomerName','')\"  >"
                           + fn.ToString() + "</a></th>");
                        else
                            sb.Append("<th width=\"10%\"> <a id=\"btnCustomerName\" class=\"sort asc\" href=\"javascript:__doPostBack('ctl00$MainContent$rptReports$ctl00$btnCustomerName','')\"  >"
                                                       + fn.ToString() + "</a></th>");
                    }
                });
                li.Text = sb.ToString();

            }

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (e.Item.FindControl("literals") != null)
            {
                DataRowView drv = (DataRowView)e.Item.DataItem;
                Literal li = e.Item.FindControl("literals") as Literal;
                StringBuilder sb = new StringBuilder();
                fieldName().ForEach(delegate(string fn)
                {
                    sb.Append("<td>" + drv[fn.ToString()] + "</td>");
                });
                li.Text = sb.ToString();
            }
        }
    }

    protected void rptCustom_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        HiddenField hdnCustomReportId = e.Item.FindControl("hdnCustomReportId") as HiddenField;
        if (e.CommandName == "Delete_Record")
        {
            objReport.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            objReport.CustomReportId = Convert.ToInt32(hdnCustomReportId.Value);
            string status = objReport.DeleteRecord();
            LoadCustomData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
            rptExport.LoadddlReport();
        }
        else if (e.CommandName == "Edit_Record")
        {
            Session[Declarations.CustomReportId] = hdnCustomReportId.Value;
            Server.Transfer("CustomReportCreate.aspx");
        }
        else if (e.CommandName == "Schedule_Record")
        {
            Session[Declarations.CustomReportId] = hdnCustomReportId.Value;
            //Server.Transfer("ScheduleCustomReports.aspx");
            Server.Transfer("CustomReportCreate.aspx");
        }
    }

    protected void btnAddRecord_Click(object sender, EventArgs e)
    {
        Server.Transfer("CustomReportCreate.aspx");
    }

}