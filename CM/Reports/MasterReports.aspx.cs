﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReportBLL;
using CommonBLL;
using System.Data;
using System.Data.SqlClient;
using System.Text;

public partial class Reports_MasterReports : System.Web.UI.Page
{
    // navigation//
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 50;
    // navigation//

    IMasterReports objReport;
    public bool View;
    List<string> fn ;

    protected void Page_Load(object sender, EventArgs e)
    {
        hdnReportFlag.Value = "75";
        Session["MasterSeelcted"] = null;
        Session["ChlidSeelcted"] = null;

        CreateObjects();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            ViewState[Declarations.View] = Convert.ToBoolean(Access.isCustomised(int.Parse(hdnReportFlag.Value)));
        }

        setAccessValues();
        if (!IsPostBack || hdnSearch.Value.Trim() != string.Empty)
        {
            //Bind Contract Type
            MasterBLL.IContractType objType = MasterBLL.FactoryMaster.GetContractTypeDetail();
            ddlContracttype.extDataBind(objType.SelectData());
            //End
            Session[Declarations.SortControl] = null;
            ReadData(1, RecordsPerPage);
            Session["ReportAccessId"] = "75";
        }
        else
        {
            SetNavigationButtonParameters();
        }
    }

    protected void btnSort_Click(object sender, EventArgs e)
    {
        hdnSearch.Value = "";
        LinkButton clickBtn = (LinkButton)sender;
        Session.Add(Declarations.SortControl, clickBtn);
        ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
    }

    // navigation//

    #region Navigation
    void SetNavigationButtonParameters()
    {
        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }


    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);

        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

    }
    #endregion

    // navigation//


    void ReadData(int pageNo, int recordsPerPage)
    {
        if (View == true)
        {
            Page.TraceWrite("ReadData starts.");
            try
            {
                if (txtFromDate.Value.Trim() != "")
                {
                    objReport.FromDate = Convert.ToDateTime(txtFromDate.Value);
                }
                if (txtToDate.Value.Trim() != "")
                {
                    objReport.ToDate = Convert.ToDateTime(txtToDate.Value);
                }


                if (hdnContractTypeID.Value != "0")
                {
                    objReport.ContractTypeID = Convert.ToInt32(hdnContractTypeID.Value);
                    ddlContracttype.Value = hdnContractTypeID.Value;
                }

                objReport.PageNo = pageNo;
                objReport.RecordsPerPage = recordsPerPage;
                objReport.Search = txtSearch.Value.Trim();
                //LinkButton btnSort = null;
                //if (Session[Declarations.SortControl] != null && Session[Declarations.SortControl] != string.Empty)
                //{
                //    btnSort = (LinkButton)Session[Declarations.SortControl];
                //    objReport.Direction = btnSort.CssClass.ToLower() == "sort desc" ? 1 : 0;
                //    //objReport.SortColumn = btnSort.Text.Trim();
                //    objReport.SortColumn = Convert.ToString(hdnColumnName.Value).Trim();
                //}

                if (Session[Declarations.SortControl] != null && Session[Declarations.SortControl] != string.Empty)
                {
                    objReport.Direction = hdnColumnOrder.Value.ToLower() == "sort desc" ? 1 : 0;
                    objReport.SortColumn = Convert.ToString(hdnColumnName.Value).Trim();
                }
                

                //MasterReportsDAL obj = new MasterReportsDAL();
                //rptReports.DataSource = objReport.GetReport();
                //rptReports.DataBind();
                objReport.UserID = int.Parse(Session[Declarations.User].ToString());
                DataTable dt = objReport.GetREports();
                ViewState["dtColumnsCount"] = dt.Columns.Count;

                fn = new List<string>();
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (dt.Columns[i].ColumnName != "Maxcount" && dt.Columns[i].ColumnName != "N" && dt.Columns[i].ColumnName != "RequestId"
                        && dt.Columns[i].ColumnName != "RequestId_ID" && dt.Columns[i].ColumnName != "RequestId_KO" && dt.Columns[i].ColumnName != "N1"
                        && dt.Columns[i].ColumnName != "N2")
                    {
                        fn.Add(dt.Columns[i].ColumnName);
                    }
                }

                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                //if (btnSort != null)
                //{
                //    rptReports.ClassChange(btnSort);
                //    //Repeater1.ClassChange(btnSort);
                //}
                Page.TraceWrite("ReadData ends.");
                ViewState[Declarations.TotalRecord] = objReport.TotalRecords;
                PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
                PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
                PaginationButtons1.RecordsPerPage = RecordsPerPage;
                PaginationButtons1.AddpagingButton();
            }
            catch (Exception ex)
            {
                Page.TraceWarn("ReadData fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objReport = FactoryReports.MasterReportDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }
   
    private void setAccessValues()
    {
        if (ViewState[Declarations.View] != null)
        {
            View = Convert.ToBoolean(ViewState[Declarations.View].ToString());
        }
    }

    public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
    {

    }

    public List<string> fieldName()
    {
        //DataTable dt = FactoryExport.ExportData(Convert.ToInt32(hdnReportFlag.Value));
        //List<string> fn=new List<string>();
        //for(int i=0;i<dt.Columns.Count;i++)
        //{
        //    if (dt.Columns[i].ColumnName != "Maxcount" && dt.Columns[i].ColumnName != "N" && dt.Columns[i].ColumnName != "RequestId"
        //        && dt.Columns[i].ColumnName != "RequestId_ID" && dt.Columns[i].ColumnName != "RequestId_KO" && dt.Columns[i].ColumnName != "N1"
        //        && dt.Columns[i].ColumnName != "N2" )
        //    {
        //        fn.Add(dt.Columns[i].ColumnName);
        //    }
        //}
        return fn;
    }

    //protected void Repeater1_databinding(object sender, RepeaterItemEventArgs e)
    //{
    //    if (e.Item.ItemType == ListItemType.Header)
    //    {
    //        if (e.Item.FindControl("literalHeader") != null)
    //        {
    //            StringBuilder sb = new StringBuilder();
    //            Literal li = e.Item.FindControl("literalHeader") as Literal;

    //            fieldName().ForEach(delegate (string fn)
    //            {
    //                if (hdnColumnName.Value != fn.ToString())
    //                {
    //                    sb.Append("<th width=\"10%\"> <a id=\"btnCustomerName\" class=\"sort desc\" href=\"javascript:__doPostBack('ctl00$MainContent$rptReports$ctl00$btnCustomerName','')\"  >"
    //                        + fn.ToString() + "</a></th>");
    //                }
    //                else
    //                {
    //                    if(hdnColumnOrder.Value=="sort asc")
    //                        sb.Append("<th width=\"10%\"> <a id=\"btnCustomerName\" class=\"sort desc\" href=\"javascript:__doPostBack('ctl00$MainContent$rptReports$ctl00$btnCustomerName','')\"  >"
    //                       + fn.ToString() + "</a></th>");
    //                    else
    //                        sb.Append("<th width=\"10%\"> <a id=\"btnCustomerName\" class=\"sort asc\" href=\"javascript:__doPostBack('ctl00$MainContent$rptReports$ctl00$btnCustomerName','')\"  >"
    //                                                   + fn.ToString() + "</a></th>");
    //                }
    //            });
    //            li.Text = sb.ToString();

    //        }
           
    //    }
    //    if (e.Item.ItemType == ListItemType.Item ||e.Item.ItemType == ListItemType.AlternatingItem)
    //    {
    //        if (e.Item.FindControl("literals") != null)
    //        {
    //            DataRowView drv = (DataRowView)e.Item.DataItem;
    //            Literal li = e.Item.FindControl("literals") as Literal;
    //            StringBuilder sb = new StringBuilder();
    //            fieldName().ForEach(delegate(string fn)
    //            {
    //                sb.Append("<td>" + drv[fn.ToString()] + "</td>");
    //            });
    //            li.Text = sb.ToString();
    //        }
    //    }
    //}

    protected void Repeater1_databinding(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {
            if (e.Item.FindControl("literalHeader") != null)
            {
                StringBuilder sb = new StringBuilder();
                Literal li = e.Item.FindControl("literalHeader") as Literal;

                fieldName().ForEach(delegate(string fn)
                {
                    if (hdnColumnName.Value != fn.ToString())
                    {
                        sb.Append("<th width=\"10%\"> <a id=\"btnCustomerName\" class=\"sort desc\" onclick=\"btnSorts_onclick()\" style=\"cursor:pointer;text-decoration: none !important;\" >"
                            + fn.ToString() + "</a></th>");
                    }
                    else
                    {
                        if (hdnColumnOrder.Value == "sort asc")
                            sb.Append("<th width=\"10%\"> <a id=\"btnCustomerName\" class=\"sort desc\"  onclick=\"btnSorts_onclick()\" style=\"cursor:pointer;text-decoration: none !important;\" >"
                           + fn.ToString() + "</a></th>");
                        else
                            sb.Append("<th width=\"10%\"> <a id=\"btnCustomerName\" class=\"sort asc\" onclick=\"btnSorts_onclick()\" style=\"cursor:pointer;text-decoration: none !important;\">"
                                                       + fn.ToString() + "</a></th>");
                    }
                });
                li.Text = sb.ToString();

            }

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (e.Item.FindControl("literals") != null)
            {
                DataRowView drv = (DataRowView)e.Item.DataItem;
                Literal li = e.Item.FindControl("literals") as Literal;
                StringBuilder sb = new StringBuilder();
                fieldName().ForEach(delegate(string fn)
                {
                    sb.Append("<td>" + drv[fn.ToString()] + "</td>");
                });
                li.Text = sb.ToString();
            }
        }
    }

    protected void lnkSort_Click(object sender, EventArgs e)
    {
        hdnSearch.Value = "";
        Session[Declarations.SortControl] = "SortControl";
        ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);

    }

}