﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;
using ReportBLL;
using System.Data;

public partial class Reports_PredefinedScheduleReport : System.Web.UI.Page
{
    ICustomReports objReport;
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 50;
    List<string> fn;

    protected void Page_Load(object sender, EventArgs e)
    {
        //hdnReportFlag.Value = "76";
        CreateObjects();
        setAccessValues();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            ViewState["ReportAccessId"] = Convert.ToString(Session["ReportAccessId"]);
            Session.Remove("ReportAccessId");
            FillDropDown();
            LoadReportName();

        }

    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objReport = FactoryReports.CustomReportDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    private void setAccessValues()
    {
        //if (ViewState[Declarations.View] != null)
        //{
        //    View = Convert.ToBoolean(ViewState[Declarations.View].ToString());
        //}
    }

    private void FillDropDown()
    {
        DataTable dt = objReport.GetRecipentMailIdData();
        if (dt.Rows.Count > 0)
        {
            ddlUser.DataSource = dt;
            ddlUser.DataTextField = "FullName";
            ddlUser.DataValueField = "Email";
            //ddlUser.DataValueField = "UsersId";
            ddlUser.DataBind();
            ddlUser.Multiple = true;
        }

    }

    protected void LoadReportName()
    {
        string spName = "";
        if (Convert.ToString(ViewState["ReportAccessId"]) == "64")
        {
            lblReportName.InnerText = "Contract Requester Analysis";
            spName = "CustomerRequesterStatus_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "70")
        {
            lblReportName.InnerText = "Contract Status Analysis";
            spName = "ContractStatusAnalysis_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "35")
        {
            lblReportName.InnerText = "Customer Contracts - Active";
            spName = "CustomerActiveContracts_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "58")
        {
            lblReportName.InnerText = "Customer Activie Contact Value";
            spName = "CustomerActivieContactValue_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "54")
        {
            lblReportName.InnerText = "Customer Contracts - Change Of Control";
            spName = "CustomerChangeControl_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "56")
        {
            lblReportName.InnerText = "Customer Inprocess Contracts";
            spName = "CustomerClientInprocess_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "71")
        {
            lblReportName.InnerText = "Customer Contracts - Terminated";
            spName = "CustomerContractsTerminated_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "47")
        {
            lblReportName.InnerText = "Customer Contract Type Analysis";
            spName = "CustomerContractTypeAnalysis_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "39")
        {
            lblReportName.InnerText = "Customer Contract Value Analysis";
            spName = "CustomerContractValue_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "62")
        {
            lblReportName.InnerText = "Department Analysis for Customer Contracts";
            spName = "CustomerDepartment_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "37")
        {
            lblReportName.InnerText = "Customer Contracts - Expired";
            spName = "CustomerExpiredTerminatedContracts_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "75")
        {
            lblReportName.InnerText = "Master Report";
            spName = "Master_Reports";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "41")
        {
            lblReportName.InnerText = "Customer Contracts - Geography Analysis";
            spName = "CustomerGeography_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "65")
        {
            lblReportName.InnerText = "Customer Contracts - Important Dates";
            spName = "CustomerImportantDates_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "52")
        {
            lblReportName.InnerText = "Customer Contracts - In Process";
            spName = "CustomerInprocess_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "50")
        {
            lblReportName.InnerText = "Customer Contracts - Key Obligations Analysis";
            spName = "CustomerKeyobligations_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "60")
        {
            lblReportName.InnerText = "Customer Renewal Date";
            spName = "CustomerRenewalDate_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "67")
        {
            lblReportName.InnerText = "Customer Contracts - Termination Requests";
            spName = "CustomerTerminatedContracts_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "43")
        {
            lblReportName.InnerText = "Customer Contracts - Type, Value and Geography Analysis";
            spName = "CustomerTypeValueGeography_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "45")
        {
            lblReportName.InnerText = "Customer Contracts - Upcoming Renewals";
            spName = "CustomerUpcomingRenewals_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "69")
        {
            lblReportName.InnerText = "Request History";
            spName = "HistoricReports_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "78")
        {
            lblReportName.InnerText = "Other Contracts - Active";
            spName = "OtherActiveContracts_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "87")
        {
            lblReportName.InnerText = "Other Contracts - Change Of Control";
            spName = "OtherChangeControl_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "71")
        {
            lblReportName.InnerText = "Other Contracts - Terminated";
            spName = "OtherContractsTerminated_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "84")
        {
            lblReportName.InnerText = "Other Contract Type Analysis";
            spName = "OtherContractTypeAnalysis_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "80")
        {
            lblReportName.InnerText = "Other Contract Value Analysis";
            spName = "OtherContractValue_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "88")
        {
            lblReportName.InnerText = "Department Analysis for Other Contracts";
            spName = "OtherDepartment_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "79")
        {
            lblReportName.InnerText = "Other Contracts - Expired";
            spName = "OtherExpiredTerminatedContracts_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "81")
        {
            lblReportName.InnerText = "Other Contracts - Geography Analysis";
            spName = "OtherGeography_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "89")
        {
            lblReportName.InnerText = "Other Contracts - Important Dates";
            spName = "OtherImportantDates_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "86")
        {
            lblReportName.InnerText = "Other Contracts - In Process";
            spName = "OtherInprocess_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "85")
        {
            lblReportName.InnerText = "Other Contracts - Key Obligations Analysis";
            spName = "OtherKeyobligations_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "67")
        {
            lblReportName.InnerText = "Other Contracts - Termination Requests";
            spName = "OtherTerminatedContracts_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "82")
        {
            lblReportName.InnerText = "Other Contracts - Type, Value and Geography Analysis";
            spName = "OtherTypeValueGeography_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "83")
        {
            lblReportName.InnerText = " Other Contracts - Upcoming Renewals";
            spName = "OtherUpcomingRenewals_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "36")
        {
            lblReportName.InnerText = "Supplier Contracts - Active";
            spName = "SupplierActiveContracts_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "59")
        {
            lblReportName.InnerText = "Supplier Activie Contact Value";
            spName = "SupplierActivieContactValue_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "55")
        {
            lblReportName.InnerText = "Supplier Contracts - Change Of Control";
            spName = "SupplierChangeControl_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "57")
        {
            lblReportName.InnerText = "Supplier Inprocess Contracts";
            spName = "SupplierClientInprocess_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "72")
        {
            lblReportName.InnerText = "Supplier Contracts - Terminated";
            spName = "SupplierContractsTerminated_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "48")
        {
            lblReportName.InnerText = "Supplier Contract Type Analysis";
            spName = "SupplierContractTypeAnalysis_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "40")
        {
            lblReportName.InnerText = "Supplier Contract Value Analysis";
            spName = "SupplierContractValue_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "63")
        {
            lblReportName.InnerText = "Department Analysis for Supplier Contracts";
            spName = "SupplierDepartment_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "38")
        {
            lblReportName.InnerText = "Supplier Contracts - Expired";
            spName = "SupplierExpiredTerminatedContracts_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "42")
        {
            lblReportName.InnerText = "Supplier Contracts - Geography Analysis";
            spName = "SupplierGeography_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "66")
        {
            lblReportName.InnerText = "Supplier Contracts - Important Dates";
            spName = "SupplierImportantDates_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "53")
        {
            lblReportName.InnerText = "Supplier Contracts - In Process";
            spName = "SupplierInprocess_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "51")
        {
            lblReportName.InnerText = "Supplier Contracts - Key Obligations Analysis";
            spName = "SupplierKeyobligations_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "61")
        {
            lblReportName.InnerText = "Supplier Renewal Date";
            spName = "SupplierRenewalDate_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "68")
        {
            lblReportName.InnerText = "Supplier Contracts - Termination Requests";
            spName = "SupplierTerminatedContracts_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "44")
        {
            lblReportName.InnerText = "Supplier Contracts - Type, Value and Geography Analysis";
            spName = "SupplierTypeValueGeography_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "46")
        {
            lblReportName.InnerText = "Supplier Contracts - Upcoming Renewals";
            spName = "SupplierUpcomingRenewals_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "90")
        {
            lblReportName.InnerText = "Other Contracts - Termination Requests";
            spName = "OtherTerminatedContracts_Report";
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "91")
        {
            lblReportName.InnerText = "Other Contracts - Terminated";
            spName = "OtherContractsTerminated_Report";
        }

        ViewState["spName"] = spName;

        ICustomReportsNew objReport = FactoryReports.CustomReportDetails();
        objReport.PageNo = 1;
        objReport.RecordsPerPage = 100;
        objReport.CustomReportId = Convert.ToInt32(ViewState["ReportAccessId"]);

        int Param = 0;
        DataTable dtSchedule = objReport.GetPredefineScheduleRecord();
        if (dtSchedule.Rows.Count > 0)
        {
            List<string> Chkdata = new List<string>();
            try
            {
                for (int i = 0; i < dtSchedule.Rows.Count; i++)
                {
                    try
                    {
                        ddlUser.extSelectedValues(Convert.ToString(dtSchedule.Rows[i]["RecipiantMailId"]).TrimStart(','));
                    }
                    catch (Exception ex)
                    {
                        Param = 1;
                    }


                    txtMailId.Text = Convert.ToString(dtSchedule.Rows[i]["MailId"]);
                    TimeSpan ts = TimeSpan.Parse(Convert.ToString(dtSchedule.Rows[i]["ScheduleTime"]));
                    if (Convert.ToString(dtSchedule.Rows[i]["ScheduleType"]) == "Daily")
                    {
                        chkDailys.Checked = true;
                        txtDailyTime.Text = Convert.ToString(ts.ToString(@"hh\:mm"));
                        Chkdata.Add("Daily");
                    }
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleType"]) == "Weekly")
                    {
                        Chkdata.Add("Weekly");
                        chkWeeklys.Checked = true;
                        txtWeeklyTime.Text = Convert.ToString(ts.ToString(@"hh\:mm"));
                        ddlWeeklyDay.SelectedValue = Convert.ToString(dtSchedule.Rows[i]["ScheduleWeekday"]);
                    }
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleType"]) == "Monthly")
                    {
                        Chkdata.Add("Monthly");
                        chkMonthlys.Checked = true;
                        txtMonthlyTime.Text = Convert.ToString(ts.ToString(@"hh\:mm"));
                        ddlMontlyDay.SelectedValue = Convert.ToString(dtSchedule.Rows[i]["ScheduleDay"]);
                        if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "1")
                            txtMonthlyMonth.Text = "January";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "2")
                            txtMonthlyMonth.Text = "February";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "3")
                            txtMonthlyMonth.Text = "March";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "4")
                            txtMonthlyMonth.Text = "April";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "5")
                            txtMonthlyMonth.Text = "May";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "6")
                            txtMonthlyMonth.Text = "June";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "7")
                            txtMonthlyMonth.Text = "July";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "8")
                            txtMonthlyMonth.Text = "August";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "9")
                            txtMonthlyMonth.Text = "September";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "10")
                            txtMonthlyMonth.Text = "October";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "11")
                            txtMonthlyMonth.Text = "November";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "12")
                            txtMonthlyMonth.Text = "December";

                    }
                    else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleType"]) == "Yearly")
                    {
                        Chkdata.Add("Yearly");
                        chkYearlys.Checked = true;
                        txtYearlyTime.Text = Convert.ToString(ts.ToString(@"hh\:mm"));
                        ddlYearlyDay.SelectedValue = Convert.ToString(dtSchedule.Rows[i]["ScheduleDay"]);
                        if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "1")
                            txtYearlyMonth.Text = "January";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "2")
                            txtYearlyMonth.Text = "February";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "3")
                            txtYearlyMonth.Text = "March";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "4")
                            txtYearlyMonth.Text = "April";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "5")
                            txtYearlyMonth.Text = "May";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "6")
                            txtYearlyMonth.Text = "June";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "7")
                            txtYearlyMonth.Text = "July";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "8")
                            txtYearlyMonth.Text = "August";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "9")
                            txtYearlyMonth.Text = "September";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "10")
                            txtYearlyMonth.Text = "October";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "11")
                            txtYearlyMonth.Text = "November";
                        else if (Convert.ToString(dtSchedule.Rows[i]["ScheduleMonth"]) == "12")
                            txtYearlyMonth.Text = "December";

                        txtYearlyYear.Text = Convert.ToString(dtSchedule.Rows[i]["ScheduleYear"]);
                    }

                }

                if (Param == 1)
                {
                    string script = "window.onload = function() { alert('Some recipients were removed as their email ids have changed.'); };";
                    ClientScript.RegisterStartupScript(this.GetType(), "UpdateTime", script, true);

                }
            }
            catch (Exception ex)
            {
            }

            ViewState["Chkdata"] = Chkdata;
        }

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        objReport.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objReport.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : "";
        if (Convert.ToString(ViewState["ReportAccessId"]) != "")
        {
            string status = "";

            objReport.CustomReportName = lblReportName.InnerText.Trim();
            objReport.PredefinedReportSP = Convert.ToString(ViewState["spName"]);
            objReport.IsCustomReport = Convert.ToString("N");
            objReport.CustomReportId = Convert.ToInt32(ViewState["ReportAccessId"]);
            objReport.MailId = txtMailId.Text.Trim();

            if (txtMailId.Text.Trim() != "")
                objReport.RecipiantMailId = "," + hdnUsers.Value;
            else
                objReport.RecipiantMailId = hdnUsers.Value;

            List<string> mylist = (List<string>)ViewState["Chkdata"];
            if (chkDailys.Checked == true)
            {
                objReport.Flag = 4;
                objReport.ScheduleTime = txtDailyTime.Text.Trim();
                objReport.ScheduleType = "Daily";
                status = objReport.InsertRecord();
                if (mylist != null)
                {
                    mylist.Remove("Daily");
                }
            }
            if (chkWeeklys.Checked == true)
            {
                objReport.Flag = 4;
                objReport.ScheduleTime = txtWeeklyTime.Text.Trim();
                objReport.ScheduleWeek = ddlWeeklyDay.SelectedValue;
                objReport.ScheduleType = "Weekly";
                status = objReport.InsertRecord();
                if (mylist != null)
                {
                    mylist.Remove("Weekly");
                }
            }
            if (chkMonthlys.Checked == true)
            {
                objReport.ScheduleWeek = "";
                objReport.Flag = 4;
                objReport.ScheduleTime = txtMonthlyTime.Text.Trim();
                if (txtMonthlyMonth.Text.Trim() == "January")
                    objReport.ScheduleMonth = "1";
                else if (txtMonthlyMonth.Text.Trim() == "February")
                    objReport.ScheduleMonth = "2";
                else if (txtMonthlyMonth.Text.Trim() == "March")
                    objReport.ScheduleMonth = "3";
                else if (txtMonthlyMonth.Text.Trim() == "April")
                    objReport.ScheduleMonth = "4";
                else if (txtMonthlyMonth.Text.Trim() == "May")
                    objReport.ScheduleMonth = "5";
                else if (txtMonthlyMonth.Text.Trim() == "June")
                    objReport.ScheduleMonth = "6";
                else if (txtMonthlyMonth.Text.Trim() == "July")
                    objReport.ScheduleMonth = "7";
                else if (txtMonthlyMonth.Text.Trim() == "August")
                    objReport.ScheduleMonth = "8";
                else if (txtMonthlyMonth.Text.Trim() == "September")
                    objReport.ScheduleMonth = "9";
                else if (txtMonthlyMonth.Text.Trim() == "October")
                    objReport.ScheduleMonth = "10";
                else if (txtMonthlyMonth.Text.Trim() == "November")
                    objReport.ScheduleMonth = "11";
                else if (txtMonthlyMonth.Text.Trim() == "December")
                    objReport.ScheduleMonth = "12";

                objReport.ScheduleDay = ddlMontlyDay.SelectedValue;
                objReport.ScheduleType = "Monthly";
                status = objReport.InsertRecord();
                if (mylist != null)
                {
                    mylist.Remove("Monthly");
                }
            }

            if (chkYearlys.Checked == true)
            {
                objReport.ScheduleWeek = "";
                objReport.ScheduleDay = ddlYearlyDay.SelectedValue;
                objReport.Flag = 4;
                objReport.ScheduleTime = txtYearlyTime.Text.Trim();
                if (txtYearlyMonth.Text.Trim() == "January")
                    objReport.ScheduleMonth = "1";
                else if (txtYearlyMonth.Text.Trim() == "February")
                    objReport.ScheduleMonth = "2";
                else if (txtYearlyMonth.Text.Trim() == "March")
                    objReport.ScheduleMonth = "3";
                else if (txtYearlyMonth.Text.Trim() == "April")
                    objReport.ScheduleMonth = "4";
                else if (txtYearlyMonth.Text.Trim() == "May")
                    objReport.ScheduleMonth = "5";
                else if (txtYearlyMonth.Text.Trim() == "June")
                    objReport.ScheduleMonth = "6";
                else if (txtYearlyMonth.Text.Trim() == "July")
                    objReport.ScheduleMonth = "7";
                else if (txtYearlyMonth.Text.Trim() == "August")
                    objReport.ScheduleMonth = "8";
                else if (txtYearlyMonth.Text.Trim() == "September")
                    objReport.ScheduleMonth = "9";
                else if (txtYearlyMonth.Text.Trim() == "October")
                    objReport.ScheduleMonth = "10";
                else if (txtYearlyMonth.Text.Trim() == "November")
                    objReport.ScheduleMonth = "11";
                else if (txtYearlyMonth.Text.Trim() == "December")
                    objReport.ScheduleMonth = "12";

                objReport.ScheduleYear = txtYearlyYear.Text.Trim();
                objReport.ScheduleType = "Yearly";
                status = objReport.InsertRecord();
                if (mylist != null)
                {
                    mylist.Remove("Yearly");
                }
            }

            if (status != "0")
            {
                txtMailId.Text = "";
                Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
            }
            if (mylist != null)
            {
                if (mylist.Count > 0)
                {
                    for (int j = 0; j < mylist.Count; j++)
                    {
                        objReport.Flag = 6;
                        objReport.ScheduleType = Convert.ToString(mylist[j]);
                        status = objReport.InsertRecord();
                    }
                }
            }
            chkDailys.Checked = false;
            chkWeeklys.Checked = false;
            chkMonthlys.Checked = false;
            chkYearlys.Checked = false;
            Page.Message("1", "0");
            rptExport.LoadddlReport();
            PageRedirect();
        }
        else
        {
            #region Edit Custom Report
            string status = "";
            objReport.PredefinedReportSP = Convert.ToString(ViewState["spName"]);
            objReport.IsCustomReport = Convert.ToString("N");
            objReport.CustomReportId = Convert.ToInt32(ViewState["ReportAccessId"]);
            objReport.MailId = txtMailId.Text.Trim();

            if (txtMailId.Text.Trim() != "")
                objReport.RecipiantMailId = "," + hdnUsers.Value;
            else
                objReport.RecipiantMailId = hdnUsers.Value;

            #region Schedule
            List<string> mylist = (List<string>)ViewState["Chkdata"];
            if (chkDailys.Checked == true)
            {
                objReport.Flag = 4;
                objReport.ScheduleTime = txtDailyTime.Text.Trim();
                objReport.ScheduleType = "Daily";
                status = objReport.InsertRecord();
                if (mylist != null)
                {
                    mylist.Remove("Daily");
                }
            }
            if (chkWeeklys.Checked == true)
            {
                objReport.Flag = 4;
                objReport.ScheduleTime = txtWeeklyTime.Text.Trim();
                objReport.ScheduleWeek = ddlWeeklyDay.SelectedValue;
                objReport.ScheduleType = "Weekly";
                status = objReport.InsertRecord();
                if (mylist != null)
                {
                    mylist.Remove("Weekly");
                }
            }
            if (chkMonthlys.Checked == true)
            {
                objReport.ScheduleWeek = "";
                objReport.Flag = 4;
                objReport.ScheduleTime = txtMonthlyTime.Text.Trim();
                if (txtMonthlyMonth.Text.Trim() == "January")
                    objReport.ScheduleMonth = "1";
                else if (txtMonthlyMonth.Text.Trim() == "February")
                    objReport.ScheduleMonth = "2";
                else if (txtMonthlyMonth.Text.Trim() == "March")
                    objReport.ScheduleMonth = "3";
                else if (txtMonthlyMonth.Text.Trim() == "April")
                    objReport.ScheduleMonth = "4";
                else if (txtMonthlyMonth.Text.Trim() == "May")
                    objReport.ScheduleMonth = "5";
                else if (txtMonthlyMonth.Text.Trim() == "June")
                    objReport.ScheduleMonth = "6";
                else if (txtMonthlyMonth.Text.Trim() == "July")
                    objReport.ScheduleMonth = "7";
                else if (txtMonthlyMonth.Text.Trim() == "August")
                    objReport.ScheduleMonth = "8";
                else if (txtMonthlyMonth.Text.Trim() == "September")
                    objReport.ScheduleMonth = "9";
                else if (txtMonthlyMonth.Text.Trim() == "October")
                    objReport.ScheduleMonth = "10";
                else if (txtMonthlyMonth.Text.Trim() == "November")
                    objReport.ScheduleMonth = "11";
                else if (txtMonthlyMonth.Text.Trim() == "December")
                    objReport.ScheduleMonth = "12";

                objReport.ScheduleDay = ddlMontlyDay.SelectedValue;
                objReport.ScheduleType = "Monthly";
                status = objReport.InsertRecord();
                if (mylist != null)
                {
                    mylist.Remove("Monthly");
                }
            }

            if (chkYearlys.Checked == true)
            {
                objReport.ScheduleWeek = "";
                objReport.ScheduleDay = ddlYearlyDay.SelectedValue;
                objReport.Flag = 4;
                objReport.ScheduleTime = txtYearlyTime.Text.Trim();
                if (txtYearlyMonth.Text.Trim() == "January")
                    objReport.ScheduleMonth = "1";
                else if (txtYearlyMonth.Text.Trim() == "February")
                    objReport.ScheduleMonth = "2";
                else if (txtYearlyMonth.Text.Trim() == "March")
                    objReport.ScheduleMonth = "3";
                else if (txtYearlyMonth.Text.Trim() == "April")
                    objReport.ScheduleMonth = "4";
                else if (txtYearlyMonth.Text.Trim() == "May")
                    objReport.ScheduleMonth = "5";
                else if (txtYearlyMonth.Text.Trim() == "June")
                    objReport.ScheduleMonth = "6";
                else if (txtYearlyMonth.Text.Trim() == "July")
                    objReport.ScheduleMonth = "7";
                else if (txtYearlyMonth.Text.Trim() == "August")
                    objReport.ScheduleMonth = "8";
                else if (txtYearlyMonth.Text.Trim() == "September")
                    objReport.ScheduleMonth = "9";
                else if (txtYearlyMonth.Text.Trim() == "October")
                    objReport.ScheduleMonth = "10";
                else if (txtYearlyMonth.Text.Trim() == "November")
                    objReport.ScheduleMonth = "11";
                else if (txtYearlyMonth.Text.Trim() == "December")
                    objReport.ScheduleMonth = "12";

                objReport.ScheduleYear = txtYearlyYear.Text.Trim();
                objReport.ScheduleType = "Yearly";
                status = objReport.InsertRecord();
                if (mylist != null)
                {
                    mylist.Remove("Yearly");
                }
            }

            if (mylist != null)
            {
                if (mylist.Count > 0)
                {
                    for (int j = 0; j < mylist.Count; j++)
                    {
                        objReport.Flag = 6;
                        objReport.ScheduleType = Convert.ToString(mylist[j]);
                        status = objReport.InsertRecord();
                    }
                }
            }
            #endregion

            Page.Message("1", hdnPrimeId.Value);
            PageRedirect();
            #endregion
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        PageRedirect();
    }

    protected void PageRedirect()
    {
        Session[Declarations.CustomReportId] = null;
        if (Convert.ToString(ViewState["ReportAccessId"]) == "35")
            Server.Transfer("CustomerActiveContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "64")
            Server.Transfer("ContractRequesterStatus.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "70")
            Server.Transfer("ContractStatusAnalysis.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "58")
            Server.Transfer("CustomerActivieContactValue.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "54")
            Server.Transfer("CustomerChangeControl.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "56")
            Server.Transfer("CustomerClientInprocessContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "71")
            Server.Transfer("CustomerContractsTerminated.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "47")
            Server.Transfer("CustomerContractTypeAnalysis.aspx");

        else if (Convert.ToString(ViewState["ReportAccessId"]) == "39")
            Server.Transfer("CustomerContractValue.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "62")
            Server.Transfer("CustomerDepartment.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "37")
            Server.Transfer("CustomerExpiredTerminatedContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "41")
            Server.Transfer("CustomerGeographyContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "65")
            Server.Transfer("CustomerImportantDates.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "52")
            Server.Transfer("CustomerInprocessContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "50")
            Server.Transfer("CustomerKeyobligationsContract.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "60")
            Server.Transfer("CustomerRenewalDate.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "67")
            Server.Transfer("CustomerTerminatedContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "43")
            Server.Transfer("CustomerTypeValueGeographyContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "45")
            Server.Transfer("CustomerUpcomingRenewalsContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "69")
            Server.Transfer("HistoricReports.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "75")
            Server.Transfer("MasterReports.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "78")
            Server.Transfer("OtherActiveContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "87")
            Server.Transfer("OtherChangeControl.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "71")
            Server.Transfer("OtherContractsTerminated.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "84")
            Server.Transfer("OtherContractTypeAnalysis.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "80")
            Server.Transfer("OtherContractValue.aspx");

        else if (Convert.ToString(ViewState["ReportAccessId"]) == "88")
            Server.Transfer("OtherDepartment.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "79")
            Server.Transfer("OtherExpiredTerminatedContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "81")
            Server.Transfer("OtherGeographyContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "89")
            Server.Transfer("OtherImportantDates.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "86")
            Server.Transfer("OtherInprocessContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "85")
            Server.Transfer("OtherKeyobligationsContract.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "67")
            Server.Transfer("OtherTerminatedContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "82")
            Server.Transfer("OtherTypeValueGeographyContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "83")
            Server.Transfer("OtherUpcomingRenewalsContracts.aspx");

        else if (Convert.ToString(ViewState["ReportAccessId"]) == "36")
            Server.Transfer("SupplierActiveContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "59")
            Server.Transfer("SupplierActivieContactValue.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "55")
            Server.Transfer("SupplierChangeControl.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "57")
            Server.Transfer("SupplierClientInprocessContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "72")
            Server.Transfer("SupplierContractsTerminated.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "48")
            Server.Transfer("SupplierContractTypeAnalysis.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "40")
            Server.Transfer("SupplierContractValue.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "63")
            Server.Transfer("SupplierDepartment.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "38")
            Server.Transfer("SupplierExpiredTerminatedContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "42")
            Server.Transfer("SupplierGeographyContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "66")
            Server.Transfer("SupplierImportantDates.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "53")
            Server.Transfer("SupplierInprocessContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "51")
            Server.Transfer("CustomerKeyobligationsContract.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "61")
            Server.Transfer("SupplierRenewalDate.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "68")
            Server.Transfer("SupplierTerminatedContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "44")
            Server.Transfer("SupplierTypeValueGeographyContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "46")
            Server.Transfer("SupplierUpcomingRenewalsContracts.aspx");
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "90")
        {
            Server.Transfer("OtherTerminatedContracts.aspx");
        }
        else if (Convert.ToString(ViewState["ReportAccessId"]) == "91")
        {
            Server.Transfer("OtherContractsTerminated.aspx");
        }
    }


    [System.Web.Services.WebMethod]
    public static string GetReportIDs(string ReportId)
    {
        ICustomReportsNew objReport = FactoryReports.CustomReportDetails();
        objReport.PageNo = 1;
        objReport.RecordsPerPage = 100;

        if (Convert.ToString(HttpContext.Current.Session["ReportAccessId"]) != "")
        {
            //objReport.CustomReportId = Convert.ToInt32(ReportId);
            objReport.CustomReportId = Convert.ToInt32(HttpContext.Current.Session["ReportAccessId"]);
            DataTable dtSchedule = objReport.GetPredefineScheduleRecord();
            if (dtSchedule.Rows.Count > 0)
            {
                return "Report Name is already exists.";
            }
            else
                return "";
        }
        else
            return "";

    }


}