﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="CustomReportsNew.aspx.cs" Inherits="Reports_CustomReportsNew" EnableEventValidation="false" %>

<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/reportrightactions.ascx" TagName="rptExport" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridNavigation.js" type="text/javascript"></script>
    <script src="../JQueryValidations/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    <script src="../CommonScripts/datePickerReports.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../scripts/jquery-ui-1.8.16.custom.css" />
    <script type="text/javascript">
        $("#reportLink").addClass("menulink");
        $("#reportTab").addClass("selectedtab");
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#ddlReport').removeClass('required');
            $('#lnkPredefineSheduleReport').css('display', 'none');
            $('.sort').click(function () {
                $('#hdnColumnName').val($(this).text());
                $('#hdnColumnOrder').val($(this).attr('class'));
                $(this).toggleClass("desc asc");
                try {
                    $("#hdnEventArgument").val('_Events');
                } catch (e) { }
                $("#lnkSort").click();
            });
        });
        function SetPageSize(pageSize) {
            $("#hdnpageSize").val(pageSize)
            $("#hdnRecordsPerPage").val(pageSize)
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnReportFlag" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnColumnName" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnColumnOrder" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnpageSize" runat="server" clientidmode="Static" type="hidden" value="" />
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <uc1:rptExport ID="rptExport" runat="server" />
    <h2>
        My Reports
    </h2>
    <h2 style="font-size: medium">
        <asp:Label ID="lblReportName" runat="server" Text="Custom Report" ClientIDMode="Static"></asp:Label>
    </h2>
    <div style="margin: 0; padding: 0; display: inline">
        <div class="autoscroll">
            <table width="100%">
                <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_databinding">
                    <HeaderTemplate>
                        <table id="masterDataTable" class="reportTable list issues" width="100%">
                            <thead>
                                <tr>
                                    <asp:Literal ID="literalHeader" runat="server"></asp:Literal>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <asp:Literal ID="literals" runat="server"></asp:Literal>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
            <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
        </div>
    </div>
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <div style="display: none;">
        <asp:Button ID="lnkSort" runat="server" ClientIDMode="Static" OnClick="lnkSort_Click">
        </asp:Button>
    </div>
    <asp:HiddenField ID="hdnContractTypeID" runat="server" Value="0" ClientIDMode="Static" />
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
        function GetContractTypeID() {
            var ID = $("#ddlContracttype").val();
            $("#hdnContractTypeID").val(ID);
        }
    </script>
</asp:Content>
