﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReportBLL;
using CommonBLL;
using System.Data;
using System.Data.SqlClient;
using System.Text;

public partial class Reports_CustomReportsNew : System.Web.UI.Page
{
    public bool View;
    ICustomReportsNew objReport;

    // navigation//
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 50;
    // navigation//
    List<string> fn;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Convert.ToString(Request.QueryString["AccessId"]) != "")
            hdnReportFlag.Value = Convert.ToString(Request.QueryString["AccessId"]);
        else
            hdnReportFlag.Value = "0";

        if (Convert.ToString(Request.QueryString["Name"]) != "")
            lblReportName.Text = Convert.ToString(Request.QueryString["Name"]);

        CreateObjects();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            ViewState[Declarations.View] = Convert.ToBoolean(Access.isCustomised(int.Parse(hdnReportFlag.Value)));
        }
        setAccessValues();
        if (!IsPostBack)
        {
            Session[Declarations.SortControl] = null;
            ReadData(1, RecordsPerPage, Convert.ToString(ViewState["selectQuery"]));
        }
        else
        {
            SetNavigationButtonParameters();
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objReport = FactoryReports.CustomReportDetails();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    #region Navigation
    void SetNavigationButtonParameters()
    {
        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage, Convert.ToString(ViewState["selectQuery"]));
        }
    }

    #endregion

    private void setAccessValues()
    {
        if (ViewState[Declarations.View] != null)
        {
            View = Convert.ToBoolean(ViewState[Declarations.View].ToString());
        }
    }

    protected void btnSort_Click(object sender, EventArgs e)
    {
        hdnSearch.Value = "";
        LinkButton clickBtn = (LinkButton)sender;
        Session.Add(Declarations.SortControl, clickBtn);
        ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage, Convert.ToString(ViewState["selectQuery"]));
    }


    protected void btnReset_Click(object sender, EventArgs e)
    {
        ReadData(1, RecordsPerPage, Convert.ToString(ViewState["selectQuery"]));

    }

    void ReadData(int pageNo, int recordsPerPage, string Selectquery)
    {
        Page.TraceWrite("ReadData starts.");
        try
        {
            ICustomReportsNew objReport = FactoryReports.CustomReportDetails();
            objReport.PageNo = pageNo;
            if (hdnpageSize.Value == "")
                objReport.RecordsPerPage = recordsPerPage;
            else
                objReport.RecordsPerPage = int.Parse(hdnpageSize.Value);
            if (Session[Declarations.SortControl] != null)
            {
                //btnSort = (LinkButton)Session[Declarations.SortControl];
                objReport.Direction = hdnColumnOrder.Value.ToLower() == "sort desc" ? 1 : 0;
                objReport.SortColumn = Convert.ToString(hdnColumnName.Value).Trim();
            }

            //IMasterReports objReport1 = FactoryReports.MasterReportDetail();
            //rptReports.DataSource = objReport1.GetReport();
            //rptReports.DataBind();

            objReport.UserID = int.Parse(Session[Declarations.User].ToString());
            objReport.CustomReportId = Convert.ToInt32(Request.QueryString["ReportId"]);
            objReport.UserID = int.Parse(Session[Declarations.User].ToString());
            DataTable dt = objReport.GetCustomRecord();
            fn = new List<string>();
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                if (dt.Columns[i].ColumnName != "Maxcount")
                {
                    fn.Add(dt.Columns[i].ColumnName);
                }
            }

            Repeater1.DataSource = dt;
            Repeater1.DataBind();
            Page.TraceWrite("ReadData ends.");
            ViewState[Declarations.TotalRecord] = objReport.TotalRecords;
            PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
            PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
            if (hdnpageSize.Value == "")
                PaginationButtons1.RecordsPerPage = RecordsPerPage;
            else
                PaginationButtons1.RecordsPerPage = int.Parse(hdnpageSize.Value);
            
            PaginationButtons1.AddpagingButton();
            ViewState["SortControl"] = null;
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }

    public List<string> fieldName()
    {
        return fn;
    }

    //protected void Repeater1_databinding(object sender, RepeaterItemEventArgs e)
    //{
    //    if (e.Item.ItemType == ListItemType.Header)
    //    {
    //        if (e.Item.FindControl("literalHeader") != null)
    //        {
    //            StringBuilder sb = new StringBuilder();
    //            Literal li = e.Item.FindControl("literalHeader") as Literal;

    //            fieldName().ForEach(delegate(string fn)
    //            {
    //                if (hdnColumnName.Value != fn.ToString())
    //                {
    //                    sb.Append("<th width=\"10%\"> <a id=\"btnCustomerName\" class=\"sort desc\" href=\"javascript:__doPostBack('ctl00$MainContent$rptReports$ctl00$btnCustomerName','')\"  >"
    //                        + fn.ToString() + "</a></th>");
    //                }
    //                else
    //                {
    //                    if (hdnColumnOrder.Value == "sort asc")
    //                        sb.Append("<th width=\"10%\"> <a id=\"btnCustomerName\" class=\"sort desc\" href=\"javascript:__doPostBack('ctl00$MainContent$rptReports$ctl00$btnCustomerName','')\"  >"
    //                       + fn.ToString() + "</a></th>");
    //                    else
    //                        sb.Append("<th width=\"10%\"> <a id=\"btnCustomerName\" class=\"sort asc\" href=\"javascript:__doPostBack('ctl00$MainContent$rptReports$ctl00$btnCustomerName','')\"  >"
    //                                                   + fn.ToString() + "</a></th>");
    //                }
    //            });
    //            li.Text = sb.ToString();

    //        }

    //    }
    //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
    //    {
    //        if (e.Item.FindControl("literals") != null)
    //        {
    //            DataRowView drv = (DataRowView)e.Item.DataItem;
    //            Literal li = e.Item.FindControl("literals") as Literal;
    //            StringBuilder sb = new StringBuilder();
    //            fieldName().ForEach(delegate(string fn)
    //            {
    //                sb.Append("<td>" + drv[fn.ToString()] + "</td>");
    //            });
    //            li.Text = sb.ToString();
    //        }
    //    }
    //}

    protected void Repeater1_databinding(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {
            if (e.Item.FindControl("literalHeader") != null)
            {
                StringBuilder sb = new StringBuilder();
                Literal li = e.Item.FindControl("literalHeader") as Literal;

                fieldName().ForEach(delegate(string fn)
                {
                    if (hdnColumnName.Value != fn.ToString())
                    {
                        sb.Append("<th width=\"10%\"> <a id=\"btnCustomerName\" class=\"sort desc\" style=\"cursor:pointer;text-decoration: none !important;\" >"
                            + fn.ToString() + "</a></th>");
                    }
                    else
                    {
                        if (hdnColumnOrder.Value == "sort asc")
                            sb.Append("<th width=\"10%\"> <a id=\"btnCustomerName\" class=\"sort desc\"  style=\"cursor:pointer;text-decoration: none !important;\" >"
                           + fn.ToString() + "</a></th>");
                        else
                            sb.Append("<th width=\"10%\"> <a id=\"btnCustomerName\" class=\"sort asc\" style=\"cursor:pointer;text-decoration: none !important;\">"
                                                       + fn.ToString() + "</a></th>");
                    }
                });
                li.Text = sb.ToString();

            }

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (e.Item.FindControl("literals") != null)
            {
                DataRowView drv = (DataRowView)e.Item.DataItem;
                Literal li = e.Item.FindControl("literals") as Literal;
                StringBuilder sb = new StringBuilder();
                fieldName().ForEach(delegate(string fn)
                {
                    sb.Append("<td>" + drv[fn.ToString()] + "</td>");
                });
                li.Text = sb.ToString();
            }
        }
    }

    protected void lnkSort_Click(object sender, EventArgs e)
    {
        hdnSearch.Value = "";
        Session[Declarations.SortControl] = "SortControl";
        ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage, Convert.ToString(ViewState["selectQuery"]));

    }
}