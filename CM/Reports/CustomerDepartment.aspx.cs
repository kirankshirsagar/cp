﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReportBLL;
using CommonBLL;



public partial class Reports_CustomerDepartment : System.Web.UI.Page
{
    // navigation//
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 50;
    // navigation//

    ICustomerDepartment objReport;
    public bool View;
    protected void Page_Load(object sender, EventArgs e)
    {
        hdnReportFlag.Value = "62";
        CreateObjects();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            ViewState[Declarations.View] = Convert.ToBoolean(Access.isCustomised(int.Parse(hdnReportFlag.Value)));
        }

        setAccessValues();
        if (!IsPostBack || hdnSearch.Value.Trim() != string.Empty)
        {
            //Bind Contract Type
            MasterBLL.IContractType objType = MasterBLL.FactoryMaster.GetContractTypeDetail();
            ddlContracttype.extDataBind(objType.SelectData());
            //End
            Session[Declarations.SortControl] = null;
            ReadData(1, RecordsPerPage);
            Session["ReportAccessId"] = "62";
        }
        else
        {
           SetNavigationButtonParameters();
        }
    }

    protected void btnSort_Click(object sender, EventArgs e)
    {
        hdnSearch.Value = "";
        LinkButton clickBtn = (LinkButton)sender;
        Session.Add(Declarations.SortControl, clickBtn);
        ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
    }

 

    // navigation//

    #region Navigation
    void SetNavigationButtonParameters()
    {
        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }


    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
            
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

    }
    #endregion

    // navigation//


    void ReadData(int pageNo, int recordsPerPage)
    {
        if (View == true)
        {
            Page.TraceWrite("ReadData starts.");
            try
            {
                if (txtFromDate.Value.Trim() != "")
                {
                    objReport.FromDate = Convert.ToDateTime(txtFromDate.Value);
                }
                if (txtToDate.Value.Trim() != "")
                {
                    objReport.ToDate = Convert.ToDateTime(txtToDate.Value);
                }

                if (hdnContractTypeID.Value != "0")
                {
                    objReport.ContractTypeID = Convert.ToInt32(hdnContractTypeID.Value);
                    ddlContracttype.Value = hdnContractTypeID.Value;
                }
                objReport.PageNo = pageNo;
                objReport.RecordsPerPage = recordsPerPage;
                objReport.Search = txtSearch.Value.Trim();
                LinkButton btnSort = null;
                if (Session[Declarations.SortControl] != null && Session[Declarations.SortControl] != string.Empty)
                {
                    btnSort = (LinkButton)Session[Declarations.SortControl];
                    objReport.Direction = btnSort.CssClass.ToLower() == "sort desc" ? 1 : 0;
                    objReport.SortColumn = btnSort.Text.Trim();
                }
                objReport.UserID = int.Parse(Session[Declarations.User].ToString());
                rptReports.DataSource = objReport.GetReport();
                rptReports.DataBind();
                if (btnSort != null) 
                {
                    rptReports.ClassChange(btnSort);
                }
                Page.TraceWrite("ReadData ends.");
                ViewState[Declarations.TotalRecord] = objReport.TotalRecords;
                PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
                PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
                PaginationButtons1.RecordsPerPage = RecordsPerPage;
            }
            catch (Exception ex)
            {
                Page.TraceWarn("ReadData fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objReport = FactoryReports.CustomerDepartment();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

   
    private void setAccessValues()
    {
        if (ViewState[Declarations.View] != null)
        {
            View = Convert.ToBoolean(ViewState[Declarations.View].ToString());
        }
    }



    public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
    {

    }
  
}










