﻿//function to allow only numbers
function numericsonly(ob) {
    var invalidChars = /[^0-9]/gi
    if (invalidChars.test(ob.value)) {
        ob.value = ob.value.replace(invalidChars, "");
    }
} //function to allow only numbers ends here

//On page load hide all tool tips
$('.required').next('.tooltip_outer_feedback').hide();
$('.required_feedback').next('.tooltip_outer').hide();

//Onpage load
$(document).ready(function () {
    (function (k, e, i, j) {
        k.fn.caret = function (b, l) {
            var a, c, f = this[0], d = k.browser.msie; if (typeof b === "object" && typeof b.start === "number" && typeof b.end === "number") { a = b.start; c = b.end } else if (typeof b === "number" && typeof l === "number") { a = b; c = l } else if (typeof b === "string") if ((a = f.value.indexOf(b)) > -1) c = a + b[e]; else a = null; else if (Object.prototype.toString.call(b) === "[object RegExp]") { b = b.exec(f.value); if (b != null) { a = b.index; c = a + b[0][e] } } if (typeof a != "undefined") {
                if (d) {
                    d = this[0].createTextRange(); d.collapse(true);
                    d.moveStart("character", a); d.moveEnd("character", c - a); d.select()
                } else { this[0].selectionStart = a; this[0].selectionEnd = c } this[0].focus(); return this
            } else {
                if (d) { c = document.selection; if (this[0].tagName.toLowerCase() != "textarea") { d = this.val(); a = c[i]()[j](); a.moveEnd("character", d[e]); var g = a.text == "" ? d[e] : d.lastIndexOf(a.text); a = c[i]()[j](); a.moveStart("character", -d[e]); var h = a.text[e] } else { a = c[i](); c = a[j](); c.moveToElementText(this[0]); c.setEndPoint("EndToEnd", a); g = c.text[e] - a.text[e]; h = g + a.text[e] } } else {
                    g =
    f.selectionStart; h = f.selectionEnd
                } a = f.value.substring(g, h); return { start: g, end: h, text: a, replace: function (m) { return f.value.substring(0, g) + m + f.value.substring(h, f.value[e]) } }
            }
        }
    })(jQuery, "length", "createRange", "duplicate");


    //Autogrow textarea
    (function ($) {
        $.fn.autogrow = function (options) {
            return this.filter('textarea').each(function () {
                var self = this;
                var $self = $(self);
                var minHeight = $self.height();
                var noFlickerPad = $self.hasClass('autogrow-short') ? 0 : parseInt($self.css('lineHeight')) || 0;
                var settings = $.extend({
                    preGrowCallback: null,
                    postGrowCallback: null
                }, options);

                var shadow = $('<div></div>').css({
                    position: 'absolute',
                    top: -10000,
                    left: -10000,
                    width: $self.width(),
                    fontSize: $self.css('fontSize'),
                    fontFamily: $self.css('fontFamily'),
                    fontWeight: $self.css('fontWeight'),
                    lineHeight: $self.css('lineHeight'),
                    resize: 'none',
                    'word-wrap': 'break-word'
                }).appendTo(document.body);

                var update = function (event) {
                    var times = function (string, number) {
                        for (var i = 0, r = ''; i < number; i++) r += string;
                        return r;
                    };

                    var val = self.value.replace(/</g, '&lt;')
                                    .replace(/>/g, '&gt;')
                                    .replace(/&/g, '&amp;')
                                    .replace(/\n$/, '<br/>&nbsp;')
                                    .replace(/\n/g, '<br/>')
                                    .replace(/ {2,}/g, function (space) { return times('&nbsp;', space.length - 1) + ' ' });

                    // Did enter get pressed?  Resize in this keydown event so that the flicker doesn't occur.
                    if (event && event.data && event.data.event === 'keydown' && event.keyCode === 13) {
                        val += '<br />';
                    }

                    shadow.css('width', $self.width());
                    shadow.html(val + (noFlickerPad === 0 ? '...' : '')); // Append '...' to resize pre-emptively.

                    var newHeight = Math.max(shadow.height() + noFlickerPad, minHeight);
                    if (settings.preGrowCallback != null) {
                        newHeight = settings.preGrowCallback($self, shadow, newHeight, minHeight);
                    }

                    $self.height(newHeight);

                    if (settings.postGrowCallback != null) {
                        settings.postGrowCallback($self);
                    }
                }

                $self.change(update).keyup(update).keydown({ event: 'keydown' }, update);
                $(window).resize(update);

                update();
            });
        };
    })(jQuery);

    $('textarea').autogrow();

    $("input").on("keypress", function (e) {
        if (e.which === 32 && !this.value.length)
            e.preventDefault();
    });

    $('.ValOnlyChar').keypress(function (key) {
        if ((key.charCode < 97 || key.charCode > 122) && (key.charCode < 65 || key.charCode > 90) && (key.charCode != 45)) return false;
    });
    $('.NumericOnly').keypress(function (key) {
        var charCode = key.charCode;
        if (charCode == 8) {
            return true;
        }
        else if ((charCode == 46 || $(this).val().indexOf('.') != -1) || (charCode < 48 || charCode > 57)) {
            return false;
        }
    });

    $('.required').find("input:radio").change(function () {
        if ($(this).val() != '' && $(this).val().trim() != '') {
            $(this).parent().next('.tooltip_outer').hide();
            $(this).parent().css('border-color', '');
        }
        else {
            $(this).parent().next('.tooltip_outer').show();
            $(this).parent().css('border-color', 'red');
        }
    });


    $('.ValOnlyNumChar').keypress(function (key) {
        debugger;
        var charCode = (key.which) ? key.which : key.keyCode
        if (charCode == 8) {
            return true;
        }
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^[a-z\d\s]+$/i;
        if (!pattern.test(myNum)) {
            return false;

        }
        return true;
        //  
    });

    //Code starts here added by Bharati   
    $('.ValOnlyNumChar').on('paste', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        $obj = $(this);
        setTimeout(function () {
            myNum = $obj.val();
            var pattern = /^[a-z\d\s]+$/i;

            if (!pattern.test(myNum)) {
                myNum = myNum.replace(/[^a-z\d\s]/g, '');
                $obj.val(myNum);
                return false;
            }
        }, 0);
    });

    $('.ClauseFieldsPatterns').keypress(function (key) {
        var charCode = (key.which) ? key.which : key.keyCode
        if (charCode == 8) {
            return true;
        }
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /[\^\#]/;
        if (pattern.test(myNum)) {
            return false;

        }
        return true;
        //  
    });
    $('.ClauseFieldsPatterns').on('paste', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        $obj = $(this);
        setTimeout(function () {
            myNum = $obj.val();
            var pattern = /[\^\#]/;

            if (pattern.test(myNum)) {
                myNum = myNum.replace(/[\^\#]/g, '');
                $obj.val(myNum);
                return false;
            }
        }, 0);
    });

    $('.StateCityName').keypress(function (key) {
        var charCode = (key.which) ? key.which : key.keyCode
        if (charCode == 8) {
            return true;
        }
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^[a-z\d\s]+$/i;
        if (!pattern.test(myNum)) {
            return false;
        }
        return true;
        //  
    });
    $('.StateCityName').on('paste', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        $obj = $(this);
        setTimeout(function () {
            myNum = $obj.val();
            var pattern = /^[a-z\d\s]+$/i;

            if (!pattern.test(myNum)) {
                myNum = myNum.replace(/[^a-z\d\s]/g, '');
                $obj.val(myNum);
                return false;
            }
        }, 0);
    });
    //Code ends here added by Bharati

    //On key up in texbox's hide error messages for required fields
    $('.required').keyup(function () {
        if ($(this).val() != '' && $(this).val().trim() != '') {
            $(this).next('.tooltip_outer').hide();
            $(this).css('border-color', '');
        }
        else {
            $(this).next('.tooltip_outer').show();
            $(this).css('border-color', 'red');
        }
    });

    // nilesh 01072014
    $('input.required').change(function () {
        if ($(this).val() != '' && $(this).val().trim() != '') {
            if ($(this).hasClass("datepicker")) {
                $(this).next("img").next('.tooltip_outer').hide();
            }
            else {
                $(this).next('.tooltip_outer').hide();
            }
            $(this).css('border-color', '');
        }
        else {
            $(this).next('.tooltip_outer').show();
            $(this).css('border-color', 'red');
        }
    });


    //On selected item change in dropdownlist hide error messages for required fields
    $('.chzn-select.required').change(function () {
        if ($(this).val() != '0' && $(this).val() != null) {
            $(this).next('div').next('.tooltip_outer').hide();
            $(this).css('border-color', '');
            $(this).next('div').find("a").css('border-color', '');
            $(this).next('div').find("ul.chzn-choices").css('border-color', '');
        }
        else if ($(this).val() == '0' || $(this).val() == null) {
            $(this).next('div').next('.tooltip_outer').show();
            $(this).css('border-color', '');
            $(this).next('div').find("a").css('border-color', 'red');
            $(this).next('div').find("ul.chzn-choices").css('border-color', 'red');
        }
        else {
            $(this).next('div').next('.tooltip_outer').show();
            $(this).css('border-color', '');
            $(this).next('div').find("a").css('border-color', 'red');
            $(this).next('div').find("ul.chzn-choices").css('border-color', 'red');
        }
    });

    //added by Bharati 04062014 12.42PM
    function ValidateMultiline(MultiSelectObj) {
        $(MultiSelectObj).parent(".chzn-container").next('.tooltip_outer').hide();
        $(MultiSelectObj).parent(".chzn-container").css('border-color', '');
    }
    $('.chzn-select').next('div.chzn-container').find('ul.chzn-results li').on('click', function () {
        //var HasContainer = $(this).next(".chzn-container").val() == "" ? true : false;
        $(this).parent('div').find("a").css('border-color', '');
        $(this).parent(".chzn-container").next('.tooltip_outer').hide();
        $(this).parent(".chzn-container").css('border-color', '');
    });

    //On key up for pin number avoid non-numeric characters
    $('.Pincode').keyup(function () {
        $(this).next('.tooltip_outer').hide();
        $(this).css('border-color', '');
        numericsonly(this); // definition of this function is above
    });

    // On button click or submitting values
    $('.btn_validate').click(function (e) {
        var empty_count = 0; // variable to store error occured status
        $('.required').next('.tooltip_outer').hide();
        $('.required').next().next('.tooltip_outer').hide();
        $('.required').css('border-color', '');

        $('.email').each(function () {
            $(this).next('.tooltip_outer').hide();
            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            if (filter.test($(this).val()) === false && $(this).val() != "") {
                $(this).after('<div class="tooltip_outer" style="filter:alpha(opacity=50); opacity:0.9;"><div class="arrow-left"></div><div class="tool_tip"  style="filter:alpha(opacity=50); opacity:0.5;">Invalid Email</div></div>').show("slow");
                empty_count = 1;
            }
            else {
                $(this).next('.tooltip_outer').hide();
            }

        });
        //        $('.EmailList').each(function () {
        //            $(this).next('.tooltip_outer').hide();
        //            var filter = /^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)|(['\&quot;][^\<\>'\&quot;]*['\&quot;]\s*\<\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\>))(,\s*((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)|(['\&quot;][^\<\>'\&quot;]*['\&quot;]\s*\<\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\>)))*$/;
        //            if (filter.test($(this).val()) === false && $(this).val() != "") {
        //                $(this).after('<div class="tooltip_outer" style="filter:alpha(opacity=50); opacity:0.9;"><div class="arrow-left"></div><div class="tool_tip"  style="filter:alpha(opacity=50); opacity:0.5;">Invalid Email Id(s)</div></div>').show("slow");
        //                empty_count = 1;
        //            }
        //            else {
        //                $(this).next('.tooltip_outer').hide();
        //            }

        //        });
        //-------------------code added by yogita 20/06/2014--------------for password validation on add user--------------
        $('.password').each(function () {
            $(this).next('.tooltip_outer').hide();
            if ($(this).val().length < 8) {
                $(this).after('<div class="tooltip_outer"  style="filter:alpha(opacity=50); opacity:0.9;"><div class="arrow-left"></div><div class="tool_tip"  style="filter:alpha(opacity=50); opacity:0.5;">length must be 8 and above</div></div>').show("slow");
                empty_count = 1;
            }
            else {
                $('.comfirm_password').next('.tooltip_outer').hide();
                if ($(this).val() === $('.comfirm_password').val()) {
                    $(this).next('.tooltip_outer').hide();
                }
                else {
                    $('.comfirm_password').after('<div class="tooltip_outer"  style="filter:alpha(opacity=50); opacity:0.9;"><div class="arrow-left"></div><div class="tool_tip"  style="filter:alpha(opacity=50); opacity:0.5;">Password mismatch</div></div>').show("slow");
                    empty_count = 1;
                }
            }
        });

        //-------------------------------

        $('.required').each(function () {
            if ($(this).get(0).tagName == "INPUT") {
                //alert($(this).hasClass("datepicker"));
                if ($(this).get(0).type == "text") {
                    if ($(this).val().length == 0 || $(this).val().trim() == '') {
                        // $(this).after("<div class='tooltip_outer'><div class='arrow-left'></div><div class='tool_tip'>Can't be blank</div></div>").show("slow");
                        $(this).css('border-color', 'red');
                        if (!$(this).hasClass("datepicker")) {
                            /* this comment by kk on 6 Aug 2016 for on contract request mailid radio button chck and notification dropdown list change position   */
                            //                            $(this).after("<div class='tooltip_outer'  style='filter:alpha(opacity=50); opacity:0.9;'><div class='tool_tip' style='filter:alpha(opacity=50); opacity:0.5;'>This field is required.</div></div>").show("slow");

                            /* this added by kk on 6 Aug 2016 for on contract request mailid radio button chck and notification dropdown list change position   */
                            try {
                                //if ($(this).closest('input').find('input:radio').attr('checked', true)) {
                                if ($(this).closest('input').next('span').find('label').attr('id') == 'lblNotifyCustomer') {
                                    $(this).closest('input').next('span').after("<div class='tooltip_outer'  style='filter:alpha(opacity=50); opacity:0.9;'><div class='tool_tip' style='filter:alpha(opacity=50); opacity:0.5;'>This field is required.</div></div>").show("slow");
                                }
                                //}
                                else {
                                    $(this).after("<div class='tooltip_outer'  style='filter:alpha(opacity=50); opacity:0.9;'><div class='tool_tip' style='filter:alpha(opacity=50); opacity:0.5;'>This field is required.</div></div>").show("slow");
                                }
                            }
                            catch (err) {
                                $(this).after("<div class='tooltip_outer'  style='filter:alpha(opacity=50); opacity:0.9;'><div class='tool_tip' style='filter:alpha(opacity=50); opacity:0.5;'>This field is required.</div></div>").show("slow");
                            }
                        }
                        else
                            $(this).next("img").after("<div class='tooltip_outer'  style='filter:alpha(opacity=50); opacity:0.9;'><div class='tool_tip' style='filter:alpha(opacity=50); opacity:0.5;'>This field is required.</div></div>").show("slow");
                        empty_count++;
                    }
                }
                else if ($(this).get(0).type == "radio") {

                }
            }
            else if ($(this).get(0).tagName == "TEXTAREA") {
                if ($(this).val().length == 0) {
                    // $(this).after("<div class='tooltip_outer'><div class='arrow-left'></div><div class='tool_tip'>Can't be blank</div></div>").show("slow");
                    $(this).css('border-color', 'red');
                    $(this).after("<div class='tooltip_outer'  style='filter:alpha(opacity=50); opacity:0.9;'><div class='tool_tip' style='filter:alpha(opacity=50); opacity:0.5;'>This field is required.</div></div>").show("slow");
                    empty_count++;
                }
            }
            else {
                $(this).css('border-color', '');
                $(this).next('.tooltip_outer').hide();

                if ($(this).hasClass('radio')) {
                    var isChecked = false;
                    $(this).find("input:radio").each(function (index, chk) {
                        if ($(chk).attr("checked") == "checked") {
                            isChecked = true;
                            return false;
                        }
                    });
                    if (!isChecked) {
                        $(this).after('<div class="tooltip_outer"  style="filter:alpha(opacity=50); opacity:0.9;"><div class="arrow-left"></div><div class="tool_tip"  style="filter:alpha(opacity=50); opacity:0.5;">Please select any option</div></div>').show("slow");
                        $(this).css('border-color', 'red');
                        empty_count++;
                    }
                }

                if ($(this).hasClass('Pincode')) {
                    if ($(this).val().length != 6) {
                        $(this).after('<div class="tooltip_outer"  style="filter:alpha(opacity=50); opacity:0.9;"><div class="arrow-left"></div><div class="tool_tip"  style="filter:alpha(opacity=50); opacity:0.5;">Pin number should be 6 digits</div></div>').show("slow");
                        $(this).css('border-color', 'red');
                        empty_count++;
                    }
                    else {
                        $(this).next('.tooltip_outer').hide();
                        $(this).css('border-color', '');
                    }
                }

                if ($(this).hasClass('chzn-select')) {
                    $(this).next('.tooltip_outer').hide();
                    var sText = $(this).text().toLowerCase();
                    var sVal = $(this).val();
                    var len = $(this).val() == null ? 0 : $(this).val().length;
                    //addes by Bharati 04062014 12.11PM
                    var ValidationMessage = '<div class="tooltip_outer ' + $(this).attr("id") + '"  style="filter:alpha(opacity=50); opacity:0.9;" ><div class="arrow-left"></div><div class="tool_tip"  style="filter:alpha(opacity=50); opacity:0.5;">Please select any option</div></div>';
                    var HasContainer = $(this).next(".chzn-container").val() == "" ? true : false;

                    if (len > 0 && sVal == 0) {
                        $("div ." + $(this).attr("id")).remove();
                        if (!HasContainer)
                            $(this).after(ValidationMessage).show("slow");
                        else
                            $(this).next(".chzn-container").after(ValidationMessage).show("slow");

                        if ($(this).next('div').hasClass("chzn-container-multi"))
                            $(this).next('div.chzn-container').find('ul.chzn-choices').css('border-color', 'red');
                        else
                            $(this).next('div').find("a").css('border-color', 'red');
                        empty_count++;
                    }
                    else if (len == 0) {
                        $("div ." + $(this).attr("id")).remove();
                        if (!HasContainer)
                            $(this).after(ValidationMessage).show("slow");
                        else
                            $(this).next(".chzn-container").after(ValidationMessage).show("slow");
                        // $(this).find("a").css('border-color', 'red');
                        if ($(this).next('div').hasClass("chzn-container-multi"))
                            $(this).next('div.chzn-container').find('ul.chzn-choices').css('border-color', 'red');
                        else
                            $(this).next('div').find("a").css('border-color', 'red');
                        empty_count++;
                    }
                    else {
                        if (!HasContainer) {
                            $(this).next('.tooltip_outer').remove();
                            $(this).css('border-color', '');
                        }
                        else {
                            $(this).next('div').find("a").css('border-color', '');
                            $(this).next(".chzn-container").next('.tooltip_outer').remove();
                            $(this).next(".chzn-container").css('border-color', '');
                        }
                    }
                }
            }
        }); //end of required

        if (empty_count > 0) {
            return false;
            e.preventDefault();
        }
        else {
            $('.tooltip_outer').hide();
            // alert('Successful');
        }
    }); //end of save button click functio

    //$('.EmailList') function added by Bharati [to validate quama separated emailids]
    $('.EmailList').bind('blur', function (element) {
        $(this).css('border-color', '');
        $(this).next('div.EmailList').hide();
        var value = element.currentTarget.value;
        var count = 0;
        if (value != "" && value != undefined) {
            var mySplitResult = value.split(",");
            for (i = 0; i < mySplitResult.length; i++) {
                var Result = $.trim(mySplitResult[i]);
                count++;
                if (!/^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)|(['\&quot;][^\<\>'\&quot;]*['\&quot;]\s*\<\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\>))(,\s*((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)|(['\&quot;][^\<\>'\&quot;]*['\&quot;]\s*\<\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\>)))*$/i.test(Result)) {
                    $(this).css('border-color', 'red');
                    $(this).after('<div class="EmailList" style="filter:alpha(opacity=50); opacity:0.9;"><div class="arrow-left"></div><div class="tool_tip"  style="filter:alpha(opacity=50); opacity:0.5;">Invalid Email Id(s)</div></div>').show("slow");
                    //return 0;
                }
                else {
                    $(this).css('border-color', '');
                    $(this).next('div.EmailList').hide();
                }
            }
        }
    }); //end of $('.emaillist').live('blur'

});              //end ready function

//ShowProgress() function added by Bharati [to show processing with image]
function ShowProgress() {
    setTimeout(function () {
        var modal = $('<div />');
        modal.addClass("modal");
        $('body').append(modal);
        var loading = $(".loading");
        loading.show();
        var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
        var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
        loading.css({ top: top, left: left });
    }, 200);
}



$(document).ready(function () {
    try {

        $('.txtCharNumberPaste').bind('paste', function (evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            $obj = $(this);
            setTimeout(function () {
                myNum = $obj.val();
                var pattern = /^[a-zA-Z-0-9 ]+$/i;
                if (!pattern.test(myNum)) {
                    myNum = myNum.replace(/[^a-zA-Z-0-9 \d\s]/g, '');
                    myNum = myNum.trim();
                    $obj.val(myNum);
                    return false;
                }
            }, 0);
        });

        $('.txtCharNumber').bind('keypress', function (evt) {
            var regex = new RegExp("^[a-zA-Z-0-9 ]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        });

        $('.PhoneNumberTxtNumSplCharPaste').bind('paste', function (evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            $obj = $(this);
            setTimeout(function () {
                myNum = $obj.val();
                var pattern = /^[a-zA-Z-0-9()&+]+$/i;
                if (!pattern.test(myNum)) {
                    myNum = myNum.replace(/[^a-zA-Z-0-9()&+\d\s]/g, '');
                    myNum = myNum.trim();
                    $obj.val(myNum);
                    return false;
                }
            }, 0);
        });

        $('.PhoneNumberTxtNumSplChar').bind('keypress', function (evt) {
            var regex = new RegExp("^[a-zA-Z-0-9()&+]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        });

        $('.TxtNumSplCharValidation').bind('keypress', function (evt) {
            var regex = new RegExp("^[a-zA-Z0-9()&+,. ]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        });

        $('.TxtNumSplCharValidation').bind('paste', function (evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            $obj = $(this);
            setTimeout(function () {
                myNum = $obj.val();
                var pattern = /^[a-zA-Z0-9()&+,. ]+$/i;
                if (!pattern.test(myNum)) {
                    myNum = myNum.replace(/[^a-zA-Z0-9()&+,. \d\s]/g, '');
                    myNum = myNum.trim();
                    $obj.val(myNum);
                    return false;
                }
            }, 0);
        });

        $('.Searchfilters').bind('keypress', function (evt) {
            var regex = new RegExp("^[a-zA-Z0-9(),. ]+$");   //a-zA-Z-0-9(),.
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (!regex.test(key) && keycode != 13) {
                event.preventDefault();
                return false;
            }
        });

        $('.Searchfilters').bind('paste', function (evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            $obj = $(this);
            setTimeout(function () {
                myNum = $obj.val();
                var pattern = /^[a-zA-Z0-9(),. ]+$/i;
                if (!pattern.test(myNum)) {
                    myNum = myNum.replace(/[^a-zA-Z0-9(),. \d\s]/g, '');
                    myNum = myNum.trim();
                    $obj.val(myNum);
                    return false;
                }
            }, 0);
        });
              
        $('.number').bind('keypress', function (evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode == 8) {
                return true;
            }
            else if ((charCode < 48) || (charCode > 57)) {
                return false;
            }
        });

        $('.int').bind('keypress', function (evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode == 8) {
                return true;
            }
            else if ((charCode < 48) || (charCode > 57)) {
                return false;
            }
        });

        $('.int').bind('paste', function (evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            $obj = $(this);
            setTimeout(function () {
                myNum = $obj.val();

                var pattern = /^[0-9]+$/;

                if (!pattern.test(myNum)) {
                    myNum = myNum.replace(/[^0-9]/g, '');
                    $obj.val(myNum);
                    return false;
                }
            }, 0);
        });

        $('.numbersOnly').bind('keypress', function (evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode == 8) {
                return true;
            }
            else if ((charCode != 46 || $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57)) {
                return false;
            }
        });


        /*money  - old name comCurrency*/
        $('.money,.moneyZero').bind('keypress', function (evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode == 8) {
                return true;
            }
            else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                if (charCode != 46) {
                    return false;
                }
            }

            var caretpos = $(this).caret().start;
            valuestart = $(this).val().substring(0, caretpos);
            valueend = $(this).val().substring(caretpos);
            myNum = valuestart + String.fromCharCode(charCode) + valueend;

            var count = 0;

            var pattern = /^\d{0,8}\.?\d{0,2}$/;

            if (!pattern.test(myNum)) {
                count = 1;
            }
            if (count == 1) {
                return false;
            }
            else {
                if (myNum > 1000000000) {
                    return false;
                }
            }
            return true;
        });


        $('.money').bind('blur', function () {
            if ($(this).val() != "") {
                var amt = parseFloat($(this).val());
                if (amt <= 0) {
                    $(this).val('');
                }
            }
        });


        $('.percentage').bind('keypress', function (evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode

            if (charCode == 8) {
                return true;
            }
            else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                if (charCode != 46) {
                    return false;
                }
            }
            var count = 0;
            var caretpos = $(this).caret().start;
            valuestart = $(this).val().substring(0, caretpos);
            valueend = $(this).val().substring(caretpos);
            myNum = valuestart + String.fromCharCode(charCode) + valueend;


            var pattern = /^\d{0,3}\.?\d{0,2}$/;
            if (!pattern.test(myNum)) {
                count = 1;

            }
            if (count == 1) {
                return false;
            }
            else {
                if (myNum > 100) {
                    return false;
                }
            }
            return true;
        });


        $('.Landline').bind('keypress', function (evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode

            if (charCode == 8) {
                return true;
            }
            else if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            var caretpos = $(this).caret().start;
            valuestart = $(this).val().substring(0, caretpos);
            valueend = $(this).val().substring(caretpos);
            myNum = valuestart + String.fromCharCode(charCode) + valueend;

            if (myNum.length > 10) {
                return false;
            }
            return true;
        });

        $('.alphaspaceonly').bind('keypress', function (event) {
            debugger;
            var regex = new RegExp("^[a-zA-Z ]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        });
        $('.Currency').bind('keypress', function (evt) {
            
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode == 8) {
                return true;
            }
            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                if (charCode != 46) {
                    return false;
                }
            }
            var count = 0;
            var myNum = this.value + String.fromCharCode(charCode);
            //  var pattern = /^\d{0,8}$/;
            var pattern = /^\d{0,18}\.?\d{0,2}$/;
            // var pattern = /^\d{0,8}\.?\d{0,0}$/;
            if (!pattern.test(myNum)) {
                count = 1;
            }
            if (count == 1) {
                return false;
            }

            else if (myNum > 99999999) {
                return false;
            }
            else
                return true;
        });

        $('.decimalss').keypress(function (event) {
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) &&
    ((event.which < 48 || event.which > 57) &&
      (event.which != 0 && event.which != 8))) {
                event.preventDefault();
            }

            var text = $(this).val();

            if ((text.indexOf('.') != -1) &&
    (text.substring(text.indexOf('.')).length > 2) &&
    (event.which != 0 && event.which != 8) &&
    ($(this)[0].selectionStart >= text.length - 2)) {
                event.preventDefault();
            }
        });




        $(".mws-datepicker").datepicker({ showOn: "both",
            buttonImage: "../Styles/css/icons/16/calendar_1.png",
            buttonImageOnly: true,
            prevText: 'Previous',
            yearRange: 'c-50:c+50',
            changeMonth: true,
            buttonText: '',
            onClose: function (dateText, inst) {
                $(this).focus();
            },
            changeYear: true
        }).attr('placeholder', 'dd-Mmm-yyyy').blur(function () { return RemoveErrorClass($(this)); });
    } catch (e) {

    }


    try {
        $(".mws-datepicker-MinMax1Month").datepicker({ showOn: "both",
            buttonImage: "../Styles/css/icons/16/calendar_1.png",
            buttonImageOnly: true,
            prevText: 'Previous',
            changeMonth: true,
            buttonText: '',
            changeYear: false,
            onClose: function (dateText, inst) {
                $(this).focus();
            },
            maxDate: "+1M",
            minDate: "-1M"
        }).attr('placeholder', 'dd-Mmm-yyyy').blur(function () { return RemoveErrorClass($(this)); });
    } catch (e) {

    }

    try {

        $(".mws-datepicker-Max1Month").datepicker({ showOn: "both",
            buttonImage: "../Styles/css/icons/16/calendar_1.png",
            buttonImageOnly: true,
            prevText: 'Previous',
            changeMonth: true,
            buttonText: '',
            onClose: function (dateText, inst) {
                $(this).focus();
            },
            changeYear: false,
            maxDate: "+1M",
            minDate: "0"
        }).attr('placeholder', 'dd-Mmm-yyyy').blur(function () { return RemoveErrorClass($(this)); });
    } catch (e) {

    }

    try {

        $(".mws-datepicker-MinToday").datepicker({ showOtherMonths: true, showOn: "both",
            changeMonth: true,
            changeYear: true,
            buttonImage: "../Styles/css/icons/16/calendar_1.png",
            maxDate: "0",
            yearRange: 'c-50:c+50',
            buttonText: '',
            onClose: function (dateText, inst) {
                $(this).focus();
            },
            prevText: 'Previous',
            buttonImageOnly: true,
            dateFormat: 'dd-MM-yy'
        }).attr('placeholder', 'dd-MM-yy').blur(function () { return RemoveErrorClass($(this)); });
    } catch (e) {

    }

    try {

        $(".mws-datepicker-wk").datepicker({ showWeek: true, showOn: "both",
            buttonImage: "../Styles/css/icons/16/calendar_1.png",
            changeMonth: true,
            prevText: 'Previous',
            changeYear: true,
            buttonText: '',
            onClose: function (dateText, inst) {
                $(this).focus();
            },
            buttonImageOnly: true,
            dateFormat: 'dd-MM-yy'
        }).blur(function () { return RemoveErrorClass($(this)); });
    } catch (e) {

    }

    try {

        $(".mws-datepicker-mm").datepicker({ numberOfMonths: 3, showOn: "both",
            buttonImage: "../Styles/css/icons/16/calendar_1.png",
            changeMonth: true,
            prevText: 'Previous',
            changeYear: true,
            buttonText: '',
            onClose: function (dateText, inst) {
                $(this).focus();
            },
            buttonImageOnly: true,
            dateFormat: 'dd-MM-yy'
        }).blur(function () { return RemoveErrorClass($(this)); });
    } catch (e) {

    }


    try {

        $(".mws-dtpicker").datetimepicker({ showOn: "both",
            buttonImage: "../Styles/css/icons/16/calendar_1.png",
            changeMonth: true,
            prevText: 'Previous',
            changeYear: true,
            onClose: function (dateText, inst) {
                $(this).focus();
            },
            buttonText: '',
            buttonImageOnly: true,
            dateFormat: 'dd-M-yy',
            timeFormat: 'hh:mm tt'
        }).attr('placeholder', 'dd-Mmm-yyyy').blur(function () { return RemoveErrorClass($(this)); });

    } catch (e) {

    }

    try {

        $(".mws-dtpicker-CurrentDate").datetimepicker({ showOn: "both",
            buttonImage: "../Styles/css/icons/16/calendar_1.png",
            changeMonth: true,
            prevText: 'Previous',
            changeYear: true,
            minDate: "0",
            onClose: function (dateText, inst) {
                $(this).focus();
            },
            buttonText: '',
            buttonImageOnly: true,
            dateFormat: 'dd-MM-yy'
        }).attr('placeholder', 'dd-MM-yy hh:mm').blur(function () { return RemoveErrorClass($(this)); });
    } catch (e) {

    }

    try {

        $(".mws-themerdtpicker").datetimepicker({ showOn: "both",
            buttonImage: "../Styles/css/icons/16/calendar_1.png",
            changeMonth: true,
            prevText: 'Previous',
            changeYear: true,
            minDate: "0",
            buttonText: '',
            onClose: function (dateText, inst) {
                $(this).focus();
            },
            buttonImageOnly: true,
            beforeShow: function (input, inst) {
                // Handle calendar position before showing it.
                // It's not supported by Datepicker itself (for now) so we need to use its internal variables.
                var calendar = inst.dpDiv;
                var offset = $(input).offset();
                var height = $(input).height();

                setTimeout(function () {
                    calendar.css({ top: 100 + 'px' })
                }, 1);
            }
        }).attr('placeholder', 'dd-mmm-yyyy hh:mm');
    } catch (e) {

    }

    try {

        $(".mws-datepicker-MMM-yyyy").datepicker({ showOtherMonths: true, showOn: "both",
            buttonImage: "../Styles/css/icons/16/calendar_1.png",
            buttonImageOnly: true,
            changeMonth: true,
            buttonText: '',
            CausesValidation: false,
            yearRange: 'c-70:c+70',
            changeYear: true,
            showButtonPanel: true,
            maxDate: "0",
            onClose: function (dateText, inst) {
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).val(new Date(year, month, 1).format("MMM-yyyy"));
            }
        }).attr('placeholder', 'MMM-yyyy').blur(function () { return RemoveErrorClass($(this)); });
    } catch (e) {

    }

    try {

        $(".mws-datepicker-MMM-yyyy").focus(function () {
            $(".ui-datepicker-calendar").hide();
            $("#ui-datepicker-div").position({
                my: "center top",
                at: "center bottom",
                of: $(this)
            });
            $(".ui-datepicker-buttonpane").find(".ui-datepicker-current").remove();
        });

    } catch (e) {

    }


    try {

        $(".mws-datepicker-MMM-yyyy").blur(function () { return RemoveErrorClass($(this)); });
    } catch (e) {

    }

    try {

        $(".mws-timepicker").timepicker({ showOn: "both",
            buttonImage: "../Styles/css/icons/16/clock.png",
            onClose: function (time, inst) {
                $(this).focus();
            },
            buttonText: '',
            buttonImageOnly: true
        }).attr('placeholder', 'hh:mm').blur(function () {
            return RemoveErrorClass($(this));
        });
    } catch (e) {

    }
    try {


        $(".mws-slider").slider({ range: "min" });
    } catch (e) {

    }
    try {

        $(".mws-progressbar").progressbar({ value: 90 });
    } catch (e) {

    }

    try {
        $(".mws-range-slider").slider({ range: true, min: 0, max: 500, values: [75, 300] });
    } catch (e) {

    }
});


/* nilesh calender  ends*/


$('.pasteBlock').bind('paste', function (e) {
    return false;
});

$.fn.getTab = function () {
    this.keydown(function (e) {
        if (e.keyCode === 9) {
            var val = this.value,
                start = this.selectionStart,
                end = this.selectionEnd;
            this.value = val.substring(0, start) + '\t' + val.substring(end);
            this.selectionStart = this.selectionEnd = start + 1;
            return false;
        }
        return true;
    });
    return this;
};
