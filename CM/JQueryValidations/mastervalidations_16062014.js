﻿//function to allow only numbers
function numericsonly(ob) {
    var invalidChars = /[^0-9]/gi
    if (invalidChars.test(ob.value)) {
        ob.value = ob.value.replace(invalidChars, "");
    }
} //function to allow only numbers ends here

//On page load hide all tool tips
$('.required').next('.tooltip_outer_feedback').hide();
$('.required_feedback').next('.tooltip_outer').hide();
//---

//Onpage load
$(document).ready(function () {

    //On key up in texbox's hide error messages for required fields
    $('.required').keyup(function () {
        if ($(this).val() != '') {
            $(this).next('.tooltip_outer').hide();
            $(this).css('border-color', '');
        }
        else {
            $(this).next('.tooltip_outer').show();
            $(this).css('border-color', '');

        }
    });



    $('.required').change(function () {
        if ($(this).val() != '') {
            $(this).next('.tooltip_outer').hide();
            $(this).css('border-color', '');
        }
        else {
            $(this).next('.tooltip_outer').show();
            $(this).css('border-color', '');

        }
    });





    //On selected item change in dropdownlist hide error messages for required fields
    $('.chzn-select').change(function () {        
        if ($(this).val() != '0') {
            $(this).next('div').next('.tooltip_outer').hide();
            $(this).next('div').css('border-color', '');
        }
        else {
            $(this).next('div').next('.tooltip_outer').show();
            $(this).next('div').css('border-color', '');
        }
    });

    //added by Bharati 04062014 12.42PM
    function ValidateMultiline(MultiSelectObj) {        
        $(MultiSelectObj).parent(".chzn-container").next('.tooltip_outer').hide();
        $(MultiSelectObj).parent(".chzn-container").css('border-color', '');
    }
    $('.chzn-select').next('div.chzn-container').find('ul.chzn-results li').on('click', function () {
        //var HasContainer = $(this).next(".chzn-container").val() == "" ? true : false;
        alert('in');
        debugger;
        $(this).parent(".chzn-container").next('.tooltip_outer').hide();
        $(this).parent(".chzn-container").css('border-color', '');
    });

    //On key up for pin number avoid non-numeric characters
    $('.Pincode').keyup(function () {
        $(this).next('.tooltip_outer').hide();
        $(this).css('border-color', '');
        numericsonly(this); // definition of this function is above
    });

    // On button click or submitting values
    $('.btn_validate').click(function (e) {
        var empty_count = 0; // variable to store error occured status
        $('.required').next('.tooltip_outer').hide();
        $('.required').next().next('.tooltip_outer').hide();
        $('.required').css('border-color', '');

        $('.required').each(function () {
            if ($(this).get(0).tagName == "INPUT") {
                if ($(this).val().length == 0) {
                    // $(this).after("<div class='tooltip_outer'><div class='arrow-left'></div><div class='tool_tip'>Can't be blank</div></div>").show("slow");
                    $(this).css('border-color', 'red');
                    $(this).after("<div class='tooltip_outer'  style='filter:alpha(opacity=50); opacity:0.9;'><div class='tool_tip' style='filter:alpha(opacity=50); opacity:0.5;'>This field is required.</div></div>").show("slow");
                    empty_count++;
                }
            }
            else if ($(this).get(0).tagName == "TEXTAREA") {
                if ($(this).val().length == 0) {
                    // $(this).after("<div class='tooltip_outer'><div class='arrow-left'></div><div class='tool_tip'>Can't be blank</div></div>").show("slow");
                    $(this).css('border-color', 'red');
                    $(this).after("<div class='tooltip_outer'  style='filter:alpha(opacity=50); opacity:0.9;'><div class='tool_tip' style='filter:alpha(opacity=50); opacity:0.5;'>This field is required.</div></div>").show("slow");
                    empty_count++;
                }
            }
            else {
                $(this).css('border-color', '');
                $(this).next('.tooltip_outer').hide();

                if ($(this).hasClass('Pincode')) {
                    if ($(this).val().length != 6) {
                        $(this).after('<div class="tooltip_outer"  style="filter:alpha(opacity=50); opacity:0.9;"><div class="arrow-left"></div><div class="tool_tip"  style="filter:alpha(opacity=50); opacity:0.5;">Pin number should be 6 digits</div></div>').show("slow");
                        $(this).css('border-color', 'red');
                        empty_count++;
                    }
                    else {
                        $(this).next('.tooltip_outer').hide();
                        $(this).css('border-color', '');
                    }
                }

                if ($(this).hasClass('chzn-select')) {
                    $(this).next('.tooltip_outer').hide();
                    var sText = $(this).text().toLowerCase();
                    var sVal = $(this).val();
                    var len = $(this).val() == null ? 0 : $(this).val().length;
                    //addes by Bharati 04062014 12.11PM
                    var ValidationMessage = '<div class="tooltip_outer ' + $(this).attr("id") + '"  style="filter:alpha(opacity=50); opacity:0.9;" ><div class="arrow-left"></div><div class="tool_tip"  style="filter:alpha(opacity=50); opacity:0.5;">Please select any option</div></div>';
                    var HasContainer = $(this).next(".chzn-container").val() == "" ? true : false;
                    debugger;
                    if (len > 0 && sVal == 0) {
                        $("div ." + $(this).attr("id")).remove();
                        if (!HasContainer)
                            $(this).after(ValidationMessage).show("slow");
                        else
                            $(this).next(".chzn-container").after(ValidationMessage).show("slow");
                        $(this).find("a").css('border-color', 'red');
                        empty_count++;
                    }
                    else if (len == 0) {
                        $("div ." + $(this).attr("id")).remove();
                        if (!HasContainer)
                            $(this).after(ValidationMessage).show("slow");
                        else
                            $(this).next(".chzn-container").after(ValidationMessage).show("slow");
                        $(this).find("a").css('border-color', 'red');
                        empty_count++;
                    }
                    else {
                        if (!HasContainer) {
                            $(this).next('.tooltip_outer').remove();
                            $(this).css('border-color', '');
                        }
                        else {
                            $(this).next(".chzn-container").next('.tooltip_outer').remove();
                            $(this).next(".chzn-container").css('border-color', '');
                        }
                    }
                }
            }
        }); //end of required

        debugger;
        if (empty_count > 0) {
            debugger;
            return false;
            e.preventDefault();

        }
        else {
            $('.tooltip_outer').hide();
            // alert('Successful');

        }
    });      //end of save button click functio
});                                         //end ready function
