﻿$(document).ready(function () {


    $('.pasteBlock').live('paste', function (e) {
        return false;
    });

    $('.cutCopyPasteBlock').live('copy cut paste', function (e) {
        return false;
    });

    $('.noInput').live('keypress', function (e) {
        return false;
    });


    //    $('.number , .int , .numbersOnly , .money , .percentage , .Landline , .mobile').live('copy cut paste', function (e) {
    //        e.preventDefault();
    //    });





    /* 
    * Validators


    * 1 - number                2 - numbersOnly       3 - money                 4 - percentage
    * 5 - Landline              6 - mobile            7- pin                       8- pancard 
    * 9 - ValChar             10 - ValOnlyNumChar    11 - ValOnlyChar          12 -  ValOnlyCharSingleSpace
    * 13 - email                14 - url
    */

    /* number  - old name comNoDecimal */
    $('.number').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        else if ((charCode < 48) || (charCode > 57)) {
            return false;
        }
    });

    $('.int').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        else if ((charCode < 48) || (charCode > 57)) {
            return false;
        }
    });

    $('.numbersOnly').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        else if ((charCode != 46 || $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57)) {
            return false;
        }
    });


    /*money  - old name comCurrency*/
    $('.money').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode == 8) {
            return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            if (charCode != 46) {
                return false;
            }
        }

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var count = 0;

        var pattern = /^\d{0,8}\.?\d{0,2}$/;

        if (!pattern.test(myNum)) {
            count = 1;
        }
        if (count == 1) {
            return false;
        }
        else {
            if (myNum > 10000000) {
                return false;
            }
        }
        return true;
    });



    $('.percentage').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode == 8) {
            return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            if (charCode != 46) {
                return false;
            }
        }
        var count = 0;
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;


        var pattern = /^\d{0,3}\.?\d{0,2}$/;
        if (!pattern.test(myNum)) {
            count = 1;

        }
        if (count == 1) {
            return false;
        }
        else {
            if (myNum > 100) {
                return false;
            }
        }
        return true;
    });


    $('.Landline').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode == 8) {
            return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;


        if (myNum.length > 10) {
            return false;
        }

        return true;


    });

    $('.Telephone').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode == 8) {
            return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;


        if (myNum.length > 10) {
            return false;
        }

        return true;


    });

    $('.Residential').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode == 8) {
            return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;


        if (myNum.length > 10) {
            return false;
        }

        return true;


    });


    $('.fax').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode == 8) {
            return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length > 10) {
            return false;
        }
        return true;


    });

    $('.mobile').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode == 8) {
            return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length > 10) {
            return false;
        }
        return true;


    });


    $('.pin').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length > 6) {
            return false;
        }
        return true;


    });


    $('.panCard').live('keyup', function () {
        $(this).val(($(this).val()).toUpperCase());
    });

    $('.panCard').live('keypress', function (evt) {

        //        var pattern = /^[A-Z]{3}[G|A|F|C|T|H|P]{1}[A-Z]{1}\d{4}[A-Z]{1}$/;
        //  this.val(this.val().toUpperCase());

        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        if (charCode == 13)
            return false;



        var caretpos = $(this).caret().start;
        var valuestart = $(this).val().substring(0, caretpos);
        var valueend = $(this).val().substring(caretpos);
        var myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length <= 3) {
            var pattern = /^[a-zA-Z]{0,3}$/;
            if (!pattern.test(myNum)) {
                return false;
            }
        } else if (myNum.length == 4) {
            var pattern = /^[a-zA-Z]{3}[G|g|A|a|F|f|C|c|T|t|H|h|P|p]{1}$/;
            if (!pattern.test(myNum)) {
                return false;
            }
        } else if (myNum.length == 5) {
            var pattern = /^[a-zA-Z]{3}[G|g|A|a|F|f|C|c|T|t|H|h|P|p]{1}[a-zA-Z]{1}$/;
            if (!pattern.test(myNum)) {
                return false;
            }
        }
        else if (myNum.length <= 9) {
            var pattern = /^[a-zA-Z]{3}[G|g|A|a|F|f|C|c|T|t|H|h|P|p]{1}[a-zA-Z]{1}\d{0,4}$/;
            if (!pattern.test(myNum)) {
                return false;

            }
        }
        else {
            var pattern = /^[a-zA-Z]{3}[G|g|A|a|F|f|C|c|T|t|H|h|P|p]{1}[a-zA-Z]{1}\d{4}[a-zA-Z]{1}$/;
            if (!pattern.test(myNum)) {
                return false;

            }
        }



        return true;




    });


    $('.ValChar').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        else if (charCode > 31 && (charCode < 42 || charCode > 57) && charCode != 46) {

            if ((charCode == 34) || (charCode == 39) || (charCode == 93) || (charCode == 92) || (charCode == 124) || (charCode == 125)) {
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return false;
        }

    });


    $('.ValOnlyNumChar').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^[a-z0-9]+$/i;
        if (!pattern.test(myNum)) {
            return false;

        }
        return true;
        //  
    });

    $('.ValOnlyChar').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^[a-zA-Z]+$/;
        if (!pattern.test(myNum)) {
            return false;

        }


        return true;

    });


    $('.ValOnlyCharSingleSpace').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^([A-Za-z]+ ?)*$/;
        if (!pattern.test(myNum)) {
            return false;
        }


        return true;

    });

    $('.ValforTextArea').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if ((charCode == 39 || charCode == 34))
            return false;

        return true;

    });


    $('.email').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        if (charCode == 13) {

            return false;
        }

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;
        if (myNum.length > 50) {
            $(this).addClass('required');
            return false;

        }

        return true;
    });

    $('.url').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        if (charCode == 13) {
            return false;
        }

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;


        if (myNum.length > 250) {
            return false;
        }

        return true;
    });


    $('*[class*="AllowMax"]').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode


        var classname = $(this).attr('class');
        var MaxClass = '';
        var classes = classname.split(" ");
        for (var i = 0; i < classes.length; i++) {
            if (classes[i].indexOf("AllowMax") >= 0) {
                MaxClass = classes[i];
            }
        }

        if (MaxClass != '') {
            var vallength = MaxClass.substring(MaxClass.indexOf("x") + 1);

            var caretpos = $(this).caret().start;
            valuestart = $(this).val().substring(0, caretpos);
            valueend = $(this).val().substring(caretpos);
            myNum = valuestart + String.fromCharCode(charCode) + valueend;

            if (myNum.length > vallength) {
                return false;
            }
        }
        return true;

    });

    $('.UserName').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^[A-Za-z@.]*$/;
        if (!pattern.test(myNum)) {
            return false;

        }
        return true;
        //  
    });


    $('.Hours').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            if (charCode != 46) {
                return false;
            }
        }
        var count = 0;
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;


        var pattern = /^\d{0,3}\.?\d{0,2}$/;
        if (!pattern.test(myNum)) {
            count = 1;

        }
        if (count == 1) {
            return false;
        }
        else {
            if (myNum > 1000) {
                return false;
            }
        }
        return true;

    });


});
