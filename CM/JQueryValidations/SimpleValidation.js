﻿

$(document).ready(function () {

    //Caret Function for Validation 

    (function (k, e, i, j) {
        k.fn.caret = function (b, l) {
            var a, c, f = this[0], d = k.browser.msie; if (typeof b === "object" && typeof b.start === "number" && typeof b.end === "number") { a = b.start; c = b.end } else if (typeof b === "number" && typeof l === "number") { a = b; c = l } else if (typeof b === "string") if ((a = f.value.indexOf(b)) > -1) c = a + b[e]; else a = null; else if (Object.prototype.toString.call(b) === "[object RegExp]") { b = b.exec(f.value); if (b != null) { a = b.index; c = a + b[0][e] } } if (typeof a != "undefined") {
                if (d) {
                    d = this[0].createTextRange(); d.collapse(true);
                    d.moveStart("character", a); d.moveEnd("character", c - a); d.select()
                } else { this[0].selectionStart = a; this[0].selectionEnd = c } this[0].focus(); return this
            } else {
                if (d) { c = document.selection; if (this[0].tagName.toLowerCase() != "textarea") { d = this.val(); a = c[i]()[j](); a.moveEnd("character", d[e]); var g = a.text == "" ? d[e] : d.lastIndexOf(a.text); a = c[i]()[j](); a.moveStart("character", -d[e]); var h = a.text[e] } else { a = c[i](); c = a[j](); c.moveToElementText(this[0]); c.setEndPoint("EndToEnd", a); g = c.text[e] - a.text[e]; h = g + a.text[e] } } else {
                    g =
    f.selectionStart; h = f.selectionEnd
                } a = f.value.substring(g, h); return { start: g, end: h, text: a, replace: function (m) { return f.value.substring(0, g) + m + f.value.substring(h, f.value[e]) } }
            }
        }
    })(jQuery, "length", "createRange", "duplicate");


    $(".mws-datepicker").datepicker({ showOn: "both",
        buttonImage: "../Styles/css/icons/16/calendar_1.png",
        buttonImageOnly: true,
        prevText: 'Previous',
        yearRange: 'c-50:c+50',
        changeMonth: true,
        buttonText: '',
        onClose: function (dateText, inst) {
            $(this).focus();
        },
        changeYear: true
    }).attr('placeholder', 'dd-Mmm-yyyy').blur(function () { return RemoveErrorClass($(this)); });


    $(".mws-datepicker-MinMax1Month").datepicker({ showOn: "both",
        buttonImage: "../Styles/css/icons/16/calendar_1.png",
        buttonImageOnly: true,
        prevText: 'Previous',
        changeMonth: true,
        buttonText: '',
        changeYear: false,
        onClose: function (dateText, inst) {
            $(this).focus();
        },
        maxDate: "+1M",
        minDate: "-1M"
    }).attr('placeholder', 'dd-Mmm-yyyy').blur(function () { return RemoveErrorClass($(this)); });


    $(".mws-datepicker-Max1Month").datepicker({ showOn: "both",
        buttonImage: "../Styles/css/icons/16/calendar_1.png",
        buttonImageOnly: true,
        prevText: 'Previous',
        changeMonth: true,
        buttonText: '',
        onClose: function (dateText, inst) {
            $(this).focus();
        },
        changeYear: false,
        maxDate: "+1M",
        minDate: "0"
    }).attr('placeholder', 'dd-Mmm-yyyy').blur(function () { return RemoveErrorClass($(this)); });


    $(".mws-datepicker-MinToday").datepicker({ showOtherMonths: true, showOn: "both",
        changeMonth: true,
        changeYear: true,
        buttonImage: "../Styles/css/icons/16/calendar_1.png",
        maxDate: "0",
        yearRange: 'c-50:c+50',
        buttonText: '',
        onClose: function (dateText, inst) {
            $(this).focus();
        },
        prevText: 'Previous',
        buttonImageOnly: true,
        dateFormat: 'dd-MM-yy'
    }).attr('placeholder', 'dd-MM-yy').blur(function () { return RemoveErrorClass($(this)); });



    $(".mws-datepicker-wk").datepicker({ showWeek: true, showOn: "both",
        buttonImage: "../Styles/css/icons/16/calendar_1.png",
        changeMonth: true,
        prevText: 'Previous',
        changeYear: true,
        buttonText: '',
        onClose: function (dateText, inst) {
            $(this).focus();
        },
        buttonImageOnly: true,
        dateFormat: 'dd-MM-yy'
    }).blur(function () { return RemoveErrorClass($(this)); });



    $(".mws-datepicker-mm").datepicker({ numberOfMonths: 3, showOn: "both",
        buttonImage: "../Styles/css/icons/16/calendar_1.png",
        changeMonth: true,
        prevText: 'Previous',
        changeYear: true,
        buttonText: '',
        onClose: function (dateText, inst) {
            $(this).focus();
        },
        buttonImageOnly: true,
        dateFormat: 'dd-MM-yy'
    }).blur(function () { return RemoveErrorClass($(this)); });




    $(".mws-dtpicker").datetimepicker({ showOn: "both",
        buttonImage: "../Styles/css/icons/16/calendar_1.png",
        changeMonth: true,
        prevText: 'Previous',
        changeYear: true,
        onClose: function (dateText, inst) {
            $(this).focus();
        },
        buttonText: '',
        buttonImageOnly: true,
        dateFormat: 'dd-MM-yy'
    }).attr('placeholder', 'dd-Mmm-yyyy hh:mm').blur(function () { return RemoveErrorClass($(this)); });




    $(".mws-dtpicker-CurrentDate").datetimepicker({ showOn: "both",
        buttonImage: "../Styles/css/icons/16/calendar_1.png",
        changeMonth: true,
        prevText: 'Previous',
        changeYear: true,
        minDate: "0",
        onClose: function (dateText, inst) {
            $(this).focus();
        },
        buttonText: '',
        buttonImageOnly: true,
        dateFormat: 'dd-MM-yy'
    }).attr('placeholder', 'dd-MM-yy hh:mm').blur(function () { return RemoveErrorClass($(this)); });



    $(".mws-themerdtpicker").datetimepicker({ showOn: "both",
        buttonImage: "../Styles/css/icons/16/calendar_1.png",
        changeMonth: true,
        prevText: 'Previous',
        changeYear: true,
        minDate: "0",
        buttonText: '',
        onClose: function (dateText, inst) {
            $(this).focus();
        },
        buttonImageOnly: true,
        beforeShow: function (input, inst) {
            // Handle calendar position before showing it.
            // It's not supported by Datepicker itself (for now) so we need to use its internal variables.
            var calendar = inst.dpDiv;
            var offset = $(input).offset();
            var height = $(input).height();

            setTimeout(function () {
                calendar.css({ top: 100 + 'px' })
            }, 1);
        }
    }).attr('placeholder', 'dd-mmm-yyyy hh:mm');


    //Mahesh

    $(".mws-datepicker-MMM-yyyy").datepicker({ showOtherMonths: true, showOn: "both",
        buttonImage: "../Styles/css/icons/16/calendar_1.png",
        buttonImageOnly: true,
        changeMonth: true,
        buttonText: '',
        CausesValidation: false,
        yearRange: 'c-70:c+70',
        changeYear: true,
        showButtonPanel: true,
        maxDate: "0",
        onClose: function (dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).val(new Date(year, month, 1).format("MMM-yyyy"));
        }
    }).attr('placeholder', 'MMM-yyyy').blur(function () { return RemoveErrorClass($(this)); });

    $(".mws-datepicker-MMM-yyyy").focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });
        $(".ui-datepicker-buttonpane").find(".ui-datepicker-current").remove();


    });



    $(".mws-datepicker-MMM-yyyy").blur(function () { return RemoveErrorClass($(this)); });


    $(".mws-timepicker").timepicker({ showOn: "both",
        buttonImage: "../Styles/css/icons/16/clock.png",
        onClose: function (time, inst) {
            $(this).focus();
        },
        buttonText: '',
        buttonImageOnly: true
    })
    .attr('placeholder', 'hh:mm').blur(function () {
        return RemoveErrorClass($(this));
    });

    $(".mws-slider").slider({ range: "min" });

    $(".mws-progressbar").progressbar({ value: 90 });

    $(".mws-range-slider").slider({ range: true, min: 0, max: 500, values: [75, 300] });


    $('.Currency').live('keypress', function (evt) {
        debugger;
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        if (charCode == 13)
            return false;

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            if (charCode != 46) {
                return false;
            }
        }

        var count = 0;
        var myNum = this.value + String.fromCharCode(charCode);
        //  var pattern = /^\d{0,8}$/;
        var pattern = /^\d{0,18}\.?\d{0,2}$/;
        // var pattern = /^\d{0,8}\.?\d{0,0}$/;
        if (!pattern.test(myNum)) {

            count = 1;
        }
        if (count == 1) {
            return false;
        }
        else {
            if (myNum > 99999999) {
                return false;
            }

        }
        return true;
    });


    $('input[type="checkbox"]').change(function (event) {
        $(this).closest('.checkreq').find('tbody').next('label').remove();

        if ($(this).closest('.checkreq').find('tbody tr td').find('input:checkbox:checked').length == 0) {
            $(this).closest('.checkreq').append('<label for="checkre" generated="true" class="error" style="">This field is required.</label>')
        }
        else {
            $(this).closest('.checkreq').find('tbody').next('label').remove();
        }

    });

    $('select').each(function () {
        if ($(this).attr('multiple') == 'multiple') {
            $(this).find('option[value="0"]').remove();
        }

    });

    $('select.required').change(function () {

        if ($(this).val() == "0") {
            $(this).next('div').append('<label for="checkre" generated="true" class="error" style="">This field is required.</label>');
            $('.error').css('font-weight', 'normal');
        }
        else {

            $(this).next('div').find('label').remove()
        }

    });





    $('input[type="radio"]').change(function (event) {
        $(this).closest('.checkreq').find('tbody').next('label').remove();

        if ($(this).closest('.checkreq').find('tbody tr td').find('input:radio:checked').length == 0) {
            $(this).closest('.checkreq').append('<label for="checkre" generated="true" class="error" style="">This field is required.</label>')
        }
        else {
            $(this).closest('.checkreq').find('tbody').next('label').remove();
        }



    });




    $('.number').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        else if ((charCode < 48) || (charCode > 57)) {
            return false;
        }
    });
    $('.int').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        else if ((charCode < 48) || (charCode > 57)) {
            return false;
        }

    });

    $('.int').live('paste', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        $obj = $(this);
        setTimeout(function () {
            myNum = $obj.val();

            var pattern = /^[0-9]+$/;

            if (!pattern.test(myNum)) {
                myNum = myNum.replace(/[^0-9]/g, '');
                $obj.val(myNum);
                return false;
            }
        }, 0);
    });

    $('.numbersOnly').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        else if ((charCode != 46 || $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57)) {
            return false;
        }
    });


    //Number Validation Ends Here




    //Percentage Validation 

    $('.percentagePrecisionOne').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode == 8) {
            return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            if (charCode != 46) {
                return false;
            }
        }
        var count = 0;
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;


        var pattern = /^\d{0,3}\.?\d{0,1}$/;
        if (!pattern.test(myNum)) {
            count = 1;

        }
        if (count == 1) {
            return false;
        }
        else {
            if (myNum > 100) {
                return false;
            }
        }
        return true;
    });
    $('.percentage').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode == 8) {
            return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            if (charCode != 46) {
                return false;
            }
        }
        var count = 0;
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;


        var pattern = /^\d{0,3}\.?\d{0,2}$/;
        if (!pattern.test(myNum)) {
            count = 1;

        }
        if (count == 1) {
            return false;
        }
        else {
            if (myNum > 100) {
                return false;
            }
        }
        return true;
    });


    //Percentage Validation Ends Here



    //Contact Number and Address validation

    $('.Landline').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode == 8) {
            return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length > 10) {
            return false;
        }
        return true;

    });

    //Added by Nilesh - Landline field should not allow 0
    $('.Landline').live('blur', function () {
        if ($(this).val() != "") {
            var amt = parseInt($(this).val());
            if (amt <= 0) {
                $(this).val('');
            }
        }
    });

    //Added by Nilesh - Landline field should not allow 0
    $('.mobile').live('blur', function () {
        if ($(this).val() != "") {
            var amt = parseInt($(this).val());
            if (amt <= 0) {
                $(this).val('');
            }
        }
    });

    //Added by Nilesh - Landline field should not allow 0
    $('.fax').live('blur', function () {
        if ($(this).val() != "") {
            var amt = parseInt($(this).val());
            if (amt <= 0) {
                $(this).val('');
            }
        }
    });

    $('.Contact').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode == 8) {
            return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length > 10) {
            return false;
        }
        return true;
    });

    $('.Telephone').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode == 8) {
            return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length > 10) {
            return false;
        }
        return true;

    });

    $('.Residential').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode == 8) {
            return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length > 10) {
            return false;
        }
        return true;


    });


    $('.fax').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length > 10) {
            return false;
        }
        return true;

    });

    $('.mobile').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode == 8) {
            return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length > 10) {
            return false;
        }
        return true;


    });


    $('.pin').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length > 6) {
            return false;
        }
        return true;


    });

    $('.pin').live('keyup', function (evt) {
        $(this).parent().find('label.pincodeerror').remove();
    });

    $('.pin').live('paste', function (evt) {

        setTimeout(function () {

            var num = $('.pin').val().match(/\d/g);
            if (num != null) {
                num = num.join("");
                num = num.substring(0, 6);
                $('.pin').val(num);
            } else {
                $('.pin').val('');
            }
            return false;

        }, 0);
    })




    $('.email').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        if (charCode == 13) {
            return false;
        }

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;


        if (myNum.length > 50) {
            return false;
        }

        return true;
    });
    //Contact Number and address validation


    //Varchar Fields validation

    $('.ValChar').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        else if (charCode > 31 && (charCode < 42 || charCode > 57) && charCode != 46) {

            if ((charCode == 34) || (charCode == 39) || (charCode == 93) || (charCode == 92) || (charCode == 124) || (charCode == 125)) {
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return false;
        }

    });


    $('.ValOnlyNumChar').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^[a-z0-9]+$/i;
        if (!pattern.test(myNum)) {
            return false;

        }
        return true;
        //  
    });


    $('.ValOnlyChar').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^[a-zA-Z]+$/;
        if (!pattern.test(myNum)) {
            return false;

        }


        return true;

    });


    $('.ValOnlyCharSingleSpace').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^([A-Za-z]+ ?)*$/;
        if (!pattern.test(myNum)) {
            return false;
        }


        return true;

    });

    $('.ValforTextArea').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if ((charCode == 39 || charCode == 34))
            return false;

        return true;

    });






    $(".required").bind("paste", function () {
        var Value = $(this).val().trim();
        var Class = $(this).attr('class');
        if (Value != '' || Value != null || Value != undefined) {

            $(this).addClass('' + Class + ' invalid');

        }
        else {

            $(this).addClass('' + Class + ' valid');

        }

    });


    $('select.required').live('change', function () {

        var $group = $(this).attr('group');
        var isValid = true;

        if (!$(this).valid())
            isValid = false;


        $('select').each(function () {
            var DetachHtml = $(this).next('label').detach();
            $(this).next('div').after(DetachHtml);

        });

    });


    //Button save validation



    $('.btn_validate').on('click', function () {

        var isValidS = true;
        $('.checkreq').each(function () {
            $(this).find('tbody').next('label').remove();

            if ($(this).find('tbody tr td').find('input:checkbox:checked').length == 0) {

                $(this).append('<label for="checkre" generated="true" class="error" style="">This field is required.</label>')
                isValidS = false;

            }
            else {
                $(this).find('tbody').next('label').remove();
                isValidS = true;
            }

        });



        $('.checkreq').each(function () {
            $(this).find('tbody').next('label').remove();

            if ($(this).find('tbody tr td').find('input:radio:checked').length == 0) {

                $(this).append('<label for="checkre" generated="true" class="error" style="">This field is required.</label>');
                isValidS = false;
                //return 
            }
            else {
                $(this).find('tbody').next('label').remove();
                isValidS = true;
            }

        });


        var $group = $('#tblContainer');

        $group.find('.required').each(function (i, item) {
            if (!$(item).is('span')) {
                if (!$(item).valid())
                    isValidS = false;

            }
        });

        $('select.required').each(function () {
            //  $(this).next('div').next('label').remove();

            if ($(this).val() == null || $(this).val() == '0') {
                if ($(this).attr('multiple') == undefined) {
                    $(this).next('div').find('label').remove();
                }
                else {
                    $(this).next('div').next('label').remove();
                }

                $(this).next('div').append('<label for="checkre" generated="true" class="error" style="">This field is required.</label>');
                $('.error').css('font-weight', 'normal');
                isValidS = false;
                //return;
            }

        });
        $('select').each(function () {
            if ($(this).attr('multiple') == undefined) {
                $(this).next('div').next('label').remove();
            }
            else {
                var DetachHtml = $(this).next('label').detach();
                $(this).next('div').after(DetachHtml);
            }

        });

        $('.error').css('font-weight', 'normal');

        if (isValidS != true) {

            return false;
        }



    });






});