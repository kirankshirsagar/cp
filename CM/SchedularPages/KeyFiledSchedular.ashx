﻿<%@ WebHandler Language="C#" Class="KeyFiledSchedular" %>

using System;
using System.Web;
using MasterBLL;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Data;
using System.Text;
using System.Web.Security;
using MailBee;
using MailBee.Mime;
using MailBee.SmtpMail;
using System.Data.SqlClient;
using WorkflowBLL;
using EmailBLL;

public class KeyFiledSchedular : IHttpHandler
{
    IKeyFields objkey;
    public void ProcessRequest(HttpContext context)
    {
        try
        {
            objkey = FactoryWorkflow.GetKeyFieldsDetails();
            objkey.Param = 1;
            objkey.ReminderDate = DateTime.Now;
            List<KeyFeilds> AssignUserlist = new List<KeyFeilds>();
            AssignUserlist = objkey.ReadActivityReminderData();
            int AssignUserID;
            string Assignee = string.Empty;
            string Email=string.Empty;
            if (AssignUserlist.Count > 0)
            {
                foreach (var item in AssignUserlist)
                {
                    AssignUserID = Convert.ToInt32(item.AssignToId);
                    Assignee = (item.AssignUser);
                    Email=(item.Email);
                    SendMail(AssignUserID, Assignee,Email);
                }
            }

            
            objkey.URLForWindowsService = HttpContext.Current.Request.Url.AbsoluteUri;
            objkey.UpdateURLLastRunTimeForWindowsService();
        }
            
        catch { }
    }

    public void SendMail(int AssignUserID, string Assignee,string Email)
    {
        try
        {
            objkey = FactoryWorkflow.GetKeyFieldsDetails();
            objkey.Param = 2;
            objkey.AssignToId = AssignUserID;
            objkey.ReminderDate = DateTime.Now;
            List<KeyFeilds> ActivityList = new List<KeyFeilds>();
            ActivityList = objkey.ReadActivityReminderData();
            StringBuilder sb = new StringBuilder();
            if (ActivityList.Count > 0)
            {
                sb.AppendFormat(" <p style='margin: 0in 0in 0pt 0pt; text-align: left;font-size:10pt;font-family:Calibri;color:Navy; '>Dear " + Assignee + ",</p>");

                sb.AppendFormat(" <br />");
                sb.Append("<table WIDTH='100%' cellspacing='0' cellpadding='1' border='1' style='border-color:lightyellow'>");
                sb.Append("<tr>");
                sb.Append("<td WIDTH='100%' colspan='6' style='border-color:lightyellow'>");
                sb.AppendFormat("<p style='margin: 0in 0in 0pt 20pt;font-weight:bold;text-align:center;font-size:10pt;font-family:Calibri;color:black;'>Activity Reminder Mail </p>");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr >");
                sb.Append("<td WIDTH='10%' style='border-color:lightyellow'>");
                sb.AppendFormat("<p style='margin: 0in 0in 0pt 0pt; text-align: left;font-weight:bold;font-size:10pt;font-family:Calibri;color:black;'>Activity Date</p>");
                sb.Append("</td>");
                sb.Append("<td WIDTH='15%' style='border-color:lightyellow'>");
                sb.AppendFormat("<p style='margin: 0in 0in 0pt 0pt; text-align: left;font-weight:bold;font-size:10pt;font-family:Calibri;color:black;'>Activity Type </p>");
                sb.Append("</td>");
                sb.Append("<td WIDTH='10%' style='border-color:lightyellow'>");
                sb.AppendFormat("<p style='margin: 0in 0in 0pt 0pt; text-align: left;font-weight:bold;font-size:10pt;font-family:Calibri;color:black;'>Status </p>");
                sb.Append("</td>");
                sb.Append("<td WIDTH='35%' style='border-color:lightyellow'>");
                sb.AppendFormat("<p style='margin: 0in 0in 0pt 0pt; text-align: left;font-weight:bold;font-size:10pt;font-family:Calibri;color:black;'>Remarks </p>");
                sb.Append("</td>");
                sb.Append("<td WIDTH='20%' style='border-color:lightyellow'>");
                sb.AppendFormat("<p style='margin: 0in 0in 0pt 0pt; text-align: left;font-weight:bold;font-size:10pt;font-family:Calibri;color:black;'>Added by </p>");
                sb.Append("</td>");
                sb.Append("<td WIDTH='10%' style='border-color:lightyellow'>");
                sb.AppendFormat("<p style='margin: 0in 0in 0pt 0pt; text-align: left;font-weight:bold;font-size:10pt;font-family:Calibri;color:black;'>Reminder Date </p>");
                sb.Append("</td>");

                sb.Append("</tr>");

                foreach (var item in ActivityList)
                {
                    sb.Append("<tr>");
                    sb.Append("<td WIDTH='10%' style='border-color:lightyellow'>");
                    sb.AppendFormat("<p style='margin: 0in 0in 0pt 0pt; text-align: left;font-size:10pt;color:black;'>" + item.ActActivityDate + " </p>");
                    sb.Append("</td>");
                    sb.Append("<td WIDTH='15%' style='border-color:lightyellow'>");
                    sb.AppendFormat("<p style='margin: 0in 0in 0pt 0pt; text-align: left;font-size:10pt;color:black;'>" + item.ActivityTypeName + " </p>");
                    sb.Append("</td>");
                    sb.Append("<td WIDTH='10%' style='border-color:lightyellow'>");
                    sb.AppendFormat("<p style='margin: 0in 0in 0pt 0pt; text-align: left;font-size:10pt;color:black;'>" + item.StatusName + " </p>");
                    sb.Append("</td>");
                    sb.Append("<td WIDTH='35%' style='border-color:lightyellow'>");
                    sb.AppendFormat("<p style='margin: 0in 0in 0pt 0pt; text-align: left;font-size:10pt;color:black;'>" + item.Description + " </p>");
                    sb.Append("</td>");
                    sb.Append("<td WIDTH='20%' style='border-color:lightyellow'>");
                    sb.AppendFormat("<p style='margin: 0in 0in 0pt 0pt; text-align: left;font-size:10pt;color:black;'>" + item.User + " </p>");
                    sb.Append("</td>");
                    sb.Append("<td WIDTH='10%' style='border-color:lightyellow'>");
                    sb.AppendFormat("<p style='margin: 0in 0in 0pt 0pt; text-align: left;font-size:10pt;color:black;'>" + item.ActReminderDate + " </p>");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                }

                sb.Append("</table>");
                sb.AppendFormat(" <br />");
                sb.AppendFormat("<p style='margin: 0in 0in 0pt 0pt; text-align: left;font-size:10pt;font-family:Calibri;color:Navy;'>Regards, </p>");
                sb.AppendFormat("<p style='margin: 0in 0in 0pt 0pt; text-align: left;font-size:10pt;font-family:Calibri;color:Navy;'>Contract Management Team</p>");
                sb.AppendFormat(" <br /><br /><br />");
                sb.AppendFormat(" <p style='margin: 0in 0in 0pt 0pt; text-align: center;font-size:10pt;font-family:Calibri;color:Navy; '>********** This is a system generated mail. Kindly do not reply. ********** </p>");
                string Body = sb.ToString();
                // Page.TraceWrite("Activity reminder send emails.");
                SendMail sm = new SendMail();
                sm.MailFrom = System.Configuration.ConfigurationManager.AppSettings["MailFrom"];
                sm.Subject = "Activity Reminder";
                sm.Body = Body;
               // sm.MailTo =Email;//
                sm.MailTo = "dchoudhari@practiceleague.com";//
                sm.SendSimpleMail();
            }
        }
        catch { }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}