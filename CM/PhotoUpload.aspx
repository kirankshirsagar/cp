﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PhotoUpload.aspx.cs" Inherits="PhotoUpload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Upload Photo</title>

     <link href="Styles/application.css" media="all" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="Scripts/jquery.js"></script>
    <link href="Styles/jquery.Jcrop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="Scripts/jquery.Jcrop.js"></script>
    <script type="text/javascript">
        function UploadSucessful(str) {
            alert('Profile picture added successfully.');
            window.open('Masters/Register.aspx', '_self', '');
          
        }
        function ShowErrorMessage() {
            alert('Image file could not be uploaded.Image size should be less than 1024 X 768');
        }
        jQuery(document).ready(function () {
            $('#imgCrop').Jcrop({
                setSelect: [0, 0, 500, 700],
                aspectRatio: 1,
                boxWidth: 500,
                boxHeight: 500,
                minSize: [109, 109],
                onSelect: storeCoords
            });
        });
        function storeCoords(c) {
            $('#X').val(c.x);
            $('#Y').val(c.y);
            $('#W').val(c.w);
            $('#H').val(c.h);
        }

        function CloseForm(obj) {

            window.opener.setValue(obj);
            window.close();

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="X" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="Y" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="W" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="H" ClientIDMode="Static" runat="server" />
    <center>
        <br />
        <br />
        <div style="border-style: dashed; border-color: Black; border-width: thin; width: auto;
            height: auto">
            <span id="Noimagespan" runat="server">No photo uploaded</span>
            <img src="" alt="" class="" id="userimg" clientidmode="Static" runat="server" visible="false" />
            <asp:Image ID="imgCrop" Style="position: relative" ClientIDMode="Static" runat="server"
                Visible="false" />
        </div>
        <asp:FileUpload ID="Upload" BorderStyle="None" BackColor="Window" CssClass="docfile red"
            ClientIDMode="Static" Style="position: relative; margin-left: -70px; margin-top: 10px;
            width: 81px; height: 23px; text-decoration: underline" runat="server" />&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnCrop" Style="position: absolute; margin-left: 30px; margin-top: 10px;
            width: 80px" class="mws-button red" OnClick="btnCrop_Click" runat="server" Text="Save"
            Visible="false" />
    </center>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnCrop" />
            <asp:PostBackTrigger ControlID="btnUpload" />
        </Triggers>
        <ContentTemplate>
            <br />
            <asp:Panel ID="pnlUpload" runat="server">
                <asp:Button ID="btnUpload" OnClick="btnUpload_Click" Style="margin-left: 50px; display: none"
                    class="mws-button red" runat="server" Text="Crop" />
                <center>
                    <span style="color: Red">
                        <asp:Label ID="lblError" runat="server" Visible="false" /></span></center>
            </asp:Panel>
            <br />
            <asp:Panel ID="pnlCrop" runat="server" Visible="true">
                <asp:HiddenField ID="hdnIsImageCropped" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="hdnHeight" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="hdnWidth" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="hdnUid" ClientIDMode="Static" runat="server" />
                <input id="hdnSearchStr" type="hidden" value='' runat="server" clientidmode="Static" />
                <input id="Hidden1" type="hidden" runat="server" clientidmode="Static" />
                <label id="imgMessage" clientidmode="static" style="visibility: hidden;">
                    <b></b>
                </label>
                <br>
            </asp:Panel>
            <br />
            <asp:Panel ID="pnlCropped" runat="server" Visible="false" Style="width: auto; height: auto">
                <asp:Image ID="imgCropped" ClientIDMode="Static" runat="server" Visible="false" />
            </asp:Panel>
            <div>
                
                <center>
                    <span style="color: Red">Note : Following file extensions are allowed: .jpeg , .jpg , .png
                        , .gif , .bmp. and photo size should be less than 1 Mb.</span> </span>
                </center>
                
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
