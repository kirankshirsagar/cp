﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true" CodeFile="OutlookCalendarSynch.aspx.cs" Inherits="Calendar_OutlookCalendarSynch" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
 <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <link href="../CalendarCSS/jquery-ui-1.8.21.css" media="all" rel="stylesheet" type="text/css">
    <script src="../CalendarCSS/jquery-1.7.2-ui-1.8.21-ujs-2.0.3.js" type="text/javascript"></script>
    <link href="../JQueryValidations/dhtmlwindow.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/dhtmlwindow.js" type="text/javascript"></script>
    <link href="../JQueryValidations/modal.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/modal.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<div id="rightlinks" style="display: none;">
            <uc1:Masterlinks ID="rightactions" runat="server" />
        </div>
        <script type="text/javascript">
            $("#sidebar").html($("#rightlinks").html());
        </script>


    <div style="font-size:14px; font-weight:bold;color:black;" > 
          <h2>  Outlook Calendar Sync </h2> 
          <a href="Calendar.aspx">Back to Calendar</a>.
          To configure calendar with Outlook on Mac <a href="MACOutlookCalendarSynch.aspx">click here</a>.
<ol>
<br /><br />
            <li>	Open MS Outlook and click on Calendar
            <br />
                <img src="../Images/OutlookSynch/1.png" />
<br /><br />           </li>
             <li>	Go to My Calendars => Add Calendar => From Internet…
              <br />
                <img src="../Images/OutlookSynch/2.png" />
<br /><br /></li>
             <li>	Copy below url and click OK.
            <br />
                <u><asp:Label ID="lblOutlookUrl" runat="server" Text="" style="color:blue"></asp:Label></u>      
            <br /><br />
            <img src="../Images/OutlookSynch/3.png" />
<br /><br /></li>
            <li>	Click on Yes<br />
        <img src="../Images/OutlookSynch/4.png" />
 <br /><br /></li>
             <li>	Your ContractPod calendar gets synched and the folder name will be displayed under My Calendars. You can rename the folder name as you want.
               =>  <br />
        <img src="../Images/OutlookSynch/5.png" /><img src="../Images/OutlookSynch/6.png" />
<br /><br /></li>
             <li>	The activities will be shown by their Type name<br />
        <img src="../Images/OutlookSynch/7.png" />
<br /><br /></li>
             <li>	The details can be viewed by simply double clicking it. 
<br /><br /></li>          
           <span style="color:Red;"> NOTE : The activity cannot be updated and deleted from Outlook.</span>
<br /><br />
</ol>     
    </div>

    <br /><br />
</asp:Content>

