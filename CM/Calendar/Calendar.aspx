﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="Calendar.aspx.cs" Inherits="Calendar" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <link href="../CalendarCSS/jquery-ui-1.8.21.css" media="all" rel="stylesheet" type="text/css">
    <script src="../CalendarCSS/jquery-1.7.2-ui-1.8.21-ujs-2.0.3.js" type="text/javascript"></script>
    <link href="../JQueryValidations/dhtmlwindow.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/dhtmlwindow.js" type="text/javascript"></script>
    <link href="../JQueryValidations/modal.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/modal.js" type="text/javascript"></script>
    <script type="text/javascript">
        var datepickerOptions = { dateFormat: 'yy-mm-dd', firstDay: 0, showOn: 'button', buttonImageOnly: true, buttonImage: '/redmine/images/calendar.png?1349001717', showButtonPanel: true };
    </script>
    <script type="text/javascript">

        // these are labels for the days of the week
        cal_days_labels = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

        // these are human-readable month name labels, in order
        cal_months_labels = ['January', 'February', 'March', 'April',
                     'May', 'June', 'July', 'August', 'September',
                     'October', 'November', 'December'];

        // these are the days of the week for each month, in order
        cal_days_in_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        // this is the current date
        cal_current_date = new Date();

        function Calendar(month, year) {
            this.month = (isNaN(month) || month == null || month == undefined) ? cal_current_date.getMonth() : month;
            this.year = (isNaN(year) || year == null || year == undefined) ? cal_current_date.getFullYear() : year;
            this.html = '';
        }

        function getFirstDay(month, year) {
            //  alert(month + '--' + year);
            var date = new Date(month.toString() + '/' + '1/' + year.toString());
            //  date.setFullYear(14, 0, 2); // 1 January, 14
            y = date.getFullYear(), m = date.getMonth();
            var firstDay = new Date(y, m, 1);
            var lastDay = new Date(y, m + 1, 0);
            return firstDay.getDay();
        }

        Calendar.prototype.generateHTML = function () {

            var firstDay = getFirstDay($('#month').val(), $('#year').val());
            var startingDay = firstDay;

            var monthLength = cal_days_in_month[parseInt($('#month').val()) - 1];

            if (parseInt($('#month').val()) == 2) { // February only!
                if ((this.year % 4 == 0 && this.year % 100 != 0) || this.year % 400 == 0) {
                    monthLength = 29;
                }
                else {
                    monthLength = 28;
                }
            }

            var monthName = cal_months_labels[this.month]
            var html = '<body class="controller-calendars action-show"><table class="cal">';
            html += '<thead><tr>';

            for (var i = 0; i <= 6; i++) {
                html += '<th scope="col">';
                html += cal_days_labels[i];
                html += '</th>';
            }
            html += '</tr></thead>';

            var day = 1;

            for (var i = 0; i < 6; i++) {
                for (var j = 0; j <= 6; j++) {
                    html += '<td class="odd"><p class="day-num"><a title="Add activity" href="#" target="name" onclick="opennewsletter(this); return false">';
                    if (day <= monthLength && (i > 0 || j >= startingDay)) {

                        var MonthValue = $('#month').val().length == 1 ? '0' + $('#month').val() : $('#month').val();
                        var DateValueTemp = day.toString().length == 1 ? '0' + day : day;
                        //   var DS = new Date($('#year').val(), MonthValue, DateValueTemp);

                        var DateValue = DateValueTemp + '/' + MonthValue + '/' + $('#year').val();
                        html += "<font size='4px'>" + day + "</font>&nbsp;<img title='Add activity'  style='margin-right:0px' src='../Images/addbig.png'></img>";
                        html += '<input type="hidden" id="hdnDateVal" value=' + DateValue + '>';

                        day++;
                    }
                    html += '</p></a></td>';
                }
                if (day > monthLength) {
                    break;
                } else {
                    html += '</tr><tr>';
                }
            }
            html += '</tr></table></div></body>';

            this.html = html;

            if ($('#hdnisAdd').val() == "N") {
                $('p.day-num').find('a[target="name"]').attr('onclick', 'return false').addClass('areadOnly');
                $('p.day-num').find('img[title="Add activity"]').attr('onclick', 'return false');
            }

            if ($('#hdnIsUpdate').val() == "N") {
                $('p.day-num').find('a.issue').attr('onclick', 'return false').addClass('areadOnly');
            }

            if ($('#hdnIsDelete').val() == "N") {
                $('img[id="CloseUploaderImg"]').css('display', 'none');
            }
        }

        Calendar.prototype.getHTML = function () {
            return this.html;
        }

        function DeleteActivity(ActivityId, obj) {

            var fpath = $(obj).attr('filepath');
            var IsOCR = $(obj).attr('isocr');

            if (confirm('Do you want to delete this activity ?')) {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Calendar.aspx/DeleteActivity",
                    data: "{ActivityId:'" + ActivityId + "',FileUpload:'" + fpath + "',IsOCR:'" + IsOCR + "'}",
                    dataType: "json",
                    success: function (output) {

                        $('#dvMesssage').html('Activity deleted Successfully').show();
                        $(obj).closest('div').remove();
                        setTimeout(function () {
                            $('#dvMesssage').hide();
                            // location.reload(true);
                        }, 2000);
                    }
                });
            }
        }

        function BindActivities() {
            debugger;
            $.ajax({
                type: 'POST',
                contentType: "text/html; charset=utf-8",
                url: "../Handlers/BindActivities.ashx?Month=" + $('#month').val() + "&Year=" + $('#year').val() + "&UserId=" + $('#hdnUserId').val(),
                data: {},
                datatype: "html",
                async: false,
                success: function (response) {

                    $('#MyDiv').html('');
                    $('#MyDiv').html(response);

                    $('.cal').find('tbody tr').find('td').each(function () {
                        var obj = $(this);
                        var pp = $(this).find('input[id="hdnDateVal"]').val();

                        $('#MyDiv').find('table tbody tr').find('td:eq(4)').each(function () {

                            var activityText = $(this).prev('td').html();
                            var IsOnCustomerPortal = $(this).parent('tr').find('td:eq(13)').html() == 'Y' ? 'Yes' : 'No';
                            var ReminderDate = $(this).parent('tr').find('td:eq(14)').html() == '01-Jan-0001 12:00 AM' ? '' : $(this).parent('tr').find('td:eq(14)').html();
                            var ActivityType = $(this).parent('tr').find('td:eq(2)').html();
                            var ActivityId = $(this).parent('tr').find('td:eq(0)').html();
                            var Status = $(this).parent('tr').find('td:eq(11)').html();
                            var AddedByName = $(this).parent('tr').find('td:eq(7)').html();
                            var ActivityDateDefaultFormat = $(this).parent('tr').find('td:eq(6)').html();
                            var ReminderDateText = $(this).parent('tr').find('td:eq(7)').html();
                            var ContractIdtext = $(this).parent('tr').find('td:eq(15)').html();
                            var isEditable = $(this).parent('tr').find('td:eq(16)').html();
                            var FileUpload = $(this).parent('tr').find('td:eq(17)').html();
                            var ReqID = $(this).parent('tr').find('td:eq(18)').html();
                            var ContractTypeId = $(this).parent('tr').find('td:eq(19)').html();
                            var IsOCR = $(this).parent('tr').find('td:eq(20)').html();
                            var ContractTypeIds = $(this).parent('tr').find('td:eq(21)').html();
                            //alert(ContractTypeIds);
                            var strDate = $(this).html().split('/');
                            var CloseClass = ""
                            //   alert(strDate[2] + '-' + strDate[1] + '-' + strDate[0]);
                            var DS = new Date(strDate[2], strDate[1], strDate[0]);
                            var DateValue = strDate[0] + '/' + strDate[1] + '/' + strDate[2];


                            if (pp == DateValue && pp != 'NaN/NaN/NaN' && DateValue != 'NaN/NaN/NaN') {
                                //alert(ContractIdtext);
                                if (Status == 'Open') {

                                    CloseClass = "starting";
                                }
                                else {
                                    CloseClass = "ending";
                                }
                                var TextHtml = '<div style="margin-top:10px" id=' + ActivityId + '   class="issue status-4 priority-2 child ' + CloseClass + ' tooltip"> <img id="CloseUploaderImg" align="right" src="../Images/cross_cal.png" style="margin-top:-5px;margin-right:-8px" title="Delete activity" onclick="return DeleteActivity(' + ActivityId + ');" IsOCR = ' + IsOCR + '></img><br> <a onclick="ShowActivityDetails(' + ActivityId + ' )"  href="#" class="issue status-4 priority-2 child">Activity details</a>:' + activityText + '<span class="tip"><a onclick="ShowActivityDetails(' + ActivityId + ' )" href="#" class="issue status-4 priority-2 child">Activity Description</a>: ' + activityText + '<br><br><strong>Contract ID</strong>: <label>' + ContractIdtext + '</label><br><br><strong>Added by</strong>: <label>' + AddedByName + '</label><br> <strong>Activity date</strong>: <label>' + ActivityDateDefaultFormat + '</label><br><strong>Activity type</strong>:' + ActivityType + '<br><strong>Status </strong>:' + Status + '<br><strong>Activity Remarks</strong>:' + activityText + '<br><strong>Reminder date</strong>: ' + ReminderDate + '<br>  <strong>Post on customer portal</strong>: ' + IsOnCustomerPortal + '</span>  </div>'
                                var TextHtml = '';
                                if (isEditable == 'Y') {

                                    if (ContractIdtext != "") {

                                        TextHtml = '<div style="margin-top:10px;" id=' + ActivityId + '   class="issue status-4 priority-2 child ' + CloseClass + ' tooltip"> <img id="CloseUploaderImg"  ContractTypeId=' + ContractTypeId + ' filepath=' + escape(FileUpload) + ' align="right" src="../Images/cross_cal.png" style="margin-top:-5px;margin-right:-8px" title="Delete activity" onclick="return DeleteActivity(' + ActivityId + ',this);" IsOCR = ' + IsOCR + '></img><br> <a onclick="ShowActivityDetails(' + ActivityId + ',this )"  href="#" class="issue status-4 priority-2 child">Activity details</a>:' + activityText + '<span class="tip" style="margin-left:-200px"><a onclick="ShowActivityDetails(' + ActivityId + ',this )" href="#" class="issue status-4 priority-2 child">Activity Description</a>: ' + activityText + '<br><br><strong>Contract ID</strong>:  <a onclick="GoContractToDetails(' + ReqID + ',' + ContractTypeId + ',this )" href="#" class="issue status-4 priority-2 child" >' + ContractIdtext + '</a><br><br><strong>Added by</strong>: ' + AddedByName + '<br> <strong>Activity date</strong>: ' + ActivityDateDefaultFormat + '<br><strong>Activity type</strong>:' + ActivityType + '<br><strong>Status </strong>:' + Status + '<br><strong>Activity Remarks</strong>:' + activityText + '<br><strong>Reminder date</strong>: ' + ReminderDate + '<br>  </span>  </div>'

                                    }
                                    else {
                                        TextHtml = '<div style="margin-top:10px;" id=' + ActivityId + '   class="issue status-4 priority-2 child ' + CloseClass + ' tooltip"> <img id="CloseUploaderImg"  ContractTypeId=' + ContractTypeId + ' filepath=' + escape(FileUpload) + ' align="right" src="../Images/cross_cal.png" style="margin-top:-5px;margin-right:-8px" title="Delete activity" onclick="return DeleteActivity(' + ActivityId + ',this);" IsOCR = ' + IsOCR + '></img><br> <a onclick="ShowActivityDetails(' + ActivityId + ',this )"  href="#" class="issue status-4 priority-2 child">Activity details</a>:' + activityText + '<span class="tip" style="margin-left:-200px"><a onclick="ShowActivityDetails(' + ActivityId + ',this )" href="#" class="issue status-4 priority-2 child">Activity Description</a>: ' + activityText + '<br><br><strong>Contract ID</strong>: ' + ContractIdtext + '<br><br><strong>Added by</strong>: ' + AddedByName + '<br> <strong>Activity date</strong>: ' + ActivityDateDefaultFormat + '<br><strong>Activity type</strong>:' + ActivityType + '<br><strong>Status </strong>:' + Status + '<br><strong>Activity Remarks</strong>:' + activityText + '<br><strong>Reminder date</strong>: ' + ReminderDate + '<br>  </span>  </div>'
                                    }
                                }
                                else {
                                    TextHtml = '<div style="margin-top:10px;" id=' + ActivityId + '   class="issue status-4 priority-2 child ' + CloseClass + ' tooltip"> <img id="CloseUploaderImg"  ContractTypeId=' + ContractTypeId + ' filepath=' + escape(FileUpload) + ' align="right" src="../Images/cross_cal.png" style="margin-top:-5px;margin-right:-8px" title="Delete activity" onclick="return false;" IsOCR = ' + IsOCR + '></img><br> <a onclick="return false;"  href="#" class="issue status-4 priority-2 child areadOnly">Activity details</a>:' + activityText + '<span class="tip" style="margin-left:-200px"><a onclick="return false;" href="#" class="issue status-4 priority-2 child areadOnly">Activity Description</a>: ' + activityText + '<br><br><strong>Contract ID</strong>:  <a onclick="GoContractToDetails(' + ReqID + ',' + ContractTypeId + ',this )" href="#" class="issue status-4 priority-2 child" >' + ContractIdtext + '</a><br><br><strong>Added by</strong>: ' + AddedByName + '<br> <strong>Activity date</strong>: ' + ActivityDateDefaultFormat + '<br><strong>Activity type</strong>:' + ActivityType + '<br><strong>Status </strong>:' + Status + '<br><strong>Activity Remarks</strong>:' + activityText + '<br><strong>Reminder date</strong>: ' + ReminderDate + '<br>  </span>  </div>'
                                }


                                $(obj).css('z-index', '-1000');
                                obj.find('a[target="name"]').after(TextHtml);
                            }
                        });
                    });

                    if (response != '2') {
                        isValid = true;
                    }
                    else {
                        // alert('Stage name already exists.');
                        isValid = false;
                    }
                },
                error: function () {
                    alert("some problem in loading data");
                    return false;
                }
            });

            if ($('#hdnisAdd').val() == "N") {
                $('p.day-num').find('a[target="name"]').attr('onclick', 'return false').addClass('areadOnly');
                $('p.day-num').find('img[title="Add activity"]').attr('onclick', 'return false');
            }

            if ($('#hdnIsUpdate').val() == "N") {
                $('p.day-num').find('a.issue').attr('onclick', 'return false').addClass('areadOnly');
            }

            if ($('#hdnIsDelete').val() == "N") {
                $('img[id="CloseUploaderImg"]').css('display', 'none');
            }
        }

        function BindActivitiesonLoad() {
            $.ajax({
                type: 'POST',
                contentType: "text/html; charset=utf-8",
                url: "Handlers/BindActivities.ashx?Month=" + $('#hdnMonth').val() + "&Year=" + $('#hdnYear').val() + "&UserId=" + $('#hdnUserId').val(),
                data: {},
                datatype: "html",
                async: false,
                success: function (response) {
                    $('#MyDiv').html('');
                    $('#MyDiv').html(response);

                    $('.cal').find('tbody tr').find('td').each(function () {
                        var obj = $(this);
                        var pp = $(this).find('input[id="hdnDateVal"]').val();

                        $('#MyDiv').find('table tbody tr').find('td:eq(4)').each(function () {
                            var activityText = $(this).prev('td').html();
                            var IsOnCustomerPortal = $(this).parent('tr').find('td:eq(13)').html() == 'Y' ? 'Yes' : 'No';
                            var ReminderDate = $(this).parent('tr').find('td:eq(14)').html();
                            var ActivityType = $(this).parent('tr').find('td:eq(2)').html();
                            var ActivityId = $(this).parent('tr').find('td:eq(0)').html();
                            var Status = $(this).parent('tr').find('td:eq(11)').html();
                            var strDate = $(this).html();

                            if (pp == strDate && pp != 'NaN/NaN/NaN' && strDate != 'NaN/NaN/NaN') {
                                //                            var TextHtml = '<div id=' + ActivityId + '   class="issue status-4 priority-2 child starting  tooltip"> <img id="CloseUploaderImg" align="right" src="Images/cross_cal.png" style="margin-top:-15px;margin-right:-20px" onclick="return DeleteActivity(' + ActivityId + ');"></img> <a onclick="ShowActivityDetails(' + ActivityId + ' )"  href="#" class="issue status-4 priority-2 child">Activity details</a>:' + activityText + '<span class="tip"><a onclick="ShowActivityDetails(' + ActivityId + ' )" href="#" class="issue status-4 priority-2 child">Activity Description</a>: ' + activityText + '<br><br><strong>Added by</strong>: <a onclick="ShowActivityDetails(' + ActivityId + ' )" href="#">  John Doe</a><br> <strong>Activity date</strong>: <a onclick="ShowActivityDetails(' + ActivityId + ' )" href="#">' + $(this).html() + '</a><br><strong>Activity type</strong>:' + ActivityType + '<br><strong>Status </strong>:' + Status + '<br><strong>Activity Remarks</strong>:' + activityText + '<br><strong>Reminder date</strong>: ' + ReminderDate + '<br>  <strong>Post on customer portal</strong>: ' + IsOnCustomerPortal + '</span>  </div>'
                                var TextHtml = '<div id=' + ActivityId + '   class="issue status-4 priority-2 child starting  tooltip"> <img id="CloseUploaderImg" filepath=' + escape(FileUpload) + ' align="right" src="Images/cross_cal.png" style="margin-top:-15px;margin-right:-20px" onclick="return DeleteActivity(' + ActivityId + ',this);"></img> <a onclick="ShowActivityDetails(' + ActivityId + ' )"  href="#" class="issue status-4 priority-2 child">Activity details</a>:' + activityText + '<span class="tip"><a onclick="ShowActivityDetails(' + ActivityId + ' )" href="#" class="issue status-4 priority-2 child">Activity Description</a>: ' + activityText + '<br><br><strong>Added by</strong>: <a onclick="ShowActivityDetails(' + ActivityId + ' )" href="#">  John Doe</a><br> <strong>Activity date</strong>: <a onclick="ShowActivityDetails(' + ActivityId + ' )" href="#">' + $(this).html() + '</a><br><strong>Activity type</strong>:' + ActivityType + '<br><strong>Status </strong>:' + Status + '<br><strong>Activity Remarks</strong>:' + activityText + '<br><strong>Reminder date</strong>: ' + ReminderDate + '<br> </span>  </div>'
                                obj.find('a[target="name"]').after(TextHtml);
                            }
                        });
                    });

                    if (response != '2') {
                        isValid = true;
                    }
                    else {
                        //alert('Stage name already exists.');
                        isValid = false;
                    }
                },
                error: function () {
                    alert("some problem in loading data");
                    return false;
                }
            });

            if ($('#hdnisAdd').val() == "N") {
                $('p.day-num').find('a[target="name"]').attr('onclick', 'return false').addClass('areadOnly');
                $('p.day-num').find('img[title="Add activity"]').attr('onclick', 'return false');
            }

            if ($('#hdnIsUpdate').val() == "N") {
                $('p.day-num').find('a.issue').attr('onclick', 'return false').addClass('areadOnly');
            }

            if ($('#hdnIsDelete').val() == "N") {
                $('img[id="CloseUploaderImg"]').css('display', 'none');
            }
        }

        function SelectMonth(obj) {
            $('#hdnMonth').val($(obj).val());
        }
        function SelectYear(obj) {
            $('#hdnYear').val($(obj).val());
        }

        function CallCalendar() {

            var SelectedId = $('.selected').attr('id');
            var d = new Date(parseInt($('#year').val()), 1, parseInt($('#month').val()));
            $('#hdnDatePointer').val(d.toLocaleDateString());

            if (SelectedId == 'tab-workflow') {
                var cal = new Calendar();
                cal.generateHTML();
                $('#Calendar').html(cal.getHTML());
                BindActivities();

                $('#lblMonthName').text(cal_months_labels[parseInt($('#hdnMonth').val()) - 1] + '-' + $('#hdnYear').val());
                $('#LastMonth').removeClass().addClass((parseInt($('#hdnMonth').val()) - 1).toString());
                $('#NextMonth').removeClass().addClass((parseInt($('#hdnMonth').val()) + 1).toString());

            }
            else if (SelectedId == 'tab-activity') {

                ShowWeekTab(undefined);
            }
            else {
                ShowDayTab(undefined);
            }


            if ($('#hdnisAdd').val() == "N") {
                $('p.day-num').find('a[target="name"]').attr('onclick', 'return false').addClass('areadOnly');
                $('p.day-num').find('img[title="Add activity"]').attr('onclick', 'return false');

            }

            if ($('#hdnIsUpdate').val() == "N") {
                $('p.day-num').find('a.issue').attr('onclick', 'return false').addClass('areadOnly');

            }


            if ($('#hdnIsDelete').val() == "N") {
                $('img[id="CloseUploaderImg"]').css('display', 'none');

            }

        }


        $(document).ready(function () {
            // HideShow();
            $('#toggleimage').hide();
            var date = new Date();

            y = date.getFullYear(), m = date.getMonth();

            $('#month').val(m + 1);
            $('#year').val(y);
            $('#hdnMonth').val((parseInt($('#month').val())).toString());

            $('#hdnYear').val(parseInt($('#year').val()).toString());

            var cal = new Calendar();
            cal.generateHTML();
            $('#Calendar').html(cal.getHTML());
            BindActivities();
            $('#lblMonthName').text(cal_months_labels[m] + '-' + y);
            //Setting Month Number
            $('#LastMonth').removeClass().addClass((m).toString());
            $('#NextMonth').removeClass().addClass((m + 2).toString());
            $('#hdnStartWeekDay').val(new Date());
            var d = new Date();
            d.setDate(d.getDate() + 1);
            $('#hdnDatePointer').val(d.toString())

            if ($('#hdnisAdd').val() == "N") {
                $('p.day-num').find('a[target="name"]').attr('onclick', 'return false').addClass('areadOnly');
                $('p.day-num').find('img[title="Add activity"]').attr('onclick', 'return false');
            }

            if ($('#hdnIsUpdate').val() == "N") {
                $('p.day-num').find('a.issue').attr('onclick', 'return false').addClass('areadOnly');
            }

            if ($('#hdnIsDelete').val() == "N") {
                $('img[id="CloseUploaderImg"]').css('display', 'none');

            }
        });
        function ClosePopup() {
            $.unblockUI();
            return false;
        }

        function setValue(activityText, status) {


            $('#hdnStatusMessage').val(status);
            var WhichTabSelected = $('a.selected').attr('id');
            $('#hdnTabSeleted').val(WhichTabSelected);

            if ($('#hdnStatusMessage').val() == '1') {

                $('#dvMesssage').removeClass().addClass('flash notice').html('Activity added Successfully').show();


            }
            else {
                $('#dvMesssage').removeClass().addClass('flash notice').html('Activity Updated Successfully').show();
            }

            if ($('#hdnTabSeleted').val() == 'tab-workflow') {

                $('#Calendar').html('');
                CallCalendar();

            }
            else if ($('#hdnTabSeleted').val() == 'tab-activity') {

                var Dates = new Date($('#hdnWeekStart').val()).getWeek();
                var todayStart = new Date(Dates[0]);
                var ddStart = todayStart.getDate();
                var mmStart = todayStart.getMonth() + 1; //January is 0!

                var yyyyStart = todayStart.getFullYear();
                if (ddStart < 10) { ddStart = '0' + ddStart } if (mmStart < 10) { mmStart = '0' + mmStart }
                todayStart = mmStart + '/' + ddStart + '/' + yyyyStart;


                var todayEnd = new Date(Dates[1]);
                var ddEnd = todayEnd.getDate();
                var mmEnd = todayEnd.getMonth() + 1; //January is 0!

                var yyyyEnd = todayEnd.getFullYear();
                if (ddEnd < 10) { ddEnd = '0' + ddEnd } if (mmEnd < 10) { mmEnd = '0' + mmEnd }
                todayEnd = mmEnd + '/' + ddEnd + '/' + yyyyEnd;

                var MonthNumberStart = todayStart.split('/');
                var MonthNumberEnd = todayEnd.split('/');

                var startMonthName = cal_months_labels[MonthNumberStart[0] - 1] + ' ' + MonthNumberStart[1];

                var EndMonthName = cal_months_labels[MonthNumberEnd[0] - 1] + ' ' + MonthNumberEnd[1];

                var today = new Date($('#hdnWeekStart').val());


                var tomorrow = new Date(today.getTime() + (168 * 60 * 60 * 1000));


                if (tomorrow.getMonth() == 0) {
                    var d = new Date($('#hdnWeekEnd').val());
                    // $('#hdnYear').val((parseInt($('#hdnYear').val()) + 1).toString());
                    $('#hdnWeekStart').val(d.toString());
                    BindWeekTable(startMonthName, EndMonthName, yyyyStart, yyyyEnd);
                }
                else {

                    BindWeekTable(startMonthName, EndMonthName, yyyyStart, yyyyEnd);
                }
                BindWeeklyActivities();

            }
            else {
                $('#CalendarDay').html('');
                BindDayTable($('#hdnDatePointer').val());
                BindDayActivities();


            }
            setTimeout(function () {
                $('#dvMesssage').hide();
                // location.reload(true);
            }, 2000);

            if ($('#hdnisAdd').val() == "N") {
                $('p.day-num').find('a[target="name"]').attr('onclick', 'return false').addClass('areadOnly');
                $('p.day-num').find('img[title="Add activity"]').attr('onclick', 'return false');

            }


            if ($('#hdnIsUpdate').val() == "N") {
                $('p.day-num').find('a.issue').attr('onclick', 'return false').addClass('areadOnly');

            }


            if ($('#hdnIsDelete').val() == "N") {
                $('img[id="CloseUploaderImg"]').css('display', 'none');

            }

        }

        function opennewsletter(obj) {
            var ContractTypeId = $('#CloseUploaderImg').attr('ContractTypeId')
            //            alert(ContractTypeId);
            $("html, body").scrollTop(0);
            $('#hdnDateBlock').val($(obj).find('input').val());
            var DateSplit = $(obj).find('input').val().split('/');
            //   var DateTempVal1 = new Date(DateSplit[2], DateSplit[1], DateSplit[0]);
            $(document).find('div.tooltip').removeClass('tooltip');

            //   var DS = new Date(DateSplit[2] ,DateSplit[1],DateSplit[0]);
            var DateTempVal = DateSplit[1] + '/' + DateSplit[0] + '/' + DateSplit[2];

            emailwindow = dhtmlmodal.open('FieldDetails', 'iframe', 'EventAdd.aspx?ActivityDate=' + DateTempVal, '', 'width=600px,scrollbars=no,height=500px,center=1,resize=0"');
            emailwindow.onclose = function () { //Define custom code to run when window is closed

                var theform = this.contentDoc.forms[0] //Access first form inside iframe just for your reference

                return true;
            }


        }


        function opennewsletterDay(obj) {
            $("html, body").scrollTop(0);

            $(document).find('div.tooltip').removeClass('tooltip');

            var DateSplit = $(obj).closest('td').find('input:eq(0)').val().split('/');

            var DateTempVal = DateSplit[1] + '/' + DateSplit[0] + '/' + DateSplit[2] + ' ' + $(obj).find('input:eq(1)').val() + ' ' + $(obj).find('input:eq(2)').val();

            emailwindow = dhtmlmodal.open('FieldDetailsMA', 'iframe', 'EventAdd.aspx?ActivityDate=' + DateTempVal, '', 'width=500px,scrollbars=no,height=500px,center=1,resize=0"');
            emailwindow.onclose = function () { //Define custom code to run when window is closed

                var theform = this.contentDoc.forms[0] //Access first form inside iframe just for your reference
                return true;
            }
        }

        function opennewsletterWeek(obj) {
            $("html, body").scrollTop(0);

            $(document).find('div.tooltip').removeClass('tooltip');

            var DateSplit = $(obj).closest('td').find('input:eq(0)').val().split('/');

            var DateTempVal = DateSplit[1] + '/' + DateSplit[0] + '/' + DateSplit[2] + ' ' + $(obj).find('input:eq(1)').val() + ' ' + $(obj).find('input:eq(2)').val();

            emailwindow = dhtmlmodal.open('FieldDetailsMAW', 'iframe', 'EventAdd.aspx?ActivityDate=' + DateTempVal, '', 'width=500px,scrollbars=no,height=500px,center=1,resize=0"');
            emailwindow.onclose = function () { //Define custom code to run when window is closed

                var theform = this.contentDoc.forms[0] //Access first form inside iframe just for your reference
                return true;
            }
        }

        function NavigateBack(obj) {
            $('#hdnMonth').val((parseInt($('#month').val()) - 1).toString());
            $('#hdnYear').val($('#year').val());
            $('#month').val((parseInt($('#month').val()) - 1).toString());

            //  alert(parseInt($('#hdnMonth').val()));
            if (parseInt($('#hdnMonth').val()) == 0) {
                $('#month').val('12');
                $('#year').val((parseInt($('#year').val()) - 1).toString());
                $('#hdnMonth').val($('#month').val());
                $('#hdnYear').val($('#year').val());
            }
            $('.icon-checked').click();

            if ($('#hdnisAdd').val() == "N") {
                $('p.day-num').find('a[target="name"]').attr('onclick', 'return false').addClass('areadOnly');
                $('p.day-num').find('img[title="Add activity"]').attr('onclick', 'return false');
            }

            if ($('#hdnIsUpdate').val() == "N") {
                $('p.day-num').find('a.issue').attr('onclick', 'return false').addClass('areadOnly');
            }

            if ($('#hdnIsDelete').val() == "N") {
                $('img[id="CloseUploaderImg"]').css('display', 'none');
            }
            return true;
        }

        function NavigateNext(obj) {
            $('#hdnMonth').val((parseInt($('#month').val()) + 1).toString());
            $('#hdnYear').val($('#year').val());
            $('#month').val((parseInt($('#month').val()) + 1).toString());

            if (parseInt($('#hdnMonth').val()) == 13) {
                $('#month').val('1');
                $('#year').val((parseInt($('#year').val()) + 1).toString());
                $('#hdnMonth').val('1');
                $('#hdnYear').val((parseInt($('#year').val())).toString());

            }

            $('.icon-checked').click();
            if ($('#hdnisAdd').val() == "N") {
                $('p.day-num').find('a[target="name"]').attr('onclick', 'return false').addClass('areadOnly');
                $('p.day-num').find('img[title="Add activity"]').attr('onclick', 'return false');
            }

            if ($('#hdnIsUpdate').val() == "N") {
                $('p.day-num').find('a.issue').attr('onclick', 'return false').addClass('areadOnly');
            }

            if ($('#hdnIsDelete').val() == "N") {
                $('img[id="CloseUploaderImg"]').css('display', 'none');
            }
            return true;
        }

        function ShowActivityDetails(ActivityId, obj) {


            var ContractTypeId = $('#CloseUploaderImg').attr('ContractTypeId')
            //            var ContractTypeId = $(obj).attr('ContractTypeId');

            //            alert(ContractTypeId);
            $("html, body").scrollTop(0);
            $(document).find('div.tooltip').removeClass('tooltip');
            emailwindow = dhtmlmodal.open('FieldDetailss', 'iframe', 'EventAdd.aspx?ActivityId=' + ActivityId, '', 'width=500px,scrollbars=no,height=500px,center=1,resize=0"');
            emailwindow.onclose = function () { //Define custom code to run when window is closed

                var theform = this.contentDoc.forms[0] //Access first form inside iframe just for your reference

                return true;
            }
        }


        function GoContractToDetails(RequestId, ContractTypeId, obj) {

            //debugger;
            var ContractAssemblyAccess = '<%= ViewState["RequestTypeClickAccess"] %>';
            var Status = "Workflow";
            var CTypeId = $("#ContractTypeIds").text().split(',');
            if (ContractAssemblyAccess == 'True') {

                for (var i = 0; i < CTypeId.length; i++) {
                    if (CTypeId == "0" || CTypeId[i] == ContractTypeId) {

                        window.location.href = "../Workflow/RequestFlow.aspx?Status=" + Status + "&RequestId=" + RequestId + "";
                    }
                }
            }

            return true;

        }

        function ShowWeekActivityDetails(ActivityId, obj) {
            var ContractTypeId = $('#CloseUploaderImg').attr('ContractTypeId')
            //            alert(ContractTypeId);
            $("html, body").scrollTop(0);
            $(document).find('div.tooltip').removeClass('tooltip');
            emailwindow = dhtmlmodal.open('FieldDetailssS', 'iframe', 'EventAdd.aspx?ActivityId=' + ActivityId, '', 'width=500px,scrollbars=no,height=500px,center=1,resize=0"');
            emailwindow.onclose = function () { //Define custom code to run when window is closed

                var theform = this.contentDoc.forms[0] //Access first form inside iframe just for your reference

                return true;
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <input type="hidden" clientidmode="Static" id="hdnYear" runat="Server" />
    <input type="hidden" clientidmode="Static" id="hdnMonth" runat="Server" />
    <input type="hidden" clientidmode="Static" id="hdnDateBlock" runat="Server" />
    <input type="hidden" clientidmode="Static" id="hdnUserId" runat="Server" />
    <input type="hidden" clientidmode="Static" id="hdnStatusMessage" runat="Server" />
    <input type="hidden" clientidmode="Static" id="hdnWeekStart" runat="Server" />
    <input type="hidden" clientidmode="Static" id="hdnWeekend" runat="Server" />
    <input type="hidden" clientidmode="Static" id="hdnStartWeekDay" runat="Server" />
    <input type="hidden" clientidmode="Static" id="hdnEndWeekDay" runat="Server" />
    <input type="hidden" clientidmode="Static" id="hdnDatePointer" runat="Server" />
    <input type="hidden" clientidmode="Static" id="hdnTabSeleted" runat="Server" />
    <input type="hidden" clientidmode="Static" id="hdnisAdd" runat="Server" />
    <input type="hidden" clientidmode="Static" id="hdnIsUpdate" runat="Server" />
    <input type="hidden" clientidmode="Static" id="hdnIsDelete" runat="Server" />
    <div class="controller-calendars action-show">
        <div id="rightlinks" style="display: none;">
            <uc1:Masterlinks ID="rightactions" runat="server" />
        </div>
        <script type="text/javascript">
            $("#sidebar").html($("#rightlinks").html());
        </script>
        <div id="wrapper">
            <div id="dvMesssage" class="flash notice" style="display: none">
            </div>
            <h2>
                Calendar</h2>
            <div id="MyDiv" style="display: none">
            </div>
            <div id="MyDivWeek" style="display: none">
            </div>
            <div id="MyDivDay" style="display: none">
            </div>
            <input id="set_filter" name="set_filter" type="hidden" value="1">
            <p class="buttons">
                <label for="month">
                    Month</label>
                <select id="month" name="month" onchange="return SelectMonth(this)">
                    <option value="1">January</option>
                    <option value="2">February</option>
                    <option value="3">March</option>
                    <option selected="selected" value="4">April</option>
                    <option value="5">May</option>
                    <option value="6">June</option>
                    <option value="7">July</option>
                    <option value="8">August</option>
                    <option value="9">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                </select>
                <label for="year">
                    Year</label>
                <select id="year" name="year" onchange="return SelectYear(this)">
                    <option value="2009">2009</option>
                    <option value="2010">2010</option>
                    <option value="2011">2011</option>
                    <option value="2012">2012</option>
                    <option value="2013">2013</option>
                    <option selected="selected" value="2014">2014</option>
                    <option value="2015">2015</option>
                    <option value="2016">2016</option>
                    <option value="2017">2017</option>
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                </select>
                <a class="icon icon-checked" href="#" onclick="CallCalendar();">Apply</a> <a class="icon"
                    href="OutlookCalendarSynch.aspx" target="_self" id="linkOutlookSynch" runat="server"
                    clientidmode="Static" visible="false">Outlook Calendar Sync</a>
                <div class="tabs" id="CalendarTabs" style="width: 100%">
                    <ul>
                        <li id="ulMonthTab"><a href="#" class="selected" id="tab-workflow" onclick="ShowMonthTab('Activity'); this.blur(); return false;">
                            Month</a></li>
                        <li id="ulWeekTab"><a href="#" id="tab-activity" onclick="ShowWeekT(); this.blur(); return false;">
                            Week</a></li>
                        <li id="ulDayTab"><a href="#" id="tab-document" onclick="ShowDayD(); this.blur(); return false;">
                            Day</a></li>
                        <li id="Li1"><a href="#" id="A7" onclick="ShowAgenda(''); this.blur();">Agenda</a></li>
                    </ul>
                </div>
                <center>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<table>
                        <tr>
                            <td>
                                <a href="#" id="A1" onclick="return NavigateBack(this);">
                                    <img src="../Images/prev.png"></img></a><a href="#" style="display: none" id="A2"
                                        onclick="return NavigateBackWeek(this);"><img src="../Images/prev.png"></img></a><a
                                            href="#" style="display: none" id="A5" onclick="return NavigateBackDay(this);"><img
                                                src="../Images/prev.png"></img></a>
                            </td>
                            <td>
                                <h1>
                                    <label id="lblMonthName">
                                    </lable></h1>
                            </td>
                            <td>
                                <a href="#" id="A3" onclick="return NavigateNext(this);">
                                    <img src="../Images/next.png"></img></a><a href="#" style="display: none" id="A4"
                                        onclick="return NavigateNextWeek(this);"><img src="../Images/next.png"></img></a><a
                                            href="#" style="display: none" id="A6" onclick="return NavigateNextDay(this);"><img
                                                src="../Images/next.png"></img></a>
                            </td>
                        </tr>
                    </table>
                </center>
                Note:Click on the (+) icon in cell to add activity.
                <div id="Calendar">
                </div>
                <div id="CalendarWeek">
                </div>
                <div id="CalendarDay">
                </div>
        </div>
        <img src="../Images/images/bullet_go.png"></img>Open Activities<br>
        <img src="../Images/images/bullet_end.png"></img>Closed Activities
    </div>
    <script type="text/javascript">

        $("#calendarLink").addClass("menulink");
        $("#calendarTab").addClass("selectedtab");

    </script>
    <%--
Week Code starts Here--%>
    <script type="text/javascript">
        //Week Number starts Here

        function ShowAgenda() {
            location.href = "CalendarAgenda.aspx";
        }

        function ShowMonthTab() {
            $('#Calendar').show();
            $('#CalendarWeek').hide();
            $('#CalendarDay').hide();
            $('#A1').show();
            $('#A2').hide();
            $('#A3').show();
            $('#A4').hide();
            $('#A5').hide();
            $('#A6').hide();
            $('#tab-workflow').addClass('selected');
            $('#tab-activity').removeClass('selected');
            $('#tab-document').removeClass('selected')

            var cal = new Calendar();
            cal.generateHTML();
            $('#Calendar').html(cal.getHTML());
            BindActivities();

            $('#lblMonthName').text(cal_months_labels[m] + '-' + y);

            //Setting Month Number
            $('#LastMonth').removeClass().addClass((m).toString());
            $('#NextMonth').removeClass().addClass((m + 2).toString());
            $('#hdnStartWeekDay').val(new Date());
            if ($('#hdnisAdd').val() == "N") {
                $('p.day-num').find('a[target="name"]').attr('onclick', 'return false').addClass('areadOnly');
                $('p.day-num').find('img[title="Add activity"]').attr('onclick', 'return false');

            }

            if ($('#hdnIsUpdate').val() == "N") {
                $('p.day-num').find('a.issue').attr('onclick', 'return false').addClass('areadOnly');

            }

            if ($('#hdnIsDelete').val() == "N") {
                $('img[id="CloseUploaderImg"]').css('display', 'none');
            }
        }

        function AddWeeklyActivity(obj) {

            $("html, body").scrollTop(0);
            var DateSplit = $(obj).closest('td').find('input:eq(0)').val().split('/');
            var DateTempVal = DateSplit[1] + '/' + DateSplit[0] + '/' + DateSplit[2] + ' ' + $(obj).find('input:eq(1)').val() + ' ' + $(obj).find('input:eq(2)').val();
            emailwindow = dhtmlmodal.open('FieldDetailssMA', 'iframe', 'EventAdd.aspx?ActivityDate=' + DateTempVal, '', 'width=500px,scrollbars=no,height=500px,center=1,resize=0"');
            emailwindow.onclose = function () { //Define custom code to run when window is closed
                var theform = this.contentDoc.forms[0] //Access first form inside iframe just for your reference
                return true;
            }
        }

        Date.prototype.getWeek = function (start) {
            start = start || 0;
            // alert(start);
            var today = new Date(this.setHours(0, 0, 0, 0));
            var day = today.getDay() - start;
            var date = today.getDate() - day;
            var StartDate = new Date(today.setDate(date));
            var EndDate = new Date(today).addDays(6);
            return [StartDate, EndDate];
        }

        // test code

        //Returns the four-digit year corresponding to the ISO week of the date.
        Date.prototype.getWeekYear = function () {
            var date = new Date(this.getTime());
            date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
            return date.getFullYear();
        }

        //Week Number section ends here

        function ShowWeekView() {

        }

        function BindDayTable(CurrentDate) {

            var d = new Date(CurrentDate);
            var TDate = d.getDate();
            var TMonth = d.getMonth() + 1;
            var Tyear = d.getFullYear();
            var FullDate = TDate + '/' + TMonth + '/' + Tyear;

            var MonthNumberStart = FullDate.split('/');
            var Year = Tyear;
            var startMonthName = cal_months_labels[MonthNumberStart[1] - 1] + ' ' + MonthNumberStart[0];
            var html = '<body class="controller-calendars action-show"><table class="cal">';
            html += '<thead><tr><th scope="col" style="width:5%"></th>';
            for (var i = 0; i < 1; i++) {

                html += '<th scope="col" style="width:98%">';
                html += startMonthName + ', ' + Year;
                html += '</th>';
            }
            html += '</tr></thead>';

            var day = 1;

            for (var i = 0; i < 24; i++) {

                for (var j = 0; j <= 1; j++) {

                    if (j > 0) {
                        html += '<td  colspan="7"><p class="day-num"><a href="#" target="name" onclick="opennewsletterDay(this); return false"><img title="Add activity"  style="margin-right:0px" src="../Images/addbig.png"></img>';

                        var MonthValue = $('#hdnMonth').val().length == 1 ? '0' + $('#hdnMonth').val() : $('#hdnMonth').val();

                        var DateValueTemp = day.toString().length == 1 ? '0' + day : day;
                        var now = new Date();
                        var DateValue = MonthValue + '/' + DateValueTemp + '/' + $('#hdnYear').val();
                        if (i <= 11) {
                            var DateToSplit = FullDate.split('/');
                            var Weekday = DateToSplit[0] + '/' + DateToSplit[1] + '/' + DateToSplit[2];
                            var Form = (i == 0 ? 12 : i).toString() + ':' + '00';
                            var TimeFormat = 'AM'

                        }
                        else {
                            var DateToSplit = FullDate.split('/');
                            var Weekday = DateToSplit[0] + '/' + DateToSplit[1] + '/' + DateToSplit[2];
                            if (i == 12) {
                                var Form = '12:' + '00';

                            }
                            else {
                                var Form = (i - 12).toString() + ':' + '00';
                            }
                            var TimeFormat = 'PM'
                            // alert(Weekday + Form);
                        }
                        html += '<input type="hidden" id="hdnWeekId" value=' + Weekday + '>';
                        html += '<input type="hidden" id="hdnTimeId" value=' + Form + '>';
                        html += '<input type="hidden" id="hdnTimeFormatId" value=' + TimeFormat + '>';

                        day++;

                        html += '</p></a></td>';
                    }
                    else {
                        html += '<td colspan="1"><p class="day-num">';

                        var MonthValue = $('#hdnMonth').val().length == 1 ? '0' + $('#hdnMonth').val() : $('#hdnMonth').val();
                        var DateValueTemp = day.toString().length == 1 ? '0' + day : day;
                        var DateValue = MonthValue + '/' + DateValueTemp + '/' + $('#hdnYear').val();
                        html += "<b>" + cal_Time_labels[i] + "</b>";
                        html += '<input type="hidden" id="hdnDateVal" value=' + DateValue + '>';
                        day++;
                        html += '</p></td>';
                    }
                }

                html += '</tr><tr>';
            }
            html += '</tr></table></div></body>';
            $('#Calendar').hide();
            $('#CalendarWeek').hide();

            $('#CalendarDay').show();
            $('#CalendarDay').html(html);
            if ($('#hdnisAdd').val() == "N") {
                $('p.day-num').find('a[target="name"]').attr('onclick', 'return false').addClass('areadOnly');
                $('p.day-num').find('img[title="Add activity"]').attr('onclick', 'return false');
            }

            if ($('#hdnIsUpdate').val() == "N") {
                $('p.day-num').find('a.issue').attr('onclick', 'return false').addClass('areadOnly');
            }

            if ($('#hdnIsDelete').val() == "N") {
                $('img[id="CloseUploaderImg"]').css('display', 'none');
            }
        }

        function ShowDayD() {
            $('#Calendar').hide();
            $('#CalendarWeek').hide();
            $('#CalendarDay').show();
            $('#tab-workflow').removeClass('selected');
            $('#tab-activity').removeClass('selected');
            $('#tab-document').addClass('selected');
            $('#A1').hide();
            $('#A2').hide();
            $('#A3').hide();
            $('#A4').hide();
            $('#A5').show();
            $('#A6').show();

            var d = new Date();
            //alert((d.getMonth() + 1).toString() + '--' + $('#month').val());

            if ((d.getMonth() + 1).toString() == $('#month').val()) {
                d = new Date();
            }
            else {
                d = new Date(parseInt($('#year').val()), parseInt($('#month').val()) - 1, 1);
            }

            d.setDate(d.getDate());
            $('#hdnDatePointer').val(d.toString())

            BindDayTable($('#hdnDatePointer').val());
            BindDayActivities();

            var d = new Date($('#hdnDatePointer').val());
            var TDate = d.getDate();
            var TMonth = d.getMonth();
            var Tyear = d.getFullYear();
            var FullDate = TDate + '/' + TMonth + '/' + Tyear;

            var MonthNumberStart = FullDate.split('/');
            var MonthNumberEnd = FullDate.split('/');
            var Year = Tyear;
            var startMonthName = cal_months_labels[TMonth] + ' ' + MonthNumberStart[0];

            $('#lblMonthName').text(startMonthName + ', ' + Year);
            if ($('#hdnisAdd').val() == "N") {
                $('p.day-num').find('a[target="name"]').attr('onclick', 'return false').addClass('areadOnly');
                $('p.day-num').find('img[title="Add activity"]').attr('onclick', 'return false');
            }

            if ($('#hdnIsUpdate').val() == "N") {
                $('p.day-num').find('a.issue').attr('onclick', 'return false').addClass('areadOnly');
            }

            if ($('#hdnIsDelete').val() == "N") {
                $('img[id="CloseUploaderImg"]').css('display', 'none');
            }
        }

        function ShowDayTab(Direction) {
            $('#Calendar').hide();
            $('#CalendarWeek').hide();
            $('#CalendarDay').show();
            $('#tab-workflow').removeClass('selected');
            $('#tab-activity').removeClass('selected');
            $('#tab-document').addClass('selected');
            $('#A1').hide();
            $('#A2').hide();
            $('#A3').hide();
            $('#A4').hide();
            $('#A5').show();
            $('#A6').show();

            if (Direction == 'next') {

                var d = new Date($('#hdnDatePointer').val());
                d.setDate(d.getDate() + 1);
                $('#hdnDatePointer').val(d.toString())
            }
            else if (Direction == undefined) {

                var d = new Date(parseInt($('#year').val()), parseInt($('#month').val()) - 1, 1);
                d.setDate(d.getDate());
                $('#hdnDatePointer').val(d.toString())
                //  $('#hdnDatePointer').val(new Date($('#hdnDatePointer').val()).getDate() -1)

            }
            else {
                var d = new Date($('#hdnDatePointer').val());
                d.setDate(d.getDate() - 1);
                $('#hdnDatePointer').val(d.toString());
            }

            BindDayTable($('#hdnDatePointer').val());
            BindDayActivities();

            var d = new Date($('#hdnDatePointer').val());

            var TDate = d.getDate();
            var TMonth = d.getMonth();
            var Tyear = d.getFullYear();
            var FullDate = TDate + '/' + TMonth + '/' + Tyear;

            var MonthNumberStart = FullDate.split('/');
            var MonthNumberEnd = FullDate.split('/');
            var Year = Tyear;
            var startMonthName = cal_months_labels[TMonth] + ' ' + MonthNumberStart[0];

            $('#lblMonthName').text(startMonthName + ', ' + Year);
            if ($('#hdnisAdd').val() == "N") {
                $('p.day-num').find('a[target="name"]').attr('onclick', 'return false').addClass('areadOnly');
                $('p.day-num').find('img[title="Add activity"]').attr('onclick', 'return false');
            }
            if ($('#hdnIsUpdate').val() == "N") {
                $('p.day-num').find('a.issue').attr('onclick', 'return false').addClass('areadOnly');
            }
            if ($('#hdnIsDelete').val() == "N") {
                $('img[id="CloseUploaderImg"]').css('display', 'none');
            }
        }

        function ShowWeekT() {
            $('#Calendar').hide();
            $('#CalendarWeek').show();
            $('#CalendarDay').hide();
            $('#A1').hide();
            $('#A2').show();
            $('#A3').hide();
            $('#A4').show();
            $('#A5').hide();
            $('#A6').hide();


            d = new Date(parseInt($('#year').val()), parseInt($('#month').val()) - 1, 1);
            //alert(new Date().getMonth() + '--' + $('#month').val());
            if (new Date().getMonth() + 1 == $('#month').val()) {
                d = new Date();
            }
            else {
                d = new Date(parseInt($('#year').val()), parseInt($('#month').val()) - 1, 1);
            }
            d.setDate(d.getDate());
            $('#hdnStartWeekDay').val(d.toString());

            //            if ($('#hdnWeekStart').val().length != 0) {

            //            var d = new Date($('#hdnWeekStart').val());
            //            d.setDate(d.getDate());

            //            $('#hdnStartWeekDay').val(d.toString());
            var Dates = new Date($('#hdnStartWeekDay').val()).getWeek();
            //            }
            //            else
            //            {
            //               var Dates = new Date($('#hdnStartWeekDay').val()).getWeek();
            //            }

            var todayStart = new Date(Dates[0]);
            var ddStart = todayStart.getDate();
            var mmStart = todayStart.getMonth() + 1; //January is 0!

            var yyyyStart = todayStart.getFullYear();
            if (ddStart < 10) { ddStart = '0' + ddStart } if (mmStart < 10) { mmStart = '0' + mmStart }
            todayStart = mmStart + '/' + ddStart + '/' + yyyyStart;

            var todayEnd = new Date(Dates[1]);
            var ddEnd = todayEnd.getDate();
            var mmEnd = todayEnd.getMonth() + 1; //January is 0!

            var yyyyEnd = todayEnd.getFullYear();
            if (ddEnd < 10) { ddEnd = '0' + ddEnd } if (mmEnd < 10) { mmEnd = '0' + mmEnd }
            todayEnd = mmEnd + '/' + ddEnd + '/' + yyyyEnd;

            var MonthNumberStart = todayStart.split('/');
            var MonthNumberEnd = todayEnd.split('/');

            var startMonthName = cal_months_labels[MonthNumberStart[0] - 1] + ' ' + MonthNumberStart[1];

            var EndMonthName = cal_months_labels[MonthNumberEnd[0] - 1] + ' ' + MonthNumberEnd[1];

            var today = new Date($('#hdnStartWeekDay').val());

            var tomorrow = new Date(today.getTime() + (168 * 60 * 60 * 1000));

            if (tomorrow.getMonth() == 0) {
                var d = new Date($('#hdnWeekEnd').val());
                // $('#hdnYear').val((parseInt($('#hdnYear').val()) + 1).toString());
                $('#hdnStartWeekDay').val(d.toString());
                BindWeekTable(startMonthName, EndMonthName, yyyyStart, yyyyEnd);
            }
            else {

                BindWeekTable(startMonthName, EndMonthName, yyyyStart, yyyyEnd);
            }

            $('#lblMonthName').text(startMonthName + ', ' + yyyyStart + ' To ' + EndMonthName + ', ' + yyyyEnd);
            $('#hdnStartWeekDay').val(tomorrow);

            $('#hdnWeekStart').val(todayStart);
            $('#hdnWeekend').val(todayEnd);

            $('#tab-workflow').removeClass('selected');
            $('#tab-activity').addClass('selected');
            $('#tab-document').removeClass('selected');
            BindWeeklyActivities();
            if ($('#hdnisAdd').val() == "N") {
                $('p.day-num').find('a[target="name"]').attr('onclick', 'return false').addClass('areadOnly');
                $('p.day-num').find('img[title="Add activity"]').attr('onclick', 'return false');

            }
            if ($('#hdnIsUpdate').val() == "N") {
                $('p.day-num').find('a.issue').attr('onclick', 'return false').addClass('areadOnly');
            }
            if ($('#hdnIsDelete').val() == "N") {
                $('img[id="CloseUploaderImg"]').css('display', 'none');

            }
        }

        function ShowWeekTab(Direction) {
            $('#Calendar').hide();
            $('#CalendarWeek').show();
            $('#CalendarDay').hide();
            $('#A1').hide();
            $('#A2').show();
            $('#A3').hide();
            $('#A4').show();
            $('#A5').hide();
            $('#A6').hide();

            if (Direction == 'next') {
                if ($('#hdnWeekend').val().length != 0) {
                    var d = new Date($('#hdnWeekend').val());
                    d.setDate(d.getDate() + 1);
                    $('#hdnStartWeekDay').val(d.toString());
                    var Dates = new Date($('#hdnStartWeekDay').val()).getWeek();
                }
                else {
                    var Dates = new Date($('#hdnStartWeekDay').val()).getWeek();
                }
            }
            else if (Direction == undefined) {

                var d = new Date(parseInt($('#year').val()), parseInt($('#month').val()) - 1, 1);
                d.setDate(d.getDate());

                $('#hdnStartWeekDay').val(d.toString());
                var Dates = new Date($('#hdnStartWeekDay').val()).getWeek();
            }

            else {
                if ($('#hdnWeekStart').val().length != 0) {
                    var d = new Date($('#hdnWeekStart').val());
                    d.setDate(d.getDate() - 6);

                    $('#hdnStartWeekDay').val(d.toString());
                    var Dates = new Date($('#hdnStartWeekDay').val()).getWeek();
                }
                else {
                    var Dates = new Date($('#hdnStartWeekDay').val()).getWeek();
                }
            }
            // alert(Dates[0].toLocaleDateString() + '--' + Dates[1].toLocaleDateString());

            var todayStart = new Date(Dates[0]);
            var ddStart = todayStart.getDate();
            var mmStart = todayStart.getMonth() + 1; //January is 0!

            var yyyyStart = todayStart.getFullYear();
            if (ddStart < 10) { ddStart = '0' + ddStart } if (mmStart < 10) { mmStart = '0' + mmStart }
            todayStart = mmStart + '/' + ddStart + '/' + yyyyStart;

            var todayEnd = new Date(Dates[1]);
            var ddEnd = todayEnd.getDate();
            var mmEnd = todayEnd.getMonth() + 1; //January is 0!

            var yyyyEnd = todayEnd.getFullYear();
            if (ddEnd < 10) { ddEnd = '0' + ddEnd } if (mmEnd < 10) { mmEnd = '0' + mmEnd }
            todayEnd = mmEnd + '/' + ddEnd + '/' + yyyyEnd;

            var MonthNumberStart = todayStart.split('/');
            var MonthNumberEnd = todayEnd.split('/');

            var startMonthName = cal_months_labels[MonthNumberStart[0] - 1] + ' ' + MonthNumberStart[1];

            var EndMonthName = cal_months_labels[MonthNumberEnd[0] - 1] + ' ' + MonthNumberEnd[1];

            var today = new Date($('#hdnStartWeekDay').val());

            var tomorrow = new Date(today.getTime() + (168 * 60 * 60 * 1000));

            if (tomorrow.getMonth() == 0) {
                var d = new Date($('#hdnWeekEnd').val());
                // $('#hdnYear').val((parseInt($('#hdnYear').val()) + 1).toString());
                $('#hdnStartWeekDay').val(d.toString());
                BindWeekTable(startMonthName, EndMonthName, yyyyStart, yyyyEnd);
            }
            else {

                BindWeekTable(startMonthName, EndMonthName, yyyyStart, yyyyEnd);
            }


            $('#lblMonthName').text(startMonthName + ', ' + yyyyStart + ' To ' + EndMonthName + ', ' + yyyyEnd);
            $('#hdnStartWeekDay').val(tomorrow);


            $('#hdnWeekStart').val(todayStart);
            $('#hdnWeekend').val(todayEnd);

            $('#tab-workflow').removeClass('selected');
            $('#tab-activity').addClass('selected');
            $('#tab-document').removeClass('selected');
            BindWeeklyActivities();
            if ($('#hdnisAdd').val() == "N") {
                $('p.day-num').find('a[target="name"]').attr('onclick', 'return false').addClass('areadOnly');
                $('p.day-num').find('img[title="Add activity"]').attr('onclick', 'return false');
            }
            if ($('#hdnIsUpdate').val() == "N") {
                $('p.day-num').find('a.issue').attr('onclick', 'return false').addClass('areadOnly');

            }
            if ($('#hdnIsDelete').val() == "N") {
                $('img[id="CloseUploaderImg"]').css('display', 'none');
            }
        }

        cal_Time_labels = ['12 am', '1 am', '2 am', '3 am',
                     '4 am', '5 am', '6 am', '7 am', '8 am',
                     '9 am', '10 am', '11 am', '12 pm', '1 pm', '2 pm', '3 pm',
                     '4 pm', '5 pm', '6 pm', '7 pm', '8 pm',
                     '9 pm', '10 pm', '11 pm'];

        function BindWeekTable(startMonthName, EndMonthName, YearStart, YearEnd) {
            var StartDate = startMonthName.split(' ');
            var EndDate = EndMonthName.split(' ');

            var dateArray = getDates(new Date(StartDate[1] + ' ' + StartDate[0] + ' ' + YearStart), new Date(EndDate[1] + ' ' + EndDate[0] + ' ' + YearEnd));

            var html = '<body class="controller-calendars action-show"><table class="cal">';
            html += '<thead><tr><th scope="col" style="width:5%">All Day</th>';

            for (var i = 0; i < dateArray.length; i++) {
                var now = new Date(dateArray[i]);


                html += '<th scope="col" style="width:16%">';
                html += cal_months_labels[now.getMonth()] + ' ' + now.getDate() + ',' + now.getFullYear();
                html += '</th>';
            }
            html += '</tr></thead>';

            var day = 1;

            for (var i = 0; i < 24; i++) {

                for (var j = 0; j <= 7; j++) {

                    if (j > 0) {
                        html += '<td  rowspan="1"><p class="day-num"><a href="#" target="name" onclick="opennewsletterWeek(this); return false"><img  title="Add activity"   style="margin-right:0px" src="../Images/addbig.png"></img>';

                        var MonthValue = $('#hdnMonth').val().length == 1 ? '0' + $('#hdnMonth').val() : $('#hdnMonth').val();

                        var DateValueTemp = day.toString().length == 1 ? '0' + day : day;

                        var d = new Date(dateArray[j - 1]);
                        var TDate = d.getDate();
                        var TMonth = d.getMonth() + 1;
                        var Tyear = d.getFullYear();
                        var FullDate = TDate + '/' + TMonth + '/' + Tyear;


                        // var now = new Date(dateArray[j - 1]);
                        var DateValue = TMonth + '/' + TDate + '/' + Tyear;
                        if (i <= 11) {
                            var DateToSplit = FullDate.split('/');
                            var Weekday = DateToSplit[0] + '/' + DateToSplit[1] + '/' + DateToSplit[2];
                            var Form = (i == 0 ? 12 : i) + ':' + '00';
                            var TimeFormat = 'AM'

                        }
                        else {
                            var DateToSplit = FullDate.split('/');
                            var Weekday = DateToSplit[0] + '/' + DateToSplit[1] + '/' + DateToSplit[2];
                            if (i == 12) {
                                var Form = '12:' + '00';

                            }
                            else {
                                var Form = (i - 12).toString() + ':' + '00';
                            }
                            var TimeFormat = 'PM'
                            // alert(Weekday + Form);

                        }


                        html += '<input type="hidden" id="hdnWeekId" value=' + Weekday + '>';
                        html += '<input type="hidden" id="hdnTimeId" value=' + Form + '>';
                        html += '<input type="hidden" id="hdnTimeFormatId" value=' + TimeFormat + '>';

                        day++;

                        html += '</p></a></td>';
                    }
                    else {
                        html += '<td rowspan="1"><p class="day-num">';

                        var MonthValue = $('#hdnMonth').val().length == 1 ? '0' + $('#hdnMonth').val() : $('#hdnMonth').val();

                        var DateValueTemp = day.toString().length == 1 ? '0' + day : day;

                        var DateValue = MonthValue + '/' + DateValueTemp + '/' + $('#hdnYear').val();

                        html += "<b>" + cal_Time_labels[i] + "</b>";

                        html += '<input type="hidden" id="hdnDateVal" value=' + DateValue + '>';
                        day++;
                        html += '</p></td>';
                    }
                }
                html += '</tr><tr>';
            }
            html += '</tr></table></div></body>';
            $('#Calendar').hide();
            $('#CalendarWeek').show();
            $('#CalendarWeek').html(html);
            if ($('#hdnisAdd').val() == "N") {
                $('p.day-num').find('a[target="name"]').attr('onclick', 'return false').addClass('areadOnly');
                $('p.day-num').find('img[title="Add activity"]').attr('onclick', 'return false');

            }
            if ($('#hdnIsUpdate').val() == "N") {
                $('p.day-num').find('a.issue').attr('onclick', 'return false').addClass('areadOnly');

            }
            if ($('#hdnIsDelete').val() == "N") {
                $('img[id="CloseUploaderImg"]').css('display', 'none');
            }
        }
        Date.prototype.addHours = function (hours) {
            this.setHours(this.getHours() + hours);
            return this;
        };

        function NavigateNextWeek(obj) {
            $('#CalendarWeek').html('');
            ShowWeekTab('next');
        }
        function NavigateBackWeek(obj) {
            $('#CalendarWeek').html('');
            ShowWeekTab('back');
        }
        function NavigateBackDay(obj) {
            $('#CalendarDay').html('');
            ShowDayTab('back');
        }

        function NavigateNextDay(obj) {
            $('#CalendarDay').html('');
            ShowDayTab('next');
        }

        function getFormattedDate(input) {
            var pattern = /(.*?)\/(.*?)\/(.*?)$/;
            var result = input.replace(pattern, function (match, p1, p2, p3) {
                var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                return (p2 < 10 ? "0" + p2 : p2) + " " + months[(p1 - 1)] + " " + p3;
            });
            //alert(result);
        }

        Date.prototype.addDays = function (days) {
            var dat = new Date(this.valueOf())
            dat.setDate(dat.getDate() + days);
            return dat;
        }

        function getDates(startDate, stopDate) {
            // alert(startDate + '--' + stopDate);
            var dateArray = new Array();
            var currentDate = startDate;
            while (currentDate <= stopDate) {
                dateArray.push(currentDate)
                currentDate = currentDate.addDays(1);
            }
            return dateArray;
        }
        //Bind Weekly Activities
        function BindWeeklyActivities() {
            $.ajax({
                type: 'POST',
                contentType: "text/html; charset=utf-8",
                url: "../Handlers/BindActivities.ashx?Op=Week&StartDate=" + $('#hdnWeekStart').val() + "&EndDate=" + $('#hdnWeekend').val() + "&UserId=" + $('#hdnUserId').val(),
                data: {},
                datatype: "html",
                async: false,
                success: function (response) {
                    $('#MyDivWeek').html('');
                    $('#MyDivWeek').html(response);

                    $('#CalendarWeek').find('tbody tr').find('td').each(function () {
                        var obj = $(this);
                        var pp = $(this).find('input:eq(0)').val();
                        var Time = $(this).find('input:eq(1)').val();
                        var Format = $(this).find('input:eq(2)').val();
                        var Timeformat = Time + ' ' + Format;
                        $('#MyDivWeek').find('table tbody tr').find('td:eq(4)').each(function () {
                            var activityText = $(this).prev('td').html();
                            var IsOnCustomerPortal = $(this).parent('tr').find('td:eq(13)').html() == 'Y' ? 'Yes' : 'No';
                            var ReminderDate = $(this).parent('tr').find('td:eq(14)').html() == '01-Jan-0001 12:00 AM' ? '' : $(this).parent('tr').find('td:eq(14)').html();
                            var ActivityType = $(this).parent('tr').find('td:eq(2)').html();
                            var ActivityId = $(this).parent('tr').find('td:eq(0)').html();
                            var Status = $(this).parent('tr').find('td:eq(11)').html();
                            var AddedByName = $(this).parent('tr').find('td:eq(7)').html();
                            var TimeFormatToCompare = $(this).parent('tr').find('td:eq(5)').html();
                            var ActivityDateDefaultFormat = $(this).parent('tr').find('td:eq(6)').html();
                            var ContractIdtext = $(this).parent('tr').find('td:eq(15)').html();
                            var isEditable = $(this).parent('tr').find('td:eq(16)').html();
                            var FileUpload = $(this).parent('tr').find('td:eq(17)').html();
                            var ReqID = $(this).parent('tr').find('td:eq(18)').html();
                            var ContractTypeId = $(this).parent('tr').find('td:eq(19)').html();

                            var IsOCR = $(this).parent('tr').find('td:eq(20)').html();
                            var ContractTypeIds = $(this).parent('tr').find('td:eq(21)').html();

                            var strDate = $(this).html();
                            var CloseClass = ""

                            var Pd = pp.split('/');
                            var Sd = strDate.split('/');

                            try {
                                var TT = TimeFormatToCompare.toString().split(':');
                                var TTFormat = TT[1].split(' ');
                                var TTTimeFormat = parseInt(TT[0]).toString() + ' ' + TTFormat[1];
                            }
                            catch (err) {
                            }

                            try {
                                var TTA = Timeformat.toString().split(':');
                                var TTFormatA = TTA[1].split(' ');
                                var TTTimeFormatA = parseInt(TTA[0]).toString() + ' ' + TTFormatA[1];
                            }
                            catch (err) {
                            }
                            try {
                                // alert(TTTimeFormat.toLocaleLowerCase() + '---' + TTTimeFormatA.toLocaleLowerCase());
                            }
                            catch (err)
                        { }
                            try {
                                if (TTTimeFormat.toLocaleLowerCase() == TTTimeFormatA.toLocaleLowerCase() && parseInt(Pd[0]) == parseInt(Sd[0]) && parseInt(Pd[1]) == parseInt(Sd[1]) && parseInt(Pd[2]) == parseInt(Sd[2]) && pp != 'NaN/NaN/NaN' && strDate != 'NaN/NaN/NaN') {
                                    if (Status == 'Open') {
                                        CloseClass = "starting";
                                    }
                                    else {
                                        CloseClass = "ending";
                                    }
                                    //var TextHtml = '<div style="margin-top:10px" id=' + ActivityId + '   class="issue status-4 priority-2 child ' + CloseClass + ' tooltip"> <img id="CloseUploaderImg" align="right" src="../Images/cross_cal.png" style="margin-top:-5px;margin-right:-8px" onclick="return DeleteActivity(' + ActivityId + ');"></img><br> <a onclick="ShowActivityDetails(' + ActivityId + ' )"  href="#" class="issue status-4 priority-2 child">Activity details</a>:' + activityText + '<span class="tip"><a onclick="ShowActivityDetails(' + ActivityId + ' )" href="#" class="issue status-4 priority-2 child">Activity Description</a>: ' + activityText + '<br><br><strong>Added by</strong>: <a onclick="ShowActivityDetails(' + ActivityId + ' )" href="#">' + AddedByName + '</a><br> <strong>Activity date</strong>: <a onclick="ShowActivityDetails(' + ActivityId + ' )" href="#">' + $(this).html() + '</a><br><strong>Activity type</strong>:' + ActivityType + '<br><strong>Status </strong>:' + Status + '<br><strong>Activity Remarks</strong>:' + activityText + '<br><strong>Reminder date</strong>: ' + ReminderDate + '<br>  <strong>Post on customer portal</strong>: ' + IsOnCustomerPortal + '</span>  </div>'
                                    var TextHtml = '';
                                    if (isEditable == 'Y') {
                                        TextHtml = '<div style="margin-top:10px" id=' + ActivityId + '   class="issue status-4 priority-2 child ' + CloseClass + ' tooltip"> <img id="CloseUploaderImg"  ContractTypeId=' + ContractTypeId + ' filepath=' + escape(FileUpload) + ' align="right" src="../Images/cross_cal.png" style="margin-top:-5px;margin-right:-8px" onclick="return DeleteActivity(' + ActivityId + ',this);" IsOCR = ' + IsOCR + '></img><br> <a onclick="ShowWeekActivityDetails(' + ActivityId + ' )"  href="#" class="issue status-4 priority-2 child">Activity details</a>:' + activityText + '<span class="tip"  style="margin-left:-200px"><a onclick="ShowWeekActivityDetails(' + ActivityId + ' )" href="#" class="issue status-4 priority-2 child">Activity Description</a>: ' + activityText + '<br><strong>Contract ID</strong>: <a onclick="GoContractToDetails(' + ReqID + ',' + ContractTypeId + ',this )" href="#" class="issue status-4 priority-2 child" >' + ContractIdtext + '</a><br><br><strong>Added by</strong>:' + AddedByName + '<br> <strong>Activity date</strong>:' + ActivityDateDefaultFormat + '<br><strong>Activity type</strong>:' + ActivityType + '<br><strong>Status </strong>:' + Status + '<br><strong>Activity Remarks</strong>:' + activityText + '<br><strong>Reminder date</strong>: ' + ReminderDate + '<br>  </span>  </div>'
                                    }
                                    else {
                                        TextHtml = '<div style="margin-top:10px" id=' + ActivityId + '   class="issue status-4 priority-2 child ' + CloseClass + ' tooltip"> <img id="CloseUploaderImg"  ContractTypeId=' + ContractTypeId + ' filepath=' + escape(FileUpload) + ' align="right" src="../Images/cross_cal.png" style="margin-top:-5px;margin-right:-8px" onclick="return false;" IsOCR = ' + IsOCR + '></img><br> <a onclick="return false;"  href="#" class="issue status-4 priority-2 child areadOnly">Activity details</a>:' + activityText + '<span class="tip"  style="margin-left:-200px"><a onclick="return false;" href="#" class="issue status-4 priority-2 child areadOnly">Activity Description</a>: ' + activityText + '<br><strong>Contract ID</strong>: <a onclick="GoContractToDetails(' + ReqID + ',' + ContractTypeId + ',this )" href="#" class="issue status-4 priority-2 child" >' + ContractIdtext + '</a><br><br><strong>Added by</strong>:' + AddedByName + '<br> <strong>Activity date</strong>:' + ActivityDateDefaultFormat + '<br><strong>Activity type</strong>:' + ActivityType + '<br><strong>Status </strong>:' + Status + '<br><strong>Activity Remarks</strong>:' + activityText + '<br><strong>Reminder date</strong>: ' + ReminderDate + '<br>  </span>  </div>'
                                    }

                                    obj.find('a[target="name"]').after(TextHtml);
                                }
                            }
                            catch (err) {
                            }
                        });
                    });

                    if (response != '2') {
                        isValid = true;
                    }
                    else {

                        //alert('Stage name already exists.');
                        isValid = false;
                    }
                },
                error: function () {
                    alert("some problem in loading data");
                    return false;
                }
            });

            if ($('#hdnisAdd').val() == "N") {
                $('p.day-num').find('a[target="name"]').attr('onclick', 'return false').addClass('areadOnly');
                $('p.day-num').find('img[title="Add activity"]').attr('onclick', 'return false');
            }

            if ($('#hdnIsUpdate').val() == "N") {
                $('p.day-num').find('a.issue').attr('onclick', 'return false').addClass('areadOnly');
            }

            if ($('#hdnIsDelete').val() == "N") {
                $('img[id="CloseUploaderImg"]').css('display', 'none');
            }
        }
        //Bind Daywise Activities

        function BindDayActivities() {
            //alert($('#hdnDatePointer').val());
            var d = new Date($('#hdnDatePointer').val());
            var TDate = d.getDate();
            var TMonth = d.getMonth() + 1;
            var Tyear = d.getFullYear();
            var FullDate = TMonth + '/' + TDate + '/' + Tyear;
            //        var DateToSplit = $('#hdnDatePointer').val().split('/');
            //        var Weekday = DateToSplit[0] + '/' + DateToSplit[1] + '/' + DateToSplit[2];

            $.ajax({
                type: 'POST',
                contentType: "text/html; charset=utf-8",
                url: "../Handlers/BindActivities.ashx?Op=Day&CurrentDay=" + FullDate + "&UserId=" + $('#hdnUserId').val(),
                data: {},
                datatype: "html",
                async: false,
                success: function (response) {

                    $('#MyDivDay').html('');
                    $('#MyDivDay').html(response);
                    var DIvCounter = 0;
                    $('#CalendarDay').find('tbody tr').find('td').each(function () {
                        var obj = $(this);
                        var pp = $(this).find('input:eq(0)').val();
                        var Time = $(this).find('input:eq(1)').val();
                        var Format = $(this).find('input:eq(2)').val();
                        var Timeformat = Time + ' ' + Format;
                        $('#MyDivDay').find('table tbody tr').find('td:eq(4)').each(function () {
                            var activityText = $(this).prev('td').html();
                            var IsOnCustomerPortal = $(this).parent('tr').find('td:eq(13)').html() == 'Y' ? 'Yes' : 'No';
                            var ReminderDate = $(this).parent('tr').find('td:eq(14)').html() == '01-Jan-0001 12:00 AM' ? '' : $(this).parent('tr').find('td:eq(14)').html();
                            var ActivityType = $(this).parent('tr').find('td:eq(2)').html();
                            var ActivityId = $(this).parent('tr').find('td:eq(0)').html();
                            var Status = $(this).parent('tr').find('td:eq(11)').html();
                            var AddedByName = $(this).parent('tr').find('td:eq(7)').html();
                            var TimeFormatToCompare = $(this).parent('tr').find('td:eq(5)').html();
                            var ActivityDateDefaultFormat = $(this).parent('tr').find('td:eq(6)').html();
                            var ContractIdtext = $(this).parent('tr').find('td:eq(15)').html();
                            var isEditable = $(this).parent('tr').find('td:eq(16)').html();
                            var FileUpload = $(this).parent('tr').find('td:eq(17)').html();
                            var ReqID = $(this).parent('tr').find('td:eq(18)').html();
                            var ContractTypeId = $(this).parent('tr').find('td:eq(19)').html();
                            var IsOCR = $(this).parent('tr').find('td:eq(20)').html();

                            var strDate = $(this).html();
                            var CloseClass = ""

                            var Pd = pp.split('/');
                            var Sd = strDate.split('/');

                            try {
                                var TT = TimeFormatToCompare.toString().split(':');
                                var TTFormat = TT[1].split(' ');
                                var TTTimeFormat = parseInt(TT[0]).toString() + ' ' + TTFormat[1];
                            }
                            catch (err) {
                            }

                            try {
                                var TTA = Timeformat.toString().split(':');
                                var TTFormatA = TTA[1].split(' ');
                                var TTTimeFormatA = parseInt(TTA[0]).toString() + ' ' + TTFormatA[1];
                            }
                            catch (err) {
                            }
                            try {

                                // alert(TTTimeFormat.toLocaleLowerCase() + '---' + TTTimeFormatA.toLocaleLowerCase());
                            }
                            catch (err)
                        { }
                            try {
                                if (TTTimeFormat.toLocaleLowerCase() == TTTimeFormatA.toLocaleLowerCase() && parseInt(Pd[0]) == parseInt(Sd[0]) && parseInt(Pd[1]) == parseInt(Sd[1]) && parseInt(Pd[2]) == parseInt(Sd[2]) && pp != 'NaN/NaN/NaN' && strDate != 'NaN/NaN/NaN') {

                                    if (Status == 'Open') {

                                        CloseClass = "starting";
                                    }
                                    else {
                                        CloseClass = "ending";
                                    }
                                    //                                var TextHtml = '<div style="margin-top:10px" id=' + ActivityId + '   class="issue status-4 priority-2 child ' + CloseClass + ' tooltip"> <img id="CloseUploaderImg" align="right" src="../Images/cross_cal.png" style="margin-top:-5px;margin-right:-8px" onclick="return DeleteActivity(' + ActivityId + ');"></img><br> <a onclick="ShowActivityDetails(' + ActivityId + ' )"  href="#" class="issue status-4 priority-2 child">Activity details</a>:' + activityText + '<span class="tip"><a onclick="ShowActivityDetails(' + ActivityId + ' )" href="#" class="issue status-4 priority-2 child">Activity Description</a>: ' + activityText + '<br><br><strong>Added by</strong>: <a onclick="ShowActivityDetails(' + ActivityId + ' )" href="#">' + AddedByName + '</a><br> <strong>Activity date</strong>: <a onclick="ShowActivityDetails(' + ActivityId + ' )" href="#">' + $(this).html() + '</a><br><strong>Activity type</strong>:' + ActivityType + '<br><strong>Status </strong>:' + Status + '<br><strong>Activity Remarks</strong>:' + activityText + '<br><strong>Reminder date</strong>: ' + ReminderDate + '<br>  <strong>Post on customer portal</strong>: ' + IsOnCustomerPortal + '</span>  </div>'
                                    var TextHtml = '';
                                    //                                    if (isEditable == 'Y') {
                                    //                                        var TextHtml = '<div style="width:25%;margin-top:10px" id=' + ActivityId + '   class="issue status-4 priority-2 child ' + CloseClass + ' tooltip"> <img id="CloseUploaderImg"  ContractTypeId=' + ContractTypeId + ' filepath=' + escape(FileUpload) + ' align="right" src="../Images/cross_cal.png" style="margin-top:-5px;margin-right:-8px" onclick="return DeleteActivity(' + ActivityId + ',this);"></img><br> <a onclick="ShowWeekActivityDetails(' + ActivityId + ' )"  href="#" class="issue status-4 priority-2 child">Activity details</a>:' + activityText + '<span class="tip"  style="margin-left:-200px"><a onclick="ShowWeekActivityDetails(' + ActivityId + ' )" href="#" class="issue status-4 priority-2 child">Activity Description</a>: ' + activityText + '<br><strong>Contract ID</strong>: ' + ContractIdtext + '<br><br><strong>Added by</strong>:' + AddedByName + '<br> <strong>Activity date</strong>:' + ActivityDateDefaultFormat + '<br><strong>Activity type</strong>:' + ActivityType + '<br><strong>Status </strong>:' + Status + '<br><strong>Activity Remarks</strong>:' + activityText + '<br><strong>Reminder date</strong>: ' + ReminderDate + '<br>  </span>  </div>'
                                    //                                    }
                                    //                                    else {
                                    //                                        var TextHtml = '<div style="width:25%;margin-top:10px" id=' + ActivityId + '   class="issue status-4 priority-2 child ' + CloseClass + ' tooltip"> <img id="CloseUploaderImg"  ContractTypeId=' + ContractTypeId + ' filepath=' + escape(FileUpload) + ' align="right" src="../Images/cross_cal.png" style="margin-top:-5px;margin-right:-8px" onclick="return false;"></img><br> <a onclick="return false;"  href="#" class="issue status-4 priority-2 child areadOnly">Activity details</a>:' + activityText + '<span class="tip"  style="margin-left:-200px"><a onclick="return false;" href="#" class="issue status-4 priority-2 child areadOnly">Activity Description</a>: ' + activityText + '<br><strong>Contract ID</strong>: ' + ContractIdtext + '<br><br><strong>Added by</strong>:' + AddedByName + '<br> <strong>Activity date</strong>:' + ActivityDateDefaultFormat + '<br><strong>Activity type</strong>:' + ActivityType + '<br><strong>Status </strong>:' + Status + '<br><strong>Activity Remarks</strong>:' + activityText + '<br><strong>Reminder date</strong>: ' + ReminderDate + '<br>  </span>  </div>'
                                    //                                    }

                                    if (isEditable == 'Y') {

                                        if (ReqID != "0") {
                                            var TextHtml = '<div style="width:25%;margin-top:10px" id=' + ActivityId + '   class="issue status-4 priority-2 child ' + CloseClass + ' tooltip"> <img id="CloseUploaderImg" filepath=' + escape(FileUpload) + ' align="right" src="../Images/cross_cal.png" style="margin-top:-5px;margin-right:-8px" onclick="return DeleteActivity(' + ActivityId + ',this);" IsOCR = ' + IsOCR + '></img><br> <a onclick="ShowWeekActivityDetails(' + ActivityId + ' )"  href="#" class="issue status-4 priority-2 child">Activity details</a>:' + activityText + '<span class="tip"  style="margin-left:-200px"><a onclick="ShowWeekActivityDetails(' + ActivityId + ' )" href="#" class="issue status-4 priority-2 child">Activity Description</a>: ' + activityText + '<br><br><strong>Contract ID</strong>:  <a onclick="GoContractToDetails(' + ReqID + ',' + ContractTypeId + ',this )" href="#" class="issue status-4 priority-2 child" >' + ContractIdtext + '</a><br><br><strong>Added by</strong>:' + AddedByName + '<br> <strong>Activity date</strong>:' + ActivityDateDefaultFormat + '<br><strong>Activity type</strong>:' + ActivityType + '<br><strong>Status </strong>:' + Status + '<br><strong>Activity Remarks</strong>:' + activityText + '<br><strong>Reminder date</strong>: ' + ReminderDate + '<br>  </span>  </div>'
                                        }
                                        else {
                                            var TextHtml = '<div style="width:25%;margin-top:10px" id=' + ActivityId + '   class="issue status-4 priority-2 child ' + CloseClass + ' tooltip"> <img id="CloseUploaderImg" filepath=' + escape(FileUpload) + ' align="right" src="../Images/cross_cal.png" style="margin-top:-5px;margin-right:-8px" onclick="return DeleteActivity(' + ActivityId + ',this);" IsOCR = ' + IsOCR + '></img><br> <a onclick="ShowWeekActivityDetails(' + ActivityId + ' )"  href="#" class="issue status-4 priority-2 child">Activity details</a>:' + activityText + '<span class="tip"  style="margin-left:-200px"><a onclick="ShowWeekActivityDetails(' + ActivityId + ' )" href="#" class="issue status-4 priority-2 child">Activity Description</a>: ' + activityText + '<br><br><strong>Contract ID</strong>:  <a onclick="GoContractToDetails(' + ReqID + ',' + ContractTypeId + ',this )" href="#" class="issue status-4 priority-2 child" >' + ContractIdtext + '</a><br><br><strong>Added by</strong>:' + AddedByName + '<br> <strong>Activity date</strong>:' + ActivityDateDefaultFormat + '<br><strong>Activity type</strong>:' + ActivityType + '<br><strong>Status </strong>:' + Status + '<br><strong>Activity Remarks</strong>:' + activityText + '<br><strong>Reminder date</strong>: ' + ReminderDate + '<br>  </span>  </div>'
                                        }

                                        if (ContractTypeId == 'Y') {
                                            var TextHtml = '<div style="width:25%;margin-top:10px" id=' + ActivityId + '   class="issue status-4 priority-2 child ' + CloseClass + ' tooltip"> <img id="CloseUploaderImg" filepath=' + escape(FileUpload) + ' align="right" src="../Images/cross_cal.png" style="margin-top:-5px;margin-right:-8px" onclick="return DeleteActivity(' + ActivityId + ',this);" IsOCR = ' + IsOCR + '></img><br> <a onclick="ShowWeekActivityDetails(' + ActivityId + ' )"  href="#" class="issue status-4 priority-2 child">Activity details</a>:' + activityText + '<span class="tip"  style="margin-left:-200px"><a onclick="ShowWeekActivityDetails(' + ActivityId + ' )" href="#" class="issue status-4 priority-2 child">Activity Description</a>: ' + activityText + '<br><br><strong>Contract ID</strong>:  <a onclick="GoContractToDetails(' + ReqID + ',' + ContractTypeId + ',this )" href="#" class="issue status-4 priority-2 child" >' + ContractIdtext + '</a><br><br><strong>Added by</strong>:' + AddedByName + '<br> <strong>Activity date</strong>:' + ActivityDateDefaultFormat + '<br><strong>Activity type</strong>:' + ActivityType + '<br><strong>Status </strong>:' + Status + '<br><strong>Activity Remarks</strong>:' + activityText + '<br><strong>Reminder date</strong>: ' + ReminderDate + '<br>  </span>  </div>'
                                        }
                                        else {
                                            var TextHtml = '<div style="width:25%;margin-top:10px" id=' + ActivityId + '   class="issue status-4 priority-2 child ' + CloseClass + ' tooltip"> <img id="CloseUploaderImg" filepath=' + escape(FileUpload) + ' align="right" src="../Images/cross_cal.png" style="margin-top:-5px;margin-right:-8px" onclick="return DeleteActivity(' + ActivityId + ',this);" IsOCR = ' + IsOCR + '></img><br> <a onclick="ShowWeekActivityDetails(' + ActivityId + ' )"  href="#" class="issue status-4 priority-2 child ">Activity details</a>:' + activityText + '<span class="tip"  style="margin-left:-200px"><a onclick="ShowWeekActivityDetails(' + ActivityId + ' )"  href="#" class="issue status-4 priority-2 child ">Activity Description</a>: ' + activityText + '<br><strong>Contract ID</strong>:  <a onclick="GoContractToDetails(' + ReqID + ',' + ContractTypeId + ',this )" href="#" class="issue status-4 priority-2 child" >' + ContractIdtext + '</a><br><br><strong>Added by</strong>:' + AddedByName + '<br> <strong>Activity date</strong>:' + ActivityDateDefaultFormat + '<br><strong>Activity type</strong>:' + ActivityType + '<br><strong>Status </strong>:' + Status + '<br><strong>Activity Remarks</strong>:' + activityText + '<br><strong>Reminder date</strong>: ' + ReminderDate + '<br>  </span>  </div>'
                                        }
                                    }
                                    else {
                                        var TextHtml = '<div style="width:25%;margin-top:10px" id=' + ActivityId + '   class="issue status-4 priority-2 child ' + CloseClass + ' tooltip"> <img id="CloseUploaderImg" filepath=' + escape(FileUpload) + ' align="right" src="../Images/cross_cal.png" style="margin-top:-5px;margin-right:-8px" onclick="return false;" IsOCR = ' + IsOCR + '></img><br> <a onclick="return false;"  href="#" class="issue status-4 priority-2 child areadOnly">Activity details</a>:' + activityText + '<span class="tip"  style="margin-left:-200px"><a onclick="return false;" href="#" class="issue status-4 priority-2 child areadOnly">Activity Description</a>: ' + activityText + '<br><strong>Contract ID</strong>: <a onclick="return false;" href="#" class="issue status-4 priority-2 child areadOnly">' + ContractIdtext + '</a><br><br><strong>Added by</strong>:' + AddedByName + '<br> <strong>Activity date</strong>:' + ActivityDateDefaultFormat + '<br><strong>Activity type</strong>:' + ActivityType + '<br><strong>Status </strong>:' + Status + '<br><strong>Activity Remarks</strong>:' + activityText + '<br><strong>Reminder date</strong>: ' + ReminderDate + '<br>  </span>  </div>'
                                        if (ContractTypeId == 'Y') {
                                            var TextHtml = '<div style="width:25%;margin-top:10px" id=' + ActivityId + '   class="issue status-4 priority-2 child ' + CloseClass + ' tooltip"> <img id="CloseUploaderImg" filepath=' + escape(FileUpload) + ' align="right" src="../Images/cross_cal.png" style="margin-top:-5px;margin-right:-8px" onclick="return false;" IsOCR = ' + IsOCR + '></img><br> <a onclick="ShowWeekActivityDetails(' + ActivityId + ' )"  href="#" class="issue status-4 priority-2 child">Activity details</a>:' + activityText + '<span class="tip"  style="margin-left:-200px"><a onclick="return false;"  href="#" class="issue status-4 priority-2 child areadOnly">Activity Description</a>: ' + activityText + '<br><br><strong>Contract ID</strong>:  <a onclick="GoContractToDetails(' + ReqID + ',' + ContractTypeId + ',this )" href="#" class="issue status-4 priority-2 child" >' + ContractIdtext + '</a><br><br><strong>Added by</strong>:' + AddedByName + '<br> <strong>Activity date</strong>:' + ActivityDateDefaultFormat + '<br><strong>Activity type</strong>:' + ActivityType + '<br><strong>Status </strong>:' + Status + '<br><strong>Activity Remarks</strong>:' + activityText + '<br><strong>Reminder date</strong>: ' + ReminderDate + '<br>  </span>  </div>'
                                        }
                                        else {
                                            var TextHtml = '<div style="width:25%;margin-top:10px" id=' + ActivityId + '   class="issue status-4 priority-2 child ' + CloseClass + ' tooltip"> <img id="CloseUploaderImg" filepath=' + escape(FileUpload) + ' align="right" src="../Images/cross_cal.png" style="margin-top:-5px;margin-right:-8px" onclick="return false;" IsOCR = ' + IsOCR + '></img><br> <a onclick="ShowWeekActivityDetails(' + ActivityId + ' )"  href="#" class="issue status-4 priority-2 child ">Activity details</a>:' + activityText + '<span class="tip"  style="margin-left:-200px"><a onclick="return false;"  href="#" class="issue status-4 priority-2 child areadOnly">Activity Description</a>: ' + activityText + '<br><strong>Contract ID</strong>: <a onclick="GoContractToDetails(' + ReqID + ',' + ContractTypeId + ',this )" href="#" class="issue status-4 priority-2 child" >' + ContractIdtext + '</a><br><br><strong>Added by</strong>:' + AddedByName + '<br> <strong>Activity date</strong>:' + ActivityDateDefaultFormat + '<br><strong>Activity type</strong>:' + ActivityType + '<br><strong>Status </strong>:' + Status + '<br><strong>Activity Remarks</strong>:' + activityText + '<br><strong>Reminder date</strong>: ' + ReminderDate + '<br>  </span>  </div>'
                                        }
                                    }

                                    obj.find('a[target="name"]').after(TextHtml);
                                    DIvCounter++;
                                }
                            }
                            catch (err) {

                            }
                        });
                    });
                    if (response != '2') {
                        isValid = true;
                    }
                    else {

                        //alert('Stage name already exists.');
                        isValid = false;
                    }
                },
                error: function (response) {

                    alert("some problem in loading data\n" + response.error);
                    return false;
                }
            });

            if ($('#hdnisAdd').val() == "N") {
                $('p.day-num').find('a[target="name"]').attr('onclick', 'return false').addClass('areadOnly');
                $('p.day-num').find('img[title="Add activity"]').attr('onclick', 'return false');
            }

            if ($('#hdnIsUpdate').val() == "N") {
                $('p.day-num').find('a.issue').attr('onclick', 'return false').addClass('areadOnly');
            }

            if ($('#hdnIsDelete').val() == "N") {
                $('img[id="CloseUploaderImg"]').css('display', 'none');
            }
        }
    </script>
</asp:Content>
