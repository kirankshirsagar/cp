﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="CalendarAgenda.aspx.cs" Inherits="Calendar_CalendarAgenda" %>

<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<%--<%@ Register Src="../UserControl/masterrightlinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>--%>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridSelect.js" type="text/javascript"></script>
    <script src="../CommonScripts/GridNavigation.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <link href="../CalendarCSS/jquery-ui-1.8.21.css" media="all" rel="stylesheet" type="text/css">
    <script src="../CalendarCSS/jquery-1.7.2-ui-1.8.21-ujs-2.0.3.js" type="text/javascript"></script>
    <link href="../JQueryValidations/dhtmlwindow.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/dhtmlwindow.js" type="text/javascript"></script>
    <link href="../JQueryValidations/modal.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/modal.js" type="text/javascript"></script>
    <script type="text/javascript">
        $("#calendarLink").addClass("menulink");
        $("#calendarTab").addClass("selectedtab");
    </script>
    <div id="rightlinks" style="display: none;">
        <uc3:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
    <div id="dvMesssage" class="flash notice" style="display: none">
    </div>
    <h2>
        Agenda</h2>
    <input id="hdnPrimeIds" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <a href="Calendar.aspx">Back to Calendar</a>
    <div style="margin: 0; padding: 0; display: inline">
        <div id="query_form_content" class="hide-when-print">
            <fieldset id="filters" class="collapsible">
                <legend onclick="toggleFieldset(this);">Filters</legend>
                <div style="">
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td>
                                    <table id="filters-table" width="100%">
                                        <tbody>
                                            <tr>
                                                <td width="10%">
                                                    Keywords :
                                                </td>
                                                <td width="90%">
                                                    <input id="txtSearch" clientidmode="Static" runat="server" type="text" maxlength="50"
                                                        onkeydown="return (event.keyCode!=13);" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </fieldset>
        </div>
        <p class="buttons hide-when-print">
            <asp:LinkButton ID="btnSearch" runat="server" OnClientClick="return searchClick();"
                CssClass="icon icon-checked" OnClick="btnSearch_Click">Filter</asp:LinkButton>
            <asp:LinkButton ID="btnShowAll" runat="server" OnClientClick="return resetClick();"
                CssClass="icon icon-reload" OnClick="btnSearch_Click">Reset Filter</asp:LinkButton>
            <asp:LinkButton ID="btnAddRecord" CssClass="icon icon-add" runat="server" OnClientClick="opennewsletter(this); return false">Add new activity</asp:LinkButton>
            <%--<asp:LinkButton ID="btnAddRecord" CssClass="icon icon-add" runat="server">Add new activity</asp:LinkButton>--%>
            <asp:LinkButton ID="btnDelete" CssClass="icon icon-del" runat="server" OnClientClick="return GetSelectedItems('D');"
                OnClick="btnDelete_Click">Delete</asp:LinkButton>
        </p>
        <div class="autoscroll">
            <table width="100%">
                <asp:Repeater ID="rptCityMaster" runat="server"  OnItemDataBound ="rptCityMaster_ItemDataBound">
                    <HeaderTemplate>
                        <table id="masterDataTable" class="masterTable list issues" width="100%">
                            <thead>
                                <tr>
                                    <th style="text-align: left; padding-left: 2px;" width="1%">
                                        <input id="chkSelectAll" type="checkbox" class="checkbox maincheckbox" onkeydown="return (event.keyCode!=13);" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </th>
                                    <th width="0%" style="display: none;">
                                        Activity Id
                                    </th>
                                    <th width="20%">
                                        <asp:LinkButton ID="btnSortActivityDate" OnClick="btnSort_Click" CssClass="sort desc"
                                            runat="server">Activity Date</asp:LinkButton>
                                    </th>
                                    <th width="30%">
                                        <asp:LinkButton ID="btnSortRemark" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Activity remark</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="btnSortType" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Activity type</asp:LinkButton>
                                    </th>
                                    <th width="10%">
                                        <asp:LinkButton ID="btnSortStatus" OnClick="btnSort_Click" CssClass="sort desc" runat="server">Activity status</asp:LinkButton>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: left; padding-bottom: 2px">
                                <input type="checkbox" ID="chkAgenda" runat="server" clientidmode="Static" onchange="SetMainCheckbox(this);" class="mid-margin-left"
                                    onkeydown="return (event.keyCode!=13);" />
                            </td>
                            <td style="display: none">
                                <asp:Label ID="lblActivityId" runat="server" ClientIDMode="Static" Text='<%#Eval("ActivityId") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Label1" runat="server" ClientIDMode="Static" Text='<%#Eval("ActivityTime") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:LinkButton ID="imgEdit" runat="server"  isEditable='<%#Eval("isEditable") %>' ReqID='<%#Eval("ReqID") %>'  ActId='<%#Eval("ActivityId") %>' OnClientClick="ShowActivityDetails(this); return false"><%#Eval("ActivityText")%></asp:LinkButton>
                                <%--<input type="hidden" runat="server" id="isEditable" value='<%#Eval("isEditable") %>' />--%>
                                <asp:HiddenField ID="isEditable" runat="server" Value='<%#Eval("isEditable") %>' ClientIDMode="Static" />
                            </td>
                            <td>
                                <asp:Label ID="lclCountryName" runat="server" ClientIDMode="Static" Text='<%#Eval("ActivityTypeName") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Label2" runat="server" ClientIDMode="Static" Text='<%#Eval("ActivityStatusName") %>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
            <asp:PlaceHolder runat="server" ID="Placeholder1">
                <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
            </asp:PlaceHolder>
        </div>
        <table style="display: none" width="100%">
            <tr>
                <td align="left" width="10%">
                </td>
                <td runat="server" id="actinacttd" style="padding-right: 60%" align="left" width="90%">
                    <input id="rdActive" name="ActiveInactive" runat="server" type="radio" />Active
                    <input id="rdInactive" name="ActiveInactive" runat="server" type="radio" />Inactive
                    <asp:Button ID="btnChangeStatus" runat="server" Text="Status" OnClientClick="return GetSelectedItems('S');"
                        OnClick="btnChangeStatus_Click" />
                </td>
            </tr>
        </table>
        <script type="text/javascript">

            function opennewsletter(obj) {
                if ("<%=Add%>" == 'Y') {
                    $("html, body").scrollTop(0);

                    var todayStart = new Date(new Date());
                    var ddStart = todayStart.getDate();
                    var mmStart = todayStart.getMonth() + 1; //January is 0!

                    var yyyyStart = todayStart.getFullYear();
                    if (ddStart < 10) { ddStart = '0' + ddStart } if (mmStart < 10) { mmStart = '0' + mmStart }
                    todayStart = mmStart + '/' + ddStart + '/' + yyyyStart;

                    emailwindow = dhtmlmodal.open('FieldDetails', 'iframe', 'EventAdd.aspx?ActivityDate=' + todayStart, '', 'width=600px,scrollbars=no,height=500px,center=1,resize=0"');
                    emailwindow.onclose = function () { //Define custom code to run when window is closed
                        var theform = this.contentDoc.forms[0] //Access first form inside iframe just for your reference
                        return true;
                    }
                }
                else
                    return false;
            }


            function GetSelectedItems(flg) {
                $('#hdnPrimeIds').val('');
                $('#hdnUsedNames').val('');
                var tableClass = 'masterTable';
                var deleteLabelId = 'lblActivityId';
                var deleteLabelName = 'Label1';
                var objCheck = new CkeckBoxSelect(tableClass, deleteLabelId, deleteLabelName);
                var deletedIds = objCheck.DeletedIds;
                var usedNames = objCheck.UsedNames;

                if (deletedIds == '') {
                    MessageMasterDiv('Please select record(s).');
                    return false;
                }
                else if (usedNames != '' && flg == 'D') {
                    MessageMasterDiv('Some records can not be deleted.' + usedNames, 1);
                    return false;
                }
                if (flg == 'D') {
                    if (DeleteConfrim() == true) {
                        $('#hdnPrimeIds').val(deletedIds);
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    $('#hdnPrimeIds').val(deletedIds);
                    return true;
                }
                $('#hdnPrimeIds').val(deletedIds);
                return true;
            }


            $(document).ready(function () {
                // HideShow();
                $('#toggleimage').hide();
                defaultTableValueSetting('lblCityId', 'MstCity', 'CityId');
                LockUnLockImage('masterTable');

            });


            function setSelectedId(obj) {
                var pId = $(obj).closest('tr').find('#lblCityId').text();
                if (pId == '' || pId == '0') {
                    return false;
                }
                else {
                    $('#hdnPrimeIds').val(pId);
                    return true;
                }
            }

            function setValue(activityText, status) {


                if (status == '1') {

                    $('#dvMesssage').removeClass().addClass('flash notice').html('Activity added Successfully').show();


                }
                else if (status == '3') {
                    $('#dvMesssage').removeClass().addClass('flash notice').html('Activity Deleted Successfully').show();
                }
                else {
                    $('#dvMesssage').removeClass().addClass('flash notice').html('Activity Updated Successfully').show();
                }

                setTimeout(function () {
                    $('#dvMesssage').hide();
                    window.location = window.location;
                }, 2000);
            }

            function ShowActivityDetails(obj) {
                if (("<%=Update%>") == 'Y' && $(obj).attr('isEditable') == 'Y') {
                    $("html, body").scrollTop(0);
                    var ReqID = $(obj).attr('ReqID');
                    var ActivityId = $(obj).attr('ActId');
                    emailwindow = dhtmlmodal.open('FieldDetailss', 'iframe', 'EventAdd.aspx?ActivityId=' + ActivityId + '&RequestId=' + ReqID, '', 'width=600px,scrollbars=no,height=500px,center=1,resize=0"');
                    emailwindow.onclose = function () { //Define custom code to run when window is closed

                        var theform = this.contentDoc.forms[0] //Access first form inside iframe just for your reference
                        return true;
                    }
                } else
                    return false;
            }
        </script>
</asp:Content>
