﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;
using WorkflowBLL;
using System.Security.Cryptography;
using System.Globalization;
using System.Web.Services;
using ClauseLibraryBLL;

public partial class CalendarOld : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Declarations.User] != null)
        {

            hdnUserId.Value = Session[Declarations.User].ToString();
        }
    }

    [WebMethod]
    public static string DeleteActivity(string ActivityId)
    {
        string IsDeleted = "";
        IActivity objActivity;
        objActivity = FactoryWorkflow.GetActivityDetails();
        objActivity.ActivityIds=ActivityId;
        IsDeleted=objActivity.DeleteRecord();
        return IsDeleted;
    }


    
}