﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EventAdd.aspx.cs" Inherits="EventAdd" %>

<%@ Register Src="../UserControl/CalendarActivity.ascx" TagName="activitiescontrol" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  
    <script src="../scripts/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
   

 <style type="text/css"> 
html, body { 
overflow-x:hidden; 
overflow-y:auto; 
} 
</style> 
  <script type="text/javascript">
      $(document).ready(function () {
          $("#txtRequestId").autocomplete({
              source: function (request, response) {
                  $.ajax({
                      url: "../Calendar/EventAdd.aspx/LoadContractIds",
                      data: "{input:'" + request.term + "'}",
                      dataType: "json",
                      type: "POST",
                      contentType: "application/json; charset=utf-8",
                      success: function (output) {
                          response($.map(output.d, function (item) {
                              return {
                                  label: item.split('##@@##')[0],
                                  val: item.split('##@@##')[1]                                  
                              }
                          }))
                      },
                      error: function (errormsg) {
                          alert(errormsg.responseText);
                      }
                  });
              },
              select: function (e, i) {
                  if (i.item.val == "0" || i.item.val == undefined) {
                      $("#hdnSelectedRequestId").val('0');
                      return false;
                  }
                  else {
                      var CustomerName = i.item.label;
                      //   var n = i.item.val;
                      var CustomerName = i.item.label.split('(');
                      var n = i.item.label.lastIndexOf('(');
                      var result = i.item.label.substring(n + 1).replace(')', '');
                      BindOtherFields(i.item.label);
                      $("#hdnSelectedRequestId").val(i.item.val);                      
                  }
              }
          });
      });


      $(function () {
      });

      function CloseActivityWindow() {

          parent.emailwindow.hide();
          return true;
      }
      function CloseForm(Activitytext,status) {
          parent.setValue(Activitytext,status);
          parent.emailwindow.hide();
          return true;
      }

      function CloseForm1(Activitytext) {
          window.opener.setValue(Activitytext);
          window.close();
          return true;
      }

  </script>

     <script type="text/javascript">

         function BindOtherFields(ClientName) {
             var AddressStringArray = new Array();
             $.ajax({
                 type: "POST",
                 contentType: "application/json; charset=utf-8",
                 url: "EventAdd.aspx/LoadOtherFields",
                 data: "{ClientName:'" + ClientName + "'}",
                 dataType: "json",
                 async: false,
                 success: function (output) {
                     if (output.d != "") {
                         var res = $.parseJSON(output.d);
                         for (var i = 0; i < res.length; i++) {
                             var ContractTypeId = res[i].ContractTypeID;
                         }
                         if (ContractTypeId != '') {
                             fillusers(ContractTypeId);
                         }
                     }
                 }
             });
         }

//<a href="../Handlers/GetStageConfiguratorDropDownHandler.ashx">../Handlers/GetStageConfiguratorDropDownHandler.ashx</a>

         function fillusers(ContractTypeId) {
             var type;
             var strType = "POST";
             var strContentType = "text/html; charset=utf-8";
             var strData = "{}";
             var strCatch = false;
             var strDataType = 'json';
             var strAsync = false;

             type = 'user';
             var strURL = '../Handlers/GetStageConfiguratorDropDownHandler.ashx?Type=' + type + '&ContractTemplateID=' + 0 + '&ID=' + ContractTypeId;
             objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
             globalUserList = objHandler.HandlerReturnedData;
             var userArray = new Array();
             var position = 0;
             $(globalUserList).each(function (i, val) {

                 userArray[position] = new Array();
                 userArray[position]["UsersId"] = val.UsersId;
                 userArray[position]["UserName"] = val.UserName;
                 position = position + 1;

             });
             bindSelectCustom(userArray, '#ddlUsers', 'UsersId', 'UserName');
             if ($('#ddlUsers').find('option').length > 0) {
                 $('#ddlUsers').next('div').next('.tooltip_outer').hide();
             }

         }
     </script>
</head>
<body style="overflow-x:">
  <form id="form1" runat="server">
  <asp:PlaceHolder ID="PlaceHolder1" runat="server">

        <uc2:activitiescontrol ID="activitiescontrolid" runat="server" />
    </asp:PlaceHolder>
 
    </form>
</body>
</html>
