﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using WorkflowBLL;
using CommonBLL;

public partial class EventAdd : System.Web.UI.Page
{
    public static string hdnSelectedRequestId;
    protected void Page_Load(object sender, EventArgs e)
    {
     
    }

   
    [WebMethod]
    public static string LoadCountry(string input)
    {
        IActivity objActivity;
        objActivity = FactoryWorkflow.GetActivityDetails();
        DataSet ds = new DataSet();
        List<SelectControlFields> result = new List<SelectControlFields>();

        result = objActivity.SelectContractIds();
        DataTable table = ConvertListToDataTable(result);
        table.DefaultView.RowFilter = "Column1 like '%" + input + "%' and Column1<>'--------Select--------'";
      
        ds.Tables.Add(table.DefaultView.ToTable());
        return ds.GetXml();

    }
    [WebMethod]
    public static string[] LoadContractIds(string input)
    {
        IActivity objActivity;
        objActivity = FactoryWorkflow.GetActivityDetails();
        List<SelectControlFields> result = new List<SelectControlFields>();
        result = objActivity.SelectContractIds();

        var p = from SelectControlFields s in result where s.Name != "--------Select--------" &&    System.Text.RegularExpressions.Regex.Split( s.Name,"##@@##")[0].Contains(input) select s;
        
        result = p.ToList();
        string[] retArray = result.Select(i => i.Name.ToString()).ToArray();
        return retArray;
    }
    public static DataTable ConvertListToDataTable(List<SelectControlFields> result)
	{
	    // New table.
	    DataTable table = new DataTable();

	    // Get max columns.
	    int columns = 0;
	    foreach (var array in result)
	    {
		if (result.Count > columns)
		{
            columns = result.Count;
		}
	    }

	    // Add columns.
	    for (int i = 0; i < columns; i++)
	    {
		table.Columns.Add();
	    }

	    // Add rows.
	    foreach (var array in result)
	    {
		table.Rows.Add(array.Name);
	    }

	    return table;
	}
    public static string GetCountries(string input)
    {
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        dt.Columns.Add("Country");
        dt.Rows.Add("India");
        dt.Rows.Add("United States");
        dt.Rows.Add("United Kingdom");
        dt.Rows.Add("Canada");
        dt.Rows.Add("South Korea");
        dt.Rows.Add("France");
        dt.Rows.Add("Mexico");
        dt.Rows.Add("Russia");
        dt.Rows.Add("Australia");
        dt.Rows.Add("Turkey");
        dt.Rows.Add("Kenya");
        dt.Rows.Add("New Zealand");
        dt.DefaultView.RowFilter = "Country like '%" + input + "%'";
        ds.Tables.Add(dt.DefaultView.ToTable());


        return ds.GetXml();
    }

    //[WebMethod]
    //public static string BindAssine(string RequestId)
    //{
        
    //    IActivity objActivity;
    //    objActivity = FactoryWorkflow.GetActivityDetails();
    //    objActivity.RequestId = Convert.ToInt32(RequestId);
    //    string IsCustomer = objActivity.ReadAssineDetails();
    //    return IsCustomer;
    //}

    [WebMethod]
    public static string LoadOtherFields(string ClientName)
    {
        string ClientAddressDetails = "";
        if (ClientName.LastIndexOf("(") > -1)
        {
            string FinalString;
            int Pos1 = ClientName.LastIndexOf("(") + 1;
            int Pos2 = ClientName.LastIndexOf(")");
            FinalString = ClientName.Substring(Pos1, Pos2 - Pos1);

            IContractRequest objContractRequest;
            objContractRequest = FactoryWorkflow.GetContractRequestDetails();
            objContractRequest.ClientID = Convert.ToInt32(FinalString.Trim());
            ClientAddressDetails = objContractRequest.ReadOtherDetails();
        }
        return ClientAddressDetails;
    }

}