﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;

public partial class Calendar_MACOutlookCalendarSynch : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            //string url = "https://app.contractpod.com/OutlookCalendar/" + Request.Url.Segments[1].Replace("/", "") + "/";
            //lblOutlookUrl.Text = url + Session[Declarations.OutlookFileName].ToString();
            lblOutlookUrl.Text = Session[Declarations.OutlookURL].ToString(); ;// lblOutlookUrl.Text.Trim();
        }
    }
}