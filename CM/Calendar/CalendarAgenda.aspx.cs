﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;
using WorkflowBLL;
using System.Web.UI.HtmlControls;

public partial class Calendar_CalendarAgenda : System.Web.UI.Page
{
    // navigation//
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 10;
    // navigation//

    IActivity objActivity;
    public string Add;
    public string View;
    public string Update;
    public string Delete;

    string OpenActivityType = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        OpenActivityType = Convert.ToString(Request.QueryString["OpenActivitiestype"]) == null ? "" : Convert.ToString(Request.QueryString["OpenActivitiestype"]);

        if (Session[Declarations.User] != null && !IsPostBack)
        {
            Access.PageAccess(this, Session[Declarations.User].ToString(), "calendar_");
            ViewState[Declarations.Add] = Access.Add.Trim();
            ViewState[Declarations.View] = Access.View.Trim();
            ViewState[Declarations.Delete] = Access.Delete.Trim();
            ViewState[Declarations.Update] = Access.Update.Trim();
        }


        if (!IsPostBack || hdnSearch.Value.Trim() != string.Empty)
        {
            Session[Declarations.SortControl] = null;
            rdActive.Checked = true;
            ReadData(1, RecordsPerPage, OpenActivityType);
            AccessVisibility();
            Message();
        }
        else
        {
            SetNavigationButtonParameters();
        }
    }

    protected void btnSort_Click(object sender, EventArgs e)
    {
        hdnSearch.Value = "";
        LinkButton clickBtn = (LinkButton)sender;
        Session.Add(Declarations.SortControl, clickBtn);
        ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage, OpenActivityType);
        AccessVisibility();
    }

    void Message()
    {
        if (Session[Declarations.Message] != null)
        {
            var flg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }

    }

    // navigation//

    #region Navigation
    void SetNavigationButtonParameters()
    {
        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }


    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage, OpenActivityType);
            AccessVisibility();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

    }
    #endregion

    // navigation//

    void ReadData(int pageNo, int recordsPerPage, string OpenActivityType)
    {


        Page.TraceWrite("ReadData starts.");
        try
        {
            objActivity.PageNo = pageNo;
            objActivity.RecordsPerPage = recordsPerPage;
            objActivity.Search = txtSearch.Value.Trim();
            LinkButton btnSort = null;
            if (Session[Declarations.SortControl] != null && Session[Declarations.SortControl] != string.Empty)
            {
                btnSort = (LinkButton)Session[Declarations.SortControl];
                objActivity.Direction = btnSort.CssClass.ToLower() == "sort desc" ? 1 : 0;
                objActivity.SortColumn = btnSort.Text.Trim();
            }

            objActivity.StartWeekDate = null;
            objActivity.EndWeekDate = null;
            objActivity.AddedBy = Convert.ToInt32(Session[Declarations.User].ToString());
            objActivity.OpenActivityType = OpenActivityType;
            objActivity.ReadAgendaData();
            List<Activity> ListA = objActivity.ActivityDates;
            rptCityMaster.DataSource = ListA;
            rptCityMaster.DataBind();
            if (btnSort != null)
            {
                rptCityMaster.ClassChange(btnSort);
            }
            Page.TraceWrite("ReadData ends.");
            ViewState[Declarations.TotalRecord] = objActivity.TotalRecords;
            PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
            PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
            PaginationButtons1.RecordsPerPage = RecordsPerPage;
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }


    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objActivity = FactoryWorkflow.GetActivityDetails();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Delete starts.");
        string status = "";
        objActivity.ActivityIds = hdnPrimeIds.Value;
        try
        {
            status = objActivity.DeleteRecord();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "tytyttrrr", "setValue('','3');", true);
        }
        catch (Exception ex)
        {
            hdnPrimeIds.Value = string.Empty;
            Session[Declarations.DeleteMSG] = "PageGetMessage('DF')";
            Page.TraceWarn("Delete fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        hdnPrimeIds.Value = string.Empty;
        Page.TraceWrite("Delete ends.");
        //  Session[Declarations.DeleteMSG] = "PageGetMessage('DS')";
        //Server.Transfer("CalendarAgenda.aspx");


    }


    void deleteMessage()
    {
        if (Session[Declarations.DeleteMSG] != null)
        {
            Page.JavaScriptClientScriptBlock("status", Session[Declarations.DeleteMSG].ToString());
            Session[Declarations.DeleteMSG] = null;
        }
    }

    void loadPagination()
    {
        UserControl_PaginationButtons ucSimpleControl = LoadControl("../UserControl/masterrightlinks.ascx") as UserControl_PaginationButtons;
        Placeholder1.Controls.Add(ucSimpleControl);

    }

    protected void btnChangeStatus_Click(object sender, EventArgs e)
    {
        //Page.TraceWrite("Change Status starts.");
        //string status = "";
        //objCity.CityIds = hdnPrimeIds.Value;
        //try
        //{
        //    objCity.isActive = rdActive.Checked == true ? "Y" : "N";
        //    status = objCity.ChangeIsActive();
        //    ReadData(1, RecordsPerPage);
        //}
        //catch (Exception ex)
        //{
        //    hdnPrimeIds.Value = string.Empty;
        //    Page.JavaScriptClientScriptBlock("status", "PageGetMessage('SF')");
        //    Page.TraceWarn("Change Status fails.");
        //    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //}
        //Page.JavaScriptClientScriptBlock("status", "PageGetMessage('SS')");
        //Page.TraceWrite("Change Status ends.");
    }


    protected void imgEdit_Click(object sender, EventArgs e)
    {
        if (Update == "Y")
        {
            Server.Transfer("CityMaster.aspx");
        }
    }

    protected void btnAddRecord_Click(object sender, EventArgs e)
    {
        //Server.Transfer("../Calendar/EventAdd.aspx");
    }

    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (View == "N")
        {
            Page.extDisableControls();
            btnChangeStatus.Enabled = false;

        }
        else
        {
            btnSearch.Enabled = true;
            txtSearch.Disabled = false;
        }

        if (Update == "N")
        {
            foreach (RepeaterItem item in rptCityMaster.Items)
            {
                LinkButton imgEdit = (LinkButton)item.FindControl("imgEdit");
                imgEdit.Enabled = false;
            }
            btnDelete.Enabled = false;
            btnChangeStatus.Enabled = false;
        }
        if (Add == "N")
        {
            Page.extDisableControls();
            btnChangeStatus.Enabled = false;
            btnAddRecord.Enabled = false;
        }
        else
        {
            btnAddRecord.Enabled = true;
        }

        if (Delete == "N")
        {
            btnDelete.Enabled = false;
            btnDelete.OnClientClick = null;
        }
        else if (Delete == "Y")
        {
            btnDelete.Enabled = true;
        }

        if (View == "Y")
        {
            txtSearch.Disabled = false;
            btnSearch.Enabled = true;
            btnShowAll.Enabled = true;
        }

        Page.TraceWrite("AccessVisibility starts.");
    }

    protected void rptCityMaster_ItemDataBound(object source, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdnisEditable = (HiddenField)e.Item.FindControl("isEditable");
            HtmlControl chkAgenda = (HtmlControl)e.Item.FindControl("chkAgenda");
            LinkButton imgEdit = (LinkButton)e.Item.FindControl("imgEdit");
            if (hdnisEditable.Value == "N") {
                imgEdit.Attributes.Add("class", "areadOnly");
                chkAgenda.Attributes.Add("disabled", "disabled");
            }
        }
    }
}