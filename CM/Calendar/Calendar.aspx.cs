﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;
using WorkflowBLL;
using System.Security.Cryptography;
using System.Globalization;
using System.Web.Services;
using ClauseLibraryBLL;

public partial class Calendar : System.Web.UI.Page
{
    public string Add;
    public string View;
    public string Update;
    public string Delete;
    public bool RequestTypeClickAccess;
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["MasterSeelcted"] = null;
        Session["ChlidSeelcted"] = null;

        if (Session[Declarations.User] != null && !IsPostBack)
        {
            Access.PageAccess(this, Session[Declarations.User].ToString(), "calendar_");
            ViewState[Declarations.Add] = Access.Add.Trim();
            ViewState[Declarations.View] = Access.View.Trim();
            ViewState[Declarations.Delete] = Access.Delete.Trim();
            ViewState[Declarations.Update] = Access.Update.Trim();
            ViewState["RequestTypeClickAccess"] = Access.isCustomised(12);

            if (Session[Declarations.IsOutlookSynchEnable].ToString() == "Y" && !string.IsNullOrEmpty(Session[Declarations.OutlookFileName].ToString()))
            {
                linkOutlookSynch.Visible = true;
            }
        }

        AccessVisibility();

        if (Session[Declarations.User] != null)
        {
            hdnUserId.Value = Session[Declarations.User].ToString();
        }
    }

    [WebMethod]
    public static string DeleteActivity(string ActivityId, string FileUpload, string IsOCR)
    {
        string IsDeleted = "";
        IActivity objActivity;
        objActivity = FactoryWorkflow.GetActivityDetails();
        objActivity.ActivityIds=ActivityId;
        string strFileUpload = FileUpload;

        IsDeleted = objActivity.DeleteRecord();
         
        string strUrl = "";
        strUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + HttpContext.Current.Session["TenantDIR"] + FileUpload.Replace("../Uploads", "/Uploads");
        AsyncIndexer.GetInstance().QueueForIndexing(strUrl, 2);
        DeleteFileFromPhysicalLocation.DeleteFile(FileUpload);

        if (IsOCR.ToUpper() == "Y")
        {
            DeleteFileFromPhysicalLocation.DeleteFile(FileUpload.Replace("ActivitiesDocuments", "ActivitiesDocuments/ScannedActivitiesDocuments"));
        }
        return IsDeleted;
    }




    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

        if (ViewState["RequestTypeClickAccess"] != null)
        {
            RequestTypeClickAccess = Convert.ToBoolean(ViewState["RequestTypeClickAccess"].ToString());

        }

    }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (View == "N")
        {
            Page.extDisableControls();
           // btnChangeStatus.Enabled = false;

        }
        else
        {
           // btnSearch.Enabled = true;
           // txtSearch.Disabled = false;
        }

        if (Update == "N")
        {
            Page.extDisableControls();
            hdnIsUpdate.Value = "N";
            //foreach (RepeaterItem item in rptCityMaster.Items)
            //{
            //    LinkButton imgEdit = (LinkButton)item.FindControl("imgEdit");
            //    imgEdit.Enabled = false;
            //}
          //  btnDelete.Enabled = false;
         //   btnChangeStatus.Enabled = false;
        }
        if (Add == "N")
        {
            Page.extDisableControls();
            hdnisAdd.Value = "N";

          //  btnChangeStatus.Enabled = false;
          //  btnAddRecord.Enabled = false;
        }
        else
        {
          //  btnAddRecord.Enabled = true;
        }

        if (Delete == "N")
        {
            hdnIsDelete.Value = "N";
          //  btnDelete.Enabled = false;
           // btnDelete.OnClientClick = null;
        }
        else if (Delete == "Y")
        {
            hdnIsDelete.Value = "Y";
           // btnDelete.Enabled = true;
        }

        if (View == "Y")
        {
           // txtSearch.Disabled = false;
          //  btnSearch.Enabled = true;
          //  btnShowAll.Enabled = true;
        }

        Page.TraceWrite("AccessVisibility starts.");
    }


    
}