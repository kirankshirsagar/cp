﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true" CodeFile="MACOutlookCalendarSynch.aspx.cs" Inherits="Calendar_MACOutlookCalendarSynch" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
 <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <link href="../CalendarCSS/jquery-ui-1.8.21.css" media="all" rel="stylesheet" type="text/css">
    <script src="../CalendarCSS/jquery-1.7.2-ui-1.8.21-ujs-2.0.3.js" type="text/javascript"></script>
    <link href="../JQueryValidations/dhtmlwindow.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/dhtmlwindow.js" type="text/javascript"></script>
    <link href="../JQueryValidations/modal.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/modal.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
        <div id="rightlinks" style="display: none;">
            <uc1:Masterlinks ID="rightactions" runat="server" />
        </div>
        <script type="text/javascript">
            $("#sidebar").html($("#rightlinks").html());
        </script>


    <div style="font-size:14px; font-weight:bold;color:black;" > 
          <h2>  MAC/Outlook Calendar Sync </h2> 
          <a href="Calendar.aspx">Back to Calendar</a>.
<ol>
<br /><br />
            <li>	Open outlook.office.com and login with your outlook account
            <br />
                
<br /><br />           </li>
             <li>	Click on top-left most corner icon => Calendar
              <br /><br />
                <img src="../Images/MacOutlookSynch/2.png" />
<br /><br /></li>
             <li>	Go to Add Calendar => From Internet
              <br /><br />
                <img src="../Images/MacOutlookSynch/3.png" />
<br /><br /></li>
             <li>	Copy and paste below url in "Link to the Calendar" textbox, give appropriate name to your calendar and click Save.
            <br /><br />
                <u><asp:Label ID="lblOutlookUrl" runat="server" Text="" style="color:blue"></asp:Label></u>      
            <br /><br />
            <img src="../Images/MacOutlookSynch/4.png" />
<br /><br /></li>            
             <li>	Your ContractPod calendar gets synched and the calendar name will be displayed under Other Calendars. You can rename the folder name as you want.
                 <br /><br />
        <img src="../Images/MacOutlookSynch/5.png" />
<br /><br /></li>
             <li>	The activities will be shown by their Type name<br /><br />
        <img src="../Images/MacOutlookSynch/7.png" />
<br /><br /></li>
            <li>	Similarly, go to Outlook => Calendar and check the new calendar has been synced.<br /><br />
            <img src="../Images/MacOutlookSynch/9.png" />
<br /><br /></li>
             <li>	The details can be viewed by simply double clicking it. <%--The activity cannot be updated and deleted.--%>
                    Whenever the changes made to the events, Outlook.com updates your calendar.<br />Note that this update can take upto 24 hours.
<br /></li>

</ol>     
    </div>

    <br /><br />
</asp:Content>