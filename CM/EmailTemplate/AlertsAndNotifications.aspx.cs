﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using EmailTemplateBLL;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;


public partial class EmailTemplate_AlertsAndNotifications : System.Web.UI.Page
{

    List<SelectControlFields> emailTemplates = new List<SelectControlFields>();
    List<SelectControlFields> emailTemplatesWithOutTemplate = new List<SelectControlFields>();


    IEmailAlertsAndNotifications objEmailAlerts;
    IEmailTemplate objEmailTemplate;
 

    
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        if (!IsPostBack)
        {
            emailTemplates = objEmailTemplate.SelectData(1);
            emailTemplatesWithOutTemplate = objEmailTemplate.SelectDataWithOutTemplate(1);

            if (Session[Declarations.User] != null)
            {
                Access.PageAccess(this, Session[Declarations.User].ToString(), "emailtemplate_");
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
            }
            AccessVisibility();
            ReadData();
        }
    
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objEmailAlerts = FactoryEmailTemplate.GetEmailAlertsAndNotificationDetail();
            objEmailTemplate = FactoryEmailTemplate.GetEmailTemplateDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }


    void ReadData()
    {
        if (View == "Y")
        {
            Page.TraceWrite("ReadData starts.");
            try
            {
                rptEvents.DataSource = objEmailAlerts.ReadData();
                rptEvents.DataBind();
                Page.TraceWrite("ReadData ends.");
            }
            catch (Exception ex)
            {
                Page.TraceWarn("ReadData fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
    }
   
    

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        InsertUpdate();
        Page.TraceWrite("Save Update button clicked event ends.");
    }

   

  

    void InsertUpdate()
    {
        Page.TraceWrite("Insert, Update starts.");
        string status = "";
        objEmailAlerts.EmailAlertsAndNotification = objEmailAlerts.getEmailAlertsAndNotification(rptEvents);
        objEmailAlerts.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objEmailAlerts.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;

        try
        {
            status = objEmailAlerts.UpdateRecord();
            Page.Message(status, hdnPrimeId.Value);
        }
        catch (Exception ex)
        {
            Page.Message("0", hdnPrimeId.Value);
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Insert, Update ends.");
        Session[Declarations.Message] = "1";
        
    }

   

    private void setAccessValues()
    {
        
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (Update == "N" && (hdnPrimeId.Value == "" || hdnPrimeId.Value == "0" || hdnPrimeId.Value == null))
        {
            Page.extDisableControls();
        }
       
        Page.TraceWrite("AccessVisibility starts.");
    }



    protected void rptEvents_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HtmlSelect ddl = (HtmlSelect)e.Item.FindControl("ddlEmailTemplate");
            Label lbl = (Label)e.Item.FindControl("lblEmailTemplateIds");
            Label lblEmailTriggerId = (Label)e.Item.FindControl("lblEmailTriggerId");
            if (lblEmailTriggerId.Text.Trim() == "1")
            {
                ddl.extDataBind(emailTemplatesWithOutTemplate);
            }
            else
            {
                ddl.extDataBind(emailTemplates);
            }

            ddl.Multiple = true;
            ddl.extSelectedValues(lbl.Text);

            if (Update == "N")
            {
                ddl.Disabled = true;
            }

        }
      

    }
}










