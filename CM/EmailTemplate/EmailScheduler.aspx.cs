﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EmailTemplateBLL;
using CommonBLL;
using WorkflowBLL;

public partial class EmailTemplate_EmailScheduler : System.Web.UI.Page
{
    IKeyFields objkey;
    protected void Page_Load(object sender, EventArgs e)
    {
        objkey = FactoryWorkflow.GetKeyFieldsDetails();
        try
        {
            string v = Request.QueryString["garbageEmail"];
            if (v == "emailGarbage")
            {
                SendEmail();

                objkey.URLForWindowsService = HttpContext.Current.Request.Url.AbsoluteUri;
                objkey.UpdateURLLastRunTimeForWindowsService();
            }            

        }
        catch (Exception)
        {
            
        }
       
        
    }


    void SendEmail()
    {
        try
        {
            Page.TraceWrite("send emails.");
            EmailTemplateBLL.IEmailTrigger objEmail = EmailTemplateBLL.FactoryEmailTemplate.GetEmailTriggerDetail();
            objEmail.userId = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            objEmail.BulkEmailScheduler();
        }
        catch (Exception)
        {
            Page.TraceWarn("send emails fails.");
        }
    }
}