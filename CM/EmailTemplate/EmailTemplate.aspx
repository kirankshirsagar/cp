﻿<%@ Page Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true" CodeFile="EmailTemplate.aspx.cs"
    ValidateRequest="false" EnableEventValidation="false" Inherits="EmailTemplate_EmailTemplate" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../Styles/css/smoothness/jquery-ui-1.8.13.custom.css"
        type="text/css" media="screen" charset="utf-8" />
    <script src="../scripts/elrte.min.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../Styles/css/elrte.full.css" type="text/css" media="screen"
        charset="utf-8" />
    <script src="../scripts/elrte.full.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../UploadJs/js/jquery.plupload.queue/css/jquery.plupload.queue.css"
        type="text/css" media="screen" />
    <script type="text/javascript" src="../UploadJs/js/plupload.full.min.js"></script>
    <script src="../UploadJs/pluploadGeneric.queue.js" type="text/javascript"></script>
    <script type="text/javascript">

        function DeleteDocument(obj) {
            $(obj).closest('p').remove();
            return false;

        }




        function GetFiles() {
            var ind = 0
            var fileInfoStr = '';
            $('.attachments').find('p').each(function () {

                var originalFileName = $(this).find("a[class*='icon-attachment']").html().trim();
                var modifiedFileName = $(this).find("a[class*='icon-attachment']").attr('href').replace('../Uploads/' + Session["TenantDIR"] + '/ContractDocs/', '').trim();
                var fileSizeBytes = $(this).find("span[class*='sizeBytes']").html().replace('(', '').replace(')', '').trim();

                if (ind == 0) {
                    fileInfoStr = originalFileName + '#@@#' + modifiedFileName + '#@@#' + fileSizeBytes;
                }
                else {
                    fileInfoStr = fileInfoStr + "$##$" + originalFileName + '#@@#' + modifiedFileName + '#@@#' + fileSizeBytes;
                }
                ind = ind + 1;
            });


            return fileInfoStr;


        }

    
    
    </script>
    <script type="text/javascript" charset="utf-8">
        $().ready(function () {
            var opts = {
                cssClass: 'el-rte',
                // lang     : 'ru',
                height: 450,
                toolbar: 'complete',
                cssfiles: ['css/elrte-inner.css']
            }
            $('#editor').elrte(opts);
        })
	</script>
    <style type="text/css" media="screen">
        body
        {
            padding: 20px;
        }
    </style>
    <script type="text/javascript">
        var upl;

        $(function () {

            // Setup html5 version
            upl = $("#uploader").pluploadQueue({
                // General settings
                runtimes: 'html5, html4',
                url: '../ClauseLiabrary/DocumentHandler.ashx',
                max_file_size: '80mb',
                max_file_count: 20, // user can add no more then 20 files at a time
                chunk_size: '80mb',
                unique_names: true,
                multiple_queues: true,
                multipart: true,
                multipart_params: {

                },

                resize: { width: 1024, height: 768, quality: 90 },

                // Rename files by clicking on their titles
                rename: true,

                // Sort files
                sortable: true,

                // Specify what files to browse for
                filters: [
               	{ title: "Image files", extensions: "jpg,gif,png,bmp,tiff,jpeg,TIF,blob" },
                { title: "Document files", extensions: "xls,xlsx,doc,docx,pdf,swf,ppt,pptx,txt,csv,odt,odp,odg,ods,mp3,mp4,wmv" },
                { title: "Zip files", extensions: "zip,avi" }
                //             {title: "Document files", extensions: "doc,docx" },
		],


                // PreInit events, bound before any internal events
                preinit: {
                    Init: function (up, info) {

                    },

                    PostInit: function (up) {


                    },
                    UploadFile: function (up, file) {

                        var fname = file.name.replace(' & ', '').replace('&', '').replace('#', '');
                        var filesize = file.size;
                        var user = $('#hdnUserName').val();
                        up.settings.url = '../Handlers/GetEmailTemplateFiles.ashx?FileName=' + fname + '&FileSize=' + filesize + '&UserName=' + user;

                    },

                    // Post init events, bound after the internal events
                    init: {

                        Refresh: function (up) {

                            // Called when upload shim is moved
                            //log('[Refresh]');
                        },

                        StateChanged: function (up) {

                            // Called when the state of the queue is changed

                            //  log('[StateChanged]', up.state == plupload.STARTED ? "STARTED" : "STOPPED");
                        },

                        QueueChanged: function (up) {
                            // Called when the files in queue are changed by adding/removing files

                            //log('[QueueChanged]');
                        },

                        UploadProgress: function (up, file) {
                            // Called while a file is being uploaded
                            // log('[UploadProgress]', 'File:', file, "Total:", up.total);

                        },

                        FilesAdded: function (up, files) {

                            plupload.each(files, function (file) {
                                //  log('  File:', file);
                            });
                        },

                        FilesRemoved: function (up, files) {

                            plupload.each(files, function (file) {
                                //  log('  File:', file);
                            });
                        },

                        FileUploaded: function (up, file, info) {
                            // Called when a file has finished uploading                             
                            //  alert(info.response);
                            var FileArray = new Array();
                            FileArray = info.response.split('#');
                            var arr = "";

                            if (FileArray[0] != "Chunk") {
                                arr = FileArray[0].split(/=/);

                            }

                            else if (chunkreponse != "") {
                                arr = chunkreponse.split(/=/);

                            }

                            //  alert($('#hdnDocumentIDs').val());
                            if (arr != "") {

                                chunkreponse = "";
                                // log('[FileUploaded] File:', file, "Info:", info);

                            }

                        },

                        ChunkUploaded: function (up, file, info) {
                            // Called when a file chunk has finished uploading

                            if (info.response != "Chunk") {

                                chunkreponse = info.response;

                                // log('[FileUploaded] File:', file, "Info:", info);
                            }


                        },

                        Error: function (up, args) {
                            // Called when a error has occured
                            //  log('[error] ', args);
                        }

                    },

                    // Resize images on clientside if we can
                    resize: { width: 320, height: 240, quality: 90 },

                    flash_swf_url: '../../js/Moxie.swf',
                    silverlight_xap_url: '../../js/Moxie.xap'

                }
            });


        });



        $('.NumericOnly').live('keypress', function (evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode

            if (charCode == 8) {
                return true;
            }
            else if ((charCode == 46 || $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57)) {
                return false;
            }
        });


     
     
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab"); 
    </script>
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <input id="hdnContractTemplateId" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnSubject" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnAttachnents" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnBodyText" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnRoles" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnUsers" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnContractTemplateIdRead" runat="server" clientidmode="Static" type="hidden" />
    <input id="hndSubjectRead" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnBodyTextRead" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnUserName" runat="server" clientidmode="Static" type="hidden" />
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
    </h2>
    <div class="box tabular">
        <p>
            <label for="time_entry_issue_id">
                <span class="required">*</span>Email Template name</label>
            <input id="txtEmailTemplateName" disabled="disabled" runat="server" type="text" class="required ui-autocomplete-input"
                style="width: 49.5%" maxlength="50 " autocomplete="off" role="textbox" aria-autocomplete="list"
                aria-haspopup="true" />
            <em></em>
        </p>
       <%-- <p>
            <label for="time_entry_issue_id">
                <span class="required">*</span>Contract Type</label>
            <select id="ddlContractType" onchange="JQSelectBind('ddlContractType','ddlContractTemplate', 'ContractTemplateWithInt');setContractTemplateValue();cloneSubjectToPicklist(this);"
                clientidmode="Static" runat="server" style="width: 50%" class="chzn-select  chzn-select">
                <option></option>
            </select>
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id">
                Contract Template</label>
            <select id="ddlContractTemplate" onchange="JQSelectBindWithPrompt('ddlContractTemplate','ddlSubject', 'vwFieldList','Select Fields'); cloneSubjectToPicklist(this);"
                clientidmode="Static" runat="server" style="width: 50%" class="chzn-select">
                <option></option>
            </select>
            <em></em>
        </p>--%>
        <p>
            <label for="time_entry_issue_id">
                Subject</label>
            <select id="ddlSubject" clientidmode="Static" style="width: 50%" class="chzn-select">
                <option></option>
            </select><br />
            <input id="txtSubject" clientidmode="Static" runat="server" type="text" class="ui-autocomplete-input required"
                style="width: 49.5%" maxlength="200" autocomplete="off" role="textbox" aria-autocomplete="list"
                aria-haspopup="true" />
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id">
                Recipient</label>
            <select id="ddlRole" clientidmode="Static" runat="server" style="width:50%" class="chzn-select">
                <option></option>
            </select>
            <em></em>
        </p>
        <p>
            <label for="time_entry_issue_id">
                Recipient Users</label>
            <select id="ddlUser" clientidmode="Static" runat="server" style="width:50%" class="chzn-select">
                <option></option>
            </select>
            <em></em>
           
        </p>
        <p style="display:none">
            <label for="time_entry_issue_id">
                Reply To</label>
            <input id="txtReplyTo" runat="server" type="text" class="ui-autocomplete-input email" style="width: 49.5%"
                maxlength="100" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" />
            <em></em>
        </p>
       
        <p class="condtional" style="display:none">
            <label for="time_entry_issue_id">
                Event Execution Before</label>
            <input id="txtEventExecutionValue" runat="server" type="text" class="NumericOnly"
                clientidmode="Static" style="width: 5%;" maxlength="2" autocomplete="off" role="textbox"
                aria-autocomplete="list" aria-haspopup="true" />
            <em></em>
        </p>
        <p class="condtional" style="display:none">
            <label for="time_entry_issue_id">
                Event Execution</label>
            <select id="ddlEventExecution" clientidmode="Static" runat="server" style="width: 25%;
                width: 20%" class="chzn-select">
                <option value="On time">On time</option>
                <%--<option value="Hours" >Hours</option>--%>
                <option value="Days">Days</option>
                <option value="Week">Week</option>
            </select>
            <em></em>
        </p>
        <p>
         <label for="time_entry_issue_id">
                Status</label>
            <input id="rdActive" name="ActiveInactive" runat="server" type="radio" class="required" />Active
            <input id="rdInactive" name="ActiveInactive" runat="server" type="radio" class="required" />Inactive
            <em></em>
        </p>
    </div>
    <div class="box tabular">
        <div id="uploader" style="display: block; width: 600px; margin-left: 175px">
        </div>
        <div class="attachments">
            <asp:Repeater ID="rptAttachments" runat="server">
                <ItemTemplate>
                    <p>
                        <a id="A1" href='../Uploads/' +<%# Session["TenantDIR"] %> + '/ContractDocs/<%#Eval("FileName")%>' class="icon icon-attachment">
                            <%#Eval("FileNameOriginal")%></a> &nbsp; <span class="size">
                                <%#Eval("SizeModified")%>
                            </span><a id="0" href="/redmine/attachments/5279" class="delete" onclick="return DeleteDocument(this)"
                                data-confirm="Are you sure?" data-method="delete" rel="nofollow" title="Delete">
                                <img alt="Delete" src="../images/icon-del.jpg?1349001717"></a> <span class="author">
                                    <%#Eval("AddedByName")%></span><span style="display: none" class="sizeBytes"><%#Eval("SizeBytes")%></span>
                    </p>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div id="FileName" class="attachments">
        </div>
        <div id="editor">
        </div>
    </div>
    <asp:Button ID="btnSave" runat="server" OnClientClick="return SetValues();" Text="Save"
        OnClick="btnSave_Click" class="btn_validate" />
    <asp:Button ID="btnSaveAndContinue" OnClientClick="return SetValues();" runat="server"
        Text="Save and Add more" OnClick="SaveAndContinue_Click" class="btn_validate" />
    <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back_Click" />
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
    <script type="text/javascript">

        $('#ddlSubject').change(function () {

            if ($('#ddlSubject').val() > 0) {
                var selected = $("#ddlSubject option:selected").text();
                var result;
                if ($('#txtSubject').val() == '') {
                    result = '#' + selected + '#';
                }
                else {
                    result = $('#txtSubject').val() + ' #' + selected + '#';
                }
                $('#txtSubject').val(result);
                $('#hdnSubject').val('');
                $('#hdnSubject').val(result);
                $('#txtSubject').change();
            }
        });


        function setContractTemplateValue() {

            $('#hdnContractTemplateId').val('');
            $('#ddlContractTemplate').change();
           // $('#txtSubject').val('');
            //clearBodyContent();
            $('#hdnContractTemplateId').val($('#ddlContractTemplate').val());

        }

        function cloneSubjectToPicklist() {

//            if ($(obj).attr('id') == 'ddlContractTemplate' && $(obj).val() == '0') {
//                //JQSelectBindDefalut('1000000', 'ddlSubject', 'vwFieldList');
                JQSelectBindDefalutWithPrompt('1000000', 'ddlSubject', 'vwFieldList', 'Select Fields');
//            }

            $('#customHashTag').empty();
            $('#ddlSubject option').clone().appendTo('#customHashTag');
            //clearBodyContent();
            $('#ddlContractTemplate').next('div').find('a').css("border-color", "");
        }

        function clearBodyContent() {
            $('.workzone').find('iframe').contents().find('body').html('');
        }


        function SetValues() {

//            if ($('#ddlUser').val() == null && $('#ddlRole').val() == null) {
//                alert("select role or user.");
//                return false;
//            }

            $('#hdnSubject').val('');
            $('#hdnBodyText').val('');
            $('#hdnRoles').val('');
            $('#hdnUsers').val('');
            $('#hdnAttachnents').val('');
            $('#hdnContractTemplateId').val($('#ddlContractTemplate').val());
            $('#hdnSubject').val($('#txtSubject').val());
            $('#hdnBodyText').val($('.workzone').find('iframe').contents().find('body').html());
            var attachment = GetFiles();

            $('#hdnAttachnents').val(attachment);
            $('#hdnRoles').val(getCSVFromArray($('#ddlRole').val()));
            $('#hdnUsers').val(getCSVFromArray($('#ddlUser').val()));

        }
        function showConditional(obj) {           
//            if (obj == 'Completion of Task and Task Deadline' || obj == 'Expiry Date/ Contract Overdue and Renewal Notification') {
//                $('.condtional').css('display', 'block');
//            }
            switch ($.trim(obj)) {
                case "Completion of Task":
                case "Task Deadline":
                case "Expiry Date":
                //case "Contract Overdue":
                case "Renewal Notification":
                    $('.condtional').hide();
                    break;
                default:
                    $('.condtional').hide();
                    break;
            }
        }


        $(document).ready(function () {
            try {
                hideUnwanted();
          
                //  $('#ddlContractType').change();
                // $('#ddlContractTemplate').val($('#hdnContractTemplateIdRead').val());
                updateSettings('#ddlContractTemplate');
                // $('#hdnContractTemplateId').val($('#hdnContractTemplateIdRead').val());
                // $('#ddlContractTemplate').change();
                // JQSelectBind('ddlContractType', 'ddlContractTemplate', 'ContractTemplateWithInt')

                JQSelectBind('ddlContractType', 'ddlContractTemplate', 'ContractTemplateWithInt');
                setContractTemplateValue();
                cloneSubjectToPicklist();
                $('.workzone').find('iframe').contents().find('body').html($('#hdnBodyTextRead').val());
                if ($('#hdnContractTemplateIdRead').val() != '') {
                    $('#txtSubject').val($('#hndSubjectRead').val());
                    $('#hdnSubject').val($('#txtSubject').val());
                }

            } catch (e) {

            }

        });



        $(window).load(function () {

            try {

                
                $('.panel-media').remove();
                enableDisableEventExecutionValue();
            } catch (e) {

            }

        });


        function enableDisableEventExecutionValue() {

            if ($('#ddlEventExecution').val() == 'On time') {
                $('#txtEventExecutionValue').attr("disabled", "disabled");
                $('#txtEventExecutionValue').removeClass('required');
                $('#txtEventExecutionValue').css("border-color", "");
                $('#txtEventExecutionValue').next('div.tooltip_outer').remove();
                $('#txtEventExecutionValue').val('');
            }
            else {
                $('#txtEventExecutionValue').removeAttr('disabled');
                $('#txtEventExecutionValue').addClass('required');
            }
        }

        $('#txtEventExecutionValue').keyup(function () {

            if ($('#txtEventExecutionValue').val() != '') {
                $('#txtEventExecutionValue').removeClass('required');
                $('#txtEventExecutionValue').css("border-color", "");
                $('#txtEventExecutionValue').next('div.tooltip_outer').remove();
            }
            else {
                $('#txtEventExecutionValue').addClass('required');
            }

        });




        $('#ddlEventExecution').change(function () {

            enableDisableEventExecutionValue();
        });


        function ClearAll() {
           
            $('#ddlRole').val('').trigger('liszt:updated');
            $('#ddlUser').val('').trigger('liszt:updated');
        }


        function hideUnwanted() {
            try {

                $("div.toolbar ul.panel-save").remove();
                $("div.toolbar ul.panel-copypaste").remove();
                $("div.toolbar ul.panel-links").remove();
                $("div.toolbar ul.panel-elements").remove();
                $("div.toolbar ul.panel-fullscreen").remove();
                $('div.tabsbar').hide();
            } catch (e) {

            }
        }


       


    </script>
</asp:Content>
