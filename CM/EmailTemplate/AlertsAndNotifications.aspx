﻿<%@ Page Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true" CodeFile="AlertsAndNotifications.aspx.cs"
    ValidateRequest="false" EnableEventValidation="false" Inherits="EmailTemplate_AlertsAndNotifications" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab"); 
    </script>
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text="">Alerts and Notifications</asp:Label>
    </h2>
  
        <table width="100%">
            <asp:Repeater ID="rptEvents" runat="server" 
                onitemdatabound="rptEvents_ItemDataBound">
                <HeaderTemplate>
                    <table id="masterDataTable" class="masterTable list issues" width="100%" cellpadding="10">
                        <thead>
                            <tr>
                                <th width="0%" style="display: none;">
                                    EmailTrigger Id
                                </th>
                                <th width="20%">
                                    Events
                                </th>
                                <th width="80%">
                                    Email Template
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td style="display: none">
                             <asp:Label ID="lblEmailTriggerId" runat="server" ClientIDMode="Static" Text='<%#Eval("EmailTriggerId") %>'></asp:Label>
                             <asp:Label ID="lblEmailTemplateIds" runat="server" ClientIDMode="Static" Text='<%#Eval("EmailTemplateIds") %>'></asp:Label>
                        </td>
                        <td style="padding-bottom:50px;">
                            <b><asp:Label ID="lblEvents" runat="server" ClientIDMode="Static" Text='<%#Eval("EmailTriggerName") %>'></asp:Label></b>
                        </td>
                        <td>
                           
                            <select id="ddlEmailTemplate" clientidmode="Static" runat="server" style="width: 90%"
                                class="chzn-select">
                                <option></option>
                            </select>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody> </table>
                </FooterTemplate>
            </asp:Repeater>
        </table>
   
    <asp:Button ID="btnSave" runat="server" Text="Update" OnClick="btnSave_Click" class="btn_validate" />
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
</asp:Content>
