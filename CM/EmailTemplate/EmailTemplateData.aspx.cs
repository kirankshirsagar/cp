﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;
using EmailTemplateBLL;



public partial class EmailTemplate_EmailTemplateData : System.Web.UI.Page
{
    // navigation//
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 10;
    // navigation//

    IEmailTemplate objEmailTemplate;
    public string Add;
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {

        CreateObjects();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            Access.PageAccess(this, Session[Declarations.User].ToString(), "emailtemplate_");
            ViewState[Declarations.Add] = Access.Add.Trim();
            ViewState[Declarations.View] = Access.View.Trim();
            ViewState[Declarations.Delete] = Access.Delete.Trim();
            ViewState[Declarations.Update] = Access.Update.Trim();
        }
        AccessVisibility();
        if (!IsPostBack || hdnSearch.Value.Trim()!=string.Empty)
        {
            Session[Declarations.SortControl] = null;
            rdActive.Checked = true;
            ReadData(1, RecordsPerPage);
            Message();
        
        }
        else
        {
            SetNavigationButtonParameters();
        }

      

    }

    protected void btnSort_Click(object sender, EventArgs e)
    {
        LinkButton clickBtn = (LinkButton)sender;
        Session.Add(Declarations.SortControl, clickBtn);
        ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
    }

    void Message()
    { 
         if (Session[Declarations.Message] != null)
        {
            var flg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }
       
    }

    // navigation//

    #region Navigation
    void SetNavigationButtonParameters()
    {
        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }


    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
            AccessVisibility();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

    } 
    #endregion

    // navigation//


    void ReadData(int pageNo, int recordsPerPage)
    {
        if (View == "Y")
        {
            Page.TraceWrite("ReadData starts.");
            try
            {
                objEmailTemplate.PageNo = pageNo;
                objEmailTemplate.RecordsPerPage = recordsPerPage;
                objEmailTemplate.Search = txtSearch.Value.Trim();
                LinkButton btnSort = null;
                if (Session[Declarations.SortControl] != null && Session[Declarations.SortControl] != string.Empty)
                {
                    btnSort = (LinkButton)Session[Declarations.SortControl];
                    objEmailTemplate.Direction = btnSort.CssClass.ToLower() == "sort desc" ? 1 : 0;
                    objEmailTemplate.SortColumn = btnSort.Text.Trim();
                }
                rptEmailTemplateMaster.DataSource = objEmailTemplate.ReadData();
                rptEmailTemplateMaster.DataBind();
                if (btnSort != null)
                {
                    rptEmailTemplateMaster.ClassChange(btnSort);
                }
                Page.TraceWrite("ReadData ends.");
                ViewState[Declarations.TotalRecord] = objEmailTemplate.TotalRecords;
                PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
                PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
                PaginationButtons1.RecordsPerPage = RecordsPerPage;
            }
            catch (Exception ex)
            {
                Page.TraceWarn("ReadData fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
    }


    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objEmailTemplate = FactoryEmailTemplate.GetEmailTemplateDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);  
         }
        Page.TraceWrite("Page object create ends.");
    }


  
    //protected void btnDelete_Click(object sender, EventArgs e)
    //{
    //    Page.TraceWrite("Delete starts.");
    //    string status = "";
    //    objEmailTemplate.EmailTemplateIds = hdnPrimeIds.Value;
    //    try
    //    {
    //        status = objEmailTemplate.DeleteRecord();
    //    }
    //    catch (Exception ex)
    //    {
    //        hdnPrimeIds.Value = string.Empty;
    //       Page.JavaScriptClientScriptBlock("status", "PageGetMessage('DF')");
    //       Page.TraceWarn("Delete fails.");
    //       Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
    //    }
    //    hdnPrimeIds.Value = string.Empty;
    //    Page.JavaScriptClientScriptBlock("status", "PageGetMessage('DS')");
    //    Page.TraceWrite("Delete ends.");
    //    ReadData(1, RecordsPerPage);
    //    Server.Transfer("EmailTemplateData.aspx"); 
        

    //}


    protected void btnChangeStatus_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Change Status starts.");
        string status = "";
        objEmailTemplate.EmailTemplateIds = hdnPrimeIds.Value;
        try
        {
            objEmailTemplate.isActive = rdActive.Checked == true ? "Y" : "N";
            status = objEmailTemplate.ChangeIsActive();
            ReadData(1, RecordsPerPage);
        }
        catch (Exception ex)
        {
            hdnPrimeIds.Value = string.Empty;
            Page.JavaScriptClientScriptBlock("status", "PageGetMessage('SF')");
            Page.TraceWarn("Change Status fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.JavaScriptClientScriptBlock("status", "PageGetMessage('SS')");
        Page.TraceWrite("Change Status ends.");
    }


    protected void imgEdit_Click(object sender, EventArgs e)
    {
        var edit = Update;
        if (edit == "Y")
            Server.Transfer("EmailTemplate.aspx");
    }

    protected void btnAddRecord_Click(object sender, EventArgs e)
    {
        Server.Transfer("EmailTemplate.aspx");
    }

    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (View == "N")
        {
            Page.extDisableControls();
            btnChangeStatus.Enabled = false;

        }
        else
        {
            btnSearch.Enabled = true;
            txtSearch.Disabled = false;
        }

        if (Update == "N")
        {
            foreach (RepeaterItem item in rptEmailTemplateMaster.Items)
            {
                LinkButton imgEdit = (LinkButton)item.FindControl("imgEdit");
                imgEdit.Enabled = false;
               
            }
           // btnDelete.Enabled = false;
            btnChangeStatus.Enabled = false;
        }
        if (Add == "N")
        {
            Page.extDisableControls();
            btnChangeStatus.Enabled = false;
            //btnAddRecord.Enabled = false;
        }
        else
        {
           // btnAddRecord.Enabled = true;
        }

        if (Delete == "N")
        {
           // btnDelete.Enabled = false;
          //  btnDelete.OnClientClick = null;
        }
        else if (Delete =="Y")
        {
           // btnDelete.Enabled = true;
        }

        Page.TraceWrite("AccessVisibility starts.");
    }
}


    




 


