﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EmailTemplateBLL;
using CommonBLL;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;



public partial class EmailTemplate_EmailTemplate : System.Web.UI.Page
{

    IEmailTemplate objEmailTemplate;
    public string Add;
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        if (!IsPostBack)
        {
            hdnUserName.Value = Session[Declarations.UserFullName]!=null ? Session[Declarations.UserFullName].ToString() : "";
            ContractType();
            RoleBind();
            UserBind();
          
            if (String.IsNullOrEmpty(Request.extValue("hdnPrimeIds")) != true)
            {
                hdnPrimeId.Value = Request.extValue("hdnPrimeIds");
                ReadData();
                lblTitle.Text = "Edit Email Template";
                btnSave.Text = "Update";
            }
            else
            {
                rdActive.Checked = true;
                hdnPrimeId.Value = "0";
                lblTitle.Text = "Add Email Template";
            }

            if (Session[Declarations.User] != null)
            {
                Access.PageAccess(this, Session[Declarations.User].ToString(), "emailtemplate_");
                ViewState[Declarations.Add] = Access.Add.Trim();
                ViewState[Declarations.View] = Access.View.Trim();
                ViewState[Declarations.Delete] = Access.Delete.Trim();
                ViewState[Declarations.Update] = Access.Update.Trim();
            }
            AccessVisibility();
        }
    
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objEmailTemplate = FactoryEmailTemplate.GetEmailTemplateDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }


    void ContractType()
    {
        MasterBLL.IContractType objContract = MasterBLL.FactoryMaster.GetContractTypeDetail();
        //ddlContractType.extDataBind(objContract.SelectData());
    }
    
 
     void RoleBind()
    {
        UserManagementBLL.IRole objRole = UserManagementBLL.FactoryUser.GetRoleDetail();
        ddlRole.extDataBind(objRole.SelectData(1));
        ddlRole.Multiple = true;
    }

    void UserBind()
    {
        UserManagementBLL.IUsers objUser = UserManagementBLL.FactoryUser.GetUsersDetail();
        ddlUser.extDataBind(objUser.SelectData(1));
        ddlUser.Multiple = true;
    }



    protected void btnSave_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        InsertUpdate();
        Page.TraceWrite("Save Update button clicked event ends.");
    }

    protected void SaveAndContinue_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save and Add more button clicked event starts.");
        InsertUpdate(1);
        Page.TraceWrite("Save and Add more button clicked event ends.");
    }

    protected void Back_Click(object sender, EventArgs e)
    {
        Server.Transfer("EmailTemplateData.aspx");
    }

    void ReadData()
    {
        Page.TraceWrite("ReadData starts.");
        try
        {
            objEmailTemplate.EmailTemplateId = int.Parse(hdnPrimeId.Value);
            List<EmailTemplate> EmailTemplate = objEmailTemplate.ReadData();
            txtEmailTemplateName.Value = EmailTemplate[0].EmailTemplateName;
        
            txtReplyTo.Value = EmailTemplate[0].ReplyTo;
            txtSubject.Value = EmailTemplate[0].Subject;

            ddlRole.extSelectedValues(EmailTemplate[0].RoleIds);
            ddlUser.extSelectedValues(EmailTemplate[0].UsersIds);            

            hndSubjectRead.Value = EmailTemplate[0].Subject;
            btnSaveAndContinue.Visible = false;
            hdnBodyTextRead.Value = EmailTemplate[0].TemplateBody;
            rptAttachments.DataSource = objEmailTemplate.AttachmentLists;
            rptAttachments.DataBind();

            if (EmailTemplate[0].isActive == "Y")
            {
                rdActive.Checked = true;
            }
            else
            {
                rdInactive.Checked = true;
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "tytyttrrr", "showConditional('" + txtEmailTemplateName.Value + "');", true);
            ddlEventExecution.extSelectedValues(EmailTemplate[0].EventExecutionType);
            txtEventExecutionValue.Value = EmailTemplate[0].EventExecutionValue.ToString() != "0" ? EmailTemplate[0].EventExecutionValue.ToString() : ""; 
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ReadData fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ReadData ends.");
    }

    void InsertUpdate(int flg =0)
    {
        Page.TraceWrite("Insert, Update starts.");
        string status = "";
        
        objEmailTemplate.EmailTemplateName = txtEmailTemplateName.Value.Trim();
        objEmailTemplate.RoleIds = hdnRoles.Value;
        objEmailTemplate.UsersIds = hdnUsers.Value;
        objEmailTemplate.Subject = hdnSubject.Value;
        objEmailTemplate.TemplateBody = hdnBodyText.Value;
        objEmailTemplate.FileName = hdnAttachnents.Value;
        objEmailTemplate.ReplyTo = txtReplyTo.Value.Trim();
        objEmailTemplate.Attachments = objEmailTemplate.getAttachments();
        objEmailTemplate.Description = "";
        objEmailTemplate.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objEmailTemplate.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objEmailTemplate.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
        objEmailTemplate.isActive = rdActive.Checked == true ? "Y" : "N";
        objEmailTemplate.EventExecutionType = ddlEventExecution.Value;
        if (txtEventExecutionValue.Value.Trim() != string.Empty)
        {
            objEmailTemplate.EventExecutionValue = Convert.ToInt16(txtEventExecutionValue.Value);
        }
        try
        {
            if (hdnPrimeId.Value == "0")
            {
                objEmailTemplate.EmailTemplateId = 0;
                status = objEmailTemplate.InsertRecord();
            }
            else
            {
                objEmailTemplate.EmailTemplateId = int.Parse(hdnPrimeId.Value);
                status = objEmailTemplate.UpdateRecord();
            }
            Page.Message(status, hdnPrimeId.Value);
        }
        catch (Exception ex)
        {
            Page.Message("0", hdnPrimeId.Value);
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Insert, Update ends.");
        if (flg == 0 && status =="1")
        {   
            Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
            Server.Transfer("EmailTemplateData.aspx");
        }
        ClearData(status);


    }

    void ClearData(string status)
    {
        if (status == "1")
        {
            Page.TraceWrite("clearData starts.");
            objEmailTemplate.EmailTemplateId = 0;
           // ddlContractType.Value = "0";
            ddlEventExecution.Value = "On time";
            txtSubject.Value = "";
            txtReplyTo.Value = "";
            txtEventExecutionValue.Value = "";
            txtEmailTemplateName.Value = "";
            Page.JavaScriptStartupScript("clearselected", "ClearAll();");
            Page.TraceWrite("clear Data ends.");            
        }
    }


    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }

    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (Add == "N" && (hdnPrimeId.Value == "" || hdnPrimeId.Value == "0" || hdnPrimeId.Value == null))
        {
            Page.extDisableControls();
            btnBack.Enabled = true;
        }
        if (View == "N")
        {
            btnBack.Enabled = false;
        }
        Page.TraceWrite("AccessVisibility starts.");
    }



}










