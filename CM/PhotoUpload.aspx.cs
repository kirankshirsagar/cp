﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using MD = System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing;

public partial class PhotoUpload : System.Web.UI.Page
{
    string ExtImage = "";

    String path = HttpContext.Current.Request.PhysicalApplicationPath + "UserImages\\";
    protected void Page_Load(object sender, EventArgs e)
    {
        Upload.Attributes.Add("onchange", "document.getElementById('" + btnUpload.ClientID + "').click();");
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        btnCrop.Visible = false;
        imgCropped.Visible = false;
        imgCrop.Visible = false;
        pnlCrop.Visible = false;

        if (Upload.FileBytes.Length == 0)
            return;

        string ExtImage = System.IO.Path.GetExtension(Upload.FileName).TrimStart(".".ToCharArray()).ToLower();
        Int32 maxFileSize = 1048576; //bytes
        Int32 fileSize = Upload.PostedFile.ContentLength;

        if ((ExtImage != "jpeg") && (ExtImage != "jpg") && (ExtImage != "png") && (ExtImage != "gif") && (ExtImage != "bmp"))
        {
            lblError.Text = "Invalid file extension. Please upload valid extension image file.";
            lblError.Visible = true;
            return;
        }
        else if (fileSize > maxFileSize)
             {
                 lblError.Text = "Can not upload image file more than 1 MB.";
                 lblError.Visible = true;
                 return;
             }
        else
            lblError.Visible = false;
        Bitmap uploadedImage = new Bitmap(Upload.FileContent);
        int maxWidth = 480;
        int maxHeight = 0;
        Bitmap resizedImage = GetScaledPicture(uploadedImage, maxWidth, maxHeight);

        try
        {
            Boolean FileOK = false;
            Boolean FileSaved = false;
            if (Upload.HasFile)
            {
                Session["WorkingImage"] = Upload.FileName;
                Session["Extension"] = ExtImage;
                String FileExtension =
                Path.GetExtension(Session["WorkingImage"].ToString()).ToLower();
                String[] allowedExtensions = { ".png", ".jpeg", ".jpg", ".gif" };
                for (int i = 0; i < allowedExtensions.Length; i++)
                {
                    if (FileExtension == allowedExtensions[i])
                    {
                        FileOK = true;
                        lblError.Visible = false;
                    }
                }
            }
            if (FileOK)
            {
                try
                {
                    FileSaved = true;
                }
                catch (Exception ex)
                {
                    lblError.Text = "File could not be uploaded." + ex.Message.ToString();
                    lblError.Visible = true;
                    FileSaved = false;
                }
            }
            else
            {
                lblError.Text = "Invalid file extension. Please upload valid extension image file.";
                lblError.Visible = true;
            }
            if (FileSaved)
            {

                btnCrop.Visible = true;
                imgCropped.Visible = false;
                imgCrop.Visible = true;
                pnlCrop.Visible = true;

                String virtualPath = "~\\UserImages\\TempIMage." + ExtImage;
                String tempFileName = Server.MapPath(virtualPath);
                resizedImage.Save(tempFileName, uploadedImage.RawFormat);

                imgCrop.ImageUrl = "~\\UserImages\\TempIMage." + ExtImage;
                hdnIsImageCropped.Value = "N";
                Upload.Style.Add("margin-top", "10px");
                Noimagespan.Visible = false;
            }
        }

        catch (Exception ex)
        {
            // Log.WriteLog(ex);
        }
    }



    protected Bitmap GetScaledPicture(Bitmap source, int maxWidth, int maxHeight)
    {
        int width, height;
        float aspectRatio = (float)source.Width / (float)source.Height;

        if ((maxHeight > 0) && (maxWidth > 0))
        {
            if ((source.Width < maxWidth) && (source.Height < maxHeight))
            {
                //Return unchanged image
                return source;
            }
            else if (aspectRatio > 1)
            {
                // Calculated width and height,
                // and recalcuate if the height exceeds maxHeight
                width = maxWidth;
                height = (int)(width / aspectRatio);
                if (height > maxHeight)
                {
                    height = maxHeight;
                    width = (int)(height * aspectRatio);
                }
            }
            else
            {
                // Calculated width and height,
                // and recalcuate if the width exceeds maxWidth
                height = maxHeight;
                width = (int)(height * aspectRatio);
                if (width > maxWidth)
                {
                    width = maxWidth;
                    height = (int)(width / aspectRatio);
                }
            }
        }
        else if ((maxHeight == 0) && (source.Width > maxWidth))
        {
            width = maxWidth;
            height = (int)(width / aspectRatio);
        }
        else if ((maxWidth == 0) && (source.Height > maxHeight))
        {
            // If MaxWidth is not provided (unlimited), and the
            // source height exceeds maxHeight, then
            // recalculate width
            height = maxHeight;
            width = (int)(height * aspectRatio);
        }
        else
        {
            //Return unchanged image
            return source;
        }

        Bitmap newImage = GetResizedImage(source, width, height);
        return newImage;
    }


    protected Bitmap GetResizedImage(Bitmap source, int width, int height)
    {
        //This function creates the thumbnail image.
        //The logic is to create a blank image and to
        // draw the source image onto it

        Bitmap thumb = new Bitmap(width, height);
        Graphics gr = Graphics.FromImage(thumb);

        gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
        gr.SmoothingMode = SmoothingMode.HighQuality;
        gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
        gr.CompositingQuality = CompositingQuality.HighQuality;

        gr.DrawImage(source, 0, 0, width, height);
        return thumb;
    }


    protected void btnCrop_Click(object sender, EventArgs e)
    {
        try
        {
            DateTime d = DateTime.Now;
            string dateString = d.ToString("yyyyMMddHHmmss");
         
            imgCrop.Visible = false;
            imgCropped.Visible = false;
            string ImageName = "\\TempImage." + Session["Extension"];
            int x, y, w, h;
            if (W.Value.Contains(".") == true)
            {
                w = Convert.ToInt32(W.Value.Substring(0, W.Value.IndexOf(".")));
            }
            else
            {
                w = Convert.ToInt32(W.Value);
            }

            if (H.Value.Contains(".") == true)
            {
                h = Convert.ToInt32(H.Value.Substring(0, H.Value.IndexOf(".")));
            }
            else
            {
                h = Convert.ToInt32(H.Value);
            }

            if (X.Value.Contains(".") == true)
            {
                x = Convert.ToInt32(X.Value.Substring(0, X.Value.IndexOf(".")));
            }
            else
            {
                x = Convert.ToInt32(X.Value);
            }

            if (Y.Value.Contains(".") == true)
            {
                y = Convert.ToInt32(Y.Value.Substring(0, Y.Value.IndexOf(".")));
            }
            else
            {
                y = Convert.ToInt32(Y.Value);
            }
            string pathimage = "";
            byte[] CropImage = Crop(path + ImageName, w, h, x, y);
            using (MemoryStream ms = new MemoryStream(CropImage, 0, CropImage.Length))
            {
                ms.Write(CropImage, 0, CropImage.Length);
                using (MD.Image CroppedImage = MD.Image.FromStream(ms, true))
                {
                    string ext = ImageName.Substring(ImageName.Length - 4, 4);
                    if (File.Exists(path + "\\" + hdnUid.Value + ext))
                    {
                        File.Delete(path + "\\" + hdnUid.Value + ext);
                    }
                   string SaveTo = path + "\\" + dateString + ext;
                    CroppedImage.Save(SaveTo, CroppedImage.RawFormat);
                    pnlCrop.Visible = false;
                    pnlCropped.Visible = false;
                    pathimage = "..\\\\UserImages\\\\" + dateString + ext;

                }
            }
            hdnIsImageCropped.Value = "Y";
            Upload.Style.Add("margin-top", "0px");

            ScriptManager.RegisterStartupScript(this, this.GetType(), "tytytt", "CloseForm('" + pathimage + "');", true);
                   }
        catch (Exception ex)
        {
            //  Log.WriteLog(ex);
        }
    }



    static byte[] Crop(string Img, int Width, int Height, int X, int Y)
    {
        try
        {
            using (MD.Image OriginalImage = MD.Image.FromFile(Img))
            {
                using (MD.Bitmap bmp = new MD.Bitmap(Width, Height))
                {
                    bmp.SetResolution(OriginalImage.HorizontalResolution,
                    OriginalImage.VerticalResolution);
                    using (MD.Graphics Graphic = MD.Graphics.FromImage(bmp))
                    {
                        Graphic.SmoothingMode = SmoothingMode.AntiAlias;
                        Graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        Graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        Graphic.DrawImage(OriginalImage, new MD.Rectangle(0, 0, Width, Height),
                        X, Y, Width, Height, MD.GraphicsUnit.Pixel);
                        MemoryStream ms = new MemoryStream();
                        bmp.Save(ms, OriginalImage.RawFormat);
                        return ms.GetBuffer();
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            throw (Ex);
        }
    }

}