﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;
using System.Web.Security;


public partial class CMMaster : System.Web.UI.MasterPage
{

    public bool WorkFlowTabAccess;
    public bool SetUpTabAccess;
    public bool CalendarTabAccess;

    protected void Page_Load(object sender, EventArgs e)
    {
        DateTime time = DateTime.Now;              // Use current time
        string format = "dd MMMM yyyy";    // Use this format
        lblCurrentDate.Text = time.ToString(format);  // Write to console       

        if (Session[Declarations.User] != null && !IsPostBack)
        {
            hdnGlobalUserId.Value = Session[Declarations.User].ToString();
            ViewState["WorkFlowTabAccess"] = Access.isCustomised(2);
            ViewState["SetUpTabAccess"] = Access.isCustomised(3);
            ViewState["CalendarTabAccess"] = Access.isCustomised(29);
            string Url = Convert.ToString(Session["ImageUrl"]);
           
            if (Url != "")
            {
                if (!System.IO.File.Exists(Server.MapPath(Url)))
                {
                    imgprofile.Src = "UserImages/getThumb.jpg";
                }
                else
                {
                    imgprofile.Src = "UserImages/" + Url;
                }
            }
            else
            {
                imgprofile.Src = "UserImages/getThumb.jpg";
            }
            HeadLoginName.FormatString = Session[Declarations.UserFullName].ToString() + " [" + Session[Declarations.UserRole].ToString() + "]";
            Session["TenantDIR"] = Request.Url.Segments[1].Replace("/", "");
        }

        setAccessValues();
        try
        {
            if (Session[Declarations.User] == null || Session[Declarations.User].ToString() == string.Empty)
            {
                Response.Redirect("../Login.aspx");
            }
        }
        catch (Exception ex)
        {
        }
    }


    private void setAccessValues()
    {
        if (ViewState["WorkFlowTabAccess"] != null)
        {
            WorkFlowTabAccess = Convert.ToBoolean(ViewState["WorkFlowTabAccess"].ToString());
        }

        if (ViewState["SetUpTabAccess"] != null)
        {
            SetUpTabAccess = Convert.ToBoolean(ViewState["SetUpTabAccess"].ToString());
        }

        if (ViewState["CalendarTabAccess"] != null)
        {
            CalendarTabAccess = Convert.ToBoolean(ViewState["CalendarTabAccess"].ToString());
        }


        if (WorkFlowTabAccess == false)
        {
            requesttab.Visible = false;
        }

        if (SetUpTabAccess == false)
        {
            setuptab.Visible = false;
        }

        if (CalendarTabAccess == false)
        {
            calendarTab.Visible = false;
        }

    }


    private void CheckSessionTimeout()
{
    //Session[Declarations.User] = null;
    //string msgSession = "Warning: Within next 1 minutes, if you do not do anything,our system will redirect to the login page. Please save changed data.";
    //time to remind, 3 minutes before session ends
    int int_MilliSecondsTimeReminder = (this.Session.Timeout * 60000) - 10 * 60000;
    //time to redirect, 5 milliseconds before session ends
    int int_MilliSecondsTimeOut = (this.Session.Timeout * 60000) - 5;
    string str = "login.aspx";
    string str_Script = @"
            var myTimeReminder, myTimeOut; 
            clearTimeout(myTimeReminder); 
            clearTimeout(myTimeOut); " +
            "var sessionTimeReminder = " +
        int_MilliSecondsTimeReminder.ToString() + "; " +
            "var sessionTimeout = " + int_MilliSecondsTimeOut.ToString() + ";" +
            "function doReminder(){}" +
            "function doRedirect(){ window.location.href='" + str + "'; }" + @"
            myTimeReminder=setTimeout('doReminder()', sessionTimeReminder); 
            myTimeOut=setTimeout('doRedirect()', sessionTimeout); ";

     ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), 
           "CheckSessionOut", str_Script, true);
}
  
    

    public static string getCurrentUserGUID()
    {
        if (HttpContext.Current.User.Identity.IsAuthenticated ==  true)
        {
            MembershipUser myObject;
            myObject = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            return myObject.ProviderUserKey.ToString();
        }
        return null;
    }
    //protected void lnkGlobalSearch_Click(object sender, EventArgs e)
    //{
    //    Server.Transfer("../Workflow/GlobalSearch.aspx?GlobalSearch=" + txtGlobalSearch.Text);
    //}

    //public string EncryptQueryString(string strQueryString)
    //{
    //    EncryptDecryptQueryString objEDQueryString = new EncryptDecryptQueryString();
    //    return objEDQueryString.Encrypt(strQueryString, "your_Key"); //the key must be only 8 digit in length        
    //}
}
