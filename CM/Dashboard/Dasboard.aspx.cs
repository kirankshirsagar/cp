﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Data.SqlClient;
using DashboardBLL;
using CommonBLL;



public partial class Dashboard_Dasboard : System.Web.UI.Page
{
    IChart objChart;
    public string chartData { get; set; }
    public string ContractTypeVsContractStatusData { get; set; }
    public string CustomerSupplierAnalysisData { get; set; }
    public string chartWorldMapData { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObject();
        List<Chart> _data = new List<Chart>();
        Session["MasterSeelcted"] = "";
        Session["ChlidSeelcted"] = "";
        if (Session[Declarations.User] != null )
        {
            objChart.UserId = Session[Declarations.User].ToString();          
        }
        _data = objChart.GetBaseCurrency();
        hdnBaseCurrency.Value = _data[0].BaseCurrency;
        Session["BaseCurrency"] = hdnBaseCurrency.Value;
        
        objChart.Tenure = hdnTenure.Value;
        _data= objChart.DrawPieChart();
         PieDrawChart(_data);
        _data.Clear();
        _data=objChart.DrawContractTypeVsContractStatus();
         ContractTypeVsContractStatusChartd(_data);
        _data.Clear();
        _data=objChart.DrawCustomerSupplier();
         CustomerSupplierAnalysisChart(_data);
        _data.Clear();
        _data=objChart.DrawMyContrats();
        MyContratsChart(_data);
        _data.Clear();
        Session["TenureD"] = hdnTenure.Value;     
    }

    public void CreateObject()
    {
        objChart = FactoryChart.GetChartReference();
    }

    public void PieDrawChart(List<Chart> _data)
    {
        JavaScriptSerializer jss = new JavaScriptSerializer();
        chartData = jss.Serialize(_data); //this make your list in jSON format like [88,99,10]    
    }

    public void MyContratsChart(List<Chart> _data)
    {
        TotalContracts.InnerText = _data[0].TotalContracts.ToString();
        DayActivities.InnerText = _data[0].DayActivities.ToString();
        WeekActivities.InnerText = _data[0].WeekActivities.ToString();
        MyContracts.InnerText = _data[0].MyContracts.ToString();
        MyApprovals.InnerText = _data[0].MyApprovals.ToString();
        TotalAwaitingContracts.InnerText = _data[0].AwaitingsignaturesCount.ToString();
    
    }

    

    public void ContractTypeVsContractStatusChartd(List<Chart> _data)
    {
        JavaScriptSerializer jss = new JavaScriptSerializer();
        ContractTypeVsContractStatusData = jss.Serialize(_data); //this make your list in jSON format like [88,99,10]

    }

  
    public void CustomerSupplierAnalysisChart(List<Chart> _data)
    {
        JavaScriptSerializer jss = new JavaScriptSerializer();
        CustomerSupplierAnalysisData = jss.Serialize(_data); //this make your list in jSON format like [88,99,10]

    }
    //public void WorldMapDataChart(List<Chart> _data)
    //{
    //    JavaScriptSerializer jss = new JavaScriptSerializer();
    //    chartWorldMapData = jss.Serialize(_data); //this make your list in jSON format like [88,99,10]

    //}



}