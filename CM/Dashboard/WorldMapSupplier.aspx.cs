﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Data.SqlClient;
using DashboardBLL;
using CommonBLL;

public partial class Dashboard_WorldMapSupplier : System.Web.UI.Page
{
    IChart objChart;
    public string chartWorldMapData { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObject();
        string Op = Session["TenureD"].ToString();
        List<Chart> _data = new List<Chart>();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            objChart.UserId = Session[Declarations.User].ToString();
        }
        objChart.Tenure = Op;
        _data = objChart.chartWorldMapData();
        WorldMapDataChart(_data);
        _data.Clear();
    }

    public void CreateObject()
    {
        objChart = FactoryChart.GetChartReference();
    }
    public void WorldMapDataChart(List<Chart> _data)
    {
        JavaScriptSerializer jss = new JavaScriptSerializer();
        chartWorldMapData = jss.Serialize(_data); //this make your list in jSON format like [88,99,10]
    }

}