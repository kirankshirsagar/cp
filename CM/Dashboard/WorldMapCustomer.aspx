﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WorldMapCustomer.aspx.cs" Inherits="Dashboard_WorldMapCustomer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
  <div style="text-align:right"> <img src="print.gif" id="btnPrint" /></div>
  <div id="ContainerChart5" style="min-width:220px; height:380px; border-style: none; margin: 0 auto">
  <div class="loading">
		<i class="icon-spinner icon-spin icon-large"></i>
		Please wait...
	</div>
  </div>
</body>


<%--<script src="http://code.jquery.com/jquery-1.9.0.js"></script>
<script src="http://code.highcharts.com/maps/highmaps.js"></script>
<script src="http://code.highcharts.com/maps/modules/data.js"></script>
<script src="http://code.highcharts.com/mapdata/custom/world.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>--%>

<script src="JS/jquery-1.9.0.js" type="text/javascript"></script>
<script src="JS/highmaps.js" type="text/javascript"></script>
<script src="JS/data.js" type="text/javascript"></script>
<script src="JS/world.js" type="text/javascript"></script>
<script src="JS/exporting.js" type="text/javascript"></script>

<script type="text/javascript">
// debugger;
    var BaseCurrency='<%= Session["BaseCurrency"] %>';
    $(document).ready(function () {

       $("#btnPrint").on("click", function() {
            var divContents = $("#ContainerChart5").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>DIV Contents</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
           });




        var dataMap =<%= chartWorldMapData%> ;
        
        $(function () {

//            // Load the data from a Google Spreadsheet
//            // https://docs.google.com/a/highsoft.com/spreadsheet/pub?hl=en_GB&hl=en_GB&key=0AoIaUO7wH1HwdFJHaFI4eUJDYlVna3k5TlpuXzZubHc&output=html
            Highcharts.data({
                googleSpreadsheetKey: '0AoIaUO7wH1HwdFJHaFI4eUJDYlVna3k5TlpuXzZubHc',

                // custom handler when the spreadsheet is parsed
                parsed: function (columns) {

                    // Read the columns into the data array
                    var data = [];
                    var CountryCode = [];
                    var CountryName = [];

                    for(var i=0;i<dataMap.length;i++)
                    {
                
                          data.push({
                            code: dataMap[i].CountryCode,
                            value: dataMap[i].CustomerCount,
                            name:dataMap[i].Country,
                            value1: dataMap[i].CustomerRevenue,
                            value2: dataMap[i].SupplierRevenue,
                            CustomerCount: dataMap[i].CustomerCount,
                            SupplierCount: dataMap[i].SupplierCount
                        });

                    }

                    // Initiate the chart
                    $('#ContainerChart5').highcharts('Map', {
                        chart: {
                            borderWidth: 1
                        },
                         exporting: { enabled: false },
                      

                        title: {
                            text: ''
                        },

                        mapNavigation: {
                            enabled: true
                        },

                        legend: {
                            title: {
                                text: '',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                                }
                            },
                            align: 'left',
                            verticalAlign: 'bottom',
                            floating: true,
                            layout: 'vertical',
                            valueDecimals: 0,
                            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255, 255, 255, 0.85)',
                            symbolRadius: 0,
                            symbolHeight: 14
                        },

                        colorAxis: {
                            dataClasses: [
                            {
                                 from:0 ,
                                 to:0 ,
                                 name:'No Customers',
                                 color:'#ABE7C8'
                            },
                           {
                                from:1 ,
                                to:10000000000000000 ,
                                name:'Customer',
                                color:'#F85C2C'

                            }]
                        },
                          credits: {
                                 enabled: false
                            },

                        series: [{
                            data: data,
                            mapData: Highcharts.maps['custom/world'],
                            joinBy: ['iso-a2', 'code'],
                            animation: true,
                            name:' ',
                            showInLegend :false,
                            states: 
                            {
                                hover: 
                                {
                                    color: ''
                                }
                            },
                            tooltip: 
                            {
                            pointFormat: '<b>{point.name}<b><br> Number of Customer(s):{point.CustomerCount}<br> Customer revenue:<b>{point.value1} '+BaseCurrency+'</b>'
                            }
                        }]
                    });
                },
                error: function () {
                    $('#ContainerChart5').html('<div class="loading">' +
                '<i class="icon-frown icon-large"></i> ' +
                'Error loading data from Google Spreadsheets' +
                '</div>');
                }
            });



        });

        
    });





</script>

</html>
