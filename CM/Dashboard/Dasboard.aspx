﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="Dasboard.aspx.cs" Inherits="Dashboard_Dasboard" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <%--  <script src="http://code.jquery.com/jquery-1.9.0.js" type="text/javascript"></script>
    <script src="http://code.highcharts.com/stock/highstock.js" type="text/javascript"></script>
    <script src="http://code.highcharts.com/stock/modules/exporting.js" type="text/javascript"></script>--%>
    <script src="../JS/Highchart/exporting.js" type="text/javascript"></script>
    <script src="../JS/Highchart/highcharts-3d.js" type="text/javascript"></script>
    <script src="../JS/Highchart/highcharts.js" type="text/javascript"></script>
    <style type="text/css">
        .loading
        {
            margin-top: 10em;
            text-align: center;
            color: gray;
        }
        .highcharts-container
        {
            position: inherit !important;
        }
    </style>
    <input type="hidden" runat="Server" id="hdnBaseCurrency" clientidmode="static" />
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());

    </script>
    <script type="text/javascript">
  var BaseCurrency=$("#hdnBaseCurrency").val();

  var data =<%= CustomerSupplierAnalysisData%> ;
    var xCategoriess = new Array();

    var is, cats;

    for(is = 0; is < data.length; is++){
     cats = data[is].MonthYear;
  
     if(xCategoriess.indexOf(cats) === -1)
     {

        xCategoriess.push(cats);
     }
     }

        // split the data set into ohlc and volume
        var ohlc = [];
        var volume = [];
        var dataLength = data.length;
        var i = 0;
        for (i; i < dataLength; i += 1) {
             ohlc.push([
                 data[i].MonthYear,
                 data[i].CustomerRevenue // Revenue             
            ]); 
        }
        i=0;
            for (i; i < dataLength; i += 1) {
             volume.push([
                     data[i].MonthYear,
                   data[i].SupplierRevenue // / Revenue
            ]);     
        }



    $(document).ready(function() {
  
           $('#iFramesrc').attr('src', "WorldMapCustomer.aspx?Op=" + $('#hdnTenure').val());
           $('#iFrame1').attr('src', "WorldMapSupplier.aspx?Op=" + $('#hdnTenure').val());

//       $('#CustomerSupplierDiv').highcharts('StockChart', {

//            chart: {
//                renderTo: 'CustomerSupplierDiv',
//                zoomType: 'xy'
//            },
//              rangeSelector: {
//                    inputEnabled:false
//    
//                },

           // exporting: { enabled: false },
//            title: {
//                text: ''
//            },
//            legend:{
//                itemStyle: {
//                fontSize: '13px',
//                fontFamily: 'Arial,sans-serif'
//                }
//            },
//            xAxis: {
//                type: 'datetime',
//                labels: {
//                step: 1,
//		               	style: {
//		                        display: 'none',
//		                        fontFamily: 'Arial,sans-serif'
//		                        }
//		                },
//		                dateTimeLabelFormats: { // don't display the dummy year
//		                    month: '%b \'%y',
//			                year: '%Y'
//		                }
//            		},
//             yAxis: [{
//                    labels: 
//                    {
//                        align: 'right',
//                        x: -3
//                    },
//                    title: 
//                    {
//                        text: 'Customer'
//                    },

//                    height: '60%',
//                    lineWidth: 2

//                }, {
//                    labels: {
//                        align: 'right',
//                        x: -3
//                    },
//                    title: {
//                        text: 'Supplier'
//                    },
//                    top: '65%',
//                    height: '35%',
//                    offset: 0,
//                    lineWidth: 2
//                }],
//           tooltip: {
//            		enabled: true,
//                       valueSuffix: ' GBP'
//        		},
//         
//            credits: {
//                enabled: false
//            },
//               series: [{
//                    type: 'spline',
//                    name: 'Customer',
//                    color:'black',
//                    data: ohlc
//       
//                }, {
//                    type: 'area',
//                    name: 'Supplier',
//                    color:'#999EFF',
//                    data: volume,
//                 
//                    yAxis: 1
//                }]
//        });


    $(function () {
    $('#CustomerSupplierDiv').highcharts({
        chart: {
            zoomType: 'xy'
   
        },
//          exporting: {
//        buttons: {
//            exportButton: {
//                symbolFill: '#55BE3B'
//            },
//            printButton: {
//                symbolFill: '#7797BE'
//            }
//        }
//    },

            exporting: { enabled: false },
        title: {
            text: 'Customer/Supplier Analysis'
        },
      
        xAxis: [{
            categories: xCategoriess,
              labels: {
                rotation: -30,
                antialiasing:false}
                ,title: {
             text: 'Months',
                style: {
                    color:'black',
                       fontWeight: 'normal'
                }
          }

        }
          
        ],
         credits: {
           enabled: false
            },
        yAxis: [{ // Primary yAxis
            labels: {
             enabled: false,
                format: '{value}°C',
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            opposite: false,
             min: 0

        },
         { // Secondary yAxis
        enabled:false,
           min: 0,
            gridLineWidth: 0,
            title: {
                text: 'Revenue in '+BaseCurrency,
                style: {
                    color:'black'
                }
            },
            labels: {
                format: '{value} '+BaseCurrency,
                style: {
                    color:'black'
             
                }
            }

        }],
        tooltip: {
            shared: true
        },
         scrollbar: {
        enabled: true
    },
        legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom',
            floating: false,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [{
            name: 'Customer',
            type: 'line',
            yAxis: 1,
            color:'#2D88EF',
            data:ohlc,
            tooltip: {
                valueSuffix: ' '+BaseCurrency
            }

        }, {
            name: 'Supplier',
            type: 'area',
            yAxis:1,
            data:volume,
            color:'#06B445',
            marker: {
                enabled: true
            },
            dashStyle: 'area',
            tooltip: {
                valueSuffix: ' '+BaseCurrency
            },

        }]
        });
    });

        $('.highcharts-button').hide();
        $('.highcharts-scrollbar').next('text').remove();
    });
    </script>
    <script src="http://code.highcharts.com/highcharts-3d.js"></script>
    <script src="http://code.highcharts.com/modules/exporting.js"></script>
    <script type="text/javascript">
    function OpenActivities(obj) 
    {
        location.href = "../Calendar/CalendarAgenda.aspx?ContractType=" + obj;    
    }
  $(document).ready(function(){
  Array.prototype.insert = function(index) {
    this.splice.apply(this, [index, 0].concat(
        Array.prototype.slice.call(arguments, 1)));
    return this;
};
$(function () {
var data=<%= ContractTypeVsContractStatusData%>

var seriesData = [];
var xCategories = [];
var yCategories = [];
var StatusAr=['Active Contract',
'Awaiting approval', 
'Awaiting signatures',
'Contract Generation Request submitted',
'Contract Renewal Request submitted',
'Contract Revision request submitted',
'Contract termination request submitted',
'Expired Contract',
'Negotiation Stage',
'New Contract Review request submitted',
'Request in progress',
'Terminated Contract']

var i, cat;

for(i = 0; i < data.length; i++){
     cat = data[i].ContractTypeName;

     if(xCategories.indexOf(cat) === -1){
        xCategories[xCategories.length] = cat;
     }

}




 var dataArr=[];
 var MyA=new Array();
      


      

for(x=0;x<xCategories.length;x++)
{
dataArr[x]=0;
}

       $.each(data, function(j, data)
   {

     if(data.StatusName==StatusAr[0]  )
      {

      

                       dataArr[xCategories.indexOf(data.ContractTypeName)]=data.Count;

   
      }

 



    });

    // debugger;
      MyA.push( {name: StatusAr[0], data:dataArr});




      
      
 var dataArr=new Array();
for(x=0;x<xCategories.length;x++)
{
dataArr[x]=0;
}

       $.each(data, function(j, data)
   {



     if(data.StatusName.trim()==StatusAr[1]  )
      {
                       dataArr[xCategories.indexOf(data.ContractTypeName)]=data.Count;
      }

 
    });

    // debugger;
      MyA.push( {name: StatusAr[1], data:dataArr});





      
      
 var dataArr=new Array();
for(x=0;x<xCategories.length;x++)
{
dataArr[x]=0;
}

       $.each(data, function(j, data)
   {

     if(data.StatusName==StatusAr[2]  )
      {

      

                     dataArr[xCategories.indexOf(data.ContractTypeName)]=data.Count;

   
      }

 



    });

    // debugger;
      MyA.push( {name: StatusAr[2], data:dataArr});




      
 var dataArr=new Array();
for(x=0;x<xCategories.length;x++)
{
dataArr[x]=0;
}

       $.each(data, function(j, data)
   {

     if(data.StatusName==StatusAr[3]  )
      {

      

                    dataArr[xCategories.indexOf(data.ContractTypeName)]=data.Count;

   
      }

 



    });

    // debugger;
      MyA.push( {name: StatusAr[3], data:dataArr});


            
 var dataArr=new Array();
for(x=0;x<xCategories.length;x++)
{
dataArr[x]=0;
}

       $.each(data, function(j, data)
   {

     if(data.StatusName==StatusAr[4]  )
      {

      

                 dataArr[xCategories.indexOf(data.ContractTypeName)]=data.Count;

   
      }

 



    });

    // debugger;
      MyA.push( {name: StatusAr[4], data:dataArr});





            
 var dataArr=new Array();
for(x=0;x<xCategories.length;x++)
{
dataArr[x]=0;
}

       $.each(data, function(j, data)
   {

     if(data.StatusName==StatusAr[5]  )
      {

      

                  dataArr[xCategories.indexOf(data.ContractTypeName)]=data.Count;

   
      }

 



    });

    // debugger;
      MyA.push( {name: StatusAr[5], data:dataArr});





      
 var dataArr=new Array();
for(x=0;x<xCategories.length;x++)
{
dataArr[x]=0;
}

       $.each(data, function(j, data)
   {

     if(data.StatusName==StatusAr[6]  )
      {

      

           dataArr[xCategories.indexOf(data.ContractTypeName)]=data.Count;

   
      }

 



    });

    // debugger;
      MyA.push( {name: StatusAr[6], data:dataArr});









      
      
 var dataArr=new Array();
for(x=0;x<xCategories.length;x++)
{
dataArr[x]=0;
}

       $.each(data, function(j, data)
   {

     if(data.StatusName==StatusAr[7]  )
      {

                dataArr[xCategories.indexOf(data.ContractTypeName)]=data.Count;

       

   
      }

 



    });

    // debugger;
      MyA.push( {name: StatusAr[7], data:dataArr});





      
      
      
 var dataArr=new Array();
for(x=0;x<xCategories.length;x++)
{
dataArr[x]=0;
}

       $.each(data, function(j, data)
   {

     if(data.StatusName.trim()==StatusAr[8]  )
      {
                 dataArr[xCategories.indexOf(data.ContractTypeName)]=data.Count;
      }

 



    });


      MyA.push( {name: StatusAr[8], data:dataArr});





      
      
      
 var dataArr=new Array();
for(x=0;x<xCategories.length;x++)
{
dataArr[x]=0;
}

       $.each(data, function(j, data)
   {

     if(data.StatusName==StatusAr[9]  )
      {

      

                    dataArr[xCategories.indexOf(data.ContractTypeName)]=data.Count;

   
      }

 



    });

    // debugger;
      MyA.push( {name: StatusAr[9], data:dataArr});





      
      
 var dataArr=new Array();
for(x=0;x<xCategories.length;x++)
{
dataArr[x]=0;
}

       $.each(data, function(j, data)
   {

     if(data.StatusName==StatusAr[10]  )
      {

      

           dataArr[xCategories.indexOf(data.ContractTypeName)]=data.Count;

   
      }

 



    });

    // debugger;
      MyA.push( {name: StatusAr[10], data:dataArr});



      
 var dataArr=new Array();
for(x=0;x<xCategories.length;x++)
{
dataArr[x]=0;
}

       $.each(data, function(j, data)
   {

     if(data.StatusName==StatusAr[11]  )
      {

      
                 dataArr[xCategories.indexOf(data.ContractTypeName)]=data.Count;

   
      }

 



    });

    // debugger;
      MyA.push( {name: StatusAr[11], data:dataArr});



            
 var dataArr=new Array();

  
       var chart1;
    
        chart1 = new Highcharts.Chart({
         colors: ['#FAC130', '#F87B00', '#06B445', '#53658D', '#2D88EF', '#D34927', 
             '#5938B4', '#A43AE3', '#6AF9C4'],
            chart: {
                renderTo: 'ContainerChart1',
                type: 'column',
                spacingBottom:0
     
            },

           exporting: { enabled: false },


            title: {
     
                text: 'Contract Type Vs Contract Status'
            },
            xAxis: {
                categories: xCategories,
                  labels: {

                rotation:-30,
                antialiasing:false,
                fontFamily: 'verdana'
                }
    
            },
              credits: {
           enabled: false
            },
            yAxis: {
                min: 0,

                title: {
                    text: 'Total number of contracts'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'green'
                    }
                },
                  tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: false
        },
            },
            legend: {
                       enabled: false
            },
            plotOptions: {
                            column: {
                                    stacking: 'bar',
                                    dataLabels: {
                                            enabled: false,
                                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                                            style: {
                                                textShadow: '0 0 3px white, 0 0 3px black'
                                            },
                                            formatter: function() {
                                                return Highcharts.numberFormat(parseFloat(this.point.percentage).toFixed(2), 2)+"%";
                                            }
                                    }
                            },
                            series: {
                                        cursor: 'pointer',
                                        point: {
                                            events: {
                                                click: function() {
                                                    var ContractType=this.category.toString();
                                                    var RequestType=this.series.name.toString();
                                                    var Tenure= $('#hdnTenure').val();
                                                    window.location.href="../WorkFlow/ContractRequestData.aspx?DashboardRequestType=CTS&CType="+ContractType+"&CStatus="+RequestType+"&Tenure="+Tenure;                                                 
                                                }
                                            }
                                        }
                                    }
                    },
            series: MyA
        });
    });





    



        var chartData=new Array();
        var PP;
        for( i=0; i< <%= chartData%>.length; i++)
        {
         chartData.push([<%=chartData%>[i]['SenderName'], <%= chartData%>[i]['PaymentStatus'] ]);
        }
        chart = new Highcharts.Chart({
        chart: {
            type: 'pie',
             renderTo: 'ContainerChart3',
            options3d: {
                enabled: true,
                alpha: 35,
                beta:0
            }
        },
                exporting: { enabled:false },
        title: {
            text: ''
        },
        credits: {
      enabled: false
  },
       tooltip: {
      formatter: function () 
      {
       return '<b>'+this.point.name+'</b>: ' + this.point.y+"%";
      }
       },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled:false,
                    format: '{point.name}'
                },
                  showInLegend: true

            }
        },
        series: [{
            type: 'pie',
            name: 'Number Of Requests',
            data: chartData
          
        }]



    });


    });


  
    </script>
    <script type="text/javascript">
        function OpenActivities(obj) {
            location.href = "../Calendar/CalendarAgenda.aspx?OpenActivitiestype=" + obj;
        }
        function OpenRequests(obj) {
            location.href = "../Workflow/ContractRequestData.aspx?DashboardRequestType=" + obj + "&Tenure=" + $('#hdnTenure').val() + "";
        }
        function SetDDlValues(obj) {
            $('#hdnTenure').val(obj);
        }
        $(window).load(function () {
            $('#ContainerChart1').find('.highcharts-button:eq(0)').each(function () {
                $(this).remove();
            });
        });
    </script>
    <div class="dashdiv">
        <asp:Button runat="server" Text="Current Year" ID="btnCurrentYear" OnClientClick="return SetDDlValues('1');"
            class="btn_validate" />
        <asp:Button runat="server" Text="Last 3 Years" ID="btnLast3Years" OnClientClick="return SetDDlValues('3');"
            class="btn_validate" />
        <asp:Button runat="server" Text="Last 5 Years" ID="btnLast5Years" OnClientClick="return SetDDlValues('5');"
            class="btn_validate" />
        <input type="hidden" runat="Server" id="hdnTenure" clientidmode="static" value="5"></input>
        <table width="100%">
            <tr valign="top">
                <td width="60%">
                    <div id="ContainerChart1" style="min-width: 100%; height: 428px; margin: 0 auto">
                    </div>
                </td>
                <td width="40%" style="padding-left: 15px;">
                    <table width="100%">
                        <tr>
                            <td width="50%">
                                <div onclick="OpenRequests('T')" style="cursor: pointer;">
                                    <table width="95%" style="padding-top: 10px; width: 165px; background-color: #1EAD65;
                                        height: 90px;">
                                        <tr>
                                            <td width="10%">
                                                <img src="../images/metro-btn-icon-6.png">
                                            </td>
                                            <td style="text-align: left" width="90%">
                                                <span style="color: White; font-size: 40px; text-align: left"><a href="#" id="TotalContracts"
                                                    onclick="OpenRequests('T')" runat="Server" style="text-decoration: none; color: white;font-size: 25px !important;font-weight: bold;">
                                                </a></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <span style="color: White; font-size: 15px;">Total active contracts </span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td width="50%">
                                <div onclick="OpenRequests('AWC')" style="cursor: pointer;">
                                    <table width="95%" style="background-color: #814997;width: 165px; height: 90px;">
                                        <tr>
                                            <td width="10%">
                                                <img src="../images/metro-btn-icon-8.png" width="100px" height="86px">
                                            </td>
                                            <td style="text-align: left" width="90%">
                                                <span style="color: White; font-size: 40px; text-align: left"><a href="#" onclick="OpenRequests('AWC')"
                                                    id="TotalAwaitingContracts" runat="Server" style="text-decoration: none; color: white;font-size: 25px !important;font-weight: bold;"></a></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <span style="color: White; font-size: 14px;">Awaiting signature contract</span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table width="100%">
                        <tr>
                            <td width="50%">
                                <div onclick="OpenActivities('D')" style="cursor: pointer;">
                                    <table width="95%" style="background-color: #F85C2B; width: 165px; height: 140px;">
                                        <tr>
                                            <td width="10%">
                                                <img src="../images/metro-btn-icon-5.png">
                                            </td>
                                            <td style="text-align: left" width="90%">
                                                <span style="color: White; font-size: 40px; text-align: left"><a href="#" onclick="OpenActivities('D')"
                                                    id="DayActivities" runat="Server" style="text-decoration: none; color: white;font-size: 25px !important;font-weight: bold;"></a>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <span style="color: White; font-size: 15px;">Open activities for today</span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td width="50%">
                                <div onclick="OpenActivities('W')" style="cursor: pointer;">
                                    <table width="95%" style="background-color: #226F9D; width: 165px; height: 140px;">
                                        <tr>
                                            <td width="10%">
                                                <img src="../images/metro-btn-icon-5.png">
                                            </td>
                                            <td style="text-align: left" width="95%">
                                                <span style="color: White; font-size: 40px; text-align: left"><a href="#" onclick="OpenActivities('W')"
                                                    id="WeekActivities" runat="Server" style="text-decoration: none; color: white;font-size: 25px !important;font-weight: bold;"></a>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <span style="color: White; font-size: 15px;">Open activities for the week</span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table width="100%">
                        <tr>
                            <td width="50%">
                                <div onclick="OpenRequests('PC')" style="cursor: pointer;">
                                    <table width="95%" style="background-color: #F89C2C; width: 165px; height: 140px;">
                                        <tr>
                                            <td width="10%">
                                                <img src="../images/metro-btn-icon-12.png">
                                            </td>
                                            <td style="text-align: left" width="90%">
                                                <span style="color: White; font-size: 40px; text-align: left"><a href="#" onclick="OpenRequests('PC')"
                                                    id="MyContracts" runat="Server" style="text-decoration: none; color: white;font-size: 25px !important;font-weight: bold;"></a>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <span style="color: White; font-size: 15px;">Priority Contract</span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td width="50%">
                                <div onclick="OpenRequests('MA')" style="cursor: pointer;">
                                    <table width="95%" style="background-color: #777777; width: 165px; height: 140px;">
                                        <tr>
                                            <td width="10%">
                                                <img src="../images/metro-btn-icon-10.png">
                                            </td>
                                            <td style="text-align: left" width="90%">
                                                <span style="color: White; font-size: 40px; text-align: left"><a href="#" onclick="OpenRequests('MA')"
                                                    id="MyApprovals" runat="Server" style="text-decoration: none; color: white;font-size: 25px !important;font-weight: bold;"></a>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <span style="color: White; font-size: 15px;">Approval requests</span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br>
        <table width="100%" style="margin-left: 30px">
            <tr valign="top">
                <td width="50%">
                    <table width="94%">
                        <tr>
                            <td style="background-color: #F89C2C; height: 24px; -moz-border-radius-topleft: 5px;
                                -moz-border-radius-topright: 5px; -webkit-border-top-left-radius: 5px; -webkit-border-top-right-radius: 5px;">
                                <span style="color: White; font-size: 15px;">Customer/Supplier Analysis</span>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #FFFFFF;">
                                <div id="CustomerSupplierDiv" style="width: 100%; height: 400px; margin: 0 auto">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="50%">
                    <table width="94%">
                        <tr>
                            <td style="background-color: #23709E; height: 24px; -moz-border-radius-topleft: 5px;
                                -moz-border-radius-topright: 5px; -webkit-border-top-left-radius: 5px; -webkit-border-top-right-radius: 5px;">
                                <span style="color: White; font-size: 15px;">Contract Type Analysis</span>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #FFFFFF;">
                                <div id="ContainerChart3" style="height: 350px; margin-top: 0px auto">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr valign="top">
                <td width="50%">
                    <table width="98%">
                        <tr>
                            <td style="background-color: #40B97C; width: 50%; height: 24px; -moz-border-radius-topleft: 5px;
                                -moz-border-radius-topright: 5px; -webkit-border-top-left-radius: 5px; -webkit-border-top-right-radius: 5px;">
                                <span style="color: White; font-size: 15px;">Customer Geographical Analysis</span>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #FFFFFF; width: 50%; border-style: none">
                                <iframe src="WorldMapCustomer.aspx" id="iFramesrc" width="102%" style="margin-top: -10px;
                                    margin-left: -10px" height="420px" frameborder="0" scrolling="no"></iframe>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="50%">
                    <table width="98%" style="margin-top: 3px;">
                        <tr>
                            <td style="background-color: #F89C2C; width: 50%; height: 24px; -moz-border-radius-topleft: 5px;
                                -moz-border-radius-topright: 5px; -webkit-border-top-left-radius: 5px; -webkit-border-top-right-radius: 5px;">
                                <span style="color: White; font-size: 15px;">Supplier Geographical Analysis</span>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #FFFFFF; width: 50%; border-style: none">
                                <iframe src="WorldMapSupplier.aspx" id="iFrame1" width="102%" height="420px" style="margin-top: -10px;
                                    margin-left: -10px" frameborder="0" scrolling="no" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <script src="http://code.highcharts.com/highcharts.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {


            //$('#iFramesrc').attr('src', "WorldMapCustomer.aspx?Op=" + $('#hdnTenure').val());
            window.frames["iFramesrc"].href = "WorldMapCustomer.aspx?Op=" + $('#hdnTenure').val()
            window.frames["iFrame1"].href = "WorldMapSupplier.aspx?Op=" + $('#hdnTenure').val()

        });
  
    </script>
</asp:Content>
