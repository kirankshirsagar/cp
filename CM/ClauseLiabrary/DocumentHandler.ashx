﻿<%@ WebHandler Language="C#" Class="DocumentHandler" %>

using System;
using System.Web;
using CommonBLL;
using ClauseLibraryBLL;
using WorkflowBLL;
using System.Web.SessionState;

public class DocumentHandler : IHttpHandler, IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {
        //context.Response.ContentType = "text/plain";
        //context.Response.Write("Hello World");
        //string Operation = context.Request.QueryString["Operation"];

        /***********File Uploading Part Starts Here******/

        IClauseLibrary ObjDocumentType;
        ObjDocumentType = FactoryClause.GetContractTypesDetails();
        System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
        foreach (string file in context.Request.Files)
        {
            HttpPostedFile hpf = context.Request.Files[file] as HttpPostedFile;
            int chunk = context.Request["chunk"] != null ? int.Parse(context.Request["chunk"]) : 0;

            string FileName = Convert.ToString(context.Request.QueryString["filename"]);
            string DocumentTypeId = Convert.ToString(context.Request.QueryString["DocumentTypeId"]);
            string UserID = Convert.ToString(context.Request.QueryString["UserID"]);

            #region Bharati 24062014 6.05PM
            string RequestID = "0";
            if (context.Request.QueryString["RequestID"] != null)
            {
                RequestID = Convert.ToString(context.Request.QueryString["RequestID"]);
            }
            #endregion

            string IsOCR = context.Request.QueryString["IsOCR"] == "true" ? "Y" : "N";
            // string FileName = context.Request["name"] != null ? context.Request["name"] : string.Empty;

            ////   string FileName = string.Empty;
            //   if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
            //   {
            //       string[] files = hpf.FileName.Split(new char[] { '\\' });
            //       FileName = files[files.Length - 1] != "blob" ? files[files.Length - 1] : FileName;
            //   }
            //   else
            //   {
            //       FileName = hpf.FileName != "blob" ? hpf.FileName : FileName;
            //   }
            if (hpf.ContentLength == 0 && context.Request.Files.Count == 1)
                throw new Exception();
            else if (hpf.ContentLength == 0)
                continue;
            // string FolderPath = ConfigurationManager.AppSettings["DocsFolderPath"];
            //string FolderPath = HttpContext.Current.Server.MapPath(@"..\Uploads\" + context.Request.Url.Segments[1].Replace("/", "") + "\\ContractDocs");

            string myfile = FileName;
            if (hpf.FileName != "blob")
            {
                myfile = DateTime.Now.ToString().Replace("/", "").Replace(".", "").Replace(":", "").Replace(" ", "") + FileName;
            }


            string extension = System.IO.Path.GetExtension(hpf.FileName); //myfile.Substring(myfile.LastIndexOf("."));
            string FolderPath = @"~/Uploads/" + context.Request.Url.Segments[1].Replace("/", "") + "/ContractDocs";
            
            //string savedFileName = FolderPath + "//" + myfile.Replace(" ", "");
            //     hpf.SaveAs(savedFileName);

            if (extension == ".pdf" && IsOCR == "Y")
            {
                FolderPath = @"~/Uploads/" + context.Request.Url.Segments[1].Replace("/", "") + "/ContractDocs/ScannedContractDocs";
                bool isExists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(FolderPath));
                if (!isExists)
                {
                    System.IO.Directory.CreateDirectory(context.Server.MapPath(FolderPath));
                }
            }

            using (
                //var fs = new System.IO.FileStream(System.IO.Path.Combine(FolderPath, myfile.Replace(" ", "")),
                //chunk == 0 ? System.IO.FileMode.Create : System.IO.FileMode.Append))
            
            var fs = new System.IO.FileStream(System.IO.Path.Combine(HttpContext.Current.Server.MapPath(FolderPath), myfile.Replace(" ", "")),
                chunk == 0 ? System.IO.FileMode.Create : System.IO.FileMode.Append))
            {
                var buffer = new byte[hpf.InputStream.Length];
                hpf.InputStream.Read(buffer, 0, buffer.Length);

                fs.Write(buffer, 0, buffer.Length);
            }

            if (extension == ".pdf" && IsOCR == "Y")
            {
                AsyncIndexer.GetInstance().ConvertOCRFile(myfile.Replace(" ", "").Replace(extension, ""), extension, "contractdocs");
            }
            
            string strUrl = "";
            string ContractInfo = "";
            if (RequestID != "" || RequestID != "0")
            {
                IActivity objActivity;
                objActivity = FactoryWorkflow.GetActivityDetails();
                objActivity.RequestId = Convert.ToInt32(RequestID);
                ContractInfo = objActivity.ContractDetailsByRequestId();
            }
            else
            {
                ContractInfo = UserID;
            }


            strUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + HttpContext.Current.Request.Url.Segments[1] + "Uploads/" + HttpContext.Current.Request.Url.Segments[1].Replace("/", "") + "/ContractDocs/" + myfile.Replace(" ", "");
            AsyncIndexer.GetInstance().QueueForIndexing(strUrl, 1, ContractInfo);

            ObjDocumentType.RequestId = 1;
            ObjDocumentType.DocumentTypeId = Convert.ToInt32(DocumentTypeId);
            ObjDocumentType.FilePath = myfile.Replace(" ", "");
            ObjDocumentType.OrigionalFileName = FileName;
            ObjDocumentType.Addedby = Convert.ToInt32(UserID);
            ObjDocumentType.RequestId = Convert.ToInt32(RequestID); //Bharati 24062014 6.05PM
            string ID = ObjDocumentType.InsertDocumentTypeRecord();
            string[] DocumentArray = ID.Split('#');
            //if (chunk == 0)
            //{
            //   // context.Response.Write("Chunk" + "#" + ID);
            //}
            //else
            //{
            context.Response.Write("Chunk#" + DocumentArray[0] + "#../Uploads/" + context.Request.Url.Segments[1].Replace("/", "") + "/ContractDocs/" + myfile.Replace(" ", "") + '#' + DocumentArray[1] + '#' + myfile.Replace(" ", ""));
            //}
        }
        /***********File Uploading Part Ends Here********/
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}