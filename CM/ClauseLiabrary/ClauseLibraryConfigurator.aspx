﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ClauseLibraryConfigurator.aspx.cs" Inherits="ClauseLiabrary_ClauseLibraryConfigurator" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
 <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
 <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
 <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
 <link rel="stylesheet" href="http://code.jquery.com/resources/demos/style.css">
 <style type="text/css">

       body 
       {
        font-family:Calibri, Verdana;
        font-size:18px;
       }
        table 
        {
        font-family:Calibri, Verdana;
        font-size:18px;
        }
   
     
        
 </style>
   <script language="javascript" type="text/javascript">
<!--
       /****************************************************
       Author: Nilesh Agnihotri
       ****************************************************/
       var win = null;

       function SetContractTypeValue(obj) 
       {
           $('#hdnContractTypeID').val($(obj).val())

       }

       function SetValueDropDown(obj) {
           $('#hdnContractTypeID').val($(obj).val())


       }

       function setValue(name) {

           var txt = $.trim(name);
           var box = $("#txtClauseText");
           box.val(box.val() + '##' + txt + "##"); ;

         //  $('#txtClauseText').append($('#txtClauseText').val()+'##' + name + "##");

       }

       function NewWindow(mypage, myname, w, h, scroll, pos) {
           if (pos == "random") { LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100; TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100; }
           if (pos == "center") { LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100; TopPosition = (screen.height) ? (screen.height - h) / 2 : 100; }
           else if ((pos != "center" && pos != "random") || pos == null) { LeftPosition = 0; TopPosition = 20 }
           settings = 'width=' + w + ',height=' + h + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=' + scroll + ',location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no';
           win = window.open(mypage, myname, settings);
       }
// -->




       function validate() {

           var isValidS = true;
           var $group = $('#UpdatePanel1');
           $('.requiredtext').html('');
           $group.find('.required').each(function (i, item) {

               if ($(this).val() == "") {
                   $(this).after(' <font color="red" class="requiredtext">This field is Required.</font>');
                   isValidS = false;
               }

           });
           var isValidType = ShowDialouge();
           if (isValidS == false || isValidType == false) {
               return false;
           }
           else {
               return true;
           }


       }


       function ShowDialouge() {
           var isvalid = false;
           if ($('#ddlContractType').val() == "0") {
               alert('Select Field Type ID');
               isvalid = false;
           }
           else {
               isvalid = true;
           }

           return isvalid;
       }



       function CheckRequired(obj) {

           $(obj).next('font').html('');

           if ($(obj).val() != "") {
               $(obj).css('border-color', 'black');
               $(obj).next('font').html('');
           }
           else {
               $(obj).css('border-color', 'red');
               $(obj).after(' <font color="red" class="requiredtext">This field is Required.</font>');
           }
           return false;
       }

</script>


  
</head>
<body bgcolor="#FFFFFF" text="#000000">
<b>Clause Library</b><br>
    <form id="form1" runat="server">
    <input type="hidden" runat="Server" id="hdnContractTypeID"></input>
     <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
   <center> <table width="50%" id="tblFIeldADD">
    <tr>
    <td  style=" font-family:Calibri, Verdana; font-size:18px;">
<b> Cluase Name </b>
    </td>
    <td>
   
    </td>
    </tr>
    <tr>
    <td>
    <input id="txtClauseName"  class="required" onkeyup="CheckRequired(this);" type="text" 
   runat="server" clientidmode="Static"  />
    </td>
    <td>
    </td>
    </tr>

     <tr>
    <td style=" font-family:Calibri, Verdana; font-size:18px;">
     <b>Contract type  </b>
    </td>
    <td>
    </td>
    </tr>
    <tr>
    <td>
     <select id="ddlContractType" runat="server" onchange="SetContractTypeValue(this)" >
    </select>
    </td>
    <td>
    
    </td>
    </tr>

    <tr>
    <td style=" font-family:Calibri, Verdana; font-size:18px;">
     <b>Version </b>
    </td>
    <td>
  
    </td>
    </tr>
    <tr>
    <td>
     <input id="txtVersion" class="required" onkeyup="CheckRequired(this);"  type="text" 
   runat="server" clientidmode="Static"  />
    </td>
    <td>
    </td>
    </tr>


    <tr style="display:none">
    <td valign=middle"  style=" font-family:Calibri, Verdana; font-size:18px;">
    <b> Text  </b>
    </td>
    <td>
    </td>
    </tr>
    <tr style="display:none">
    <td>
     <center> <a href="AddClauseFields.aspx" onclick="NewWindow(this.href,'mywin','1000','500','no','center');return false" onfocus="this.blur()">Add Field</a></center>
   
   <textarea id="txtClauseText" cols=100 rows=20
   runat="server" clientidmode="Static"   />
    </td>
    <td>
    </td>
    </tr>
   </table></center>


   <center> <asp:Button ID="btnSaveCLuase" Visible="true" runat="server" 
   Text="Save Clause" onclick="btnSaveCustomFieldValues_Click" OnClientClick="return validate();" /></center>

  
    </ContentTemplate>
    </asp:UpdatePanel>

    </form>
</body>
</html>

  <script type="text/javascript">
      $("#sidebar").html($("#rightlinks").html());
      $("#main").addClass("nosidebar");
      $("#toggleimage").hide();

        </script>
