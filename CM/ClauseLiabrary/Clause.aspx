﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="Clause.aspx.cs" Inherits="ClauseLiabrary_Clause" %>

<%--<%@ Register Src="~/UserControl/masterrightlinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>--%>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/setuplinks.ascx" TagName="Setuplinks" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <%-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/resources/demos/style.css">--%>
    <link href="../JQueryValidations/dhtmlwindow.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/dhtmlwindow.js" type="text/javascript"></script>
    <link href="../JQueryValidations/modal.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/modal.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $("#setupLink").addClass("menulink");
        $("#setuptab").addClass("selectedtab");
        // var ClauseVariables = new Array();
        function ClosePopup() {
            $.unblockUI();
            return false;
        }
        var MyArray = new Array();


        function GiveCall(obj) {

            alert(obj);
            return false;
        }
        function validate() {

            var TextC = $('#txtClauseText').val();
            var FinalText = $('#txtClauseText').val();
           

            for (var i = 0; i <= 10000; i++) {
                if (MyArray[i] != undefined) {
                    FinalText = FinalText.replace(MyArray[i], "##" + MyArray[i].replace("#", "").replace("#", "").replace("#", "").replace("#", "") + ":" + i + "##");
                }
            }
            function msgSaveSuccess() {
                alert('Clause Library Updated');
            }

            $('#hdnClauseMain').val(FinalText);
        }

        function isClauseNameexist() 
        {
            alert('Clause name already exist');

        }

        function InsertCodeInTextArea(textValue, FieldID) {
            MyArray[FieldID] = textValue;

            var txtArea = document.getElementById("txtClauseText");
            var txtAreaHidden = document.getElementById("hdnClauseMain");

            //IE
            if (document.selection) {
                txtArea.focus();
                var sel = document.selection.createRange();
                sel.text = textValue;
                return;
            }
            //Firefox, chrome, mozilla
            else if (txtArea.selectionStart || txtArea.selectionStart == '0') {
                var startPos = txtArea.selectionStart;
                var endPos = txtArea.selectionEnd;
                var scrollTop = txtArea.scrollTop;
                // txtAreaHidden.value = txtAreaHidden.value.substring(0, startPos) + textValue + txtAreaHidden.value.substring(endPos, txtAreaHidden.value.length);
                txtArea.value = txtArea.value.substring(0, startPos) + textValue + txtArea.value.substring(endPos, txtArea.value.length);
                //  txtArea.value = txtAreaHidden.value.substring(0, startPos) + textValue + ":" + FieldID + txtAreaHidden.value.substring(endPos, txtAreaHidden.value.length);
                txtArea.focus();
                txtArea.selectionStart = startPos + textValue.length;
                txtArea.selectionEnd = startPos + textValue.length;
            }
            else {
                txtArea.value += textArea.value;
                txtArea.focus();
            }
        }



        function SelectClauseName(obj) {

            $('#hdnClauseId').val($(obj).val())
            $('a').attr('href', '');
            $('a').attr('href', "AddClauseFields.aspx?ClauseID=" + $('#hdnClauseId').val());
        }

        function setValue(name, ClauseId, FieldID) {
           
            var txt = $.trim(name);
            var box = $("#txtClauseText");
            InsertCodeInTextArea('##' + txt.replace(/\r?\n/g, '<br />') + ':' + FieldID + "##", FieldID);
            $('#hdnNewVersionAttributes').val($('#hdnNewVersionAttributes').val() + ',' + FieldID);
            $('#hdnClauseId').val(ClauseId);
            $(box).next('div').remove();
            $(box).css('border-color', '');
        }

        function NewWindow(mypage, myname, w, h, scroll, pos) {
            if (pos == "random") { LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100; TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100; }
            if (pos == "center") { LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100; TopPosition = (screen.height) ? (screen.height - h) / 2 : 100; }
            else if ((pos != "center" && pos != "random") || pos == null) { LeftPosition = 0; TopPosition = 20 }
            settings = 'width=' + w + ',height=' + h + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=' + scroll + ',location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no';
            win = window.open(mypage, myname, settings);
        }

        function SetValueHiddenTextArea() 
        {
            
            return false;
        }



//        var selectedText;
//        var OrigionalText;
//        $(document).ready(function () {
//            $("textarea").select(function () {

//                var textComponent = document.getElementById('txtClauseText');
//                OrigionalText = textComponent.value;
//                // IE version
//                if (document.selection != undefined) {
//                    textComponent.focus();
//                    var sel = document.selection.createRange();
//                    selectedText = sel.text;
//                }
//                // Mozilla version
//                else if (textComponent.selectionStart != undefined) {
//                    var startPos = textComponent.selectionStart;
//                    var endPos = textComponent.selectionEnd;
//                    selectedText = textComponent.value.substring(startPos, endPos)
//                }

//                var newStatus = selectedText.replace(/#([^ ]+)#/g, '<span class="atsign">@$1</span>');

//            });

//        });

        $(document).ready(function () {

//            $('textarea').on('keydown', function () {
//                if (selectedText.match(/##([^ ]+)##/g)) {
//                    alert('Selection contains some Fields used in clause.');
//                    $(this).val(OrigionalText);
//                    selectedText = "";
//                    return false;
//                }
//            });

        });


       
        

    </script>
    <uc2:Setuplinks ID="SetuplinksID" runat="server" />
    <h2>
        <label id="lblEdittext" runat="Server">
            Add new clause</label></h2>
    <input type="hidden" runat="Server" id="hdnClauseId" />
    <input type="hidden" runat="Server" id="hdnClauseMain" />
    <input type="hidden" runat="Server" id="hdnVal" />
    <input type="hidden" runat="Server" id="hdnPrimeId" />
    <input type="hidden" runat="Server" id="hdnVersionID" />
    <input type="hidden" runat="Server" id="hdnNewVersionAttributes" />
    <input type="hidden" runat="Server" id="hdnContractTypeID" clientidmode="Static" />


    <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr>
            <td width="19%" align="right">
                <b>
                    <label for="time_entry_activity_id">
                       <span class="required">*</span>Contract type</label></b>
            </td>
            <td width="81%">
                <select id="ddlContractType" clientidmode="Static" runat="server" style="width: 31%"
                    class="chzn-select required chzn-select">
                    <option></option>
                </select>
            </td>
        </tr>
        <tr>
            <td width="19%" align="right">
                <b>
                    <label for="time_entry_issue_id">
                        <span class="required">*</span>Clause name</label></b>
            </td>
            <td width="81%">
                <input id="txtClause" type="text" size="40" class="ui-autocomplete-input required"
                    autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true"
                    name="text" runat="server" clientidmode="Static" maxlength="200" />
            </td>
        </tr>
        <tr>
            <td width="19%" align="right">
                <b>
                    <label for="time_entry_issue_id">
                        <span class="required">*</span>Version name</label></b>
            </td>
            <td width="81%">
                <input id="txtVersion" type="text" runat="server" clientidmode="Static" size="40"
                    class="ui-autocomplete-input required" autocomplete="off" role="textbox" aria-autocomplete="list"
                    aria-haspopup="true" name="text2" maxlength="200" />
            </td>
        </tr>
        <tr runat="server" id="description">
            <td width="19%" align="right" valign="top">
                <b>
                    <label for="time_entry_issue_id">
                        <span class="required"> *</span>Clause description</label></b>
            </td>
            <td width="50%">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr align="right">
                        <td>
                            <a href="#" onclick="opennewsletter(); return false">Add Fields</a>
                       
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="txtClauseText" onkeyup="return SetValueHiddenTextArea()" onfocus="window.lstText=this;"
                                cols="100" rows="20" runat="server" clientidmode="Static" class="ui-autocomplete-input required"
                                autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true"
                                name="text22" onclick="return txtClauseText_onclick()" />
                            <textarea id="txtClauseHidden" onkeyup="return SetValueHiddenTextArea()" onfocus="window.lstText=this;"
                                cols="100" rows="30" runat="server" clientidmode="Static" style="display: none" />
                            <input id="txtHiddenCluaseId" type="text" size="40" style="display: none" runat="server"
                                clientidmode="Static" maxlength="200" />
                        </td>
                          <td width="50%" valign="top">
                         <%--  Variable Edit starts here--%>
                         <table width="100%">
                  <asp:Repeater ID="rptClauseDetails" runat="server">
                    <HeaderTemplate>
                        <table id="masterDataTable" class="masterTable list issues" width="150%">
                            <thead>
                                <tr>                                  
                                    <th width="0%" style="display: none;">
                                   
                                    </th>
                                    <th width="100%">
                                        Existing Clause Fields
                                    </th>
                                    <th >
                               
                                    </th>
                                    <th>
                                  
                                    </th>

                                   <%-- <th width="15%">
                                       Version ID  
                                    </th>--%>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>                       
                            <td style="display: none">
                                <asp:Label ID="lblClauseFieldId" style="display:none" runat="server" ClientIDMode="Static" Text='<%#Eval("FieldID") %>'></asp:Label>
                            </td>
                            <td style="display: none">
                                <asp:Label ID="lblContractTypeId" style="display:none" runat="server" ClientIDMode="Static" Text='<%#Eval("ContractTypeId") %>'></asp:Label>
                            </td>
                            <td>
                                 <asp:Label ID="lblContractName" runat="server" ClientIDMode="Static" Text='<%#Eval("FieldName")%>'></asp:Label>
                            </td>

                            <td >

                                  <asp:LinkButton ID="imgDelete" CommandName='<%#Eval("FieldName")%>'  CommandArgument='<%#Eval("FieldID") %>'  runat="server" OnClientClick="return setSelectedId(this);" OnClick="imgDelete_Click"
                                   ><img title="Delete" src="../Images/delete.png"></img></asp:LinkButton>
                            
                            </td>

                            <td >

                               <asp:LinkButton ID="imgEdit" runat="server" OnClientClick="EditField(this); return false"
                               ><img title="Edit" src="../Images/edit.png"></img></asp:LinkButton>

                            </td>

                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </table>


                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <asp:Button ID="btnSaveCLause" Visible="true" runat="server" Text="Save Clause" OnClick="btnSave_Click"
        class="btn_validate" OnClientClick="return validate();" />
    <asp:Button ID="btnSaveCLuase" Visible="true" runat="server" Text="Save Clause" OnClick="btnSaveCustomFieldValues_Click"
        class="btn_validate" OnClientClick="return validate();" />
    <asp:Button ID="btnSaveAsNewVersion" Visible="true" style="display:none" runat="server" Text="Save as new version"
        class="btn_validate" OnClick="btnSaveAsNewVersion_Click" OnClientClick="return validate();" />
    <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back_Click" />
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript" language="javascript">
        $("#sidebar").html($("#rightlinks").html());

        function setSelectedId(obj) {
            if (confirm('Do you want to really delete this caluse variable ?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function AddField() {

            return true;
        }

        function SetIsDeleted(obj)
        {alert(obj);}

        function handleFileUpload() {
            // $.blockUI({ message: $('#uploader'), css: { width: '600px'} });
            popupshow();
            return false;
        }


        $('.chzn-select').change(function () 
        {
           
            if ($(this).val() != '0') {
               
                $(this).next('div').next('.tooltip_outer').hide();
                $(this).next('div').css('border-color', '');
                $('#hdnContractTypeID').val($('#ddlContractType').val());

            }
            else {
                $(this).next('div').next('.tooltip_outer').show();
                $(this).next('div').css('border-color', '');
            }
        });


        function popupshow() {
            $('.FieldValueHeader').hide();
            $('.TextArea').hide();
            $('.TextBox').hide();
            $(document).find('.DateField').hide();
            $(document).find('.SingleSelect').hide();
            $(document).find('.SingleSelectMaster').hide();
            $(".chzn-container").css("width", "200px");

        }

        function CheckRequired(obj) {

            $(obj).next('font').html('');

            if ($(obj).val() != "") {
                $(obj).css('border-color', 'black');
                $(obj).next('font').html('');
            }
            else {
                $(obj).css('border-color', 'red');
                $(obj).after(' <font color="red" class="requiredtext">This field is Required.</font>');
            }
            return false;
        }

        function opennewsletter() {
            var contractTypeId = $('#ddlContractType').val();

            emailwindow = dhtmlmodal.open('FieldDetails', 'iframe', '../ClauseLiabrary/AddClauseFields.aspx?ClauseId=' + $('#txtHiddenCluaseId').val() + "&ContractTypeId=" + contractTypeId, '', 'width=1100px,scrollbars=no,height=550px,center=1,resize=0"');
            emailwindow.onclose = function () { //Define custom code to run when window is closed
               
                var theform = this.contentDoc.forms[0] //Access first form inside iframe just for your reference

                return true;
            }
        } 



        function EditField(obj) 
        {
            var FieldID = $(obj).closest('tr').find('td:eq(0)').find('span').text();
            var ContractTypeId = $(obj).closest('tr').find('td:eq(1)').find('span').text();
            emailwindow = dhtmlmodal.open('FieldDetails', 'iframe', '../ClauseLiabrary/EditClauseFields.aspx?ClauseFIledID=' + FieldID + '&ContractTypeID=' + ContractTypeId, '', 'width=1100px,scrollbars=no,height=550px,center=1,resize=0"');
            emailwindow.onclose = function () 
            { //Define custom code to run when window is closed
                var theform = this.contentDoc.forms[0] //Access first form inside iframe just for your reference
                return true;
            }
        } 


        function SetValueDropDown(obj) {

            $('#hdnClauseId').val(obj)

        }

       
    </script>

      <script type="text/javascript">
          $("#sidebar").html($("#rightlinks").html());
         // $("#main").addClass("nosidebar");
        // $("#toggleimage").hide();

          function txtClauseText_onclick() {

          }

      </script>

</asp:Content>
