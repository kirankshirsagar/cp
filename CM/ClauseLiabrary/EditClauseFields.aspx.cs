﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using CommonBLL;
using ClauseLibraryBLL;
using System.Web.UI.WebControls;

public partial class ClauseLiabrary_EditClauseFields : System.Web.UI.Page
{
    IClauseLibrary ObjFieldDataType;
    IClauseFields ClauseFields;
    int ClauseFieldID;
    int ContractTypeID;

    protected void Page_Load(object sender, EventArgs e)
    {
        ClauseFieldID = Convert.ToInt32(Request.QueryString["ClauseFIledID"]);
        ContractTypeID = Convert.ToInt32(Request.QueryString["ContractTypeID"]);
        hdnContractTypeId.Value = ContractTypeID.ToString();
        hdnClauseId.Value = ClauseFieldID.ToString();

        CreateObjects();
        if (!IsPostBack)
        {
       
            drpDropDownList.extDataBind(ObjFieldDataType.SelectFieldDataType());
            drpSingleSelectMaster.extDataBind(ObjFieldDataType.SelectMasterTables());
            drpDropDownList.Attributes.Add("onchange", "ChangeType(this)");
            drpSingleSelectMaster.Attributes.Add("onchange", "ChangeMasterType(this)");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "tytytt", "SetValueDropDown('" + drpDropDownList.SelectedValue + "');", true);
            ReadCLauseFieldDetails();
        }



    }

    protected void ReadCLauseFieldDetails()
    {

        ClauseFields.ClauseFieldID = ClauseFieldID;
        ClauseFields.ReadClauseFieldDetailsandValues();
        List<ClauseFields> lstClauseField = new List<ClauseFields>();
        List<ClauseFields> lstClauseDetails = new List<ClauseFields>();
        List<ClauseFields> lstClauseValues = new List<ClauseFields>();
        //Get list of clause field details
        lstClauseField = ClauseFields.ReadClauseField;
        lstClauseDetails=ClauseFields.ReadClauseFieldDetails;
        int FieldTypeId = lstClauseField[0].FieldTypeID;
        string FieldName = lstClauseField[0].FieldName;
        string Question = lstClauseField[0].Question;

        txtSequence.Text = Convert.ToString(lstClauseField[0].FieldSequence);

        bool isRevise = lstClauseField[0].isRevise == "Y" ? chkAVailableFor.Items[0].Selected = true : chkAVailableFor.Items[0].Selected = false;
        bool IsRenew = lstClauseField[0].isRenew == "Y" ? chkAVailableFor.Items[1].Selected = true : chkAVailableFor.Items[1].Selected = false;
        bool IsTerminate = lstClauseField[0].isTerminate == "Y" ? chkAVailableFor.Items[2].Selected = true : chkAVailableFor.Items[2].Selected = false;

        txtCustomFieldValue.Text = FieldName;
        txtQuestionText.Text = Question;
        txtTextAreaSize.Text = Convert.ToString(lstClauseDetails[0].DefaultSize);
        drpDropDownList.SelectedValue = FieldTypeId.ToString();

        hdnQuestionTextTemp.Value=Question;
        hdnDefaultSizeTemp.Value=Convert.ToString(lstClauseDetails[0].DefaultSize);
        hdnFieldTypeTemp.Value=lstClauseField[0].FieldTypeID.ToString();
        hdnIsReviseTemp.Value = lstClauseField[0].isRevise == "Y" ? "true" :"false";
        hdnIsRenewTemp.Value = lstClauseField[0].isRenew == "Y" ? "true" :"false";
        hdnIsTerminateTemp.Value = lstClauseField[0].isTerminate == "Y" ? "true" :"false";


        if (FieldTypeId == 9 || FieldTypeId == 11)
        {
            hdnMasterID.Value = lstClauseDetails[0].ClauseFieldValue.ToString();
            drpSingleSelectMaster.SelectedValue = hdnMasterID.Value;
            
        }
        if (FieldTypeId == 8 || FieldTypeId == 10 || FieldTypeId == 12 || FieldTypeId == 14 || FieldTypeId == 16 )
        {
            hdnItems.Value = lstClauseDetails[0].ClauseFieldValue.ToString();      
        }


  
        //Set UI
        ScriptManager.RegisterStartupScript(this, this.GetType(), "tytytt", "ChangeType($(drpDropDownList));", true);
       // ScriptManager.RegisterStartupScript(this, this.GetType(), "tytyttee", "SetBasicValues('" + FieldTypeId + "','" + FieldName + "','" + Question + "');", true);

        //Set Details
       // ScriptManager.RegisterStartupScript(this, this.GetType(), "tytytt", "SetAllDetails('" + FieldTypeId + "');", true);


    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            ObjFieldDataType = FactoryClause.GetFieldDataTypesDetails();
            ClauseFields = FactoryClause.GetFieldReferenceDetails();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void btnAddNewCustomField_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        InsertUpdate();
        Page.TraceWrite("Save Update button clicked event ends.");

    }

    void InsertUpdate()
    {
        Page.TraceWrite("Insert, Update starts.");

        ClauseFields.ClauseFieldID = ClauseFieldID;
        ClauseFields.FieldTypeID = Convert.ToInt32(hdnFieldTypeTemp.Value);
        ClauseFields.Question = Convert.ToString(hdnQuestionTextTemp.Value);
        ClauseFields.isRequired = "Y";

        ClauseFields.FieldSequence = Convert.ToInt32(txtSequence.Text);

        if (hdnDefaultSizeTemp.Value == "0")
        {
            hdnDefaultSizeTemp.Value = "50";
        }
        ClauseFields.DefaultSize = Convert.ToInt32(hdnDefaultSizeTemp.Value);
        InsertPickListObjects(ClauseFields.FieldTypeID.ToString());
        ClauseFields.ClauseFieldValue = Convert.ToString(hdnSignleSelectValues.Value);
        ClauseFields.FieldAttachedClasues = hdnSignleSelectValuesWithClauses.Value;
        ClauseFields.isRevise = hdnIsReviseTemp.Value == "checked" ? "Y" : "N";
        ClauseFields.isRenew = hdnIsRenewTemp.Value == "checked" ? "Y" : "N";
        ClauseFields.isTerminate = hdnIsTerminateTemp.Value == "checked" ? "Y" : "N";
        ClauseFields.Addedby = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        ClauseFields.Modifiedby = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        ClauseFields.IP = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
        ClauseFields.ValidationMessage = "";
        try
        {
            string FieldID = ClauseFields.UpdateFieldRecord();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

        Page.TraceWrite("Insert, Update ends.");


    }




    protected void InsertPickListObjects(string isMaster)
    {
        if (isMaster == "9" || isMaster == "11" || isMaster == "13" || isMaster == "15")
        {
            hdnSignleSelectValues.Value = drpSingleSelectMaster.SelectedValue;
        }
        else if (isMaster == "10")
        {
            hdnSignleSelectValuesWithClauses.Value = "";
        }
    }
}