﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using CommonBLL;
using ClauseLibraryBLL;
using System.Web.UI.WebControls;

public partial class ClauseLiabrary_AddClause : System.Web.UI.Page
{
    IClauseLibrary Obj;

    protected void Page_Load(object sender, EventArgs e)
    {
       
        CreateObjects();
        if (!IsPostBack)
        {
        
            drpClauseName.extDataBind(Obj.SelectClauseName());
            drpClauseName.Attributes.Add("onchange", "SelectClauseName(this)");
            if (String.IsNullOrEmpty(Request.extValue("hdnPrimeIds")) != true)
            {
                drpClauseName.Attributes.Add("disabled", "disabled");
          
                hdnPrimeId.Value = Request.extValue("hdnPrimeIds");
                drpClauseName.Value = hdnPrimeId.Value;
              
                EditClause();
          

            }
            else 
            {
                AddClause();
            }
         
          
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "tytytt", "SetValueDropDown('" + drpClauseName.Value + "');", true);
    }

    protected void EditClause()
    {
        Obj.CluaseID = Convert.ToInt32(hdnPrimeId.Value);
        List<ClauseLibrary> lstClauseData = new List<ClauseLibrary>();
        lstClauseData = Obj.ReadClauseDataData();
        drpClauseName.extSelectedValues(Convert.ToString(lstClauseData[0].CluaseID));
        txtVersion.Value = lstClauseData[0].VersionName;
        hdnVersionID.Value = lstClauseData[0].VersionID;
        txtClauseText.Value = lstClauseData[0].Language.Replace("<br>","\r\n");
        txtClause.Value = drpClauseName.Items.FindByValue(Convert.ToString(lstClauseData[0].CluaseID)).Text;
        lblContractType.InnerText = lstClauseData[0].ContractName;
        hdnContractTypeID.Value = Convert.ToString(lstClauseData[0].ContractTypeId);
    }

    protected void AddClause()
    { 
    
            
    
    }


    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            Obj = FactoryClause.GetContractTypesDetails();

            //ClauseFields = FactoryClause.GetFieldReference();

        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void btnSaveCustomFieldValues_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
         InsertUpdate();
        Page.TraceWrite("Save Update button clicked event ends.");

    }


    protected void btnSaveAsNewVersion_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
       
        InsertUpdateAsNewVersion();
 
        Page.TraceWrite("Save Update button clicked event ends.");

    }


    void InsertUpdateAsNewVersion()
    { 
        //Save As New Version Code Starts Here.
        Page.TraceWrite("Insert, Update for New Version Starts Here.");
        string status = "";
        try
        {

            Obj.CluaseID = Convert.ToInt32(hdnPrimeId.Value);
           
            Obj.ClauseName = txtClause.Value;
            Obj.ParentID = Obj.CluaseID;
            Obj.VersionName = txtVersion.Value;
            Obj.Language = Convert.ToString(txtClauseText.Value.Replace("\r\n", "<br>"));
         
            txtClauseText.Attributes.Add("MyText", hdnClauseMain.Value.Replace("\r\n", "<br>"));
            Obj.Addedby = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            Obj.Modifiedby = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            Obj.IP = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
            Obj.isActive = true;
            Obj.ContractTypeId =Convert.ToInt32( hdnContractTypeID.Value);
            string ClauseIDNew=Obj.InsertRecord();
            Obj.CluaseID = Convert.ToInt32(ClauseIDNew);
            Obj.UpdateClauseLanguage();
            Page.Message("1", "0");   
        }
        catch (Exception ex)
        {
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
     
        Page.TraceWrite("Insert, Update for New Version Ends Here.");
        Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
        Response.Redirect("EditClause.aspx");
        ClearData(status);
      }

    
    




       
    void InsertUpdate()
    {
        Page.TraceWrite("Insert, Update starts.");
        string status = "1";
        try
        {
            Obj.CluaseID = Convert.ToInt32(hdnPrimeId.Value);

            Obj.Language = Convert.ToString(txtClauseText.Value.Replace("\r\n", "<br>"));
            txtClauseText.Attributes.Add("MyText", hdnClauseMain.Value.Replace("\r\n", "<br>"));
            Obj.VersionName = txtVersion.Value;
            Obj.ClauseName = txtClause.Value;
            Obj.Addedby = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            Obj.Modifiedby = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            Obj.IP = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
            Obj.UpdateClauseLanguage();
            Page.Message("1", "0");     
 
        }
        catch (Exception ex)
        {
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
            Page.TraceWrite("Insert, Update ends.");
    
            Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
            Response.Redirect("EditClause.aspx");
            ClearData(status);
    }
    void ClearData(string status)
    {
        if (status == "1")
        {
            Page.TraceWrite("clearData starts.");
          
            hdnPrimeId.Value = "0";
        
            Page.TraceWrite("clear Data ends.");
        }
    }




}