﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;
using ClauseLibraryBLL;
 using System.Data;

public partial class ClauseLiabrary_EditClause : System.Web.UI.Page
{
    // navigation//
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 10;
    // navigation//
    IClauseLibrary Obj;

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
     
        if (!IsPostBack )
        {
          
            ReadData(1, RecordsPerPage);
            Message();

        }
        else
        {
            SetNavigationButtonParameters();
        }

        AccessVisibility();

      
    }

    protected void imgEdit_Click(object sender, EventArgs e)
    {
     
            Server.Transfer("AddClause.aspx");
    }

    // navigation//


    void SetNavigationButtonParameters()
    {
        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }


    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            Obj = FactoryClause.GetContractTypesDetails();

        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void btnSaveCustomFieldValues_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");

        Page.TraceWrite("Save Update button clicked event ends.");

    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
            AccessVisibility();
        }
    }




    void ReadData(int pageNo, int recordsPerPage)
    {
        if (true)
        {
            Page.TraceWrite("ReadData starts.");
            try
            {
                Obj.PageNo = pageNo;
                Obj.RecordsPerPage = recordsPerPage;
                Obj.Search = "";
                rptClauseDetails.DataSource =Obj.ReadData();
                rptClauseDetails.DataBind();
                Page.TraceWrite("ReadData ends.");
                ViewState[Declarations.TotalRecord] = Obj.TotalRecords;
                PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
                PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
                PaginationButtons1.RecordsPerPage = RecordsPerPage;
            }
            catch (Exception ex)
            {
                Page.TraceWarn("ReadData fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
    }



   
    void AccessVisibility()
    {

        Page.TraceWrite("AccessVisibility starts.");
        if (Access.View == "N")
        {
            Page.extDisableControls();
    
        }
        else
        {
    
        }

        if (Access.Update == "N")
        {
            foreach (RepeaterItem item in rptClauseDetails.Items)
            {
                ImageButton imgEdit = (ImageButton)item.FindControl("imgEdit");
                imgEdit.Enabled = false;
                imgEdit.Visible = false;
            }
      
        }
        if (Access.Add == "N")
        {
            Page.extDisableControls();
     
        }
        else
        {
    
        }

        if (Access.Delete == "N")
        {
     
        }

    
        Page.TraceWrite("AccessVisibility starts.");
    }


    void Message()
    {
        if (Session[Declarations.Message] != null)
        {
            var flg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }

    }






   
}