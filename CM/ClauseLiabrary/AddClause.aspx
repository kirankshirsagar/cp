﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddClause.aspx.cs" Inherits="ClauseLiabrary_AddClause" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add Clause</title>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/resources/demos/style.css">
    <link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Font.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        body
        {
            font-family: Calibri, Verdana;
            font-size: 18px;
        }
        table
        {
            font-family: Calibri, Verdana;
            font-size: 18px;
        }
    </style>
    <script type="text/javascript">
        var MyArray = new Array();


        function validate() {


            var TextC = $('#txtClauseText').val();
            var FinalText = $('#txtClauseText').val();
            for (var i = 0; i <= 10000; i++) {
                if (MyArray[i] != undefined) {
                    FinalText = FinalText.replace(MyArray[i], "##" + MyArray[i].replace("#", "").replace("#", "").replace("#", "").replace("#", "") + ":" + i + "##");
                }


            }
            function msgSaveSuccess() {
                alert('Clause Library Updated');
            }


            $('#hdnClauseMain').val(FinalText);
        }

        function InsertCodeInTextArea(textValue, FieldID) {
            MyArray[FieldID] = textValue;

            var txtArea = document.getElementById("txtClauseText");
            var txtAreaHidden = document.getElementById("hdnClauseMain");

            //IE
            if (document.selection) {
                txtArea.focus();
                var sel = document.selection.createRange();
                sel.text = textValue;
                return;
            }
            //Firefox, chrome, mozilla
            else if (txtArea.selectionStart || txtArea.selectionStart == '0') {
                var startPos = txtArea.selectionStart;
                var endPos = txtArea.selectionEnd;
                var scrollTop = txtArea.scrollTop;
                // txtAreaHidden.value = txtAreaHidden.value.substring(0, startPos) + textValue + txtAreaHidden.value.substring(endPos, txtAreaHidden.value.length);
                txtArea.value = txtArea.value.substring(0, startPos) + textValue + txtArea.value.substring(endPos, txtArea.value.length);
                //  txtArea.value = txtAreaHidden.value.substring(0, startPos) + textValue + ":" + FieldID + txtAreaHidden.value.substring(endPos, txtAreaHidden.value.length);

                txtArea.focus();
                txtArea.selectionStart = startPos + textValue.length;
                txtArea.selectionEnd = startPos + textValue.length;
            }
            else {
                txtArea.value += textArea.value;
                txtArea.focus();
            }


        }



        function SetValueDropDown(obj) {
            $('#hdnClauseId').val($(obj).val())
            $('a').attr('href', '');
            $('a').attr('href', "AddClauseFields.aspx?ClauseID=" + obj);

        }

        function SelectClauseName(obj) {

            $('#hdnClauseId').val($(obj).val())
            $('a').attr('href', '');
            $('a').attr('href', "AddClauseFields.aspx?ClauseID=" + $('#hdnClauseId').val());

        }

        function setValue(name, ClauseId, FieldID) {

            var txt = $.trim(name);
            var box = $("#txtClauseText");
            InsertCodeInTextArea('##' + txt.replace(/\r?\n/g, '<br />') + ':' + FieldID + "##", FieldID);
            $('#hdnNewVersionAttributes').val($('#hdnNewVersionAttributes').val() + ',' + FieldID);
            $('#hdnClauseId').val(ClauseId);
        }

        function NewWindow(mypage, myname, w, h, scroll, pos) {

            if (pos == "random") { LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100; TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100; }
            if (pos == "center") { LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100; TopPosition = (screen.height) ? (screen.height - h) / 2 : 100; }
            else if ((pos != "center" && pos != "random") || pos == null) { LeftPosition = 0; TopPosition = 20 }
            settings = 'width=' + w + ',height=' + h + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=' + scroll + ',location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no';
            win = window.open(mypage, myname, settings);
        }

        function SetValueHiddenTextArea() {

            //$('#txtCLauseHidden').val($('#txtClauseText').val());
        }
 
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <input type="hidden" runat="Server" id="hdnClauseId" />
    <input type="hidden" runat="Server" id="hdnClauseMain" />
    <input type="hidden" runat="Server" id="hdnVal" />
    <input type="hidden" runat="Server" id="hdnPrimeId" />
    <input type="hidden" runat="Server" id="hdnVersionID" />
    <input type="hidden" runat="Server" id="hdnNewVersionAttributes" />
    <input type="hidden" runat="Server" id="hdnContractTypeID" />
    <div>
        <div class="formMessage" style="display: none; width: 95%" id="MydivMessage" clientidmode="Static"
            runat="server">
        </div>
        <center>
            <table width="50%">
                <tr>
                    <td style="font-family: Calibri, Verdana; font-size: 18px;">
                        <b>Contract Type </b>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="font-family: Calibri, Verdana; font-size: 18px;">
                        <label id="lblContractType" runat="Server">
                        </label>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="font-family: Calibri, Verdana; font-size: 18px;">
                        <b>Cluase Name </b>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <select id="drpClauseName" runat="server" onchange="SelectCLauseName(this)" style="display: none">
                        </select>
                        <input id="txtClause" type="text" runat="server" clientidmode="Static" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="font-family: Calibri, Verdana; font-size: 18px;">
                        <b>Version </b>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="txtVersion" type="text" runat="server" clientidmode="Static" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" style="font-family: Calibri, Verdana; font-size: 18px;">
                        <b>Text </b>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <center>
                            <a href="AddClauseFields.aspx" onclick="NewWindow(this.href,'mywin','1000','500','no','left');return false"
                                onfocus="this.blur()">Add Field</a></center>
                        <textarea id="txtClauseText" onkeyup="SetValueHiddenTextArea()" onfocus="window.lstText=this;"
                            cols="100" rows="20" runat="server" clientidmode="Static" />
                        <textarea id="txtClauseHidden" onkeyup="SetValueHiddenTextArea()" onfocus="window.lstText=this;"
                            cols="100" rows="20" runat="server" clientidmode="Static" style="display: none" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </center>
        <center>
            <asp:Button ID="btnSaveCLuase" Visible="true" runat="server" Text="Save Clause" OnClick="btnSaveCustomFieldValues_Click"
                OnClientClick="return validate();" />
            &nbsp;&nbsp;
            <asp:Button ID="btnSaveAsNewVersion" Visible="true" runat="server" Text="Save as new version"
                OnClick="btnSaveAsNewVersion_Click" OnClientClick="return validate();" />
        </center>
    </div>
    </form>
</body>
</html>

  <script type="text/javascript">
      $("#sidebar").html($("#rightlinks").html());
      $("#main").addClass("nosidebar");
      $("#toggleimage").hide();

        </script>
