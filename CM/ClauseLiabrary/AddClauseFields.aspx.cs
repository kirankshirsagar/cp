﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using CommonBLL;
using ClauseLibraryBLL;
using System.Web.UI.WebControls;

public partial class ClauseLiabrary_AddClauseFields : System.Web.UI.Page
{
    IClauseLibrary ObjFieldDataType;
    IClauseFields ClauseFields;
    int ClauseID;
   
    protected void Page_Load(object sender, EventArgs e)
    {
        ClauseID = Convert.ToInt32(Request.QueryString["ClauseID"]);
        hdnClauseId.Value = ClauseID.ToString();
        hdnContractTypeId.Value = Request.QueryString["ContractTypeId"];
        CreateObjects();

        if (!IsPostBack)
        {
            ClauseFields.ClauseID = ClauseID;
            txtSequence.Text = (ClauseFields.GetMaxClauseFieldSequence() + 1).ToString();
  
            drpDropDownList.extDataBind(ObjFieldDataType.SelectFieldDataType());
            drpSingleSelectMaster.extDataBind(ObjFieldDataType.SelectMasterTables());
            drpDropDownList.Attributes.Add("onchange", "ChangeType(this)");
            drpSingleSelectMaster.Attributes.Add("onchange", "ChangeMasterType(this)");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "tytytt", "SetValueDropDown('" + drpDropDownList.SelectedValue + "');", true);
        }
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            ObjFieldDataType = FactoryClause.GetFieldDataTypesDetails();
            ClauseFields = FactoryClause.GetFieldReferenceDetails();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    protected void btnAddNewCustomField_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        InsertUpdate();
        Page.TraceWrite("Save Update button clicked event ends.");
    
    }

    void InsertUpdate()
    {
            Page.TraceWrite("Insert, Update starts.");
           
            ClauseFields.ClauseID = ClauseID;
            ClauseFields.FieldName = Convert.ToString(txtCustomFieldValue.Text);
            ClauseFields.FieldSequence = Convert.ToInt32(txtSequence.Text);
            ClauseFields.FieldTypeID = Convert.ToInt32(hdnFieldTypeID.Value);
            ClauseFields.Question = Convert.ToString(txtQuestionText.Text);
            ClauseFields.isRequired = "Y";
         
            if (txtTextAreaSize.Text == "")
            {
                txtTextAreaSize.Text = "1000";
            }
            ClauseFields.DefaultSize = Convert.ToInt32(txtTextAreaSize.Text);
            InsertPickListObjects(ClauseFields.FieldTypeID.ToString());
            ClauseFields.ClauseFieldValue = Convert.ToString(hdnSignleSelectValues.Value);
            ClauseFields.FieldAttachedClasues = hdnSignleSelectValuesWithClauses.Value;
            ClauseFields.isRevise = Convert.ToString(chkAVailableFor.Items[0].Selected) == Convert.ToString(true) ? "Y" : "N";
            ClauseFields.isRenew = Convert.ToString(chkAVailableFor.Items[1].Selected) == Convert.ToString(true) ? "Y" : "N";
            ClauseFields.isTerminate = Convert.ToString(chkAVailableFor.Items[2].Selected) == Convert.ToString(true) ? "Y" : "N";
            ClauseFields.Addedby = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            ClauseFields.Modifiedby = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            ClauseFields.IP = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
            ClauseFields.ValidationMessage = "";
            try
            {
                string FieldID=ClauseFields.InsertRecord();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tytytt", "CloseForm('" + ClauseFields.FieldName + "','" + ClauseFields.ClauseID + "','" + FieldID + "');", true);
            }
            catch (Exception ex)
            {
                Page.TraceWarn("Insert, Update fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

           Page.TraceWrite("Insert, Update ends.");
    }

    protected void InsertPickListObjects(string isMaster)
    {
        if (isMaster == "9" || isMaster == "11" || isMaster == "13" || isMaster == "15")
        {
            hdnSignleSelectValues.Value = drpSingleSelectMaster.SelectedValue;
        }
        else if (isMaster == "10")
        {
            hdnSignleSelectValuesWithClauses.Value = "";
        }
       
    }

}