﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;
using ClauseLibraryBLL;
 using System.Data;

public partial class ClauseLiabrary_ClauseData : System.Web.UI.Page
{
    // navigation//
    public static int RecordsPerPage = 50;
    public static int VisibleButtonNumbers = 10;
    // navigation//
    IClauseLibrary Obj;
    public string Add;
    public string View;
    public string Update;
    public string Delete;

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        if (Session[Declarations.User] != null && !IsPostBack)
        {
            Access.PageAccess(this, Session[Declarations.User].ToString(), "ClauseLiabrary_");
            ViewState[Declarations.Add] = Access.Add.Trim();
            ViewState[Declarations.View] = Access.View.Trim();
            ViewState[Declarations.Delete] = Access.Delete.Trim();
            ViewState[Declarations.Update] = Access.Update.Trim();
        }

        AccessVisibility();
        if (!IsPostBack || hdnSearch.Value.Trim() != string.Empty)
        {
            rdActive.Checked = true;
            ReadData(1, RecordsPerPage);
            Message();
        }
        else
        {
            SetNavigationButtonParameters();
        }

    }

    void Message()
    {
        if (Session[Declarations.Message] != null)
        {
            var flg = Session[Declarations.Message].ToString();
            Session[Declarations.Message] = null;
            Page.JavaScriptClientScriptBlock("msg", "PageGetMessage('" + flg + "')");
        }

    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            Obj = FactoryClause.GetContractTypesDetails();

        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

   

    // navigation//

    #region Navigation
    void SetNavigationButtonParameters()
    {
        PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
        PaginationButtons1.RecordsPerPage = RecordsPerPage;
        PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (PaginationButtons1.PageNumber > 0)
        {
            ReadData(PaginationButtons1.PageNumber, PaginationButtons1.RecordsPerPage);
            AccessVisibility();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
    }
    #endregion
    // navigation//

    protected void btnSaveCustomFieldValues_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");

        Page.TraceWrite("Save Update button clicked event ends.");

    }

    protected void imgEdit_Click(object sender, EventArgs e)
    {
        if (Update == "Y")
        {
            Server.Transfer("Clause.aspx");
        }
    }

    void ReadData(int pageNo, int recordsPerPage)
    {
        if (View == "Y")
        {
            if (true)
            {
                Page.TraceWrite("ReadData starts.");
                try
                {

                    Obj.PageNo = pageNo;
                    Obj.RecordsPerPage = recordsPerPage;
                    Obj.Search = txtSearch.Value.Trim();
                    Obj.UserID = Convert.ToInt32(Session[Declarations.User].ToString());
                    rptClauseDetails.DataSource = Obj.ReadData();
                    rptClauseDetails.DataBind();
                    Page.TraceWrite("ReadData ends.");
                    ViewState[Declarations.TotalRecord] = Obj.TotalRecords;
                    PaginationButtons1.VisibleButtonNumbers = VisibleButtonNumbers;
                    PaginationButtons1.TotalRecord = ViewState[Declarations.TotalRecord] != null ? (long)ViewState[Declarations.TotalRecord] : 0;
                    PaginationButtons1.RecordsPerPage = RecordsPerPage;
                }
                catch (Exception ex)
                {
                    Page.TraceWarn("ReadData fails.");
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
        }
    }

    protected void btnAddRecord_Click(object sender, EventArgs e)
    {
        Server.Transfer("Clause.aspx?CluaseID=0"); 
        //Server.Transfer("Clause.aspx");
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Delete starts.");
        string status = "";
        Obj.ClauseIds = hdnPrimeIds.Value;
        try
        {
            status = Obj.DeleteRecord();
        }
        catch (Exception ex)
        {
            hdnPrimeIds.Value = string.Empty;
            Page.JavaScriptClientScriptBlock("status", "PageGetMessage('DF')");
            Page.TraceWarn("Delete fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        hdnPrimeIds.Value = string.Empty;
        Page.JavaScriptClientScriptBlock("status", "PageGetMessage('DS')");
        Page.TraceWrite("Delete ends.");
        ReadData(1, RecordsPerPage);

    }

    protected void btnChangeStatus_Click(object sender, EventArgs e)
    {
    }


    private void setAccessValues()
    {
        if (ViewState[Declarations.Add] != null)
        {
            Add = ViewState[Declarations.Add].ToString();
        }
        if (ViewState[Declarations.Update] != null)
        {
            Update = ViewState[Declarations.Update].ToString();
        }
        if (ViewState[Declarations.Delete] != null)
        {
            Delete = ViewState[Declarations.Delete].ToString();
        }
        if (ViewState[Declarations.View] != null)
        {
            View = ViewState[Declarations.View].ToString();
        }

    }


    void AccessVisibility()
    {
        setAccessValues();
        Page.TraceWrite("AccessVisibility starts.");
        if (View == "N")
        {
            Page.extDisableControls();
            btnChangeStatus.Enabled = false;

        }
        else
        {
            btnSearch.Enabled = true;
            txtSearch.Disabled = false;
        }

        if (Update == "N")
        {
            foreach (RepeaterItem item in rptClauseDetails.Items)
            {
                LinkButton imgEdit = (LinkButton)item.FindControl("imgEdit");
                imgEdit.Enabled = false;
               
            }
            btnDelete.Enabled = false;
            btnChangeStatus.Enabled = false;
        }
        if (Add == "N")
        {
            Page.extDisableControls();
            btnChangeStatus.Enabled = false;
            btnAddRecord.Enabled = false;
        }
        else
        {
            btnAddRecord.Enabled = true;
        }

        if (Delete == "N")
        {
            btnDelete.Enabled = false;
            btnDelete.OnClientClick = null;
        }
        else if (Delete == "Y")
        {
            btnDelete.Enabled = true;
        }

        if (View == "Y")
        {
            txtSearch.Disabled = false;
            btnSearch.Enabled = true;
            btnShowAll.Enabled = true;
        }


        Page.TraceWrite("AccessVisibility starts.");
    }
  




   
}