﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditClauseFields.aspx.cs"
    Inherits="ClauseLiabrary_EditClauseFields" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add Clause Field</title>
    <%--    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>--%>
    <%--    <link rel="stylesheet" href="http://code.jquery.com/resources/demos/style.css" />--%>
    <script src="../scripts/jquery-1.7.2.min.js" type="text/javascript"></script>
    <link href="../Styles/chosen.css" rel="stylesheet" type="text/css" />
    <script src="../scripts/chosen.jquery.js" type="text/javascript"></script>
    <script src="../CommonScripts/dropdownlist.js" type="text/javascript"></script>
    <%-- <script src="../JQueryValidations/mastervalidations.js" type="text/javascript"></script>--%>
    <script src="../Scripts/application.js" type="text/javascript"></script>
    <script src="../JQueryValidations/mastervalidations.js" type="text/javascript"></script>
    <link href="../Styles/application.css" media="all" rel="stylesheet" type="text/css" />
    <script src="../CommonScripts/callHandler.js" type="text/javascript"></script>
    <script type="text/javascript">

        var GlobalClauses = '';

        function ClosePopup() {
            $.unblockUI();
            return false;
        }
        function CloseForm(obj, ClauseId, FieldID) {
            parent.setValue(obj, ClauseId, FieldID);
        }


        function ShowDialouge() {

            var isvalid = false;
            if ($('#drpDropDownList').val() == "0") {
                alert('Select Field Type ID');
                isvalid = false;
            }
            else {
                isvalid = true;
            }

            return isvalid;
        }



        function ChangeMasterType(obj) {
            $('#hdnSingleSelectMaster').val($(obj).val());

        }
        function SetValueDropDown(obj) {
            $('#hdnFieldTypeID').val("1")


        }

        function SetBasicValues(FieldTypeId, FieldName, Question) {
            alert(FieldName + '--' + txtQuestionText);



        }

        function SetAllDetails(FIeldTypeId) {

            $('#mainTable').find('table[id^="tblData"] tbody tr:not(:first)').remove();

            hideShowLabel(0);
            $('#hdnFieldTypeID').val(FIeldTypeId);
            $('.FieldValueHeader').show();
            $("#txtRemoveOption").val('');



            if (FIeldTypeId == "1" || FIeldTypeId == "2" || FIeldTypeId == "3" || FIeldTypeId == "4" || FIeldTypeId == "5" || FIeldTypeId == "6" || FIeldTypeId == "7") {
                if (FIeldTypeId == "7") {
                    $('.TextArea').hide();
                    $('.FieldValueHeader').hide();
                }
                else {
                    $('.TextArea').show();
                }
                $('.TextBox').show();
                $('.DateField').hide();
                $('.SingleSelect').hide();
                $('.SingleSelectMaster').hide();

            }
            else if (FIeldTypeId == "8" || FIeldTypeId == "10" || FIeldTypeId == "12" || FIeldTypeId == "14" || FIeldTypeId == "16") {
                $('.TextArea').hide();
                $('.TextBox').hide();
                $('.DateField').hide();
                $('.SingleSelect').show();
                $('.SingleSelectMaster').hide();

            }
            else if (FIeldTypeId == "9" || FIeldTypeId == "11" || FIeldTypeId == "13" || FIeldTypeId == "15") {
                $('.TextArea').hide();
                $('.TextBox').hide();
                $('.DateField').hide();
                $('.SingleSelect').hide();
                $('.SingleSelectMaster').show();
            }
        }

        var PgLoadCnt = 0;
        function ChangeType(obj) {
            $('#mainTable').find('table[id^="tblData"] tbody tr:not(:first)').remove();
            hideShowLabel(0);
            $('#hdnFieldTypeID').val($(obj).val());
            $('.FieldValueHeader').show();
            $("#txtRemoveOption").val('');

            /*Added By Prashant for defect No 19228*/
            if (PgLoadCnt == 0) {
                switch ($(obj).val()) {
                    case "1":
                    case "6":
                        $("#txtTextAreaSize").attr({
                            maxlength: "4"
                        });
                        break;

                    case "4":
                    case "5":
                        $("#txtTextAreaSize").attr({
                            maxlength: "2"
                        });
                        break;
                    default:
                        $("#txtTextAreaSize").attr({
                            maxlength: "5"
                        });
                }
            }
            else {
                switch ($(obj).val()) {
                    case "1":
                    case "6":
                        $("#txtTextAreaSize").attr({
                            value: "8000",
                            maxlength: "4"
                        });
                        break;

                    case "4":
                    case "5":
                        $("#txtTextAreaSize").attr({
                            value: "18",
                            maxlength: "2"
                        });
                        break;
                    default:
                        $("#txtTextAreaSize").attr({
                            value: "50",
                            maxlength: "5"
                        });
                }
            }


            if ($(obj).val() == "0") {
                $('.TextArea').hide();
                $('.FieldValueHeader').hide();
            }
            /*Added By Prashant for defect No 19228*/

            if ($(obj).val() == "1" || $(obj).val() == "2" || $(obj).val() == "3" || $(obj).val() == "4" || $(obj).val() == "5" || $(obj).val() == "6" || $(obj).val() == "7") {
                if ($(obj).val() == "7") {
                    $('.TextArea').hide();
                    $('.FieldValueHeader').hide();
                }
                else {
                    $('.TextArea').show();
                }
                $('.TextBox').show();
                $('.DateField').hide();
                $('.SingleSelect').hide();
                $('.SingleSelectMaster').hide();

            }
            else if ($(obj).val() == "8" || $(obj).val() == "10" || $(obj).val() == "12" || $(obj).val() == "14" || $(obj).val() == "16") {
                $('.TextArea').hide();
                $('.TextBox').hide();
                $('.DateField').hide();
                $('.SingleSelect').show();
                $('.SingleSelectMaster').hide();
            }
            else if ($(obj).val() == "9" || $(obj).val() == "11" || $(obj).val() == "13" || $(obj).val() == "15") {
                $('.TextArea').hide();
                $('.TextBox').hide();
                $('.DateField').hide();
                $('.SingleSelect').hide();
                $('.SingleSelectMaster').show();
            }
        }

        function validate() {
            var isValidS = true;
            var $group = $('#tblFIeldADD');
            $('.requiredtext').html('');
            $group.find('.required').each(function (i, item) {

                if ($(this).val() == "") {
                    $(this).after(' <font color="red" class="requiredtext">This field is Required.</font>');
                    isValidS = false;
                }
            });
            var isValidType = ShowDialouge();
            if (isValidS == false || isValidType == false) {
                return false;
            }
            else {
                return true;
            }
        }

        function CheckRequired(obj) {
            $(obj).next('font').html('');

            if ($(obj).val() != "") {
                $(obj).css('border-color', 'blue');
                $(obj).next('font').html('');
            }
            else {
                $(obj).css('border-color', 'red');
                $(obj).after(' <font color="red" class="requiredtext">This field is Required.</font>');
            }
            return false;
        }

        $(document).ready(function () {
            $('.TextArea').hide();
            $('.TextBox').hide();
            $('.FieldValueHeader').hide();
            $(document).find('.DateField').hide();
            $(document).find('.SingleSelect').hide();
            $(document).find('.SingleSelectMaster').hide();
            ChangeType('#drpDropDownList');
            var SelectedValues = new Array();
            var SelectedValues = $('#hdnItems').val().split(',');

            for (i = 0; i <= SelectedValues.length; i++) {

                var ind = $('#drpDropDownList').val();
                var rowData;

                rowData = "<tr>"
                + "<td width='25%'><input class='clsValue required' maxlength='50' type='text' value='" + SelectedValues[i] + "' /></td>"
                + "<td width='65%'> <select style='width:95%' class='clsClause'></select> </td>"
                + "<td width='5%'><a href='#' class='btnDelete' clientidmode='static'>Delete</a></td>"
                + "<td width='10%'>&nbsp;<a href='#'class='up'>Up</a>&nbsp;&nbsp;<a href='#' class='down'>Down</a></td>"
                "</tr>";

                if (ind == 10 || ind == 8) {
                    rowData = "<tr>"
                + "<td width='25%'><input class='clsValue required' maxlength='50' type='text' value='" + SelectedValues[i] + "' /></td>"
                + "<td width='5%'><a href='#' class='btnDelete' clientidmode='static'>Delete</a></select> </td>"
                + "<td width='10%'>&nbsp;<a href='#'class='up'>Up</a>&nbsp;&nbsp;<a href='#' class='down'>Down</a></td>"
                + "<td width='65%'></td>"
                + "</tr>";
                }
                if (SelectedValues[i] != '' && SelectedValues[i] != undefined) {
                    $('#mainTable').find('table[id^="tblData"] tbody').append(rowData);
                    $('#txtSingleSelectValue').val('').focus();
                    hideShowLabel(ind);
                }
                //SetChosen();
                fillSelectInTable(ind);
            }

            $("#txtTextAreaSize").keydown(function (e) {
                $("#txtTextAreaSize").next("div.tooltip_outer").remove();
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <h2>
        <asp:Label ID="lblTitle" Style="margin-left: 5px" runat="server" Text="">Add Clause Field</asp:Label>
    </h2>
    <input type="hidden" runat="Server" id="hdnFieldTypeID" />
    <input type="hidden" runat="Server" id="hdnSignleSelectValues" />
    <input type="hidden" runat="Server" id="hdnSingleSelectMaster" />
    <input type="hidden" runat="Server" id="hdnContractTypeId" />
    <input type="hidden" runat="Server" id="hdnSignleSelectValuesWithClauses" />
    <input type="hidden" runat="Server" id="hdnClauseId" clientidmode="Static" />
    <input type="hidden" runat="Server" id="hdnQuestionTextTemp" clientidmode="Static" />
    <input type="hidden" runat="Server" id="hdnDefaultSizeTemp" clientidmode="Static" />
    <input type="hidden" runat="Server" id="hdnFieldTypeTemp" clientidmode="Static" />
    <input type="hidden" runat="Server" id="hdnIsReviseTemp" clientidmode="Static" />
    <input type="hidden" runat="Server" id="hdnIsRenewTemp" clientidmode="Static" />
    <input type="hidden" runat="Server" id="hdnIsTerminateTemp" clientidmode="Static" />
    <input type="hidden" runat="Server" id="hdnMasterID" />
    <input type="hidden" runat="Server" id="hdnItems" />
    <div id="uploader" style="display: block;">
        <br />
        <br />
        <table id="mainTable" width="100%" border="0" cellspacing="1" cellpadding="2">
            <tr>
                <td width="20%" align="right">
                    <span class="required">*</span>Field Sequence
                </td>
                <td width="2%" align="center">
                    :
                </td>
                <td width="78%" align="left">
                    <asp:TextBox ID="txtSequence" MaxLength="5" class="required NumericOnly" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td width="20%" align="right">
                    <span class="required">*</span>Field Name
                </td>
                <td width="2%" align="center">
                    :
                </td>
                <td width="78%" style="text-align: left;">
                    <asp:TextBox class="required pasteBlock" ReadOnly="true" Width="35%" ClientIDMode="Static"
                        MaxLength="100" ID="txtCustomFieldValue" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td width="20%" align="right">
                    Required for
                </td>
                <td width="2%" align="center">
                    :
                </td>
                <td width="78%" style="text-align: left;">
                    <asp:CheckBoxList ID="chkAVailableFor" TextAlign="Right" RepeatDirection="Horizontal"
                        runat="server">
                        <asp:ListItem>Revise</asp:ListItem>
                        <asp:ListItem>Renew</asp:ListItem>
                        <asp:ListItem>Terminate</asp:ListItem>
                    </asp:CheckBoxList>
                </td>
            </tr>
            <tr>
                <td width="20%" align="right">
                    <span class="required">*</span>Question
                </td>
                <td width="2%" align="center">
                    :
                </td>
                <td width="78%" style="text-align: left;">
                    <asp:TextBox class="required pasteBlock" Width="35%" ClientIDMode="Static" MaxLength="500"
                        ID="txtQuestionText" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td width="20%" align="right">
                    <span class="required">*</span>Field Type
                </td>
                <td width="2%" align="center">
                    :
                </td>
                <td width="78%" style="text-align: left;">
                    <asp:DropDownList ID="drpDropDownList" Width="35.5%" class="required chzn-select"
                        ClientIDMode="Static" runat="server" AutoPostBack="false">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr valign="top" class="FieldValueHeader">
                <td colspan="3" bgcolor="#efefef">
                    <b>Field Values &amp; Validations</b>
                </td>
            </tr>
            <tr valign="top" class="TextArea">
                <td width="20%" align="right">
                    <span class="required">*</span>Default Size
                </td>
                <td width="2%" align="center">
                    :
                </td>
                <td width="78%" align="left">
                    <asp:TextBox ID="txtTextAreaSize" MaxLength="5" class="required" runat="server" Text="50"></asp:TextBox>
                </td>
            </tr>
            <tr valign="top" class="DateField">
                <td width="20%" align="right">
                    Default Date
                </td>
                <td width="2%" align="center">
                    :
                </td>
                <td width="78%" align="left">
                    <input id="txtDatePicker" clientidmode="Static" runat="server" class="datepicker"
                        maxlength="50" type="text" />
                </td>
            </tr>
            <tr valign="top" class="SingleSelect">
                <td width="20%" align="right">
                    Values
                </td>
                <td width="2%" align="center">
                    :
                </td>
                <td width="78%" align="left">
                    <asp:TextBox ID="txtSingleSelectValue" runat="server" MaxLength="50"></asp:TextBox>
                    <input type="button" id="btnAddSingleSelectValue" value="Add" />
                </td>
            </tr>
            <tr valign="top" class="SingleSelectMaster">
                <td width="20%" align="right">
                    Master Name
                </td>
                <td width="2%" align="center">
                    :
                </td>
                <td width="78%" align="left">
                    <asp:DropDownList ID="drpSingleSelectMaster" runat="server" AutoPostBack="false">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="SingleSelect">
                <td width="20%">
                </td>
                <td width="1%">
                </td>
                <td width="79%" align="left">
                    <table id="tblData" width="70%">
                        <tr>
                            <th id="thFieldValue" width="25%">
                                <asp:Label ID="lblValue" ClientIDMode="Static" runat="server" Text="Field Value"></asp:Label>
                            </th>
                            <th id="thAttachClause" width="65%">
                                <asp:Label ID="lblAttachClause" ClientIDMode="Static" runat="server" Text="Attach Clauses"></asp:Label>
                            </th>
                            <th id="thDelete" width="5%">
                            </th>
                            <th id="thUpDown" width="10%">
                            </th>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr valign="top">
                <td colspan="3" style="text-align: left;">
                    <br />
                    <asp:Button ID="btnAddNewCustomField" Visible="true" runat="server" Text="Save Clause Field"
                        OnClick="btnAddNewCustomField_Click" OnClientClick="return hidewindow();" />
                </td>
            </tr>
        </table>
    </div>
    </form>
    <script type="text/javascript">
        $('#btnAddSingleSelectValue').click(function () {

            var val = $('#txtSingleSelectValue').val();
            var ind = $('#drpDropDownList').val();
            var rowData;
            //<select style='width:95%' class='clsClause chzn-select'multiple='multiple'></select>

            rowData = "<tr>"
            + "<td width='25%'><input class='clsValue required' maxlength='50' type='text' value='" + val + "' /></td>"
            + "<td width='65%'> <select style='width:95%' class='clsClause'></select> </td>"
            + "<td width='5%'><a href='#' class='btnDelete' clientidmode='static'>Delete</a></td>"
            + "<td width='10%'>&nbsp;<a href='#'class='up'>Up</a>&nbsp;&nbsp;<a href='#' class='down'>Down</a></td>"
            "</tr>";

            if (ind == 10 || ind == 8) {
                rowData = "<tr>"
            + "<td width='25%'><input class='clsValue required' maxlength='50' type='text' value='" + val + "' /></td>"
            + "<td width='5%'><a href='#' class='btnDelete' clientidmode='static'>Delete</a></select> </td>"
            + "<td width='10%'>&nbsp;<a href='#'class='up'>Up</a>&nbsp;&nbsp;<a href='#' class='down'>Down</a></td>"
            + "<td width='65%'></td>"
            + "</tr>";
            }


            if (val != '' && checkExists(val) == true) {
                $('#mainTable').find('table[id^="tblData"] tbody').append(rowData);
                $('#txtSingleSelectValue').val('').focus();
                hideShowLabel(ind);
            }
            else if (val == '') {
                alert('Please enter value.');
                $('#txtSingleSelectValue').focus().val('');
                return false;
            }
            else {
                alert('This value already exists.');
                $('#txtSingleSelectValue').focus().val('');
                return false;
            }
            //SetChosen();
            fillSelectInTable(ind);
        });

        function fillSelectInTable(flg) {
            $('#mainTable').find('table[id^="tblData"] tbody').find('select').each(function () {
                if ($(this).has('option').length <= 0) {
                    bindSelect(GlobalClauses, this);
                }
            });

            if (flg == 10 || flg == 8 || flg == 16) { // change table head width accordingly..
                $('#mainTable').find('table[id^="tblData"] th').each(function () {

                    if ($(this).attr('id') == 'thFieldValue') {
                        $(this).attr('width', '25');
                    }
                    if ($(this).attr('id') == 'thAttachClause') {
                        $(this).attr('width', '5');
                    }
                    if ($(this).attr('id') == 'thDelete') {
                        $(this).attr('width', '10');
                    }
                    if ($(this).attr('id') == 'thUpDown') {
                        $(this).attr('width', '65');
                    }
                });
            }
        }

        $('.clsValue').live('blur', function () {
            var rvalue = true;
            val = $(this).val();
            var cnt = 0;
            $('#mainTable').find('table[id^="tblData"] tbody').find('input[class^="clsValue"]').each(function () {
                if (val == $(this).val()) {
                    cnt = cnt + 1;
                }
            });

            if (cnt > 1 || val == '') {
                alert('please enter valid value.');
                $(this).focus();
                rvalue = false;
            }
            return rvalue;
        });

        $('.NumericOnly').live('keypress', function (evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode == 8) {
                return true;
            }
            else if ((charCode != 46 || $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57)) {
                return false;
            }
        });

        function checkExists(val) {
            var rvalue = true;
            $('#mainTable').find('table[id^="tblData"] tbody').find('input[class^="clsValue"]').each(function () {
                if (val.toLowerCase() == $(this).val().toLowerCase()) {
                    rvalue = false;
                }
            });
            return rvalue;
        }

        function getSignleSelectValuesWithClauses() {

            var rvalueWithClause = '';
            var rvalue = '';
            var value = '';
            var clauses = '';
            $('#mainTable').find('table[id^="tblData"] tbody').find('tr').each(function () {
                value = $(this).find('input[class^="clsValue"]').val();
                clauses = $(this).find('select').val();

                if (value != undefined && value != null) {
                    if (clauses == undefined || clauses == null) {
                        clauses = '';
                    }
                    if (rvalueWithClause == '') {
                        rvalueWithClause = value + '#' + clauses;
                        rvalue = value;
                    }
                    else {
                        rvalueWithClause = rvalueWithClause + '$' + value + '#' + clauses;
                        rvalue = rvalue + ',' + value;
                    }
                }

            });

            $('#hdnSignleSelectValuesWithClauses').val(rvalueWithClause);
            $('#hdnSignleSelectValues').val(rvalue);
            //return false;
        }

        $('.btnDelete').live('click', function () {
            var par = $(this).parent().parent();
            par.remove();
            $('#hdnSignleSelectValuesWithClauses').val('');
            $('#hdnSignleSelectValues').val('');
        });

        $(document).ready(function () {
            $(".up,.down").live('click', function () {
                var $row = $(this).parents('tr:first');

                if ($(this).is(".up")) {
                    if ($row.index() > 1) {
                        $row.insertBefore($row.prev());
                    }
                } else {
                    $row.insertAfter($row.next());
                }
            });

        });


        $(window).ready(function () {
            function GetClauses() {
                var tableName = 'Clause';
                var parentId = $('#hdnContractTypeId').val();
                var strType = "POST";
                var strContentType = "text/html; charset=utf-8";
                var strData = "{}";
                var strURL = '../Handlers/GetMasterList.ashx?TableName=' + tableName + '&ParentId=' + parentId; //'../Handlers/GetCityList.ashx';
                var strCatch = false;
                var strDataType = 'json';
                var strAsync = false;
                var objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
                var totalClauses = objHandler.HandlerReturnedData;
                //GlobalClauses.shift();

                var clauseId = parseInt($('#hdnClauseId').val());

                GlobalClauses = jQuery.grep(totalClauses, function (a) {
                    return (a.Id !== clauseId);
                });

                hideShowLabel(0);

            }
            GetClauses();
        });


        function hideShowLabel(flg) {

            if (flg == 0) {
                $('#lblValue').hide();
                $('#lblAttachClause').hide();
            }
            else if (flg == 10 || flg == 8) {
                $('#lblAttachClause').hide();
            }
            else {
                $('#lblValue').show();
                $('#lblAttachClause').show();
            }
        }


        function hidewindow() {
            getSignleSelectValuesWithClauses();

            $('#hdnQuestionTextTemp').val($('#txtQuestionText').val());
            $('#hdnDefaultSize').val($('#txtTextAreaSize').val());
            $('#hdnFieldTypeTemp').val($('#drpDropDownList').val());
            $('#hdnIsReviseTemp').val($('#chkAVailableFor_0').attr('checked'));
            $('#hdnIsRenewTemp').val($('#chkAVailableFor_1').attr('checked'));
            $('#hdnIsTerminateTemp').val($('#chkAVailableFor_2').attr('checked'));
            $('#hdnDefaultSizeTemp').val($('#txtTextAreaSize').val());

            var tp = $('#drpDropDownList').val();
            var empty_count = 0;
            $('.required').each(function () {
                if ($(this).get(0).tagName == "INPUT") {
                    if ($(this).val().length == 0) {
                        $(this).css('border-color', 'red');
                        $(this).next("div.tooltip_outer").remove();
                        $(this).after("<div class='tooltip_outer'  style='filter:alpha(opacity=50); opacity:0.9;'><div class='tool_tip' style='filter:alpha(opacity=50); opacity:0.5;color:red;'>This field is required.</div></div>").show("slow");
                        empty_count++;
                    }
                    else {
                        $(this).css('border-color', '');
                        $(this).next("div.tooltip_outer").remove();
                    }
                }
                else {
                    if ($(this).hasClass('chzn-select')) {
                        $(this).next('.tooltip_outer').hide();
                        var sText = $(this).text().toLowerCase();
                        var sVal = $(this).val();
                        var len = $(this).val() == null ? 0 : $(this).val().length;
                        //addes by Bharati 04062014 12.11PM
                        var ValidationMessage = '<div class="tooltip_outer ' + $(this).attr("id") + '"style="filter:alpha(opacity=50); opacity:0.9;" ><div class="arrow-left"></div><div class="tool_tip"  style="filter:alpha(opacity=50); opacity:0.5;color:red;">Please select any option</div></div>';
                        var HasContainer = $(this).val() == "" ? true : false;
                        if (len > 0 && sVal == 0) {
                            $("div ." + $(this).attr("id")).remove();
                            if (!HasContainer)
                                $(this).after(ValidationMessage).show("slow");
                            $(this).css('border-color', 'red');
                            empty_count++;
                        }
                        else {
                            if (!HasContainer) {
                                $(this).next('.tooltip_outer').remove();
                                $(this).css('border-color', '');

                                //separate part
                                if (sVal == "1" || sVal == "4" || sVal == "5" || sVal == "6" || sVal == "7") {
                                    if ($('#<%=txtTextAreaSize.ClientID %>').val() == "") {
                                        $('#<%=txtTextAreaSize.ClientID %>').css('border-color', 'red');
                                        $('#<%=txtTextAreaSize.ClientID %>').next("div.tooltip_outer").remove();
                                        $('#<%=txtTextAreaSize.ClientID %>').after("<div class='tooltip_outer' style='filter:alpha(opacity=50); opacity:0.9;'><div class='tool_tip' style='filter:alpha(opacity=50); opacity:0.5;color:red;'>This field is required.</div></div>").show("slow");
                                        empty_count++;
                                    }
                                    else {
                                        $('#<%=txtTextAreaSize.ClientID %>').css('border-color', '');
                                        $('#<%=txtTextAreaSize.ClientID %>').next("div.tooltip_outer").remove();
                                    }
                                }

                                else if (sVal == "9" || sVal == "11") {
                                    var sTextmaster = $('#drpSingleSelectMaster').text().toLowerCase();
                                    var sValmaster = $('#drpSingleSelectMaster').val();
                                    var lenmaster = $('#drpSingleSelectMaster').val() == null ? 0 : $('#drpSingleSelectMaster').val().length;
                                    var ValidationmasterMessage = '<div class="tooltip_outer ' + $('#drpSingleSelectMaster').attr("id") + '"style="filter:alpha(opacity=50); opacity:0.9;" ><div class="arrow-left"></div><div class="tool_tip"  style="filter:alpha(opacity=50); opacity:0.5;color:red;">Please select any option</div></div>';
                                    var HasmasterContainer = $('#drpSingleSelectMaster').val() == "" ? true : false;
                                    if (lenmaster > 0 && sValmaster == 0) {
                                        $("div ." + $('#drpSingleSelectMaster').attr("id")).remove();
                                        if (!HasmasterContainer) {
                                            $('#drpSingleSelectMaster').after(ValidationMessage).show("slow");
                                            $('#drpSingleSelectMaster').css('border-color', 'red');
                                            empty_count++;
                                        }
                                        else {
                                            if (!HasmasterContainer) {
                                                $(this).next('.tooltip_outer').remove();
                                                $(this).css('border-color', '');
                                            }
                                        } //lenmaster > 0 && sValmaster == 0
                                    } //(sVal == "9" || sVal == "11")
                                }
                            } //separate part
                            else {
                                $(this).next('.tooltip_outer').remove();
                                $(this).css('border-color', '');
                            }
                        }
                    }
                } //end of else
            });

            /*Added By Prashant for defect No 19228*/
            if (tp == "4" || tp == "5") {
                if ($('#<%=txtTextAreaSize.ClientID %>').val() > 18) {
                    $('#<%=txtTextAreaSize.ClientID %>').css('border-color', 'red');
                    $('#<%=txtTextAreaSize.ClientID %>').next("div.tooltip_outer").remove();
                    $('#<%=txtTextAreaSize.ClientID %>').after("<div class='tooltip_outer' style='filter:alpha(opacity=50); opacity:0.9;'><div class='tool_tip' style='filter:alpha(opacity=50); opacity:0.5;color:red;'>Default size should not be greater than 18.</div></div>").show("slow");
                    empty_count++;
                }
            }
            else if (tp == "1" || tp == "6") {
                if ($('#<%=txtTextAreaSize.ClientID %>').val() > 8000) {
                    $('#<%=txtTextAreaSize.ClientID %>').css('border-color', 'red');
                    $('#<%=txtTextAreaSize.ClientID %>').next("div.tooltip_outer").remove();
                    $('#<%=txtTextAreaSize.ClientID %>').after("<div class='tooltip_outer' style='filter:alpha(opacity=50); opacity:0.9;'><div class='tool_tip' style='filter:alpha(opacity=50); opacity:0.5;color:red;'>Default size should not be greater than 8000.</div></div>").show("slow");
                    empty_count++;
                }
            }
            /*Added By Prashant for defect No 19228 END*/

            if (empty_count > 0) {
                // parent.emailwindow.show();
                return false;
            }
            else if ($('#hdnSignleSelectValues').val() == '' && (tp == 8 || tp == 10 || tp == 16)) {
                alert('Please insert required values .');
                $('#txtSingleSelectValue').focus();
                return false;
            }

            else {
                alert('Clause variable updated successfully.');
                parent.emailwindow.hide();
                return true;
            }



        }



    </script>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
        $("#main").addClass("nosidebar");
        $("#toggleimage").hide();
        //SetChosen();

    </script>
</body>
</html>
