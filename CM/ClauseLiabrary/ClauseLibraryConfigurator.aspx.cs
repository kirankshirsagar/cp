﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using CommonBLL;
using ClauseLibraryBLL;
using System.Web.UI.WebControls;

public partial class ClauseLiabrary_ClauseLibraryConfigurator : System.Web.UI.Page
{ 

    IClauseLibrary objContractType;

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();

        ddlContractType.extDataBind(objContractType.SelectContractType());
        ScriptManager.RegisterStartupScript(this, this.GetType(), "tytytt", "SetValueDropDown('" + ddlContractType.Value + "');", true);
    }

    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            objContractType = FactoryClause.GetContractTypesDetails();
         
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }


    protected void btnSaveCustomFieldValues_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        InsertUpdate();
        Page.TraceWrite("Save Update button clicked event ends.");
    }

    void InsertUpdate()
    {
        Page.TraceWrite("Insert, Update starts.");
        string status = "";
        objContractType.ClauseName = Convert.ToString(txtClauseName.Value);
        objContractType.ContractTypeId = Convert.ToInt32(hdnContractTypeID.Value);
        objContractType.VersionName = Convert.ToString(txtVersion.Value);
        objContractType.Language = Convert.ToString(txtClauseText.Value);
        objContractType.Addedby = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objContractType.Modifiedby = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objContractType.IP = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
        objContractType.isActive =true;
  

        try
        {
            string ID=objContractType.InsertRecord();
            objContractType.CluaseID = Convert.ToInt32(ID);
            objContractType.isNewVersion = "Y";
            objContractType.UpdateClauseLanguage();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Insert, Update ends.");
        Response.Redirect("EditClause.aspx");
    }


}