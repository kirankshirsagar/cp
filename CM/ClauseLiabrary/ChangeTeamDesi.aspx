﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using CommonBLL;
using ClauseLibraryBLL;
using System.Web.Services;
using System.Web.UI.WebControls;

public partial class ClauseLiabrary_Clause : System.Web.UI.Page
{
    IClauseLibrary Obj;
    IClauseFields ClauseFields;
   

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        if (!IsPostBack)
        {
            ContractTypeBind();
            if (String.IsNullOrEmpty(Request.extValue("hdnPrimeIds")) != true)
            {
                hdnPrimeId.Value = Request.extValue("hdnPrimeIds");
                lblEdittext.InnerText = "Edit clause";
                EditClause();
            }
            else if (Request.QueryString["CluaseID"] != null && Request.QueryString["CluaseID"] != "0") 
            {
                hdnPrimeId.Value = Request.QueryString["CluaseID"].ToString();
                string msg = "Clause saved successfully. Please continue to add clause description.";
                lblEdittext.InnerText = "Add clause description";
                Page.JavaScriptClientScriptBlock("msgdfg", "MessageMasterDiv('" + msg + "',0,500)");
                EditClause();
           }

            else
            {
                lblEdittext.InnerText = "Add new clause";
                AddClause();
            }
        }
      
    }

   
    void ContractTypeBind()
    {
        Page.TraceWrite("ContractType dropdown bind starts.");
        try
        {
            ddlContractType.extDataBind(Obj.SelectContractType());
        }
        catch (Exception ex)
        {
            Page.TraceWarn("ContractType dropdown bind fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("ContractType dropdown bind ends.");
    }

    protected void EditClause()
    {
        description.Visible = true;
        btnSaveAsNewVersion.Visible = true;
        btnSaveCLuase.Visible = true;
        btnSaveCLause.Visible = false;
        Obj.CluaseID = Convert.ToInt32(hdnPrimeId.Value);
        List<ClauseLibrary> lstClauseData = new List<ClauseLibrary>();
        lstClauseData = Obj.ReadClauseDataData();
        txtVersion.Value = lstClauseData[0].VersionName;
        hdnVersionID.Value = lstClauseData[0].VersionID;
        txtClauseText.Value = lstClauseData[0].Language.Replace("<br>", "\r\n");
        txtClause.Value = lstClauseData[0].ClauseName;
        ddlContractType.Value = lstClauseData[0].ContractTypeId.ToString();
        hdnContractTypeID.Value = Convert.ToString(lstClauseData[0].ContractTypeId);
        txtHiddenCluaseId.Value = hdnPrimeId.Value;
    }
    protected void AddClause()
    {
        description.Visible = false;
        btnSaveAsNewVersion.Visible = false;
        btnSaveCLause.Visible = true;
        btnSaveCLuase.Visible = false;
    }


    void CreateObjects()
    {
        Page.TraceWrite("Page object create starts.");
        try
        {
            Obj = FactoryClause.GetContractTypesDetails();
            ClauseFields = FactoryClause.GetFieldReferenceDetails();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }
    protected void btnSaveCustomFieldValues_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");

        InsertUpdate();

        Page.TraceWrite("Save Update button clicked event ends.");

    }
    protected void btnSaveAsNewVersion_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");

        InsertUpdateAsNewVersion();

        Page.TraceWrite("Save Update button clicked event ends.");

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Save Update button clicked event starts.");
        //save clause only
        Insert();

        Page.TraceWrite("Save Update button clicked event ends.");
    }
    void Insert()
    {
        string VariablesUsed = GetVariablesInUse();

        Page.TraceWrite("Insert starts.");
        IClauseLibrary ObjDocumentType;
        ObjDocumentType = FactoryClause.GetContractTypesDetails();
        ObjDocumentType.ClauseName = txtClause.Value;
        ObjDocumentType.ContractTypeId =Convert.ToInt32(ddlContractType.Value);

        string IsDuplicate = ObjDocumentType.CheckDuplicateClause();
        if (Convert.ToInt32(IsDuplicate) >0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "tytyttt", "javascript:isClauseNameexist();", true);
            return;
        }

        Obj.ClauseName = Convert.ToString(txtClause.Value);
        if(hdnContractTypeID.Value != "")
        Obj.ContractTypeId = Convert.ToInt32(hdnContractTypeID.Value);
        Obj.VersionName = Convert.ToString(txtVersion.Value);
        Obj.Language = Convert.ToString(txtClauseText.Value);
        Obj.Addedby = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        Obj.Modifiedby = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        Obj.IP = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
        Obj.isActive = true;
        Obj.VariablesInUseIds = VariablesUsed;
        try
        {
            string ID = Obj.InsertRecord();
            Obj.CluaseID = Convert.ToInt32(ID);
            Obj.isNewVersion = "Y";
            Obj.UpdateClauseLanguage();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("Insert fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Insert ends.");
        Server.Transfer("Clause.aspx?CluaseID=" + Obj.CluaseID); 
    }
    void InsertUpdateAsNewVersion()
    {
       
        //Save As New Version Code Starts Here.
        Page.TraceWrite("Insert, Update for New Version Starts Here.");
        string status = "";
        try
        {
            Obj.CluaseID = Convert.ToInt32(hdnPrimeId.Value);
            Obj.ClauseName = txtClause.Value;
            Obj.ParentID = Obj.CluaseID;
            Obj.VersionName = txtVersion.Value;
            Obj.Language = Convert.ToString(txtClauseText.Value.Replace("\r\n", "<br>"));

            txtClauseText.Attributes.Add("MyText", hdnClauseMain.Value.Replace("\r\n", "<br>"));
            Obj.Addedby = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            Obj.Modifiedby = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            Obj.IP = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
            Obj.isActive = true;
            Obj.ContractTypeId = Convert.ToInt32(hdnContractTypeID.Value);
            string ClauseIDNew = Obj.InsertRecord();
            Obj.CluaseID = Convert.ToInt32(ClauseIDNew);
            Obj.UpdateClauseLanguage();
            Page.Message("1", "0");
        }
        catch (Exception ex)
        {
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

        Page.TraceWrite("Insert, Update for New Version Ends Here.");
            Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
            //Response.Redirect("ClauseDetails.aspx");
        ClearData(status);
      //  Server.Transfer("ClauseData.aspx");
    }

    string GetVariablesInUse()
    {

        string VariableIdConc = "";
        string[] VariableIds;
        int Counter = 0;
        string StringClause = Convert.ToString(txtClauseText.Value);
        VariableIds = StringClause.Split(new string[] { "##" }, StringSplitOptions.None);
        foreach (var item in VariableIds)
        {
            try
            {
                if (Counter % 2 != 0 && item.Split(':')[1].Length != 0)
                {
                    VariableIdConc = VariableIdConc + "," + item.Split(':')[1];
                }
            }
            catch (Exception ex)
            {


            }
            Counter++;
        }

        var FinalIds = VariableIdConc.TrimStart(',');
        return FinalIds;
    
    }
    void InsertUpdate()
    {

        string VariablesUsed = GetVariablesInUse();
      
        Page.TraceWrite("Insert, Update starts.");
        string status = "1";
        try
        {
            if (hdnPrimeId.Value != "")
            {
                Obj.CluaseID = Convert.ToInt32(hdnPrimeId.Value);
            }
            Obj.Language = Convert.ToString(txtClauseText.Value.Replace("\r\n", "<br>"));
            txtClauseText.Attributes.Add("MyText", hdnClauseMain.Value.Replace("\r\n", "<br>"));
            Obj.VersionName = txtVersion.Value;
            Obj.ClauseName = txtClause.Value;
            Obj.Addedby = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            Obj.Modifiedby = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
            Obj.IP = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
            Obj.VariablesInUseIds = VariablesUsed;
            Obj.UpdateClauseLanguage();
            Page.Message("1", "0");
        }
        catch (Exception ex)
        {
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Insert, Update ends.");

        Session[Declarations.Message] = (hdnPrimeId.Value == "0") ? "0" : "1";
        Server.Transfer("ClauseData.aspx");
        ClearData(status);
    }


    protected void Back_Click(object sender, EventArgs e)
    {
        Server.Transfer("ClauseData.aspx");
    }

    void ClearData(string status)
    {
        if (status == "1")
        {
            Page.TraceWrite("clearData starts.");
            hdnPrimeId.Value = "0";
            txtClauseText.Value = "";
            txtClause.Value = "";
            txtVersion.Value = "";
            ddlContractType.Value = "0";
            Page.TraceWrite("clear Data ends.");
        }
    }


    //protected void btnAddNewCustomField_Click(object sender, EventArgs e)
    //{
    //    Page.TraceWrite("Save Update button clicked event starts.");
    //    InsertUpdatepopup();
    //    Page.TraceWrite("Save Update button clicked event ends.");
    //}

    //void InsertUpdatepopup()
    //{
    //    Page.TraceWrite("Insert, Update starts.");
    //    string status = "";
    //    ClauseFields.ClauseID = ClauseID;
    //    ClauseFields.FieldName = Convert.ToString(txtCustomFieldValue.Text);
    //   // ClauseFields.FieldHelpBubble = Convert.ToString(txtCustomFieldHelpBubble.Text);
    //    ClauseFields.FieldTypeID = Convert.ToInt32(hdnFieldTypeID.Value);
    //    ClauseFields.Question = Convert.ToString(txtQuestionText.Text);
    //    ClauseFields.isRequired = "Y";

    //    if (txtTextAreaSize.Text == "")
    //    {
    //        txtTextAreaSize.Text = "1000";
    //    }
    //    ClauseFields.DefaultSize = Convert.ToInt32(txtTextAreaSize.Text);
    //    InsertPickListObjects(1, ClauseFields.FieldTypeID.ToString());
    //    ClauseFields.ClauseFieldValue = Convert.ToString(hdnSignleSelectValues.Value);
    //    ClauseFields.Addedby = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
    //    ClauseFields.Modifiedby = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
    //    ClauseFields.IP = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
    //    ClauseFields.ValidationMessage = "";
    //    try
    //    {
    //        string FieldID = ClauseFields.InsertRecord();
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "tytytt", "CloseForm('" + ClauseFields.FieldName + "','" + ClauseFields.ClauseID + "','" + FieldID + "');", true);
    //    }
    //    catch (Exception ex)
    //    {
    //        Page.TraceWarn("Insert, Update fails.");
    //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
    //    }

    //    Page.TraceWrite("Insert, Update ends.");
    //}

    //protected void InsertPickListObjects(int ClauseFieldID, string isMaster)
    //{
    //    string[] SingleSelectArray;

    //    if (isMaster == "9" || isMaster == "11" || isMaster == "13" || isMaster == "15")
    //    {
    //        hdnSignleSelectValues.Value = drpSingleSelectMaster.SelectedValue;
    //    }
    //    else
    //    {

    //        string SingleSelectValues = hdnSignleSelectValues.Value.TrimEnd(',');
    //        SingleSelectArray = SingleSelectValues.Split(',');
    //        string ListItems = "";
    //        try
    //        {
    //            foreach (string Item in SingleSelectArray)
    //            {
    //                ListItems = ListItems + Item + ",";
    //            }

    //            hdnSignleSelectValues.Value = ListItems.TrimEnd(',').TrimStart(',');

    //        }
    //        catch (Exception ex)
    //        {
    //        }
    //        finally
    //        {

    //        }
    //    }

    //}

}