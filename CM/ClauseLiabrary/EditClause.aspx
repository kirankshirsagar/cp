﻿<%@ Page Language="C#" AutoEventWireup="true"  EnableEventValidation="false" MasterPageFile="~/CM.master" CodeFile="EditClause.aspx.cs" Inherits="ClauseLiabrary_EditClause" %>

<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../CommonScripts/GridSelect.js" type="text/javascript"></script>
    <link href="../Styles/Font.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div>
    <input id="hdnPrimeIds" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnSearch" runat="server" clientidmode="Static" type="hidden" />
    <a href="ClauseLibraryConfigurator.aspx" runat="Server"  ><b>Add New Clause</b></a>
        <table width="100%">
         
       
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:Repeater ID="rptClauseDetails" runat="server" 
                                   >
                                    <HeaderTemplate>
                                        <table id="masterClauseDetalDataTable" class="masterTable" width="100%">
                                            <thead>
                                                <tr>
                                                  
                                                    <td width="5%">

                                                    </td>

                                                    <td width="0%" style="display: none;">
                                                       <b> Clause ID   </b> 
                                                    </td>

                                                    <td width="20%" class="labelfont">
                                                          <b>  Clause Name   </b> 
                                                    </td>

                                                    <td width="20%" class="labelfont">
                                                         <b>  Version Name     </b> 
                                                    </td>

                                                    <td width="20%" class="labelfont">
                                                         <b>  Version ID    </b> 
                                                    </td>

                                                    <td style="display: none" width="40%" class="labelfont">
                                                           <b> Version ID   </b> 
                                                    </td>
                                                    <td style="display: none" width="20%" class="labelfont">
                                                         <b>  Version Name   </b> 
                                                    </td>

                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                          
                                            <td>
                                                <asp:ImageButton ID="imgEdit" OnClientClick="return setSelectedId(this);" runat="server"
                                                    ImageUrl="../Images/application_edit.png" OnClick="imgEdit_Click"  />
                                            </td>
                                            <td style="display: block">
                                                <asp:Label ID="lblClauseID" style="display:none" runat="server" ClientIDMode="Static" Text='<%#Eval("CluaseID") %>'></asp:Label>
                                                <asp:Label ID="lblClauseName" runat="server" ClientIDMode="Static" Text='<%#Eval("ClauseName") %>'></asp:Label>
                                            </td>
                                            <td  style="display: none">
                                                <asp:Label ID="lblFieldID" runat="server" ClientIDMode="Static" Text='<%#Eval("FieldID") %>'></asp:Label>
                                            </td>
                                            <td style="display: none" >
                                              <asp:Label ID="lblFieldName" runat="server" ClientIDMode="Static" Text='<%#Eval("FieldName") %>'></asp:Label>
                                            </td>
                                            <td>
                                                  <asp:Label ID="lblVersionName" runat="server" ClientIDMode="Static" Text='<%#Eval("VersionName") %>'></asp:Label>
                                            </td>
                                            <td>
                                            <asp:Label ID="lblVersionID" runat="server" ClientIDMode="Static" Text='<%#Eval("VersionID") %>'></asp:Label>
                                            </td>
                                           
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody> </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <uc1:PaginationButtons ID="PaginationButtons1" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">



        function GetSelectedItems(flg) {
//            $('#hdnPrimeIds').val('');
//            $('#hdnUsedNames').val('');
//            var tableClass = 'masterTable';
//            var deleteLabelId = 'lblCountryId';
//            var deleteLabelName = 'lblCountryName';
//            var objCheck = new CkeckBoxSelect(tableClass, deleteLabelId, deleteLabelName);
//            var deletedIds = objCheck.DeletedIds;
//            var usedNames = objCheck.UsedNames;

//            if (deletedIds == '') {
//                MessageMasterDiv('Please select record(s).');
//                return false;
//            }
//            else if (usedNames != '' && flg == 'D') {
//                MessageMasterDiv('Some records can not be deleted.' + usedNames, 1);
//                return false;
//            }
//            if (flg == 'D') {
//                if (DeleteConfrim() == true) {
//                    $('#hdnPrimeIds').val(deletedIds);
//                    return true;
//                }
//                else {
//                    return false;
//                }
//            }
//            else {
//                $('#hdnPrimeIds').val(deletedIds);
//                return true;
//            }
//            $('#hdnPrimeIds').val(deletedIds);

//            return true;
        }


        $(document).ready(function () {
         //   defaultTableValueSetting('lblCountryId', 'MstCountry', 'CountryId');
         //   LockUnLockImage('masterTable');
        });


        function setSelectedId(obj) {
            var pId = $(obj).closest('tr').find('#lblClauseID').text();
            if (pId == '' || pId == '0') {
                return false;
            }
            else {
                $('#hdnPrimeIds').val(pId);
                return true;
            }
        }



      

    </script>

      <script type="text/javascript">
          $("#sidebar").html($("#rightlinks").html());
          $("#main").addClass("nosidebar");
          $("#toggleimage").hide();

        </script>

</asp:Content>