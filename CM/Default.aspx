﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

  <script type="text/javascript">

      function redirectPage() {
          location.href = "Workflow/ContractRequest.aspx";
          return false;
      }

      redirectPage();

   </script>

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <table width="100%" align="center" cellpadding="10">
        <tr>
            <td class="heading" style="padding-left: 6%">
                Menu
            </td>
        </tr>
        <tr>
            <td>
                <table align="left" width="100%">
                    <tr>
                        <td class="labelfont" width="5%">
                            <em>- </em>
                        </td>
                        <td class="labelfont" width="95%">
                            <asp:LinkButton runat="server" PostBackUrl="~/Masters/CountryMasterData.aspx" ID="lbtn_country"> Country </asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="labelfont" width="5%">
                            <em>- </em>
                        </td>
                        <td class="labelfont" width="95%">
                            <asp:LinkButton runat="server" PostBackUrl="~/Masters/StateMasterData.aspx" ID="lbtn_state"> State </asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="labelfont" width="5%">
                            <em>- </em>
                        </td>
                        <td class="labelfont" width="95%">
                            <asp:LinkButton runat="server" PostBackUrl="~/Masters/CityMasterData.aspx" ID="lbtn_city"> City </asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="labelfont" width="5%">
                            <em>- </em>
                        </td>
                        <td class="labelfont" width="95%">
                            <asp:LinkButton runat="server" PostBackUrl="~/Masters/ContractTypeMasterData.aspx"
                                ID="lbtn_contract"> Contract Type </asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="labelfont" width="5%">
                            <em>- </em>
                        </td>
                        <td class="labelfont" width="95%">
                            <asp:LinkButton runat="server" PostBackUrl="~/Masters/RequestMasterData.aspx" ID="lbtn_request"> Request Type </asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="labelfont" width="5%">
                            <em>- </em>
                        </td>
                        <td class="labelfont" width="95%">
                            <asp:LinkButton runat="server" PostBackUrl="~/Masters/DepartmentMasterData.aspx"
                                ID="lbtn_department"> Department Type </asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="labelfont" width="5%">
                            <em>- </em>
                        </td>
                        <td class="labelfont" width="95%">
                            <asp:LinkButton runat="server" PostBackUrl="~/Masters/TitleMasterData.aspx" ID="lbtn_title"> Title </asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="labelfont" width="5%">
                            <em>- </em>
                        </td>
                        <td class="labelfont" width="95%">
                            <asp:LinkButton runat="server" PostBackUrl="~/UserManagement/Register.aspx" ID="LinkButton1"> Create User </asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="labelfont" width="5%">
                            <em>- </em>
                        </td>
                        <td class="labelfont" width="95%">
                            <asp:LinkButton runat="server" PostBackUrl="~/UserManagement/RegisterData.aspx"
                                ID="LinkButton2">  User list </asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="labelfont" width="5%">
                            <em>- </em>
                        </td>
                        <td class="labelfont" width="95%">
                            <asp:LinkButton runat="server" PostBackUrl="~/UserManagement/RoleMasterData.aspx"
                                ID="lnkrole">  Role Master </asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="labelfont" width="5%">
                            <em>- </em>
                        </td>
                        <td class="labelfont" width="95%">
                            <asp:LinkButton runat="server" PostBackUrl="~/UserManagement/ChangePassword.aspx"
                                ID="LinkButton3">  Change Password </asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="labelfont" width="5%">
                            <em>- </em>
                        </td>
                        <td class="labelfont" width="95%">
                            <asp:LinkButton runat="server" PostBackUrl="~/UserManagement/RolePermissions.aspx"
                                ID="LinkButton4">  Permission Management </asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="labelfont" width="5%">
                            <em>- </em>
                        </td>
                        <td class="labelfont" width="95%">
                            <asp:LinkButton runat="server" PostBackUrl="~/Workflow/ContractRequest.aspx" ID="lnkcr">  Contract Request </asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

  

</asp:Content>
