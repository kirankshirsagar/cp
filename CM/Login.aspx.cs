﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;
using UserManagementBLL;
using System.Web.Security;
using System.Reflection;


public partial class User_Login : System.Web.UI.Page
{
    CryptoGraphy objEncrypt = new CryptoGraphy();

    protected void Page_Load(object sender, EventArgs e)
    {

        PropertyInfo isreadonly = typeof(System.Collections.Specialized.NameValueCollection).GetProperty("IsReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
        // make collection editable
        isreadonly.SetValue(this.Request.QueryString, false, null);
        // remove
        //this.Request.QueryString.Remove("ReturnUrl");
        if (Convert.ToString(Request["ReturnUrl"]) != null)
        {
            string RetUrl = Convert.ToString(Request["ReturnUrl"].Contains("RequestFlow.aspx"));
            bool bRetUrl = Convert.ToBoolean(RetUrl);
            if (bRetUrl == false)
                this.Request.QueryString.Remove("ReturnUrl");
            else
            {
                try
                {
                    string ReturnUrl = Server.UrlDecode(Request.Url.PathAndQuery);

                    int startPos = ReturnUrl.LastIndexOf("ReturnUrl=") + "ReturnUrl=".Length;
                    int length = ReturnUrl.Length - startPos;
                    string subReturnUrl = ReturnUrl.Substring(startPos, length);
                    Session["ReturnUrl"] = subReturnUrl;
                }
                catch { }

            }
        }
        else
            Session["ReturnUrl"] = null;
        if (!IsPostBack)
        {
            if (Request.Cookies["myCookie"] != null)
            {
                HttpCookie myCookie = new HttpCookie("myCookie");
                myCookie = Request.Cookies.Get("myCookie");
                if (myCookie.Values["UserName"] != null)
                {
                    LoginUser.UserName = myCookie.Values["UserName"].ToString();
                }
                if (myCookie.Values["Password"] != null)
                {
                    hdnPassword.Value = myCookie.Values["Password"].ToString();
                }
                TextBox tb = (TextBox)LoginUser.FindControl("Password");
                tb.Attributes["Value"] = hdnPassword.Value;
                LoginUser.RememberMeSet = true;
            }

            if (Session[Declarations.Message] != null)
            {
                Message();
            }
            else
            {
                errorExplanation.Attributes.Remove("class");
                errorExplanation.Style.Add("display", "none");
            }
        }

        errorExplanation.Attributes.Remove("class");
        errorExplanation.Style.Add("display", "none");

        var Ip = Request.ServerVariables["REMOTE_ADDR"].ToString();
        if (Ip != null && Session[Declarations.IP] == null)
        {
            Session[Declarations.IP] = Ip;
        }

        if (Request.QueryString["ErrorMessage"] != null)
        {
            ExpiryMsg(Request.QueryString["ErrorMessage"].ToString());
        }


    }


    void Message()
    {
        Session[Declarations.Message] = null;
        errorExplanation.Attributes.Add("class", "flash error");
        LoginUser.FailureText = "Invalid user name or password.";
        errorExplanation.Style.Add("display", "block");
        errorExplanation.InnerHtml = "Invalid user name or password.";
    }


    protected void ExpiryMsg(string msg)
    {
        errorExplanation.Attributes.Add("class", "flash error");
        LoginUser.FailureText = "Invalid user name or password.";
        errorExplanation.Style.Add("display", "block");
        errorExplanation.InnerHtml = msg;
        Page.TraceWarn(msg);
    }

    protected void LoginButton_Click(object sender, EventArgs e)
    {
        Page.TraceWrite("Login button click event.");
          
        IUsers obj = FactoryUser.GetUsersDetail();
            obj.UserName = LoginUser.UserName;

            #region DO NOT DELETE
            List<Users> l = obj.ReadData(); //Stated to form connection to DB for executing Membership.ValidateUser()
            #endregion

            try
            {
                //if (Membership.ValidateUser(LoginUser.UserName.Trim(), hdnPassword.Value=="" ? LoginUser.Password : hdnPassword.Value) == true)
                if (Membership.ValidateUser(LoginUser.UserName.Trim(), LoginUser.Password.Trim()) == true)
                {
                    obj.GetUsersDetails();
                    LogPasswordActivity(obj.UserName);
                }
                else
                {
                    int failedPWds = 0;
                    failedPWds = int.Parse(obj.getTotalLoginfailed().ToString());

                    if (failedPWds >= 10)
                    {
                        obj.GetUsersDetails();
                        obj.UserIds = (obj.UserId).ToString();
                        obj.Flag = "L";
                        string status = obj.LockUser();
                        LoginUser.FailureText = "This user account has been locked. Please contact system Administrator.";
                        errorExplanation.Style.Add("display", "block");
                        errorExplanation.Attributes.Add("class", "flash error");
                        errorExplanation.InnerHtml = "This user account has been locked. Please contact system Administrator.";
                        return;
                    }
                    else
                    {
                        if (LoginUser.RememberMeSet == true)
                        {
                            HttpCookie myCookie = new HttpCookie("myCookie");
                            Response.Cookies.Remove("myCookie");

                            myCookie.Values.Add("UserName", LoginUser.UserName);
                            myCookie.Values.Add("Password", LoginUser.Password);
                            DateTime dtxpiry = DateTime.Now.AddDays(15);

                            myCookie.Expires = dtxpiry;
                            Response.Cookies.Add(myCookie);
                        }
                        else
                        {
                            HttpCookie cookie = null;
                            if (Request.Cookies["myCookie"] != null)
                            {
                                cookie = HttpContext.Current.Request.Cookies["myCookie"];
                                //You can't directly delete cookie you should 
                                //set its expiry date to earlier date 
                                cookie.Expires = DateTime.Now.AddDays(-1);
                                HttpContext.Current.Response.Cookies.Add(cookie);
                            }
                        }
                        errorExplanation.Attributes.Remove("class");
                        errorExplanation.Style.Add("display", "none");
                    }
                }

                if (obj.UserId > 0)
                {
                    obj.UserIds = (obj.UserId).ToString();

                    obj.Flag = "U";
                    string status = obj.LockUser();
                    Page.TraceWrite("login session set.");
                    if (LoginUser.RememberMeSet == true)
                    {
                        HttpCookie myCookie = new HttpCookie("myCookie");
                        Response.Cookies.Remove("myCookie");

                        myCookie.Values.Add("UserName", LoginUser.UserName);
                        myCookie.Values.Add("Password", LoginUser.Password);
                        DateTime dtxpiry = DateTime.Now.AddDays(15);

                        myCookie.Expires = dtxpiry;
                        Response.Cookies.Add(myCookie);
                    }
                    else
                    {
                        HttpCookie cookie = null;
                        if (Request.Cookies["myCookie"] != null)
                        {
                            cookie = HttpContext.Current.Request.Cookies["myCookie"];
                            //You can't directly delete cookie you should 
                            //set its expiry date to earlier date 
                            cookie.Expires = DateTime.Now.AddDays(-1);
                            HttpContext.Current.Response.Cookies.Add(cookie);
                        }
                    }

                    Session[Declarations.User] = obj.UserId;
                    Session[Declarations.AccessCSV] = obj.AccessIds;
                    Session[Declarations.Department] = obj.DepartmentId;
                    Session[Declarations.UserFullName] = obj.FullName;
                    Session[Declarations.UserRole] = obj.RoleName;
                    Session[Declarations.AccessMainSectionCSV] = obj.AccessMainSectionIds;
                    Session[Declarations.OutlookFileName] = obj.OutlookFileName;
                    Session[Declarations.IsOutlookSynchEnable] = obj.IsOutlookSynchEnable;
                    Session[Declarations.Message] = null;

                    Session["TenantDIR"] = Request.Url.Segments[1].Replace("/", "");
                    string Baseurl = Request.Url.AbsoluteUri.Remove(Request.Url.AbsoluteUri.IndexOf("/Login.aspx"));
                    Session[Declarations.URL] = Baseurl;
                    Session[Declarations.OutlookURL] = Baseurl.Remove(Baseurl.LastIndexOf("/")) + "/OutlookCalendarCP/" + Session["TenantDIR"] + "/" + Session[Declarations.OutlookFileName];

                    Session["ImageUrl"] = obj.ProfileImgUrl();
                    errorExplanation.Style.Add("display", "none");
                }
                else
                {
                    errorExplanation.Attributes.Add("class", "flash error");
                    LoginUser.FailureText = "Invalid user name or password.";
                    errorExplanation.Style.Add("display", "block");
                    errorExplanation.InnerHtml = "Invalid user name or password.";
                    Session[Declarations.Message] = "Invalid user name or password.";
                    FormsAuthentication.SignOut();
                    Session[Declarations.User] = null;
                    Session[Declarations.AccessCSV] = null;
                    Session[Declarations.Department] = null;
                    Session[Declarations.UserRole] = null;
                    Page.TraceWarn("Unable to set login session.");
                }

            }
            catch (Exception ex)
            {
                errorExplanation.Attributes.Add("class", "flash error");
                LoginUser.FailureText = "Invalid user name or password.";
                errorExplanation.Style.Add("display", "block");
                errorExplanation.InnerHtml = "Invalid user name or password.";
                Page.TraceWarn("Login authentication fails.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }       
    }

    private void LogPasswordActivity(string userName)
    {
        IUsers objuser = FactoryUser.GetUsersDetail();
        objuser.UserName = userName;
        objuser.UserPasswordActivityLog(1);
    }
}
