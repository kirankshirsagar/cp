﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aquaforest.OCR.Api;
using Aquaforest.OCR.Definitions;
using Aquaforest.PDF;
using System.IO;
using System.Configuration;
using word = Aspose.Words;
using pdf = Aspose.Pdf;

public class PDFtoWORD
{
    static Ocr ocr;
    string AIDirectory = HttpContext.Current.Server.MapPath("~/Uploads/" + HttpContext.Current.Session["TenantDIR"] + "/AINLP/");    

    string ContractVersionDOCX = HttpContext.Current.Server.MapPath("~/Uploads/" + HttpContext.Current.Session["TenantDIR"] + "/Contract Documents/");
    string ContractVersionPDF = HttpContext.Current.Server.MapPath("~/Uploads/" + HttpContext.Current.Session["TenantDIR"] + "/Contract Documents/PDF/");

    static string ErrorLog = HttpContext.Current.Server.MapPath("~/Uploads/" + HttpContext.Current.Session["TenantDIR"] + "/Error.txt");

    Aspose.Words.License WORDlicense = new Aspose.Words.License();
    Aspose.Pdf.License PDFlicense = new Aspose.Pdf.License();

    public PDFtoWORD()
    {
        WORDlicense.SetLicense("Aspose.Words.lic");
        PDFlicense.SetLicense("Aspose.PDF.lic");
    }

    public string Convert(string PDFFilePath)
    {
        string WordFilePath = "";
        if (!Directory.Exists(AIDirectory))
            Directory.CreateDirectory(AIDirectory);

        FileInfo file = new FileInfo(PDFFilePath);
      
        string DOCXfile = Path.Combine(AIDirectory, Path.ChangeExtension(file.Name, "doc"));       

        pdf.Document PDFfile = new pdf.Document(PDFFilePath);
        pdf.DocSaveOptions saveOptions = new pdf.DocSaveOptions();
        saveOptions.Format = pdf.DocSaveOptions.DocFormat.Doc;

        saveOptions.Mode = pdf.DocSaveOptions.RecognitionMode.Flow;
        saveOptions.RecognizeBullets = true;

        PDFfile.Save(DOCXfile, saveOptions);
        WordFilePath = DOCXfile;

        return WordFilePath;
    }
   
    #region using AQUAFOREST
    public string ConvertDOCX(string PDFFilePath)
    {
        string WordFilePath = "";
        if (!Directory.Exists(AIDirectory))
            Directory.CreateDirectory(AIDirectory);

        FileInfo file = new FileInfo(PDFFilePath);

        var documentPagesText = new Dictionary<int, string>();
        bool hasImageOnlyPage = CheckDocumentUsingToolkit(file.FullName, documentPagesText);
        if (hasImageOnlyPage)
        {
            OcrPdf(file.FullName, documentPagesText);
        }
        if (documentPagesText.Count > 0)
        {
            string DOCfile = Path.Combine(AIDirectory, Path.ChangeExtension(file.Name, "doc"));
            string DOCXfile = Path.Combine(AIDirectory, Path.ChangeExtension(file.Name, "docx"));

            WriteTextToFile(DOCfile, documentPagesText);
            word.Document wordDoc = new word.Document(DOCfile);
            wordDoc.Save(DOCXfile, Aspose.Words.SaveFormat.Docx);
            File.Delete(DOCfile);

            WordFilePath = DOCXfile;
        }
        else
        {
            Console.WriteLine("No text found.");
        }
        return WordFilePath;
    }

    static bool CheckDocumentUsingToolkit(string source, Dictionary<int, string> documentPagesText)
    {
        PDFDocument doc = null;
        bool hasImageOnlyPage = false;
        try
        {
            doc = new PDFDocument(source);
            PDFToolkit.LicenseKey = ConfigurationManager.AppSettings["PDFToolkitLicenseKey"];
            for (int i = 1; i <= doc.NumberOfPages; i++)
            {
                string pageText = doc.GetText(i);
                documentPagesText[i] = pageText;
                // If the document has at least 1 image-only page, it will be OCRed
                if (!hasImageOnlyPage)
                {
                    // Check if this page is image-only.
                    // ASSUMPTION: if the page doesn't have any text, it is considered as
                    // image-only, even if it is a blank page with no images
                    hasImageOnlyPage = string.IsNullOrEmpty(pageText.Replace("\r\n", "").Replace("\n", "").Replace("\r", ""));
                }
            }
        }
        catch (Exception e)
        {
            File.WriteAllText(ErrorLog, "\n" + e.Message);
            throw e;
        }
        finally
        {
            if (doc != null) doc.Close();
        }
        return hasImageOnlyPage;
    }

    static bool OcrPdf(string source, Dictionary<int, string> documentPagesText)
    {
        try
        {
            Console.WriteLine("OCRing...");
            using (ocr = new Ocr())
            {
                string resourceFolder = @"C:\Aquaforest\OCRSDK\bin";
                string currentEnvironmentVariables =
                Environment.GetEnvironmentVariable("PATH");
                if (!currentEnvironmentVariables.Contains(resourceFolder))
                {
                    Environment.SetEnvironmentVariable("PATH",
                    currentEnvironmentVariables + ";" + resourceFolder);
                }
                ocr.ResourceFolder = resourceFolder;
                ocr.EnableConsoleOutput = true;
                ocr.Language = SupportedLanguages.English;
                ocr.EnablePdfOutput = true;
                ocr.StatusUpdate += (sender, pageCompleteEventArgs) =>
                OcrStatusUpdate(sender, pageCompleteEventArgs, documentPagesText);
                PreProcessor preProcessor = new PreProcessor();
                preProcessor.Deskew = true;
                preProcessor.Autorotate = false;
                ocr.ReadPDFSource(source);
                ocr.Recognize(preProcessor);
                ocr.DeleteTemporaryFiles();
                return true;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("Error in OCR Processing : {0}", e.Message);
            return false;
        }
    }

    static void OcrStatusUpdate(object sender, StatusUpdateEventArgs pageCompleteEventArgs, Dictionary<int, string> documentPagesText)
    {
        if (pageCompleteEventArgs.TextAvailable)
        {
            int pageNumber = pageCompleteEventArgs.PageNumber;
            string pageString = ocr.ReadPageString(pageNumber);
            // Optional
            string existingText;
            if (documentPagesText.TryGetValue(pageNumber, out existingText))
            {
                if (!string.IsNullOrEmpty(existingText))
                {
                    pageString += Environment.NewLine + existingText;
                }
            }
            documentPagesText[pageNumber] = pageString;
        }
    }

    static void WriteTextToFile(string output, Dictionary<int, string> documentPagesText)
    {
        Console.WriteLine("Writing text to: {0}", output);
        for (int i = 1; i <= documentPagesText.Keys.Max(); i++)
        {
            string pageText;
            if (documentPagesText.TryGetValue(i, out pageText))
            {
                File.AppendAllText(output, pageText + Environment.NewLine);
            }
        }
    }
    #endregion using AQUAFOREST
}