﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Keyoti.SearchEngine;
using Keyoti.SearchEngine.Index;
using Keyoti.SearchEngine.DataAccess.IndexableSourceRecords;
using Keyoti.SearchEngine.Search;
using System.Runtime.Remoting.Messaging;
using System.Web;
using WorkflowBLL;
using Aquaforest.OCR.Api;
using System.IO;

public class AsyncIndexer
{
    public static int debugPause;
    Queue<string> urlQueue = new Queue<string>();
    public int QueueSize { get { return urlQueue.Count; } }
    bool busy = false;
    static AsyncIndexer instance = null;
    string CustomData = "";

    private Ocr ocr;
    private PreProcessor preProcessor;

    string indexDirPath = HttpContext.Current.Server.MapPath(@"~/Uploads/" + HttpContext.Current.Request.Url.Segments[1].Replace("/", "") + "/IndexDirectory");

    public static AsyncIndexer GetInstance()
    {
        if (instance == null) instance = new AsyncIndexer();
        return instance;
    }

    delegate void IndexDelegate(Keyoti.SearchEngine.Configuration config, int opt);

    public void QueueForIndexing(string documentURL, int opt, string ContractInfo = "")
    {
        /* ContractInfo Parameter Contains # Seperated Extra information related to Contract Request whose file is going to be indexed.
           Sequence of parameters are - > RequestId#ContractId#RequestDate#ContractTypeId#Customer_SupplierId#Requester_Assigner#Assign_User#Assigned_Dept#ContractValue#SignatureDate */
        string strHTTPS = System.Configuration.ConfigurationManager.AppSettings["httpsPath"];
        if (strHTTPS == "https://")
            documentURL = documentURL.Replace("http://", "https://");

        Configuration configuration = new Configuration();
        configuration.IndexDirectory = indexDirPath;
        CustomData = ContractInfo;

        urlQueue.Enqueue(documentURL);
        IndexDelegate indexDelegate = new IndexDelegate(delegate(Keyoti.SearchEngine.Configuration configuration1, int opt1)
        {
            if (!busy)
            {
                busy = true;
                while (urlQueue.Count > 0)
                    ProcessQueueItems(configuration1, opt1);
                busy = false;
            }
        });
        //IAsyncResult ar = indexDelegate.BeginInvoke(configuration, new AsyncCallback(MyCallback), null);
        IAsyncResult ar = indexDelegate.BeginInvoke(configuration, opt, null, null);
    }

    public void BulkQueueForIndexing(string strUrl, List<ContractRequest> mylist, int opt)
    {
        string documentURL = "";
        string filePathpdf = "";
        string filePath = "";
        Configuration configuration = new Configuration();
        configuration.IndexDirectory = indexDirPath;

        foreach (ContractRequest files in mylist)
        {
            if (files.FileLoc == "contract")
            {
                if (files.isPDF != "Y")
                {
                    filePath = @"\Contract Documents\" + files.FileName + ".docx";
                    documentURL = strUrl + "/Contract Documents/" + files.FileName + ".docx";
                    urlQueue.Enqueue(documentURL);
                }

                if (files.docusign == "Y")
                {
                    filePathpdf = @"\Contract Documents\SignedDocuments\" + files.FileName + ".pdf";
                    documentURL = strUrl + "/Contract Documents/SignedDocuments/" + files.FileName + ".pdf";
                }
                else
                {
                    filePathpdf = @"\Contract Documents\PDF\" + files.FileName + ".pdf";
                    documentURL = strUrl + "/Contract Documents/PDF/" + files.FileName + ".pdf";
                }

                urlQueue.Enqueue(documentURL);
                IndexDelegate indexDelegate = new IndexDelegate(delegate(Keyoti.SearchEngine.Configuration configuration1, int opt1)
                {
                    if (!busy)
                    {
                        busy = true;
                        //this outer 'while' just takes care of anything added to the queue during the documentIndex.Close() call.
                        while (urlQueue.Count > 0)
                            ProcessQueueItems(configuration1, opt1);
                        busy = false;
                    }
                });
                IAsyncResult ar = indexDelegate.BeginInvoke(configuration, opt, null, null);
            }
            else if (files.FileLoc == "activity")
            {
                filePath = @"\ActivitiesDocuments\" + files.FileName;
            }
            else if (files.FileLoc == "supportdoc")
            {
                filePath = @"\ContractDocs\" + files.FileName;
            }

            if (files.isPDF != "Y")
            {
                System.IO.File.Delete(indexDirPath.Replace("\\IndexDirectory", filePath));
            }
            if (!string.IsNullOrEmpty(Convert.ToString(filePathpdf)))
                System.IO.File.Delete(indexDirPath.Replace("\\IndexDirectory", filePathpdf));

        }
    }

    void ProcessQueueItems(Keyoti.SearchEngine.Configuration configration, int opt)
    {
        DocumentIndex documentIndex = new DocumentIndex(configration);
        try
        {
            while (urlQueue.Count > 0)
            {
                configration.WordBreakingCharacters = @"*=()[]{}<>""\-@.'#";
                configration.WordNonBreakingCharacters = ",!$%^&+|;:?~`’”_/";
                configration.Logging = true;

                if (opt == 1)
                {
                    configration.CentralEventDispatcher.Action += new Keyoti.SearchEngine.Events.ActionEventHandler(CentralEventDispatcher_Action);
                    documentIndex.AddDocument(new Keyoti.SearchEngine.Documents.Document(urlQueue.Dequeue(), configration));
                }
                else
                {
                    documentIndex.RemoveDocument(new Keyoti.SearchEngine.Documents.Document(urlQueue.Dequeue(), configration));
                }
                //System.Threading.Thread.Sleep(debugPause);
            }
        }
        finally
        {
            documentIndex.Close();
        }
    }

    void MyCallback(IAsyncResult ar)
    {
        AsyncResult aResult = (AsyncResult)ar;
        IndexDelegate idxDelegate = (IndexDelegate)aResult.AsyncDelegate;
        idxDelegate.EndInvoke(ar);
    }

    void CentralEventDispatcher_Action(object sender, Keyoti.SearchEngine.Events.ActionEventArgs e)
    {
        //if (e.ActionData.Name == Keyoti.SearchEngine.Events.ActionName.ReadText)
        //{
        //    string documentUri = (sender as Keyoti.SearchEngine.Documents.Document).Uri.AbsoluteUri;

        //    (e.ActionData.Data as Keyoti.SearchEngine.Documents.DocumentText).MetaCustomData = CustomData;
        //}

        try
        {
            if (e.ActionData.Name == Keyoti.SearchEngine.Events.ActionName.ReadText)
            {
                //string documentUri = (sender as Keyoti.SearchEngine.Documents.Document).Uri.AbsoluteUri;
                (e.ActionData.Data as Keyoti.SearchEngine.Documents.DocumentText).MetaCustomData = CustomData;
            }
        }
        catch (Exception ex)
        {
            busy = false;
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }


    #region "OCR Code"
    public void ConvertOCRFile(string fileName, string extension, string UploadSection, string DirectoryName = "", string ParentDirectory = "")
    {
        string FolderName = "";
        switch (UploadSection.ToUpper())
        {
            case "ACTIVITY":
                FolderName = "/ActivitiesDocuments/ScannedActivitiesDocuments";
                break;
            case "CONTRACTDOCS":
                FolderName = "/ContractDocs/ScannedContractDocs";
                break;
            case "CONTRACT":
                FolderName = "/Contract Documents" + DirectoryName; //"/Contract Documents/ScannedContractDocuments";
                break;
            case "AI":
                FolderName = "/AINLP/PDF/ScannedPDF";
                break;
        }

        string sourceFile = "";
        try
        {
            string subPath = HttpContext.Current.Server.MapPath(@"~/Uploads/" + HttpContext.Current.Session["TenantDIR"] + FolderName);
            if (!Directory.Exists(subPath))
                Directory.CreateDirectory(subPath);
            sourceFile = Path.Combine(subPath, fileName + (UploadSection.ToUpper().Equals("AI") ? string.Empty : extension));
            SetupOcr();

            if (File.Exists(sourceFile))
            {
                if (extension.ToLower().Contains(".pdf"))
                {
                    ocr.ReadPDFSource(sourceFile);
                }

                ocr.EnablePdfOutput = true;
                ocr.EnableRtfOutput = true;
                ocr.EnableTextOutput = true;

                bool r = ocr.Recognize(preProcessor);

                if (r)
                {
                    SaveOcrOutput(fileName, UploadSection, ParentDirectory);
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    private void SaveOcrOutput(string fileName, string UploadSection, string ParentDirectory = "")
    {
        string FolderName = "";

        switch (UploadSection.ToUpper())
        {
            case "ACTIVITY":
                FolderName = "/ActivitiesDocuments/";
                break;
            case "CONTRACTDOCS":
                FolderName = "/ContractDocs/";
                break;
            case "CONTRACT":
                FolderName = "/Contract Documents" + ParentDirectory + "/";     //"/Contract Documents/ScannedContractDocuments";
                break;
            case "AI":
                FolderName = "/AINLP/PDF/";
                break;
        }

        string subPath = @"~/Uploads/" + HttpContext.Current.Session["TenantDIR"] + FolderName;
        Path.Combine(HttpContext.Current.Server.MapPath(subPath), fileName);
        ocr.SavePDFOutput(HttpContext.Current.Server.MapPath(subPath) + fileName + (UploadSection.ToUpper().Equals("AI") ? string.Empty : ".pdf"), false);

        ocr.DeleteTemporaryFiles();
    }

    private void SetupOcr()
    {
        ocr = new Ocr();

        //IMPORTANT: You need a license to run this ASP.NET sample.
        ocr.License = System.Configuration.ConfigurationManager.AppSettings["OCRLicenseKey"];

        ocr.EnableDebugOutput = 16383;
        ocr.Language = SupportedLanguages.English;

        //Enter Full Path because web applications run from the context of the web server
        string resourceFolder = HttpContext.Current.Server.MapPath(@"~/Aquaforest/OCRSDK/lib");

        string currentEnvironmentVariables = Environment.GetEnvironmentVariable("PATH");
        if (!currentEnvironmentVariables.Contains(resourceFolder))
        {
            Environment.SetEnvironmentVariable("PATH", currentEnvironmentVariables + ";" + resourceFolder);
        }
        ocr.ResourceFolder = resourceFolder;

        preProcessor = new PreProcessor();
        preProcessor.Autorotate = true;

        preProcessor.Deskew = true;
        preProcessor.NoPictures = false;
        preProcessor.Tables = true;
    }
    #endregion

    #region "PDF to WORD"
    public void ConvertPDFtoWORD(string fileName, string extension, string UploadSection, string DirectoryName = "", string ParentDirectory = "")
    {
        string FolderName = "";

        string sourceFile = "";
        try
        {
            string subPath = @"~/Uploads/" + HttpContext.Current.Session["TenantDIR"] + FolderName;
            sourceFile = Path.Combine(HttpContext.Current.Server.MapPath(subPath), fileName + extension);
            SetupOcr();

            if (File.Exists(sourceFile))
            {
                if (extension.ToLower().Contains(".pdf"))
                {
                    ocr.ReadPDFSource(sourceFile);
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
}

