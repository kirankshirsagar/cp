﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Web.UI;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
/// <summary>
/// Summary description for LoadUserControl
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class LoadUserControl : System.Web.Services.WebService
{

    public LoadUserControl()
    {


    }

    [WebMethod]
    public string RenderUC(string path)
    {
        Page pageHolder = new PageOverride();
        string[] ReqString = path.Split('?');
        string url = "", LeadId = "0";
        string querystring = "";
        if (ReqString.Length > 1)
        {
            url = ReqString[0];
            if (ReqString[1].Contains("="))
            {
                LeadId = ReqString[1].Split('&')[0].Split('=')[1];
            }

            querystring = ReqString[1];

        }
        else
        {
            url = path;
        }

        if (url != "")
        {
            UserControl viewControl = (UserControl)pageHolder.LoadControl(url);
            HiddenField hdnLeadId = new HiddenField();
            hdnLeadId.Value = LeadId;
            viewControl.Controls.AddAt(0, hdnLeadId);
            int i = 0;
            try
            {
                if (querystring != "")
                {
                    string[] strquery = querystring.Split('&');
                    for (; i < strquery.Length; i++)
                    {
                        if (i != 0)
                        {
                            string strQr = strquery[i];
                            string[] strq = strQr.Split('=');
                            HiddenField hdfield = new HiddenField();
                            hdfield.Value = strq[1];
                            viewControl.Controls.AddAt(i, hdfield);
                        }

                    }

                }
            }
            catch { }
            //     var form = new HtmlForm();

            HiddenField hdurl = new HiddenField();
            hdurl.Value = Convert.ToString(url.Substring(url.IndexOf("/")+1));
            viewControl.Controls.AddAt(i, hdurl);

            
            //      form.Controls.Add(viewControl);
            pageHolder.Controls.Add(viewControl);
            StringWriter output = new StringWriter();

            HttpContext.Current.Server.Execute(pageHolder, output, true);
            return output.ToString();
        }
        else
        {
            return "Error";
        }
        //trick to output exactly what you want (without wcf wrapping it)
        // return new MemoryStream(Encoding.UTF8.GetBytes(output.ToString()));
    }


    [WebMethod]
    public string RenderMyUC(string path)
    {

        const string STR_BeginRenderControlBlock = "<!-- BEGIN RENDERCONTROL BLOCK -->";
        const string STR_EndRenderControlBlock = "<!-- End RENDERCONTROL BLOCK -->";
        Page pageHolder = new PageOverride();
        string[] ReqString = path.Split('?');
        string url = "", LeadId = "0";
        string querystring = "";
        if (ReqString.Length > 1)
        {
            url = ReqString[0];
            if (ReqString[1].Contains("="))
            {
                LeadId = ReqString[1].Split('&')[0].Split('=')[1];
            }

            querystring = ReqString[1];

        }
        else
        {
            url = path;
        }
        if (url != "")
        {
            UserControl viewControl = (UserControl)pageHolder.LoadControl(url);
            HiddenField hdnLeadId = new HiddenField();
            hdnLeadId.Value = LeadId;
            viewControl.Controls.AddAt(0, hdnLeadId);
            try
            {
                if (querystring != "")
                {
                    string[] strquery = querystring.Split('&');
                    for (int i = 0; i < strquery.Length; i++)
                    {
                        if (i != 0)
                        {
                            string strQr = strquery[i];
                            string[] strq = strQr.Split('=');
                            HiddenField hdfield = new HiddenField();
                            hdfield.Value = strq[1];
                            viewControl.Controls.AddAt(i, hdfield);
                        }

                    }

                }
            }
            catch { }
            var form = new HtmlForm();
            form.Controls.Add(new LiteralControl(STR_BeginRenderControlBlock));
            form.Controls.Add(viewControl);
            form.Controls.Add(new LiteralControl(STR_EndRenderControlBlock));

            pageHolder.Controls.Add(form);
            StringWriter output = new StringWriter();

            HttpContext.Current.Server.Execute(pageHolder, output, true);

            string Html = output.ToString();

            // Strip out form and ViewState, Event Validation etc.  
            int at1 = Html.IndexOf(STR_BeginRenderControlBlock);
            int at2 = Html.IndexOf(STR_EndRenderControlBlock);
            if (at1 > -1 && at2 > at1)
            {
                Html = Html.Substring(at1 + STR_BeginRenderControlBlock.Length);
                Html = Html.Substring(0, at2 - at1 - STR_BeginRenderControlBlock.Length);
            }

            return Html; 

         //   return output.ToString();
        }
        else
        {
            return "Error";
        }
        //trick to output exactly what you want (without wcf wrapping it)
        // return new MemoryStream(Encoding.UTF8.GetBytes(output.ToString()));
    }

    [WebMethod]
    public Stream RenderUControl(string path)
    {
        Page pageHolder = new Page();
        UserControl viewControl = (UserControl)pageHolder.LoadControl(path);
        pageHolder.Controls.Add(viewControl);
        StringWriter output = new StringWriter();
        HttpContext.Current.Server.Execute(pageHolder, output, true);

        //trick to output exactly what you want (without wcf wrapping it)
        return new MemoryStream(Encoding.UTF8.GetBytes(output.ToString()));
    }

}


public class PageOverride : System.Web.UI.Page
{

    public override void VerifyRenderingInServerForm(System.Web.UI.Control control) { }

    //protected override void LoadViewState(object savedState)
    //{
    //    base.LoadViewState(savedState);
    //  //  CreateControl(ControlName);
    //}

}