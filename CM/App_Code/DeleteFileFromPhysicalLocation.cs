﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DeleteFileFromPhysicalLocation
/// </summary>
public class DeleteFileFromPhysicalLocation
{
	public DeleteFileFromPhysicalLocation()
	{
		
	}
    public static void DeleteFile(string FilePath)
    {
        string[] Files = FilePath.Split(',');
        for (int i = 0; i < Files.Length; i++)
        {
            try
            {
                System.IO.File.Delete(HttpContext.Current.Server.MapPath(Files[i]));
            }
            catch (Exception ex)
            {

            }
        }
    }
}