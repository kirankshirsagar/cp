﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Keyoti.SearchEngine;
using Keyoti.SearchEngine.Index;
using Keyoti.SearchEngine.DataAccess.IndexableSourceRecords;
using Keyoti.SearchEngine.Search;
using System.Runtime.Remoting.Messaging;
using System.Web;
using WorkflowBLL;
using Aquaforest.OCR.Api;
using System.IO;


public class AsyncIndexerProduction
{
    public static int debugPause;
    Queue<string> urlQueue = new Queue<string>();
    public int QueueSize { get { return urlQueue.Count; } }
    bool busy = false;
    static AsyncIndexerProduction instance = null;
    string CustomData = "";

    private Ocr ocr;
    private PreProcessor preProcessor;
    public static string Tenant { get; set; }

    string indexDirPath = HttpContext.Current.Server.MapPath(@"~/Uploads/" + Tenant + "/IndexDirectory");

    public static AsyncIndexerProduction GetInstance()
    {
        if (instance == null) instance = new AsyncIndexerProduction();
        return instance;
    }

    delegate void IndexDelegate(Keyoti.SearchEngine.Configuration config, int opt);

    public void QueueForIndexing(string documentURL, int opt, string ContractInfo = "")
    {
        /* ContractInfo Parameter Contains # Seperated Extra information related to Contract Request whose file is going to be indexed.
             Sequence of parameters are - > RequestId#ContractId#RequestDate#ContractTypeId#Customer_SupplierId#Requester_Assigner#Assign_User#Assigned_Dept#ContractValue#SignatureDate */
        string strHTTPS = System.Configuration.ConfigurationManager.AppSettings["httpsPath"];
        if (strHTTPS == "https://")
            documentURL = documentURL.Replace("http://", "https://");

        Configuration configuration = new Configuration();
        configuration.IndexDirectory = indexDirPath;
        //CustomData = ContractInfo;

        urlQueue.Enqueue(documentURL);
        File.AppendAllText(indexDirPath + "/QueueForIndexing.txt", "\n\n " + DateTime.Now + " Count- " + urlQueue.Count + " \n\nURL- " + documentURL);
        IndexDelegate indexDelegate = new IndexDelegate(delegate(Keyoti.SearchEngine.Configuration configuration1, int opt1)
        {
            if (!busy)
            {
                busy = true;
                while (urlQueue.Count > 0)
                    ProcessQueueItems(configuration1, opt1);
                busy = false;
            }
        });
        //IAsyncResult ar = indexDelegate.BeginInvoke(configuration, new AsyncCallback(MyCallback), null);
        IAsyncResult ar = indexDelegate.BeginInvoke(configuration, opt, null, null);
    }

    public void BulkQueueForIndexing(string strUrl, List<ContractRequest> mylist, int opt)
    {
        string documentURL = "";
        string filePathpdf = "";
        string filePath = "";
        Configuration configuration = new Configuration();
        configuration.IndexDirectory = indexDirPath;

        foreach (ContractRequest files in mylist)
        {
            if (files.FileLoc == "contract")
            {
                if (files.isPDF != "Y")
                {
                    filePath = @"\Contract Documents\" + files.FileName + ".docx";
                    documentURL = strUrl + "/Contract Documents/" + files.FileName + ".docx";
                    urlQueue.Enqueue(documentURL);
                }
               
                if (files.docusign == "Y")
                {
                    filePathpdf = @"\Contract Documents\SignedDocuments\" + files.FileName + ".pdf";
                    documentURL = strUrl + "/Contract Documents/SignedDocuments/" + files.FileName + ".pdf";
                }
                else
                {
                    filePathpdf = @"\Contract Documents\PDF\" + files.FileName + ".pdf";
                    documentURL = strUrl + "/Contract Documents/PDF/" + files.FileName + ".pdf";
                }

                urlQueue.Enqueue(documentURL);
                IndexDelegate indexDelegate = new IndexDelegate(delegate(Keyoti.SearchEngine.Configuration configuration1, int opt1)
                {
                    if (!busy)
                    {
                        busy = true;
                        //this outer 'while' just takes care of anything added to the queue during the documentIndex.Close() call.
                        while (urlQueue.Count > 0)
                            ProcessQueueItems(configuration1, opt1);
                        busy = false;
                    }
                });
                IAsyncResult ar = indexDelegate.BeginInvoke(configuration, opt, null, null);
            }
            else if (files.FileLoc == "activity")
            {
                filePath = @"\ActivitiesDocuments\" + files.FileName;
            }
            else if (files.FileLoc == "supportdoc")
            {
                filePath = @"\ContractDocs\" + files.FileName;
            }

            if (files.isPDF != "Y")
            {
                System.IO.File.Delete(indexDirPath.Replace("\\IndexDirectory", filePath));
            }
            if (!string.IsNullOrEmpty(Convert.ToString(filePathpdf)))
            System.IO.File.Delete(indexDirPath.Replace("\\IndexDirectory", filePathpdf));
           
        }
    }

    void ProcessQueueItems(Keyoti.SearchEngine.Configuration configration, int opt)
    {
        DocumentIndex documentIndex = new DocumentIndex(configration);        

        try
        {
            while (urlQueue.Count > 0)
            {
                string DQUrl=urlQueue.Dequeue();
                File.AppendAllText(indexDirPath + "/Dequeue.txt", "\n\n " + DateTime.Now + " Count- " + urlQueue.Count + " \n\nURL- " + DQUrl);
                string[] urldetails = Regex.Split(DQUrl, "##@@##");
                CustomData = urldetails[1];

                configration.WordBreakingCharacters = @"*=()[]{}<>""\-@.'#";
                configration.WordNonBreakingCharacters = ",!$%^&+|;:?~`’”_/";
                configration.Logging = true;

                if (opt == 1)
                {
                    configration.CentralEventDispatcher.Action += new Keyoti.SearchEngine.Events.ActionEventHandler(CentralEventDispatcher_Action);
                    documentIndex.AddDocument(new Keyoti.SearchEngine.Documents.Document(urldetails[0], configration));
                    File.AppendAllText(indexDirPath + "/Queue.txt", "\n\n " + DateTime.Now + " URL- " + urldetails[0]);
                }
                else if (opt == 2)
                {
                    documentIndex.Cancel();
                    documentIndex.Close();
                    File.AppendAllText(indexDirPath + "/QueueCount.txt", "\n\n " + DateTime.Now + " URL- " + urlQueue.Count);
                    break;
                }
                else
                {
                    documentIndex.RemoveDocument(new Keyoti.SearchEngine.Documents.Document(urldetails[0], configration));
                }
                //System.Threading.Thread.Sleep(debugPause);
            }
        }
        finally
        {
            documentIndex.Close();
        }
    }

    void MyCallback(IAsyncResult ar)
    {
        AsyncResult aResult = (AsyncResult)ar;
        IndexDelegate idxDelegate = (IndexDelegate)aResult.AsyncDelegate;
        idxDelegate.EndInvoke(ar);
    }

    void CentralEventDispatcher_Action(object sender, Keyoti.SearchEngine.Events.ActionEventArgs e)
    {
        try
        {
            if (e.ActionData.Name == Keyoti.SearchEngine.Events.ActionName.ReadText)
            {
                string documentUri = (sender as Keyoti.SearchEngine.Documents.Document).Uri.AbsoluteUri;
                File.AppendAllText(indexDirPath + "/IndexLog.txt", "\n\n " + DateTime.Now + " CentralEventDispatcher_Action Document URI- " + documentUri + " Custom Data: " + CustomData);

                (e.ActionData.Data as Keyoti.SearchEngine.Documents.DocumentText).MetaCustomData = CustomData;
            }
        }
        catch (Exception ex)
        {
            busy = false;
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            File.AppendAllText(indexDirPath + "/ErrorLog.txt", "\n\n " + DateTime.Now + " Error- " + ex.Message + " Custom Data: " + CustomData);
        }
    }


    #region "OCR Code"
    public void ConvertOCRFile(string fileName, string extension, string UploadSection, string DirectoryName = "", string ParentDirectory = "")
    {
        string FolderName = "";
        switch (UploadSection.ToUpper())
        {
            case "ACTIVITY":
                FolderName = "/ActivitiesDocuments/ScannedActivitiesDocuments";
                break;
            case "CONTRACTDOCS":
                FolderName = "/ContractDocs/ScannedContractDocs";
                break;
            case "CONTRACT":
                FolderName = "/Contract Documents" + DirectoryName; //"/Contract Documents/ScannedContractDocuments";
                break;
        }

        string sourceFile = "";
        try
        {
            string subPath = @"~/Uploads/" + HttpContext.Current.Session["TenantDIR"] + FolderName;
            sourceFile = Path.Combine(HttpContext.Current.Server.MapPath(subPath), fileName + extension);
            SetupOcr();

            if (File.Exists(sourceFile))
            {
                if (extension.ToLower().Contains(".pdf"))
                {
                    ocr.ReadPDFSource(sourceFile);
                }

                ocr.EnablePdfOutput = true;
                ocr.EnableRtfOutput = true;
                ocr.EnableTextOutput = true;

                bool r = ocr.Recognize(preProcessor);

                if (r)
                {
                    SaveOcrOutput(fileName, UploadSection, ParentDirectory);
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    private void SaveOcrOutput(string fileName, string UploadSection, string ParentDirectory = "")
    {
        string FolderName = "";

        switch (UploadSection.ToUpper())
        {
            case "ACTIVITY":
                FolderName = "/ActivitiesDocuments/";
                break;
            case "CONTRACTDOCS":
                FolderName = "/ContractDocs/";
                break;
            case "CONTRACT":
                FolderName = "/Contract Documents" + ParentDirectory + "/";     //"/Contract Documents/ScannedContractDocuments";
                break;
        }

        string subPath = @"~/Uploads/" + HttpContext.Current.Session["TenantDIR"] + FolderName;
        Path.Combine(HttpContext.Current.Server.MapPath(subPath), fileName);
        ocr.SavePDFOutput(HttpContext.Current.Server.MapPath(subPath) + fileName + ".pdf", false);

        ocr.DeleteTemporaryFiles();
    }

    private void SetupOcr()
    {
        ocr = new Ocr();

        //IMPORTANT: You need a license to run this ASP.NET sample.
        ocr.License = System.Configuration.ConfigurationManager.AppSettings["OCRLicenseKey"];

        ocr.EnableDebugOutput = 16383;
        ocr.Language = SupportedLanguages.English;

        //Enter Full Path because web applications run from the context of the web server
        string resourceFolder = HttpContext.Current.Server.MapPath(@"~/Aquaforest/OCRSDK/bin");

        string currentEnvironmentVariables = Environment.GetEnvironmentVariable("PATH");
        if (!currentEnvironmentVariables.Contains(resourceFolder))
        {
            Environment.SetEnvironmentVariable("PATH", currentEnvironmentVariables + ";" + resourceFolder);
        }
        ocr.ResourceFolder = resourceFolder;

        preProcessor = new PreProcessor();
        preProcessor.Autorotate = true;

        preProcessor.Deskew = true;
        preProcessor.NoPictures = false;
        preProcessor.Tables = true;
    }
    #endregion
}

