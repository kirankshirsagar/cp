﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Data;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using CommonBLL;
using WorkflowBLL;
using DocumentOperationsBLL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using com.valgut.libs.bots.Wit;
using com.valgut.libs.bots.Wit.Models;
using RestSharp;
using Aspose.Words;

public class ArtificialIntelligence
{
    private static readonly Encoding Encoding = Encoding.UTF8;
    private static readonly string url = ConfigurationManager.AppSettings["AIWebService"];
    static string AIFileName = "";
    static string IsPDF = "N";
    static string IsOCR = "N";

    static string ContractingParty = "";
    public static string ClientName = "";
    static string ContractType = "";
    public static string ClientAddress = "";
    public static string ClientAddressId = "";
    static string CountryName = "";

    static string flagTermnationforMaterialBreach = "N";
    static string flagTermnationforInsolvency = "N";
    static string flagTerminationforChangeofControl = "N";
    static string flagTerminationforConvenience = "N";

    static string flagThirdPartyGuarantee = "N";
    static string flagEntireAgreement = "N";
    static string flagStrictLiability = "N";
    static string flagChangeOfControl = "N";

    //static string Indemnity = "";
    static string Indemnity = "";
    static string Insurance = "";
    static string LiabilityCap = "Uncapped Liablity";
    static string Assignment = "";

    static string IsIndirectConsequentialLosses = "N";
    static string IndirectConsequentialLosses = "Uncapped Liablity";
    static string IsIndemnity = "N";
    static string IsWarranties = "N";
    static string Warranties = "";
    static string EntireAgreementClause = "";
    static string ThirdPartyGuaranteeRequired = "";
    static string IsForceMajeure = "N";
    static string ForceMajeure = "";
    static string IsSetOffRights = "N";
    static string SetOffRights = "";
    static string IsGoverningLaw = "N";
    static string GoverningLaw = "";

    static string EffectiveDate = "";
    static string ExpiryDate = "";
    static decimal AIFileSize = 0;
    static string OriginalAIPDFName = "";

    static string ErrorLog = HttpContext.Current.Server.MapPath(@"~/Uploads/CP/AIError.txt");

    public ArtificialIntelligence()
    {
        //
        // TODO: Add constructor logic here
        //
    }



    //#region Mapped on 22-Dec-17 for http://split2.contractpod.com/api/upload
    //private static void SetValues(dynamic data)
    //{
    //    if (data.knowledgeStudioEntity.ContractType != null)
    //    {
    //        string ctype = data.knowledgeStudioEntity.ContractType.Value;
    //        if (ctype.Contains(","))
    //        {
    //            ctype = ctype.Split(',')[0];
    //        }
    //        ContractType = ctype;
    //    }

    //    if (data.knowledgeStudioEntity.Party != null)
    //    {
    //        string[] partyname = data.knowledgeStudioEntity.Party.ToString().Replace("\r", "").Replace("\n", "").Split(',');

    //        ClientName = partyname[0];
    //        if (partyname.Length > 1)
    //            ClientName = partyname[1];
    //    }

    //    if (data.knowledgeStudioEntity.Address != null)
    //    {
    //        ClientAddress = data.knowledgeStudioEntity.Address.Value;
    //    }
    //    if (data.knowledgeStudioEntity.CountryName != null)
    //    {
    //        CountryName = data.knowledgeStudioEntity.CountryName.Value;
    //    }

    //    if (data.knowledgeStudioEntity.StrictLiability != null)
    //    {
    //        if (!string.IsNullOrEmpty(data.knowledgeStudioEntity.StrictLiability.Value))
    //        {
    //            flagStrictLiability = "Y";
    //        }
    //    }       

    //    if (data.knowledgeStudioRelation.Termination_coc != null)
    //    {
    //        if (!string.IsNullOrEmpty(data.knowledgeStudioRelation.Termination_coc.Value))
    //        {
    //            flagChangeOfControl = "Y";
    //        }
    //    }       

    //    if (data.knowledgeStudioRelation.Termination_coc != null)
    //    {
    //        if (!string.IsNullOrEmpty(data.knowledgeStudioRelation.Termination_coc.Value))
    //        {
    //            flagTerminationforChangeofControl = "Y";
    //        }
    //    }

    //    if (data.knowledgeStudioRelation.Termination_Breach != null)
    //    {
    //        if (!string.IsNullOrEmpty(data.knowledgeStudioRelation.Termination_Breach.Value))
    //        {
    //            flagTermnationforMaterialBreach = "Y";
    //        }
    //    }

    //    if (data.knowledgeStudioRelation.Termination_Convenience != null)
    //    {
    //        if (!string.IsNullOrEmpty(data.knowledgeStudioRelation.Termination_Convenience.Value))
    //        {
    //            flagTerminationforConvenience = "Y";
    //        }
    //    }

    //    if (data.knowledgeStudioEntity.Insolvency != null)
    //     {
    //         if (!string.IsNullOrEmpty(data.knowledgeStudioEntity.Insolvency.Value))
    //         {
    //             flagTermnationforInsolvency = "Y";
    //         }
    //     }

    //    WitClient client = new WitClient("LPTIVMCVS2SWHXKOH5ZNWKETM6UNHVBI");
    //    //Message message = client.GetMessage("the sixteenth day of January two thousand and nine");

    //    //if (data.knowledgeStudioEntity.Date != null)
    //    if (data.knowledgeStudioEntity.AgreementDate != null)
    //    {
    //        Message message = client.GetMessage(data.knowledgeStudioEntity.AgreementDate.Value);
    //        EffectiveDate = ExtractDate(message);
    //    }

    //    if (data.knowledgeStudioEntity.ExpiryDate != null)
    //    {
    //        Message message = client.GetMessage(data.knowledgeStudioEntity.ExpiryDate.Value);
    //        //ExpiryDate = message._text;
    //        ExpiryDate = ExtractDate(message);
    //    }

    //    if (data.Liability != null)
    //    {
    //        if (!string.IsNullOrEmpty(data.Liability.Value))
    //        {
    //            LiabilityCap = data.Liability.Value.Trim().Replace("'", "");
    //        }
    //    }
    //    if (data.Liability == null || string.IsNullOrEmpty(data.Liability.Value))
    //    {
    //        if (data.knowledgeStudioRelation.Liability != null)
    //        {
    //            if (!string.IsNullOrEmpty(data.knowledgeStudioRelation.Liability.Value))
    //            {
    //                LiabilityCap = data.knowledgeStudioRelation.Liability.Value.Trim().Replace("'", "");
    //            }
    //        }
    //    }

    //    #region Insurance
    //    // For Insurance
    //    if (data.knowledgeStudioEntity.GeneralLiability != null)
    //    {
    //        if (!string.IsNullOrEmpty(data.knowledgeStudioEntity.GeneralLiability.Value))
    //        {
    //            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudioEntity.GeneralLiability.Value : data.knowledgeStudioEntity.GeneralLiability.Value;
    //        }
    //    }
    //    if (data.knowledgeStudioEntity.OtherInsurance != null)
    //    {
    //        if (!string.IsNullOrEmpty(data.knowledgeStudioEntity.OtherInsurance.Value))
    //        {
    //            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudioEntity.OtherInsurance.Value : data.knowledgeStudioEntity.OtherInsurance.Value;
    //        }
    //    }

    //    if (data.knowledgeStudioEntity.WaiverofSubrogation != null)
    //    {
    //        if (!string.IsNullOrEmpty(data.knowledgeStudioEntity.WaiverofSubrogation.Value))
    //        {
    //            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudioEntity.WaiverofSubrogation.Value : data.knowledgeStudioEntity.WaiverofSubrogation.Value;
    //        }
    //    }
    //    if (data.knowledgeStudioEntity.ProfessionalLiability != null)
    //    {
    //        if (!string.IsNullOrEmpty(data.knowledgeStudioEntity.ProfessionalLiability.Value))
    //        {
    //            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudioEntity.ProfessionalLiability.Value : data.knowledgeStudioEntity.ProfessionalLiability.Value;
    //        }
    //    }
    //    if (data.knowledgeStudioEntity.EmployerLiability != null)
    //    {
    //        if (!string.IsNullOrEmpty(data.knowledgeStudioEntity.EmployerLiability.Value))
    //        {
    //            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudioEntity.EmployerLiability.Value : data.knowledgeStudioEntity.EmployerLiability.Value;
    //        }
    //    }
    //    if (data.knowledgeStudioEntity.WorkersCompensation != null)
    //    {
    //        if (!string.IsNullOrEmpty(data.knowledgeStudioEntity.WorkersCompensation.Value))
    //        {
    //            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudioEntity.WorkersCompensation.Value : data.knowledgeStudioEntity.WorkersCompensation.Value;
    //        }
    //    }
    //    if (data.knowledgeStudioEntity.AutomobileLiability != null)
    //    {
    //        if (!string.IsNullOrEmpty(data.knowledgeStudioEntity.AutomobileLiability.Value))
    //        {
    //            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudioEntity.AutomobileLiability.Value : data.knowledgeStudioEntity.AutomobileLiability.Value;
    //        }
    //    }
    //    #endregion  Insurance

    //    //---------------Assignment-------------
    //    if (data.knowledgeStudioRelation.Assignment != null)
    //    {
    //        Assignment = data.knowledgeStudioRelation.Assignment.Value;
    //    }
    //    if (string.IsNullOrEmpty(Assignment))
    //    {
    //        Assignment = data.Assignment.Value.Trim().Replace("'", "");
    //    }

    //    //---------------Thirdparty Guarantee-------------
    //    if (data.ThirdPartyGuarantee != null)
    //    {
    //        ThirdPartyGuaranteeRequired = data.ThirdPartyGuarantee.Value;
    //    }
    //    if (string.IsNullOrEmpty(ThirdPartyGuaranteeRequired) && data.knowledgeStudioEntity.ThirdPartyGuarantee != null)
    //    {
    //        ThirdPartyGuaranteeRequired = data.knowledgeStudioEntity.ThirdPartyGuarantee.Value;
    //    }
    //    if (!string.IsNullOrEmpty(ThirdPartyGuaranteeRequired))
    //    {
    //        flagThirdPartyGuarantee = "Y";
    //    }

    //    //---------------Indemnity-------------
    //    if (data.Indemnity != null)
    //    {
    //        Indemnity = "The agreement contains Indemnity provision";
    //    }
    //    if (string.IsNullOrEmpty(Indemnity) && data.knowledgeStudioEntity.Indemnity != null)
    //    {
    //        Indemnity = "The agreement contains Indemnity provision";
    //    }
    //    if (!string.IsNullOrEmpty(Indemnity))
    //    {
    //        IsIndemnity = "Y";
    //    }

    //    //phase 2 fields
    //    //---------------SetOffClause-------------
    //    if (data.knowledgeStudioRelation.SetOffClause != null)
    //    {
    //        SetOffRights = data.knowledgeStudioRelation.SetOffClause.Value;
    //    }
    //    if (string.IsNullOrEmpty(SetOffRights) && data.SetOffClause != null)
    //    {
    //        SetOffRights = data.SetOffClause.Value.Trim().Replace("'", "");
    //    }
    //    if (!string.IsNullOrEmpty(SetOffRights))
    //    {
    //        IsSetOffRights = "Y";
    //    }

    //    //---------------ForceMajeure-------------
    //    if (data.knowledgeStudioRelation.ForceMajeure != null)
    //    {
    //        ForceMajeure = data.knowledgeStudioRelation.ForceMajeure.Value;
    //    }
    //    if (string.IsNullOrEmpty(ForceMajeure) && data.ForceMajeure != null)
    //    {
    //        ForceMajeure = data.ForceMajeure.Value.Trim().Replace("'", "");
    //    }
    //    if (!string.IsNullOrEmpty(ForceMajeure))
    //    {
    //        IsForceMajeure = "Y";
    //    }

    //    //---------------Warranties-------------
    //    /*if (data.knowledgeStudioRelation.Warranties != null)
    //    {
    //        Warranties = data.knowledgeStudioRelation.Warranties.Value;
    //    }
    //    if (string.IsNullOrEmpty(Warranties) && data.Warranties != null)
    //    {
    //        Warranties = data.Warranties.Value.Trim().Replace("'", "");
    //    }
    //    if (!string.IsNullOrEmpty(Warranties))
    //    {
    //        IsWarranties = "Y";
    //    }*/

    //    //---------------ConsequentialLoss-------------
    //    if (data.knowledgeStudioRelation.ConsequentialLoss != null)
    //    {
    //        IndirectConsequentialLosses = data.knowledgeStudioRelation.ConsequentialLoss.Value;
    //    }
    //    if (string.IsNullOrEmpty(IndirectConsequentialLosses) && data.ConsequentialLoss != null)
    //    {
    //        IndirectConsequentialLosses = data.ConsequentialLoss.Value.Trim().Replace("'", "");
    //    }
    //    if (!string.IsNullOrEmpty(IndirectConsequentialLosses))
    //    {
    //        IsIndirectConsequentialLosses = "Y";
    //    }
    //    else
    //        IndirectConsequentialLosses = "Uncapped Liabilty";


    //    //---------------GoverningLaw-------------
    //    if (data.knowledgeStudioRelation.GoverningLaw != null)
    //    {
    //        GoverningLaw = data.knowledgeStudioRelation.GoverningLaw.Value;
    //    }
    //    if (string.IsNullOrEmpty(GoverningLaw) && data.GoverningLaw != null)
    //    {
    //        GoverningLaw = data.GoverningLaw.Value.Trim().Replace("'", "");
    //    }
    //    if (!string.IsNullOrEmpty(GoverningLaw))
    //    {
    //        IsGoverningLaw = "Y";
    //    }

    //    //----------------Entire Agreement -------------
    //    if (data.knowledgeStudioRelation.EntireAgreement != null)
    //    {
    //        EntireAgreementClause = data.knowledgeStudioRelation.EntireAgreement.Value;
    //    }
    //    if (string.IsNullOrEmpty(EntireAgreementClause) && data.EntireAgreement != null)
    //    {
    //        EntireAgreementClause = data.EntireAgreement.Value.Trim().Replace("'", "");
    //    }
    //    if (!string.IsNullOrEmpty(EntireAgreementClause))
    //    {
    //        flagEntireAgreement = "Y";
    //    }

    //    //----------------Assignment -------------
    //    if (data.knowledgeStudioRelation.Assignment != null)
    //    {
    //        Assignment = data.knowledgeStudioRelation.Assignment.Value;
    //    }
    //    if (string.IsNullOrEmpty(Assignment))
    //    {
    //        Assignment = data.Assignment.Value.Trim().Replace("'", "");
    //    }
    //}
    //#endregion

    #region Commented on 22-Dec-17 for http://split2.contractpod.com/api/upload
    //http://49.248.112.163/cp2/api/upload
    private static void SetValues(dynamic data)
    {
        //if (data.knowledgeStudio.contracttype != null)
        if (data.knowledgeStudioEntity.ctype != null)
        {
            string ctype = data.knowledgeStudioEntity.ctype.Value;
            if (ctype.Contains(","))
            {
                ctype = ctype.Split(',')[0];
            }
            ContractType = ctype;
        }

        if (data.knowledgeStudioEntity.contractingparty != null)
        {
            ContractingParty = data.knowledgeStudioEntity.contractingparty.Value;
        }

        if (data.knowledgeStudioEntity.thirdpartyname != null)
        {
            //ClientName = data.knowledgeStudioEntity.thirdpartyname.Value;
            string[] partyname = data.knowledgeStudioEntity.thirdpartyname.ToString().Replace("\r", "").Replace("\n", "").Split(',');
            ClientName = partyname[0].Trim();

            if (partyname.Length > 1)
                ClientName = partyname[1].Trim();
        }

        if (data.knowledgeStudioEntity.thirdpartyaddress != null)
        {
            ClientAddress = data.knowledgeStudioEntity.thirdpartyaddress.Value;
        }
        if (data.knowledgeStudioEntity.countryname != null)
        {
            CountryName = data.knowledgeStudioEntity.countryname.Value;
        }

        if (data.knowledgeStudioEntity.strictliability != null)
        {
            flagStrictLiability = "Y";
        }

        if (data.knowledgeStudioEntity.thirdpartyguarantee != null)
        {
            flagThirdPartyGuarantee = "Y";
        }

        //if (data.knowledgeStudio.changeofcontrol != null) // Debayan requested to change 
        //if (data.knowledgeStudio.terminationforchangeofcontrol != null)//changed again
        if (data.knowledgeStudioRelation.termination_coc != null)
        {
            flagChangeOfControl = "Y";
        }

        //if (data.knowledgeStudio.terminationforinsolvency != null)//changed 
        //if (data.knowledgeStudio.insolvency != null)//changed again
        if (data.knowledgeStudioRelation.termination_insolvecy != null)
        {
            flagTermnationforInsolvency = "Y";
        }

        //if (data.knowledgeStudio.terminationforchangeofcontrol != null)//changed again
        if (data.knowledgeStudioRelation.termination_coc != null)
        {
            flagTerminationforChangeofControl = "Y";
        }

        //if (data.knowledgeStudio.terminationformaterialbreach != null)//changed 
        //if (data.knowledgeStudio.termnationformaterialbreach != null)//changed again
        //if (data.knowledgeStudio.termination_breach != null)
        if (data.knowledgeStudioRelation.termination_breach != null)
        {
            flagTermnationforMaterialBreach = "Y";
        }

        //if (data.knowledgeStudio.terminationforconvenience != null)//changed 
        //if (data.knowledgeStudio.termination_convenience != null)
        if (data.knowledgeStudioRelation.termination_convenience != null)
        {
            flagTerminationforConvenience = "Y";
        }

        WitClient client = new WitClient("LPTIVMCVS2SWHXKOH5ZNWKETM6UNHVBI");
        //Message message = client.GetMessage("the sixteenth day of January two thousand and nine");

        //if (data.knowledgeStudio.effectivedate != null)
        if (data.knowledgeStudioEntity.date != null)
        {
            //EffectiveDate = data.knowledgeStudio.effectivedate.Value;
            Message message = client.GetMessage(data.knowledgeStudioEntity.date.Value);
            EffectiveDate = ExtractDate(message);
        }

        if (data.knowledgeStudioEntity.expirydate != null)
        {
            //ExpiryDate = data.knowledgeStudio.expirydate.Value;
            Message message = client.GetMessage(data.knowledgeStudioEntity.expirydate.Value);
            ExpiryDate = message._text;
        }


        if (data.liability != null)
        {
            if (!string.IsNullOrEmpty(data.liability.Value))
            {
                LiabilityCap = data.liability.Value.Trim().Replace("'", "");
            }
        }
        if (data.liability == null || string.IsNullOrEmpty(data.liability.Value))
        {
            if (data.knowledgeStudioRelation.liabilitycap != null)
            {
                LiabilityCap = data.knowledgeStudioRelation.liabilitycap.Value.Trim().Replace("'", "");
            }
        }

        #region Insurance
        // For Insurance
        if (data.knowledgeStudioEntity.generalliability != null)
        {
            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudioEntity.generalliability.Value : data.knowledgeStudioEntity.generalliability.Value;
        }
        if (data.knowledgeStudioEntity.otherinsurance != null)
        {
            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudioEntity.otherinsurance.Value : data.knowledgeStudioEntity.otherinsurance.Value;
        }

        if (data.knowledgeStudioEntity.waiverofsubrogation != null)
        {
            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudioEntity.waiverofsubrogation.Value : data.knowledgeStudioEntity.waiverofsubrogation.Value;
        }
        if (data.knowledgeStudioEntity.professionalliability != null)
        {
            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudioEntity.professionalliability.Value : data.knowledgeStudioEntity.professionalliability.Value;
        }
        if (data.knowledgeStudioEntity.employerliability != null)
        {
            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudioEntity.employerliability.Value : data.knowledgeStudioEntity.employerliability.Value;
        }
        if (data.knowledgeStudioEntity.workerscompensation != null)
        {
            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudioEntity.workerscompensation.Value : data.knowledgeStudioEntity.workerscompensation.Value;
        }
        if (data.knowledgeStudioEntity.automobileliability != null)
        {
            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudioEntity.automobileliability.Value : data.knowledgeStudioEntity.automobileliability.Value;
        }
        #endregion  Insurance

        //Indemnity = data.indemnity.Value.Trim().Replace("'", "");
        //Insurance = data.insurance.Value.Trim().Replace("'", "");
        //LiabilityCap = data.liability.Value.Trim().Replace("'", "");     


        //---------------Assignment-------------
        if (data.knowledgeStudioRelation.assignment != null)
        {
            Assignment = data.knowledgeStudioRelation.assignment.Value;
        }
        if (string.IsNullOrEmpty(Assignment) && data.assignment != null)
        {
            Assignment = data.assignment.Value.Trim().Replace("'", "");
        }

        //---------------Thirdparty Guarantee-------------
        if (data.thirdpartyguarantee != null)
        {
            ThirdPartyGuaranteeRequired = data.thirdpartyguarantee.Value;
        }
        if (string.IsNullOrEmpty(ThirdPartyGuaranteeRequired) && data.knowledgeStudioEntity.thirdpartyguarantee != null)
        {
            ThirdPartyGuaranteeRequired = data.knowledgeStudioEntity.thirdpartyguarantee.Value;
        }
        if (!string.IsNullOrEmpty(ThirdPartyGuaranteeRequired))
        {
            flagThirdPartyGuarantee = "Y";
        }

        //---------------Indemnity-------------
        if (data.any != null)
        {
            //Indemnity = data.any.Value; 
            Indemnity = "The agreement contains Indemnity provision";
        }
        if (string.IsNullOrEmpty(Indemnity) && data.knowledgeStudioEntity.indemnity != null)
        {
            Indemnity = "The agreement contains Indemnity provision";
        }
        if (!string.IsNullOrEmpty(Indemnity))
        {
            IsIndemnity = "Y";
        }

        //phase 2 fields
        //---------------SetOffClause-------------
        if (data.knowledgeStudioRelation.setoffrights != null)
        {
            SetOffRights = data.knowledgeStudioRelation.setoffrights.Value;
        }
        if (string.IsNullOrEmpty(SetOffRights) && data.SetOffClause != null)
        {
            SetOffRights = data.SetOffClause.Value.Trim().Replace("'", "");
        }
        if (!string.IsNullOrEmpty(SetOffRights))
        {
            IsSetOffRights = "Y";
        }

        //---------------ForceMajeure-------------
        if (data.knowledgeStudioRelation.forcemajeure != null)
        {
            ForceMajeure = data.knowledgeStudioRelation.forcemajeure.Value;
        }
        if (string.IsNullOrEmpty(ForceMajeure) && data.forcemajeure != null)
        {
            ForceMajeure = data.forcemajeure.Value.Trim().Replace("'", "");
        }
        if (!string.IsNullOrEmpty(ForceMajeure))
        {
            IsForceMajeure = "Y";
        }

        //---------------Warranties-------------
        /*if (data.knowledgeStudioRelation.Warranties != null)
        {
            Warranties = data.knowledgeStudioRelation.Warranties.Value;
        }
        if (string.IsNullOrEmpty(Warranties) && data.Warranties != null)
        {
            Warranties = data.Warranties.Value.Trim().Replace("'", "");
        }
        if (!string.IsNullOrEmpty(Warranties))
        {
            IsWarranties = "Y";
        }*/

        //---------------ConsequentialLoss-------------
        if (data.knowledgeStudioRelation.consequentialdamages != null)
        {
            IndirectConsequentialLosses = data.knowledgeStudioRelation.consequentialdamages.Value;
        }
        if (string.IsNullOrEmpty(IndirectConsequentialLosses) && data.ConsequentialLoss != null)
        {
            IndirectConsequentialLosses = data.ConsequentialLoss.Value.Trim().Replace("'", "");
        }
        if (!string.IsNullOrEmpty(IndirectConsequentialLosses))
        {
            IsIndirectConsequentialLosses = "Y";
        }

        //---------------GoverningLaw-------------
        if (data.knowledgeStudioRelation.glaw != null)
        {
            GoverningLaw = data.knowledgeStudioRelation.glaw.Value;
        }
        if (string.IsNullOrEmpty(GoverningLaw) && data.GoverningLaw != null)
        {
            GoverningLaw = data.GoverningLaw.Value.Trim().Replace("'", "");
        }
        if (!string.IsNullOrEmpty(GoverningLaw))
        {
            IsGoverningLaw = "Y";
        }

        //----------------Entire Agreement -------------
        if (data.knowledgeStudioRelation.entireagreement != null)
        {
            EntireAgreementClause = data.knowledgeStudioRelation.entireagreement.Value;
        }
        if (string.IsNullOrEmpty(EntireAgreementClause) && data.insurance != null)
        {
            EntireAgreementClause = data.insurance.Value.Trim().Replace("'", "");
        }
        if (!string.IsNullOrEmpty(EntireAgreementClause))
        {
            flagEntireAgreement = "Y";
        }

        if (data.knowledgeStudioRelation.assignment != null)
        {
            Assignment = data.knowledgeStudioRelation.assignment.Value;
        }
        if (string.IsNullOrEmpty(Assignment))
        {
            Assignment = data.assignment.Value.Trim().Replace("'", "");
        }
    }
    #endregion

    private static string ExtractDate(Message message)
    {
        string date = "", day = "", month = "", year = "";

        Dictionary<string, List<com.valgut.libs.bots.Wit.Models.Entity>> dict = message.entities;
        try
        {
            List<com.valgut.libs.bots.Wit.Models.Entity> DateCapture = dict["date_capture"].ToList();
            day = DateCapture[0].value.ToString();

            DateCapture = dict["month_capture"].ToList();
            month = DateCapture[0].value.ToString();

            DateCapture = dict["year_capture"].ToList();
            year = DateCapture[0].value.ToString();
            date = day + "-" + month + "-" + year;
        }
        catch (Exception ex)
        {
            date = "";
        }
        return date;
    }

    public static string SendDocumentRequest(string FilePath)
    {
        // Reference: https://briangrinstead.com/blog/multipart-form-post-in-c/
        var webRequest = WebRequest.Create(url);
        webRequest.Method = "POST";

        // form data formatting

        string boundary = string.Format("----------{0:N}", Guid.NewGuid());
        webRequest.ContentType = "multipart/form-data; boundary=" + boundary;

        // define content boundary
        Stream formDataStream = new MemoryStream();
        var fileName = FilePath.Substring(FilePath.LastIndexOf('\\') + 1);
        string header = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"document\"; filename=\"{1}\"\r\nContent-Type: application/octet-stream\r\n\r\n",
            boundary, fileName);

        formDataStream.Write(Encoding.GetBytes(header), 0, Encoding.GetByteCount(header));

        // write the file contents to the form stream in between boundaries
        var fileBytes = File.ReadAllBytes(FilePath);
        formDataStream.Write(fileBytes, 0, fileBytes.Length);

        // indicate end of file boundary
        string footer = "\r\n--" + boundary + "--\r\n";
        formDataStream.Write(Encoding.GetBytes(footer), 0, Encoding.GetByteCount(footer));

        // output the formDataStream into a byte[] to be sent with the request
        formDataStream.Position = 0;
        byte[] formData = new byte[formDataStream.Length];
        formDataStream.Read(formData, 0, formData.Length);
        formDataStream.Close();

        // send the form data in the request to the upload API
        using (var requestStream = webRequest.GetRequestStream())
        {
            requestStream.Write(formData, 0, formData.Length);
            requestStream.Flush();
            requestStream.Close();
        }

        // read response from API server
        using (var responseStream = webRequest.GetResponse().GetResponseStream())
        {
            using (var reader = new StreamReader(responseStream))
            {
                var content = reader.ReadToEnd();
                return content;
            }
        }
    }

    static void ResetValues()
    {
        IsPDF = "N";
        IsOCR = "N";
        AIFileName = "";
        ContractingParty = "";
        ClientName = "";
        ContractType = "";
        ClientAddress = "";
        CountryName = "";

        flagTermnationforMaterialBreach = "N";
        flagTermnationforInsolvency = "N";
        flagTerminationforChangeofControl = "N";
        flagTerminationforConvenience = "N";

        flagThirdPartyGuarantee = "N";
        flagEntireAgreement = "N";
        flagStrictLiability = "N";
        flagChangeOfControl = "N";

        Indemnity = "";
        Insurance = "";
        LiabilityCap = "Uncapped Liablity";
        Assignment = "";

        IsIndirectConsequentialLosses = "N";
        IndirectConsequentialLosses = "";
        IsIndemnity = "N";
        IsWarranties = "N";
        Warranties = "";
        EntireAgreementClause = "";
        ThirdPartyGuaranteeRequired = "";
        IsForceMajeure = "N";
        ForceMajeure = "";
        IsSetOffRights = "N";
        SetOffRights = "";
        IsGoverningLaw = "N";
        GoverningLaw = "";

        EffectiveDate = "";
        ExpiryDate = "";
        AIFileSize = 0;
    }

    private static DataTable MetaData()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("MetaDataFieldId", Type.GetType("System.Int32"));
        dt.Columns.Add("FieldValue", Type.GetType("System.String"));
        return dt;
    }

    public static string GetClientName(string FilePath)
    {
        var response = SendDocumentRequest(FilePath);

        JavaScriptSerializer jss = new JavaScriptSerializer();
        string v = (string)jss.Deserialize(response, typeof(string));
        dynamic data = JObject.Parse(v);
        string det = "";

        if (data.knowledgeStudioEntity.thirdpartyname != null)
        {
            string[] partyname = data.knowledgeStudioEntity.thirdpartyname.ToString().Replace("\r", "").Replace("\n", "").Split(',');
            ClientName = partyname[0].Trim();

            if (partyname.Length > 1)
                ClientName = partyname[1].Trim();

            if (!string.IsNullOrEmpty(ClientName))
                det = ClientName;
        }

        if (data.knowledgeStudioEntity.thirdpartyaddress != null)
        {
            ClientAddress = data.knowledgeStudioEntity.thirdpartyaddress.Value.Trim();
            if (!string.IsNullOrEmpty(ClientAddress))
                det += "@@##@@" + ClientAddress;
        }
        if (data.knowledgeStudioEntity.countryname != null)
        {
            CountryName = data.knowledgeStudioEntity.countryname.Value.Trim();
            if (!string.IsNullOrEmpty(CountryName))
                det += "@@##@@" + CountryName;
        }
        if (ClientName.Length > 3)
            ClientName = ClientName.Substring(0, 3);

        IContractRequest objcontractreq = new ContractRequest();
        objcontractreq.ClientName = ClientName;
        string IsClientExist = objcontractreq.IsClientExist();

        if (IsClientExist.Equals("N"))
            det = string.Empty;
        return det;
    }

    public static string ReadAndExtract(string FilePath, bool IsRequestInsert, bool IsMetaDataUpdate, int RequestId, string IsPDFUpload)
    {
        string STATUS = "";
        IsPDF = IsPDFUpload;
        try
        {
            var response = SendDocumentRequest(FilePath);

            JavaScriptSerializer jss = new JavaScriptSerializer();
            string v = (string)jss.Deserialize(response, typeof(string));
            dynamic data = JObject.Parse(v);

            SetValues(data);
            AIFileName = FilePath;
            string ContractFileName = "";

            if (RequestId != null && RequestId < 0)
            {
                RequestId = 0;
            }

            if (IsRequestInsert)
            {
                FileInfo fi = new FileInfo(AIFileName);
                AIFileSize = Math.Round(Convert.ToDecimal(fi.Length) / 1024, 2);

                string retVal = ContractRequestInsert(FilePath);
                RequestId = Convert.ToInt32(System.Text.RegularExpressions.Regex.Split(retVal, ",")[0]);
                ContractFileName = System.Text.RegularExpressions.Regex.Split(retVal, ",")[1];
                GenerateContractVersion(ContractFileName, RequestId);
            }
            if (IsMetaDataUpdate)
            {
                if (RequestId > 0)
                {
                    string KOStatus = KeyObligationUpdate(RequestId);
                    string IDStatus = ImportantDatesUpdate(RequestId);
                }
            }
            STATUS = Convert.ToString(RequestId);
        }
        catch (Exception ex)
        {
            STATUS = "";
            File.AppendAllText(ErrorLog, "\n\n " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm") + " " + ex.Message);
        }
        finally
        {
            ResetValues();
        }
        return STATUS;
    }

    static string ContractRequestInsert(string FilePath)
    {
        string RequestId = "", ContractFileName = "";
        string ExtractFileName = FilePath.Substring(FilePath.LastIndexOf("\\") + 1);
        OriginalAIPDFName = ExtractFileName.Replace(ExtractFileName.Substring(ExtractFileName.LastIndexOf("_"), ExtractFileName.LastIndexOf(".") - ExtractFileName.LastIndexOf("_")), "");

        if (IsPDF.Equals("Y"))
        {
            string extn = ExtractFileName.Substring(ExtractFileName.LastIndexOf("."));
            OriginalAIPDFName = ExtractFileName.Replace(extn, ".pdf");
        }

        IContractRequest objcontractreq = new ContractRequest();
        objcontractreq.ContractTypeName = ContractType;
        objcontractreq.ClientName = ClientName;
        objcontractreq.AddressID = string.IsNullOrEmpty(ClientAddressId) ? 0 : Convert.ToInt32(ClientAddressId);
        objcontractreq.Street1 = ClientAddress;
        objcontractreq.CountryName = CountryName;
        objcontractreq.UserID = Convert.ToInt32(HttpContext.Current.Session[Declarations.User]);
        objcontractreq.DepartmentId = Convert.ToInt32(HttpContext.Current.Session[Declarations.Department]);
        objcontractreq.ContractingpartyName = ContractingParty;
        objcontractreq.IpAddress = HttpContext.Current.Session[Declarations.IP] != null ? HttpContext.Current.Session[Declarations.IP].ToString() : null;
        objcontractreq.AIFileSize = AIFileSize;
        objcontractreq.isPDF = IsPDF;
        //objcontractreq.IsOCR = "";
        objcontractreq.AIUpdatedFileName = ExtractFileName;
        objcontractreq.AIOriginalFileName = OriginalAIPDFName;
        try
        {
            objcontractreq.RequestID = 0;
            string retVal = objcontractreq.AIInsertRecord();
            RequestId = System.Text.RegularExpressions.Regex.Split(retVal, ",")[0];
            ContractFileName = System.Text.RegularExpressions.Regex.Split(retVal, ",")[1];
            return retVal;
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            return "";
        }
    }

    static string KeyObligationUpdate(int RequestId)
    {
        try
        {
            IKeyFields kf = new KeyFeilds();
            kf.RequestId = RequestId;
            kf.Indemnity = Indemnity;
            kf.LiabilityCap = LiabilityCap;
            kf.Insurance = Insurance;
            kf.Indemnity = Indemnity;
            kf.AssignmentNovation = Assignment;
            kf.IsTerminationConvenience = flagTerminationforConvenience;
            kf.IsTerminationinChangeofcontrol = flagTerminationforChangeofControl;
            kf.IsTerminationinsolvency = flagTermnationforInsolvency;
            kf.IsTerminationmaterial = flagTermnationforMaterialBreach;
            kf.IsChangeofControl = flagChangeOfControl;
            kf.IsGuaranteeRequired = flagThirdPartyGuarantee;
            kf.IsStrictliability = flagStrictLiability;

            kf.IsIndirectConsequentialLosses = IsIndirectConsequentialLosses;
            kf.IndirectConsequentialLosses = IndirectConsequentialLosses;
            kf.IsIndemnity = IsIndemnity;
            kf.IsWarranties = IsWarranties;
            kf.Warranties = Warranties;
            kf.IsAgreementClause = flagEntireAgreement;
            kf.EntireAgreementClause = EntireAgreementClause;
            kf.ThirdPartyGuaranteeRequired = ThirdPartyGuaranteeRequired;
            kf.IsForceMajeure = IsForceMajeure;
            kf.ForceMajeure = ForceMajeure;
            kf.IsSetOffRights = IsSetOffRights;
            kf.SetOffRights = SetOffRights;
            kf.IsGoverningLaw = IsGoverningLaw;
            kf.GoverningLaw = GoverningLaw;

            kf.dtMatadataFieldValues = MetaData();
            kf.Param = 2;
            string KOStatus = kf.InsertRecord();
            return KOStatus;
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            return "";
        }
    }

    static string ImportantDatesUpdate(int RequestId)
    {
        try
        {
            IKeyFields kf = new KeyFeilds();
            kf.RequestId = RequestId;
            if (!string.IsNullOrEmpty(EffectiveDate))
                kf.EffectiveDate = Convert.ToDateTime(EffectiveDate);

            if (!string.IsNullOrEmpty(ExpiryDate))
                kf.ExpirationDate = Convert.ToDateTime(ExpiryDate);
            kf.Param = 1;
            string IDStatus = kf.InsertRecord();
            return IDStatus;
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            return "";
        }
    }

    static void GenerateContractVersion(string ContractFileName, int RequestId)
    {
        string DestinationDOCX = HttpContext.Current.Server.MapPath("~/Uploads/" + HttpContext.Current.Session["TenantDIR"] + "/Contract Documents/") + ContractFileName + ".docx";
        string DestinationPDF = HttpContext.Current.Server.MapPath("~/Uploads/" + HttpContext.Current.Session["TenantDIR"] + "/Contract Documents/PDF/") + ContractFileName + ".pdf";
        string OriginalAIPDFpath = HttpContext.Current.Server.MapPath("~/Uploads/" + HttpContext.Current.Session["TenantDIR"] + "/AINLP/PDF/") + OriginalAIPDFName;

        try
        {
            if (!string.IsNullOrEmpty(AIFileName))
            {
                License license = new License();
                license.SetLicense("Aspose.Words.lic");
                Document doc = new Document(AIFileName);
                if (IsPDF.Equals("Y"))
                {
                    string ExtractFileName = AIFileName.Substring(AIFileName.LastIndexOf("\\") + 1);
                    File.Copy(OriginalAIPDFpath, DestinationPDF);
                }
                else
                {
                    doc.Save(DestinationDOCX, SaveFormat.Docx);
                    doc.Save(DestinationPDF, SaveFormat.Pdf);
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            throw ex;
        }
    }
}