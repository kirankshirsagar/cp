﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Web.Services.Protocols;

/// <summary>
/// Summary description for DocusignConnect
/// </summary>
[WebService(Namespace = "http://DocuSignConnectListener")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class DocusignConnect : System.Web.Services.WebService {


    public DocusignConnect()
    {
    }

    [WebMethod]
    public string DocuSignConnectUpdateMe()
    {
        string envelopeId = "";
       
        return envelopeId;
    }


    [WebMethod]
    public string DocuSignConnectUpdate(ClauseLibraryBLL.DocuSignAPI.DocuSignEnvelopeInformation DocuSignEnvelopeInformation)
    {
        string envelopeId = "";
        try
        {

            envelopeId = DocuSignEnvelopeInformation.EnvelopeStatus.EnvelopeID;

            long count = System.Web.HttpContext.Current.Request.InputStream.Length;

            byte[] bytes = new byte[count];

            System.Web.HttpContext.Current.Request.InputStream.Read(bytes, 0, (int)count);

            string xml = System.Text.UTF8Encoding.UTF8.GetString(bytes);

            XmlSerializer xs = new XmlSerializer(typeof( ClauseLibraryBLL.DocuSignAPI.DocuSignEnvelopeInformation), "http://www.docusign.net/API/3.0");

            using (MemoryStream ms = new MemoryStream())
            {
                XmlTextWriter xmlTextWriter = new XmlTextWriter(ms, System.Text.Encoding.UTF8);

                xs.Serialize(xmlTextWriter, DocuSignEnvelopeInformation);


                File.WriteAllBytes(Server.MapPath("~") + "\\EnvelopeCerts\\" + DocuSignEnvelopeInformation.EnvelopeStatus.EnvelopeID + ".xml", ms.ToArray());
            }

        }
        catch (Exception ex)
        {
            // could not serialize
            File.WriteAllText(Server.MapPath("~") + "\\errorlog.txt", "Exception: " + ex.Message);

            throw new SoapException(ex.Message, SoapException.ClientFaultCode);
        }
        return envelopeId;
    }

       

    
    
}
