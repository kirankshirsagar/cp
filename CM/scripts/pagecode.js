﻿
          function UploadSucessful() 
          {
              alert('Profile picture added successfully.');
              window.open('', '_self', '');
              window.close();
          }
        function ShowErrorMessage() 
        {
            alert('Image could not be uploaded.Image size should be less than 1024 X 768');
        }
        jQuery(document).ready(function () {

            $('#imgCrop').Jcrop({
                setSelect: [0, 0, 500, 700],
                aspectRatio: 1,
                boxWidth: 500,
                boxHeight: 500,
                minSize: [109, 109],
                onSelect: storeCoords
            });
        });
        function storeCoords(c)
       {
                $('#X').val(c.x);
                $('#Y').val(c.y);
                $('#W').val(c.w);
                $('#H').val(c.h);
        }
