﻿
/*
jquery.caret.1.0.2.min.js
CommonFunctions.js
CommonValidation.js
ApplicationValidation.js
 jquery.autogrowtextarea.js
jquery.effects.core.js
jQuery UI Effects Slide 1.8.1
tabmenu.js
jquery.mousewheel.min.js
jquery.fileinput.js
jquery.ui.timepicker.min.js
jquery.ui.touch-punch.js
ui.spinner.min.js
jquery.imgareaselect.min.js
jquery.dualListBox-1.3.js
jquery.jgrowl-min.js

jquery.dataTables.min.js
chosen.jquery.min.js
colorpicker-min.js
jquery.tipsy-min.js
Sourcerer-1.2-min.js
jquery.validate.js
elrte.min.js
elfinder.min.js
mws.js
mws.wizard.js
themer.js
jquery.noty.js
Ensure.js
Master page Scripts

*/

/*
 * jquery.caret.1.0.2.min.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 * Copyright (c) 2010 C. F., Wong (<a href="http://cloudgen.w0ng.hk">Cloudgen Examplet Store</a>)
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 *
 */
﻿(function(k,e,i,j){k.fn.caret=function(b,l){var a,c,f=this[0],d=k.browser.msie;if(typeof b==="object"&&typeof b.start==="number"&&typeof b.end==="number"){a=b.start;c=b.end}else if(typeof b==="number"&&typeof l==="number"){a=b;c=l}else if(typeof b==="string")if((a=f.value.indexOf(b))>-1)c=a+b[e];else a=null;else if(Object.prototype.toString.call(b)==="[object RegExp]"){b=b.exec(f.value);if(b!=null){a=b.index;c=a+b[0][e]}}if(typeof a!="undefined"){if(d){d=this[0].createTextRange();d.collapse(true);
d.moveStart("character",a);d.moveEnd("character",c-a);d.select()}else{this[0].selectionStart=a;this[0].selectionEnd=c}this[0].focus();return this}else{if(d){c=document.selection;if(this[0].tagName.toLowerCase()!="textarea"){d=this.val();a=c[i]()[j]();a.moveEnd("character",d[e]);var g=a.text==""?d[e]:d.lastIndexOf(a.text);a=c[i]()[j]();a.moveStart("character",-d[e]);var h=a.text[e]}else{a=c[i]();c=a[j]();c.moveToElementText(this[0]);c.setEndPoint("EndToEnd",a);g=c.text[e]-a.text[e];h=g+a.text[e]}}else{g=
f.selectionStart;h=f.selectionEnd}a=f.value.substring(g,h);return{start:g,end:h,text:a,replace:function(m){return f.value.substring(0,g)+m+f.value.substring(h,f.value[e])}}}}})(jQuery,"length","createRange","duplicate");

/*
 * CommonFunctions.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 */

 function rdoCheckOnOff(rdoId, gridName) {
    var rdo = document.getElementById(rdoId);
    var all = document.getElementsByTagName("input");    /* Getting an array of all the "INPUT" controls on the form.*/
    /*Checking if it is a radio button, and also checking if the id of that radio button is different than "rdoId" */
    for (i = 0; i < all.length; i++) {
        if (all[i].type == "radio" && all[i].id != rdo.id) {
            var count = all[i].id.indexOf(gridName);
            if (count != -1) {
                all[i].checked = false;
            }
        }
    }
    rdo.checked = true; 
}





function roundNumber(num, dec) {
    var result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
    return result;
}

function RepelaceNtoBR(str) {

   
    var res1 = str.split('\n');
    str = res1.join("<br />");
    str = $.trim(str);
    return str;
}

function ValidateDate(dtValue) {
    var dtRegex = new RegExp(/\b\d{1,2}[\/-]\d{1,2}[\/-]\d{4}\b/);
    return dtRegex.test(dtValue);
}


function isNumeric(str) {
    var val = 0;
    str = str.substring(4, str.length);
    switch (str.toLowerCase()) {
        case "bigint":
            val = 1;
            break;
        case "bit":
            val = 2;
            break;
        case "decimal":
            val = 3;
            break;
        case "float":
            val = 4;
            break;
        case "int":
            val = 5;
            break;
        case "money":
            val = 6;
            break;
        case "numeric":
            val = 7;
            break;
        case "uniqueidentifier":
            val = 8;
            break;
    }
    return val;
}


function isDate(str) {
    var val = 0;
    str = str.substring(4, str.length);
    switch (str.toLowerCase()) {
        case "date":
            val = 1;
            break;
        case "datetime":
            val = 2;
            break;
        case "smalldatetime":
            val = 3;
            break;
    }
    return val;
}

function isMaster(str) {

    var val = 0;
    switch (str.toLowerCase()) {
        case "country":
            val = 1;
            break;
        case "industry":
            val = 2;
            break;
        case "product":
            val = 3;
            break;
        case "lead source":
            val = 4;
            break;
        case "lead status":
            val = 5;
            break;
        case "stage":
            val = 6;
            break;
        case "lead sales user":
            val = 7;
            break;
        case "lead created by":
            val = 8;
            break;
        case "city":
            val = 9;
            break;
    }
    return val;
}



jQuery.fn.centerDiv = function (options) {
    var o = this;
    $(window).bind('load resize', function () {
        $(o).each(function () {
            $(this).css({
                position: 'absolute',
                left: ($(window).width() - $(this).outerWidth()) / 2,
                top: ($(window).height() - $(this).outerHeight()) / 2
            });

        });
    });
}



function myDateFormat(strDate) {
    var tempDate = new Array();
    tempDate = strDate.split('-');
    var dt = tempDate[0];
    var mt = getMonth(tempDate[1]);
    var yr = tempDate[2];
    return (yr + '/' + mt + '/' + dt);
}

function getMonth(monthName) {
    var val = 0;
    switch (monthName.toLowerCase()) {
        case "jan":
            val = 1;
            break;
        case "feb":
            val = 2;
            break;
        case "mar":
            val = 3;
            break;
        case "apr":
            val = 4;
            break;
        case "may":
            val = 5;
            break;
        case "jun":
            val = 6;
            break;
        case "jul":
            val = 7;
            break;
        case "aug":
            val = 8;
            break;
        case "sep":
            val = 9;
            break;
        case "oct":
            val = 10;
            break;
        case "nov":
            val = 11;
            break;
        case "dec":
            val = 12;
            break;
    }
    return val;
}


function isDecimalKey(evt, obj) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        if (charCode != 46) {
            return false;
        }
    }

    var count = 0;
    var myNum = obj.value + String.fromCharCode(charCode);
    var pattern = /^\d{0,8}\.?\d{0,2}$/;

    if (!pattern.test(myNum)) {
        count = 1;
    }
    if (count == 1) {
        return false;
    }
    else {
        if (myNum > 10000000) {
            return false;
        }
    }
    return true;
}


function ToNumber(inputValue) {
    var val = 0;
    if (isNaN(inputValue) == false) {
        val = inputValue;
    }
    else {
        val = 0;
    }
    return val;

}


function FormatNumber(inputValue) {
    inputValue = parseFloat(inputValue);
    inputValue = inputValue.toFixed(2);
    return inputValue;
}


jQuery.fn.capitalize = function() {
    $(this[0]).keyup(function(event) {
        var box = event.target;
        var txt = $(this).val();
        var start = box.selectionStart;
        var end = box.selectionEnd;
         $(this).val(txt.replace(/^(.)|\s+(.)/g, function($1) {
            return $1.toUpperCase();
        }));

////        $(this).val(txt.replace(/^(.)|(\s|\-)(.)/g, function($1) {
////            return $1.toUpperCase();
////        }));
        box.setSelectionRange(start, end);
    });

   return this;
}

/*
 * CommonValidation.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 */
 $(document).ready(function () {

 // added by girish
  $('.removeSpecialChar').bind('keyup','blur',function(){
  var initVal = $(this).val();
            outputVal = initVal.replace(/[^a-zA-Z0-9 ]/g, "");
            if (initVal != outputVal) {
                $(this).val(outputVal);
          }
  });

  //Added by Girish - changes starts
   $('.MstIndustryName').bind('keyup','blur',function(){
  var initVal = $(this).val();
            outputVal = initVal.replace(/[^a-zA-Z & ]/g, "");
            if (initVal != outputVal) {
                $(this).val(outputVal);
          }
  });
  //Added by Girish - changes ends

    $("textarea[maxlength], input[maxlength]").keypress(function(event){
            var key = event.which;
              
            //all keys including return.
            if(key >= 33 || key == 13 || key == 32) {
                var maxLength = $(this).attr("maxlength");
                var length = this.value.length;
                if(length >= maxLength) {                    
                    event.preventDefault();
                }
            }
        });

   // comitted by yogesh 
    $(':text , textarea').live('change', function (evt) { 
        if(!$(this).hasClass('url') && !$(this).hasClass('email') && !$(this).hasClass('Password') && !$(this).hasClass('UserName') && !$(this).hasClass('emaillist')&& !$(this).hasClass('FTPUserId')){
         var txt = $.trim($(this).val());
//        var box = event.target;
//        var start = box.selectionStart;
//        var end = box.selectionEnd;
                                    // $(this).val(txt.replace(/^(.)|\s+(.)/g, function($1){ return $1.toUpperCase( ); })); //url  email 
          //     $(this).val(txt.replace(/((^| )[a-z])/g, function($1){ return $1.toUpperCase( ); })); //url  email 
   //       $(this).capitalize();
            txt = txt.slice(0,1).toUpperCase() + txt.slice(1);
           $(this).val(txt);
      //      box.setSelectionRange(start, end);
        }
    });

     //----------added by Bharati---------------------

     try {
    
  $('.FolderName').live('keypress', function (evt) {  
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
         return true;
        }
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;
       
        var specialChar="\/:*?\"\\<>|#&";
        specialChar=specialChar.split('');
        for(var i=0; i<specialChar.length; i++)
        {
            var ind=myNum.indexOf(specialChar[i]);
            if(ind!=-1)
            {
            $(this).attr('TITLE','A folder name can\'t contain any of these characters:  \ / : * ? \" \\ < > | # &').tipsy("show");
            return false;
            }
            else
                $(this).tipsy("hide");
        }
    }).tipsy({trigger: 'keypress',fade:true,delayOut: 300});
   
    } catch (e) {
    //alert('common validation');
}





    $('.FolderName').live('paste', function (evt) {       
        setTimeout(function () {               
        myNum = $('.FolderName').val();        
        var specialChar="\/:*?\"\\<>|#&";
        specialChar=specialChar.split('');
        for(var i=0; i<specialChar.length;i++)
        {
            if(myNum.indexOf(specialChar[i]) >-1)
            {
                $('.FolderName').attr('TITLE','A folder name can\'t contain any of these characters:  \ / : * ? \" \\ < > | # & ').tipsy("show");
                break;
            }
        }
        myNum=myNum.replace(/[\/:*?\"\\<>|# &]/g,'');
        $('.FolderName').val(myNum);
        return false;
               
         }, 0);
         
    }).tipsy({trigger: 'paste',fade:true,delayOut: 300});
     //---------------------------------------------------------------------

   $('.pasteBlock').live('paste', function (e) {
        return false;
    });

    $('.cutCopyPasteBlock').live('copy cut paste', function (e) {
         return false;
    });

    $('.noInput').live('keypress', function (e) {
        return false;
    });


    $('.percentage').live('paste', function (e) {
        return false;
    });

//    $('.number , .int , .numbersOnly , .money , .percentage , .Landline , .mobile').live('copy cut paste', function (e) { 
//        e.preventDefault();
//    });





    /* 
    * Validators


    * 1 - number                2 - numbersOnly       3 - money                 4 - percentage
    * 5 - Landline              6 - mobile            7- pin                       8- pancard 
    * 9 - ValChar             10 - ValOnlyNumChar    11 - ValOnlyChar          12 -  ValOnlyCharSingleSpace
    * 13 - email                14 - url
    */

    /* number  - old name comNoDecimal */
    $('.number').live('keypress', function (evt) {
     var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
         return true;
        }
        else if ((charCode < 48) || (charCode > 57)) {
            return false;
        }
    });

    $('.int').live('keypress', function (evt) {
     var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
         return true;
        }
        else if ((charCode < 48) || (charCode > 57)) {
           return false;
        }

    });

     $('.int').live('paste', function (evt) {         
        var charCode = (evt.which) ? evt.which : event.keyCode
        $obj= $(this);
        setTimeout(function () {
             myNum = $obj.val();
        
            var pattern=/^[0-9]+$/;            
            
            if (!pattern.test(myNum)) {                                                
                myNum=myNum.replace(/[^0-9]/g,'');                
                $obj.val(myNum);
                return false;                 
            }       
         }, 0);   
    });

    $('.numbersOnly').live('keypress', function (evt) {
     var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
         return true;
        }
        else if ((charCode != 46 || $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57)) {
            return false;
        }
    });


    /*money  - old name comCurrency*/
    $('.money,.moneyZero').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if(charCode == 8){
         return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            if (charCode != 46) {
                return false;
            }
        }

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var count = 0;

        var pattern = /^\d{0,8}\.?\d{0,2}$/;

        if (!pattern.test(myNum)) {
            count = 1;
        }
        if (count == 1) {
            return false;
        }
        else {
            if (myNum > 1000000000) {
                return false;
            }
        }
       
        return true;
    });
    
    //Added by Pankaj - money field should not allow 0
    $('.money').live('blur',function () {
            if ($(this).val() != "") {
                var amt = parseFloat($(this).val());
                if (amt <= 0) {
                    $(this).val('');
                }
            }
        });


    $('.percentage').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        
        if(charCode == 8){
         return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            if (charCode != 46) {
                return false;
            }
        }
        var count = 0;
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;


        var pattern = /^\d{0,3}\.?\d{0,2}$/;
        if (!pattern.test(myNum)) {
            count = 1;

        }
        if (count == 1) {
            return false;
        }
        else {
            if (myNum > 100) {
                return false;
            }
        }
        return true;
    });


    $('.Landline').live('keypress', function (evt) {
     var charCode = (evt.which) ? evt.which : event.keyCode

        if(charCode == 8){
         return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;
       
        if (myNum.length > 10) {
            return false;
        }
        return true;
//        var charCode = (evt.which) ? evt.which : event.keyCode

//        if(charCode == 8){
//         return true;
//        }
//        else if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 40 && charCode != 41 && charCode != 45)
//            return false;

//        var caretpos = $(this).caret().start;
//        valuestart = $(this).val().substring(0, caretpos);
//        valueend = $(this).val().substring(caretpos);
//        myNum = valuestart + String.fromCharCode(charCode) + valueend;


//        if (myNum.length > 20) {
//            return false;
//        }

//        return true;


    });

    //Added by Pankaj - Landline field should not allow 0
    $('.Landline').live('blur',function () {
            if ($(this).val() != "") {
                var amt = parseInt($(this).val());
                if (amt <= 0) {
                    $(this).val('');
                }
            }
        });

        //Added by Pankaj - Landline field should not allow 0
    $('.mobile').live('blur',function () {
            if ($(this).val() != "") {
                var amt = parseInt($(this).val());
                if (amt <= 0) {
                    $(this).val('');
                }
            }
        });

        //Added by Pankaj - Landline field should not allow 0
    $('.fax').live('blur',function () {
            if ($(this).val() != "") {
                var amt = parseInt($(this).val());
                if (amt <= 0) {
                    $(this).val('');
                }
            }
        });

     $('.Contact').live('keypress', function (evt) {
     var charCode = (evt.which) ? evt.which : event.keyCode

        if(charCode == 8){
         return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length > 10) {
            return false;
        }
        return true;
    });

    $('.Telephone').live('keypress', function (evt) {
     var charCode = (evt.which) ? evt.which : event.keyCode

        if(charCode == 8){
         return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length > 10) {
            return false;
        }
        return true;
//        var charCode = (evt.which) ? evt.which : event.keyCode

//        if(charCode == 8){
//         return true;
//        }
//        else if (charCode > 31 && (charCode < 48 || charCode > 57))
//            return false;

//        var caretpos = $(this).caret().start;
//        valuestart = $(this).val().substring(0, caretpos);
//        valueend = $(this).val().substring(caretpos);
//        myNum = valuestart + String.fromCharCode(charCode) + valueend;


//        if (myNum.length > 15) {
//            return false;
//        }

//        return true;


    });

    $('.Residential').live('keypress', function (evt) {
     var charCode = (evt.which) ? evt.which : event.keyCode

        if(charCode == 8){
         return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length > 10) {
            return false;
        }
        return true;
//        var charCode = (evt.which) ? evt.which : event.keyCode

//        if(charCode == 8){
//         return true;
//        }
//        else if (charCode > 31 && (charCode < 48 || charCode > 57))
//            return false;

//        var caretpos = $(this).caret().start;
//        valuestart = $(this).val().substring(0, caretpos);
//        valueend = $(this).val().substring(caretpos);
//        myNum = valuestart + String.fromCharCode(charCode) + valueend;


//        if (myNum.length > 15) {
//            return false;
//        }

//        return true;


    });


     $('.fax').live('keypress', function (evt) {
//       var charCode = (evt.which) ? evt.which : event.keyCode

//        if(charCode == 8){
//         return true;
//        }
//        else if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 40 && charCode != 41 && charCode != 45)
//            return false;

//        var caretpos = $(this).caret().start;
//        valuestart = $(this).val().substring(0, caretpos);
//        valueend = $(this).val().substring(caretpos);
//        myNum = valuestart + String.fromCharCode(charCode) + valueend;


//        if (myNum.length > 20) {
//            return false;
//        }

//        return true;
        var charCode = (evt.which) ? evt.which : event.keyCode

        if(charCode == 8){
         return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length > 10) {
            return false;
        }
        return true;

    });

    $('.mobile').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode

        if(charCode == 8){
         return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length > 10) {
            return false;
        }
        return true;


    });


    $('.pin').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
         return true;
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length > 6) {
            return false;
        }
        return true;


    });

    $('.pin').live('keyup', function (evt) {
          $(this).parent().find('label.pincodeerror').remove();
    });

    $('.pin').live('paste', function (evt) { 
   
        setTimeout(function () { 
 
            var num = $('.pin').val().match(/\d/g);
            if(num != null){
                num=num.join("");
                num = num.substring(0,6);
                $('.pin').val(num);
            }else{
                $('.pin').val('');
            }
            return false;
           
         }, 0);     
    })

     $('.panCard,.TAN').live('keyup', function () {// added 'TAN' by Bharati 14Nov2013 [Defect #7159]
        $(this).val(($(this).val()).toUpperCase());
     });

     $('.panCard,.TAN').live('keypress', function (evt) { // added 'TAN' by Bharati 14Nov2013 [Defect #7159]

        //        var pattern = /^[A-Z]{3}[G|A|F|C|T|H|P]{1}[A-Z]{1}\d{4}[A-Z]{1}$/;
      //  this.val(this.val().toUpperCase());

        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 8) {
            return true;
        }
        if (charCode == 13)
            return false;        

        var caretpos = $(this).caret().start;
        var valuestart = $(this).val().substring(0, caretpos);
        var valueend = $(this).val().substring(caretpos);
        var myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length <= 3) {
            var pattern = /^[a-zA-Z]{0,3}$/;
            if (!pattern.test(myNum)) {
                return false;
            }
        } else if (myNum.length == 4) {
            var pattern = /^[a-zA-Z]{3}[G|g|A|a|F|f|C|c|T|t|H|h|P|p]{1}$/;
            if (!pattern.test(myNum)) {
                return false;
            }
        } else if (myNum.length == 5) {
            var pattern = /^[a-zA-Z]{3}[G|g|A|a|F|f|C|c|T|t|H|h|P|p]{1}[a-zA-Z]{1}$/;
            if (!pattern.test(myNum)) {
                return false;
            }
        }
        else if (myNum.length <= 9) {
            var pattern = /^[a-zA-Z]{3}[G|g|A|a|F|f|C|c|T|t|H|h|P|p]{1}[a-zA-Z]{1}\d{0,4}$/;
            if (!pattern.test(myNum)) {
                return false;
            }
        }
        else {
            var pattern = /^[a-zA-Z]{3}[G|g|A|a|F|f|C|c|T|t|H|h|P|p]{1}[a-zA-Z]{1}\d{4}[a-zA-Z]{1}$/;
            if (!pattern.test(myNum)) {
                return false;
            }
        }
        return true;
    });


    $('.ValChar').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
         return true;
        }
        else if (charCode > 31 && (charCode < 42 || charCode > 57) && charCode != 46) {

            if ((charCode == 34) || (charCode == 39) || (charCode == 93) || (charCode == 92) || (charCode == 124) || (charCode == 125)) {
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return false;
        }

    });


    $('.ValOnlyNumChar').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
         return true;
        }
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^[a-z0-9]+$/i;
        if (!pattern.test(myNum)) {
            return false;

        }
        return true;
        //  
    });
    $('.ValOnlyNumChar').live('paste', function (evt) {
        var obj=$(this);
        setTimeout(function(evt){      
        var myval=obj.val();
        var pattern = /^[a-z0-9]+$/i;
        if (!pattern.test(myval)) {
            obj.val(myval.replace(/[^a-zA-Z0-9]/g,''));
            return false;
        }
        return true;
        },0);
        //  
    });

    $('.ValOnlyChar').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
         if(charCode == 8){
         return true;
        }
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^[a-zA-Z]+$/;
        if (!pattern.test(myNum)) {
            return false;
        }
        return true;
    });


    $('.ValOnlyCharSingleSpace').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
         if(charCode == 8){
           return true;
        }
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^([A-Za-z]+ ?)*$/;
        if (!pattern.test(myNum)) {
            return false;
        }
        return true;
    });

    $('.ValOnlyCharSingleSpace').live('paste', function (evt) {
        $obj=$(this);
         setTimeout(function () { 
             myNum = $obj.val();
        
            var pattern=/^([A-Za-z]+ ?)*$/;            
            
            if (!pattern.test(myNum)) {                                                
                myNum=myNum.replace(/[-@#!$%^&*()_+|~=`{}\\\[\]:";'<>?,.\/]/g,'');                
                $obj.val(myNum);
                return false;                 
            }       
         }, 0);         
    });

       $('.ValNumOnlyCharSingleSpace').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
         if(charCode == 8){
         return true;
        }
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;
        var pattern = /^([A-Za-z0-9]+ ?)*$/;
        if (!pattern.test(myNum)) {
            return false;
        }
        return true;
    });

    
       $('.ValOnlyCharNumSingleSpace').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
         if(charCode == 8){
         return true;
        }
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^([A-Za-z0-9]+ ?)*$/;
        if (!pattern.test(myNum)) {
            return false;
        }
        return true;
    });
     $('.ValOnlyCharNumSingleSpace').live('paste', function (evt) {
        $obj=$(this);
         setTimeout(function () { 
             myNum = $obj.val();
        
            var pattern=/^([A-Za-z0-9]+ ?)*$/;     
            
            if (!pattern.test(myNum)) {                                                
                myNum=myNum.replace(/[-@#!$%^&*()_+|~=`{}\\\[\]:";'<>?,.\/]/g,'');                
                $obj.val(myNum);
                return false;                 
            }       
         }, 0);         
    });
       $('.ValCharNumSpace').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
         if(charCode == 8){
         return true;
        }
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^([A-Za-z0-9@._-]+ ?)*$/;
        if (!pattern.test(myNum)) {
            return false;
        }
        return true;
    });


    $('.ValforTextArea').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode  
             if ((charCode == 39 || charCode == 34))
                 return false;

        return true;

    });



    $('.email').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
         if(charCode == 8){
         return true;
        }
        if (charCode == 13) {
            return false;
        }

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

         var pattern = /^[A-Za-z0-9@._-]*$/;       
        if (!pattern.test(myNum)) {
            return false;

        }

        if (myNum.length > 100) {
            return false;
        }

        return true;
    });

    $('.url').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
         if(charCode == 8){
         return true;
        }
        if (charCode == 13) {
            return false;
        }

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;


        if (myNum.length > 250) {
            return false;
        }

        return true;
    });

    $('.FTP').live('keypress', function (evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
                if(charCode == 8){
                return true;
            }
            if (charCode == 13) {
                return false;
            }

            var caretpos = $(this).caret().start;
            valuestart = $(this).val().substring(0, caretpos);
            valueend = $(this).val().substring(caretpos);
            myNum = valuestart + String.fromCharCode(charCode) + valueend;


            if (myNum.length > 250) {
                return false;
            }
            return true;
        });

    $('*[class*="AllowMax"]').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode


        var classname = $(this).attr('class');
        var MaxClass = '';
        var classes = classname.split(" ");
        for (var i = 0; i < classes.length; i++) {
            if (classes[i].indexOf("AllowMax") >= 0) {
                MaxClass = classes[i];
            }
        }

        if (MaxClass != '') {
            var vallength = MaxClass.substring(MaxClass.indexOf("x") + 1);

            var caretpos = $(this).caret().start;
            valuestart = $(this).val().substring(0, caretpos);
            valueend = $(this).val().substring(caretpos);
            myNum = valuestart + String.fromCharCode(charCode) + valueend;

            if (myNum.length > vallength) {
                return false;
            }
        }
        return true;

    });

    $('.UserName').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
         if(charCode == 8){
         return true;
        }
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^[A-Za-z@.]*$/;       
        if (!pattern.test(myNum)) {
            return false;

        }
        return true;
        //  
    });


    $('.Hours').live('keypress', function (evt) {
    
        var charCode = (evt.which) ? evt.which : event.keyCode
         if(charCode == 8){
         return true;
        }

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            if (charCode != 46) {
                return false;
            }
        }
        var count = 0;
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;


        var pattern = /^\d{0,3}\.?\d{0,2}$/;
        if (!pattern.test(myNum)) {
            count = 1;

        }
        if (count == 1) {
            return false;
        }
        else {
            if (myNum > 1000) {
                return false;
            }
        }
        return true;
         
    });


});



/*
 * ApplicationValidation.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 */

 //Use For Application Form Created By Mahesh 12-Sep-2012 //
//1 - name                2 - ComAndTrdName       3 - DIN                 4 - TypeLoan
//5 - financierName       6 - enterpriseName      7 - bankName
//8 - branchName          9 - facilityName        10 - DoBusinessSince    11 - acquirerName
//12 - Location           13 - MID                14 - TID                15 - SlaMgrCode
//16 - DSACode            17 - Remarks            18 - CaseNo             19 - AccountNo
//20 - Pendingwith        21 - Plaintiff          22 - Defendant          23 - accountNature
//24 - accountoperated    25 - Securityoffered    26 - address            27 - AppNo
//28 - panCard            30 - tanNo              31 - Edustatus          32 - designation
//33 - pin                34 - Landline           35 - mobile             36 - sharesheld
//37 - percentage         38 - AreaInFeet         39 - Currency           40 - EMI
//41 - Tenure             40 - Workingsince       41 - anyother

$(document).ready(function () {

    $('.name').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

//        var pattern = /^[a-zA-Z]+$/;
//        if (!pattern.test(myNum)) {
//            return false;

//        }

        var len = myNum.length;
        if (len > 30)
            return false;

        return true;
    });


    $('.onlyChar').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^[a-zA-Z]+$/;
        if (!pattern.test(myNum)) {
            return false;

        }


        return true;

    });
    $('.Bigname').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

        if (charCode == 13)
            return false;


        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

//        var pattern = /^[a-zA-Z ]*$/;
//        if (!pattern.test(myNum)) {
//            return false;

//        }

        var len = myNum.length;
        if (len > 50)
            return false;

        return true;
    });


    $('.anyother').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

        if (charCode == 13)
            return false;


        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 200) {
            return false;
        }
        else {
            return true;
        }


        return true;
    });


    $('.ComAndTrdName').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

        if (charCode == 13)
            return false;


        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 50) {
            return false;
        }
        else {
            return true;
        }


        return true;
    });

    $('.DIN').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

        if (charCode == 13)
            return false;


        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 8) {
            return false;
        }
        else {
            return true;
        }


        return true;
    });

    $('.Workingsince').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 8) {
            return false;
        }
        else {
            return true;
        }


        return true;
    });


    $('.TypeLoan').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^[a-zA-Z ]*$/;
        if (!pattern.test(myNum)) {
            return false;

        }

        var len = myNum.length;
        if (len > 50)
            return false;

        return true;
    });
    $('.financierName').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

//        var pattern = /^[a-zA-Z .&]*$/;
//        if (!pattern.test(myNum)) {
//            return false;

//        }

        var len = myNum.length;
        if (len > 50)
            return false;

        return true;
    });
    $('.enterpriseName').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^[a-zA-Z .]*$/;
        if (!pattern.test(myNum)) {
            return false;

        }

        var len = myNum.length;
        if (len > 50)
            return false;

        return true;
    });
    $('.bankName').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 50) {
            return false;
        }
        else {
            return true;
        }


        return true;
    });
    $('.branchName').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^[a-zA-Z .()]*$/;
        if (!pattern.test(myNum)) {
            return false;

        }

        var len = myNum.length;
        if (len > 100)
            return false;

        return true;
    });
    $('.facilityName').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 50) {
            return false;
        }
        else {
            return true;
        }


        return true;
    });

    $('.DoBusinessSince').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 8) {
            return false;
        }
        else {
            return true;
        }


        return true;
    });

    $('.acquirerName').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

//        var pattern = /^[a-zA-Z .]*$/;
//        if (!pattern.test(myNum)) {
//            return false;

//        }

        var len = myNum.length;
        if (len > 50)
            return false;

        return true;
    });
    $('.Location').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 100) {
            return false;
        }
        else {
            return true;
        }


        return true;
    });
    $('.MID').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 15) {
            return false;
        }
        else {
            return true;
        }


        return true;
    });
    $('.TID').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 8) {
            return false;
        }
        else {
            return true;
        }

        return true;
    });


    $('.SlaMgrCode').live('keypress', function (evt) {
       
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 15) {
            return false;
        }
        else {
            return true;
        }


        return true;
    });
    $('.DSACode').live('keypress', function (evt) {

    
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 15) {
            return false;
        }
        else {
            return true;
        }


        return true;
    });

    $('.Remarks').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 200) {
            return false;
        }
        else {
            return true;
        }


        return true;
    });

    $('.CaseNo').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 20) {
            return false;
        }
        else {
            return true;
        }


        return true;
    });
    $('.AccountNo').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 20) {
            return false;
        }
        else {
            return true;
        }

        return true;
    });

    $('.Pendingwith').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 50) {
            return false;
        }
        else {
            return true;
        }


        return true;
    });
    $('.Plaintiff').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 50) {
            return false;
        }
        else {
            return true;
        }


        return true;
    });

    $('.Defendant').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 50) {
            return false;
        }
        else {
            return true;
        }


        return true;
    });
    $('.accountNature').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 50) {
            return false;
        }
        else {
            return true;
        }


        return true;
    });
    $('.accountoperated').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 8) {
            return false;
        }
        else {
            return true;
        }


        return true;
    });


    $('.Securityoffered').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 50) {
            return false;
        }
        else {
            return true;
        }


        return true;
    });


    $('.address').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

//        if (charCode == 13)
//            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 300) {
            return false;
        }
        else {
            return true;
        }


        return true;

    });

    $('.AppNo').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend

        var len = myNum.length;
        if (len > 10) {
            return false;
        }
        else {
            return true;
        }


        return true;

    });

   

    $('.tanNo').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        var valuestart = $(this).val().substring(0, caretpos);
        var valueend = $(this).val().substring(caretpos);
        var myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length <= 4) {
            var pattern = /^[a-zA-Z]{0,4}$/;
            if (!pattern.test(myNum)) {
                return false;

            }
        } else if (myNum.length <= 9) {
            var pattern = /^[a-zA-Z]{0,4}\d{0,5}$/;
            if (!pattern.test(myNum)) {
                return false;

            }
        }
        else {
            var pattern = /^[a-zA-Z]{4}\d{5}[a-zA-Z]{1}$/;
            if (!pattern.test(myNum)) {
                return false;

            }
        }



        return true;

    });

    $('.Edustatus').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^[a-zA-Z .]*$/;
        if (!pattern.test(myNum)) {
            return false;

        }

        var len = myNum.length;
        if (len > 50)
            return false;

        return true;

    });

    $('.designation').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

        if (charCode == 13)
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^[a-zA-Z .]*$/;
        if (!pattern.test(myNum)) {
            return false;

        }

        var len = myNum.length;
        if (len > 50)
            return false;

        return true;

    });
    $('.pin').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length > 6) {
            return false;
        }
        return true;

    });
//    $('.Landline').live('keypress', function (evt) {
//        var charCode = (evt.which) ? evt.which : event.keyCode
//        if(charCode == 8){
//             return true;
//        }
//        if (charCode == 13)
//            return false;

//        if (charCode > 31 && (charCode < 48 || charCode > 57))
//            return false;

//        var caretpos = $(this).caret().start;
//        valuestart = $(this).val().substring(0, caretpos);
//        valueend = $(this).val().substring(caretpos);
//        myNum = valuestart + String.fromCharCode(charCode) + valueend;

//        if (myNum.length > 15) {
//            return false;
//        }
//        return true;

//    });
    $('.mobile').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length > 10) {
            return false;
        }
        return true;
    });

    $('.sharesheld').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        if (myNum.length > 4) {
            return false;
        }
        return true;
    });


    $('.percentage').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            if (charCode != 46) {
                return false;
            }
        }

        var count = 0;
        var myNum = this.value + String.fromCharCode(charCode);
        var pattern = /^\d{0,2}\.?\d{0,2}$/;

        if (!pattern.test(myNum)) {
            count = 1;
        }
        if (count == 1) {
            return false;
        }
        else {
            if (myNum > 100) {
                return false;
            }
        }
        return true;

    });


    $('.AreaInFeet').live('keypress', function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            if (charCode != 46) {
                return false;
            }
        }

        var count = 0;
        var myNum = this.value + String.fromCharCode(charCode);
        var pattern = /^\d{0,4}\.?\d{0,3}$/;

        if (!pattern.test(myNum)) {
            count = 1;
        }
        if (count == 1) {
            return false;
        }
        else {
            if (myNum > 9999.999) {
                return false;
            }
        }
        return true;

    });

    $('.Currency').live('keypress', function (evt) {
    debugger;
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            if (charCode != 46) {
                return false;
            }
        }

        var count = 0;
        var myNum = this.value + String.fromCharCode(charCode);
      //  var pattern = /^\d{0,8}$/;
          var pattern = /^\d{0,8}\.?\d{0,2}$/;
        // var pattern = /^\d{0,8}\.?\d{0,0}$/;
        if (!pattern.test(myNum)) {

            count = 1;
        }
        if (count == 1) {
            return false;
        }
        else {
            if (myNum > 99999999) {
                return false;
            }

        }
        return true;
    });

    $('.Tenure').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }
        if (charCode == 13)
            return false;

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            if (charCode != 46) {
                return false;
            }
        }

        var count = 0;
        var myNum = this.value + String.fromCharCode(charCode);
        var pattern = /^\d{0,8}$/;
        // var pattern = /^\d{0,8}\.?\d{0,0}$/;
        if (!pattern.test(myNum)) {

            count = 1;
        }
        if (count == 1) {
            return false;
        }
        else {
            if (myNum > 9999) {
                return false;
            }
        }
        return true;
    });

    $('.EMI').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
             return true;
        }

        if (charCode == 13)
            return false;

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            if (charCode != 46) {
                return false;
            }
        }

        var count = 0;
        var myNum = this.value + String.fromCharCode(charCode);
        var pattern = /^\d{0,8}$/;
        // var pattern = /^\d{0,8}\.?\d{0,0}$/;
        if (!pattern.test(myNum)) {

            count = 1;
        }
        if (count == 1) {
            return false;
        }
        else {
            if (myNum > 999999) {
                return false;
            }
        }
        return true;
    });


      $('.CIBIL').live('keypress', function (evt) {
     
        var charCode = (evt.which) ? evt.which : event.keyCode
        
        if (charCode == 8 || charCode == 45) {
            return true;
        }
      

        if (charCode == 13)
            return false;

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            if (charCode != 46) {
                return false;
            }
        }

        var count = 0;
        var myNum = this.value + String.fromCharCode(charCode);
         if (myNum > 900) {
                return false;
            }
        return true;
    });
   

});


/*
 * jquery.autogrowtextarea.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 */

 //Private variables
var colsDefault = 0;
var rowsDefault = 0;
//var rowsCounter = 0;

//Private functions
function setDefaultValues(txtArea) {
    colsDefault = txtArea.cols;
    rowsDefault = txtArea.rows;

    //rowsCounter = document.getElementById("rowsCounter");
}

function bindEvents(txtArea) {
    if (txtArea.value != '') {
        $(txtArea).css('height', 'auto');
        grow(txtArea);
    }

    txtArea.onkeyup = function () {
        $(txtArea).css('height', 'auto');
        // txtArea.rows = (txtArea.value.split("\n").length || 1);
        grow(txtArea);
    }
    
}

//Helper functions
function grow(txtArea) {
    //  alert(txtArea.rows);

    while (txtArea.rows > 1 && txtArea.scrollHeight < txtArea.offsetHeight) {
        txtArea.rows--;
    }
 

    var h = 0;
    while (txtArea.scrollHeight > txtArea.offsetHeight && h !== txtArea.offsetHeight) {
        h = txtArea.offsetHeight;
        txtArea.rows++;
    }
    //   txtArea.rows++;
  
}

//Public Method
jQuery.fn.autoGrow = function () {
    return this.each(function () {

        setDefaultValues(this);
        bindEvents(this);
    });
};


 
//;(function($, doc) {
//    'use strict';

//    // Plugin interface
//    $.fn.autoGrowTextarea = autoGrowTextArea;
//    $.fn.autoGrowTextArea = autoGrowTextArea;

//    // Shorthand alias
//    if (!('autoGrow' in $.fn)) {
//        $.fn.autoGrow = autoGrowTextArea;
//    }

//    /**
//     * Initialization on each element
//     */
//    function autoGrowTextArea() {
//        return this.each(init);
//    }

//    /**
//     * Actual initialization
//     */
//    function init() {
//        var $textarea, $origin, origin, hasOffset, innerHeight, height, offset = -5;



//        $textarea = $(this).css({overflow: 'hidden', resize: 'none'});
//        $origin = $textarea.clone().val('').appendTo(doc.body);
//        origin = $origin.get(0);

//        height = $origin.height();
//        origin.scrollHeight; // necessary for IE6-8. @see http://bit.ly/LRl3gf
//        hasOffset = (origin.scrollHeight !== height);

//        // `hasOffset` detects whether `.scrollHeight` includes padding.
//        // This behavior differs between browsers.
//        if (hasOffset) {
//            innerHeight = $origin.innerHeight();
//            offset = innerHeight - height;
//        }

//        $origin.hide();

//        $textarea
//            .data('autogrow-origin', $origin)
//            .data('autogrow-offset', offset)
//            .data('autogrow-initial-height', height)
//            .on('focus', onTextAreaFocus)
//            .on('blur', onTextAreaBlur)
//            ;

//        grow($textarea, $origin, origin,  height, offset);
//    }

//    /**
//     * on focus
//     */
//    function onTextAreaFocus() {
//        var $textarea, $origin, origin, initialHeight, offset, doGrow, timerId;

//        $textarea = $(this);
//        $origin = $textarea.data('autogrow-origin');
//        origin = $origin.get(0);
//        initialHeight = $textarea.data('autogrow-initial-height');
//        offset = $textarea.data('autogrow-offset');
//        grow.prev = $textarea.attr('value');
//        doGrow = function() {
//            grow($textarea, $origin, origin, initialHeight, offset);
//        };

//        timerId = setInterval(doGrow, 10);
//        $textarea.data('autoGrowTimerId', timerId);
//    }

//    /**
//     * on blur
//     */
//    function onTextAreaBlur() {
//        var timerId = $(this).data('autoGrowTimerId');
//        clearInterval(timerId);
//    }

//    /**
//     * grow textarea height if its value changed
//     */
//    function grow($textarea, $origin, origin, initialHeight, offset) {
//        var current, prev, scrollHeight, height;

//        current = $textarea.attr('value');
//        prev = grow.prev;
//        if (current === prev) return;

//        grow.prev = current;

//        $origin.attr('value', current).show();
//        origin.scrollHeight; // necessary for IE6-8. @see http://bit.ly/LRl3gf
//        scrollHeight = origin.scrollHeight;
//        height = scrollHeight - offset;
//        $origin.hide();

//        $textarea.height(height > initialHeight ? height : initialHeight);
//    }
//}(jQuery, document));

 /*
 * jquery.effects.core.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 * Copyright (c) 2010 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI/Effects/
 */
;jQuery.effects || (function($) {

$.effects = {};



/******************************************************************************/
/****************************** COLOR ANIMATIONS ******************************/
/******************************************************************************/

$.each(['backgroundColor', 'borderBottomColor', 'borderLeftColor',
	'borderRightColor', 'borderTopColor', 'color', 'outlineColor'],
function(i, attr) {
	$.fx.step[attr] = function(fx) {
		if (!fx.colorInit) {
			fx.start = getColor(fx.elem, attr);
			fx.end = getRGB(fx.end);
			fx.colorInit = true;
		}

		fx.elem.style[attr] = 'rgb(' +
			Math.max(Math.min(parseInt((fx.pos * (fx.end[0] - fx.start[0])) + fx.start[0], 10), 255), 0) + ',' +
			Math.max(Math.min(parseInt((fx.pos * (fx.end[1] - fx.start[1])) + fx.start[1], 10), 255), 0) + ',' +
			Math.max(Math.min(parseInt((fx.pos * (fx.end[2] - fx.start[2])) + fx.start[2], 10), 255), 0) + ')';
	};
});

// Color Conversion functions from highlightFade
// By Blair Mitchelmore
// http://jquery.offput.ca/highlightFade/

// Parse strings looking for color tuples [255,255,255]
function getRGB(color) {
		var result;

		// Check if we're already dealing with an array of colors
		if ( color && color.constructor == Array && color.length == 3 )
				return color;

		// Look for rgb(num,num,num)
		if (result = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(color))
				return [parseInt(result[1],10), parseInt(result[2],10), parseInt(result[3],10)];

		// Look for rgb(num%,num%,num%)
		if (result = /rgb\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*\)/.exec(color))
				return [parseFloat(result[1])*2.55, parseFloat(result[2])*2.55, parseFloat(result[3])*2.55];

		// Look for #a0b1c2
		if (result = /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(color))
				return [parseInt(result[1],16), parseInt(result[2],16), parseInt(result[3],16)];

		// Look for #fff
		if (result = /#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])/.exec(color))
				return [parseInt(result[1]+result[1],16), parseInt(result[2]+result[2],16), parseInt(result[3]+result[3],16)];

		// Look for rgba(0, 0, 0, 0) == transparent in Safari 3
		if (result = /rgba\(0, 0, 0, 0\)/.exec(color))
				return colors['transparent'];

		// Otherwise, we're most likely dealing with a named color
		return colors[$.trim(color).toLowerCase()];
}

function getColor(elem, attr) {
		var color;

		do {
				color = $.curCSS(elem, attr);

				// Keep going until we find an element that has color, or we hit the body
				if ( color != '' && color != 'transparent' || $.nodeName(elem, "body") )
						break;

				attr = "backgroundColor";
		} while ( elem = elem.parentNode );

		return getRGB(color);
};

// Some named colors to work with
// From Interface by Stefan Petre
// http://interface.eyecon.ro/

var colors = {
	aqua:[0,255,255],
	azure:[240,255,255],
	beige:[245,245,220],
	black:[0,0,0],
	blue:[0,0,255],
	brown:[165,42,42],
	cyan:[0,255,255],
	darkblue:[0,0,139],
	darkcyan:[0,139,139],
	darkgrey:[169,169,169],
	darkgreen:[0,100,0],
	darkkhaki:[189,183,107],
	darkmagenta:[139,0,139],
	darkolivegreen:[85,107,47],
	darkorange:[255,140,0],
	darkorchid:[153,50,204],
	darkred:[139,0,0],
	darksalmon:[233,150,122],
	darkviolet:[148,0,211],
	fuchsia:[255,0,255],
	gold:[255,215,0],
	green:[0,128,0],
	indigo:[75,0,130],
	khaki:[240,230,140],
	lightblue:[173,216,230],
	lightcyan:[224,255,255],
	lightgreen:[144,238,144],
	lightgrey:[211,211,211],
	lightpink:[255,182,193],
	lightyellow:[255,255,224],
	lime:[0,255,0],
	magenta:[255,0,255],
	maroon:[128,0,0],
	navy:[0,0,128],
	olive:[128,128,0],
	orange:[255,165,0],
	pink:[255,192,203],
	purple:[128,0,128],
	violet:[128,0,128],
	red:[255,0,0],
	silver:[192,192,192],
	white:[255,255,255],
	yellow:[255,255,0],
	transparent: [255,255,255]
};



/******************************************************************************/
/****************************** CLASS ANIMATIONS ******************************/
/******************************************************************************/

var classAnimationActions = ['add', 'remove', 'toggle'],
	shorthandStyles = {
		border: 1,
		borderBottom: 1,
		borderColor: 1,
		borderLeft: 1,
		borderRight: 1,
		borderTop: 1,
		borderWidth: 1,
		margin: 1,
		padding: 1
	};

function getElementStyles() {
	var style = document.defaultView
			? document.defaultView.getComputedStyle(this, null)
			: this.currentStyle,
		newStyle = {},
		key,
		camelCase;

	// webkit enumerates style porperties
	if (style && style.length && style[0] && style[style[0]]) {
		var len = style.length;
		while (len--) {
			key = style[len];
			if (typeof style[key] == 'string') {
				camelCase = key.replace(/\-(\w)/g, function(all, letter){
					return letter.toUpperCase();
				});
				newStyle[camelCase] = style[key];
			}
		}
	} else {
		for (key in style) {
			if (typeof style[key] === 'string') {
				newStyle[key] = style[key];
			}
		}
	}
	
	return newStyle;
}

function filterStyles(styles) {
	var name, value;
	for (name in styles) {
		value = styles[name];
		if (
			// ignore null and undefined values
			value == null ||
			// ignore functions (when does this occur?)
			$.isFunction(value) ||
			// shorthand styles that need to be expanded
			name in shorthandStyles ||
			// ignore scrollbars (break in IE)
			(/scrollbar/).test(name) ||

			// only colors or values that can be converted to numbers
			(!(/color/i).test(name) && isNaN(parseFloat(value)))
		) {
			delete styles[name];
		}
	}
	
	return styles;
}

function styleDifference(oldStyle, newStyle) {
	var diff = { _: 0 }, // http://dev.jquery.com/ticket/5459
		name;

	for (name in newStyle) {
		if (oldStyle[name] != newStyle[name]) {
			diff[name] = newStyle[name];
		}
	}

	return diff;
}

$.effects.animateClass = function(value, duration, easing, callback) {
	if ($.isFunction(easing)) {
		callback = easing;
		easing = null;
	}

	return this.each(function() {

		var that = $(this),
			originalStyleAttr = that.attr('style') || ' ',
			originalStyle = filterStyles(getElementStyles.call(this)),
			newStyle,
			className = that.attr('className');

		$.each(classAnimationActions, function(i, action) {
			if (value[action]) {
				that[action + 'Class'](value[action]);
			}
		});
		newStyle = filterStyles(getElementStyles.call(this));
		that.attr('className', className);

		that.animate(styleDifference(originalStyle, newStyle), duration, easing, function() {
			$.each(classAnimationActions, function(i, action) {
				if (value[action]) { that[action + 'Class'](value[action]); }
			});
			// work around bug in IE by clearing the cssText before setting it
			if (typeof that.attr('style') == 'object') {
				that.attr('style').cssText = '';
				that.attr('style').cssText = originalStyleAttr;
			} else {
				that.attr('style', originalStyleAttr);
			}
			if (callback) { callback.apply(this, arguments); }
		});
	});
};

$.fn.extend({
	_addClass: $.fn.addClass,
	addClass: function(classNames, speed, easing, callback) {
		return speed ? $.effects.animateClass.apply(this, [{ add: classNames },speed,easing,callback]) : this._addClass(classNames);
	},

	_removeClass: $.fn.removeClass,
	removeClass: function(classNames,speed,easing,callback) {
		return speed ? $.effects.animateClass.apply(this, [{ remove: classNames },speed,easing,callback]) : this._removeClass(classNames);
	},

	_toggleClass: $.fn.toggleClass,
	toggleClass: function(classNames, force, speed, easing, callback) {
		if ( typeof force == "boolean" || force === undefined ) {
			if ( !speed ) {
				// without speed parameter;
				return this._toggleClass(classNames, force);
			} else {
				return $.effects.animateClass.apply(this, [(force?{add:classNames}:{remove:classNames}),speed,easing,callback]);
			}
		} else {
			// without switch parameter;
			return $.effects.animateClass.apply(this, [{ toggle: classNames },force,speed,easing]);
		}
	},

	switchClass: function(remove,add,speed,easing,callback) {
		return $.effects.animateClass.apply(this, [{ add: add, remove: remove },speed,easing,callback]);
	}
});



/******************************************************************************/
/*********************************** EFFECTS **********************************/
/******************************************************************************/

$.extend($.effects, {
	version: "1.8.1",

	// Saves a set of properties in a data storage
	save: function(element, set) {
		for(var i=0; i < set.length; i++) {
			if(set[i] !== null) element.data("ec.storage."+set[i], element[0].style[set[i]]);
		}
	},

	// Restores a set of previously saved properties from a data storage
	restore: function(element, set) {
		for(var i=0; i < set.length; i++) {
			if(set[i] !== null) element.css(set[i], element.data("ec.storage."+set[i]));
		}
	},

	setMode: function(el, mode) {
		if (mode == 'toggle') mode = el.is(':hidden') ? 'show' : 'hide'; // Set for toggle
		return mode;
	},

	getBaseline: function(origin, original) { // Translates a [top,left] array into a baseline value
		// this should be a little more flexible in the future to handle a string & hash
		var y, x;
		switch (origin[0]) {
			case 'top': y = 0; break;
			case 'middle': y = 0.5; break;
			case 'bottom': y = 1; break;
			default: y = origin[0] / original.height;
		};
		switch (origin[1]) {
			case 'left': x = 0; break;
			case 'center': x = 0.5; break;
			case 'right': x = 1; break;
			default: x = origin[1] / original.width;
		};
		return {x: x, y: y};
	},

	// Wraps the element around a wrapper that copies position properties
	createWrapper: function(element) {

		// if the element is already wrapped, return it
		if (element.parent().is('.ui-effects-wrapper')) {
			return element.parent();
		}

		// wrap the element
		var props = {
				width: element.outerWidth(true),
				height: element.outerHeight(true),
				'float': element.css('float')
			},
			wrapper = $('<div></div>')
				.addClass('ui-effects-wrapper')
				.css({
					fontSize: '100%',
					background: 'transparent',
					border: 'none',
					margin: 0,
					padding: 0
				});

		element.wrap(wrapper);
		wrapper = element.parent(); //Hotfix for jQuery 1.4 since some change in wrap() seems to actually loose the reference to the wrapped element

		// transfer positioning properties to the wrapper
		if (element.css('position') == 'static') {
			wrapper.css({ position: 'relative' });
			element.css({ position: 'relative' });
		} else {
			$.extend(props, {
				position: element.css('position'),
				zIndex: element.css('z-index')
			});
			$.each(['top', 'left', 'bottom', 'right'], function(i, pos) {
				props[pos] = element.css(pos);
				if (isNaN(parseInt(props[pos], 10))) {
					props[pos] = 'auto';
				}
			});
			element.css({position: 'relative', top: 0, left: 0 });
		}

		return wrapper.css(props).show();
	},

	removeWrapper: function(element) {
		if (element.parent().is('.ui-effects-wrapper'))
			return element.parent().replaceWith(element);
		return element;
	},

	setTransition: function(element, list, factor, value) {
		value = value || {};
		$.each(list, function(i, x){
			unit = element.cssUnit(x);
			if (unit[0] > 0) value[x] = unit[0] * factor + unit[1];
		});
		return value;
	}
});


function _normalizeArguments(effect, options, speed, callback) {
	// shift params for method overloading
	if (typeof effect == 'object') {
		callback = options;
		speed = null;
		options = effect;
		effect = options.effect;
	}
	if ($.isFunction(options)) {
		callback = options;
		speed = null;
		options = {};
	}
	if ($.isFunction(speed)) {
		callback = speed;
		speed = null;
	}
	if (typeof options == 'number' || $.fx.speeds[options]) {
		callback = speed;
		speed = options;
		options = {};
	}

	options = options || {};

	speed = speed || options.duration;
	speed = $.fx.off ? 0 : typeof speed == 'number'
		? speed : $.fx.speeds[speed] || $.fx.speeds._default;

	callback = callback || options.complete;

	return [effect, options, speed, callback];
}

$.fn.extend({
	effect: function(effect, options, speed, callback) {
		var args = _normalizeArguments.apply(this, arguments),
			// TODO: make effects takes actual parameters instead of a hash
			args2 = {
				options: args[1],
				duration: args[2],
				callback: args[3]
			},
			effectMethod = $.effects[effect];
		
		return effectMethod && !$.fx.off ? effectMethod.call(this, args2) : this;
	},

	_show: $.fn.show,
	show: function(speed) {
		if (!speed || typeof speed == 'number' || $.fx.speeds[speed]) {
			return this._show.apply(this, arguments);
		} else {
			var args = _normalizeArguments.apply(this, arguments);
			args[1].mode = 'show';
			return this.effect.apply(this, args);
		}
	},

	_hide: $.fn.hide,
	hide: function(speed) {
		if (!speed || typeof speed == 'number' || $.fx.speeds[speed]) {
			return this._hide.apply(this, arguments);
		} else {
			var args = _normalizeArguments.apply(this, arguments);
			args[1].mode = 'hide';
			return this.effect.apply(this, args);
		}
	},

	// jQuery core overloads toggle and create _toggle
	__toggle: $.fn.toggle,
	toggle: function(speed) {
		if (!speed || typeof speed == 'number' || $.fx.speeds[speed] ||
			typeof speed == 'boolean' || $.isFunction(speed)) {
			return this.__toggle.apply(this, arguments);
		} else {
			var args = _normalizeArguments.apply(this, arguments);
			args[1].mode = 'toggle';
			return this.effect.apply(this, args);
		}
	},

	// helper functions
	cssUnit: function(key) {
		var style = this.css(key), val = [];
		$.each( ['em','px','%','pt'], function(i, unit){
			if(style.indexOf(unit) > 0)
				val = [parseFloat(style), unit];
		});
		return val;
	}
});



/******************************************************************************/
/*********************************** EASING ***********************************/
/******************************************************************************/

/*
 * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
 *
 * Uses the built in easing capabilities added In jQuery 1.1
 * to offer multiple easing options
 *
 * TERMS OF USE - jQuery Easing
 *
 * Open source under the BSD License.
 *
 * Copyright 2008 George McGinley Smith
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list
 * of conditions and the following disclaimer in the documentation and/or other materials
 * provided with the distribution.
 *
 * Neither the name of the author nor the names of contributors may be used to endorse
 * or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

// t: current time, b: begInnIng value, c: change In value, d: duration
$.easing.jswing = $.easing.swing;

$.extend($.easing,
{
	def: 'easeOutQuad',
	swing: function (x, t, b, c, d) {
		//alert($.easing.default);
		return $.easing[$.easing.def](x, t, b, c, d);
	},
	easeInQuad: function (x, t, b, c, d) {
		return c*(t/=d)*t + b;
	},
	easeOutQuad: function (x, t, b, c, d) {
		return -c *(t/=d)*(t-2) + b;
	},
	easeInOutQuad: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t + b;
		return -c/2 * ((--t)*(t-2) - 1) + b;
	},
	easeInCubic: function (x, t, b, c, d) {
		return c*(t/=d)*t*t + b;
	},
	easeOutCubic: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t + 1) + b;
	},
	easeInOutCubic: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t + b;
		return c/2*((t-=2)*t*t + 2) + b;
	},
	easeInQuart: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t + b;
	},
	easeOutQuart: function (x, t, b, c, d) {
		return -c * ((t=t/d-1)*t*t*t - 1) + b;
	},
	easeInOutQuart: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
		return -c/2 * ((t-=2)*t*t*t - 2) + b;
	},
	easeInQuint: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t*t + b;
	},
	easeOutQuint: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t*t*t + 1) + b;
	},
	easeInOutQuint: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
		return c/2*((t-=2)*t*t*t*t + 2) + b;
	},
	easeInSine: function (x, t, b, c, d) {
		return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
	},
	easeOutSine: function (x, t, b, c, d) {
		return c * Math.sin(t/d * (Math.PI/2)) + b;
	},
	easeInOutSine: function (x, t, b, c, d) {
		return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
	},
	easeInExpo: function (x, t, b, c, d) {
		return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
	},
	easeOutExpo: function (x, t, b, c, d) {
		return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
	},
	easeInOutExpo: function (x, t, b, c, d) {
		if (t==0) return b;
		if (t==d) return b+c;
		if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
		return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
	},
	easeInCirc: function (x, t, b, c, d) {
		return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
	},
	easeOutCirc: function (x, t, b, c, d) {
		return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
	},
	easeInOutCirc: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
		return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
	},
	easeInElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
	},
	easeOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
	},
	easeInOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
		return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
	},
	easeInBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*(t/=d)*t*((s+1)*t - s) + b;
	},
	easeOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
	},
	easeInOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
		return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
	},
	easeInBounce: function (x, t, b, c, d) {
		return c - $.easing.easeOutBounce (x, d-t, 0, c, d) + b;
	},
	easeOutBounce: function (x, t, b, c, d) {
		if ((t/=d) < (1/2.75)) {
			return c*(7.5625*t*t) + b;
		} else if (t < (2/2.75)) {
			return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
		} else if (t < (2.5/2.75)) {
			return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
		} else {
			return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
		}
	},
	easeInOutBounce: function (x, t, b, c, d) {
		if (t < d/2) return $.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
		return $.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
	}
});

/*
 *
 * TERMS OF USE - EASING EQUATIONS
 *
 * Open source under the BSD License.
 *
 * Copyright 2001 Robert Penner
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list
 * of conditions and the following disclaimer in the documentation and/or other materials
 * provided with the distribution.
 *
 * Neither the name of the author nor the names of contributors may be used to endorse
 * or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

})(jQuery);








/*
 * jQuery UI Effects Slide 1.8.1
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 * Copyright (c) 2010 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI/Effects/Slide
 *
 * Depends:
 *	jquery.effects.core.js
 */


 (function($) {

$.effects.slide = function(o) {

	return this.queue(function() {

		// Create element
		var el = $(this), props = ['position','top','left'];

		// Set options
		var mode = $.effects.setMode(el, o.options.mode || 'show'); // Set Mode
		var direction = o.options.direction || 'left'; // Default Direction

		// Adjust
		$.effects.save(el, props); el.show(); // Save & Show
		$.effects.createWrapper(el).css({overflow:'hidden'}); // Create Wrapper
		var ref = (direction == 'up' || direction == 'down') ? 'top' : 'left';
		var motion = (direction == 'up' || direction == 'left') ? 'pos' : 'neg';
		var distance = o.options.distance || (ref == 'top' ? el.outerHeight({margin:true}) : el.outerWidth({margin:true}));
		if (mode == 'show') el.css(ref, motion == 'pos' ? -distance : distance); // Shift

		// Animation
		var animation = {};
		animation[ref] = (mode == 'show' ? (motion == 'pos' ? '+=' : '-=') : (motion == 'pos' ? '-=' : '+=')) + distance;

		// Animate
		el.animate(animation, { queue: false, duration: o.duration, easing: o.options.easing, complete: function() {
			if(mode == 'hide') el.hide(); // Hide
			$.effects.restore(el, props); $.effects.removeWrapper(el); // Restore
			if(o.callback) o.callback.apply(this, arguments); // Callback
			el.dequeue();
		}});

	});

};

})(jQuery);



/*
 * tabmenu.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 */

 $(function () {



    $("#iddash").click(function (event) {
        $(".ui-tabs-selected").removeClass("ui-tabs-selected");
        $(this).parent().addClass("ui-tabs-selected");

        location.href = "../Portfolio/Portfolio.aspx";
    });

     $("#idOperationDash").click(function (event) {
        $(".ui-tabs-selected").removeClass("ui-tabs-selected");
        $(this).parent().addClass("ui-tabs-selected");
      
        location.href = "../Portfolio/OperationDashboard.aspx";
    });

     $("#idSalesDash").click(function (event) {
        $(".ui-tabs-selected").removeClass("ui-tabs-selected");
        $(this).parent().addClass("ui-tabs-selected");
      
        location.href = "../Portfolio/SalesDashboard.aspx";
    });

     $("#idCreditDash").click(function (event) {
        $(".ui-tabs-selected").removeClass("ui-tabs-selected");
        $(this).parent().addClass("ui-tabs-selected");
      
        location.href = "../Portfolio/CreditDashboard.aspx";
    });


     $("#idAccountDash").click(function (event) {
        $(".ui-tabs-selected").removeClass("ui-tabs-selected");
        $(this).parent().addClass("ui-tabs-selected");
      
        location.href = "../Portfolio/AccountingDashboard.aspx";
    });


    //modified by Bharati
    $("#idrpt").click(function (event) {
        $(".ui-tabs-selected").removeClass("ui-tabs-selected");
        $("#idrpt").parent().addClass("ui-tabs-selected");
        location.href ="../Reports/DefaultReport.aspx?ActiveTab=Other Reports";    
    });
    //-------------------------------------------------------------------------

    $("#idsales").click(function (event) {
        $(".ui-tabs-selected").removeClass("ui-tabs-selected");
        $(this).parent().addClass("ui-tabs-selected");
      
        location.href = "../Queue/QueueViewData.aspx";
    });

    $("#idcredit").click(function (event) {
        $(".ui-tabs-selected").removeClass("ui-tabs-selected");
        $(this).parent().addClass("ui-tabs-selected");
       
        location.href = "#";
    });

    $("#idoper").click(function (event) {
        $(".ui-tabs-selected").removeClass("ui-tabs-selected");
        $(this).parent().addClass("ui-tabs-selected");
       
        location.href = "#";
    });

    $("#idact").click(function (event) {
        $(".ui-tabs-selected").removeClass("ui-tabs-selected");
        $(this).parent().addClass("ui-tabs-selected");
       
         location.href = "../Accounting/LedgerDetails.aspx?ActiveTab=Ledger";
    });

    $("#idrisk").click(function (event) {
        $(".ui-tabs-selected").removeClass("ui-tabs-selected");
        $(this).parent().addClass("ui-tabs-selected");
       
        location.href = "#";
    });

    $("#idservice").click(function (event) {
        $(".ui-tabs-selected").removeClass("ui-tabs-selected");
        $(this).parent().addClass("ui-tabs-selected");
       
        location.href = "#";
    });

    $("#idportfolio").click(function (event) {
        $(".ui-tabs-selected").removeClass("ui-tabs-selected");
        $(this).parent().addClass("ui-tabs-selected");
      
        location.href = "#";
    });

  $("#idsetup").click(function (event) {
        $(".ui-tabs-selected").removeClass("ui-tabs-selected");
        $(this).parent().addClass("ui-tabs-selected");
       location.href = "../RoleManagement/User_Management.aspx?ActiveTab=Users";
       // location.href = "../RoleManagement/rolemanagement.aspx";
    });

    $("#idAutomation").click(function (event) {
        $(".ui-tabs-selected").removeClass("ui-tabs-selected");
        $(this).parent().addClass("ui-tabs-selected");
       location.href = "../GlobalFileProcessor/AddNewTemplate.aspx?ActiveTab=Import Setup";
       
    });



    $("#idAgent").click(function (event) {
        $(".ui-tabs-selected").removeClass("ui-tabs-selected");
        $(this).parent().addClass("ui-tabs-selected");
      
        location.href = "../Agent/AgentManagement.aspx?ActiveTab=Inbox";
    });


      $("#idcalendar").click(function (event) {
        
        $(".ui-tabs-selected").removeClass("ui-tabs-selected");
        $(this).parent().addClass("ui-tabs-selected");
      
        location.href = "../Calendar/CalendarView.aspx?ActiveTab=Inbox";
    });


      $("#idARReport").click(function (event) {
        
        $(".ui-tabs-selected").removeClass("ui-tabs-selected");
        $(this).parent().addClass("ui-tabs-selected");
      
       location.href = "../Portfolio/ARReport.aspx";
    });

     $("#idBranch").click(function (event) {
        $(".ui-tabs-selected").removeClass("ui-tabs-selected");
        $(this).parent().addClass("ui-tabs-selected");
       location.href = "../Branch/BranchInformation.aspx?ActiveTab=Inbox";
       
    });

});



/*
 * jquery.mousewheel.min.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
  Copyright (c) 2011 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.0.6
 * 
 * Requires: 1.2.2+
 */
(function(d){var b=["DOMMouseScroll","mousewheel"];if(d.event.fixHooks){for(var a=b.length;a;){d.event.fixHooks[b[--a]]=d.event.mouseHooks}}d.event.special.mousewheel={setup:function(){if(this.addEventListener){for(var e=b.length;e;){this.addEventListener(b[--e],c,false)}}else{this.onmousewheel=c}},teardown:function(){if(this.removeEventListener){for(var e=b.length;e;){this.removeEventListener(b[--e],c,false)}}else{this.onmousewheel=null}}};d.fn.extend({mousewheel:function(e){return e?this.bind("mousewheel",e):this.trigger("mousewheel")},unmousewheel:function(e){return this.unbind("mousewheel",e)}});function c(j){var h=j||window.event,g=[].slice.call(arguments,1),k=0,i=true,f=0,e=0;j=d.event.fix(h);j.type="mousewheel";if(h.wheelDelta){k=h.wheelDelta/120}if(h.detail){k=-h.detail/3}e=k;if(h.axis!==undefined&&h.axis===h.HORIZONTAL_AXIS){e=0;f=-1*k}if(h.wheelDeltaY!==undefined){e=h.wheelDeltaY/120}if(h.wheelDeltaX!==undefined){f=-1*h.wheelDeltaX/120}g.unshift(j,k,f,e);return(d.event.dispatch||d.event.handle).apply(this,g)}})(jQuery);




 
/*
 * jquery.fileinput.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
* --------------------------------------------------------------------
 * jQuery customfileinput plugin
 * Author: Scott Jehl, scott@filamentgroup.com
 * Copyright (c) 2009 Filament Group 
 * licensed under MIT (filamentgroup.com/examples/mit-license.txt)
 * --------------------------------------------------------------------
 * Modified by maimairel (maimairel@yahoo.com) for use in a ThemeForest theme
 * --------------------------------------------------------------------
 */
 
$.fn.customFileInput = function(){
	
	return $(this).each(function() {
		var fileInput = $(this)
			.addClass('customfile-input') 
			.mouseover(function(){ upload.addClass('customfile-hover'); })
			.mouseout(function(){ upload.removeClass('customfile-hover'); })
			.focus(function(){
				upload.addClass('customfile-focus'); 
				fileInput.data('val', fileInput.val());
			})
			.blur(function(){ 
				upload.removeClass('customfile-focus');
				$(this).trigger('checkChange');
			 })
			 .bind('disable',function(){
				fileInput.attr('disabled',true);
				upload.addClass('customfile-disabled');
			})
			.bind('enable',function(){
				fileInput.removeAttr('disabled');
				upload.removeClass('customfile-disabled');
			})
			.bind('checkChange', function(){
				if(fileInput.val() && fileInput.val() != fileInput.data('val')){
					fileInput.trigger('change');
				}
			})
			.bind('change',function(){
				
				var fileName = $(this).val().split(/\\/).pop();
				
				var fileExt = 'customfile-ext-' + fileName.split('.').pop().toLowerCase();
				
				uploadFeedback
					.text(fileName) 
					.removeClass(uploadFeedback.data('fileExt') || '') 
					.addClass(fileExt) 
					.data('fileExt', fileExt) 
					.addClass('customfile-feedback-populated'); 
				
				uploadButton.text('Change');	
			})
			.click(function(){ 
				fileInput.data('val', fileInput.val());
				setTimeout(function(){
					fileInput.trigger('checkChange');
				},100);
			});
			
		
		var upload = $('<div class="customfile"></div>');
		
		var uploadButton = $('<span class="customfile-button" aria-hidden="true">Browse</span>').appendTo(upload);
		
		var uploadFeedback = $('<span class="customfile-feedback" aria-hidden="true">No file selected...</span>').appendTo(upload);
		
		
		if(fileInput.is('[disabled]')){
			fileInput.trigger('disable');
		}
			
		
		
		upload
			.mousemove(function(e){
				fileInput.css({
					'left': e.pageX - upload.offset().left - fileInput.outerWidth() + 20, 
					'top': e.pageY - upload.offset().top - 3
				});
			})
			.insertAfter(fileInput);
		
		fileInput.appendTo(upload);
	});
};





  
/*
 * jquery.ui.timepicker.min.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 * jQuery timepicker addon
* By: Trent Richardson [http://trentrichardson.com]
* Version 1.0.0
* Last Modified: 02/05/2012
*
* Copyright 2012 Trent Richardson
* Dual licensed under the MIT and GPL licenses.
* http://trentrichardson.com/Impromptu/GPL-LICENSE.txt
* http://trentrichardson.com/Impromptu/MIT-LICENSE.txt
*
* HERES THE CSS:
* .ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
* .ui-timepicker-div dl { text-align: left; }
* .ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; }
* .ui-timepicker-div dl dd { margin: 0 10px 10px 65px; }
* .ui-timepicker-div td { font-size: 90%; }
* .ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }
*/

; (function ($) { $.ui.timepicker = $.ui.timepicker || {}; if ($.ui.timepicker.version) { return } $.extend($.ui, { timepicker: { version: "1.0.0"} }); function Timepicker() { this.regional = []; this.regional[""] = { currentText: "Now", closeText: "Done", ampm: false, amNames: ["AM", "A"], pmNames: ["PM", "P"], timeFormat: "hh:mm tt", timeSuffix: "", timeOnlyTitle: "Choose Time", timeText: "Time", hourText: "Hour", minuteText: "Minute", secondText: "Second", millisecText: "Millisecond", timezoneText: "Time Zone" }; this._defaults = { showButtonPanel: true, timeOnly: false, showHour: true, showMinute: true, showSecond: false, showMillisec: false, showTimezone: false, showTime: true, stepHour: 1, stepMinute: 1, stepSecond: 1, stepMillisec: 1, hour: 0, minute: 0, second: 0, millisec: 0, timezone: "+0000", hourMin: 0, minuteMin: 0, secondMin: 0, millisecMin: 0, hourMax: 23, minuteMax: 59, secondMax: 59, millisecMax: 999, minDateTime: null, maxDateTime: null, onSelect: null, hourGrid: 0, minuteGrid: 0, secondGrid: 0, millisecGrid: 0, alwaysSetTime: true, separator: " ", altFieldTimeOnly: true, showTimepicker: true, timezoneIso8609: false, timezoneList: null, addSliderAccess: false, sliderAccessArgs: null }; $.extend(this._defaults, this.regional[""]) } $.extend(Timepicker.prototype, { $input: null, $altInput: null, $timeObj: null, inst: null, hour_slider: null, minute_slider: null, second_slider: null, millisec_slider: null, timezone_select: null, hour: 0, minute: 0, second: 0, millisec: 0, timezone: "+0000", hourMinOriginal: null, minuteMinOriginal: null, secondMinOriginal: null, millisecMinOriginal: null, hourMaxOriginal: null, minuteMaxOriginal: null, secondMaxOriginal: null, millisecMaxOriginal: null, ampm: "", formattedDate: "", formattedTime: "", formattedDateTime: "", timezoneList: null, setDefaults: function (settings) { extendRemove(this._defaults, settings || {}); return this }, _newInst: function ($input, o) { var tp_inst = new Timepicker(), inlineSettings = {}; for (var attrName in this._defaults) { var attrValue = $input.attr("time:" + attrName); if (attrValue) { try { inlineSettings[attrName] = eval(attrValue) } catch (err) { inlineSettings[attrName] = attrValue } } } tp_inst._defaults = $.extend({}, this._defaults, inlineSettings, o, { beforeShow: function (input, dp_inst) { if ($.isFunction(o.beforeShow)) { return o.beforeShow(input, dp_inst, tp_inst) } }, onChangeMonthYear: function (year, month, dp_inst) { tp_inst._updateDateTime(dp_inst); if ($.isFunction(o.onChangeMonthYear)) { o.onChangeMonthYear.call($input[0], year, month, dp_inst, tp_inst) } }, onClose: function (dateText, dp_inst) { if (tp_inst.timeDefined === true && $input.val() != "") { tp_inst._updateDateTime(dp_inst) } if ($.isFunction(o.onClose)) { o.onClose.call($input[0], dateText, dp_inst, tp_inst) } }, timepicker: tp_inst }); tp_inst.amNames = $.map(tp_inst._defaults.amNames, function (val) { return val.toUpperCase() }); tp_inst.pmNames = $.map(tp_inst._defaults.pmNames, function (val) { return val.toUpperCase() }); if (tp_inst._defaults.timezoneList === null) { var timezoneList = []; for (var i = -11; i <= 12; i++) { timezoneList.push((i >= 0 ? "+" : "-") + ("0" + Math.abs(i).toString()).slice(-2) + "00") } if (tp_inst._defaults.timezoneIso8609) { timezoneList = $.map(timezoneList, function (val) { return val == "+0000" ? "Z" : (val.substring(0, 3) + ":" + val.substring(3)) }) } tp_inst._defaults.timezoneList = timezoneList } tp_inst.hour = tp_inst._defaults.hour; tp_inst.minute = tp_inst._defaults.minute; tp_inst.second = tp_inst._defaults.second; tp_inst.millisec = tp_inst._defaults.millisec; tp_inst.ampm = ""; tp_inst.$input = $input; if (o.altField) { tp_inst.$altInput = $(o.altField).css({ cursor: "pointer" }).focus(function () { $input.trigger("focus") }) } if (tp_inst._defaults.minDate == 0 || tp_inst._defaults.minDateTime == 0) { tp_inst._defaults.minDate = new Date() } if (tp_inst._defaults.maxDate == 0 || tp_inst._defaults.maxDateTime == 0) { tp_inst._defaults.maxDate = new Date() } if (tp_inst._defaults.minDate !== undefined && tp_inst._defaults.minDate instanceof Date) { tp_inst._defaults.minDateTime = new Date(tp_inst._defaults.minDate.getTime()) } if (tp_inst._defaults.minDateTime !== undefined && tp_inst._defaults.minDateTime instanceof Date) { tp_inst._defaults.minDate = new Date(tp_inst._defaults.minDateTime.getTime()) } if (tp_inst._defaults.maxDate !== undefined && tp_inst._defaults.maxDate instanceof Date) { tp_inst._defaults.maxDateTime = new Date(tp_inst._defaults.maxDate.getTime()) } if (tp_inst._defaults.maxDateTime !== undefined && tp_inst._defaults.maxDateTime instanceof Date) { tp_inst._defaults.maxDate = new Date(tp_inst._defaults.maxDateTime.getTime()) } return tp_inst }, _addTimePicker: function (dp_inst) { var currDT = (this.$altInput && this._defaults.altFieldTimeOnly) ? this.$input.val() + " " + this.$altInput.val() : this.$input.val(); this.timeDefined = this._parseTime(currDT); this._limitMinMaxDateTime(dp_inst, false); this._injectTimePicker() }, _parseTime: function (timeString, withDate) { var regstr = this._defaults.timeFormat.toString().replace(/h{1,2}/ig, "(\\d?\\d)").replace(/m{1,2}/ig, "(\\d?\\d)").replace(/s{1,2}/ig, "(\\d?\\d)").replace(/l{1}/ig, "(\\d?\\d?\\d)").replace(/t{1,2}/ig, this._getPatternAmpm()).replace(/z{1}/ig, "(z|[-+]\\d\\d:?\\d\\d)?").replace(/\s/g, "\\s?") + this._defaults.timeSuffix + "$", order = this._getFormatPositions(), ampm = "", treg; if (!this.inst) { this.inst = $.datepicker._getInst(this.$input[0]) } if (withDate || !this._defaults.timeOnly) { var dp_dateFormat = $.datepicker._get(this.inst, "dateFormat"); var specials = new RegExp("[.*+?|()\\[\\]{}\\\\]", "g"); regstr = "^.{" + dp_dateFormat.length + ",}?" + this._defaults.separator.replace(specials, "\\$&") + regstr } treg = timeString.match(new RegExp(regstr, "i")); if (treg) { if (order.t !== -1) { if (treg[order.t] === undefined || treg[order.t].length === 0) { ampm = ""; this.ampm = "" } else { ampm = $.inArray(treg[order.t].toUpperCase(), this.amNames) !== -1 ? "AM" : "PM"; this.ampm = this._defaults[ampm == "AM" ? "amNames" : "pmNames"][0] } } if (order.h !== -1) { if (ampm == "AM" && treg[order.h] == "12") { this.hour = 0 } else { if (ampm == "PM" && treg[order.h] != "12") { this.hour = (parseFloat(treg[order.h]) + 12).toFixed(0) } else { this.hour = Number(treg[order.h]) } } } if (order.m !== -1) { this.minute = Number(treg[order.m]) } if (order.s !== -1) { this.second = Number(treg[order.s]) } if (order.l !== -1) { this.millisec = Number(treg[order.l]) } if (order.z !== -1 && treg[order.z] !== undefined) { var tz = treg[order.z].toUpperCase(); switch (tz.length) { case 1: tz = this._defaults.timezoneIso8609 ? "Z" : "+0000"; break; case 5: if (this._defaults.timezoneIso8609) { tz = tz.substring(1) == "0000" ? "Z" : tz.substring(0, 3) + ":" + tz.substring(3) } break; case 6: if (!this._defaults.timezoneIso8609) { tz = tz == "Z" || tz.substring(1) == "00:00" ? "+0000" : tz.replace(/:/, "") } else { if (tz.substring(1) == "00:00") { tz = "Z" } } break } this.timezone = tz } return true } return false }, _getPatternAmpm: function () { var markers = [], o = this._defaults; if (o.amNames) { $.merge(markers, o.amNames) } if (o.pmNames) { $.merge(markers, o.pmNames) } markers = $.map(markers, function (val) { return val.replace(/[.*+?|()\[\]{}\\]/g, "\\$&") }); return "(" + markers.join("|") + ")?" }, _getFormatPositions: function () { var finds = this._defaults.timeFormat.toLowerCase().match(/(h{1,2}|m{1,2}|s{1,2}|l{1}|t{1,2}|z)/g), orders = { h: -1, m: -1, s: -1, l: -1, t: -1, z: -1 }; if (finds) { for (var i = 0; i < finds.length; i++) { if (orders[finds[i].toString().charAt(0)] == -1) { orders[finds[i].toString().charAt(0)] = i + 1 } } } return orders }, _injectTimePicker: function () { var $dp = this.inst.dpDiv, o = this._defaults, tp_inst = this, hourMax = parseInt((o.hourMax - ((o.hourMax - o.hourMin) % o.stepHour)), 10), minMax = parseInt((o.minuteMax - ((o.minuteMax - o.minuteMin) % o.stepMinute)), 10), secMax = parseInt((o.secondMax - ((o.secondMax - o.secondMin) % o.stepSecond)), 10), millisecMax = parseInt((o.millisecMax - ((o.millisecMax - o.millisecMin) % o.stepMillisec)), 10), dp_id = this.inst.id.toString().replace(/([^A-Za-z0-9_])/g, ""); if ($dp.find("div#ui-timepicker-div-" + dp_id).length === 0 && o.showTimepicker) { var noDisplay = ' style="display:none;"', html = '<div class="ui-timepicker-div" id="ui-timepicker-div-' + dp_id + '"><dl><dt class="ui_tpicker_time_label" id="ui_tpicker_time_label_' + dp_id + '"' + ((o.showTime) ? "" : noDisplay) + ">" + o.timeText + '</dt><dd class="ui_tpicker_time" id="ui_tpicker_time_' + dp_id + '"' + ((o.showTime) ? "" : noDisplay) + '></dd><dt class="ui_tpicker_hour_label" id="ui_tpicker_hour_label_' + dp_id + '"' + ((o.showHour) ? "" : noDisplay) + ">" + o.hourText + "</dt>", hourGridSize = 0, minuteGridSize = 0, secondGridSize = 0, millisecGridSize = 0, size = null; html += '<dd class="ui_tpicker_hour"><div id="ui_tpicker_hour_' + dp_id + '"' + ((o.showHour) ? "" : noDisplay) + "></div>"; if (o.showHour && o.hourGrid > 0) { html += '<div style="padding-left: 1px"><table class="ui-tpicker-grid-label"><tr>'; for (var h = o.hourMin; h <= hourMax; h += parseInt(o.hourGrid, 10)) { hourGridSize++; var tmph = (o.ampm && h > 12) ? h - 12 : h; if (tmph < 10) { tmph = "0" + tmph } if (o.ampm) { if (h == 0) { tmph = 12 + "a" } else { if (h < 12) { tmph += "a" } else { tmph += "p" } } } html += "<td>" + tmph + "</td>" } html += "</tr></table></div>" } html += "</dd>"; html += '<dt class="ui_tpicker_minute_label" id="ui_tpicker_minute_label_' + dp_id + '"' + ((o.showMinute) ? "" : noDisplay) + ">" + o.minuteText + '</dt><dd class="ui_tpicker_minute"><div id="ui_tpicker_minute_' + dp_id + '"' + ((o.showMinute) ? "" : noDisplay) + "></div>"; if (o.showMinute && o.minuteGrid > 0) { html += '<div style="padding-left: 1px"><table class="ui-tpicker-grid-label"><tr>'; for (var m = o.minuteMin; m <= minMax; m += parseInt(o.minuteGrid, 10)) { minuteGridSize++; html += "<td>" + ((m < 10) ? "0" : "") + m + "</td>" } html += "</tr></table></div>" } html += "</dd>"; html += '<dt class="ui_tpicker_second_label" id="ui_tpicker_second_label_' + dp_id + '"' + ((o.showSecond) ? "" : noDisplay) + ">" + o.secondText + '</dt><dd class="ui_tpicker_second"><div id="ui_tpicker_second_' + dp_id + '"' + ((o.showSecond) ? "" : noDisplay) + "></div>"; if (o.showSecond && o.secondGrid > 0) { html += '<div style="padding-left: 1px"><table><tr>'; for (var s = o.secondMin; s <= secMax; s += parseInt(o.secondGrid, 10)) { secondGridSize++; html += "<td>" + ((s < 10) ? "0" : "") + s + "</td>" } html += "</tr></table></div>" } html += "</dd>"; html += '<dt class="ui_tpicker_millisec_label" id="ui_tpicker_millisec_label_' + dp_id + '"' + ((o.showMillisec) ? "" : noDisplay) + ">" + o.millisecText + '</dt><dd class="ui_tpicker_millisec"><div id="ui_tpicker_millisec_' + dp_id + '"' + ((o.showMillisec) ? "" : noDisplay) + "></div>"; if (o.showMillisec && o.millisecGrid > 0) { html += '<div style="padding-left: 1px"><table><tr>'; for (var l = o.millisecMin; l <= millisecMax; l += parseInt(o.millisecGrid, 10)) { millisecGridSize++; html += "<td>" + ((l < 10) ? "0" : "") + l + "</td>" } html += "</tr></table></div>" } html += "</dd>"; html += '<dt class="ui_tpicker_timezone_label" id="ui_tpicker_timezone_label_' + dp_id + '"' + ((o.showTimezone) ? "" : noDisplay) + ">" + o.timezoneText + "</dt>"; html += '<dd class="ui_tpicker_timezone" id="ui_tpicker_timezone_' + dp_id + '"' + ((o.showTimezone) ? "" : noDisplay) + "></dd>"; html += "</dl></div>"; $tp = $(html); if (o.timeOnly === true) { $tp.prepend('<div class="ui-widget-header ui-helper-clearfix ui-corner-all"><div class="ui-datepicker-title">' + o.timeOnlyTitle + "</div></div>"); $dp.find(".ui-datepicker-header, .ui-datepicker-calendar").hide() } this.hour_slider = $tp.find("#ui_tpicker_hour_" + dp_id).slider({ orientation: "horizontal", value: this.hour, min: o.hourMin, max: hourMax, step: o.stepHour, slide: function (event, ui) { tp_inst.hour_slider.slider("option", "value", ui.value); tp_inst._onTimeChange() } }); this.minute_slider = $tp.find("#ui_tpicker_minute_" + dp_id).slider({ orientation: "horizontal", value: this.minute, min: o.minuteMin, max: minMax, step: o.stepMinute, slide: function (event, ui) { tp_inst.minute_slider.slider("option", "value", ui.value); tp_inst._onTimeChange() } }); this.second_slider = $tp.find("#ui_tpicker_second_" + dp_id).slider({ orientation: "horizontal", value: this.second, min: o.secondMin, max: secMax, step: o.stepSecond, slide: function (event, ui) { tp_inst.second_slider.slider("option", "value", ui.value); tp_inst._onTimeChange() } }); this.millisec_slider = $tp.find("#ui_tpicker_millisec_" + dp_id).slider({ orientation: "horizontal", value: this.millisec, min: o.millisecMin, max: millisecMax, step: o.stepMillisec, slide: function (event, ui) { tp_inst.millisec_slider.slider("option", "value", ui.value); tp_inst._onTimeChange() } }); this.timezone_select = $tp.find("#ui_tpicker_timezone_" + dp_id).append("<select></select>").find("select"); $.fn.append.apply(this.timezone_select, $.map(o.timezoneList, function (val, idx) { return $("<option />").val(typeof val == "object" ? val.value : val).text(typeof val == "object" ? val.label : val) })); this.timezone_select.val((typeof this.timezone != "undefined" && this.timezone != null && this.timezone != "") ? this.timezone : o.timezone); this.timezone_select.change(function () { tp_inst._onTimeChange() }); if (o.showHour && o.hourGrid > 0) { size = 100 * hourGridSize * o.hourGrid / (hourMax - o.hourMin); $tp.find(".ui_tpicker_hour table").css({ width: size + "%", marginLeft: (size / (-2 * hourGridSize)) + "%", borderCollapse: "collapse" }).find("td").each(function (index) { $(this).click(function () { var h = $(this).html(); if (o.ampm) { var ap = h.substring(2).toLowerCase(), aph = parseInt(h.substring(0, 2), 10); if (ap == "a") { if (aph == 12) { h = 0 } else { h = aph } } else { if (aph == 12) { h = 12 } else { h = aph + 12 } } } tp_inst.hour_slider.slider("option", "value", h); tp_inst._onTimeChange(); tp_inst._onSelectHandler() }).css({ cursor: "pointer", width: (100 / hourGridSize) + "%", textAlign: "center", overflow: "hidden" }) }) } if (o.showMinute && o.minuteGrid > 0) { size = 100 * minuteGridSize * o.minuteGrid / (minMax - o.minuteMin); $tp.find(".ui_tpicker_minute table").css({ width: size + "%", marginLeft: (size / (-2 * minuteGridSize)) + "%", borderCollapse: "collapse" }).find("td").each(function (index) { $(this).click(function () { tp_inst.minute_slider.slider("option", "value", $(this).html()); tp_inst._onTimeChange(); tp_inst._onSelectHandler() }).css({ cursor: "pointer", width: (100 / minuteGridSize) + "%", textAlign: "center", overflow: "hidden" }) }) } if (o.showSecond && o.secondGrid > 0) { $tp.find(".ui_tpicker_second table").css({ width: size + "%", marginLeft: (size / (-2 * secondGridSize)) + "%", borderCollapse: "collapse" }).find("td").each(function (index) { $(this).click(function () { tp_inst.second_slider.slider("option", "value", $(this).html()); tp_inst._onTimeChange(); tp_inst._onSelectHandler() }).css({ cursor: "pointer", width: (100 / secondGridSize) + "%", textAlign: "center", overflow: "hidden" }) }) } if (o.showMillisec && o.millisecGrid > 0) { $tp.find(".ui_tpicker_millisec table").css({ width: size + "%", marginLeft: (size / (-2 * millisecGridSize)) + "%", borderCollapse: "collapse" }).find("td").each(function (index) { $(this).click(function () { tp_inst.millisec_slider.slider("option", "value", $(this).html()); tp_inst._onTimeChange(); tp_inst._onSelectHandler() }).css({ cursor: "pointer", width: (100 / millisecGridSize) + "%", textAlign: "center", overflow: "hidden" }) }) } var $buttonPanel = $dp.find(".ui-datepicker-buttonpane"); if ($buttonPanel.length) { $buttonPanel.before($tp) } else { $dp.append($tp) } this.$timeObj = $tp.find("#ui_tpicker_time_" + dp_id); if (this.inst !== null) { var timeDefined = this.timeDefined; this._onTimeChange(); this.timeDefined = timeDefined } var onSelectDelegate = function () { tp_inst._onSelectHandler() }; this.hour_slider.bind("slidestop", onSelectDelegate); this.minute_slider.bind("slidestop", onSelectDelegate); this.second_slider.bind("slidestop", onSelectDelegate); this.millisec_slider.bind("slidestop", onSelectDelegate); if (this._defaults.addSliderAccess) { var sliderAccessArgs = this._defaults.sliderAccessArgs; setTimeout(function () { if ($tp.find(".ui-slider-access").length == 0) { $tp.find(".ui-slider:visible").sliderAccess(sliderAccessArgs); var sliderAccessWidth = $tp.find(".ui-slider-access:eq(0)").outerWidth(true); if (sliderAccessWidth) { $tp.find("table:visible").each(function () { var $g = $(this), oldWidth = $g.outerWidth(), oldMarginLeft = $g.css("marginLeft").toString().replace("%", ""), newWidth = oldWidth - sliderAccessWidth, newMarginLeft = ((oldMarginLeft * newWidth) / oldWidth) + "%"; $g.css({ width: newWidth, marginLeft: newMarginLeft }) }) } } }, 0) } } }, _limitMinMaxDateTime: function (dp_inst, adjustSliders) { var o = this._defaults, dp_date = new Date(dp_inst.selectedYear, dp_inst.selectedMonth, dp_inst.selectedDay); if (!this._defaults.showTimepicker) { return } if ($.datepicker._get(dp_inst, "minDateTime") !== null && $.datepicker._get(dp_inst, "minDateTime") !== undefined && dp_date) { var minDateTime = $.datepicker._get(dp_inst, "minDateTime"), minDateTimeDate = new Date(minDateTime.getFullYear(), minDateTime.getMonth(), minDateTime.getDate(), 0, 0, 0, 0); if (this.hourMinOriginal === null || this.minuteMinOriginal === null || this.secondMinOriginal === null || this.millisecMinOriginal === null) { this.hourMinOriginal = o.hourMin; this.minuteMinOriginal = o.minuteMin; this.secondMinOriginal = o.secondMin; this.millisecMinOriginal = o.millisecMin } if (dp_inst.settings.timeOnly || minDateTimeDate.getTime() == dp_date.getTime()) { this._defaults.hourMin = minDateTime.getHours(); if (this.hour <= this._defaults.hourMin) { this.hour = this._defaults.hourMin; this._defaults.minuteMin = minDateTime.getMinutes(); if (this.minute <= this._defaults.minuteMin) { this.minute = this._defaults.minuteMin; this._defaults.secondMin = minDateTime.getSeconds() } else { if (this.second <= this._defaults.secondMin) { this.second = this._defaults.secondMin; this._defaults.millisecMin = minDateTime.getMilliseconds() } else { if (this.millisec < this._defaults.millisecMin) { this.millisec = this._defaults.millisecMin } this._defaults.millisecMin = this.millisecMinOriginal } } } else { this._defaults.minuteMin = this.minuteMinOriginal; this._defaults.secondMin = this.secondMinOriginal; this._defaults.millisecMin = this.millisecMinOriginal } } else { this._defaults.hourMin = this.hourMinOriginal; this._defaults.minuteMin = this.minuteMinOriginal; this._defaults.secondMin = this.secondMinOriginal; this._defaults.millisecMin = this.millisecMinOriginal } } if ($.datepicker._get(dp_inst, "maxDateTime") !== null && $.datepicker._get(dp_inst, "maxDateTime") !== undefined && dp_date) { var maxDateTime = $.datepicker._get(dp_inst, "maxDateTime"), maxDateTimeDate = new Date(maxDateTime.getFullYear(), maxDateTime.getMonth(), maxDateTime.getDate(), 0, 0, 0, 0); if (this.hourMaxOriginal === null || this.minuteMaxOriginal === null || this.secondMaxOriginal === null) { this.hourMaxOriginal = o.hourMax; this.minuteMaxOriginal = o.minuteMax; this.secondMaxOriginal = o.secondMax; this.millisecMaxOriginal = o.millisecMax } if (dp_inst.settings.timeOnly || maxDateTimeDate.getTime() == dp_date.getTime()) { this._defaults.hourMax = maxDateTime.getHours(); if (this.hour >= this._defaults.hourMax) { this.hour = this._defaults.hourMax; this._defaults.minuteMax = maxDateTime.getMinutes(); if (this.minute >= this._defaults.minuteMax) { this.minute = this._defaults.minuteMax; this._defaults.secondMax = maxDateTime.getSeconds() } else { if (this.second >= this._defaults.secondMax) { this.second = this._defaults.secondMax; this._defaults.millisecMax = maxDateTime.getMilliseconds() } else { if (this.millisec > this._defaults.millisecMax) { this.millisec = this._defaults.millisecMax } this._defaults.millisecMax = this.millisecMaxOriginal } } } else { this._defaults.minuteMax = this.minuteMaxOriginal; this._defaults.secondMax = this.secondMaxOriginal; this._defaults.millisecMax = this.millisecMaxOriginal } } else { this._defaults.hourMax = this.hourMaxOriginal; this._defaults.minuteMax = this.minuteMaxOriginal; this._defaults.secondMax = this.secondMaxOriginal; this._defaults.millisecMax = this.millisecMaxOriginal } } if (adjustSliders !== undefined && adjustSliders === true) { var hourMax = parseInt((this._defaults.hourMax - ((this._defaults.hourMax - this._defaults.hourMin) % this._defaults.stepHour)), 10), minMax = parseInt((this._defaults.minuteMax - ((this._defaults.minuteMax - this._defaults.minuteMin) % this._defaults.stepMinute)), 10), secMax = parseInt((this._defaults.secondMax - ((this._defaults.secondMax - this._defaults.secondMin) % this._defaults.stepSecond)), 10), millisecMax = parseInt((this._defaults.millisecMax - ((this._defaults.millisecMax - this._defaults.millisecMin) % this._defaults.stepMillisec)), 10); if (this.hour_slider) { this.hour_slider.slider("option", { min: this._defaults.hourMin, max: hourMax }).slider("value", this.hour) } if (this.minute_slider) { this.minute_slider.slider("option", { min: this._defaults.minuteMin, max: minMax }).slider("value", this.minute) } if (this.second_slider) { this.second_slider.slider("option", { min: this._defaults.secondMin, max: secMax }).slider("value", this.second) } if (this.millisec_slider) { this.millisec_slider.slider("option", { min: this._defaults.millisecMin, max: millisecMax }).slider("value", this.millisec) } } }, _onTimeChange: function () { var hour = (this.hour_slider) ? this.hour_slider.slider("value") : false, minute = (this.minute_slider) ? this.minute_slider.slider("value") : false, second = (this.second_slider) ? this.second_slider.slider("value") : false, millisec = (this.millisec_slider) ? this.millisec_slider.slider("value") : false, timezone = (this.timezone_select) ? this.timezone_select.val() : false, o = this._defaults; if (typeof (hour) == "object") { hour = false } if (typeof (minute) == "object") { minute = false } if (typeof (second) == "object") { second = false } if (typeof (millisec) == "object") { millisec = false } if (typeof (timezone) == "object") { timezone = false } if (hour !== false) { hour = parseInt(hour, 10) } if (minute !== false) { minute = parseInt(minute, 10) } if (second !== false) { second = parseInt(second, 10) } if (millisec !== false) { millisec = parseInt(millisec, 10) } var ampm = o[hour < 12 ? "amNames" : "pmNames"][0]; var hasChanged = (hour != this.hour || minute != this.minute || second != this.second || millisec != this.millisec || (this.ampm.length > 0 && (hour < 12) != ($.inArray(this.ampm.toUpperCase(), this.amNames) !== -1)) || timezone != this.timezone); if (hasChanged) { if (hour !== false) { this.hour = hour } if (minute !== false) { this.minute = minute } if (second !== false) { this.second = second } if (millisec !== false) { this.millisec = millisec } if (timezone !== false) { this.timezone = timezone } if (!this.inst) { this.inst = $.datepicker._getInst(this.$input[0]) } this._limitMinMaxDateTime(this.inst, true) } if (o.ampm) { this.ampm = ampm } this.formattedTime = $.datepicker.formatTime(this._defaults.timeFormat, this, this._defaults); if (this.$timeObj) { this.$timeObj.text(this.formattedTime + o.timeSuffix) } this.timeDefined = true; if (hasChanged) { this._updateDateTime() } }, _onSelectHandler: function () { var onSelect = this._defaults.onSelect; var inputEl = this.$input ? this.$input[0] : null; if (onSelect && inputEl) { onSelect.apply(inputEl, [this.formattedDateTime, this]) } }, _formatTime: function (time, format) { time = time || { hour: this.hour, minute: this.minute, second: this.second, millisec: this.millisec, ampm: this.ampm, timezone: this.timezone }; var tmptime = (format || this._defaults.timeFormat).toString(); tmptime = $.datepicker.formatTime(tmptime, time, this._defaults); if (arguments.length) { return tmptime } else { this.formattedTime = tmptime } }, _updateDateTime: function (dp_inst) { dp_inst = this.inst || dp_inst; var dt = $.datepicker._daylightSavingAdjust(new Date(dp_inst.selectedYear, dp_inst.selectedMonth, dp_inst.selectedDay)), dateFmt = $.datepicker._get(dp_inst, "dateFormat"), formatCfg = $.datepicker._getFormatConfig(dp_inst), timeAvailable = dt !== null && this.timeDefined; this.formattedDate = $.datepicker.formatDate(dateFmt, (dt === null ? new Date() : dt), formatCfg); var formattedDateTime = this.formattedDate; if (dp_inst.lastVal !== undefined && (dp_inst.lastVal.length > 0 && this.$input.val().length === 0)) { return } if (this._defaults.timeOnly === true) { formattedDateTime = this.formattedTime } else { if (this._defaults.timeOnly !== true && (this._defaults.alwaysSetTime || timeAvailable)) { formattedDateTime += this._defaults.separator + this.formattedTime + this._defaults.timeSuffix } } this.formattedDateTime = formattedDateTime; if (!this._defaults.showTimepicker) { this.$input.val(this.formattedDate) } else { if (this.$altInput && this._defaults.altFieldTimeOnly === true) { this.$altInput.val(this.formattedTime); this.$input.val(this.formattedDate) } else { if (this.$altInput) { this.$altInput.val(formattedDateTime); this.$input.val(formattedDateTime) } else { this.$input.val(formattedDateTime) } } } this.$input.trigger("change") } }); $.fn.extend({ timepicker: function (o) { o = o || {}; var tmp_args = arguments; if (typeof o == "object") { tmp_args[0] = $.extend(o, { timeOnly: true }) } return $(this).each(function () { $.fn.datetimepicker.apply($(this), tmp_args) }) }, datetimepicker: function (o) { o = o || {}; tmp_args = arguments; if (typeof (o) == "string") { if (o == "getDate") { return $.fn.datepicker.apply($(this[0]), tmp_args) } else { return this.each(function () { var $t = $(this); $t.datepicker.apply($t, tmp_args) }) } } else { return this.each(function () { var $t = $(this); $t.datepicker($.timepicker._newInst($t, o)._defaults) }) } } }); $.datepicker.formatTime = function (format, time, options) { options = options || {}; options = $.extend($.timepicker._defaults, options); time = $.extend({ hour: 0, minute: 0, second: 0, millisec: 0, timezone: "+0000" }, time); var tmptime = format; var ampmName = options.amNames[0]; var hour = parseInt(time.hour, 10); if (options.ampm) { if (hour > 11) { ampmName = options.pmNames[0]; if (hour > 12) { hour = hour % 12 } } if (hour === 0) { hour = 12 } } tmptime = tmptime.replace(/(?:hh?|mm?|ss?|[tT]{1,2}|[lz])/g, function (match) { switch (match.toLowerCase()) { case "hh": return ("0" + hour).slice(-2); case "h": return hour; case "mm": return ("0" + time.minute).slice(-2); case "m": return time.minute; case "ss": return ("0" + time.second).slice(-2); case "s": return time.second; case "l": return ("00" + time.millisec).slice(-3); case "z": return time.timezone; case "t": case "tt": if (options.ampm) { if (match.length == 1) { ampmName = ampmName.charAt(0) } return match.charAt(0) == "T" ? ampmName.toUpperCase() : ampmName.toLowerCase() } return "" } }); tmptime = $.trim(tmptime); return tmptime }; $.datepicker._base_selectDate = $.datepicker._selectDate; $.datepicker._selectDate = function (id, dateStr) { var inst = this._getInst($(id)[0]), tp_inst = this._get(inst, "timepicker"); if (tp_inst) { tp_inst._limitMinMaxDateTime(inst, true); inst.inline = inst.stay_open = true; this._base_selectDate(id, dateStr); inst.inline = inst.stay_open = false; this._notifyChange(inst); this._updateDatepicker(inst) } else { this._base_selectDate(id, dateStr) } }; $.datepicker._base_updateDatepicker = $.datepicker._updateDatepicker; $.datepicker._updateDatepicker = function (inst) { var input = inst.input[0]; if ($.datepicker._curInst && $.datepicker._curInst != inst && $.datepicker._datepickerShowing && $.datepicker._lastInput != input) { return } if (typeof (inst.stay_open) !== "boolean" || inst.stay_open === false) { this._base_updateDatepicker(inst); var tp_inst = this._get(inst, "timepicker"); if (tp_inst) { tp_inst._addTimePicker(inst) } } }; $.datepicker._base_doKeyPress = $.datepicker._doKeyPress; $.datepicker._doKeyPress = function (event) { var inst = $.datepicker._getInst(event.target), tp_inst = $.datepicker._get(inst, "timepicker"); if (tp_inst) { if ($.datepicker._get(inst, "constrainInput")) { var ampm = tp_inst._defaults.ampm, dateChars = $.datepicker._possibleChars($.datepicker._get(inst, "dateFormat")), datetimeChars = tp_inst._defaults.timeFormat.toString().replace(/[hms]/g, "").replace(/TT/g, ampm ? "APM" : "").replace(/Tt/g, ampm ? "AaPpMm" : "").replace(/tT/g, ampm ? "AaPpMm" : "").replace(/T/g, ampm ? "AP" : "").replace(/tt/g, ampm ? "apm" : "").replace(/t/g, ampm ? "ap" : "") + " " + tp_inst._defaults.separator + tp_inst._defaults.timeSuffix + (tp_inst._defaults.showTimezone ? tp_inst._defaults.timezoneList.join("") : "") + (tp_inst._defaults.amNames.join("")) + (tp_inst._defaults.pmNames.join("")) + dateChars, chr = String.fromCharCode(event.charCode === undefined ? event.keyCode : event.charCode); return event.ctrlKey || (chr < " " || !dateChars || datetimeChars.indexOf(chr) > -1) } } return $.datepicker._base_doKeyPress(event) }; $.datepicker._base_doKeyUp = $.datepicker._doKeyUp; $.datepicker._doKeyUp = function (event) { var inst = $.datepicker._getInst(event.target), tp_inst = $.datepicker._get(inst, "timepicker"); if (tp_inst) { if (tp_inst._defaults.timeOnly && (inst.input.val() != inst.lastVal)) { try { $.datepicker._updateDatepicker(inst) } catch (err) { $.datepicker.log(err) } } } return $.datepicker._base_doKeyUp(event) }; $.datepicker._base_gotoToday = $.datepicker._gotoToday; $.datepicker._gotoToday = function (id) { var inst = this._getInst($(id)[0]), $dp = inst.dpDiv; this._base_gotoToday(id); var now = new Date(); var tp_inst = this._get(inst, "timepicker"); if (tp_inst && tp_inst._defaults.showTimezone && tp_inst.timezone_select) { var tzoffset = now.getTimezoneOffset(); var tzsign = tzoffset > 0 ? "-" : "+"; tzoffset = Math.abs(tzoffset); var tzmin = tzoffset % 60; tzoffset = tzsign + ("0" + (tzoffset - tzmin) / 60).slice(-2) + ("0" + tzmin).slice(-2); if (tp_inst._defaults.timezoneIso8609) { tzoffset = tzoffset.substring(0, 3) + ":" + tzoffset.substring(3) } tp_inst.timezone_select.val(tzoffset) } this._setTime(inst, now); $(".ui-datepicker-today", $dp).click() }; $.datepicker._disableTimepickerDatepicker = function (target, date, withDate) { var inst = this._getInst(target), tp_inst = this._get(inst, "timepicker"); $(target).datepicker("getDate"); if (tp_inst) { tp_inst._defaults.showTimepicker = false; tp_inst._updateDateTime(inst) } }; $.datepicker._enableTimepickerDatepicker = function (target, date, withDate) { var inst = this._getInst(target), tp_inst = this._get(inst, "timepicker"); $(target).datepicker("getDate"); if (tp_inst) { tp_inst._defaults.showTimepicker = true; tp_inst._addTimePicker(inst); tp_inst._updateDateTime(inst) } }; $.datepicker._setTime = function (inst, date) { var tp_inst = this._get(inst, "timepicker"); if (tp_inst) { var defaults = tp_inst._defaults, hour = date ? date.getHours() : defaults.hour, minute = date ? date.getMinutes() : defaults.minute, second = date ? date.getSeconds() : defaults.second, millisec = date ? date.getMilliseconds() : defaults.millisec; if ((hour < defaults.hourMin || hour > defaults.hourMax) || (minute < defaults.minuteMin || minute > defaults.minuteMax) || (second < defaults.secondMin || second > defaults.secondMax) || (millisec < defaults.millisecMin || millisec > defaults.millisecMax)) { hour = defaults.hourMin; minute = defaults.minuteMin; second = defaults.secondMin; millisec = defaults.millisecMin } tp_inst.hour = hour; tp_inst.minute = minute; tp_inst.second = second; tp_inst.millisec = millisec; if (tp_inst.hour_slider) { tp_inst.hour_slider.slider("value", hour) } if (tp_inst.minute_slider) { tp_inst.minute_slider.slider("value", minute) } if (tp_inst.second_slider) { tp_inst.second_slider.slider("value", second) } if (tp_inst.millisec_slider) { tp_inst.millisec_slider.slider("value", millisec) } tp_inst._onTimeChange(); tp_inst._updateDateTime(inst) } }; $.datepicker._setTimeDatepicker = function (target, date, withDate) { var inst = this._getInst(target), tp_inst = this._get(inst, "timepicker"); if (tp_inst) { this._setDateFromField(inst); var tp_date; if (date) { if (typeof date == "string") { tp_inst._parseTime(date, withDate); tp_date = new Date(); tp_date.setHours(tp_inst.hour, tp_inst.minute, tp_inst.second, tp_inst.millisec) } else { tp_date = new Date(date.getTime()) } if (tp_date.toString() == "Invalid Date") { tp_date = undefined } this._setTime(inst, tp_date) } } }; $.datepicker._base_setDateDatepicker = $.datepicker._setDateDatepicker; $.datepicker._setDateDatepicker = function (target, date) { var inst = this._getInst(target), tp_date = (date instanceof Date) ? new Date(date.getTime()) : date; this._updateDatepicker(inst); this._base_setDateDatepicker.apply(this, arguments); this._setTimeDatepicker(target, tp_date, true) }; $.datepicker._base_getDateDatepicker = $.datepicker._getDateDatepicker; $.datepicker._getDateDatepicker = function (target, noDefault) { var inst = this._getInst(target), tp_inst = this._get(inst, "timepicker"); if (tp_inst) { this._setDateFromField(inst, noDefault); var date = this._getDate(inst); if (date && tp_inst._parseTime($(target).val(), tp_inst.timeOnly)) { date.setHours(tp_inst.hour, tp_inst.minute, tp_inst.second, tp_inst.millisec) } return date } return this._base_getDateDatepicker(target, noDefault) }; $.datepicker._base_parseDate = $.datepicker.parseDate; $.datepicker.parseDate = function (format, value, settings) { var date; try { date = this._base_parseDate(format, value, settings) } catch (err) { if (err.indexOf(":") >= 0) { date = this._base_parseDate(format, value.substring(0, value.length - (err.length - err.indexOf(":") - 2)), settings) } else { throw err } } return date }; $.datepicker._base_formatDate = $.datepicker._formatDate; $.datepicker._formatDate = function (inst, day, month, year) { var tp_inst = this._get(inst, "timepicker"); if (tp_inst) { tp_inst._updateDateTime(inst); return tp_inst.$input.val() } return this._base_formatDate(inst) }; $.datepicker._base_optionDatepicker = $.datepicker._optionDatepicker; $.datepicker._optionDatepicker = function (target, name, value) { var inst = this._getInst(target), tp_inst = this._get(inst, "timepicker"); if (tp_inst) { var min = null, max = null, onselect = null; if (typeof name == "string") { if (name === "minDate" || name === "minDateTime") { min = value } else { if (name === "maxDate" || name === "maxDateTime") { max = value } else { if (name === "onSelect") { onselect = value } } } } else { if (typeof name == "object") { if (name.minDate) { min = name.minDate } else { if (name.minDateTime) { min = name.minDateTime } else { if (name.maxDate) { max = name.maxDate } else { if (name.maxDateTime) { max = name.maxDateTime } } } } } } if (min) { if (min == 0) { min = new Date() } else { min = new Date(min) } tp_inst._defaults.minDate = min; tp_inst._defaults.minDateTime = min } else { if (max) { if (max == 0) { max = new Date() } else { max = new Date(max) } tp_inst._defaults.maxDate = max; tp_inst._defaults.maxDateTime = max } else { if (onselect) { tp_inst._defaults.onSelect = onselect } } } } if (value === undefined) { return this._base_optionDatepicker(target, name) } return this._base_optionDatepicker(target, name, value) }; function extendRemove(target, props) { $.extend(target, props); for (var name in props) { if (props[name] === null || props[name] === undefined) { target[name] = props[name] } } return target } $.timepicker = new Timepicker(); $.timepicker.version = "1.0.0" })(jQuery);



/*
 * jquery.ui.touch-punch.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
  * jQuery UI Touch Punch 0.2.2
 *
 * Copyright 2011, Dave Furfero
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Depends:
 *  jquery.ui.widget.js
 *  jquery.ui.mouse.js
 */
(function ($) {

  // Detect touch support
  $.support.touch = 'ontouchend' in document;

  // Ignore browsers without touch support
  if (!$.support.touch) {
    return;
  }

  var mouseProto = $.ui.mouse.prototype,
      _mouseInit = mouseProto._mouseInit,
      touchHandled;

  /**
   * Simulate a mouse event based on a corresponding touch event
   * @param {Object} event A touch event
   * @param {String} simulatedType The corresponding mouse event
   */
  function simulateMouseEvent (event, simulatedType) {

    // Ignore multi-touch events
    if (event.originalEvent.touches.length > 1) {
      return;
    }

    event.preventDefault();

    var touch = event.originalEvent.changedTouches[0],
        simulatedEvent = document.createEvent('MouseEvents');
    
    // Initialize the simulated mouse event using the touch event's coordinates
    simulatedEvent.initMouseEvent(
      simulatedType,    // type
      true,             // bubbles                    
      true,             // cancelable                 
      window,           // view                       
      1,                // detail                     
      touch.screenX,    // screenX                    
      touch.screenY,    // screenY                    
      touch.clientX,    // clientX                    
      touch.clientY,    // clientY                    
      false,            // ctrlKey                    
      false,            // altKey                     
      false,            // shiftKey                   
      false,            // metaKey                    
      0,                // button                     
      null              // relatedTarget              
    );

    // Dispatch the simulated event to the target element
    event.target.dispatchEvent(simulatedEvent);
  }

  /**
   * Handle the jQuery UI widget's touchstart events
   * @param {Object} event The widget element's touchstart event
   */
  mouseProto._touchStart = function (event) {

    var self = this;

    // Ignore the event if another widget is already being handled
    if (touchHandled || !self._mouseCapture(event.originalEvent.changedTouches[0])) {
      return;
    }

    // Set the flag to prevent other widgets from inheriting the touch event
    touchHandled = true;

    // Track movement to determine if interaction was a click
    self._touchMoved = false;

    // Simulate the mouseover event
    simulateMouseEvent(event, 'mouseover');

    // Simulate the mousemove event
    simulateMouseEvent(event, 'mousemove');

    // Simulate the mousedown event
    simulateMouseEvent(event, 'mousedown');
  };

  /**
   * Handle the jQuery UI widget's touchmove events
   * @param {Object} event The document's touchmove event
   */
  mouseProto._touchMove = function (event) {

    // Ignore event if not handled
    if (!touchHandled) {
      return;
    }

    // Interaction was not a click
    this._touchMoved = true;

    // Simulate the mousemove event
    simulateMouseEvent(event, 'mousemove');
  };

  /**
   * Handle the jQuery UI widget's touchend events
   * @param {Object} event The document's touchend event
   */
  mouseProto._touchEnd = function (event) {

    // Ignore event if not handled
    if (!touchHandled) {
      return;
    }

    // Simulate the mouseup event
    simulateMouseEvent(event, 'mouseup');

    // Simulate the mouseout event
    simulateMouseEvent(event, 'mouseout');

    // If the touch interaction did not move, it should trigger a click
    if (!this._touchMoved) {

      // Simulate the click event
      simulateMouseEvent(event, 'click');
    }

    // Unset the flag to allow other widgets to inherit the touch event
    touchHandled = false;
  };

  /**
   * A duck punch of the $.ui.mouse _mouseInit method to support touch events.
   * This method extends the widget with bound touch event handlers that
   * translate touch events to mouse events and pass them to the widget's
   * original mouse event handling methods.
   */
  mouseProto._mouseInit = function () {
    
    var self = this;

    // Delegate the touch handlers to the widget's element
    self.element
      .bind('touchstart', $.proxy(self, '_touchStart'))
      .bind('touchmove', $.proxy(self, '_touchMove'))
      .bind('touchend', $.proxy(self, '_touchEnd'));

    // Call the original $.ui.mouse init method
    _mouseInit.call(self);
  };

})(jQuery);








/*
 * ui.spinner.min.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 
 * @license jQuery UI Spinner 1.20
 *
 * Copyright (c) 2009-2010 Brant Burnett
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Modified by maimairel <maimairel@yahoo.com> to support jQuery 1.7.1
 * - removed the native mousewheel support by using Brandon Aaron's mousewheel plugin instead.
 * - Removed inline css for the input
 */

 (function($, undefined) {

var 
	// constants
	active = 'ui-state-active',
	hover = 'ui-state-hover',
	disabled = 'ui-state-disabled',
	
	keyCode = $.ui.keyCode,
	up = keyCode.UP,
	down = keyCode.DOWN,
	right = keyCode.RIGHT,
	left = keyCode.LEFT,
	pageUp = keyCode.PAGE_UP,
	pageDown = keyCode.PAGE_DOWN,
	home = keyCode.HOME,
	end = keyCode.END,
	
	msie = $.browser.msie,
	
	// namespace for events on input
	eventNamespace = '.uispinner', 
	
	// only these special keys will be accepted, all others will be ignored unless CTRL or ALT are pressed
	validKeys = [up, down, right, left, pageUp, pageDown, home, end, keyCode.BACKSPACE, keyCode.DELETE, keyCode.TAB],
	
	// stores the currently focused spinner
	// Note: due to oddities in the focus/blur events, this is part of a two-part system for confirming focus
	// this must set to the control, and the focus variable must be true
	// this is because hitting up/down arrows with mouse causes focus to change, but blur event for previous control doesn't fire
	focusCtrl;
	
$.widget('ui.spinner', {
	options: {
		min: null,
		max: null,
		allowNull: false,
		
		group: '',
		point: '.',
		prefix: '',
		suffix: '',
		places: null, // null causes it to detect the number of places in step
		
		defaultStep: 1, // real value is 'step', and should be passed as such.  This value is used to detect if passed value should override HTML5 attribute
		largeStep: 10,
		mouseWheel: true,
		increment: 'slow',		
		className: null,
		showOn: 'always',
		width: 16,
		upIconClass: "ui-icon-triangle-1-n",
		downIconClass: "ui-icon-triangle-1-s",
		
		format: function(num, places) {
			var options = this,
				regex = /(\d+)(\d{3})/,
				result = ((isNaN(num) ? 0 : Math.abs(num)).toFixed(places)) + '';
				
			for (result = result.replace('.', options.point); regex.test(result) && options.group; result=result.replace(regex, '$1'+options.group+'$2')) {};
			return (num < 0 ? '-' : '') + options.prefix + result + options.suffix;
		},
		
		parse: function(val) {
			var options = this;
			
			if (options.group == '.')
				val = val.replace('.', '');
			if (options.point != '.')
				val = val.replace(options.point, '.');
			return parseFloat(val.replace(/[^0-9\-\.]/g, ''));
		}
	},
	
	// * Widget fields *
	// curvalue - current value
	// places - currently effective number of decimal places
	// oWidth - original input width (used for destroy)
	// oMargin - original input right margin (used for destroy)
	// counter - number of spins at the current spin speed
	// incCounter - index within options.increment of the current spin speed
	// selfChange - indicates that change event is being fired by the widget, so don't reprocess input value
	// inputMaxLength - initial maxLength value on the input
	// focused - this spinner currently has the focus

	_create: function() {
		// shortcuts
		var self = this,
			input = self.element,
			type = input.attr('type');
		
		if (!input.is('input') || ((type != 'text') && (type != 'number'))) {
			console.error('Invalid target for ui.spinner');
			return;
		}
		
		self._procOptions(true);
		self._createButtons(input);

		if (!input.is(':enabled'))
			self.disable();
	},
	
	_createButtons: function(input) {
		function getMargin(margin) {
			// IE8 returns auto if no margin specified
			return margin == 'auto' ? 0 : parseInt(margin);
		}

		var self = this,
			options = self.options,
			className = options.className,
			buttonWidth = options.width,
			showOn = options.showOn,
			box = $.support.boxModel,
			height = input.outerHeight(),
			rightMargin = self.oMargin = getMargin(input.css('margin-right')), // store original width and right margin for later destroy
			wrapper = self.wrapper = input/*.css({ width: (self.oWidth = (box ? input.width() : input.outerWidth())) - buttonWidth, 
												 marginRight: rightMargin + buttonWidth, textAlign: 'right' })*/
				.after('<span class="ui-spinner ui-widget"></span>').next(),
			btnContainer = self.btnContainer = $(
				'<div class="ui-spinner-buttons">' + 
					'<div class="ui-spinner-up ui-spinner-button ui-state-default ui-corner-tr"><span class="ui-icon '+options.upIconClass+'">&nbsp;</span></div>' + 
					'<div class="ui-spinner-down ui-spinner-button ui-state-default ui-corner-br"><span class="ui-icon '+options.downIconClass+'">&nbsp;</span></div>' + 
				'</div>'),

			// object shortcuts
			upButton, downButton, buttons, icons,

			hoverDelay,
			hoverDelayCallback,
			
			// current state booleans
			hovered, inKeyDown, inSpecialKey, inMouseDown,
						
			// used to reverse left/right key directions
			rtl = input[0].dir == 'rtl';
		
		// apply className before doing any calculations because it could affect them
		if (className) wrapper.addClass(className);
		
		wrapper.append(btnContainer/*.css({ height: height, left: -buttonWidth-rightMargin,
			// use offset calculation to fix vertical position in Firefox
			top: (input.offset().top - wrapper.offset().top) + 'px' })*/);
		
		buttons = self.buttons = btnContainer.find('.ui-spinner-button');
		/*buttons.css({ width: buttonWidth - (box ? buttons.outerWidth() - buttons.width() : 0), height: height/2 - (box ? buttons.outerHeight() - buttons.height() : 0) });*/
		upButton = buttons[0];
		downButton = buttons[1];

		// fix icon centering
		icons = buttons.find('.ui-icon');
		/*icons.css({ marginLeft: (buttons.innerWidth() - icons.width()) / 2, marginTop:  (buttons.innerHeight() - icons.height()) / 2 });*/
		
		// set width of btnContainer to be the same as the buttons
		/*btnContainer.width(buttons.outerWidth());*/
		if (showOn != 'always')
			btnContainer.css('opacity', 0);
		
		/* Event Bindings */

		// bind hover events to show/hide buttons
		if (showOn == 'hover' || showOn == 'both')
			buttons.add(input)
				.bind('mouseenter' + eventNamespace, function() {
					setHoverDelay(function() {
						hovered = true;
						if (!self.focused || (showOn == 'hover')) // ignore focus flag if show on hover only
							self.showButtons();
					});
				})
				
				.bind('mouseleave' + eventNamespace, function hoverOut() {
					setHoverDelay(function() {
						hovered = false;
						if (!self.focused || (showOn == 'hover')) // ignore focus flag if show on hover only
							self.hideButtons();
					});
				});

	
		buttons.hover(function() {
					// ensure that both buttons have hover removed, sometimes they get left on
					self.buttons.removeClass(hover);
					
					if (!options.disabled)
						$(this).addClass(hover);
				}, function() {
					$(this).removeClass(hover);
				})
			.mousedown(mouseDown)
			.mouseup(mouseUp)
			.mouseout(mouseUp);
			
		if (msie)
			// fixes dbl click not firing second mouse down in IE
			buttons.dblclick(function() {
					if (!options.disabled) {
						// make sure any changes are posted
						self._change();
						self._doSpin((this === upButton ? 1 : -1) * options.step);
					}
					
					return false;
				}) 
				
				// fixes IE8 dbl click selection highlight
				.bind('selectstart', function() {return false;});
				
		input.bind('keydown' + eventNamespace, function(e) {
					var dir, large, limit,
						keyCode = e.keyCode; // shortcut for minimization
					if (e.ctrl || e.alt) return true; // ignore these events
					
					if (isSpecialKey(keyCode))
						inSpecialKey = true;
					
					if (inKeyDown) return false; // only one direction at a time, and suppress invalid keys
					
					switch (keyCode) {
						case up:
						case pageUp:
							dir = 1;
							large = keyCode == pageUp;
							break;
							
						case down:
						case pageDown:
							dir = -1;
							large = keyCode == pageDown;
							break;
							
						case right:
						case left:
							dir = (keyCode == right) ^ rtl ? 1 : -1;
							break;
							
						case home:
							limit = self.options.min;
							if (limit != null) self._setValue(limit);
							return false;
							
						case end:
							limit = self.options.max;
							limit = self.options.max;
							if (limit != null) self._setValue(limit);
							return false;
					}
					
					if (dir) { // only process if dir was set above
						if (!inKeyDown && !options.disabled) {
							keyDir = dir;
							
							$(dir > 0 ? upButton : downButton).addClass(active);
							inKeyDown = true;
							self._startSpin(dir, large);
						}
						
						return false;
					}
				})
				
			.bind('keyup' + eventNamespace, function(e) {
					if (e.ctrl || e.alt) return true; // ignore these events
					
					if (isSpecialKey(keyCode))
						inSpecialKey = false;
					
					switch (e.keyCode) {
						case up:
						case right:
						case pageUp:
						case down:
						case left:
						case pageDown:
							buttons.removeClass(active)
							self._stopSpin();
							inKeyDown = false;
							return false;
					}
				})
				
			.bind('keypress' + eventNamespace, function(e) {
					if (invalidKey(e.keyCode, e.charCode)) return false;
				})
				
			.bind('change' + eventNamespace, function() { self._change(); })
			
			.bind('focus' + eventNamespace, function() {
					function selectAll() {
						self.element.select();
					}

					msie ? selectAll() : setTimeout(selectAll, 0); // add delay for Chrome, but breaks IE8
					self.focused = true;
					focusCtrl = self;
					if (!hovered && (showOn == 'focus' || showOn == 'both')) // hovered will only be set if hover affects show
						self.showButtons();
				})
				
			.bind('blur' + eventNamespace, function() {
					self.focused = false;
					if (!hovered && (showOn == 'focus' || showOn == 'both')) // hovered will only be set if hover affects show
						self.hideButtons();
				});
				
		function isSpecialKey(keyCode) {
			for (var i=0; i<validKeys.length; i++) // predefined list of special keys
				if (validKeys[i] == keyCode) return true;
				
			return false;
		}
			
		function invalidKey(keyCode, charCode) {
			if (inSpecialKey) return false;				
			
			var ch = String.fromCharCode(charCode || keyCode),
				options = self.options;
				
			if ((ch >= '0') && (ch <= '9') || (ch == '-')) return false;
			if (((self.places > 0) && (ch == options.point))
				|| (ch == options.group)) return false;
						
			return true;
		}
		
		// used to delay start of hover show/hide by 100 milliseconds
		function setHoverDelay(callback) {
			if (hoverDelay) {
				// don't do anything if trying to set the same callback again
				if (callback === hoverDelayCallback) return;
				
				clearTimeout(hoverDelay);
			}
			
			hoverDelayCallback = callback;
			hoverDelay = setTimeout(execute, 100);
			
			function execute() {
				hoverDelay = 0;
				callback();
			}
		}
			
		function mouseDown() {
			if (!options.disabled) {
				var input = self.element[0],
					dir = (this === upButton ? 1 : -1);
					
				input.focus();
				input.select();
				$(this).addClass(active);
				
				inMouseDown = true;
				self._startSpin(dir);
			}

			return false;
		}
		
		function mouseUp() {
			if (inMouseDown) {
				$(this).removeClass(active);
				self._stopSpin();
				inMouseDown = false;
			}
			return false;
		}
	},
	
	_procOptions: function(init) {
		var self = this,
			input = self.element,
			options = self.options,
			min = options.min,
			max = options.max,
			step = options.step,
			places = options.places,
			maxlength = -1, temp;
			
		// setup increment based on speed string
		if (options.increment == 'slow')
			options.increment = [{count: 1, mult: 1, delay: 250},
								 {count: 3, mult: 1, delay: 100},
								 {count: 0, mult: 1, delay: 50}];
		else if (options.increment == 'fast')
			options.increment = [{count: 1, mult: 1, delay: 250},
								 {count: 19, mult: 1, delay: 100},
								 {count: 80, mult: 1, delay: 20},
								 {count: 100, mult: 10, delay: 20},
								 {count: 0, mult: 100, delay: 20}];

		if ((min == null) && ((temp = input.attr('min')) != null))
			min = parseFloat(temp);
		
		if ((max == null) && ((temp = input.attr('max')) != null))
			max = parseFloat(temp);
		
		if (!step && ((temp = input.attr('step')) != null))
			if (temp != 'any') {
				step = parseFloat(temp);
				options.largeStep *= step;
			}
		options.step = step = step || options.defaultStep;

		// Process step for decimal places if none are specified
		if ((places == null) && ((temp = step + '').indexOf('.') != -1))
			places = temp.length - temp.indexOf('.') - 1;
		self.places = places;
		
		if ((max != null) && (min != null)) {
			// ensure that min is less than or equal to max
			if (min > max) min = max;
			
			// set maxlength based on min/max
			maxlength = Math.max(Math.max(maxlength, options.format(max, places, input).length), options.format(min, places, input).length);
		}
		
		// only lookup input maxLength on init
		if (init) self.inputMaxLength = input[0].maxLength;
		temp = self.inputMaxLength;
			
		if (temp > 0) {
			maxlength = maxlength > 0 ? Math.min(temp, maxlength) : temp;
			temp = Math.pow(10, maxlength) - 1;
			if ((max == null) || (max > temp))
				max = temp;
			temp = -(temp + 1) / 10 + 1;
			if ((min == null) || (min < temp))
				min = temp;
		}
		
		if (maxlength > 0)
			input.attr('maxlength', maxlength);
					
		options.min = min;
		options.max = max;
		
		// ensures that current value meets constraints
		self._change();
		
		if (options.mouseWheel && $.fn.mousewheel) {
			input.mousewheel(self._mouseWheel);
		}
	},
		
	_mouseWheel: function(e, delta) {
		var self = $.data(this, 'spinner');
		if (!self.options.disabled && self.focused && (focusCtrl === self)) {
			// make sure changes are posted
			self._change();
			self._doSpin(delta * self.options.step);
			return false;
		}
	},
	
	// sets an interval to call the _spin function
	_setTimer: function(delay, dir, large) {
		var self = this;
		self._stopSpin();
		self.timer = setInterval(fire, delay);
		
		function fire() {
			self._spin(dir, large);
		}
	},
	
	// stops the spin timer
	_stopSpin: function() {
		if (this.timer) {
			clearInterval(this.timer);
			this.timer = 0;
		}
	},
	
	// performs first step, and starts the spin timer if increment is set
	_startSpin: function(dir, large) {
		// shortcuts
		var self = this,
			options = self.options,
			increment = options.increment;
			
		// make sure any changes are posted
		self._change();
		self._doSpin(dir * (large ? self.options.largeStep : self.options.step));
		
		if (increment && increment.length > 0) {		
			self.counter = 0;
			self.incCounter = 0;
			self._setTimer(increment[0].delay, dir, large);
		}
	},
	
	// called by timer for each step in the spin
	_spin: function(dir, large) {
		// shortcuts
		var self = this,
			increment = self.options.increment,
			curIncrement = increment[self.incCounter];
		
		self._doSpin(dir * curIncrement.mult * (large ? self.options.largeStep : self.options.step));
		self.counter++;

		if ((self.counter > curIncrement.count) && (self.incCounter < increment.length-1)) {
			self.counter = 0;
			curIncrement = increment[++self.incCounter];
			self._setTimer(curIncrement.delay, dir, large);
		}
	},
	
	// actually spins the timer by a step
	_doSpin: function(step) {
		// shortcut
		var self = this,
			value = self.curvalue;
			
		if (value == null)
			value = (step > 0 ? self.options.min : self.options.max) || 0;
		
		self._setValue(value + step);
	},
	
	// Parse the value currently in the field
	_parseValue: function() {
		var value = this.element.val();
		return value ? this.options.parse(value, this.element) : null;
	},
	
	_validate: function(value) {
		var options = this.options,
			min = options.min,
			max = options.max;

		if ((value == null) && !options.allowNull)
			value = this.curvalue != null ? this.curvalue : min || max || 0; // must confirm not null in case just initializing and had blank value

		if ((max != null) && (value > max))
			return max;
		else if ((min != null) && (value < min))
			return min;
		else
			return value;
	},
	
	_change: function() {
		var self = this, // shortcut
			value = self._parseValue(),
			min = self.options.min,
			max = self.options.max;
			
		// don't reprocess if change was self triggered
		if (!self.selfChange) {
			if (isNaN(value))
				value = self.curvalue;

			self._setValue(value, true);
		}
	},
	
	// overrides _setData to force option parsing
	_setOption: function(key, value) {
		$.Widget.prototype._setOption.call(this, key, value);
		this._procOptions();
	},
	
	increment: function() {
		this._doSpin(this.options.step);
	},
	
	decrement: function() {
		this._doSpin(-this.options.step);
	},
	
	showButtons: function(immediate) {
		var btnContainer = this.btnContainer.stop();
		if (immediate)
			btnContainer.css('opacity', 1);
		else
			btnContainer.fadeTo('fast', 1);
	},
	
	hideButtons: function(immediate) {
		var btnContainer = this.btnContainer.stop();
		if (immediate)
			btnContainer.css('opacity', 0);
		else
			btnContainer.fadeTo('fast', 0);
		this.buttons.removeClass(hover);
	},
	
	// Set the value directly
	_setValue: function(value, suppressFireEvent) {
		var self = this;
		
		self.curvalue = value = self._validate(value);
		self.element.val(value != null ? 
			self.options.format(value, self.places, self.element) :
			'');
		
		if (!suppressFireEvent) {
			self.selfChange = true;
			self.element.change();
			self.selfChange = false;
		}
	},

	// Set or retrieve the value
	value: function(newValue) {
		if (arguments.length) {
			this._setValue(newValue);
			
			// maintains chaining
			return this.element;
		}

		return this.curvalue;
	},

	enable: function() {
		this.buttons.removeClass(disabled);
		this.element[0].disabled = false;
		$.Widget.prototype.enable.call(this);
	},
	
	disable: function() {
		this.buttons.addClass(disabled)
			// in case hover class got left on
			.removeClass(hover);
			
		this.element[0].disabled = true;
		$.Widget.prototype.disable.call(this);
	},
	
	destroy: function(target) {
		this.wrapper.remove();
		this.element.unbind(eventNamespace).css({ width: this.oWidth, marginRight: this.oMargin });
		
		if ($.fn.mousewheel) {
			this.element.unmousewheel();
		}		
		
		$.Widget.prototype.destroy.call(this);
	}	
});

})( jQuery );



/*
 * jquery.imgareaselect.min.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 */


 (function($){var abs=Math.abs,max=Math.max,min=Math.min,round=Math.round;function div(){return $('<div/>')}$.imgAreaSelect=function(img,options){var $img=$(img),imgLoaded,$box=div(),$area=div(),$border=div().add(div()).add(div()).add(div()),$outer=div().add(div()).add(div()).add(div()),$handles=$([]),$areaOpera,left,top,imgOfs={left:0,top:0},imgWidth,imgHeight,$parent,parOfs={left:0,top:0},zIndex=0,position='absolute',startX,startY,scaleX,scaleY,resize,minWidth,minHeight,maxWidth,maxHeight,aspectRatio,shown,x1,y1,x2,y2,selection={x1:0,y1:0,x2:0,y2:0,width:0,height:0},docElem=document.documentElement,$p,d,i,o,w,h,adjusted;function viewX(x){return x+imgOfs.left-parOfs.left}function viewY(y){return y+imgOfs.top-parOfs.top}function selX(x){return x-imgOfs.left+parOfs.left}function selY(y){return y-imgOfs.top+parOfs.top}function evX(event){return event.pageX-parOfs.left}function evY(event){return event.pageY-parOfs.top}function getSelection(noScale){var sx=noScale||scaleX,sy=noScale||scaleY;return{x1:round(selection.x1*sx),y1:round(selection.y1*sy),x2:round(selection.x2*sx),y2:round(selection.y2*sy),width:round(selection.x2*sx)-round(selection.x1*sx),height:round(selection.y2*sy)-round(selection.y1*sy)}}function setSelection(x1,y1,x2,y2,noScale){var sx=noScale||scaleX,sy=noScale||scaleY;selection={x1:round(x1/sx||0),y1:round(y1/sy||0),x2:round(x2/sx||0),y2:round(y2/sy||0)};selection.width=selection.x2-selection.x1;selection.height=selection.y2-selection.y1}function adjust(){if(!$img.width())return;imgOfs={left:round($img.offset().left),top:round($img.offset().top)};imgWidth=$img.innerWidth();imgHeight=$img.innerHeight();imgOfs.top+=($img.outerHeight()-imgHeight)>>1;imgOfs.left+=($img.outerWidth()-imgWidth)>>1;minWidth=round(options.minWidth/scaleX)||0;minHeight=round(options.minHeight/scaleY)||0;maxWidth=round(min(options.maxWidth/scaleX||1<<24,imgWidth));maxHeight=round(min(options.maxHeight/scaleY||1<<24,imgHeight));if($().jquery=='1.3.2'&&position=='fixed'&&!docElem['getBoundingClientRect']){imgOfs.top+=max(document.body.scrollTop,docElem.scrollTop);imgOfs.left+=max(document.body.scrollLeft,docElem.scrollLeft)}parOfs=/absolute|relative/.test($parent.css('position'))?{left:round($parent.offset().left)-$parent.scrollLeft(),top:round($parent.offset().top)-$parent.scrollTop()}:position=='fixed'?{left:$(document).scrollLeft(),top:$(document).scrollTop()}:{left:0,top:0};left=viewX(0);top=viewY(0);if(selection.x2>imgWidth||selection.y2>imgHeight)doResize()}function update(resetKeyPress){if(!shown)return;$box.css({left:viewX(selection.x1),top:viewY(selection.y1)}).add($area).width(w=selection.width).height(h=selection.height);$area.add($border).add($handles).css({left:0,top:0});$border.width(max(w-$border.outerWidth()+$border.innerWidth(),0)).height(max(h-$border.outerHeight()+$border.innerHeight(),0));$($outer[0]).css({left:left,top:top,width:selection.x1,height:imgHeight});$($outer[1]).css({left:left+selection.x1,top:top,width:w,height:selection.y1});$($outer[2]).css({left:left+selection.x2,top:top,width:imgWidth-selection.x2,height:imgHeight});$($outer[3]).css({left:left+selection.x1,top:top+selection.y2,width:w,height:imgHeight-selection.y2});w-=$handles.outerWidth();h-=$handles.outerHeight();switch($handles.length){case 8:$($handles[4]).css({left:w>>1});$($handles[5]).css({left:w,top:h>>1});$($handles[6]).css({left:w>>1,top:h});$($handles[7]).css({top:h>>1});case 4:$handles.slice(1,3).css({left:w});$handles.slice(2,4).css({top:h})}if(resetKeyPress!==false){if($.imgAreaSelect.keyPress!=docKeyPress)$(document).unbind($.imgAreaSelect.keyPress,$.imgAreaSelect.onKeyPress);if(options.keys)$(document)[$.imgAreaSelect.keyPress]($.imgAreaSelect.onKeyPress=docKeyPress)}if($.browser.msie&&$border.outerWidth()-$border.innerWidth()==2){$border.css('margin',0);setTimeout(function(){$border.css('margin','auto')},0)}}function doUpdate(resetKeyPress){adjust();update(resetKeyPress);x1=viewX(selection.x1);y1=viewY(selection.y1);x2=viewX(selection.x2);y2=viewY(selection.y2)}function hide($elem,fn){options.fadeSpeed?$elem.fadeOut(options.fadeSpeed,fn):$elem.hide()}function areaMouseMove(event){var x=selX(evX(event))-selection.x1,y=selY(evY(event))-selection.y1;if(!adjusted){adjust();adjusted=true;$box.one('mouseout',function(){adjusted=false})}resize='';if(options.resizable){if(y<=options.resizeMargin)resize='n';else if(y>=selection.height-options.resizeMargin)resize='s';if(x<=options.resizeMargin)resize+='w';else if(x>=selection.width-options.resizeMargin)resize+='e'}$box.css('cursor',resize?resize+'-resize':options.movable?'move':'');if($areaOpera)$areaOpera.toggle()}function docMouseUp(event){$('body').css('cursor','');if(options.autoHide||selection.width*selection.height==0)hide($box.add($outer),function(){$(this).hide()});$(document).unbind('mousemove',selectingMouseMove);$box.mousemove(areaMouseMove);options.onSelectEnd(img,getSelection())}function areaMouseDown(event){if(event.which!=1)return false;adjust();if(resize){$('body').css('cursor',resize+'-resize');x1=viewX(selection[/w/.test(resize)?'x2':'x1']);y1=viewY(selection[/n/.test(resize)?'y2':'y1']);$(document).mousemove(selectingMouseMove).one('mouseup',docMouseUp);$box.unbind('mousemove',areaMouseMove)}else if(options.movable){startX=left+selection.x1-evX(event);startY=top+selection.y1-evY(event);$box.unbind('mousemove',areaMouseMove);$(document).mousemove(movingMouseMove).one('mouseup',function(){options.onSelectEnd(img,getSelection());$(document).unbind('mousemove',movingMouseMove);$box.mousemove(areaMouseMove)})}else $img.mousedown(event);return false}function fixAspectRatio(xFirst){if(aspectRatio)if(xFirst){x2=max(left,min(left+imgWidth,x1+abs(y2-y1)*aspectRatio*(x2>x1||-1)));y2=round(max(top,min(top+imgHeight,y1+abs(x2-x1)/aspectRatio*(y2>y1||-1))));x2=round(x2)}else{y2=max(top,min(top+imgHeight,y1+abs(x2-x1)/aspectRatio*(y2>y1||-1)));x2=round(max(left,min(left+imgWidth,x1+abs(y2-y1)*aspectRatio*(x2>x1||-1))));y2=round(y2)}}function doResize(){x1=min(x1,left+imgWidth);y1=min(y1,top+imgHeight);if(abs(x2-x1)<minWidth){x2=x1-minWidth*(x2<x1||-1);if(x2<left)x1=left+minWidth;else if(x2>left+imgWidth)x1=left+imgWidth-minWidth}if(abs(y2-y1)<minHeight){y2=y1-minHeight*(y2<y1||-1);if(y2<top)y1=top+minHeight;else if(y2>top+imgHeight)y1=top+imgHeight-minHeight}x2=max(left,min(x2,left+imgWidth));y2=max(top,min(y2,top+imgHeight));fixAspectRatio(abs(x2-x1)<abs(y2-y1)*aspectRatio);if(abs(x2-x1)>maxWidth){x2=x1-maxWidth*(x2<x1||-1);fixAspectRatio()}if(abs(y2-y1)>maxHeight){y2=y1-maxHeight*(y2<y1||-1);fixAspectRatio(true)}selection={x1:selX(min(x1,x2)),x2:selX(max(x1,x2)),y1:selY(min(y1,y2)),y2:selY(max(y1,y2)),width:abs(x2-x1),height:abs(y2-y1)};update();options.onSelectChange(img,getSelection())}function selectingMouseMove(event){x2=/w|e|^$/.test(resize)||aspectRatio?evX(event):viewX(selection.x2);y2=/n|s|^$/.test(resize)||aspectRatio?evY(event):viewY(selection.y2);doResize();return false}function doMove(newX1,newY1){x2=(x1=newX1)+selection.width;y2=(y1=newY1)+selection.height;$.extend(selection,{x1:selX(x1),y1:selY(y1),x2:selX(x2),y2:selY(y2)});update();options.onSelectChange(img,getSelection())}function movingMouseMove(event){x1=max(left,min(startX+evX(event),left+imgWidth-selection.width));y1=max(top,min(startY+evY(event),top+imgHeight-selection.height));doMove(x1,y1);event.preventDefault();return false}function startSelection(){$(document).unbind('mousemove',startSelection);adjust();x2=x1;y2=y1;doResize();resize='';if(!$outer.is(':visible'))$box.add($outer).hide().fadeIn(options.fadeSpeed||0);shown=true;$(document).unbind('mouseup',cancelSelection).mousemove(selectingMouseMove).one('mouseup',docMouseUp);$box.unbind('mousemove',areaMouseMove);options.onSelectStart(img,getSelection())}function cancelSelection(){$(document).unbind('mousemove',startSelection).unbind('mouseup',cancelSelection);hide($box.add($outer));setSelection(selX(x1),selY(y1),selX(x1),selY(y1));if(!this instanceof $.imgAreaSelect){options.onSelectChange(img,getSelection());options.onSelectEnd(img,getSelection())}}function imgMouseDown(event){if(event.which!=1||$outer.is(':animated'))return false;adjust();startX=x1=evX(event);startY=y1=evY(event);$(document).mousemove(startSelection).mouseup(cancelSelection);return false}function windowResize(){doUpdate(false)}function imgLoad(){imgLoaded=true;setOptions(options=$.extend({classPrefix:'imgareaselect',movable:true,parent:'body',resizable:true,resizeMargin:10,onInit:function(){},onSelectStart:function(){},onSelectChange:function(){},onSelectEnd:function(){}},options));$box.add($outer).css({visibility:''});if(options.show){shown=true;adjust();update();$box.add($outer).hide().fadeIn(options.fadeSpeed||0)}setTimeout(function(){options.onInit(img,getSelection())},0)}var docKeyPress=function(event){var k=options.keys,d,t,key=event.keyCode;d=!isNaN(k.alt)&&(event.altKey||event.originalEvent.altKey)?k.alt:!isNaN(k.ctrl)&&event.ctrlKey?k.ctrl:!isNaN(k.shift)&&event.shiftKey?k.shift:!isNaN(k.arrows)?k.arrows:10;if(k.arrows=='resize'||(k.shift=='resize'&&event.shiftKey)||(k.ctrl=='resize'&&event.ctrlKey)||(k.alt=='resize'&&(event.altKey||event.originalEvent.altKey))){switch(key){case 37:d=-d;case 39:t=max(x1,x2);x1=min(x1,x2);x2=max(t+d,x1);fixAspectRatio();break;case 38:d=-d;case 40:t=max(y1,y2);y1=min(y1,y2);y2=max(t+d,y1);fixAspectRatio(true);break;default:return}doResize()}else{x1=min(x1,x2);y1=min(y1,y2);switch(key){case 37:doMove(max(x1-d,left),y1);break;case 38:doMove(x1,max(y1-d,top));break;case 39:doMove(x1+min(d,imgWidth-selX(x2)),y1);break;case 40:doMove(x1,y1+min(d,imgHeight-selY(y2)));break;default:return}}return false};function styleOptions($elem,props){for(option in props)if(options[option]!==undefined)$elem.css(props[option],options[option])}function setOptions(newOptions){if(newOptions.parent)($parent=$(newOptions.parent)).append($box.add($outer));$.extend(options,newOptions);adjust();if(newOptions.handles!=null){$handles.remove();$handles=$([]);i=newOptions.handles?newOptions.handles=='corners'?4:8:0;while(i--)$handles=$handles.add(div());$handles.addClass(options.classPrefix+'-handle').css({position:'absolute',fontSize:0,zIndex:zIndex+1||1});if(!parseInt($handles.css('width'))>=0)$handles.width(5).height(5);if(o=options.borderWidth)$handles.css({borderWidth:o,borderStyle:'solid'});styleOptions($handles,{borderColor1:'border-color',borderColor2:'background-color',borderOpacity:'opacity'})}scaleX=options.imageWidth/imgWidth||1;scaleY=options.imageHeight/imgHeight||1;if(newOptions.x1!=null){setSelection(newOptions.x1,newOptions.y1,newOptions.x2,newOptions.y2);newOptions.show=!newOptions.hide}if(newOptions.keys)options.keys=$.extend({shift:1,ctrl:'resize'},newOptions.keys);$outer.addClass(options.classPrefix+'-outer');$area.addClass(options.classPrefix+'-selection');for(i=0;i++<4;)$($border[i-1]).addClass(options.classPrefix+'-border'+i);styleOptions($area,{selectionColor:'background-color',selectionOpacity:'opacity'});styleOptions($border,{borderOpacity:'opacity',borderWidth:'border-width'});styleOptions($outer,{outerColor:'background-color',outerOpacity:'opacity'});if(o=options.borderColor1)$($border[0]).css({borderStyle:'solid',borderColor:o});if(o=options.borderColor2)$($border[1]).css({borderStyle:'dashed',borderColor:o});$box.append($area.add($border).add($areaOpera).add($handles));if($.browser.msie){if(o=$outer.css('filter').match(/opacity=(\d+)/))$outer.css('opacity',o[1]/100);if(o=$border.css('filter').match(/opacity=(\d+)/))$border.css('opacity',o[1]/100)}if(newOptions.hide)hide($box.add($outer));else if(newOptions.show&&imgLoaded){shown=true;$box.add($outer).fadeIn(options.fadeSpeed||0);doUpdate()}aspectRatio=(d=(options.aspectRatio||'').split(/:/))[0]/d[1];$img.add($outer).unbind('mousedown',imgMouseDown);if(options.disable||options.enable===false){$box.unbind('mousemove',areaMouseMove).unbind('mousedown',areaMouseDown);$(window).unbind('resize',windowResize)}else{if(options.enable||options.disable===false){if(options.resizable||options.movable)$box.mousemove(areaMouseMove).mousedown(areaMouseDown);$(window).resize(windowResize)}if(!options.persistent)$img.add($outer).mousedown(imgMouseDown)}options.enable=options.disable=undefined}this.remove=function(){setOptions({disable:true});$box.add($outer).remove()};this.getOptions=function(){return options};this.setOptions=setOptions;this.getSelection=getSelection;this.setSelection=setSelection;this.cancelSelection=cancelSelection;this.update=doUpdate;$p=$img;while($p.length){zIndex=max(zIndex,!isNaN($p.css('z-index'))?$p.css('z-index'):zIndex);if($p.css('position')=='fixed')position='fixed';$p=$p.parent(':not(body)')}zIndex=options.zIndex||zIndex;if($.browser.msie)$img.attr('unselectable','on');$.imgAreaSelect.keyPress=$.browser.msie||$.browser.safari?'keydown':'keypress';if($.browser.opera)$areaOpera=div().css({width:'100%',height:'100%',position:'absolute',zIndex:zIndex+2||2});$box.add($outer).css({visibility:'hidden',position:position,overflow:'hidden',zIndex:zIndex||'0'});$box.css({zIndex:zIndex+2||2});$area.add($border).css({position:'absolute',fontSize:0});img.complete||img.readyState=='complete'||!$img.is('img')?imgLoad():$img.one('load',imgLoad);if($.browser.msie&&$.browser.version>=7)img.src=img.src};$.fn.imgAreaSelect=function(options){options=options||{};this.each(function(){if($(this).data('imgAreaSelect')){if(options.remove){$(this).data('imgAreaSelect').remove();$(this).removeData('imgAreaSelect')}else $(this).data('imgAreaSelect').setOptions(options)}else if(!options.remove){if(options.enable===undefined&&options.disable===undefined)options.enable=true;$(this).data('imgAreaSelect',new $.imgAreaSelect(this,options))}});if(options.instance)return $(this).data('imgAreaSelect');return this}})(jQuery);


 /*
 * jquery.dualListBox-1.3.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
*       Developed by Justin Mead
*       ©2011 MeadMiracle
*		www.meadmiracle.com / meadmiracle@gmail.com
*       Version 1.3
*       Testing: IE8/Windows XP
*                Firefox/Windows XP
*                Chrome/Windows XP
*       Licensed under the Creative Commons GPL http://creativecommons.org/licenses/GPL/2.0/
*/


(function ($) {
    var settings = new Array();
    var group1 = new Array();
    var group2 = new Array();
    var onSort = new Array();

    //the main method that the end user will execute to setup the DLB
    $.configureBoxes = function (options) {
        //define default settings
        var index = settings.push({
            box1View: 'box1View',
            box1Storage: 'box1Storage',
            box1Filter: 'box1Filter',
            box1Clear: 'box1Clear',
            box1Counter: 'box1Counter',
            box2View: 'box2View',
            box2Storage: 'box2Storage',
            box2Filter: 'box2Filter',
            box2Clear: 'box2Clear',
            box2Counter: 'box2Counter',
            to1: 'to1',
            allTo1: 'allTo1',
            to2: 'to2',
            allTo2: 'allTo2',
            transferMode: 'move',
            sortBy: 'value',
            useFilters: true,
            useCounters: true,
            useSorting: false,
            selectOnSubmit: false
        });

        index--;

        //merge default settings w/ user defined settings (with user-defined settings overriding defaults)
        $.extend(settings[index], options);

        //define box groups
        group1.push({
            view: settings[index].box1View,
            storage: settings[index].box1Storage,
            filter: settings[index].box1Filter,
            clear: settings[index].box1Clear,
            counter: settings[index].box1Counter,
            index: index
        });
        group2.push({
            view: settings[index].box2View,
            storage: settings[index].box2Storage,
            filter: settings[index].box2Filter,
            clear: settings[index].box2Clear,
            counter: settings[index].box2Counter,
            index: index
        });

        //define sort function
        if (settings[index].sortBy == 'text') {
            onSort.push(function (a, b) {
                var aVal = a.text.toLowerCase();
                var bVal = b.text.toLowerCase();
                if (aVal < bVal) { return -1; }
                if (aVal > bVal) { return 1; }
                return 0;
            });
        } else {
            onSort.push(function (a, b) {
                var aVal = a.value.toLowerCase();
                var bVal = b.value.toLowerCase();
                if (aVal < bVal) { return -1; }
                if (aVal > bVal) { return 1; }
                return 0;
            });
        }

        //configure events
        if (settings[index].useFilters) {
            $('#' + group1[index].filter).keyup(function () {
                Filter(group1[index]);
            });
            $('#' + group2[index].filter).keyup(function () {
                Filter(group2[index]);
            });
            $('#' + group1[index].clear).click(function () {
                ClearFilter(group1[index]);
            });
            $('#' + group2[index].clear).click(function () {
                ClearFilter(group2[index]);
            });
        }
        if (IsMoveMode(settings[index])) {
            $('#' + group2[index].view).dblclick(function () {
                MoveSelected(group2[index], group1[index]);
            });
            $('#' + settings[index].to1).click(function () {
                MoveSelected(group2[index], group1[index]);
            });
            $('#' + settings[index].allTo1).click(function () {
                MoveAll(group2[index], group1[index]);
            });
        } else {
            $('#' + group2[index].view).dblclick(function () {
                RemoveSelected(group2[index], group1[index]);
            });
            $('#' + settings[index].to1).click(function () {
                RemoveSelected(group2[index], group1[index]);

            });
            $('#' + settings[index].allTo1).click(function () {
                RemoveAll(group2[index], group1[index]);
            });
        }
        $('#' + group1[index].view).dblclick(function () {
            MoveSelected(group1[index], group2[index]);
        });
        $('#' + settings[index].to2).click(function () {
            MoveSelected(group1[index], group2[index]);
        });
        $('#' + settings[index].allTo2).click(function () {
            MoveAll(group1[index], group2[index]);
        });

        $('#btnup').bind('click', function () {
        
          if ($('#box2View option').length <= 1) {
               return false;
            }

            $('#box2View option:selected').each(function () {

                var newPos = $('#box2View option').index(this) - 1;
                if (newPos > -1) {
                    $('#box2View option').eq(newPos).before("<option value='" + $(this).val() + "' selected='selected'>" + $(this).text() + "</option>");
                    $(this).remove();
                }
            });
//            $('#btnToRadioNext').trigger('click');
        });
        $('#btndown').bind('click', function () {
        if ($('#box2View option').length <= 1) {
               return false;
            }

            var countOptions = $('#box2View option').size();
            $('#box2View option:selected').each(function () {
                var newPos = $('#box2View option').index(this) + 1;
                if (newPos < countOptions) {
                    $('#box2View option').eq(newPos).after("<option value='" + $(this).val() + "' selected='selected'>" + $(this).text() + "</option>");
                    $(this).remove();
                }
            });
//            $('#btnToRadioNext').trigger('click');
        });


        //initialize the counters
        if (settings[index].useCounters) {
            UpdateLabel(group1[index]);
            UpdateLabel(group2[index]);
        }

        //pre-sort item sets
        if (settings[index].useSorting) {
            SortOptions(group1[index]);
            SortOptions(group2[index]);
        }

        //hide the storage boxes
        $('#' + group1[index].storage + ',#' + group2[index].storage).css('display', 'none');

        //attach onSubmit functionality if desired
        if (settings[index].selectOnSubmit) {
            $('#' + settings[index].box2View).closest('form').submit(function () {
                $('#' + settings[index].box2View).children('option').attr('selected', 'selected');
            });
        }
    };

    function UpdateLabel(group) {
        var showingCount = $("#" + group.view + " option").size();
        var hiddenCount = $("#" + group.storage + " option").size();
        $("#" + group.counter).text('Showing ' + showingCount + ' of ' + (showingCount + hiddenCount));
    }

    function Filter(group) {
        var index = group.index;
        var filterLower;
        if (settings[index].useFilters) {
            filterLower = $('#' + group.filter).val().toString().toLowerCase();
        } else {
            filterLower = '';
        }
        $('#' + group.view + ' option').filter(function (i) {
            var toMatch = $(this).text().toString().toLowerCase();
            return toMatch.indexOf(filterLower) == -1;
        }).appendTo('#' + group.storage);
        $('#' + group.storage + ' option').filter(function (i) {
            var toMatch = $(this).text().toString().toLowerCase();
            return toMatch.indexOf(filterLower) != -1;
        }).appendTo('#' + group.view);
        try {
            $('#' + group.view + ' option').removeAttr('selected');
        }
        catch (ex) {
            //swallow the error for IE6
        }
        if (settings[index].useSorting) { SortOptions(group); }
        if (settings[index].useCounters) { UpdateLabel(group); }
    }

    function SortOptions(group) {
        var $toSortOptions = $('#' + group.view + ' option');
        $toSortOptions.sort(onSort[group.index]);
        $('#' + group.view).empty().append($toSortOptions);
    }

    function MoveSelected(fromGroup, toGroup) {
      
        if (IsMoveMode(settings[fromGroup.index])) {
            $('#' + fromGroup.view + ' option:selected').appendTo('#' + toGroup.view);
        } else {
            $('#' + fromGroup.view + ' option:selected:not([class*=copiedOption])').clone().appendTo('#' + toGroup.view).end().end().addClass('copiedOption');
        }
        try {
            $('#' + fromGroup.view + ' option,#' + toGroup.view + ' option').removeAttr('selected');
        }
        catch (ex) {
            //swallow the error for IE6
        }
        //        alert("Moved single to 1...");
        $('#btnToRadioNext').trigger('click');
        Filter(toGroup);
        if (settings[fromGroup.index].useCounters) { UpdateLabel(fromGroup); }
        
    }

    function MoveAll(fromGroup, toGroup) {
        if (IsMoveMode(settings[fromGroup.index])) {
            $('#' + fromGroup.view + ' option').appendTo('#' + toGroup.view);
        } else {
            $('#' + fromGroup.view + ' option:not([class*=copiedOption])').clone().appendTo('#' + toGroup.view).end().end().addClass('copiedOption');
        }
        try {
            $('#' + fromGroup.view + ' option,#' + toGroup.view + ' option').removeAttr('selected');
        }
       
        catch (ex) {
            //swallow the error for IE6
        }
        //        alert("Moved all to 1...");
        $('#btnToRadioNext').trigger('click');
        Filter(toGroup);
        if (settings[fromGroup.index].useCounters) { UpdateLabel(fromGroup); }
     
    }

    function RemoveSelected(removeGroup, otherGroup) {
        $('#' + otherGroup.view + ' option.copiedOption').add('#' + otherGroup.storage + ' option.copiedOption').remove();
        try {
            $('#' + removeGroup.view + ' option:selected').appendTo('#' + otherGroup.view).removeAttr('selected');
        }
        catch (ex) {
            //swallow the error for IE6
        }
        $('#' + removeGroup.view + ' option').add('#' + removeGroup.storage + ' option').clone().addClass('copiedOption').appendTo('#' + otherGroup.view);
        Filter(otherGroup);
        if (settings[removeGroup.index].useCounters) { UpdateLabel(removeGroup); }
        //        alert("Removed single to 2...");
    }

    function RemoveAll(removeGroup, otherGroup) {
        $('#' + otherGroup.view + ' option.copiedOption').add('#' + otherGroup.storage + ' option.copiedOption').remove();
        try {
            $('#' + removeGroup.storage + ' option').clone().addClass('copiedOption').add('#' + removeGroup.view + ' option').appendTo('#' + otherGroup.view).removeAttr('selected');
        }
        catch (ex) {
            //swallow the error for IE6
        }
        Filter(otherGroup);
        if (settings[removeGroup.index].useCounters) { UpdateLabel(removeGroup); }
        //        alert("Removed all to 2...");
    }

    function ClearFilter(group) {
        $('#' + group.filter).val('');
        $('#' + group.storage + ' option').appendTo('#' + group.view);
        try {
            $('#' + group.view + ' option').removeAttr('selected');
        }
        catch (ex) {
            //swallow the error for IE6
        }
        if (settings[group.index].useSorting) { SortOptions(group); }
        if (settings[group.index].useCounters) { UpdateLabel(group); }
    }

    function IsMoveMode(currSettings) {
        return currSettings.transferMode == 'move';
    }
})(jQuery);

/*
 * jquery.jgrowl-min.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 */

 (function(a){a.jGrowl=function(b,c){if(a("#jGrowl").size()==0){a('<div id="jGrowl"></div>').addClass((c&&c.position)?c.position:a.jGrowl.defaults.position).appendTo("body")}a("#jGrowl").jGrowl(b,c)};a.fn.jGrowl=function(b,d){if(a.isFunction(this.each)){var c=arguments;return this.each(function(){var e=this;if(a(this).data("jGrowl.instance")==undefined){a(this).data("jGrowl.instance",a.extend(new a.fn.jGrowl(),{notifications:[],element:null,interval:null}));a(this).data("jGrowl.instance").startup(this)}if(a.isFunction(a(this).data("jGrowl.instance")[b])){a(this).data("jGrowl.instance")[b].apply(a(this).data("jGrowl.instance"),a.makeArray(c).slice(1))}else{a(this).data("jGrowl.instance").create(b,d)}})}};a.extend(a.fn.jGrowl.prototype,{defaults:{pool:0,header:"",group:"",sticky:false,position:"top-right",glue:"after",theme:"default",themeState:"highlight",corners:"10px",check:250,life:3000,closeDuration:"normal",openDuration:"normal",easing:"swing",closer:true,closeTemplate:"&times;",closerTemplate:"<div>[ close all ]</div>",log:function(c,b,d){},beforeOpen:function(c,b,d){},afterOpen:function(c,b,d){},open:function(c,b,d){},beforeClose:function(c,b,d){},close:function(c,b,d){},animateOpen:{opacity:"show"},animateClose:{opacity:"hide"}},notifications:[],element:null,interval:null,create:function(b,c){var c=a.extend({},this.defaults,c);if(typeof c.speed!=="undefined"){c.openDuration=c.speed;c.closeDuration=c.speed}this.notifications.push({message:b,options:c});c.log.apply(this.element,[this.element,b,c])},render:function(d){var b=this;var c=d.message;var e=d.options;e.themeState=(e.themeState=="")?"":"ui-state-"+e.themeState;var d=a('<div class="jGrowl-notification '+e.themeState+" ui-corner-all"+((e.group!=undefined&&e.group!="")?" "+e.group:"")+'"><div class="jGrowl-close">'+e.closeTemplate+'</div><div class="jGrowl-header">'+e.header+'</div><div class="jGrowl-message">'+c+"</div></div>").data("jGrowl",e).addClass(e.theme).children("div.jGrowl-close").bind("click.jGrowl",function(){a(this).parent().trigger("jGrowl.close")}).parent();a(d).bind("mouseover.jGrowl",function(){a("div.jGrowl-notification",b.element).data("jGrowl.pause",true)}).bind("mouseout.jGrowl",function(){a("div.jGrowl-notification",b.element).data("jGrowl.pause",false)}).bind("jGrowl.beforeOpen",function(){if(e.beforeOpen.apply(d,[d,c,e,b.element])!=false){a(this).trigger("jGrowl.open")}}).bind("jGrowl.open",function(){if(e.open.apply(d,[d,c,e,b.element])!=false){if(e.glue=="after"){a("div.jGrowl-notification:last",b.element).after(d)}else{a("div.jGrowl-notification:first",b.element).before(d)}a(this).animate(e.animateOpen,e.openDuration,e.easing,function(){if(a.browser.msie&&(parseInt(a(this).css("opacity"),10)===1||parseInt(a(this).css("opacity"),10)===0)){this.style.removeAttribute("filter")}if(a(this).data("jGrowl")!=null){a(this).data("jGrowl").created=new Date()}a(this).trigger("jGrowl.afterOpen")})}}).bind("jGrowl.afterOpen",function(){e.afterOpen.apply(d,[d,c,e,b.element])}).bind("jGrowl.beforeClose",function(){if(e.beforeClose.apply(d,[d,c,e,b.element])!=false){a(this).trigger("jGrowl.close")}}).bind("jGrowl.close",function(){a(this).data("jGrowl.pause",true);a(this).animate(e.animateClose,e.closeDuration,e.easing,function(){if(a.isFunction(e.close)){if(e.close.apply(d,[d,c,e,b.element])!==false){a(this).remove()}}else{a(this).remove()}})}).trigger("jGrowl.beforeOpen");if(e.corners!=""&&a.fn.corner!=undefined){a(d).corner(e.corners)}if(a("div.jGrowl-notification:parent",b.element).size()>1&&a("div.jGrowl-closer",b.element).size()==0&&this.defaults.closer!=false){a(this.defaults.closerTemplate).addClass("jGrowl-closer "+this.defaults.themeState+" ui-corner-all").addClass(this.defaults.theme).appendTo(b.element).animate(this.defaults.animateOpen,this.defaults.speed,this.defaults.easing).bind("click.jGrowl",function(){a(this).siblings().trigger("jGrowl.beforeClose");if(a.isFunction(b.defaults.closer)){b.defaults.closer.apply(a(this).parent()[0],[a(this).parent()[0]])}})}},update:function(){a(this.element).find("div.jGrowl-notification:parent").each(function(){if(a(this).data("jGrowl")!=undefined&&a(this).data("jGrowl").created!=undefined&&(a(this).data("jGrowl").created.getTime()+parseInt(a(this).data("jGrowl").life))<(new Date()).getTime()&&a(this).data("jGrowl").sticky!=true&&(a(this).data("jGrowl.pause")==undefined||a(this).data("jGrowl.pause")!=true)){a(this).trigger("jGrowl.beforeClose")}});if(this.notifications.length>0&&(this.defaults.pool==0||a(this.element).find("div.jGrowl-notification:parent").size()<this.defaults.pool)){this.render(this.notifications.shift())}if(a(this.element).find("div.jGrowl-notification:parent").size()<2){a(this.element).find("div.jGrowl-closer").animate(this.defaults.animateClose,this.defaults.speed,this.defaults.easing,function(){a(this).remove()})}},startup:function(b){this.element=a(b).addClass("jGrowl").append('<div class="jGrowl-notification"></div>');this.interval=setInterval(function(){a(b).data("jGrowl.instance").update()},parseInt(this.defaults.check));if(a.browser.msie&&parseInt(a.browser.version)<7&&!window.XMLHttpRequest){a(this.element).addClass("ie6")}},shutdown:function(){a(this.element).removeClass("jGrowl").find("div.jGrowl-notification").remove();clearInterval(this.interval)},close:function(){a(this.element).find("div.jGrowl-notification").each(function(){a(this).trigger("jGrowl.beforeClose")})}});a.jGrowl.defaults=a.fn.jGrowl.prototype.defaults})(jQuery);


 
 
 /*
 * jquery.dataTables.min.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 * File:        jquery.dataTables.min.js
 * Version:     1.9.1
 * Author:      Allan Jardine (www.sprymedia.co.uk)
 * Info:        www.datatables.net
 * 
 * Copyright 2008-2012 Allan Jardine, all rights reserved.
 *
 * This source file is free software, under either the GPL v2 license or a
 * BSD style license, available at:
 *   http://datatables.net/license_gpl2
 *   http://datatables.net/license_bsd
 * 
 * This source file is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the license files for details.
 */
(function(h,V,l,m){var j=function(e){function o(a,b){var c=j.defaults.columns,d=a.aoColumns.length,c=h.extend({},j.models.oColumn,c,{sSortingClass:a.oClasses.sSortable,sSortingClassJUI:a.oClasses.sSortJUI,nTh:b?b:l.createElement("th"),sTitle:c.sTitle?c.sTitle:b?b.innerHTML:"",aDataSort:c.aDataSort?c.aDataSort:[d],mDataProp:c.mDataProp?c.oDefaults:d});a.aoColumns.push(c);if(a.aoPreSearchCols[d]===m||null===a.aoPreSearchCols[d])a.aoPreSearchCols[d]=h.extend({},j.models.oSearch);else{c=a.aoPreSearchCols[d];
if(c.bRegex===m)c.bRegex=!0;if(c.bSmart===m)c.bSmart=!0;if(c.bCaseInsensitive===m)c.bCaseInsensitive=!0}s(a,d,null)}function s(a,b,c){b=a.aoColumns[b];if(c!==m&&null!==c){if(c.sType!==m)b.sType=c.sType,b._bAutoType=!1;h.extend(b,c);p(b,c,"sWidth","sWidthOrig");if(c.iDataSort!==m)b.aDataSort=[c.iDataSort];p(b,c,"aDataSort")}b.fnGetData=W(b.mDataProp);b.fnSetData=ta(b.mDataProp);if(!a.oFeatures.bSort)b.bSortable=!1;if(!b.bSortable||-1==h.inArray("asc",b.asSorting)&&-1==h.inArray("desc",b.asSorting))b.sSortingClass=
a.oClasses.sSortableNone,b.sSortingClassJUI="";else if(b.bSortable||-1==h.inArray("asc",b.asSorting)&&-1==h.inArray("desc",b.asSorting))b.sSortingClass=a.oClasses.sSortable,b.sSortingClassJUI=a.oClasses.sSortJUI;else if(-1!=h.inArray("asc",b.asSorting)&&-1==h.inArray("desc",b.asSorting))b.sSortingClass=a.oClasses.sSortableAsc,b.sSortingClassJUI=a.oClasses.sSortJUIAscAllowed;else if(-1==h.inArray("asc",b.asSorting)&&-1!=h.inArray("desc",b.asSorting))b.sSortingClass=a.oClasses.sSortableDesc,b.sSortingClassJUI=
a.oClasses.sSortJUIDescAllowed}function k(a){if(!1===a.oFeatures.bAutoWidth)return!1;ba(a);for(var b=0,c=a.aoColumns.length;b<c;b++)a.aoColumns[b].nTh.style.width=a.aoColumns[b].sWidth}function x(a,b){for(var c=-1,d=0;d<a.aoColumns.length;d++)if(!0===a.aoColumns[d].bVisible&&c++,c==b)return d;return null}function r(a,b){for(var c=-1,d=0;d<a.aoColumns.length;d++)if(!0===a.aoColumns[d].bVisible&&c++,d==b)return!0===a.aoColumns[d].bVisible?c:null;return null}function v(a){for(var b=0,c=0;c<a.aoColumns.length;c++)!0===
a.aoColumns[c].bVisible&&b++;return b}function A(a){for(var b=j.ext.aTypes,c=b.length,d=0;d<c;d++){var f=b[d](a);if(null!==f)return f}return"string"}function E(a,b){for(var c=b.split(","),d=[],f=0,g=a.aoColumns.length;f<g;f++)for(var i=0;i<g;i++)if(a.aoColumns[f].sName==c[i]){d.push(i);break}return d}function y(a){for(var b="",c=0,d=a.aoColumns.length;c<d;c++)b+=a.aoColumns[c].sName+",";return b.length==d?"":b.slice(0,-1)}function J(a,b,c,d){var f,g,i,e,u;if(b)for(f=b.length-1;0<=f;f--){var n=b[f].aTargets;
h.isArray(n)||F(a,1,"aTargets must be an array of targets, not a "+typeof n);for(g=0,i=n.length;g<i;g++)if("number"===typeof n[g]&&0<=n[g]){for(;a.aoColumns.length<=n[g];)o(a);d(n[g],b[f])}else if("number"===typeof n[g]&&0>n[g])d(a.aoColumns.length+n[g],b[f]);else if("string"===typeof n[g])for(e=0,u=a.aoColumns.length;e<u;e++)("_all"==n[g]||h(a.aoColumns[e].nTh).hasClass(n[g]))&&d(e,b[f])}if(c)for(f=0,a=c.length;f<a;f++)d(f,c[f])}function H(a,b){var c;c=h.isArray(b)?b.slice():h.extend(!0,{},b);var d=
a.aoData.length,f=h.extend(!0,{},j.models.oRow);f._aData=c;a.aoData.push(f);for(var g,f=0,i=a.aoColumns.length;f<i;f++)if(c=a.aoColumns[f],"function"===typeof c.fnRender&&c.bUseRendered&&null!==c.mDataProp?I(a,d,f,R(a,d,f)):I(a,d,f,w(a,d,f)),c._bAutoType&&"string"!=c.sType&&(g=w(a,d,f,"type"),null!==g&&""!==g))if(g=A(g),null===c.sType)c.sType=g;else if(c.sType!=g&&"html"!=c.sType)c.sType="string";a.aiDisplayMaster.push(d);a.oFeatures.bDeferRender||ca(a,d);return d}function ua(a){var b,c,d,f,g,i,e,
u,n;if(a.bDeferLoading||null===a.sAjaxSource){e=a.nTBody.childNodes;for(b=0,c=e.length;b<c;b++)if("TR"==e[b].nodeName.toUpperCase()){u=a.aoData.length;e[b]._DT_RowIndex=u;a.aoData.push(h.extend(!0,{},j.models.oRow,{nTr:e[b]}));a.aiDisplayMaster.push(u);i=e[b].childNodes;g=0;for(d=0,f=i.length;d<f;d++)if(n=i[d].nodeName.toUpperCase(),"TD"==n||"TH"==n)I(a,u,g,h.trim(i[d].innerHTML)),g++}}e=S(a);i=[];for(b=0,c=e.length;b<c;b++)for(d=0,f=e[b].childNodes.length;d<f;d++)g=e[b].childNodes[d],n=g.nodeName.toUpperCase(),
("TD"==n||"TH"==n)&&i.push(g);for(f=0,e=a.aoColumns.length;f<e;f++){n=a.aoColumns[f];if(null===n.sTitle)n.sTitle=n.nTh.innerHTML;g=n._bAutoType;u="function"===typeof n.fnRender;var o=null!==n.sClass,k=n.bVisible,m,s;if(g||u||o||!k)for(b=0,c=a.aoData.length;b<c;b++){d=a.aoData[b];m=i[b*e+f];if(g&&"string"!=n.sType&&(s=w(a,b,f,"type"),""!==s))if(s=A(s),null===n.sType)n.sType=s;else if(n.sType!=s&&"html"!=n.sType)n.sType="string";if("function"===typeof n.mDataProp)m.innerHTML=w(a,b,f,"display");if(u)s=
R(a,b,f),m.innerHTML=s,n.bUseRendered&&I(a,b,f,s);o&&(m.className+=" "+n.sClass);k?d._anHidden[f]=null:(d._anHidden[f]=m,m.parentNode.removeChild(m));n.fnCreatedCell&&n.fnCreatedCell.call(a.oInstance,m,w(a,b,f,"display"),d._aData,b,f)}}if(0!==a.aoRowCreatedCallback.length)for(b=0,c=a.aoData.length;b<c;b++)d=a.aoData[b],D(a,"aoRowCreatedCallback",null,[d.nTr,d._aData,b])}function K(a,b){return b._DT_RowIndex!==m?b._DT_RowIndex:null}function da(a,b,c){for(var b=L(a,b),d=0,a=a.aoColumns.length;d<a;d++)if(b[d]===
c)return d;return-1}function X(a,b,c){for(var d=[],f=0,g=a.aoColumns.length;f<g;f++)d.push(w(a,b,f,c));return d}function w(a,b,c,d){var f=a.aoColumns[c];if((c=f.fnGetData(a.aoData[b]._aData,d))===m){if(a.iDrawError!=a.iDraw&&null===f.sDefaultContent)F(a,0,"Requested unknown parameter "+("function"==typeof f.mDataProp?"{mDataprop function}":"'"+f.mDataProp+"'")+" from the data source for row "+b),a.iDrawError=a.iDraw;return f.sDefaultContent}if(null===c&&null!==f.sDefaultContent)c=f.sDefaultContent;
else if("function"===typeof c)return c();return"display"==d&&null===c?"":c}function I(a,b,c,d){a.aoColumns[c].fnSetData(a.aoData[b]._aData,d)}function W(a){if(null===a)return function(){return null};if("function"===typeof a)return function(b,d){return a(b,d)};if("string"===typeof a&&-1!=a.indexOf(".")){var b=a.split(".");return function(a){for(var d=0,f=b.length;d<f;d++)if(a=a[b[d]],a===m)return m;return a}}return function(b){return b[a]}}function ta(a){if(null===a)return function(){};if("function"===
typeof a)return function(b,d){a(b,"set",d)};if("string"===typeof a&&-1!=a.indexOf(".")){var b=a.split(".");return function(a,d){for(var f=0,g=b.length-1;f<g;f++)if(a=a[b[f]],a===m)return;a[b[b.length-1]]=d}}return function(b,d){b[a]=d}}function Y(a){for(var b=[],c=a.aoData.length,d=0;d<c;d++)b.push(a.aoData[d]._aData);return b}function ea(a){a.aoData.splice(0,a.aoData.length);a.aiDisplayMaster.splice(0,a.aiDisplayMaster.length);a.aiDisplay.splice(0,a.aiDisplay.length);B(a)}function fa(a,b){for(var c=
-1,d=0,f=a.length;d<f;d++)a[d]==b?c=d:a[d]>b&&a[d]--; -1!=c&&a.splice(c,1)}function R(a,b,c){var d=a.aoColumns[c];return d.fnRender({iDataRow:b,iDataColumn:c,oSettings:a,aData:a.aoData[b]._aData,mDataProp:d.mDataProp},w(a,b,c,"display"))}function ca(a,b){var c=a.aoData[b],d;if(null===c.nTr){c.nTr=l.createElement("tr");c.nTr._DT_RowIndex=b;if(c._aData.DT_RowId)c.nTr.id=c._aData.DT_RowId;c._aData.DT_RowClass&&h(c.nTr).addClass(c._aData.DT_RowClass);for(var f=0,g=a.aoColumns.length;f<g;f++){var i=a.aoColumns[f];
d=l.createElement(i.sCellType);d.innerHTML="function"===typeof i.fnRender&&(!i.bUseRendered||null===i.mDataProp)?R(a,b,f):w(a,b,f,"display");if(null!==i.sClass)d.className=i.sClass;i.bVisible?(c.nTr.appendChild(d),c._anHidden[f]=null):c._anHidden[f]=d;i.fnCreatedCell&&i.fnCreatedCell.call(a.oInstance,d,w(a,b,f,"display"),c._aData,b,f)}D(a,"aoRowCreatedCallback",null,[c.nTr,c._aData,b])}}function va(a){var b,c,d;if(0!==a.nTHead.getElementsByTagName("th").length)for(b=0,d=a.aoColumns.length;b<d;b++){if(c=
a.aoColumns[b].nTh,c.setAttribute("role","columnheader"),a.aoColumns[b].bSortable&&(c.setAttribute("tabindex",a.iTabIndex),c.setAttribute("aria-controls",a.sTableId)),null!==a.aoColumns[b].sClass&&h(c).addClass(a.aoColumns[b].sClass),a.aoColumns[b].sTitle!=c.innerHTML)c.innerHTML=a.aoColumns[b].sTitle}else{var f=l.createElement("tr");for(b=0,d=a.aoColumns.length;b<d;b++)c=a.aoColumns[b].nTh,c.innerHTML=a.aoColumns[b].sTitle,c.setAttribute("tabindex","0"),null!==a.aoColumns[b].sClass&&h(c).addClass(a.aoColumns[b].sClass),
f.appendChild(c);h(a.nTHead).html("")[0].appendChild(f);T(a.aoHeader,a.nTHead)}h(a.nTHead).children("tr").attr("role","row");if(a.bJUI)for(b=0,d=a.aoColumns.length;b<d;b++){c=a.aoColumns[b].nTh;f=l.createElement("div");f.className=a.oClasses.sSortJUIWrapper;h(c).contents().appendTo(f);var g=l.createElement("span");g.className=a.oClasses.sSortIcon;f.appendChild(g);c.appendChild(f)}if(a.oFeatures.bSort)for(b=0;b<a.aoColumns.length;b++)!1!==a.aoColumns[b].bSortable?ga(a,a.aoColumns[b].nTh,b):h(a.aoColumns[b].nTh).addClass(a.oClasses.sSortableNone);
""!==a.oClasses.sFooterTH&&h(a.nTFoot).children("tr").children("th").addClass(a.oClasses.sFooterTH);if(null!==a.nTFoot){c=O(a,null,a.aoFooter);for(b=0,d=a.aoColumns.length;b<d;b++)if(c[b])a.aoColumns[b].nTf=c[b],a.aoColumns[b].sClass&&h(c[b]).addClass(a.aoColumns[b].sClass)}}function U(a,b,c){var d,f,g,i=[],e=[],h=a.aoColumns.length,n;c===m&&(c=!1);for(d=0,f=b.length;d<f;d++){i[d]=b[d].slice();i[d].nTr=b[d].nTr;for(g=h-1;0<=g;g--)!a.aoColumns[g].bVisible&&!c&&i[d].splice(g,1);e.push([])}for(d=0,f=
i.length;d<f;d++){if(a=i[d].nTr)for(;g=a.firstChild;)a.removeChild(g);for(g=0,b=i[d].length;g<b;g++)if(n=h=1,e[d][g]===m){a.appendChild(i[d][g].cell);for(e[d][g]=1;i[d+h]!==m&&i[d][g].cell==i[d+h][g].cell;)e[d+h][g]=1,h++;for(;i[d][g+n]!==m&&i[d][g].cell==i[d][g+n].cell;){for(c=0;c<h;c++)e[d+c][g+n]=1;n++}i[d][g].cell.rowSpan=h;i[d][g].cell.colSpan=n}}}function z(a){var b,c,d=[],f=0,g=a.asStripeClasses.length;b=a.aoOpenRows.length;c=D(a,"aoPreDrawCallback","preDraw",[a]);if(-1!==h.inArray(!1,c))G(a,
!1);else{a.bDrawing=!0;if(a.iInitDisplayStart!==m&&-1!=a.iInitDisplayStart)a._iDisplayStart=a.oFeatures.bServerSide?a.iInitDisplayStart:a.iInitDisplayStart>=a.fnRecordsDisplay()?0:a.iInitDisplayStart,a.iInitDisplayStart=-1,B(a);if(a.bDeferLoading)a.bDeferLoading=!1,a.iDraw++;else if(a.oFeatures.bServerSide){if(!a.bDestroying&&!wa(a))return}else a.iDraw++;if(0!==a.aiDisplay.length){var i=a._iDisplayStart;c=a._iDisplayEnd;if(a.oFeatures.bServerSide)i=0,c=a.aoData.length;for(;i<c;i++){var e=a.aoData[a.aiDisplay[i]];
null===e.nTr&&ca(a,a.aiDisplay[i]);var j=e.nTr;if(0!==g){var n=a.asStripeClasses[f%g];if(e._sRowStripe!=n)h(j).removeClass(e._sRowStripe).addClass(n),e._sRowStripe=n}D(a,"aoRowCallback",null,[j,a.aoData[a.aiDisplay[i]]._aData,f,i]);d.push(j);f++;if(0!==b)for(e=0;e<b;e++)if(j==a.aoOpenRows[e].nParent){d.push(a.aoOpenRows[e].nTr);break}}}else{d[0]=l.createElement("tr");if(a.asStripeClasses[0])d[0].className=a.asStripeClasses[0];b=a.oLanguage;g=b.sZeroRecords;if(1==a.iDraw&&null!==a.sAjaxSource&&!a.oFeatures.bServerSide)g=
b.sLoadingRecords;else if(b.sEmptyTable&&0===a.fnRecordsTotal())g=b.sEmptyTable;b=l.createElement("td");b.setAttribute("valign","top");b.colSpan=v(a);b.className=a.oClasses.sRowEmpty;b.innerHTML=ha(a,g);d[f].appendChild(b)}D(a,"aoHeaderCallback","header",[h(a.nTHead).children("tr")[0],Y(a),a._iDisplayStart,a.fnDisplayEnd(),a.aiDisplay]);D(a,"aoFooterCallback","footer",[h(a.nTFoot).children("tr")[0],Y(a),a._iDisplayStart,a.fnDisplayEnd(),a.aiDisplay]);f=l.createDocumentFragment();b=l.createDocumentFragment();
if(a.nTBody){g=a.nTBody.parentNode;b.appendChild(a.nTBody);if(!a.oScroll.bInfinite||!a._bInitComplete||a.bSorted||a.bFiltered)for(;b=a.nTBody.firstChild;)a.nTBody.removeChild(b);for(b=0,c=d.length;b<c;b++)f.appendChild(d[b]);a.nTBody.appendChild(f);null!==g&&g.appendChild(a.nTBody)}D(a,"aoDrawCallback","draw",[a]);a.bSorted=!1;a.bFiltered=!1;a.bDrawing=!1;a.oFeatures.bServerSide&&(G(a,!1),a._bInitComplete||Z(a))}}function $(a){a.oFeatures.bSort?P(a,a.oPreviousSearch):a.oFeatures.bFilter?M(a,a.oPreviousSearch):
(B(a),z(a))}function xa(a){var b=h("<div></div>")[0];a.nTable.parentNode.insertBefore(b,a.nTable);a.nTableWrapper=h('<div id="'+a.sTableId+'_wrapper" class="'+a.oClasses.sWrapper+'" role="grid"></div>')[0];a.nTableReinsertBefore=a.nTable.nextSibling;for(var c=a.nTableWrapper,d=a.sDom.split(""),f,g,i,e,u,n,o,k=0;k<d.length;k++){g=0;i=d[k];if("<"==i){e=h("<div></div>")[0];u=d[k+1];if("'"==u||'"'==u){n="";for(o=2;d[k+o]!=u;)n+=d[k+o],o++;"H"==n?n="fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix":
"F"==n&&(n="fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix");-1!=n.indexOf(".")?(u=n.split("."),e.id=u[0].substr(1,u[0].length-1),e.className=u[1]):"#"==n.charAt(0)?e.id=n.substr(1,n.length-1):e.className=n;k+=o}c.appendChild(e);c=e}else if(">"==i)c=c.parentNode;else if("l"==i&&a.oFeatures.bPaginate&&a.oFeatures.bLengthChange)f=ya(a),g=1;else if("f"==i&&a.oFeatures.bFilter)f=za(a),g=1;else if("r"==i&&a.oFeatures.bProcessing)f=Aa(a),g=1;else if("t"==i)f=Ba(a),g=
1;else if("i"==i&&a.oFeatures.bInfo)f=Ca(a),g=1;else if("p"==i&&a.oFeatures.bPaginate)f=Da(a),g=1;else if(0!==j.ext.aoFeatures.length){e=j.ext.aoFeatures;o=0;for(u=e.length;o<u;o++)if(i==e[o].cFeature){(f=e[o].fnInit(a))&&(g=1);break}}1==g&&null!==f&&("object"!==typeof a.aanFeatures[i]&&(a.aanFeatures[i]=[]),a.aanFeatures[i].push(f),c.appendChild(f))}b.parentNode.replaceChild(a.nTableWrapper,b)}function T(a,b){var c=h(b).children("tr"),d,f,g,i,e,j,n,o;a.splice(0,a.length);for(f=0,j=c.length;f<j;f++)a.push([]);
for(f=0,j=c.length;f<j;f++)for(g=0,n=c[f].childNodes.length;g<n;g++)if(d=c[f].childNodes[g],"TD"==d.nodeName.toUpperCase()||"TH"==d.nodeName.toUpperCase()){var k=1*d.getAttribute("colspan"),m=1*d.getAttribute("rowspan"),k=!k||0===k||1===k?1:k,m=!m||0===m||1===m?1:m;for(i=0;a[f][i];)i++;o=i;for(e=0;e<k;e++)for(i=0;i<m;i++)a[f+i][o+e]={cell:d,unique:1==k?!0:!1},a[f+i].nTr=c[f]}}function O(a,b,c){var d=[];if(!c)c=a.aoHeader,b&&(c=[],T(c,b));for(var b=0,f=c.length;b<f;b++)for(var g=0,i=c[b].length;g<
i;g++)if(c[b][g].unique&&(!d[g]||!a.bSortCellsTop))d[g]=c[b][g].cell;return d}function wa(a){if(a.bAjaxDataGet){a.iDraw++;G(a,!0);var b=Ea(a);ia(a,b);a.fnServerData.call(a.oInstance,a.sAjaxSource,b,function(b){Fa(a,b)},a);return!1}return!0}function Ea(a){var b=a.aoColumns.length,c=[],d,f,g,i;c.push({name:"sEcho",value:a.iDraw});c.push({name:"iColumns",value:b});c.push({name:"sColumns",value:y(a)});c.push({name:"iDisplayStart",value:a._iDisplayStart});c.push({name:"iDisplayLength",value:!1!==a.oFeatures.bPaginate?
a._iDisplayLength:-1});for(g=0;g<b;g++)d=a.aoColumns[g].mDataProp,c.push({name:"mDataProp_"+g,value:"function"===typeof d?"function":d});if(!1!==a.oFeatures.bFilter){c.push({name:"sSearch",value:a.oPreviousSearch.sSearch});c.push({name:"bRegex",value:a.oPreviousSearch.bRegex});for(g=0;g<b;g++)c.push({name:"sSearch_"+g,value:a.aoPreSearchCols[g].sSearch}),c.push({name:"bRegex_"+g,value:a.aoPreSearchCols[g].bRegex}),c.push({name:"bSearchable_"+g,value:a.aoColumns[g].bSearchable})}if(!1!==a.oFeatures.bSort){var e=
0;d=null!==a.aaSortingFixed?a.aaSortingFixed.concat(a.aaSorting):a.aaSorting.slice();for(g=0;g<d.length;g++){f=a.aoColumns[d[g][0]].aDataSort;for(i=0;i<f.length;i++)c.push({name:"iSortCol_"+e,value:f[i]}),c.push({name:"sSortDir_"+e,value:d[g][1]}),e++}c.push({name:"iSortingCols",value:e});for(g=0;g<b;g++)c.push({name:"bSortable_"+g,value:a.aoColumns[g].bSortable})}return c}function ia(a,b){D(a,"aoServerParams","serverParams",[b])}function Fa(a,b){if(b.sEcho!==m){if(1*b.sEcho<a.iDraw)return;a.iDraw=
1*b.sEcho}(!a.oScroll.bInfinite||a.oScroll.bInfinite&&(a.bSorted||a.bFiltered))&&ea(a);a._iRecordsTotal=parseInt(b.iTotalRecords,10);a._iRecordsDisplay=parseInt(b.iTotalDisplayRecords,10);var c=y(a),c=b.sColumns!==m&&""!==c&&b.sColumns!=c,d;c&&(d=E(a,b.sColumns));for(var f=W(a.sAjaxDataProp)(b),g=0,i=f.length;g<i;g++)if(c){for(var e=[],h=0,n=a.aoColumns.length;h<n;h++)e.push(f[g][d[h]]);H(a,e)}else H(a,f[g]);a.aiDisplay=a.aiDisplayMaster.slice();a.bAjaxDataGet=!1;z(a);a.bAjaxDataGet=!0;G(a,!1)}function za(a){var b=
a.oPreviousSearch,c=a.oLanguage.sSearch,c=-1!==c.indexOf("_INPUT_")?c.replace("_INPUT_",'<input type="text" />'):""===c?'<input type="text" />':c+' <input type="text" />',d=l.createElement("div");d.className=a.oClasses.sFilter;d.innerHTML="<label>"+c+"</label>";if(!a.aanFeatures.f)d.id=a.sTableId+"_filter";c=h('input[type="text"]',d);d._DT_Input=c[0];c.val(b.sSearch.replace('"',"&quot;"));c.bind("keyup.DT",function(){if(this.value.length <= 3 && this.value.length > 0)return false;for(var c=a.aanFeatures.f,d=""===this.value?"":this.value,i=0,e=c.length;i<e;i++)c[i]!=
h(this).parents("div.dataTables_filter")[0]&&h(c[i]._DT_Input).val(d);d!=b.sSearch&&M(a,{sSearch:d,bRegex:b.bRegex,bSmart:b.bSmart,bCaseInsensitive:b.bCaseInsensitive})});c.attr("aria-controls",a.sTableId).bind("keypress.DT",function(a){if(13==a.keyCode)return!1});return d}function M(a,b,c){var d=a.oPreviousSearch,f=a.aoPreSearchCols,g=function(a){d.sSearch=a.sSearch;d.bRegex=a.bRegex;d.bSmart=a.bSmart;d.bCaseInsensitive=a.bCaseInsensitive};if(a.oFeatures.bServerSide)g(b);else{Ga(a,b.sSearch,c,b.bRegex,
b.bSmart,b.bCaseInsensitive);g(b);for(b=0;b<a.aoPreSearchCols.length;b++)Ha(a,f[b].sSearch,b,f[b].bRegex,f[b].bSmart,f[b].bCaseInsensitive);Ia(a)}a.bFiltered=!0;h(a.oInstance).trigger("filter",a);a._iDisplayStart=0;B(a);z(a);ja(a,0)}function Ia(a){for(var b=j.ext.afnFiltering,c=0,d=b.length;c<d;c++)for(var f=0,g=0,i=a.aiDisplay.length;g<i;g++){var e=a.aiDisplay[g-f];b[c](a,X(a,e,"filter"),e)||(a.aiDisplay.splice(g-f,1),f++)}}function Ha(a,b,c,d,f,g){if(""!==b)for(var i=0,b=ka(b,d,f,g),d=a.aiDisplay.length-
1;0<=d;d--)f=la(w(a,a.aiDisplay[d],c,"filter"),a.aoColumns[c].sType),b.test(f)||(a.aiDisplay.splice(d,1),i++)}function Ga(a,b,c,d,f,g){d=ka(b,d,f,g);f=a.oPreviousSearch;c||(c=0);0!==j.ext.afnFiltering.length&&(c=1);if(0>=b.length)a.aiDisplay.splice(0,a.aiDisplay.length),a.aiDisplay=a.aiDisplayMaster.slice();else if(a.aiDisplay.length==a.aiDisplayMaster.length||f.sSearch.length>b.length||1==c||0!==b.indexOf(f.sSearch)){a.aiDisplay.splice(0,a.aiDisplay.length);ja(a,1);for(b=0;b<a.aiDisplayMaster.length;b++)d.test(a.asDataSearch[b])&&
a.aiDisplay.push(a.aiDisplayMaster[b])}else for(b=c=0;b<a.asDataSearch.length;b++)d.test(a.asDataSearch[b])||(a.aiDisplay.splice(b-c,1),c++)}function ja(a,b){if(!a.oFeatures.bServerSide){a.asDataSearch.splice(0,a.asDataSearch.length);for(var c=b&&1===b?a.aiDisplayMaster:a.aiDisplay,d=0,f=c.length;d<f;d++)a.asDataSearch[d]=ma(a,X(a,c[d],"filter"))}}function ma(a,b){var c="";if(a.__nTmpFilter===m)a.__nTmpFilter=l.createElement("div");for(var d=a.__nTmpFilter,f=0,g=a.aoColumns.length;f<g;f++)a.aoColumns[f].bSearchable&&
(c+=la(b[f],a.aoColumns[f].sType)+"  ");if(-1!==c.indexOf("&"))d.innerHTML=c,c=d.textContent?d.textContent:d.innerText,c=c.replace(/\n/g," ").replace(/\r/g,"");return c}function ka(a,b,c,d){if(c)return a=b?a.split(" "):na(a).split(" "),a="^(?=.*?"+a.join(")(?=.*?")+").*$",RegExp(a,d?"i":"");a=b?a:na(a);return RegExp(a,d?"i":"")}function la(a,b){return"function"===typeof j.ext.ofnSearch[b]?j.ext.ofnSearch[b](a):null===a?"":"html"==b?a.replace(/[\r\n]/g," ").replace(/<.*?>/g,""):"string"===typeof a?
a.replace(/[\r\n]/g," "):a}function na(a){return a.replace(RegExp("(\\/|\\.|\\*|\\+|\\?|\\||\\(|\\)|\\[|\\]|\\{|\\}|\\\\|\\$|\\^)","g"),"\\$1")}function Ca(a){var b=l.createElement("div");b.className=a.oClasses.sInfo;if(!a.aanFeatures.i)a.aoDrawCallback.push({fn:Ja,sName:"information"}),b.id=a.sTableId+"_info";a.nTable.setAttribute("aria-describedby",a.sTableId+"_info");return b}function Ja(a){if(a.oFeatures.bInfo&&0!==a.aanFeatures.i.length){var b=a.oLanguage,c=a._iDisplayStart+1,d=a.fnDisplayEnd(),
f=a.fnRecordsTotal(),g=a.fnRecordsDisplay(),i;i=0===g&&g==f?b.sInfoEmpty:0===g?b.sInfoEmpty+" "+b.sInfoFiltered:g==f?b.sInfo:b.sInfo+" "+b.sInfoFiltered;i+=b.sInfoPostFix;i=ha(a,i);null!==b.fnInfoCallback&&(i=b.fnInfoCallback.call(a.oInstance,a,c,d,f,g,i));a=a.aanFeatures.i;b=0;for(c=a.length;b<c;b++)h(a[b]).html(i)}}function ha(a,b){var c=a.fnFormatNumber(a._iDisplayStart+1),d=a.fnDisplayEnd(),d=a.fnFormatNumber(d),f=a.fnRecordsDisplay(),f=a.fnFormatNumber(f),g=a.fnRecordsTotal(),g=a.fnFormatNumber(g);
a.oScroll.bInfinite&&(c=a.fnFormatNumber(1));return b.replace("_START_",c).replace("_END_",d).replace("_TOTAL_",f).replace("_MAX_",g)}function aa(a){var b,c,d=a.iInitDisplayStart;if(!1===a.bInitialised)setTimeout(function(){aa(a)},200);else{xa(a);va(a);U(a,a.aoHeader);a.nTFoot&&U(a,a.aoFooter);G(a,!0);a.oFeatures.bAutoWidth&&ba(a);for(b=0,c=a.aoColumns.length;b<c;b++)if(null!==a.aoColumns[b].sWidth)a.aoColumns[b].nTh.style.width=q(a.aoColumns[b].sWidth);a.oFeatures.bSort?P(a):a.oFeatures.bFilter?
M(a,a.oPreviousSearch):(a.aiDisplay=a.aiDisplayMaster.slice(),B(a),z(a));null!==a.sAjaxSource&&!a.oFeatures.bServerSide?(c=[],ia(a,c),a.fnServerData.call(a.oInstance,a.sAjaxSource,c,function(c){var g=""!==a.sAjaxDataProp?W(a.sAjaxDataProp)(c):c;for(b=0;b<g.length;b++)H(a,g[b]);a.iInitDisplayStart=d;a.oFeatures.bSort?P(a):(a.aiDisplay=a.aiDisplayMaster.slice(),B(a),z(a));G(a,!1);Z(a,c)},a)):a.oFeatures.bServerSide||(G(a,!1),Z(a))}}function Z(a,b){a._bInitComplete=!0;D(a,"aoInitComplete","init",[a,
b])}function oa(a){var b=j.defaults.oLanguage;!a.sEmptyTable&&a.sZeroRecords&&"No data available in table"===b.sEmptyTable&&p(a,a,"sZeroRecords","sEmptyTable");!a.sLoadingRecords&&a.sZeroRecords&&"Loading..."===b.sLoadingRecords&&p(a,a,"sZeroRecords","sLoadingRecords")}function ya(a){if(a.oScroll.bInfinite)return null;var b='<select size="1" '+('name="'+a.sTableId+'_length"')+">",c,d,f=a.aLengthMenu;if(2==f.length&&"object"===typeof f[0]&&"object"===typeof f[1])for(c=0,d=f[0].length;c<d;c++)b+='<option value="'+
f[0][c]+'">'+f[1][c]+"</option>";else for(c=0,d=f.length;c<d;c++)b+='<option value="'+f[c]+'">'+f[c]+"</option>";b+="</select>";f=l.createElement("div");if(!a.aanFeatures.l)f.id=a.sTableId+"_length";f.className=a.oClasses.sLength;f.innerHTML="<label>"+a.oLanguage.sLengthMenu.replace("_MENU_",b)+"</label>";h('select option[value="'+a._iDisplayLength+'"]',f).attr("selected",!0);h("select",f).bind("change.DT",function(){var b=h(this).val(),f=a.aanFeatures.l;for(c=0,d=f.length;c<d;c++)f[c]!=this.parentNode&&
h("select",f[c]).val(b);a._iDisplayLength=parseInt(b,10);B(a);if(a.fnDisplayEnd()==a.fnRecordsDisplay()&&(a._iDisplayStart=a.fnDisplayEnd()-a._iDisplayLength,0>a._iDisplayStart))a._iDisplayStart=0;if(-1==a._iDisplayLength)a._iDisplayStart=0;z(a)});h("select",f).attr("aria-controls",a.sTableId);return f}function B(a){a._iDisplayEnd=!1===a.oFeatures.bPaginate?a.aiDisplay.length:a._iDisplayStart+a._iDisplayLength>a.aiDisplay.length||-1==a._iDisplayLength?a.aiDisplay.length:a._iDisplayStart+a._iDisplayLength}
function Da(a){if(a.oScroll.bInfinite)return null;var b=l.createElement("div");b.className=a.oClasses.sPaging+a.sPaginationType;j.ext.oPagination[a.sPaginationType].fnInit(a,b,function(a){B(a);z(a)});a.aanFeatures.p||a.aoDrawCallback.push({fn:function(a){j.ext.oPagination[a.sPaginationType].fnUpdate(a,function(a){B(a);z(a)})},sName:"pagination"});return b}function pa(a,b){var c=a._iDisplayStart;if("number"===typeof b){if(a._iDisplayStart=b*a._iDisplayLength,a._iDisplayStart>a.fnRecordsDisplay())a._iDisplayStart=
0}else if("first"==b)a._iDisplayStart=0;else if("previous"==b){if(a._iDisplayStart=0<=a._iDisplayLength?a._iDisplayStart-a._iDisplayLength:0,0>a._iDisplayStart)a._iDisplayStart=0}else if("next"==b)0<=a._iDisplayLength?a._iDisplayStart+a._iDisplayLength<a.fnRecordsDisplay()&&(a._iDisplayStart+=a._iDisplayLength):a._iDisplayStart=0;else if("last"==b)if(0<=a._iDisplayLength){var d=parseInt((a.fnRecordsDisplay()-1)/a._iDisplayLength,10)+1;a._iDisplayStart=(d-1)*a._iDisplayLength}else a._iDisplayStart=
0;else F(a,0,"Unknown paging action: "+b);h(a.oInstance).trigger("page",a);return c!=a._iDisplayStart}function Aa(a){var b=l.createElement("div");if(!a.aanFeatures.r)b.id=a.sTableId+"_processing";b.innerHTML=a.oLanguage.sProcessing;b.className=a.oClasses.sProcessing;a.nTable.parentNode.insertBefore(b,a.nTable);return b}function G(a,b){if(a.oFeatures.bProcessing)for(var c=a.aanFeatures.r,d=0,f=c.length;d<f;d++)c[d].style.visibility=b?"visible":"hidden";h(a.oInstance).trigger("processing",[a,b])}function Ba(a){if(""===
a.oScroll.sX&&""===a.oScroll.sY)return a.nTable;var b=l.createElement("div"),c=l.createElement("div"),d=l.createElement("div"),f=l.createElement("div"),g=l.createElement("div"),i=l.createElement("div"),e=a.nTable.cloneNode(!1),j=a.nTable.cloneNode(!1),n=a.nTable.getElementsByTagName("thead")[0],o=0===a.nTable.getElementsByTagName("tfoot").length?null:a.nTable.getElementsByTagName("tfoot")[0],k=a.oClasses;c.appendChild(d);g.appendChild(i);f.appendChild(a.nTable);b.appendChild(c);b.appendChild(f);d.appendChild(e);
e.appendChild(n);null!==o&&(b.appendChild(g),i.appendChild(j),j.appendChild(o));b.className=k.sScrollWrapper;c.className=k.sScrollHead;d.className=k.sScrollHeadInner;f.className=k.sScrollBody;g.className=k.sScrollFoot;i.className=k.sScrollFootInner;if(a.oScroll.bAutoCss)c.style.overflow="hidden",c.style.position="relative",g.style.overflow="hidden",f.style.overflow="auto";c.style.border="0";c.style.width="100%";g.style.border="0";d.style.width=""!==a.oScroll.sXInner?a.oScroll.sXInner:"100%";e.removeAttribute("id");
e.style.marginLeft="0";a.nTable.style.marginLeft="0";if(null!==o)j.removeAttribute("id"),j.style.marginLeft="0";d=h(a.nTable).children("caption");0<d.length&&(d=d[0],"top"===d._captionSide?e.appendChild(d):"bottom"===d._captionSide&&o&&j.appendChild(d));if(""!==a.oScroll.sX){c.style.width=q(a.oScroll.sX);f.style.width=q(a.oScroll.sX);if(null!==o)g.style.width=q(a.oScroll.sX);h(f).scroll(function(){c.scrollLeft=this.scrollLeft;if(null!==o)g.scrollLeft=this.scrollLeft})}if(""!==a.oScroll.sY)f.style.height=
q(a.oScroll.sY);a.aoDrawCallback.push({fn:Ka,sName:"scrolling"});a.oScroll.bInfinite&&h(f).scroll(function(){!a.bDrawing&&0!==h(this).scrollTop()&&h(this).scrollTop()+h(this).height()>h(a.nTable).height()-a.oScroll.iLoadGap&&a.fnDisplayEnd()<a.fnRecordsDisplay()&&(pa(a,"next"),B(a),z(a))});a.nScrollHead=c;a.nScrollFoot=g;return b}function Ka(a){var b=a.nScrollHead.getElementsByTagName("div")[0],c=b.getElementsByTagName("table")[0],d=a.nTable.parentNode,f,g,i,e,j,n,o,k,m=[],s=null!==a.nTFoot?a.nScrollFoot.getElementsByTagName("div")[0]:
null,p=null!==a.nTFoot?s.getElementsByTagName("table")[0]:null,l=h.browser.msie&&7>=h.browser.version;h(a.nTable).children("thead, tfoot").remove();i=h(a.nTHead).clone()[0];a.nTable.insertBefore(i,a.nTable.childNodes[0]);null!==a.nTFoot&&(j=h(a.nTFoot).clone()[0],a.nTable.insertBefore(j,a.nTable.childNodes[1]));if(""===a.oScroll.sX)d.style.width="100%",b.parentNode.style.width="100%";var r=O(a,i);for(f=0,g=r.length;f<g;f++)o=x(a,f),r[f].style.width=a.aoColumns[o].sWidth;null!==a.nTFoot&&N(function(a){a.style.width=
""},j.getElementsByTagName("tr"));if(a.oScroll.bCollapse&&""!==a.oScroll.sY)d.style.height=d.offsetHeight+a.nTHead.offsetHeight+"px";f=h(a.nTable).outerWidth();if(""===a.oScroll.sX){if(a.nTable.style.width="100%",l&&(h("tbody",d).height()>d.offsetHeight||"scroll"==h(d).css("overflow-y")))a.nTable.style.width=q(h(a.nTable).outerWidth()-a.oScroll.iBarWidth)}else if(""!==a.oScroll.sXInner)a.nTable.style.width=q(a.oScroll.sXInner);else if(f==h(d).width()&&h(d).height()<h(a.nTable).height()){if(a.nTable.style.width=
q(f-a.oScroll.iBarWidth),h(a.nTable).outerWidth()>f-a.oScroll.iBarWidth)a.nTable.style.width=q(f)}else a.nTable.style.width=q(f);f=h(a.nTable).outerWidth();g=a.nTHead.getElementsByTagName("tr");i=i.getElementsByTagName("tr");N(function(a,b){n=a.style;n.paddingTop="0";n.paddingBottom="0";n.borderTopWidth="0";n.borderBottomWidth="0";n.height=0;k=h(a).width();b.style.width=q(k);m.push(k)},i,g);h(i).height(0);null!==a.nTFoot&&(e=j.getElementsByTagName("tr"),j=a.nTFoot.getElementsByTagName("tr"),N(function(a,
b){n=a.style;n.paddingTop="0";n.paddingBottom="0";n.borderTopWidth="0";n.borderBottomWidth="0";n.height=0;k=h(a).width();b.style.width=q(k);m.push(k)},e,j),h(e).height(0));N(function(a){a.innerHTML="";a.style.width=q(m.shift())},i);null!==a.nTFoot&&N(function(a){a.innerHTML="";a.style.width=q(m.shift())},e);if(h(a.nTable).outerWidth()<f){e=d.scrollHeight>d.offsetHeight||"scroll"==h(d).css("overflow-y")?f+a.oScroll.iBarWidth:f;if(l&&(d.scrollHeight>d.offsetHeight||"scroll"==h(d).css("overflow-y")))a.nTable.style.width=
q(e-a.oScroll.iBarWidth);d.style.width=q(e);b.parentNode.style.width=q(e);if(null!==a.nTFoot)s.parentNode.style.width=q(e);""===a.oScroll.sX?F(a,1,"The table cannot fit into the current element which will cause column misalignment. The table has been drawn at its minimum possible width."):""!==a.oScroll.sXInner&&F(a,1,"The table cannot fit into the current element which will cause column misalignment. Increase the sScrollXInner value or remove it to allow automatic calculation")}else if(d.style.width=
q("100%"),b.parentNode.style.width=q("100%"),null!==a.nTFoot)s.parentNode.style.width=q("100%");if(""===a.oScroll.sY&&l)d.style.height=q(a.nTable.offsetHeight+a.oScroll.iBarWidth);if(""!==a.oScroll.sY&&a.oScroll.bCollapse&&(d.style.height=q(a.oScroll.sY),l=""!==a.oScroll.sX&&a.nTable.offsetWidth>d.offsetWidth?a.oScroll.iBarWidth:0,a.nTable.offsetHeight<d.offsetHeight))d.style.height=q(a.nTable.offsetHeight+l);l=h(a.nTable).outerWidth();c.style.width=q(l);b.style.width=q(l);c=h(a.nTable).height()>
d.clientHeight||"scroll"==h(d).css("overflow-y");b.style.paddingRight=c?a.oScroll.iBarWidth+"px":"0px";if(null!==a.nTFoot)p.style.width=q(l),s.style.width=q(l),s.style.paddingRight=c?a.oScroll.iBarWidth+"px":"0px";h(d).scroll();if(a.bSorted||a.bFiltered)d.scrollTop=0}function N(a,b,c){for(var d=0,f=b.length;d<f;d++)for(var g=0,i=b[d].childNodes.length;g<i;g++)1==b[d].childNodes[g].nodeType&&(c?a(b[d].childNodes[g],c[d].childNodes[g]):a(b[d].childNodes[g]))}function La(a,b){if(!a||null===a||""===a)return 0;
b||(b=l.getElementsByTagName("body")[0]);var c,d=l.createElement("div");d.style.width=q(a);b.appendChild(d);c=d.offsetWidth;b.removeChild(d);return c}function ba(a){var b=0,c,d=0,f=a.aoColumns.length,g,i=h("th",a.nTHead),e=a.nTable.getAttribute("width");for(g=0;g<f;g++)if(a.aoColumns[g].bVisible&&(d++,null!==a.aoColumns[g].sWidth)){c=La(a.aoColumns[g].sWidthOrig,a.nTable.parentNode);if(null!==c)a.aoColumns[g].sWidth=q(c);b++}if(f==i.length&&0===b&&d==f&&""===a.oScroll.sX&&""===a.oScroll.sY)for(g=
0;g<a.aoColumns.length;g++){if(c=h(i[g]).width(),null!==c)a.aoColumns[g].sWidth=q(c)}else{b=a.nTable.cloneNode(!1);g=a.nTHead.cloneNode(!0);d=l.createElement("tbody");c=l.createElement("tr");b.removeAttribute("id");b.appendChild(g);null!==a.nTFoot&&(b.appendChild(a.nTFoot.cloneNode(!0)),N(function(a){a.style.width=""},b.getElementsByTagName("tr")));b.appendChild(d);d.appendChild(c);d=h("thead th",b);0===d.length&&(d=h("tbody tr:eq(0)>td",b));i=O(a,g);for(g=d=0;g<f;g++){var j=a.aoColumns[g];j.bVisible&&
null!==j.sWidthOrig&&""!==j.sWidthOrig?i[g-d].style.width=q(j.sWidthOrig):j.bVisible?i[g-d].style.width="":d++}for(g=0;g<f;g++)a.aoColumns[g].bVisible&&(d=Ma(a,g),null!==d&&(d=d.cloneNode(!0),""!==a.aoColumns[g].sContentPadding&&(d.innerHTML+=a.aoColumns[g].sContentPadding),c.appendChild(d)));f=a.nTable.parentNode;f.appendChild(b);if(""!==a.oScroll.sX&&""!==a.oScroll.sXInner)b.style.width=q(a.oScroll.sXInner);else if(""!==a.oScroll.sX){if(b.style.width="",h(b).width()<f.offsetWidth)b.style.width=
q(f.offsetWidth)}else if(""!==a.oScroll.sY)b.style.width=q(f.offsetWidth);else if(e)b.style.width=q(e);b.style.visibility="hidden";Na(a,b);f=h("tbody tr:eq(0)",b).children();0===f.length&&(f=O(a,h("thead",b)[0]));if(""!==a.oScroll.sX){for(g=d=c=0;g<a.aoColumns.length;g++)a.aoColumns[g].bVisible&&(c=null===a.aoColumns[g].sWidthOrig?c+h(f[d]).outerWidth():c+(parseInt(a.aoColumns[g].sWidth.replace("px",""),10)+(h(f[d]).outerWidth()-h(f[d]).width())),d++);b.style.width=q(c);a.nTable.style.width=q(c)}for(g=
d=0;g<a.aoColumns.length;g++)if(a.aoColumns[g].bVisible){c=h(f[d]).width();if(null!==c&&0<c)a.aoColumns[g].sWidth=q(c);d++}f=h(b).css("width");a.nTable.style.width=-1!==f.indexOf("%")?f:q(h(b).outerWidth());b.parentNode.removeChild(b)}if(e)a.nTable.style.width=q(e)}function Na(a,b){if(""===a.oScroll.sX&&""!==a.oScroll.sY)h(b).width(),b.style.width=q(h(b).outerWidth()-a.oScroll.iBarWidth);else if(""!==a.oScroll.sX)b.style.width=q(h(b).outerWidth())}function Ma(a,b){var c=Oa(a,b);if(0>c)return null;
if(null===a.aoData[c].nTr){var d=l.createElement("td");d.innerHTML=w(a,c,b,"");return d}return L(a,c)[b]}function Oa(a,b){for(var c=-1,d=-1,f=0;f<a.aoData.length;f++){var g=w(a,f,b,"display")+"",g=g.replace(/<.*?>/g,"");if(g.length>c)c=g.length,d=f}return d}function q(a){if(null===a)return"0px";if("number"==typeof a)return 0>a?"0px":a+"px";var b=a.charCodeAt(a.length-1);return 48>b||57<b?a:a+"px"}function Pa(){var a=l.createElement("p"),b=a.style;b.width="100%";b.height="200px";b.padding="0px";var c=
l.createElement("div"),b=c.style;b.position="absolute";b.top="0px";b.left="0px";b.visibility="hidden";b.width="200px";b.height="150px";b.padding="0px";b.overflow="hidden";c.appendChild(a);l.body.appendChild(c);b=a.offsetWidth;c.style.overflow="scroll";a=a.offsetWidth;if(b==a)a=c.clientWidth;l.body.removeChild(c);return b-a}function P(a,b){var c,d,f,g,i,e,o=[],n=[],k=j.ext.oSort,s=a.aoData,l=a.aoColumns,p=a.oLanguage.oAria;if(!a.oFeatures.bServerSide&&(0!==a.aaSorting.length||null!==a.aaSortingFixed)){o=
null!==a.aaSortingFixed?a.aaSortingFixed.concat(a.aaSorting):a.aaSorting.slice();for(c=0;c<o.length;c++)if(d=o[c][0],f=r(a,d),g=a.aoColumns[d].sSortDataType,j.ext.afnSortData[g])if(i=j.ext.afnSortData[g].call(a.oInstance,a,d,f),i.length===s.length)for(f=0,g=s.length;f<g;f++)I(a,f,d,i[f]);else F(a,0,"Returned data sort array (col "+d+") is the wrong length");for(c=0,d=a.aiDisplayMaster.length;c<d;c++)n[a.aiDisplayMaster[c]]=c;var q=o.length,x;for(c=0,d=s.length;c<d;c++)for(f=0;f<q;f++){x=l[o[f][0]].aDataSort;
for(i=0,e=x.length;i<e;i++)g=l[x[i]].sType,g=k[(g?g:"string")+"-pre"],s[c]._aSortData[x[i]]=g?g(w(a,c,x[i],"sort")):w(a,c,x[i],"sort")}a.aiDisplayMaster.sort(function(a,b){var c,d,f,g,i;for(c=0;c<q;c++){i=l[o[c][0]].aDataSort;for(d=0,f=i.length;d<f;d++)if(g=l[i[d]].sType,g=k[(g?g:"string")+"-"+o[c][1]](s[a]._aSortData[i[d]],s[b]._aSortData[i[d]]),0!==g)return g}return k["numeric-asc"](n[a],n[b])})}(b===m||b)&&!a.oFeatures.bDeferRender&&Q(a);for(c=0,d=a.aoColumns.length;c<d;c++)g=l[c].sTitle.replace(/<.*?>/g,
""),f=l[c].nTh,f.removeAttribute("aria-sort"),f.removeAttribute("aria-label"),l[c].bSortable?0<o.length&&o[0][0]==c?(f.setAttribute("aria-sort","asc"==o[0][1]?"ascending":"descending"),f.setAttribute("aria-label",g+("asc"==(l[c].asSorting[o[0][2]+1]?l[c].asSorting[o[0][2]+1]:l[c].asSorting[0])?p.sSortAscending:p.sSortDescending))):f.setAttribute("aria-label",g+("asc"==l[c].asSorting[0]?p.sSortAscending:p.sSortDescending)):f.setAttribute("aria-label",g);a.bSorted=!0;h(a.oInstance).trigger("sort",a);
a.oFeatures.bFilter?M(a,a.oPreviousSearch,1):(a.aiDisplay=a.aiDisplayMaster.slice(),a._iDisplayStart=0,B(a),z(a))}function ga(a,b,c,d){Qa(b,{},function(b){if(!1!==a.aoColumns[c].bSortable){var g=function(){var d,g;if(b.shiftKey){for(var e=!1,h=0;h<a.aaSorting.length;h++)if(a.aaSorting[h][0]==c){e=!0;d=a.aaSorting[h][0];g=a.aaSorting[h][2]+1;a.aoColumns[d].asSorting[g]?(a.aaSorting[h][1]=a.aoColumns[d].asSorting[g],a.aaSorting[h][2]=g):a.aaSorting.splice(h,1);break}!1===e&&a.aaSorting.push([c,a.aoColumns[c].asSorting[0],
0])}else 1==a.aaSorting.length&&a.aaSorting[0][0]==c?(d=a.aaSorting[0][0],g=a.aaSorting[0][2]+1,a.aoColumns[d].asSorting[g]||(g=0),a.aaSorting[0][1]=a.aoColumns[d].asSorting[g],a.aaSorting[0][2]=g):(a.aaSorting.splice(0,a.aaSorting.length),a.aaSorting.push([c,a.aoColumns[c].asSorting[0],0]));P(a)};a.oFeatures.bProcessing?(G(a,!0),setTimeout(function(){g();a.oFeatures.bServerSide||G(a,!1)},0)):g();"function"==typeof d&&d(a)}})}function Q(a){var b,c,d,f,g,e=a.aoColumns.length,j=a.oClasses;for(b=0;b<
e;b++)a.aoColumns[b].bSortable&&h(a.aoColumns[b].nTh).removeClass(j.sSortAsc+" "+j.sSortDesc+" "+a.aoColumns[b].sSortingClass);f=null!==a.aaSortingFixed?a.aaSortingFixed.concat(a.aaSorting):a.aaSorting.slice();for(b=0;b<a.aoColumns.length;b++)if(a.aoColumns[b].bSortable){g=a.aoColumns[b].sSortingClass;d=-1;for(c=0;c<f.length;c++)if(f[c][0]==b){g="asc"==f[c][1]?j.sSortAsc:j.sSortDesc;d=c;break}h(a.aoColumns[b].nTh).addClass(g);a.bJUI&&(c=h("span."+j.sSortIcon,a.aoColumns[b].nTh),c.removeClass(j.sSortJUIAsc+
" "+j.sSortJUIDesc+" "+j.sSortJUI+" "+j.sSortJUIAscAllowed+" "+j.sSortJUIDescAllowed),c.addClass(-1==d?a.aoColumns[b].sSortingClassJUI:"asc"==f[d][1]?j.sSortJUIAsc:j.sSortJUIDesc))}else h(a.aoColumns[b].nTh).addClass(a.aoColumns[b].sSortingClass);g=j.sSortColumn;if(a.oFeatures.bSort&&a.oFeatures.bSortClasses){d=L(a);if(a.oFeatures.bDeferRender)h(d).removeClass(g+"1 "+g+"2 "+g+"3");else if(d.length>=e)for(b=0;b<e;b++)if(-1!=d[b].className.indexOf(g+"1"))for(c=0,a=d.length/e;c<a;c++)d[e*c+b].className=
h.trim(d[e*c+b].className.replace(g+"1",""));else if(-1!=d[b].className.indexOf(g+"2"))for(c=0,a=d.length/e;c<a;c++)d[e*c+b].className=h.trim(d[e*c+b].className.replace(g+"2",""));else if(-1!=d[b].className.indexOf(g+"3"))for(c=0,a=d.length/e;c<a;c++)d[e*c+b].className=h.trim(d[e*c+b].className.replace(" "+g+"3",""));var j=1,o;for(b=0;b<f.length;b++){o=parseInt(f[b][0],10);for(c=0,a=d.length/e;c<a;c++)d[e*c+o].className+=" "+g+j;3>j&&j++}}}function qa(a){if(a.oFeatures.bStateSave&&!a.bDestroying){var b,
c;b=a.oScroll.bInfinite;var d={iCreate:(new Date).getTime(),iStart:b?0:a._iDisplayStart,iEnd:b?a._iDisplayLength:a._iDisplayEnd,iLength:a._iDisplayLength,aaSorting:h.extend(!0,[],a.aaSorting),oSearch:h.extend(!0,{},a.oPreviousSearch),aoSearchCols:h.extend(!0,[],a.aoPreSearchCols),abVisCols:[]};for(b=0,c=a.aoColumns.length;b<c;b++)d.abVisCols.push(a.aoColumns[b].bVisible);D(a,"aoStateSaveParams","stateSaveParams",[a,d]);a.fnStateSave.call(a.oInstance,a,d)}}function Ra(a,b){if(a.oFeatures.bStateSave){var c=
a.fnStateLoad.call(a.oInstance,a);if(c){var d=D(a,"aoStateLoadParams","stateLoadParams",[a,c]);if(-1===h.inArray(!1,d)){a.oLoadedState=h.extend(!0,{},c);a._iDisplayStart=c.iStart;a.iInitDisplayStart=c.iStart;a._iDisplayEnd=c.iEnd;a._iDisplayLength=c.iLength;a.aaSorting=c.aaSorting.slice();a.saved_aaSorting=c.aaSorting.slice();h.extend(a.oPreviousSearch,c.oSearch);h.extend(!0,a.aoPreSearchCols,c.aoSearchCols);b.saved_aoColumns=[];for(d=0;d<c.abVisCols.length;d++)b.saved_aoColumns[d]={},b.saved_aoColumns[d].bVisible=
c.abVisCols[d];D(a,"aoStateLoaded","stateLoaded",[a,c])}}}}function Sa(a){for(var b=V.location.pathname.split("/"),a=a+"_"+b[b.length-1].replace(/[\/:]/g,"").toLowerCase()+"=",b=l.cookie.split(";"),c=0;c<b.length;c++){for(var d=b[c];" "==d.charAt(0);)d=d.substring(1,d.length);if(0===d.indexOf(a))return decodeURIComponent(d.substring(a.length,d.length))}return null}function t(a){for(var b=0;b<j.settings.length;b++)if(j.settings[b].nTable===a)return j.settings[b];return null}function S(a){for(var b=
[],a=a.aoData,c=0,d=a.length;c<d;c++)null!==a[c].nTr&&b.push(a[c].nTr);return b}function L(a,b){var c=[],d,f,g,e,h,j;f=0;var o=a.aoData.length;b!==m&&(f=b,o=b+1);for(g=f;g<o;g++)if(j=a.aoData[g],null!==j.nTr){f=[];for(e=0,h=j.nTr.childNodes.length;e<h;e++)d=j.nTr.childNodes[e].nodeName.toLowerCase(),("td"==d||"th"==d)&&f.push(j.nTr.childNodes[e]);d=0;for(e=0,h=a.aoColumns.length;e<h;e++)a.aoColumns[e].bVisible?c.push(f[e-d]):(c.push(j._anHidden[e]),d++)}return c}function F(a,b,c){a=null===a?"DataTables warning: "+
c:"DataTables warning (table id = '"+a.sTableId+"'): "+c;if(0===b)if("alert"==j.ext.sErrMode)alert(a);else throw Error(a);else V.console&&console.log&&console.log(a)}function p(a,b,c,d){d===m&&(d=c);b[c]!==m&&(a[d]=b[c])}function Ta(a,b){for(var c in b)b.hasOwnProperty(c)&&("object"===typeof e[c]&&!1===h.isArray(b[c])?h.extend(!0,a[c],b[c]):a[c]=b[c]);return a}function Qa(a,b,c){h(a).bind("click.DT",b,function(b){a.blur();c(b)}).bind("keypress.DT",b,function(a){13===a.which&&c(a)}).bind("selectstart.DT",
function(){return!1})}function C(a,b,c,d){c&&a[b].push({fn:c,sName:d})}function D(a,b,c,d){for(var b=a[b],f=[],g=b.length-1;0<=g;g--)f.push(b[g].fn.apply(a.oInstance,d));null!==c&&h(a.oInstance).trigger(c,d);return f}function Ua(a){return function(){var b=[t(this[j.ext.iApiIndex])].concat(Array.prototype.slice.call(arguments));return j.ext.oApi[a].apply(this,b)}}var Va=V.JSON?JSON.stringify:function(a){var b=typeof a;if("object"!==b||null===a)return"string"===b&&(a='"'+a+'"'),a+"";var c,d,f=[],g=
h.isArray(a);for(c in a)d=a[c],b=typeof d,"string"===b?d='"'+d+'"':"object"===b&&null!==d&&(d=Va(d)),f.push((g?"":'"'+c+'":')+d);return(g?"[":"{")+f+(g?"]":"}")};this.$=function(a,b){var c,d,f=[],g=t(this[j.ext.iApiIndex]);b||(b={});b=h.extend({},{filter:"none",order:"current",page:"all"},b);if("current"==b.page)for(c=g._iDisplayStart,d=g.fnDisplayEnd();c<d;c++)f.push(g.aoData[g.aiDisplay[c]].nTr);else if("current"==b.order&&"none"==b.filter)for(c=0,d=g.aiDisplayMaster.length;c<d;c++)f.push(g.aoData[g.aiDisplayMaster[c]].nTr);
else if("current"==b.order&&"applied"==b.filter)for(c=0,d=g.aiDisplay.length;c<d;c++)f.push(g.aoData[g.aiDisplay[c]].nTr);else if("original"==b.order&&"none"==b.filter)for(c=0,d=g.aoData.length;c<d;c++)f.push(g.aoData[c].nTr);else if("original"==b.order&&"applied"==b.filter)for(c=0,d=g.aoData.length;c<d;c++)-1!==h.inArray(c,g.aiDisplay)&&f.push(g.aoData[c].nTr);else F(g,1,"Unknown selection options");d=h(f);c=d.filter(a);d=d.find(a);return h([].concat(h.makeArray(c),h.makeArray(d)))};this._=function(a,
b){var c=[],d,f,g=this.$(a,b);for(d=0,f=g.length;d<f;d++)c.push(this.fnGetData(g[d]));return c};this.fnAddData=function(a,b){if(0===a.length)return[];var c=[],d,f=t(this[j.ext.iApiIndex]);if("object"===typeof a[0]&&null!==a[0])for(var g=0;g<a.length;g++){d=H(f,a[g]);if(-1==d)return c;c.push(d)}else{d=H(f,a);if(-1==d)return c;c.push(d)}f.aiDisplay=f.aiDisplayMaster.slice();(b===m||b)&&$(f);return c};this.fnAdjustColumnSizing=function(a){var b=t(this[j.ext.iApiIndex]);k(b);a===m||a?this.fnDraw(!1):
(""!==b.oScroll.sX||""!==b.oScroll.sY)&&this.oApi._fnScrollDraw(b)};this.fnClearTable=function(a){var b=t(this[j.ext.iApiIndex]);ea(b);(a===m||a)&&z(b)};this.fnClose=function(a){for(var b=t(this[j.ext.iApiIndex]),c=0;c<b.aoOpenRows.length;c++)if(b.aoOpenRows[c].nParent==a)return(a=b.aoOpenRows[c].nTr.parentNode)&&a.removeChild(b.aoOpenRows[c].nTr),b.aoOpenRows.splice(c,1),0;return 1};this.fnDeleteRow=function(a,b,c){var d=t(this[j.ext.iApiIndex]),f,g,a="object"===typeof a?K(d,a):a,e=d.aoData.splice(a,
1);for(f=0,g=d.aoData.length;f<g;f++)if(null!==d.aoData[f].nTr)d.aoData[f].nTr._DT_RowIndex=f;f=h.inArray(a,d.aiDisplay);d.asDataSearch.splice(f,1);fa(d.aiDisplayMaster,a);fa(d.aiDisplay,a);"function"===typeof b&&b.call(this,d,e);if(d._iDisplayStart>=d.aiDisplay.length&&(d._iDisplayStart-=d._iDisplayLength,0>d._iDisplayStart))d._iDisplayStart=0;if(c===m||c)B(d),z(d);return e};this.fnDestroy=function(a){var b=t(this[j.ext.iApiIndex]),c=b.nTableWrapper.parentNode,d=b.nTBody,f,g,a=a===m?!1:!0;b.bDestroying=
!0;D(b,"aoDestroyCallback","destroy",[b]);for(f=0,g=b.aoColumns.length;f<g;f++)!1===b.aoColumns[f].bVisible&&this.fnSetColumnVis(f,!0);h(b.nTableWrapper).find("*").andSelf().unbind(".DT");h("tbody>tr>td."+b.oClasses.sRowEmpty,b.nTable).parent().remove();b.nTable!=b.nTHead.parentNode&&(h(b.nTable).children("thead").remove(),b.nTable.appendChild(b.nTHead));b.nTFoot&&b.nTable!=b.nTFoot.parentNode&&(h(b.nTable).children("tfoot").remove(),b.nTable.appendChild(b.nTFoot));b.nTable.parentNode.removeChild(b.nTable);
h(b.nTableWrapper).remove();b.aaSorting=[];b.aaSortingFixed=[];Q(b);h(S(b)).removeClass(b.asStripeClasses.join(" "));h("th, td",b.nTHead).removeClass([b.oClasses.sSortable,b.oClasses.sSortableAsc,b.oClasses.sSortableDesc,b.oClasses.sSortableNone].join(" "));b.bJUI&&(h("th span."+b.oClasses.sSortIcon+", td span."+b.oClasses.sSortIcon,b.nTHead).remove(),h("th, td",b.nTHead).each(function(){var a=h("div."+b.oClasses.sSortJUIWrapper,this),c=a.contents();h(this).append(c);a.remove()}));!a&&b.nTableReinsertBefore?
c.insertBefore(b.nTable,b.nTableReinsertBefore):a||c.appendChild(b.nTable);for(f=0,g=b.aoData.length;f<g;f++)null!==b.aoData[f].nTr&&d.appendChild(b.aoData[f].nTr);if(!0===b.oFeatures.bAutoWidth)b.nTable.style.width=q(b.sDestroyWidth);h(d).children("tr:even").addClass(b.asDestroyStripes[0]);h(d).children("tr:odd").addClass(b.asDestroyStripes[1]);for(f=0,g=j.settings.length;f<g;f++)j.settings[f]==b&&j.settings.splice(f,1);b=null};this.fnDraw=function(a){var b=t(this[j.ext.iApiIndex]);!1===a?(B(b),
z(b)):$(b)};this.fnFilter=function(a,b,c,d,f,g){var e=t(this[j.ext.iApiIndex]);if(e.oFeatures.bFilter){if(c===m||null===c)c=!1;if(d===m||null===d)d=!0;if(f===m||null===f)f=!0;if(g===m||null===g)g=!0;if(b===m||null===b){if(M(e,{sSearch:a+"",bRegex:c,bSmart:d,bCaseInsensitive:g},1),f&&e.aanFeatures.f){b=e.aanFeatures.f;c=0;for(d=b.length;c<d;c++)h(b[c]._DT_Input).val(a)}}else h.extend(e.aoPreSearchCols[b],{sSearch:a+"",bRegex:c,bSmart:d,bCaseInsensitive:g}),M(e,e.oPreviousSearch,1)}};this.fnGetData=
function(a,b){var c=t(this[j.ext.iApiIndex]);if(a!==m){var d=a;if("object"===typeof a){var f=a.nodeName.toLowerCase();"tr"===f?d=K(c,a):"td"===f&&(d=K(c,a.parentNode),b=da(c,d,a))}return b!==m?w(c,d,b,""):c.aoData[d]!==m?c.aoData[d]._aData:null}return Y(c)};this.fnGetNodes=function(a){var b=t(this[j.ext.iApiIndex]);return a!==m?b.aoData[a]!==m?b.aoData[a].nTr:null:S(b)};this.fnGetPosition=function(a){var b=t(this[j.ext.iApiIndex]),c=a.nodeName.toUpperCase();if("TR"==c)return K(b,a);return"TD"==c||
"TH"==c?(c=K(b,a.parentNode),a=da(b,c,a),[c,r(b,a),a]):null};this.fnIsOpen=function(a){for(var b=t(this[j.ext.iApiIndex]),c=0;c<b.aoOpenRows.length;c++)if(b.aoOpenRows[c].nParent==a)return!0;return!1};this.fnOpen=function(a,b,c){var d=t(this[j.ext.iApiIndex]),f=S(d);if(-1!==h.inArray(a,f)){this.fnClose(a);var f=l.createElement("tr"),g=l.createElement("td");f.appendChild(g);g.className=c;g.colSpan=v(d);"string"===typeof b?g.innerHTML=b:h(g).html(b);b=h("tr",d.nTBody);-1!=h.inArray(a,b)&&h(f).insertAfter(a);
d.aoOpenRows.push({nTr:f,nParent:a});return f}};this.fnPageChange=function(a,b){var c=t(this[j.ext.iApiIndex]);pa(c,a);B(c);(b===m||b)&&z(c)};this.fnSetColumnVis=function(a,b,c){var d=t(this[j.ext.iApiIndex]),f,g,e=d.aoColumns,h=d.aoData,o,n;if(e[a].bVisible!=b){if(b){for(f=g=0;f<a;f++)e[f].bVisible&&g++;n=g>=v(d);if(!n)for(f=a;f<e.length;f++)if(e[f].bVisible){o=f;break}for(f=0,g=h.length;f<g;f++)null!==h[f].nTr&&(n?h[f].nTr.appendChild(h[f]._anHidden[a]):h[f].nTr.insertBefore(h[f]._anHidden[a],L(d,
f)[o]))}else for(f=0,g=h.length;f<g;f++)null!==h[f].nTr&&(o=L(d,f)[a],h[f]._anHidden[a]=o,o.parentNode.removeChild(o));e[a].bVisible=b;U(d,d.aoHeader);d.nTFoot&&U(d,d.aoFooter);for(f=0,g=d.aoOpenRows.length;f<g;f++)d.aoOpenRows[f].nTr.colSpan=v(d);if(c===m||c)k(d),z(d);qa(d)}};this.fnSettings=function(){return t(this[j.ext.iApiIndex])};this.fnSort=function(a){var b=t(this[j.ext.iApiIndex]);b.aaSorting=a;P(b)};this.fnSortListener=function(a,b,c){ga(t(this[j.ext.iApiIndex]),a,b,c)};this.fnUpdate=function(a,
b,c,d,f){var e=t(this[j.ext.iApiIndex]),b="object"===typeof b?K(e,b):b;if(e.__fnUpdateDeep===m&&h.isArray(a)&&"object"===typeof a){e.aoData[b]._aData=a.slice();e.__fnUpdateDeep=!0;for(c=0;c<e.aoColumns.length;c++)this.fnUpdate(w(e,b,c),b,c,!1,!1);e.__fnUpdateDeep=m}else if(e.__fnUpdateDeep===m&&null!==a&&"object"===typeof a){e.aoData[b]._aData=h.extend(!0,{},a);e.__fnUpdateDeep=!0;for(c=0;c<e.aoColumns.length;c++)this.fnUpdate(w(e,b,c),b,c,!1,!1);e.__fnUpdateDeep=m}else{I(e,b,c,a);var a=w(e,b,c,"display"),
i=e.aoColumns[c];null!==i.fnRender&&(a=R(e,b,c),i.bUseRendered&&I(e,b,c,a));if(null!==e.aoData[b].nTr)L(e,b)[c].innerHTML=a}c=h.inArray(b,e.aiDisplay);e.asDataSearch[c]=ma(e,X(e,b,"filter"));(f===m||f)&&k(e);(d===m||d)&&$(e);return 0};this.fnVersionCheck=j.ext.fnVersionCheck;this.oApi={_fnExternApiFunc:Ua,_fnInitialise:aa,_fnInitComplete:Z,_fnLanguageCompat:oa,_fnAddColumn:o,_fnColumnOptions:s,_fnAddData:H,_fnCreateTr:ca,_fnGatherData:ua,_fnBuildHead:va,_fnDrawHead:U,_fnDraw:z,_fnReDraw:$,_fnAjaxUpdate:wa,
_fnAjaxParameters:Ea,_fnAjaxUpdateDraw:Fa,_fnServerParams:ia,_fnAddOptionsHtml:xa,_fnFeatureHtmlTable:Ba,_fnScrollDraw:Ka,_fnAdjustColumnSizing:k,_fnFeatureHtmlFilter:za,_fnFilterComplete:M,_fnFilterCustom:Ia,_fnFilterColumn:Ha,_fnFilter:Ga,_fnBuildSearchArray:ja,_fnBuildSearchRow:ma,_fnFilterCreateSearch:ka,_fnDataToSearch:la,_fnSort:P,_fnSortAttachListener:ga,_fnSortingClasses:Q,_fnFeatureHtmlPaginate:Da,_fnPageChange:pa,_fnFeatureHtmlInfo:Ca,_fnUpdateInfo:Ja,_fnFeatureHtmlLength:ya,_fnFeatureHtmlProcessing:Aa,
_fnProcessingDisplay:G,_fnVisibleToColumnIndex:x,_fnColumnIndexToVisible:r,_fnNodeToDataIndex:K,_fnVisbleColumns:v,_fnCalculateEnd:B,_fnConvertToWidth:La,_fnCalculateColumnWidths:ba,_fnScrollingWidthAdjust:Na,_fnGetWidestNode:Ma,_fnGetMaxLenString:Oa,_fnStringToCss:q,_fnDetectType:A,_fnSettingsFromNode:t,_fnGetDataMaster:Y,_fnGetTrNodes:S,_fnGetTdNodes:L,_fnEscapeRegex:na,_fnDeleteIndex:fa,_fnReOrderIndex:E,_fnColumnOrdering:y,_fnLog:F,_fnClearTable:ea,_fnSaveState:qa,_fnLoadState:Ra,_fnCreateCookie:function(a,
b,c,d,e){var g=new Date;g.setTime(g.getTime()+1E3*c);var c=V.location.pathname.split("/"),a=a+"_"+c.pop().replace(/[\/:]/g,"").toLowerCase(),i;null!==e?(i="function"===typeof h.parseJSON?h.parseJSON(b):eval("("+b+")"),b=e(a,i,g.toGMTString(),c.join("/")+"/")):b=a+"="+encodeURIComponent(b)+"; expires="+g.toGMTString()+"; path="+c.join("/")+"/";e="";g=9999999999999;if(4096<(null!==Sa(a)?l.cookie.length:b.length+l.cookie.length)+10){for(var a=l.cookie.split(";"),j=0,o=a.length;j<o;j++)if(-1!=a[j].indexOf(d)){var k=
a[j].split("=");try{i=eval("("+decodeURIComponent(k[1])+")")}catch(m){continue}if(i.iCreate&&i.iCreate<g)e=k[0],g=i.iCreate}if(""!==e)l.cookie=e+"=; expires=Thu, 01-Jan-1970 00:00:01 GMT; path="+c.join("/")+"/"}l.cookie=b},_fnReadCookie:Sa,_fnDetectHeader:T,_fnGetUniqueThs:O,_fnScrollBarWidth:Pa,_fnApplyToChildren:N,_fnMap:p,_fnGetRowData:X,_fnGetCellData:w,_fnSetCellData:I,_fnGetObjectDataFn:W,_fnSetObjectDataFn:ta,_fnApplyColumnDefs:J,_fnBindAction:Qa,_fnExtend:Ta,_fnCallbackReg:C,_fnCallbackFire:D,
_fnJsonString:Va,_fnRender:R,_fnNodeToColumnIndex:da,_fnInfoMacros:ha};h.extend(j.ext.oApi,this.oApi);for(var ra in j.ext.oApi)ra&&(this[ra]=Ua(ra));var sa=this;return this.each(function(){var a=0,b,c,d;c=this.getAttribute("id");var f=!1,g=!1;if("table"!=this.nodeName.toLowerCase())F(null,0,"Attempted to initialise DataTables on a node which is not a table: "+this.nodeName);else{for(a=0,b=j.settings.length;a<b;a++){if(j.settings[a].nTable==this){if(e===m||e.bRetrieve)return j.settings[a].oInstance;
if(e.bDestroy){j.settings[a].oInstance.fnDestroy();break}else{F(j.settings[a],0,"Cannot reinitialise DataTable.\n\nTo retrieve the DataTables object for this table, pass no arguments or see the docs for bRetrieve and bDestroy");return}}if(j.settings[a].sTableId==this.id){j.settings.splice(a,1);break}}if(null===c||""===c)this.id=c="DataTables_Table_"+j.ext._oExternConfig.iNextUnique++;var i=h.extend(!0,{},j.models.oSettings,{nTable:this,oApi:sa.oApi,oInit:e,sDestroyWidth:h(this).width(),sInstance:c,
sTableId:c});j.settings.push(i);i.oInstance=1===sa.length?sa:h(this).dataTable();e||(e={});e.oLanguage&&oa(e.oLanguage);e=Ta(h.extend(!0,{},j.defaults),e);p(i.oFeatures,e,"bPaginate");p(i.oFeatures,e,"bLengthChange");p(i.oFeatures,e,"bFilter");p(i.oFeatures,e,"bSort");p(i.oFeatures,e,"bInfo");p(i.oFeatures,e,"bProcessing");p(i.oFeatures,e,"bAutoWidth");p(i.oFeatures,e,"bSortClasses");p(i.oFeatures,e,"bServerSide");p(i.oFeatures,e,"bDeferRender");p(i.oScroll,e,"sScrollX","sX");p(i.oScroll,e,"sScrollXInner",
"sXInner");p(i.oScroll,e,"sScrollY","sY");p(i.oScroll,e,"bScrollCollapse","bCollapse");p(i.oScroll,e,"bScrollInfinite","bInfinite");p(i.oScroll,e,"iScrollLoadGap","iLoadGap");p(i.oScroll,e,"bScrollAutoCss","bAutoCss");p(i,e,"asStripeClasses");p(i,e,"asStripClasses","asStripeClasses");p(i,e,"fnServerData");p(i,e,"fnFormatNumber");p(i,e,"sServerMethod");p(i,e,"aaSorting");p(i,e,"aaSortingFixed");p(i,e,"aLengthMenu");p(i,e,"sPaginationType");p(i,e,"sAjaxSource");p(i,e,"sAjaxDataProp");p(i,e,"iCookieDuration");
p(i,e,"sCookiePrefix");p(i,e,"sDom");p(i,e,"bSortCellsTop");p(i,e,"iTabIndex");p(i,e,"oSearch","oPreviousSearch");p(i,e,"aoSearchCols","aoPreSearchCols");p(i,e,"iDisplayLength","_iDisplayLength");p(i,e,"bJQueryUI","bJUI");p(i,e,"fnCookieCallback");p(i,e,"fnStateLoad");p(i,e,"fnStateSave");p(i.oLanguage,e,"fnInfoCallback");C(i,"aoDrawCallback",e.fnDrawCallback,"user");C(i,"aoServerParams",e.fnServerParams,"user");C(i,"aoStateSaveParams",e.fnStateSaveParams,"user");C(i,"aoStateLoadParams",e.fnStateLoadParams,
"user");C(i,"aoStateLoaded",e.fnStateLoaded,"user");C(i,"aoRowCallback",e.fnRowCallback,"user");C(i,"aoRowCreatedCallback",e.fnCreatedRow,"user");C(i,"aoHeaderCallback",e.fnHeaderCallback,"user");C(i,"aoFooterCallback",e.fnFooterCallback,"user");C(i,"aoInitComplete",e.fnInitComplete,"user");C(i,"aoPreDrawCallback",e.fnPreDrawCallback,"user");i.oFeatures.bServerSide&&i.oFeatures.bSort&&i.oFeatures.bSortClasses?C(i,"aoDrawCallback",Q,"server_side_sort_classes"):i.oFeatures.bDeferRender&&C(i,"aoDrawCallback",
Q,"defer_sort_classes");if(e.bJQueryUI){if(h.extend(i.oClasses,j.ext.oJUIClasses),e.sDom===j.defaults.sDom&&"lfrtip"===j.defaults.sDom)i.sDom='<"H"lfr>t<"F"ip>'}else h.extend(i.oClasses,j.ext.oStdClasses);h(this).addClass(i.oClasses.sTable);if(""!==i.oScroll.sX||""!==i.oScroll.sY)i.oScroll.iBarWidth=Pa();if(i.iInitDisplayStart===m)i.iInitDisplayStart=e.iDisplayStart,i._iDisplayStart=e.iDisplayStart;if(e.bStateSave)i.oFeatures.bStateSave=!0,Ra(i,e),C(i,"aoDrawCallback",qa,"state_save");if(null!==e.iDeferLoading)i.bDeferLoading=
!0,a=h.isArray(e.iDeferLoading),i._iRecordsDisplay=a?e.iDeferLoading[0]:e.iDeferLoading,i._iRecordsTotal=a?e.iDeferLoading[1]:e.iDeferLoading;null!==e.aaData&&(g=!0);""!==e.oLanguage.sUrl?(i.oLanguage.sUrl=e.oLanguage.sUrl,h.getJSON(i.oLanguage.sUrl,null,function(a){oa(a);h.extend(!0,i.oLanguage,e.oLanguage,a);aa(i)}),f=!0):h.extend(!0,i.oLanguage,e.oLanguage);if(null===e.asStripeClasses)i.asStripeClasses=[i.oClasses.sStripeOdd,i.oClasses.sStripeEven];c=!1;d=h(this).children("tbody").children("tr");
for(a=0,b=i.asStripeClasses.length;a<b;a++)if(d.filter(":lt(2)").hasClass(i.asStripeClasses[a])){c=!0;break}if(c)i.asDestroyStripes=["",""],h(d[0]).hasClass(i.oClasses.sStripeOdd)&&(i.asDestroyStripes[0]+=i.oClasses.sStripeOdd+" "),h(d[0]).hasClass(i.oClasses.sStripeEven)&&(i.asDestroyStripes[0]+=i.oClasses.sStripeEven),h(d[1]).hasClass(i.oClasses.sStripeOdd)&&(i.asDestroyStripes[1]+=i.oClasses.sStripeOdd+" "),h(d[1]).hasClass(i.oClasses.sStripeEven)&&(i.asDestroyStripes[1]+=i.oClasses.sStripeEven),
d.removeClass(i.asStripeClasses.join(" "));c=[];a=this.getElementsByTagName("thead");0!==a.length&&(T(i.aoHeader,a[0]),c=O(i));if(null===e.aoColumns){d=[];for(a=0,b=c.length;a<b;a++)d.push(null)}else d=e.aoColumns;for(a=0,b=d.length;a<b;a++){if(e.saved_aoColumns!==m&&e.saved_aoColumns.length==b)null===d[a]&&(d[a]={}),d[a].bVisible=e.saved_aoColumns[a].bVisible;o(i,c?c[a]:null)}J(i,e.aoColumnDefs,d,function(a,b){s(i,a,b)});for(a=0,b=i.aaSorting.length;a<b;a++){i.aaSorting[a][0]>=i.aoColumns.length&&
(i.aaSorting[a][0]=0);var k=i.aoColumns[i.aaSorting[a][0]];i.aaSorting[a][2]===m&&(i.aaSorting[a][2]=0);e.aaSorting===m&&i.saved_aaSorting===m&&(i.aaSorting[a][1]=k.asSorting[0]);for(c=0,d=k.asSorting.length;c<d;c++)if(i.aaSorting[a][1]==k.asSorting[c]){i.aaSorting[a][2]=c;break}}Q(i);a=h(this).children("caption").each(function(){this._captionSide=h(this).css("caption-side")});b=h(this).children("thead");0===b.length&&(b=[l.createElement("thead")],this.appendChild(b[0]));i.nTHead=b[0];b=h(this).children("tbody");
0===b.length&&(b=[l.createElement("tbody")],this.appendChild(b[0]));i.nTBody=b[0];i.nTBody.setAttribute("role","alert");i.nTBody.setAttribute("aria-live","polite");i.nTBody.setAttribute("aria-relevant","all");b=h(this).children("tfoot");if(0===b.length&&0<a.length&&(""!==i.oScroll.sX||""!==i.oScroll.sY))b=[l.createElement("tfoot")],this.appendChild(b[0]);if(0<b.length)i.nTFoot=b[0],T(i.aoFooter,i.nTFoot);if(g)for(a=0;a<e.aaData.length;a++)H(i,e.aaData[a]);else ua(i);i.aiDisplay=i.aiDisplayMaster.slice();
i.bInitialised=!0;!1===f&&aa(i)}})};j.fnVersionCheck=function(e){for(var h=function(e,h){for(;e.length<h;)e+="0";return e},m=j.ext.sVersion.split("."),e=e.split("."),k="",l="",r=0,v=e.length;r<v;r++)k+=h(m[r],3),l+=h(e[r],3);return parseInt(k,10)>=parseInt(l,10)};j.fnIsDataTable=function(e){for(var h=j.settings,m=0;m<h.length;m++)if(h[m].nTable===e||h[m].nScrollHead===e||h[m].nScrollFoot===e)return!0;return!1};j.fnTables=function(e){var o=[];jQuery.each(j.settings,function(j,k){(!e||!0===e&&h(k.nTable).is(":visible"))&&
o.push(k.nTable)});return o};j.version="1.9.1";j.settings=[];j.models={};j.models.ext={afnFiltering:[],afnSortData:[],aoFeatures:[],aTypes:[],fnVersionCheck:j.fnVersionCheck,iApiIndex:0,ofnSearch:{},oApi:{},oStdClasses:{},oJUIClasses:{},oPagination:{},oSort:{},sVersion:j.version,sErrMode:"alert",_oExternConfig:{iNextUnique:0}};j.models.oSearch={bCaseInsensitive:!0,sSearch:"",bRegex:!1,bSmart:!0};j.models.oRow={nTr:null,_aData:[],_aSortData:[],_anHidden:[],_sRowStripe:""};j.models.oColumn={aDataSort:null,
asSorting:null,bSearchable:null,bSortable:null,bUseRendered:null,bVisible:null,_bAutoType:!0,fnCreatedCell:null,fnGetData:null,fnRender:null,fnSetData:null,mDataProp:null,nTh:null,nTf:null,sClass:null,sContentPadding:null,sDefaultContent:null,sName:null,sSortDataType:"std",sSortingClass:null,sSortingClassJUI:null,sTitle:null,sType:null,sWidth:null,sWidthOrig:null};j.defaults={aaData:null,aaSorting:[[0,"asc"]],aaSortingFixed:null,aLengthMenu:[10,25,50,100],aoColumns:null,aoColumnDefs:null,aoSearchCols:[],
asStripeClasses:null,bAutoWidth:!0,bDeferRender:!1,bDestroy:!1,bFilter:!0,bInfo:!0,bJQueryUI:!1,bLengthChange:!0,bPaginate:!0,bProcessing:!1,bRetrieve:!1,bScrollAutoCss:!0,bScrollCollapse:!1,bScrollInfinite:!1,bServerSide:!1,bSort:!0,bSortCellsTop:!1,bSortClasses:!0,bStateSave:!1,fnCookieCallback:null,fnCreatedRow:null,fnDrawCallback:null,fnFooterCallback:null,fnFormatNumber:function(e){if(1E3>e)return e;for(var h=e+"",e=h.split(""),j="",h=h.length,k=0;k<h;k++)0===k%3&&0!==k&&(j=this.oLanguage.sInfoThousands+
j),j=e[h-k-1]+j;return j},fnHeaderCallback:null,fnInfoCallback:null,fnInitComplete:null,fnPreDrawCallback:null,fnRowCallback:null,fnServerData:function(e,j,m,k){k.jqXHR=h.ajax({url:e,data:j,success:function(e){h(k.oInstance).trigger("xhr",k);m(e)},dataType:"json",cache:!1,type:k.sServerMethod,error:function(e,h){"parsererror"==h&&k.oApi._fnLog(k,0,"DataTables warning: JSON data from server could not be parsed. This is caused by a JSON formatting error.")}})},fnServerParams:null,fnStateLoad:function(e){var e=
this.oApi._fnReadCookie(e.sCookiePrefix+e.sInstance),j;try{j="function"===typeof h.parseJSON?h.parseJSON(e):eval("("+e+")")}catch(m){j=null}return j},fnStateLoadParams:null,fnStateLoaded:null,fnStateSave:function(e,h){this.oApi._fnCreateCookie(e.sCookiePrefix+e.sInstance,this.oApi._fnJsonString(h),e.iCookieDuration,e.sCookiePrefix,e.fnCookieCallback)},fnStateSaveParams:null,iCookieDuration:7200,iDeferLoading:null,iDisplayLength:10,iDisplayStart:0,iScrollLoadGap:100,iTabIndex:0,oLanguage:{oAria:{sSortAscending:": activate to sort column ascending",
sSortDescending:": activate to sort column descending"},oPaginate:{sFirst:"First",sLast:"Last",sNext:"Next",sPrevious:"Previous"},sEmptyTable:"No data available in table",sInfo:"Showing _START_ to _END_ of _TOTAL_ entries",sInfoEmpty:"Showing 0 to 0 of 0 entries",sInfoFiltered:"(filtered from _MAX_ total entries)",sInfoPostFix:"",sInfoThousands:",",sLengthMenu:"Show _MENU_ entries",sLoadingRecords:"Loading...",sProcessing:" <img src='../images/ajaxloader.gif' />",sSearch:"Search:",sUrl:"",sZeroRecords:"No matching records found"},
oSearch:h.extend({},j.models.oSearch),sAjaxDataProp:"aaData",sAjaxSource:null,sCookiePrefix:"SpryMedia_DataTables_",sDom:"lfrtip",sPaginationType:"two_button",sScrollX:"",sScrollXInner:"",sScrollY:"",sServerMethod:"GET"};j.defaults.columns={aDataSort:null,asSorting:["asc","desc"],bSearchable:!0,bSortable:!0,bUseRendered:!0,bVisible:!0,fnCreatedCell:null,fnRender:null,iDataSort:-1,mDataProp:null,sCellType:"td",sClass:"",sContentPadding:"",sDefaultContent:null,sName:"",sSortDataType:"std",sTitle:null,
sType:null,sWidth:null};j.models.oSettings={oFeatures:{bAutoWidth:null,bDeferRender:null,bFilter:null,bInfo:null,bLengthChange:null,bPaginate:null,bProcessing:null,bServerSide:null,bSort:null,bSortClasses:null,bStateSave:null},oScroll:{bAutoCss:null,bCollapse:null,bInfinite:null,iBarWidth:0,iLoadGap:null,sX:null,sXInner:null,sY:null},oLanguage:{fnInfoCallback:null},aanFeatures:[],aoData:[],aiDisplay:[],aiDisplayMaster:[],aoColumns:[],aoHeader:[],aoFooter:[],asDataSearch:[],oPreviousSearch:{},aoPreSearchCols:[],
aaSorting:null,aaSortingFixed:null,asStripeClasses:null,asDestroyStripes:[],sDestroyWidth:0,aoRowCallback:[],aoHeaderCallback:[],aoFooterCallback:[],aoDrawCallback:[],aoRowCreatedCallback:[],aoPreDrawCallback:[],aoInitComplete:[],aoStateSaveParams:[],aoStateLoadParams:[],aoStateLoaded:[],sTableId:"",nTable:null,nTHead:null,nTFoot:null,nTBody:null,nTableWrapper:null,bDeferLoading:!1,bInitialised:!1,aoOpenRows:[],sDom:null,sPaginationType:"two_button",iCookieDuration:0,sCookiePrefix:"",fnCookieCallback:null,
aoStateSave:[],aoStateLoad:[],oLoadedState:null,sAjaxSource:null,sAjaxDataProp:null,bAjaxDataGet:!0,jqXHR:null,fnServerData:null,aoServerParams:[],sServerMethod:null,fnFormatNumber:null,aLengthMenu:null,iDraw:0,bDrawing:!1,iDrawError:-1,_iDisplayLength:10,_iDisplayStart:0,_iDisplayEnd:10,_iRecordsTotal:0,_iRecordsDisplay:0,bJUI:null,oClasses:{},bFiltered:!1,bSorted:!1,bSortCellsTop:null,oInit:null,aoDestroyCallback:[],fnRecordsTotal:function(){return this.oFeatures.bServerSide?parseInt(this._iRecordsTotal,
10):this.aiDisplayMaster.length},fnRecordsDisplay:function(){return this.oFeatures.bServerSide?parseInt(this._iRecordsDisplay,10):this.aiDisplay.length},fnDisplayEnd:function(){return this.oFeatures.bServerSide?!1===this.oFeatures.bPaginate||-1==this._iDisplayLength?this._iDisplayStart+this.aiDisplay.length:Math.min(this._iDisplayStart+this._iDisplayLength,this._iRecordsDisplay):this._iDisplayEnd},oInstance:null,sInstance:null,iTabIndex:0,nScrollHead:null,nScrollFoot:null};j.ext=h.extend(!0,{},j.models.ext);
h.extend(j.ext.oStdClasses,{sTable:"dataTable",sPagePrevEnabled:"paginate_enabled_previous",sPagePrevDisabled:"paginate_disabled_previous",sPageNextEnabled:"paginate_enabled_next",sPageNextDisabled:"paginate_disabled_next",sPageJUINext:"",sPageJUIPrev:"",sPageButton:"paginate_button",sPageButtonActive:"paginate_active",sPageButtonStaticDisabled:"paginate_button paginate_button_disabled",sPageFirst:"first",sPagePrevious:"previous",sPageNext:"next",sPageLast:"last",sStripeOdd:"odd",sStripeEven:"even",
sRowEmpty:"dataTables_empty",sWrapper:"dataTables_wrapper",sFilter:"dataTables_filter",sInfo:"dataTables_info",sPaging:"dataTables_paginate paging_",sLength:"dataTables_length",sProcessing:"dataTables_processing",sSortAsc:"sorting_asc",sSortDesc:"sorting_desc",sSortable:"sorting",sSortableAsc:"sorting_asc_disabled",sSortableDesc:"sorting_desc_disabled",sSortableNone:"sorting_disabled",sSortColumn:"sorting_",sSortJUIAsc:"",sSortJUIDesc:"",sSortJUI:"",sSortJUIAscAllowed:"",sSortJUIDescAllowed:"",sSortJUIWrapper:"",
sSortIcon:"",sScrollWrapper:"dataTables_scroll",sScrollHead:"dataTables_scrollHead",sScrollHeadInner:"dataTables_scrollHeadInner",sScrollBody:"dataTables_scrollBody",sScrollFoot:"dataTables_scrollFoot",sScrollFootInner:"dataTables_scrollFootInner",sFooterTH:""});h.extend(j.ext.oJUIClasses,j.ext.oStdClasses,{sPagePrevEnabled:"fg-button ui-button ui-state-default ui-corner-left",sPagePrevDisabled:"fg-button ui-button ui-state-default ui-corner-left ui-state-disabled",sPageNextEnabled:"fg-button ui-button ui-state-default ui-corner-right",
sPageNextDisabled:"fg-button ui-button ui-state-default ui-corner-right ui-state-disabled",sPageJUINext:"ui-icon ui-icon-circle-arrow-e",sPageJUIPrev:"ui-icon ui-icon-circle-arrow-w",sPageButton:"fg-button ui-button ui-state-default",sPageButtonActive:"fg-button ui-button ui-state-default ui-state-disabled",sPageButtonStaticDisabled:"fg-button ui-button ui-state-default ui-state-disabled",sPageFirst:"first ui-corner-tl ui-corner-bl",sPageLast:"last ui-corner-tr ui-corner-br",sPaging:"dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_",
sSortAsc:"ui-state-default",sSortDesc:"ui-state-default",sSortable:"ui-state-default",sSortableAsc:"ui-state-default",sSortableDesc:"ui-state-default",sSortableNone:"ui-state-default",sSortJUIAsc:"css_right ui-icon ui-icon-triangle-1-n",sSortJUIDesc:"css_right ui-icon ui-icon-triangle-1-s",sSortJUI:"css_right ui-icon ui-icon-carat-2-n-s",sSortJUIAscAllowed:"css_right ui-icon ui-icon-carat-1-n",sSortJUIDescAllowed:"css_right ui-icon ui-icon-carat-1-s",sSortJUIWrapper:"DataTables_sort_wrapper",sSortIcon:"DataTables_sort_icon",
sScrollHead:"dataTables_scrollHead ui-state-default",sScrollFoot:"dataTables_scrollFoot ui-state-default",sFooterTH:"ui-state-default"});h.extend(j.ext.oPagination,{two_button:{fnInit:function(e,j,m){var k=e.oLanguage.oPaginate,l=function(h){e.oApi._fnPageChange(e,h.data.action)&&m(e)},k=!e.bJUI?'<a class="'+e.oClasses.sPagePrevDisabled+'" tabindex="'+e.iTabIndex+'" role="button">'+k.sPrevious+'</a><a class="'+e.oClasses.sPageNextDisabled+'" tabindex="'+e.iTabIndex+'" role="button">'+k.sNext+"</a>":
'<a class="'+e.oClasses.sPagePrevDisabled+'" tabindex="'+e.iTabIndex+'" role="button"><span class="'+e.oClasses.sPageJUIPrev+'"></span></a><a class="'+e.oClasses.sPageNextDisabled+'" tabindex="'+e.iTabIndex+'" role="button"><span class="'+e.oClasses.sPageJUINext+'"></span></a>';h(j).append(k);var r=h("a",j),k=r[0],r=r[1];e.oApi._fnBindAction(k,{action:"previous"},l);e.oApi._fnBindAction(r,{action:"next"},l);if(!e.aanFeatures.p)j.id=e.sTableId+"_paginate",k.id=e.sTableId+"_previous",r.id=e.sTableId+
"_next",k.setAttribute("aria-controls",e.sTableId),r.setAttribute("aria-controls",e.sTableId)},fnUpdate:function(e){if(e.aanFeatures.p)for(var h=e.oClasses,j=e.aanFeatures.p,k=0,m=j.length;k<m;k++)if(0!==j[k].childNodes.length)j[k].childNodes[0].className=0===e._iDisplayStart?h.sPagePrevDisabled:h.sPagePrevEnabled,j[k].childNodes[1].className=e.fnDisplayEnd()==e.fnRecordsDisplay()?h.sPageNextDisabled:h.sPageNextEnabled}},iFullNumbersShowPages:5,full_numbers:{fnInit:function(e,j,m){var k=e.oLanguage.oPaginate,
l=e.oClasses,r=function(h){e.oApi._fnPageChange(e,h.data.action)&&m(e)};h(j).append('<a  tabindex="'+e.iTabIndex+'" class="'+l.sPageButton+" "+l.sPageFirst+'">'+k.sFirst+'</a><a  tabindex="'+e.iTabIndex+'" class="'+l.sPageButton+" "+l.sPagePrevious+'">'+k.sPrevious+'</a><span></span><a tabindex="'+e.iTabIndex+'" class="'+l.sPageButton+" "+l.sPageNext+'">'+k.sNext+'</a><a tabindex="'+e.iTabIndex+'" class="'+l.sPageButton+" "+l.sPageLast+'">'+k.sLast+"</a>");var v=h("a",j),k=v[0],l=v[1],A=v[2],v=v[3];
e.oApi._fnBindAction(k,{action:"first"},r);e.oApi._fnBindAction(l,{action:"previous"},r);e.oApi._fnBindAction(A,{action:"next"},r);e.oApi._fnBindAction(v,{action:"last"},r);if(!e.aanFeatures.p)j.id=e.sTableId+"_paginate",k.id=e.sTableId+"_first",l.id=e.sTableId+"_previous",A.id=e.sTableId+"_next",v.id=e.sTableId+"_last"},fnUpdate:function(e,m){if(e.aanFeatures.p){var l=j.ext.oPagination.iFullNumbersShowPages,k=Math.floor(l/2),x=Math.ceil(e.fnRecordsDisplay()/e._iDisplayLength),r=Math.ceil(e._iDisplayStart/
e._iDisplayLength)+1,v="",A,E=e.oClasses,y,J=e.aanFeatures.p,H=function(h){e.oApi._fnBindAction(this,{page:h+A-1},function(h){e.oApi._fnPageChange(e,h.data.page);m(e);h.preventDefault()})};-1===e._iDisplayLength?r=k=A=1:x<l?(A=1,k=x):r<=k?(A=1,k=l):r>=x-k?(A=x-l+1,k=x):(A=r-Math.ceil(l/2)+1,k=A+l-1);for(l=A;l<=k;l++)v+=r!==l?'<a tabindex="'+e.iTabIndex+'" class="'+E.sPageButton+'">'+e.fnFormatNumber(l)+"</a>":'<a tabindex="'+e.iTabIndex+'" class="'+E.sPageButtonActive+'">'+e.fnFormatNumber(l)+"</a>";
for(l=0,k=J.length;l<k;l++)0!==J[l].childNodes.length&&(h("span:eq(0)",J[l]).html(v).children("a").each(H),y=J[l].getElementsByTagName("a"),y=[y[0],y[1],y[y.length-2],y[y.length-1]],h(y).removeClass(E.sPageButton+" "+E.sPageButtonActive+" "+E.sPageButtonStaticDisabled),h([y[0],y[1]]).addClass(1==r?E.sPageButtonStaticDisabled:E.sPageButton),h([y[2],y[3]]).addClass(0===x||r===x||-1===e._iDisplayLength?E.sPageButtonStaticDisabled:E.sPageButton))}}}});h.extend(j.ext.oSort,{"string-pre":function(e){"string"!=
typeof e&&(e=null!==e&&e.toString?e.toString():"");return e.toLowerCase()},"string-asc":function(e,h){return e<h?-1:e>h?1:0},"string-desc":function(e,h){return e<h?1:e>h?-1:0},"html-pre":function(e){return e.replace(/<.*?>/g,"").toLowerCase()},"html-asc":function(e,h){return e<h?-1:e>h?1:0},"html-desc":function(e,h){return e<h?1:e>h?-1:0},"date-pre":function(e){e=Date.parse(e);if(isNaN(e)||""===e)e=Date.parse("01/01/1970 00:00:00");return e},"date-asc":function(e,h){return e-h},"date-desc":function(e,
h){return h-e},"numeric-pre":function(e){return"-"==e||""===e?0:1*e},"numeric-asc":function(e,h){return e-h},"numeric-desc":function(e,h){return h-e}});h.extend(j.ext.aTypes,[function(e){if("number"===typeof e)return"numeric";if("string"!==typeof e)return null;var h,j=!1;h=e.charAt(0);if(-1=="0123456789-".indexOf(h))return null;for(var k=1;k<e.length;k++){h=e.charAt(k);if(-1=="0123456789.".indexOf(h))return null;if("."==h){if(j)return null;j=!0}}return"numeric"},function(e){var h=Date.parse(e);return null!==
h&&!isNaN(h)||"string"===typeof e&&0===e.length?"date":null},function(e){return"string"===typeof e&&-1!=e.indexOf("<")&&-1!=e.indexOf(">")?"html":null}]);h.fn.DataTable=j;h.fn.dataTable=j;h.fn.dataTableSettings=j.settings;h.fn.dataTableExt=j.ext})(jQuery,window,document,void 0);





 /*
 * chosen.jquery.min.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 */




 //((function(){var a;a=function(){function a(){this.options_index=0,this.parsed=[]}return a.prototype.add_node=function(a){return a.nodeName==="OPTGROUP"?this.add_group(a):this.add_option(a)},a.prototype.add_group=function(a){var b,c,d,e,f,g;b=this.parsed.length,this.parsed.push({array_index:b,group:!0,label:a.label,children:0,disabled:a.disabled}),f=a.childNodes,g=[];for(d=0,e=f.length;d<e;d++)c=f[d],g.push(this.add_option(c,b,a.disabled));return g},a.prototype.add_option=function(a,b,c){if(a.nodeName==="OPTION")return a.text!==""?(b!=null&&(this.parsed[b].children+=1),this.parsed.push({array_index:this.parsed.length,options_index:this.options_index,value:a.value,text:a.text,html:a.innerHTML,selected:a.selected,disabled:c===!0?c:a.disabled,group_array_index:b,classes:a.className,style:a.style.cssText})):this.parsed.push({array_index:this.parsed.length,options_index:this.options_index,empty:!0}),this.options_index+=1},a}(),a.select_to_array=function(b){var c,d,e,f,g;d=new a,g=b.childNodes;for(e=0,f=g.length;e<f;e++)c=g[e],d.add_node(c);return d.parsed},this.SelectParser=a})).call(this),function(){var a,b;b=this,a=function(){function a(a,b){this.form_field=a,this.options=b!=null?b:{},this.set_default_values(),this.is_multiple=this.form_field.multiple,this.default_text_default=this.is_multiple?"Select Some Options":"Select an Option",this.setup(),this.set_up_html(),this.register_observers(),this.finish_setup()}return a.prototype.set_default_values=function(){var a=this;return this.click_test_action=function(b){return a.test_active_click(b)},this.activate_action=function(b){return a.activate_field(b)},this.active_field=!1,this.mouse_on_container=!1,this.results_showing=!1,this.result_highlighted=null,this.result_single_selected=null,this.allow_single_deselect=this.options.allow_single_deselect!=null&&this.form_field.options[0]!=null&&this.form_field.options[0].text===""?this.options.allow_single_deselect:!1,this.disable_search_threshold=this.options.disable_search_threshold||0,this.search_contains=this.options.search_contains||!1,this.choices=0,this.max_selected_options=this.options.max_selected_options||Infinity,this.results_none_found=this.options.no_results_text||"No results match"},a.prototype.mouse_enter=function(){return this.mouse_on_container=!0},a.prototype.mouse_leave=function(){return this.mouse_on_container=!1},a.prototype.input_focus=function(a){var b=this;if(!this.active_field)return setTimeout(function(){return b.container_mousedown()},50)},a.prototype.input_blur=function(a){var b=this;if(!this.mouse_on_container)return this.active_field=!1,setTimeout(function(){return b.blur_test()},100)},a.prototype.result_add_option=function(a){var b,c;return a.disabled?"":(a.dom_id=this.container_id+"_o_"+a.array_index,b=a.selected&&this.is_multiple?[]:["active-result"],a.selected&&b.push("result-selected"),a.group_array_index!=null&&b.push("group-option"),a.classes!==""&&b.push(a.classes),c=a.style.cssText!==""?' style="'+a.style+'"':"",'<li id="'+a.dom_id+'" class="'+b.join(" ")+'"'+c+">"+a.html+"</li>")},a.prototype.results_update_field=function(){return this.result_clear_highlight(),this.result_single_selected=null,this.results_build()},a.prototype.results_toggle=function(){return this.results_showing?this.results_hide():this.results_show()},a.prototype.results_search=function(a){return this.results_showing?this.winnow_results():this.results_show()},a.prototype.keyup_checker=function(a){var b,c;b=(c=a.which)!=null?c:a.keyCode,this.search_field_scale();switch(b){case 8:if(this.is_multiple&&this.backstroke_length<1&&this.choices>0)return this.keydown_backstroke();if(!this.pending_backstroke)return this.result_clear_highlight(),this.results_search();break;case 13:a.preventDefault();if(this.results_showing)return this.result_select(a);break;case 27:return this.results_showing&&this.results_hide(),!0;case 9:case 38:case 40:case 16:case 91:case 17:break;default:return this.results_search()}},a.prototype.generate_field_id=function(){var a;return a=this.generate_random_id(),this.form_field.id=a,a},a.prototype.generate_random_char=function(){var a,b,c;return a="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ",c=Math.floor(Math.random()*a.length),b=a.substring(c,c+1)},a}(),b.AbstractChosen=a}.call(this),function(){var a,b,c,d,e=Object.prototype.hasOwnProperty,f=function(a,b){function d(){this.constructor=a}for(var c in b)e.call(b,c)&&(a[c]=b[c]);return d.prototype=b.prototype,a.prototype=new d,a.__super__=b.prototype,a};d=this,a=jQuery,a.fn.extend({chosen:function(c){return!a.browser.msie||a.browser.version!=="6.0"&&a.browser.version!=="7.0"?this.each(function(d){var e;e=a(this);if(!e.hasClass("chzn-done"))return e.data("chosen",new b(this,c))}):this}}),b=function(b){function e(){e.__super__.constructor.apply(this,arguments)}return f(e,b),e.prototype.setup=function(){return this.form_field_jq=a(this.form_field),this.is_rtl=this.form_field_jq.hasClass("chzn-rtl")},e.prototype.finish_setup=function(){return this.form_field_jq.addClass("chzn-done")},e.prototype.set_up_html=function(){var b,d,e,f;return this.container_id=this.form_field.id.length?this.form_field.id.replace(/[^\w]/g,"_"):this.generate_field_id(),this.container_id+="_chzn",this.f_width=this.form_field_jq.outerWidth(),this.default_text=this.form_field_jq.data("placeholder")?this.form_field_jq.data("placeholder"):this.default_text_default,b=a("<div />",{id:this.container_id,"class":"chzn-container"+(this.is_rtl?" chzn-rtl":""),style:"width: "+this.f_width+"px;"}),this.is_multiple?b.html('<ul class="chzn-choices"><li class="search-field"><input type="text" value="'+this.default_text+'" class="default" autocomplete="off" style="width:25px;" /></li></ul><div class="chzn-drop" style="left:-9000px;"><ul class="chzn-results"></ul></div>'):b.html('<a href="javascript:void(0)" class="chzn-single chzn-default"><span>'+this.default_text+'</span><div><b></b></div></a><div class="chzn-drop" style="left:-9000px;"><div class="chzn-search"><input type="text" autocomplete="off" /></div><ul class="chzn-results"></ul></div>'),this.form_field_jq.hide().after(b),this.container=a("#"+this.container_id),this.container.addClass("chzn-container-"+(this.is_multiple?"multi":"single")),this.dropdown=this.container.find("div.chzn-drop").first(),d=this.container.height(),e=this.f_width-c(this.dropdown),this.dropdown.css({width:e+"px",top:d+"px"}),this.search_field=this.container.find("input").first(),this.search_results=this.container.find("ul.chzn-results").first(),this.search_field_scale(),this.search_no_results=this.container.find("li.no-results").first(),this.is_multiple?(this.search_choices=this.container.find("ul.chzn-choices").first(),this.search_container=this.container.find("li.search-field").first()):(this.search_container=this.container.find("div.chzn-search").first(),this.selected_item=this.container.find(".chzn-single").first(),f=e-c(this.search_container)-c(this.search_field),this.search_field.css({width:f+"px"})),this.results_build(),this.set_tab_index(),this.form_field_jq.trigger("liszt:ready",{chosen:this})},e.prototype.register_observers=function(){var a=this;return this.container.mousedown(function(b){return a.container_mousedown(b)}),this.container.mouseup(function(b){return a.container_mouseup(b)}),this.container.mouseenter(function(b){return a.mouse_enter(b)}),this.container.mouseleave(function(b){return a.mouse_leave(b)}),this.search_results.mouseup(function(b){return a.search_results_mouseup(b)}),this.search_results.mouseover(function(b){return a.search_results_mouseover(b)}),this.search_results.mouseout(function(b){return a.search_results_mouseout(b)}),this.form_field_jq.bind("liszt:updated",function(b){return a.results_update_field(b)}),this.search_field.blur(function(b){return a.input_blur(b)}),this.search_field.keyup(function(b){return a.keyup_checker(b)}),this.search_field.keydown(function(b){return a.keydown_checker(b)}),this.is_multiple?(this.search_choices.click(function(b){return a.choices_click(b)}),this.search_field.focus(function(b){return a.input_focus(b)})):this.container.click(function(a){return a.preventDefault()})},e.prototype.search_field_disabled=function(){this.is_disabled=this.form_field_jq[0].disabled;if(this.is_disabled)return this.container.addClass("chzn-disabled"),this.search_field[0].disabled=!0,this.is_multiple||this.selected_item.unbind("focus",this.activate_action),this.close_field();this.container.removeClass("chzn-disabled"),this.search_field[0].disabled=!1;if(!this.is_multiple)return this.selected_item.bind("focus",this.activate_action)},e.prototype.container_mousedown=function(b){var c;if(!this.is_disabled)return c=b!=null?a(b.target).hasClass("search-choice-close"):!1,b&&b.type==="mousedown"&&!this.results_showing&&b.stopPropagation(),!this.pending_destroy_click&&!c?(this.active_field?!this.is_multiple&&b&&(a(b.target)[0]===this.selected_item[0]||a(b.target).parents("a.chzn-single").length)&&(b.preventDefault(),this.results_toggle()):(this.is_multiple&&this.search_field.val(""),a(document).click(this.click_test_action),this.results_show()),this.activate_field()):this.pending_destroy_click=!1},e.prototype.container_mouseup=function(a){if(a.target.nodeName==="ABBR")return this.results_reset(a)},e.prototype.blur_test=function(a){if(!this.active_field&&this.container.hasClass("chzn-container-active"))return this.close_field()},e.prototype.close_field=function(){return a(document).unbind("click",this.click_test_action),this.is_multiple||(this.selected_item.attr("tabindex",this.search_field.attr("tabindex")),this.search_field.attr("tabindex",-1)),this.active_field=!1,this.results_hide(),this.container.removeClass("chzn-container-active"),this.winnow_results_clear(),this.clear_backstroke(),this.show_search_field_default(),this.search_field_scale()},e.prototype.activate_field=function(){return!this.is_multiple&&!this.active_field&&(this.search_field.attr("tabindex",this.selected_item.attr("tabindex")),this.selected_item.attr("tabindex",-1)),this.container.addClass("chzn-container-active"),this.active_field=!0,this.search_field.val(this.search_field.val()),this.search_field.focus()},e.prototype.test_active_click=function(b){return a(b.target).parents("#"+this.container_id).length?this.active_field=!0:this.close_field()},e.prototype.results_build=function(){var a,b,c,e,f;this.parsing=!0,this.results_data=d.SelectParser.select_to_array(this.form_field),this.is_multiple&&this.choices>0?(this.search_choices.find("li.search-choice").remove(),this.choices=0):this.is_multiple||(this.selected_item.find("span").text(this.default_text),this.form_field.options.length<=this.disable_search_threshold?this.container.addClass("chzn-container-single-nosearch"):this.container.removeClass("chzn-container-single-nosearch")),a="",f=this.results_data;for(c=0,e=f.length;c<e;c++)b=f[c],b.group?a+=this.result_add_group(b):b.empty||(a+=this.result_add_option(b),b.selected&&this.is_multiple?this.choice_build(b):b.selected&&!this.is_multiple&&(this.selected_item.removeClass("chzn-default").find("span").text(b.text),this.allow_single_deselect&&this.single_deselect_control_build()));return this.search_field_disabled(),this.show_search_field_default(),this.search_field_scale(),this.search_results.html(a),this.parsing=!1},e.prototype.result_add_group=function(b){return b.disabled?"":(b.dom_id=this.container_id+"_g_"+b.array_index,'<li id="'+b.dom_id+'" class="group-result">'+a("<div />").text(b.label).html()+"</li>")},e.prototype.result_do_highlight=function(a){var b,c,d,e,f;if(a.length){this.result_clear_highlight(),this.result_highlight=a,this.result_highlight.addClass("highlighted"),d=parseInt(this.search_results.css("maxHeight"),10),f=this.search_results.scrollTop(),e=d+f,c=this.result_highlight.position().top+this.search_results.scrollTop(),b=c+this.result_highlight.outerHeight();if(b>=e)return this.search_results.scrollTop(b-d>0?b-d:0);if(c<f)return this.search_results.scrollTop(c)}},e.prototype.result_clear_highlight=function(){return this.result_highlight&&this.result_highlight.removeClass("highlighted"),this.result_highlight=null},e.prototype.results_show=function(){var a;if(!this.is_multiple)this.selected_item.addClass("chzn-single-with-drop"),this.result_single_selected&&this.result_do_highlight(this.result_single_selected);else if(this.max_selected_options<=this.choices)return this.form_field_jq.trigger("liszt:maxselected",{chosen:this}),!1;return a=this.is_multiple?this.container.height():this.container.height()-1,this.dropdown.css({top:a+"px",left:0}),this.results_showing=!0,this.search_field.focus(),this.search_field.val(this.search_field.val()),this.winnow_results()},e.prototype.results_hide=function(){return this.is_multiple||this.selected_item.removeClass("chzn-single-with-drop"),this.result_clear_highlight(),this.dropdown.css({left:"-9000px"}),this.results_showing=!1},e.prototype.set_tab_index=function(a){var b;if(this.form_field_jq.attr("tabindex"))return b=this.form_field_jq.attr("tabindex"),this.form_field_jq.attr("tabindex",-1),this.is_multiple?this.search_field.attr("tabindex",b):(this.selected_item.attr("tabindex",b),this.search_field.attr("tabindex",-1))},e.prototype.show_search_field_default=function(){return this.is_multiple&&this.choices<1&&!this.active_field?(this.search_field.val(this.default_text),this.search_field.addClass("default")):(this.search_field.val(""),this.search_field.removeClass("default"))},e.prototype.search_results_mouseup=function(b){var c;c=a(b.target).hasClass("active-result")?a(b.target):a(b.target).parents(".active-result").first();if(c.length)return this.result_highlight=c,this.result_select(b)},e.prototype.search_results_mouseover=function(b){var c;c=a(b.target).hasClass("active-result")?a(b.target):a(b.target).parents(".active-result").first();if(c)return this.result_do_highlight(c)},e.prototype.search_results_mouseout=function(b){if(a(b.target).hasClass("active-result"))return this.result_clear_highlight()},e.prototype.choices_click=function(b){b.preventDefault();if(this.active_field&&!a(b.target).hasClass("search-choice")&&!this.results_showing)return this.results_show()},e.prototype.choice_build=function(b){var c,d,e=this;return this.is_multiple&&this.max_selected_options<=this.choices?(this.form_field_jq.trigger("liszt:maxselected",{chosen:this}),!1):(c=this.container_id+"_c_"+b.array_index,this.choices+=1,this.search_container.before('<li class="search-choice" id="'+c+'"><span>'+b.html+'</span><a href="javascript:void(0)" class="search-choice-close" rel="'+b.array_index+'"></a></li>'),d=a("#"+c).find("a").first(),d.click(function(a){return e.choice_destroy_link_click(a)}))},e.prototype.choice_destroy_link_click=function(b){return b.preventDefault(),this.is_disabled?b.stopPropagation:(this.pending_destroy_click=!0,this.choice_destroy(a(b.target)))},e.prototype.choice_destroy=function(a){return this.choices-=1,this.show_search_field_default(),this.is_multiple&&this.choices>0&&this.search_field.val().length<1&&this.results_hide(),this.result_deselect(a.attr("rel")),a.parents("li").first().remove()},e.prototype.results_reset=function(b){this.form_field.options[0].selected=!0,this.selected_item.find("span").text(this.default_text),this.is_multiple||this.selected_item.addClass("chzn-default"),this.show_search_field_default(),a(b.target).remove(),this.form_field_jq.trigger("change");if(this.active_field)return this.results_hide()},e.prototype.result_select=function(a){var b,c,d,e;if(this.result_highlight)return b=this.result_highlight,c=b.attr("id"),this.result_clear_highlight(),this.is_multiple?this.result_deactivate(b):(this.search_results.find(".result-selected").removeClass("result-selected"),this.result_single_selected=b,this.selected_item.removeClass("chzn-default")),b.addClass("result-selected"),e=c.substr(c.lastIndexOf("_")+1),d=this.results_data[e],d.selected=!0,this.form_field.options[d.options_index].selected=!0,this.is_multiple?this.choice_build(d):(this.selected_item.find("span").first().text(d.text),this.allow_single_deselect&&this.single_deselect_control_build()),(!a.metaKey||!this.is_multiple)&&this.results_hide(),this.search_field.val(""),this.form_field_jq.trigger("change"),this.search_field_scale()},e.prototype.result_activate=function(a){return a.addClass("active-result")},e.prototype.result_deactivate=function(a){return a.removeClass("active-result")},e.prototype.result_deselect=function(b){var c,d;return d=this.results_data[b],d.selected=!1,this.form_field.options[d.options_index].selected=!1,c=a("#"+this.container_id+"_o_"+b),c.removeClass("result-selected").addClass("active-result").show(),this.result_clear_highlight(),this.winnow_results(),this.form_field_jq.trigger("change"),this.search_field_scale()},e.prototype.single_deselect_control_build=function(){if(this.allow_single_deselect&&this.selected_item.find("abbr").length<1)return this.selected_item.find("span").first().after('<abbr class="search-choice-close"></abbr>')},e.prototype.winnow_results=function(){var b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s;this.no_results_clear(),j=0,k=this.search_field.val()===this.default_text?"":a("<div/>").text(a.trim(this.search_field.val())).html(),g=this.search_contains?"":"^",f=new RegExp(g+k.replace(/[-[\]{}()*+?.,\\^$|#\s]/g,"\\$&"),"i"),n=new RegExp(k.replace(/[-[\]{}()*+?.,\\^$|#\s]/g,"\\$&"),"i"),s=this.results_data;for(o=0,q=s.length;o<q;o++){c=s[o];if(!c.disabled&&!c.empty)if(c.group)a("#"+c.dom_id).css("display","none");else if(!this.is_multiple||!c.selected){b=!1,i=c.dom_id,h=a("#"+i);if(f.test(c.html))b=!0,j+=1;else if(c.html.indexOf(" ")>=0||c.html.indexOf("[")===0){e=c.html.replace(/\[|\]/g,"").split(" ");if(e.length)for(p=0,r=e.length;p<r;p++)d=e[p],f.test(d)&&(b=!0,j+=1)}b?(k.length?(l=c.html.search(n),m=c.html.substr(0,l+k.length)+"</em>"+c.html.substr(l+k.length),m=m.substr(0,l)+"<em>"+m.substr(l)):m=c.html,h.html(m),this.result_activate(h),c.group_array_index!=null&&a("#"+this.results_data[c.group_array_index].dom_id).css("display","list-item")):(this.result_highlight&&i===this.result_highlight.attr("id")&&this.result_clear_highlight(),this.result_deactivate(h))}}return j<1&&k.length?this.no_results(k):this.winnow_results_set_highlight()},e.prototype.winnow_results_clear=function(){var b,c,d,e,f;this.search_field.val(""),c=this.search_results.find("li"),f=[];for(d=0,e=c.length;d<e;d++)b=c[d],b=a(b),b.hasClass("group-result")?f.push(b.css("display","auto")):!this.is_multiple||!b.hasClass("result-selected")?f.push(this.result_activate(b)):f.push(void 0);return f},e.prototype.winnow_results_set_highlight=function(){var a,b;if(!this.result_highlight){b=this.is_multiple?[]:this.search_results.find(".result-selected.active-result"),a=b.length?b.first():this.search_results.find(".active-result").first();if(a!=null)return this.result_do_highlight(a)}},e.prototype.no_results=function(b){var c;return c=a('<li class="no-results">'+this.results_none_found+' "<span></span>"</li>'),c.find("span").first().html(b),this.search_results.append(c)},e.prototype.no_results_clear=function(){return this.search_results.find(".no-results").remove()},e.prototype.keydown_arrow=function(){var b,c;this.result_highlight?this.results_showing&&(c=this.result_highlight.nextAll("li.active-result").first(),c&&this.result_do_highlight(c)):(b=this.search_results.find("li.active-result").first(),b&&this.result_do_highlight(a(b)));if(!this.results_showing)return this.results_show()},e.prototype.keyup_arrow=function(){var a;if(!this.results_showing&&!this.is_multiple)return this.results_show();if(this.result_highlight)return a=this.result_highlight.prevAll("li.active-result"),a.length?this.result_do_highlight(a.first()):(this.choices>0&&this.results_hide(),this.result_clear_highlight())},e.prototype.keydown_backstroke=function(){return this.pending_backstroke?(this.choice_destroy(this.pending_backstroke.find("a").first()),this.clear_backstroke()):(this.pending_backstroke=this.search_container.siblings("li.search-choice").last(),this.pending_backstroke.addClass("search-choice-focus"))},e.prototype.clear_backstroke=function(){return this.pending_backstroke&&this.pending_backstroke.removeClass("search-choice-focus"),this.pending_backstroke=null},e.prototype.keydown_checker=function(a){var b,c;b=(c=a.which)!=null?c:a.keyCode,this.search_field_scale(),b!==8&&this.pending_backstroke&&this.clear_backstroke();switch(b){case 8:this.backstroke_length=this.search_field.val().length;break;case 9:this.results_showing&&!this.is_multiple&&this.result_select(a),this.mouse_on_container=!1;break;case 13:a.preventDefault();break;case 38:a.preventDefault(),this.keyup_arrow();break;case 40:this.keydown_arrow()}},e.prototype.search_field_scale=function(){var b,c,d,e,f,g,h,i,j;if(this.is_multiple){d=0,h=0,f="position:absolute; left: -1000px; top: -1000px; display:none;",g=["font-size","font-style","font-weight","font-family","line-height","text-transform","letter-spacing"];for(i=0,j=g.length;i<j;i++)e=g[i],f+=e+":"+this.search_field.css(e)+";";return c=a("<div />",{style:f}),c.text(this.search_field.val()),a("body").append(c),h=c.width()+25,c.remove(),h>this.f_width-10&&(h=this.f_width-10),this.search_field.css({width:h+"px"}),b=this.container.height(),this.dropdown.css({top:b+"px"})}},e.prototype.generate_random_id=function(){var b;b="sel"+this.generate_random_char()+this.generate_random_char()+this.generate_random_char();while(a("#"+b).length>0)b+=this.generate_random_char();return b},e}(AbstractChosen),c=function(a){var b;return b=a.outerWidth()-a.width()},d.get_side_border_padding=c}.call(this);

 
(function() {
  var SelectParser;

  SelectParser = (function() {

    function SelectParser() {
      this.options_index = 0;
      this.parsed = [];
    }

    SelectParser.prototype.add_node = function(child) {
      if (child.nodeName === "OPTGROUP") {
        return this.add_group(child);
      } else {
        return this.add_option(child);
      }
    };

    SelectParser.prototype.add_group = function(group) {
      var group_position, option, _i, _len, _ref, _results;
      group_position = this.parsed.length;
      this.parsed.push({
        array_index: group_position,
        group: true,
        label: group.label,
        children: 0,
        disabled: group.disabled
      });
      _ref = group.childNodes;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        option = _ref[_i];
        _results.push(this.add_option(option, group_position, group.disabled));
      }
      return _results;
    };

    SelectParser.prototype.add_option = function(option, group_position, group_disabled) {
      if (option.nodeName === "OPTION") {
        if (option.text !== "") {
          if (group_position != null) this.parsed[group_position].children += 1;
          this.parsed.push({
            array_index: this.parsed.length,
            options_index: this.options_index,
            value: option.value,
            text: option.text,
            html: option.innerHTML,
            selected: option.selected,
            disabled: group_disabled === true ? group_disabled : option.disabled,
            group_array_index: group_position,
            classes: option.className,
            style: option.style.cssText
          });
        } else {
          this.parsed.push({
            array_index: this.parsed.length,
            options_index: this.options_index,
            empty: true
          });
        }
        return this.options_index += 1;
      }
    };

    return SelectParser;

  })();

  SelectParser.select_to_array = function(select) {
    var child, parser, _i, _len, _ref;
    parser = new SelectParser();
    _ref = select.childNodes;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      child = _ref[_i];
      parser.add_node(child);
    }
    return parser.parsed;
  };

  this.SelectParser = SelectParser;

}).call(this);

/*
Chosen source: generate output using 'cake build'
Copyright (c) 2011 by Harvest
*/

(function() {
  var AbstractChosen, root;

  root = this;

  AbstractChosen = (function() {

    function AbstractChosen(form_field, options) {
      this.form_field = form_field;
      this.options = options != null ? options : {};
      this.set_default_values();
      this.is_multiple = this.form_field.multiple;
      this.default_text_default = this.is_multiple ? "Select" : "Select";
      this.setup();
      this.set_up_html();
      this.register_observers();
      this.finish_setup();
    }

    AbstractChosen.prototype.set_default_values = function() {
      var _this = this;
      this.click_test_action = function(evt) {
        return _this.test_active_click(evt);
      };
      this.activate_action = function(evt) {
        return _this.activate_field(evt);
      };
      this.active_field = false;
      this.mouse_on_container = false;
      this.results_showing = false;
      this.result_highlighted = null;
      this.result_single_selected = null;
      this.allow_single_deselect = (this.options.allow_single_deselect != null) && (this.form_field.options[0] != null) && this.form_field.options[0].text === "" ? this.options.allow_single_deselect : false;
      this.disable_search_threshold = this.options.disable_search_threshold || 0;
      this.search_contains = this.options.search_contains || false;
      this.choices = 0;
      this.max_selected_options = this.options.max_selected_options || Infinity;
      return this.results_none_found = this.options.no_results_text || "No results match";
    };

    AbstractChosen.prototype.mouse_enter = function() {
      return this.mouse_on_container = true;
    };

    AbstractChosen.prototype.mouse_leave = function() {
      return this.mouse_on_container = false;
    };

    AbstractChosen.prototype.input_focus = function(evt) {
      var _this = this;
      if (!this.active_field) {
        return setTimeout((function() {
          return _this.container_mousedown();
        }), 50);
      }
    };

    AbstractChosen.prototype.input_blur = function(evt) {
      var _this = this;
      if (!this.mouse_on_container) {
        this.active_field = false;
        return setTimeout((function() {
          return _this.blur_test();
        }), 100);
      }
    };

    AbstractChosen.prototype.result_add_option = function(option) {
      var classes, style;
      if (!option.disabled) {
        option.dom_id = this.container_id + "_o_" + option.array_index;
        classes = option.selected && this.is_multiple ? [] : ["active-result"];
        if (option.selected) classes.push("result-selected");
        if (option.group_array_index != null) classes.push("group-option");
        if (option.classes !== "") classes.push(option.classes);
        style = option.style.cssText !== "" ? " style=\"" + option.style + "\"" : "";
        return '<li id="' + option.dom_id + '" class="' + classes.join(' ') + '"' + style + '>' + option.html + '</li>';
      } else {
        return "";
      }
    };

    AbstractChosen.prototype.results_update_field = function() {
      this.result_clear_highlight();
      this.result_single_selected = null;
      return this.results_build();
    };

    AbstractChosen.prototype.results_toggle = function() {
      if (this.results_showing) {
        return this.results_hide();
      } else {
        return this.results_show();
      }
    };

    AbstractChosen.prototype.results_search = function(evt) {
      if (this.results_showing) {
      this.winnow_results();
       var ddl_top1 = parseInt(this.dropdown.css('top').replace('px',''));
       if(ddl_top1 < 0){
           this.dropdown.css({
              "display": "block"
           }).position({ my: 'left bottom', at: 'left top', of: this.container, collision: 'flip'});
      }

       // return this.winnow_results();
      } else {
        return this.results_show();
      }
    };

    AbstractChosen.prototype.keyup_checker = function(evt) {
      var stroke, _ref;
      stroke = (_ref = evt.which) != null ? _ref : evt.keyCode;
      this.search_field_scale();
      switch (stroke) {
        case 8:
          if (this.is_multiple && this.backstroke_length < 1 && this.choices > 0) {
            return this.keydown_backstroke();
          } else if (!this.pending_backstroke) {
            this.result_clear_highlight();
            return this.results_search();
          }
          break;
        case 13:
          evt.preventDefault();
          if (this.results_showing) return this.result_select(evt);
          break;
        case 27:
          if (this.results_showing) this.results_hide();
          return true;
        case 9:
        case 38:
        case 40:
        case 16:
        case 91:
        case 17:
          break;
        default:
          return this.results_search();
      }
    };

    AbstractChosen.prototype.generate_field_id = function() {
      var new_id;
      new_id = this.generate_random_id();
      this.form_field.id = new_id;
      return new_id;
    };

    AbstractChosen.prototype.generate_random_char = function() {
      var chars, newchar, rand;
      chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      rand = Math.floor(Math.random() * chars.length);
      return newchar = chars.substring(rand, rand + 1);
    };

    return AbstractChosen;

  })();

  root.AbstractChosen = AbstractChosen;

}).call(this);

/*
Chosen source: generate output using 'cake build'
Copyright (c) 2011 by Harvest
*/

(function() {
  var $, Chosen, get_side_border_padding, root,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  root = this;

  $ = jQuery;

  $.fn.extend({
    chosen: function(options) {
      if ($.browser.msie && ($.browser.version === "6.0" || $.browser.version === "7.0")) {
        return this;
      }
      return this.each(function(input_field) {
        var $this;
        $this = $(this);
        if (!$this.hasClass("chzn-done")) {
          return $this.data('chosen', new Chosen(this, options));
        }
      });
    }
  });

  Chosen = (function(_super) {

    __extends(Chosen, _super);

    function Chosen() {
      Chosen.__super__.constructor.apply(this, arguments);
    }

    Chosen.prototype.setup = function() {
      this.form_field_jq = $(this.form_field);
      return this.is_rtl = this.form_field_jq.hasClass("chzn-rtl");
    };

    Chosen.prototype.finish_setup = function() {
      return this.form_field_jq.addClass("chzn-done");
    };

    Chosen.prototype.set_up_html = function() {
      var container_div, dd_top, dd_width, sf_width;
      this.container_id = this.form_field.id.length ? this.form_field.id.replace(/[^\w]/g, '_') : this.generate_field_id();
      this.container_id += "_chzn";
      this.f_width = this.form_field_jq.outerWidth();
      this.default_text = this.form_field_jq.data('placeholder') ? this.form_field_jq.data('placeholder') : this.default_text_default;
      container_div = $("<div />", {
        id: this.container_id,
        "class": "chzn-container" + (this.is_rtl ? ' chzn-rtl' : ''),
        style: 'width: ' + this.f_width + 'px;'
      });
      if (this.is_multiple) {
        container_div.html('<ul class="chzn-choices"><li class="search-field"><input type="text" value="' + this.default_text + '" class="default" autocomplete="off" style="width:25px;" /></li></ul><div class="chzn-drop" style="left:-9000px;"><ul class="chzn-results"></ul></div>');
      } else {
        container_div.html('<a href="javascript:void(0)" class="chzn-single chzn-default"><span>' +escapeHtml(this.default_text,1) + '</span><div><b></b></div></a><div class="chzn-drop" style="left:-9000px;"><div class="chzn-search"><input type="text" autocomplete="off" /></div><ul class="chzn-results"></ul></div>');
      }
      this.form_field_jq.hide().after(container_div);
      this.container = $('#' + this.container_id);
      this.container.addClass("chzn-container-" + (this.is_multiple ? "multi" : "single"));
      this.dropdown = this.container.find('div.chzn-drop').first();
      dd_top = this.container.height();
      
      dd_width = this.f_width - get_side_border_padding(this.dropdown);
      this.dropdown.css({
        "width": dd_width + "px",
        "top": dd_top + "px"
      });
      this.search_field = this.container.find('input').first();
      this.search_results = this.container.find('ul.chzn-results').first();
      this.search_field_scale();
      this.search_no_results = this.container.find('li.no-results').first();
      if (this.is_multiple) {
        this.search_choices = this.container.find('ul.chzn-choices').first();
        this.search_container = this.container.find('li.search-field').first();
      } else {
        this.search_container = this.container.find('div.chzn-search').first();
        this.selected_item = this.container.find('.chzn-single').first();
        sf_width = dd_width - get_side_border_padding(this.search_container) - get_side_border_padding(this.search_field);
        this.search_field.css({
          "width": sf_width + "px"
        });
      }
      this.results_build();
      this.set_tab_index();
      return this.form_field_jq.trigger("liszt:ready", {
        chosen: this
      });
    };

    Chosen.prototype.register_observers = function() {
      var _this = this;
      this.container.mousedown(function(evt) {
        return _this.container_mousedown(evt);
      });
      this.container.mouseup(function(evt) {
        return _this.container_mouseup(evt);
      });
      this.container.mouseenter(function(evt) {
        return _this.mouse_enter(evt);
      });
      this.container.mouseleave(function(evt) {
        return _this.mouse_leave(evt);
      });
      this.search_results.mouseup(function(evt) {
        return _this.search_results_mouseup(evt);
      });
      this.search_results.mouseover(function(evt) {
        return _this.search_results_mouseover(evt);
      });
      this.search_results.mouseout(function(evt) {
        return _this.search_results_mouseout(evt);
      });
      this.form_field_jq.bind("liszt:updated", function(evt) {
        return _this.results_update_field(evt);
      });
      this.search_field.blur(function(evt) {
        return _this.input_blur(evt);
      });
      this.search_field.keyup(function(evt) {
        return _this.keyup_checker(evt);
      });
      this.search_field.keydown(function(evt) {
        return _this.keydown_checker(evt);
      });
      if (this.is_multiple) {
        this.search_choices.click(function(evt) {
          return _this.choices_click(evt);
        });
        return this.search_field.focus(function(evt) {
          return _this.input_focus(evt);
        });
      } else {
        return this.container.click(function(evt) {
          return evt.preventDefault();
        });
      }
    };

    Chosen.prototype.search_field_disabled = function() {
      this.is_disabled = this.form_field_jq[0].disabled;
      if (this.is_disabled) {
        this.container.addClass('chzn-disabled');
        this.search_field[0].disabled = true;
        if (!this.is_multiple) {
          this.selected_item.unbind("focus", this.activate_action);
        }
        return this.close_field();
      } else {
        this.container.removeClass('chzn-disabled');
        this.search_field[0].disabled = false;
        if (!this.is_multiple) {
          return this.selected_item.bind("focus", this.activate_action);
        }
      }
    };

    Chosen.prototype.container_mousedown = function(evt) {
      var target_closelink;
      if (!this.is_disabled) {
        target_closelink = evt != null ? ($(evt.target)).hasClass("search-choice-close") : false;
        if (evt && evt.type === "mousedown" && !this.results_showing) {
          evt.stopPropagation();
        }
        if (!this.pending_destroy_click && !target_closelink) {
          if (!this.active_field) {
            if (this.is_multiple) this.search_field.val("");
            $(document).click(this.click_test_action);
            this.results_show();
          } else if (!this.is_multiple && evt && (($(evt.target)[0] === this.selected_item[0]) || $(evt.target).parents("a.chzn-single").length)) {
            evt.preventDefault();
            this.results_toggle();
          }
          return this.activate_field();
        } else {
          return this.pending_destroy_click = false;
        }
      }
    };

    Chosen.prototype.container_mouseup = function(evt) {
      if (evt.target.nodeName === "ABBR") return this.results_reset(evt);
    };

    Chosen.prototype.blur_test = function(evt) {
      if (!this.active_field && this.container.hasClass("chzn-container-active")) {
        return this.close_field();
      }
    };

    Chosen.prototype.close_field = function() {
      $(document).unbind("click", this.click_test_action);
      if (!this.is_multiple) {
        this.selected_item.attr("tabindex", this.search_field.attr("tabindex"));
        this.search_field.attr("tabindex", -1);
      }
      this.active_field = false;
      this.results_hide();
      this.container.removeClass("chzn-container-active");
      this.winnow_results_clear();
      this.clear_backstroke();
      this.show_search_field_default();
      return this.search_field_scale();
    };

    Chosen.prototype.activate_field = function() {
      if (!this.is_multiple && !this.active_field) {
        this.search_field.attr("tabindex", this.selected_item.attr("tabindex"));
        this.selected_item.attr("tabindex", -1);
      }
      this.container.addClass("chzn-container-active");
      this.active_field = true;
      this.search_field.val(this.search_field.val());
      return this.search_field.focus();
    };

    Chosen.prototype.test_active_click = function(evt) {
      if ($(evt.target).parents('#' + this.container_id).length) {
        return this.active_field = true;
      } else {
        return this.close_field();
      }
    };

    Chosen.prototype.results_build = function() {
      var content, data, _i, _len, _ref;
      this.parsing = true;
      this.results_data = root.SelectParser.select_to_array(this.form_field);
      if (this.is_multiple && this.choices > 0) {
        this.search_choices.find("li.search-choice").remove();
        this.choices = 0;
      } else if (!this.is_multiple) {
        this.selected_item.find("span").text(this.default_text);
        if (this.form_field.options.length <= this.disable_search_threshold) {
          this.container.addClass("chzn-container-single-nosearch");
        } else {
          this.container.removeClass("chzn-container-single-nosearch");
        }
      }
      content = '';
      _ref = this.results_data;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        data = _ref[_i];
        if (data.group) {
          content += this.result_add_group(data);
        } else if (!data.empty) {
          content += this.result_add_option(data);
          if (data.selected && this.is_multiple) {
            this.choice_build(data);
          } else if (data.selected && !this.is_multiple) {
            this.selected_item.removeClass("chzn-default").find("span").text(data.text);
            if (this.allow_single_deselect) this.single_deselect_control_build();
          }
        }
      }
      this.search_field_disabled();
      this.show_search_field_default();
      this.search_field_scale();
      this.search_results.html(content);
      return this.parsing = false;
    };

    Chosen.prototype.result_add_group = function(group) {
      if (!group.disabled) {
        group.dom_id = this.container_id + "_g_" + group.array_index;
        return '<li id="' + group.dom_id + '" class="group-result">' + $("<div />").text(group.label).html() + '</li>';
      } else {
        return "";
      }
    };

    Chosen.prototype.result_do_highlight = function(el) {
      var high_bottom, high_top, maxHeight, visible_bottom, visible_top;
      if (el.length) {
        this.result_clear_highlight();
        this.result_highlight = el;
        this.result_highlight.addClass("highlighted");
        maxHeight = parseInt(this.search_results.css("maxHeight"), 10);
        visible_top = this.search_results.scrollTop();
        visible_bottom = maxHeight + visible_top;
        high_top = this.result_highlight.position().top + this.search_results.scrollTop();
        high_bottom = high_top + this.result_highlight.outerHeight();
        if (high_bottom >= visible_bottom) {
          return this.search_results.scrollTop((high_bottom - maxHeight) > 0 ? high_bottom - maxHeight : 0);
        } else if (high_top < visible_top) {
          return this.search_results.scrollTop(high_top);
        }
      }
    };

    Chosen.prototype.result_clear_highlight = function() {
      if (this.result_highlight) this.result_highlight.removeClass("highlighted");
      return this.result_highlight = null;
    };

    Chosen.prototype.results_show = function() {
      var dd_top;
      if (!this.is_multiple) {
        this.selected_item.addClass("chzn-single-with-drop");
        if (this.result_single_selected) {
          this.result_do_highlight(this.result_single_selected);
        }
      } else if (this.max_selected_options <= this.choices) {
        this.form_field_jq.trigger("liszt:maxselected", {
          chosen: this
        });
        return false;
      }
      dd_top = this.is_multiple ? this.container.height() : this.container.height() - 1;
//      this.dropdown.css({
//        "top": dd_top + "px",
//        "left": 0
//      });

        this.dropdown.css({
        "display": "block"
      }).position({ my: 'left top', at: 'left bottom', of: this.container, collision: 'fit flip'});

      if (!this.is_multiple) {
          var ddl_top1 = parseInt(this.dropdown.css('top').replace('px',''));
          if(ddl_top1 < 0){
            $(this.search_container).insertAfter(this.search_results);
          }else{
           $(this.search_container).insertBefore(this.search_results);
          }
       }
      //this.container.find('div.chzn-search').first()

      this.results_showing = true;
      this.search_field.focus();
      this.container.find('.chzn-single').css('border-bottom','none');
      this.search_field.val(this.search_field.val());
      return this.winnow_results();
    };

    Chosen.prototype.results_hide = function() {
      if (!this.is_multiple) {
        this.selected_item.removeClass("chzn-single-with-drop");
      }
      this.result_clear_highlight();
      this.dropdown.css({
        "left": "-9000px",
         "display": "none"
      });
      return this.results_showing = false;
    };

    Chosen.prototype.set_tab_index = function(el) {
      var ti;
      if (this.form_field_jq.attr("tabindex")) {
        ti = this.form_field_jq.attr("tabindex");
        this.form_field_jq.attr("tabindex", -1);
        if (this.is_multiple) {
          return this.search_field.attr("tabindex", ti);
        } else {
          this.selected_item.attr("tabindex", ti);
          return this.search_field.attr("tabindex", -1);
        }
      }
    };

    Chosen.prototype.show_search_field_default = function() {
      if (this.is_multiple && this.choices < 1 && !this.active_field) {
        this.search_field.val(this.default_text);
        return this.search_field.addClass("default");
      } else {
        this.search_field.val("");
        return this.search_field.removeClass("default");
      }
    };

    Chosen.prototype.search_results_mouseup = function(evt) {
      var target;
      target = $(evt.target).hasClass("active-result") ? $(evt.target) : $(evt.target).parents(".active-result").first();
      if (target.length) {
        this.result_highlight = target;
        return this.result_select(evt);
      }
    };

    Chosen.prototype.search_results_mouseover = function(evt) {
      var target;
      target = $(evt.target).hasClass("active-result") ? $(evt.target) : $(evt.target).parents(".active-result").first();
      if (target) return this.result_do_highlight(target);
    };

    Chosen.prototype.search_results_mouseout = function(evt) {
      if ($(evt.target).hasClass("active-result" || $(evt.target).parents('.active-result').first())) {
        return this.result_clear_highlight();
      }
    };

    Chosen.prototype.choices_click = function(evt) {
      evt.preventDefault();
      if (this.active_field && !($(evt.target).hasClass("search-choice" || $(evt.target).parents('.search-choice').first)) && !this.results_showing) {
        return this.results_show();
      }
    };

    Chosen.prototype.choice_build = function(item) {
      var choice_id, link,
        _this = this;
      if (this.is_multiple && this.max_selected_options <= this.choices) {
        this.form_field_jq.trigger("liszt:maxselected", {
          chosen: this
        });
        return false;
      }
      choice_id = this.container_id + "_c_" + item.array_index;
      this.choices += 1;
      this.search_container.before('<li class="search-choice" id="' + choice_id + '"><span>' + item.html + '</span><a href="javascript:void(0)" class="search-choice-close" rel="' + item.array_index + '"></a></li>');
      link = $('#' + choice_id).find("a").first();
      return link.click(function(evt) {
        return _this.choice_destroy_link_click(evt);
      });
    };

    Chosen.prototype.choice_destroy_link_click = function(evt) {
      evt.preventDefault();
      if (!this.is_disabled) {
        this.pending_destroy_click = true;
        return this.choice_destroy($(evt.target));
      } else {
        return evt.stopPropagation;
      }
    };

    Chosen.prototype.choice_destroy = function(link) {
      this.choices -= 1;
      this.show_search_field_default();
      if (this.is_multiple && this.choices > 0 && this.search_field.val().length < 1) {
        this.results_hide();
      }
      this.result_deselect(link.attr("rel"));
      return link.parents('li').first().remove();
    };

    Chosen.prototype.results_reset = function(evt) {
      this.form_field.options[0].selected = true;
      this.selected_item.find("span").text(this.default_text);
      if (!this.is_multiple) this.selected_item.addClass("chzn-default");
      this.show_search_field_default();
      $(evt.target).remove();
      this.form_field_jq.trigger("change");
      if (this.active_field) return this.results_hide();
    };

    Chosen.prototype.result_select = function(evt) {
      var high, high_id, item, position;
      if (this.result_highlight) {
        high = this.result_highlight;
        high_id = high.attr("id");
        this.result_clear_highlight();
        if (this.is_multiple) {
          this.result_deactivate(high);
        } else {
          this.search_results.find(".result-selected").removeClass("result-selected");
          this.result_single_selected = high;
          this.selected_item.removeClass("chzn-default");
        }
        high.addClass("result-selected");
        position = high_id.substr(high_id.lastIndexOf("_") + 1);
        item = this.results_data[position];
        item.selected = true;
        this.form_field.options[item.options_index].selected = true;
        if (this.is_multiple) {
          this.choice_build(item);
        } else {
          this.selected_item.find("span").first().text(item.text);
          if (this.allow_single_deselect) this.single_deselect_control_build();
        }
        if (!(evt.metaKey && this.is_multiple)) this.results_hide();
        this.search_field.val("");
        this.form_field_jq.trigger("change");
        return this.search_field_scale();
      }
    };

    Chosen.prototype.result_activate = function(el) {
      return el.addClass("active-result");
    };

    Chosen.prototype.result_deactivate = function(el) {
      return el.removeClass("active-result");
    };

    Chosen.prototype.result_deselect = function(pos) {
      var result, result_data;
      result_data = this.results_data[pos];
      result_data.selected = false;
      this.form_field.options[result_data.options_index].selected = false;
      result = $("#" + this.container_id + "_o_" + pos);
      result.removeClass("result-selected").addClass("active-result").show();
      this.result_clear_highlight();
      this.winnow_results();
      this.form_field_jq.trigger("change");
      return this.search_field_scale();
    };

    Chosen.prototype.single_deselect_control_build = function() {
      if (this.allow_single_deselect && this.selected_item.find("abbr").length < 1) {
        return this.selected_item.find("span").first().after("<abbr class=\"search-choice-close\"></abbr>");
      }
    };

    Chosen.prototype.winnow_results = function() {
      var found, option, part, parts, regex, regexAnchor, result, result_id, results, searchText, startpos, text, zregex, _i, _j, _len, _len2, _ref;
      this.no_results_clear();
      results = 0;
      searchText = this.search_field.val() === this.default_text ? "" : $('<div/>').text($.trim(this.search_field.val())).html();
      
      regexAnchor = this.search_contains ? "" : "^";
      regex = new RegExp(regexAnchor + searchText.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&"), 'i');
      zregex = new RegExp(searchText.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&"), 'i');
      _ref = this.results_data;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        option = _ref[_i];
        if (!option.disabled && !option.empty) {
          if (option.group) {
            $('#' + option.dom_id).css('display', 'none');
          } else if (!(this.is_multiple && option.selected)) {
            found = false;
            result_id = option.dom_id;
            result = $("#" + result_id);
            if (regex.test(option.html)) {
              found = true;
              results += 1;
            } else if (option.html.indexOf(" ") >= 0 || option.html.indexOf("[") === 0) {
              parts = option.html.replace(/\[|\]/g, "").split(" ");
              if (parts.length) {
                for (_j = 0, _len2 = parts.length; _j < _len2; _j++) {
                  part = parts[_j];
                  if (regex.test(part)) {
                    found = true;
                    results += 1;
                  }
                }
              }
            }
            if (found) {
              if (searchText.length) {
                startpos = option.html.search(zregex);
                text = option.html.substr(0, startpos + searchText.length) + '</em>' + option.html.substr(startpos + searchText.length);
                text = text.substr(0, startpos) + '<em>' + text.substr(startpos);

              } else {
                text = option.html;
              }
              result.html(text);
              this.result_activate(result);
              if (option.group_array_index != null) {
                $("#" + this.results_data[option.group_array_index].dom_id).css('display', 'list-item');
              }
            } else {
              if (this.result_highlight && result_id === this.result_highlight.attr('id')) {
                this.result_clear_highlight();
              }
               
              this.result_deactivate(result);
            }

            

          }
        }
      }
      if (results < 1 && searchText.length) {
        return this.no_results(searchText);
      } else {
        return this.winnow_results_set_highlight();
      }
    };

    Chosen.prototype.winnow_results_clear = function() {
      var li, lis, _i, _len, _results;
      this.search_field.val("");
      lis = this.search_results.find("li");
      _results = [];
      for (_i = 0, _len = lis.length; _i < _len; _i++) {
        li = lis[_i];
        li = $(li);
        if (li.hasClass("group-result")) {
          _results.push(li.css('display', 'auto'));
        } else if (!this.is_multiple || !li.hasClass("result-selected")) {
          _results.push(this.result_activate(li));
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };

    Chosen.prototype.winnow_results_set_highlight = function() {
      var do_high, selected_results;
      if (!this.result_highlight) {
        selected_results = !this.is_multiple ? this.search_results.find(".result-selected.active-result") : [];
        do_high = selected_results.length ? selected_results.first() : this.search_results.find(".active-result").first();
        if (do_high != null) return this.result_do_highlight(do_high);
      }
    };

    Chosen.prototype.no_results = function(terms) {
      var no_results_html;
      no_results_html = $('<li class="no-results">' + this.results_none_found + ' "<span></span>"</li>');
      no_results_html.find("span").first().html(terms);
      return this.search_results.append(no_results_html);
    };

    Chosen.prototype.no_results_clear = function() {
      return this.search_results.find(".no-results").remove();
    };

    Chosen.prototype.keydown_arrow = function() {
      var first_active, next_sib;
      if (!this.result_highlight) {
        first_active = this.search_results.find("li.active-result").first();
        if (first_active) this.result_do_highlight($(first_active));
      } else if (this.results_showing) {
        next_sib = this.result_highlight.nextAll("li.active-result").first();
        if (next_sib) this.result_do_highlight(next_sib);
      }
      if (!this.results_showing) return this.results_show();
    };

    Chosen.prototype.keyup_arrow = function() {
      var prev_sibs;
      if (!this.results_showing && !this.is_multiple) {
        return this.results_show();
      } else if (this.result_highlight) {
        prev_sibs = this.result_highlight.prevAll("li.active-result");
        if (prev_sibs.length) {
          return this.result_do_highlight(prev_sibs.first());
        } else {
          if (this.choices > 0) this.results_hide();
          return this.result_clear_highlight();
        }
      }
    };

    Chosen.prototype.keydown_backstroke = function() {
      if (this.pending_backstroke) {
        this.choice_destroy(this.pending_backstroke.find("a").first());
        return this.clear_backstroke();
      } else {
        this.pending_backstroke = this.search_container.siblings("li.search-choice").last();
        return this.pending_backstroke.addClass("search-choice-focus");
      }
    };

    Chosen.prototype.clear_backstroke = function() {
      if (this.pending_backstroke) {
        this.pending_backstroke.removeClass("search-choice-focus");
      }
      return this.pending_backstroke = null;
    };

    Chosen.prototype.keydown_checker = function(evt) {
      var stroke, _ref;
      stroke = (_ref = evt.which) != null ? _ref : evt.keyCode;
      this.search_field_scale();
      if (stroke !== 8 && this.pending_backstroke) this.clear_backstroke();
      switch (stroke) {
        case 8:
          this.backstroke_length = this.search_field.val().length;
          break;
        case 9:
          if (this.results_showing && !this.is_multiple) this.result_select(evt);
          this.mouse_on_container = false;
          break;
        case 13:
          evt.preventDefault();
          break;
        case 38:
          evt.preventDefault();
          this.keyup_arrow();
          break;
        case 40:
          this.keydown_arrow();
          break;
      }
    };

    Chosen.prototype.search_field_scale = function() {
      var dd_top, div, h, style, style_block, styles, w, _i, _len;

      if (this.is_multiple) {
        h = 0;
        w = 0;
        style_block = "position:absolute; left: -1000px; top: -1000px; display:none;";
        styles = ['font-size', 'font-style', 'font-weight', 'font-family', 'line-height', 'text-transform', 'letter-spacing'];
        for (_i = 0, _len = styles.length; _i < _len; _i++) {
          style = styles[_i];
          style_block += style + ":" + this.search_field.css(style) + ";";
        }
        div = $('<div />', {
          'style': style_block
        });
        div.text(this.search_field.val());
        $('body').append(div);
        w = div.width() + 25;
        div.remove();
        if (w > this.f_width - 10) w = this.f_width - 10;
        this.search_field.css({
          'width': w + 'px'
        });
        dd_top = this.container.height();
        return this.dropdown.css({
          "top": dd_top + "px"
        });
      //  return this.dropdown.position({ my: 'left top', at: 'left bottom', of: this.container, collision: 'fit flip' });
      }
    };

    Chosen.prototype.generate_random_id = function() {
      var string;
      string = "sel" + this.generate_random_char() + this.generate_random_char() + this.generate_random_char();
      while ($("#" + string).length > 0) {
        string += this.generate_random_char();
      }
      return string;
    };

    return Chosen;

  })(AbstractChosen);

  get_side_border_padding = function(elmt) {
    var side_border_padding;
    return side_border_padding = elmt.outerWidth() - elmt.width();
  };

  root.get_side_border_padding = get_side_border_padding;

}).call(this);



 /*
 * colorpicker-min.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 */

 (function(b){var a=function(){var S={},c,N=65,t,P='<div class="colorpicker"><div class="colorpicker_color"><div><div></div></div></div><div class="colorpicker_hue"><div></div></div><div class="colorpicker_new_color"></div><div class="colorpicker_current_color"></div><div class="colorpicker_hex"><input type="text" maxlength="6" size="6" /></div><div class="colorpicker_rgb_r colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_rgb_g colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_rgb_b colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_hsb_h colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_hsb_s colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_hsb_b colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_submit"></div></div>',B={eventName:"click",onShow:function(){},onBeforeShow:function(){},onHide:function(){},onChange:function(){},onSubmit:function(){},color:"ff0000",livePreview:true,flat:false},J=function(T,V){var U=j(T);b(V).data("colorpicker").fields.eq(1).val(U.r).end().eq(2).val(U.g).end().eq(3).val(U.b).end()},u=function(T,U){b(U).data("colorpicker").fields.eq(4).val(T.h).end().eq(5).val(T.s).end().eq(6).val(T.b).end()},g=function(T,U){b(U).data("colorpicker").fields.eq(0).val(R(T)).end()},l=function(T,U){b(U).data("colorpicker").selector.css("backgroundColor","#"+R({h:T.h,s:100,b:100}));b(U).data("colorpicker").selectorIndic.css({left:parseInt(150*T.s/100,10),top:parseInt(150*(100-T.b)/100,10)})},G=function(T,U){b(U).data("colorpicker").hue.css("top",parseInt(150-150*T.h/360,10))},h=function(T,U){b(U).data("colorpicker").currentColor.css("backgroundColor","#"+R(T))},E=function(T,U){b(U).data("colorpicker").newColor.css("backgroundColor","#"+R(T))},n=function(T){var V=T.charCode||T.keyCode||-1;if((V>N&&V<=90)||V==32){return false}var U=b(this).parent().parent();if(U.data("colorpicker").livePreview===true){e.apply(this)}},e=function(U){var V=b(this).parent().parent(),T;if(this.parentNode.className.indexOf("_hex")>0){V.data("colorpicker").color=T=m(y(this.value))}else{if(this.parentNode.className.indexOf("_hsb")>0){V.data("colorpicker").color=T=f({h:parseInt(V.data("colorpicker").fields.eq(4).val(),10),s:parseInt(V.data("colorpicker").fields.eq(5).val(),10),b:parseInt(V.data("colorpicker").fields.eq(6).val(),10)})}else{V.data("colorpicker").color=T=i(M({r:parseInt(V.data("colorpicker").fields.eq(1).val(),10),g:parseInt(V.data("colorpicker").fields.eq(2).val(),10),b:parseInt(V.data("colorpicker").fields.eq(3).val(),10)}))}}if(U){J(T,V.get(0));g(T,V.get(0));u(T,V.get(0))}l(T,V.get(0));G(T,V.get(0));E(T,V.get(0));V.data("colorpicker").onChange.apply(V,[T,R(T),j(T)])},o=function(T){var U=b(this).parent().parent();U.data("colorpicker").fields.parent().removeClass("colorpicker_focus")},K=function(){N=this.parentNode.className.indexOf("_hex")>0?70:65;b(this).parent().parent().data("colorpicker").fields.parent().removeClass("colorpicker_focus");b(this).parent().addClass("colorpicker_focus")},I=function(T){var V=b(this).parent().find("input").focus();var U={el:b(this).parent().addClass("colorpicker_slider"),max:this.parentNode.className.indexOf("_hsb_h")>0?360:(this.parentNode.className.indexOf("_hsb")>0?100:255),y:T.pageY,field:V,val:parseInt(V.val(),10),preview:b(this).parent().parent().data("colorpicker").livePreview};b(document).bind("mouseup",U,s);b(document).bind("mousemove",U,L)},L=function(T){T.data.field.val(Math.max(0,Math.min(T.data.max,parseInt(T.data.val+T.pageY-T.data.y,10))));if(T.data.preview){e.apply(T.data.field.get(0),[true])}return false},s=function(T){e.apply(T.data.field.get(0),[true]);T.data.el.removeClass("colorpicker_slider").find("input").focus();b(document).unbind("mouseup",s);b(document).unbind("mousemove",L);return false},w=function(T){var U={cal:b(this).parent(),y:b(this).offset().top};U.preview=U.cal.data("colorpicker").livePreview;b(document).bind("mouseup",U,r);b(document).bind("mousemove",U,k)},k=function(T){e.apply(T.data.cal.data("colorpicker").fields.eq(4).val(parseInt(360*(150-Math.max(0,Math.min(150,(T.pageY-T.data.y))))/150,10)).get(0),[T.data.preview]);return false},r=function(T){J(T.data.cal.data("colorpicker").color,T.data.cal.get(0));g(T.data.cal.data("colorpicker").color,T.data.cal.get(0));b(document).unbind("mouseup",r);b(document).unbind("mousemove",k);return false},x=function(T){var U={cal:b(this).parent(),pos:b(this).offset()};U.preview=U.cal.data("colorpicker").livePreview;b(document).bind("mouseup",U,A);b(document).bind("mousemove",U,q)},q=function(T){e.apply(T.data.cal.data("colorpicker").fields.eq(6).val(parseInt(100*(150-Math.max(0,Math.min(150,(T.pageY-T.data.pos.top))))/150,10)).end().eq(5).val(parseInt(100*(Math.max(0,Math.min(150,(T.pageX-T.data.pos.left))))/150,10)).get(0),[T.data.preview]);return false},A=function(T){J(T.data.cal.data("colorpicker").color,T.data.cal.get(0));g(T.data.cal.data("colorpicker").color,T.data.cal.get(0));b(document).unbind("mouseup",A);b(document).unbind("mousemove",q);return false},v=function(T){b(this).addClass("colorpicker_focus")},Q=function(T){b(this).removeClass("colorpicker_focus")},p=function(U){var V=b(this).parent();var T=V.data("colorpicker").color;V.data("colorpicker").origColor=T;h(T,V.get(0));V.data("colorpicker").onSubmit(T,R(T),j(T),V.data("colorpicker").el)},D=function(T){var X=b("#"+b(this).data("colorpickerId"));X.data("colorpicker").onBeforeShow.apply(this,[X.get(0)]);var Y=b(this).offset();var W=z();var V=Y.top+this.offsetHeight;var U=Y.left;if(V+176>W.t+W.h){V-=this.offsetHeight+176}if(U+356>W.l+W.w){U-=356}X.css({left:U+"px",top:V+"px"});if(X.data("colorpicker").onShow.apply(this,[X.get(0)])!=false){X.show()}b(document).bind("mousedown",{cal:X},O);return false},O=function(T){if(!H(T.data.cal.get(0),T.target,T.data.cal.get(0))){if(T.data.cal.data("colorpicker").onHide.apply(this,[T.data.cal.get(0)])!=false){T.data.cal.hide()}b(document).unbind("mousedown",O)}},H=function(V,U,T){if(V==U){return true}if(V.contains){return V.contains(U)}if(V.compareDocumentPosition){return !!(V.compareDocumentPosition(U)&16)}var W=U.parentNode;while(W&&W!=T){if(W==V){return true}W=W.parentNode}return false},z=function(){var T=document.compatMode=="CSS1Compat";return{l:window.pageXOffset||(T?document.documentElement.scrollLeft:document.body.scrollLeft),t:window.pageYOffset||(T?document.documentElement.scrollTop:document.body.scrollTop),w:window.innerWidth||(T?document.documentElement.clientWidth:document.body.clientWidth),h:window.innerHeight||(T?document.documentElement.clientHeight:document.body.clientHeight)}},f=function(T){return{h:Math.min(360,Math.max(0,T.h)),s:Math.min(100,Math.max(0,T.s)),b:Math.min(100,Math.max(0,T.b))}},M=function(T){return{r:Math.min(255,Math.max(0,T.r)),g:Math.min(255,Math.max(0,T.g)),b:Math.min(255,Math.max(0,T.b))}},y=function(V){var T=6-V.length;if(T>0){var W=[];for(var U=0;U<T;U++){W.push("0")}W.push(V);V=W.join("")}return V},d=function(T){var T=parseInt(((T.indexOf("#")>-1)?T.substring(1):T),16);return{r:T>>16,g:(T&65280)>>8,b:(T&255)}},m=function(T){return i(d(T))},i=function(V){var U={h:0,s:0,b:0};var W=Math.min(V.r,V.g,V.b);var T=Math.max(V.r,V.g,V.b);var X=T-W;U.b=T;if(T!=0){}U.s=T!=0?255*X/T:0;if(U.s!=0){if(V.r==T){U.h=(V.g-V.b)/X}else{if(V.g==T){U.h=2+(V.b-V.r)/X}else{U.h=4+(V.r-V.g)/X}}}else{U.h=-1}U.h*=60;if(U.h<0){U.h+=360}U.s*=100/255;U.b*=100/255;return U},j=function(T){var V={};var Z=Math.round(T.h);var Y=Math.round(T.s*255/100);var U=Math.round(T.b*255/100);if(Y==0){V.r=V.g=V.b=U}else{var aa=U;var X=(255-Y)*U/255;var W=(aa-X)*(Z%60)/60;if(Z==360){Z=0}if(Z<60){V.r=aa;V.b=X;V.g=X+W}else{if(Z<120){V.g=aa;V.b=X;V.r=aa-W}else{if(Z<180){V.g=aa;V.r=X;V.b=X+W}else{if(Z<240){V.b=aa;V.r=X;V.g=aa-W}else{if(Z<300){V.b=aa;V.g=X;V.r=X+W}else{if(Z<360){V.r=aa;V.g=X;V.b=aa-W}else{V.r=0;V.g=0;V.b=0}}}}}}}return{r:Math.round(V.r),g:Math.round(V.g),b:Math.round(V.b)}},C=function(T){var U=[T.r.toString(16),T.g.toString(16),T.b.toString(16)];b.each(U,function(V,W){if(W.length==1){U[V]="0"+W}});return U.join("")},R=function(T){return C(j(T))},F=function(){var U=b(this).parent();var T=U.data("colorpicker").origColor;U.data("colorpicker").color=T;J(T,U.get(0));g(T,U.get(0));u(T,U.get(0));l(T,U.get(0));G(T,U.get(0));E(T,U.get(0))};return{init:function(T){T=b.extend({},B,T||{});if(typeof T.color=="string"){T.color=m(T.color)}else{if(T.color.r!=undefined&&T.color.g!=undefined&&T.color.b!=undefined){T.color=i(T.color)}else{if(T.color.h!=undefined&&T.color.s!=undefined&&T.color.b!=undefined){T.color=f(T.color)}else{return this}}}return this.each(function(){if(!b(this).data("colorpickerId")){var U=b.extend({},T);U.origColor=T.color;var W="collorpicker_"+parseInt(Math.random()*1000);b(this).data("colorpickerId",W);var V=b(P).attr("id",W);if(U.flat){V.appendTo(this).show()}else{V.appendTo(document.body)}U.fields=V.find("input").bind("keyup",n).bind("change",e).bind("blur",o).bind("focus",K);V.find("span").bind("mousedown",I).end().find(">div.colorpicker_current_color").bind("click",F);U.selector=V.find("div.colorpicker_color").bind("mousedown",x);U.selectorIndic=U.selector.find("div div");U.el=this;U.hue=V.find("div.colorpicker_hue div");V.find("div.colorpicker_hue").bind("mousedown",w);U.newColor=V.find("div.colorpicker_new_color");U.currentColor=V.find("div.colorpicker_current_color");V.data("colorpicker",U);V.find("div.colorpicker_submit").bind("mouseenter",v).bind("mouseleave",Q).bind("click",p);J(U.color,V.get(0));u(U.color,V.get(0));g(U.color,V.get(0));G(U.color,V.get(0));l(U.color,V.get(0));h(U.color,V.get(0));E(U.color,V.get(0));if(U.flat){V.css({position:"relative",display:"block"})}else{b(this).bind(U.eventName,D)}}})},showPicker:function(){return this.each(function(){if(b(this).data("colorpickerId")){D.apply(this)}})},hidePicker:function(){return this.each(function(){if(b(this).data("colorpickerId")){b("#"+b(this).data("colorpickerId")).hide()}})},setColor:function(T){if(typeof T=="string"){T=m(T)}else{if(T.r!=undefined&&T.g!=undefined&&T.b!=undefined){T=i(T)}else{if(T.h!=undefined&&T.s!=undefined&&T.b!=undefined){T=f(T)}else{return this}}}return this.each(function(){if(b(this).data("colorpickerId")){var U=b("#"+b(this).data("colorpickerId"));U.data("colorpicker").color=T;U.data("colorpicker").origColor=T;J(T,U.get(0));u(T,U.get(0));g(T,U.get(0));G(T,U.get(0));l(T,U.get(0));h(T,U.get(0));E(T,U.get(0))}})}}}();b.fn.extend({ColorPicker:a.init,ColorPickerHide:a.hidePicker,ColorPickerShow:a.showPicker,ColorPickerSetColor:a.setColor})})(jQuery);





 

 /*
 * jquery.tipsy-min.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 */

 (function($){function maybeCall(thing,ctx){return(typeof thing=='function')?(thing.call(ctx)):thing};function Tipsy(element,options){this.$element=$(element);this.options=options;this.enabled=true;this.fixTitle()};Tipsy.prototype={show:function(){var title=this.getTitle();if(title&&this.enabled){var $tip=this.tip();$tip.find('.tipsy-inner')[this.options.html?'html':'text'](title);$tip[0].className='tipsy';$tip.remove().css({top:0,left:0,visibility:'hidden',display:'block'}).prependTo(document.body);var pos=$.extend({},this.$element.offset(),{width:this.$element[0].offsetWidth,height:this.$element[0].offsetHeight});var actualWidth=$tip[0].offsetWidth,actualHeight=$tip[0].offsetHeight,gravity=maybeCall(this.options.gravity,this.$element[0]);var tp;switch(gravity.charAt(0)){case'n':tp={top:pos.top+pos.height+this.options.offset,left:pos.left+pos.width/2-actualWidth/2};break;case's':tp={top:pos.top-actualHeight-this.options.offset,left:pos.left+pos.width/2-actualWidth/2};break;case'e':tp={top:pos.top+pos.height/2-actualHeight/2,left:pos.left-actualWidth-this.options.offset};break;case'w':tp={top:pos.top+pos.height/2-actualHeight/2,left:pos.left+pos.width+this.options.offset};break}if(gravity.length==2){if(gravity.charAt(1)=='w'){tp.left=pos.left+pos.width/2-15}else{tp.left=pos.left+pos.width/2-actualWidth+15}}$tip.css(tp).addClass('tipsy-'+gravity);$tip.find('.tipsy-arrow')[0].className='tipsy-arrow tipsy-arrow-'+gravity.charAt(0);if(this.options.className){$tip.addClass(maybeCall(this.options.className,this.$element[0]))}if(this.options.fade){$tip.stop().css({opacity:0,display:'block',visibility:'visible'}).animate({opacity:this.options.opacity})}else{$tip.css({visibility:'visible',opacity:this.options.opacity})}}},hide:function(){if(this.options.fade){this.tip().stop().fadeOut(function(){$(this).remove()})}else{this.tip().remove()}},fixTitle:function(){var $e=this.$element;if($e.attr('title')||typeof($e.attr('original-title'))!='string'){$e.attr('original-title',$e.attr('title')||'').removeAttr('title')}},getTitle:function(){var title,$e=this.$element,o=this.options;this.fixTitle();var title,o=this.options;if(typeof o.title=='string'){title=$e.attr(o.title=='title'?'original-title':o.title)}else if(typeof o.title=='function'){title=o.title.call($e[0])}title=(''+title).replace(/(^\s*|\s*$)/,"");return title||o.fallback},tip:function(){if(!this.$tip){this.$tip=$('<div class="tipsy"></div>').html('<div class="tipsy-arrow"></div><div class="tipsy-inner"></div>')}return this.$tip},validate:function(){if(!this.$element[0].parentNode){this.hide();this.$element=null;this.options=null}},enable:function(){this.enabled=true},disable:function(){this.enabled=false},toggleEnabled:function(){this.enabled=!this.enabled}};$.fn.tipsy=function(options){if(options===true){return this.data('tipsy')}else if(typeof options=='string'){var tipsy=this.data('tipsy');if(tipsy)tipsy[options]();return this}options=$.extend({},$.fn.tipsy.defaults,options);function get(ele){var tipsy=$.data(ele,'tipsy');if(!tipsy){tipsy=new Tipsy(ele,$.fn.tipsy.elementOptions(ele,options));$.data(ele,'tipsy',tipsy)}return tipsy}function enter(){var tipsy=get(this);tipsy.hoverState='in';if(options.delayIn==0){tipsy.show()}else{tipsy.fixTitle();setTimeout(function(){if(tipsy.hoverState=='in')tipsy.show()},options.delayIn)}};function leave(){var tipsy=get(this);tipsy.hoverState='out';if(options.delayOut==0){tipsy.hide()}else{setTimeout(function(){if(tipsy.hoverState=='out')tipsy.hide()},options.delayOut)}};if(!options.live)this.each(function(){get(this)});if(options.trigger!='manual'){var binder=options.live?'live':'bind',eventIn=options.trigger=='hover'?'mouseenter':'focus',eventOut=options.trigger=='hover'?'mouseleave':'blur';this[binder](eventIn,enter)[binder](eventOut,leave)}return this};$.fn.tipsy.defaults={className:null,delayIn:0,delayOut:0,fade:false,fallback:'',gravity:'n',html:true,live:true,offset:0,opacity:0.8,title:'title',trigger:'hover'};$.fn.tipsy.elementOptions=function(ele,options){return $.metadata?$.extend({},options,$(ele).metadata()):options};$.fn.tipsy.autoNS=function(){return $(this).offset().top>($(document).scrollTop()+$(window).height()/2)?'s':'n'};$.fn.tipsy.autoWE=function(){return $(this).offset().left>($(document).scrollLeft()+$(window).width()/2)?'e':'w'};$.fn.tipsy.autoBounds=function(margin,prefer){return function(){var dir={ns:prefer[0],ew:(prefer.length>1?prefer[1]:false)},boundTop=$(document).scrollTop()+margin,boundLeft=$(document).scrollLeft()+margin,$this=$(this);if($this.offset().top<boundTop)dir.ns='n';if($this.offset().left<boundLeft)dir.ew='w';if($(window).width()+$(document).scrollLeft()-$this.offset().left<margin)dir.ew='e';if($(window).height()+$(document).scrollTop()-$this.offset().top<margin)dir.ns='s';return dir.ns+(dir.ew?dir.ew:'')}}})(jQuery);


 
 /*
 * Sourcerer-1.2-min.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 */

 (function(e){String.prototype.highlight=function(n){var h=this;if(this.length){for(var j=0;j<n.length;j++){for(var g in n[j]){var k=new RegExp(g,"gi");var l=h.search(k);var m=h.indexOf("//");if(l<=m||m===-1||m===0){h=h.replace(k,"<span class='"+n[j][g]+"'>$&</span>")}}}}else{h="&nbsp;"}return h};function b(h){var g=[];if(h.indexOf("html")!==-1){g.push(f)}if(h.indexOf("js")!==-1){g.push(d)}if(h.indexOf("css")!==-1){g.push(c)}if(h.indexOf("php")!==-1){g.push(a)}return(g.length)?g:false}String.prototype.escapeHTML=function(){return(this.replace(/>/g,"&gt;").replace(/</g,"&lt;").replace(/&lt;!--\?php/g,"&lt;?php").replace(/\?--&gt;/g,"?&gt;"))};var f={"&lt;!--.*--&gt;":"SC_grey","&lt;\\/*(div|p|br|hr|link|em|meta|html|h2|h1|h3|ul|!DOCTYPE html|li|ol|body|select|span|head|title|canvas|video|audio|source|pre)\\s*(([A-Za-z]+)=(\"|')[a-zA-Z0-9\\.\\/\\(\\)\\?,&'_#\\s;:%=\\-~]*(\"|')\\s*)*\\s*\\/*&gt;(?!.*--&gt;)":"SC_blue",'&lt;\\/*script\\s*(([A-Za-z]+)="[a-zA-Z0-9\\.\\/]*"\\s*)*\\s*\\/*&gt;(?!.*--&gt;)':"SC_red",'&lt;\\/*a\\s*(([A-Za-z]+)="[a-zA-Z0-9\\.\\/\\(\\)\\[\\]\\?\\s\\$=,\'_#;:&~(&lt;)(&gt;)-]*"\\s*)*\\s*\\/*&gt;(?!.*--&gt;)':"SC_green",'&lt;img\\s+(([A-Za-z]+)="[a-zA-Z0-9\\.\\/\\(\\),\\s\'_#;:]+"\\s*)*\\s*\\/*&gt;(?!.*--&gt;)':"SC_red",'&lt;\\/*(form|input|textarea|button|option|select)\\s*(([A-Za-z]+)="[a-zA-Z0-9\\(\\);,\':\\s\\.]*"\\s*)*\\s*\\/*&gt;(?!.*--&gt;)':"SC_gold",'&lt;\\/*(td|tr|table|tbody)\\s*(([A-Za-z]+)="[a-zA-Z0-9\\.\\/\\(\\)\\s,\'_%#;:~-]*"\\s*)*\\s*\\/*&gt;(?!.*--&gt;)':"SC_teal",'"[a-zA-Z0-9\\s\\/\\.\\?\\[\\]\\$#;:_\',&=%\\)\\(~-]*"(?!(.*--&gt;)|(s*[A-Za-z]))':"SC_navy"};var c={"((#|\\.)*[a-zA-Z0-9_:,\\s\\.\\*#]+\\s*\\{)|(\\})":"SC_pink","\\/\\*.*\\*\\/":"SC_grey","[a-zA-Z0-9-]+:(?![a-zA-Z0-9_:\\s\\.]+\\{)":"SC_blue","[a-zA-Z0-9,'_#\\(\\)\\/%@\\s\\.\"-]+;":"SC_green"};var d={"\\/\\/.*":"SC_grey","((function|var|this|for|if|else|switch|while|try|catch)\\s*(\\{|\\())":"SC_blue","(\\.[a-zA-Z]+(?=\\())|(alert\\()|((String|Date|Array|Number)\\.)":"SC_teal","(((\"|')[a-zA-Z0-9\\s-\\/\\.\\?#!;:_',&=%\\)\\(~]*(\"|'))(?!(\\>)|(.*--&gt;)|(s*[A-Za-z])))|(this\\.)|(var\\s)|(return\\s)|(true;)|(false;)|(return;)":"SC_navy","([\\(\\)\\{\\}\\*\\[\\]\\+-]{1})":"SC_blue","[0-9]+(?![A-Za-z]+)":"SC_red","(function|var|this|false|true|return|for|if|else|switch|while|try|catch|do|in)(\\s*[A-Za-z]*\\s*(\\(|\\{))*":"SC_bold"};var a={"(\\/\\/.*)|(\\/\\*.*\\*\\/)":"SC_grey","(((\"|')[a-zA-Z0-9\\s-\\/\\.\\?\\*#;:_',&=%\\)\\(~]*(\"|'))(?!\\>))":"SC_navy","(((&lt;\\?php)|(\\?&gt;))(?!.*\\*\\/))|([0-9](?![A_Za-z]*\"|'))":"SC_red","(([a-zA-Z0-9_]+\\()|(echo)|(\\$_[a-zA-Z0-9]+))(?!.*\\*\\/)":"SC_green","[\\(\\)\\{\\}(\\*\\+]{1}(?!.*\\*\\/)":"SC_blue","\\$[A-Za-z0-9]+":"SC_teal"};e.fn.extend({sourcerer:function(h){var j=this.selector;var i={lang:false,numbers:true,title:false};var g={};return this.each(function(){if(!e(this).children(".SRC_Wrap").length){h=(typeof(h)=="string")?{lang:h}:h;e.extend(g,i,h);var o=[];if(g.lang){var m=e(this).html().escapeHTML().split("\n");o.push("<div class='SRC_Wrap'>");if(g.title){o.push("<div class='SRC_Title'>"+g.title+"</div>")}o.push("<div class='SRC_NumBox'></div><div class='SRC_CodeBox'>");for(var k=0;k<m.length;k++){o.push("<div class='SRC_Line'><pre class='SRC_CodeContent'>"+m[k].highlight(b(g.lang))+"</pre></div>")}o.push("</div><div style='clear:both;'></div></div>");e(this).html(o.join(""));if(g.numbers){var n=e(this).find(".SRC_NumBox");o=[];for(var k=0;k<m.length;k++){var l=e(e(".SRC_Line")[k]).height();o.push("<div class='SRC_Line' style='height:"+l+"px'><div class='SRC_NumContent'>"+(k+1)+"</div></div>")}n.html(o.join(""))}else{e(this).find(".SRC_CodeBox").width("100%");e(this).find(".SRC_CodeContent").css("border-left",0)}}else{throw"::No language given or language was invalid for sourcerer call with selector '"+j+"'"}}})}})})(jQuery);


 /*
 * jquery.validate.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 * jQuery Validation Plugin 1.9.0
 *
 * http://bassistance.de/jquery-plugins/jquery-plugin-validation/
 * http://docs.jquery.com/Plugins/Validation
 *
 * Copyright (c) 2006 - 2011 Jörn Zaefferer
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */



(function ($) {

    $.extend($.fn, {
        // http://docs.jquery.com/Plugins/Validation/validate
        validate: function (options) {

            // if nothing is selected, return nothing; can't chain anyway
            if (!this.length) {
                options && options.debug && window.console && console.warn("nothing selected, can't validate, returning nothing");
                return;
            }

            // check if a validator for this form was already created
            var validator = $.data(this[0], 'validator');
            if (validator) {
                return validator;
            }

            // Add novalidate tag if HTML5.
            this.attr('novalidate', 'novalidate');

            validator = new $.validator(options, this[0]);
            $.data(this[0], 'validator', validator);

            if (validator.settings.onsubmit) {

                var inputsAndButtons = this.find("input, button");

                // allow suppresing validation by adding a cancel class to the submit button
                inputsAndButtons.filter(".cancel").click(function () {
                    validator.cancelSubmit = true;
                });

                // when a submitHandler is used, capture the submitting button
                if (validator.settings.submitHandler) {
                    inputsAndButtons.filter(":submit").click(function () {
                        validator.submitButton = this;
                    });
                }

                // validate the form on submit
                this.submit(function (event) {
                    if (validator.settings.debug)
                    // prevent form submit to be able to see console output
                        event.preventDefault();

                    function handle() {
                        if (validator.settings.submitHandler) {
                            if (validator.submitButton) {
                                // insert a hidden input as a replacement for the missing submit button
                                var hidden = $("<input type='hidden'/>").attr("name", validator.submitButton.name).val(validator.submitButton.value).appendTo(validator.currentForm);
                            }
                            validator.settings.submitHandler.call(validator, validator.currentForm);
                            if (validator.submitButton) {
                                // and clean up afterwards; thanks to no-block-scope, hidden can be referenced
                                hidden.remove();
                            }
                            return false;
                        }
                        return true;
                    }

                    // prevent submit for invalid forms or custom submit handlers
                    if (validator.cancelSubmit) {
                        validator.cancelSubmit = false;
                        return handle();
                    }
                    if (validator.form()) {
                        if (validator.pendingRequest) {
                            validator.formSubmitted = true;
                            return false;
                        }
                        return handle();
                    } else {
                        validator.focusInvalid();
                        return false;
                    }
                });
            }

            return validator;
        },
        // http://docs.jquery.com/Plugins/Validation/valid
        valid: function () {
            if ($(this[0]).is('form')) {
                return this.validate().form();
            } else {
                var valid = true;
                var validator = $(this[0].form).validate();
                this.each(function () {//
                    if ($(this).attr('placeholder') == $(this).val()) {
                        $(this).val('');
                        valid &= validator.element(this);

                        $(this).val($(this).attr('placeholder'));


                    } else {
                        valid &= validator.element(this);
                    }




                });
                return valid;
            }
        },
        // attributes: space seperated list of attributes to retrieve and remove
        removeAttrs: function (attributes) {
            var result = {},
			$element = this;
            $.each(attributes.split(/\s/), function (index, value) {
                result[value] = $element.attr(value);
                $element.removeAttr(value);
            });
            return result;
        },
        // http://docs.jquery.com/Plugins/Validation/rules
        rules: function (command, argument) {
            var element = this[0];

            if (command) {
                var settings = $.data(element.form, 'validator').settings;
                var staticRules = settings.rules;
                var existingRules = $.validator.staticRules(element);
                switch (command) {
                    case "add":
                        $.extend(existingRules, $.validator.normalizeRule(argument));
                        staticRules[element.name] = existingRules;
                        if (argument.messages)
                            settings.messages[element.name] = $.extend(settings.messages[element.name], argument.messages);
                        break;
                    case "remove":
                        if (!argument) {
                            delete staticRules[element.name];
                            return existingRules;
                        }
                        var filtered = {};
                        $.each(argument.split(/\s/), function (index, method) {
                            filtered[method] = existingRules[method];
                            delete existingRules[method];
                        });
                        return filtered;
                }
            }

            var data = $.validator.normalizeRules(
		$.extend(
			{},
			$.validator.metadataRules(element),
			$.validator.classRules(element),
			$.validator.attributeRules(element),
			$.validator.staticRules(element)
		), element);

            // make sure required is at front
            if (data.required) {
                var param = data.required;
                delete data.required;
                data = $.extend({ required: param }, data);
            }

            return data;
        }
    });

    // Custom selectors
    $.extend($.expr[":"], {
        // http://docs.jquery.com/Plugins/Validation/blank
        blank: function (a) { return !$.trim("" + a.value); },
        // http://docs.jquery.com/Plugins/Validation/filled
        filled: function (a) { return !!$.trim("" + a.value); },
        // http://docs.jquery.com/Plugins/Validation/unchecked
        unchecked: function (a) { return !a.checked; }
    });

    // constructor for validator
    $.validator = function (options, form) {
        this.settings = $.extend(true, {}, $.validator.defaults, options);
        this.currentForm = form;
        this.init();
    };

    $.validator.format = function (source, params) {
        if (arguments.length == 1)
            return function () {
                var args = $.makeArray(arguments);
                args.unshift(source);
                return $.validator.format.apply(this, args);
            };
        if (arguments.length > 2 && params.constructor != Array) {
            params = $.makeArray(arguments).slice(1);
        }
        if (params.constructor != Array) {
            params = [params];
        }
        $.each(params, function (i, n) {
            source = source.replace(new RegExp("\\{" + i + "\\}", "g"), n);
        });
        return source;
    };

    $.extend($.validator, {

        defaults: {
            messages: {},
            groups: {},
            rules: {},
            errorClass: "error",
            validClass: "valid",
            errorElement: "label",
            focusInvalid: true,
            errorContainer: $([]),
            errorLabelContainer: $([]),
            onsubmit: true,
            ignore: "",
            ignoreTitle: false,
            onfocusin: function (element, event) {
                this.lastActive = element;

                // hide error label and remove error class on focus if enabled
                if (this.settings.focusCleanup && !this.blockFocusCleanup) {
                    this.settings.unhighlight && this.settings.unhighlight.call(this, element, this.settings.errorClass, this.settings.validClass);
                    this.addWrapper(this.errorsFor(element)).hide();
                }
            },
            onfocusout: function (element, event) {
                if (!this.checkable(element) && (element.name in this.submitted || !this.optional(element))) {
                    this.element(element);
                }
            },
            onkeyup: function (element, event) {
                if (element.name in this.submitted || element == this.lastElement) {
                    this.element(element);
                }
            },
            onclick: function (element, event) {
                // click on selects, radiobuttons and checkboxes
                if (element.name in this.submitted)
                    this.element(element);
                // or option elements, check parent select in that case
                else if (element.parentNode.name in this.submitted)
                    this.element(element.parentNode);
            },
            highlight: function (element, errorClass, validClass) {
                if (element.type === 'radio') {
                    this.findByName(element.name).addClass(errorClass).removeClass(validClass);
                } else {
                    $(element).addClass(errorClass).removeClass(validClass);
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                if (element.type === 'radio') {
                    this.findByName(element.name).removeClass(errorClass).addClass(validClass);
                } else {
                    $(element).removeClass(errorClass).addClass(validClass);
                }
            }
        },

        // http://docs.jquery.com/Plugins/Validation/Validator/setDefaults
        setDefaults: function (settings) {
            $.extend($.validator.defaults, settings);
        },

        messages: {
            required: "This field is required.",
            remote: "Please fix this field.",
            email: "Please enter a valid email address.",
            emaillist: "Please enter a valid email address.",
            url: "Please enter a valid URL.",
            date: "Please enter a valid date.",
            dateISO: "Please enter a valid date (ISO).",
            number: "Please enter a valid number.",
            digits: "Please enter only digits.",
            fax:"Please enter valid fax number",
            CIBIL:"Please enter valid CIBIL score",
            Landline: "Please enter valid landline number",  
            Contact : "Please enter valid contact number",          
            Residential:"Please enter valid residential number",
            Telephone:"Please enter valid telephone number",   
            mobile: "Please enter valid mobile number",
            pin: "Please enter valid pin",
            MaxMin3: "Please enter valid id",
            creditcard: "Please enter a valid credit card number.",
            panCard: "Please enter a valid pan card number.",
            TAN: "Please enter a valid TAN No.", // added by Bharati 14Nov2013 [Defect #7159]
            equalTo: "Please enter the same value again.",
            accept: "Please select valid extension file.",
            xlsaccept: "Please upload .csv or .xls file.",
            maxlength: $.validator.format("Please enter maximum {0} characters."),
            minlength: $.validator.format("Please enter at least {0} characters."),
            rangelength: $.validator.format("Please enter a value between {0} and {1} characters long."),
            range: $.validator.format("Please enter a value between {0} and {1}."),
            max: $.validator.format("Please enter a value less than or equal to {0}."),
            min: $.validator.format("Please enter a value greater than or equal to {0}."),
            FTP: "Invalid FTP url."
        },

        autoCreateRanges: false,

        prototype: {

            init: function () {
                this.labelContainer = $(this.settings.errorLabelContainer);
                this.errorContext = this.labelContainer.length && this.labelContainer || $(this.currentForm);
                this.containers = $(this.settings.errorContainer).add(this.settings.errorLabelContainer);
                this.submitted = {};
                this.valueCache = {};
                this.pendingRequest = 0;
                this.pending = {};
                this.invalid = {};
                this.reset();

                var groups = (this.groups = {});
                $.each(this.settings.groups, function (key, value) {
                    $.each(value.split(/\s/), function (index, name) {
                        groups[name] = key;
                    });
                });
                var rules = this.settings.rules;
                $.each(rules, function (key, value) {
                    rules[key] = $.validator.normalizeRule(value);
                });

                function delegate(event) {
                    var validator = $.data(this[0].form, "validator"),
					eventType = "on" + event.type.replace(/^validate/, "");
                    validator.settings[eventType] && validator.settings[eventType].call(validator, this[0], event);
                }
                $(this.currentForm)
			       .validateDelegate("[type='text'], [type='password'], [type='file'], select, textarea, " +
						"[type='number'], [type='search'] ,[type='tel'], [type='url'], " +
						"[type='email'], [type='datetime'], [type='date'], [type='month'], " +
						"[type='week'], [type='time'], [type='datetime-local'], " +
						"[type='range'], [type='color'], [type='FTP']",
						"focusin focusout keyup", delegate)
				.validateDelegate("[type='radio'], [type='checkbox'], select, option", "click", delegate);

                if (this.settings.invalidHandler)
                    $(this.currentForm).bind("invalid-form.validate", this.settings.invalidHandler);
            },

            // http://docs.jquery.com/Plugins/Validation/Validator/form
            form: function () {
                this.checkForm();
                $.extend(this.submitted, this.errorMap);
                this.invalid = $.extend({}, this.errorMap);
                if (!this.valid())
                    $(this.currentForm).triggerHandler("invalid-form", [this]);
                this.showErrors();
                return this.valid();
            },

            checkForm: function () {
                this.prepareForm();
                for (var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++) {
                    this.check(elements[i]);
                }
                return this.valid();
            },

            // http://docs.jquery.com/Plugins/Validation/Validator/element
            element: function (element) {
                element = this.validationTargetFor(this.clean(element));
                this.lastElement = element;
                this.prepareElement(element);
                this.currentElements = $(element);
                var result = this.check(element);
                if (result) {
                    delete this.invalid[element.name];
                } else {
                    this.invalid[element.name] = true;
                }
                if (!this.numberOfInvalids()) {
                    // Hide error containers on last error
                    this.toHide = this.toHide.add(this.containers);
                }
                this.showErrors();
                return result;
            },

            // http://docs.jquery.com/Plugins/Validation/Validator/showErrors
            showErrors: function (errors) {
                if (errors) {
                    // add items to error list and map
                    $.extend(this.errorMap, errors);
                    this.errorList = [];
                    for (var name in errors) {
                        this.errorList.push({
                            message: errors[name],
                            element: this.findByName(name)[0]
                        });
                    }
                    // remove items from success list
                    this.successList = $.grep(this.successList, function (element) {
                        return !(element.name in errors);
                    });
                }
                this.settings.showErrors
				? this.settings.showErrors.call(this, this.errorMap, this.errorList)
				: this.defaultShowErrors();
            },

            // http://docs.jquery.com/Plugins/Validation/Validator/resetForm
            resetForm: function () {
                if ($.fn.resetForm)
                    $(this.currentForm).resetForm();
                this.submitted = {};
                this.lastElement = null;
                this.prepareForm();
                this.hideErrors();
                this.elements().removeClass(this.settings.errorClass);
            },

            numberOfInvalids: function () {
                return this.objectLength(this.invalid);
            },

            objectLength: function (obj) {
                var count = 0;
                for (var i in obj)
                    count++;
                return count;
            },

            hideErrors: function () {
                this.addWrapper(this.toHide).hide();
            },

            valid: function () {
                return this.size() == 0;
            },

            size: function () {
                return this.errorList.length;
            },

            focusInvalid: function () {
                if (this.settings.focusInvalid) {
                    try {
                        $(this.findLastActive() || this.errorList.length && this.errorList[0].element || [])
					.filter(":visible")
					.focus()
                        // manually trigger focusin event; without it, focusin handler isn't called, findLastActive won't have anything to find
					.trigger("focusin");
                    } catch (e) {
                        // ignore IE throwing errors when focusing hidden elements
                    }
                }
            },

            findLastActive: function () {
                var lastActive = this.lastActive;
                return lastActive && $.grep(this.errorList, function (n) {
                    return n.element.name == lastActive.name;
                }).length == 1 && lastActive;
            },

            elements: function () {
                var validator = this,
				rulesCache = {};

                // select all valid inputs inside the form (no submit or reset buttons)
                return $(this.currentForm)
			.find("input, select, textarea")
			.not(":submit, :reset, :image, [disabled]")
			.not(this.settings.ignore)
			.filter(function () {
			    !this.name && validator.settings.debug && window.console && console.error("%o has no name assigned", this);

			    // select only the first element for each name, and only those with rules specified
			    if (this.name in rulesCache || !validator.objectLength($(this).rules()))
			        return false;

			    rulesCache[this.name] = true;
			    return true;
			});
            },

            clean: function (selector) {
                return $(selector)[0];
            },

            errors: function () {
                return $(this.settings.errorElement + "." + this.settings.errorClass, this.errorContext);
            },

            reset: function () {
                this.successList = [];
                this.errorList = [];
                this.errorMap = {};
                this.toShow = $([]);
                this.toHide = $([]);
                this.currentElements = $([]);
            },

            prepareForm: function () {
                this.reset();
                this.toHide = this.errors().add(this.containers);
            },

            prepareElement: function (element) {
                this.reset();
                this.toHide = this.errorsFor(element);
            },

            check: function (element) {
                element = this.validationTargetFor(this.clean(element));

                var rules = $(element).rules();
                var dependencyMismatch = false;
                for (var method in rules) {
                    var rule = { method: method, parameters: rules[method] };
                    try {
                        var result = $.validator.methods[method].call(this, element.value.replace(/\r/g, ""), element, rule.parameters);
                         if(rule.method=="pin" && element.value == "")
                        {
                        result="dependency-mismatch";
                        }

                        // if a method indicates that the field is optional and therefore valid,
                        // don't mark it as valid when there are no other rules
                        if (result == "dependency-mismatch") {
                            dependencyMismatch = true;
                            continue;
                        }
                        dependencyMismatch = false;

                        if (result == "pending") {
                            this.toHide = this.toHide.not(this.errorsFor(element));
                            return;
                        }

                        if (!result) {
                            this.formatAndAdd(element, rule);
                            return false;
                        }
                    } catch (e) {
                        this.settings.debug && window.console && console.log("exception occured when checking element " + element.id
						 + ", check the '" + rule.method + "' method", e);
                        throw e;
                    }
                }
                if (dependencyMismatch)
                    return;
                if (this.objectLength(rules))
                    this.successList.push(element);
                return true;
            },

            // return the custom message for the given element and validation method
            // specified in the element's "messages" metadata
            customMetaMessage: function (element, method) {
                if (!$.metadata)
                    return;

                var meta = this.settings.meta
				? $(element).metadata()[this.settings.meta]
				: $(element).metadata();

                return meta && meta.messages && meta.messages[method];
            },

            // return the custom message for the given element name and validation method
            customMessage: function (name, method) {
                var m = this.settings.messages[name];
                return m && (m.constructor == String
				? m
				: m[method]);
            },

            // return the first defined argument, allowing empty strings
            findDefined: function () {
                for (var i = 0; i < arguments.length; i++) {
                    if (arguments[i] !== undefined)
                        return arguments[i];
                }
                return undefined;
            },

            defaultMessage: function (element, method) {
                return this.findDefined(
				this.customMessage(element.name, method),
				this.customMetaMessage(element, method),
                // title is never undefined, so handle empty string as undefined
				!this.settings.ignoreTitle && element.title || undefined,
				$.validator.messages[method],
				"<strong>Warning: No message defined for " + element.name + "</strong>"
			);
            },

            formatAndAdd: function (element, rule) {
                var message = this.defaultMessage(element, rule.method),
				theregex = /\$?\{(\d+)\}/g;
                if (typeof message == "function") {
                    message = message.call(this, rule.parameters, element);
                } else if (theregex.test(message)) {
                    message = jQuery.format(message.replace(theregex, '{$1}'), rule.parameters);
                }
                this.errorList.push({
                    message: message,
                    element: element
                });

                this.errorMap[element.name] = message;
                this.submitted[element.name] = message;
            },

            addWrapper: function (toToggle) {
                if (this.settings.wrapper)
                    toToggle = toToggle.add(toToggle.parent(this.settings.wrapper));
                return toToggle;
            },

            defaultShowErrors: function () {
                for (var i = 0; this.errorList[i]; i++) {
                    var error = this.errorList[i];
                    this.settings.highlight && this.settings.highlight.call(this, error.element, this.settings.errorClass, this.settings.validClass);
                    this.showLabel(error.element, error.message);
                }
                if (this.errorList.length) {
                    this.toShow = this.toShow.add(this.containers);
                }
                if (this.settings.success) {
                    for (var i = 0; this.successList[i]; i++) {
                        this.showLabel(this.successList[i]);
                    }
                }
                if (this.settings.unhighlight) {
                    for (var i = 0, elements = this.validElements(); elements[i]; i++) {
                        this.settings.unhighlight.call(this, elements[i], this.settings.errorClass, this.settings.validClass);
                    }
                }
                this.toHide = this.toHide.not(this.toShow);
                this.hideErrors();
                this.addWrapper(this.toShow).show();
            },

            validElements: function () {
                return this.currentElements.not(this.invalidElements());
            },

            invalidElements: function () {
                return $(this.errorList).map(function () {
                    return this.element;
                });
            },

            showLabel: function (element, message) {
                var label = this.errorsFor(element);
                if (label.length) {
                    // refresh error/success class
                    label.removeClass(this.settings.validClass).addClass(this.settings.errorClass);

                    // check if we have a generated label, replace the message then
                    label.attr("generated") && label.html(message);
                } else {
                    // create label
                    //--changes added by Bharati---
                    // set width to error label
                    //
                    var tag=$(element).get(0).tagName;
                    var wid=0;
                    if(tag=="INPUT" || tag=="TEXTAREA") 
                    {
                        if($(element).attr('type')=='checkbox' || $(element).attr('type')=='radio')
                        {
                            var grp=$(element).closest('div').attr('group');
                            if(grp!=undefined || grp!='')
                                wid=(parseInt($(element).closest('div').css('width')))+10;
                            else
                                wid=(parseInt($(element).css('width')))+10;
                        }
                        else
                            wid=(parseInt($(element).css('width')))+10;
                    }
                    if(tag=="SELECT") 
                        wid=(parseInt($(element).next().css('width')));
                    if(wid<50)
                        wid=50;
                    //-- changes completed here

                    label = $("<" + this.settings.errorElement + "/>")
					.attr({ "for": this.idOrName(element), generated: true })
					.addClass(this.settings.errorClass)
					.html(message || "")
                    .css({'width':wid+'px'});
                    //alert(parseInt($(element).css('width')));
                    if (this.settings.wrapper) {
                        // make sure the element is visible, even in IE
                        // actually showing the wrapped element is handled elsewhere
                        label = label.hide().show().wrap("<" + this.settings.wrapper + "/>").parent();
                    }
                    if (!this.labelContainer.append(label).length)
                        this.settings.errorPlacement
						? this.settings.errorPlacement(label, $(element))
						: label.insertAfter(element);
                }
                if (!message && this.settings.success) {
                    label.text("");
                    typeof this.settings.success == "string"
					? label.addClass(this.settings.success)
					: this.settings.success(label);
                }
                this.toShow = this.toShow.add(label);
            },

            errorsFor: function (element) {
                var name = this.idOrName(element);
                return this.errors().filter(function () {
                    return $(this).attr('for') == name;
                });
            },

            idOrName: function (element) {
                return this.groups[element.name] || (this.checkable(element) ? element.name : element.id || element.name);
            },

            validationTargetFor: function (element) {
                // if radio/checkbox, validate first element in group instead
                if (this.checkable(element)) {
                    element = this.findByName(element.name).not(this.settings.ignore)[0];
                }
                return element;
            },

            checkable: function (element) {
                return /radio|checkbox/i.test(element.type);
            },

            findByName: function (name) {
                // select by name and filter by form for performance over form.find("[name=...]")
                var form = this.currentForm;
                return $(document.getElementsByName(name)).map(function (index, element) {
                    return element.form == form && element.name == name && element || null;
                });
            },

            getLength: function (value, element) {
                switch (element.nodeName.toLowerCase()) {
                    case 'select':
                        return $("option:selected", element).length;
                    case 'input':
                        if (this.checkable(element))
                            return this.findByName(element.name).filter(':checked').length;
                }
                return value.length;
            },

            depend: function (param, element) {
                return this.dependTypes[typeof param]
				? this.dependTypes[typeof param](param, element)
				: true;
            },

            dependTypes: {
                "boolean": function (param, element) {
                    return param;
                },
                "string": function (param, element) {
                    return !!$(param, element.form).length;
                },
                "function": function (param, element) {
                    return param(element);
                }
            },

            optional: function (element) {
                return !$.validator.methods.required.call(this, $.trim(element.value), element) && "dependency-mismatch";
            },

            startRequest: function (element) {
                if (!this.pending[element.name]) {
                    this.pendingRequest++;
                    this.pending[element.name] = true;
                }
            },

            stopRequest: function (element, valid) {
                this.pendingRequest--;
                // sometimes synchronization fails, make sure pendingRequest is never < 0
                if (this.pendingRequest < 0)
                    this.pendingRequest = 0;
                delete this.pending[element.name];
                if (valid && this.pendingRequest == 0 && this.formSubmitted && this.form()) {
                    $(this.currentForm).submit();
                    this.formSubmitted = false;
                } else if (!valid && this.pendingRequest == 0 && this.formSubmitted) {
                    $(this.currentForm).triggerHandler("invalid-form", [this]);
                    this.formSubmitted = false;
                }
            },

            previousValue: function (element) {
                return $.data(element, "previousValue") || $.data(element, "previousValue", {
                    old: null,
                    valid: true,
                    message: this.defaultMessage(element, "remote")
                });
            }

        },

        classRuleSettings: {
            required: { required: true },
            accept: { accept: true },
            xlsaccept: { xlsaccept: true },
            email: { email: true },
            emaillist: { emaillist: true },
            url: { url: true },
            date: { date: true },
            dateISO: { dateISO: true },
            dateDE: { dateDE: true },
            number: { number: true },
            numberDE: { numberDE: true },
            digits: { digits: true },
            Landline: { Landline: true },
            Contact:{ Contact: true },
            CIBIL: { CIBIL: true },
            fax:{fax:true},
            mobile:{mobile:true},
            Residential:{Residential:true},
            Telephone:{Telephone:true},
            pin: { pin: true },
            MaxMin3: { MaxMin3: true },
            creditcard: { creditcard: true },
            panCard: { panCard: true },
            TAN: { TAN: true }, // added by Bharati 14Nov2013 [Defect #7159]
            FTP: { FTP: true }
        },

        addClassRules: function (className, rules) {
            className.constructor == String ?
			this.classRuleSettings[className] = rules :
			$.extend(this.classRuleSettings, className);
        },

        classRules: function (element) {
            var rules = {};
            var classes = $(element).attr('class');
            classes && $.each(classes.split(' '), function () {
                if (this in $.validator.classRuleSettings) {
                    $.extend(rules, $.validator.classRuleSettings[this]);
                }
            });
            return rules;
        },

        attributeRules: function (element) {
            var rules = {};
            var $element = $(element);

            for (var method in $.validator.methods) {
                var value;
                // If .prop exists (jQuery >= 1.6), use it to get true/false for required
                if (method === 'required' && typeof $.fn.prop === 'function') {
                    value = $element.prop(method);
                } else {
                    value = $element.attr(method);
                }
                if (value) {
                    rules[method] = value;
                } else if ($element[0].getAttribute("type") === method) {
                    rules[method] = true;
                }
            }

            // maxlength may be returned as -1, 2147483647 (IE) and 524288 (safari) for text inputs
            if (rules.maxlength && /-1|2147483647|524288/.test(rules.maxlength)) {
                delete rules.maxlength;
            }

            return rules;
        },

        metadataRules: function (element) {
            if (!$.metadata) return {};

            var meta = $.data(element.form, 'validator').settings.meta;
            return meta ?
			$(element).metadata()[meta] :
			$(element).metadata();
        },

        staticRules: function (element) {
            var rules = {};
            var validator = $.data(element.form, 'validator');
            if (validator.settings.rules) {
                rules = $.validator.normalizeRule(validator.settings.rules[element.name]) || {};
            }
            return rules;
        },

        normalizeRules: function (rules, element) {
            // handle dependency check
            $.each(rules, function (prop, val) {
                // ignore rule when param is explicitly false, eg. required:false
                if (val === false) {
                    delete rules[prop];
                    return;
                }
                if (val.param || val.depends) {
                    var keepRule = true;
                    switch (typeof val.depends) {
                        case "string":
                            keepRule = !!$(val.depends, element.form).length;
                            break;
                        case "function":
                            keepRule = val.depends.call(element, element);
                            break;
                    }
                    if (keepRule) {
                        rules[prop] = val.param !== undefined ? val.param : true;
                    } else {
                        delete rules[prop];
                    }
                }
            });

            // evaluate parameters
            $.each(rules, function (rule, parameter) {
                rules[rule] = $.isFunction(parameter) ? parameter(element) : parameter;
            });

            // clean number parameters
            $.each(['minlength', 'maxlength', 'min', 'max'], function () {
                if (rules[this]) {
                    rules[this] = Number(rules[this]);
                }
            });
            $.each(['rangelength', 'range'], function () {
                if (rules[this]) {
                    rules[this] = [Number(rules[this][0]), Number(rules[this][1])];
                }
            });

            if ($.validator.autoCreateRanges) {
                // auto-create ranges
                if (rules.min && rules.max) {
                    rules.range = [rules.min, rules.max];
                    delete rules.min;
                    delete rules.max;
                }
                if (rules.minlength && rules.maxlength) {
                    rules.rangelength = [rules.minlength, rules.maxlength];
                    delete rules.minlength;
                    delete rules.maxlength;
                }
            }

            // To support custom messages in metadata ignore rule methods titled "messages"
            if (rules.messages) {
                delete rules.messages;
            }

            return rules;
        },

        // Converts a simple string to a {string: true} rule, e.g., "required" to {required:true}
        normalizeRule: function (data) {
            if (typeof data == "string") {
                var transformed = {};
                $.each(data.split(/\s/), function () {
                    transformed[this] = true;
                });
                data = transformed;
            }
            return data;
        },

        // http://docs.jquery.com/Plugins/Validation/Validator/addMethod
        addMethod: function (name, method, message) {
            $.validator.methods[name] = method;
            $.validator.messages[name] = message != undefined ? message : $.validator.messages[name];
            if (method.length < 3) {
                $.validator.addClassRules(name, $.validator.normalizeRule(name));
            }
        },

        methods: {

            // http://docs.jquery.com/Plugins/Validation/Methods/required
            required: function (value, element, param) {
                // check if dependency is met
                if (!this.depend(param, element))
                    return "dependency-mismatch";
                switch (element.nodeName.toLowerCase()) {
                    case 'select':
                        // could be an array for select-multiple or a string, both are fine this way
                        //var val = $(element).val();
                        //	return val && val.length > 0;
                        return $(element).val() != "0";
                    case 'input':
                        if (this.checkable(element))
                            return this.getLength(value, element) > 0;
                    default:
                        return $.trim(value).length > 0;
                }
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/remote
            remote: function (value, element, param) {
                if (this.optional(element))
                    return "dependency-mismatch";

                var previous = this.previousValue(element);
                if (!this.settings.messages[element.name])
                    this.settings.messages[element.name] = {};
                previous.originalMessage = this.settings.messages[element.name].remote;
                this.settings.messages[element.name].remote = previous.message;

                param = typeof param == "string" && { url: param} || param;

                if (this.pending[element.name]) {
                    return "pending";
                }
                if (previous.old === value) {
                    return previous.valid;
                }

                previous.old = value;
                var validator = this;
                this.startRequest(element);
                var data = {};
                data[element.name] = value;
                $.ajax($.extend(true, {
                    url: param,
                    mode: "abort",
                    port: "validate" + element.name,
                    dataType: "json",
                    data: data,
                    success: function (response) {
                        validator.settings.messages[element.name].remote = previous.originalMessage;
                        var valid = response === true;
                        if (valid) {
                            var submitted = validator.formSubmitted;
                            validator.prepareElement(element);
                            validator.formSubmitted = submitted;
                            validator.successList.push(element);
                            validator.showErrors();
                        } else {
                            var errors = {};
                            var message = response || validator.defaultMessage(element, "remote");
                            errors[element.name] = previous.message = $.isFunction(message) ? message(value) : message;
                            validator.showErrors(errors);
                        }
                        previous.valid = valid;
                        validator.stopRequest(element, valid);
                    }
                }, param));
                return "pending";
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/minlength
            minlength: function (value, element, param) {
                return this.optional(element) || this.getLength($.trim(value), element) >= param;
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/maxlength
            maxlength: function (value, element, param) {
                return this.optional(element) || this.getLength($.trim(value), element) <= param;
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/rangelength
            rangelength: function (value, element, param) {
                var length = this.getLength($.trim(value), element);
                return this.optional(element) || (length >= param[0] && length <= param[1]);
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/min
            min: function (value, element, param) {
                return this.optional(element) || value >= param;
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/max 
            max: function (value, element, param) {
                return this.optional(element) || value <= param;
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/range
            range: function (value, element, param) {
                return this.optional(element) || (value >= param[0] && value <= param[1]);
            },

          
            // http://docs.jquery.com/Plugins/Validation/Methods/email
            email: function (value, element) {
                // contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
                return this.optional(element) || /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(value);
            },
            emaillist: function (value, element) {
                if (this.optional(element)) {
                    return 1;
                }
                else {
                    var count = 0;
                    var mySplitResult = value.split(",");
               
                    for (i = 0; i < mySplitResult.length; i++) {
                        var Result = $.trim(mySplitResult[i]);
                        count++;
                        if (!/^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)|(['\&quot;][^\<\>'\&quot;]*['\&quot;]\s*\<\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\>))(,\s*((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)|(['\&quot;][^\<\>'\&quot;]*['\&quot;]\s*\<\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\>)))*$/i.test(Result)) {
                       // if (!/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(Result)) {
                            return 0;
                        }
                    }
                    if (count == mySplitResult.length) {
                        return 1;
                    }
                }
             //   return this.optional(element) || /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(value);
            },
            // http://docs.jquery.com/Plugins/Validation/Methods/url
            url: function (value, element) {
                // contributed by Scott Gonzalez: http://projects.scottsplayground.com/iri/
                //   return this.optional(element) || /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
         //       return this.optional(element) || /^((ftp|http|https):\/\/)?(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
          //      return this.optional(element) || /^(([ftp|http|https|FTP|HTTPS|HTTP]+:\/\/))?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([#-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/i.test(value);
             //     return this.optional(element) || /^(([ftp|http|https|FTP|HTTPS|HTTP]+:\/\/))?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([#-+_~-.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~-.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~-.\d\w]|%[a-fA-f\d]{2,2})*)?$/i.test(value);
                 return this.optional(element) || /^(([ftp|http|https|FTP|HTTPS|HTTP]+:\/\/))?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([#+-_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/i.test(value);
   
            },
            FTP: function (value, element) {
                return this.optional(element) || /^(([ftp|FTP|]+:\/\/))?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([#+-_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/i.test(value);   
            },
            // http://docs.jquery.com/Plugins/Validation/Methods/date
            date: function (value, element) {
                return this.optional(element) || !/Invalid|NaN/.test(new Date(value));
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/dateISO
            dateISO: function (value, element) {
                return this.optional(element) || /^\d{4}[\/-]\d{1,2}[\/-]\d{1,2}$/.test(value);
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/number
            number: function (value, element) {
                return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);
            },

            Landline: function (value, element) {

                return  this.optional(element) || (value.length == 10 && /^\d+$/.test(value) );
	         //   return this.optional(element) || (value.length > 9 && value.length < 20 && /^[\(\)\.\- ]{0,}[0-9]{2,6}[\(\)\.\- ]{0,}[0-9]{2,6}[\(\)\.\- ]{0,}[0-9]{4,7}[\(\)\.\- ]{0,}$/.test(value));
            },
             Contact: function (value, element) {
                return  this.optional(element) || (value.length == 10 && /^\d+$/.test(value) );
	        },
            CIBIL: function (value, element) {
                return this.optional(element) || value == -1 || value == 1 || (value >= 300 && value <= 900);
            },

            fax: function (value, element) {
                return  this.optional(element) || (value.length == 10  && /^\d+$/.test(value) );
           //   return this.optional(element) || (value.length > 9 && value.length < 20 && /^[\(\)\.\- ]{0,}[0-9]{2,6}[\(\)\.\- ]{0,}[0-9]{2,6}[\(\)\.\- ]{0,}[0-9]{4,7}[\(\)\.\- ]{0,}$/.test(value));
            },
            mobile: function (value, element) {
                
                return this.optional(element) || (value.length == 10 && value.substring(0,1) != 0 && /^\d+$/.test(value));
            },
            
            Residential: function (value, element) {
                
                return  this.optional(element) ||  (value.length == 10 && /^\d+$/.test(value) );
            },

            Telephone: function (value, element) {
                
                return  this.optional(element) ||  (value.length == 10 && /^\d+$/.test(value) );
            },

            pin: function (value, element) {

                return  value > 99999;
            },
             MaxMin3: function (value, element) {
               
                return value.length = 3;
            },
            // http://docs.jquery.com/Plugins/Validation/Methods/digits
            digits: function (value, element) {
                return this.optional(element) || /^\d+$/.test(value);
            },
            panCard: function (value, element) {
                return value.length == 10;
            },
            TAN: function (value, element) { // added by Bharati 14Nov2013 [Defect #7159]
                return value.length == 10;
            },
            
            // http://docs.jquery.com/Plugins/Validation/Methods/creditcard
            // based on http://en.wikipedia.org/wiki/Luhn
            creditcard: function (value, element) {
                if (this.optional(element))
                    return "dependency-mismatch";
                // accept only spaces, digits and dashes
                if (/[^0-9 -]+/.test(value))
                    return false;
                var nCheck = 0,
				nDigit = 0,
				bEven = false;

                value = value.replace(/\D/g, "");

                for (var n = value.length - 1; n >= 0; n--) {
                    var cDigit = value.charAt(n);
                    var nDigit = parseInt(cDigit, 10);
                    if (bEven) {
                        if ((nDigit *= 2) > 9)
                            nDigit -= 9;
                    }
                    nCheck += nDigit;
                    bEven = !bEven;
                }

                return (nCheck % 10) == 0;
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/accept
            accept: function (value, element, param) {
                param = typeof param == "string" ? param.replace(/,/g, '|') : "xls|csv";
                return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
            },
            xlsaccept: function (value, element, param) {
                param = typeof param == "string" ? param.replace(/,/g, '|') : "xls|csv";
                return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
            },
            // http://docs.jquery.com/Plugins/Validation/Methods/equalTo
            equalTo: function (value, element, param) {
                // bind to the blur event of the target in order to revalidate whenever the target field is updated
                // TODO find a way to bind the event just once, avoiding the unbind-rebind overhead
                var target = $(param).unbind(".validate-equalTo").bind("blur.validate-equalTo", function () {
                    $(element).valid();
                });
                return value == target.val();
            }

        }

    });

    // deprecated, use $.validator.format instead
    $.format = $.validator.format;

})(jQuery);

// ajax mode: abort
// usage: $.ajax({ mode: "abort"[, port: "uniqueport"]});
// if mode:"abort" is used, the previous request on that port (port can be undefined) is aborted via XMLHttpRequest.abort()
; (function ($) {
    var pendingRequests = {};
    // Use a prefilter if available (1.5+)
    if ($.ajaxPrefilter) {
        $.ajaxPrefilter(function (settings, _, xhr) {
            var port = settings.port;
            if (settings.mode == "abort") {
                if (pendingRequests[port]) {
                    pendingRequests[port].abort();
                }
                pendingRequests[port] = xhr;
            }
        });
    } else {
        // Proxy ajax
        var ajax = $.ajax;
        $.ajax = function (settings) {
            var mode = ("mode" in settings ? settings : $.ajaxSettings).mode,
				port = ("port" in settings ? settings : $.ajaxSettings).port;
            if (mode == "abort") {
                if (pendingRequests[port]) {
                    pendingRequests[port].abort();
                }
                return (pendingRequests[port] = ajax.apply(this, arguments));
            }
            return ajax.apply(this, arguments);
        };
    }
})(jQuery);

// provides cross-browser focusin and focusout events
// IE has native support, in other browsers, use event caputuring (neither bubbles)

// provides delegate(type: String, delegate: Selector, handler: Callback) plugin for easier event delegation
// handler is only called when $(event.target).is(delegate), in the scope of the jquery-object for event.target
; (function ($) {
    // only implement if not provided by jQuery core (since 1.4)
    // TODO verify if jQuery 1.4's implementation is compatible with older jQuery special-event APIs
    if (!jQuery.event.special.focusin && !jQuery.event.special.focusout && document.addEventListener) {
        $.each({
            focus: 'focusin',
            blur: 'focusout'
        }, function (original, fix) {
            $.event.special[fix] = {
                setup: function () {
                    this.addEventListener(original, handler, true);
                },
                teardown: function () {
                    this.removeEventListener(original, handler, true);
                },
                handler: function (e) {
                    arguments[0] = $.event.fix(e);
                    arguments[0].type = fix;
                    return $.event.handle.apply(this, arguments);
                }
            };
            function handler(e) {
                e = $.event.fix(e);
                e.type = fix;
                return $.event.handle.call(this, e);
            }
        });
    };
    $.extend($.fn, {
        validateDelegate: function (delegate, type, handler) {
            return this.bind(type, function (event) {
                var target = $(event.target);
                if (target.is(delegate)) {
                    return handler.apply(target, arguments);
                }
            });
        }
    });
})(jQuery);

 
 /*
 * elrte.min.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 */



 function eli18n(a){this.textdomain=function(b){return this.messages[b]?this._domain=b:this._domain};a&&a.messages&&this.load(a.messages);a&&a.textdomain&&this.textdomain(a.textdomain)}eli18n.prototype=new function(){this.messages={};this._domain="";this.load=function(c){if(typeof(c)=="object"){for(var f in c){var b=c[f];if(typeof(b)=="object"){if(!this.messages[f]){this.messages[f]={}}for(var a in b){if(typeof(b[a])=="string"){this.messages[f][a]=b[a]}}}}}return this};this.translate=function(b,a){var a=a&&this.messages[a]?a:this._domain;
return this.messages[a]&&this.messages[a][b]?this.messages[a][b]:b};this.format=function(f,b,c){f=this.translate(f,c);if(typeof(b)=="object"){for(var a in b){f=f.replace("%"+a,this.translate(b[a],c))}}return f}};function elDialogForm(c){var a=this;var b={"class":"el-dialogform",submit:function(f,g){g.close()},form:{action:window.location.href,method:"post"},ajaxForm:null,validate:null,spinner:"Loading",tabs:{active:0,selected:0},tabPrefix:"el-df-tab-",dialog:{title:"dialog",autoOpen:false,modal:true,resizable:false,closeOnEscape:true,buttons:{Cancel:function(){a.close()
},Ok:function(){a.form.trigger("submit")}}}};this.opts=jQuery.extend(true,{},b,c);this.opts.dialog.close=function(){a.close()};if(this.opts.rtl){this.opts["class"]+=" el-dialogform-rtl"}if(c&&c.dialog&&c.dialog.buttons&&typeof(c.dialog.buttons)=="object"){this.opts.dialog.buttons=c.dialog.buttons}this.ul=null;this.tabs={};this._table=null;this.dialog=jQuery("<div />").addClass(this.opts["class"]).dialog(this.opts.dialog);this.message=jQuery('<div class="el-dialogform-message rounded-5" />').hide().appendTo(this.dialog);
this.error=jQuery('<div class="el-dialogform-error rounded-5" />').hide().appendTo(this.dialog);this.spinner=jQuery('<div class="spinner" />').hide().appendTo(this.dialog);this.content=jQuery('<div class="el-dialogform-content" />').appendTo(this.dialog);this.form=jQuery("<form />").attr(this.opts.form).appendTo(this.content);if(this.opts.submit){this.form.bind("submit",function(f){a.opts.submit(f,a)})}if(this.opts.ajaxForm&&jQuery.fn.ajaxForm){this.form.ajaxForm(this.opts.ajaxForm)}if(this.opts.validate){this.form.validate(this.opts.validate)
}this.option=function(f,g){return this.dialog.dialog("option",f,g)};this.showError=function(f,g){this.hideMessage();this.hideSpinner();this.error.html(f).show();g&&this.content.hide();return this};this.hideError=function(){this.error.text("").hide();this.content.show();return this};this.showSpinner=function(f){this.error.hide();this.message.hide();this.content.hide();this.spinner.text(f||this.opts.spinner).show();this.option("buttons",{});return this};this.hideSpinner=function(){this.content.show();
this.spinner.hide();return this};this.showMessage=function(f,g){this.hideError();this.hideSpinner();this.message.html(f||"").show();g&&this.content.hide();return this};this.hideMessage=function(){this.message.hide();this.content.show();return this};this.tab=function(g,f){g=this.opts.tabPrefix+g;if(!this.ul){this.ul=jQuery("<ul />").prependTo(this.form)}jQuery("<li />").append(jQuery("<a />").attr("href","#"+g).html(f)).appendTo(this.ul);this.tabs[g]={tab:jQuery("<div />").attr("id",g).addClass("tab").appendTo(this.form),table:null};
return this};this.table=function(f){f=f&&f.indexOf(this.opts.tabPrefix)==-1?this.opts.tabPrefix+f:f;if(f&&this.tabs&&this.tabs[f]){this.tabs[f].table=jQuery("<table />").appendTo(this.tabs[f].tab)}else{this._table=jQuery("<table />").appendTo(this.form)}return this};this.append=function(j,k,g){k=k?"el-df-tab-"+k:"";if(!j){return this}if(k&&this.tabs[k]){if(g){!this.tabs[k].table&&this.table(k);var h=jQuery("<tr />").appendTo(this.tabs[k].table);if(!jQuery.isArray(j)){h.append(jQuery("<td />").append(j))
}else{for(var f=0;f<j.length;f++){h.append(jQuery("<td />").append(j[f]))}}}else{if(!jQuery.isArray(j)){this.tabs[k].tab.append(j)}else{for(var f=0;f<j.length;f++){this.tabs[k].tab.append(j[f])}}}}else{if(!g){if(!jQuery.isArray(j)){this.form.append(j)}else{for(var f=0;f<j.length;f++){this.form.append(j[f])}}}else{if(!this._table){this.table()}var h=jQuery("<tr />").appendTo(this._table);if(!jQuery.isArray(j)){h.append(jQuery("<td />").append(j))}else{for(var f=0;f<j.length;f++){h.append(jQuery("<td />").append(j[f]))
}}}}return this};this.separator=function(f){f="el-df-tab-"+f;if(this.tabs&&this.tabs[f]){this.tabs[f].tab.append(jQuery("<div />").addClass("separator"));this.tabs[f].table&&this.table(f)}else{this.form.append(jQuery("<div />").addClass("separator"))}return this};this.open=function(){var f=this;this.ul&&this.form.tabs(this.opts.tabs);setTimeout(function(){f.dialog.find(":text").keydown(function(g){if(g.keyCode==13){g.preventDefault();f.form.submit()}}).filter(":first")[0].focus()},200);this.dialog.dialog("open");
return this};this.close=function(){if(typeof(this.opts.close)=="function"){this.opts.close()}this.dialog.dialog("destroy")}}(function(a){a.fn.elColorPicker=function(h){var b=this;var f=a.extend({},a.fn.elColorPicker.defaults,h);this.hidden=a('<input type="hidden" />').attr("name",f.name).val(f.color||"").appendTo(this);this.palette=null;this.preview=null;this.input=null;function c(j){b.val(j);f.change&&f.change(b.val());b.palette.slideUp()}function g(){b.palette=a("<div />").addClass(f.paletteClass+" rounded-3");
for(var j=0;j<f.colors.length;j++){a("<div />").addClass("color").css("background-color",f.colors[j]).attr({title:f.colors[j],unselectable:"on"}).appendTo(b.palette).mouseenter(function(){var k=a(this).attr("title");b.input.val(k);b.preview.css("background-color",k)}).click(function(k){k.stopPropagation();c(a(this).attr("title"))})}b.input=a('<input type="text" />').addClass("rounded-3").attr("size",8).click(function(k){k.stopPropagation();a(this).focus()}).keydown(function(p){if(p.ctrlKey||p.metaKey){return true
}var o=p.keyCode;if(o==27){return b.mouseleave()}if(o!=8&&o!=13&&o!=46&&o!=37&&o!=39&&(o<48||o>57)&&(o<65||o>70)){return false}var q=a(this).val();if(q.length==7||q.length==0){if(o==13){p.stopPropagation();p.preventDefault();c(q);b.palette.slideUp()}if(p.keyCode!=8&&p.keyCode!=46&&o!=37&&o!=39){return false}}}).keyup(function(k){var o=a(this).val();o.length==7&&/^#[0-9abcdef]{6}$/i.test(o)&&b.val(o)});b.preview=a("<div />").addClass("preview rounded-3").click(function(k){k.stopPropagation();c(b.input.val())
});b.palette.append(a("<div />").addClass("clearfix")).append(a("<div />").addClass("panel").append(b.input).append(b.preview));if(f.palettePosition=="outer"){b.palette.hide().appendTo(b.parents("body").eq(0)).mouseleave(function(){if(!b.palette.is(":animated")){a(this).slideUp();b.val(b.val())}});b.mouseleave(function(k){if(k.relatedTarget!=b.palette.get(0)){if(!b.palette.is(":animated")){b.palette.slideUp();b.val(b.val())}}})}else{b.append(b.palette.hide()).mouseleave(function(k){b.palette.slideUp();
b.val(b.val())})}b.val(b.val())}this.empty().addClass(f["class"]+" rounded-3").css({position:"relative","background-color":f.color||""}).click(function(p){if(!b.hasClass("disabled")){!b.palette&&g();if(f.palettePosition=="outer"&&b.palette.css("display")=="none"){var q=a(this).offset();var k=b.palette.width();var j=b.parents("body").width()-q.left>=k?q.left:q.left+a(this).outerWidth()-k;b.palette.css({left:j+"px",top:q.top+a(this).height()+1+"px"})}b.palette.slideToggle()}});this.val=function(j){if(!j&&j!==""){return this.hidden.val()
}else{this.hidden.val(j);if(f.update){f.update(this.hidden.val())}else{this.css("background-color",j)}if(b.palette){b.preview.css("background-color",j);b.input.val(j)}}return this};return this};a.fn.elColorPicker.defaults={"class":"el-colorpicker",paletteClass:"el-palette",palettePosition:"inner",name:"color",color:"",update:null,change:function(b){},colors:["#ffffff","#cccccc","#999999","#666666","#333333","#000000","#ffcccc","#cc9999","#996666","#663333","#330000","#ff9999","#cc6666","#cc3333","#993333","#660000","#ff6666","#ff3333","#ff0000","#cc0000","#990000","#ff9966","#ff6633","#ff3300","#cc3300","#993300","#ffcc99","#cc9966","#cc6633","#996633","#663300","#ff9933","#ff6600","#ff9900","#cc6600","#cc9933","#ffcc66","#ffcc33","#ffcc00","#cc9900","#996600","#ffffcc","#cccc99","#999966","#666633","#333300","#ffff99","#cccc66","#cccc33","#999933","#666600","#ffff66","#ffff33","#ffff00","#cccc00","#999900","#ccff66","#ccff33","#ccff00","#99cc00","#669900","#ccff99","#99cc66","#99cc33","#669933","#336600","#99ff33","#99ff00","#66ff00","#66cc00","#66cc33","#99ff66","#66ff33","#33ff00","#33cc00","#339900","#ccffcc","#99cc99","#669966","#336633","#003300","#99ff99","#66cc66","#33cc33","#339933","#006600","#66ff66","#33ff33","#00ff00","#00cc00","#009900","#66ff99","#33ff66","#00ff33","#00cc33","#009933","#99ffcc","#66cc99","#33cc66","#339966","#006633","#33ff99","#00ff66","#00ff99","#00cc66","#33cc99","#66ffcc","#33ffcc","#00ffcc","#00cc99","#009966","#ccffff","#99cccc","#669999","#336666","#003333","#99ffff","#66cccc","#33cccc","#339999","#006666","#66cccc","#33ffff","#00ffff","#00cccc","#009999","#66ccff","#33ccff","#00ccff","#0099cc","#006699","#99ccff","#6699cc","#3399cc","#336699","#003366","#3399ff","#0099ff","#0066ff","#066ccc","#3366cc","#6699ff","#3366ff","#0033ff","#0033cc","#003399","#ccccff","#9999cc","#666699","#333366","#000033","#9999ff","#6666cc","#3333cc","#333399","#000066","#6666ff","#3333ff","#0000ff","#0000cc","#009999","#9966ff","#6633ff","#3300ff","#3300cc","#330099","#cc99ff","#9966cc","#6633cc","#663399","#330066","#9933ff","#6600ff","#9900ff","#6600cc","#9933cc","#cc66ff","#cc33ff","#cc00ff","#9900cc","#660099","#ffccff","#cc99cc","#996699","#663366","#330033","#ff99ff","#cc66cc","#cc33cc","#993399","#660066","#ff66ff","#ff33ff","#ff00ff","#cc00cc","#990099","#ff66cc","#ff33cc","#ff00cc","#cc0099","#990066","#ff99cc","#cc6699","#cc3399","#993366","#660033","#ff3399","#ff0099","#ff0066","#cc0066","#cc3366","#ff6699","#ff3366","#ff0033","#cc0033","#990033"]}
})(jQuery);(function(a){a.fn.elBorderSelect=function(h){var k=this;var q=this.eq(0);var b=a.extend({},a.fn.elBorderSelect.defaults,h);var f=a('<input type="text" />').attr({name:b.name+"[width]",size:3}).css("text-align","right").change(function(){k.change()});var j=a("<div />").css("position","relative").elColorPicker({"class":"el-colorpicker ui-icon ui-icon-pencil",name:b.name+"[color]",palettePosition:"outer",change:function(){k.change()}});var c=a("<div />").elSelect({tpl:'<div style="border-bottom:4px %val #000;width:100%;margin:7px 0"> </div>',tpls:{"":"%label"},maxHeight:b.styleHeight||null,select:function(){k.change()
},src:{"":"none",solid:"solid",dashed:"dashed",dotted:"dotted","double":"double",groove:"groove",ridge:"ridge",inset:"inset",outset:"outset"}});q.empty().addClass(b["class"]).attr("name",b.name||"").append(a("<table />").attr("cellspacing",0).append(a("<tr />").append(a("<td />").append(f).append(" px")).append(a("<td />").append(c)).append(a("<td />").append(j))));function g(t){function r(u){hexDigits=["0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"];return !u?"00":hexDigits[(u-u%16)/16]+hexDigits[u%16]
}var o=(t||"").match(/\(([0-9]{1,3}),\s*([0-9]{1,3}),\s*([0-9]{1,3})\)/);return o?"#"+r(o[1])+r(o[2])+r(o[3]):""}function p(r){if(!r){return r}var o=r.match(/([0-9]+\.?[0-9]*)\s*(px|pt|em|%)/);if(o){r=o[1];unit=o[2]}if(r[0]=="."){r="0"+r}r=parseFloat(r);if(isNaN(r)){return""}var t=parseInt(a(document.body).css("font-size"))||16;switch(unit){case"em":return parseInt(r*t);case"pt":return parseInt(r*t/12);case"%":return parseInt(r*t/100)}return r}this.change=function(){b.change&&b.change(this.val())
};this.val=function(u){var t,x,z,r,o;if(!u&&u!==""){t=parseInt(f.val());t=!isNaN(t)?t+"px":"";x=c.val();z=j.val();return{width:t,style:x,color:z,css:a.trim(t+" "+x+" "+z)}}else{r="";if(u.nodeName||u.css){if(!u.css){u=a(u)}r=u.css("border");if((r=u.css("border"))){t=x=z=r}else{t=u.css("border-width");x=u.css("border-style");z=u.css("border-color")}}else{t=u.width||"";x=u.style||"";z=u.color||""}f.val(p(t));o=x?x.match(/(solid|dashed|dotted|double|groove|ridge|inset|outset)/i):"";c.val(o?o[1]:"");j.val(z.indexOf("#")===0?z:g(z));
return this}};this.val(b.value);return this};a.fn.elBorderSelect.defaults={name:"el-borderselect","class":"el-borderselect",value:{},change:null}})(jQuery);(function(a){a.fn.elPaddingInput=function(g){var c=this;var f=a.extend({},a.fn.elPaddingInput.defaults,{name:this.attr("name")},g);this.regexps={main:new RegExp(f.type=="padding"?'paddings*:s*([^;"]+)':'margins*:s*([^;"]+)',"im"),left:new RegExp(f.type=="padding"?'padding-lefts*:s*([^;"]+)':'margin-lefts*:s*([^;"]+)',"im"),top:new RegExp(f.type=="padding"?'padding-tops*:s*([^;"]+)':'margin-tops*:s*([^;"]+)',"im"),right:new RegExp(f.type=="padding"?'padding-rights*:s*([^;"]+)':'margin-rights*:s*([^;"]+)',"im"),bottom:new RegExp(f.type=="padding"?'padding-bottoms*:s*([^;"]+)':'margin-bottoms*:s*([^;"]+)',"im")};
a.each(["left","top","right","bottom"],function(){c[this]=a('<input type="text" />').attr("size",3).css("text-align","right").css("border-"+this,"2px solid red").bind("change",function(){a(this).val(b(a(this).val()));h()}).attr("name",f.name+"["+this+"]")});a.each(["uleft","utop","uright","ubottom"],function(){c[this]=a("<select />").append('<option value="px">px</option>').append('<option value="em">em</option>').append('<option value="pt">pt</option>').bind("change",function(){h()}).attr("name",f.name+"["+this+"]");
if(f.percents){c[this].append('<option value="%">%</option>')}});this.empty().addClass(f["class"]).append(this.left).append(this.uleft).append(" x ").append(this.top).append(this.utop).append(" x ").append(this.right).append(this.uright).append(" x ").append(this.bottom).append(this.ubottom);this.val=function(z){if(!z&&z!==""){var q=b(this.left.val());var A=b(this.top.val());var j=b(this.right.val());var x=b(this.bottom.val());var w={left:q=="auto"||q==0?q:(q!==""?q+this.uleft.val():""),top:A=="auto"||A==0?A:(A!==""?A+this.utop.val():""),right:j=="auto"||j==0?j:(j!==""?j+this.uright.val():""),bottom:x=="auto"||x==0?x:(x!==""?x+this.ubottom.val():""),css:""};
if(w.left!==""&&w.right!==""&&w.top!==""&&w.bottom!==""){if(w.left==w.right&&w.top==w.bottom){w.css=w.top+" "+w.left}else{w.css=w.top+" "+w.right+" "+w.bottom+" "+w.left}}return w}else{if(z.nodeName||z.css){if(!z.css){z=a(z)}var o={left:"",top:"",right:"",bottom:""};var k=(z.attr("style")||"").toLowerCase();if(k){k=a.trim(k);var p=k.match(this.regexps.main);if(p){var u=a.trim(p[1]).replace(/\s+/g," ").split(" ",4);o.top=u[0];o.right=u[1]&&u[1]!==""?u[1]:o.top;o.bottom=u[2]&&u[2]!==""?u[2]:o.top;o.left=u[3]&&u[3]!==""?u[3]:o.right
}else{a.each(["left","top","right","bottom"],function(){var r=this.toString();p=k.match(c.regexps[r]);if(p){o[r]=p[1]}})}}var z=o}a.each(["left","top","right","bottom"],function(){var t=this.toString();c[t].val("");c["u"+t].val();if(typeof(z[t])!="undefined"&&z[t]!==null){z[t]=z[t].toString();var v=b(z[t]);c[t].val(v);var r=z[t].match(/(px|em|pt|%)/i);c["u"+t].val(r?r[1]:"px")}});return this}};function b(j){j=a.trim(j.toString());if(j[0]=="."){j="0"+j}n=parseFloat(j);return !isNaN(n)?n:(j=="auto"?j:"")
}function h(){f.change&&f.change(c)}this.val(f.value);return this};a.fn.elPaddingInput.defaults={name:"el-paddinginput","class":"el-paddinginput",type:"padding",value:{},percents:true,change:null}})(jQuery);(function(a){a.fn.elSelect=function(c){var q=this;var u=this.eq(0);var b=a.extend({},a.fn.elSelect.defaults,c);var g=a('<input type="hidden" />').attr("name",b.name);var p=a("<label />").attr({unselectable:"on"}).addClass("rounded-left-3");var h=null;var k=null;if(u.get(0).nodeName=="SELECT"){b.src={};
u.children("option").each(function(){b.src[a(this).val()]=a(this).text()});b.value=u.val();b.name=u.attr("name");u.replaceWith((u=a("<div />")))}if(!b.value||!b.src[b.val]){b.value=null;var f=0;for(var r in b.src){if(f++==0){b.value=r}}}this.val=function(o){if(!o&&o!==""){return g.val()}else{if(b.src[o]){g.val(o);j(o);if(h){h.children().each(function(){if(a(this).attr("name")==o){a(this).addClass("active")}else{a(this).removeClass("active")}})}}return this}};function j(o){var w=b.labelTpl||b.tpls[o]||b.tpl;
p.html(w.replace(/%val/g,o).replace(/%label/,b.src[o])).children().attr({unselectable:"on"})}u.empty().addClass(b["class"]+" rounded-3").attr({unselectable:"on"}).append(g).append(p).hover(function(){a(this).addClass("hover")},function(){a(this).removeClass("hover")}).click(function(o){!h&&t();h.slideToggle();if(a.browser.msie&&!k){h.children().each(function(){k=Math.max(k,a(this).width())});if(k>h.width()){h.width(k+40)}}});this.val(b.value);function t(){h=a("<div />").addClass(b.listClass+" rounded-3").hide().appendTo(u.mouseleave(function(v){h.slideUp()
}));for(var x in b.src){var z=b.tpls[x]||b.tpl;a("<div />").attr("name",x).append(a(z.replace(/%val/g,x).replace(/%label/g,b.src[x])).attr({unselectable:"on"})).appendTo(h).hover(function(){a(this).addClass("hover")},function(){a(this).removeClass("hover")}).click(function(B){B.stopPropagation();B.preventDefault();var w=a(this).attr("name");q.val(w);b.select(w);h.slideUp()})}var o=u.outerWidth();if(h.width()<o){h.width(o)}var A=h.height();if(b.maxHeight>0&&A>b.maxHeight){h.height(b.maxHeight)}q.val(g.val())
}return this};a.fn.elSelect.defaults={name:"el-select","class":"el-select",listClass:"list",labelTpl:null,tpl:"<%val>%label</%val>",tpls:{},value:null,src:{},select:function(b){window.console&&window.console.log&&window.console.log("selected: "+b)},maxHeight:410}})(jQuery);(function(a){elRTE=function(o,j){if(!o||!o.nodeName){return alert('elRTE: argument "target" is not DOM Element')}var c=this,f;this.version="1.3";this.build="2011-06-23";this.options=a.extend(true,{},this.options,j);this.browser=a.browser;
this.target=a(o);this.lang=(""+this.options.lang);this._i18n=new eli18n({textdomain:"rte",messages:{rte:this.i18Messages[this.lang]||{}}});this.rtl=!!(/^(ar|fa|he)$/.test(this.lang)&&this.i18Messages[this.lang]);if(this.rtl){this.options.cssClass+=" el-rte-rtl"}this.toolbar=a('<div class="toolbar"/>');this.iframe=document.createElement("iframe");this.iframe.setAttribute("frameborder",0);this.workzone=a('<div class="workzone"/>').append(this.iframe).append(this.source);this.statusbar=a('<div class="statusbar"/>');
this.tabsbar=a('<div class="tabsbar"/>');this.editor=a('<div class="'+this.options.cssClass+'" />').append(this.toolbar).append(this.workzone).append(this.statusbar).append(this.tabsbar);this.doc=null;this.$doc=null;this.window=null;this.utils=new this.utils(this);this.dom=new this.dom(this);this.filter=new this.filter(this);this.updateHeight=function(){c.workzone.add(c.iframe).add(c.source).height(c.workzone.height())};this.resizable=function(q){var p=this;if(this.options.resizable&&a.fn.resizable){if(q){this.editor.resizable({handles:"se",alsoResize:this.workzone,minWidth:300,minHeight:200}).bind("resize",p.updateHeight)
}else{this.editor.resizable("destroy").unbind("resize",p.updateHeight)}}};this.editor.insertAfter(o);var h="";if(o.nodeName=="TEXTAREA"){this.source=this.target;this.source.insertAfter(this.iframe).hide();h=this.target.val()}else{this.source=a("<textarea />").insertAfter(this.iframe).hide();h=this.target.hide().html()}this.source.attr("name",this.target.attr("name")||this.target.attr("id"));h=a.trim(h);if(!h){h=" "}if(this.options.allowSource){this.tabsbar.append('<div class="tab editor rounded-bottom-7 active">'+c.i18n("Editor")+'</div><div class="tab source rounded-bottom-7">'+c.i18n("Source")+'</div><div class="clearfix" style="clear:both"/>').children(".tab").click(function(p){if(!a(this).hasClass("active")){c.tabsbar.children(".tab").toggleClass("active");
c.workzone.children().toggle();if(a(this).hasClass("editor")){c.updateEditor();c.window.focus();c.ui.update(true)}else{c.updateSource();c.source.focus();if(a.browser.msie){}else{c.source[0].setSelectionRange(0,0)}c.ui.disable();c.statusbar.empty()}}})}this.window=this.iframe.contentWindow;this.doc=this.iframe.contentWindow.document;this.$doc=a(this.doc);f='<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';a.each(c.options.cssfiles,function(){f+='<link rel="stylesheet" type="text/css" href="'+this+'" />'
});this.doc.open();var g=this.filter.wysiwyg(h),b=this.rtl?' class="el-rte-rtl"':"";this.doc.write(c.options.doctype+f+"</head><body"+b+">"+(g)+"</body></html>");this.doc.close();if(a.browser.msie){this.doc.body.contentEditable=true}else{try{this.doc.designMode="on"}catch(k){}this.doc.execCommand("styleWithCSS",false,this.options.styleWithCSS)}if(this.options.height>0){this.workzone.height(this.options.height)}if(this.options.width>0){this.editor.width(this.options.width)}this.updateHeight();this.resizable(true);
this.window.focus();this.history=new this.history(this);this.selection=new this.selection(this);this.ui=new this.ui(this);this.target.parents("form").bind("submit.elfinder",function(p){c.source.parents("form").find('[name="el-select"]').remove();c.beforeSave()});this.source.bind("keydown",function(t){if(t.keyCode==9){t.preventDefault();if(a.browser.msie){var p=document.selection.createRange();p.text="\t"+p.text;this.focus()}else{var q=this.value.substr(0,this.selectionStart),u=this.value.substr(this.selectionEnd);
this.value=q+"\t"+u;this.setSelectionRange(q.length+1,q.length+1)}}});a(this.doc.body).bind("dragend",function(p){setTimeout(function(){try{c.window.focus();var r=c.selection.getBookmark();c.selection.moveToBookmark(r);c.ui.update()}catch(q){}},200)});this.typing=false;this.lastKey=null;this.$doc.bind("mouseup",function(){c.typing=false;c.lastKey=null;c.ui.update()}).bind("keyup",function(p){if((p.keyCode>=8&&p.keyCode<=13)||(p.keyCode>=32&&p.keyCode<=40)||p.keyCode==46||(p.keyCode>=96&&p.keyCode<=111)){c.ui.update()
}}).bind("keydown",function(p){if((p.metaKey||p.ctrlKey)&&p.keyCode==65){c.ui.update()}else{if(p.keyCode==13){var q=c.selection.getNode();if(c.dom.selfOrParent(q,/^PRE$/)){c.selection.insertNode(c.doc.createTextNode("\r\n"));return false}else{if(a.browser.safari&&p.shiftKey){c.selection.insertNode(c.doc.createElement("br"));return false}}}}if((p.keyCode>=48&&p.keyCode<=57)||p.keyCode==61||p.keyCode==109||(p.keyCode>=65&&p.keyCode<=90)||p.keyCode==188||p.keyCode==190||p.keyCode==191||(p.keyCode>=219&&p.keyCode<=222)){if(!c.typing){c.history.add(true)
}c.typing=true;c.lastKey=null}else{if(p.keyCode==8||p.keyCode==46||p.keyCode==32||p.keyCode==13){if(p.keyCode!=c.lastKey){c.history.add(true)}c.lastKey=p.keyCode;c.typing=false}}if(p.keyCode==32&&a.browser.opera){c.selection.insertNode(c.doc.createTextNode(" "));return false}}).bind("paste",function(q){if(!c.options.allowPaste){q.stopPropagation();q.preventDefault()}else{var t=a(c.dom.create("div"))[0],p=c.doc.createTextNode("_");c.history.add(true);c.typing=true;c.lastKey=null;t.appendChild(p);c.selection.deleteContents().insertNode(t);
c.selection.select(p);setTimeout(function(){if(t.parentNode){a(t).html(c.filter.proccess("paste",a(t).html()));p=t.lastChild;c.dom.unwrap(t);if(p){c.selection.select(p);c.selection.collapse(false)}}else{t.parentNode&&t.parentNode.removeChild(t);c.val(c.filter.proccess("paste",c.filter.wysiwyg2wysiwyg(a(c.doc.body).html())));c.selection.select(c.doc.body.firstChild);c.selection.collapse(true)}a(c.doc.body).mouseup()},15)}});if(a.browser.msie){this.$doc.bind("keyup",function(p){if(p.keyCode==86&&(p.metaKey||p.ctrlKey)){c.history.add(true);
c.typing=true;c.lastKey=null;c.selection.saveIERange();c.val(c.filter.proccess("paste",c.filter.wysiwyg2wysiwyg(a(c.doc.body).html())));c.selection.restoreIERange();a(c.doc.body).mouseup();this.ui.update()}})}if(a.browser.safari){this.$doc.bind("click",function(p){a(c.doc.body).find(".elrte-webkit-hl").removeClass("elrte-webkit-hl");if(p.target.nodeName=="IMG"){a(p.target).addClass("elrte-webkit-hl")}}).bind("keyup",function(p){a(c.doc.body).find(".elrte-webkit-hl").removeClass("elrte-webkit-hl")
})}this.window.focus();this.destroy=function(){this.updateSource();this.target.is("textarea")?this.target.val(a.trim(this.source.val())):this.target.html(a.trim(this.source.val()));this.editor.remove();this.target.show().parents("form").unbind("submit.elfinder")}};elRTE.prototype.i18n=function(b){return this._i18n.translate(b)};elRTE.prototype.open=function(){this.editor.show()};elRTE.prototype.close=function(){this.editor.hide()};elRTE.prototype.updateEditor=function(){this.val(this.source.val())
};elRTE.prototype.updateSource=function(){this.source.val(this.filter.source(a(this.doc.body).html()))};elRTE.prototype.val=function(b){if(typeof(b)=="string"){b=""+b;if(this.source.is(":visible")){this.source.val(this.filter.source2source(b))}else{if(a.browser.msie){this.doc.body.innerHTML="<br />"+this.filter.wysiwyg(b);this.doc.body.removeChild(this.doc.body.firstChild)}else{this.doc.body.innerHTML=this.filter.wysiwyg(b)}}}else{if(this.source.is(":visible")){return this.filter.source2source(this.source.val()).trim()
}else{return this.filter.source(a(this.doc.body).html()).trim()}}};elRTE.prototype.beforeSave=function(){this.source.val(a.trim(this.val())||"")};elRTE.prototype.save=function(){this.beforeSave();this.editor.parents("form").submit()};elRTE.prototype.log=function(b){if(window.console&&window.console.log){window.console.log(b)}};elRTE.prototype.i18Messages={};a.fn.elrte=function(g,b){var f=typeof(g)=="string"?g:"",c;this.each(function(){if(!this.elrte){this.elrte=new elRTE(this,typeof(g)=="object"?g:{})
}switch(f){case"open":case"show":this.elrte.open();break;case"close":case"hide":this.elrte.close();break;case"updateSource":this.elrte.updateSource();break;case"destroy":this.elrte.destroy()}});if(f=="val"){if(!this.length){return""}else{if(this.length==1){return b?this[0].elrte.val(b):this[0].elrte.val()}else{c={};this.each(function(){c[this.elrte.source.attr("name")]=this.elrte.val()});return c}}}return this}})(jQuery);(function(a){elRTE.prototype.dom=function(c){this.rte=c;var b=this;this.regExp={textNodes:/^(A|ABBR|ACRONYM|ADDRESS|B|BDO|BIG|BLOCKQUOTE|CAPTION|CENTER|CITE|CODE|DD|DEL|DFN|DIV|DT|EM|FIELDSET|FONT|H[1-6]|I|INS|KBD|LABEL|LEGEND|LI|MARQUEE|NOBR|NOEMBED|P|PRE|Q|SAMP|SMALL|SPAN|STRIKE|STRONG|SUB|SUP|TD|TH|TT|VAR)$/,textContainsNodes:/^(A|ABBR|ACRONYM|ADDRESS|B|BDO|BIG|BLOCKQUOTE|CAPTION|CENTER|CITE|CODE|DD|DEL|DFN|DIV|DL|DT|EM|FIELDSET|FONT|H[1-6]|I|INS|KBD|LABEL|LEGEND|LI|MARQUEE|NOBR|NOEMBED|OL|P|PRE|Q|SAMP|SMALL|SPAN|STRIKE|STRONG|SUB|SUP|TABLE|THEAD|TBODY|TFOOT|TD|TH|TR|TT|UL|VAR)$/,block:/^(APPLET|BLOCKQUOTE|BR|CAPTION|CENTER|COL|COLGROUP|DD|DIV|DL|DT|H[1-6]|EMBED|FIELDSET|LI|MARQUEE|NOBR|OBJECT|OL|P|PRE|TABLE|THEAD|TBODY|TFOOT|TD|TH|TR|UL)$/,selectionBlock:/^(APPLET|BLOCKQUOTE|BR|CAPTION|CENTER|COL|COLGROUP|DD|DIV|DL|DT|H[1-6]|EMBED|FIELDSET|LI|MARQUEE|NOBR|OBJECT|OL|P|PRE|TD|TH|TR|UL)$/,header:/^H[1-6]$/,formElement:/^(FORM|INPUT|HIDDEN|TEXTAREA|SELECT|BUTTON)$/};
this.root=function(){return this.rte.body};this.create=function(f){return this.rte.doc.createElement(f)};this.createBookmark=function(){var f=this.rte.doc.createElement("span");f.id="elrte-bm-"+Math.random().toString().substr(2);a(f).addClass("elrtebm elrte-protected");return f};this.indexOf=function(g){var f=0;g=a(g);while((g=g.prev())&&g.length){f++}return f};this.attr=function(h,f){var g="";if(h.nodeType==1){g=a(h).attr(f);if(g&&f!="src"&&f!="href"&&f!="title"&&f!="alt"){g=g.toString().toLowerCase()
}}return g||""};this.findCommonAncestor=function(j,h){if(!j||!h){return this.rte.log("dom.findCommonAncestor invalid arguments")}if(j==h){return j}else{if(j.nodeName=="BODY"||h.nodeName=="BODY"){return this.rte.doc.body}}var o=a(j).parents(),k=a(h).parents(),f=k.length-1,p=k[f];for(var g=o.length-1;g>=0;g--,f--){if(o[g]==k[f]){p=o[g]}else{break}}return p};this.isEmpty=function(f){if(f.nodeType==1){return this.regExp.textNodes.test(f.nodeName)?a.trim(a(f).text()).length==0:false}else{if(f.nodeType==3){return/^(TABLE|THEAD|TFOOT|TBODY|TR|UL|OL|DL)$/.test(f.parentNode.nodeName)||f.nodeValue==""||(a.trim(f.nodeValue).length==0&&!(f.nextSibling&&f.previousSibling&&f.nextSibling.nodeType==1&&f.previousSibling.nodeType==1&&!this.regExp.block.test(f.nextSibling.nodeName)&&!this.regExp.block.test(f.previousSibling.nodeName)))
}}return true};this.next=function(f){while(f.nextSibling&&(f=f.nextSibling)){if(f.nodeType==1||(f.nodeType==3&&!this.isEmpty(f))){return f}}return null};this.prev=function(f){while(f.previousSibling&&(f=f.previousSibling)){if(f.nodeType==1||(f.nodeType==3&&!this.isEmpty(f))){return f}}return null};this.isPrev=function(g,f){while((g=this.prev(g))){if(g==f){return true}}return false};this.nextAll=function(g){var f=[];while((g=this.next(g))){f.push(g)}return f};this.prevAll=function(g){var f=[];while((g=this.prev(g))){f.push(g)
}return f};this.toLineEnd=function(g){var f=[];while((g=this.next(g))&&g.nodeName!="BR"&&g.nodeName!="HR"&&this.isInline(g)){f.push(g)}return f};this.toLineStart=function(g){var f=[];while((g=this.prev(g))&&g.nodeName!="BR"&&g.nodeName!="HR"&&this.isInline(g)){f.unshift(g)}return f};this.isFirstNotEmpty=function(f){while((f=this.prev(f))){if(f.nodeType==1||(f.nodeType==3&&a.trim(f.nodeValue)!="")){return false}}return true};this.isLastNotEmpty=function(f){while((f=this.next(f))){if(!this.isEmpty(f)){return false
}}return true};this.isOnlyNotEmpty=function(f){return this.isFirstNotEmpty(f)&&this.isLastNotEmpty(f)};this.findLastNotEmpty=function(f){this.rte.log("findLastNotEmpty Who is here 0_o");if(f.nodeType==1&&(l=f.lastChild)){if(!this.isEmpty(l)){return l}while(l.previousSibling&&(l=l.previousSibling)){if(!this.isEmpty(l)){return l}}}return false};this.isInline=function(j){if(j.nodeType==3){return true}else{if(j.nodeType==1){j=a(j);var h=j.css("display");var g=j.css("float");return h=="inline"||h=="inline-block"||g=="left"||g=="right"
}}return true};this.is=function(h,g){if(h&&h.nodeName){if(typeof(g)=="string"){g=this.regExp[g]||/.?/}if(g instanceof RegExp&&h.nodeName){return g.test(h.nodeName)}else{if(typeof(g)=="function"){return g(h)}}}return false};this.filter=function(j,h){var f=[],g;if(!j.push){return this.is(j,h)?j:null}for(g=0;g<j.length;g++){if(this.is(j[g],h)){f.push(j[g])}}return f};this.parents=function(h,g){var f=[];while(h&&(h=h.parentNode)&&h.nodeName!="BODY"&&h.nodeName!="HTML"){if(this.is(h,g)){f.push(h)}}return f
};this.parent=function(h,g){return this.parents(h,g)[0]||null};this.selfOrParent=function(h,g,f){return this.is(h,g)?h:this.parent(h,f||g)};this.selfOrParentLink=function(f){f=this.selfOrParent(f,/^A$/);return f&&f.href?f:null};this.selfOrParentAnchor=function(f){f=this.selfOrParent(f,/^A$/);return f&&!f.href&&f.name?f:null};this.childLinks=function(g){var f=[];a("a[href]",g).each(function(){f.push(this)});return f};this.selectionHas=function(h){var j=this.rte.selection.cloneContents(),g;if(j&&j.childNodes&&j.childNodes.length){for(g=0;
g<j.childNodes.length;g++){if(typeof(h)=="function"){if(h(j.childNodes[g])){return true}}else{if(j instanceof RegExp){if(h.test(j.childNodes[g].nodeName)){return true}}}}}return false};this.wrap=function(g,f){g=a.isArray(g)?g:[g];f=f.nodeName?f:this.create(f);if(g[0]&&g[0].nodeType&&g[0].parentNode){f=g[0].parentNode.insertBefore(f,g[0]);a(g).each(function(){if(this!=f){f.appendChild(this)}})}return f};this.unwrap=function(f){if(f&&f.parentNode){while(f.firstChild){f.parentNode.insertBefore(f.firstChild,f)
}f.parentNode.removeChild(f)}};this.wrapContents=function(h,f){f=f.nodeName?f:this.create(f);for(var g=0;g<h.childNodes.length;g++){f.appendChild(h.childNodes[g])}h.appendChild(f);return f};this.cleanNode=function(f){if(f.nodeType!=1){return}if(/^(P|LI)$/.test(f.nodeName)&&(l=this.findLastNotEmpty(f))&&l.nodeName=="BR"){a(l).remove()}$n=a(f);$n.children().each(function(){this.cleanNode(this)});if(f.nodeName!="BODY"&&!/^(TABLE|TR|TD)$/.test(f)&&this.isEmpty(f)){return $n.remove()}if($n.attr("style")===""){$n.removeAttr("style")
}if(this.rte.browser.safari&&$n.hasClass("Apple-span")){$n.removeClass("Apple-span")}if(f.nodeName=="SPAN"&&!$n.attr("style")&&!$n.attr("class")&&!$n.attr("id")){$n.replaceWith($n.html())}};this.cleanChildNodes=function(g){var f=this.cleanNode;a(g).children().each(function(){f(this)})};this.tableMatrix=function(j){var h=[];if(j&&j.nodeName=="TABLE"){var g=0;function f(o){for(var k=0;k<=g;k++){if(!h[o][k]){return k}}}a(j).find("tr").each(function(k){if(!a.isArray(h[k])){h[k]=[]}a(this).children("td,th").each(function(){var p=parseInt(a(this).attr("colspan")||1);
var t=parseInt(a(this).attr("rowspan")||1);var r=f(k);for(var v=0;v<t;v++){for(var o=0;o<p;o++){var q=k+v;if(!a.isArray(h[q])){h[q]=[]}var u=o==0&&v==0?this:(v==0?o:"-");h[q][r+o]=u}}g=Math.max(g,h[k].length)})})}return h};this.indexesOfCell=function(j,h){for(var f=0;f<h.length;f++){for(var g=0;g<h[f].length;g++){if(h[f][g]==j){return[f,g]}}}};this.fixTable=function(q){if(q&&q.nodeName=="TABLE"){var h=a(q);var p=this.tableMatrix(q);var f=0;a.each(p,function(){f=Math.max(f,this.length)});if(f==0){return h.remove()
}for(var k=0;k<p.length;k++){var g=p[k].length;if(g==0){h.find("tr").eq(k).remove()}else{if(g<f){var j=f-g;var o=h.find("tr").eq(k);for(i=0;i<j;i++){o.append("<td>&nbsp;</td>")}}}}}};this.tableColumn=function(h,g,o){h=this.selfOrParent(h,/^TD|TH$/);var j=this.selfOrParent(h,/^TABLE$/);ret=[];info={offset:[],delta:[]};if(h&&j){o&&this.fixTable(j);var u=this.tableMatrix(j);var v=false;var t;for(var f=0;f<u.length;f++){for(var q=0;q<u[f].length;q++){if(u[f][q]==h){t=q;v=true;break}}if(v){break}}if(t>=0){for(var f=0;
f<u.length;f++){var k=u[f][t]||null;if(k){if(k.nodeName){ret.push(k);if(g){info.delta.push(0);info.offset.push(t)}}else{var p=parseInt(k);if(!isNaN(p)&&u[f][t-p]&&u[f][t-p].nodeName){ret.push(u[f][t-p]);if(g){info.delta.push(p);info.offset.push(t)}}}}}}}return !g?ret:{column:ret,info:info}}}})(jQuery);(function(a){elRTE.prototype.filter=function(c){var b=this,f=a("<span/>").addClass("elrtetesturl").appendTo(document.body)[0];this.url=(typeof(f.currentStyle)!="undefined"?f.currentStyle.backgroundImage:document.defaultView.getComputedStyle(f,null)["backgroundImage"]).replace(/^url\((['"]?)([\s\S]+\/)[\s\S]+\1\)$/i,"$2");
a(f).remove();this.rte=c;this.xhtml=/xhtml/i.test(c.options.doctype);this.boolAttrs=c.utils.makeObject("checked,compact,declare,defer,disabled,ismap,multiple,nohref,noresize,noshade,nowrap,readonly,selected".split(","));this.tagRegExp=/<(\/?)([\w:]+)((?:\s+[a-z\-]+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*\/?>/g;this.openTagRegExp=/<([\w:]+)((?:\s+\w+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*\/?>/g;this.attrRegExp=/(\w+)(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^\s]+))?/g;this.scriptRegExp=/<script([^>]*)>([\s\S]*?)<\/script>/gi;
this.styleRegExp=/(<style([^>]*)>[\s\S]*?<\/style>)/gi;this.linkRegExp=/(<link([^>]+)>)/gi;this.cdataRegExp=/<!\[CDATA\[([\s\S]+)\]\]>/g;this.objRegExp=/<object([^>]*)>([\s\S]*?)<\/object>/gi;this.embRegExp=/<(embed)((?:\s+\w+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*>/gi;this.paramRegExp=/<(param)((?:\s+\w+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*>/gi;this.iframeRegExp=/<iframe([^>]*)>([\s\S]*?)<\/iframe>/gi;this.yMapsRegExp=/<div\s+([^>]*id\s*=\s*('|")?YMapsID[^>]*)>/gi;this.gMapsRegExp=/<iframe\s+([^>]*src\s*=\s*"http:\/\/maps\.google\.\w+[^>]*)>([\s\S]*?)<\/iframe>/gi;
this.videoHostRegExp=/^(http:\/\/[\w\.]*)?(youtube|vimeo|rutube).*/i;this.serviceClassRegExp=/<(\w+)([^>]*class\s*=\s*"[^>]*elrte-[^>]*)>\s*(<\/\1>)?/gi;this.pagebreakRegExp=/<(\w+)([^>]*style\s*=\s*"[^>]*page-break[^>]*)>\s*(<\/\1>)?/gi;this.pbRegExp=new RegExp("<!-- pagebreak -->","gi");this.allowTags=c.options.allowTags.length?c.utils.makeObject(c.options.allowTags):null;this.denyTags=c.options.denyTags.length?c.utils.makeObject(c.options.denyTags):null;this.denyAttr=c.options.denyAttr?c.utils.makeObject(c.options.denyAttr):null;
this.pasteDenyAttr=c.options.pasteDenyAttr?c.utils.makeObject(c.options.pasteDenyAttr):null;this.fontSize=["medium","xx-small","small","medium","large","x-large","xx-large"];this.fontFamily={"sans-serif":/^(arial|tahoma|verdana)$/i,serif:/^(times|times new roman)$/i,monospace:/^courier$/i};this.scripts={};this._chains={};a.each(this.chains,function(g){b._chains[g]=[];a.each(this,function(h,j){typeof(b.rules[j])=="function"&&b._chains[g].push(b.rules[j])})});this.proccess=function(h,g){g=a.trim(g).replace(/^\s*(&nbsp;)+/gi,"").replace(/(&nbsp;|<br[^>]*>)+\s*$/gi,"");
a.each(this._chains[h]||[],function(){g=this.call(b,g)});g=g.replace(/\t/g,"  ").replace(/\r/g,"").replace(/\s*\n\s*\n+/g,"\n")+"  ";return a.trim(g)?g:" "};this.wysiwyg=function(g){return this.proccess("wysiwyg",g)};this.source=function(g){return this.proccess("source",g)};this.source2source=function(g){return this.proccess("source2source",g)};this.wysiwyg2wysiwyg=function(g){return this.proccess("wysiwyg2wysiwyg",g)};this.parseAttrs=function(p){var j={},h=this.boolAttrs,g=p.match(this.attrRegExp),o,q,k;
g&&a.each(g,function(r,t){o=t.split("=");q=a.trim(o[0]).toLowerCase();if(o.length>2){o.shift();k=o.join("=")}else{k=h[q]||o[1]||""}j[q]=a.trim(k).replace(/^('|")(.*)(\1)$/,"$2")});j.style=this.rte.utils.parseStyle(j.style);j["class"]=this.rte.utils.parseClass(j["class"]||"");return j};this.serializeAttrs=function(g,k){var j=[],h=this;a.each(g,function(p,o){if(p=="style"){o=h.rte.utils.serializeStyle(o,k)}else{if(p=="class"){o=h.rte.utils.serializeClass(o)}}o&&j.push(p+'="'+o+'"')});return j.join(" ")
};this.cleanAttrs=function(g,j){var h=this,o=this.replaceAttrs;a.each(g["class"],function(p){/^(Apple-style-span|mso\w+)$/i.test(p)&&delete g["class"][p]});function k(p){return p+(/\d$/.test(p)?"px":"")}a.each(g,function(q,p){o[q]&&o[q].call(h,g,j);if(q=="style"){a.each(p,function(t,r){switch(t){case"mso-padding-alt":case"mso-padding-top-alt":case"mso-padding-right-alt":case"mso-padding-bottom-alt":case"mso-padding-left-alt":case"mso-margin-alt":case"mso-margin-top-alt":case"mso-margin-right-alt":case"mso-margin-bottom-alt":case"mso-margin-left-alt":case"mso-table-layout-alt":case"mso-height":case"mso-width":case"mso-vertical-align-alt":g.style[t.replace(/^mso-|-alt$/g,"")]=k(r);
delete g.style[t];break;case"horiz-align":g.style["text-align"]=r;delete g.style[t];break;case"vert-align":g.style["vertical-align"]=r;delete g.style[t];break;case"font-color":case"mso-foreground":g.style.color=r;delete g.style[t];break;case"mso-background":case"mso-highlight":g.style.background=r;delete g.style[t];break;case"mso-default-height":g.style["min-height"]=k(r);delete g.style[t];break;case"mso-default-width":g.style["min-width"]=k(r);delete g.style[t];break;case"mso-padding-between-alt":g.style["border-collapse"]="separate";
g.style["border-spacing"]=k(r);delete g.style[t];break;case"text-line-through":if(r.match(/(single|double)/i)){g.style["text-decoration"]="line-through"}delete g.style[t];break;case"mso-zero-height":if(r=="yes"){g.style.display="none"}delete g.style[t];break;case"font-weight":if(r==700){g.style["font-weight"]="bold"}break;default:if(t.match(/^(mso|column|font-emph|lang|layout|line-break|list-image|nav|panose|punct|row|ruby|sep|size|src|tab-|table-border|text-(?!align|decor|indent|trans)|top-bar|version|vnd|word-break)/)){delete g.style[t]
}}})}});return g}};elRTE.prototype.filter.prototype.replaceTags={b:{tag:"strong"},big:{tag:"span",style:{"font-size":"large"}},center:{tag:"div",style:{"text-align":"center"}},i:{tag:"em"},font:{tag:"span"},nobr:{tag:"span",style:{"white-space":"nowrap"}},menu:{tag:"ul"},plaintext:{tag:"pre"},s:{tag:"strike"},small:{tag:"span",style:{"font-size":"small"}},u:{tag:"span",style:{"text-decoration":"underline"}},xmp:{tag:"pre"}};elRTE.prototype.filter.prototype.replaceAttrs={align:function(b,c){switch(c){case"img":b.style[b.align.match(/(left|right)/)?"float":"vertical-align"]=b.align;
break;case"table":if(b.align=="center"){b.style["margin-left"]=b.style["margin-right"]="auto"}else{b.style["float"]=b.align}break;default:b.style["text-align"]=b.align}delete b.align},border:function(b){!b.style["border-width"]&&(b.style["border-width"]=(parseInt(b.border)||1)+"px");!b.style["border-style"]&&(b.style["border-style"]="solid");delete b.border},bordercolor:function(b){!b.style["border-color"]&&(b.style["border-color"]=b.bordercolor);delete b.bordercolor},background:function(b){!b.style["background-image"]&&(b.style["background-image"]="url("+b.background+")");
delete b.background},bgcolor:function(b){!b.style["background-color"]&&(b.style["background-color"]=b.bgcolor);delete b.bgcolor},clear:function(b){b.style.clear=b.clear=="all"?"both":b.clear;delete b.clear},color:function(b){!b.style.color&&(b.style.color=b.color);delete b.color},face:function(b){var c=b.face.toLowerCase();a.each(this.fontFamily,function(g,f){if(c.match(f)){b.style["font-family"]=c+","+g}});delete b.face},hspace:function(b,f){if(f=="img"){var c=parseInt(b.hspace)||0;!b.style["margin-left"]&&(b.style["margin-left"]=c+"px");
!b.style["margin-right"]&&(b.style["margin-right"]=c+"px");delete b.hspace}},size:function(b,c){if(c!="input"){b.style["font-size"]=this.fontSize[parseInt(b.size)||0]||"medium";delete b.size}},valign:function(b){if(!b.style["vertical-align"]){b.style["vertical-align"]=b.valign}delete b.valign},vspace:function(b,f){if(f=="img"){var c=parseInt(b.vspace)||0;!b.style["margin-top"]&&(b.style["margin-top"]=c+"px");!b.style["margin-bottom"]&&(b.style["margin-bottom"]=c+"px");delete b.hspace}}};elRTE.prototype.filter.prototype.rules={allowedTags:function(c){var b=this.allowTags;
return b?c.replace(this.tagRegExp,function(f,h,g){return b[g.toLowerCase()]?f:""}):c},deniedTags:function(b){var c=this.denyTags;return c?b.replace(this.tagRegExp,function(f,h,g){return c[g.toLowerCase()]?"":f}):b},clean:function(g){var c=this,b=this.replaceTags,h=this.replaceAttrs,f=this.denyAttr,j;g=g.replace(/<!DOCTYPE([\s\S]*)>/gi,"").replace(/<p [^>]*class="?MsoHeading"?[^>]*>(.*?)<\/p>/gi,"<p><strong>$1</strong></p>").replace(/<span\s+style\s*=\s*"\s*mso-spacerun\s*:\s*yes\s*;?\s*"\s*>([\s&nbsp;]*)<\/span>/gi,"$1").replace(/(<p[^>]*>\s*<\/p>|<p[^>]*\/>)/gi,"<br>").replace(/(<\/p>)(?:\s*<br\s*\/?>\s*|\s*&nbsp;\s*)+\s*(<p[^>]*>)/gi,function(o,k,p){return k+"\n"+p
}).replace(this.tagRegExp,function(o,q,p,k){p=p.toLowerCase();if(q){return"</"+(b[p]?b[p].tag:p)+">"}k=c.cleanAttrs(c.parseAttrs(k||""),p);if(b[p]){b[p].style&&a.extend(k.style,b[p].style);p=b[p].tag}f&&a.each(k,function(r){if(f[r]){delete k[r]}});k=c.serializeAttrs(k);return"<"+p+(k?" ":"")+k+">"});j=a("<div>"+g+"</div>");j.find("span:not([id]):not([class])").each(function(){var k=a(this);if(!k.attr("style")){a.trim(k.html()).length?c.rte.dom.unwrap(this):k.remove()}}).end().find("span span:only-child").each(function(){var o=a(this),u=o.parent().eq(0),r=o.attr("id"),k=u.attr("id"),w,q,v;
if(c.rte.dom.isOnlyNotEmpty(this)&&(!r||!k)){v=a.trim(u.attr("class")+" "+o.attr("class"));v&&u.attr("class",v);q=c.rte.utils.serializeStyle(a.extend(c.rte.utils.parseStyle(a(this).attr("style")||""),c.rte.utils.parseStyle(a(u).attr("style")||"")));q&&u.attr("style",q);w=r||k;w&&u.attr("id",w);this.firstChild?a(this.firstChild).unwrap():o.remove()}}).end().find("a[name]").each(function(){a(this).addClass("elrte-protected elrte-anchor")});return j.html()},cleanPaste:function(c){var b=this,f=this.pasteDenyAttr;
c=c.replace(this.scriptRegExp,"").replace(this.styleRegExp,"").replace(this.linkRegExp,"").replace(this.cdataRegExp,"").replace(/\<\!--[\s\S]*?--\>/g,"");if(this.rte.options.pasteOnlyText){c=c.replace(this.tagRegExp,function(g,j,h){return/br/i.test(h)||(j&&/h[1-6]|p|ol|ul|li|div|blockquote|tr/i)?"<br>":""}).replace(/(&nbsp;|<br[^>]*>)+\s*$/gi,"")}else{if(f){c=c.replace(this.openTagRegExp,function(h,j,g){g=b.parseAttrs(g);a.each(g,function(k){if(f[k]){delete g[k]}});g=b.serializeAttrs(g,true);return"<"+j+(g?" ":"")+g+">"
})}}return c},replace:function(o){var q=this,b=this.rte.options.replace||[],g;if(b.length){a.each(b,function(r,t){if(typeof(t)=="function"){o=t.call(q,o)}})}function k(x,D){var E=r(),B=E&&q.videoHostRegExp.test(E)?E.replace(q.videoHostRegExp,"$2"):D.replace(/^\w+\/(.+)/,"$1"),C=parseInt((x.obj?x.obj.width||x.obj.style.width:0)||(x.embed?x.embed.width||x.embed.style.width:0))||150,A=parseInt((x.obj?x.obj.height||x.obj.style.height:0)||(x.embed?x.embed.height||x.embed.style.height:0))||100,v="media"+Math.random().toString().substring(2),u="",z;
function r(){if(x.embed&&x.embed.src){return x.embed.src}if(x.params&&x.params.length){z=x.params.length;while(z--){if(x.params[z].name=="src"||x.params[z].name=="movie"){return x.params[z].value}}}}if(x.obj&&x.obj.style&&x.obj.style["float"]){u=' style="float:'+x.obj.style["float"]+'"'}q.scripts[v]=x;return'<img src="'+q.url+'pixel.gif" class="elrte-media elrte-media-'+B+' elrte-protected" title="'+(E?q.rte.utils.encode(E):"")+'" rel="'+v+'" width="'+C+'" height="'+A+'"'+u+">"}o=o.replace(this.styleRegExp,"<!-- ELRTE_COMMENT$1 -->").replace(this.linkRegExp,"<!-- ELRTE_COMMENT$1-->").replace(this.cdataRegExp,"<!--[CDATA[$1]]-->").replace(this.scriptRegExp,function(u,r,v){var w;
if(q.denyTags.script){return""}w="script"+Math.random().toString().substring(2);r=q.parseAttrs(r);!r.type&&(r.type="text/javascript");q.scripts[w]="<script "+q.serializeAttrs(r)+">"+v+"<\/script>";return"<!-- ELRTE_SCRIPT:"+(w)+" -->"}).replace(this.yMapsRegExp,function(u,r){r=q.parseAttrs(r);r["class"]["elrte-yandex-maps"]="elrte-yandex-maps";r["class"]["elrte-protected"]="elrte-protected";return"<div "+q.serializeAttrs(r)+">"}).replace(this.gMapsRegExp,function(v,u){var z="gmaps"+Math.random().toString().substring(2),r,x;
u=q.parseAttrs(u);r=parseInt(u.width||u.style.width||100);x=parseInt(u.height||u.style.height||100);q.scripts[z]=v;return'<img src="'+q.url+'pixel.gif" class="elrte-google-maps elrte-protected" id="'+z+'" style="width:'+r+"px;height:"+x+'px">'}).replace(this.objRegExp,function(w,u,z){var r=z.match(q.embRegExp),x={obj:q.parseAttrs(u),embed:r&&r.length?q.parseAttrs(r[0].substring(7)):null,params:[]},v=q.rte.utils.mediaInfo(x.embed?x.embed.type||"":"",x.obj.classid||"");if(v){if((r=z.match(q.paramRegExp))){a.each(r,function(t,A){x.params.push(q.parseAttrs(A.substring(6)))
})}!x.obj.classid&&(x.obj.classid=v.classid[0]);!x.obj.codebase&&(x.obj.codebase=v.codebase);x.embed&&!x.embed.type&&(x.embed.type=v.type);x.obj.width=="1"&&delete x.obj.width;x.obj.height=="1"&&delete x.obj.height;if(x.embed){x.embed.width=="1"&&delete x.embed.width;x.embed.height=="1"&&delete x.embed.height}return k(x,v.type)}return w}).replace(this.embRegExp,function(v,w,r){var r=q.parseAttrs(r),u=q.rte.utils.mediaInfo(r.type||"");r.width=="1"&&delete r.width;r.height=="1"&&delete r.height;return u?k({embed:r},u.type):v
}).replace(this.iframeRegExp,function(x,u){var u=q.parseAttrs(u);var r=u.style.width||(parseInt(u.width)>1?parseInt(u.width)+"px":"100px");var z=u.style.height||(parseInt(u.height)>1?parseInt(u.height)+"px":"100px");var A="iframe"+Math.random().toString().substring(2);q.scripts[A]=x;var v='<img id="'+A+'" src="'+q.url+'pixel.gif" class="elrte-protected elrte-iframe" style="width:'+r+"; height:"+z+'">';return v}).replace(this.vimeoRegExp,function(u,v,r){r=q.parseAttrs(r);delete r.frameborder;r.width=="1"&&delete r.width;
r.height=="1"&&delete r.height;r.type="application/x-shockwave-flash";return k({embed:r},"application/x-shockwave-flash")}).replace(/<\/(embed|param)>/gi,"").replace(this.pbRegExp,function(){return'<img src="'+q.url+'pixel.gif" class="elrte-protected elrte-pagebreak">'});g=a("<div>"+o+"</div>");if(!this.rte.options.allowTextNodes){var h=this.rte.dom,c=[],p=[];if(a.browser.msie){for(var j=0;j<g[0].childNodes.length;j++){c.push(g[0].childNodes[j])}}else{c=Array.prototype.slice.call(g[0].childNodes)
}function f(){if(p.length&&h.filter(p,"notEmpty").length){h.wrap(p,document.createElement("p"))}p=[]}a.each(c,function(r,t){if(h.is(t,"block")){f()}else{if(p.length&&t.previousSibling!=p[p.length-1]){f()}p.push(t)}});f()}return g.html()},restore:function(c){var b=this,f=this.rte.options.restore||[];if(f.length){a.each(f,function(g,h){if(typeof(h)=="function"){c=h.call(b,c)}})}c=c.replace(/\<\!--\[CDATA\[([\s\S]*?)\]\]--\>/gi,"<![CDATA[$1]]>").replace(/\<\!--\s*ELRTE_SCRIPT\:\s*(script\d+)\s*--\>/gi,function(g,h){if(b.scripts[h]){g=b.scripts[h];
delete b.scripts[h]}return g||""}).replace(/\<\!-- ELRTE_COMMENT([\s\S]*?) --\>/gi,"$1").replace(this.serviceClassRegExp,function(k,r,g,p){var g=b.parseAttrs(g),h,q="";if(g["class"]["elrte-google-maps"]){var k="";if(b.scripts[g.id]){k=b.scripts[g.id];delete b.scripts[g.id]}return k}else{if(g["class"]["elrte-iframe"]){return b.scripts[g.id]||""}else{if(g["class"]["elrtebm"]){return""}else{if(g["class"]["elrte-media"]){h=b.scripts[g.rel]||{};h.params&&a.each(h.params,function(j,o){q+="<param "+b.serializeAttrs(o)+">\n"
});h.embed&&(q+="<embed "+b.serializeAttrs(h.embed)+">");h.obj&&(q="<object "+b.serializeAttrs(h.obj)+">\n"+q+"\n</object>\n");return q||k}else{if(g["class"]["elrte-pagebreak"]){return"<!-- pagebreak -->"}}}}}a.each(g["class"],function(j){if(/^elrte-\w+/i.test(j)){delete (g["class"][j])}});return"<"+r+" "+b.serializeAttrs(g)+">"+(p||"")});return c},compactStyles:function(c){var b=this;return c.replace(this.tagRegExp,function(g,j,h,f){f=!j&&f?b.serializeAttrs(b.parseAttrs(f),true):"";return"<"+j+h.toLowerCase()+(f?" ":"")+f+">"
})},xhtmlTags:function(b){return this.xhtml?b.replace(/<(img|hr|br|embed|param|link|area)([^>]*\/*)>/gi,"<$1$2 />"):b}};elRTE.prototype.filter.prototype.chains={wysiwyg:["replace","clean","allowedTags","deniedTags","compactStyles"],source:["clean","allowedTags","restore","compactStyles","xhtmlTags"],paste:["clean","allowedTags","cleanPaste","replace","deniedTags","compactStyles"],wysiwyg2wysiwyg:["clean","allowedTags","restore","replace","deniedTags","compactStyles"],source2source:["clean","allowedTags","replace","deniedTags","restore","compactStyles","xhtmlTags"]}
})(jQuery);(function(a){elRTE.prototype.history=function(b){this.rte=b;this._prev=[];this._next=[];this.add=function(){if(this.rte.options.historyLength>0&&this._prev.length>=this.rte.options.historyLength){this._prev.slice(this.rte.options.historyLength)}var c=this.rte.selection.getBookmark();this._prev.push([a(this.rte.doc.body).html(),c]);this.rte.selection.moveToBookmark(c);this._next=[]};this.back=function(){if(this._prev.length){var c=this.rte.selection.getBookmark(),f=this._prev.pop();this._next.push([a(this.rte.doc.body).html(),c]);
a(this.rte.doc.body).html(f[0]);this.rte.selection.moveToBookmark(f[1])}};this.fwd=function(){if(this._next.length){var c=this.rte.selection.getBookmark(),f=this._next.pop();this._prev.push([a(this.rte.doc.body).html(),c]);a(this.rte.doc.body).html(f[0]);this.rte.selection.moveToBookmark(f[1])}};this.canBack=function(){return this._prev.length};this.canFwd=function(){return this._next.length}}})(jQuery);(function(a){elRTE.prototype.options={doctype:'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">',cssClass:"el-rte",cssfiles:[],height:null,resizable:true,lang:"en",toolbar:"normal",absoluteURLs:true,allowSource:true,stripWhiteSpace:true,styleWithCSS:false,fmAllow:true,fmOpen:null,allowTags:[],denyTags:["applet","base","basefont","bgsound","blink","body","col","colgroup","isindex","frameset","html","head","meta","marquee","noframes","noembed","o:p","title","xml"],denyAttr:[],pasteDenyAttr:["id","name","class","style","language","onclick","ondblclick","onhover","onkeup","onkeydown","onkeypress"],allowTextNodes:true,allowBrowsersSpecStyles:false,allowPaste:true,pasteOnlyText:false,replace:[],restore:[],pagebreak:'<div style="page-break-after: always;"></div>',buttons:{save:"Save",copy:"Copy",cut:"Cut",css:"Css style and class",paste:"Paste",pastetext:"Paste only text",pasteformattext:"Paste formatted text",removeformat:"Clean format",undo:"Undo last action",redo:"Redo previous action",bold:"Bold",italic:"Italic",underline:"Underline",strikethrough:"Strikethrough",superscript:"Superscript",subscript:"Subscript",justifyleft:"Align left",justifyright:"Ailgn right",justifycenter:"Align center",justifyfull:"Align full",indent:"Indent",outdent:"Outdent",rtl:"Right to left",ltr:"Left to right",forecolor:"Font color",hilitecolor:"Background color",formatblock:"Format",fontsize:"Font size",fontname:"Font",insertorderedlist:"Ordered list",insertunorderedlist:"Unordered list",horizontalrule:"Horizontal rule",blockquote:"Blockquote",div:"Block element (DIV)",link:"Link",unlink:"Delete link",anchor:"Bookmark",image:"Image",pagebreak:"Page break",smiley:"Smiley",flash:"Flash",table:"Table",tablerm:"Delete table",tableprops:"Table properties",tbcellprops:"Table cell properties",tbrowbefore:"Insert row before",tbrowafter:"Insert row after",tbrowrm:"Delete row",tbcolbefore:"Insert column before",tbcolafter:"Insert column after",tbcolrm:"Delete column",tbcellsmerge:"Merge table cells",tbcellsplit:"Split table cell",docstructure:"Toggle display document structure",elfinder:"Open file manager",fullscreen:"Toggle full screen mode",nbsp:"Non breakable space",stopfloat:"Stop element floating",about:"About this software"},panels:{eol:[],save:["save"],copypaste:["copy","cut","paste","pastetext","pasteformattext","removeformat","docstructure"],undoredo:["undo","redo"],style:["bold","italic","underline","strikethrough","subscript","superscript"],colors:["forecolor","hilitecolor"],alignment:["justifyleft","justifycenter","justifyright","justifyfull"],indent:["outdent","indent"],format:["formatblock","fontsize","fontname"],lists:["insertorderedlist","insertunorderedlist"],elements:["horizontalrule","blockquote","div","stopfloat","css","nbsp","smiley","pagebreak"],direction:["ltr","rtl"],links:["link","unlink","anchor"],images:["image"],media:["image","flash"],tables:["table","tableprops","tablerm","tbrowbefore","tbrowafter","tbrowrm","tbcolbefore","tbcolafter","tbcolrm","tbcellprops","tbcellsmerge","tbcellsplit"],elfinder:["elfinder"],fullscreen:["fullscreen","about"]},toolbars:{tiny:["style"],compact:["save","undoredo","style","alignment","lists","links","fullscreen"],normal:["save","copypaste","undoredo","style","alignment","colors","indent","lists","links","elements","images","fullscreen"],complete:["save","copypaste","undoredo","style","alignment","colors","format","indent","lists","links","elements","media","fullscreen"],maxi:["save","copypaste","undoredo","elfinder","style","alignment","direction","colors","format","indent","lists","links","elements","media","tables","fullscreen"],eldorado:["save","copypaste","elfinder","undoredo","style","alignment","colors","format","indent","lists","links","elements","media","tables","fullscreen"]},panelNames:{save:"Save",copypaste:"Copy/Pase",undoredo:"Undo/Redo",style:"Text styles",colors:"Colors",alignment:"Alignment",indent:"Indent/Outdent",format:"Text format",lists:"Lists",elements:"Misc elements",direction:"Script direction",links:"Links",images:"Images",media:"Media",tables:"Tables",elfinder:"File manager (elFinder)"}}
})(jQuery);(function(a){elRTE.prototype.selection=function(g){this.rte=g;var c=this;this.w3cRange=null;var o,b,j,k;a(this.rte.doc).keyup(function(p){if(p.ctrlKey||p.metaKey||(p.keyCode>=8&&p.keyCode<=13)||(p.keyCode>=32&&p.keyCode<=40)||p.keyCode==46||(p.keyCode>=96&&p.keyCode<=111)){c.cleanCache()}}).mousedown(function(p){if(p.target.nodeName=="HTML"){o=c.rte.doc.body}else{o=p.target}b=j=null}).mouseup(function(p){if(p.target.nodeName=="HTML"){b=c.rte.doc.body}else{b=p.target}b=p.target;j=null}).click();
function h(){return c.rte.window.getSelection?c.rte.window.getSelection():c.rte.window.document.selection}function f(t,r,q){while(t.nodeName!="BODY"&&t.parentNode&&t.parentNode.nodeName!="BODY"&&(r?t!==r&&t.parentNode!=r:1)&&((q=="left"&&c.rte.dom.isFirstNotEmpty(t))||(q=="right"&&c.rte.dom.isLastNotEmpty(t))||(c.rte.dom.isFirstNotEmpty(t)&&c.rte.dom.isLastNotEmpty(t)))){t=t.parentNode}return t}this.collapsed=function(){return this.getRangeAt().isCollapsed()};this.collapse=function(p){var q=h(),t=this.getRangeAt();
t.collapse(p?true:false);if(!a.browser.msie){q.removeAllRanges();q.addRange(t)}return this};this.getRangeAt=function(t){if(this.rte.browser.msie){if(!this.w3cRange){this.w3cRange=new this.rte.w3cRange(this.rte)}t&&this.w3cRange.update();return this.w3cRange}var p=h();var q=p.rangeCount>0?p.getRangeAt(0):this.rte.doc.createRange();q.getStart=function(){return this.startContainer.nodeType==1?this.startContainer.childNodes[Math.min(this.startOffset,this.startContainer.childNodes.length-1)]:this.startContainer
};q.getEnd=function(){return this.endContainer.nodeType==1?this.endContainer.childNodes[Math.min(this.startOffset==this.endOffset?this.endOffset:this.endOffset-1,this.endContainer.childNodes.length-1)]:this.endContainer};q.isCollapsed=function(){return this.collapsed};return q};this.saveIERange=function(){if(a.browser.msie){k=this.getRangeAt().getBookmark()}};this.restoreIERange=function(){a.browser.msie&&k&&this.getRangeAt().moveToBookmark(k)};this.cloneContents=function(){var v=this.rte.dom.create("div"),q,u,p;
if(a.browser.msie){try{q=this.rte.window.document.selection.createRange()}catch(t){q=this.rte.doc.body.createTextRange()}a(v).html(q.htmlText)}else{u=this.getRangeAt().cloneContents();for(p=0;p<u.childNodes.length;p++){v.appendChild(u.childNodes[p].cloneNode(true))}}return v};this.select=function(t,w){w=w||t;if(this.rte.browser.msie){var u=this.rte.doc.body.createTextRange(),q=u.duplicate(),p=u.duplicate();q.moveToElementText(t);p.moveToElementText(w);u.setEndPoint("StartToStart",q);u.setEndPoint("EndToEnd",p);
u.select()}else{var v=h(),u=this.getRangeAt();u.setStartBefore(t);u.setEndAfter(w);v.removeAllRanges();v.addRange(u)}return this.cleanCache()};this.selectContents=function(u){var q=this.getRangeAt();if(u&&u.nodeType==1){if(this.rte.browser.msie){q.range();q.r.moveToElementText(u.parentNode);q.r.select()}else{try{q.selectNodeContents(u)}catch(t){return this.rte.log("unable select node contents "+u)}var p=h();p.removeAllRanges();p.addRange(q)}}return this};this.deleteContents=function(){if(!a.browser.msie){this.getRangeAt().deleteContents()
}return this};this.insertNode=function(v,u){if(u&&!this.collapsed()){this.collapse()}if(this.rte.browser.msie){var p=v.nodeType==3?v.nodeValue:a(this.rte.dom.create("span")).append(a(v)).html();var t=this.getRangeAt();t.insertNode(p)}else{var t=this.getRangeAt();t.insertNode(v);t.setStartAfter(v);t.setEndAfter(v);var q=h();q.removeAllRanges();q.addRange(t)}return this.cleanCache()};this.insertHtml=function(p,q){if(q&&!this.collapsed()){this.collapse()}if(this.rte.browser.msie){this.getRangeAt().range().pasteHTML(p)
}else{var r=a(this.rte.dom.create("span")).html(p||"").get(0);this.insertNode(r);a(r).replaceWith(a(r).html())}return this.cleanCache()};this.insertText=function(q,p){var r=this.rte.doc.createTextNode(q);return this.insertHtml(r.nodeValue)};this.getBookmark=function(){this.rte.window.focus();var p,v,t,z,w,A=this.rte.dom.createBookmark(),x=this.rte.dom.createBookmark();if(a.browser.msie){try{p=this.rte.window.document.selection.createRange()}catch(x){p=this.rte.doc.body.createTextRange()}if(p.item){var u=p.item(0);
p=this.rte.doc.body.createTextRange();p.moveToElementText(u)}v=p.duplicate();t=p.duplicate();z=this.rte.dom.create("span");w=this.rte.dom.create("span");z.appendChild(A);w.appendChild(x);v.collapse(true);v.pasteHTML(z.innerHTML);t.collapse(false);t.pasteHTML(w.innerHTML)}else{var q=h();var p=q.rangeCount>0?q.getRangeAt(0):this.rte.doc.createRange();v=p.cloneRange();t=p.cloneRange();t.collapse(false);t.insertNode(x);v.collapse(true);v.insertNode(A);this.select(A,x)}return[A.id,x.id]};this.moveToBookmark=function(p){this.rte.window.focus();
if(p&&p.length==2){var q=this.rte.doc.getElementById(p[0]),v=this.rte.doc.getElementById(p[1]),u,t;if(q&&v){this.select(q,v);if(this.rte.dom.next(q)==v){this.collapse(true)}if(!a.browser.msie){u=h();t=u.rangeCount>0?u.getRangeAt(0):this.rte.doc.createRange();u.removeAllRanges();u.addRange(t)}q.parentNode.removeChild(q);v.parentNode.removeChild(v)}}return this};this.removeBookmark=function(p){this.rte.window.focus();if(p.length==2){var q=this.rte.doc.getElementById(p[0]),r=this.rte.doc.getElementById(p[1]);
if(q&&r){q.parentNode.removeChild(q);r.parentNode.removeChild(r)}}};this.cleanCache=function(){o=b=j=null;return this};this.getStart=function(){if(!o){var p=this.getRangeAt();o=p.getStart()}return o};this.getEnd=function(){if(!b){var p=this.getRangeAt();b=p.getEnd()}return b};this.getNode=function(){if(!j){j=this.rte.dom.findCommonAncestor(this.getStart(),this.getEnd())}return j};this.selected=function(r){var p={collapsed:false,blocks:false,filter:false,wrap:"text",tag:"span"};p=a.extend({},p,r);
if(p.blocks){var v=this.getNode(),u=null;if(u=this.rte.dom.selfOrParent(v,"selectionBlock")){return[u]}}var t=this.selectedRaw(p.collapsed,p.blocks);var A=[];var w=[];var B=null;function q(){function D(){for(var F=0;F<w.length;F++){if(w[F].nodeType==1&&(c.rte.dom.selfOrParent(w[F],/^P$/)||a(w[F]).find("p").length>0)){return false}}return true}if(w.length>0){var C=p.tag=="p"&&!D()?"div":p.tag;var E=c.rte.dom.wrap(w,C);A[B]=E;B=null;w=[]}}function z(E){if(E.nodeType==1){if(/^(THEAD|TFOOT|TBODY|COL|COLGROUP|TR)$/.test(E.nodeName)){a(E).find("td,th").each(function(){var F=p.tag=="p"&&a(this).find("p").length>0?"div":p.tag;
var G=c.rte.dom.wrapContents(this,F);return A.push(G)})}else{if(/^(CAPTION|TD|TH|LI|DT|DD)$/.test(E.nodeName)){var C=p.tag=="p"&&a(E).find("p").length>0?"div":p.tag;var E=c.rte.dom.wrapContents(E,C);return A.push(E)}}}var D=w.length>0?w[w.length-1]:null;if(D&&D!=c.rte.dom.prev(E)){q()}w.push(E);if(B===null){B=A.length;A.push("dummy")}}if(t.nodes.length>0){for(var x=0;x<t.nodes.length;x++){var v=t.nodes[x];if(v.nodeType==3&&(x==0||x==t.nodes.length-1)&&a.trim(v.nodeValue).length>0){if(x==0&&t.so>0){v=v.splitText(t.so)
}if(x==t.nodes.length-1&&t.eo>0){v.splitText(x==0&&t.so>0?t.eo-t.so:t.eo)}}switch(p.wrap){case"text":if((v.nodeType==1&&v.nodeName=="BR")||(v.nodeType==3&&a.trim(v.nodeValue).length>0)){z(v)}else{if(v.nodeType==1){A.push(v)}}break;case"inline":if(this.rte.dom.isInline(v)){z(v)}else{if(v.nodeType==1){A.push(v)}}break;case"all":if(v.nodeType==1||!this.rte.dom.isEmpty(v)){z(v)}break;default:if(v.nodeType==1||!this.rte.dom.isEmpty(v)){A.push(v)}}}q()}if(A.length){this.rte.window.focus();this.select(A[0],A[A.length-1])
}return p.filter?this.rte.dom.filter(A,p.filter):A};this.dump=function(p,t,w,v,q){var u=this.getRangeAt();this.rte.log("commonAncestorContainer");this.rte.log(p||u.commonAncestorContainer);this.rte.log("startContainer");this.rte.log(t||u.startContainer);this.rte.log("startOffset: "+(v>=0?v:u.startOffset));this.rte.log("endContainer");this.rte.log(w||u.endContainer);this.rte.log("endOffset: "+(q>=0?q:u.endOffset))};this.selectedRaw=function(z,p){var F={so:null,eo:null,nodes:[]};var q=this.getRangeAt(true);
var w=q.commonAncestorContainer;var H,E;var D=false;var B=false;function C(J,r,I){if(J.nodeType==3){I=I>=0?I:J.nodeValue.length;return(r==0&&I==J.nodeValue.length)||a.trim(J.nodeValue).length==a.trim(J.nodeValue.substring(r,I)).length}return true}function x(J,r,I){if(J.nodeType==1){return c.rte.dom.isEmpty(J)}else{if(J.nodeType==3){return a.trim(J.nodeValue.substring(r||0,I>=0?I:J.nodeValue.length)).length==0}}return true}if(q.startContainer.nodeType==1){if(q.startOffset<q.startContainer.childNodes.length){H=q.startContainer.childNodes[q.startOffset];
F.so=H.nodeType==1?null:0}else{H=q.startContainer.childNodes[q.startOffset-1];F.so=H.nodeType==1?null:H.nodeValue.length}}else{H=q.startContainer;F.so=q.startOffset}if(q.collapsed){if(z){if(p){H=f(H);if(!this.rte.dom.isEmpty(H)||(H=this.rte.dom.next(H))){F.nodes=[H]}if(this.rte.dom.isInline(H)){F.nodes=this.rte.dom.toLineStart(H).concat(F.nodes,this.rte.dom.toLineEnd(H))}if(F.nodes.length>0){F.so=F.nodes[0].nodeType==1?null:0;F.eo=F.nodes[F.nodes.length-1].nodeType==1?null:F.nodes[F.nodes.length-1].nodeValue.length
}}else{if(!this.rte.dom.isEmpty(H)){F.nodes=[H]}}}return F}if(q.endContainer.nodeType==1){E=q.endContainer.childNodes[q.endOffset-1];F.eo=E.nodeType==1?null:E.nodeValue.length}else{E=q.endContainer;F.eo=q.endOffset}if(H.nodeType==1||p||C(H,F.so,H.nodeValue.length)){H=f(H,w,"left");D=true;F.so=H.nodeType==1?null:0}if(E.nodeType==1||p||C(E,0,F.eo)){E=f(E,w,"right");B=true;F.eo=E.nodeType==1?null:E.nodeValue.length}if(p){if(H.nodeType!=1&&H.parentNode!=w&&H.parentNode.nodeName!="BODY"){H=H.parentNode;
F.so=null}if(E.nodeType!=1&&E.parentNode!=w&&E.parentNode.nodeName!="BODY"){E=E.parentNode;F.eo=null}}if(H.parentNode==E.parentNode&&H.parentNode.nodeName!="BODY"&&(D&&this.rte.dom.isFirstNotEmpty(H))&&(B&&this.rte.dom.isLastNotEmpty(E))){H=E=H.parentNode;F.so=H.nodeType==1?null:0;F.eo=E.nodeType==1?null:E.nodeValue.length}if(H==E){if(!this.rte.dom.isEmpty(H)){F.nodes.push(H)}return F}var t=H;while(t.nodeName!="BODY"&&t.parentNode!==w&&t.parentNode.nodeName!="BODY"){t=t.parentNode}var G=E;while(G.nodeName!="BODY"&&G.parentNode!==w&&G.parentNode.nodeName!="BODY"){G=G.parentNode
}if(!x(H,F.so,H.nodeType==3?H.nodeValue.length:null)){F.nodes.push(H)}var v=H;while(v!==t){var u=v;while((u=this.rte.dom.next(u))){F.nodes.push(u)}v=v.parentNode}v=t;while((v=this.rte.dom.next(v))&&v!=G){F.nodes.push(v)}var A=[];v=E;while(v!==G){var u=v;while((u=this.rte.dom.prev(u))){A.push(u)}v=v.parentNode}if(A.length){F.nodes=F.nodes.concat(A.reverse())}if(!x(E,0,E.nodeType==3?F.eo:null)){F.nodes.push(E)}if(p){if(this.rte.dom.isInline(H)){F.nodes=this.rte.dom.toLineStart(H).concat(F.nodes);F.so=F.nodes[0].nodeType==1?null:0
}if(this.rte.dom.isInline(E)){F.nodes=F.nodes.concat(this.rte.dom.toLineEnd(E));F.eo=F.nodes[F.nodes.length-1].nodeType==1?null:F.nodes[F.nodes.length-1].nodeValue.length}}return F}}})(jQuery);(function(a){elRTE.prototype.ui=function(f){this.rte=f;this._buttons=[];var v=this,o=this.rte.options.toolbars[f.options.toolbar&&f.options.toolbars[f.options.toolbar]?f.options.toolbar:"normal"],r=o.length,g,k,j,h,t,u,q;for(q in this.buttons){if(this.buttons.hasOwnProperty(q)&&q!="button"){this.buttons[q].prototype=this.buttons.button.prototype
}}while(r--){first=(r==0?true:false);if(o[r-1]=="eol"){first=true}k=o[r];if(k=="eol"){a(this.rte.doc.createElement("br")).prependTo(this.rte.toolbar);continue}g=a('<ul class="panel-'+k+(first?" first":"")+'" />').prependTo(this.rte.toolbar);g.bind("mousedown",function(b){b.preventDefault()});j=this.rte.options.panels[k].length;while(j--){h=this.rte.options.panels[k][j];t=this.buttons[h]||this.buttons.button;this._buttons.push((u=new t(this.rte,h)));g.prepend(u.domElem)}}this.update();this.disable=function(){a.each(v._buttons,function(){!this.active&&this.domElem.addClass("disabled")
})}};elRTE.prototype.ui.prototype.update=function(h){h&&this.rte.selection.cleanCache();var f=this.rte.selection.getNode(),c=this.rte.dom.parents(f,"*"),j=this.rte.rtl,q=j?" &laquo; ":" &raquo; ",o="",b,g;function k(r){var p=r.nodeName.toLowerCase();r=a(r);if(p=="img"){if(r.hasClass("elrte-media")){p="media"}else{if(r.hasClass("elrte-google-maps")){p="google map"}else{if(r.hasClass("elrte-yandex-maps")){p="yandex map"}else{if(r.hasClass("elrte-pagebreak")){p="pagebreak"}}}}}return p}if(f&&f.nodeType==1&&f.nodeName!="BODY"){c.unshift(f)
}if(!j){c=c.reverse()}for(g=0;g<c.length;g++){o+=(g>0?q:"")+k(c[g])}this.rte.statusbar.html(o);a.each(this._buttons,function(){this.update()});this.rte.window.focus()};elRTE.prototype.ui.prototype.buttons={button:function(f,c){var b=this;this.rte=f;this.active=false;this.name=c;this.val=null;this.domElem=a('<li style="-moz-user-select:-moz-none" class="'+c+' rounded-3" name="'+c+'" title="'+this.rte.i18n(this.rte.options.buttons[c]||c)+'" unselectable="on" />').hover(function(){a(this).addClass("hover")
},function(){a(this).removeClass("hover")}).click(function(g){g.stopPropagation();g.preventDefault();if(!a(this).hasClass("disabled")){b.command()}b.rte.window.focus()})}};elRTE.prototype.ui.prototype.buttons.button.prototype.command=function(){this.rte.history.add();try{this.rte.doc.execCommand(this.name,false,this.val)}catch(b){return this.rte.log("commands failed: "+this.name)}this.rte.ui.update(true)};elRTE.prototype.ui.prototype.buttons.button.prototype.update=function(){try{if(!this.rte.doc.queryCommandEnabled(this.name)){return this.domElem.addClass("disabled")
}else{this.domElem.removeClass("disabled")}}catch(b){return}try{if(this.rte.doc.queryCommandState(this.name)){this.domElem.addClass("active")}else{this.domElem.removeClass("active")}}catch(b){}}})(jQuery);(function(a){elRTE.prototype.utils=function(c){this.rte=c;this.url=null;this.reg=/^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/;this.baseURL="";this.path="";
this.entities={"&":"&amp;",'"':"&quot;","<":"&lt;",">":"&gt;"};this.entitiesRegExp=/[<>&\"]/g;this.media=[{type:"application/x-shockwave-flash",classid:["clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"],codebase:"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0"},{type:"application/x-director",classid:["clsid:166b1bca-3f9c-11cf-8075-444553540000"],codebase:"http://download.macromedia.com/pub/shockwave/cabs/director/sw.cab#version=8,5,1,0"},{type:"application/x-mplayer2",classid:["clsid:6bf52a52-394a-11d3-b153-00c04f79faa6","clsid:22d6f312-b0f6-11d0-94ab-0080c74c7e95","clsid:05589fa1-c356-11ce-bf01-00aa0055595a"],codebase:"http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701"},{type:"video/quicktime",classid:["clsid:02bf25d5-8c17-4b23-bc80-d3488abddc6b"],codebase:"http://www.apple.com/qtactivex/qtplugin.cab#version=6,0,2,0"},{type:"audio/x-pn-realaudio-plugin",classid:["clsid:cfcdaa03-8be4-11cf-b84b-0020afbbccfa"],codebase:"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0"}];
this.rgbRegExp=/\s*rgb\s*?\(\s*?([0-9]+)\s*?,\s*?([0-9]+)\s*?,\s*?([0-9]+)\s*?\)\s*/i;this.colorsRegExp=/aqua|black|blue|fuchsia|gray|green|lime|maroon|navy|olive|orange|purple|red|silver|teal|white|yellow|rgb\s*\([^\)]+\)/i;this.colors={aqua:"#00ffff",black:"#000000",blue:"#0000ff",fuchsia:"#ff00ff",gray:"#808080",green:"#008000",lime:"#00ff00",maroon:"#800000",navy:"#000080",olive:"#808000",orange:"#ffa500",purple:"#800080",red:"#ff0000",silver:"#c0c0c0",teal:"#008080",white:"#fffffff",yellow:"#ffff00"};
var b=this;this.rgb2hex=function(f){return this.color2Hex(""+f)};this.toPixels=function(g){var f=g.match(/([0-9]+\.?[0-9]*)\s*(px|pt|em|%)/);if(f){g=f[1];unit=f[2]}if(g[0]=="."){g="0"+g}g=parseFloat(g);if(isNaN(g)){return""}var h=parseInt(a(document.body).css("font-size"))||16;switch(unit){case"em":return parseInt(g*h);case"pt":return parseInt(g*h/12);case"%":return parseInt(g*h/100)}return g};this.absoluteURL=function(g){!this.url&&this._url();g=a.trim(g);if(!g){return""}if(g[0]=="#"){return g}var f=this.parseURL(g);
if(!f.host&&!f.path&&!f.anchor){return""}if(!this.rte.options.absoluteURLs){return g}if(f.protocol){return g}if(f.host&&(f.host.indexOf(".")!=-1||f.host=="localhost")){return this.url.protocol+"://"+g}if(g[0]=="/"){g=this.baseURL+g}else{if(g.indexOf("./")==0){g=g.substring(2)}g=this.baseURL+this.path+g}return g};this.parseURL=function(h){var g=h.match(this.reg);var f={};a.each(["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],function(j){f[this]=g[j]
});if(!f.host.match(/[a-z0-9]/i)){f.host=""}return f};this.trimEventCallback=function(f){f=f?f.toString():"";return a.trim(f.replace(/\r*\n/mg,"").replace(/^function\s*on[a-z]+\s*\(\s*event\s*\)\s*\{(.+)\}$/igm,"$1"))};this._url=function(){this.url=this.parseURL(window.location.href);this.baseURL=this.url.protocol+"://"+(this.url.userInfo?parts.userInfo+"@":"")+this.url.host+(this.url.port?":"+this.url.port:"");this.path=!this.url.file?this.url.path:this.url.path.substring(0,this.url.path.length-this.url.file.length)
};this.makeObject=function(g){var f={};a.each(g,function(h,j){f[j]=j});return f};this.encode=function(f){var g=this.entities;return(""+f).replace(this.entitiesRegExp,function(h){return g[h]})};this.decode=function(f){return a("<div/>").html(f||"").text()};this.parseStyle=function(k){var h={},f=this.rte.options.allowBrowsersSpecStyles,j,q,g,o;if(typeof(k)=="string"&&k.length){a.each(k.replace(/&quot;/gi,"'").split(";"),function(p,r){if((o=r.indexOf(":"))!==-1){q=a.trim(r.substr(0,o));g=a.trim(r.substr(o+1));
if(q=="color"||q=="background-color"){g=g.toLowerCase()}if(q&&g&&(f||q.substring(0,1)!="-")){h[q]=g}}})}return h};this.compactStyle=function(g){var f=this;if(g.border=="medium none"){delete g.border}a.each(g,function(j,h){if(/color$/i.test(j)){g[j]=f.color2Hex(h)}else{if(/^(border|background)$/i.test(j)){g[j]=h.replace(f.colorsRegExp,function(k){return f.color2Hex(k)})}}});if(g["border-width"]){g.border=g["border-width"]+" "+(g["border-style"]||"solid")+" "+(g["border-color"]||"#000");delete g["border-width"];
delete g["border-style"];delete g["border-color"]}if(g["background-image"]){g.background=(g["background-color"]+" ")||""+g["background-image"]+" "+g["background-position"]||"0 0 "+g["background-repeat"]||"repeat";delete g["background-image"];delete ["background-image"];delete ["background-position"];delete ["background-repeat"]}if(g["margin-top"]&&g["margin-right"]&&g["margin-bottom"]&&g["margin-left"]){g.margin=g["margin-top"]+" "+g["margin-right"]+" "+g["margin-bottom"]+" "+g["margin-left"];delete g["margin-top"];
delete g["margin-right"];delete g["margin-bottom"];delete g["margin-left"]}if(g["padding-top"]&&g["padding-right"]&&g["padding-bottom"]&&g["padding-left"]){g.padding=g["padding-top"]+" "+g["padding-right"]+" "+g["padding-bottom"]+" "+g["padding-left"];delete g["padding-top"];delete g["padding-right"];delete g["padding-bottom"];delete g["padding-left"]}if(g["list-style-type"]||g["list-style-position"]||g["list-style-image"]){g["list-style"]=a.trim(g["list-style-type"]||" "+g["list-style-position"]||""+g["list-style-image"]||"");
delete g["list-style-type"];delete g["list-style-position"];delete g["list-style-image"]}return g};this.serializeStyle=function(g,h){var f=[];a.each(h?this.compactStyle(g):g,function(k,j){j&&f.push(k+":"+j)});return f.join(";")};this.parseClass=function(f){f=a.trim(f);return f.length?this.makeObject(f.split(/\s+/)):{};return f.length?f.split(/\s+/):[]};this.serializeClass=function(h){var g=[];var f=this.rte;a.each(h,function(j){g.push(j)});return g.join(" ")};this.mediaInfo=function(g,h){var f=this.media.length;
while(f--){if(g===this.media[f].type||(h&&a.inArray(h,this.media[f].classid)!=-1)){return this.media[f]}}};this.color2Hex=function(h){var f;h=h||"";if(h.indexOf("#")===0){return h}function g(j){j=parseInt(j).toString(16);return j.length>1?j:"0"+j}if(this.colors[h]){return this.colors[h]}if((f=h.match(this.rgbRegExp))){return"#"+g(f[1])+g(f[2])+g(f[3])}return""}}})(jQuery);(function(a){elRTE.prototype.w3cRange=function(c){var b=this;this.rte=c;this.r=null;this.collapsed=true;this.startContainer=null;
this.endContainer=null;this.startOffset=0;this.endOffset=0;this.commonAncestorContainer=null;this.range=function(){try{this.r=this.rte.window.document.selection.createRange()}catch(f){this.r=this.rte.doc.body.createTextRange()}return this.r};this.insertNode=function(f){this.range();b.r.collapse(false);var g=b.r.duplicate();g.pasteHTML(f)};this.getBookmark=function(){this.range();if(this.r.item){var f=this.r.item(0);this.r=this.rte.doc.body.createTextRange();this.r.moveToElementText(f)}return this.r.getBookmark()
};this.moveToBookmark=function(f){this.rte.window.focus();this.range().moveToBookmark(f);this.r.select()};this.update=function(){function h(x){var k="\uFEFF";var o=offset=0;var t=b.r.duplicate();t.collapse(x);var u=t.parentElement();if(!u||u.nodeName=="HTML"){return{parent:b.rte.doc.body,ndx:o,offset:offset}}t.pasteHTML(k);childs=u.childNodes;for(var q=0;q<childs.length;q++){var w=childs[q];if(q>0&&(w.nodeType!==3||childs[q-1].nodeType!==3)){o++}if(w.nodeType!==3){offset=0}else{var v=w.nodeValue.indexOf(k);
if(v!==-1){offset+=v;break}offset+=w.nodeValue.length}}t.moveStart("character",-1);t.text="";return{parent:u,ndx:Math.min(o,u.childNodes.length-1),offset:offset}}this.range();this.startContainer=this.endContainer=null;if(this.r.item){this.collapsed=false;var g=this.r.item(0);this.setStart(g.parentNode,this.rte.dom.indexOf(g));this.setEnd(g.parentNode,this.startOffset+1)}else{this.collapsed=this.r.boundingWidth==0;var j=h(true);var f=h(false);j.parent.normalize();f.parent.normalize();j.ndx=Math.min(j.ndx,j.parent.childNodes.length-1);
f.ndx=Math.min(f.ndx,f.parent.childNodes.length-1);if(j.parent.childNodes[j.ndx].nodeType&&j.parent.childNodes[j.ndx].nodeType==1){this.setStart(j.parent,j.ndx)}else{this.setStart(j.parent.childNodes[j.ndx],j.offset)}if(f.parent.childNodes[f.ndx].nodeType&&f.parent.childNodes[f.ndx].nodeType==1){this.setEnd(f.parent,f.ndx)}else{this.setEnd(f.parent.childNodes[f.ndx],f.offset)}this.select()}return this};this.isCollapsed=function(){this.range();this.collapsed=this.r.item?false:this.r.boundingWidth==0;
return this.collapsed};this.collapse=function(f){this.range();if(this.r.item){var g=this.r.item(0);this.r=this.rte.doc.body.createTextRange();this.r.moveToElementText(g)}this.r.collapse(f);this.r.select();this.collapsed=true};this.getStart=function(){this.range();if(this.r.item){return this.r.item(0)}var g=this.r.duplicate();g.collapse(true);var f=g.parentElement();return f&&f.nodeName=="BODY"?f.firstChild:f};this.getEnd=function(){this.range();if(this.r.item){return this.r.item(0)}var f=this.r.duplicate();
f.collapse(false);var g=f.parentElement();return g&&g.nodeName=="BODY"?g.lastChild:g};this.setStart=function(f,g){this.startContainer=f;this.startOffset=g;if(this.endContainer){this.commonAncestorContainer=this.rte.dom.findCommonAncestor(this.startContainer,this.endContainer)}};this.setEnd=function(f,g){this.endContainer=f;this.endOffset=g;if(this.startContainer){this.commonAncestorContainer=this.rte.dom.findCommonAncestor(this.startContainer,this.endContainer)}};this.setStartBefore=function(f){if(f.parentNode){this.setStart(f.parentNode,this.rte.dom.indexOf(f))
}};this.setStartAfter=function(f){if(f.parentNode){this.setStart(f.parentNode,this.rte.dom.indexOf(f)+1)}};this.setEndBefore=function(f){if(f.parentNode){this.setEnd(f.parentNode,this.rte.dom.indexOf(f))}};this.setEndAfter=function(f){if(f.parentNode){this.setEnd(f.parentNode,this.rte.dom.indexOf(f)+1)}};this.select=function(){function o(B,z){if(B.nodeType!=3){return -1}var A="\uFEFF";var x=B.nodeValue;var v=b.rte.doc.body.createTextRange();B.nodeValue=x.substring(0,z)+A+x.substring(z);v.moveToElementText(B.parentNode);
v.findText(A);var w=Math.abs(v.moveStart("character",-1048575));B.nodeValue=x;return w}this.r=this.rte.doc.body.createTextRange();var k=this.startOffset;var g=this.endOffset;var u=this.startContainer.nodeType==1?this.startContainer.childNodes[Math.min(k,this.startContainer.childNodes.length-1)]:this.startContainer;var q=this.endContainer.nodeType==1?this.endContainer.childNodes[Math.min(k==g?g:g-1,this.endContainer.childNodes.length-1)]:this.endContainer;if(this.collapsed){if(u.nodeType==3){var h=o(u,k);
this.r.move("character",h)}else{this.r.moveToElementText(u);this.r.collapse(true)}}else{var f=this.rte.doc.body.createTextRange();var j=o(u,k);var t=o(q,g);if(u.nodeType==3){this.r.move("character",j)}else{this.r.moveToElementText(u)}if(q.nodeType==3){f.move("character",t)}else{f.moveToElementText(q)}this.r.setEndPoint("EndToEnd",f)}try{this.r.select()}catch(q){}if(f){f=null}};this.dump=function(){this.rte.log("collapsed: "+this.collapsed);this.rte.log("startContainer: "+(this.startContainer?this.startContainer.nodeName:"non"));
this.rte.log("startOffset: "+this.startOffset);this.rte.log("endContainer: "+(this.endContainer?this.endContainer.nodeName:"none"));this.rte.log("endOffset: "+this.endOffset)}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.about=function(c,b){this.constructor.prototype.constructor.call(this,c,b);this.active=true;this.command=function(){var g,h,f;g={rtl:c.rtl,submit:function(j,k){k.close()},dialog:{width:560,title:this.rte.i18n("About this software"),buttons:{Ok:function(){a(this).dialog("destroy")
}}}};f='<div class="elrte-logo"></div><h3>'+this.rte.i18n("About elRTE")+'</h3><br clear="all"/><div class="elrte-ver">'+this.rte.i18n("Version")+": "+this.rte.version+" ("+this.rte.build+')</div><div class="elrte-ver">jQuery: '+a("<div/>").jquery+'</div><div class="elrte-ver">jQueryUI: '+a.ui.version+'</div><div class="elrte-ver">'+this.rte.i18n("Licence")+": BSD Licence</div><p>"+this.rte.i18n("elRTE is an open-source JavaScript based WYSIWYG HTML-editor.")+"<br/>"+this.rte.i18n("Main goal of the editor - simplify work with text and formating (HTML) on sites, blogs, forums and other online services.")+"<br/>"+this.rte.i18n("You can use it in any commercial or non-commercial projects.")+"</p><h4>"+this.rte.i18n("Authors")+'</h4><table class="elrte-authors"><tr><td>Dmitry (dio) Levashov &lt;dio@std42.ru&gt;</td><td>'+this.rte.i18n("Chief developer")+"</td></tr><tr><td>Troex Nevelin &lt;troex@fury.scancode.ru&gt;</td><td>"+this.rte.i18n("Developer, tech support")+"</td></tr><tr><td>Valentin Razumnyh &lt;content@std42.ru&gt;</td><td>"+this.rte.i18n("Interface designer")+"</td></tr><tr><td>Tawfek Daghistani &lt;tawfekov@gmail.com&gt;</td><td>"+this.rte.i18n("RTL support")+"</td></tr>"+(this.rte.options.lang!="en"?"<tr><td>"+this.rte.i18n("_translator")+"</td><td>"+this.rte.i18n("_translation")+"</td></tr>":"")+'</table><div class="elrte-copy">Copyright &copy; 2009-2011, <a href="http://www.std42.ru">Studio 42</a></div><div class="elrte-copy">'+this.rte.i18n("For more information about this software visit the")+' <a href="http://elrte.org">'+this.rte.i18n("elRTE website")+'.</a></div><div class="elrte-copy">Twitter: <a href="http://twitter.com/elrte_elfinder">elrte_elfinder</a></div>';
h=new elDialogForm(g);h.append(f);h.open()};this.update=function(){this.domElem.removeClass("disabled")}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.anchor=function(f,c){this.constructor.prototype.constructor.call(this,f,c);this.input=a('<input type="text" />').attr("name","anchor").attr("size","16");var b=this;this.command=function(){var g={rtl:this.rte.rtl,submit:function(j,k){j.stopPropagation();j.preventDefault();k.close();b.set()},dialog:{title:this.rte.i18n("Bookmark")}};this.anchor=this.rte.dom.selfOrParentAnchor(this.rte.selection.getEnd())||f.dom.create("a");
!this.rte.selection.collapsed()&&this.rte.selection.collapse(false);this.input.val(a(this.anchor).addClass("elrte-anchor").attr("name"));this.rte.selection.saveIERange();var h=new elDialogForm(g);h.append([this.rte.i18n("Bookmark name"),this.input],null,true).open();setTimeout(function(){b.input.focus()},20)};this.update=function(){var g=this.rte.selection.getNode();if(this.rte.dom.selfOrParentLink(g)){this.domElem.addClass("disabled")}else{if(this.rte.dom.selfOrParentAnchor(g)){this.domElem.removeClass("disabled").addClass("active")
}else{this.domElem.removeClass("disabled").removeClass("active")}}};this.set=function(){var g=a.trim(this.input.val());if(g){this.rte.history.add();if(!this.anchor.parentNode){this.rte.selection.insertHtml('<a name="'+g+'" title="'+this.rte.i18n("Bookmark")+": "+g+'" class="elrte-anchor"></a>')}else{this.anchor.name=g;this.anchor.title=this.rte.i18n("Bookmark")+": "+g}}else{if(this.anchor.parentNode){this.rte.history.add();this.anchor.parentNode.removeChild(this.anchor)}}}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.blockquote=function(c,b){this.constructor.prototype.constructor.call(this,c,b);
this.command=function(){var g,f;this.rte.history.add();if(this.rte.selection.collapsed()&&(g=this.rte.dom.selfOrParent(this.rte.selection.getNode(),/^BLOCKQUOTE$/))){a(g).replaceWith(a(g).html())}else{f=this.rte.selection.selected({wrap:"all",tag:"blockquote"});f.length&&this.rte.selection.select(f[0],f[f.length-1])}this.rte.ui.update(true)};this.update=function(){if(this.rte.selection.collapsed()){if(this.rte.dom.selfOrParent(this.rte.selection.getNode(),/^BLOCKQUOTE$/)){this.domElem.removeClass("disabled").addClass("active")
}else{this.domElem.addClass("disabled").removeClass("active")}}else{this.domElem.removeClass("disabled active")}}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.copy=function(c,b){this.constructor.prototype.constructor.call(this,c,b);this.command=function(){if(this.rte.browser.mozilla){try{this.rte.doc.execCommand(this.name,false,null)}catch(h){var f=" Ctl + C";if(this.name=="cut"){f=" Ctl + X"}else{if(this.name=="paste"){f=" Ctl + V"}}var g={dialog:{title:this.rte.i18n("Warning"),buttons:{Ok:function(){a(this).dialog("close")
}}}};var j=new elDialogForm(g);j.append(this.rte.i18n("This operation is disabled in your browser on security reason. Use shortcut instead.")+": "+f).open()}}else{this.constructor.prototype.command.call(this)}}};elRTE.prototype.ui.prototype.buttons.cut=elRTE.prototype.ui.prototype.buttons.copy;elRTE.prototype.ui.prototype.buttons.paste=elRTE.prototype.ui.prototype.buttons.copy})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.css=function(f,c){var b=this;this.constructor.prototype.constructor.call(this,f,c);
this.cssStyle=a('<input type="text" size="42" name="style" />');this.cssClass=a('<input type="text" size="42" name="class" />');this.elementID=a('<input type="text" size="42" name="id" />');this.command=function(){var j=this.node(),g;this.rte.selection.saveIERange();if(j){var g={submit:function(k,o){k.stopPropagation();k.preventDefault();o.close();b.set()},dialog:{title:this.rte.i18n("Style"),width:450,resizable:true,modal:true}};this.cssStyle.val(a(j).attr("style"));this.cssClass.val(a(j).attr("class"));
this.elementID.val(a(j).attr("id"));var h=new elDialogForm(g);h.append([this.rte.i18n("Css style"),this.cssStyle],null,true);h.append([this.rte.i18n("Css class"),this.cssClass],null,true);h.append([this.rte.i18n("ID"),this.elementID],null,true);h.open();setTimeout(function(){b.cssStyle.focus()},20)}};this.set=function(){var g=this.node();this.rte.selection.restoreIERange();if(g){a(g).attr("style",this.cssStyle.val());a(g).attr("class",this.cssClass.val());a(g).attr("id",this.elementID.val());this.rte.ui.update()
}};this.node=function(){var g=this.rte.selection.getNode();if(g.nodeType==3){g=g.parentNode}return g.nodeType==1&&g.nodeName!="BODY"?g:null};this.update=function(){this.domElem.toggleClass("disabled",this.node()?false:true)}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.rtl=function(f,c){this.constructor.prototype.constructor.call(this,f,c);var b=this;this.command=function(){var h=this.rte.selection.getNode(),g=this;if(a(h).attr("dir")=="rtl"||a(h).parents('[dir="rtl"]').length||a(h).find('[dir="rtl"]').length){a(h).removeAttr("dir");
a(h).parents('[dir="rtl"]').removeAttr("dir");a(h).find('[dir="rtl"]').removeAttr("dir")}else{if(this.rte.dom.is(h,"textNodes")&&this.rte.dom.is(h,"block")){a(h).attr("dir","rtl")}else{a.each(this.rte.dom.parents(h,"textNodes"),function(j,k){if(g.rte.dom.is(k,"block")){a(k).attr("dir","rtl");return false}})}}this.rte.ui.update()};this.update=function(){var g=this.rte.selection.getNode();this.domElem.removeClass("disabled");if(a(g).attr("dir")=="rtl"||a(g).parents('[dir="rtl"]').length||a(g).find('[dir="rtl"]').length){this.domElem.addClass("active")
}else{this.domElem.removeClass("active")}}};elRTE.prototype.ui.prototype.buttons.ltr=function(f,c){this.constructor.prototype.constructor.call(this,f,c);var b=this;this.command=function(){var h=this.rte.selection.getNode(),g=this;if(a(h).attr("dir")=="ltr"||a(h).parents('[dir="ltr"]').length||a(h).find('[dir="ltr"]').length){a(h).removeAttr("dir");a(h).parents('[dir="ltr"]').removeAttr("dir");a(h).find('[dir="ltr"]').removeAttr("dir")}else{if(this.rte.dom.is(h,"textNodes")&&this.rte.dom.is(h,"block")){a(h).attr("dir","ltr")
}else{a.each(this.rte.dom.parents(h,"textNodes"),function(j,k){if(g.rte.dom.is(k,"block")){a(k).attr("dir","ltr");return false}})}}this.rte.ui.update()};this.update=function(){var g=this.rte.selection.getNode();this.domElem.removeClass("disabled");if(a(g).attr("dir")=="ltr"||a(g).parents('[dir="ltr"]').length||a(g).find('[dir="ltr"]').length){this.domElem.addClass("active")}else{this.domElem.removeClass("active")}}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.div=function(c,b){this.constructor.prototype.constructor.call(this,c,b);
this.command=function(){var g,f;this.rte.history.add();if(this.rte.selection.collapsed()){g=this.rte.dom.selfOrParent(this.rte.selection.getNode(),/^DIV$/);if(g){a(g).replaceWith(a(g).html())}}else{f=this.rte.selection.selected({wrap:"all",tag:"div"});f.length&&this.rte.selection.select(f[0],f[f.length-1])}this.rte.ui.update(true)};this.update=function(){if(this.rte.selection.collapsed()){if(this.rte.dom.selfOrParent(this.rte.selection.getNode(),/^DIV$/)){this.domElem.removeClass("disabled").addClass("active")
}else{this.domElem.addClass("disabled active")}}else{this.domElem.removeClass("disabled active")}}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.docstructure=function(c,b){this.constructor.prototype.constructor.call(this,c,b);this.command=function(){this.domElem.toggleClass("active");a(this.rte.doc.body).toggleClass("el-rte-structure")};this.command();this.update=function(){this.domElem.removeClass("disabled")}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.elfinder=function(f,c){this.constructor.prototype.constructor.call(this,f,c);
var b=this,f=this.rte;this.command=function(){if(b.rte.options.fmAllow&&typeof(b.rte.options.fmOpen)=="function"){b.rte.options.fmOpen(function(h){var g=decodeURIComponent(h.split("/").pop().replace(/\+/g," "));if(f.selection.collapsed()){f.selection.insertHtml('<a href="'+h+'" >'+g+"</a>")}else{f.doc.execCommand("createLink",false,h)}})}};this.update=function(){if(b.rte.options.fmAllow&&typeof(b.rte.options.fmOpen)=="function"){this.domElem.removeClass("disabled")}else{this.domElem.addClass("disabled")
}}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.flash=function(f,c){this.constructor.prototype.constructor.call(this,f,c);var b=this;this.swf=null;this.placeholder=null;this.src={url:a('<input type="text" name="url" />').css("width","99%"),type:a('<select name="type"/>').append('<option value="application/x-shockwave-flash">Flash</option>').append('<option value="video/quicktime">Quicktime movie</option>').append('<option value="application/x-mplayer2">Windows media</option>'),width:a('<input type="text" />').attr("size",5).css("text-align","right"),height:a('<input type="text" />').attr("size",5).css("text-align","right"),wmode:a("<select />").append(a("<option />").val("").text(this.rte.i18n("Not set","dialogs"))).append(a("<option />").val("transparent").text(this.rte.i18n("Transparent"))),align:a("<select />").append(a("<option />").val("").text(this.rte.i18n("Not set","dialogs"))).append(a("<option />").val("left").text(this.rte.i18n("Left"))).append(a("<option />").val("right").text(this.rte.i18n("Right"))).append(a("<option />").val("top").text(this.rte.i18n("Top"))).append(a("<option />").val("text-top").text(this.rte.i18n("Text top"))).append(a("<option />").val("middle").text(this.rte.i18n("middle"))).append(a("<option />").val("baseline").text(this.rte.i18n("Baseline"))).append(a("<option />").val("bottom").text(this.rte.i18n("Bottom"))).append(a("<option />").val("text-bottom").text(this.rte.i18n("Text bottom"))),margin:a("<div />")};
this.command=function(){var r=this.rte.selection.getEnd(),j,k="",B="",t="",v,z,x,A,q,C;this.rte.selection.saveIERange();this.src.margin.elPaddingInput({type:"margin"});this.placeholder=null;this.swf=null;if(a(r).hasClass("elrte-media")&&(A=a(r).attr("rel"))&&this.rte.filter.scripts[A]){this.placeholder=a(r);q=this.rte.filter.scripts[A];k="";if(q.embed&&q.embed.src){k=q.embed.src}if(q.params&&q.params.length){l=q.params.length;while(l--){if(q.params[l].name=="src"||q.params[l].name=="movie"){k=q.params[l].value
}}}if(q.embed){B=q.embed.width||parseInt(q.embed.style.width)||"";t=q.embed.height||parseInt(q.embed.style.height)||"";C=q.embed.wmode||""}else{if(q.obj){B=q.obj.width||parseInt(q.obj.style.width)||"";t=q.obj.height||parseInt(q.obj.style.height)||"";C=q.obj.wmode||""}}if(q.obj){v=q.obj.style["float"]||"";z=q.obj.style["vertical-align"]||""}else{if(q.embed){v=q.embed.style["float"]||"";z=q.embed.style["vertical-align"]||""}}this.src.margin.val(r);this.src.type.val(q.embed?q.embed.type:"")}if(a(r).hasClass("elrte-swf-placeholder")){this.placeholder=a(r);
k=a(r).attr("rel");B=parseInt(a(r).css("width"))||"";t=parseInt(a(r).css("height"))||"";v=a(r).css("float");z=a(r).css("vertical-align");this.src.margin.val(r);this.src.wmode.val(a(r).attr("wmode"))}this.src.url.val(k);this.src.width.val(B);this.src.height.val(t);this.src.align.val(v||z);this.src.wmode.val(C);var j={rtl:this.rte.rtl,submit:function(h,o){h.stopPropagation();h.preventDefault();b.set();o.close()},dialog:{width:580,position:"top",title:this.rte.i18n("Flash")}};var x=new elDialogForm(j);
if(this.rte.options.fmAllow&&this.rte.options.fmOpen){var g=a("<span />").append(this.src.url.css("width","85%")).append(a("<span />").addClass("ui-state-default ui-corner-all").css({"float":"right","margin-right":"3px"}).attr("title",b.rte.i18n("Open file manger")).append(a("<span />").addClass("ui-icon ui-icon-folder-open")).click(function(){b.rte.options.fmOpen(function(h){b.src.url.val(h).change()})}).hover(function(){a(this).addClass("ui-state-hover")},function(){a(this).removeClass("ui-state-hover")
}))}else{var g=this.src.url}x.append([this.rte.i18n("URL"),g],null,true);x.append([this.rte.i18n("Type"),this.src.type],null,true);x.append([this.rte.i18n("Size"),a("<span />").append(this.src.width).append(" x ").append(this.src.height).append(" px")],null,true);x.append([this.rte.i18n("Wmode"),this.src.wmode],null,true);x.append([this.rte.i18n("Alignment"),this.src.align],null,true);x.append([this.rte.i18n("Margins"),this.src.margin],null,true);x.open();var u=a("<fieldset />").append(a("<legend />").text(this.rte.i18n("Preview")));
x.append(u,"main");var p=document.createElement("iframe");a(p).attr("src","#").addClass("el-rte-preview").appendTo(u);html=this.rte.options.doctype+'<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /></head><body style="padding:0;margin:0;font-size:9px"> Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin</body></html>';
p.contentWindow.document.open();p.contentWindow.document.write(html);p.contentWindow.document.close();this.frame=p.contentWindow.document;this.preview=a(p.contentWindow.document.body);this.src.type.change(function(){b.src.url.change()});this.src.width.change(function(){if(b.swf){var h=parseInt(a(this).val())||"";a(this).val(h);b.swf.css("width",h);b.swf.children("embed").css("width",h)}else{a(this).val("")}});this.src.height.change(function(){if(b.swf){var o=parseInt(a(this).val())||"";a(this).val(o);
b.swf.css("height",o);b.swf.children("embed").css("height",o)}else{a(this).val("")}});this.src.wmode.change(function(){if(b.swf){var h=a(this).val();if(h){b.swf.attr("wmode",h);b.swf.children("embed").attr("wmode",h)}else{b.swf.removeAttr("wmode");b.swf.children("embed").removeAttr("wmode")}}});this.src.align.change(function(){var h=a(this).val(),o=h=="left"||h=="right";if(b.swf){b.swf.css({"float":o?h:"","vertical-align":o?"":h})}else{a(this).val("")}});this.src.margin.change(function(){if(b.swf){var h=b.src.margin.val();
if(h.css){b.swf.css("margin",h.css)}else{b.swf.css("margin-top",h.top);b.swf.css("margin-right",h.right);b.swf.css("margin-bottom",h.bottom);b.swf.css("margin-left",h.left)}}});this.src.url.change(function(){var h=b.rte.utils.absoluteURL(a(this).val()),o,w;if(h){o=b.rte.utils.mediaInfo(b.src.type.val());if(!o){o=b.rte.util.mediaInfo("application/x-shockwave-flash")}w='<object classid="'+o.classid+'" codebase="'+o.codebase+'"><param name="src" value="'+h+'" /><embed quality="high" src="'+h+'" type="'+o.type+'"></object>';
b.preview.children("object").remove().end().prepend(w);b.swf=b.preview.children("object").eq(0)}else{if(b.swf){b.swf.remove();b.swf=null}}b.src.width.trigger("change");b.src.height.trigger("change");b.src.align.trigger("change")}).trigger("change")};this.set=function(){b.swf=null;var g=this.rte.utils.absoluteURL(this.src.url.val()),B=parseInt(this.src.width.val())||"",t=parseInt(this.src.height.val())||"",C=this.src.wmode.val(),z=this.src.align.val(),v=z=="left"||z=="right"?z:"",A=this.placeholder?this.placeholder.attr("rel"):"",p,k,x,q=this.src.margin.val(),r;
if(!g){if(this.placeholder){this.placeholder.remove();delete this.rte.filter.scripts[A]}}else{i=b.rte.utils.mediaInfo(b.src.type.val());if(!i){i=b.rte.util.mediaInfo("application/x-shockwave-flash")}x=this.rte.filter.videoHostRegExp.test(g)?g.replace(this.rte.filter.videoHostRegExp,"$2"):i.type.replace(/^\w+\/(.+)/,"$1");p={obj:{classid:i.classid[0],codebase:i.codebase,style:{}},params:[{name:"src",value:g}],embed:{src:g,type:i.type,quality:"high",wmode:C,style:{}}};if(B){p.obj.width=B;p.embed.width=B
}if(t){p.obj.height=t;p.embed.height=t}if(v){p.obj.style["float"]=v}else{if(z){p.obj.style["vertical-align"]=z}}if(q.css){r={margin:q.css}}else{r={"margin-top":q.top,"margin-right":q.right,"margin-bottom":q.bottom,"margin-left":q.left}}p.obj.style=a.extend({},p.obj.style,r);if(this.placeholder&&A){k=this.rte.filter.scripts[A]||{};p=a.extend(true,k,p);delete p.obj.style.width;delete p.obj.style.height;delete p.embed.style.width;delete p.embed.style.height;this.rte.filter.scripts[A]=p;this.placeholder.removeAttr("class")
}else{var j="media"+Math.random().toString().substring(2);this.rte.filter.scripts[j]=p;this.placeholder=a(this.rte.dom.create("img")).attr("rel",j).attr("src",this.rte.filter.url+"pixel.gif");var u=true}this.placeholder.attr("title",this.rte.utils.encode(g)).attr("width",B||150).attr("height",t||100).addClass("elrte-protected elrte-media elrte-media-"+x).css(p.obj.style);if(v){this.placeholder.css("float",v).css("vertical-align","")}else{if(z){this.placeholder.css("float","").css("vertical-align",z)
}else{this.placeholder.css("float","").css("vertical-align","")}}if(u){this.rte.window.focus();this.rte.selection.restoreIERange();this.rte.selection.insertNode(this.placeholder.get(0))}}};this.update=function(){this.domElem.removeClass("disabled");var g=this.rte.selection.getNode();this.domElem.toggleClass("active",g&&g.nodeName=="IMG"&&a(g).hasClass("elrte-media"))}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.fontname=function(f,c){this.constructor.prototype.constructor.call(this,f,c);
var b=this;var g={tpl:'<span style="font-family:%val">%label</span>',select:function(h){b.set(h)},src:{"":this.rte.i18n("Font"),"andale mono,sans-serif":"Andale Mono","arial,helvetica,sans-serif":"Arial","arial black,gadget,sans-serif":"Arial Black","book antiqua,palatino,sans-serif":"Book Antiqua","comic sans ms,cursive":"Comic Sans MS","courier new,courier,monospace":"Courier New","georgia,palatino,serif":"Georgia","helvetica,sans-serif":"Helvetica","impact,sans-serif":"Impact","lucida console,monaco,monospace":"Lucida console","lucida sans unicode,lucida grande,sans-serif":"Lucida grande","tahoma,sans-serif":"Tahoma","times new roman,times,serif":"Times New Roman","trebuchet ms,lucida grande,verdana,sans-serif":"Trebuchet MS","verdana,geneva,sans-serif":"Verdana"}};
this.select=this.domElem.elSelect(g);this.command=function(){};this.set=function(j){this.rte.history.add();var h=this.rte.selection.selected({filter:"textContainsNodes"});a.each(h,function(){$this=/^(THEAD|TFOOT|TBODY|COL|COLGROUP|TR)$/.test(this.nodeName)?a(this).find("td,th"):a(this);a(this).css("font-family",j).find("[style]").css("font-family","")});this.rte.ui.update()};this.update=function(){this.domElem.removeClass("disabled");var j=this.rte.selection.getNode();if(j.nodeType!=1){j=j.parentNode
}var h=a(j).css("font-family");h=h?h.toString().toLowerCase().replace(/,\s+/g,",").replace(/'|"/g,""):"";this.select.val(g.src[h]?h:"")}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.fontsize=function(f,c){this.constructor.prototype.constructor.call(this,f,c);var b=this;var g={labelTpl:"%label",tpl:'<span style="font-size:%val;line-height:1.2em">%label</span>',select:function(h){b.set(h)},src:{"":this.rte.i18n("Font size"),"xx-small":this.rte.i18n("Small (8pt)"),"x-small":this.rte.i18n("Small (10px)"),small:this.rte.i18n("Small (12pt)"),medium:this.rte.i18n("Normal (14pt)"),large:this.rte.i18n("Large (18pt)"),"x-large":this.rte.i18n("Large (24pt)"),"xx-large":this.rte.i18n("Large (36pt)")}};
this.select=this.domElem.elSelect(g);this.command=function(){};this.set=function(j){this.rte.history.add();var h=this.rte.selection.selected({filter:"textContainsNodes"});a.each(h,function(){$this=/^(THEAD|TFOOT|TBODY|COL|COLGROUP|TR)$/.test(this.nodeName)?a(this).find("td,th"):a(this);$this.css("font-size",j).find("[style]").css("font-size","")});this.rte.ui.update()};this.update=function(){this.domElem.removeClass("disabled");var h=this.rte.selection.getNode();this.select.val((m=this.rte.dom.attr(h,"style").match(/font-size:\s*([^;]+)/i))?m[1]:"")
}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.forecolor=function(f,c){var b=this;this.constructor.prototype.constructor.call(this,f,c);var g={"class":"",palettePosition:"outer",color:this.defaultColor,update:function(h){b.indicator.css("background-color",h)},change:function(h){b.set(h)}};this.defaultColor=this.name=="forecolor"?"#000000":"#ffffff";this.picker=this.domElem.elColorPicker(g);this.indicator=a("<div />").addClass("color-indicator").prependTo(this.domElem);this.command=function(){};
this.set=function(k){if(!this.rte.selection.collapsed()){this.rte.history.add();var h=this.rte.selection.selected({collapse:false,wrap:"text"}),j=this.name=="forecolor"?"color":"background-color";a.each(h,function(){if(/^(THEAD|TBODY|TFOOT|TR)$/.test(this.nodeName)){a(this).find("td,th").each(function(){a(this).css(j,k).find("*").css(j,"")})}else{a(this).css(j,k).find("*").css(j,"")}});this.rte.ui.update(true)}};this.update=function(){this.domElem.removeClass("disabled");var h=this.rte.selection.getNode();
this.picker.val(this.rte.utils.rgb2hex(a(h.nodeType!=1?h.parentNode:h).css(this.name=="forecolor"?"color":"background-color"))||this.defaultColor)}};elRTE.prototype.ui.prototype.buttons.hilitecolor=elRTE.prototype.ui.prototype.buttons.forecolor})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.formatblock=function(f,c){this.constructor.prototype.constructor.call(this,f,c);var h=this.rte.browser.msie?function(j){b.val=j;b.constructor.prototype.command.call(b)}:function(j){b.ieCommand(j)};
var b=this;var g={labelTpl:"%label",tpls:{"":"%label"},select:function(j){b.formatBlock(j)},src:{span:this.rte.i18n("Format"),h1:this.rte.i18n("Heading 1"),h2:this.rte.i18n("Heading 2"),h3:this.rte.i18n("Heading 3"),h4:this.rte.i18n("Heading 4"),h5:this.rte.i18n("Heading 5"),h6:this.rte.i18n("Heading 6"),p:this.rte.i18n("Paragraph"),address:this.rte.i18n("Address"),pre:this.rte.i18n("Preformatted"),div:this.rte.i18n("Normal (DIV)")}};this.select=this.domElem.elSelect(g);this.command=function(){};
this.formatBlock=function(u){function t(z,v){function x(A){a(A).find("h1,h2,h3,h4,h5,h6,p,address,pre").each(function(){a(this).replaceWith(a(this).html())});return A}if(/^(LI|DT|DD|TD|TH|CAPTION)$/.test(z.nodeName)){!b.rte.dom.isEmpty(z)&&b.rte.dom.wrapContents(x(z),v)}else{if(/^(UL|OL|DL|TABLE)$/.test(z.nodeName)){b.rte.dom.wrap(z,v)}else{!b.rte.dom.isEmpty(z)&&a(x(z)).replaceWith(a(b.rte.dom.create(v)).html(a(z).html()))}}}this.rte.history.add();var w=u.toUpperCase(),p,o,r,q=this.rte.selection.collapsed(),k=this.rte.selection.getBookmark(),j=this.rte.selection.selected({collapsed:true,blocks:true,filter:"textContainsNodes",wrap:"inline",tag:"span"});
l=j.length,s=a(j[0]).prev(),e=a(j[j.length-1]).next();while(l--){o=j[l];r=a(o);if(w=="DIV"||w=="SPAN"){if(/^(H[1-6]|P|ADDRESS|PRE)$/.test(o.nodeName)){r.replaceWith(a(this.rte.dom.create("div")).html(r.html()||""))}}else{if(/^(THEAD|TBODY|TFOOT|TR)$/.test(o.nodeName)){r.find("td,th").each(function(){t(this,w)})}else{if(o.nodeName!=w){t(o,w)}}}}this.rte.selection.moveToBookmark(k);this.rte.ui.update(true)};this.update=function(){this.domElem.removeClass("disabled");var j=this.rte.dom.selfOrParent(this.rte.selection.getNode(),/^(H[1-6]|P|ADDRESS|PRE)$/);
this.select.val(j?j.nodeName.toLowerCase():"span")}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.fullscreen=function(g,f){var b=this;this.constructor.prototype.constructor.call(this,g,f);this.active=true;this.editor=g.editor;this.wz=g.workzone;this.height=0;this.delta=0;this._class="el-fullscreen";setTimeout(function(){b.height=b.wz.height();b.delta=b.editor.outerHeight()-b.height},50);function c(){b.wz.height(a(window).height()-b.delta);b.rte.updateHeight()}this.command=function(){var B=a(window),u=this.editor,k=u.parents().filter(function(h,p){return !/^(html|body)$/i.test(p.nodeName)&&a(p).css("position")=="relative"
}),q=this.wz,z=this._class,t=u.hasClass(z),j=this.rte,C=this.rte.selection,o=a.browser.mozilla,A,r;function x(){if(o){A=C.getBookmark()}}function v(){if(o){b.wz.children().toggle();b.rte.source.focus();b.wz.children().toggle();C.moveToBookmark(A)}}x();k.css("position",t?"relative":"static");if(t){u.removeClass(z);q.height(this.height);B.unbind("resize",c);this.domElem.removeClass("active")}else{u.addClass(z).removeAttr("style");q.height(B.height()-this.delta).css("width","100%");B.bind("resize",c);
this.domElem.addClass("active")}j.updateHeight();j.resizable(t);v()};this.update=function(){this.domElem.removeClass("disabled")}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.horizontalrule=function(f,c){this.constructor.prototype.constructor.call(this,f,c);var b=this;this.src={width:a('<input type="text" />').attr({name:"width",size:4}).css("text-align","right"),wunit:a("<select />").attr("name","wunit").append(a("<option />").val("%").text("%")).append(a("<option />").val("px").text("px")).val("%"),height:a('<input type="text" />').attr({name:"height",size:4}).css("text-align","right"),bg:a("<div />"),border:a("<div />"),"class":a('<input type="text" />').css("width","100%"),style:a('<input type="text" />').css("width","100%")};
this.command=function(){this.src.bg.elColorPicker({palettePosition:"outer","class":"el-colorpicker ui-icon ui-icon-pencil"});var k=this.rte.selection.getEnd();this.hr=k.nodeName=="HR"?a(k):a(f.doc.createElement("hr")).css({width:"100%",height:"1px"});this.src.border.elBorderSelect({styleHeight:73,value:this.hr});var g=this.hr.css("width")||this.hr.attr("width");this.src.width.val(parseInt(g)||100);this.src.wunit.val(g.indexOf("px")!=-1?"px":"%");this.src.height.val(this.rte.utils.toPixels(this.hr.css("height")||this.hr.attr("height"))||1);
this.src.bg.val(this.rte.utils.color2Hex(this.hr.css("background-color")));this.src["class"].val(this.rte.dom.attr(this.hr,"class"));this.src.style.val(this.rte.dom.attr(this.hr,"style"));var h={rtl:this.rte.rtl,submit:function(o,p){o.stopPropagation();o.preventDefault();b.set();p.close()},dialog:{title:this.rte.i18n("Horizontal rule")}};var j=new elDialogForm(h);j.append([this.rte.i18n("Width"),a("<span />").append(this.src.width).append(this.src.wunit)],null,true).append([this.rte.i18n("Height"),a("<span />").append(this.src.height).append(" px")],null,true).append([this.rte.i18n("Border"),this.src.border],null,true).append([this.rte.i18n("Background"),this.src.bg],null,true).append([this.rte.i18n("Css class"),this.src["class"]],null,true).append([this.rte.i18n("Css style"),this.src.style],null,true).open()
};this.update=function(){this.domElem.removeClass("disabled");if(this.rte.selection.getEnd().nodeName=="HR"){this.domElem.addClass("active")}else{this.domElem.removeClass("active")}};this.set=function(){this.rte.history.add();!this.hr.parentNode&&this.rte.selection.insertNode(this.hr.get(0));var h={noshade:true,style:this.src.style.val()};var g=this.src.border.val();var j={width:(parseInt(this.src.width.val())||100)+this.src.wunit.val(),height:parseInt(this.src.height.val())||1,"background-color":this.src.bg.val(),border:g.width&&g.style?g.width+" "+g.style+" "+g.color:""};
this.hr.removeAttr("class").removeAttr("style").removeAttr("width").removeAttr("height").removeAttr("align").attr(h).css(j);if(this.src["class"].val()){this.hr.attr("class",this.src["class"].val())}this.rte.ui.update()}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.image=function(b,c){this.constructor.prototype.constructor.call(this,b,c);var q=this,b=q.rte,h=0,f=0,p=0,g=null,j=function(t){a.each(q.src,function(u,v){a.each(v,function(x,w){if(x=="src"&&t){return}w.val("")})})},o=function(t){a.each(q.src,function(u,v){a.each(v,function(E,B){var C,x,D,A,z;
if(E=="width"){C=t.width()}else{if(E=="height"){C=t.height()}else{if(E=="border"){C="";z=t.css("border")||b.utils.parseStyle(t.attr("style")).border||"";if(z){x=z.match(/(\d(px|em|%))/);D=z.match(/(#[a-z0-9]+)/);C={width:x?x[1]:z,style:z,color:b.utils.color2Hex(D?D[1]:z)}}}else{if(E=="margin"){C=t}else{if(E=="align"){C=t.css("float");if(C!="left"&&C!="right"){C=t.css("vertical-align")}}else{C=t.attr(E)||""}}}}}if(u=="events"){C=b.utils.trimEventCallback(C)}B.val(C)})})},k=function(){var t=q.src.main.src.val();
j(true);if(!t){q.preview.children("img").remove();q.prevImg=null}else{if(q.prevImg){q.prevImg.removeAttr("src").removeAttr("style").removeAttr("class").removeAttr("id").removeAttr("title").removeAttr("alt").removeAttr("longdesc");a.each(q.src.events,function(v,u){q.prevImg.removeAttr(v)})}else{q.prevImg=a("<img/>").prependTo(q.preview)}q.prevImg.load(function(){q.prevImg.unbind("load");setTimeout(function(){f=q.prevImg.width();p=q.prevImg.height();h=(f/p).toFixed(2);q.src.main.width.val(f);q.src.main.height.val(p)
},100)}).attr("src",t)}},r=function(v){var t=parseInt(q.src.main.width.val())||0,u=parseInt(q.src.main.height.val())||0;if(q.prevImg){if(t&&u){if(v.target===q.src.main.width[0]){u=parseInt(t/h)}else{t=parseInt(u*h)}}else{t=f;u=p}q.src.main.height.val(u);q.src.main.width.val(t);q.prevImg.width(t).height(u);q.src.adv.style.val(q.prevImg.attr("style"))}};this.img=null;this.prevImg=null;this.preview=a('<div class="elrte-image-preview"/>').text("Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin");
this.init=function(){this.labels={main:"Properies",link:"Link",adv:"Advanced",events:"Events",id:"ID","class":"Css class",style:"Css style",longdesc:"Detail description URL",href:"URL",target:"Open in",title:"Title"};this.src={main:{src:a('<input type="text" />').css("width","100%").change(k),title:a('<input type="text" />').css("width","100%"),alt:a('<input type="text" />').css("width","100%"),width:a('<input type="text" />').attr("size",5).css("text-align","right").change(r),height:a('<input type="text" />').attr("size",5).css("text-align","right").change(r),margin:a("<div />").elPaddingInput({type:"margin",change:function(){var t=q.src.main.margin.val();
if(q.prevImg){if(t.css){q.prevImg.css("margin",t.css)}else{q.prevImg.css({"margin-left":t.left,"margin-top":t.top,"margin-right":t.right,"margin-bottom":t.bottom})}}}}),align:a("<select />").css("width","100%").append(a("<option />").val("").text(this.rte.i18n("Not set","dialogs"))).append(a("<option />").val("left").text(this.rte.i18n("Left"))).append(a("<option />").val("right").text(this.rte.i18n("Right"))).append(a("<option />").val("top").text(this.rte.i18n("Top"))).append(a("<option />").val("text-top").text(this.rte.i18n("Text top"))).append(a("<option />").val("middle").text(this.rte.i18n("middle"))).append(a("<option />").val("baseline").text(this.rte.i18n("Baseline"))).append(a("<option />").val("bottom").text(this.rte.i18n("Bottom"))).append(a("<option />").val("text-bottom").text(this.rte.i18n("Text bottom"))).change(function(){var u=a(this).val(),t={"float":"","vertical-align":""};
if(q.prevImg){if(u=="left"||u=="right"){t["float"]=u;t["vertical-align"]=""}else{if(u){t["float"]="";t["vertical-align"]=u}}q.prevImg.css(t)}}),border:a("<div />").elBorderSelect({name:"border",change:function(){var t=q.src.main.border.val();if(q.prevImg){q.prevImg.css("border",t.width?t.width+" "+t.style+" "+t.color:"")}}})},adv:{},events:{}};a.each(["id","class","style","longdesc"],function(u,t){q.src.adv[t]=a('<input type="text" style="width:100%" />')});this.src.adv["class"].change(function(){if(q.prevImg){q.prevImg.attr("class",a(this).val())
}});this.src.adv.style.change(function(){if(q.prevImg){q.prevImg.attr("style",a(this).val());o(q.prevImg)}});a.each(["onblur","onfocus","onclick","ondblclick","onmousedown","onmouseup","onmouseover","onmouseout","onmouseleave","onkeydown","onkeypress","onkeyup"],function(){q.src.events[this]=a('<input type="text"  style="width:100%"/>')})};this.command=function(){!this.src&&this.init();var t,w={rtl:b.rtl,submit:function(z,A){z.stopPropagation();z.preventDefault();q.set();u.close()},close:function(){g&&b.selection.moveToBookmark(g)
},dialog:{autoOpen:false,width:500,position:"top",title:b.i18n("Image"),resizable:true,open:function(){a.fn.resizable&&a(this).parents(".ui-dialog:first").resizable("option","alsoResize",".elrte-image-preview")}}},u=new elDialogForm(w),v=!!b.options.fmOpen,x=v?a('<div class="elrte-image-src-fm"><span class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-folder-open"/></span></div>').append(this.src.main.src.css("width","87%")):this.src.main.src;j();this.preview.children("img").remove();
this.prevImg=null;t=b.selection.getEnd();this.img=t.nodeName=="IMG"&&!a(t).is(".elrte-protected")?a(t):a("<img/>");g=b.selection.getBookmark();if(v){x.children(".ui-state-default").click(function(){b.options.fmOpen(function(z){q.src.main.src.val(z).change()})}).hover(function(){a(this).toggleClass("ui-state-hover")})}u.tab("main",this.rte.i18n("Properies")).append([this.rte.i18n("Image URL"),x],"main",true).append([this.rte.i18n("Title"),this.src.main.title],"main",true).append([this.rte.i18n("Alt text"),this.src.main.alt],"main",true).append([this.rte.i18n("Size"),a("<span />").append(this.src.main.width).append(" x ").append(this.src.main.height).append(" px")],"main",true).append([this.rte.i18n("Alignment"),this.src.main.align],"main",true).append([this.rte.i18n("Margins"),this.src.main.margin],"main",true).append([this.rte.i18n("Border"),this.src.main.border],"main",true);
u.append(a("<fieldset><legend>"+this.rte.i18n("Preview")+"</legend></fieldset>").append(this.preview),"main");a.each(this.src,function(A,z){if(A=="main"){return}u.tab(A,b.i18n(q.labels[A]));a.each(z,function(B,C){q.src[A][B].val(A=="events"?b.utils.trimEventCallback(q.img.attr(B)):q.img.attr(B)||"");u.append([b.i18n(q.labels[B]||B),q.src[A][B]],A,true)})});u.open();if(this.img.attr("src")){o(this.img);this.prevImg=this.img.clone().prependTo(this.preview);h=(this.img.width()/this.img.height()).toFixed(2);
f=parseInt(this.img.width());p=parseInt(this.img.height())}};this.set=function(){var u=this.src.main.src.val(),t;this.rte.history.add();g&&b.selection.moveToBookmark(g);if(!u){t=b.dom.selfOrParentLink(this.img[0]);t&&t.remove();return this.img.remove()}!this.img[0].parentNode&&(this.img=a(this.rte.doc.createElement("img")));this.img.attr("src",u).attr("style",this.src.adv.style.val());a.each(this.src,function(v,w){a.each(w,function(x,A){var B=A.val(),z;switch(x){case"width":q.img.css("width",B);break;
case"height":q.img.css("height",B);break;case"align":q.img.css(B=="left"||B=="right"?"float":"vertical-align",B);break;case"margin":if(B.css){q.img.css("margin",B.css)}else{q.img.css({"margin-left":B.left,"margin-top":B.top,"margin-right":B.right,"margin-bottom":B.bottom})}break;case"border":if(!B.width){B=""}else{B="border:"+B.css+";"+a.trim((q.img.attr("style")||"").replace(/border\-[^;]+;?/ig,""));x="style";q.img.attr("style",B);return}break;case"src":case"style":return;default:B?q.img.attr(x,B):q.img.removeAttr(x)
}})});!this.img[0].parentNode&&b.selection.insertNode(this.img[0]);this.rte.ui.update()};this.update=function(){this.domElem.removeClass("disabled");var u=this.rte.selection.getEnd(),t=a(u);if(u.nodeName=="IMG"&&!t.hasClass("elrte-protected")){this.domElem.addClass("active")}else{this.domElem.removeClass("active")}}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.indent=function(f,c){this.constructor.prototype.constructor.call(this,f,c);var b=this;this.command=function(){this.rte.history.add();
var h=this.rte.selection.selected({collapsed:true,blocks:true,wrap:"inline",tag:"p"});function g(q){var o=/(IMG|HR|TABLE|EMBED|OBJECT)/.test(q.nodeName)?"margin-left":"padding-left";var p=b.rte.dom.attr(q,"style").indexOf(o)!=-1?parseInt(a(q).css(o))||0:0;a(q).css(o,p+40+"px")}for(var j=0;j<h.length;j++){if(/^(TABLE|THEAD|TFOOT|TBODY|COL|COLGROUP|TR)$/.test(h[j].nodeName)){a(h[j]).find("td,th").each(function(){g(this)})}else{if(/^LI$/.test(h[j].nodeName)){var k=a(h[j]);a(this.rte.dom.create(h[j].parentNode.nodeName)).append(a(this.rte.dom.create("li")).html(k.html()||"")).appendTo(k.html("&nbsp;"))
}else{g(h[j])}}}this.rte.ui.update()};this.update=function(){this.domElem.removeClass("disabled")}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.justifyleft=function(c,b){this.constructor.prototype.constructor.call(this,c,b);this.align=this.name=="justifyfull"?"justify":this.name.replace("justify","");this.command=function(){var g=this.rte.selection.selected({collapsed:true,blocks:true,tag:"div"}),f=g.length;f&&this.rte.history.add();while(f--){this.rte.dom.filter(g[f],"textNodes")&&a(g[f]).css("text-align",this.align)
}this.rte.ui.update()};this.update=function(){var f=this.rte.selection.getNode(),g=f.nodeName=="BODY"?f:this.rte.dom.selfOrParent(f,"textNodes")||(f.parentNode&&f.parentNode.nodeName=="BODY"?f.parentNode:null);if(g){this.domElem.removeClass("disabled").toggleClass("active",a(g).css("text-align")==this.align)}else{this.domElem.addClass("disabled")}}};elRTE.prototype.ui.prototype.buttons.justifycenter=elRTE.prototype.ui.prototype.buttons.justifyleft;elRTE.prototype.ui.prototype.buttons.justifyright=elRTE.prototype.ui.prototype.buttons.justifyleft;
elRTE.prototype.ui.prototype.buttons.justifyfull=elRTE.prototype.ui.prototype.buttons.justifyleft})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.link=function(f,c){this.constructor.prototype.constructor.call(this,f,c);var b=this;this.img=false;this.bm;function g(){b.labels={id:"ID","class":"Css class",style:"Css style",dir:"Script direction",lang:"Language",charset:"Charset",type:"Target MIME type",rel:"Relationship page to target (rel)",rev:"Relationship target to page (rev)",tabindex:"Tab index",accesskey:"Access key"};
b.src={main:{href:a('<input type="text" />'),title:a('<input type="text" />'),anchor:a("<select />").attr("name","anchor"),target:a("<select />").append(a("<option />").text(b.rte.i18n("In this window")).val("")).append(a("<option />").text(b.rte.i18n("In new window (_blank)")).val("_blank"))},popup:{use:a('<input type="checkbox" />'),url:a('<input type="text" />').val("http://"),name:a('<input type="text" />'),width:a('<input type="text" />').attr({size:6,title:b.rte.i18n("Width")}).css("text-align","right"),height:a('<input type="text" />').attr({size:6,title:b.rte.i18n("Height")}).css("text-align","right"),left:a('<input type="text" />').attr({size:6,title:b.rte.i18n("Left")}).css("text-align","right"),top:a('<input type="text" />').attr({size:6,title:b.rte.i18n("Top")}).css("text-align","right"),location:a('<input type="checkbox" />'),menubar:a('<input type="checkbox" />'),toolbar:a('<input type="checkbox" />'),scrollbars:a('<input type="checkbox" />'),status:a('<input type="checkbox" />'),resizable:a('<input type="checkbox" />'),dependent:a('<input type="checkbox" />'),retfalse:a('<input type="checkbox" />').attr("checked",true)},adv:{id:a('<input type="text" />'),"class":a('<input type="text" />'),style:a('<input type="text" />'),dir:a("<select />").append(a("<option />").text(b.rte.i18n("Not set")).val("")).append(a("<option />").text(b.rte.i18n("Left to right")).val("ltr")).append(a("<option />").text(b.rte.i18n("Right to left")).val("rtl")),lang:a('<input type="text" />'),charset:a('<input type="text" />'),type:a('<input type="text" />'),rel:a('<input type="text" />'),rev:a('<input type="text" />'),tabindex:a('<input type="text" />'),accesskey:a('<input type="text" />')},events:{}};
a.each(["onblur","onfocus","onclick","ondblclick","onmousedown","onmouseup","onmouseover","onmouseout","onmouseleave","onkeydown","onkeypress","onkeyup"],function(){b.src.events[this]=a('<input type="text" />')});a.each(b.src,function(){for(var j in this){var h=this[j].attr("type");if(!h||(h=="text"&&!this[j].attr("size"))){this[j].css("width","100%")}}})}this.command=function(){var p=this.rte.selection.getNode(),o,u,x,h,t,j,w,k,z;!this.src&&g();this.bm=this.rte.selection.getBookmark();function q(r){return r.nodeName=="A"&&r.href
}this.link=this.rte.dom.selfOrParentLink(p);if(!this.link){o=a.browser.msie?this.rte.selection.selected():this.rte.selection.selected({wrap:false});if(o.length){for(u=0;u<o.length;u++){if(q(o[u])){this.link=o[u];break}}if(!this.link){this.link=this.rte.dom.parent(o[0],q)||this.rte.dom.parent(o[o.length-1],q)}}}this.link=this.link?a(this.link):a(this.rte.doc.createElement("a"));this.img=p.nodeName=="IMG"?p:null;this.updatePopup();this.src.main.anchor.empty();a('a[href!=""][name]',this.rte.doc).each(function(){var r=a(this).attr("name");
b.src.main.anchor.append(a("<option />").val(r).text(r))});if(this.src.main.anchor.children().length){this.src.main.anchor.prepend(a("<option />").val("").text(this.rte.i18n("Select bookmark"))).change(function(){var r=a(this).val();if(r){b.src.main.href.val("#"+r)}})}h={rtl:this.rte.rtl,submit:function(r,v){r.stopPropagation();r.preventDefault();b.set();v.close()},tabs:{show:function(v,r){if(r.index==3){b.updateOnclick()}}},close:function(){b.rte.browser.msie&&b.rte.selection.restoreIERange()},dialog:{width:"auto",width:430,title:this.rte.i18n("Link")}};
d=new elDialogForm(h);t=a("<div />").append(a("<label />").append(this.src.popup.location).append(this.rte.i18n("Location bar"))).append(a("<label />").append(this.src.popup.menubar).append(this.rte.i18n("Menu bar"))).append(a("<label />").append(this.src.popup.toolbar).append(this.rte.i18n("Toolbar"))).append(a("<label />").append(this.src.popup.scrollbars).append(this.rte.i18n("Scrollbars")));j=a("<div />").append(a("<label />").append(this.src.popup.status).append(this.rte.i18n("Status bar"))).append(a("<label />").append(this.src.popup.resizable).append(this.rte.i18n("Resizable"))).append(a("<label />").append(this.src.popup.dependent).append(this.rte.i18n("Depedent"))).append(a("<label />").append(this.src.popup.retfalse).append(this.rte.i18n("Add return false")));
d.tab("main",this.rte.i18n("Properies")).tab("popup",this.rte.i18n("Popup")).tab("adv",this.rte.i18n("Advanced")).tab("events",this.rte.i18n("Events")).append(a("<label />").append(this.src.popup.use).append(this.rte.i18n("Open link in popup window")),"popup").separator("popup").append([this.rte.i18n("URL"),this.src.popup.url],"popup",true).append([this.rte.i18n("Window name"),this.src.popup.name],"popup",true).append([this.rte.i18n("Window size"),a("<span />").append(this.src.popup.width).append(" x ").append(this.src.popup.height).append(" px")],"popup",true).append([this.rte.i18n("Window position"),a("<span />").append(this.src.popup.left).append(" x ").append(this.src.popup.top).append(" px")],"popup",true).separator("popup").append([t,j],"popup",true);
w=this.link.get(0);k=this.rte.dom.attr(w,"href");this.src.main.href.val(k).change(function(){a(this).val(b.rte.utils.absoluteURL(a(this).val()))});if(this.rte.options.fmAllow&&this.rte.options.fmOpen){var z=a("<span />").append(this.src.main.href.css("width","87%")).append(a("<span />").addClass("ui-state-default ui-corner-all").css({"float":"right","margin-right":"3px"}).attr("title",b.rte.i18n("Open file manger")).append(a("<span />").addClass("ui-icon ui-icon-folder-open")).click(function(){b.rte.options.fmOpen(function(r){b.src.main.href.val(r).change()
})}).hover(function(){a(this).addClass("ui-state-hover")},function(){a(this).removeClass("ui-state-hover")}));d.append([this.rte.i18n("Link URL"),z],"main",true)}else{d.append([this.rte.i18n("Link URL"),this.src.main.href],"main",true)}this.src.main.href.change();d.append([this.rte.i18n("Title"),this.src.main.title.val(this.rte.dom.attr(w,"title"))],"main",true);if(this.src.main.anchor.children().length){d.append([this.rte.i18n("Bookmark"),this.src.main.anchor.val(k)],"main",true)}if(!(this.rte.options.doctype.match(/xhtml/)&&this.rte.options.doctype.match(/strict/))){d.append([this.rte.i18n("Target"),this.src.main.target.val(this.link.attr("target")||"")],"main",true)
}for(var p in this.src.adv){this.src.adv[p].val(this.rte.dom.attr(w,p));d.append([this.rte.i18n(this.labels[p]?this.labels[p]:p),this.src.adv[p]],"adv",true)}for(var p in this.src.events){var x=this.rte.utils.trimEventCallback(this.rte.dom.attr(w,p));this.src.events[p].val(x);d.append([this.rte.i18n(this.labels[p]?this.labels[p]:p),this.src.events[p]],"events",true)}this.src.popup.use.change(function(){var r=a(this).attr("checked");a.each(b.src.popup,function(){if(a(this).attr("name")!="use"){if(r){a(this).removeAttr("disabled")
}else{a(this).attr("disabled",true)}}})});this.src.popup.use.change();d.open()};this.update=function(){var h=this.rte.selection.getNode();if(this.rte.dom.selfOrParentLink(h)){this.domElem.removeClass("disabled").addClass("active")}else{if(this.rte.dom.selectionHas(function(j){return j.nodeName=="A"&&j.href})){this.domElem.removeClass("disabled").addClass("active")}else{if(!this.rte.selection.collapsed()||h.nodeName=="IMG"){this.domElem.removeClass("disabled active")}else{this.domElem.addClass("disabled").removeClass("active")
}}}};this.updatePopup=function(){var h=""+this.link.attr("onclick");if(h.length>0&&(m=h.match(/window.open\('([^']+)',\s*'([^']*)',\s*'([^']*)'\s*.*\);\s*(return\s+false)?/))){this.src.popup.use.attr("checked","on");this.src.popup.url.val(m[1]);this.src.popup.name.val(m[2]);if(/location=yes/.test(m[3])){this.src.popup.location.attr("checked",true)}if(/menubar=yes/.test(m[3])){this.src.popup.menubar.attr("checked",true)}if(/toolbar=yes/.test(m[3])){this.src.popup.toolbar.attr("checked",true)}if(/scrollbars=yes/.test(m[3])){this.src.popup.scrollbars.attr("checked",true)
}if(/status=yes/.test(m[3])){this.src.popup.status.attr("checked",true)}if(/resizable=yes/.test(m[3])){this.src.popup.resizable.attr("checked",true)}if(/dependent=yes/.test(m[3])){this.src.popup.dependent.attr("checked",true)}if((_m=m[3].match(/width=([^,]+)/))){this.src.popup.width.val(_m[1])}if((_m=m[3].match(/height=([^,]+)/))){this.src.popup.height.val(_m[1])}if((_m=m[3].match(/left=([^,]+)/))){this.src.popup.left.val(_m[1])}if((_m=m[3].match(/top=([^,]+)/))){this.src.popup.top.val(_m[1])}if(m[4]){this.src.popup.retfalse.attr("checked",true)
}}else{a.each(this.src.popup,function(){var j=a(this);if(j.attr("type")=="text"){j.val(j.attr("name")=="url"?"http://":"")}else{if(j.attr("name")=="retfalse"){this.attr("checked",true)}else{j.removeAttr("checked")}}})}};this.updateOnclick=function(){var o=this.src.popup.url.val();if(this.src.popup.use.attr("checked")&&o){var p="";if(this.src.popup.location.attr("checked")){p+="location=yes,"}if(this.src.popup.menubar.attr("checked")){p+="menubar=yes,"}if(this.src.popup.toolbar.attr("checked")){p+="toolbar=yes,"
}if(this.src.popup.scrollbars.attr("checked")){p+="scrollbars=yes,"}if(this.src.popup.status.attr("checked")){p+="status=yes,"}if(this.src.popup.resizable.attr("checked")){p+="resizable=yes,"}if(this.src.popup.dependent.attr("checked")){p+="dependent=yes,"}if(this.src.popup.width.val()){p+="width="+this.src.popup.width.val()+","}if(this.src.popup.height.val()){p+="height="+this.src.popup.height.val()+","}if(this.src.popup.left.val()){p+="left="+this.src.popup.left.val()+","}if(this.src.popup.top.val()){p+="top="+this.src.popup.top.val()+","
}if(p.length>0){p=p.substring(0,p.length-1)}var j=this.src.popup.retfalse.attr("checked")?"return false;":"";var k="window.open('"+o+"', '"+a.trim(this.src.popup.name.val())+"', '"+p+"'); "+j;this.src.events.onclick.val(k);if(!this.src.main.href.val()){this.src.main.href.val("#")}}else{var h=this.src.events.onclick.val();h=h.replace(/window\.open\([^\)]+\)\s*;?\s*return\s*false\s*;?/i,"");this.src.events.onclick.val(h)}};this.set=function(){var j,k;this.updateOnclick();this.rte.selection.moveToBookmark(this.bm);
this.rte.history.add();j=this.rte.utils.absoluteURL(this.src.main.href.val());if(!j){var p=this.rte.selection.getBookmark();this.rte.dom.unwrap(this.link[0]);this.rte.selection.moveToBookmark(p)}else{if(this.img&&this.img.parentNode){this.link=a(this.rte.dom.create("a")).attr("href",j);this.rte.dom.wrap(this.img,this.link[0])}else{if(!this.link[0].parentNode){k="#--el-editor---"+Math.random();this.rte.doc.execCommand("createLink",false,k);this.link=a('a[href="'+k+'"]',this.rte.doc);this.link.each(function(){var r=a(this);
if(!a.trim(r.html())&&!a.trim(r.text())){r.replaceWith(r.text())}})}}this.src.main.href.val(j);for(var o in this.src){if(o!="popup"){for(var q in this.src[o]){if(q!="anchors"){var h=a.trim(this.src[o][q].val());if(h){this.link.attr(q,h)}else{this.link.removeAttr(q)}}}}}this.img&&this.rte.selection.select(this.img)}this.rte.ui.update(true)}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.nbsp=function(c,b){this.constructor.prototype.constructor.call(this,c,b);this.command=function(){this.rte.history.add();
this.rte.selection.insertHtml("&nbsp;",true);this.rte.window.focus();this.rte.ui.update()};this.update=function(){this.domElem.removeClass("disabled")}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.outdent=function(f,c){this.constructor.prototype.constructor.call(this,f,c);var b=this;this.command=function(){var g=this.find();if(g.node){this.rte.history.add();a(g.node).css(g.type,(g.val>40?g.val-40:0)+"px");this.rte.ui.update()}};this.find=function(j){function g(p){var k={type:"",val:0};
var o;if((o=b.rte.dom.attr(p,"style"))){k.type=o.indexOf("padding-left")!=-1?"padding-left":(o.indexOf("margin-left")!=-1?"margin-left":"");k.val=k.type?parseInt(a(p).css(k.type))||0:0}return k}var j=this.rte.selection.getNode();var h=g(j);if(h.val){h.node=j}else{a.each(this.rte.dom.parents(j,"*"),function(){h=g(this);if(h.val){h.node=this;return h}})}return h};this.update=function(){var g=this.find();if(g.node){this.domElem.removeClass("disabled")}else{this.domElem.addClass("disabled")}}}})(jQuery);
(function(a){elRTE.prototype.ui.prototype.buttons.pagebreak=function(c,b){this.constructor.prototype.constructor.call(this,c,b);a(this.rte.doc.body).bind("mousedown",function(f){if(a(f.target).hasClass("elrte-pagebreak")){f.preventDefault()}});this.command=function(){this.rte.selection.insertHtml('<img src="'+this.rte.filter.url+'pixel.gif" class="elrte-protected elrte-pagebreak"/>',false)};this.update=function(){this.domElem.removeClass("disabled")}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.pasteformattext=function(f,c){this.constructor.prototype.constructor.call(this,f,c);
this.iframe=a(document.createElement("iframe")).addClass("el-rte-paste-input");this.doc=null;var b=this;this.command=function(){this.rte.selection.saveIERange();var g=this,h={submit:function(o,p){o.stopPropagation();o.preventDefault();g.paste();p.close()},dialog:{width:500,title:this.rte.i18n("Paste formatted text")}},k=new elDialogForm(h);k.append(this.iframe).open();this.doc=this.iframe.get(0).contentWindow.document;html=this.rte.options.doctype+'<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
html+="</head><body> <br /> </body></html>";this.doc.open();this.doc.write(html);this.doc.close();if(!this.rte.browser.msie){try{this.doc.designMode="on"}catch(j){}}else{this.doc.body.contentEditable=true}setTimeout(function(){g.iframe[0].contentWindow.focus()},50)};this.paste=function(){a(this.doc.body).find("[class]").removeAttr("class");var g=a.trim(a(this.doc.body).html());if(g){this.rte.history.add();this.rte.selection.restoreIERange();this.rte.selection.insertHtml(this.rte.filter.wysiwyg2wysiwyg(this.rte.filter.proccess("paste",g)));
this.rte.ui.update(true)}};this.update=function(){this.domElem.removeClass("disabled")}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.pastetext=function(f,c){this.constructor.prototype.constructor.call(this,f,c);this.input=a("<textarea />").addClass("el-rte-paste-input");var b=this;this.command=function(){this.rte.browser.msie&&this.rte.selection.saveIERange();var g={submit:function(j,k){j.stopPropagation();j.preventDefault();b.paste();k.close()},dialog:{width:500,title:this.rte.i18n("Paste only text")}};
var h=new elDialogForm(g);h.append(this.input).open()};this.paste=function(){var g=a.trim(this.input.val());if(g){this.rte.history.add();this.rte.browser.msie&&this.rte.selection.restoreIERange();this.rte.selection.insertText(g.replace(/\r?\n/g,"<br />"),true);this.rte.ui.update(true)}this.input.val("")};this.update=function(){this.domElem.removeClass("disabled")}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.save=function(c,b){this.constructor.prototype.constructor.call(this,c,b);
this.active=true;this.command=function(){this.rte.save()};this.update=function(){}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.smiley=function(f,c){this.constructor.prototype.constructor.call(this,f,c);var b=this;this.img=null;this.url=this.rte.filter.url+"smileys/";this.smileys={smile:"smile.png",happy:"happy.png",tongue:"tongue.png",surprised:"surprised.png",waii:"waii.png",wink:"wink.png",evilgrin:"evilgrin.png",grin:"grin.png",unhappy:"unhappy.png"};this.width=120;this.command=function(){var h=this,j=this.url,o,k,g;
this.rte.browser.msie&&this.rte.selection.saveIERange();k={dialog:{height:120,width:this.width,title:this.rte.i18n("Smiley"),buttons:{}}};o=new elDialogForm(k);a.each(this.smileys,function(q,p){o.append(a('<img src="'+j+p+'" title="'+q+'" id="'+q+'" class="el-rte-smiley"/>').click(function(){h.set(this.id,o)}))});o.open()};this.update=function(){this.domElem.removeClass("disabled");this.domElem.removeClass("active")};this.set=function(g,h){this.rte.browser.msie&&this.rte.selection.restoreIERange();
if(this.smileys[g]){this.img=a(this.rte.doc.createElement("img"));this.img.attr({src:this.url+this.smileys[g],title:g,alt:g});this.rte.selection.insertNode(this.img.get(0));this.rte.ui.update()}h.close()}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.stopfloat=function(c,b){this.constructor.prototype.constructor.call(this,c,b);this.find=function(){if(this.rte.selection.collapsed()){var f=this.rte.dom.selfOrParent(this.rte.selection.getEnd(),/^DIV$/);if(f&&(this.rte.dom.attr(f,"clear")||a(f).css("clear")!="none")){return f
}}};this.command=function(){var f;if((f=this.find())){var f=a(f);this.rte.history.add();if(!f.children().length&&!a.trim(f.text()).length){f.remove()}else{f.removeAttr("clear").css("clear","")}}else{this.rte.history.add();this.rte.selection.insertNode(a(this.rte.dom.create("div")).css("clear","both").get(0),true)}this.rte.ui.update(true)};this.update=function(){this.domElem.removeClass("disabled");if(this.find()){this.domElem.addClass("active")}else{this.domElem.removeClass("active")}}}})(jQuery);
(function(a){elRTE.prototype.ui.prototype.buttons.table=function(f,c){this.constructor.prototype.constructor.call(this,f,c);var b=this;this.src=null;this.labels=null;function g(){b.labels={main:"Properies",adv:"Advanced",events:"Events",id:"ID","class":"Css class",style:"Css style",dir:"Script direction",summary:"Summary",lang:"Language",href:"URL"};b.src={main:{caption:a('<input type="text" />'),rows:a('<input type="text" />').attr("size",5).val(2),cols:a('<input type="text" />').attr("size",5).val(2),width:a('<input type="text" />').attr("size",5),wunit:a("<select />").append(a("<option />").val("%").text("%")).append(a("<option />").val("px").text("px")),height:a('<input type="text" />').attr("size",5),hunit:a("<select />").append(a("<option />").val("%").text("%")).append(a("<option />").val("px").text("px")),align:a("<select />").append(a("<option />").val("").text(b.rte.i18n("Not set"))).append(a("<option />").val("left").text(b.rte.i18n("Left"))).append(a("<option />").val("center").text(b.rte.i18n("Center"))).append(a("<option />").val("right").text(b.rte.i18n("Right"))),spacing:a('<input type="text" />').attr("size",5),padding:a('<input type="text" />').attr("size",5),border:a("<div />"),rules:a("<select />").append(a("<option />").val("none").text(b.rte.i18n("No"))).append(a("<option />").val("all").text(b.rte.i18n("Cells"))).append(a("<option />").val("groups").text(b.rte.i18n("Groups"))).append(a("<option />").val("rows").text(b.rte.i18n("Rows"))).append(a("<option />").val("cols").text(b.rte.i18n("Columns"))),margin:a("<div />"),bg:a("<div />"),bgimg:a('<input type="text" />').css("width","90%")},adv:{id:a('<input type="text" />'),summary:a('<input type="text" />'),"class":a('<input type="text" />'),style:a('<input type="text" />'),dir:a("<select />").append(a("<option />").text(b.rte.i18n("Not set")).val("")).append(a("<option />").text(b.rte.i18n("Left to right")).val("ltr")).append(a("<option />").text(b.rte.i18n("Right to left")).val("rtl")),lang:a('<input type="text" />')},events:{}};
a.each(b.src,function(){for(var j in this){this[j].attr("name",j);var h=this[j].get(0).nodeName;if(h=="INPUT"&&j!="bgimg"){this[j].css(this[j].attr("size")?{"text-align":"right"}:{width:"100%"})}else{if(h=="SELECT"&&j!="wunit"&&j!="hunit"){this[j].css("width","100%")}}}});a.each(["onblur","onfocus","onclick","ondblclick","onmousedown","onmouseup","onmouseover","onmouseout","onmouseleave","onkeydown","onkeypress","onkeyup"],function(){b.src.events[this]=a('<input type="text" />').css("width","100%")
});b.src.main.align.change(function(){var j=a(this).val();if(j=="center"){b.src.main.margin.val({left:"auto",right:"auto"})}else{var h=b.src.main.margin.val();if(h.left=="auto"&&h.right=="auto"){b.src.main.margin.val({left:"",right:""})}}});b.src.main.bgimg.change(function(){var h=a(this);h.val(b.rte.utils.absoluteURL(h.val()))})}this.command=function(){var r=this.rte.dom.selfOrParent(this.rte.selection.getNode(),/^TABLE$/);if(this.name=="table"){this.table=a(this.rte.doc.createElement("table"))}else{this.table=r?a(r):a(this.rte.doc.createElement("table"))
}!this.src&&g();this.src.main.border.elBorderSelect({styleHeight:117});this.src.main.bg.elColorPicker({palettePosition:"outer","class":"el-colorpicker ui-icon ui-icon-pencil"});this.src.main.margin.elPaddingInput({type:"margin",value:this.table});if(this.table.parents().length){this.src.main.rows.val("").attr("disabled",true);this.src.main.cols.val("").attr("disabled",true)}else{this.src.main.rows.val(2).removeAttr("disabled");this.src.main.cols.val(2).removeAttr("disabled")}var D=this.table.css("width")||this.table.attr("width");
this.src.main.width.val(parseInt(D)||"");this.src.main.wunit.val(D.indexOf("px")!=-1?"px":"%");var z=this.table.css("height")||this.table.attr("height");this.src.main.height.val(parseInt(z)||"");this.src.main.hunit.val(z&&z.indexOf("px")!=-1?"px":"%");var B=this.table.css("float");this.src.main.align.val("");if(B=="left"||B=="right"){this.src.main.align.val(B)}else{var t=this.table.css("margin-left");var p=this.table.css("margin-right");if(t=="auto"&&p=="auto"){this.src.main.align.val("center")}}this.src.main.border.val(this.table);
this.src.main.rules.val(this.rte.dom.attr(this.table.get(0),"rules"));this.src.main.bg.val(this.table.css("background-color"));var j=(this.table.css("background-image")||"").replace(/url\(([^\)]+)\)/i,"$1");this.src.main.bgimg.val(j!="none"?j:"");var k={rtl:this.rte.rtl,submit:function(h,v){h.stopPropagation();h.preventDefault();b.set();v.close()},dialog:{width:530,title:this.rte.i18n("Table")}};var C=new elDialogForm(k);for(var q in this.src){C.tab(q,this.rte.i18n(this.labels[q]));if(q=="main"){var A=a("<table />").append(a("<tr />").append("<td>"+this.rte.i18n("Rows")+"</td>").append(a("<td />").append(this.src.main.rows))).append(a("<tr />").append("<td>"+this.rte.i18n("Columns")+"</td>").append(a("<td />").append(this.src.main.cols)));
var x=a("<table />").append(a("<tr />").append("<td>"+this.rte.i18n("Width")+"</td>").append(a("<td />").append(this.src.main.width).append(this.src.main.wunit))).append(a("<tr />").append("<td>"+this.rte.i18n("Height")+"</td>").append(a("<td />").append(this.src.main.height).append(this.src.main.hunit)));var u=a("<table />").append(a("<tr />").append("<td>"+this.rte.i18n("Spacing")+"</td>").append(a("<td />").append(this.src.main.spacing.val(this.table.attr("cellspacing")||"")))).append(a("<tr />").append("<td>"+this.rte.i18n("Padding")+"</td>").append(a("<td />").append(this.src.main.padding.val(this.table.attr("cellpadding")||""))));
C.append([this.rte.i18n("Caption"),this.src.main.caption.val(this.table.find("caption").eq(0).text()||"")],"main",true).separator("main").append([A,x,u],"main",true).separator("main").append([this.rte.i18n("Border"),this.src.main.border],"main",true).append([this.rte.i18n("Inner borders"),this.src.main.rules],"main",true).append([this.rte.i18n("Alignment"),this.src.main.align],"main",true).append([this.rte.i18n("Margins"),this.src.main.margin],"main",true).append([this.rte.i18n("Background"),a("<span />").append(a("<span />").css({"float":"left","margin-right":"3px"}).append(this.src.main.bg)).append(this.src.main.bgimg)],"main",true)
}else{for(var o in this.src[q]){var E=this.rte.dom.attr(this.table,o);if(q=="events"){E=this.rte.utils.trimEventCallback(E)}C.append([this.rte.i18n(this.labels[o]?this.labels[o]:o),this.src[q][o].val(E)],q,true)}}}C.open()};this.set=function(){if(!this.table.parents().length){var k=parseInt(this.src.main.rows.val())||0;var B=parseInt(this.src.main.cols.val())||0;if(k<=0||B<=0){return}this.rte.history.add();var D=a(this.rte.doc.createElement("tbody")).appendTo(this.table);for(var u=0;u<k;u++){var C="<tr>";
for(var t=0;t<B;t++){C+="<td>&nbsp;</td>"}D.append(C+"</tr>")}}else{this.table.removeAttr("width").removeAttr("height").removeAttr("border").removeAttr("align").removeAttr("bordercolor").removeAttr("bgcolor").removeAttr("cellspacing").removeAttr("cellpadding").removeAttr("frame").removeAttr("rules").removeAttr("style")}var I=a.trim(this.src.main.caption.val());if(I){if(!this.table.children("caption").length){this.table.prepend("<caption />")}this.table.children("caption").text(I)}else{this.table.children("caption").remove()
}for(var p in this.src){if(p!="main"){for(var o in this.src[p]){var G=a.trim(this.src[p][o].val());if(G){this.table.attr(o,G)}else{this.table.removeAttr(o)}}}}var A,E,H;if((A=parseInt(this.src.main.spacing.val()))&&A>=0){this.table.attr("cellspacing",A)}if((E=parseInt(this.src.main.padding.val()))&&E>=0){this.table.attr("cellpadding",E)}if((H=this.src.main.rules.val())){this.table.attr("rules",H)}var F=parseInt(this.src.main.width.val())||"",x=parseInt(this.src.main.height.val())||"",u=a.trim(this.src.main.bgimg.val()),D=this.src.main.border.val(),q=this.src.main.margin.val(),z=this.src.main.align.val();
this.table.css({width:F?F+this.src.main.wunit.val():"",height:x?x+this.src.main.hunit.val():"",border:a.trim(D.width+" "+D.style+" "+D.color),"background-color":this.src.main.bg.val(),"background-image":u?"url("+u+")":""});if(q.css){this.table.css("margin",q.css)}else{this.table.css({"margin-top":q.top,"margin-right":q.right,"margin-bottom":q.bottom,"margin-left":q.left})}if((z=="left"||z=="right")&&this.table.css("margin-left")!="auto"&&this.table.css("margin-right")!="auto"){this.table.css("float",z)
}if(!this.table.attr("style")){this.table.removeAttr("style")}if(!this.table.parents().length){this.rte.selection.insertNode(this.table.get(0),true)}this.rte.ui.update()};this.update=function(){this.domElem.removeClass("disabled");if(this.name=="tableprops"&&!this.rte.dom.selfOrParent(this.rte.selection.getNode(),/^TABLE$/)){this.domElem.addClass("disabled").removeClass("active")}}};elRTE.prototype.ui.prototype.buttons.tableprops=elRTE.prototype.ui.prototype.buttons.table})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.tablerm=function(c,b){this.constructor.prototype.constructor.call(this,c,b);
this.command=function(){var f=this.rte.dom.parent(this.rte.selection.getNode(),/^TABLE$/);if(f){this.rte.history.add();a(f).remove()}this.rte.ui.update(true)};this.update=function(){if(this.rte.dom.parent(this.rte.selection.getNode(),/^TABLE$/)){this.domElem.removeClass("disabled")}else{this.domElem.addClass("disabled")}}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.tbcellprops=function(f,c){this.constructor.prototype.constructor.call(this,f,c);var b=this;this.src=null;this.labels=null;
function g(){b.labels={main:"Properies",adv:"Advanced",events:"Events",id:"ID","class":"Css class",style:"Css style",dir:"Script direction",lang:"Language"};b.src={main:{type:a("<select />").css("width","100%").append(a("<option />").val("td").text(b.rte.i18n("Data"))).append(a("<option />").val("th").text(b.rte.i18n("Header"))),width:a('<input type="text" />').attr("size",4),wunit:a("<select />").append(a("<option />").val("%").text("%")).append(a("<option />").val("px").text("px")),height:a('<input type="text" />').attr("size",4),hunit:a("<select />").append(a("<option />").val("%").text("%")).append(a("<option />").val("px").text("px")),align:a("<select />").css("width","100%").append(a("<option />").val("").text(b.rte.i18n("Not set"))).append(a("<option />").val("left").text(b.rte.i18n("Left"))).append(a("<option />").val("center").text(b.rte.i18n("Center"))).append(a("<option />").val("right").text(b.rte.i18n("Right"))).append(a("<option />").val("justify").text(b.rte.i18n("Justify"))),border:a("<div />"),padding:a("<div />"),bg:a("<div />"),bgimg:a('<input type="text" />').css("width","90%"),apply:a("<select />").css("width","100%").append(a("<option />").val("").text(b.rte.i18n("Current cell"))).append(a("<option />").val("row").text(b.rte.i18n("All cells in row"))).append(a("<option />").val("column").text(b.rte.i18n("All cells in column"))).append(a("<option />").val("table").text(b.rte.i18n("All cells in table")))},adv:{id:a('<input type="text" />'),"class":a('<input type="text" />'),style:a('<input type="text" />'),dir:a("<select />").css("width","100%").append(a("<option />").text(b.rte.i18n("Not set")).val("")).append(a("<option />").text(b.rte.i18n("Left to right")).val("ltr")).append(a("<option />").text(b.rte.i18n("Right to left")).val("rtl")),lang:a('<input type="text" />')},events:{}};
a.each(b.src,function(){for(var h in this){this[h].attr("name",h);if(this[h].attr("type")=="text"&&!this[h].attr("size")&&h!="bgimg"){this[h].css("width","100%")}}});a.each(["onblur","onfocus","onclick","ondblclick","onmousedown","onmouseup","onmouseover","onmouseout","onmouseleave","onkeydown","onkeypress","onkeyup"],function(){b.src.events[this]=a('<input type="text" />').css("width","100%")})}this.command=function(){!this.src&&g();this.cell=this.rte.dom.selfOrParent(this.rte.selection.getNode(),/^(TD|TH)$/);
if(!this.cell){return}this.src.main.type.val(this.cell.nodeName.toLowerCase());this.cell=a(this.cell);this.src.main.border.elBorderSelect({styleHeight:117,value:this.cell});this.src.main.bg.elColorPicker({palettePosition:"outer","class":"el-colorpicker ui-icon ui-icon-pencil"});this.src.main.padding.elPaddingInput({value:this.cell});var j=this.cell.css("width")||this.cell.attr("width");this.src.main.width.val(parseInt(j)||"");this.src.main.wunit.val(j.indexOf("px")!=-1?"px":"%");var q=this.cell.css("height")||this.cell.attr("height");
this.src.main.height.val(parseInt(q)||"");this.src.main.hunit.val(q.indexOf("px")!=-1?"px":"%");this.src.main.align.val(this.cell.attr("align")||this.cell.css("text-align"));this.src.main.bg.val(this.cell.css("background-color"));var t=this.cell.css("background-image");this.src.main.bgimg.val(t&&t!="none"?t.replace(/url\(([^\)]+)\)/i,"$1"):"");this.src.main.apply.val("");var r={rtl:this.rte.rtl,submit:function(h,v){h.stopPropagation();h.preventDefault();b.set();v.close()},dialog:{width:520,title:this.rte.i18n("Table cell properties")}};
var u=new elDialogForm(r);for(var p in this.src){u.tab(p,this.rte.i18n(this.labels[p]));if(p=="main"){u.append([this.rte.i18n("Width"),a("<span />").append(this.src.main.width).append(this.src.main.wunit)],"main",true).append([this.rte.i18n("Height"),a("<span />").append(this.src.main.height).append(this.src.main.hunit)],"main",true).append([this.rte.i18n("Table cell type"),this.src.main.type],"main",true).append([this.rte.i18n("Border"),this.src.main.border],"main",true).append([this.rte.i18n("Alignment"),this.src.main.align],"main",true).append([this.rte.i18n("Paddings"),this.src.main.padding],"main",true).append([this.rte.i18n("Background"),a("<span />").append(a("<span />").css({"float":"left","margin-right":"3px"}).append(this.src.main.bg)).append(this.src.main.bgimg)],"main",true).append([this.rte.i18n("Apply to"),this.src.main.apply],"main",true)
}else{for(var o in this.src[p]){var k=this.cell.attr(o)||"";if(p=="events"){k=this.rte.utils.trimEventCallback(k)}u.append([this.rte.i18n(this.labels[o]?this.labels[o]:o),this.src[p][o].val(k)],p,true)}}}u.open()};this.set=function(){var x=this.cell,B=this.src.main.apply.val();switch(this.src.main.apply.val()){case"row":x=this.cell.parent("tr").children("td,th");break;case"column":x=a(this.rte.dom.tableColumn(this.cell.get(0)));break;case"table":x=this.cell.parents("table").find("td,th");break}for(var o in this.src){if(o!="main"){for(var k in this.src[o]){var C=a.trim(this.src[o][k].val());
if(C){x.attr(k,C)}else{x.removeAttr(k)}}}}x.removeAttr("width").removeAttr("height").removeAttr("border").removeAttr("align").removeAttr("bordercolor").removeAttr("bgcolor");var D=this.src.main.type.val();var A=parseInt(this.src.main.width.val())||"";var r=parseInt(this.src.main.height.val())||"";var q=a.trim(this.src.main.bgimg.val());var z=this.src.main.border.val();var u={width:A?A+this.src.main.wunit.val():"",height:r?r+this.src.main.hunit.val():"","background-color":this.src.main.bg.val(),"background-image":q?"url("+q+")":"",border:a.trim(z.width+" "+z.style+" "+z.color),"text-align":this.src.main.align.val()||""};
var j=this.src.main.padding.val();if(j.css){u.padding=j.css}else{u["padding-top"]=j.top;u["padding-right"]=j.right;u["padding-bottom"]=j.bottom;u["padding-left"]=j.left}x=x.get();a.each(x,function(){var w=this.nodeName.toLowerCase();var F=a(this);if(w!=D){var h={};for(var t in b.src.adv){var p=F.attr(t);if(p){h[t]=p.toString()}}for(var t in b.src.events){var p=F.attr(t);if(p){h[t]=p.toString()}}var G=F.attr("colspan")||1;var E=F.attr("rowspan")||1;if(G>1){h.colspan=G}if(E>1){h.rowspan=E}F.replaceWith(a("<"+D+" />").html(F.html()).attr(h).css(u))
}else{F.css(u)}});this.rte.ui.update()};this.update=function(){if(this.rte.dom.parent(this.rte.selection.getNode(),/^TABLE$/)){this.domElem.removeClass("disabled")}else{this.domElem.addClass("disabled")}}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.tbcellsmerge=function(g,f){this.constructor.prototype.constructor.call(this,g,f);var c=this;function b(){var j=c.rte.dom.selfOrParent(c.rte.selection.getStart(),/^(TD|TH)$/);var h=c.rte.dom.selfOrParent(c.rte.selection.getEnd(),/^(TD|TH)$/);
if(j&&h&&j!=h&&a(j).parents("table").get(0)==a(h).parents("table").get(0)){return[j,h]}return null}this.command=function(){var z=b();if(z){var x=this.rte.dom.indexOf(a(z[0]).parent("tr").get(0));var r=this.rte.dom.indexOf(a(z[1]).parent("tr").get(0));var q=Math.min(x,r);var u=Math.max(x,r)-q+1;var j=this.rte.dom.tableColumn(z[0],true,true);var h=this.rte.dom.tableColumn(z[1],true);var B=a.inArray(z[0],j.column);var w=a.inArray(z[1],h.column);var p=j.info.offset[B]<h.info.offset[w]?j:h;var v=j.info.offset[B]>=h.info.offset[w]?j:h;
var k=0;var t=null;var o="";this.rte.history.add();var A=a(a(z[0]).parents("table").eq(0).find("tr").get().slice(q,q+u)).each(function(E){var D=o.length;var C=false;a(this).children("td,th").each(function(){var K=a(this);var I=a.inArray(this,p.column);var H=a.inArray(this,v.column);if(I!=-1||H!=-1){C=I!=-1&&H==-1;var G=parseInt(K.attr("colspan")||1);if(E==0){k+=G}if(I!=-1&&E>0){var L=p.info.delta[I];if(L>0){if(K.css("text-align")=="left"){var F=K.clone(true);K.html("&nbsp;")}else{var F=K.clone().html("&nbsp;")
}F.removeAttr("colspan").removeAttr("id").insertBefore(this);if(L>1){F.attr("colspan",L)}}}if(H!=-1){var L=v.info.delta[H];if(G-L>1){var J=G-L-1;if(K.css("text-align")=="right"){var F=K.clone(true);K.html("&nbsp;")}else{var F=K.clone().html("&nbsp;")}F.removeAttr("colspan").removeAttr("id").insertAfter(this);if(J>1){F.attr("colspan",J)}}}if(!t){t=K}else{o+=K.html();K.remove()}}else{if(C){if(E==0){k+=parseInt(K.attr("colspan")||1)}o+=K.html();K.remove()}}});o+=D!=o.length?"<br />":""});t.removeAttr("colspan").removeAttr("rowspan").html(t.html()+o);
if(k>1){t.attr("colspan",k)}if(u>1){t.attr("rowspan",u)}this.rte.dom.fixTable(a(z[0]).parents("table").get(0))}};this.update=function(){if(b()){this.domElem.removeClass("disabled")}else{this.domElem.addClass("disabled")}}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.tbcellsplit=function(c,b){this.constructor.prototype.constructor.call(this,c,b);this.command=function(){var j=this.rte.dom.selfOrParent(this.rte.selection.getNode(),/^(TD|TH)$/);if(j){this.rte.history.add();var k=parseInt(this.rte.dom.attr(j,"colspan"));
var o=parseInt(this.rte.dom.attr(j,"rowspan"));if(k>1||o>1){var u=k-1;var v=o-1;var q=this.rte.dom.parent(j,/^TABLE$/);var p=this.rte.dom.tableMatrix(q);if(u){for(var t=0;t<u;t++){a(this.rte.dom.create(j.nodeName)).html("&nbsp;").insertAfter(j)}}if(v){var x=this.rte.dom.indexesOfCell(j,p);var h=x[0];var g=x[1];for(var f=h+1;f<h+v+1;f++){var w;if(!p[f][g].nodeName){if(p[f][g-1].nodeName){w=p[f][g-1]}else{for(var t=g-1;t>=0;t--){if(p[f][t].nodeName){w=p[f][t];break}}}if(w){for(var t=0;t<=u;t++){a(this.rte.dom.create(w.nodeName)).html("&nbsp;").insertAfter(w)
}}}}}a(j).removeAttr("colspan").removeAttr("rowspan");this.rte.dom.fixTable(q)}}this.rte.ui.update(true)};this.update=function(){var f=this.rte.dom.selfOrParent(this.rte.selection.getNode(),/^(TD|TH)$/);if(f&&(parseInt(this.rte.dom.attr(f,"colspan"))>1||parseInt(this.rte.dom.attr(f,"rowspan"))>1)){this.domElem.removeClass("disabled")}else{this.domElem.addClass("disabled")}}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.tbcolbefore=function(f,c){this.constructor.prototype.constructor.call(this,f,c);
var b=this;this.command=function(){var g=this;var h=this.rte.dom.tableColumn(this.rte.selection.getNode(),false,true);if(h.length){this.rte.history.add();a.each(h,function(){var k=a(this);var j=parseInt(k.attr("colspan")||1);if(j>1){k.attr("colspan",j+1)}else{var o=a(g.rte.dom.create(this.nodeName)).html("&nbsp;");if(g.name=="tbcolbefore"){o.insertBefore(this)}else{o.insertAfter(this)}}});this.rte.ui.update()}};this.update=function(){if(this.rte.dom.selfOrParent(this.rte.selection.getNode(),/^(TD|TH)$/)){this.domElem.removeClass("disabled")
}else{this.domElem.addClass("disabled")}}};elRTE.prototype.ui.prototype.buttons.tbcolafter=elRTE.prototype.ui.prototype.buttons.tbcolbefore})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.tbcolrm=function(f,c){this.constructor.prototype.constructor.call(this,f,c);var b=this;this.command=function(){var p=this.rte.selection.getNode();var o=this.rte.dom.selfOrParent(p,/^(TD|TH)$/);var k=a(o).prev("td,th").get(0);var j=a(o).next("td,th").get(0);var g=this.rte.dom.parent(p,/^TABLE$/);var h=this.rte.dom.tableColumn(p,false,true);
if(h.length){this.rte.history.add();a.each(h,function(){var r=a(this);var q=parseInt(r.attr("colspan")||1);if(q>1){r.attr("colspan",q-1)}else{r.remove()}});this.rte.dom.fixTable(g);if(k||j){this.rte.selection.selectContents(k?k:j).collapse(true)}this.rte.ui.update(true)}};this.update=function(){if(this.rte.dom.selfOrParent(this.rte.selection.getNode(),/^(TD|TH)$/)){this.domElem.removeClass("disabled")}else{this.domElem.addClass("disabled")}}}})(jQuery);elRTE.prototype.ui.prototype.buttons.tbrowbefore=function(b,a){this.constructor.prototype.constructor.call(this,b,a);
this.command=function(){var h=this.rte.selection.getNode();var t=this.rte.dom.selfOrParent(h,/^(TD|TH)$/);var f=this.rte.dom.selfOrParent(t,/^TR$/);var w=this.rte.dom.tableMatrix(this.rte.dom.selfOrParent(t,/^TABLE$/));if(t&&f&&w){this.rte.history.add();var u=this.name=="tbrowbefore";var q=$(f).prevAll("tr").length;var j=0;var p=[];function k(c,r){while(r>0){r--;if(w[r]&&w[r][c]&&w[r][c].nodeName){return w[r][c]}}}for(var o=0;o<w[q].length;o++){if(w[q][o]&&w[q][o].nodeName){var v=$(w[q][o]);var g=parseInt(v.attr("colspan")||1);
if(parseInt(v.attr("rowspan")||1)>1){if(u){j+=g}else{p.push(v)}}else{j+=g}}else{if(w[q][o]=="-"){v=k(o,q);v&&p.push($(v))}}}var x=$(this.rte.dom.create("tr"));for(var o=0;o<j;o++){x.append("<td>&nbsp;</td>")}if(u){x.insertBefore(f)}else{x.insertAfter(f)}$.each(p,function(){$(this).attr("rowspan",parseInt($(this).attr("rowspan")||1)+1)});this.rte.ui.update()}};this.update=function(){if(this.rte.dom.selfOrParent(this.rte.selection.getNode(),/^TR$/)){this.domElem.removeClass("disabled")}else{this.domElem.addClass("disabled")
}}};elRTE.prototype.ui.prototype.buttons.tbrowafter=elRTE.prototype.ui.prototype.buttons.tbrowbefore;(function(a){elRTE.prototype.ui.prototype.buttons.tbrowrm=function(f,c){this.constructor.prototype.constructor.call(this,f,c);var b=this;this.command=function(){var h=this.rte.selection.getNode(),x=this.rte.dom.selfOrParent(h,/^(TD|TH)$/),g=this.rte.dom.selfOrParent(x,/^TR$/),q=this.rte.dom.selfOrParent(x,/^TABLE$/),A=this.rte.dom.tableMatrix(q);if(x&&g&&A.length){this.rte.history.add();if(A.length==1){a(q).remove();
return this.rte.ui.update()}var u=[];var v=a(g).prevAll("tr").length;function p(r,C){while(C>0){C--;if(A[C]&&A[C][r]&&A[C][r].nodeName){return A[C][r]}}}function B(C,r){y=v+1;var E=null;if(A[y]){for(var D=0;D<r;D++){if(A[y][D]&&A[y][D].nodeName){E=A[y][D]}}C=C.remove();if(E){C.insertAfter(E)}else{C.prependTo(a(g).next("tr").eq(0))}}}function o(C){for(var r=0;r<C.length;r++){if(C[r]==x){return r<C.length-1?C[r+1]:C[r-1]}}}for(var t=0;t<A[v].length;t++){var z=null;var j=false;if(A[v][t]&&A[v][t].nodeName){z=A[v][t];
j=true}else{if(A[v][t]=="-"&&(z=p(t,v))){j=false}}if(z){z=a(z);var k=parseInt(z.attr("rowspan")||1);if(k>1){z.attr("rowspan",k-1);j&&B(z,t,v)}}}var w=o(this.rte.dom.tableColumn(x));if(w){this.rte.selection.selectContents(w).collapse(true)}a(g).remove()}this.rte.ui.update()};this.update=function(){if(this.rte.dom.selfOrParent(this.rte.selection.getNode(),/^TR$/)){this.domElem.removeClass("disabled")}else{this.domElem.addClass("disabled")}}}})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.undo=function(c,b){this.constructor.prototype.constructor.call(this,c,b);
this.command=function(){if(this.name=="undo"&&this.rte.history.canBack()){this.rte.history.back();this.rte.ui.update()}else{if(this.name=="redo"&&this.rte.history.canFwd()){this.rte.history.fwd();this.rte.ui.update()}}};this.update=function(){this.domElem.toggleClass("disabled",this.name=="undo"?!this.rte.history.canBack():!this.rte.history.canFwd())}};elRTE.prototype.ui.prototype.buttons.redo=elRTE.prototype.ui.prototype.buttons.undo})(jQuery);(function(a){elRTE.prototype.ui.prototype.buttons.unlink=function(c,b){this.constructor.prototype.constructor.call(this,c,b);
this.command=function(){var k=this.rte.selection.getNode(),f=this.rte.dom.selfOrParentLink(k);function j(o){return o.nodeName=="A"&&o.href}if(!f){var h=a.browser.msie?this.rte.selection.selected():this.rte.selection.selected({wrap:false});if(h.length){for(var g=0;g<h.length;g++){if(j(h[g])){f=h[g];break}}if(!f){f=this.rte.dom.parent(h[0],j)||this.rte.dom.parent(h[h.length-1],j)}}}if(f){this.rte.history.add();this.rte.selection.select(f);this.rte.doc.execCommand("unlink",false,null);this.rte.ui.update(true)
}};this.update=function(){var f=this.rte.selection.getNode();if(this.rte.dom.selfOrParentLink(f)){this.domElem.removeClass("disabled").addClass("active")}else{if(this.rte.dom.selectionHas(function(g){return g.nodeName=="A"&&g.href})){this.domElem.removeClass("disabled").addClass("active")}else{this.domElem.addClass("disabled").removeClass("active")}}}}})(jQuery);


 
 /*
 *elfinder.min.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 */


 (function(a){elFinder=function(d,g){var b=this,h;this.log=function(i){window.console&&window.console.log&&window.console.log(i)};this.options=a.extend({},this.options,g||{});if(!this.options.url){alert("Invalid configuration! You have to set URL option.");return}this.id="";if((h=a(d).attr("id"))){this.id=h}else{}this.version="1.2";this.jquery=a.fn.jquery.split(".").join("");this.cwd={};this.cdc={};this.buffer={};this.selected=[];this.history=[];this.locked=false;this.zIndex=2;this.dialog=null;this.anchor=this.options.docked?a("<div/>").hide().insertBefore(d):null;this.params={dotFiles:false,arc:"",uplMaxSize:""};this.vCookie="el-finder-view-"+this.id;this.pCookie="el-finder-places-"+this.id;this.lCookie="el-finder-last-"+this.id;this.view=new this.view(this,d);this.ui=new this.ui(this);this.eventsManager=new this.eventsManager(this);this.quickLook=new this.quickLook(this);this.cookie=function(j,l){if(typeof l=="undefined"){if(document.cookie&&document.cookie!=""){var k,p=document.cookie.split(";");j+="=";for(k=0;k<p.length;k++){p[k]=a.trim(p[k]);if(p[k].substring(0,j.length)==j){return decodeURIComponent(p[k].substring(j.length))}}}return""}else{var n,m=a.extend({},this.options.cookie);if(l===null){l="";m.expires=-1}if(typeof(m.expires)=="number"){n=new Date();n.setTime(n.getTime()+(m.expires*24*60*60*1000));m.expires=n}document.cookie=j+"="+encodeURIComponent(l)+"; expires="+m.expires.toUTCString()+(m.path?"; path="+m.path:"")+(m.domain?"; domain="+m.domain:"")+(m.secure?"; secure":"")}};this.lock=function(i){this.view.spinner((this.locked=i||false));this.eventsManager.lock=this.locked};this.lockShortcuts=function(i){this.eventsManager.lock=!!i};this.setView=function(i){if(i=="list"||i=="icons"){this.options.view=i;this.cookie(this.vCookie,i)}};this.ajax=function(k,l,i){var j={url:this.options.url,async:true,type:"GET",data:k,dataType:"json",cache:false,lock:true,force:false,silent:false};if(typeof(i)=="object"){j=a.extend({},j,i)}if(!j.silent){j.error=b.view.fatal}j.success=function(m){j.lock&&b.lock();if(m){m.debug&&b.log(m.debug);if(m.error){!j.silent&&b.view.error(m.error,m.errorData);if(!j.force){return}}l(m);delete m}};j.lock&&this.lock(true);a.ajax(j)};this.tmb=function(){this.ajax({cmd:"tmb",current:b.cwd.hash},function(k){if(b.options.view=="icons"&&k.images&&k.current==b.cwd.hash){for(var j in k.images){if(b.cdc[j]){b.cdc[j].tmb=k.images[j];a('div[key="'+j+'"]>p',b.view.cwd).css("background",' url("'+k.images[j]+'") 0 0 no-repeat')}}k.tmb&&b.tmb()}},{lock:false,silent:true})};this.getPlaces=function(){var i=[],j=this.cookie(this.pCookie);if(j.length){if(j.indexOf(":")!=-1){i=j.split(":")}else{i.push(j)}}return i};this.addPlace=function(j){var i=this.getPlaces();if(a.inArray(j,i)==-1){i.push(j);this.savePlaces(i);return true}};this.removePlace=function(j){var i=this.getPlaces();if(a.inArray(j,i)!=-1){this.savePlaces(a.map(i,function(k){return k==j?null:k}));return true}};this.savePlaces=function(i){this.cookie(this.pCookie,i.join(":"))};this.reload=function(m){var k;this.cwd=m.cwd;this.cdc={};for(k=0;k<m.cdc.length;k++){if(m.cdc[k].hash&&m.cdc[k].name){this.cdc[m.cdc[k].hash]=m.cdc[k];this.cwd.size+=m.cdc[k].size}}if(m.tree){this.view.renderNav(m.tree);this.eventsManager.updateNav()}this.updateCwd();if(m.tmb&&!b.locked&&b.options.view=="icons"){b.tmb()}if(m.select&&m.select.length){var j=m.select.length;while(j--){this.cdc[m.select[j]]&&this.selectById(m.select[j])}}this.lastDir(this.cwd.hash);if(this.options.autoReload>0){this.iID&&clearInterval(this.iID);this.iID=setInterval(function(){!b.locked&&b.ui.exec("reload")},this.options.autoReload*60000)}};this.updateCwd=function(){this.lockShortcuts(true);this.selected=[];this.view.renderCwd();this.eventsManager.updateCwd();this.view.tree.find('a[key="'+this.cwd.hash+'"]').trigger("select");this.lockShortcuts()};this.drop=function(l,j,k){if(j.helper.find('[key="'+k+'"]').length){return b.view.error("Unable to copy into itself")}var i=[];j.helper.find('div:not(.noaccess):has(>label):not(:has(em[class="readonly"],em[class=""]))').each(function(){i.push(a(this).hide().attr("key"))});if(!j.helper.find("div:has(>label):visible").length){j.helper.hide()}if(i.length){b.setBuffer(i,l.shiftKey?0:1,k);if(b.buffer.files){setTimeout(function(){b.ui.exec("paste");b.buffer={}},300)}}else{a(this).removeClass("el-finder-droppable")}};this.getSelected=function(j){var k,l=[];if(j>=0){return this.cdc[this.selected[j]]||{}}for(k=0;k<this.selected.length;k++){this.cdc[this.selected[k]]&&l.push(this.cdc[this.selected[k]])}return l};this.select=function(i,j){j&&a(".ui-selected",b.view.cwd).removeClass("ui-selected");i.addClass("ui-selected");b.updateSelect()};this.selectById=function(j){var i=a('[key="'+j+'"]',this.view.cwd);if(i.length){this.select(i);this.checkSelectedPos()}};this.unselect=function(i){i.removeClass("ui-selected");b.updateSelect()};this.toggleSelect=function(i){i.toggleClass("ui-selected");this.updateSelect()};this.selectAll=function(){a("[key]",b.view.cwd).addClass("ui-selected");b.updateSelect()};this.unselectAll=function(){a(".ui-selected",b.view.cwd).removeClass("ui-selected");b.updateSelect()};this.updateSelect=function(){b.selected=[];a(".ui-selected",b.view.cwd).each(function(){b.selected.push(a(this).attr("key"))});b.view.selectedInfo();b.ui.update();b.quickLook.update()};this.checkSelectedPos=function(k){var j=b.view.cwd.find(".ui-selected:"+(k?"last":"first")).eq(0),l=j.position(),i=j.outerHeight(),m=b.view.cwd.height();if(l.top<0){b.view.cwd.scrollTop(l.top+b.view.cwd.scrollTop()-2)}else{if(m-l.top<i){b.view.cwd.scrollTop(l.top+i-m+b.view.cwd.scrollTop())}}};this.setBuffer=function(k,m,o){var j,n,l;this.buffer={src:this.cwd.hash,dst:o,files:[],names:[],cut:m||0};for(j=0;j<k.length;j++){n=k[j];l=this.cdc[n];if(l&&l.read&&l.type!="link"){this.buffer.files.push(l.hash);this.buffer.names.push(l.name)}}if(!this.buffer.files.length){this.buffer={}}};this.isValidName=function(i){if(!this.cwd.dotFiles&&i.indexOf(".")==0){return false}return i.match(/^[^\\\/\<\>:]+$/)};this.fileExists=function(k){for(var j in this.cdc){if(this.cdc[j].name==k){return j}}return false};this.uniqueName=function(m,l){m=b.i18n(m);var j=m,k=0,l=l||"";if(!this.fileExists(j+l)){return j+l}while(k++<100){if(!this.fileExists(j+k+l)){return j+k+l}}return j.replace("100","")+Math.random()+l};this.lastDir=function(i){if(this.options.rememberLastDir){return i?this.cookie(this.lCookie,i):this.cookie(this.lCookie)}};function c(i,j){i&&b.view.win.width(i);j&&b.view.nav.add(b.view.cwd).height(j)}function e(){c(null,b.dialog.height()-b.view.tlb.parent().height()-(a.browser.msie?47:32))}this.time=function(){return new Date().getMilliseconds()};this.setView(this.cookie(this.vCookie));c(b.options.width,b.options.height);if(this.options.dialog||this.options.docked){this.options.dialog=a.extend({width:570,dialogClass:"",minWidth:480,minHeight:330},this.options.dialog||{});this.options.dialog.open=function(){setTimeout(function(){a('<input type="text" value="f"/>').appendTo(b.view.win).focus().select().remove()},200)};this.options.dialog.dialogClass+="el-finder-dialog";this.options.dialog.resize=e;if(this.options.docked){this.options.dialog.close=function(){b.dock()};this.view.win.data("size",{width:this.view.win.width(),height:this.view.nav.height()})}else{this.options.dialog.close=function(){b.destroy()};this.dialog=a("<div/>").append(this.view.win).dialog(this.options.dialog)}}this.ajax({cmd:"open",target:this.lastDir()||"",init:true,tree:true},function(i){if(i.cwd){b.eventsManager.init();b.reload(i);a.extend(b.params,i.params||{});a("*",document.body).each(function(){var j=parseInt(a(this).css("z-index"));if(j>=b.zIndex){b.zIndex=j+1}});b.ui.init(i.disabled)}},{force:true});this.open=function(){this.dialog?this.dialog.dialog("open"):this.view.win.show();this.eventsManager.lock=false};this.close=function(){this.quickLook.hide();if(this.options.docked&&this.view.win.attr("undocked")){this.dock()}else{this.dialog?this.dialog.dialog("close"):this.view.win.hide()}this.eventsManager.lock=true};this.destroy=function(){this.eventsManager.lock=true;this.quickLook.hide();this.quickLook.win.remove();if(this.dialog){this.dialog.dialog("destroy");this.view.win.parent().remove()}else{this.view.win.remove()}this.ui.menu.remove()};this.dock=function(){if(this.options.docked&&this.view.win.attr("undocked")){this.quickLook.hide();var i=this.view.win.data("size");this.view.win.insertAfter(this.anchor).removeAttr("undocked");c(i.width,i.height);this.dialog.dialog("destroy");this.dialog=null}};this.undock=function(){if(this.options.docked&&!this.view.win.attr("undocked")){this.quickLook.hide();this.dialog=a("<div/>").append(this.view.win.css("width","100%").attr("undocked",true).show()).dialog(this.options.dialog);e()}}};elFinder.prototype.i18n=function(b){return this.options.i18n[this.options.lang]&&this.options.i18n[this.options.lang][b]?this.options.i18n[this.options.lang][b]:b};elFinder.prototype.options={url:"",lang:"en",cssClass:"",wrap:14,places:"Places",placesFirst:true,editorCallback:null,cutURL:"",closeOnEditorCallback:true,i18n:{},view:"icons",width:"",height:"",disableShortcuts:false,rememberLastDir:true,cookie:{expires:30,domain:"",path:"/",secure:false},toolbar:[["back","reload"],["select","open"],["mkdir","mkfile","upload"],["copy","paste","rm"],["rename","edit"],["info","quicklook","resize"],["icons","list"],["help"]],contextmenu:{cwd:["reload","delim","mkdir","mkfile","upload","delim","paste","delim","info"],file:["select","open","quicklook","delim","copy","cut","rm","delim","duplicate","rename","edit","resize","archive","extract","delim","info"],group:["select","copy","cut","rm","delim","archive","extract","delim","info"]},dialog:null,docked:false,autoReload:0,selectMultiple:false};a.fn.elfinder=function(b){return this.each(function(){var c=typeof(b)=="string"?b:"";if(!this.elfinder){this.elfinder=new elFinder(this,typeof(b)=="object"?b:{})}switch(c){case"close":case"hide":this.elfinder.close();break;case"open":case"show":this.elfinder.open();break;case"dock":this.elfinder.dock();break;case"undock":this.elfinder.undock();break;case"destroy":this.elfinder.destroy();break}})}})(jQuery);(function(a){elFinder.prototype.view=function(d,c){var b=this;this.fm=d;this.kinds={unknown:"Unknown",directory:"Folder",symlink:"Alias","symlink-broken":"Broken alias","application/x-empty":"Plain text","application/postscript":"Postscript document","application/octet-stream":"Application","application/vnd.ms-office":"Microsoft Office document","application/vnd.ms-word":"Microsoft Word document","application/vnd.ms-excel":"Microsoft Excel document","application/vnd.ms-powerpoint":"Microsoft Powerpoint presentation","application/pdf":"Portable Document Format (PDF)","application/vnd.oasis.opendocument.text":"Open Office document","application/x-shockwave-flash":"Flash application","application/xml":"XML document","application/x-bittorrent":"Bittorrent file","application/x-7z-compressed":"7z archive","application/x-tar":"TAR archive","application/x-gzip":"GZIP archive","application/x-bzip2":"BZIP archive","application/zip":"ZIP archive","application/x-rar":"RAR archive","application/javascript":"Javascript application","text/plain":"Plain text","text/x-php":"PHP source","text/html":"HTML document","text/javascript":"Javascript source","text/css":"CSS style sheet","text/rtf":"Rich Text Format (RTF)","text/rtfd":"RTF with attachments (RTFD)","text/x-c":"C source","text/x-c++":"C++ source","text/x-shellscript":"Unix shell script","text/x-python":"Python source","text/x-java":"Java source","text/x-ruby":"Ruby source","text/x-perl":"Perl script","text/xml":"XML document","image/x-ms-bmp":"BMP image","image/jpeg":"JPEG image","image/gif":"GIF Image","image/png":"PNG image","image/x-targa":"TGA image","image/tiff":"TIFF image","image/vnd.adobe.photoshop":"Adobe Photoshop image","audio/mpeg":"MPEG audio","audio/midi":"MIDI audio","audio/ogg":"Ogg Vorbis audio","audio/mp4":"MP4 audio","audio/wav":"WAV audio","video/x-dv":"DV video","video/mp4":"MP4 video","video/mpeg":"MPEG video","video/x-msvideo":"AVI video","video/quicktime":"Quicktime video","video/x-ms-wmv":"WM video","video/x-flv":"Flash video","video/x-matroska":"Matroska video"};this.tlb=a("<ul />");this.nav=a('<div class="el-finder-nav"/>').resizable({handles:"e",autoHide:true,minWidth:200,maxWidth:500});this.cwd=a('<div class="el-finder-cwd"/>').attr("unselectable","on");this.spn=a('<div class="el-finder-spinner"/>');this.err=a('<p class="el-finder-err"><strong/></p>').click(function(){a(this).hide()});this.nfo=a('<div class="el-finder-stat"/>');this.pth=a('<div class="el-finder-path"/>');this.sel=a('<div class="el-finder-sel"/>');this.stb=a('<div class="el-finder-statusbar"/>').append(this.pth).append(this.nfo).append(this.sel);this.wrz=a('<div class="el-finder-workzone" />').append(this.nav).append(this.cwd).append(this.spn).append(this.err).append('<div style="clear:both" />');this.win=a(c).empty().attr("id",this.fm.id).addClass("el-finder "+(d.options.cssClass||"")).append(a('<div class="el-finder-toolbar" />').append(this.tlb)).append(this.wrz).append(this.stb);this.tree=a('<ul class="el-finder-tree"></ul>').appendTo(this.nav);this.plc=a('<ul class="el-finder-places"><li><a href="#" class="el-finder-places-root"><div/>'+this.fm.i18n(this.fm.options.places)+"</a><ul/></li></ul>").hide();this.nav[this.fm.options.placesFirst?"prepend":"append"](this.plc);this.spinner=function(e){this.win.toggleClass("el-finder-disabled",e);this.spn.toggle(e)};this.fatal=function(e){b.error(e.status!="404"?"Invalid backend configuration":"Unable to connect to backend")};this.error=function(e,g){this.fm.lock();this.err.show().children("strong").html(this.fm.i18n(e)+"!"+this.formatErrorData(g));setTimeout(function(){b.err.fadeOut("slow")},4000)};this.renderNav=function(g){var i=g.dirs.length?h(g.dirs):"",e='<li><a href="#" class="el-finder-tree-root" key="'+g.hash+'"><div'+(i?' class="collapsed expanded"':"")+"/>"+g.name+"</a>"+i+"</li>";this.tree.html(e);this.fm.options.places&&this.renderPlaces();function h(j){var l,m,n,k='<ul style="display:none">';for(l=0;l<j.length;l++){if(!j[l].name||!j[l].hash){continue}n="";if(!j[l].read&&!j[l].write){n="noaccess"}else{if(!j[l].read){n="dropbox"}else{if(!j[l].write){n="readonly"}}}k+='<li><a href="#" class="'+n+'" key="'+j[l].hash+'"><div'+(j[l].dirs.length?' class="collapsed"':"")+"/>"+j[l].name+"</a>";if(j[l].dirs.length){k+=h(j[l].dirs)}k+="</li>"}return k+"</ul>"}};this.renderPlaces=function(){var g,j,h=this.fm.getPlaces(),e=this.plc.show().find("ul").empty().hide();a("div:first",this.plc).removeClass("collapsed expanded");if(h.length){h.sort(function(k,i){var m=b.tree.find('a[key="'+k+'"]').text()||"",l=b.tree.find('a[key="'+i+'"]').text()||"";return m.localeCompare(l)});for(g=0;g<h.length;g++){if((j=this.tree.find('a[key="'+h[g]+'"]:not(.dropbox)').parent())&&j.length){e.append(j.clone().children("ul").remove().end().find("div").removeClass("collapsed expanded").end())}else{this.fm.removePlace(h[g])}}e.children().length&&a("div:first",this.plc).addClass("collapsed")}};this.renderCwd=function(){this.cwd.empty();var e=0,h=0,g="";for(var i in this.fm.cdc){e++;h+=this.fm.cdc[i].size;g+=this.fm.options.view=="icons"?this.renderIcon(this.fm.cdc[i]):this.renderRow(this.fm.cdc[i],e%2)}if(this.fm.options.view=="icons"){this.cwd.append(g)}else{this.cwd.append('<table><tr><th colspan="2">'+this.fm.i18n("Name")+"</th><th>"+this.fm.i18n("Permissions")+"</th><th>"+this.fm.i18n("Modified")+'</th><th class="size">'+this.fm.i18n("Size")+"</th><th>"+this.fm.i18n("Kind")+"</th></tr>"+g+"</table>")}this.pth.text(d.cwd.rel);this.nfo.text(d.i18n("items")+": "+e+", "+this.formatSize(h));this.sel.empty()};this.renderIcon=function(e){var g="<p"+(e.tmb?" style=\"background:url('"+e.tmb+"') 0 0 no-repeat\"":"")+"/><label>"+this.formatName(e.name)+"</label>";if(e.link||e.mime=="symlink-broken"){g+="<em/>"}if(!e.read&&!e.write){g+='<em class="noaccess"/>'}else{if(e.read&&!e.write){g+='<em class="readonly"/>'}else{if(!e.read&&e.write){g+='<em class="'+(e.mime=="directory"?"dropbox":"noread")+'" />'}}}return'<div class="'+this.mime2class(e.mime)+'" key="'+e.hash+'">'+g+"</div>"};this.renderRow=function(g,e){var h=g.link||g.mime=="symlink-broken"?"<em/>":"";if(!g.read&&!g.write){h+='<em class="noaccess"/>'}else{if(g.read&&!g.write){h+='<em class="readonly"/>'}else{if(!g.read&&g.write){h+='<em class="'+(g.mime=="directory"?"dropbox":"noread")+'" />'}}}return'<tr key="'+g.hash+'" class="'+b.mime2class(g.mime)+(e?" el-finder-row-odd":"")+'"><td class="icon"><p>'+h+"</p></td><td>"+g.name+"</td><td>"+b.formatPermissions(g.read,g.write,g.rm)+"</td><td>"+b.formatDate(g.date)+'</td><td class="size">'+b.formatSize(g.size)+"</td><td>"+b.mime2kind(g.link?"symlink":g.mime)+"</td></tr>"};this.updateFile=function(g){var h=this.cwd.find('[key="'+g.hash+'"]');h.replaceWith(h[0].nodeName=="DIV"?this.renderIcon(g):this.renderRow(g))};this.selectedInfo=function(){var e,g=0,h;if(b.fm.selected.length){h=this.fm.getSelected();for(e=0;e<h.length;e++){g+=h[e].size}}this.sel.text(e>0?this.fm.i18n("selected items")+": "+h.length+", "+this.formatSize(g):"")};this.formatName=function(g){var e=b.fm.options.wrap;if(e>0){if(g.length>e*2){return g.substr(0,e)+"&shy;"+g.substr(e,e-5)+"&hellip;"+g.substr(g.length-3)}else{if(g.length>e){return g.substr(0,e)+"&shy;"+g.substr(e)}}}return g};this.formatErrorData=function(h){var e,g="";if(typeof(h)=="object"){g="<br />";for(e in h){g+=e+" "+b.fm.i18n(h[e])+"<br />"}}return g};this.mime2class=function(e){return e.replace("/"," ").replace(/\./g,"-")};this.formatDate=function(e){return e.replace(/([a-z]+)\s/i,function(h,g){return b.fm.i18n(g)+" "})};this.formatSize=function(g){var h=1,e="bytes";if(g>1073741824){h=1073741824;e="Gb"}else{if(g>1048576){h=1048576;e="Mb"}else{if(g>1024){h=1024;e="Kb"}}}return Math.round(g/h)+" "+e};this.formatPermissions=function(g,e,i){var h=[];g&&h.push(b.fm.i18n("read"));e&&h.push(b.fm.i18n("write"));i&&h.push(b.fm.i18n("remove"));return h.join("/")};this.mime2kind=function(e){return this.fm.i18n(this.kinds[e]||"unknown")}}})(jQuery);(function(a){elFinder.prototype.ui=function(c){var b=this;this.fm=c;this.cmd={};this.buttons={};this.menu=a('<div class="el-finder-contextmenu" />').appendTo(document.body).hide();this.dockButton=a('<div class="el-finder-dock-button" title="'+b.fm.i18n("Dock/undock filemanager window")+'" />');this.exec=function(e,d){if(this.cmd[e]){if(e!="open"&&!this.cmd[e].isAllowed()){return this.fm.view.error("Command not allowed")}if(!this.fm.locked){this.fm.quickLook.hide();a(".el-finder-info").remove();this.cmd[e].exec(d);this.update()}}};this.cmdName=function(d){if(this.cmd[d]&&this.cmd[d].name){return d=="archive"&&this.fm.params.archives.length==1?this.fm.i18n("Create")+" "+this.fm.view.mime2kind(this.fm.params.archives[0]).toLowerCase():this.fm.i18n(this.cmd[d].name)}return d};this.isCmdAllowed=function(d){return b.cmd[d]&&b.cmd[d].isAllowed()};this.execIfAllowed=function(d){this.isCmdAllowed(d)&&this.exec(d)};this.includeInCm=function(e,d){return this.isCmdAllowed(e)&&this.cmd[e].cm(d)};this.showMenu=function(i){var g,h,d,k="";this.hideMenu();if(!b.fm.selected.length){g="cwd"}else{if(b.fm.selected.length==1){g="file"}else{g="group"}}j(g);h=a(window);d={height:h.height(),width:h.width(),sT:h.scrollTop(),cW:this.menu.width(),cH:this.menu.height()};this.menu.css({left:((i.clientX+d.cW)>d.width?(i.clientX-d.cW):i.clientX),top:((i.clientY+d.cH)>d.height&&i.clientY>d.cH?(i.clientY+d.sT-d.cH):i.clientY+d.sT)}).show().find("div[name]").hover(function(){var l=a(this),m=l.children("div"),e;l.addClass("hover");if(m.length){if(!m.attr("pos")){e=l.outerWidth();m.css(a(window).width()-e-l.offset().left>m.width()?"left":"right",e-5).attr("pos",true)}m.show()}},function(){a(this).removeClass("hover").children("div").hide()}).click(function(m){m.stopPropagation();var l=a(this);if(!l.children("div").length){b.hideMenu();b.exec(l.attr("name"),l.attr("argc"))}});function j(q){var p,n,m,o,e,r=b.fm.options.contextmenu[q]||[];for(p=0;p<r.length;p++){if(r[p]=="delim"){b.menu.children().length&&!b.menu.children(":last").hasClass("delim")&&b.menu.append('<div class="delim" />')}else{if(b.fm.ui.includeInCm(r[p],q)){m=b.cmd[r[p]].argc();o="";if(m.length){o='<span/><div class="el-finder-contextmenu-sub" style="z-index:'+(parseInt(b.menu.css("z-index"))+1)+'">';for(var n=0;n<m.length;n++){o+='<div name="'+r[p]+'" argc="'+m[n].argc+'" class="'+m[n]["class"]+'">'+m[n].text+"</div>"}o+="</div>"}b.menu.append('<div class="'+r[p]+'" name="'+r[p]+'">'+o+b.cmdName(r[p])+"</div>")}}}}};this.hideMenu=function(){this.menu.hide().empty()};this.update=function(){for(var d in this.buttons){this.buttons[d].toggleClass("disabled",!this.cmd[d].isAllowed())}};this.init=function(k){var h,d,o,m=false,g=2,l,e=this.fm.options.toolbar;if(!this.fm.options.editorCallback){k.push("select")}if(!this.fm.params.archives.length&&a.inArray("archive",k)==-1){k.push("archive")}for(h in this.commands){if(a.inArray(h,k)==-1){this.commands[h].prototype=this.command.prototype;this.cmd[h]=new this.commands[h](this.fm)}}for(h=0;h<e.length;h++){if(m){this.fm.view.tlb.append('<li class="delim" />')}m=false;for(d=0;d<e[h].length;d++){o=e[h][d];if(this.cmd[o]){m=true;this.buttons[o]=a('<li class="'+o+'" title="'+this.cmdName(o)+'" name="'+o+'" />').appendTo(this.fm.view.tlb).click(function(i){i.stopPropagation()}).bind("click",(function(i){return function(){!a(this).hasClass("disabled")&&i.exec(a(this).attr("name"))}})(this)).hover(function(){!a(this).hasClass("disabled")&&a(this).addClass("el-finder-tb-hover")},function(){a(this).removeClass("el-finder-tb-hover")})}}}this.update();this.menu.css("z-index",this.fm.zIndex);if(this.fm.options.docked){this.dockButton.hover(function(){a(this).addClass("el-finder-dock-button-hover")},function(){a(this).removeClass("el-finder-dock-button-hover")}).click(function(){b.fm.view.win.attr("undocked")?b.fm.dock():b.fm.undock();a(this).trigger("mouseout")}).prependTo(this.fm.view.tlb)}}};elFinder.prototype.ui.prototype.command=function(b){};elFinder.prototype.ui.prototype.command.prototype.isAllowed=function(){return true};elFinder.prototype.ui.prototype.command.prototype.cm=function(b){return false};elFinder.prototype.ui.prototype.command.prototype.argc=function(b){return[]};elFinder.prototype.ui.prototype.commands={back:function(c){var b=this;this.name="Back";this.fm=c;this.exec=function(){if(this.fm.history.length){this.fm.ajax({cmd:"open",target:this.fm.history.pop()},function(d){b.fm.reload(d)})}};this.isAllowed=function(){return this.fm.history.length}},reload:function(c){var b=this;this.name="Reload";this.fm=c;this.exec=function(){this.fm.ajax({cmd:"open",target:this.fm.cwd.hash,tree:true},function(d){b.fm.reload(d)})};this.cm=function(d){return d=="cwd"}},open:function(c){var b=this;this.name="Open";this.fm=c;this.exec=function(e){var g=null;if(e){g={hash:a(e).attr("key"),mime:"directory",read:!a(e).hasClass("noaccess")&&!a(e).hasClass("dropbox")}}else{g=this.fm.getSelected(0)}if(!g.hash){return}if(!g.read){return this.fm.view.error("Access denied")}if(g.type=="link"&&!g.link){return this.fm.view.error("Unable to open broken link")}if(g.mime=="directory"){h(g.link||g.hash)}else{d(g)}function h(i){b.fm.history.push(b.fm.cwd.hash);b.fm.ajax({cmd:"open",target:i},function(j){b.fm.reload(j)})}function d(k){var j,i="";if(k.dim){j=k.dim.split("x");i="width="+(parseInt(j[0])+20)+",height="+(parseInt(j[1])+20)+","}window.open(k.url||b.fm.options.url+"?cmd=open&current="+(k.parent||b.fm.cwd.hash)+"&target="+(k.link||k.hash),false,"top=50,left=50,"+i+"scrollbars=yes,resizable=yes")}};this.isAllowed=function(){return this.fm.selected.length==1&&this.fm.getSelected(0).read};this.cm=function(d){return d=="file"}},select:function(b){this.name="Select file";this.fm=b;if(b.options.selectMultiple){this.exec=function(){var c=a(b.getSelected()).map(function(){return b.options.cutURL=="root"?this.url.substr(b.params.url.length):this.url.replace(new RegExp("^("+b.options.cutURL+")"),"")});b.options.editorCallback(c);if(b.options.closeOnEditorCallback){b.dock();b.close()}}}else{this.exec=function(){var c=this.fm.getSelected(0);if(!c.url){return this.fm.view.error("File URL disabled by connector config")}this.fm.options.editorCallback(this.fm.options.cutURL=="root"?c.url.substr(this.fm.params.url.length):c.url.replace(new RegExp("^("+this.fm.options.cutURL+")"),""));if(this.fm.options.closeOnEditorCallback){this.fm.dock();this.fm.close();this.fm.destroy()}}}this.isAllowed=function(){return((this.fm.options.selectMultiple&&this.fm.selected.length>=1)||this.fm.selected.length==1)&&!/(symlink\-broken|directory)/.test(this.fm.getSelected(0).mime)};this.cm=function(c){return c!="cwd"}},quicklook:function(c){var b=this;this.name="Preview with Quick Look";this.fm=c;this.exec=function(){b.fm.quickLook.toggle()};this.isAllowed=function(){return this.fm.selected.length==1};this.cm=function(){return true}},info:function(c){var b=this;this.name="Get info";this.fm=c;this.exec=function(){var j,i,e=this.fm.selected.length,d=a(window).width(),g=a(window).height();this.fm.lockShortcuts(true);if(!e){k(b.fm.cwd)}else{a.each(this.fm.getSelected(),function(){k(this)})}function k(m){var n=["50%","50%"],h,q,o,l='<table cellspacing="0"><tr><td>'+b.fm.i18n("Name")+"</td><td>"+m.name+"</td></tr><tr><td>"+b.fm.i18n("Kind")+"</td><td>"+b.fm.view.mime2kind(m.link?"symlink":m.mime)+"</td></tr><tr><td>"+b.fm.i18n("Size")+"</td><td>"+b.fm.view.formatSize(m.size)+"</td></tr><tr><td>"+b.fm.i18n("Modified")+"</td><td>"+b.fm.view.formatDate(m.date)+"</td></tr><tr><td>"+b.fm.i18n("Permissions")+"</td><td>"+b.fm.view.formatPermissions(m.read,m.write,m.rm)+"</td></tr>";if(m.link){l+="<tr><td>"+b.fm.i18n("Link to")+"</td><td>"+m.linkTo+"</td></tr>"}if(m.dim){l+="<tr><td>"+b.fm.i18n("Dimensions")+"</td><td>"+m.dim+" px.</td></tr>"}if(m.url){l+="<tr><td>"+b.fm.i18n("URL")+'</td><td><a href="'+m.url+'" target="_blank">'+m.url+"</a></td></tr>"}if(e>1){o=a(".el-finder-dialog-info:last");if(!o.length){h=Math.round(((d-350)/2)-(e*10));q=Math.round(((g-300)/2)-(e*10));n=[h>20?h:20,q>20?q:20]}else{h=o.offset().left+10;q=o.offset().top+10;n=[h<d-350?h:20,q<g-300?q:20]}}a("<div />").append(l+"</table>").dialog({dialogClass:"el-finder-dialog el-finder-dialog-info",width:390,position:n,title:b.fm.i18n(m.mime=="directory"?"Folder info":"File info"),close:function(){if(--e<=0){b.fm.lockShortcuts()}a(this).dialog("destroy")},buttons:{Ok:function(){a(this).dialog("close")}}})}};this.cm=function(d){return true}},rename:function(c){var b=this;this.name="Rename";this.fm=c;this.exec=function(){var i=this.fm.getSelected(),h,l,e,j,k;if(i.length==1){j=i[0];h=this.fm.view.cwd.find('[key="'+j.hash+'"]');l=this.fm.options.view=="icons"?h.children("label"):h.find("td").eq(1);k=l.html();e=a('<input type="text" />').val(j.name).appendTo(l.empty()).bind("change blur",d).keydown(function(m){m.stopPropagation();if(m.keyCode==27){g()}else{if(m.keyCode==13){if(j.name==e.val()){g()}else{a(this).trigger("change")}}}}).click(function(m){m.stopPropagation()}).select().focus();this.fm.lockShortcuts(true)}function g(){l.html(k);b.fm.lockShortcuts()}function d(){if(!b.fm.locked){var n,m=e.val();if(j.name==e.val()){return g()}if(!b.fm.isValidName(m)){n="Invalid name"}else{if(b.fm.fileExists(m)){n="File or folder with the same name already exists"}}if(n){b.fm.view.error(n);h.addClass("ui-selected");b.fm.lockShortcuts(true);return e.select().focus()}b.fm.ajax({cmd:"rename",current:b.fm.cwd.hash,target:j.hash,name:m},function(o){if(o.error){g()}else{j.mime=="directory"&&b.fm.removePlace(j.hash)&&b.fm.addPlace(o.target);b.fm.reload(o)}},{force:true})}}};this.isAllowed=function(){return this.fm.cwd.write&&this.fm.getSelected(0).write};this.cm=function(d){return d=="file"}},copy:function(b){this.name="Copy";this.fm=b;this.exec=function(){this.fm.setBuffer(this.fm.selected)};this.isAllowed=function(){if(this.fm.selected.length){var d=this.fm.getSelected(),c=d.length;while(c--){if(d[c].read){return true}}}return false};this.cm=function(c){return c!="cwd"}},cut:function(b){this.name="Cut";this.fm=b;this.exec=function(){this.fm.setBuffer(this.fm.selected,1)};this.isAllowed=function(){if(this.fm.selected.length){var d=this.fm.getSelected(),c=d.length;while(c--){if(d[c].read&&d[c].rm){return true}}}return false};this.cm=function(c){return c!="cwd"}},paste:function(c){var b=this;this.name="Paste";this.fm=c;this.exec=function(){var e,l,h,g,k="";if(!this.fm.buffer.dst){this.fm.buffer.dst=this.fm.cwd.hash}l=this.fm.view.tree.find('[key="'+this.fm.buffer.dst+'"]');if(!l.length||l.hasClass("noaccess")||l.hasClass("readonly")){return this.fm.view.error("Access denied")}if(this.fm.buffer.src==this.fm.buffer.dst){return this.fm.view.error("Unable to copy into itself")}var j={cmd:"paste",current:this.fm.cwd.hash,src:this.fm.buffer.src,dst:this.fm.buffer.dst,cut:this.fm.buffer.cut};if(this.fm.jquery>132){j.targets=this.fm.buffer.files}else{j["targets[]"]=this.fm.buffer.files}this.fm.ajax(j,function(d){d.cdc&&b.fm.reload(d)},{force:true})};this.isAllowed=function(){return this.fm.buffer.files};this.cm=function(d){return d=="cwd"}},rm:function(c){var b=this;this.name="Remove";this.fm=c;this.exec=function(){var d,g=[],e=this.fm.getSelected();for(var d=0;d<e.length;d++){if(!e[d].rm){return this.fm.view.error(e[d].name+": "+this.fm.i18n("Access denied"))}g.push(e[d].hash)}if(g.length){this.fm.lockShortcuts(true);a('<div><div class="ui-state-error ui-corner-all"><span class="ui-icon ui-icon-alert"/><strong>'+this.fm.i18n("Are you sure you want to remove files?<br /> This cannot be undone!")+"</strong></div></div>").dialog({title:this.fm.i18n("Confirmation required"),dialogClass:"el-finder-dialog",width:350,close:function(){b.fm.lockShortcuts()},buttons:{Cancel:function(){a(this).dialog("close")},Ok:function(){a(this).dialog("close");var h={cmd:"rm",current:b.fm.cwd.hash};if(b.fm.jquery>132){h.targets=g}else{h["targets[]"]=g}b.fm.ajax(h,function(i){i.tree&&b.fm.reload(i)},{force:true})}}})}};this.isAllowed=function(g){if(this.fm.selected.length){var e=this.fm.getSelected(),d=e.length;while(d--){if(e[d].rm){return true}}}return false};this.cm=function(d){return d!="cwd"}},mkdir:function(c){var b=this;this.name="New folder";this.fm=c;this.exec=function(){b.fm.unselectAll();var e=this.fm.uniqueName("untitled folder");input=a('<input type="text"/>').val(e);prev=this.fm.view.cwd.find(".directory:last");f={name:e,hash:"",mime:"directory",read:true,write:true,date:"",size:0},el=this.fm.options.view=="list"?a(this.fm.view.renderRow(f)).children("td").eq(1).empty().append(input).end().end():a(this.fm.view.renderIcon(f)).children("label").empty().append(input).end();el.addClass("directory ui-selected");if(prev.length){el.insertAfter(prev)}else{if(this.fm.options.view=="list"){el.insertAfter(this.fm.view.cwd.find("tr").eq(0))}else{el.prependTo(this.fm.view.cwd)}}b.fm.checkSelectedPos();input.select().focus().click(function(g){g.stopPropagation()}).bind("change blur",d).keydown(function(g){g.stopPropagation();if(g.keyCode==27){el.remove();b.fm.lockShortcuts()}else{if(g.keyCode==13){d()}}});b.fm.lockShortcuts(true);function d(){if(!b.fm.locked){var h,g=input.val();if(!b.fm.isValidName(g)){h="Invalid name"}else{if(b.fm.fileExists(g)){h="File or folder with the same name already exists"}}if(h){b.fm.view.error(h);b.fm.lockShortcuts(true);el.addClass("ui-selected");return input.select().focus()}b.fm.ajax({cmd:"mkdir",current:b.fm.cwd.hash,name:g},function(i){if(i.error){el.addClass("ui-selected");return input.select().focus()}b.fm.reload(i)},{force:true})}}};this.isAllowed=function(){return this.fm.cwd.write};this.cm=function(d){return d=="cwd"}},mkfile:function(c){var b=this;this.name="New text file";this.fm=c;this.exec=function(){b.fm.unselectAll();var i=this.fm.uniqueName("untitled file",".txt"),e=a('<input type="text"/>').val(i),h={name:i,hash:"",mime:"text/plain",read:true,write:true,date:"",size:0},g=this.fm.options.view=="list"?a(this.fm.view.renderRow(h)).children("td").eq(1).empty().append(e).end().end():a(this.fm.view.renderIcon(h)).children("label").empty().append(e).end();g.addClass("text ui-selected").appendTo(this.fm.options.view=="list"?b.fm.view.cwd.children("table"):b.fm.view.cwd);e.select().focus().bind("change blur",d).click(function(j){j.stopPropagation()}).keydown(function(j){j.stopPropagation();if(j.keyCode==27){g.remove();b.fm.lockShortcuts()}else{if(j.keyCode==13){d()}}});b.fm.lockShortcuts(true);function d(){if(!b.fm.locked){var k,j=e.val();if(!b.fm.isValidName(j)){k="Invalid name"}else{if(b.fm.fileExists(j)){k="File or folder with the same name already exists"}}if(k){b.fm.view.error(k);b.fm.lockShortcuts(true);g.addClass("ui-selected");return e.select().focus()}b.fm.ajax({cmd:"mkfile",current:b.fm.cwd.hash,name:j},function(l){if(l.error){g.addClass("ui-selected");return e.select().focus()}b.fm.reload(l)},{force:true})}}};this.isAllowed=function(d){return this.fm.cwd.write};this.cm=function(d){return d=="cwd"}},upload:function(c){var b=this;this.name="Upload files";this.fm=c;this.exec=function(){var g="el-finder-io-"+(new Date().getTime()),l=a('<div class="ui-state-error ui-corner-all"><span class="ui-icon ui-icon-alert"/><div/></div>'),h=this.fm.params.uplMaxSize?"<p>"+this.fm.i18n("Maximum allowed files size")+": "+this.fm.params.uplMaxSize+"</p>":"",s=a('<p class="el-finder-add-field"><span class="ui-state-default ui-corner-all"><em class="ui-icon ui-icon-circle-plus"/></span>'+this.fm.i18n("Add field")+"</p>").click(function(){a(this).before('<p><input type="file" name="upload[]"/></p>')}),k='<form method="post" enctype="multipart/form-data" action="'+b.fm.options.url+'" target="'+g+'"><input type="hidden" name="cmd" value="upload" /><input type="hidden" name="current" value="'+b.fm.cwd.hash+'" />',o=a("<div/>"),j=3;while(j--){k+='<p><input type="file" name="upload[]"/></p>'}var r=a("meta[name=csrf-token]").attr("content");var q=a("meta[name=csrf-param]").attr("content");if(q!=null&&r!=null){k+='<input name="'+q+'" value="'+r+'" type="hidden" />'}k=a(k+"</form>");o.append(k.append(l.hide()).prepend(h).append(s)).dialog({dialogClass:"el-finder-dialog",title:b.fm.i18n("Upload files"),modal:true,resizable:false,close:function(){b.fm.lockShortcuts()},buttons:{Cancel:function(){a(this).dialog("close")},Ok:function(){if(!a(":file[value]",k).length){return p(b.fm.i18n("Select at least one file to upload"))}setTimeout(function(){b.fm.lock();if(a.browser.safari){a.ajax({url:b.fm.options.url,data:{cmd:"ping"},error:n,success:n})}else{n()}});a(this).dialog("close")}}});b.fm.lockShortcuts(true);function p(d){l.show().find("div").empty().text(d)}function n(){var v=a('<iframe name="'+g+'" name="'+g+'" src="about:blank"/>'),w=v[0],i=50,u,e,t;v.css({position:"absolute",top:"-1000px",left:"-1000px"}).appendTo("body").bind("load",function(){v.unbind("load");d()});b.fm.lock(true);k.submit();function d(){try{u=w.contentWindow?w.contentWindow.document:w.contentDocument?w.contentDocument:w.document;if(u.body==null||u.body.innerHTML==""){if(--i){return setTimeout(d,100)}else{m();return b.fm.view.error("Unable to access iframe DOM after 50 tries")}}e=a(u.body).html();if(b.fm.jquery>=141){t=a.parseJSON(e)}else{if(/^[\],:{}\s]*$/.test(e.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,""))){t=window.JSON&&window.JSON.parse?window.JSON.parse(e):(new Function("return "+e))()}else{t={error:"Unable to parse server response"}}}}catch(x){t={error:"Unable to parse server response"}}m();t.error&&b.fm.view.error(t.error,t.errorData);t.cwd&&b.fm.reload(t);t.tmb&&b.fm.tmb()}function m(){b.fm.lock();v.remove()}}};this.isAllowed=function(){return this.fm.cwd.write};this.cm=function(d){return d=="cwd"}},duplicate:function(c){var b=this;this.name="Duplicate";this.fm=c;this.exec=function(){this.fm.ajax({cmd:"duplicate",current:this.fm.cwd.hash,target:this.fm.selected[0]},function(d){b.fm.reload(d)})};this.isAllowed=function(){return this.fm.cwd.write&&this.fm.selected.length==1&&this.fm.getSelected()[0].read};this.cm=function(d){return d=="file"}},edit:function(c){var b=this;this.name="Edit text file";this.fm=c;this.exec=function(){var d=this.fm.getSelected(0);this.fm.lockShortcuts(true);this.fm.ajax({cmd:"read",current:this.fm.cwd.hash,target:d.hash},function(g){b.fm.lockShortcuts(true);var e=a("<textarea/>").val(g.content||"").keydown(function(j){j.stopPropagation();if(j.keyCode==9){j.preventDefault();if(a.browser.msie){var h=document.selection.createRange();h.text="\t"+h.text;this.focus()}else{var i=this.value.substr(0,this.selectionStart),k=this.value.substr(this.selectionEnd);this.value=i+"\t"+k;this.setSelectionRange(i.length+1,i.length+1)}}});a("<div/>").append(e).dialog({dialogClass:"el-finder-dialog",title:b.fm.i18n(b.name),modal:true,width:500,close:function(){b.fm.lockShortcuts()},buttons:{Cancel:function(){a(this).dialog("close")},Ok:function(){var h=e.val();a(this).dialog("close");b.fm.ajax({cmd:"edit",current:b.fm.cwd.hash,target:d.hash,content:h},function(i){if(i.target){b.fm.cdc[i.target.hash]=i.target;b.fm.view.updateFile(i.target);b.fm.selectById(i.target.hash)}},{type:"POST"})}}})})};this.isAllowed=function(){if(b.fm.selected.length==1){var d=this.fm.getSelected()[0];return d.write&&d.read&&(d.mime.indexOf("text")==0||d.mime=="application/x-empty"||d.mime=="application/xml")}};this.cm=function(d){return d=="file"}},archive:function(c){var b=this;this.name="Create archive";this.fm=c;this.exec=function(d){var e={cmd:"archive",current:b.fm.cwd.hash,type:a.inArray(d,this.fm.params.archives)!=-1?d:this.fm.params.archives[0],name:b.fm.i18n("Archive")};if(this.fm.jquery>132){e.targets=b.fm.selected}else{e["targets[]"]=b.fm.selected}this.fm.ajax(e,function(g){b.fm.reload(g)})};this.isAllowed=function(){if(this.fm.cwd.write&&this.fm.selected.length){var e=this.fm.getSelected(),d=e.length;while(d--){if(e[d].read){return true}}}return false};this.cm=function(d){return d!="cwd"};this.argc=function(){var e,d=[];for(e=0;e<b.fm.params.archives.length;e++){d.push({"class":"archive",argc:b.fm.params.archives[e],text:b.fm.view.mime2kind(b.fm.params.archives[e])})}return d}},extract:function(c){var b=this;this.name="Uncompress archive";this.fm=c;this.exec=function(){this.fm.ajax({cmd:"extract",current:this.fm.cwd.hash,target:this.fm.getSelected(0).hash},function(d){b.fm.reload(d)})};this.isAllowed=function(){var e=this.fm.params.extract,d=e&&e.length;return this.fm.cwd.write&&this.fm.selected.length==1&&this.fm.getSelected(0).read&&d&&a.inArray(this.fm.getSelected(0).mime,e)!=-1};this.cm=function(d){return d=="file"}},resize:function(c){var b=this;this.name="Resize image";this.fm=c;this.exec=function(){var l=this.fm.getSelected();if(l[0]&&l[0].write&&l[0].dim){var j=l[0].dim.split("x"),g=parseInt(j[0]),k=parseInt(j[1]),e=g/k;iw=a('<input type="text" size="9" value="'+g+'" name="width"/>'),ih=a('<input type="text" size="9" value="'+k+'" name="height"/>'),f=a("<form/>").append(iw).append(" x ").append(ih).append(" px");iw.add(ih).bind("change",i);b.fm.lockShortcuts(true);var m=a("<div/>").append(a("<div/>").text(b.fm.i18n("Dimensions")+":")).append(f).dialog({title:b.fm.i18n("Resize image"),dialogClass:"el-finder-dialog",width:230,modal:true,close:function(){b.fm.lockShortcuts()},buttons:{Cancel:function(){a(this).dialog("close")},Ok:function(){var d=parseInt(iw.val())||0,h=parseInt(ih.val())||0;if(d>0&&d!=g&&h>0&&h!=k){b.fm.ajax({cmd:"resize",current:b.fm.cwd.hash,target:l[0].hash,width:d,height:h},function(n){b.fm.reload(n)})}a(this).dialog("close")}}})}function i(){var d=parseInt(iw.val())||0,h=parseInt(ih.val())||0;if(d<=0||h<=0){d=g;h=k}else{if(this==iw.get(0)){h=parseInt(d/e)}else{d=parseInt(h*e)}}iw.val(d);ih.val(h)}};this.isAllowed=function(){return this.fm.selected.length==1&&this.fm.cdc[this.fm.selected[0]].write&&this.fm.cdc[this.fm.selected[0]].read&&this.fm.cdc[this.fm.selected[0]].resize};this.cm=function(d){return d=="file"}},icons:function(b){this.name="View as icons";this.fm=b;this.exec=function(){this.fm.view.win.addClass("el-finder-disabled");this.fm.setView("icons");this.fm.updateCwd();this.fm.view.win.removeClass("el-finder-disabled");a("div.image",this.fm.view.cwd).length&&this.fm.tmb()};this.isAllowed=function(){return this.fm.options.view!="icons"};this.cm=function(c){return c=="cwd"}},list:function(b){this.name="View as list";this.fm=b;this.exec=function(){this.fm.view.win.addClass("el-finder-disabled");this.fm.setView("list");this.fm.updateCwd();this.fm.view.win.removeClass("el-finder-disabled")};this.isAllowed=function(){return this.fm.options.view!="list"};this.cm=function(c){return c=="cwd"}},help:function(b){this.name="Help";this.fm=b;this.exec=function(){var j,e=this.fm.i18n("helpText"),c,i,g;j='<div class="el-finder-logo"/><strong>'+this.fm.i18n("elFinder: Web file manager")+"</strong><br/>"+this.fm.i18n("Version")+": "+this.fm.version+"<br/>jQuery/jQueryUI: "+a().jquery+"/"+a.ui.version+'<br clear="all"/><p><strong><a href="http://elrte.org/'+this.fm.options.lang+'/elfinder" target="_blank">'+this.fm.i18n("Donate to support project development")+'</a></strong></p><p><a href="http://elrte.org/redmine/projects/elfinder/wiki" target="_blank">'+this.fm.i18n("elFinder documentation")+"</a></p>";j+="<p>"+(e!="helpText"?e:"elFinder works similar to file manager on your computer. <br /> To make actions on files/folders use icons on top panel. If icon action it is not clear for you, hold mouse cursor over it to see the hint. <br /> Manipulations with existing files/folders can be done through the context menu (mouse right-click).<br/> To copy/delete a group of files/folders, select them using Shift/Alt(Command) + mouse left-click.")+"</p>";j+="<p><strong>"+this.fm.i18n("elFinder support following shortcuts")+":</strong><ul><li><kbd>Ctrl+A</kbd> - "+this.fm.i18n("Select all files")+"</li><li><kbd>Ctrl+C/Ctrl+X/Ctrl+V</kbd> - "+this.fm.i18n("Copy/Cut/Paste files")+"</li><li><kbd>Enter</kbd> - "+this.fm.i18n("Open selected file/folder")+"</li><li><kbd>Space</kbd> - "+this.fm.i18n("Open/close QuickLook window")+"</li><li><kbd>Delete/Cmd+Backspace</kbd> - "+this.fm.i18n("Remove selected files")+"</li><li><kbd>Ctrl+I</kbd> - "+this.fm.i18n("Selected files or current directory info")+"</li><li><kbd>Ctrl+N</kbd> - "+this.fm.i18n("Create new directory")+"</li><li><kbd>Ctrl+U</kbd> - "+this.fm.i18n("Open upload files form")+"</li><li><kbd>Left arrow</kbd> - "+this.fm.i18n("Select previous file")+"</li><li><kbd>Right arrow </kbd> - "+this.fm.i18n("Select next file")+"</li><li><kbd>Ctrl+Right arrow</kbd> - "+this.fm.i18n("Open selected file/folder")+"</li><li><kbd>Ctrl+Left arrow</kbd> - "+this.fm.i18n("Return into previous folder")+"</li><li><kbd>Shift+arrows</kbd> - "+this.fm.i18n("Increase/decrease files selection")+"</li></ul></p><p>"+this.fm.i18n("Contacts us if you need help integrating elFinder in you products")+": dev@std42.ru</p>";c='<div class="el-finder-help-std"/><p>'+this.fm.i18n("Javascripts/PHP programming: Dmitry (dio) Levashov, dio@std42.ru")+"</p><p>"+this.fm.i18n("Python programming, techsupport: Troex Nevelin, troex@fury.scancode.ru")+"</p><p>"+this.fm.i18n("Design: Valentin Razumnih")+"</p><p>"+this.fm.i18n("Chezh localization")+": Roman Matěna, info@romanmatena.cz</p><p>"+this.fm.i18n("Chinese (traditional) localization")+": Tad, tad0616@gmail.com</p><p>"+this.fm.i18n("Dutch localization")+': Kurt Aerts, <a href="http://ilabsolutions.net/" target="_blank">http://ilabsolutions.net</a></p><p>'+this.fm.i18n("Greek localization")+": Panagiotis Skarvelis</p><p>"+this.fm.i18n("Hungarian localization")+": Viktor Tamas, tamas.viktor@totalstudio.hu</p><p>"+this.fm.i18n("Italian localization")+":  Ugo Punzolo, sadraczerouno@gmail.com</p><p>"+this.fm.i18n("Latvian localization")+":  Uldis Plotiņš, uldis.plotins@gmail.com</p><p>"+this.fm.i18n("Poland localization")+":  Darek Wapiński, darek@wapinski.us</p><p>"+this.fm.i18n("Spanish localization")+': Alex (xand) Vavilin, xand@xand.es, <a href="http://xand.es" target="_blank">http://xand.es</a></p><p>'+this.fm.i18n("Icons")+': <a href="http://pixelmixer.ru/" target="_blank">pixelmixer</a>,  <a href="http://www.famfamfam.com/lab/icons/silk/" target="_blank">Famfam silk icons</a>, <a href="http://www.fatcow.com/free-icons/" target="_blank">Fatcow icons</a></p><p>'+this.fm.i18n('Copyright: <a href="http://www.std42.ru" target="_blank">Studio 42 LTD</a>')+"</p><p>"+this.fm.i18n("License: BSD License")+"</p><p>"+this.fm.i18n('Web site: <a href="http://elrte.org/elfinder/" target="_blank">elrte.org/elfinder</a>')+"</p>";i='<div class="el-finder-logo"/><strong><a href="http://www.eldorado-cms.ru" target="_blank">ELDORADO.CMS</a></strong><br/>'+this.fm.i18n("Simple and usefull Content Management System")+"<hr/>"+this.fm.i18n("Support project development and we will place here info about you");g='<ul><li><a href="#el-finder-help-h">'+this.fm.i18n("Help")+'</a></li><li><a href="#el-finder-help-a">'+this.fm.i18n("Authors")+'</a><li><a href="#el-finder-help-sp">'+this.fm.i18n("Sponsors")+'</a></li></ul><div id="el-finder-help-h"><p>'+j+'</p></div><div id="el-finder-help-a"><p>'+c+'</p></div><div id="el-finder-help-sp"><p>'+i+"</p></div>";var k=a("<div/>").html(g).dialog({width:617,title:this.fm.i18n("Help"),dialogClass:"el-finder-dialog",modal:true,close:function(){k.tabs("destroy").dialog("destroy").remove()},buttons:{Ok:function(){a(this).dialog("close")}}}).tabs()};this.cm=function(c){return c=="cwd"}}}})(jQuery);(function(a){elFinder.prototype.quickLook=function(l,b){var p=this;this.fm=l;this._hash="";this.title=a("<strong/>");this.ico=a("<p/>");this.info=a("<label/>");this.media=a('<div class="el-finder-ql-media"/>').hide();this.name=a('<span class="el-finder-ql-name"/>');this.kind=a('<span class="el-finder-ql-kind"/>');this.size=a('<span class="el-finder-ql-size"/>');this.date=a('<span class="el-finder-ql-date"/>');this.url=a('<a href="#"/>').hide().click(function(i){i.preventDefault();window.open(a(this).attr("href"));p.hide()});this.add=a("<div/>");this.content=a('<div class="el-finder-ql-content"/>');this.win=a('<div class="el-finder-ql"/>').hide().append(a('<div class="el-finder-ql-drag-handle"/>').append(a('<span class="ui-icon ui-icon-circle-close"/>').click(function(){p.hide()})).append(this.title)).append(this.ico).append(this.media).append(this.content.append(this.name).append(this.kind).append(this.size).append(this.date).append(this.url).append(this.add)).appendTo("body").draggable({handle:".el-finder-ql-drag-handle"}).resizable({minWidth:420,minHeight:150,resize:function(){if(p.media.children().length){var m=p.media.children(":first");switch(m[0].nodeName){case"IMG":var e=m.width(),n=m.height(),i=p.win.width(),s=p.win.css("height")=="auto"?350:p.win.height()-p.content.height()-p.th,q=e>i||n>s?Math.min(Math.min(i,e)/e,Math.min(s,n)/n):Math.min(Math.max(i,e)/e,Math.max(s,n)/n);m.css({width:Math.round(m.width()*q),height:Math.round(m.height()*q)});break;case"IFRAME":case"EMBED":m.css("height",p.win.height()-p.content.height()-p.th);break;case"OBJECT":m.children("embed").css("height",p.win.height()-p.content.height()-p.th)}}}});this.th=parseInt(this.win.children(":first").css("height"))||18;this.mimes={"image/jpeg":"jpg","image/gif":"gif","image/png":"png"};for(var h=0;h<navigator.mimeTypes.length;h++){var o=navigator.mimeTypes[h].type;if(o&&o!="*"){this.mimes[o]=navigator.mimeTypes[h].suffixes}}if((a.browser.safari&&navigator.platform.indexOf("Mac")!=-1)||a.browser.msie){this.mimes["application/pdf"]="pdf"}else{for(var c=0;c<navigator.plugins.length;c++){for(var d=0;d<navigator.plugins[c].length;d++){var k=navigator.plugins[c][d].description.toLowerCase();if(k.substring(0,k.indexOf(" "))=="pdf"){this.mimes["application/pdf"]="pdf";break}}}}if(this.mimes["image/x-bmp"]){this.mimes["image/x-ms-bmp"]="bmp"}if(a.browser.msie&&!this.mimes["application/x-shockwave-flash"]){this.mimes["application/x-shockwave-flash"]="swf"}this.show=function(){if(this.win.is(":hidden")&&p.fm.selected.length==1){g();var m=p.fm.selected[0],e=p.fm.view.cwd.find('[key="'+m+'"]'),i=e.offset();p.fm.lockShortcuts(true);this.win.css({width:e.width()-20,height:e.height(),left:i.left,top:i.top,opacity:0}).show().animate({width:420,height:150,opacity:1,top:Math.round(a(window).height()/5),left:a(window).width()/2-210},450,function(){p.win.css({height:"auto"});p.fm.lockShortcuts()})}};this.hide=function(){if(this.win.is(":visible")){var i,e=p.fm.view.cwd.find('[key="'+this._hash+'"]');if(e){i=e.offset();this.media.hide(200);this.win.animate({width:e.width()-20,height:e.height(),left:i.left,top:i.top,opacity:0},350,function(){p.fm.lockShortcuts();j();p.win.hide().css("height","auto")})}else{this.win.fadeOut(200);j();p.fm.lockShortcuts()}}};this.toggle=function(){if(this.win.is(":visible")){this.hide()}else{this.show()}};this.update=function(){if(this.fm.selected.length!=1){this.hide()}else{if(this.win.is(":visible")&&this.fm.selected[0]!=this._hash){g()}}};this.mediaHeight=function(){return this.win.is(":animated")||this.win.css("height")=="auto"?315:this.win.height()-this.content.height()-this.th};function j(){p.media.hide().empty();p.win.attr("class","el-finder-ql").css("z-index",p.fm.zIndex);p.title.empty();p.ico.attr("style","").show();p.add.hide().empty();p._hash=""}function g(){var m=p.fm.getSelected(0);j();p._hash=m.hash;p.title.text(m.name);p.win.addClass(p.fm.view.mime2class(m.mime));p.name.text(m.name);p.kind.text(p.fm.view.mime2kind(m.link?"symlink":m.mime));p.size.text(p.fm.view.formatSize(m.size));p.date.text(p.fm.i18n("Modified")+": "+p.fm.view.formatDate(m.date));m.dim&&p.add.append("<span>"+m.dim+" px</span>").show();m.tmb&&p.ico.css("background",'url("'+m.tmb+'") 0 0 no-repeat');if(m.url){p.url.text(m.url).attr("href",m.url).show();for(var e in p.plugins){if(p.plugins[e].test&&p.plugins[e].test(m.mime,p.mimes,m.name)){p.plugins[e].show(p,m);return}}}else{p.url.hide()}p.win.css({width:"420px",height:"auto"})}};elFinder.prototype.quickLook.prototype.plugins={image:new function(){this.test=function(c,b){return c.match(/^image\//)};this.show=function(e,d){var b,c;if(e.mimes[d.mime]&&d.hash==e._hash){a("<img/>").hide().appendTo(e.media.show()).attr("src",d.url+(a.browser.msie||a.browser.opera?"?"+Math.random():"")).load(function(){c=a(this).unbind("load");if(d.hash==e._hash){e.win.is(":animated")?setTimeout(function(){g(c)},330):g(c)}})}function g(k){var j=k.width(),m=k.height(),i=e.win.is(":animated"),l=i?420:e.win.width(),o=i||e.win.css("height")=="auto"?315:e.win.height()-e.content.height()-e.th,n=j>l||m>o?Math.min(Math.min(l,j)/j,Math.min(o,m)/m):Math.min(Math.max(l,j)/j,Math.max(o,m)/m);e.fm.lockShortcuts(true);e.ico.hide();k.css({width:e.ico.width(),height:e.ico.height()}).show().animate({width:Math.round(n*j),height:Math.round(n*m)},450,function(){e.fm.lockShortcuts()})}}},text:new function(){this.test=function(c,b){return(c.indexOf("text")==0&&c.indexOf("rtf")==-1)||c.match(/application\/(xml|javascript|json)/)};this.show=function(c,b){if(b.hash==c._hash){c.ico.hide();c.media.append('<iframe src="'+b.url+'" style="height:'+c.mediaHeight()+'px" />').show()}}},swf:new function(){this.test=function(c,b){return c=="application/x-shockwave-flash"&&b[c]};this.show=function(c,b){if(b.hash==c._hash){c.ico.hide();var d=c.media.append('<embed pluginspage="http://www.macromedia.com/go/getflashplayer" quality="high" src="'+b.url+'" style="width:100%;height:'+c.mediaHeight()+'px" type="application/x-shockwave-flash" />');if(c.win.is(":animated")){d.slideDown(450)}else{d.show()}}}},audio:new function(){this.test=function(c,b){return c.indexOf("audio")==0&&b[c]};this.show=function(d,c){if(c.hash==d._hash){d.ico.hide();var b=d.win.is(":animated")||d.win.css("height")=="auto"?100:d.win.height()-d.content.height()-d.th;d.media.append('<embed src="'+c.url+'" style="width:100%;height:'+b+'px" />').show()}}},video:new function(){this.test=function(c,b){return c.indexOf("video")==0&&b[c]};this.show=function(c,b){if(b.hash==c._hash){c.ico.hide();c.media.append('<embed src="'+b.url+'" style="width:100%;height:'+c.mediaHeight()+'px" />').show()}}},pdf:new function(){this.test=function(c,b){return c=="application/pdf"&&b[c]};this.show=function(c,b){if(b.hash==c._hash){c.ico.hide();c.media.append('<embed src="'+b.url+'" style="width:100%;height:'+c.mediaHeight()+'px" />').show()}}}}})(jQuery);(function(a){elFinder.prototype.eventsManager=function(d,c){var b=this;this.lock=false;this.fm=d;this.ui=d.ui;this.tree=d.view.tree;this.cwd=d.view.cwd;this.pointer="";this.init=function(){var g=this,h=false;g.lock=false;this.cwd.bind("click",function(j){var i=a(j.target);if(i.hasClass("ui-selected")){g.fm.unselectAll()}else{if(!i.attr("key")){i=i.parent("[key]")}if(j.ctrlKey||j.metaKey){g.fm.toggleSelect(i)}else{g.fm.select(i,true)}}}).bind(window.opera?"click":"contextmenu",function(j){if(window.opera&&!j.ctrlKey){return}j.preventDefault();j.stopPropagation();var i=a(j.target);if(a.browser.mozilla){h=true}if(i.hasClass("el-finder-cwd")){g.fm.unselectAll()}else{g.fm.select(i.attr("key")?i:i.parent("[key]"))}g.fm.quickLook.hide();g.fm.ui.showMenu(j)}).selectable({filter:"[key]",delay:300,stop:function(){g.fm.updateSelect();g.fm.log("mouseup")}});a(document).bind("click",function(i){!h&&g.fm.ui.hideMenu();h=false;a("input",g.cwd).trigger("change");if(!a(i.target).is("input,textarea,select")){a("input,textarea").blur()}});a("input,textarea").live("focus",function(i){g.lock=true}).live("blur",function(i){g.lock=false});this.tree.bind("select",function(i){g.tree.find("a").removeClass("selected");a(i.target).addClass("selected").parents("li:has(ul)").children("ul").show().prev().children("div").addClass("expanded")});if(this.fm.options.places){this.fm.view.plc.click(function(l){l.preventDefault();var j=a(l.target),k=j.attr("key"),i;if(k){k!=g.fm.cwd.hash&&g.ui.exec("open",l.target)}else{if(l.target.nodeName=="A"||l.target.nodeName=="DIV"){i=g.fm.view.plc.find("ul");if(i.children().length){i.toggle(300);g.fm.view.plc.children("li").find("div").toggleClass("expanded")}}}});this.fm.view.plc.droppable({accept:"(div,tr).directory",tolerance:"pointer",over:function(){a(this).addClass("el-finder-droppable")},out:function(){a(this).removeClass("el-finder-droppable")},drop:function(k,j){a(this).removeClass("el-finder-droppable");var i=false;j.helper.children(".directory:not(.noaccess,.dropbox)").each(function(){if(g.fm.addPlace(a(this).attr("key"))){i=true;a(this).hide()}});if(i){g.fm.view.renderPlaces();g.updatePlaces();g.fm.view.plc.children("li").children("div").trigger("click")}if(!j.helper.children("div:visible").length){j.helper.hide()}}})}a(document).bind(a.browser.mozilla||a.browser.opera?"keypress":"keydown",function(j){var i=j.ctrlKey||j.metaKey;if(g.lock){return}switch(j.keyCode){case 37:case 38:j.stopPropagation();j.preventDefault();if(j.keyCode==37&&i){g.ui.execIfAllowed("back")}else{e(false,!j.shiftKey)}break;case 39:case 40:j.stopPropagation();j.preventDefault();if(i){g.ui.execIfAllowed("open")}else{e(true,!j.shiftKey)}break}});a(document).bind(a.browser.opera?"keypress":"keydown",function(i){if(g.lock){return}switch(i.keyCode){case 32:i.preventDefault();i.stopPropagation();g.fm.quickLook.toggle();break;case 27:g.fm.quickLook.hide();break}});if(!this.fm.options.disableShortcuts){a(document).bind("keydown",function(j){var i=j.ctrlKey||j.metaKey;if(g.lock){return}switch(j.keyCode){case 8:if(i&&g.ui.isCmdAllowed("rm")){j.preventDefault();g.ui.exec("rm")}break;case 13:if(g.ui.isCmdAllowed("select")){return g.ui.exec("select")}g.ui.execIfAllowed("open");break;case 46:g.ui.execIfAllowed("rm");break;case 65:if(i){j.preventDefault();g.fm.selectAll()}break;case 67:i&&g.ui.execIfAllowed("copy");break;case 73:if(i){j.preventDefault();g.ui.exec("info")}break;case 78:if(i){j.preventDefault();g.ui.execIfAllowed("mkdir")}break;case 85:if(i){j.preventDefault();g.ui.execIfAllowed("upload")}break;case 86:i&&g.ui.execIfAllowed("paste");break;case 88:i&&g.ui.execIfAllowed("cut");break;case 113:g.ui.execIfAllowed("rename");break}})}};this.updateNav=function(){a("a",this.tree).click(function(h){h.preventDefault();var g=a(this),i;if(h.target.nodeName=="DIV"&&a(h.target).hasClass("collapsed")){a(h.target).toggleClass("expanded").parent().next("ul").toggle(300)}else{if(g.attr("key")!=b.fm.cwd.hash){if(g.hasClass("noaccess")||g.hasClass("dropbox")){b.fm.view.error("Access denied")}else{b.ui.exec("open",g.trigger("select")[0])}}else{i=g.children(".collapsed");if(i.length){i.toggleClass("expanded");g.next("ul").toggle(300)}}}});a("a:not(.noaccess,.readonly)",this.tree).droppable({tolerance:"pointer",accept:"div[key],tr[key]",over:function(){a(this).addClass("el-finder-droppable")},out:function(){a(this).removeClass("el-finder-droppable")},drop:function(h,g){a(this).removeClass("el-finder-droppable");b.fm.drop(h,g,a(this).attr("key"))}});this.fm.options.places&&this.updatePlaces()};this.updatePlaces=function(){this.fm.view.plc.children("li").find("li").draggable({scroll:false,stop:function(){if(b.fm.removePlace(a(this).children("a").attr("key"))){a(this).remove();if(!a("li",b.fm.view.plc.children("li")).length){b.fm.view.plc.children("li").find("div").removeClass("collapsed expanded").end().children("ul").hide()}}}})};this.updateCwd=function(){a("[key]",this.cwd).bind("dblclick",function(g){b.fm.select(a(this),true);b.ui.exec(b.ui.isCmdAllowed("select")?"select":"open")}).draggable({delay:3,addClasses:false,appendTo:".el-finder-cwd",revert:true,drag:function(h,g){g.helper.toggleClass("el-finder-drag-copy",h.shiftKey||h.ctrlKey)},helper:function(){var g=a(this),i=a('<div class="el-finder-drag-helper"/>'),j=0;!g.hasClass("ui-selected")&&b.fm.select(g,true);b.cwd.find(".ui-selected").each(function(h){var k=b.fm.options.view=="icons"?a(this).clone().removeClass("ui-selected"):a(b.fm.view.renderIcon(b.fm.cdc[a(this).attr("key")]));if(j++==0||j%12==0){k.css("margin-left",0)}i.append(k)});return i.css("width",(j<=12?85+(j-1)*29:387)+"px")}}).filter(".directory").droppable({tolerance:"pointer",accept:"div[key],tr[key]",over:function(){a(this).addClass("el-finder-droppable")},out:function(){a(this).removeClass("el-finder-droppable")},drop:function(h,g){a(this).removeClass("el-finder-droppable");b.fm.drop(h,g,a(this).attr("key"))}});if(a.browser.msie){a("*",this.cwd).attr("unselectable","on").filter("[key]").bind("dragstart",function(){b.cwd.selectable("disable").removeClass("ui-state-disabled ui-selectable-disabled")}).bind("dragstop",function(){b.cwd.selectable("enable")})}};function e(h,i){var j,g,k;if(!a("[key]",b.cwd).length){return}if(b.fm.selected.length==0){j=a("[key]:"+(h?"first":"last"),b.cwd);b.fm.select(j)}else{if(i){j=a(".ui-selected:"+(h?"last":"first"),b.cwd);g=j[h?"next":"prev"]("[key]");if(g.length){j=g}b.fm.select(j,true)}else{if(b.pointer){k=a('[key="'+b.pointer+'"].ui-selected',b.cwd)}if(!k||!k.length){k=a(".ui-selected:"+(h?"last":"first"),b.cwd)}j=k[h?"next":"prev"]("[key]");if(!j.length){j=k}else{if(!j.hasClass("ui-selected")){b.fm.select(j)}else{if(!k.hasClass("ui-selected")){b.fm.unselect(j)}else{g=k[h?"prev":"next"]("[key]");if(!g.length||!g.hasClass("ui-selected")){b.fm.unselect(k)}else{while((g=h?j.next("[key]"):j.prev("[key]"))&&j.hasClass("ui-selected")){j=g}b.fm.select(j)}}}}}}b.pointer=j.attr("key");b.fm.checkSelectedPos(h)}}})(jQuery);


 /*
 * mws.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
* MWS Admin v1.5 - Core JS
* This file is part of MWS Admin, an Admin template build for sale at ThemeForest.
* All copyright to this file is hold by Mairel Theafila <maimairel@yahoo.com> a.k.a nagaemas on ThemeForest.
* Last Updated:
* April 29, 2012
*
*/

(function ($) {
    $(document).ready(function () {

     

        $(".mws-panel.mws-collapsible .mws-panel-header")
			.live("click", function (event) {
			    var page = $(this).find('a').attr('tag');
			   
			    if (page == undefined) {
			       
			        $(this)
					.parents(".mws-panel")
					.toggleClass("mws-collapsed")
					.find(".mws-panel-body")
					.slideToggle("fast");

			    }
			});



     

        $(".mwschild-panel.mws-collapsible .mwschild-panel-header")
			.live("click", function (event) {
			    var page = $(this).find('a').attr('tag');

			    if (page == undefined) {

			        $(this)
					.parents(".mwschild-panel")
					.toggleClass("mws-collapsed")
					.find(".mwschild-panel-body")
					.slideToggle("fast");
			    }
			});


        $(".formtabheader")
			.live("click", function (event) {
			    if ($(this).hasClass('formminus')) {
			        $(this).removeClass('formminus').addClass('formplus');
			    } else {
			        $(this).removeClass('formplus').addClass('formminus');
			    }

			    $(this)
					.next("table")
					.toggleClass("mws-collapsed")
					.slideToggle("fast");
			});

          $(".formtabSubheader")
			.live("click", function (event) {
			    if ($(this).hasClass('formminus')) {
			        $(this).removeClass('formminus').addClass('formplus');
			    } else {
			        $(this).removeClass('formplus').addClass('formminus');
			    }

			    $(this)
					.next("table")
					.toggleClass("mws-collapsed")
					.slideToggle("fast");
			});



        $("div#mws-navigation ul li a, div#mws-navigation ul li span")
			.bind('click', function (event) {
			    if ($(this).next('ul').size() !== 0) {
			        $(this).next('ul').slideToggle('fast', function () {
                 
			            $(this).toggleClass('closed');
                        if($('#ucMenu_divSideMenuCtrl') != undefined){
                             if (parseInt($('#ucMenu_divSideMenuCtrl').height()) > 600) {
			                        $('#mws-leftsidebar').height($('#ucMenu_divSideMenuCtrl').height() + 200);
			                }
                        }
			        });
			        event.preventDefault();
			    }
			});

        /* Responsive Layout Script */

        $("div#mws-navigation").live('click', function (event) {
            if (event.target === this) {
                $(this).toggleClass('toggled');
            }
        });

        /* Form Messages */

        $(".mws-form-message").live("click", function () {
            $(this).animate({ opacity: 0 }, function () {
                $(this).slideUp("medium", function () {
                    $(this).css("opacity", '');
                });
            });
        });

        /* Message & Notifications Dropdown */
        $("div#mws-user-tools .mws-dropdown-menu a").click(function (event) {
            $(".mws-dropdown-menu.toggled").not($(this).parent()).removeClass("toggled");
            $(this).parent().toggleClass("toggled");
        });

        $('html').click(function (event) {
            if ($(event.target).parents('.mws-dropdown-menu').size() == 0) {
                $(".mws-dropdown-menu").removeClass("toggled");
            }
        });

        
        $(".mws-nav-tooltip").addClass("mws-inset");

   
    });
})(jQuery);



 /*
 *mws.wizard.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 * This file is part of MWS Admin, an Admin template build for sale at ThemeForest.
 * All copyright to this file is hold by Mairel Theafila <maimairel@yahoo.com> a.k.a nagaemas on ThemeForest.
 *
 * Last Updated:
 * May 20, 2012
 *
 * 'Highly configurable' mutable plugin boilerplate
 * Author: @markdalgleish
 * Further changes, comments: @addyosmani
 * Licensed under the MIT license
 *
 */

;(function( $, window, document, undefined ) {
	// our plugin constructor
	var mwsWizard = function( elem, options ) {
		this.$elem = $(elem);
		this.options = options;
    };
	
	// the plugin prototype
	mwsWizard.prototype = {
		defaults: {
			element: 'fieldset', 
			navigationContainer: '.mws-wizard-nav', 
			buttonContainerClass: 'mws-button-row', 
			nextButtonClass: 'mws-button red', 
			prevButtonClass: 'mws-button gray left', 
			submitButtonClass: 'mws-button green', 
			nextButtonLabel: 'Next', 
			prevButtonLabel: 'Prev', 
			submitButtonLabel: 'Submit', 
			forwardOnly: true, 
			onLeaveStep: null, 
			onShowStep: null, 
			onBeforeSubmit: null
		}, 
		
		init: function() {				
			// Introduce defaults that can be extended either
			// globally or using an object literal.
			this.config = $.extend({}, this.defaults, this.options);
			
			this.steps = $(this.config.element, this.$elem);
			
			this.$elem.addClass('mws-wizard-form');
			
			this.data = $.extend({}, this._parseNavigation(), this._buildButtons());
			
			this.initSteps();
			
			return this;
		}, 
		
		// Public methods
		
		initSteps: function() {
			this.steps.hide().first().show();
			this.data.activeStep = 0;
			this.data.stepsMap[this.data.activeStep].anchor.parent().addClass('active');
			this._processSteps();
		}, 
		
		goTo: function(stepNumber) {
			var self = this;
			
			if(stepNumber !== self.data.activeStep) {
				if(this.config.forwardOnly && stepNumber < self.data.activeStep)
					return;
				
				if(self.config.onLeaveStep && $.isFunction(self.config.onLeaveStep)) {
					if(false === self.config.onLeaveStep.apply(self, [self.data.activeStep, self.steps.eq(self.data.activeStep)]))
						return;
				}
				
				self.steps.filter(self.data.stepsMap[self.data.activeStep].id).data('done', true).fadeOut('fast', function() {
					self.steps.filter(self.data.stepsMap[stepNumber].id).fadeIn('fast');
					
					self.data.stepsMap[self.data.activeStep].anchor.parent().removeClass('current').addClass('done');
					self.data.stepsMap[stepNumber].anchor.parent().addClass('current');
							
					self.data.activeStep = stepNumber;
					self._processSteps();
					
					if(self.config.onShowStep && $.isFunction(self.config.onShowStep)) {
						self.config.onShowStep.apply(self, [stepNumber, self.steps.eq(stepNumber)]);
					}
				});
			}
		}, 
		
		goBackward: function() {
			if(this.data.activeStep > 0) {
				this.goTo(this.data.activeStep - 1);
			}
		}, 
		
		goForward: function() {
			if(this.data.activeStep < this.steps.size() - 1) {
				this.goTo(this.data.activeStep + 1);
			}
		}, 
		
		submitForm: function() {
			var shouldSubmit = true;
			if(this.config.onBeforeSubmit && $.isFunction(this.config.onBeforeSubmit)) {
				shouldSubmit = this.config.onBeforeSubmit.apply(self, []);
			}
			
			if(false !== shouldSubmit) this.$elem.submit();
		}, 
		
		// Private methods
		
		_navigate: function(stepNumber) {
			if(this._isStepDone(stepNumber))
				this.goTo(stepNumber);
		}, 
		
		_isStepDone: function(stepNumber) {
			return (typeof(this.steps.filter(this.data.stepsMap[stepNumber].id).data('done')) !== 'undefined');
		}, 
		
		_parseNavigation: function() {
			var self = this, 
				navContainer = this.$elem.find(this.config.navigationContainer), 
				stepsMap = navContainer.find('ul li a').map(function(k, v) {
					$(v).bind('click', function(event) {
						self._navigate(k);
						event.preventDefault();
					});
					
					return { id: $(v).attr('href'), anchor: $(v) };
				}).get();
				
			return { activeStep: -1, stepsMap: stepsMap };
		}, 
		
		_buildButtons: function() {
			var btnContainer = $(this.config.buttonContainer, this.$elem), 
				self = this, 
				$button = $('<input />').attr('type', 'button'), 
				$prevButton = $button.clone().val(this.config.prevButtonLabel).addClass(this.config.prevButtonClass).bind('click', function(event) {
					if(!self.config.forwardOnly)
						self.goBackward();
					event.preventDefault();
				}), 
				$nextButton = $button.clone().val(this.config.nextButtonLabel).addClass(this.config.nextButtonClass).bind('click', function(event) {
					self.goForward();
					event.preventDefault();
				}), 
				$submitButton = $button.clone().val(this.config.submitButtonLabel).addClass(this.config.submitButtonClass).bind('click', function(event) {
					self.submitForm();
					event.preventDefault();
				});
			
			if(!btnContainer.get(0)) {
				btnContainer = $('<div></div>').addClass(this.config.buttonContainerClass).appendTo(this.$elem);
			}
			
			if(!self.config.forwardOnly)
				btnContainer.append($prevButton);
			btnContainer.append($nextButton).append($submitButton);
			
			return { nextButton: $nextButton, prevButton: $prevButton, submitButton: $submitButton };
		}, 
		
		_processSteps: function() {
			this.data.prevButton.toggle((this.data.activeStep > 0) && !this.data.forwardOnly);
			this.data.nextButton.toggle((this.data.activeStep < this.steps.size() - 1));
			this.data.submitButton.toggle((this.data.activeStep >= this.steps.size() - 1));
		}
	}
	
	mwsWizard.defaults = mwsWizard.prototype.defaults;
	
	$.fn.mwsWizard = function(options) {
		return this.each(function() {
			new mwsWizard(this, options).init();
		});
	};

})( jQuery, window , document );



 /*
 * themer.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 * MWS Admin v1.5 - Themer JS
 * This file is part of MWS Admin, an Admin template build for sale at ThemeForest.
 * All copyright to this file is hold by Mairel Theafila <maimairel@yahoo.com> a.k.a nagaemas on ThemeForest.
 * Last Updated:
 * April 29, 2012
 *
 */
 
(function ($) {
    $(document).ready(function () {
        var backgroundPattern = "images/core/bg/paper.png";
        var baseColor = "#35353a";
        var childbaseColor = "#35353a";
        var highlightColor = "#c5d52b";
        var textColor = "#c5d52b";
        var textGlowColor = { r: 197, g: 213, b: 42, a: 0.5 };

        var patterns = {
            Paper: {
                name: "Paper",
                img: "images/core/bg/paper.png"
            },
            Blueprint: {
                name: "Blueprint",
                img: "images/core/bg/blueprint.png"
            },
            Bricks: {
                name: "Bricks",
                img: "images/core/bg/bricks.png"
            },
            Carbon: {
                name: "Carbon",
                img: "images/core/bg/carbon.png"
            },
            Circuit: {
                name: "Circuit",
                img: "images/core/bg/circuit.png"
            },
            Holes: {
                name: "Holes",
                img: "images/core/bg/holes.png"
            },
            Mozaic: {
                name: "Mozaic",
                img: "images/core/bg/mozaic.png"
            },
            Roof: {
                name: "Roof",
                img: "images/core/bg/roof.png"
            },
            Stripes: {
                name: "Stripes",
                img: "images/core/bg/stripes.png"
            }
        };

        var presets = {
            Default: {
                name: "Default",
                baseColor: "35353a",
                childbaseColor: "e62f17",
                highlightColor: "c5d52b",
                textColor: "c5d52b",
                textGlowColor: { r: 197, g: 213, b: 42, a: 0.5 }
            },
            Army: {
                name: "Army",
                baseColor: "363d1b",
                childbaseColor: "333A19",
                highlightColor: "947131",
                textColor: "ffb575",
                textGlowColor: { r: 237, g: 255, b: 41, a: 0.4 }
            },
            RockyMountains: {
                name: "Rocky Mountains",
                baseColor: "2f2f33",
                childbaseColor: "2d2d31",
                highlightColor: "808080",
                textColor: "b0e6ff",
                textGlowColor: { r: 230, g: 232, b: 208, a: 0.4 }
            },
            ChineseTemple: {
                name: "Chinese Temple",
                baseColor: "4f1b1b",
                childbaseColor: "a732d6",
                highlightColor: "e8cb10",
                textColor: "f7ff00",
                textGlowColor: { r: 255, g: 255, b: 0, a: 0.6 }
            },
            Boutique: {
                name: "Boutique",
                baseColor: "292828",
                childbaseColor: "292828",
                highlightColor: "f08dcc",
                textColor: "fcaee3",
                textGlowColor: { r: 186, g: 9, b: 230, a: 0.5 }
            },
            Toxic: {
                name: "Toxic",
                baseColor: "42184a",
                childbaseColor: "42184a",
                highlightColor: "97c730",
                textColor: "b1ff4c",
                textGlowColor: { r: 230, g: 232, b: 208, a: 0.45 }
            },
            Aquamarine: {
                name: "Aquamarine",
                baseColor: "192a54",
                childbaseColor: "182952",
                highlightColor: "88a9eb",
                textColor: "8affe2",
                textGlowColor: { r: 157, g: 224, b: 245, a: 0.5 }
            }
        };

        var backgroundTargets =
		[
			"body",
			"div#mws-container",
            "div#mws-maincontainer",
            "div#usiltab",
            //".ui-tabs .ui-tabs-nav li.ui-tabs-selected",
            //".tabs-active > .tabs-content",

            ".side-tabs > .tabs > li.active > a"

            //".standard-tabs > .tabs > li.active > a"

		];

        var baseColorTargets =
		[
			"div#mws-sidebar-bg",
			"div#mws-header",
			".mws-panel .mws-panel-header",

			"div#mws-error-container",
			"div#mws-login",
			"div#mws-login .mws-login-lock",
			".ui-accordion .ui-accordion-header",
			".ui-tabs .ui-tabs-nav",
			".ui-datepicker",
			".fc-event-skin",
			".ui-dialog .ui-dialog-titlebar",
			"div.jGrowl div.jGrowl-notification, div.jGrowl div.jGrowl-closer",
			"div#mws-user-tools .mws-dropdown-menu .mws-dropdown-box",
			"div#mws-user-tools .mws-dropdown-menu.toggled a.mws-dropdown-trigger",
		];


        var childbaseColorTargets =
		[
         ".mwschild-panel .mwschild-panel-header"
        ];

        var borderColorTargets =
		[
			"div#mws-header",
            ".tabs-active > .tabs-content",
            ".side-tabs > .tabs-content",
            ".side-tabs > .tabs > li.active > a",
            ".standard-tabs > .tabs-content",
            ".standard-tabs > .tabs > li.active > a"
		];

        var highlightColorTargets =
		[
			"div#mws-searchbox input.mws-search-submit",
			".mws-panel .mws-panel-header .mws-collapse-button span",
            ".mwschild-panel .mwschild-panel-header .mws-collapse-button span",
			"div.dataTables_wrapper .dataTables_paginate .paginate_disabled_previous",
			"div.dataTables_wrapper .dataTables_paginate .paginate_enabled_previous",
			"div.dataTables_wrapper .dataTables_paginate .paginate_disabled_next",
			"div.dataTables_wrapper .dataTables_paginate .paginate_enabled_next",
			"div.dataTables_wrapper .dataTables_paginate .paginate_active",
			".mws-hover-table tbody tr.odd:hover td",
            ".mws-hover-table tbody tr.even:hover td",
            
//			".mws-table tbody tr.odd:hover td",
//			".mws-table tbody tr.even:hover td",
			".fc-state-highlight",
			".ui-slider-horizontal .ui-slider-range",
			".ui-slider-vertical .ui-slider-range",
			".ui-progressbar .ui-progressbar-value",
			".ui-datepicker td.ui-datepicker-current-day",
			".ui-datepicker .ui-datepicker-prev .ui-icon",
			".ui-datepicker .ui-datepicker-next .ui-icon",
			".ui-accordion-header .ui-icon",
			".ui-dialog-titlebar-close .ui-icon"
		];

        var textTargets =
		[
			".mws-panel .mws-panel-header span",
            ".mwschild-panel .mwschild-panel-header span",
			"div#mws-navigation ul li.active a",
			"div#mws-navigation ul li.active span",
			"div#mws-user-tools #mws-username",
			"div#mws-navigation ul li span.mws-nav-tooltip",
			"div#mws-user-tools #mws-user-info #mws-user-functions #mws-username",
			".ui-dialog .ui-dialog-title",
			".ui-state-default",
			".ui-state-active",
			".ui-state-hover",
			".ui-state-focus",
			".ui-state-default a",
			".ui-state-active a",
			".ui-state-hover a",
			".ui-state-focus a"
		];

        $("#mws-themer-getcss").bind("click", function (event) {
            $("#mws-themer-css-dialog textarea").val(generateCSS("../"));
            $("#mws-themer-css-dialog").dialog("open");
            event.preventDefault();
        });

        var presetDd = $('<select id="mws-theme-presets"></select>');
        for (var i in presets) {
            var option = $("<option></option>").text(presets[i].name).val(i);
            presetDd.append(option);
        }
        $("#mws-theme-presets-container").append(presetDd);

        presetDd.bind('change', function (event) {
            updateBaseColor(presets[presetDd.val()].baseColor);
            updatechildBaseColor(presets[presetDd.val()].childbaseColor);
            updateHighlightColor(presets[presetDd.val()].highlightColor);
            updateTextColor(presets[presetDd.val()].textColor);

            updateTextGlowColor(presets[presetDd.val()].textGlowColor, presets[presetDd.val()].textGlowColor.a);

            attachStylesheet();

            event.preventDefault();
        });


        var patternDd = $('<select id="mws-theme-patterns"></select>');
        for (var i in patterns) {
            var option = $("<option></option>").text(patterns[i].name).val(i);
            patternDd.append(option);
        }
        $("#mws-theme-pattern-container").append(patternDd);

        patternDd.bind('change', function (event) {
            updateBackground(patterns[patternDd.val()].img, true);

            event.preventDefault();
        });

        $("div#mws-themer #mws-themer-toggle").bind("click", function (event) {
            if ($(this).hasClass("opened")) {
                $(this).toggleClass("opened").parent().animate({ right: "0" }, "slow");
            } else {
                $(this).toggleClass("opened").parent().animate({ right: "256" }, "slow");
            }
        });

        $("div#mws-themer #mws-textglow-op").slider({
            range: "min",
            min: 0,
            max: 100,
            value: 50,
            slide: function (event, ui) {
                alpha = ui.value * 1.0 / 100.0;
                updateTextGlowColor(null, alpha);
            }
        });

        $("div#mws-themer #mws-themer-css-dialog").dialog({
            autoOpen: false,
            title: "Theme CSS",
            width: 500,
            modal: true,
            resize: false,
            buttons: {
                "Close": function () { $(this).dialog("close"); }
            }
        });

        $("#mws-base-cp").ColorPicker({
            color: baseColor,
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                updateBaseColor(hex, true);
            }
        });

        $("#mws-highlight-cp").ColorPicker({
            color: highlightColor,
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                updateHighlightColor(hex, true);
            }
        });

        $("#mws-text-cp").ColorPicker({
            color: textColor,
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                updateTextColor(hex, true);
            }
        });

        $("#mws-textglow-cp").ColorPicker({
            color: textGlowColor,
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                updateTextGlowColor(rgb, textGlowColor["a"], true);
            }
        });

        function updateBackground(bg, attach) {
            backgroundPattern = bg;

            if (attach == true)
                attachStylesheet();
        }

        function updateBaseColor(hex, attach) {
            baseColor = "#" + hex;
            $("#mws-base-cp").css('backgroundColor', baseColor);

            if (attach === true)
                attachStylesheet();
        }

        function updatechildBaseColor(hex, attach) {
            childbaseColor = "#" + hex;
           
            if (attach === true)
                attachStylesheet();
        }

        function updateHighlightColor(hex, attach) {
            highlightColor = "#" + hex;
            $("#mws-highlight-cp").css('backgroundColor', highlightColor);

            if (attach === true)
                attachStylesheet();
        }

        function updateTextColor(hex, attach) {
            textColor = "#" + hex;
            $("#mws-text-cp").css('backgroundColor', textColor);

            if (attach === true)
                attachStylesheet();
        }

        function updateTextGlowColor(rgb, alpha, attach) {
            if (rgb != null) {
                textGlowColor.r = rgb["r"];
                textGlowColor.g = rgb["g"];
                textGlowColor.b = rgb["b"];
                textGlowColor.a = alpha;
            } else {
                textGlowColor.a = alpha;
            }

            $("div#mws-themer #mws-textglow-op").slider("value", textGlowColor.a * 100);
            $("#mws-textglow-cp").css('backgroundColor', '#' + rgbToHex(textGlowColor.r, textGlowColor.g, textGlowColor.b));

            if (attach === true)
                attachStylesheet();
        }

        function attachStylesheet(basePath) {
            if ($("#mws-stylesheet-holder").size() == 0) {
                $('body').append('<div id="mws-stylesheet-holder"></div>');
            }

            $("#mws-stylesheet-holder").html($('<style type="text/css">' + generateCSS(basePath) + '</style>'));
        }

        function generateCSS(basePath) {
           
            if (!basePath)
                basePath = "";

            var css =
				backgroundTargets.join(", \n") + "\n" +
				"{\n" +
				"	background-image:url('" + basePath + backgroundPattern + "');\n" +
				"}\n\n" +
				baseColorTargets.join(", \n") + "\n" +
				"{\n" +
				"	background-color:" + baseColor + ";\n" +
				"}\n\n" +
                childbaseColorTargets.join(", \n") + "\n" +
				"{\n" +
				"	background-color:" + childbaseColor + ";\n" +
				"}\n\n" +
				borderColorTargets.join(", \n") + "\n" +
				"{\n" +
				"	border-color:" + highlightColor + ";\n" +
				"}\n\n" +
				textTargets.join(", \n") + "\n" +
				"{\n" +
				"	color:" + textColor + ";\n" +
				"	text-shadow:0 0 6px rgba(" + getTextGlowArray().join(", ") + ");\n" +
				"}\n\n" +
				highlightColorTargets.join(", \n") + "\n" +
				"{\n" +
				"	background-color:" + highlightColor + ";\n" +
				"}\n";

            return css;
        }

        function getTextGlowArray() {
            var array = new Array();
            for (var i in textGlowColor)
                array.push(textGlowColor[i]);

            return array;
        }

        function rgbToHex(r, g, b) {
            var rgb = b | (g << 8) | (r << 16);
            return rgb.toString(16);
        }

        $("div#mws-pagethemer #mws-pagethemer-pageribbon").bind("click", function (event) {
            if ($(this).hasClass("opened")) {
                $(this).toggleClass("opened").parent().animate({ right: "0" }, "slow");
            } else {
                $(this).toggleClass("opened").parent().animate({ right: "275" }, "slow");
            }
        });

        $("div#mws-themer #mws-textglow-op1").slider("destroy");

        $("div#mws-pagethemer #mws-textglow-op1").slider("destroy");
    });
})(jQuery);


/*
 * jquery.noty.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
* noty - jQuery Notification Plugin v1.2.1
* Contributors: https://github.com/needim/noty/graphs/contributors
*
* Examples and Documentation - http://needim.github.com/noty/
*
* Licensed under the MIT licenses:
* http://www.opensource.org/licenses/mit-license.php
*
**/
(function($) {
	$.noty = function(options, customContainer) {

		var base = {};
		var $noty = null;
		var isCustom = false;

		base.init = function(options) {
			base.options = $.extend({}, $.noty.defaultOptions, options);
			base.options.type = base.options.cssPrefix+base.options.type;
			base.options.id = base.options.type+'_'+new Date().getTime();
			base.options.layout = base.options.cssPrefix+'layout_'+base.options.layout;

			if (base.options.custom.container) customContainer = base.options.custom.container;
			isCustom = ($.type(customContainer) === 'object') ? true : false;

			return base.addQueue();
		};

		// Push notification to queue
		base.addQueue = function() {
			var isGrowl = ($.inArray(base.options.layout, $.noty.growls) == -1) ? false : true;
	  	if (!isGrowl) (base.options.force) ? $.noty.queue.unshift({options: base.options}) : $.noty.queue.push({options: base.options});
	  	return base.render(isGrowl);
		};

		// Render the noty
		base.render = function(isGrowl) {

			// Layout spesific container settings
			var container = (isCustom) ? customContainer.addClass(base.options.theme+' '+base.options.layout+' noty_custom_container') : $('body');
	  	if (isGrowl) {
	  		if ($('ul.noty_cont.' + base.options.layout).length == 0)
	  			container.prepend($('<ul/>').addClass('noty_cont ' + base.options.layout));
	  		container = $('ul.noty_cont.' + base.options.layout);
	  	} else {
	  		if ($.noty.available) {
					var fromQueue = $.noty.queue.shift(); // Get noty from queue
					if ($.type(fromQueue) === 'object') {
						$.noty.available = false;
						base.options = fromQueue.options;
					} else {
						$.noty.available = true; // Queue is over
						return base.options.id;
					}
	  		} else {
	  			return base.options.id;
	  		}
	  	}
	  	base.container = container;

	  	// Generating noty bar
	  	base.bar = $('<div class="noty_bar"/>').attr('id', base.options.id).addClass(base.options.theme+' '+base.options.layout+' '+base.options.type);
	  	$noty = base.bar;
	  	$noty.append(base.options.template).find('.noty_text').html(base.options.text);
	  	$noty.data('noty_options', base.options);

	  	// Close button display
	  	(base.options.closeButton) ? $noty.addClass('noty_closable').find('.noty_close').show() : $noty.find('.noty_close').remove();

	  	// Bind close event to button
	  	$noty.find('.noty_close').bind('click', function() { $noty.trigger('noty.close'); });

	  	// If we have a button we must disable closeOnSelfClick and closeOnSelfOver option
	  	if (base.options.buttons) base.options.closeOnSelfClick = base.options.closeOnSelfOver = false;
	  	// Close on self click
	  	if (base.options.closeOnSelfClick) $noty.bind('click', function() { $noty.trigger('noty.close'); }).css('cursor', 'pointer');
	  	// Close on self mouseover
	  	if (base.options.closeOnSelfOver) $noty.bind('mouseover', function() { $noty.trigger('noty.close'); }).css('cursor', 'pointer');

	  	// Set buttons if available
	  	if (base.options.buttons) {
				$buttons = $('<div/>').addClass('noty_buttons');
				$noty.find('.noty_message').append($buttons);
				$.each(base.options.buttons, function(i, button) {
					bclass = (button.type) ? button.type : 'gray';
					$button = $('<button/>').addClass(bclass).html(button.text).appendTo($noty.find('.noty_buttons'))
					.bind('click', function() {
						if ($.isFunction(button.click)) {
							button.click.call($button, $noty);
						}
					});
				});
			}

	  	return base.show(isGrowl);
		};

		base.show = function(isGrowl) {

			// is Modal?
			if (base.options.modal) $('<div/>').addClass('noty_modal').addClass(base.options.theme).prependTo($('body')).fadeIn('fast');

			$noty.close = function() { return this.trigger('noty.close'); };

			// Prepend noty to container
			(isGrowl) ? base.container.prepend($('<li/>').append($noty)) : base.container.prepend($noty);

	  	// topCenter and center specific options
	  	if (base.options.layout == 'noty_layout_topCenter' || base.options.layout == 'noty_layout_center') {
				$.noty.reCenter($noty);
			}

	  	$noty.bind('noty.setText', function(event, text) {
	  		$noty.find('.noty_text').html(text); 
	  		
	  		if (base.options.layout == 'noty_layout_topCenter' || base.options.layout == 'noty_layout_center') {
	  			$.noty.reCenter($noty);
	  		}
	  	});

	  	$noty.bind('noty.setType', function(event, type) {
	  		$noty.removeClass($noty.data('noty_options').type); 

			type = $noty.data('noty_options').cssPrefix+type;

			$noty.data('noty_options').type = type;

			$noty.addClass(type);
	  		
	  		if (base.options.layout == 'noty_layout_topCenter' || base.options.layout == 'noty_layout_center') {
	  			$.noty.reCenter($noty);
	  		}
	  	});

	  	$noty.bind('noty.getId', function(event) {
	  		return $noty.data('noty_options').id;
	  	});

	  	// Bind close event
	  	$noty.one('noty.close', function(event) {
				var options = $noty.data('noty_options');
        if(options.onClose){options.onClose();}

				// Modal Cleaning
				if (options.modal) $('.noty_modal').fadeOut('fast', function() { $(this).remove(); });

				$noty.clearQueue().stop().animate(
						$noty.data('noty_options').animateClose,
						$noty.data('noty_options').speed,
						$noty.data('noty_options').easing,
						$noty.data('noty_options').onClosed)
				.promise().done(function() {

					// Layout spesific cleaning
					if ($.inArray($noty.data('noty_options').layout, $.noty.growls) > -1) {
						$noty.parent().remove();
					} else {
						$noty.remove();

						// queue render
						$.noty.available = true;
						base.render(false);
					}

				});
			});

	  	// Start the show
      if(base.options.onShow){base.options.onShow();}
	  	$noty.animate(base.options.animateOpen, base.options.speed, base.options.easing, base.options.onShown);

	  	// If noty is have a timeout option
	  	if (base.options.timeout) $noty.delay(base.options.timeout).promise().done(function() { $noty.trigger('noty.close'); });
			return base.options.id;
		};

		// Run initializer
		return base.init(options);
	};

	// API
	$.noty.get = function(id) { return $('#'+id); };
	$.noty.close = function(id) {
		//remove from queue if not already visible
		for(var i=0;i<$.noty.queue.length;) {
			if($.noty.queue[i].options.id==id)
				$.noty.queue.splice(id,1);
			else
				i++;
		}
		//close if already visible
		$.noty.get(id).trigger('noty.close');
	};
	$.noty.setText = function(id, text) {
		$.noty.get(id).trigger('noty.setText', text);
	};
	$.noty.setType = function(id, type) {
		$.noty.get(id).trigger('noty.setType', type);
	};
	$.noty.closeAll = function() {
		$.noty.clearQueue();
		$('.noty_bar').trigger('noty.close');
	};
	$.noty.reCenter = function(noty) {
		noty.css({'left': ($(window).width() - noty.outerWidth()) / 2 + 'px'});
	};
	$.noty.clearQueue = function() {
		$.noty.queue = [];
	};
  
  var windowAlert = window.alert;
  $.noty.consumeAlert = function(options){
    window.alert = function(text){
      if(options){options.text = text;}
      else{options = {text:text};}
      $.noty(options);
    };
  }
  $.noty.stopConsumeAlert = function(){
    window.alert = windowAlert;
  }

	$.noty.queue = [];
	$.noty.growls = ['noty_layout_topLeft', 'noty_layout_topRight', 'noty_layout_bottomLeft', 'noty_layout_bottomRight'];
	$.noty.available = true;
	$.noty.defaultOptions = {
		layout: 'top',
		theme: 'noty_theme_default',
		animateOpen: {height: 'toggle'},
		animateClose: {height: 'toggle'},
		easing: 'swing',
		text: '',
		type: 'alert',
		speed: 500,
		timeout: 5000,
		closeButton: false,
		closeOnSelfClick: true,
		closeOnSelfOver: false,
		force: false,
		onShow: false,
		onShown: false,
		onClose: false,
		onClosed: false,
		buttons: false,
		modal: false,
		template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
		cssPrefix: 'noty_',
		custom: {
			container: null
		}
	};

	$.fn.noty = function(options) {
		return this.each(function() {
			 (new $.noty(options, $(this)));
		});
	};

})(jQuery);

//Helper
function noty(options) {
	return jQuery.noty(options); // returns an id
}

/*
 * Ensure.js
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 */

 /*
Script: Ensure.js

Ensure library
	A tiny javascript library that provides a handy function "ensure" which allows you to load 
	Javascript, HTML, CSS on-demand and then execute your code. Ensure ensures that relevent 
	Javascript and HTML snippets are already in the browser DOM before executing your code 
	that uses them.
	
	To download last version of this script use this link: <http://www.codeplex.com/ensure>

Version:
	1.0 - Initial release

Compatibility:
	FireFox - Version 2 and 3
	Internet Explorer - Version 6 and 7
	Opera - 9 (probably 8 too)
	Safari - Version 2 and 3 
	Konqueror - Version 3 or greater

Dependencies:
	<jQuery.js> 
	<MicrosoftAJAX.js>
	<Prototype-1.6.0.js>

Credits:
	- Global Javascript execution - <http://webreflection.blogspot.com/2007/08/global-scope-evaluation-and-dom.html>
	
Author:
	Omar AL Zabir - http://msmvps.com/blogs/omar

License:
	>Copyright (C) 2008 Omar AL Zabir - http://msmvps.com/blogs/omar
	>	
	>Permission is hereby granted, free of charge,
	>to any person obtaining a copy of this software and associated
	>documentation files (the "Software"),
	>to deal in the Software without restriction,
	>including without limitation the rights to use, copy, modify, merge,
	>publish, distribute, sublicense, and/or sell copies of the Software,
	>and to permit persons to whom the Software is furnished to do so,
	>subject to the following conditions:
	>
	>The above copyright notice and this permission notice shall be included
	>in all copies or substantial portions of the Software.
	>
	>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	>INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	>FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
	>IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
	>DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
	>ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
	>OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

(function(){

window.ensure = function( data, callback, scope )
{    
    if( typeof jQuery == "undefined" && typeof Sys == "undefined" && typeof Prototype == "undefined" )
        return alert("jQuery, Microsoft ASP.NET AJAX or Prototype library not found. One must be present for ensure to work");
        
    // There's a test criteria which when false, the associated components must be loaded. But if true, 
    // no need to load the components
    if( typeof data.test != "undefined" )
    {
        var test = function() { return data.test };
        
        if( typeof data.test == "string" )
        {
            test = function() 
            { 
                // If there's no such Javascript variable and there's no such DOM element with ID then
                // the test fails. If any exists, then test succeeds
                return !(eval( "typeof " + data.test ) == "undefined" 
                    && document.getElementById(data.test) == null); 
            }
        }    
        else if( typeof data.test == "function" )      
        {
            test = data.test;
        }
        
        // Now we have test prepared, time to execute the test and see if it returns null, undefined or false in any 
        // scenario. If it does, then load the specified javascript/html/css    
        if( test() === false || typeof test() == "undefined" || test() == null ) 
            new ensureExecutor(data, callback, scope);
        // Test succeeded! Just fire the callback
        else
            callback();
    }
    else
    {
        // No test specified. So, load necessary javascript/html/css and execute the callback
        new ensureExecutor(data, callback, scope);
    }
}

// ensureExecutor is the main class that does the job of ensure.
window.ensureExecutor = function(data, callback, scope)
{
    this.data = this.clone(data);
    this.callback = (typeof scope == "undefined" || null == scope ? callback : this.delegate(callback, scope));
    this.loadStack = [];
    
    if( data.js && data.js.constructor != Array ) this.data.js = [data.js];
    if( data.html && data.html.constructor != Array ) this.data.html = [data.html];
    if( data.css && data.css.constructor != Array ) this.data.css = [data.css];
    
    if( typeof data.js == "undefined" ) this.data.js = [];
    if( typeof data.html == "undefined" ) this.data.html = [];
    if( typeof data.css == "undefined" ) this.data.css = [];
    
    this.init();
    this.load();
}

window.ensureExecutor.prototype = {
    init : function()
    {
        // Fetch Javascript using Framework specific library
        if( typeof jQuery != "undefined" )
        {
            this.getJS = HttpLibrary.loadJavascript_jQuery;
            this.httpGet = HttpLibrary.httpGet_jQuery;
        }
        else if( typeof Prototype != "undefined" )
        {   
            this.getJS = HttpLibrary.loadJavascript_Prototype;
            this.httpGet = HttpLibrary.httpGet_Prototype; 
        }
        else if( typeof Sys != "undefined" )
        {
            this.getJS = HttpLibrary.loadJavascript_MSAJAX;
            this.httpGet = HttpLibrary.httpGet_MSAJAX;
        }
        else
        {
            throw "jQuery, Prototype or MS AJAX framework not found";
        }        
    },
    getJS : function(data)
    {
        // abstract function to get Javascript and execute it
    },
    httpGet : function(url, callback)
    {
        // abstract function to make HTTP GET call
    },    
    load : function()
    {
        this.loadJavascripts( this.delegate( function() { 
            this.loadCSS( this.delegate( function() { 
                this.loadHtml( this.delegate( function() { 
                    this.callback() 
                } ) ) 
            } ) ) 
        } ) );        
    },
    loadJavascripts : function(complete)
    {
        var scriptsToLoad = this.data.js.length;
        if( 0 === scriptsToLoad ) return complete();
        
        this.forEach(this.data.js, function(href)
        {
            if( HttpLibrary.isUrlLoaded(href) || this.isTagLoaded('script', 'src', href) )
            {
                scriptsToLoad --;
            }
            else
            {
                this.getJS({
                    url:        href, 
                    success:    this.delegate(function(content)
                                {
                                    scriptsToLoad --; 
                                    HttpLibrary.registerUrl(href);
                                }), 
                    error:      this.delegate(function(msg)
                                {
                                    scriptsToLoad --; 
                                    if(typeof this.data.error == "function") this.data.error(href, msg);
                                })
                });
            }            
        });
        
        // wait until all the external scripts are downloaded
        this.until({ 
            test:       function() { return scriptsToLoad === 0; }, 
            delay:      50,
            callback:   this.delegate(function()
            {
                complete();
            })
        });
    },    
    loadCSS : function(complete)
    {
        if( 0 === this.data.css.length ) return complete();
        
        var head = HttpLibrary.getHead();
        this.forEach(this.data.css, function(href)
        {
            if( HttpLibrary.isUrlLoaded(href) || this.isTagLoaded('link', 'href', href) )
            {
                // Do nothing
            }
            else
            {            
                var self = this;
                try
                {   
                    (function(href, head)
                    {                             
                        var link = document.createElement('link');
                        link.setAttribute("href", href);
                        link.setAttribute("rel", "Stylesheet");
                        link.setAttribute("type", "text/css");
                        head.appendChild(link);
                    
                        HttpLibrary.registerUrl(href);
                    }).apply(window, [href, head]);
                }
                catch(e)
                {
                    if(typeof self.data.error == "function") self.data.error(href, e.message);
                }                
            }
        });
        
        complete();
    },
    loadHtml : function(complete)
    {
        var htmlToDownload = this.data.html.length;
        if( 0 === htmlToDownload ) return complete();
        
        this.forEach(this.data.html, function(href)
        {
            if( HttpLibrary.isUrlLoaded(href) )
            {
                htmlToDownload --;
            }
            else
            {
                this.httpGet({
                    url:        href, 
                    success:    this.delegate(function(content)
                                {
                                    htmlToDownload --; 
                                    HttpLibrary.registerUrl(href);
                                    
                                    var parent = (this.data.parent || document.body.appendChild(document.createElement("div")));
                                    if( typeof parent == "string" ) parent = document.getElementById(parent);
                                    parent.innerHTML = content;
                                }), 
                    error:      this.delegate(function(msg)
                                {
                                    htmlToDownload --; 
                                    if(typeof this.data.error == "function") this.data.error(href, msg);
                                })
                });
            }            
        });
        
        // wait until all the external scripts are downloaded
        this.until({ 
            test:       function() { return htmlToDownload === 0; }, 
            delay:      50,
            callback:   this.delegate(function()
            {                
                complete();
            })
        });
    },
    clone : function(obj)
    {
        var cloned = {};
        for( var p in obj )
        {
            var x = obj[p];
                
            if( typeof x == "object" )
            {
                if( x.constructor == Array )
                {
                    var a = [];
                    for( var i = 0; i < x.length; i ++ ) a.push(x[i]);
                    cloned[p] = a;
                }
                else
                {
                    cloned[p] = this.clone(x);
                }
            }
            else
                cloned[p] = x;
        }
        
        return cloned;
    },
    forEach : function(arr, callback)
    {
        var self = this;
        for( var i = 0; i < arr.length; i ++ )
            callback.apply(self, [arr[i]]);
    },
    delegate : function( func, obj )
    {
        var context = obj || this;
        return function() { func.apply(context, arguments); }
    },
    until : function(o /* o = { test: function(){...}, delay:100, callback: function(){...} } */)
    {
        if( o.test() === true ) o.callback();
        else window.setTimeout( this.delegate( function() { this.until(o); } ), o.delay || 50);
    },
    isTagLoaded : function(tagName, attName, value)
    {
        // Create a temporary tag to see what value browser eventually 
        // gives to the attribute after doing necessary encoding
        var tag = document.createElement(tagName);
        tag[attName] = value;
        var tagFound = false;
        var tags = document.getElementsByTagName(tagName);
        this.forEach(tags, function(t) 
        { 
            if( tag[attName] === t[attName] ) { tagFound = true; return false } 
        });
        return tagFound;
    }
}

var userAgent = navigator.userAgent.toLowerCase();

// HttpLibrary is a cross browser, cross framework library to perform common operations
// like HTTP GET, injecting script into DOM, keeping track of loaded url etc. It provides
// implementations for various frameworks including jQuery, MSAJAX or Prototype
var HttpLibrary = {
    browser : {
	    version: (userAgent.match( /.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/ ) || [])[1],
	    safari: /webkit/.test( userAgent ),
	    opera: /opera/.test( userAgent ),
	    msie: /msie/.test( userAgent ) && !/opera/.test( userAgent ),
	    mozilla: /mozilla/.test( userAgent ) && !/(compatible|webkit)/.test( userAgent )
    },
    loadedUrls : {},
    
    isUrlLoaded : function(url)
    {
        return HttpLibrary.loadedUrls[url] === true;
    },
    unregisterUrl : function(url)
    {
        HttpLibrary.loadedUrls[url] = false;
    },
    registerUrl : function(url)
    {
        HttpLibrary.loadedUrls[url] = true;
    },
    
    createScriptTag : function(url, success, error)
    {
        var scriptTag = document.createElement("script");
        scriptTag.setAttribute("type", "text/javascript");
        scriptTag.setAttribute("src", url);
        scriptTag.onload = scriptTag.onreadystatechange = function()
        {
            if ( (!this.readyState || 
					this.readyState == "loaded" || this.readyState == "complete") ) {
				success();
			}
		};
        scriptTag.onerror = function()
        {
            error(data.url + " failed to load");
        };
	    var head = HttpLibrary.getHead();
        head.appendChild(scriptTag);
    },
    getHead : function()
    {
        return document.getElementsByTagName("head")[0] || document.documentElement
    },
    globalEval : function(data)
    {
        var script = document.createElement("script");
        script.type = "text/javascript";
		if ( HttpLibrary.browser.msie )
			script.text = data;
		else
			script.appendChild( document.createTextNode( data ) );

        var head = HttpLibrary.getHead();
		head.appendChild( script );
		//head.removeChild( script );
    },
    loadJavascript_jQuery : function(data)
    {
        if( HttpLibrary.browser.safari )
        {
           return jQuery.ajax({
			    type:       "GET",
			    url:        data.url,
			    data:       null,
			    success:    function(content)
			                {
			                    HttpLibrary.globalEval(content);
			                    data.success();
			                },
			    error:      function(xml, status, e) 
                            { 
                                if( xml && xml.responseText )
                                    data.error(xml.responseText);
                                else
                                    data.error(url +'\n' + e.message);
                            },
			    dataType: "html"
		    });
        }
        else
        {
            HttpLibrary.createScriptTag(data.url, data.success, data.error);
        }
    },    
    loadJavascript_MSAJAX : function(data)
    {
        if( HttpLibrary.browser.safari )
        {
            var params = 
            { 
                url: data.url, 
                success: function(content)
                {
                    HttpLibrary.globalEval(content);
                    data.success(content);
                },
                error : data.error 
            };
            HttpLibrary.httpGet_MSAJAX(params);
        }
        else
        {
            HttpLibrary.createScriptTag(data.url, data.success, data.error);
        }
    },
    loadJavascript_Prototype : function(data)
    {
        if( HttpLibrary.browser.safari )
        {
            var params = 
            { 
                url: data.url, 
                success: function(content)
                {
                    HttpLibrary.globalEval(content);
                    data.success(content);
                },
                error : data.error 
            };
            HttpLibrary.httpGet_Prototype(params);
        }
        else
        {
            HttpLibrary.createScriptTag(data.url, data.success, data.error);
        }        
    },
    httpGet_jQuery : function(data)
    {
        return jQuery.ajax({
			type:       "GET",
			url:        data.url,
			data:       null,
			success:    data.success,
			error:      function(xml, status, e) 
                        { 
                            if( xml && xml.responseText )
                                data.error(xml.responseText);
                            else
                                data.error("Error occured while loading: " + url +'\n' + e.message);
                        },
			dataType: data.type || "html"
		});
    },
    httpGet_MSAJAX : function(data)
    {
        var _wRequest =  new Sys.Net.WebRequest();
        _wRequest.set_url(data.url);
        _wRequest.set_httpVerb("GET");
        _wRequest.add_completed(function (result) 
        {
            var errorMsg = "Failed to load:" + data.url;
            if (result.get_timedOut()) {
                errorMsg = "Timed out";
            }
            if (result.get_aborted()) {
                errorMsg = "Aborted";
            }
            
            if (result.get_responseAvailable()) data.success( result.get_responseData() );
            else data.error( errorMsg );
        });

        var executor = new Sys.Net.XMLHttpExecutor();
        _wRequest.set_executor(executor); 
        executor.executeRequest();
    },
    httpGet_Prototype : function(data)
    {
        new Ajax.Request(data.url, {
            method:     'get',
            evalJS:     false,  // Make sure prototype does not automatically evan scripts
            onSuccess:  function(transport, json)
                        {
                            data.success(transport.responseText || "");              
                        },
            onFailure : data.error
        });
    }
};

})();

/*
 * Master page Scripts
 * Changes Done by : Yogesh Chaudhari
 * Date : 25-Sep-2012
 */

// 
  jQuery.fn.dataTableExt.afnSortData['dom-text'] = function (oSettings, iColumn) {
        var aData = [];
        $('td:eq(' + iColumn + ') input', oSettings.oApi._fnGetTrNodes(oSettings)).each(function () {
            aData.push(this.value);
        });
        return aData;
    }
    

    jQuery.fn.dataTableExt.aTypes.unshift(
    function (sData) {
        var sValidChars = "0123456789,.";
        var Char;
        var bDecimal = false;
        var iStart = 0;

        /* Negative sign is valid -  the number check start point */
        if (sData.charAt(0) === '-') {
            iStart = 1;
        }

        /* Check the numeric part */
        for (i = iStart; i < sData.length; i++) {
            Char = sData.charAt(i);
            if (sValidChars.indexOf(Char) == -1) {
                return null;
            }
        }

        return 'numeric-comma';
    }
);


    jQuery.fn.dataTableExt.oSort['numeric-comma-asc'] = function (a, b) {
   
        if(a.indexOf('moneyTbllabel') != -1 || a.indexOf('moneyTblDecimal') != -1){
        a = $(a).text();
        }
        if(b.indexOf('moneyTbllabel') != -1 || b.indexOf('moneyTblDecimal') != -1){
         b = $(b).text();
        }
   
   
    
        var x = (a == "-") ? 0 : a.split(',').join('');
        var y = (b == "-") ? 0 : b.split(',').join('');
       
        x = parseFloat(x);
        y = parseFloat(y);
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    };

    jQuery.fn.dataTableExt.oSort['numeric-comma-desc'] = function (a, b) {
        if(a.indexOf('moneyTbllabel') != -1 || a.indexOf('moneyTblDecimal') != -1){
            a = $(a).text();
        }

        if(b.indexOf('moneyTbllabel') != -1 || b.indexOf('moneyTblDecimal') != -1){
            b = $(b).text();
        }
        var x = (a == "-") ? 0 : a.split(',').join('');
        var y = (b == "-") ? 0 : b.split(',').join('');
       
        x = parseFloat(x);
        y = parseFloat(y);
        return ((x < y) ? 1 : ((x > y) ? -1 : 0));
    };

//    jQuery.extend(jQuery.fn.dataTableExt.oSort, {
//            "date-uk-pre": function (a) {            
//            var month = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
//                var ukDatea = a.split('-');
//                var mm = ((month.indexOf(ukDatea[1]))+1);
//                if(mm<10)
//                mm="0"+mm;
//                return (ukDatea[2] + mm  + ukDatea[0]) * 1;
//            },

//            "date-uk-asc": function (a, b) {           
//                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
//            },

//            "date-uk-desc": function (a, b) {           
//                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
//            }
//        });

        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
            "date-uk-pre": function (a) {           

                var d = a.toString().replace("PM", " PM").replace("AM", " AM");              
                if($.browser.safari){                                                
                var d1 = d.toString(); 
                }
                else{
                 var d1 = d.toString().replace("-", "/").replace("-", "/");  
                }                                                                  
                var tt = new Date(d1).getTime();
                return tt;
            },

            "date-uk-asc": function (a, b) {        
         
            //alert('date-uk-asc');
                 return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },

            "date-uk-desc": function (a, b) {           
            //
            //alert('date-uk-desc');
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            }
        });

          jQuery.fn.dataTableExt.aTypes.unshift(
            function (sData) {
                if (sData !== null && sData.match(/\d{1,2}\/\d{1,2}\/\d{2,4} \d{1,2}:\d{1,2} (am|pm|AM|PM|Am|Pm)/)) {
                
                    return 'date-uk';
                }
                return null;
            }
          );

  $(document).ready(function () {


               $(".mws-form").validate({
                   errorPlacement: function (error, element) {
                  
                       if ($(element).parent().get(0).tagName == "th") {
                           element.closest('th').append(error);
                       }
                       else {
                           element.closest('td').append(error);
                       }
                   },
                   onsubmit: false
               });


               $("#jttrigger-0").click(function () {
                       
                   $(".box1").slideToggle();
                   $("#rightmenu").slideToggle();
                 
               });

               $("#chngact").click(function () {
                   $("#changestatus").hide();
                   $("#assignstatus").hide();
                   $('#sendsms').hide();
                   $('#sendemail').hide();
                   $("#changeActivity").slideToggle();
                   $('#chngsourcebranch').hide();
               });

               $("#chngstatus").click(function () {
                   $("#changeActivity").hide();
                   $("#assignstatus").hide();
                   $('#sendsms').hide();
                   $('#sendemail').hide();
                   $("#changestatus").slideToggle();
                   $('#chngsourcebranch').hide();
               });

               $("#assignto").click(function () {
                   $("#changeActivity").hide();
                   $("#changestatus").hide();
                   $('#sendsms').hide();
                   $('#sendemail').hide();
                   $("#assignstatus").slideToggle();
                    $('#chngsourcebranch').hide();
               });


                 $("#smsBtn").click(function () {
                   $("#changeActivity").hide();
                   $("#changestatus").hide();
                   $('#assignstatus').hide();
                   $('#sendemail').hide();
                   $("#sendsms").slideToggle();
                    $('#chngsourcebranch').hide();
               });

                 $("#emailBtn").click(function () {
                   $("#changeActivity").hide();
                   $("#changestatus").hide();
                   $('#sendsms').hide();
                   $('#assignstatus').hide();
                   $("#sendemail").slideToggle();
                    $('#chngsourcebranch').hide();
               });

                 $("#btnSourceBranch").click(function () {
                   $("#changeActivity").hide();
                   $("#changestatus").hide();
                   $('#sendsms').hide();
                   $('#assignstatus').hide();
                   $("#sendemail").hide();
                    $('#chngsourcebranch').slideToggle();
               });


  });

           function changeNote() {
               
               $("#notedesc").val("Note Description here");
           }


           function SubmitForm() {
               $("#mappingDetails").show();
               return false;
           }
           function NextForm() {
               $("#grdDeatils").show();
               return false;
           }

           function showdetails() {

               $("#grdDeatils").show();
               
               return false;
           }

           
function ConetentToggle(obj) {
   
    $(obj).next('div').toggle();

}

//*************************************************************************************************************
    var entityMap = {
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
        '"': '&quot;',
        "'": '&#39;',
        "/": '&#x2F;',
        "\\": '\\\\',
        " ": "&nbsp;",
    };
    function escapeHtml(string, mode) {
        if (mode == 0) {
            return String(string).replace(/[&<>"'\/\\]/g, function (s) {
                return entityMap[s];
            });
        }
        else if (mode == 1) {      
            var escaped = "";
            var findReplace = [["&", /&amp;/g], ["<", /&lt;/g], [">", /&gt;/g], ["\"", /&quot;/g], ["'", /&#39;/g], ["/", /&#x2F;/g], [" ", /&amp;nbsp;/g]];
            for (var item = 0; item < findReplace.length; item++) {
                string = String(string).replace(findReplace[item][1], findReplace[item][0]);
            }
           return string;
        }
    }


   function ShowPopupOnMore(Id, Desc) {
       $(function(event){
            div = '<div id="Description' + Id + '" style="overflow-y:auto;margin-bottom:10px;padding-right:8px;margin-top:8px;" > ' +
                    '<table width="98%" style="margin-left:10px;table-layout:fixed;">  ' +
                    //'<tr> <td style="width:99%"> <b>Note Description</b> </td></tr>' +
                    '<tr> <td style="width:95%;" class="fixwordwrap"><br />' + Desc + ' </td>  </tr>' +
                    '<tr> <td style="width:99%">  </td></tr>' +
                    '</table>' +
                    '</div>';        
            $('body').append(div);        
            $('#Description' + Id).dialog({
                title: "Description",
                autoOpen: true,
                draggable: true,            
                resizable: false,
                modal: true,
                width: "600px",  
                height: 300,          
                close: function (event, ui) {
                    $(this).remove();
                }
            });           
            return false;
            });
    }

  
    function formatToolTipString(string, mode) {
         if (mode == 1) {
            var escaped = "";
            var findReplace = [["&amp;", /&amp;/g], ["&lt;", /&lt;/g], ["&gt;", /&gt;/g], ["&quot;", /&quot;/g], ["&#39;", /&#39;/g], ["&#x2F;", /&#x2F;/g]] ; //, ["<br />",  /\n/g]
            for (var item = 0; item < findReplace.length; item++) {
                string = String(string).replace(findReplace[item][1], findReplace[item][0]);
            }
           return string;
        }
    }



           function CheckValidatePinCode(obj,cityctr) {
                var PinCode = $(obj).val();
                  var CityId=$(cityctr).val();
                if (PinCode.length == 6) {

                    var isValidOuter = false;

                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "../Workflow/LeadOperations.asmx/IsValidPinCode",
                        data: "{PinCode: '" + PinCode + "', CityId: " + CityId + "}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        beforeSend: function (jqXHR) {
                        },
                        success: function (isValid) {
                      
                            isValid = isValid.d;

                            if (isValid) {
                                $(obj).parent('td').find('label.pincodeerror').remove();
                            }
                            else if (!isValid) {
                                $(obj).parent('td').find('label.pincodeerror').remove();
                                var width = $(obj).get(0).clientWidth;
                                $('<label class="error pincodeerror" style="width:' + width + 'px">Invalid pin code.</label>').insertAfter($(obj));
                            }
                            isValidOuter = isValid;
                        },
                        error: function (error) {
                            $(obj).parent('td').find('label.pincodeerror').remove();
                            isValidOuter = false;
                        }
                    });

                    return isValidOuter;

                }
                else {
                    return true;
                }
            }
    //Added by Bharati
    $('.FTPUserId').live('keypress', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode == 8){
         return true;
        }
        var caretpos = $(this).caret().start;
        valuestart = $(this).val().substring(0, caretpos);
        valueend = $(this).val().substring(caretpos);
        myNum = valuestart + String.fromCharCode(charCode) + valueend;

        var pattern = /^[a-z0-9]+$/i;
        if (!pattern.test(myNum)) {
            return false;

        }
        return true;
        //  
    });
    $('.FTPUserId').live('paste', function (evt) {
        var obj=$(this);
        setTimeout(function(evt){      
        var myval=obj.val();
        var pattern = /^[a-z0-9]+$/i;
        if (!pattern.test(myval)) {
            obj.val(myval.replace(/[^a-zA-Z0-9]/g,''));
            return false;
        }
        return true;
        },0);
        //  
    });

    $(".mws-datepicker").datepicker({ showOn: "both",
                buttonImage: "../Styles/css/icons/16/calendar_1.png",
                buttonImageOnly: true,
                prevText: 'Previous',
                yearRange: 'c-50:c+50',
                changeMonth: true,
                buttonText: '',
                onClose: function (dateText, inst) {
                    $(this).focus();
                },
                changeYear: true
            }).attr('placeholder', 'dd-Mmm-yyyy').blur(function () { return RemoveErrorClass($(this)); });

            $('.mws-timepicker').timepicker({ showOn: 'both',
                buttonImage: '../Styles/css/icons/16/clock.png',
                onClose: function (time, inst) {
                    $(this).focus();
                },
                buttonText: '',
                buttonImageOnly: true
            });
    //Completed here

       // sandeep k changes for Queue group select
        

     
        function MakeQueueSelectGroup(id, groupString) {
            var groups = new Array();
            groups = groupString.split(',');
            //var id = 'box1View';
            //$("#" + id).find('optgroup').replaceWith(function() { return this.innerHTML; });

            var parentselect = document.getElementById(id), opts = parentselect.options,
		i = opts.length, j = groups.length, groupobj = {};
            while (--j > -1) {
                groupobj[groups[j]] = [document.createElement('optgroup'), []];
                groupobj[groups[j]][0].setAttribute('label', groups[j]);
                parentselect.insertBefore(groupobj[groups[j]][0], parentselect.firstChild);
            }
            while (--i > -1) {
                j = groups.length;
                while (--j > -1) {
                    if (opts[i].value.indexOf(groups[j]) > -1) {
                        groupobj[groups[j]][1].push(opts[i]);
                    }
                }
            }
            j = groups.length;
            while (--j > -1) {
                i = groupobj[groups[j]][1].length;
                while (--i > -1) {
                    groupobj[groups[j]][0].appendChild(groupobj[groups[j]][1][i]);
                }
            }
            ExcludeEmptyQueueSelectGroup(id);
           

        }

        
        function ExcludeEmptyQueueSelectGroup(id){
               $("#" + id).find('optgroup').each(function(){
                if ($(this).children().length==0){
                    $(this).replaceWith(function() { return this.innerHTML; });
                }
            }
            );
            
         }
        
       $(document).unbind('keydown').bind('keydown', function (event) {            
            //var doPrevent = false;
            if (event.keyCode == 8) {          
                var d = event.srcElement || event.target;                
                if (d.tagName.toUpperCase() == 'BODY' || (d.tagName.toUpperCase() == 'INPUT' && (d.type.toUpperCase() == 'RADIO')||(d.type.toUpperCase() == 'CHECKBOX'))) {                   
                    //doPrevent = d.readOnly || d.disabled;
                    event.preventDefault();
                }
                //else {
                //    doPrevent = true;
                //}
            }
            //if (doPrevent) {
            //    event.preventDefault();
            //}
        });