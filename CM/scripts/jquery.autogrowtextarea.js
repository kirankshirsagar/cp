//Private variables
var colsDefault = 0;
var rowsDefault = 0;
//var rowsCounter = 0;

//Private functions
function setDefaultValues(txtArea) {
    colsDefault = txtArea.cols;
    rowsDefault = txtArea.rows;

    //rowsCounter = document.getElementById("rowsCounter");
}

function bindEvents(txtArea) {
//    if (txtArea.value != '') {
//        $(txtArea).css('height', 'auto');
//        grow(txtArea);
//    }

    txtArea.onkeyup = function () {

        // txtArea.rows = (txtArea.value.split("\n").length || 1);
        grow(txtArea);
    }
    
}

//Helper functions
function grow(txtArea) {
    //  alert(txtArea.rows);

    while (txtArea.rows > 1 && txtArea.scrollHeight < txtArea.offsetHeight) {
        txtArea.rows--;
    }
//    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
//        while (txtArea.rows >= 1 && txtArea.scrollHeight < txtArea.offsetHeight) {
//            txtArea.rows--;
//        }
//        //Do Firefox-related activities
//    }
//    else {
//        while (txtArea.rows > 1 && txtArea.scrollHeight < txtArea.offsetHeight) {
//            txtArea.rows--;
//        }
//    }

    var h = 0;
    while (txtArea.scrollHeight > txtArea.offsetHeight && h !== txtArea.offsetHeight) {
        h = txtArea.offsetHeight;
        txtArea.rows++;
    }
    //   txtArea.rows++;

    $(txtArea).css('height', 'auto');
}

//Public Method
jQuery.fn.autoGrow = function () {
    return this.each(function () {

        setDefaultValues(this);
        bindEvents(this);
    });
};