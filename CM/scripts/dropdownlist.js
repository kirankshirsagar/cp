﻿




function bindSelect(arr, mySelect) {
    
    try {
        $(mySelect).empty();
        $(arr).each(function (i, val) {
            $(mySelect).append($('<option>', {
                value: val.Id,
                text: val.Name
            }));
        });
        $(mySelect).trigger("liszt:updated");
    }
    catch (err) {

    }
}


function bindSelectStr(arr, mySelect) {

    try {
        $(mySelect).empty();
        $(arr).each(function (i, val) {
            $(mySelect).append($('<option>', {
                value: val.Ids,
                text: val.Name
            }));
        });
        $(mySelect).trigger("liszt:updated");
    }
    catch (err) {

    }
}




function SetChosen() {
    var config = {
        '.chzn-select': {},
        '.chzn-select-deselect': { allow_single_deselect: true },
        '.chzn-select-no-single': { disable_search_threshold: 10 },
        '.chzn-select-no-results': { no_results_text: 'Oops, nothing found!' },
        '.chzn-select-width': { width: "150%" }
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
}
