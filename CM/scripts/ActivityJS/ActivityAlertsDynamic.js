﻿

    var popupStatus = 0; // set value
    function loadPopup() {
        if (popupStatus == 0) { // if value is 0, show popup
            $(".toPopup").fadeIn(0500); // fadein popup div
            $("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
            $("#backgroundPopup").fadeIn(0001);
            setDivPosoitions();
            popupStatus = 1; // and set value to 1
        }
    }

    function load() {
        setTimeout(function () { // then show popup, deley in .5 second
            loadPopup(); // function show popup 
        }, 500); // .5 second
        return false;
    }


    $("div.close").live('click', function () {
        disablePopup(this);  // function close pop up
    });


    function disablePopup(obj) {
        $(obj).parent('div[class^="toPopup"]').fadeOut("normal");
        $("#backgroundPopup").fadeOut("normal");
    }


//    $('.addDivs').click(function () {
//        AllPendingActivities();
//    });


    function setDivPosoitions() {
        var i = 0;
        $('.toPopup').each(function () {
            var add = i * 25;
            var vtop = Math.max(0, (($(window).height() - $(this).outerHeight()) / 3.0) + $(window).scrollTop());
            var vleft = Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft());
            $(this).css({ top: vtop + add, left: vleft /*- add*/, position: 'absolute' });
            i = i + 1;
        });
    }


    $('.clsSnooze').live('click', function () {
        var ActivityId = $(this).parent().find('span[class^="ActivityId"]').html();
        ActivitySnoozeOrDismiss(ActivityId, '1');
        disablePopup($(this).parent().parent().find('div[class^="close"]'));
    });

    $('.clsDismiss').live('click', function () {
        var ActivityId = $(this).parent().find('span[class^="ActivityId"]').html();
        ActivitySnoozeOrDismiss(ActivityId, '0');
        disablePopup($(this).parent().parent().find('div[class^="close"]'));
    });


    function ActivitySnoozeOrDismiss(activityId, snooze) {

        var type = "Snooze";
        var strType = "POST";
        var strContentType = "text/html; charset=utf-8";
        var strData = "{}";
        var strURL = '../Handlers/Activities.ashx?ActivityId=' + activityId + '&Snooze=' + snooze + '&Type=' + type;
        var strCatch = false;
        var strDataType = 'json';
        var strAsync = false;
        var objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
        var arr = objHandler.HandlerReturnedData;

    }




    function AllPendingActivities() {
        
        var userId = $('#hdnGlobalUserId').val();
        var strType = "POST";
        var type = "Read";
        var strContentType = "text/html; charset=utf-8";
        var strData = "{}";
        var strURL = '../Handlers/Activities.ashx?UserId=' + userId + '&Type=' + type;
        var strCatch = false;
        var strDataType = 'json';
        var strAsync = false;
        var objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
        var arr = objHandler.HandlerReturnedData;

        var divContent = '<div class="toPopup clsDragMe"><div class="close"></div>';
        divContent = divContent + '<span class="ecs_tooltip">close <span class="arrow"></span></span>';
        divContent = divContent + '<div id="popup_content"><h2>Activity Reminder</h2>';
        divContent = divContent + '<table>';
        divContent = divContent + '<tr><td><b>Added by</b></td><td>:</td><td><span>#AddedByName#</span></td></tr>';
        divContent = divContent + '<tr><td><b>Activity date</b></td><td>:</td><td><span>#ActivityDateString#</span></td></tr>';
        divContent = divContent + '<tr><td><b>Activity Type</b></td><td>:</td><td><span>#ActivityType#</span></td></tr>';
        divContent = divContent + '<tr><td><b>Status</b></td><td>:</td><td><span class>#ActivityStatusName#</span></td></tr>';
        divContent = divContent + '<tr><td><b>Post on customer portal</b></td><td>:</td><td><span>#IsForCustomer#</span></td></tr>';

        divContent = divContent + '<tr><td><b>Activity Remark</b></td><td>:</td><td><span>#ActivityRemark#</span></td></tr>';
        divContent = divContent + '</table><hr>';
        divContent = divContent + '<input class="clsSnooze" type="button" value="Snooze"/>&nbsp;';
        divContent = divContent + '<input class="clsDismiss" type="button" value="Dismiss"/>';
        divContent = divContent + '<span class="ActivityId" style="display:none">#ActivityId#</span>';
        divContent = divContent + '</div></div>';

        $(arr).each(function (i, val) {
            var activityType = val.ActivityTypeName;
            var activityRemark = val.ActivityText;
            var ActivityId = val.ActivityId;

            var activityDateString = val.ActivityDateString;
            var addedByName = val.AddedByName;
            var activityStatusName = val.ActivityStatusName;
            var isForCustomer = val.IsForCustomer;

            var finalDivContent = divContent;
            finalDivContent = finalDivContent.replace("#ActivityType#", activityType);
            finalDivContent = finalDivContent.replace("#ActivityRemark#", activityRemark);
            finalDivContent = finalDivContent.replace("#ActivityId#", ActivityId);

            finalDivContent = finalDivContent.replace("#ActivityDateString#", activityDateString);
            finalDivContent = finalDivContent.replace("#AddedByName#", addedByName);
            finalDivContent = finalDivContent.replace("#ActivityStatusName#", activityStatusName);
            finalDivContent = finalDivContent.replace("#IsForCustomer#", isForCustomer);



            $('#mainAlertDiv').append(finalDivContent);
        });

        load();
        $('.clsDragMe').udraggable();
    }

