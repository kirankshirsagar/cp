﻿/*

    $.ajax({
        type: "POST",
        contentType: "text/html; charset=utf-8",
        data: "{}",
        url: '../Handlers/GetCityList.ashx',
        cache: false,
        dataType: "json",
        async: false,
        success: function (data) {
            varHandlerReturnedData = data;
        }
    });

*/


var varHandlerReturnedData;

function callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync) {
    this.HandlerReturnedData = '';


    $.ajax({
        type: strType,
        contentType: strContentType,
        data: strData,
        url: strURL,
        cache: strCatch,
        dataType: strDataType,
        async: strAsync,
        success: function (data) {
            varHandlerReturnedData = data;
        }
    });

    this.HandlerReturnedData = varHandlerReturnedData;
}


function JQSelectBind(ddlParentId, ddlChildId, TableName) {
   
    var tableName = TableName;
    var mySelect = $('#' + ddlChildId);
        $(mySelect).empty();
        var parentId = $('#' + ddlParentId).val();
        if (parentId > 0) {
            var strType = "POST";
            var strContentType = "text/html; charset=utf-8";
            var strData = "{}";
            var strURL = '../Handlers/GetMasterList.ashx?TableName=' + tableName + '&ParentId=' + parentId; //'../Handlers/GetCityList.ashx';
            var strCatch = false;
            var strDataType = 'json';
            var strAsync = false;
            var objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
            var arr = objHandler.HandlerReturnedData;
            bindSelect(arr, mySelect);
        }
        else {
            $(mySelect).empty();
            $(mySelect).trigger("liszt:updated");
        }
}

function JQSelectBindStr(ddlParentId, ddlChildId, TableName) {

    var tableName = TableName;
    var mySelect = $('#' + ddlChildId);
    $(mySelect).empty();
    var parentId = $('#' + ddlParentId).val();
    if (parentId > 0) {
        var strType = "POST";
        var strContentType = "text/html; charset=utf-8";
        var strData = "{}";
        var strURL = '../Handlers/GetMasterListStr.ashx?TableName=' + tableName + '&ParentId=' + parentId; //'../Handlers/GetCityList.ashx';
        var strCatch = false;
        var strDataType = 'json';
        var strAsync = false;
        var objHandler = new callHandlerFunction(strType, strContentType, strData, strURL, strCatch, strDataType, strAsync);
        var arr = objHandler.HandlerReturnedData;
        bindSelectStr(arr, mySelect);
    }
    else {
        $(mySelect).empty();
        $(mySelect).trigger("liszt:updated");
    }
}
   
