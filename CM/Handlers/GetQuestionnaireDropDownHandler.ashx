﻿<%@ WebHandler Language="C#" Class="GetStageDDLS" %>

using System;
using System.Web;
using MasterBLL;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using CommonBLL;

public class GetStageDDLS : IHttpHandler
{
    int ContractTemplateID = 0;
    public void ProcessRequest(HttpContext context)
    {
        string type = context.Request.QueryString["Type"].ToString().Trim();

        string MasterName = context.Request.QueryString["MasterName"].ToString().Trim();
        int parentId = int.Parse(context.Request.QueryString["parentId"].ToString().Trim());
        string search = context.Request.QueryString["Search"].ToString().Trim();
        int RequestId = int.Parse(context.Request.QueryString["RequestId"].ToString().Trim());
        
        JavaScriptSerializer jsonSerializaer = new JavaScriptSerializer();
        var result = (string)jsonSerializaer.Serialize(FillMaster(MasterName, parentId, search, RequestId));
        context.Response.Write(result);
    }
    public List<SelectControlFields> FillMaster(string tableName, int parentId, string search, int RequestId)
    {
        ICommonMaster obj = FactoryMaster.GetCommonMasterDetail();
        obj.TableName = tableName;
        obj.ParentId = parentId;
        obj.Search = search;
        obj.StageId = 0;
        obj.RequestId = RequestId;
        List<SelectControlFields> myList;
        if (string.IsNullOrEmpty(search))
            myList = obj.SelectData();
        else
            myList = obj.SelectDataNoSelect();
        return myList;
    }
   
    
    public bool IsReusable {
        get {
            return false;
        }
    }

}