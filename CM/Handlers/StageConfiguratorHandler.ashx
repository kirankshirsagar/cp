﻿<%@ WebHandler Language="C#" Class="StageConfiguratorHandler" %>

using System;
using System.Web;
using MasterBLL;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using CommonBLL;
using StageConfigueBLL;
using System.Drawing;
using System.Collections.Specialized;
using System.IO;
using System.Data;

public class StageConfiguratorHandler : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {

        string ContractTemplateId = context.Request.QueryString["ContractTemplateId"].ToString().Trim();
        string lblContractType = context.Request.QueryString["lblContractType"].ToString().Trim();
        string lblLevel = context.Request.QueryString["lblLevel"].ToString().Trim();
        string txtStageName = context.Request.QueryString["txtStageName"].ToString();
        string ddlUsers = context.Request.QueryString["ddlUsers"].ToString();
        string ddldept = context.Request.QueryString["ddldept"].ToString();
        string AddedBy=Convert.ToString(context.Request.QueryString["AddedBy"]);
         int StageId=0;
         if (context.Request.QueryString["StageId"] != "")
             StageId = int.Parse(Convert.ToString(context.Request.QueryString["StageId"]));
         else
             StageId = 0;
        
        JavaScriptSerializer js = new JavaScriptSerializer();

        string jsonData = new StreamReader(context.Request.InputStream).ReadToEnd();

        StageConfigurator[] obj = new JavaScriptSerializer().Deserialize<StageConfigurator[]>(jsonData);


        List<StageConfigurator> L = new List<StageConfigurator>();


        for (int i = 0; i < obj.Length; i++)
        {

            L.Add(new StageConfigurator()
            {
                ANDOR = obj[i].ANDOR,
                FieldLibraryID=obj[i].FieldLibraryID,
                OperatorID = obj[i].OperatorID,
                txtBox1 = obj[i].txtBox1,
                txtBox2 = obj[i].txtBox2,
                dtpick1 = obj[i].dtpick1,
                dtpick2 = obj[i].dtpick2,
                ForeignKeyValue = obj[i].ForeignKeyValue,
                DataType=obj[i].DataType
            });
        }

        IStageConfigurator objStage = FactoryStageConfigurator.GetStageConfiguratorDetail();
        objStage.StageConditions = objStage.CreateStageConditions(L);
        objStage.StageName = txtStageName.Trim().Replace("\r\n", "<br>");
        
        objStage.DepartmentId = int.Parse(ddldept);
        objStage.UsersId = int.Parse(ddlUsers);
        objStage.ContractTypeId = int.Parse(lblContractType);
        objStage.ContractTemplateId = int.Parse(ContractTemplateId);
        objStage.LevelID = int.Parse(lblLevel);
        objStage.StageId = StageId;
        objStage.AddedBy = Convert.ToInt32(AddedBy);
        string status;
        if (StageId <= 0)
            status = objStage.InsertRecord();
        else
            status = objStage.UpdateRecord();

        context.Response.Write(status);
        
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}
