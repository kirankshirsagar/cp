﻿<%@ WebHandler Language="C#" Class="BulkImport" %>

using System;
using System.Web;
using MasterBLL;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using BulkImportBLL;

public class BulkImport : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        JavaScriptSerializer jsonSerializaer = new JavaScriptSerializer();
        var result = (string)jsonSerializaer.Serialize(BulkImportProcessing());
        context.Response.Write(result);
    }


    public string BulkImportProcessing()
    {        
        HttpContext.Current.Response.Write("entered");
        IUplodedData obj = FactoryBulkImport.GetUplodedDataDetail();
        HttpContext.Current.Response.Write("begin");
        return obj.BulkImportProcessing();
    }
    
    public bool IsReusable {
        get {
            return false;
        }
    }

}