﻿<%@ WebHandler Language="C#" Class="GetStageDDLS" %>

using System;
using System.Web;
using MasterBLL;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using CommonBLL;
using StageConfigueBLL;

public class GetStageDDLS : IHttpHandler
{
    int ContractTemplateID = 0;
    public void ProcessRequest(HttpContext context)
    {
        int ContractTypeId = int.Parse(context.Request.QueryString["ID"].ToString().Trim());
        string type = context.Request.QueryString["Type"].ToString().Trim();
        ContractTemplateID = int.Parse(context.Request.QueryString["ContractTemplateID"].ToString().Trim()); 
        if (type == "fields")
        {
            
            JavaScriptSerializer jsonSerializaer = new JavaScriptSerializer();
            var result = (string)jsonSerializaer.Serialize(FieldFile());
            context.Response.Write(result);
       
        }
        else if (type == "user")
        {
            ContractTypeId = int.Parse(context.Request.QueryString["ID"].ToString().Trim());
            JavaScriptSerializer jsonSerializaer = new JavaScriptSerializer();
            var result = (string)jsonSerializaer.Serialize(UserFile(ContractTypeId));
            context.Response.Write(result);
        }
       else if (type == "dept")
        {
            JavaScriptSerializer jsonSerializaer = new JavaScriptSerializer();
            var result = (string)jsonSerializaer.Serialize(OtherFile());
            context.Response.Write(result);
        
        }
        else if (type == "FillMaster")
        {
            string MasterName = context.Request.QueryString["MasterName"].ToString().Trim();
            int parentId = int.Parse(context.Request.QueryString["parentId"].ToString().Trim());
            string search = context.Request.QueryString["Search"].ToString().Trim();
            int stageId = int.Parse(context.Request.QueryString["StageId"].ToString().Trim());
            int UserId = int.Parse(context.Request.QueryString["AddedBy"].ToString().Trim());
            JavaScriptSerializer jsonSerializaer = new JavaScriptSerializer();
            var result = (string)jsonSerializaer.Serialize(FillMaster(MasterName, parentId, search, stageId, UserId));
            context.Response.Write(result);
        }
        else if (type == "Edit")
        {
            int StageId = int.Parse(context.Request.QueryString["StageId"].ToString().Trim());
            JavaScriptSerializer jsonSerializaer = new JavaScriptSerializer();
            var result = (string)jsonSerializaer.Serialize(Edit(StageId));
            context.Response.Write(result);
        }
        else
        {
           
            JavaScriptSerializer jsonSerializaer = new JavaScriptSerializer();
            jsonSerializaer.MaxJsonLength = 20971520;
            var result = (string)jsonSerializaer.Serialize(OperatorFile(ContractTemplateID));
            context.Response.Write(result);
        
        }
        
    }
    public List<SelectControlFields> FillMaster(string tableName, int parentId, string search, int stageId, int UserId)
    {
        ICommonMaster obj = FactoryMaster.GetCommonMasterDetail();
        obj.TableName = tableName;
        obj.ParentId = parentId;
        obj.Search = search;
        obj.StageId = stageId;
        obj.UserId = UserId;
        List<SelectControlFields> myList=null;
        if (tableName.ToUpper().Contains("OPTIONS"))
        {
            myList = obj.SelectDataStrNoSelect();
        }
        else
        {
            myList = obj.SelectDataNoSelect();
        }
        return myList;
    }
    public List<StageConfigurator> Edit(int StageId)
    {
        IStageConfigurator obj = FactoryStageConfigurator.GetStageConfiguratorDetail();
        obj.StageId = StageId;
        obj.ContractTemplateId = ContractTemplateID;
        obj.ReadData();
        List<StageConfigurator> myList = obj.ReadParent;
        return myList;
    }
    public List<StageConfigurator> OtherFile()
    {
        IStageConfigurator obj = FactoryStageConfigurator.GetStageConfiguratorDetail();
        obj.ContractTemplateId = ContractTemplateID;
        obj.OtherDroDropdowns();
        List<StageConfigurator>myList = obj.DepartmentList;
        return myList;
    }
    public List<StageConfigurator> UserFile(int ContractId)
    {
        IStageConfigurator obj = FactoryStageConfigurator.GetStageConfiguratorDetail();
        obj.ContractTemplateId = ContractTemplateID;
        obj.ContractTypeId = ContractId;
        obj.OtherDroDropdowns();
        List<StageConfigurator> myList = obj.UserNameList;
        return myList;
    }
    public List<StageConfigurator> FieldFile()
    {
        IStageConfigurator obj = FactoryStageConfigurator.GetStageConfiguratorDetail();
        obj.ContractTemplateId = ContractTemplateID; // nilesh
        obj.FieldReadMethod();
        List<StageConfigurator>myList = obj.ReadParent;
        return myList;
    }

    public List<StageConfigurator> OperatorFile(int ContractTemplateID)
    {
        IStageConfigurator obj = FactoryStageConfigurator.GetStageConfiguratorDetail();
        obj.ContractTemplateId = ContractTemplateID;
        obj.FieldReadMethod();
        List<StageConfigurator> myList = obj.ReadChild;
        return myList;
    }
    
    
    public bool IsReusable {
        get {
            return false;
        }
    }

}