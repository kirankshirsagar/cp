﻿<%@ WebHandler Language="C#" Class="GetSubordinateUsersList" %>

using System;
using System.Web;
using MasterBLL;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using UserManagementBLL;
using CommonBLL;

public class GetSubordinateUsersList : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        int DepartmentId = int.Parse(context.Request.QueryString["DepartmentId"].ToString());
        int UserId = int.Parse(context.Request.QueryString["UserId"].ToString());
        JavaScriptSerializer jsonSerializaer = new JavaScriptSerializer();
        var result = (string)jsonSerializaer.Serialize(CreateFile(DepartmentId, UserId));
        context.Response.Write(result);
    }

    public List<SelectControlFields>CreateFile(int departmentId, int userId)
    {
        IUsers obj = FactoryUser.GetUsersDetail();
        obj.DepartmentId = departmentId;
        obj.UserId = userId;
        List<SelectControlFields> myList = obj.SelectSubordinateUsers();
        return myList;
    }
    
    
    public bool IsReusable {
        get {
            return false;
        }
    }

}