﻿<%@ WebHandler Language="C#" Class="User" %>

using System;
using System.Web;
using MasterBLL;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using CommonBLL;
using System.Drawing;
using UserManagementBLL;
using System.Collections.Specialized;
using System.IO;
using System.Data;
using System.Web.Security;

public class User : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        string type = context.Request.QueryString["Type"].ToString().Trim();
        string Email = context.Request.QueryString["Email"].ToString().Trim();
        string UserName = context.Request.QueryString["UserName"].ToString().Trim();
        if (type == "User")
        {
            JavaScriptSerializer jsonSerializaer = new JavaScriptSerializer();
            var result = (string)jsonSerializaer.Serialize(checkuser(UserName));
            context.Response.Write(result);
        }
       else if (type == "email")
        {
            JavaScriptSerializer jsonSerializaer = new JavaScriptSerializer();
            var result = (string)jsonSerializaer.Serialize(checkemail(Email));
            context.Response.Write(result);
        
        }
        else if (type == "IsActive")
        {
            JavaScriptSerializer jsonSerializaer = new JavaScriptSerializer();
            var result = (string)jsonSerializaer.Serialize(CheckUserIsActive(UserName));
            context.Response.Write(result);
        }
        else if (type == "CustomReport")
        {
            JavaScriptSerializer jsonSerializaer = new JavaScriptSerializer();
            var result = (string)jsonSerializaer.Serialize(checkCustomReport(UserName));
            context.Response.Write(result);
        }

    }
    public string CheckUserIsActive(string UserName)
    {
        IUsers obj = FactoryUser.GetUsersDetail();
        obj.UserName = UserName;
        if (UserName != "")
        {
            string cnt = obj.CheckUserIsActive().ToString();
            if (cnt =="1")
                //return "User is locked. Please contact to your system administrator.";
                return "Invalid username or password.";
            else
                return "";
        }
        else
            return "";
    }
    public string checkemail(string Email)
    {
        IUsers obj = FactoryUser.GetUsersDetail();
        obj.Email = Email;
        if (Email != "")
        {
            int cnt = obj.GetUserEmailExists();
            if (cnt > 0)
                return "EmailId is already exists.";
            else
                return "";
        }
        else
            return "";
    }
    public string checkuser(string UserName)
    {   
        //***********************************
        IUsers obj = FactoryUser.GetUsersDetail();
        obj.CreateDALObjForMembershipWork();
        //**********************************
        obj.UserName = UserName;
        MembershipUser user=Membership.GetUser(UserName);
        if (user!=null)
            return "User Name is already exists.";
        else
            return "";
    }

    public string checkCustomReport(string UserName)
    {
        ReportBLL.ICustomReports obj = ReportBLL.FactoryReports.CustomReportDetail();
        //obj.CreateDALObjForMembershipWork();
        obj.Customer_Name = UserName.Trim();
        if (UserName != "")
        {
            int cnt = obj.GetReportName();
            if (cnt > 0)
                return "Report Name is already exists.";
            else
                return "";
        }
        else
            return "";
    }
    public bool IsReusable {
        get {
            return false;
        }
    }

}