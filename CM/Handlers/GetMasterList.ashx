﻿<%@ WebHandler Language="C#" Class="GetMasterList" %>

using System;
using System.Web;
using MasterBLL;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using CommonBLL;

public class GetMasterList : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        string TableName = context.Request.QueryString["TableName"].ToString();
        int ParentId = int.Parse(context.Request.QueryString["ParentId"].ToString());

        string PromptName = string.Empty;
        if (context.Request.QueryString["PromptName"] != null && context.Request.QueryString["PromptName"].ToString().Trim().ToLower() != "undefined")
        {
            PromptName = context.Request.QueryString["PromptName"].ToString().Trim();
        }
        
        JavaScriptSerializer jsonSerializaer = new JavaScriptSerializer();
        var result = (string)jsonSerializaer.Serialize(CreateFile(TableName, ParentId, PromptName));
        context.Response.Write(result);
    }

  

    public List<SelectControlFields>CreateFile(string tableName, int parentId, string promptName)
    {
        ICommonMaster obj = FactoryMaster.GetCommonMasterDetail();
        obj.TableName = tableName;
        obj.ParentId = parentId;
        List<SelectControlFields> myList= null;
        if (promptName == string.Empty)
        {
            myList = obj.SelectData();
        }
        else
        { 
           myList = obj.SelectData(promptName); 
        }
        return myList;
    }
    
    
    public bool IsReusable {
        get {
            return false;
        }
    }

}