﻿<%@ WebHandler Language="C#" Class="BindActivities" %>

using System;
using System.Web;
using CommonBLL;
using WorkflowBLL;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

public class BindActivities : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {

        string Operation = context.Request.QueryString["Op"];
        if (Operation == "Week")
        {
            string StartDate = context.Request.QueryString["StartDate"];
            string EndDate = context.Request.QueryString["EndDate"];
            string UserId = context.Request.QueryString["UserId"];

            IActivity objActivity;
            objActivity = FactoryWorkflow.GetActivityDetails();

            objActivity.StartWeekDate = Convert.ToDateTime(StartDate);
            objActivity.EndWeekDate = Convert.ToDateTime(EndDate);
            objActivity.AddedBy = Convert.ToInt32(UserId);
            objActivity.ReadDataCalendar();
            List<Activity> ListA = objActivity.ActivityDates;
            string HtmlTable = "<table id='tblRawCalendar'><tbody>";

            foreach (var item in ListA)
            {


                HtmlTable += "<tr><td id='ActivityId'>" + item.ActivityId + "</td>";  //1
                HtmlTable += "<td id='ActivityTypeId'>" + item.ActivityTypeId + "</td>";//2
                HtmlTable += "<td id='ActivityTypeName'>" + item.ActivityTypeName + "</td>";//3
                HtmlTable += "<td id='ActivityText'>" + item.ActivityText + "</td>";//4
                HtmlTable += "<td id='ActivityDate'>" + item.ActivityDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) + "</td>";//5
                HtmlTable += "<td id='ActivityTime'>" + item.ActivityTime + "</td>";//6
                //HtmlTable += "<td id='ActivityDateDefaultFormat'>" + item.ActivityDateDefaultFormat.ToString("dd-MMM-yyyy hh:mm tt", CultureInfo.InvariantCulture) + "</td>";//7
                HtmlTable += "<td id='ActivityDateDefaultFormat'>" + item.ActivityDateDefaultFormat.ToString() + "</td>";//7
                HtmlTable += "<td id='AddedByName'>" + item.AddedByName + "</td>";//8
                HtmlTable += "<td id='AddedOn'>" + item.AddedOn + "</td>";//9
                HtmlTable += "<td id='AddedTime'>" + item.AddedTime + "</td>";//10
                HtmlTable += "<td id='OnlyDate'>" + item.OnlyDate + "</td>";//11
                HtmlTable += "<td id='ActivityStatusName'>" + item.ActivityStatusName + "</td>";//12
                HtmlTable += "<td id='ActivityStatusId'>" + item.ActivityStatusId + "</td>";//13
                HtmlTable += "<td id='IsForCustomer'>" + item.IsForCustomer + "</td>";//14
                HtmlTable += "<td id='ReminderDate'>" + item.ReminderDateDefaultFormat + "</td>";//15
                HtmlTable += "<td id='Description'>" + item.Description + "</td>";//16
                HtmlTable += "<td id='isEditable'>" + item.isEditable + "</td>";//17
                HtmlTable += "<td id='FileUpload'>" + item.FileUpload + "</td>";//18
                HtmlTable += "<td id='ReqID'>" + item.ReqID + "</td>"; //19
                HtmlTable += "<td id='ContractTypeId'>" + item.ContractTypeId + "</td></tr>";//20
                HtmlTable += "<td id='IsOCR'>" + item.IsOCR + "</td></tr>";//21
                HtmlTable += "<td id='ContractTypeIds'>" + item.ContractTypeIds + "</td></tr>";//22
            }
            HtmlTable += "</tbody></table>";

            context.Response.Write(HtmlTable);


        }
        else if (Operation == "Day")
        {
            string StartDate = context.Request.QueryString["CurrentDay"];
            //  string EndDate = context.Request.QueryString["EndDate"];
            string UserId = context.Request.QueryString["UserId"];

            IActivity objActivity;
            objActivity = FactoryWorkflow.GetActivityDetails();

            objActivity.StartWeekDate = Convert.ToDateTime(StartDate);
            objActivity.EndWeekDate = null;
            objActivity.AddedBy = Convert.ToInt32(UserId);
            objActivity.ReadDataCalendar();
            List<Activity> ListA = objActivity.ActivityDates;
            string HtmlTable = "<table id='tblRawCalendar'><tbody>";

            foreach (var item in ListA)
            {
                HtmlTable += "<tr><td id='ActivityId'>" + item.ActivityId + "</td>";  //1
                HtmlTable += "<td id='ActivityTypeId'>" + item.ActivityTypeId + "</td>";//2
                HtmlTable += "<td id='ActivityTypeName'>" + item.ActivityTypeName + "</td>";//3
                HtmlTable += "<td id='ActivityText'>" + item.ActivityText + "</td>";//4
                HtmlTable += "<td id='ActivityDate'>" + item.ActivityDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) + "</td>";//5
                HtmlTable += "<td id='ActivityTime'>" + item.ActivityTime + "</td>";//6
                //HtmlTable += "<td id='ActivityDateDefaultFormat'>" + item.ActivityDateDefaultFormat.ToString("dd-MMM-yyyy hh:mm tt", CultureInfo.InvariantCulture) + "</td>";//7
                HtmlTable += "<td id='ActivityDateDefaultFormat'>" + item.ActivityDateDefaultFormat.ToString() + "</td>";//7
                HtmlTable += "<td id='AddedByName'>" + item.AddedByName + "</td>";//8
                HtmlTable += "<td id='AddedOn'>" + item.AddedOn + "</td>";//9
                HtmlTable += "<td id='AddedTime'>" + item.AddedTime + "</td>";//10
                HtmlTable += "<td id='OnlyDate'>" + item.OnlyDate + "</td>";//11
                HtmlTable += "<td id='ActivityStatusName'>" + item.ActivityStatusName + "</td>";//12
                HtmlTable += "<td id='ActivityStatusId'>" + item.ActivityStatusId + "</td>";//13
                HtmlTable += "<td id='IsForCustomer'>" + item.IsForCustomer + "</td>";//14
                HtmlTable += "<td id='ReminderDate'>" + item.ReminderDateDefaultFormat + "</td>";//15
                HtmlTable += "<td id='Description'>" + item.Description + "</td>";//15
                HtmlTable += "<td id='isEditable'>" + item.isEditable + "</td>";//17
                HtmlTable += "<td id='FileUpload'>" + item.FileUpload + "</td>";//18
                HtmlTable += "<td id='ReqID'>" + item.ReqID + "</td>"; //19
                HtmlTable += "<td id='ContractTypeId'>" + item.ContractTypeId + "</td></tr>";//20
                HtmlTable += "<td id='IsOCR'>" + item.IsOCR + "</td></tr>";//21
                HtmlTable += "<td id='ContractTypeIds'>" + item.ContractTypeIds + "</td></tr>";//22
            }
            HtmlTable += "</tbody></table>";

            context.Response.Write(HtmlTable);


        }
        else
        {
            string Year = context.Request.QueryString["Year"];
            string Month = context.Request.QueryString["Month"];
            string UserId = context.Request.QueryString["UserId"];

            IActivity objActivity;
            objActivity = FactoryWorkflow.GetActivityDetails();

            objActivity.ActivityMonth = Convert.ToInt32(Month);
            objActivity.ActivityYear = Convert.ToInt32(Year);
            objActivity.StartWeekDate = null;
            objActivity.EndWeekDate = null;
            objActivity.AddedBy = Convert.ToInt32(UserId);
            objActivity.ReadDataCalendar();
            List<Activity> ListA = objActivity.ActivityDates;
            string HtmlTable = "<table id='tblRawCalendar'><tbody>";

            foreach (var item in ListA)
            {


                HtmlTable += "<tr><td id='ActivityId'>" + item.ActivityId + "</td>";  //1
                HtmlTable += "<td id='ActivityTypeId'>" + item.ActivityTypeId + "</td>";//2
                HtmlTable += "<td id='ActivityTypeName'>" + item.ActivityTypeName + "</td>";//3
                HtmlTable += "<td id='ActivityText'>" + item.ActivityText + "</td>";//4
                HtmlTable += "<td id='ActivityDate'>" + item.ActivityDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) + "</td>";//5
                HtmlTable += "<td id='ActivityTime'>" + item.ActivityTime + "</td>";//6
                //HtmlTable += "<td id='ActivityDateDefaultFormat'>" + item.ActivityDateDefaultFormat.ToString("dd-MMM-yyyy hh:mm tt", CultureInfo.InvariantCulture) + "</td>";//7
                HtmlTable += "<td id='ActivityDateDefaultFormat'>" + item.ActivityDateDefaultFormat.ToString() + "</td>";//7
                HtmlTable += "<td id='AddedByName'>" + item.AddedByName + "</td>";//8
                HtmlTable += "<td id='AddedOn'>" + item.AddedOn + "</td>";//9
                HtmlTable += "<td id='AddedTime'>" + item.AddedTime + "</td>";//10
                HtmlTable += "<td id='OnlyDate'>" + item.OnlyDate + "</td>";//11
                HtmlTable += "<td id='ActivityStatusName'>" + item.ActivityStatusName + "</td>";//12
                HtmlTable += "<td id='ActivityStatusId'>" + item.ActivityStatusId + "</td>";//13
                HtmlTable += "<td id='IsForCustomer'>" + item.IsForCustomer + "</td>";//14
                HtmlTable += "<td id='ReminderDate'>" + item.ReminderDateDefaultFormat + "</td>";//15
                HtmlTable += "<td id='Description'>" + item.Description + "</td>";//16
                HtmlTable += "<td id='isEditable'>" + item.isEditable + "</td>";//17
                HtmlTable += "<td id='FileUpload'>" + item.FileUpload + "</td>";//18
                HtmlTable += "<td id='ReqID'>" + item.ReqID + "</td>"; //19
                HtmlTable += "<td id='ContractTypeId'>" + item.ContractTypeId + "</td></tr>";//20
                HtmlTable += "<td id='IsOCR'>" + item.IsOCR + "</td></tr>";//21
                HtmlTable += "<td id='ContractTypeIds'>" + item.ContractTypeIds + "</td></tr>";//22
            }
            HtmlTable += "</tbody></table>";
            context.Response.Write(HtmlTable);

        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}