﻿<%@ WebHandler Language="C#" Class="TrackDocumentChanges" %>

using System;
using System.Web;
using MasterBLL;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using CommonBLL;
using System.Drawing;
using UserManagementBLL;
using System.Collections.Specialized;
using System.IO;
using System.Data;
using System.Web.Security;
using WorkflowBLL;
using System.Text;

public class TrackDocumentChanges : IHttpHandler
{

    ITrackDocumentChanges objTrackDocChanges;
    string lEntity="";

    void CreateObjects()
    {
       
        try
        {
            objTrackDocChanges = FactoryWorkflow.GetTrackDocumentChangesObject();
        }
        catch (Exception ex)
        {           
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
       
    }

    public void ProcessRequest(HttpContext context)
    {

        try
        {
            string lHTML = "";
            CreateObjects();
            lEntity = Convert.ToString(context.Request.QueryString["Entity"]);
            DataTable dtTrackChanges = new DataTable();
            
            //if (lEntity.Equals("Term"))
            //{
            //    objTrackDocChanges.TrackChangesFieldID = Convert.ToInt32(context.Request.QueryString["ID"]);
            //     dtTrackChanges = objTrackDocChanges.GetTrackChangesTermsSubDetails();
            //     lHTML = GenerateHTMLTableForLine(dtTrackChanges);
            //}
            
            if (lEntity.Equals("Clause"))
            {
                objTrackDocChanges.TrackChangesClauseID = Convert.ToInt32(context.Request.QueryString["ID"]);
                 dtTrackChanges = objTrackDocChanges.GetTrackChangesClauseSubDetails();
                 lHTML = GenerateHTMLTableForClause(dtTrackChanges);

            }

            if (lEntity.Equals("Line"))
            {
                objTrackDocChanges.TrackChangesID = Convert.ToInt32(context.Request.QueryString["ID"]);
                dtTrackChanges = objTrackDocChanges.GetTrackChangesLineSubDetails();
                lHTML = GenerateHTMLTableForLine(dtTrackChanges);
            }
            
           
            context.Response.Write(lHTML);

        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }


    }



    #region GenerateHTMLTableForClause
    public string GenerateHTMLTableForClause(DataTable dtContent)
    {
        try
        {
            string lOldVersion = "", lNewVersion = "", lOldValue = "", lNewValue = "", lTrackChangesClauseID = "", lTrackChangesFieldID = "", lBookmarkName = "";

            if (dtContent.Rows.Count > 0)
            {
                if (lEntity.Equals("Term"))
                {
                    lTrackChangesFieldID = Convert.ToString(dtContent.Rows[0]["TrackChangesFieldID"]);
                }
                if (lEntity.Equals("Clause"))
                {
                    lTrackChangesClauseID = Convert.ToString(dtContent.Rows[0]["TrackChangesClauseID"]);
                }

                lOldVersion = Convert.ToString(dtContent.Rows[0]["OldVersion"]);
                lOldValue = Convert.ToString(dtContent.Rows[0]["OldFileValue"]);
                lNewVersion = Convert.ToString(dtContent.Rows[0]["NewVersion"]);
                lNewValue = Convert.ToString(dtContent.Rows[0]["NewFileValue"]);
                lBookmarkName = Convert.ToString(dtContent.Rows[0]["BookmarkName"]);

            }

            StringBuilder lHTML = new StringBuilder();
            lHTML.Append("<table id='tblClauseSubDetails' width='100%' cellpadding='2' runat='server'>");
            lHTML.Append("<tbody>");

            lHTML.Append("<tr runat='server'>");

            lHTML.Append(" <td style='display:none' runat='server'>");
            if (lEntity.Equals("Term"))
            {
                lHTML.Append(lTrackChangesFieldID);////////////
            }
            if (lEntity.Equals("Clause"))
            {
                lHTML.Append(lTrackChangesClauseID);////////////
            }

            lHTML.Append("</td>");

            lHTML.Append(" <td width='50%'>");
            lHTML.Append("<b>" + lNewVersion + "</b>");////////////
            lHTML.Append("</td>");

            lHTML.Append("<td width='50%'>");
            lHTML.Append("<b>" + lOldVersion + "</b>");////////////
            lHTML.Append("</td>");

            lHTML.Append("</tr>");

            lHTML.Append("<tr id='trClauseSubDetailsValues' runat='server'>");

            lHTML.Append(" <td style='display:none'>");
            lHTML.Append(lBookmarkName);
            lHTML.Append("</td>");

            lHTML.Append("<td width='50%'>");

            lHTML.Append("<asp:Label ID='lblClauseNewValue' runat='server' ClientIDMode='Static'>" + lNewValue + "</asp:Label>");
            lHTML.Append("<a id='btnCModifyValue' title='Click for edit.' class='icon icon-edit' href='#' onclick='ModifyClauseValue(this);scrollToAnchor(this);'/>");
            lHTML.Append(" <textarea id='txtClauseNewValue'  onchange='SetNewClauseValueInParentTD(this)' runat='server' type='text'  clientidmode='Static'  style='display:none; width:100%'></textarea>");


            lHTML.Append("</td>");

            lHTML.Append("<td width='50%'>");
            lHTML.Append(lOldValue);/////////////
            lHTML.Append("</td>");

            lHTML.Append(" </tr>");

            lHTML.Append(" <tr>");

            lHTML.Append(" </tr>");

            lHTML.Append("</tbody></table>");


            return lHTML.ToString();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }  
    #endregion








    #region GenerateHTMLTableForLine
    public string GenerateHTMLTableForLine(DataTable dtContent)
    {
        try
        {
            string lOldVersion = "", lNewVersion = "", lOldValue = "", lNewValue = "", lNewValueToEdit = "", lTrackChangesID = "", lBookmarkName = "";

            if (dtContent.Rows.Count > 0)
            {

                lTrackChangesID = Convert.ToString(dtContent.Rows[0]["TrackChangesID"]);

                lOldVersion = Convert.ToString(dtContent.Rows[0]["OldVersion"]);
                lOldValue = Convert.ToString(dtContent.Rows[0]["OldFileValue"]);
                lNewVersion = Convert.ToString(dtContent.Rows[0]["NewVersion"]);
                lNewValue = Convert.ToString(dtContent.Rows[0]["NewFileValue"]);
                lNewValueToEdit = Convert.ToString(dtContent.Rows[0]["NewFileValueToEdit"]);
                lBookmarkName = Convert.ToString(dtContent.Rows[0]["BookmarkName"]);

            }

            StringBuilder lHTML = new StringBuilder();
            lHTML.Append("<table id='tblLineSubDetails' width='100%' cellpadding='2' runat='server'>");
            lHTML.Append("<tbody>");

            lHTML.Append("<tr runat='server'>");

            lHTML.Append(" <td style='display:none' runat='server'>");


            lHTML.Append(lTrackChangesID);////////////


            lHTML.Append("</td>");

            lHTML.Append(" <td width='50%'>");
            lHTML.Append("<b>" + lNewVersion + "</b>");////////////
            lHTML.Append("</td>");

            lHTML.Append("<td width='50%'>");
            lHTML.Append("<b>" + lOldVersion + "</b>");////////////
            lHTML.Append("</td>");

            lHTML.Append("</tr>");

            lHTML.Append("<tr id='trLineSubDetailsValues' runat='server'>");

            lHTML.Append(" <td style='display:none'>");
            lHTML.Append(lBookmarkName);
            lHTML.Append("</td>");

            lHTML.Append("<td width='50%'>");

            lHTML.Append("<Label ID='lblLineNewValueToEdit' style='display:none' runat='server' ClientIDMode='Static'>" + lNewValueToEdit + "</Label>");
            lHTML.Append("<asp:Label ID='lblLineNewValue' runat='server' ClientIDMode='Static'>" + lNewValue + "</asp:Label></br>");
            lHTML.Append("<a id='btnLModifyValue' title='Click for edit.' class='icon icon-edit' href='#' onclick='ModifyLineValue(this);scrollToAnchor(this);'/>");
            lHTML.Append("<textarea id='txtLineNewValue'  onchange='SetNewLineValueInParentTD(this)' runat='server' type='text'  clientidmode='Static'  style='display:none; width:100%'></textarea>");



            lHTML.Append("</td>");

            lHTML.Append("<td width='50%'>");
            lHTML.Append(lOldValue);/////////////
            lHTML.Append("</td>");

            lHTML.Append(" </tr>");

            lHTML.Append(" <tr>");

            lHTML.Append(" </tr>");

            lHTML.Append("</tbody></table>");


            return lHTML.ToString();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    } 
    #endregion
    
    
    
    
    
    
    
    
    


    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}