﻿<%@ WebHandler Language="C#" Class="GetEmailTemplateFiles" %>

using System;
using System.Web;


public class GetEmailTemplateFiles : IHttpHandler
{

    public void ProcessRequest (HttpContext context) {
        
        foreach (string file in context.Request.Files)
        {

            HttpPostedFile hpf = context.Request.Files[file] as HttpPostedFile;
            int chunk = context.Request["chunk"] != null ? int.Parse(context.Request["chunk"]) : 0;

            string FileName = Convert.ToString(context.Request.QueryString["FileName"]);
            string FileSize = Convert.ToString(context.Request.QueryString["FileSize"]);
            string UserName = Convert.ToString(context.Request.QueryString["UserName"]);
            string FileNameOriginal = FileName;

            

            if (hpf.ContentLength == 0 && context.Request.Files.Count == 1)
                throw new Exception();
            else if (hpf.ContentLength == 0)
                continue;

            string FolderPath = HttpContext.Current.Server.MapPath(@"..\Uploads\EmailAttachments");

            string myfile = FileName;
            if (hpf.FileName != "blob")
            {
                myfile = DateTime.Now.ToString().Replace("/", "").Replace(".", "").Replace(":", "").Replace(" ", "") + FileName.Replace(" ","");
            }



            string savedFileName = FolderPath + "//" + myfile;
            //     hpf.SaveAs(savedFileName);

            using (
                var fs = new System.IO.FileStream(System.IO.Path.Combine(FolderPath, myfile),
                chunk == 0 ? System.IO.FileMode.Create : System.IO.FileMode.Append))
            {
                var buffer = new byte[hpf.InputStream.Length];
                hpf.InputStream.Read(buffer, 0, buffer.Length);

                fs.Write(buffer, 0, buffer.Length);
            }

            var Id = "0";

            var FileSizeBytes = FileSize;
            FileSize = getSize(int.Parse(FileSize));
            context.Response.Write("Chunk#" + Id + "#../Uploads/" + HttpContext.Current.Session["TenantDIR"] + "/EmailAttachments/" + myfile + '#' + UserName + '#' + FileSize + '#' + FileSizeBytes);

        }
        
    }


    private string getSize(int val)
    {
        string rvalue = "";
        if (val > 1024 && val < 1024000)
        {
            rvalue = (val / 1024).ToString()+" kb";     
        }
        else if (val > 1024000)
        {
            rvalue = (val / 1024000).ToString() + " mb";
        }
        else {
           rvalue = val.ToString() +" bytes";
        }
        return "(" + rvalue + ")";
    }
    
    
    public bool IsReusable {
        get {
            return false;
        }
    }

}