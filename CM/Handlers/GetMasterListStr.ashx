﻿<%@ WebHandler Language="C#" Class="GetMasterListStr" %>

using System;
using System.Web;
using MasterBLL;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using CommonBLL;

public class GetMasterListStr : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        string TableName = context.Request.QueryString["TableName"].ToString();
        int ParentId = int.Parse(context.Request.QueryString["ParentId"].ToString());
        JavaScriptSerializer jsonSerializaer = new JavaScriptSerializer();
        var result =  (string)jsonSerializaer.Serialize(CreateFile(TableName, ParentId));
        context.Response.Write(result);
    }

    public List<SelectControlFields>CreateFile(string tableName, int parentId)
    {
        ICommonMaster obj = FactoryMaster.GetCommonMasterDetail();
        obj.TableName = tableName;
        obj.ParentId = parentId;
        List<SelectControlFields> myList = obj.SelectDataStr();
        return myList;
    }
    
    
    public bool IsReusable {
        get {
            return false;
        }
    }

}