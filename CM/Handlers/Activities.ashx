﻿<%@ WebHandler Language="C#" Class="Activities" %>

using System;
using System.Web;
using MasterBLL;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using WorkflowBLL;

public class Activities : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        string type = context.Request.QueryString["Type"].ToString();
        JavaScriptSerializer jsonSerializaer = new JavaScriptSerializer();

        if (type == "Read")
        {
            string useId = context.Request.QueryString["UserId"].ToString();
            var result = (string)jsonSerializaer.Serialize(CreateFile(useId));
            context.Response.Write(result);
        }
        else
        {
            string ActivityId = context.Request.QueryString["ActivityId"].ToString();
            string Snooze = context.Request.QueryString["Snooze"].ToString();
            var result = (string)jsonSerializaer.Serialize(ActivitySnoozeAndDismiss(Snooze,ActivityId));
            context.Response.Write(result);
        } 
       
    }


    public List<Activity>CreateFile(string useId)
    {
        IActivity obj = FactoryWorkflow.GetActivityDetails();
        obj.AddedBy = int.Parse(useId);
        List<Activity> myList = obj.GetActivityPending();
        return myList;
    }

    public string ActivitySnoozeAndDismiss(string Snooze, string ActivityId )
    { 
        IActivity obj = FactoryWorkflow.GetActivityDetails();
        obj.ActivityId = int.Parse(ActivityId);
        obj.Snooze = int.Parse(Snooze);
        return obj.ActivitySnoozeAndDismiss();
    }
    
    
    public bool IsReusable {
        get {
            return false;
        }
    }

}