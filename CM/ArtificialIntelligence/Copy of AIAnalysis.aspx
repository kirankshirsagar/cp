﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Copy of AIAnalysis.aspx.cs" Inherits="ArtificialIntelligence_AIAnalysis" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">    
       <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ContractPod AI</title>
	
    <!-- Google font including -->
    <link href="https://fonts.googleapis.com/css?family=Roboto|Ubuntu:700" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    
    <!-- Style CSS -->
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
   <div class="topbar">
    	<div class="container">
        	<div class="row">
            
                <div class="col-lg-12 clearfix">
                	<a href="../WorkFlow/home.aspx" class="home-button">
                        <i class="fa fa-home"></i>
                    </a>
                    <div class="logo-box clearfix">
                    	<div class="main-logo">
                            <a href="/"><img src="img/contractpod-ai.png" alt="ContractPod AI"></a>
                        </div>
                        <div class="powered-by">
                            <img src="img/powered-by.png" alt="ContractPod AI">
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    
    <div class="middle-area">
    	<div class="container">
        	<form class="analyze-contract-form" action="analyzed.php" method="post">
              <div class="sid-box">
                  <div class="plus-add"><i class="fa fa-plus"></i></div>
                  <h3>I am Sid, your contract analyst ! </h3>
                  <%--<p>Drag &amp; Drop your contract and I will analyze and create your contract record.</p>--%>
                  <p>Upload your contract and I will analyze and create your contract record.</p>
                  <asp:FileUpload ID="UploadContract" runat="server" onchange="Validate();"/>
              </div>
              <div class="submit-analyzing">
              	<button type="submit" class="submit-button" ID="Analyze" runat="server" disabled="disabled" onserverclick="Analyze_Click"> Start Analysis </button>
              </div>
            </form>            
        </div>    	
    </div>
    <asp:HiddenField ID="hdnRequestId" runat="server" />
    </form>
</body>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easypiechart.min.js"></script>
    <script src="js/script.js"></script>

    <script type="text/javascript">
        function Validate() {
            var FileName = $("#UploadContract").val();
            if (FileName == "") {
                $("#Analyze").attr("disabled", "true");
                $("#Analyze").addClass("disabled");
            }
            else {
                $("#Analyze").removeAttr("disabled");
                $("#Analyze").removeClass("disabled");
            }
        }
    </script>
</html>
