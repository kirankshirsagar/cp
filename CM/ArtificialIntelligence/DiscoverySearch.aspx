﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="DiscoverySearch.aspx.cs" Inherits="ArtificialIntelligence_DiscoverySearch" %>

<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/PaginationButtons.ascx" TagName="PaginationButtons" TagPrefix="PB" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    
    <script src="../CommonScripts/GridNavigation.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="Masterlinks1" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>
    
    <h2>Discovery Search</h2>
    <div>
    <br />
    <span id="MainContent_SearchBox1" style="display:inline-block;height:100px;">
    <label>Ask a question</label>
    <asp:TextBox ID="txtSearchTerm" runat="server" Height="30px" Width="50pc" CssClass="" Font-Size="20px"></asp:TextBox>
    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" style="vertical-align:middle;"/>
    </span>

   
      <div id="tblMetadataTitle" runat="server">
        <table style="table-layout: fixed; width: 100%;">
            <tr>
                <td width="80%">
                    <label>
                        Here's what I found</label>
                </td>               
            </tr>
        </table>
          <div class="autoscroll">
              <table width="100%">
                  <asp:Repeater ID="rptMetaDataSearch" runat="server">
                      <HeaderTemplate>
                          <table id="masterDataTable" class="reportTable masterTable list issues" width="100%">
                              <thead>
                                  <tr>
                                      <th style="width: 6%;">
                                          <asp:Label ID="lblSearchedText" runat="server" Text=""></asp:Label>
                                      </th>
                                  </tr>
                              </thead>
                              <tbody>
                      </HeaderTemplate>
                      <ItemTemplate>
                          <tr>
                              <td>
                                  <%#Eval("ContractId") %>
                              </td>
                          </tr>
                      </ItemTemplate>
                      <FooterTemplate>
                      </FooterTemplate>
                  </asp:Repeater>
              </table>
              <PB:PaginationButtons ID="PaginationButtons1" runat="server" />
          </div>
    </div>

     </div>
</asp:Content>
