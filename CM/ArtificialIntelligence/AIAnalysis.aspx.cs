﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Net;
using System.Data;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using CommonBLL;
using WorkflowBLL;
using DocumentOperationsBLL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using com.valgut.libs.bots.Wit;
using com.valgut.libs.bots.Wit.Models;
using RestSharp;
using Aspose.Words;

public partial class ArtificialIntelligence_AIAnalysis : System.Web.UI.Page
{

    static string FilePath = "";
    static string IsPDF = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Declarations.User] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
    }

    protected void Analyze_Click(object sender, EventArgs e)
    {
        FilePath = ""; IsPDF = "";
        string filePath = SaveAIDocument();
        hdnFilePath.Value = filePath.Replace("\\", "\\\\");
        Page.JavaScriptStartupScript("showclient2331314142", "FetchClient();");
        //InsertUpdate();
    }
    protected void btnProceed_Click(object sender, EventArgs e)
    {
        InsertUpdate();
    }

    string SaveAIDocument()
    {
         ViewState["IsPDF"] = "N";
        string docpath = "", EXTN = "";
        string location = Server.MapPath("~/Uploads/" + Session["TenantDIR"] + "/AINLP/");
        string ParentDirectory = location + "PDF";

        FileInfo fi = new FileInfo(UploadContract.PostedFile.FileName);
        EXTN = fi.Extension.ToLower();

         string FileName = fi.Name.Replace(EXTN, "") + "_" + DateTime.Now.ToString("ddMMyyyHHmmss") + EXTN;        

        if (EXTN.Equals(".pdf"))
        {
            ViewState["IsPDF"] = "Y";
            IsPDF = "Y";
            //location += "\\PDF\\";
            location += "\\PDF\\ScannedPDF\\"; // if OCR
        }
        docpath = location + "\\" + FileName;

        if (!Directory.Exists(location))
            Directory.CreateDirectory(location);

        if (UploadContract.HasFile)
        {
            UploadContract.SaveAs(docpath);
        }

        if (EXTN.Equals(".pdf"))
        {
            try
            {
                AsyncIndexer.GetInstance().ConvertOCRFile(FileName, EXTN, "AI", location, ParentDirectory);
                PDFtoWORD wrd = new PDFtoWORD();
                docpath = wrd.Convert(ParentDirectory + "\\" + FileName);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Response.Write(ex.Message);
            }
            //Path.ChangeExtension(docpath, "docx");
        }
        FilePath = docpath;
        return docpath;
    }

    [WebMethod]
    public static string ProceedAI(string ClientName, string AddressId, string FileAddress, string Sender)
    {
        ArtificialIntelligence.ClientName = ClientName;
        if (Sender.Contains("New"))
        {
            ArtificialIntelligence.ClientAddress = FileAddress;
            ArtificialIntelligence.ClientAddressId = "";
        }
        else
        {
            ArtificialIntelligence.ClientAddress = "";
            ArtificialIntelligence.ClientAddressId = AddressId;
        }
        string RequestId = ArtificialIntelligence.ReadAndExtract(FilePath, true, true, 0, IsPDF);
        return RequestId;
    }

    [WebMethod]
    public static string FetchClientName(string FilePath)
    {
        string cName = ArtificialIntelligence.GetClientName(FilePath);
        return cName;
    }

    void InsertUpdate()
    {        
        Page.TraceWrite("Insert, Update starts.");
        string filePath = FilePath;//hdnFilePath.Value;// SaveAIDocument();
        try
        {
            //string GetClientName
            string RequestId = ArtificialIntelligence.ReadAndExtract(filePath, true, true, 0, Convert.ToString(ViewState["IsPDF"]));

            if (!string.IsNullOrEmpty(RequestId))
            {
                hdnRequestId.Value = RequestId;
                //Server.Transfer("AIAnalysisResult.aspx",true);
                Response.Redirect("AIAnalysisResult.aspx?RequestId=" + RequestId, false);
            }
            else
            {
                Response.Redirect("AIAnalysisResult.aspx");
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            Response.Redirect("AIAnalysisResult.aspx");
        }       
    }
}