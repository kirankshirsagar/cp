﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using WorkflowBLL;
using CommonBLL;

public partial class CheckAIData : System.Web.UI.Page
{
    public static string hdnSelectedClientId;

    protected void Page_Load(object sender, EventArgs e)
    {        
        string ClientName = string.IsNullOrEmpty(Convert.ToString(Request.QueryString["ClientName"])) ? "" : Request.QueryString["ClientName"];
        string ClientAddr = string.IsNullOrEmpty(Convert.ToString(Request.QueryString["ClientAddr"])) ? "" : Request.QueryString["ClientAddr"]; 
        
        IContractRequest objcontractreq = new ContractRequest();
        List<SelectControlFields> Clients = new List<SelectControlFields>();
        Clients = objcontractreq.SelectClientData();
        
        ViewState["ClientName"] = ClientName;
        ViewState["ClientAddress"] = ClientAddr;
        hdnClientName.Value = ClientName;
        hdnClientAddress.Value = ClientAddr;

        if (ClientName.Length > 3)
            ClientName = ClientName.Substring(0, 3);

        var MatchedClients = from Client in Clients where Client.Name.ToLower().StartsWith(ClientName.ToLower()) select Client;

        foreach (SelectControlFields c in MatchedClients)
        {            
            if (!c.Name.Contains("Select"))
                ddlClient.Items.Add(new ListItem(c.Name, c.Id.ToString()));
        }
        //ddlClient.extDataBind(queryResults);
    }

    [WebMethod]
    public static string LoadAddress(string ClientId)
    {
        IContractRequest objContractRequest;
        objContractRequest = FactoryWorkflow.GetContractRequestDetails();
        objContractRequest.ClientID = Convert.ToInt32(ClientId);
        string ClientAddressDetails = objContractRequest.ReadAddressDetails();
        return ClientAddressDetails;
    }   
}