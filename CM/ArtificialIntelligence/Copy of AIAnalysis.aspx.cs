﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Data;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using CommonBLL;
using WorkflowBLL;
using DocumentOperationsBLL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using com.valgut.libs.bots.Wit;
using com.valgut.libs.bots.Wit.Models;
using RestSharp;
using Aspose.Words;

public partial class ArtificialIntelligence_AIAnalysis : System.Web.UI.Page
{
    private static readonly Encoding Encoding = Encoding.UTF8;    
    static string ContractingParty = "";
    static string ClientName = "";
    static string ContractType = "";
    static string ClientAddress = "";
    static string CountryName = "";

    static string flagTermnationforMaterialBreach = "N";
    static string flagTermnationforInsolvency = "N";
    static string flagTerminationforChangeofControl = "N";
    static string flagTerminationforConvenience = "N";

    static string flagThirdPartyGuarantee = "N";
    static string flagEntireAgreement = "N";
    static string flagStrictLiability = "N";
    static string flagChangeOfControl = "N";

    static string Indemnity = "";
    static string Insurance = "";
    static string LiabilityCap = "Uncapped Liablity";
    static string Assignment = "";

    static string EffectiveDate = "";
    static string ExpiryDate = "";
    static decimal AIFileSize = 0;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Analyze_Click(object sender, EventArgs e)
    {
        string url = "http://49.248.112.163/ainlp/API/Upload";
        string filePath = SaveAIDocument();
        var response = SendDocumentRequest(url, filePath);

        JavaScriptSerializer jss = new JavaScriptSerializer();
        string v = (string)jss.Deserialize(response, typeof(string));
        dynamic data = JObject.Parse(v);

        ViewState["AIFileName"] = filePath;
        SetValues(data);        
        InsertUpdate();
    }

    string SaveAIDocument()
    {
        string docpath = "";
        string location = Server.MapPath("~/Uploads/" + Session["TenantDIR"] + "/AINLP/");
        FileInfo fi = new FileInfo(UploadContract.PostedFile.FileName);
        string FileName = fi.Name.Replace(fi.Extension, "") + "_" + DateTime.Now.ToString("ddMMyyyHHmmss") + fi.Extension;
        docpath = location + "\\" + FileName;

        if (UploadContract.HasFile)
        {
            UploadContract.SaveAs(docpath);
            fi = new FileInfo(docpath);
            AIFileSize = Math.Round(Convert.ToDecimal(fi.Length) / 1024, 2);
        }
        return docpath;
    }

    void SetValues(dynamic data)
    {
        //btnProceed.Visible = true;

        if (data.knowledgeStudio.contracttype != null)
        {
            ContractType = data.knowledgeStudio.contracttype.Value;
        }

        if (data.knowledgeStudio.contractingparty != null)
        {
            ContractingParty = data.knowledgeStudio.contractingparty.Value;
        }        

        if (data.knowledgeStudio.thirdpartyname != null)
        {
            ClientName = data.knowledgeStudio.thirdpartyname.Value;
        }

        if (data.knowledgeStudio.thirdpartyaddress != null)
        {
            ClientAddress = data.knowledgeStudio.thirdpartyaddress.Value;
        }
        if (data.knowledgeStudio.countryname != null)
        {
            CountryName = data.knowledgeStudio.countryname.Value;
        }

        if (data.knowledgeStudio.entireagreement != null)
        {
            flagEntireAgreement = "Y";
        }

        if (data.knowledgeStudio.strictliability != null)
        {
            flagStrictLiability = "Y";
        }

        if (data.knowledgeStudio.thirdpartyguarantee != null)
        {
            flagThirdPartyGuarantee = "Y";
        }

        //if (data.knowledgeStudio.changeofcontrol != null) // Debayan requested to change 
        //if (data.knowledgeStudio.terminationforchangeofcontrol != null)//changed again
        if (data.knowledgeStudio.termination_coc != null)
        {
            flagChangeOfControl = "Y";
        }

        //if (data.knowledgeStudio.terminationforinsolvency != null)//changed 
        //if (data.knowledgeStudio.insolvency != null)//changed again
        if (data.knowledgeStudio.termination_insolvecy != null)
        {
            flagTermnationforInsolvency = "Y";
        }

        //if (data.knowledgeStudio.terminationforchangeofcontrol != null)//changed again
        if (data.knowledgeStudio.termination_coc != null)
        {
            flagTerminationforChangeofControl = "Y";
        }

        //if (data.knowledgeStudio.terminationformaterialbreach != null)//changed 
        //if (data.knowledgeStudio.termnationformaterialbreach != null)//changed again
        if (data.knowledgeStudio.termination_breach != null)
        {
            flagTermnationforMaterialBreach = "Y";
        }

        //if (data.knowledgeStudio.terminationforconvenience != null)//changed 
        if (data.knowledgeStudio.termination_convenience != null)            
        {
            flagTerminationforConvenience = "Y";
        }

        WitClient client = new WitClient("LPTIVMCVS2SWHXKOH5ZNWKETM6UNHVBI");
        //Message message = client.GetMessage("the sixteenth day of January two thousand and nine");

        if (data.knowledgeStudio.effectivedate != null)
        {
            //EffectiveDate = data.knowledgeStudio.effectivedate.Value;
            Message message = client.GetMessage(data.knowledgeStudio.effectivedate.Value);
            EffectiveDate = ExtractDate(message);
        }

        if (data.knowledgeStudio.expirydate != null)
        {
            //ExpiryDate = data.knowledgeStudio.expirydate.Value;
            Message message = client.GetMessage(data.knowledgeStudio.expirydate.Value);
            ExpiryDate = message._text;
        }

        if (data.indemnity != null)
        {
            Indemnity = "The agreement contains Indemnity provision";
        }
        if (data.liability != null)
        {
            if (!string.IsNullOrEmpty(data.liability.Value))
            {
                LiabilityCap = data.liability.Value.Trim().Replace("'", "");
            }
        }
        // For Insurance
        if (data.knowledgeStudio.generalliability != null)
        {
            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudio.generalliability.Value : data.knowledgeStudio.generalliability.Value;
        }
        if (data.knowledgeStudio.otherinsurance != null)
        {
            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudio.otherinsurance.Value : data.knowledgeStudio.otherinsurance.Value;
        }
        if (data.knowledgeStudio.waiverofsubrogation != null)
        {
            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudio.waiverofsubrogation.Value : data.knowledgeStudio.waiverofsubrogation.Value;
        }
        if (data.knowledgeStudio.professionalliability != null)
        {
            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudio.professionalliability.Value : data.knowledgeStudio.professionalliability.Value;
        }
        if (data.knowledgeStudio.employerliability != null)
        {
            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudio.employerliability.Value : data.knowledgeStudio.employerliability.Value;
        }
        if (data.knowledgeStudio.workerscompensation != null)
        {
            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudio.workerscompensation.Value : data.knowledgeStudio.workerscompensation.Value;
        }
        if (data.knowledgeStudio.automobileliability != null)
        {
            Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudio.automobileliability.Value : data.knowledgeStudio.automobileliability.Value;
        }

        //Indemnity = data.indemnity.Value.Trim().Replace("'", "");
        //Insurance = data.insurance.Value.Trim().Replace("'", "");
        //LiabilityCap = data.liability.Value.Trim().Replace("'", "");
        Assignment = data.assignment.Value.Trim().Replace("'", "");
    }

    string ExtractDate(Message message)
    {
        string date = "", day = "", month = "", year = "";

        Dictionary<string, List<com.valgut.libs.bots.Wit.Models.Entity>> dict = message.entities;
        try
        {
            List<com.valgut.libs.bots.Wit.Models.Entity> DateCapture = dict["date_capture"].ToList();
            day = DateCapture[0].value.ToString();

            DateCapture = dict["month_capture"].ToList();
            month = DateCapture[0].value.ToString();

            DateCapture = dict["year_capture"].ToList();
            year = DateCapture[0].value.ToString();
            date = day + "-" + month + "-" + year;
        }
        catch (Exception ex)
        {
            date = "";
        }
        return date;
    }

    public static string SendDocumentRequest(string url, string filePath)
    {
        // Reference: https://briangrinstead.com/blog/multipart-form-post-in-c/
        var webRequest = WebRequest.Create(url);
        webRequest.Method = "POST";

        // form data formatting

        string boundary = string.Format("----------{0:N}", Guid.NewGuid());
        webRequest.ContentType = "multipart/form-data; boundary=" + boundary;

        // define content boundary
        Stream formDataStream = new MemoryStream();
        var fileName = filePath.Substring(filePath.LastIndexOf('\\') + 1);
        string header = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"document\"; filename=\"{1}\"\r\nContent-Type: application/octet-stream\r\n\r\n", boundary, fileName);

        formDataStream.Write(Encoding.GetBytes(header), 0, Encoding.GetByteCount(header));

        // write the file contents to the form stream in between boundaries
        var fileBytes = File.ReadAllBytes(filePath);
        formDataStream.Write(fileBytes, 0, fileBytes.Length);

        // indicate end of file boundary
        string footer = "\r\n--" + boundary + "--\r\n";
        formDataStream.Write(Encoding.GetBytes(footer), 0, Encoding.GetByteCount(footer));

        // output the formDataStream into a byte[] to be sent with the request
        formDataStream.Position = 0;
        byte[] formData = new byte[formDataStream.Length];
        formDataStream.Read(formData, 0, formData.Length);
        formDataStream.Close();

        // send the form data in the request to the upload API
        using (var requestStream = webRequest.GetRequestStream())
        {
            requestStream.Write(formData, 0, formData.Length);
            requestStream.Flush();
            requestStream.Close();
        }

        // read response from API server
        using (var responseStream = webRequest.GetResponse().GetResponseStream())
        {
            using (var reader = new StreamReader(responseStream))
            {
                var content = reader.ReadToEnd();
                return content;
            }
        }
    }   

    void InsertUpdate()
    {
        Page.TraceWrite("Insert, Update starts.");
        string RequestId = "", ContractFileName = "";
        string val = "0";
        IContractRequest objcontractreq = new ContractRequest();
        objcontractreq.ContractTypeName = ContractType;        
        objcontractreq.ClientName = ClientName;
        objcontractreq.Street1 = ClientAddress;
        objcontractreq.CountryName = CountryName;
        objcontractreq.UserID = Convert.ToInt32(Session[Declarations.User]);
        objcontractreq.DepartmentId = Convert.ToInt32(Session[Declarations.Department]);        
        objcontractreq.ContractingpartyName = ContractingParty;
        objcontractreq.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
        objcontractreq.AIFileSize = AIFileSize;
        try
        {
            objcontractreq.RequestID = 0;
            string retVal = objcontractreq.AIInsertRecord();
            RequestId = System.Text.RegularExpressions.Regex.Split(retVal, ",")[0];
            ContractFileName = System.Text.RegularExpressions.Regex.Split(retVal, ",")[1];

            IKeyFields kf = new KeyFeilds();
            kf.RequestId = Convert.ToInt32(RequestId);
            kf.Indemnity = Indemnity;
            kf.LiabilityCap = LiabilityCap;
            kf.Insurance = Insurance;
            kf.Indemnity = Indemnity;
            kf.AssignmentNovation = Assignment;
            kf.IsTerminationConvenience = flagTerminationforConvenience;
            kf.IsTerminationinChangeofcontrol = flagTerminationforChangeofControl;
            kf.IsTerminationinsolvency = flagTermnationforInsolvency;
            kf.IsTerminationmaterial = flagTermnationforMaterialBreach;
            kf.IsAgreementClause = flagEntireAgreement;
            kf.IsChangeofControl = flagChangeOfControl;
            kf.IsGuaranteeRequired = flagThirdPartyGuarantee;
            kf.IsStrictliability = flagStrictLiability;
            kf.dtMatadataFieldValues = MetaData();
            kf.Param = 2;
            string KOStatus = kf.InsertRecord();

            #region ImportantDates

            if (!string.IsNullOrEmpty(EffectiveDate))
                kf.EffectiveDate = Convert.ToDateTime(EffectiveDate);

            if (!string.IsNullOrEmpty(ExpiryDate))
                kf.ExpirationDate = Convert.ToDateTime(ExpiryDate);
            kf.Param = 1;
            string IDStatus = kf.InsertRecord();

            #endregion

            if (KOStatus == "1")
            {
                GenerateContractVersion(ContractFileName);
                hdnRequestId.Value = RequestId;
                //Server.Transfer("AIAnalysisResult.aspx",true);
                Response.Redirect("AIAnalysisResult.aspx?RequestId=" + RequestId, false);
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            Response.Redirect("AIAnalysisResult.aspx");
        }
        finally
        {
            ResetVariables();
        }
    }

    public DataTable MetaData()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("MetaDataFieldId", Type.GetType("System.Int32"));
        dt.Columns.Add("FieldValue", Type.GetType("System.String"));
        return dt;
    }

    void GenerateContractVersion(string ContractFileName)
    {
        string AIFileName = Convert.ToString(ViewState["AIFileName"]);        

        string DestinationDOCX = Server.MapPath("~/Uploads/" + Session["TenantDIR"] + "/Contract Documents/") + ContractFileName;
        string DestinationPDF = Server.MapPath("~/Uploads/" + Session["TenantDIR"] + "/Contract Documents/PDF/") + ContractFileName;

        try
        {
            if (!string.IsNullOrEmpty(AIFileName))
            {
                //FileInfo fi = new FileInfo(AIFileName);
                //fi.CopyTo(CopyTo + fi.Extension);
                
                License license = new License();
                license.SetLicense("Aspose.Words.lic");
                Document doc = new Document(AIFileName);

                doc.Save(DestinationDOCX + ".docx", SaveFormat.Docx);
                doc.Save(DestinationPDF + ".pdf", SaveFormat.Pdf);
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    void ResetVariables()
    {
        ContractingParty = "";
        ClientName = "";
        ContractType = "";
        ClientAddress = "";
        CountryName = "";

        flagTermnationforMaterialBreach = "N";
        flagTermnationforInsolvency = "N";
        flagTerminationforChangeofControl = "N";
        flagTerminationforConvenience = "N";

        flagThirdPartyGuarantee = "N";
        flagEntireAgreement = "N";
        flagStrictLiability = "N";
        flagChangeOfControl = "N";

        Indemnity = "";
        Insurance = "";
        LiabilityCap = "Uncapped Liablity";
        Assignment = "";

        EffectiveDate = "";
        ExpiryDate = "";
        AIFileSize = 0;
    }
}