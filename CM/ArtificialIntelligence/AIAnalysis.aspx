﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AIAnalysis.aspx.cs" Inherits="ArtificialIntelligence_AIAnalysis" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ContractPod AI</title>
    <!-- Google font including -->
    <link href="https://fonts.googleapis.com/css?family=Roboto|Ubuntu:700" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <!-- Style CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="../scripts/jquery-1.7.2.min.js" type="text/javascript"></script>    
    <script src="../JQueryValidations/mastervalidations.js" type="text/javascript"></script>

<%--
<link href="../JQueryValidations/jvalidations.css" rel="stylesheet" type="text/css" />
<script src="../JQueryValidations/mastervalidations.js" type="text/javascript"></script>
<link href="../Styles/application.css" media="all" rel="stylesheet" type="text/css" />--%>
<link href="../JQueryValidations/dhtmlwindow.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/dhtmlwindow.js" type="text/javascript"></script>
    <link href="../JQueryValidations/modal.css" rel="stylesheet" type="text/css" />
    <script src="../JQueryValidations/modal.js" type="text/javascript"></script>

<script type="text/javascript">
    $(window).load(function () {        
        $("div.drag-controls").css("display", "none");
    });

    function FetchClient() {
    
        var FilePath = $("#hdnFilePath").val();
        var AddressStringArray = new Array();
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "AIAnalysis.aspx/FetchClientName",
            data: "{FilePath:'" + FilePath + "'}",
            dataType: "json",
            async: false,
            success: function (output) {
                debugger;
                if (output.d != '') {
                    ShowClientMatches(output.d);
                } else {
                    $("#btnProceed").trigger("click");
                }
            }
        });
    }

    var addr = "", client = "", sender = "", RequestId = "";
    function ShowClientMatches(ClientDet) {
        //var ClientName = $('#CloseUploaderImg').attr('ContractTypeId')
        var ClientName = ClientDet.split("@@##@@")[0];
        var ClientAddr = ClientDet.split("@@##@@")[1];
        if (ClientAddr == undefined)
            ClientAddr = "";
        $("html, body").scrollTop(0);
        $(document).find('div.tooltip').removeClass('tooltip');
       
        var MatchesFound = dhtmlmodal.open('MatchesFound', 'iframe', 'CheckAIData.aspx?ClientName=' + ClientName + '&ClientAddr=' + ClientAddr, '', 'width=800px,scrollbars=no,height=400px,center=1,resize=0"', "");

        MatchesFound.onclose = function (obj) { //Define custom code to run when window is closed         

            //var theform = this.contentDoc.forms[0] //Access first form inside iframe just for your reference
            var ClientDetailsForm = this.contentDoc.getElementById("ClientDetails"); //Access form inside iframe

            sender = ClientDetailsForm.hdnSender.value;

            if (sender.indexOf('New') > -1) {
                client = ClientDetailsForm.hdnClientName.value;
                addr = ClientDetailsForm.hdnClientAddress.value; 
            }
            else {
                client = ClientDetailsForm.ddlClient.selectedOptions[0].text;
                addr = ClientDetailsForm.hdnAdddressFromList.value;
            }

            ProceedAIAnanlysis();
            if (RequestId == "")
                location.href = "AIAnalysisResult.aspx";
            else
                location.href = "AIAnalysisResult.aspx?RequestId=" + RequestId;
            return true;
        }
    }

    function ProceedAIAnanlysis() {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "AIAnalysis.aspx/ProceedAI",
            data: "{ClientName:'" + client + "',AddressId:'" + addr + "',FileAddress:'" + addr + "',Sender:'" + sender + "'}",
            dataType: "json",
            async: false,
            success: function (output) {
                if (output.d != '') {
                    RequestId = output.d;
                }
            },
            error: function (err) {
                debugger;
            }
        });
    }

    
    </script>
<style type="text/css">
    
     .modal
    {
        position: fixed;
        top: 0;
        left: 0;
        background-color: black;
        z-index: 99;
        opacity: 0.8;
        filter: alpha(opacity=80);
        -moz-opacity: 0.8;
        min-height: 100%;
        width: 100%;
    }
    .loading
    {
        width: 100%;
        height: 100%;        
        font-family: Arial;
        font-size: 11pt;
        border: none;        
       vertical-align:middle;
        position: absolute;
       background: url(img/Background.png) no-repeat fixed;
        z-index: 999;
        text-align:center;
        color:gray;
        line-height:100px;
        display:none;
    }
   .loading span
    {
           color:white;
           font-size:25px;       
    }
    .loading img
    {
           vertical-align:middle;      
    }
</style>
</head>

<body>
    <form id="form1" runat="server">
    <div class="loading" align="center" style="display:none;">
            <br />    
            <span>Analyzing...</span>
            <br />           
            <div style="line-height:normal">sit back and relax while<br />
            I analyze your contract
            </div>
            <br />            
            <img src="img/analysing-loader.png" alt="" />       
    </div>

    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 clearfix">
                    <a href="../WorkFlow/home.aspx" class="home-button"><i class="fa fa-home"></i></a>
                    <div class="logo-box clearfix">
                        <div class="main-logo">
                            <a href="/">
                                <img src="img/contractpod-ai.png" alt="ContractPod AI"></a>
                        </div>
                        <div class="powered-by">
                            <%--<img src="img/powered-by.png" alt="ContractPod AI">--%>
                            <img src="img/with_watson_badge_primary_white.png" alt="ContractPod AI">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="middle-area">
        <div class="container">
            <form class="analyze-contract-form" action="" method="post">
            <div class="sid-box">
                <div class="plus-add">
                    <i class="fa ">ː</i></div>
                <h3>
                    I am EːV, your contract analyst!</h3>
                <%--<p>Drag &amp; Drop your contract and I will analyze and create your contract record.</p>--%>
                <p>
                    Upload your contract and I will analyze and create your contract record.</p>
                <asp:FileUpload ID="UploadContract" runat="server" onchange="Validate();" />
            </div>
            <div class="submit-analyzing">
                <button type="submit" class="submit-button" id="Analyze" runat="server" disabled="disabled" clientidmode="Static" 
                   onserverclick="Analyze_Click">
                    Start Analysis
                </button>
            </div>
            </form>
        </div>
    </div>
    <button type="submit" id="btnProceed" runat="server" disabled="disabled" clientidmode="Static" style="display:none;"
                   onserverclick="btnProceed_Click">
                    Start Analysis
                </button>
    <asp:HiddenField ID="hdnRequestId" runat="server" />
    <asp:HiddenField ID="hdnFilePath" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnClientName1" runat="server" ClientIDMode="Static" />
    <input id="hdnAdddressFromList" runat="server" clientidmode="Static" type="hidden" />
    
    </form>
</body>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.easypiechart.min.js"></script>
<script src="js/script.js"></script>


<script type="text/javascript">
    function Validate() {
        var FileName = $("#UploadContract").val();
        if (FileName == "") {
            $("#Analyze").attr("disabled", "true");
            $("#Analyze").addClass("disabled");
        }
        else {
            $("#Analyze").removeAttr("disabled");
            $("#Analyze").removeClass("disabled");
        }
    }
    $('#Analyze').bind("click", function () {
        ShowProgress();
    });

    function ShowProgress() {       
        setTimeout(function () {
            var modal = $('<div />');
            modal.addClass("modal");
            $('body').append(modal);
            var loading = $(".loading");
            loading.show();
            var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
            var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
            loading.css({ top: top, left: left });
        }, 200);
    }
</script>
 
 
</html>
