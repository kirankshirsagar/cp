﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true" CodeFile="NLP.aspx.cs" Inherits="AI_NLP" %>
<%@ Register Src="../UserControl/Requestdetaillinks.ascx" TagName="Masterlinks" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<style type="text/css">
    span
    {
        color: #777675;
        font-size: 13px;
        height: auto;
    }
</style>


 <div id="rightlinks" style="display: none;">
        <uc1:Masterlinks ID="rightactions" runat="server" />
    </div>
    <script type="text/javascript">
        $("#sidebar").html($("#rightlinks").html());
    </script>

    <asp:FileUpload ID="FileUpload1" runat="server" />
    <br />
<br />
    <asp:Button ID="Call" runat="server" Text="Preview" onclick="Call_Click" />
    <br />
    <br />
    <asp:Label ID="lblJSONData" runat="server" Text="" CssClass="search-filter-label"></asp:Label>
    <br />
    <br />
    <br />
    <asp:Button ID="btnProceed" runat="server" Text="Proceed"  Visible="false"
        onclick="btnProceed_Click" />
    <asp:Button ID="btnCancel" runat="server" Text="Cancel"  Visible="false"
        onclick="btnCancel_Click" />
</asp:Content>

