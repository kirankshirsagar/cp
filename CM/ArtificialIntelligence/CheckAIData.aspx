﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CheckAIData.aspx.cs" Inherits="CheckAIData" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../scripts/jquery-1.7.2.min.js" type="text/javascript"></script>    
    <script src="../JQueryValidations/mastervalidations.js" type="text/javascript"></script>
    <script src="../Scripts/application.js" type="text/javascript"></script>
    <link href="../Styles/application.css" media="all" rel="stylesheet" type="text/css" />

    <link href="../Styles/chosen.css" rel="stylesheet" type="text/css" />
    <script src="../scripts/chosen.jquery.js" type="text/javascript"></script>

    <style type="text/css">
        html, body
        {
            overflow-x: visible;
            overflow-y: visible;
        }
    </style>
    <script type="text/javascript">
        $(window).load(function () {
            $("div.drag-controls").css("display", "none");
        });

        function BindAddress() {
            var ClientId = $("#ddlClient").val();
            var AddressStringArray = new Array();
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "CheckAIData.aspx/LoadAddress",
                data: "{ClientId:'" + ClientId + "'}",
                dataType: "json",
                async: false,
                success: function (output) {

                    if (output.d != '') {
                        var AddressString = output.d.split('##AddressStart##')[1].split('##AddressEnd##')[0];
                        AddressStringArray = AddressString.split('####');
                        $('#divAddressList').find('ul').remove();
                        $('#divAddressList').append('<ul class="List" style="list-style-type: none;padding-left:0px;margin-top: 5px" id="ulAddress"></ul>');
                        var AddCnt = AddressStringArray.length - 1;
                        var marginTop = 5;
                       
                        for (var i = 0; i < AddressStringArray.length; i++) {
                            if (i > 0) {
                                marginTop = 15;                                
                            }
                            if (AddressStringArray[i].split('~')[1] != '') {
                               
                                $('#divAddressList ul').append('<li id="' + AddressStringArray[i].split('~')[2] + '" style="margin-top: ' + marginTop + 'px"><input type="radio" class="radio_label" runat="Server" name="Address" onchange="SetValuess(this)" ><span>' + AddressStringArray[i].split('~')[1].substring(AddressStringArray[i].split('~')[1].indexOf(",") + 1) + '</span></input></li>');
                            }
                            $('#divAddressList ul li:eq(0) input').attr("checked", "checked");
                        }
                    }
                }
            });
        }

        function SetValuess(obj) {
            if ($(obj).attr('name').indexOf('Address') > -1) {
                $('#hdnAdddressFromList').val($(obj).parent().attr('id'));
            }
        }
        
    </script>
</head>
<body style="">
    <form id="ClientDetails" runat="server">  
    <input id="hdnclientID" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnAdddressFromList" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnClientName" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnClientAddress" runat="server" clientidmode="Static" type="hidden" />
    <input id="hdnRequestId" runat="server" clientidmode="Static" type="hidden" />   
    <input id="hdnSender" runat="server" clientidmode="Static" type="hidden" /> 

    <asp:HiddenField ID="hdnPrimeId" ClientIDMode="Static" runat="server" />

    <div class="box">        
        <fieldset class="box tabularr">
            <legend>
                <h2>
                    This looks like an existing Customer/Supplier</h2>
                    
                    Please confirm if this is an existing customer or supplier.
            </legend> 
        </fieldset>        
    </div>

    <table style="vertical-align: top; width: 100%; font-size:20px; table-layout:fixed;" border="0" >
        <tbody>
            <tr>
                <th style="width: 10%; text-align: right; padding-top: 5px; vertical-align: top;"
                    class="AIPopupLabel">
                    Name :
                </th>
                <td style="width: 30%; padding: 5px 0px 0px 5px; vertical-align: top; word-wrap: break-word;
                    font-family: Arial,sans-serif; font-size: 15px; font-weight: normal">
                    <%= ViewState["ClientName"]%>
                </td>
                <th style="width: 20%; text-align: right; padding-top: 5px; vertical-align: top;"
                    class="AIPopupLabel">
                    Matches Found :
                </th>
                <td style="width: 40%; padding: 0px 0px 0px 5px; vertical-align: top;">
                    <select id="ddlClient" clientidmode="Static" runat="server" style="width: 100%; margin-top: 5px;
                        vertical-align: top; font-family: Arial,sans-serif; font-size: 15px;" class="chzn-select"
                        onchange="BindAddress();">
                    </select>
                </td>
            </tr>
            <tr>
                <th style="width: 10%; text-align: right; padding-top: 5px; vertical-align: top;"
                    class="AIPopupLabel">
                    Address :
                </th>
                <td style="width: 30%; vertical-align: top; padding: 5px 0px 0px 5px; font-family: Arial,sans-serif;
                    word-wrap: break-word; font-weight: normal; font-size: 15px;" rowspan="3">
                    <%= ViewState["ClientAddress"]%>
                </td>
                <th style="width: 20%; text-align: right; vertical-align: top; padding-top: 10px;"
                    class="AIPopupLabel" rowspan="3">
                    Address(s) :
                </th>
                <td style="width: 40%; vertical-align: top; padding: 5px 0px 0px 5px; font-weight: bold;
                    font-family: Arial,sans-serif; font-size: 15px;" rowspan="3">
                    <div id="divAddressList">
                    </div>
                </td>
            </tr>           
        </tbody>
    </table>
    <br />
    <center>
        <%--<asp:Button ID="btnAddNew" runat="server" Text="Add new" OnClientClick="ProceedAIAnanlysis('New');"/>
        <asp:Button ID="btnSelectFromList" runat="server" Text="Select from list"  OnClientClick="ProceedAIAnanlysis('FromList');"/>--%>
        <input id="btnAddNew" type="button" value="Add new" onclick="debugger; $('#hdnSender').val('New'); parent.MatchesFound.hide();"/>
        <input id="btnSelectFromList" type="button" value="Select from list" onclick="$('#hdnSender').val('List'); parent.MatchesFound.hide();"/>         
    </center>
    </form>
    <script type="text/javascript">
        //$("#ddlClient").chosen();
        BindAddress();
    </script>
</body>
</html>
