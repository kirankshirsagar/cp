﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AIAnalysisResult.aspx.cs" Inherits="ArtificialIntelligence_AIAnalysisResult" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ContractPod AI</title>
    <!-- Google font including -->
    <link href="https://fonts.googleapis.com/css?family=Roboto|Ubuntu:700" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <!-- Style CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
>
<body>
    <form id="form1" runat="server">
    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 clearfix">
                    <a href="../WorkFlow/home.aspx" class="home-button"><i class="fa fa-home"></i></a>
                    <div class="logo-box clearfix">
                        <div class="main-logo">
                            <a href="/">
                                <img src="img/contractpod-ai.png" alt="ContractPod AI"></a>
                        </div>
                        <div class="powered-by">
                            <img src="img/with_watson_badge_primary_white.png" alt="ContractPod AI">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="analyzed-message">
        <div class="container">
            <div class="analyzed-box">
                <div class="a-success" id="divSuccess" runat="server">
                    <h3>
                        All Done!
                    </h3>
                    <p>
                        I have analysed your contract and created your contract snapshot
                        <br>
                        To view click below on View Contract Snapshot.
                        <br>
                        If you need help with another contract record, click below on Analyse New Contract
                    </p>
                    <div class="a-success-buttons">
                        <ul>
                            <li><a href="#" runat="server" id="ViewSnapshot">View Contract Snapshot</a></li>
                            <li><a href="AIAnalysis.aspx">Analyze New Contract</a></li>
                        </ul>
                    </div>
                </div>
                <!-- analyse success -->
                <div class="a-fail" id="divFail" runat="server">
                    <h3>
                        Sorry! There has an error processing your contract. Please try again.
                    </h3>
                    <div class="a-success-buttons">
                        <ul>
                            <li><a href="AIAnalysis.aspx">Analyze Again</a></li>
                        </ul>
                    </div>
                </div>
                <!-- analyse failed -->
            </div>
        </div>
    </div>  
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easypiechart.min.js"></script>
    <script src="js/script.js"></script>

     <asp:HiddenField ID="hdnRequestId" runat="server" />
    </form>
</body>
</html>
