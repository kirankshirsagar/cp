// JavaScript Written by Ariful Ahsan
//Author Portfolio http://omi.technology

$(document).ready(function(e) {
	$('#chart1, #chart4').easyPieChart({
        lineWidth: 12,
		lineCap: 'round',
		barColor: '#212020',
		trackColor: '#d4e3df',
		scaleColor: false,
		size: 130,
    });
	
	$('#chart2, #chart3, #chart5').easyPieChart({
        lineWidth: 12,
		lineCap: 'round',
		barColor: '#00c3b9',
		trackColor: '#d2d4d8',
		scaleColor: false,
		size: 130,
    });
});