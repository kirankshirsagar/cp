﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//using IBM.WatsonDeveloperCloud.Discovery.v1.Model;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Newtonsoft.Json;
using System.IO;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public partial class ArtificialIntelligence_DiscoverySearch : System.Web.UI.Page
{



    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string SearchResult = File.ReadAllText(Server.MapPath("Discovery JSON.txt"));
        var obj = JObject.Parse(SearchResult);
        JObject p = JObject.Parse(SearchResult);
        JavaScriptSerializer jss = new JavaScriptSerializer();
        passages v = (passages)jss.Deserialize(SearchResult, typeof(passages));

        //Newtonsoft.Json.Linq.JToken jt= 
        //passages pa = JsonConvert.DeserializeObject<passages>(SearchResult);
        //Newtonsoft.Json.Linq.JToken jt = (new System.Linq.SystemCore_EnumerableDebugView<Newtonsoft.Json.Linq.JToken>(p)).Items[1];


        List<passages> items = JsonConvert.DeserializeObject<List<passages>>(SearchResult);

        // rptMetaDataSearch.DataSource = result;
        //rptMetaDataSearch.DataBind();

        PaginationButtons1.VisibleButtonNumbers = 5;
        PaginationButtons1.TotalRecord = 100;

        //SendDocumentRequest();
    }

    private static readonly Encoding Encoding = Encoding.UTF8;
    public string username = "3040c955-bafc-4091-8147-69af804e9475";
    public string password = "awWIVnlnSENK";
    public string endpoint = "https://gateway.watsonplatform.net/discovery/api/v1";


    private static string _createdEnvironmentId = "5d5535df-7b09-4ce1-8eec-ed20643a5290";
    private static string _createdConfigurationId = "1d2c7e98-8007-4a30-b525-d5acc3beecc1";
    private static string _createdCollectionId = "311dd2bc-8f24-4526-bbba-523c9257b83c";
    private static string _createdDocumentId;

    private string _naturalLanguageQuery = "Majeure";
    AutoResetEvent autoEvent = new AutoResetEvent(false);
    //public DiscoveryService _discovery;

    public void SendDocumentRequest()
    {
        // Create a request for the URL.   
        WebRequest request = WebRequest.Create(endpoint);
        // If required by the server, set the credentials.  
        request.Credentials = CredentialCache.DefaultCredentials;

        // Get the response.  
        WebResponse response = request.GetResponse();
        // Display the status.  
        Console.WriteLine(((HttpWebResponse)response).StatusDescription);
        // Get the stream containing content returned by the server.  
        Stream dataStream = response.GetResponseStream();
        // Open the stream using a StreamReader for easy access.  
        StreamReader reader = new StreamReader(dataStream);
        // Read the content.  
        string responseFromServer = reader.ReadToEnd();
        // Display the content.  
        Console.WriteLine(responseFromServer);
        // Clean up the streams and the response.  
        reader.Close();
        response.Close();
    }


    //public DiscoveryServiceExample(string username, string password)
    //   {
    //       _discovery = new DiscoveryService(username, password, DiscoveryService.DISCOVERY_VERSION_DATE_2016_12_01);
    //       _discovery.Endpoint = "https://gateway.watsonplatform.net/discovery/api";


    //       Query();

    //       Console.WriteLine("\n~ Discovery search complete.");
    //   }


    //private void Query()
    //        {
    //            var result = _discovery.Query(_createdEnvironmentId, _createdCollectionId, null, null, _naturalLanguageQuery, true);

    //            if (result != null)
    //            {
    //                Console.WriteLine(JsonConvert.SerializeObject(result, Formatting.Indented));
    //            }
    //            else
    //            {
    //                Console.WriteLine("result is null.");
    //            }
    //}
}
public class passages
{
    public string document_id { get; set; }
    public string passage_score { get; set; }
    public string passage_text { get; set; }
    public string start_offset { get; set; }
    public string end_offset { get; set; }
    public string field { get; set; }
}

