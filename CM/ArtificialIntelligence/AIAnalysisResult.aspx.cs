﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonBLL;

public partial class ArtificialIntelligence_AIAnalysisResult : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //hdnRequestId.Value = Request.extValue("hdnRequestId");
            hdnRequestId.Value = Request.QueryString["RequestId"];

            if (!string.IsNullOrEmpty(hdnRequestId.Value))
            {
                ViewSnapshot.HRef = "~/Workflow/RequestFlow.aspx?Status=Workflow&RequestId=" + hdnRequestId.Value;
                divSuccess.Visible = true;
                divFail.Visible = false;
            }
            else
            {
                ViewSnapshot.Visible = false;
                divSuccess.Visible = false;
                divFail.Visible = true;                
            }
        }
    }
}