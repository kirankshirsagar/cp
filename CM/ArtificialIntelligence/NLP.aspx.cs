﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using CommonBLL;
using WorkflowBLL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;

public partial class AI_NLP : System.Web.UI.Page
{
    private static readonly Encoding Encoding = Encoding.UTF8;
    static string ContractingParty = "";
    static string ClientName = "";
    static string ContractType = "";
    static string ClientAddress = "";
    static string CountryName = "";

    static string flagTermnationforMaterialBreach = "N";
    static string flagTermnationforInsolvency = "N";
    static string flagTerminationforChangeofControl = "N";
    static string flagTerminationforConvenience = "N";

    static string flagThirdPartyGuarantee = "N";
    static string flagEntireAgreement = "N";
    static string flagStrictLiability = "N";
    static string flagChangeOfControl = "N";

    static string Indemnity = "";
    static string Insurance = "";
    static string LiabilityCap = "Uncapped Liablity";
    static string Assignment = "";

    static string EffectiveDate = "";
    static string ExpiryDate = "";
    static decimal AIFileSize = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    string SaveDocument()
    {
        string docpath = "";
        string location = Server.MapPath("~/Uploads/" + Session["TenantDIR"] + "/AINLP/");
        FileInfo fi = new FileInfo(FileUpload1.PostedFile.FileName);
        string FileName = fi.Name.Replace(fi.Extension, "") + "_" + DateTime.Now.ToString("ddMMyyyHHmmss") + fi.Extension;
        docpath = location + "\\" + FileName;
        if (FileUpload1.HasFile)
        {
            FileUpload1.SaveAs(docpath);
        }
        return docpath;
    }

    protected void Call_Click(object sender, EventArgs e)
    {

        string url = "http://49.248.112.163/ainlp/API/Upload";
        string filePath = SaveDocument();
        var response = SendDocumentRequest(url, filePath);

        JavaScriptSerializer jss = new JavaScriptSerializer();
        string v = (string)jss.Deserialize(response, typeof(string));
        dynamic data = JObject.Parse(v);

        lblJSONData.Text = "";
        btnCancel.Visible = true;

        SetValues(data);
        PrintCustomiseJSON();
    }

    protected void btnProceed_Click(object sender, EventArgs e)
    {
        InsertUpdate();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        lblJSONData.Text = "";
        btnCancel.Visible = false;
    }

    void SetValues(dynamic data)
    {
        btnProceed.Visible = true;
        if (data.knowledgeStudio != null)
        {

            if (data.knowledgeStudio.contracttype != null)
            {
                ContractType = data.knowledgeStudio.contracttype.Value;
                //btnProceed.Visible = true;
            }
            else
            {
                //btnProceed.Visible = true;
            }

            if (data.knowledgeStudio.contractingparty != null)
            {
                ContractingParty = data.knowledgeStudio.contractingparty.Value;
            }
            else
            {
                ContractingParty = "";
            }

            if (data.knowledgeStudio.thirdpartyname != null)
            {
                ClientName = data.knowledgeStudio.thirdpartyname.Value;
            }
            else
            {
                ClientName = "";
            }
            if (data.knowledgeStudio.thirdpartyaddress != null)
            {
                ClientAddress = data.knowledgeStudio.thirdpartyaddress.Value;
            }

            if (data.knowledgeStudio.countryname != null)
            {
                CountryName = data.knowledgeStudio.countryname.Value;
            }

            if (data.knowledgeStudio.entireagreement != null)
            {
                flagEntireAgreement = "Y";
            }


            if (data.knowledgeStudio.strictliability != null)
            {
                flagStrictLiability = "Y";
            }

            if (data.knowledgeStudio.thirdpartyguarantee != null)
            {
                flagThirdPartyGuarantee = "Y";
            }

            //if (data.knowledgeStudio.changeofcontrol != null) // Debayan requested to change 
            if (data.knowledgeStudio.terminationforchangeofcontrol != null)
            {
                flagChangeOfControl = "Y";
            }

            //if (data.knowledgeStudio.terminationforinsolvency != null)
            if (data.knowledgeStudio.insolvency != null)
            {
                flagTermnationforInsolvency = "Y";
            }

            if (data.knowledgeStudio.terminationforchangeofcontrol != null)
            {
                flagTerminationforChangeofControl = "Y";
            }

            //if (data.knowledgeStudio.terminationformaterialbreach != null)
            if (data.knowledgeStudio.termnationformaterialbreach != null)
            {
                flagTermnationforMaterialBreach = "Y";
            }

            if (data.knowledgeStudio.terminationforconvenience != null)
            {
                flagTerminationforConvenience = "Y";
            }

            if (data.knowledgeStudio.effectivedate != null)
            {
                EffectiveDate = data.knowledgeStudio.effectivedate.Value;
            }

            if (data.knowledgeStudio.expirydate != null)
            {
                ExpiryDate = data.knowledgeStudio.expirydate.Value;
            }

            if (data.knowledgeStudio.indemnity != null)
            {
                Indemnity = "The agreement contains Indemnity provision";
            }
            if (data.knowledgeStudio.insurance != null)
            {
                Insurance = data.knowledgeStudio.indemnity.Value.Trim().Replace("'", ""); ;
            }            

            // For Insurance
            if (data.knowledgeStudio.generalliability != null)
            {
                Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudio.generalliability.Value : data.knowledgeStudio.generalliability.Value;
            }
            if (data.knowledgeStudio.otherinsurance != null)
            {
                Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudio.otherinsurance.Value : data.knowledgeStudio.otherinsurance.Value;
            }
            if (data.knowledgeStudio.waiverofsubrogation != null)
            {
                Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudio.waiverofsubrogation.Value : data.knowledgeStudio.waiverofsubrogation.Value;
            }
            if (data.knowledgeStudio.professionalliability != null)
            {
                Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudio.professionalliability.Value : data.knowledgeStudio.professionalliability.Value;
            }
            if (data.knowledgeStudio.employerliability != null)
            {
                Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudio.employerliability.Value : data.knowledgeStudio.employerliability.Value;
            }
            if (data.knowledgeStudio.workerscompensation != null)
            {
                Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudio.workerscompensation.Value : data.knowledgeStudio.workerscompensation.Value;
            }
            if (data.knowledgeStudio.automobileliability != null)
            {
                Insurance += !string.IsNullOrEmpty(Insurance) ? ", " + data.knowledgeStudio.automobileliability.Value : data.knowledgeStudio.automobileliability.Value;
            }
        }
        //Indemnity = data.indemnity.Value.Trim().Replace("'", "");
        //Insurance = data.insurance.Value.Trim().Replace("'", "");
        //LiabilityCap = data.liability.Value.Trim().Replace("'", "");
        Assignment = data.assignment.Value.Trim().Replace("'", "");
        if (data.liability != null)
        {
            if (!string.IsNullOrEmpty(data.liability.Value))
            {
                LiabilityCap = data.liability.Value.Trim().Replace("'", "");
            }
        }
    }

    public static string SendDocumentRequest(string url, string filePath)
    {
        // Reference: https://briangrinstead.com/blog/multipart-form-post-in-c/
        var webRequest = WebRequest.Create(url);
        webRequest.Method = "POST";

        // form data formatting

        string boundary = string.Format("----------{0:N}", Guid.NewGuid());
        webRequest.ContentType = "multipart/form-data; boundary=" + boundary;

        // define content boundary
        Stream formDataStream = new MemoryStream();
        var fileName = filePath.Substring(filePath.LastIndexOf('\\') + 1);
        string header = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"document\"; filename=\"{1}\"\r\nContent-Type: application/octet-stream\r\n\r\n", boundary, fileName);

        formDataStream.Write(Encoding.GetBytes(header), 0, Encoding.GetByteCount(header));

        // write the file contents to the form stream in between boundaries
        var fileBytes = File.ReadAllBytes(filePath);
        formDataStream.Write(fileBytes, 0, fileBytes.Length);

        // indicate end of file boundary
        string footer = "\r\n--" + boundary + "--\r\n";
        formDataStream.Write(Encoding.GetBytes(footer), 0, Encoding.GetByteCount(footer));

        // output the formDataStream into a byte[] to be sent with the request
        formDataStream.Position = 0;
        byte[] formData = new byte[formDataStream.Length];
        formDataStream.Read(formData, 0, formData.Length);
        formDataStream.Close();

        // send the form data in the request to the upload API
        using (var requestStream = webRequest.GetRequestStream())
        {
            requestStream.Write(formData, 0, formData.Length);
            requestStream.Flush();
            requestStream.Close();
        }

        // read response from API server
        using (var responseStream = webRequest.GetResponse().GetResponseStream())
        {
            using (var reader = new StreamReader(responseStream))
            {
                var content = reader.ReadToEnd();
                return content;
            }
        }
    }

    void PrintCustomiseJSON()
    {
        lblJSONData.Text = "<br/><b>Contract Type</b> : <span>" + ContractType + "</span><br/>";
        lblJSONData.Text += "<br/><b>Contracting Party</b> : <span>" + ContractingParty + "</span><br/>";
        lblJSONData.Text += "<br/><b>Customer Name</b> : <span>" + ClientName + "</span><br/>";
        lblJSONData.Text += "<br/><b>Customer Address</b> : <span>" + ClientAddress + "</span><br/>";
        lblJSONData.Text += "<br/><b>Country Name</b> : <span>" + CountryName + "</span><br/>";

        lblJSONData.Text = lblJSONData.Text + "<br/><b>Effective Date</b> : <span>" + EffectiveDate + "</span><br/>";
        lblJSONData.Text = lblJSONData.Text + "<br/><b>Expiry Date</b> : <span>" + ExpiryDate + "</span><br/>";

        lblJSONData.Text += "<br/><b>Entire Agreement</b> : <span>" + (flagEntireAgreement.Equals("Y") ? "Yes" : "No") + "</span><br/>";
        lblJSONData.Text += "<br/><b>Strict LiabilityCap</b> : <span>" + (flagStrictLiability.Equals("Y") ? "Yes" : "No") + "</span><br/>";
        lblJSONData.Text += "<br/><b>Third Party Guarantee</b> : <span>" + (flagThirdPartyGuarantee.Equals("Y") ? "Yes" : "No") + "</span><br/>";
        lblJSONData.Text += "<br/><b>Change of Control</b> : <span>" + (flagChangeOfControl.Equals("Y") ? "Yes" : "No") + "</span><br/>";

        lblJSONData.Text += "<br/><b>Termination For Insolvency</b> : <span>" + (flagTermnationforInsolvency.Equals("Y") ? "Yes" : "No") + "</span><br/>";
        lblJSONData.Text += "<br/><b>Termination For Change of Control</b> : <span>" + (flagTerminationforChangeofControl.Equals("Y") ? "Yes" : "No") + "</span><br/>";
        lblJSONData.Text += "<br/><b>Termination For Material Breach </b> : <span>" + (flagTermnationforMaterialBreach.Equals("Y") ? "Yes" : "No") + "</span><br/>";
        lblJSONData.Text += "<br/><b>Termination For Convenience</b> : <span>" + (flagTerminationforConvenience.Equals("Y") ? "Yes" : "No") + "</span><br/>";

        lblJSONData.Text = lblJSONData.Text + "<br/><b>Indemnity</b> : <span>" + Indemnity + "</span><br/>";
        lblJSONData.Text = lblJSONData.Text + "<br/><b>LiabilityCap</b> : <span>" + LiabilityCap + "</span><br/>";
        lblJSONData.Text = lblJSONData.Text + "<br/><b>Insurance</b> : <span>" + Insurance + "</span><br/>";
        lblJSONData.Text = lblJSONData.Text + "<br/><b>Assignment</b> : <span>" + Assignment + "</span><br/>";

        ResetVariables();
    }

    void InsertUpdate()
    {
        //ContractType = "Services agreement";
        //ClientName = "Test Usil";
        //ContractingParty = "Standard Textile Co Inc.";

        Page.TraceWrite("Insert, Update starts.");
        string status = "";
        string val = "0";
        IContractRequest objcontractreq = new ContractRequest();
        objcontractreq.ContractTypeName = ContractType;
        objcontractreq.ContractingpartyName = ContractingParty;
        objcontractreq.ClientName = ClientName;
        objcontractreq.Street1 = ClientAddress;
        objcontractreq.RequestUserID = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;

        objcontractreq.AssignToId = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objcontractreq.DepartmentId = Convert.ToInt32(Session[Declarations.Department]);
        objcontractreq.RequestType = "cnc";
        objcontractreq.ContractingpartyName = ContractingParty;
        objcontractreq.isCustomer = "C";

        objcontractreq.AddedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objcontractreq.ModifiedBy = Session[Declarations.User] != null ? int.Parse(Session[Declarations.User].ToString()) : 0;
        objcontractreq.IpAddress = Session[Declarations.IP] != null ? Session[Declarations.IP].ToString() : null;
        objcontractreq.IsCustomerPortalEnable = "N";
        objcontractreq.Priority = "N";

        try
        {
            objcontractreq.RequestID = 0;
            status = objcontractreq.AIInsertRecord();
            IKeyFields kf = new KeyFeilds();
            kf.RequestId = Convert.ToInt32(status);
            kf.Indemnity = Indemnity;
            kf.LiabilityCap = LiabilityCap;
            kf.Insurance = Insurance;
            kf.Indemnity = Indemnity;
            kf.TerminationConvenience = flagTerminationforConvenience;
            kf.TerminationinChangeofcontrol = flagTerminationforChangeofControl;
            kf.Terminationinsolvency = flagTermnationforInsolvency;
            kf.Terminationmaterial = flagTermnationforMaterialBreach;
            kf.IsAgreementClause = flagEntireAgreement;
            kf.IsChangeofControl = flagChangeOfControl;
            kf.IsForCustomer = flagThirdPartyGuarantee;
            kf.IsStrictliability = flagStrictLiability;
            kf.dtMatadataFieldValues = MetaData();
            kf.Param = 2;
            string KOStatus = kf.InsertRecord(); ;

            if (KOStatus == "1")
            {
                lblJSONData.Text = "Record saved.";
            }
            else
            {
                lblJSONData.Text = "Record failed to save.";
            }
        }

        catch (Exception ex)
        {
            Page.Message("0", val);
            Page.TraceWarn("Insert, Update fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }   

    public DataTable MetaData()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("MetaDataFieldId", Type.GetType("System.Int32"));
        dt.Columns.Add("FieldValue", Type.GetType("System.String"));
        return dt;
    }

    void ResetVariables()
    {
        ContractingParty = "";
        ClientName = "";
        ContractType = "";
        ClientAddress = "";
        CountryName = "";

        flagTermnationforMaterialBreach = "N";
        flagTermnationforInsolvency = "N";
        flagTerminationforChangeofControl = "N";
        flagTerminationforConvenience = "N";

        flagThirdPartyGuarantee = "N";
        flagEntireAgreement = "N";
        flagStrictLiability = "N";
        flagChangeOfControl = "N";

        Indemnity = "";
        Insurance = "";
        LiabilityCap = "Uncapped Liablity";
        Assignment = "";

        EffectiveDate = "";
        ExpiryDate = "";
        AIFileSize = 0;
    }
}

