﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MasterBLL;
using CommonBLL;
using System.Data;

public partial class Access_RolePermissions : System.Web.UI.Page
{
    ICountry objCountry;
    IState objState;
    

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateObjects();
        if (!IsPostBack)
        {
            DisplayCountryData();
            DisplayStateData();
        }
    }

    void CreateObjects()
    {
        
        Page.TraceWrite("Page object create starts.");
        try
        {
            objState = FactoryMaster.GetStateDetail();
            objCountry = FactoryMaster.GetCountryDetail();
        }
        catch (Exception ex)
        {
            Page.TraceWarn("create object fails.");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Page.TraceWrite("Page object create ends.");
    }

    public void DisplayCountryData()
    {   
        List<Country> Country = new List<Country>();
        Country = objCountry.ReadData();
        rptcountry.DataSource = Country;
        rptcountry.DataBind();
    }

    public void DisplayStateData()
    {
        List<State> State = new List<State>();
        State = objState.ReadData();
        for (int i = 0; i <= rptcountry.Items.Count - 1; i++)
        {
            Label id = (Label)rptcountry.Items[i].FindControl("lblcountryid");
            Repeater rptState = (Repeater)rptcountry.Items[i].FindControl("rptstate");
            var result = State.FindAll(x => x.CountryId == int.Parse(id.Text));
            rptState.DataSource = result;
            rptState.DataBind();
        }
    }

    protected void rptcountry_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        //    if (e.CommandName == "edit")
        //    {
        //        Repeater rptstate = (Repeater)e.Item.FindControl("rptstate");
        //        ((Label)e.Item.FindControl("lblcountryname")).Visible = false;
        //        ((TextBox)e.Item.FindControl("txtcountryname")).Visible = true;

        //        for (int i = 0; i <= rptstate.Items.Count - 1; i++)
        //        {
        //            ((Label)rptstate.Items[i].FindControl("lblstatename")).Visible = false;
        //            ((TextBox)rptstate.Items[i].FindControl("txtstatename")).Visible = true;
        //        }

        //        ((LinkButton)e.Item.FindControl("lnkEdit")).Visible = false;
        //        ((LinkButton)e.Item.FindControl("lnkUpdate")).Visible = true;
        //        ((LinkButton)e.Item.FindControl("lnkCancel")).Visible = true;

        //    }
        //    else if (e.CommandName == "update")
        //    {
        //        Repeater rptstate = (Repeater)e.Item.FindControl("rptstate");

        //        for (int i = 0; i <= rptstate.Items.Count - 1; i++)
        //        {
        //            Label lblStateid = (Label)rptstate.Items[i].FindControl("lblstateid");
        //            TextBox txtstatename = (TextBox)rptstate.Items[i].FindControl("txtstatename");
        //            //cm.select("update state set statename='" + txtstatename.Text + "' where stateid=" + lblStateid.Text + "");
        //        }

        //        TextBox txtcountryname = (TextBox)e.Item.FindControl("txtcountryname");
        //        //cm.select("update country set countryname='" + txtcountryname.Text + "' where countryid=" + e.CommandArgument + "");
        //        DisplayCountryData();
        //        DisplayStateData();
        //    }
        //    else if (e.CommandName == "cancel")
        //    {
        //        Repeater rptstate = (Repeater)e.Item.FindControl("rptstate");
        //        ((Label)e.Item.FindControl("lblcountryname")).Visible = true;
        //        ((TextBox)e.Item.FindControl("txtcountryname")).Visible = false;

        //        for (int i = 0; i <= rptstate.Items.Count - 1; i++)
        //        {
        //            ((Label)rptstate.Items[i].FindControl("lblstatename")).Visible = true;
        //            ((TextBox)rptstate.Items[i].FindControl("txtstatename")).Visible = false;
        //        }

        //        ((LinkButton)e.Item.FindControl("lnkEdit")).Visible = true;
        //        ((LinkButton)e.Item.FindControl("lnkUpdate")).Visible = false;
        //        ((LinkButton)e.Item.FindControl("lnkCancel")).Visible = false;
        //    }
    }
}