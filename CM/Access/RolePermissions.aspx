﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CM.master" AutoEventWireup="true"
    CodeFile="RolePermissions.aspx.cs" Inherits="Access_RolePermissions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div align="center">
        <table cellpadding="4" cellspacing="1">
            <tr>
                <td align="center" bgcolor="#FFFFCC">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Larger" ForeColor="#003300"
                        Text="Nested Repeater With Edit Delete"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Repeater ID="rptcountry" runat="server" OnItemCommand="rptcountry_ItemCommand">
                        <HeaderTemplate>
                            <table cellpadding="4" cellspacing="1" border="1">
                                <tr style="background-color: #333300; font-size: large; color: #FFFFCC; font-weight: bold;">
                                    <td>
                                        Country Name
                                    </td>
                                    <td>
                                        State Name
                                    </td>
                                    <td>
                                        Action
                                    </td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="lblcountryid" Visible="false" runat="server" Text='<%# Eval("countryid") %>'></asp:Label>
                                    <asp:Label ID="lblcountryname" runat="server" Text='<%# Eval("countryname") %>'></asp:Label>
                                    <asp:TextBox ID="txtcountryname" runat="server" Text='<%# Eval("countryname") %>'
                                        Visible="false"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Repeater ID="rptstate" runat="server">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblstateid" runat="server" Text='<%# Eval("stateid") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblstatename" runat="server" Text='<%#Eval("statename") %>'></asp:Label>
                                                        <asp:TextBox ID="txtstatename" runat="server" Text='<%#Eval("statename") %>' Visible="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandName="edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "countryid")%>'>Edit</asp:LinkButton>
                                    <asp:LinkButton Visible="false" ID="lnkUpdate" runat="server" CommandName="update"
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "countryid")%>'>Update</asp:LinkButton>
                                    <asp:LinkButton Visible="false" ID="lnkCancel" runat="server" CommandName="cancel"
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "countryid")%>'>Cancel</asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
